/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.cronograma;

import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.util.Util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class CronogramaAtividadeFaturamentoVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5796741790746846390L;

	private Long chavePrimaria;

	private int versao;

	private AtividadeSistema atividadeSistema;

	private Date dataInicio;

	private Date dataLimite;

	private Date dataPrevista;

	private Integer duracao;

	private Integer intervalo;

	private Boolean indicadorMarcado;

	private Collection<CronogramaRota> listaCronogramaRota;

	private String dataInicialAtividade;

	private String dataFinalAtividade;




	/**
	 * @return the atividadeSistema
	 */
	public AtividadeSistema getAtividadeSistema() {

		return atividadeSistema;
	}

	/**
	 * @param atividadeSistema
	 *            the atividadeSistema to set
	 */
	public void setAtividadeSistema(AtividadeSistema atividadeSistema) {

		this.atividadeSistema = atividadeSistema;
	}

	/**
	 * @return the dataLimite
	 */
	public Date getDataLimite() {
		Date data = null;
		if (this.dataLimite != null) {
			data = (Date) dataLimite.clone();
		}
		return data;
	}

	/**
	 * @param dataLimite
	 *            the dataLimite to set
	 */
	public void setDataLimite(Date dataLimite) {

		this.dataLimite = dataLimite;
	}

	/**
	 * @return dataLimite como String no padrão
	 *         Data BR
	 */
	public String getDataLimiteFormatada() {

		String dataFormatada = "";
		if(dataLimite != null) {
			dataFormatada = Util.converterDataParaString(dataLimite);
		}
		return dataFormatada;
	}

	/**
	 * @return the indicadorMarcado
	 */
	public Boolean getIndicadorMarcado() {

		return indicadorMarcado;
	}

	/**
	 * @param indicadorMarcado
	 *            the indicadorMarcado to set
	 */
	public void setIndicadorMarcado(Boolean indicadorMarcado) {

		this.indicadorMarcado = indicadorMarcado;
	}

	/**
	 * @return the dataInicio
	 */
	public Date getDataInicio() {
		Date data = null;
		if (this.dataInicio != null) {
			data = (Date) dataInicio.clone();
		}
		return data;
	}

	/**
	 * @param dataInicio
	 *            the dataInicio to set
	 */
	public void setDataInicio(Date dataInicio) {

		this.dataInicio = dataInicio;
	}

	/**
	 * @return the dataOrevista
	 */
	public Date getDataPrevista() {
		Date data = null;
		if (this.dataPrevista != null) {
			data = (Date) dataPrevista.clone();
		}
		return data;
	}

	/**
	 * @param dataPrevista
	 *            the dataPrevista to set
	 */
	public void setDataPrevista(Date dataPrevista) {

		this.dataPrevista = dataPrevista;
	}

	/**
	 * @return the duracao
	 */
	public Integer getDuracao() {

		return duracao;
	}

	/**
	 * @param duracao
	 *            the duracao to set
	 */
	public void setDuracao(Integer duracao) {

		this.duracao = duracao;
	}

	/**
	 * @return the intervalo
	 */
	public Integer getIntervalo() {

		return intervalo;
	}

	/**
	 * @param intervalo
	 *            the intervalo to set
	 */
	public void setIntervalo(Integer intervalo) {

		this.intervalo = intervalo;
	}

	/**
	 * @return dataPrevista como String no padrão
	 *         Data BR
	 */
	public String getDataPrevistaFormatada() {

		String dataFormatada = "";
		if(dataPrevista != null) {
			dataFormatada = Util.converterDataParaString(dataPrevista);
		}
		return dataFormatada;
	}

	/**
	 * @return
	 */
	public Integer getIntervaloDias() {

		Integer retorno = 0;

		if(dataLimite != null && dataInicio != null) {
			retorno = Util.intervaloDatas(dataInicio, dataLimite);
		}

		return retorno;
	}

	/**
	 * @return Lista de CronogramaRota da
	 *         AtividaSistema
	 */
	public Collection<CronogramaRota> getListaCronogramaRota() {

		return listaCronogramaRota;
	}

	/**
	 * @param listaCronogramaRota
	 *            the listaCronogramaRota to set
	 */
	public void setListaCronogramaRota(Collection<CronogramaRota> listaCronogramaRota) {

		this.listaCronogramaRota = listaCronogramaRota;
	}

	public String getDataInicialAtividade() {

		return dataInicialAtividade;
	}

	public void setDataInicialAtividade(String dataInicialAtividade) {

		this.dataInicialAtividade = dataInicialAtividade;
	}

	public String getDataFinalAtividade() {

		return dataFinalAtividade;
	}

	public void setDataFinalAtividade(String dataFinalAtividade) {

		this.dataFinalAtividade = dataFinalAtividade;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public int getVersao() {

		return versao;
	}

	public void setVersao(int versao) {

		this.versao = versao;
	}


}
