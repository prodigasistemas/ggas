package br.com.ggas.web.faturamento.creditodebito;

import java.io.Serializable;

/**
 * Método responsável pela representações dos valores utilizados 
 * nas operações de Crédito / Débito a Realizar
 * @author esantana
 *
 */
public class CreditoDebitoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -674391562143633146L;

	private Long chavePrimaria;
	
	private Long idCliente;
	
	private String documentoFormatado;
	
	private String nomeCompletoCliente;
	
	private String enderecoFormatadoCliente;
	
	private String emailCliente;
	
	private Long idImovel;
	
	private String indicadorCreditoDebito;
	
	private String nomeFantasiaImovel;
	
	private String matriculaImovel;
	
	private String cidadeImovel;
	
	private String enderecoImovel;
	
	private String nomeImovel;
	
	private Long status;
	
	private String codigoPontoConsumo;
	
	private String numeroDocumento;
	
	private Long[] chavesPrimarias;
	
	private String isAutorizacao;
	
	private Long idMotivoCancelamento;
	
	private Long idPontoConsumo;
	
	private String descricaoPontoConsumo;
	
	private String enderecoPontoConsumo;
	
	private String cepPontoConsumo;
	
	private String complementoPontoConsumo;
	
	private Long idRubrica;
	
	private String descricaoRubrica;
	
	private Long tipoAmortizacao;
	
	private Long idCreditoDebitoNegociado;
	
	private String descricaoOrigemCredito;
	
	private String valorEntrada;
	
	private String valor;
	
	private String[] valorQPNR;
	
	private String valorHidden;
	
	private String juros;
	
	private String periodicidadeJuros;
	
	private String tipoDocumento;
	
	private String numeroNotaFiscal;
	
	private String parcelas;
	
	private Long idPeriodicidade;
	
	private String descricaoPeriodicidade;
	
	private String taxaJuros;
	
	private Long idPeriodicidadeJuros;
	
	private String descricaoPeriodicidadeJuros;
	
	private String complementoDescricao;
	
	private String dataInicioCobranca;
	
	private String qtdDiasCobranca;
	
	private Long idInicioCobranca;
	
	private String descricaoEventoInicioCobranca;
	
	private String dataInicioCredito;
	
	private String valorUnitarioHidden;
	
	private Long idCreditoOrigem;
	
	private String quantidade;
	
	private String melhorDiaVencimento;
	
	private String indicadorPesquisa;

	private String numeroImovel;
	
	private String condominio;
	
	private String indicadorInicioCobranca;
	
	private String valorUnitario;
	
	private String dataVencimento;
	
	private String dataVencimentoInicial;
	
	private String dataVencimentoFinal;
	
	private Long idSituacao;
	
	private String pontoConsumoLegado;
	
	private String observacaoNota;
	
	private String valorTotal;
	
	private String saldo;
	
	private String dataEmissao;
	
	private String descricaoCancelamento;
	
	private String situacao;
	
	private String saldoQPNR;
	
	private Long[] idsApuracaoPenalidade;
	
	private String[] volumeRecuperacao;
	
	private Boolean telaHistorico;
	
	private String dataProrrogacao;
	
	private String observacoesNota;
	
	private String novaDataVencimento;
	
	private Long[] chavesNotasSelecionadas;
	
	private Long[] chavesPrimariasCreditos;
	
	private Long[] chavesPrimariasDebitos;
	
	public String getTaxaJuros() {
		return taxaJuros;
	}

	public Long getIdCreditoOrigem() {
		return idCreditoOrigem;
	}

	public void setIdCreditoOrigem(Long idCreditoOrigem) {
		this.idCreditoOrigem = idCreditoOrigem;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public String getMelhorDiaVencimento() {
		return melhorDiaVencimento;
	}

	public void setMelhorDiaVencimento(String melhorDiaVencimento) {
		this.melhorDiaVencimento = melhorDiaVencimento;
	}

	public void setTaxaJuros(String taxaJuros) {
		this.taxaJuros = taxaJuros;
	}

	public Long getIdPeriodicidadeJuros() {
		return idPeriodicidadeJuros;
	}

	public void setIdPeriodicidadeJuros(Long idPeriodicidadeJuros) {
		this.idPeriodicidadeJuros = idPeriodicidadeJuros;
	}

	public String getDescricaoPeriodicidadeJuros() {
		return descricaoPeriodicidadeJuros;
	}

	public void setDescricaoPeriodicidadeJuros(String descricaoPeriodicidadeJuros) {
		this.descricaoPeriodicidadeJuros = descricaoPeriodicidadeJuros;
	}

	public String getComplementoDescricao() {
		return complementoDescricao;
	}

	public void setComplementoDescricao(String complementoDescricao) {
		this.complementoDescricao = complementoDescricao;
	}

	public String getDataInicioCobranca() {
		return dataInicioCobranca;
	}

	public void setDataInicioCobranca(String dataInicioCobranca) {
		this.dataInicioCobranca = dataInicioCobranca;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	
	public String getNomeCompletoCliente() {
		return nomeCompletoCliente;
	}

	public Long getIdRubrica() {
		return idRubrica;
	}

	public void setIdRubrica(Long idRubrica) {
		this.idRubrica = idRubrica;
	}

	public String getDescricaoRubrica() {
		return descricaoRubrica;
	}

	public void setDescricaoRubrica(String descricaoRubrica) {
		this.descricaoRubrica = descricaoRubrica;
	}

	public void setNomeCompletoCliente(String nomeCompletoCliente) {
		this.nomeCompletoCliente = nomeCompletoCliente;
	}

	public String getEnderecoFormatadoCliente() {
		return enderecoFormatadoCliente;
	}

	public void setEnderecoFormatadoCliente(String enderecoFormatadoCliente) {
		this.enderecoFormatadoCliente = enderecoFormatadoCliente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getEnderecoPontoConsumo() {
		return enderecoPontoConsumo;
	}

	public void setEnderecoPontoConsumo(String enderecoPontoConsumo) {
		this.enderecoPontoConsumo = enderecoPontoConsumo;
	}

	public String getCepPontoConsumo() {
		return cepPontoConsumo;
	}

	public void setCepPontoConsumo(String cepPontoConsumo) {
		this.cepPontoConsumo = cepPontoConsumo;
	}

	public String getComplementoPontoConsumo() {
		return complementoPontoConsumo;
	}

	public void setComplementoPontoConsumo(String complementoPontoConsumo) {
		this.complementoPontoConsumo = complementoPontoConsumo;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public String getIndicadorCreditoDebito() {
		return indicadorCreditoDebito;
	}

	public void setIndicadorCreditoDebito(String indicadorCreditoDebito) {
		this.indicadorCreditoDebito = indicadorCreditoDebito;
	}

	public String getNomeFantasiaImovel() {
		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {
		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getMatriculaImovel() {
		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	public String getCidadeImovel() {
		return cidadeImovel;
	}

	public void setCidadeImovel(String cidadeImovel) {
		this.cidadeImovel = cidadeImovel;
	}

	public String getEnderecoImovel() {
		return enderecoImovel;
	}

	public void setEnderecoImovel(String enderecoImovel) {
		this.enderecoImovel = enderecoImovel;
	}

	public String getNomeImovel() {
		return nomeImovel;
	}

	public void setNomeImovel(String nomeImovel) {
		this.nomeImovel = nomeImovel;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getCodigoPontoConsumo() {
		return codigoPontoConsumo;
	}

	public void setCodigoPontoConsumo(String codigoPontoConsumo) {
		this.codigoPontoConsumo = codigoPontoConsumo;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Long[] getChavesPrimarias() {
		Long[] retorno = null;
		if (this.chavesPrimarias != null) {
			retorno = this.chavesPrimarias.clone();
		}
		return retorno;
	}

	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if(chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		} else {
			this.chavesPrimarias = null;
		}
	}

	public String getIsAutorizacao() {
		return isAutorizacao;
	}

	public void setIsAutorizacao(String isAutorizacao) {
		this.isAutorizacao = isAutorizacao;
	}

	public Long getIdMotivoCancelamento() {
		return idMotivoCancelamento;
	}

	public void setIdMotivoCancelamento(Long idMotivoCancelamento) {
		this.idMotivoCancelamento = idMotivoCancelamento;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public String getDocumentoFormatado() {
		return documentoFormatado;
	}

	public void setDocumentoFormatado(String documentoFormatado) {
		this.documentoFormatado = documentoFormatado;
	}

	public Long getTipoAmortizacao() {
		return tipoAmortizacao;
	}

	public void setTipoAmortizacao(Long tipoAmortizacao) {
		this.tipoAmortizacao = tipoAmortizacao;
	}

	public Long getIdCreditoDebitoNegociado() {
		return idCreditoDebitoNegociado;
	}

	public void setIdCreditoDebitoNegociado(Long idCreditoDebitoNegociado) {
		this.idCreditoDebitoNegociado = idCreditoDebitoNegociado;
	}

	public String getDescricaoOrigemCredito() {
		return descricaoOrigemCredito;
	}

	public void setDescricaoOrigemCredito(String descricaoOrigemCredito) {
		this.descricaoOrigemCredito = descricaoOrigemCredito;
	}

	public String getValorEntrada() {
		return valorEntrada;
	}

	public void setValorEntrada(String valorEntrada) {
		this.valorEntrada = valorEntrada;
	}

	public String getJuros() {
		return juros;
	}

	public void setJuros(String juros) {
		this.juros = juros;
	}

	public String getPeriodicidadeJuros() {
		return periodicidadeJuros;
	}

	public void setPeriodicidadeJuros(String periodicidadeJuros) {
		this.periodicidadeJuros = periodicidadeJuros;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getParcelas() {
		return parcelas;
	}

	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}

	public Long getIdPeriodicidade() {
		return idPeriodicidade;
	}

	public void setIdPeriodicidade(Long idPeriodicidade) {
		this.idPeriodicidade = idPeriodicidade;
	}

	public String getDescricaoPeriodicidade() {
		return descricaoPeriodicidade;
	}

	public void setDescricaoPeriodicidade(String descricaoPeriodicidade) {
		this.descricaoPeriodicidade = descricaoPeriodicidade;
	}

	public String getQtdDiasCobranca() {
		return qtdDiasCobranca;
	}

	public void setQtdDiasCobranca(String qtdDiasCobranca) {
		this.qtdDiasCobranca = qtdDiasCobranca;
	}

	public Long getIdInicioCobranca() {
		return idInicioCobranca;
	}

	public void setIdInicioCobranca(Long idInicioCobranca) {
		this.idInicioCobranca = idInicioCobranca;
	}

	public String getDescricaoEventoInicioCobranca() {
		return descricaoEventoInicioCobranca;
	}

	public void setDescricaoEventoInicioCobranca(String descricaoEventoInicioCobranca) {
		this.descricaoEventoInicioCobranca = descricaoEventoInicioCobranca;
	}

	public String getDataInicioCredito() {
		return dataInicioCredito;
	}

	public void setDataInicioCredito(String dataInicioCredito) {
		this.dataInicioCredito = dataInicioCredito;
	}

	public String getValorHidden() {
		return valorHidden;
	}

	public void setValorHidden(String valorHidden) {
		this.valorHidden = valorHidden;
	}

	public String getValorUnitarioHidden() {
		return valorUnitarioHidden;
	}

	public void setValorUnitarioHidden(String valorUnitarioHidden) {
		this.valorUnitarioHidden = valorUnitarioHidden;
	}

	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}

	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getCondominio() {
		return condominio;
	}

	public void setCondominio(String condominio) {
		this.condominio = condominio;
	}

	public String getIndicadorInicioCobranca() {
		return indicadorInicioCobranca;
	}

	public void setIndicadorInicioCobranca(String indicadorInicioCobranca) {
		this.indicadorInicioCobranca = indicadorInicioCobranca;
	}

	public String getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(String valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public String getDataVencimentoInicial() {
		return dataVencimentoInicial;
	}

	public void setDataVencimentoInicial(String dataVencimentoInicial) {
		this.dataVencimentoInicial = dataVencimentoInicial;
	}

	public String getDataVencimentoFinal() {
		return dataVencimentoFinal;
	}

	public void setDataVencimentoFinal(String dataVencimentoFinal) {
		this.dataVencimentoFinal = dataVencimentoFinal;
	}

	public Long getIdSituacao() {
		return idSituacao;
	}

	public void setIdSituacao(Long idSituacao) {
		this.idSituacao = idSituacao;
	}

	public String getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}

	public void setPontoConsumoLegado(String pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}

	public String getObservacaoNota() {
		return observacaoNota;
	}

	public void setObservacaoNota(String observacaoNota) {
		this.observacaoNota = observacaoNota;
	}

	public String getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getDescricaoCancelamento() {
		return descricaoCancelamento;
	}

	public void setDescricaoCancelamento(String descricaoCancelamento) {
		this.descricaoCancelamento = descricaoCancelamento;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getSaldoQPNR() {
		return saldoQPNR;
	}

	public void setSaldoQPNR(String saldoQPNR) {
		this.saldoQPNR = saldoQPNR;
	}

	public Long[] getIdsApuracaoPenalidade() {
		Long[] idsApuracao = null;
		if(this.idsApuracaoPenalidade != null) {
			idsApuracao = this.idsApuracaoPenalidade.clone();
		}
		return idsApuracao;
	}

	public void setIdsApuracaoPenalidade(Long[] idsApuracaoPenalidade) {
		if(idsApuracaoPenalidade != null) {
			this.idsApuracaoPenalidade = idsApuracaoPenalidade.clone();
		} else {
			this.idsApuracaoPenalidade = null;
		}
	}

	public String[] getValorQPNR() {
		String[] valor = null;
		if(this.valorQPNR != null) {
			valor = this.valorQPNR.clone();
		}
		return valor;
	}

	public void setValorQPNR(String[] valorQPNR) {
		if(valorQPNR != null) {
			this.valorQPNR = valorQPNR.clone();
		} else {
			this.valorQPNR = null;
		}
	}

	public String[] getVolumeRecuperacao() {
		String[] volume = null;
		if(this.volumeRecuperacao != null) {
			volume = this.volumeRecuperacao.clone();
		}
		return volume;
	}

	public void setVolumeRecuperacao(String[] volumeRecuperacao) {
		if(volumeRecuperacao != null) {
			this.volumeRecuperacao = volumeRecuperacao.clone();
		} else {
			this.volumeRecuperacao = null;
		}
	}

	public Boolean getTelaHistorico() {
		return telaHistorico;
	}

	public void setTelaHistorico(Boolean telaHistorico) {
		this.telaHistorico = telaHistorico;
	}

	public String getDataProrrogacao() {
		return dataProrrogacao;
	}

	public void setDataProrrogacao(String dataProrrogacao) {
		this.dataProrrogacao = dataProrrogacao;
	}

	public String getObservacoesNota() {
		return observacoesNota;
	}

	public void setObservacoesNota(String observacoes) {
		this.observacoesNota = observacoes;
	}

	public String getNovaDataVencimento() {
		return novaDataVencimento;
	}

	public void setNovaDataVencimento(String novaDataVencimento) {
		this.novaDataVencimento = novaDataVencimento;
	}

	public Long[] getChavesNotasSelecionadas() {
		Long[] chavesNotas = null;
		if(this.chavesNotasSelecionadas != null) {
			chavesNotas = this.chavesNotasSelecionadas.clone();
		} 
		return chavesNotas;
	}

	public void setChavesNotasSelecionadas(Long[] chavesNotasSelecionadas) {
		if(chavesNotasSelecionadas != null) {
			this.chavesNotasSelecionadas = chavesNotasSelecionadas.clone();
		} else {
			this.chavesNotasSelecionadas = null;
		}
	}

	public Long[] getChavesPrimariasCreditos() {
		Long[] chavesPrimarias = null;
		if(this.chavesPrimariasCreditos != null) {
			chavesPrimarias = this.chavesPrimariasCreditos.clone();
		} 
		return chavesPrimarias;
	}

	public void setChavesPrimariasCreditos(Long[] chavesPrimariasCreditos) {
		if(chavesPrimariasCreditos != null) {
			this.chavesPrimariasCreditos = chavesPrimariasCreditos.clone();
		} else {
			this.chavesPrimariasCreditos = null;
		}
	}

	public Long[] getChavesPrimariasDebitos() {
		Long[] chavesPrimarias = null;
		if(this.chavesPrimariasDebitos != null) {
			chavesPrimarias = this.chavesPrimariasDebitos.clone();
		} 
		return chavesPrimarias;
	}

	public void setChavesPrimariasDebitos(Long[] chavesPrimariasDebitos) {
		if(chavesPrimariasDebitos != null) {
			this.chavesPrimariasDebitos = chavesPrimariasDebitos.clone();
		} else {
			this.chavesPrimariasDebitos = null;
		}
	}
	
}
