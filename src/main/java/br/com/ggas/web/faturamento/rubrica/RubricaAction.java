/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.rubrica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.rubrica.RubricaValorRegulamentado;
import br.com.ggas.faturamento.rubrica.impl.RubricaImpl;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas a Rubrica
 *
 */
@Controller
public class RubricaAction extends GenericAction {

	private static final int CASAS_DECIMAIS = 2;

	private static final String RUBRICA = "rubrica";

	private static final String VALOR_DE_REFERENCIA = "Valor Referência";

	private static final String LISTA_RUBRICAS_VALOR_REGULAMENTADO = "listaRubricasValorRegulamentado";

	private static final String OBSERVACAO = "observacao";

	private static final String TRUE = "true";

	private static final String TRIBUTO_ATIVO = "tributoAtivo";

	private static final String LISTA_UNIDADE = "listaUnidade";

	private static final String VALOR_REGULAMENTADO_ATIVO = "valorRegulamentadoAtivo";

	private static final String ID_RUBRICA = "idRubrica";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String AMORTIZACAO = "amortizacao";

	private static final String FINANCIAMENTO_TIPO = "financiamentoTipo";

	private static final String LANCAMENTO_ITEM_CONTABIL = "lancamentoItemContabil";

	private static final String NOMECLATURA_COMUM_MERCOSUL = "nomeclaturaComumMercosul";

	private static final String ITEM_FATURA = "itemFatura";

	private static final String UNIDADE = "unidade";

	private static final String LISTA_AMORTIZACAO = "listaAmortizacao";

	private static final String LISTA_NOMECLATURA_COMUM_MERCOSUL = "listaNomeclaturaComumMercosul";

	private static final String LISTA_TRIBUTOS = "listaTributos";

	private static final String LISTA_ITEM_FATURA = "listaItemFatura";

	private static final String LISTA_TIPO_RUBRICA = "listaTipoRubrica";

	private static final String LISTA_LANCAMENTO_ITEM_CONTABIL = "listaLancamentoItemContabil";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String HABILITADO = "habilitado";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_IMPRESSAO = "descricaoImpressao";

	private static final String DATA_INICIO_VIGENCIA = "dataInicioVigencia";

	private static final String PERCENTUAL_MINIMO_ENTRADA = "percentualMinimoEntrada";

	private static final String VALOR_MAXIMO = "valorMaximo";

	private static final String INDICADOR_REGULAMENTADA = "indicadorRegulamentada";

	private static final String VALOR_REFERENCIA = "valorReferencia";

	private static final String ID_TRIBUTOS_ASSOCIADOS = "idTributosAssociados";

	private static final String LISTA_TRIBUTOS_ASSOCIADOS = "listaTributosAssociados";

	private static final String ID_ITEM_FATURA = "idItemFatura";

	private static final String ID_TIPO_RUBRICA = "idTipoRubrica";

	@Autowired
	private ControladorRubrica controladorRubrica;

	@Autowired
	private ControladorTributo controladorTributo;

	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	private ControladorUnidade controladorUnidade;

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Método responsável por exibir a tela relacionada a pesquisa de Rubrica
	 * 
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("exibirPesquisaRubrica")
	public String exibirPesquisaRubrica(HttpServletRequest request, Model model) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, true);
		}

		return "exibirPesquisaRubrica";
	}

	/**
	 * Método responsável por realizar a pesquisa de Rubrica.
	 * @param model - {@link Model}
	 * @param rubrica - {@link RubricaImpl}
	 * @param result - {@link BindingResult}
	 * @param request
	 * @param habilitado - {@link Boolean}
	 * @return tring - {@link String}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("pesquisarRubrica")
	public String pesquisarRubrica(Model model, RubricaImpl rubrica, BindingResult result, HttpServletRequest request,
			Boolean habilitado) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, rubrica, habilitado);
		Collection<Rubrica> rubricas = controladorRubrica.consultarRubrica(filtro);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(RUBRICA, rubrica);
		model.addAttribute("rubricas", rubricas);

		return exibirPesquisaRubrica(request, model);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro
	 *            - {@link Map}
	 * @param rubrica
	 *            - {@link RubricaImpl}
	 * @param habilitado
	 *            - {@link Boolean}
	 * @throws FormatoInvalidoException
	 *             - {@link FormatoInvalidoException}
	 */
	private void prepararFiltro(Map<String, Object> filtro, RubricaImpl rubrica, Boolean habilitado)
			throws FormatoInvalidoException {

		if (rubrica != null) {

			if (!StringUtils.isEmpty(rubrica.getDescricao())) {
				filtro.put(DESCRICAO, rubrica.getDescricao().toUpperCase().trim());
			}

			if (!StringUtils.isEmpty(rubrica.getDescricaoImpressao())) {
				filtro.put(DESCRICAO_IMPRESSAO, rubrica.getDescricaoImpressao().toUpperCase().trim());
			}

			if (rubrica.getIndicadorRegulamentada() != null) {
				filtro.put(INDICADOR_REGULAMENTADA, rubrica.getIndicadorRegulamentada());
			}
		}
		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Rubrica.
	 * 
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param rubricaTmp
	 *            - {@link RubricaImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoRubrica")
	public String exibirDetalhamentoRubrica(RubricaImpl rubricaTmp, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		RubricaImpl rubrica = rubricaTmp;
		request.getSession().removeAttribute(RUBRICA);
		salvarPesquisa(request, rubrica, RUBRICA);
		model.addAttribute(HABILITADO, Boolean.parseBoolean(request.getParameter(HABILITADO)));
		try {
			salvarPesquisa(request, rubrica, RUBRICA);
			rubrica = (RubricaImpl) controladorRubrica.obter(chavePrimaria, UNIDADE, ITEM_FATURA,
					LANCAMENTO_ITEM_CONTABIL, FINANCIAMENTO_TIPO, AMORTIZACAO, NOMECLATURA_COMUM_MERCOSUL);
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_RUBRICA, rubrica.getChavePrimaria());
			filtro.put(TRIBUTO_ATIVO, Boolean.valueOf(TRUE));
			popularForm(rubrica, request, model);
			if (rubrica.getIndicadorRegulamentada()) {
				model.addAttribute(LISTA_RUBRICAS_VALOR_REGULAMENTADO,
						controladorRubrica.consultarRubricasValorRegulamentado(filtro));
			}

			filtro.clear();
			filtro.put(ID_RUBRICA, rubrica);
			filtro.put(TRIBUTO_ATIVO, Boolean.valueOf(TRUE));
			model.addAttribute("listaRubricasTributo", controladorRubrica.consultarRubricasTributo(filtro));
			model.addAttribute(RUBRICA, rubrica);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return "exibirDetalhamentoRubrica";
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Rubrica.
	 * 
	 * @param rubrica
	 *            - {@link Rubrica}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@SuppressWarnings("unused")
	private void popularForm(Rubrica rubrica, HttpServletRequest request, Model model) throws GGASException {

		if(!rubrica.getIndicadorRegulamentada()) {
			
			if(rubrica.getValorReferencia() != null) {
				model.addAttribute(VALOR_REFERENCIA, Util.converterCampoValorDecimalParaString(VALOR_REFERENCIA,
						rubrica.getValorReferencia(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS));
			}
			
			if (rubrica.getValorMaximo() != null) {
				model.addAttribute(VALOR_MAXIMO, Util.converterCampoValorDecimalParaString(VALOR_MAXIMO,
						rubrica.getValorMaximo(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS));
			}
			
		} else {
			BigDecimal nValorReferencia = null;
			Date dDataInicioVigencia = null;

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_RUBRICA, rubrica.getChavePrimaria());
			filtro.put(VALOR_REGULAMENTADO_ATIVO, Boolean.valueOf(TRUE));
			
			Collection<RubricaValorRegulamentado> rubricasValorRegulamentado = controladorRubrica
					.consultarRubricasValorRegulamentado(filtro);
			for (RubricaValorRegulamentado rubricaValorRegulamentado : rubricasValorRegulamentado) {
				if (rubricaValorRegulamentado.getDataInicioVigencia() != null) {
					if (dDataInicioVigencia == null) {
						dDataInicioVigencia = rubricaValorRegulamentado.getDataInicioVigencia();
					}

					if (rubricaValorRegulamentado.getValorReferencia() != null) {
						nValorReferencia = rubricaValorRegulamentado.getValorReferencia();
					}

					if (rubricaValorRegulamentado.getObservacao() != null) {
						model.addAttribute(OBSERVACAO, rubricaValorRegulamentado.getObservacao());
					}
				}

			}
			
			if (nValorReferencia != null) {
				model.addAttribute(VALOR_REFERENCIA, Util.converterCampoValorDecimalParaString(VALOR_REFERENCIA,
						nValorReferencia, Constantes.LOCALE_PADRAO, CASAS_DECIMAIS));
			}
			
			if (dDataInicioVigencia != null) {
				model.addAttribute(DATA_INICIO_VIGENCIA,
						Util.converterDataParaStringSemHora(dDataInicioVigencia, Constantes.FORMATO_DATA_BR));
			}
		}
		
		if (rubrica.getValorMaximo() != null) {
			model.addAttribute(VALOR_MAXIMO, Util.converterCampoValorDecimalParaString(VALOR_MAXIMO,
					rubrica.getValorMaximo(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS));
		}

		if (rubrica.getPercentualMinimoEntrada() != null) {
			model.addAttribute(PERCENTUAL_MINIMO_ENTRADA,
					rubrica.getPercentualMinimoEntrada().multiply(BigDecimal.valueOf(100)).toBigInteger().toString());
		}
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Rubrica.
	 * 
	 * @param idTributosAssociados
	 *            - {@link Long}
	 * @param rubricaTmp
	 *            - {@link RubricaImpl}
	 * @param chavePrimaria
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @param result
	 *            - {@link BindingResult}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirAlterarRubrica")
	public String exibirAlterarRubrica(RubricaImpl rubricaTmp, BindingResult result,
			@RequestParam(value = ID_TRIBUTOS_ASSOCIADOS, required = false) Long[] idTributosAssociados,
			HttpServletRequest request, Model model) throws GGASException {
		carregarCampos(model);
		model.addAttribute(HABILITADO, Boolean.parseBoolean(request.getParameter(HABILITADO)));
		RubricaImpl rubrica = rubricaTmp;
		salvarPesquisa(request, rubrica, RUBRICA);
		if (!super.isPostBack(request)) {
			try {
				this.tratarCamposSelect(idTributosAssociados, request, model);
				rubrica = (RubricaImpl) controladorRubrica.obter(rubricaTmp.getChavePrimaria(), UNIDADE, ITEM_FATURA,
						LANCAMENTO_ITEM_CONTABIL, FINANCIAMENTO_TIPO, AMORTIZACAO, NOMECLATURA_COMUM_MERCOSUL);
				model.addAttribute(RUBRICA, rubrica);
				popularForm(rubrica, request, model);
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(ID_RUBRICA, rubrica);
				filtro.put(TRIBUTO_ATIVO, Boolean.TRUE);
				Collection<RubricaTributo> listaRubricaTributos = controladorRubrica.consultarRubricasTributo(filtro);

				if (listaRubricaTributos != null && !listaRubricaTributos.isEmpty()) {
					Collection<Tributo> listaIdRubricaTributo = new ArrayList<Tributo>();
					for (RubricaTributo rubricaTributo : listaRubricaTributos) {
						if (rubricaTributo.getDataFimVigencia() == null) {
							listaIdRubricaTributo.add(rubricaTributo.getTributo());
						}
					}

					model.addAttribute(ID_TRIBUTOS_ASSOCIADOS,
							Util.collectionParaArrayChavesPrimarias(listaIdRubricaTributo));
					Collection<Tributo> listaTributosDisp = controladorTributo.consultarTributos(null);
					listaTributosDisp.removeAll(listaIdRubricaTributo);
					model.addAttribute(LISTA_TRIBUTOS_ASSOCIADOS, listaIdRubricaTributo);
					model.addAttribute(LISTA_TRIBUTOS, listaTributosDisp);
				}
			} catch (GGASException e) {
				mensagemErroParametrizado(model, request, e);
			}

		}

		saveToken(request);

		return "exibirAlteracaoRubrica";
	}

	/**
	 * Alterar rubrica.
	 * 
	 * @param idTributosAssociados
	 *            - {@link Long}
	 * @param rubrica
	 *            - {@link RubricaImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("alterarRubrica")
	public String alterarRubrica(
			@RequestParam(value = ID_TRIBUTOS_ASSOCIADOS, required = false) Long[] idTributosAssociados,
			RubricaImpl rubrica, BindingResult result, HttpServletRequest request, Model model) throws GGASException {

		String view = "forward:exibirAlterarRubrica";
		model.addAttribute(RUBRICA, rubrica);
		// Valida o token para garantir que não
		// aconteça um duplo submit.
		try {

			if (rubrica.getValorRegulamentados() != null) {
				rubrica.getValorRegulamentados().clear();
			}

			if (rubrica.getTributos() != null) {
				rubrica.getTributos().clear();
			}

			popularRubrica(rubrica, request, model);
			validarToken(request);

			this.setarCamposSelectNoTributo(idTributosAssociados, rubrica, request);
			rubrica.setDadosAuditoria(getDadosAuditoria(request));
			rubrica.setUltimaAlteracao(Calendar.getInstance().getTime());
			controladorRubrica.validaItemFarturaRubrica(rubrica);
			controladorRubrica.atualizarRubrica(rubrica);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, Rubrica.RUBRICA_CAMPO_STRING);
			view = pesquisarRubrica(model, rubrica, result, request, rubrica.isHabilitado());
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de inclusão de Rubrica.
	 * 
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param idTributosAssociados
	 *            - {@link Long}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoRubrica")
	public String exibirInclusaoRubrica(
			@RequestParam(value = ID_TRIBUTOS_ASSOCIADOS, required = false) Long[] idTributosAssociados,
			HttpServletRequest request, Model model) throws GGASException {
		request.getSession().removeAttribute(RUBRICA);
		carregarCampos(model);
		this.tratarCamposSelect(idTributosAssociados, request, model);
		saveToken(request);

		return "exibirInclusaoRubrica";
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute(LISTA_LANCAMENTO_ITEM_CONTABIL, controladorRubrica.listarLancamentoItemContabil());
		model.addAttribute(LISTA_TIPO_RUBRICA, controladorRubrica.listarFinanciamentoTipo());
		model.addAttribute(LISTA_UNIDADE, controladorUnidade.listarUnidadesQuantidade());
		model.addAttribute(LISTA_ITEM_FATURA, controladorEntidadeConteudo.obterListaItemFatura());
		model.addAttribute(LISTA_TRIBUTOS, controladorTributo.consultarTributos(null));
		model.addAttribute(LISTA_AMORTIZACAO, controladorEntidadeConteudo.listarAmortizacoes());
		model.addAttribute(LISTA_NOMECLATURA_COMUM_MERCOSUL, controladorRubrica.listarNomeclaturaComumMercosul());
	}

	/**
	 * Método responsável por excluir as rubricas selecionados.
	 * 
	 * @param chavesPrimarias
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	@RequestMapping("removerRubrica")
	public String removerRubrica(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			request.getSession().removeAttribute(RUBRICA);
			controladorRubrica.validarRemoverRubrica(chavesPrimarias);
			controladorRubrica.removerRubrica(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, Rubrica.RUBRICA_CAMPO_STRING);
		} catch (NegocioException e) {
			mensagemErro(model, request, e);
		}

		return pesquisarRubrica(model, null, null, request, Boolean.TRUE);
	}

	/**
	 * Método responsável por incluir a Rubrica.
	 * 
	 * @param rubrica
	 *            - {@link RubricaImpl}
	 * @param result
	 *            - {@link BindingResult}
	 * @param idTributosAssociados
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @return String - {@link String}
	 */
	@RequestMapping("inserirRubrica")
	public String inserirRubrica(RubricaImpl rubrica, BindingResult result,
			@RequestParam(value = ID_TRIBUTOS_ASSOCIADOS, required = false) Long[] idTributosAssociados,
			HttpServletRequest request, Model model) {

		String view = "forward:exibirInclusaoRubrica";
		model.addAttribute(RUBRICA, rubrica);
		try {
			popularRubrica(rubrica, request, model);
			validarToken(request);

			this.setarCamposSelectNoTributo(idTributosAssociados, rubrica, request);

			rubrica.setDadosAuditoria(getDadosAuditoria(request));
			rubrica.setHabilitado(true);
			rubrica.setUltimaAlteracao(Calendar.getInstance().getTime());

			controladorRubrica.validaItemFarturaRubrica(rubrica);

			Long idRubrica = controladorRubrica.inserirRubrica(rubrica);

			model.addAttribute(CHAVE_PRIMARIA, idRubrica);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, Rubrica.RUBRICA_CAMPO_STRING);
			view = pesquisarRubrica(model, rubrica, result, request, rubrica.isHabilitado());
		} catch (NegocioException e) {
			mensagemErroParametrizado(model, request, e);
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Setar campos select no tributo.
	 * 
	 * @param idTributosAssociados
	 *            - {@link Long}
	 * @param rubrica
	 *            - {@link Rubrica}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void setarCamposSelectNoTributo(Long[] idTributosAssociados, Rubrica rubrica, HttpServletRequest request)
			throws GGASException {

		Collection<RubricaTributo> listaTributos = new ArrayList<RubricaTributo>();
		if (idTributosAssociados != null && idTributosAssociados.length > 0) {
			for (Long idTributo : idTributosAssociados) {
				RubricaTributo rubricaTributo = controladorRubrica.criarRubricaTributo();

				Tributo tributo = controladorTributo.obterTributo(idTributo);
				rubricaTributo.setRubrica(rubrica);
				rubricaTributo.setTributo(tributo);
				rubricaTributo.setDataInicioVigencia(Calendar.getInstance().getTime());
				rubricaTributo.setDataFimVigencia(null);
				rubricaTributo.setHabilitado(Boolean.TRUE);

				rubricaTributo.setUltimaAlteracao(Calendar.getInstance().getTime());
				rubricaTributo.setDadosAuditoria(getDadosAuditoria(request));

				rubrica.getTributos().add(rubricaTributo);
				listaTributos.add(rubricaTributo);
			}

			rubrica.getTributos().clear();
			rubrica.getTributos().addAll(listaTributos);
		}
	}

	/**
	 * Popular rubrica.
	 * 
	 * @param rubrica
	 *            - {@link RubricaImpl}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link Model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void popularRubrica(Rubrica rubrica, HttpServletRequest request, Model model) throws GGASException {

		String indicadorRegulamentada = (String) request.getParameter(INDICADOR_REGULAMENTADA);
		String dataInicioVigencia = (String) request.getParameter(DATA_INICIO_VIGENCIA);
		String observacao = (String) request.getParameter(OBSERVACAO);
		String percentualMinimoEntrada = (String) request.getParameter(PERCENTUAL_MINIMO_ENTRADA);
		String valorReferencia = (String) request.getParameter(VALOR_REFERENCIA);
		String valorMaximo = (String) request.getParameter(VALOR_MAXIMO);

		model.addAttribute(OBSERVACAO, observacao);
		model.addAttribute(VALOR_MAXIMO, valorMaximo);
		model.addAttribute(VALOR_REFERENCIA, valorReferencia);
		model.addAttribute(PERCENTUAL_MINIMO_ENTRADA, percentualMinimoEntrada);

		if (dataInicioVigencia != null && !dataInicioVigencia.isEmpty()) {
			model.addAttribute(DATA_INICIO_VIGENCIA, Util.converterCampoStringParaData("Data de Ínicio da Vigência",
					dataInicioVigencia, Constantes.FORMATO_DATA_BR));
		}

		if (Boolean.valueOf(indicadorRegulamentada) && !StringUtils.isEmpty(valorReferencia)) {
			rubrica.setValorMaximo(null);
			rubrica.setValorReferencia(Util.converterCampoStringParaValorBigDecimal(VALOR_DE_REFERENCIA,
					valorReferencia, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));

			if (!StringUtils.isEmpty(valorMaximo)) {
				rubrica.setValorMaximo(Util.converterCampoStringParaValorBigDecimal("Valor Máximo", valorMaximo,
						Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			} else {
				rubrica.setValorMaximo(null);
			}

			RubricaValorRegulamentado rubricaValorRegulamentado = controladorRubrica.criarRubricaValorRegulamentado();
			if (valorReferencia != null && !valorReferencia.isEmpty()) {
				rubricaValorRegulamentado
						.setValorReferencia(Util.converterCampoStringParaValorBigDecimal(VALOR_DE_REFERENCIA,
								valorReferencia, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			} else {
				rubricaValorRegulamentado.setValorReferencia(null);
			}

			if (dataInicioVigencia != null && !dataInicioVigencia.isEmpty()) {
				rubricaValorRegulamentado.setDataInicioVigencia(Util.converterCampoStringParaData(
						"Data de Ínicio da Vigência", dataInicioVigencia, Constantes.FORMATO_DATA_BR));
			} else {
				rubricaValorRegulamentado.setDataInicioVigencia(null);
			}

			if (!StringUtils.isEmpty(observacao)) {
				rubricaValorRegulamentado.setObservacao(observacao);
			} else {
				rubricaValorRegulamentado.setObservacao(null);
			}

			rubricaValorRegulamentado.setDataFimVigencia(null);

			rubricaValorRegulamentado.setRubrica(rubrica);
			rubricaValorRegulamentado.setUltimaAlteracao(Calendar.getInstance().getTime());
			rubricaValorRegulamentado.setDadosAuditoria(getDadosAuditoria(request));
			rubricaValorRegulamentado.setHabilitado(true);

			rubrica.getValorRegulamentados().add(rubricaValorRegulamentado);
		} else {
			if (!StringUtils.isEmpty(valorReferencia)) {
				rubrica.setValorReferencia(Util.converterCampoStringParaValorBigDecimal(VALOR_DE_REFERENCIA,
						valorReferencia, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			} else {
				rubrica.setValorReferencia(null);
			}

			if (!StringUtils.isEmpty(valorMaximo)) {
				rubrica.setValorMaximo(Util.converterCampoStringParaValorBigDecimal("Valor Máximo", valorMaximo,
						Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
			} else {
				rubrica.setValorMaximo(null);
			}
		}

		if (!StringUtils.isEmpty(percentualMinimoEntrada)) {
			rubrica.setPercentualMinimoEntrada(
					Util.converterCampoStringParaValorBigDecimal("Percentual Mínimo Entrada", percentualMinimoEntrada,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(100)));
		} else {
			rubrica.setPercentualMinimoEntrada(null);
		}
	}

	/**
	 * Tratar campos select.
	 * 
	 * @param idTributosAssociados
	 *            - {@link Long}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param model
	 *            - {@link model}
	 * @throws GGASException
	 *             - {@link GGASException}
	 */
	private void tratarCamposSelect(Long[] idTributosAssociados, HttpServletRequest request, Model model)
			throws GGASException {

		if (idTributosAssociados != null && idTributosAssociados.length > 0) {
			Collection<Tributo> listaTributosAssociados = new ArrayList<Tributo>();
			for (Long chaveTributo : idTributosAssociados) {
				Tributo tributo = controladorTributo.obterTributo(chaveTributo);
				listaTributosAssociados.add(tributo);
			}

			Collection<Tributo> listaTributosDisp = controladorTributo.consultarTributos(null);
			listaTributosDisp.removeAll(listaTributosAssociados);
			model.addAttribute(LISTA_TRIBUTOS, listaTributosDisp);
			model.addAttribute(LISTA_TRIBUTOS_ASSOCIADOS, listaTributosAssociados);
		}

		String tipoRubricaServico = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_SERVICO);
		String tipoRubricaParcelamento = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PARCELAMENTO);

		model.addAttribute("tipoRubricaServico", tipoRubricaServico);
		model.addAttribute("tipoRubricaParcelamento", tipoRubricaParcelamento);

		String idItemFatura = (String) request.getParameter(ID_ITEM_FATURA);
		if (idItemFatura != null) {
			// Possivel Implementação Futura.
		}

		String idFinanciamentoTipo = (String) request.getParameter(ID_TIPO_RUBRICA);
		if (idFinanciamentoTipo != null) {
			// Possivel Implementação Futura.
		}
	}

	/**
	 * Método responsável por manter os dados da pesquisa anterior.
	 * 
	 * @param rubrica
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@RequestMapping("voltarRubrica")
	public String voltar(RubricaImpl rubrica, BindingResult result,
			@RequestParam(value = "habilitadoPesquisa", required = false) Boolean habilitado,
			HttpServletRequest request, Model model) throws GGASException {

		RubricaImpl rubricaSession = rubrica;

		if (request.getSession().getAttribute(RUBRICA) != null) {
			rubricaSession = (RubricaImpl) retornaPesquisa(request, RUBRICA);
		}

		return pesquisarRubrica(model, rubricaSession, result, request, habilitado);
	}
}
