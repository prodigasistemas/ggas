/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura.dto;

import br.com.ggas.geral.apresentacao.deserializadorjson.LocalDateTimeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDateTime;
import java.util.List;

/**
 * DTO de pesquisa de ponto de consumo
 * @author jose.victor@logiquesistemas.com.br
 */
@SuppressWarnings({"common-java:InsufficientBranchCoverage", "common-java:InsufficientLineCoverage",
		"common-java:InsufficientCommentDensity"})
public class PesquisaPontoConsumo {

	private int qtdRegistros;
	private int offset;

	private String descricao;
	private List<Long> rotas;
	private Long situacaoMedidor;
	private Long situacaoContrato;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataInicioAssinaturaContrato;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataFimAssinaturaContrato;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataInicioAtivacaoMedidor;

	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataFimAtivacaoMedidor;


	public int getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(int qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Long> getRotas() {
		return rotas;
	}

	public void setRotas(List<Long> rotas) {
		this.rotas = rotas;
	}

	public Long getSituacaoMedidor() {
		return situacaoMedidor;
	}

	public void setSituacaoMedidor(Long situacaoMedidor) {
		this.situacaoMedidor = situacaoMedidor;
	}

	public Long getSituacaoContrato() {
		return situacaoContrato;
	}

	public void setSituacaoContrato(Long situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	public LocalDateTime getDataInicioAssinaturaContrato() {
		return dataInicioAssinaturaContrato;
	}

	public void setDataInicioAssinaturaContrato(LocalDateTime dataInicioAssinaturaContrato) {
		this.dataInicioAssinaturaContrato = dataInicioAssinaturaContrato;
	}

	public LocalDateTime getDataFimAssinaturaContrato() {
		return dataFimAssinaturaContrato;
	}

	public void setDataFimAssinaturaContrato(LocalDateTime dataFimAssinaturaContrato) {
		this.dataFimAssinaturaContrato = dataFimAssinaturaContrato;
	}

	public LocalDateTime getDataInicioAtivacaoMedidor() {
		return dataInicioAtivacaoMedidor;
	}

	public void setDataInicioAtivacaoMedidor(LocalDateTime dataInicioAtivacaoMedidor) {
		this.dataInicioAtivacaoMedidor = dataInicioAtivacaoMedidor;
	}

	public LocalDateTime getDataFimAtivacaoMedidor() {
		return dataFimAtivacaoMedidor;
	}

	public void setDataFimAtivacaoMedidor(LocalDateTime dataFimAtivacaoMedidor) {
		this.dataFimAtivacaoMedidor = dataFimAtivacaoMedidor;
	}
}
