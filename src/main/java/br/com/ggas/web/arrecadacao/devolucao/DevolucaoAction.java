/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.arrecadacao.devolucao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ImovelPopupVO;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import br.com.ggas.web.cadastro.cliente.ClientePopupVO;

/**
 * Classe rsponsável pelas telas relacionadas a Devoluções Financeiras
 * 
 */
@Controller
public class DevolucaoAction extends GenericAction {

	private static final String FORWARD_EXIBIR_PESQUISA_DEVOLUCAO = "forward:/exibirPesquisaDevolucao";

	private static final String VIEW_FORWARD_PESQUISAR_DEVOLUCOES = "forward:/pesquisarDevolucoes";

	private static final String IMOVEL_POPUP_VO = "imovelPopupVO";

	private static final String CLIENTE_POPUP_VO = "clientePopupVO";

	private static final String DEVOLUCAO_VO = "devolucaoVO";

	private static final String VIEW_EXIBIR_PESQUISA_DEVOLUCAO = "exibirPesquisaDevolucao";

	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorDevolucao controladorDevolucao;

	@Autowired
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorRecebimento controladorRecebimento;

	@Autowired
	private ControladorCliente controladorCliente;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorCobranca controladorCobranca;

	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;

	private static final String LISTA_RECEBIMENTO = "listaRecebimento";

	private static final String ID_DEVOLUCAO = "idDevolucao";

	private static final String LISTA_CREDITO_DEBITO = "listaCreditoDebito";

	private static final String LISTA_FATURA = "listaFatura";

	private static final String HABILITADO = "habilitado";

	private static final String ATRIBUTO_FORM_ID_CLIENTE = "idCliente";

	private static final String ATRIBUTO_FORM_ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ATRIBUTO_FORM_ID_TIPO_DEVOLUCAO = "idTipoDevolucao";

	private static final String ATRIBUTO_FORM_DEVOLUCAO_INICIAL = "dataDevolucaoInicial";

	private static final String ATRIBUTO_FORM_DEVOLUCAO_FINAL = "dataDevolucaoFinal";

	/**
	 * Método responsável por exibir a tela de pesquisa de devoluções.
	 * 
	 * @param model - {@link Model}
	 * @throws GGASException - {@link GGASException}
	 * @return view - {@link String}
	 */
	@RequestMapping(VIEW_EXIBIR_PESQUISA_DEVOLUCAO)
	public String exibirPesquisaDevolucao(Model model) throws GGASException {

		this.carregarCampos(model);

		return VIEW_EXIBIR_PESQUISA_DEVOLUCAO;
	}

	/**
	 * Método responsável por pesquisar de devoluções cadastradas no sistema.
	 * @param request - {@link HttpServletRequest}
	 * @param model - {@link Model}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO - {@link ImovelPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("pesquisarDevolucoes")
	public String pesquisarDevolucoes(HttpServletRequest request, Model model, DevolucaoVO devolucaoVO,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		model.addAttribute(DEVOLUCAO_VO, devolucaoVO);
		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(IMOVEL_POPUP_VO, imovelPopupVO);

		try {
			Map<String, Object> filtro = new HashMap<String, Object>();
			
			prepararFiltro(filtro, devolucaoVO, clientePopupVO);
		
			Collection<Devolucao> listaDevolucao = controladorDevolucao.consultarDevolucoes(filtro);
			model.addAttribute("listaDevolucao", listaDevolucao);

			saveToken(request);
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return FORWARD_EXIBIR_PESQUISA_DEVOLUCAO;
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo para a tela de pesquisa
	 * de devoluções.
	 * 
	 * @param model       - {@link Model}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO - {@link ImovelPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("pesquisarPontosConsumoDevolucao")
	public String pesquisarPontosConsumoDevolucao(Model model, DevolucaoVO devolucaoVO, ClientePopupVO clientePopupVO,
			ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		model.addAttribute(DEVOLUCAO_VO, devolucaoVO);
		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(IMOVEL_POPUP_VO, imovelPopupVO);

		try {
			obterPontosConsumo(model, imovelPopupVO.getIdImovel());
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return FORWARD_EXIBIR_PESQUISA_DEVOLUCAO;
	}

	/**
	 * Método responsável por pesquisar os pontos de consumo para a tela de inclusão
	 * de devoluções.
	 * 
	 * @param model       - {@link Model}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO - {@link ImovelPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@RequestMapping("pesquisarPontosConsumoDevolucaoTelaInclusao")
	public String pesquisarPontosConsumoDevolucaoTelaInclusao(Model model, DevolucaoVO devolucaoVO,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		pesquisarPontosConsumoDevolucao(model, devolucaoVO, clientePopupVO, imovelPopupVO, bindingResult);

		return "exibirInclusaoDevolucaoPasso1";
	}

	/**
	 * Método responsável por exibir a tela de detalhamento de devolução.
	 * 
	 * @param model       - {@link Model}
	 * @param request     - {@link HttpServletRequest}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirDetalheDevolucao")
	public String exibirDetalheDevolucao(Model model, HttpServletRequest request, DevolucaoVO devolucaoVO) {

		String view = VIEW_FORWARD_PESQUISAR_DEVOLUCOES;

		try {
			if (!super.isPostBack(request)) {
				Long chavePrimaria = devolucaoVO.getChavePrimaria();
				Devolucao devolucao = (Devolucao) controladorDevolucao.obter(chavePrimaria);
				this.popularForm(devolucao, devolucaoVO, request);

				Collection<Fatura> listaFatura = controladorDevolucao
						.listarFaturasPorChaveDevolucao(devolucao.getChavePrimaria());
				if ((listaFatura != null) && (listaFatura.isEmpty())) {
					listaFatura = null;
				}
				request.setAttribute(LISTA_FATURA, listaFatura);

				Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorDevolucao
						.listarCreditosPorChaveDevolucao(devolucao.getChavePrimaria());
				if ((listaCreditoDebito != null) && (listaCreditoDebito.isEmpty())) {
					listaCreditoDebito = null;
				}
				request.setAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebito);

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put(ID_DEVOLUCAO, devolucao.getChavePrimaria());
				Collection<Recebimento> listaRecebimento = controladorRecebimento.consultarRecebimento(filtro);

				if ((listaRecebimento != null) && (listaRecebimento.isEmpty())) {
					listaRecebimento = null;
				}
				request.setAttribute(LISTA_RECEBIMENTO, listaRecebimento);
			}
			model.addAttribute(DEVOLUCAO_VO, devolucaoVO);
			view = "exibirDetalheDevolucao";
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de inclusão de devolução.
	 * 
	 * @param request - {@link HttpServletRequest}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirInclusaoDevolucao")
	public String exibirInclusaoDevolucao(HttpServletRequest request) {

		saveToken(request);
		return "exibirInclusaoDevolucaoPasso1";
	}

	/**
	 * Método responsável por exibir a tela de alteração de devolução.
	 * 
	 * @param model       - {@link Model}
	 * @param request     - {@link HttpServletRequest}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("exibirAlteracaoDevolucao")
	public String exibirAlteracaoDevolucao(Model model, HttpServletRequest request, DevolucaoVO devolucaoVO) {

		String view = VIEW_FORWARD_PESQUISAR_DEVOLUCOES;

		try {
			saveToken(request);
			
			Long chavePrimaria = devolucaoVO.getChavePrimaria();
			Devolucao devolucao = (Devolucao) controladorDevolucao.obter(chavePrimaria);

			this.carregarCampos(model);
			this.popularForm(devolucao, devolucaoVO, request);

			if(devolucao.getCliente() != null) {
				ClientePopupVO clientePopupVO = new ClientePopupVO();
				clientePopupVO.setIdCliente(devolucao.getCliente().getChavePrimaria());
				model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
			}
			if(devolucao.getPontoConsumo() != null) {
				devolucaoVO.setIdPontoConsumo(devolucao.getPontoConsumo().getChavePrimaria());
			}

			Collection<Fatura> listaFatura = controladorDevolucao
					.listarFaturasPorChaveDevolucao(devolucao.getChavePrimaria());
			if ((listaFatura != null) && (listaFatura.isEmpty())) {
				listaFatura = null;
			}
			request.setAttribute(LISTA_FATURA, listaFatura);

			Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorDevolucao
					.listarCreditosPorChaveDevolucao(devolucao.getChavePrimaria());
			if ((listaCreditoDebito != null) && (listaCreditoDebito.isEmpty())) {
				listaCreditoDebito = null;
			}
			request.setAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebito);

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_DEVOLUCAO, devolucao.getChavePrimaria());
			Collection<Recebimento> listaRecebimento = controladorRecebimento.consultarRecebimento(filtro);

			if ((listaRecebimento != null) && (listaRecebimento.isEmpty())) {
				listaRecebimento = null;
			}
			model.addAttribute(LISTA_RECEBIMENTO, listaRecebimento);
			model.addAttribute(DEVOLUCAO_VO, devolucaoVO);

			view = "exibirAlteracaoDevolucao";
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por excluir as devoluções selecionadas.
	 * 
	 * @param model           - {@link Model}
	 * @param request         - {@link HttpServletRequest}
	 * @param chavesPrimarias - Array de Chaves Primárias
	 * @return view - {@link String}
	 */
	@RequestMapping("removerDevolucao")
	public String removerDevolucao(Model model, HttpServletRequest request,
			@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias) {

		try {
			controladorDevolucao.validarRemoverDevolucao(chavesPrimarias);
			this.estornarDevolucao(chavesPrimarias, getDadosAuditoria(request));
			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Devolucao.DEVOLUCAO_CAMPO_STRING);
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			super.mensagemErroParametrizado(model, e);
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return this.pesquisarDevolucoes(request, model, new DevolucaoVO(), null, null, null);
	}

	private void estornarDevolucao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesPrimarias);

			controladorDevolucao.estornarDevolucao(filtro, dadosAuditoria);
		}
	}

	/**
	 * Método responsável por pesquisar os recebimentos e débitos para inclusão de
	 * devoluções financeiras.
	 * 
	 * @param model       - {@link Model}
	 * @param request     - {@link HttpServletRequest}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO - {@link ImovelPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 * 
	 */
	@Transactional
	@RequestMapping("pesquisarDebitos")
	public String pesquisarDebitos(Model model, HttpServletRequest request, DevolucaoVO devolucaoVO,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		model.addAttribute(DEVOLUCAO_VO, devolucaoVO);
		model.addAttribute(CLIENTE_POPUP_VO, clientePopupVO);
		model.addAttribute(IMOVEL_POPUP_VO, imovelPopupVO);

		String view = "forward:/exibirInclusaoDevolucao";

		try {
			Long idCliente = clientePopupVO.getIdCliente();
			Long idPontoConsumo = devolucaoVO.getIdPontoConsumo();

			controladorCobranca.validarFiltroPesquisaExtratoDebito(idCliente, idPontoConsumo);
			Collection<Fatura> listaFatura = null;
			Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
			if ((idCliente != null) && (idCliente > 0)) {
				listaFatura = controladorDevolucao.consultarFaturasManterDevolucao(idCliente, null);
				listaCreditoDebito = controladorDevolucao.consultarCreditosDebitosManterDevolucao(idCliente, null);
				Cliente cliente = controladorCliente.obterCliente(idCliente);
				devolucaoVO.setNomeCompletoCliente(cliente.getNome());
			} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				listaFatura = controladorDevolucao.consultarFaturasManterDevolucao(null, idPontoConsumo);
				listaCreditoDebito = controladorDevolucao.consultarCreditosDebitosManterDevolucao(null, idPontoConsumo);
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, "cep",
						"imovel");
				devolucaoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
				devolucaoVO.setEnderecoPontoConsumo(pontoConsumo.getEnderecoFormatado());
				if (pontoConsumo.getCep() != null) {
					devolucaoVO.setCepPontoConsumo(pontoConsumo.getCep().getCep());
				}
				devolucaoVO.setComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
			}

			if ((listaFatura != null) && (listaFatura.isEmpty())) {
				listaFatura = null;
			}
			if ((listaCreditoDebito != null) && (listaCreditoDebito.isEmpty())) {
				listaCreditoDebito = null;
			}

			request.setAttribute(LISTA_FATURA, listaFatura);
			request.setAttribute(LISTA_CREDITO_DEBITO, listaCreditoDebito);

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ATRIBUTO_FORM_ID_CLIENTE, idCliente);
			filtro.put(ATRIBUTO_FORM_ID_PONTO_CONSUMO, idPontoConsumo);
			filtro.put("baixado", Boolean.FALSE);
			filtro.put("devolvido", Boolean.TRUE);
			Collection<Recebimento> listaRecebimento = controladorRecebimento.consultarRecebimento(filtro);
			if ((listaCreditoDebito != null) && (listaCreditoDebito.isEmpty())) {
				listaRecebimento = null;
			}
			request.setAttribute(LISTA_RECEBIMENTO, listaRecebimento);
			this.carregarCampos(model);
			saveToken(request);

			model.addAttribute(DEVOLUCAO_VO, devolucaoVO);
			view = "exibirInclusaoDevolucaoPasso2";
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return view;
	}

	/**
	 * Método responsável por inserir a devolução no sistema.
	 * 
	 * @param model       - {@link Model}
	 * @param request     - {@link HttpServletRequest}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @param clientePopupVO - {@link ClientePopupVO}
	 * @param imovelPopupVO - {@link ImovelPopupVO}
	 * @param bindingResult - {@link BindingResult}
	 * @return view - {@link String}
	 */
	@RequestMapping("incluirDevolucao")
	public String incluirDevolucao(Model model, HttpServletRequest request, DevolucaoVO devolucaoVO,
			ClientePopupVO clientePopupVO, ImovelPopupVO imovelPopupVO, BindingResult bindingResult) {

		String view = null;

		try {
			Long[] chavesCreditoDebito = devolucaoVO.getChavesCreditoDebito();
			Long[] chavesRecebimento = devolucaoVO.getChavesRecebimento();
			// TODO: REVER Manter o estado do checkbox
			view = pesquisarDebitos(model, request, devolucaoVO, clientePopupVO, imovelPopupVO, bindingResult);

			// Valida o token para garantir que não
			// aconteça um duplo submit.
			super.saveToken(request);
			
			Devolucao devolucao = (Devolucao) controladorDevolucao.criar();
			popularDevolucao(devolucaoVO, clientePopupVO, devolucao);
			devolucao.setDadosAuditoria(getDadosAuditoria(request));
			devolucao.setHabilitado(true);

			Long idDevolucao = controladorDevolucao.inserir(devolucao);

			// atualizar credito debito a realizar
			if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
				for (int i = 0; i < chavesCreditoDebito.length; i++) {
					CreditoDebitoARealizar creditoDebitoARealizar = (CreditoDebitoARealizar) controladorCreditoDebito
							.obter(chavesCreditoDebito[i]);
					Devolucao devolucao2 = (Devolucao) controladorDevolucao.obter(idDevolucao);
					creditoDebitoARealizar.setDevolucao(devolucao2);
					creditoDebitoARealizar.setDadosAuditoria(getDadosAuditoria(request));
					controladorCreditoDebito.atualizar(creditoDebitoARealizar);
				}
			}

			// atualizar recebimento
			if (chavesRecebimento != null) {
				for (int i = 0; i < chavesRecebimento.length; i++) {
					Recebimento recebimento = (Recebimento) controladorRecebimento.obter(chavesRecebimento[i], "documentoCobrancaItem");
					Devolucao devolucao3 = (Devolucao) controladorDevolucao.obter(idDevolucao);
					recebimento.setDevolucao(devolucao3);
					recebimento.setDadosAuditoria(getDadosAuditoria(request));
					controladorRecebimento.atualizarRecebimento(recebimento);
				}
			}

			devolucaoVO.setChavePrimaria(idDevolucao);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Devolucao.DEVOLUCAO_CAMPO_STRING);
			view = VIEW_EXIBIR_PESQUISA_DEVOLUCAO;
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}

		return view;
	}

	/**
	 * Método responsável por alterar a devolução no sistema.
	 * 
	 * @param model       - {@link Model}
	 * @param request     - {@link HttpServletRequest}
	 * @param devolucaoVO - {@link DevolucaoVO}
	 * @return view - {@link String}
	 */
	@RequestMapping("alterarDevolucao")
	public String alterarDevolucao(Model model, HttpServletRequest request, DevolucaoVO devolucaoVO) {

		String view = "forward:/exibirAlteracaoDevolucao";

		try {
			validarToken(request);

			Long chavePrimaria = devolucaoVO.getChavePrimaria();
			Devolucao devolucao = (Devolucao) controladorDevolucao.obter(chavePrimaria);

			popularDevolucaoAlteracao(devolucaoVO, devolucao);
			devolucao.setDadosAuditoria(getDadosAuditoria(request));

			controladorDevolucao.atualizar(devolucao);

			super.mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Devolucao.DEVOLUCAO_CAMPO_STRING);
			view = VIEW_FORWARD_PESQUISAR_DEVOLUCOES;
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, e);
		}

		return view;
	}

	private void popularDevolucaoAlteracao(DevolucaoVO devolucaoVO, Devolucao devolucao) throws GGASException {

		String habilitado = devolucaoVO.getHabilitado();
		Long idTipoDevolucao = devolucaoVO.getIdTipoDevolucao();
		String dataDevolucao = devolucaoVO.getDataDevolucao();
		String observacao = devolucaoVO.getObservacaoDevolucao();

		if (!StringUtils.isEmpty(habilitado)) {
			devolucao.setHabilitado(Boolean.valueOf(habilitado));
		}
		if ((idTipoDevolucao != null) && (idTipoDevolucao > 0)) {
			EntidadeConteudo tipoDevolucao = controladorEntidadeConteudo.obterEntidadeConteudo(idTipoDevolucao);
			devolucao.setTipoDevolucao(tipoDevolucao);
		} else {
			devolucao.setTipoDevolucao(null);
		}
		if (!StringUtils.isEmpty(dataDevolucao)) {
			devolucao.setDataDevolucao(
					Util.converterCampoStringParaData("Data", dataDevolucao, Constantes.FORMATO_DATA_BR));
		}
		if (!StringUtils.isEmpty(observacao)) {
			devolucao.setObservacao(observacao);
		}

		Collection<Fatura> listaFatura = controladorDevolucao
				.listarFaturasPorChaveDevolucao(devolucao.getChavePrimaria());
		Collection<Fatura> listaFaturaAux = new HashSet<Fatura>();
		for (Fatura fatura : listaFatura) {
			listaFaturaAux.add(fatura);
		}
		devolucao.setListaNotaCredito(listaFaturaAux);

		Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorDevolucao
				.listarCreditosPorChaveDevolucao(devolucao.getChavePrimaria());
		Collection<CreditoDebitoARealizar> listaCreditoDebitoAux = new HashSet<CreditoDebitoARealizar>();
		for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {
			listaCreditoDebitoAux.add(creditoDebitoARealizar);
		}
		devolucao.setListaCreditoDebitoARealizar(listaCreditoDebitoAux);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_DEVOLUCAO, devolucao.getChavePrimaria());
		Collection<Recebimento> listaRecebimento = controladorRecebimento.consultarRecebimento(filtro);
		Collection<Recebimento> listaRecebimentoAux = new HashSet<Recebimento>();
		for (Recebimento recebimento : listaRecebimento) {
			listaRecebimentoAux.add(recebimento);
		}
		devolucao.setListaRecebimento(listaRecebimentoAux);
	}

	private void popularDevolucao(DevolucaoVO devolucaoVO, ClientePopupVO clientePopupVO, Devolucao devolucao)
			throws GGASException {

		Long chavePrimaria = devolucaoVO.getChavePrimaria();
		Integer versao = devolucaoVO.getVersao();
		String habilitado = devolucaoVO.getHabilitado();
		Long idTipoDevolucao = devolucaoVO.getIdTipoDevolucao();
		String valorDevolucao = devolucaoVO.getValorDevolucao();
		String observacao = devolucaoVO.getObservacaoDevolucao();
		Long[] chavesFatura = devolucaoVO.getChavesFatura();
		Long[] chavesCreditoDebito = devolucaoVO.getChavesCreditoDebito();
		Long[] chavesRecebimento = devolucaoVO.getChavesRecebimento();
		Long idCliente = clientePopupVO.getIdCliente();
		Long idPontoConsumo = devolucaoVO.getIdPontoConsumo();

		Cliente cliente = null;
		PontoConsumo pontoConsumo = null;

		if ((idCliente == null) || (idCliente <= 0)) {
			if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				cliente = controladorCliente.obterClientePorPontoConsumo(idPontoConsumo);
				pontoConsumo = controladorPontoConsumo.obterPontoConsumo(idPontoConsumo);
			}
		} else {
			cliente = controladorCliente.obterCliente(idCliente);
		}

		devolucao.setCliente(cliente);
		devolucao.setPontoConsumo(pontoConsumo);

		if (chavePrimaria != null && chavePrimaria > 0) {
			devolucao.setChavePrimaria(chavePrimaria);
		}
		if (versao != null && versao > 0) {
			devolucao.setVersao(versao);
		}
		if (!StringUtils.isEmpty(habilitado)) {
			devolucao.setHabilitado(Boolean.valueOf(habilitado));
		}
		if ((idTipoDevolucao != null) && (idTipoDevolucao > 0)) {
			EntidadeConteudo tipoDevolucao = controladorEntidadeConteudo.obterEntidadeConteudo(idTipoDevolucao);
			devolucao.setTipoDevolucao(tipoDevolucao);
		}
		if (!StringUtils.isEmpty(valorDevolucao)) {
			controladorDevolucao.validarValorDevolucao(valorDevolucao);
			devolucao.setValorDevolucao(Util.converterCampoStringParaValorBigDecimal("Valor", valorDevolucao,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
		}
		devolucao.setDataDevolucao(Calendar.getInstance().getTime());
		if (!StringUtils.isEmpty(observacao)) {
			devolucao.setObservacao(observacao);
		}

		if ((chavesFatura != null) && (chavesFatura.length > 0)) {
			for (int i = 0; i < chavesFatura.length; i++) {
				Fatura fatura = (Fatura) controladorCobranca.obterFatura(chavesFatura[i]);
				devolucao.getListaNotaCredito().add(fatura);
			}
		}

		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			for (int i = 0; i < chavesCreditoDebito.length; i++) {
				CreditoDebitoARealizar creditoDebitoARealizar = (CreditoDebitoARealizar) controladorCreditoDebito
						.obter(chavesCreditoDebito[i]);
				creditoDebitoARealizar.setDevolucao(devolucao);
				devolucao.getListaCreditoDebitoARealizar().add(creditoDebitoARealizar);
			}
		}

		Collection<Recebimento> listaRecebimento = new HashSet<Recebimento>();
		if (chavesRecebimento != null) {
			for (int i = 0; i < chavesRecebimento.length; i++) {
				Recebimento recebimento = (Recebimento) controladorRecebimento.obter(chavesRecebimento[i]);
				listaRecebimento.add(recebimento);
			}
		}
		devolucao.setListaRecebimento(listaRecebimento);
	}

	private void obterPontosConsumo(Model model, Long idImovel) throws GGASException {

		Collection<ContratoPontoConsumo> listaContratoPontoConsumos = new ArrayList<ContratoPontoConsumo>();

		if ((idImovel != null) && (idImovel > 0)) {
			listaContratoPontoConsumos = controladorContrato.consultarContratoPontoConsumoPorImovel(idImovel);
		}

		model.addAttribute("listaContratoPontoConsumos", listaContratoPontoConsumos);
	}

	private void prepararFiltro(Map<String, Object> filtro, DevolucaoVO devolucaoVO, ClientePopupVO clientePopupVO) throws GGASException {

		String habilitado = devolucaoVO.getHabilitado();
		if (!StringUtils.isEmpty(habilitado)) {
			filtro.put(HABILITADO, Boolean.valueOf(habilitado));
		}

		if(clientePopupVO != null) {
			Long idCliente = clientePopupVO.getIdCliente();
			if ((idCliente != null) && (idCliente > 0)) {
				filtro.put(ATRIBUTO_FORM_ID_CLIENTE, idCliente);
			}
		}
		
		Long idPontoConsumo = devolucaoVO.getIdPontoConsumo();
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			filtro.put(ATRIBUTO_FORM_ID_PONTO_CONSUMO, idPontoConsumo);
		}

		Long idTipoDevolucao = devolucaoVO.getIdTipoDevolucao();
		if ((idTipoDevolucao != null) && (idTipoDevolucao > 0)) {
			filtro.put(ATRIBUTO_FORM_ID_TIPO_DEVOLUCAO, idTipoDevolucao);
		}

		String dataDevolucaoInicial = devolucaoVO.getDataDevolucaoInicial();
		String dataDevolucaoFinal = devolucaoVO.getDataDevolucaoFinal();
		controladorDevolucao.validarDataDevolucaoInicialFinal(dataDevolucaoInicial, dataDevolucaoFinal);

		if (!StringUtils.isEmpty(dataDevolucaoInicial)) {
			filtro.put(ATRIBUTO_FORM_DEVOLUCAO_INICIAL,
					Util.converterCampoStringParaData("Data", dataDevolucaoInicial, Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(dataDevolucaoFinal)) {
			filtro.put(ATRIBUTO_FORM_DEVOLUCAO_FINAL,
					Util.converterCampoStringParaData("Data", dataDevolucaoFinal, Constantes.FORMATO_DATA_BR));
		}
	}

	private void carregarCampos(Model model) throws GGASException {

		model.addAttribute("listaTipoDevolucao", controladorEntidadeConteudo.listarTipoDevolucao());
	}

	private void popularForm(Devolucao devolucao, DevolucaoVO devolucaoVO, HttpServletRequest request)
			throws GGASException {

		devolucaoVO.setChavePrimaria(devolucao.getChavePrimaria());
		devolucaoVO.setVersao(devolucao.getVersao());
		devolucaoVO.setHabilitado(String.valueOf(devolucao.isHabilitado()));

		Cliente cliente = controladorCliente.obterCliente(devolucao.getCliente().getChavePrimaria());
		devolucaoVO.setNomeCompletoCliente(cliente.getNome());

		if (devolucao.getPontoConsumo() != null) {

			PontoConsumo pontoConsumo = controladorPontoConsumo
					.obterPontoConsumo(devolucao.getPontoConsumo().getChavePrimaria());
			devolucaoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
		}

		if (!super.isPostBack(request)) {
			EntidadeConteudo tipoDevolucao = controladorEntidadeConteudo
					.obterEntidadeConteudo(devolucao.getTipoDevolucao().getChavePrimaria());
			devolucaoVO.setTipoDevolucaoDetalhamento(tipoDevolucao.getDescricao());
			devolucaoVO.setIdTipoDevolucao(tipoDevolucao.getChavePrimaria());
			devolucaoVO.setObservacaoDevolucao(devolucao.getObservacao());
		}

		devolucaoVO.setValorDevolucao(String.valueOf(devolucao.getValorDevolucao()));
		devolucaoVO.setDataDevolucao(
				Util.converterDataParaStringSemHora(devolucao.getDataDevolucao(), Constantes.FORMATO_DATA_BR));
	}
}
