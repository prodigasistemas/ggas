/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.arrecadacao.arrecadador;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorArrecadador;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.impl.ArrecadadorImpl;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Controller MVC para a entidade Arrecadador
 * 
 * @author Procenge
 */
@Controller
public class ArrecadadorAction extends GenericAction {

	private static final String ARRECADADOR = "arrecadador";
	private static final String TELA_EXIBIR_INCLUSAO_ARRECADADOR = "exibirInclusaoArrecadador";
	private static final String TELA_EXIBIR_ALTERACAO_ARRECADADOR = "exibirAlteracaoArrecadador";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String TELA_EXIBIR_PESQUISA_ARRECADADOR = "exibirPesquisaArrecadador";
	private static final Logger LOG = Logger.getLogger(ArrecadadorAction.class);
	private static final String LISTA_BANCO = "listaBanco";
	private String retorno;

	@Autowired
	@Qualifier("controladorBanco")
	private ControladorBanco controladorBanco;

	@Autowired
	@Qualifier("controladorArrecadador")
	private ControladorArrecadador controladorArrecadador;

	@Autowired
	@Qualifier("controladorArrecadacao")
	private ControladorArrecadacao controladorArrecadacao;

	@Autowired
	@Qualifier("controladorArrecadadorConvenio")
	private ControladorArrecadadorConvenio controladorArrecadadorConvenio;

	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	@Qualifier("controladorCobranca")
	private ControladorCobranca controladorCobranca;

	/**
	 * Exibir pesquisa arrecadador.
	 * 
	 * @param arrecadador
	 * @param model
	 * @param request
	 * @return exibirPesquisaArrecadador
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_ARRECADADOR)
	public String exibirPesquisaArrecadador(@ModelAttribute("ArrecadadorImpl") ArrecadadorImpl arrecadador, Model model,
			HttpServletRequest request) {

		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());

		return TELA_EXIBIR_PESQUISA_ARRECADADOR;
	}

	/**
	 * Pesquisar arrecadador.
	 * 
	 * @param arrecadador
	 * @param result
	 * @param habilitado
	 * @param model
	 * @return exibirPesquisaArrecadador
	 */
	@RequestMapping("pesquisarArrecadador")
	public String pesquisarArrecadador(@ModelAttribute("ArrecadadorImpl") ArrecadadorImpl arrecadador, BindingResult result,
			@RequestParam(value = "habilitado", required = false) String habilitado, Model model) {

		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());
		model.addAttribute("listaArrecadador", controladorArrecadador.consultarArrecadador(obterFiltroForm(arrecadador, habilitado)));
		model.addAttribute("habilitado", habilitado);
		model.addAttribute(ARRECADADOR, arrecadador);
		return TELA_EXIBIR_PESQUISA_ARRECADADOR;
	}

	/**
	 * Exibir inclusao arrecadador.
	 * 
	 * @param model
	 * @return exibirInclusaoArrecadador
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_ARRECADADOR)
	public String exibirInclusaoArrecadador(Model model) {

		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());

		return TELA_EXIBIR_INCLUSAO_ARRECADADOR;
	}

	/**
	 * Incluir arrecadador.
	 *
	 * @param arrecadador
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param attr
	 * @param model
	 * @return tela
	 * @throws NegocioException
	 */
	@RequestMapping(value = "incluirArrecadador", method = RequestMethod.POST)
	public String incluirArrecadador(@ModelAttribute("ArrecadadorImpl") ArrecadadorImpl arrecadador, BindingResult result,
			@RequestParam(value = "habilitado", required = false) String habilitado, HttpServletRequest request, RedirectAttributes attr,
			Model model) throws NegocioException {

		retorno = TELA_EXIBIR_INCLUSAO_ARRECADADOR;
		model.addAttribute(ARRECADADOR, arrecadador);
		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());
		Map<String, Object> erros = arrecadador.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorArrecadador.validarExistente(obterFiltroForm(arrecadador, habilitado));
				arrecadador.setHabilitado(true);
				arrecadador.setDadosAuditoria(getDadosAuditoria(request));
				model.addAttribute(CHAVE_PRIMARIA, controladorArrecadador.inserir(arrecadador));
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Arrecadador.ARRECADADOR_CAMPO_STRING);
				retorno = pesquisarArrecadador(arrecadador, result, String.valueOf(arrecadador.isHabilitado()), model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * Remover arrecadador.
	 * 
	 * @param arrecadador
	 * @param request
	 * @param model
	 * @param chavesPrimarias
	 * @throws NegocioException
	 * @return exibirPesquisaArrecadador
	 */

	@RequestMapping("removerArrecadador")
	public String removerArrecadador(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model) {
		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());
		try {
			controladorArrecadador.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Arrecadador.ARRECADADORES);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			mensagemErro(model, new NegocioException(Constantes.ERRO_ARRECADADOR_EM_USO, e));
		}

		return TELA_EXIBIR_PESQUISA_ARRECADADOR;
	}

	/**
	 * Exibir detalhamento arrecadador.
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoArrecadador")
	public String exibirDetalhamentoArrecadador(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(ARRECADADOR, controladorArrecadacao.obterArrecadador(chavePrimaria, "cliente"));

		return "exibirDetalhamentoArrecadador";
	}

	/**
	 * Exibir alteracao arrecadador.
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return exibirAlteracaoArrecadador
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping(TELA_EXIBIR_ALTERACAO_ARRECADADOR)
	public String exibirAlteracaoArrecadador(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request, Model model)
			throws GGASException {

		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());
		model.addAttribute(ARRECADADOR, controladorArrecadacao.obterArrecadador(chavePrimaria, "cliente"));

		return TELA_EXIBIR_ALTERACAO_ARRECADADOR;
	}

	/**
	 * Alterar arrecadador.
	 * 
	 * @param arrecadador
	 * @param result
	 * @param chavePrimaria
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return pesquisarArrecadador
	 */
	@RequestMapping("alterarArrecadador")
	public String alterarArrecadador(@ModelAttribute("ArrecadadorImpl") ArrecadadorImpl arrecadador, BindingResult result,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, @RequestParam("habilitado") String habilitado, HttpServletRequest request,
			Model model) {

		retorno = TELA_EXIBIR_ALTERACAO_ARRECADADOR;
		model.addAttribute(ARRECADADOR, arrecadador);
		model.addAttribute(LISTA_BANCO, controladorBanco.listarBanco());
		Map<String, Object> erros = arrecadador.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				arrecadador.setDadosAuditoria(getDadosAuditoria(request));
				Map<String, Object> filtro = obterFiltroForm(arrecadador, habilitado);
				controladorArrecadador.validarExistente(filtro);
				controladorArrecadador.atualizar(arrecadador);
				model.addAttribute(CHAVE_PRIMARIA, arrecadador.getChavePrimaria());
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Arrecadador.ARRECADADOR_CAMPO_STRING);
				retorno = pesquisarArrecadador(arrecadador, result, String.valueOf(arrecadador.isHabilitado()), model);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				mensagemErro(model, new NegocioException(erros));
			}
		}

		return retorno;
	}

	/**
	 * Obter filtro form.
	 * 
	 * @param arrecadador
	 * @return a map (filtro)
	 */
	private Map<String, Object> obterFiltroForm(Arrecadador arrecadador, String habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (arrecadador.getChavePrimaria() > 0) {
			Long chavePrimaria = arrecadador.getChavePrimaria();
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		}

		filtro.put("codigoAgente", arrecadador.getCodigoAgente());

		if (arrecadador.getBanco() != null && arrecadador.getBanco().getChavePrimaria() > 0) {
			Long banco = arrecadador.getBanco().getChavePrimaria();
			filtro.put("banco", banco);
		}

		if (arrecadador.getCliente() != null && arrecadador.getCliente().getChavePrimaria() > 0) {
			Long cliente = arrecadador.getCliente().getChavePrimaria();
			filtro.put("idCliente", cliente);
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put("habilitado", Boolean.parseBoolean(habilitado));
		}

		return filtro;
	}
}
