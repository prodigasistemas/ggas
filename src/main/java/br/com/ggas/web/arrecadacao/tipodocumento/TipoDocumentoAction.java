/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 02/12/2013 11:31:53
 * @author wcosta
 */

package br.com.ggas.web.arrecadacao.tipodocumento;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoModelo;
import br.com.ggas.arrecadacao.documento.DocumentoModelo;
import br.com.ggas.arrecadacao.impl.TipoDocumentoImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelas ações relacionadas as telas que manipulam a entidade {@link TipoDocumento}
 *
 */
@Controller
public class TipoDocumentoAction extends GenericAction {

	private static final String LISTA_DOCUMENTO_MODELO = "listaDocumentoModelo";

	private static final String TIPO_DOCUMENTO = "tipoDocumento";

	private static final Logger LOG = Logger.getLogger(TipoDocumentoAction.class);

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String INDICADOR_PAGAVEL = "indicadorPagavel";

	private static final String INDICADOR_CODIGO_BARRAS = "indicadorCodigoBarras";

	private static final String HABILITADO = "habilitado";

	private String retorno;

	@Autowired
	@Qualifier("controladorTipoDocumento")
	private ControladorTipoDocumento controladorTipoDocumento;

	@Autowired
	@Qualifier("controladorDocumentoModelo")
	private ControladorDocumentoModelo controladorDocumentoModelo;

	@Autowired
	@Qualifier("controladorArrecadacao")
	private ControladorArrecadacao controladorArrecadacao;

	/**
	 * Exibe resultado da pesquisa de tipo documento
	 * 
	 * @return exibirPesquisaTipoDocumento
	 */
	@RequestMapping("exibirPesquisaTipoDocumento")
	public String exibirPesquisaTipoDocumento() {

		return "exibirPesquisaTipoDocumento";
	}

	/**
	 * Pesquisa tipo documento
	 * 
	 * @param tipoDocumento
	 * @param results
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return exibirPesquisaTipoDocumento
	 */
	@RequestMapping("pesquisarTipoDocumento")
	public String pesquisarTipoDocumento(@ModelAttribute("TipoDocumentoImpl") TipoDocumentoImpl tipoDocumento, BindingResult results,
			@RequestParam(value = HABILITADO, required = false) String habilitado, HttpServletRequest request, Model model) {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, tipoDocumento, habilitado);

		adicionarFiltroPaginacao(request, filtro);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
		model.addAttribute("listaTipoDocumento",
				criarColecaoPaginada(request, filtro, controladorTipoDocumento.listarTipoDocumento(filtro)));

		return "exibirPesquisaTipoDocumento";
	}

	/**
	 * Exibe tela de detalhamento do tipo de documento
	 * 
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @return exibirDetalharTipoDocumento
	 * @throws GGASException
	 */
	@RequestMapping("exibirDetalharTipoDocumento")
	public String exibirDetalharTipoDocumento(@RequestParam("chavePrimaria") Long chavePrimaria, Model model) throws GGASException {
		TipoDocumentoImpl tipoDocumento = (TipoDocumentoImpl) controladorTipoDocumento.obter(chavePrimaria);
		model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
		model.addAttribute(HABILITADO, tipoDocumento.isHabilitado());
		return "exibirDetalharTipoDocumento";
	}

	/**
	 * Exibi tela de inclusao do tipo documento
	 * 
	 * @param tipoDocumento
	 * @param results
	 * @param habilitado
	 * @param model
	 * @return exibirIncluirTipoDocumento
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("exibirIncluirTipoDocumento")
	public String exibirIncluirTipoDocumento(@ModelAttribute("TipoDocumentoImpl") TipoDocumentoImpl tipoDocumento, BindingResult results,
			@RequestParam(value = HABILITADO, required = false) String habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltro(filtro, tipoDocumento, habilitado);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
		model.addAttribute(LISTA_DOCUMENTO_MODELO, (Collection<DocumentoModelo>) controladorDocumentoModelo.obterTodas(true));

		return "exibirIncluirTipoDocumento";
	}

	/**
	 * Inclui tipo documento
	 * 
	 * @param tipoDocumento
	 * @param results
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirTipoDocumento")
	public String incluirTipoDocumento(@ModelAttribute("TipoDocumentoImpl") TipoDocumentoImpl tipoDocumento, BindingResult results,
			@RequestParam(value = HABILITADO, required = false) String habilitado, HttpServletRequest request, Model model)
			throws GGASException {
		model.addAttribute(LISTA_DOCUMENTO_MODELO, (Collection<DocumentoModelo>) controladorDocumentoModelo.obterTodas(true));
		retorno = "exibirIncluirTipoDocumento";
		Map<String, Object> erros = tipoDocumento.validarDados();
		model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
		model.addAttribute(HABILITADO, habilitado);
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorTipoDocumento.validarExistenciaTipoDocumento(tipoDocumento);
				tipoDocumento.setDadosAuditoria(getDadosAuditoria(request));
				tipoDocumento.setHabilitado(true);
				model.addAttribute(TIPO_DOCUMENTO,
						controladorArrecadacao.obterTipoDocumento(controladorTipoDocumento.inserir(tipoDocumento)));
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Constantes.TIPO_DE_DOCUMENTO);
				retorno = pesquisarTipoDocumento(tipoDocumento, null, String.valueOf(tipoDocumento.isHabilitado()), request, model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * Exibi tela de alteração do tipo documento
	 * 
	 * @param tipoDocumentoImpl
	 * @param results
	 * @param chavePrimaria
	 * @param habilitado
	 * @param model
	 * @return exibirAlterarTipoDocumento
	 * @throws GGASException
	 */
	@RequestMapping("exibirAlterarTipoDocumento")
	@SuppressWarnings("unchecked")
	public String exibirAlterarTipoDocumento(@ModelAttribute("TipoDocumentoImpl") TipoDocumentoImpl tipoDocumentoImpl, BindingResult results,
			@RequestParam("chavePrimaria") Long chavePrimaria, @RequestParam(value = HABILITADO, required = false) String habilitado,
			Model model) throws GGASException {
		TipoDocumentoImpl tipoDocumento = tipoDocumentoImpl;
		model.addAttribute(HABILITADO, habilitado);
		tipoDocumento = (TipoDocumentoImpl) controladorTipoDocumento.obter(chavePrimaria);
		model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
		if(tipoDocumento.getModelos() != null){
			model.addAttribute("modeloDocumento", tipoDocumento.getModelos());			
		}
		model.addAttribute(LISTA_DOCUMENTO_MODELO, (Collection<DocumentoModelo>) controladorDocumentoModelo.obterTodas(true));

		return "exibirAlterarTipoDocumento";
	}

	/**
	 * Altera tipo documento
	 * 
	 * @param tipoDocumento
	 * @param results
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("alterarTipoDocumento")
	public String alterarTipoDocumento(@ModelAttribute("TipoDocumentoImpl") TipoDocumentoImpl tipoDocumento, BindingResult results,
			@RequestParam(value = HABILITADO, required = false) String habilitado, HttpServletRequest request, Model model)
			throws GGASException {
		model.addAttribute(LISTA_DOCUMENTO_MODELO, (Collection<DocumentoModelo>) controladorDocumentoModelo.obterTodas(true));
		retorno = "exibirAlterarTipoDocumento";
		model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
		model.addAttribute(HABILITADO, habilitado);
		Map<String, Object> erros = tipoDocumento.validarDados();
		if (erros != null && !erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorTipoDocumento.validarExistenciaTipoDocumento(tipoDocumento);
				controladorTipoDocumento.atualizar(tipoDocumento);
				model.addAttribute(TIPO_DOCUMENTO, tipoDocumento);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Constantes.TIPO_DE_DOCUMENTO);
				retorno = pesquisarTipoDocumento(tipoDocumento, null, String.valueOf(tipoDocumento.isHabilitado()), request, model);
			} catch (ConcorrenciaException | NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}

	/**
	 * Remove tipo documento
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return forward:pesquisarTipoDocumento
	 * @throws GGASException
	 */
	@RequestMapping("removerTipoDocumento")
	public String removerTipoDocumento(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws GGASException {

		validarSelecaoUmOuMais(chavesPrimarias);

		for (Long chavePrimaria : chavesPrimarias) {
			try {
				controladorTipoDocumento.remover(chavePrimaria, getDadosAuditoria(request));
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, Constantes.TIPO_DE_DOCUMENTO);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_TIPO_DOCUMENTO_EM_USO, e));
			}
		}

		return "forward:pesquisarTipoDocumento";
	}

	/**
	 * Prepara filtro
	 * 
	 * @param filtro
	 * @param tipodocumento
	 * @param habilitado
	 */
	private void prepararFiltro(Map<String, Object> filtro, TipoDocumentoImpl tipodocumento, String habilitado) {

		if (tipodocumento.getChavePrimaria() > 0) {
			filtro.put(CHAVE_PRIMARIA, tipodocumento.getChavePrimaria());
		}

		if (tipodocumento.getDescricao() != null) {
			filtro.put(DESCRICAO, StringUtils.trim(tipodocumento.getDescricao()));
		}

		if (tipodocumento.getDescricaoAbreviada() != null) {
			filtro.put(DESCRICAO_ABREVIADA, StringUtils.trim(tipodocumento.getDescricaoAbreviada()));
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.parseBoolean(habilitado));
		}

		if (tipodocumento.getIndicadorPagavel() != null && !"null".equals(tipodocumento.getIndicadorPagavel())) {
			if ("1".equals(tipodocumento.getIndicadorPagavel())) {
				filtro.put(INDICADOR_PAGAVEL, true);
			} else {
				filtro.put(INDICADOR_PAGAVEL, false);
			}
		}

		if (tipodocumento.getIndicadorCodigoBarras() != null && !"null".equals(tipodocumento.getIndicadorCodigoBarras())) {
			if ("1".equals(tipodocumento.getIndicadorCodigoBarras())) {
				filtro.put(INDICADOR_CODIGO_BARRAS, true);
			} else {
				filtro.put(INDICADOR_CODIGO_BARRAS, false);
			}
		}
	}

	/**
	 * Exibe modelo tipo documento
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param response
	 * @throws GGASException
	 * @throws IOException
	 */
	@RequestMapping(value="exibirModeloTipoDocumento/{chavePrimaria}")
	public void exibirModeloTipoDocumento(@PathVariable("chavePrimaria") Long chavePrimaria, HttpServletResponse response)
			throws GGASException {

		DocumentoModelo documentoModelo = (DocumentoModelo) controladorDocumentoModelo.obter(chavePrimaria);

		byte[] modelo = documentoModelo.getModelo();

		if (modelo != null) {

			try {

				ByteArrayInputStream in = new ByteArrayInputStream(modelo);

				int index = 0;

				byte[] bytearray = new byte[in.available()];

				response.reset();
				response.setContentType("image/jpeg");

				while ((index = in.read(bytearray)) != -1) {
					response.getOutputStream().write(bytearray, 0, index);
				}
				response.flushBuffer();
				in.close();

			} catch (IOException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			} finally {
				try {
					response.getOutputStream().flush();
					response.getOutputStream().close();
				} catch (IOException e) {
					LOG.error(e);
				}
			}
		}
	}
}
