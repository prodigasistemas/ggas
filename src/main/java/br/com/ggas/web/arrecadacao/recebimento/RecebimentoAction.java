/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.arrecadacao.recebimento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.arrecadacao.recebimento.impl.RecebimentoSituacaoImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Pair;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.fatura.FaturaVO;

/**
 * Classe responsável pelas telas relacionadas
 * a recebimentos financeiros
 *
 */
@Controller
public class RecebimentoAction extends GenericAction {
	
	private static final String IMOVEL = "imovel";

	private static final String CLIENTE = "cliente";

	private static final String RECEBIMENTO_VO = "recebimentoVO";

	private static final String IMPOSSIVEL_ALTERAR_RECEBIMENTO_ESTORNADO = "IMPOSSIVEL_ALTERAR_RECEBIMENTO_ESTORNADO";

	private static final String RECEBIMENTOS_ESCOLHIDOS_ESTORNADOS = "RECEBIMENTOS_ESCOLHIDOS_ESTORNADOS";
	
	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";

	private static final String DATA_DO_RECEBIMENTO = "Data do Recebimento";

	private static final String ENDERECOS = "enderecos";

	private static final String GERAR_RELATORIO_RECIBO_QUITACAO = "gerarRelatorioReciboQuitacao";

	private static final String CHAVES_RECEBIMENTOS = "chavesRecebimentos";

	private static final String INTERVALO_ANOS_ATE_DATA_ATUAL = "intervaloAnosAteDataAtual";

	private static final String INTERVALO_EMISSAO_DOCUMENTOS = "Intervalo de Emissão de Documentos";

	private static final String INTERVALO_EMISSAO_RECEBIMENTOS = "Intervalo de Emissão de Recebimentos";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String ID_RECEBIMENTO_SITUACAO = "idRecebimentoSituacao";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ID_TIPO_DOCUMENTO = "idTipoDocumento";

	private static final String ID_ARRECADADOR = "idArrecadador";

	private static final String DESCRICAO_ARRECADADOR = "descricaoArrecadador";

	private static final String DESCRICAO_AVISO_BANCARIO = "descricaoAvisoBancario";

	private static final String ID_FORMA_ARRECADACAO = "idFormaArrecadacao";

	private static final String FORMA_ARRECADACAO = "formaArrecadacao";

	private static final String OBSERVACAO = "observacao";

	private static final String ANO_MES_CONTABIL_INICIAL = "anoMesContabilInicial";

	private static final String ANO_MES_CONTABIL_FINAL = "anoMesContabilFinal";

	private static final String VALOR_RECEBIMENTO = "valorRecebimento";

	private static final String DATA_RECEBIMENTO_INICIAL = "dataRecebimentoInicial";

	private static final String DATA_RECEBIMENTO_FINAL = "dataRecebimentoFinal";

	private static final String DATA_RECEBIMENTO_INCLUSAO = "dataRecebimentoInclusao";

	private static final String LISTA_RECEBIMENTO = "listaRecebimento";

	private static final String LISTA_ARRECADADOR = "listaArrecadador";

	private static final String LISTA_RECEBIMENTO_SITUACAO = "listaRecebimentoSituacao";

	private static final String LISTA_TIPO_DOCUMENTO = "listaTipoDocumento";

	private static final String LISTA_FATURA = "listaFatura";

	private static final String LISTA_CONTRATO_PONTO_CONSUMOS = "listaContratoPontoConsumos";

	private static final String CHAVES_PRIMARIAS_FATURAS = "chavesPrimariasFaturas";

	private static final String LISTA_FORMA_ARRECADACAO = "listaFormaArrecadacao";

	private static final String DOCUMENTO_NUMERO = "documentoNumero";

	private static final String DOCUMENTO_CICLO_REFERENCIA = "documentoCicloReferencia";

	private static final String DOCUMENTO_VALOR = "documentoValor";

	private static final String DOCUMENTO_DATA_EMISSAO = "documentoDataEmissao";

	private static final String DOCUMENTO_VENCIMENTO = "documentoVencimento";

	private static final String DOCUMENTO_TIPO = "documentoTipo";

	private static final String PARAMETRO_REFERENCIA_CONTABIL = "REFERENCIA_CONTABIL";

	private static final String DATA_DOCUMENTO_INICIAL = "dataDocumentoInicial";

	private static final String DATA_DOCUMENTO_FINAL = "dataDocumentoFinal";

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorContrato controladorContrato;
	
	@Autowired
	private ControladorRecebimento controladorRecebimento;
	
	@Autowired
	private ControladorFatura controladorFatura;
	
	@Autowired
	private ControladorArrecadacao controladorArrecadacao;
	
	@Autowired
	private ControladorCobranca controladorCobranca;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	@Autowired
	private ControladorImovel controladorImovel;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	private ControladorCreditoDebito controladorCreditoDebito;
	
	/**
	 * Método responsável por exibir a tela de
	 * pesquisa de recebimento.
	 *
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param bindingResut {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaRecebimento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRecebimento")
	public String exibirPesquisaRecebimento(RecebimentoVO recebimentoVO, BindingResult bindingResut,
			HttpServletRequest request, Model model) throws GGASException {

		request.getSession().removeAttribute(CHAVES_PRIMARIAS_FATURAS);
		request.getSession().removeAttribute(CHAVES_RECEBIMENTOS);

		model.addAttribute(LISTA_ARRECADADOR, controladorArrecadacao.listarArrecadadorExistenteDescricao());
		model.addAttribute(LISTA_RECEBIMENTO_SITUACAO, controladorRecebimento.listarRecebimentoSituacaoExistente());
		model.addAttribute(LISTA_TIPO_DOCUMENTO, controladorArrecadacao.listarTipoDocumentoRecebimento());
		model.addAttribute(INTERVALO_ANOS_ATE_DATA_ATUAL, intervaloAnosAteDataAtual());

		Map<String, String> datas = Util.dataMinimaMaxima();

		model.addAttribute("dataMinima", datas.get("dataMinima"));
		model.addAttribute("dataMaxima", datas.get("dataMaxima"));
		model.addAttribute(RECEBIMENTO_VO, recebimentoVO);

		if (recebimentoVO.getIdImovel() != null && recebimentoVO.getIdImovel() > 0) {
			Collection<ContratoPontoConsumo> listaContratoPontoConsumos = controladorContrato
					.consultarPontoConsumoPorImovelComContrato(recebimentoVO.getIdImovel());
			model.addAttribute(LISTA_CONTRATO_PONTO_CONSUMOS, listaContratoPontoConsumos);
		}

		return "exibirPesquisaRecebimento";
	}

	/**
	 * Método responsável por exibir a tela de
	 * pesquisa de Recebimento.
	 *
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirPesquisaRecebimento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarRecebimento")
	public String pesquisarRecebimento(RecebimentoVO recebimentoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(CHAVES_PRIMARIAS, null);

		if (recebimentoVO.getIdCliente() != null && recebimentoVO.getIdCliente() > 0) {
			model.addAttribute(CLIENTE, controladorCliente.obter(recebimentoVO.getIdCliente()));
		}
		if (recebimentoVO.getIdImovel() != null && recebimentoVO.getIdImovel() > 0) {
			model.addAttribute(IMOVEL, controladorImovel.obter(recebimentoVO.getIdImovel()));
		}

		try {

			if ((recebimentoVO.getIdImovel() != null && recebimentoVO.getIdImovel() > 0)
					&& (recebimentoVO.getIdPontoConsumo() == null
							|| recebimentoVO.getIdPontoConsumo().intValue() < 1)) {

				Collection<ContratoPontoConsumo> listaContratoPontoConsumos = controladorContrato
						.consultarPontoConsumoPorImovelComContrato(recebimentoVO.getIdImovel());
				model.addAttribute(LISTA_CONTRATO_PONTO_CONSUMOS, listaContratoPontoConsumos);

			} else {

				Map<String, Object> filtro = new HashMap<String, Object>();
				prepararFiltro(filtro, recebimentoVO);
				controladorRecebimento.validarFiltroPesquisaRecebimento(filtro);

				Collection<Recebimento> listaRecebimento = controladorRecebimento.consultarRecebimento(filtro);

				Collection<RecebimentoVO> listaRecebimentoVO = new ArrayList<RecebimentoVO>();

				for (Recebimento recebimento : listaRecebimento) {
					RecebimentoVO recebimentoVOTmp = new RecebimentoVO();

					recebimentoVOTmp.setRecebimento(recebimento);

					if (recebimento.getFaturaGeral() != null) {
						recebimentoVOTmp.setSaldo(
								controladorFatura.calcularSaldoFatura(recebimento.getFaturaGeral().getFaturaAtual()));
					}

					listaRecebimentoVO.add(recebimentoVOTmp);
				}

				model.addAttribute(LISTA_RECEBIMENTO, listaRecebimentoVO);
			}

			model.addAttribute(INTERVALO_ANOS_ATE_DATA_ATUAL, intervaloAnosAteDataAtual());

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return exibirPesquisaRecebimento(recebimentoVO, bindingResult, request, model);
	}
	
	/**
	 * Método responsável por exibir a tela de
	 * detalhamento de um Recebimento.
	 * 
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirDetalhamentoRecebimento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirDetalhamentoRecebimento")
	public String exibirDetalhamentoRecebimento(RecebimentoVO recebimentoVO, BindingResult bindingResult,
			HttpServletRequest request, Model model) throws GGASException {

		String view = "exibirDetalhamentoRecebimento";

		try {

			Recebimento recebimento = (Recebimento) controladorRecebimento.obter(recebimentoVO.getChavePrimaria(),
					"recebimentoSituacao", "pontoConsumo");

			model.addAttribute("listaCreditoDebitoNegociado", controladorCreditoDebito
					.listarCreditoDebitoNegociadoPorRecebimento(recebimentoVO.getChavePrimaria()));

			if (recebimento != null) {
				popularForm(recebimento, model);
			}

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarRecebimento(recebimentoVO, bindingResult, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de
	 * inclusão de Recebimento.
	 *
	 * @param recebimentoVO {@link}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirInclusaoRecebimento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoRecebimento")
	public String exibirInclusaoRecebimento(RecebimentoVO recebimentoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		model.addAttribute(RECEBIMENTO_VO, recebimentoVO);

		saveToken(request);

		model.addAttribute(INTERVALO_ANOS_ATE_DATA_ATUAL, intervaloAnosAteDataAtual());

		request.getSession().removeAttribute(CHAVES_PRIMARIAS_FATURAS);
		request.getSession().removeAttribute(CHAVES_RECEBIMENTOS);
		request.removeAttribute(GERAR_RELATORIO_RECIBO_QUITACAO);

		return "exibirInclusaoRecebimento";
	}
	
	/**
	 * Método responsável por inserir um
	 * recebimento.
	 * 
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param result {@link BindingResult}
	 * @param chavesPrimariasFaturas {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirRecebimento")
	public String incluirRecebimento(RecebimentoVO recebimentoVO, BindingResult result,
			@RequestParam(value = CHAVES_PRIMARIAS_FATURAS, required = false) Long[] chavesPrimariasFaturas,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		model.addAttribute(RECEBIMENTO_VO, recebimentoVO);
		model.addAttribute(CHAVES_PRIMARIAS_FATURAS, chavesPrimariasFaturas);
		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);
		Recebimento recebimento = (Recebimento) controladorRecebimento.criar();

		try {
			validarToken(request);
			popularRecebimento(recebimento, recebimentoVO, request);

			if(("0,00").equals(recebimentoVO.getValorRecebimento())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_VALOR_ZERO, new Object[] {Recebimento.VALOR_RECEBIMENTO});
			}
			
			Long[] chavesRecebimentos = controladorRecebimento.inserirRecebimento(recebimento, chavesPrimariasFaturas,
					dadosAuditoria);

			if ((chavesPrimariasFaturas != null) && (chavesRecebimentos != null)) {
				request.getSession().setAttribute(CHAVES_PRIMARIAS_FATURAS, chavesPrimariasFaturas);
				request.getSession().setAttribute(CHAVES_RECEBIMENTOS, chavesRecebimentos);
				request.setAttribute(GERAR_RELATORIO_RECIBO_QUITACAO, true);
			}

			recebimentoVO.setDataRecebimentoInicial(
					Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));
			recebimentoVO.setDataRecebimentoFinal(
					Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Recebimento.RECEBIMENTO_CAMPO_STRING);
			view = pesquisarRecebimento(recebimentoVO, result, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirFaturasRecebimento(recebimentoVO, result, request, model);
		}

		return view;
	}

	/**
	 * Método responsável por exibir a tela de
	 * alteração de um Recebimento.
	 * 
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param bindingResult {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirAlteracaoRecebimento")
	public String exibirAlteracaoRecebimento(RecebimentoVO recebimentoVO, BindingResult bindingResult, HttpServletRequest request,
			Model model) throws GGASException {

		saveToken(request);
		String view = "exibirAlteracaoRecebimento";
		
		try {
			Long chavePrimaria = recebimentoVO.getChavePrimaria();

			Recebimento recebimento = (Recebimento) controladorRecebimento.obter(chavePrimaria);
			String recebimentoEstornado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_ESTORNADO);
			
			if (recebimento != null && recebimento.getRecebimentoSituacao().getChavePrimaria() == Long.parseLong(recebimentoEstornado)) {
				throw new NegocioException(IMPOSSIVEL_ALTERAR_RECEBIMENTO_ESTORNADO);
			}

			if (recebimento != null) {
				
				Long idPontoConsumo = 0L;
				if (recebimentoVO.getIdPontoConsumo() != null) {
					idPontoConsumo = recebimentoVO.getIdPontoConsumo();
				}

				if (idPontoConsumo == 0 && recebimento.getPontoConsumo() != null) {
					idPontoConsumo = recebimento.getPontoConsumo().getChavePrimaria();
				}

				Long idCliente = null;
				if (recebimentoVO.getIdCliente() != null) {
					idCliente = recebimentoVO.getIdCliente();
				}

				if (idCliente != null && idCliente == 0 && recebimento.getCliente() != null) {
					idCliente = recebimento.getCliente().getChavePrimaria();
				}

				if (idCliente != null && recebimento.getCliente().getChavePrimaria() != idCliente) {
					Cliente cliente = controladorCliente.obterCliente(idCliente);
					recebimento.setCliente(cliente);
				}

				Collection<ContratoPontoConsumo> listaContratoPontoConsumos = null;
				if (recebimento.getPontoConsumo() != null) {
					Long[] chavesPontoConsumo = new Long[] {0L};
					chavesPontoConsumo[0] = recebimento.getPontoConsumo().getChavePrimaria();
					listaContratoPontoConsumos = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(chavesPontoConsumo);
				}

				model.addAttribute(LISTA_CONTRATO_PONTO_CONSUMOS, listaContratoPontoConsumos);

				if (idPontoConsumo == null && !listaContratoPontoConsumos.isEmpty()) {
					ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) listaContratoPontoConsumos.toArray()[0];
					idPontoConsumo = contratoPontoConsumo.getPontoConsumo().getChavePrimaria();
				}

				Map<String, Object> filtro = new HashMap<String, Object>();
				
				if (recebimento.getDocumentoCobrancaItem() != null) {
					DocumentoCobrancaItem documentoCobrancaItem = controladorArrecadacao.obterDocumentoCobrancaItem(recebimento.getDocumentoCobrancaItem()
									.getChavePrimaria());
					filtro.put(CHAVE_PRIMARIA, documentoCobrancaItem.getFaturaGeral().getFaturaAtual().getChavePrimaria());
				} else if (recebimento.getFaturaGeral() != null) {
					FaturaGeral faturageral = controladorArrecadacao.obterFaturaGeral(recebimento.getFaturaGeral().getChavePrimaria());
					filtro.put(CHAVE_PRIMARIA, faturageral.getFaturaAtual().getChavePrimaria());
				}

				Collection<Fatura> listaFatura = null;
				
				if (!filtro.isEmpty()) {
					listaFatura = controladorFatura.consultarFatura(filtro);
				}

				Collection<FaturaVO> listaFaturaVO = new ArrayList<FaturaVO>();
				if (listaFatura != null) {
					for (Fatura fatura : listaFatura) {
						FaturaVO faturaVO = new FaturaVO();
						faturaVO.setFatura(fatura);
						faturaVO.setSaldo(controladorFatura.calcularSaldoFatura(fatura));
						listaFaturaVO.add(faturaVO);
					}
				}
				model.addAttribute(LISTA_FATURA, listaFaturaVO);

				Collection<Arrecadador> listaArrecadador = controladorArrecadacao.listarArrecadadorExistenteDescricao();
				model.addAttribute(LISTA_ARRECADADOR, listaArrecadador);

				if(!("mensagemErro").equals(model.asMap().get("mensagemTipo"))) {
					popularFormAlteracao(recebimentoVO, recebimento, model);
				}
				
				obterClientePesquisa(recebimento.getCliente().getChavePrimaria(), model);
			}

			if(recebimentoVO.getIdArrecadador() != null && recebimentoVO.getIdArrecadador() > 0) {
				Long idArrecadador = recebimentoVO.getIdArrecadador();
				Collection<EntidadeConteudo> formasArrecadacao = controladorRecebimento.obterTiposConvenioPorArrecadador(idArrecadador);
				model.addAttribute(LISTA_FORMA_ARRECADACAO, formasArrecadacao);
			}

			model.addAttribute(INTERVALO_ANOS_ATE_DATA_ATUAL, intervaloAnosAteDataAtual());
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = pesquisarRecebimento(recebimentoVO, bindingResult, request, model);
		}

		return view;
	}
	
	/**
	 * Método responsável por alterar um Recebimento.
	 * 
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavesPrimariasFaturas {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarRecebimento")
	public String alterarRecebimento(RecebimentoVO recebimentoVO, BindingResult bindingResult,
			@RequestParam(value = CHAVES_PRIMARIAS_FATURAS, required = false) Long[] chavesPrimariasFaturas,
			HttpServletRequest request, Model model) throws GGASException {

		String view = null;
		model.addAttribute(RECEBIMENTO_VO, recebimentoVO);
		model.addAttribute(CHAVES_PRIMARIAS_FATURAS, chavesPrimariasFaturas);

		try {
			validarToken(request);

			Long chavePrimaria = recebimentoVO.getChavePrimaria();
			Recebimento recebimento = (Recebimento) controladorRecebimento.obter(chavePrimaria, "documentoCobrancaItem",
					"faturaGeral", "faturaGeral.faturaAtual.situacaoPagamento");

			if (recebimento.getDocumentoCobrancaItem() != null) {
				FaturaGeral faturaGeral = controladorArrecadacao
						.obterFaturaGeral(recebimento.getDocumentoCobrancaItem().getFaturaGeral().getChavePrimaria());
				recebimento.getDocumentoCobrancaItem().setFaturaGeral(faturaGeral);
			}

			popularRecebimentoAlteracao(recebimento, recebimentoVO, request);
			
			if(("0,00").equals(recebimentoVO.getValorRecebimento())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_VALOR_ZERO, new Object[] {Recebimento.VALOR_RECEBIMENTO});
			}

			chavesPrimariasFaturas = new Long[] { 0L };

			if (recebimento.getDocumentoCobrancaItem() != null) {
				DocumentoCobrancaItem documentoCobrancaItem = controladorArrecadacao
						.obterDocumentoCobrancaItem(recebimento.getDocumentoCobrancaItem().getChavePrimaria());
				chavesPrimariasFaturas[0] = documentoCobrancaItem.getFaturaGeral().getFaturaAtual().getChavePrimaria();
			} else if (recebimento.getFaturaGeral() != null) {
				FaturaGeral faturageral = controladorArrecadacao
						.obterFaturaGeral(recebimento.getFaturaGeral().getChavePrimaria());
				chavesPrimariasFaturas[0] = faturageral.getFaturaAtual().getChavePrimaria();
			}

			controladorRecebimento.atualizarRecebimentos(recebimento, chavesPrimariasFaturas);
			recebimentoVO.setDataRecebimentoInicial(
					Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));
			recebimentoVO.setDataRecebimentoFinal(
					Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(), Constantes.FORMATO_DATA_BR));

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Recebimento.RECEBIMENTO_CAMPO_STRING);

			model.addAttribute(RECEBIMENTO_VO, recebimentoVO);

			view = pesquisarRecebimento(recebimentoVO, bindingResult, request, model);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
			view = exibirAlteracaoRecebimento(recebimentoVO, bindingResult, request, model);
		}

		return view;
	}

	/**
	 * Popular recebimento.
	 * 
	 * @param recebimento {@link Recebimento}
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularRecebimento(Recebimento recebimento, RecebimentoVO recebimentoVO, HttpServletRequest request)
			throws GGASException {

		String dataRecebimentoString = recebimentoVO.getDataRecebimentoInclusao();
		String valorRecebimentoString = recebimentoVO.getValorRecebimento();
		String fluxoBaixaPorDacao = recebimentoVO.getFluxoBaixaPorDacao();
		String observacao = recebimentoVO.getObservacao().trim();

		Long idArrecadador = recebimentoVO.getIdArrecadador();
		Long idFormaArrecadacao = recebimentoVO.getIdFormaArrecadacao();
		Long idPontoConsumo = recebimentoVO.getIdPontoConsumo();
		Long idCliente = recebimentoVO.getIdCliente();

		BigDecimal valorRecebimento = BigDecimal.ZERO;

		controladorRecebimento.validarDadosRecebimento(idArrecadador, idFormaArrecadacao, dataRecebimentoString,
				valorRecebimentoString);

		if (StringUtils.isNotEmpty(valorRecebimentoString)) {
			valorRecebimento = Util.converterCampoStringParaValorBigDecimal("Valor Recebimento", valorRecebimentoString,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}

		Integer anoMesContabilParametro = Integer.valueOf(
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(PARAMETRO_REFERENCIA_CONTABIL));

		PontoConsumo pontoConsumo = null;
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo,
					"quadraFace.quadra.setorComercial");

		}

		Cliente cliente = null;
		if (idCliente != null && idCliente > 0) {
			cliente = controladorCliente.obterCliente(idCliente, ENDERECOS);
		}

		AvisoBancario avisoBancario = (AvisoBancario) controladorArrecadacao.criarAvisoBancario();
		Arrecadador arrecadador = null;

		if (idArrecadador != null && idArrecadador > 0) {
			arrecadador = controladorArrecadacao.obterArrecadador(idArrecadador);
			avisoBancario.setArrecadador(arrecadador);
			avisoBancario.setIndicadorCreditoDebito(true);
			avisoBancario.setValorContabilizado(BigDecimal.ZERO);
			avisoBancario.setValorRealizado(valorRecebimento);
			avisoBancario.setDataPrevista(new Date());
			avisoBancario.setDataRealizada(new Date());
			avisoBancario.setValorArrecadacaoCalculado(valorRecebimento);
			avisoBancario.setValorDevolucaoCalculado(BigDecimal.ZERO);
			avisoBancario.setValorArrecadacaoInformado(valorRecebimento);
			avisoBancario.setValorDevolucaoInformado(BigDecimal.ZERO);
			avisoBancario.setHabilitado(true);
			avisoBancario.setDadosAuditoria(getDadosAuditoria(request));

			if (idFormaArrecadacao != null && idFormaArrecadacao > 0) {

				EntidadeConteudo formaArrecadacao = controladorEntidadeConteudo
						.obterEntidadeConteudo(idFormaArrecadacao);
				recebimento.setFormaArrecadacao(formaArrecadacao);

				Collection<ArrecadadorContratoConvenio> colecaoArrecadadorContratoConvenio = controladorArrecadacao
						.consultarACConvenioPorArrecadador(idArrecadador, formaArrecadacao.getChavePrimaria());
				if (colecaoArrecadadorContratoConvenio != null && !colecaoArrecadadorContratoConvenio.isEmpty()) {
					avisoBancario
							.setContaBancaria(colecaoArrecadadorContratoConvenio.iterator().next().getContaCredito());
				}
			}
		}

		if (StringUtils.isNotEmpty(dataRecebimentoString) && arrecadador != null) {
			Date dataRecebimento = Util.converterCampoStringParaData(DATA_DO_RECEBIMENTO, dataRecebimentoString,
					Constantes.FORMATO_DATA_BR);

			Integer sequencialAvisoBancario = controladorArrecadacao.obterUltimoSequencialAvisoBancario(arrecadador,
					dataRecebimento);
			if (sequencialAvisoBancario == null) {
				sequencialAvisoBancario = Integer.valueOf(0);
			} else if (sequencialAvisoBancario > 0) {
				sequencialAvisoBancario += 1;
			}
			avisoBancario.setNumeroSequencial(sequencialAvisoBancario);
			avisoBancario.setAnoMesContabil(
					controladorArrecadacao.obterAnoMesContabil(anoMesContabilParametro, dataRecebimento));
			avisoBancario.setDataLancamento(dataRecebimento);
			recebimento.setDataRecebimento(dataRecebimento);
		}

		recebimento.setDadosAuditoria(getDadosAuditoria(request));
		recebimento.setAvisoBancario(avisoBancario);
		recebimento.setAnoMesContabil(anoMesContabilParametro);
		recebimento.setBaixado(false);
		recebimento.setCliente(cliente);
		recebimento.setPontoConsumo(pontoConsumo);
		recebimento.setObservacao(observacao);
		recebimento.setValorRecebimento(valorRecebimento);

		// Valores que serão nulos
		avisoBancario.setNumeroDocumento(null);
		avisoBancario.setArrecadadorMovimento(null);
		recebimento.setLocalidade(null);

		// Valores que serão preenchidos no controlador
		recebimento.setFaturaGeral(null);
		if (fluxoBaixaPorDacao != null && !fluxoBaixaPorDacao.isEmpty()) {
			recebimento.setRecebimentoSituacao(new RecebimentoSituacaoImpl());
		} else {
			recebimento.setRecebimentoSituacao(null);
		}

	}

	/**
	 * Popular recebimento alteracao.
	 * 
	 * @param recebimento {@link Recebimento}
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularRecebimentoAlteracao(Recebimento recebimento, RecebimentoVO recebimentoVO,
			HttpServletRequest request) throws GGASException {

		String dataRecebimentoString = recebimentoVO.getDataRecebimentoInclusao();
		String valorRecebimentoString = recebimentoVO.getValorRecebimento();
		String observacao = recebimentoVO.getObservacao().trim();
		Long idArrecadador = recebimentoVO.getIdArrecadador();
		Long idFormaArrecadacao = recebimentoVO.getIdFormaArrecadacao();
		Long idCliente = recebimentoVO.getIdCliente();
		BigDecimal valorRecebimento = BigDecimal.ZERO;

		controladorRecebimento.validarDadosRecebimento(idArrecadador, idFormaArrecadacao, dataRecebimentoString,
				valorRecebimentoString);

		if (StringUtils.isNotEmpty(valorRecebimentoString)) {
			valorRecebimento = Util.converterCampoStringParaValorBigDecimal("Valor Recebimento", valorRecebimentoString,
					Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			recebimento.setValorRecebimento(valorRecebimento);
		}

		AvisoBancario avisoBancario = controladorArrecadacao
				.obterAvisoBancario(recebimento.getAvisoBancario().getChavePrimaria());

		Arrecadador arrecadador = null;

		if (idArrecadador != null && idArrecadador > 0) {
			arrecadador = controladorArrecadacao.obterArrecadador(idArrecadador);
			avisoBancario.setArrecadador(arrecadador);
			avisoBancario.setValorRealizado(valorRecebimento);
			avisoBancario.setValorArrecadacaoCalculado(valorRecebimento);
			avisoBancario.setValorArrecadacaoInformado(valorRecebimento);

			if (idFormaArrecadacao != null && idFormaArrecadacao > 0) {
				EntidadeConteudo formaArrecadacao = controladorEntidadeConteudo
						.obterEntidadeConteudo(idFormaArrecadacao);
				recebimento.setFormaArrecadacao(formaArrecadacao);

				// TODO atualmente está pegando o primeiro item da lista.
				// Na homologação será definido como recuperar o contrato/contaBancária
				Collection<ArrecadadorContratoConvenio> colecaoArrecadadorContratoConvenio = controladorArrecadacao
						.consultarACConvenioPorArrecadador(idArrecadador, formaArrecadacao.getChavePrimaria());
				if (colecaoArrecadadorContratoConvenio != null) {
					avisoBancario
							.setContaBancaria(colecaoArrecadadorContratoConvenio.iterator().next().getContaCredito());
				}
			}
		}

		if (StringUtils.isNotEmpty(dataRecebimentoString) && arrecadador != null) {
			Integer anoMesContabilParametro = Integer.valueOf(
					(String) controladorParametroSistema.obterValorDoParametroPorCodigo(PARAMETRO_REFERENCIA_CONTABIL));
			Date dataRecebimento = Util.converterCampoStringParaData(DATA_DO_RECEBIMENTO, dataRecebimentoString,
					Constantes.FORMATO_DATA_BR);

			Integer sequencialAvisoBancario = controladorArrecadacao.obterUltimoSequencialAvisoBancario(arrecadador,
					dataRecebimento);

			if (sequencialAvisoBancario == null) {
				sequencialAvisoBancario = Integer.valueOf(0);
			} else if (sequencialAvisoBancario > 0) {
				sequencialAvisoBancario += 1;
			}
			avisoBancario.setNumeroSequencial(sequencialAvisoBancario);
			avisoBancario.setAnoMesContabil(
					controladorArrecadacao.obterAnoMesContabil(anoMesContabilParametro, dataRecebimento));
			avisoBancario.setDataLancamento(dataRecebimento);
			recebimento.setDataRecebimento(dataRecebimento);
		}
		avisoBancario.setDadosAuditoria(getDadosAuditoria(request));
		recebimento.setAvisoBancario(avisoBancario);

		if (StringUtils.isNotEmpty(dataRecebimentoString)) {
			Date dataRecebimento = Util.converterCampoStringParaData(DATA_DO_RECEBIMENTO, dataRecebimentoString,
					Constantes.FORMATO_DATA_BR);
			recebimento.setDataRecebimento(dataRecebimento);
		}

		if (idFormaArrecadacao != null && (recebimento.getFormaArrecadacao() == null
				|| idFormaArrecadacao.compareTo(recebimento.getFormaArrecadacao().getChavePrimaria()) != 0)) {
			EntidadeConteudo formaArrecadacao = controladorEntidadeConteudo.obterEntidadeConteudo(idFormaArrecadacao);
			recebimento.setFormaArrecadacao(formaArrecadacao);
		}

		if (StringUtils.isNotEmpty(observacao)) {
			recebimento.setObservacao(observacao);
		}

		if (idCliente != null) {
			Cliente cliente = controladorCliente.obterCliente(idCliente);
			recebimento.setCliente(cliente);
		}

		recebimento.setDadosAuditoria(getDadosAuditoria(request));

	}

	/**
	 * Método responsável por estornar os recebimentos selecionados.
	 * 
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param bindingResult {@link BindingResult}
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return pesquisarRecebimento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("estornarRecebimento")
	public String estornarRecebimento(RecebimentoVO recebimentoVO, BindingResult bindingResult,
			@RequestParam(value = CHAVES_PRIMARIAS, required = false) Long[] chavesPrimarias,
			HttpServletRequest request, Model model) throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {

			String recebimentoEstornado = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_ESTORNADO);

			for (Long chaveRecebimento : chavesPrimarias) {
				if (chaveRecebimento != null && chaveRecebimento > 0) {
					Recebimento recebimento = (Recebimento) controladorRecebimento.obter(chaveRecebimento);

					if (recebimento != null && recebimento.getRecebimentoSituacao().getChavePrimaria() == Long
							.parseLong(recebimentoEstornado)) {
						throw new NegocioException(RECEBIMENTOS_ESCOLHIDOS_ESTORNADOS, true);
					}
				}
			}

			controladorRecebimento.estornarRecebimento(chavesPrimarias, dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ESTORNADO);

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return pesquisarRecebimento(recebimentoVO, bindingResult, request, model);
	}
	
	/**
	 * Intervalo anos ate data atual.
	 * 
	 * @return retorno {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	private String intervaloAnosAteDataAtual() throws GGASException {

		String valor = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_CALENDARIO);

		String retorno = "";
		DateTime dataAtual = new DateTime();
		DateTime dataInicial = dataAtual.minusYears(Integer.parseInt(valor));

		retorno = String.valueOf(dataInicial.getYear()) + ":" + dataAtual.getYear();

		return retorno;
	}

	/**
	 * Método responsável por exibir a lista de
	 * Faturas na inclusão de Recebimentos.
	 *
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return exibirInclusaoRecebimento {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirFaturasRecebimento")
	public String exibirFaturasRecebimento(RecebimentoVO recebimentoVO, BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		Long idCliente = recebimentoVO.getIdCliente();
		Long idPontoConsumo = recebimentoVO.getIdPontoConsumo();
		Long idImovel = recebimentoVO.getIdImovel();

		if ((idImovel != null && idImovel > 0) && (idPontoConsumo == null || idPontoConsumo.intValue() < 1)) {
			Collection<ContratoPontoConsumo> listaContratoPontoConsumos = controladorContrato
					.consultarPontoConsumoPorImovelComContrato(idImovel);
			model.addAttribute(LISTA_CONTRATO_PONTO_CONSUMOS, listaContratoPontoConsumos);
		} else {

			if (idImovel != null && idImovel > 0) {
				Collection<ContratoPontoConsumo> listaContratoPontoConsumos = controladorContrato
						.consultarPontoConsumoPorImovelComContrato(idImovel);
				model.addAttribute(LISTA_CONTRATO_PONTO_CONSUMOS, listaContratoPontoConsumos);
			}

			Collection<Fatura> listaFatura = controladorCobranca.consultarFaturasAbertasClientePontoConsumo(idCliente,
					idPontoConsumo, false);
			Collection<Arrecadador> listaArrecadador = controladorArrecadacao.listarArrecadadorExistenteDescricao();

			Collection<FaturaVO> listaFaturaVO = new ArrayList<FaturaVO>();

			for (Fatura fatura : listaFatura) {

				Pair<BigDecimal, BigDecimal> parSaldoFatura = controladorFatura.calcularSaldoFatura(fatura, true, true);
				BigDecimal saldo = parSaldoFatura.getFirst();
				BigDecimal saldoCorrigido = parSaldoFatura.getSecond();

				FaturaVO faturaVO = new FaturaVO();
				faturaVO.setFatura(fatura);
				faturaVO.setSaldo(saldo);
				faturaVO.setSaldoCorrigido(saldoCorrigido);

				listaFaturaVO.add(faturaVO);
			}

			model.addAttribute(LISTA_FATURA, listaFaturaVO);
			model.addAttribute(LISTA_ARRECADADOR, listaArrecadador);
		}

		model.addAttribute(INTERVALO_ANOS_ATE_DATA_ATUAL, intervaloAnosAteDataAtual());

		return exibirInclusaoRecebimento(recebimentoVO, result, request, model);
	}

	/**
	 * Calcular saldo fatura.
	 * 
	 * @param fatura {@link Fatura}
	 * @throws GGASException {@link GGASException}
	 */
	public void calcularSaldoFatura(Fatura fatura) throws GGASException {

		BigDecimal valorRecebimento = controladorCobranca.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
		if (valorRecebimento != null) {
			fatura.setValorTotal(fatura.getValorSaldoConciliado().subtract(valorRecebimento));
		}
	}
	
	/**
	 * Popular form.
	 * 
	 * @param recebimento {@link Recebimento}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularForm(Recebimento recebimento, Model model) throws GGASException {

		model.addAttribute(CHAVE_PRIMARIA, recebimento.getChavePrimaria());

		obterClientePesquisa(recebimento.getCliente().getChavePrimaria(), model);

		AvisoBancario avisoBancario = controladorArrecadacao
				.obterAvisoBancario(recebimento.getAvisoBancario().getChavePrimaria());
		model.addAttribute(DESCRICAO_AVISO_BANCARIO, avisoBancario.getDataRealizadaFormatada());

		Arrecadador arrecadador = controladorArrecadacao
				.obterArrecadador(avisoBancario.getArrecadador().getChavePrimaria());
		model.addAttribute(ID_ARRECADADOR, arrecadador.getChavePrimaria());

		if (arrecadador.getBanco() != null) {
			Banco banco = controladorCobranca.obterBanco(arrecadador.getBanco().getChavePrimaria());
			model.addAttribute(DESCRICAO_ARRECADADOR, banco.getNome());
		} else {
			Cliente clienteArrecadador = controladorCliente.obterCliente(arrecadador.getCliente().getChavePrimaria());
			model.addAttribute(DESCRICAO_ARRECADADOR, clienteArrecadador.getNome());
		}

		model.addAttribute(DATA_RECEBIMENTO_INCLUSAO,
				Util.converterDataParaStringSemHora(recebimento.getDataRecebimento(), Constantes.FORMATO_DATA_BR));

		EntidadeConteudo formaArrecadacao = controladorEntidadeConteudo
				.obterEntidadeConteudo(recebimento.getFormaArrecadacao().getChavePrimaria());
		model.addAttribute(ID_FORMA_ARRECADACAO, formaArrecadacao.getChavePrimaria());
		model.addAttribute(FORMA_ARRECADACAO, formaArrecadacao);

		model.addAttribute(VALOR_RECEBIMENTO, recebimento.getValorRecebimentoFormatado());

		if (recebimento.getFaturaGeral() != null) {
			FaturaGeral faturageral = controladorArrecadacao
					.obterFaturaGeral(recebimento.getFaturaGeral().getChavePrimaria());

			model.addAttribute(DOCUMENTO_NUMERO, "" + faturageral.getFaturaAtual().getChavePrimaria());

			Fatura faturaAtual = (Fatura) controladorCobranca
					.obterFatura(faturageral.getFaturaAtual().getChavePrimaria());
			model.addAttribute(DOCUMENTO_CICLO_REFERENCIA, faturaAtual.getCicloReferenciaFormatado());
			model.addAttribute(DOCUMENTO_VALOR, Util
					.converterCampoCurrencyParaString(faturaAtual.getValorSaldoConciliado(), Constantes.LOCALE_PADRAO));
			model.addAttribute(DOCUMENTO_DATA_EMISSAO,
					Util.converterDataParaStringSemHora(faturaAtual.getDataEmissao(), Constantes.FORMATO_DATA_BR));
			model.addAttribute(DOCUMENTO_VENCIMENTO,
					Util.converterDataParaStringSemHora(faturaAtual.getDataVencimento(), Constantes.FORMATO_DATA_BR));

			TipoDocumento tipoDocumento = controladorArrecadacao
					.obterTipoDocumento(faturaAtual.getTipoDocumento().getChavePrimaria());
			model.addAttribute(DOCUMENTO_TIPO, tipoDocumento.getDescricao());
		}

		if (recebimento.getRecebimentoSituacao() != null) {
			model.addAttribute("descricaoRecebimentoSituacao", recebimento.getRecebimentoSituacao().getDescricao());
		}

		if (recebimento.getObservacao() != null) {
			model.addAttribute(OBSERVACAO, recebimento.getObservacao());
		}

		if (recebimento.getPontoConsumo() != null) {
			recebimento.setPontoConsumo((PontoConsumo) controladorPontoConsumo
					.obter(recebimento.getPontoConsumo().getChavePrimaria(), IMOVEL, "cep"));
			model.addAttribute("pontoConsumoDescricaoFormatada",
					recebimento.getPontoConsumo().getChavePrimaria() + " - "
							+ recebimento.getPontoConsumo().getDescricao() + " - "
							+ recebimento.getPontoConsumo().getImovel().getNome());
		}

	}

	/**
	 * Metodo responsavél por obter Cliente adicionando o mesmo ao model
	 * 
	 * @param recebimento {@link Long}
	 * @param model {@link Model}
	 * @throws NegocioException {@link NegocioException}
	 */
	public void obterClientePesquisa(Long idCliente, Model model) throws NegocioException {
		
		if (idCliente != null && idCliente > 0) {
			model.addAttribute(CLIENTE,
					controladorCliente.obter(idCliente, ENDERECOS));
		}
	}

	/**
	 * Popular form alteracao.
	 * 
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @param recebimento {@link Recebimento}
	 * @param model {@link Model}
	 * @throws GGASException {@link GGASException}
	 */
	private void popularFormAlteracao(RecebimentoVO recebimentoVO, Recebimento recebimento, Model model)
			throws GGASException {

		recebimentoVO.setChavePrimaria(recebimento.getChavePrimaria());

		obterClientePesquisa(recebimentoVO.getIdCliente(), model);

		AvisoBancario avisoBancario = controladorArrecadacao
				.obterAvisoBancario(recebimento.getAvisoBancario().getChavePrimaria());
		model.addAttribute(DESCRICAO_AVISO_BANCARIO, avisoBancario.getDataRealizadaFormatada());

		Arrecadador arrecadador = controladorArrecadacao
				.obterArrecadador(avisoBancario.getArrecadador().getChavePrimaria());
		recebimentoVO.setIdArrecadador(arrecadador.getChavePrimaria());
		if (arrecadador.getBanco() != null) {
			Banco banco = controladorCobranca.obterBanco(arrecadador.getBanco().getChavePrimaria());
			model.addAttribute(DESCRICAO_ARRECADADOR, banco.getNome());
		} else {
			Cliente clienteArrecadador = controladorCliente.obterCliente(arrecadador.getCliente().getChavePrimaria());
			model.addAttribute(DESCRICAO_ARRECADADOR, clienteArrecadador.getNome());
		}

		model.addAttribute(DATA_RECEBIMENTO_INCLUSAO,
				Util.converterDataParaStringSemHora(recebimento.getDataRecebimento(), Constantes.FORMATO_DATA_BR));

		EntidadeConteudo formaArrecadacao = controladorEntidadeConteudo
				.obterEntidadeConteudo(recebimento.getFormaArrecadacao().getChavePrimaria());
		recebimentoVO.setIdFormaArrecadacao(formaArrecadacao.getChavePrimaria());
		model.addAttribute(FORMA_ARRECADACAO, formaArrecadacao);

		recebimentoVO.setValorRecebimento(recebimento.getValorRecebimentoFormatado());

		if (recebimento.getFaturaGeral() != null) {
			FaturaGeral faturageral = controladorArrecadacao
					.obterFaturaGeral(recebimento.getFaturaGeral().getChavePrimaria());
			model.addAttribute(DOCUMENTO_NUMERO, "" + faturageral.getChavePrimaria());

			Fatura faturaAtual = (Fatura) controladorCobranca
					.obterFatura(faturageral.getFaturaAtual().getChavePrimaria());
			model.addAttribute(DOCUMENTO_CICLO_REFERENCIA, faturaAtual.getCicloReferenciaFormatado());
			model.addAttribute(DOCUMENTO_VALOR, faturaAtual.getValorTotalFormatado());
			model.addAttribute(DOCUMENTO_DATA_EMISSAO,
					Util.converterDataParaStringSemHora(faturaAtual.getDataEmissao(), Constantes.FORMATO_DATA_BR));
			model.addAttribute(DOCUMENTO_VENCIMENTO,
					Util.converterDataParaStringSemHora(faturaAtual.getDataVencimento(), Constantes.FORMATO_DATA_BR));

			TipoDocumento tipoDocumento = controladorArrecadacao
					.obterTipoDocumento(faturaAtual.getTipoDocumento().getChavePrimaria());
			model.addAttribute(DOCUMENTO_TIPO, tipoDocumento.getDescricao());
		}

		if (recebimento.getObservacao() != null) {
			recebimentoVO.setObservacao(recebimento.getObservacao());
			model.addAttribute(RECEBIMENTO_VO, recebimentoVO);
		}

		String indicadorPesquisa = null;
		if (recebimento.getCliente() != null) {
			indicadorPesquisa = "indicadorPesquisaCliente";
		} else if (recebimento.getPontoConsumo() != null) {
			indicadorPesquisa = "indicadorPesquisaImovel";
		}
		if (indicadorPesquisa != null) {
			model.addAttribute("indicadorPesquisa", indicadorPesquisa);
		}

	}
	
	/**
	 * Responsável em gerar o recibo de quitação.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param model {@link Model}
	 * @return responseEntity {@link ResponseEntity}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("imprimirReciboQuitacao")
	public ResponseEntity<byte[]> gerarRelatorioReciboQuitacao(
			@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria, Model model)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		Recebimento recebimento = (Recebimento) controladorRecebimento.obter(chavePrimaria, "faturaGeral", CLIENTE);

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf("PDF");
		filtro.put(FORMATO_IMPRESSAO, formatoImpressao);

		List<Recebimento> listaRecebimentos = new ArrayList<Recebimento>();

		ResponseEntity<byte[]> responseEntity = null;

		Long chaveFatura = null;

		if (recebimento != null) {
			listaRecebimentos.add(recebimento);
			if (recebimento.getFaturaGeral() != null) {
				chaveFatura = recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria();
			}
		}

		Long[] chavesFatura = null;

		if (chaveFatura != null) {

			chavesFatura = new Long[] { chaveFatura };

			byte[] relatorioReciboQuitacao = controladorRecebimento.gerarRelatorioReciboQuitacao(listaRecebimentos,
					chavesFatura);

			if (relatorioReciboQuitacao != null) {

				FormatoImpressao formatoImpressao2 = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);

				MediaType contentType = MediaType.parseMediaType(this.obterContentTypeRelatorio(formatoImpressao2));

				String filename = String.format("relatorioReciboQuitacao%s",
						this.obterFormatoRelatorio((FormatoImpressao) filtro.get(FORMATO_IMPRESSAO)));

				HttpHeaders headers = new HttpHeaders();

				headers.setContentType(contentType);
				headers.setContentDispositionFormData("attachment", filename);

				model.addAttribute(EXIBIR_MENSAGEM_TELA, false);

				responseEntity = new ResponseEntity<byte[]>(relatorioReciboQuitacao, headers, HttpStatus.OK);
			}
		}

		return responseEntity;
	}
	
	/**
	 * Preparar filtro.
	 * 
	 * @param filtro {@link Map}
	 * @param recebimentoVO {@link RecebimentoVO}
	 * @throws GGASException {@link RecebimentoVO}
	 */
	private void prepararFiltro(Map<String, Object> filtro, RecebimentoVO recebimentoVO) throws GGASException {

		String anoMesContabilInicial = recebimentoVO.getAnoMesContabilInicial();
		if (StringUtils.isNotEmpty(anoMesContabilInicial)) {
			anoMesContabilInicial = anoMesContabilInicial.substring(2, 6).concat(anoMesContabilInicial.substring(0, 2));
			filtro.put(ANO_MES_CONTABIL_INICIAL, anoMesContabilInicial);
		}

		String anoMesContabilFinal = recebimentoVO.getAnoMesContabilFinal();
		if (StringUtils.isNotEmpty(anoMesContabilFinal)) {
			anoMesContabilFinal = anoMesContabilFinal.substring(2, 6).concat(anoMesContabilFinal.substring(0, 2));
			filtro.put(ANO_MES_CONTABIL_FINAL, anoMesContabilFinal);
		}

		if (StringUtils.isNotEmpty(recebimentoVO.getDataRecebimentoInicial())) {
			filtro.put(DATA_RECEBIMENTO_INICIAL, Util.converterCampoStringParaData(INTERVALO_EMISSAO_RECEBIMENTOS,
					recebimentoVO.getDataRecebimentoInicial(), Constantes.FORMATO_DATA_BR));
		}
		if (StringUtils.isNotEmpty(recebimentoVO.getDataRecebimentoFinal())) {
			filtro.put(DATA_RECEBIMENTO_FINAL, Util.converterCampoStringParaData(INTERVALO_EMISSAO_RECEBIMENTOS,
					recebimentoVO.getDataRecebimentoFinal(), Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(recebimentoVO.getDataDocumentoInicial())) {
			filtro.put(DATA_DOCUMENTO_INICIAL, Util.converterCampoStringParaData(INTERVALO_EMISSAO_DOCUMENTOS,
					recebimentoVO.getDataDocumentoInicial(), Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(recebimentoVO.getDataDocumentoFinal())) {
			filtro.put(DATA_DOCUMENTO_FINAL, Util.converterCampoStringParaData(INTERVALO_EMISSAO_DOCUMENTOS,
					recebimentoVO.getDataDocumentoFinal(), Constantes.FORMATO_DATA_BR));
		}

		if (recebimentoVO.getIdArrecadador() != null && recebimentoVO.getIdArrecadador() > 0) {
			filtro.put(ID_ARRECADADOR, recebimentoVO.getIdArrecadador());
		}

		prepararFiltroDois(filtro, recebimentoVO);
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param filtro {@link Map}
	 * @param recebimentoVO {@link recebimentoVO}
	 */
	public void prepararFiltroDois(Map<String, Object> filtro, RecebimentoVO recebimentoVO) {

		if (recebimentoVO.getIdRecebimentoSituacao() != null && recebimentoVO.getIdRecebimentoSituacao() > 0) {
			filtro.put(ID_RECEBIMENTO_SITUACAO, recebimentoVO.getIdRecebimentoSituacao());
		}

		if (recebimentoVO.getIdTipoDocumento() != null && recebimentoVO.getIdTipoDocumento() > 0) {
			filtro.put(ID_TIPO_DOCUMENTO, recebimentoVO.getIdTipoDocumento());
		}

		if (recebimentoVO.getIdCliente() != null && recebimentoVO.getIdCliente() > 0) {
			filtro.put(ID_CLIENTE, recebimentoVO.getIdCliente());
		}

		if (recebimentoVO.getIdPontoConsumo() != null && recebimentoVO.getIdPontoConsumo() > 0) {
			filtro.put(ID_PONTO_CONSUMO, recebimentoVO.getIdPontoConsumo());
		}
	}
	
}
