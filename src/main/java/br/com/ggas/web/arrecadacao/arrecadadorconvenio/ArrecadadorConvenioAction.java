/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.arrecadacao.arrecadadorconvenio;

import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.impl.ArrecadadorContratoConvenioImpl;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe responsável pelas telas relacionadas ao Arrecadador Conveniado
 *
 */
@Controller
public class ArrecadadorConvenioAction extends GenericAction {

	private static final String ARRECADADOR_CONVENIO = "arrecadadorConvenio";

	private static final String TELA_EXIBIR_INCLUSAO_ARRECADADOR_CONVENIO = "exibirInclusaoArrecadadorConvenio";

	private static final String TELA_EXIBIR_ALTERACAO_ARRECADADOR_CONVENIO = "exibirAlteracaoArrecadadorConvenio";

	private static final String METODO_PESQUISAR_ARRECADADOR_CONVENIO = "forward:pesquisarArrecadadorConvenio";

	private static final String ARRECADADOR_CONTRATO_CONVENIO = "ARRECADADOR_CONTRATO_CONVENIO";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	
	private static final String ENTIDADE_TIPO_CONVENIO = "Tipo de Convênio";

	private static final String LISTA_ARRECADADOR_CONVENIO = "listaArrecadadorConvenio";

	private static final String LISTA_TIPO_CONVENIO = "listaTipoConvenio";
	
	private static final String LISTA_LEIAUTES = "listaLeiautes";

	private static final String LISTA_ARRECADADOR_CONTRATO = "listaArrecadadorContrato";

	private static final String LISTA_CONTA_CONVENIO = "listaContaConvenio";

	private static final String LISTA_CONTA_CREDITO = "listaContaCredito";

	private static final String LISTA_CARTEIRA_COBRANCA = "listaCarteiraCobranca";

	private static final String ID_ARRECADADOR_CONVENIO = "idArrecadadorConvenio";

	private static final String ID_TIPO_CONVENIO = "tipoConvenio";

	private static final String ID_ARRECADADOR_CONTRATO = "arrecadadorContrato";

	private static final String ID_CONTA_CONVENIO = "contaConvenio";

	private static final String ID_CONTA_CREDITO = "contaCredito";

	private static final String ID_CARTEIRA_COBRANCA = "arrecadadorCarteiraCobranca";		

	private static final String CODIGO_CONVENIO = "codigoConvenio";

	private String retorno;

	@Autowired
	@Qualifier("controladorArrecadadorConvenio")
	private ControladorArrecadadorConvenio controladorArrecadadorConvenio;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	@Qualifier("controladorDocumentoLayout")
	private ControladorDocumentoLayout controladorDocumentoLayout;

	/**
	 * Exibir pesquisa arrecadador convenio.
	 * 
	 * @param arrecadadorConvenio
	 * @param model
	 * @return exibirPesquisaArrecadadorConvenio
	 */
	@RequestMapping("exibirPesquisaArrecadadorConvenio")
	public String exibirPesquisaArrecadadorConvenio(
			@ModelAttribute("ArrecadadorContratoConvenioImpl") ArrecadadorContratoConvenioImpl arrecadadorConvenio, Model model)
			throws GGASException {

		popularListas(model);
		return "exibirPesquisaArrecadadorConvenio";
	}

	/**
	 * Pesquisar arrecadador convenio.
	 * 
	 * @param arrecadadorConvenio
	 * @param result
	 * @param request
	 * @param model
	 * @param habilitado
	 * @return exibirPesquisaArrecadadorConvenio
	 */
	@RequestMapping("pesquisarArrecadadorConvenio")
	public String pesquisarArrecadadorConvenio(
			@ModelAttribute("ArrecadadorContratoConvenioImpl") ArrecadadorContratoConvenioImpl arrecadadorConvenio, BindingResult result,
			@RequestParam(value = "habilitado", required = false) String habilitado, HttpServletRequest request, Model model)
			throws GGASException {
		popularListas(model);
		Map<String, Object> filtro = prepararFiltro(arrecadadorConvenio, habilitado);
		adicionarFiltroPaginacao(request, filtro);
		model.addAttribute("habilitado", habilitado);
		model.addAttribute(ARRECADADOR_CONVENIO, arrecadadorConvenio);
		model.addAttribute(LISTA_ARRECADADOR_CONVENIO,
				criarColecaoPaginada(request, filtro, controladorArrecadadorConvenio.consultarArrecadadorConvenio(filtro)));
		return "exibirPesquisaArrecadadorConvenio";
	}

	/**
	 * Exibir inclusao arrecadador convenio.
	 * 
	 * @param model
	 * @param arrecadadorConvenio
	 * @return exibirInclusaoArrecadadorConvenio
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_ARRECADADOR_CONVENIO)
	public String exibirInclusaoArrecadadorConvenio(
			@ModelAttribute("ArrecadadorContratoConvenioImpl") ArrecadadorContratoConvenioImpl arrecadadorConvenio, Model model)
			throws GGASException {
		adicionarparametroIdTipoConvenioDebitoAutomatico(model);
		model.addAttribute(ARRECADADOR_CONVENIO, arrecadadorConvenio);
		popularListas(model);
		return TELA_EXIBIR_INCLUSAO_ARRECADADOR_CONVENIO;
	}

	/**
	 * Incluir arrecadador convenio.
	 * 
	 * @param arrecadadorConvenio
	 * @param result
	 * @param request
	 * @param model
	 * @param leiaute
	 * @return pesquisarArrecadadorConvenio
	 */
	@RequestMapping("incluirArrecadadorConvenio")
	public String incluirArrecadadorConvenio(@RequestParam(value = "leiaute") Long leiaute,
			@ModelAttribute("ArrecadadorContratoConvenioImpl") ArrecadadorContratoConvenioImpl arrecadadorConvenio, 
			BindingResult result,
			HttpServletRequest request, Model model) throws GGASException {

		retorno = TELA_EXIBIR_INCLUSAO_ARRECADADOR_CONVENIO;
		if(leiaute != null){
			arrecadadorConvenio.setLeiaute((DocumentoLayout)
							controladorDocumentoLayout.obter(leiaute));
		}
		adicionarparametroIdTipoConvenioDebitoAutomatico(model);
		model.addAttribute(ARRECADADOR_CONVENIO, arrecadadorConvenio);
		Map<String, Object> erros = arrecadadorConvenio.validarDados();
		popularListas(model);
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				getDadosAuditoria(request);
				arrecadadorConvenio.setNsaRetorno(1L);
				arrecadadorConvenio.setNsaRemessa(1L);
				controladorArrecadadorConvenio.inserirArrecadadorContratoConvenio(arrecadadorConvenio);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, obterMensagem(ARRECADADOR_CONTRATO_CONVENIO));
				retorno = pesquisarArrecadadorConvenio(arrecadadorConvenio, result, String.valueOf(arrecadadorConvenio.isHabilitado()),
						request, model);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				mensagemErro(model, new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE_CONTRATO, e));
			}
		}

		return retorno;
	}

	/**
	 * Exibir detalhamento arrecadador convenio.
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return exibirDetalhamentoArrecadadorConvenio
	 */
	@RequestMapping("exibirDetalhamentoArrecadadorConvenio")
	public String exibirDetalhamentoArrecadadorConvenio(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request,
			Model model) throws GGASException {

		model.addAttribute(ARRECADADOR_CONVENIO, controladorArrecadadorConvenio.obterArrecadadorConvenio(chavePrimaria));
		return "exibirDetalhamentoArrecadadorConvenio";

	}

	/**
	 * Exibir alteracao arrecadador convenio.
	 * 
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @return exibirAlteracaoArrecadadorConvenio
	 */
	@RequestMapping(TELA_EXIBIR_ALTERACAO_ARRECADADOR_CONVENIO)
	public String exibirAlteracaoArrecadadorConvenio(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			Model model) throws GGASException {
		adicionarparametroIdTipoConvenioDebitoAutomatico(model);
		popularListas(model);
		model.addAttribute(ARRECADADOR_CONVENIO, controladorArrecadadorConvenio.obterArrecadadorConvenio(chavePrimaria));
		return TELA_EXIBIR_ALTERACAO_ARRECADADOR_CONVENIO;
	}

	/**
	 * Alterar arrecadador convenio.
	 * 
	 * @param arrecadadorConvenio
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @param leiaute
	 * @param result
	 * @param habilitado
	 * @return pesquisarArrecadadorConvenio
	 */
	@RequestMapping("alterarArrecadadorConvenio")
	public String alterarArrecadadorConvenio(@RequestParam(value = "leiaute") Long leiaute,				
			@ModelAttribute("ArrecadadorContratoConvenioImpl") ArrecadadorContratoConvenioImpl arrecadadorConvenio, BindingResult result,
		    @RequestParam(value = "habilitado", required = false) String habilitado,
			HttpServletRequest request, Model model) throws GGASException {
		if(leiaute != null){
			arrecadadorConvenio.setLeiaute((DocumentoLayout)
							controladorDocumentoLayout.obter(leiaute));
		}		
		popularListas(model);
		retorno = TELA_EXIBIR_ALTERACAO_ARRECADADOR_CONVENIO;
		Map<String, Object> erros = arrecadadorConvenio.validarDados();
		adicionarparametroIdTipoConvenioDebitoAutomatico(model);
		model.addAttribute(ARRECADADOR_CONVENIO, arrecadadorConvenio);
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorArrecadadorConvenio.alterarArrecadadorContratoConvenio(arrecadadorConvenio);
				model.addAttribute(ARRECADADOR_CONVENIO, arrecadadorConvenio);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, obterMensagem(ARRECADADOR_CONTRATO_CONVENIO));
				retorno = pesquisarArrecadadorConvenio(arrecadadorConvenio, result, String.valueOf(arrecadadorConvenio.isHabilitado()),
						request, model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}
		return retorno;
	}

	private void adicionarparametroIdTipoConvenioDebitoAutomatico(Model model) {
		String valorStr = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO);
		Long idTipoConvenioDebitoAutomatico = Long.valueOf(valorStr);
		model.addAttribute("idTipoConvenioDebitoAutomatico", idTipoConvenioDebitoAutomatico);
	}

	/**
	 * Alterar arrecadador convenio padrao.
	 * 
	 * @param arrecadadorConvenio
	 * @param chavePrimaria
	 * @param request
	 * @param model
	 * @param habilitado - {@link String}
	 * @return pesquisarArrecadadorConvenio
	 */
	@RequestMapping("alterarArrecadadorConvenioPadrao")
	public String alterarArrecadadorConvenioPadrao(@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam(value = "habilitado", required = false) String habilitado, HttpServletRequest request, Model model)
			throws GGASException {
		ArrecadadorContratoConvenioImpl arrecadadorConvenio =
				(ArrecadadorContratoConvenioImpl) controladorArrecadadorConvenio.obterArrecadadorConvenio(chavePrimaria);
		controladorArrecadadorConvenio.tornarPadraoArrecadadorContratoConvenio(chavePrimaria);
		mensagemSucesso(model, Constantes.SUCESSO_ARRECADADOR_CONVENIO_PADRAO);
		return pesquisarArrecadadorConvenio(arrecadadorConvenio, null, String.valueOf(arrecadadorConvenio.isHabilitado()), request, model);
	}

	/**
	 * Remover convenio arrecadador.
	 * 
	 * @param chavesPrimarias
	 * @param request
	 * @param model
	 * @return pesquisarArrecadadorConvenio
	 */
	@RequestMapping("removerConvenioArrecadador")
	public String removerConvenioArrecadador(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			Model model) throws GGASException {

		try {
			controladorArrecadadorConvenio.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, obterMensagem(ARRECADADOR_CONTRATO_CONVENIO));
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return METODO_PESQUISAR_ARRECADADOR_CONVENIO;
	}

	/**
	 * Popular listas.
	 * 
	 * @param model
	 * 
	 */
	private Model popularListas(Model model) throws NegocioException {

		model.addAttribute(LISTA_ARRECADADOR_CONTRATO, controladorArrecadadorConvenio.listarArrecadadorContrato());
		model.addAttribute(LISTA_CONTA_CONVENIO, controladorArrecadadorConvenio.listarContaBancaria());
		model.addAttribute(LISTA_CONTA_CREDITO, controladorArrecadadorConvenio.listarContaBancaria());
		model.addAttribute(LISTA_CARTEIRA_COBRANCA, controladorArrecadadorConvenio.listarCarteiraCobranca());
		model.addAttribute(LISTA_TIPO_CONVENIO,
				controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(ENTIDADE_TIPO_CONVENIO));		
		model.addAttribute(LISTA_LEIAUTES,
						controladorDocumentoLayout.obterDocumentosLayout());

		return model;
	}

	/**
	 * Preparar filtro.
	 * 
	 * @param arrecadadorConvenio
	 * 
	 */
	private Map<String, Object> prepararFiltro(ArrecadadorContratoConvenioImpl arrecadadorConvenio, String habilitado)
			throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (arrecadadorConvenio.getChavePrimaria() > 0) {
			filtro.put(ID_ARRECADADOR_CONVENIO, arrecadadorConvenio.getChavePrimaria());
		}
		if (arrecadadorConvenio.getArrecadadorContrato() != null && arrecadadorConvenio.getArrecadadorContrato().getChavePrimaria() > 0) {
			filtro.put(ID_ARRECADADOR_CONTRATO, arrecadadorConvenio.getArrecadadorContrato().getChavePrimaria());
		}
		if (arrecadadorConvenio.getTipoConvenio() != null) {
			filtro.put(ID_TIPO_CONVENIO, arrecadadorConvenio.getTipoConvenio().getChavePrimaria());
		}
		if (arrecadadorConvenio.getContaConvenio() != null && arrecadadorConvenio.getContaConvenio().getChavePrimaria() > 0) {
			filtro.put(ID_CONTA_CONVENIO, arrecadadorConvenio.getContaConvenio().getChavePrimaria());
		}
		if (arrecadadorConvenio.getContaCredito() != null && arrecadadorConvenio.getContaCredito().getChavePrimaria() > 0) {
			filtro.put(ID_CONTA_CREDITO, arrecadadorConvenio.getContaCredito().getChavePrimaria());
		}
		if (arrecadadorConvenio.getArrecadadorCarteiraCobranca() != null
				&& arrecadadorConvenio.getArrecadadorCarteiraCobranca().getChavePrimaria() > 0) {
			filtro.put(ID_CARTEIRA_COBRANCA, arrecadadorConvenio.getArrecadadorCarteiraCobranca().getChavePrimaria());
		}
		if (!"".equals(arrecadadorConvenio.getCodigoConvenio()) && StringUtils.isNotEmpty(arrecadadorConvenio.getCodigoConvenio())) {
			filtro.put(CODIGO_CONVENIO, arrecadadorConvenio.getCodigoConvenio());
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put("habilitado", Boolean.parseBoolean(habilitado));
		}

		return filtro;
	}
}
