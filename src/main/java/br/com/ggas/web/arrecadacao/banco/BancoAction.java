/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.arrecadacao.banco;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.impl.BancoImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Controller MVC para a entidade Banco
 * 
 * @author Procenge
 */
@Controller
public class BancoAction extends GenericAction {

	private static final String TELA_EXIBIR_PESQUISAR_BANCO = "exibirPesquisarBanco";

	private static final String EXIBIR_INCLUSAO_BANCO = "exibirInclusaoBanco";

	private static final String EXIBIR_ALTERARACAO_BANCO = "exibirAlteracaoBanco";

	private static final String PESQUISAR_BANCO = "pesquisarBanco";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String HABILITADO = "habilitado";

	private static final String NOME_ABREVIADA = "nomeAbreviado";

	private static final String NOME = "nome";

	private static final String BANCO = "banco";

	private static final String CODIGO_BANCO = "codigoBanco";

	private static final String BANCO_LOGO = "logoBanco";

	private String retorno;

	@Autowired
	@Qualifier("controladorBanco")
	private ControladorBanco controladorBanco;

	@Autowired
	@Qualifier("controladorCobranca")
	private ControladorCobranca controladorCobranca;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresa;

	/**
	 * @return Exibir pesquisar Banco - {@link - String}
	 */

	@RequestMapping(TELA_EXIBIR_PESQUISAR_BANCO)
	public String exibirPesquisarBanco() {
		return TELA_EXIBIR_PESQUISAR_BANCO;
	}

	/**
	 * @param model - {@link Model}
	 * @param banco - {@link BancoImpl}
	 * @param result - {@link BindingResult}
	 * @param habilitado - {@link String}
	 * @return String
	 */

	@RequestMapping(PESQUISAR_BANCO)
	public String pesquisarBanco(@ModelAttribute("bancoImpl") BancoImpl banco, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) String habilitado, Model model) {

		model.addAttribute(BANCO, banco);
		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute("listaBanco", controladorBanco.consultarBanco(obterFiltroForm(banco, habilitado)));

		return TELA_EXIBIR_PESQUISAR_BANCO;
	}

	/**
	 * @param model - {@link Model}
	 * @param chavePrimaria - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param logoBanco - {@link MultipartFile}
	 * @return Tela de Detalhamento - {@link String}
	 * @throws NegocioException - {@link NegocioException}
	 */

	@RequestMapping("exibirDetalhamentoBanco")
	public String exibirDetalhamentoBanco(Model model, @RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			@RequestParam(value = "logoBanco", required = false) MultipartFile logoBanco) throws NegocioException {

		model.addAttribute(BANCO, controladorCobranca.obterBanco(chavePrimaria));

		return "exibirDetalhamentoBanco";
	}

	/**
	 * 
	 * @param banco - {@link BancoImpl}
	 * @param habilitado - {@link String}
	 * @return Um map (filtro) - {@link Map}
	 */

	private Map<String, Object> obterFiltroForm(BancoImpl banco, String habilitado) {

		Map<String, Object> filtro = new HashMap<String, Object>();

		if (banco.getChavePrimaria() > 0) {
			Long chavePrimaria = banco.getChavePrimaria();
			filtro.put(CHAVE_PRIMARIA, chavePrimaria);
		}

		if (!banco.getNome().isEmpty() && banco.getNome() != null) {
			filtro.put(NOME, banco.getNome());
		}

		if (habilitado != null && !"null".equals(habilitado)) {
			filtro.put(HABILITADO, Boolean.valueOf(habilitado));
		}

		if (!banco.getNomeAbreviado().isEmpty() && banco.getNomeAbreviado() != null) {
			filtro.put(NOME_ABREVIADA, banco.getNomeAbreviado());
		}

		if (!banco.getCodigoBanco().isEmpty() && banco.getCodigoBanco() != null) {
			filtro.put(CODIGO_BANCO, banco.getCodigoBanco().replaceAll("[^0-9]", ""));
		}

		return filtro;
	}

	/**
	 * Método responsável por exibir tela de inclusão de Banco
	 * @return String
	 */
	@RequestMapping(EXIBIR_INCLUSAO_BANCO)
	public String exibirInclusaoBanco() {
		return EXIBIR_INCLUSAO_BANCO;
	}

	/**
	 * 
	 * @param chavesPrimarias - {@link - Long}
	 * @param request - {@link - HttpServletRequest}
	 * @param model - {@link - Model}
	 * @return exibirPesquisarBanco - {@link - String}
	 * @throws NegocioException
	 */

	@RequestMapping("removerBanco")
	public String removerBanco(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, HttpServletRequest request, Model model)
			throws NegocioException {

		try {
			controladorBanco.remover(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, Banco.BANCOS);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		}

		return "forward:pesquisarBanco";
	}

	/**
	 * 
	 * @param banco - {@link- BancoImpl}
	 * @param result - {@link - BindingResult}
	 * @param habilitado {@link - String}
	 * @param request - {@link - HttpServletRequest}
	 * @param attr - {@link - RedirectAttributes}
	 * @param model - {@link - Model}
	 * @param logoBanco - {@link - MultipartFile}
	 * @return exibirPesquisarBanco
	 * @throws NegocioException
	 * @throws IOException
	 */

	@RequestMapping(value = "incluirBanco", method = RequestMethod.POST)
	public String incluirBanco(@ModelAttribute("BancoImpl") BancoImpl banco, BindingResult result,
			@RequestParam(value = HABILITADO, required = false) String habilitado, HttpServletRequest request, RedirectAttributes attr,
			Model model, @RequestParam(value = "logoBanco", required = false) MultipartFile logoBanco) throws NegocioException, IOException {

		retorno = EXIBIR_INCLUSAO_BANCO;

		model.addAttribute(HABILITADO, habilitado);
		model.addAttribute(BANCO, banco);

		Map<String, Object> erros = banco.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				popularLogo(banco, logoBanco);
				banco.setDadosAuditoria(getDadosAuditoria(request));
				controladorBanco.validarExistente(obterFiltroForm(banco, habilitado));
				if (Boolean.valueOf(habilitado)) {
					controladorBanco.inserir(banco);
				} else {
					controladorBanco.inserirInativo(banco);
				}
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Banco.BANCO_CAMPO_STRING);
				retorno = pesquisarBanco(banco, result, String.valueOf(banco.isHabilitado()), model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}

		}

		return retorno;
	}

	/**
	 * 
	 * @param chavePrimaria - {@link - Long}
	 * @param removaLogo - {@link - Boolean}
	 * @param model - {@link - Model}
	 * @return exibirTelaAlteracaoBanco
	 * @throws NegocioException
	 */

	@RequestMapping(EXIBIR_ALTERARACAO_BANCO)
	public String exibirAlterarBanco(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			@RequestParam(value = "removaLogo", required = false) Boolean removaLogo, Model model) throws NegocioException {

		BancoImpl banco = (BancoImpl) controladorBanco.obter(chavePrimaria);

		if ((removaLogo != null) && removaLogo) {
			banco.setLogoBanco(null);
		}
		model.addAttribute(BANCO, banco);

		return EXIBIR_ALTERARACAO_BANCO;
	}

	/**
	 * 
	 * @param banco - {@link - BancoImpl}
	 * @param result - {@link - BindingResult}
	 * @param model - {@link - Model}
	 * @param habilitado {@link - String}
	 * @param resquest {@link - HttpServletRequest}
	 * @param logoBanco {@link - MultipartFile}
	 * @return exibirPesquisarBanco - {@link - String}
	 * @throws NegocioException - {@link - NegocioException}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws IOException - {@link - IOException}
	 */

	@RequestMapping("alterarBanco")
	public String alterarBanco(@ModelAttribute("BancoImpl") BancoImpl banco, BindingResult result, Model model, String habilitado,
			HttpServletRequest resquest, @RequestParam(value = BANCO_LOGO, required = false) MultipartFile logoBanco)
			throws NegocioException, ConcorrenciaException, IOException {

		if (logoBanco != null && logoBanco.getBytes().length > 0) {
			banco.setLogoBanco(logoBanco.getBytes());
		} else if (banco.getLogoBanco() == null) {
			banco.setLogoBanco(null);
		} else {
			BancoImpl bancoUtilizado = (BancoImpl) controladorBanco.obter(banco.getChavePrimaria());
			banco.setLogoBanco(bancoUtilizado.getLogoBanco());
		}

		Map<String, Object> erros = banco.validarDados();

		model.addAttribute(BANCO, banco);

		retorno = EXIBIR_ALTERARACAO_BANCO;
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			try {
				controladorBanco.validarExistente(obterFiltroForm(banco, String.valueOf(banco.isHabilitado())));
				popularLogo(banco, logoBanco);
				controladorBanco.atualizar(banco);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Banco.BANCO_CAMPO_STRING);
				retorno = pesquisarBanco(banco, result, String.valueOf(banco.isHabilitado()), model);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, e);
			}
		}

		return retorno;
	}
	
	/**
	 * 
	 * @param banco - {@link - BancoImpl}
	 * @param logoBanco - {@link - MultipartFile}
	 * @throws NegocioException - {@link - NegocioException}
	 * @throws IOException - {@link - IOException}
	 */
	
	
	public void popularLogo(BancoImpl banco, MultipartFile logoBanco) throws NegocioException, IOException {

		if (controladorEmpresa.validarImagemDoArquivo(logoBanco, banco.getExtensoesArquivoLogoEmpresa(),
				Banco.TAMANHO_MAXIMO_ARQUIVO_LOGO_EMPRESA, Banco.ERRO_NEGOCIO_TIPO_ARQUIVO, Banco.ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO,
				Boolean.FALSE)) {
			banco.setLogoBanco(logoBanco.getBytes());
			if (banco.getLogoBanco() == null || !Arrays.toString(banco.getLogoBanco()).isEmpty()) {
				banco.setLogoBanco(logoBanco.getBytes());
			}
		}

	}

	/**
	 * 
	 * @param chavePrimaria - {@link - Long}
	 * @param request - {@link - HttpServletRequest}
	 * @param response - {@link - HttpServletResponse}
	 * @throws IOException - {@link - IOException}
	 * @throws NegocioException - {@link - NegocioException}
	 */

	@RequestMapping(value = "exibirLogoMarcaBanco/{chavePrimaria}")
	public void exibirLogoMarcaBanco(@PathVariable(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response) throws IOException, NegocioException {

		BancoImpl banco = (BancoImpl) controladorCobranca.obterBanco(chavePrimaria);

		byte[] logo = banco.getLogoBanco();
		if (logo != null) {
			try {
				ByteArrayInputStream in = new ByteArrayInputStream(logo);

				int index = 0;

				byte[] bytearray = new byte[in.available()];

				response.reset();
				response.setContentType("image/jpeg");

				while ((index = in.read(bytearray)) != -1) {
					response.getOutputStream().write(bytearray, 0, index);
				}
				response.flushBuffer();
				in.close();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			} finally {
				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
		}
	}
}
