package br.com.ggas.web.arrecadacao.devolucao;
/**
 * Classe responsável pela representação de valores de uma Devolução.
 * @author esantana
 *
 */
public class DevolucaoVO {

	private Long chavePrimaria;

	private String habilitado = "true";

	private Long idPontoConsumo;

	private Long idTipoDevolucao;

	private String dataDevolucaoInicial = "";

	private String dataDevolucaoFinal = "";

	private Integer versao;

	private String nomeCompletoCliente;

	private String descricaoPontoConsumo;

	private String tipoDevolucaoDetalhamento;

	private String observacaoDevolucao;

	private String valorDevolucao;

	private String dataDevolucao;

	private String enderecoPontoConsumo;

	private String cepPontoConsumo;

	private String complementoPontoConsumo;

	private Long[] chavesCreditoDebito;

	private Long[] chavesRecebimento;

	private Long[] chavesFatura;

	public Long[] getChavesCreditoDebito() {
		Long[] retorno = null;
		if(this.chavesCreditoDebito != null) {
			retorno = this.chavesCreditoDebito.clone();
		}
		return retorno;
	}

	public void setChavesCreditoDebito(Long[] chavesCreditoDebito) {
		if(chavesCreditoDebito != null) {
			this.chavesCreditoDebito = chavesCreditoDebito.clone();
		} else {
			this.chavesCreditoDebito = null;
		}
	}

	public Long[] getChavesRecebimento() {
		Long[] retorno = null;
		if(this.chavesRecebimento != null) {
			retorno = this.chavesRecebimento.clone();
		}
		return retorno;
	}

	public void setChavesRecebimento(Long[] chavesRecebimento) {
		if(chavesRecebimento != null) {
			this.chavesRecebimento = chavesRecebimento.clone();
		} else {
			this.chavesRecebimento = null;
		}
	}

	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public String getNomeCompletoCliente() {
		return nomeCompletoCliente;
	}

	public void setNomeCompletoCliente(String nomeCompletoCliente) {
		this.nomeCompletoCliente = nomeCompletoCliente;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getTipoDevolucaoDetalhamento() {
		return tipoDevolucaoDetalhamento;
	}

	public void setTipoDevolucaoDetalhamento(String tipoDevolucaoDetalhamento) {
		this.tipoDevolucaoDetalhamento = tipoDevolucaoDetalhamento;
	}

	public String getObservacaoDevolucao() {
		return observacaoDevolucao;
	}

	public void setObservacaoDevolucao(String observacaoDevolucao) {
		this.observacaoDevolucao = observacaoDevolucao;
	}

	public String getValorDevolucao() {
		return valorDevolucao;
	}

	public void setValorDevolucao(String valorDevolucao) {
		this.valorDevolucao = valorDevolucao;
	}

	public String getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}
	
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public Long getIdTipoDevolucao() {
		return idTipoDevolucao;
	}

	public void setIdTipoDevolucao(Long idTipoDevolucao) {
		this.idTipoDevolucao = idTipoDevolucao;
	}

	public String getDataDevolucaoInicial() {
		return dataDevolucaoInicial;
	}

	public void setDataDevolucaoInicial(String dataDevolucaoInicial) {
		this.dataDevolucaoInicial = dataDevolucaoInicial;
	}

	public String getDataDevolucaoFinal() {
		return dataDevolucaoFinal;
	}

	public void setDataDevolucaoFinal(String dataDevolucaoFinal) {
		this.dataDevolucaoFinal = dataDevolucaoFinal;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public String getEnderecoPontoConsumo() {
		return enderecoPontoConsumo;
	}

	public void setEnderecoPontoConsumo(String enderecoPontoConsumo) {
		this.enderecoPontoConsumo = enderecoPontoConsumo;
	}

	public String getCepPontoConsumo() {
		return cepPontoConsumo;
	}

	public void setCepPontoConsumo(String cepPontoConsumo) {
		this.cepPontoConsumo = cepPontoConsumo;
	}

	public String getComplementoPontoConsumo() {
		return complementoPontoConsumo;
	}

	public void setComplementoPontoConsumo(String complementoPontoConsumo) {
		this.complementoPontoConsumo = complementoPontoConsumo;
	}

	public Long[] getChavesFatura() {
		Long[] retorno = null;
		if(this.chavesFatura != null) {
			retorno = this.chavesFatura.clone();
		}
		return retorno;
	}

	public void setChavesFatura(Long[] chavesFatura) {
		if(chavesFatura != null) {
			this.chavesFatura = chavesFatura.clone();
		} else {
			this.chavesFatura = null;
		}
	}
}
