/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.arrecadacao.recebimento;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
/**
 * Classe responsável pela transferência de dados
 * relacionados a entidade Recebimento
 * 
 * @see RecebimentoAction
 */
public class RecebimentoVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1331773090381517053L;

	private Recebimento recebimento;
	
	private String anoMesContabilInicial;
	private String anoMesContabilFinal;
	private String dataRecebimentoInicial;
	private String dataRecebimentoFinal;
	private String dataDocumentoInicial;
	private String dataDocumentoFinal;
	private String fluxoBaixaPorDacao;
	private String dataRecebimentoInclusao;
	private String valorRecebimento;
	private String observacao;
	private String indicadorPesquisa;

	private Long chavePrimaria;
	private Long idArrecadador;
	private Long idRecebimentoSituacao;
	private Long idTipoDocumento;
	private Long idCliente;
	private Long idPontoConsumo;
	private Long idImovel;
	private Long idFormaArrecadacao;
	
	private BigDecimal saldo;

	/**
	 * @return recebimento {@link Recebimento}
	 */
	public Recebimento getRecebimento() {

		return recebimento;
	}

	/**
	 * @param recebimento {@link Recebimento}
	 */
	public void setRecebimento(Recebimento recebimento) {

		this.recebimento = recebimento;
	}

	/**
	 * @return saldo {@link BigDecimal}
	 */
	public BigDecimal getSaldo() {

		return saldo;
	}

	/**
	 * @param saldo {@link BigDecimal}
	 */
	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}
	
	/**
	 * @return anoMesContabilInicial {@link String}
	 */
	public String getAnoMesContabilInicial() {
		return anoMesContabilInicial;
	}

	/**
	 * @param anoMesContabilInicial {@link String}
	 */
	public void setAnoMesContabilInicial(String anoMesContabilInicial) {
		this.anoMesContabilInicial = anoMesContabilInicial;
	}

	/**
	 * @return anoMesContabilFinal {@link String}
	 */
	public String getAnoMesContabilFinal() {
		return anoMesContabilFinal;
	}

	/**
	 * @param anoMesContabilFinal {@link String}
	 */
	public void setAnoMesContabilFinal(String anoMesContabilFinal) {
		this.anoMesContabilFinal = anoMesContabilFinal;
	}

	/**
	 * @return dataRecebimentoInicial {@link String}
	 */
	public String getDataRecebimentoInicial() {
		return dataRecebimentoInicial;
	}

	/**
	 * @param dataRecebimentoInicial {@link String}
	 */
	public void setDataRecebimentoInicial(String dataRecebimentoInicial) {
		this.dataRecebimentoInicial = dataRecebimentoInicial;
	}

	/**
	 * @return dataRecebimentoFinal {@link String}
	 */
	public String getDataRecebimentoFinal() {
		return dataRecebimentoFinal;
	}

	/**
	 * @param dataRecebimentoFinal {@link String}
	 */
	public void setDataRecebimentoFinal(String dataRecebimentoFinal) {
		this.dataRecebimentoFinal = dataRecebimentoFinal;
	}

	/**
	 * @return dataDocumentoInicial {@link String}
	 */
	public String getDataDocumentoInicial() {
		return dataDocumentoInicial;
	}

	/**
	 * @param dataDocumentoInicial {@link String}
	 */
	public void setDataDocumentoInicial(String dataDocumentoInicial) {
		this.dataDocumentoInicial = dataDocumentoInicial;
	}

	/**
	 * @return dataDocumentoFinal {@link String}
	 */
	public String getDataDocumentoFinal() {
		return dataDocumentoFinal;
	}

	/**
	 * @param dataDocumentoFinal {@link String}
	 */
	public void setDataDocumentoFinal(String dataDocumentoFinal) {
		this.dataDocumentoFinal = dataDocumentoFinal;
	}
	
	/**
	 * @return fluxoBaixaPorDacao {@link String}
	 */
	public String getFluxoBaixaPorDacao() {
		return fluxoBaixaPorDacao;
	}

	/**
	 * @param fluxoBaixaPorDacao {@link String}
	 */
	public void setFluxoBaixaPorDacao(String fluxoBaixaPorDacao) {
		this.fluxoBaixaPorDacao = fluxoBaixaPorDacao;
	}
	
	/**
	 * @return dataRecebimentoInclusao {@link String}
	 */
	public String getDataRecebimentoInclusao() {
		return dataRecebimentoInclusao;
	}

	/**
	 * @param dataRecebimentoInclusao {@link String}
	 */
	public void setDataRecebimentoInclusao(String dataRecebimentoInclusao) {
		this.dataRecebimentoInclusao = dataRecebimentoInclusao;
	}
	
	/**
	 * @return valorRecebimento {@link String}
	 */
	public String getValorRecebimento() {
		return valorRecebimento;
	}

	/**
	 * @param valorRecebimento {@link String}
	 */
	public void setValorRecebimento(String valorRecebimento) {
		this.valorRecebimento = valorRecebimento;
	}
	
	/**
	 * @return observacao {@link String}
	 */
	public String getObservacao() {
		return observacao;
	}

	/**
	 * @param observacao {@link String}
	 */
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	/**
	 * @return indicadorPesquisa {@link String}
	 */
	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}

	/**
	 * @param indicadorPesquisa {@link String}
	 */
	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}
	
	/**
	 * @return chavePrimaria {@link Long}
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria {@link Long}
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return idArrecadador {@link Long}
	 */
	public Long getIdArrecadador() {
		return idArrecadador;
	}

	/**
	 * @param idArrecadador {@link Long}
	 */
	public void setIdArrecadador(Long idArrecadador) {
		this.idArrecadador = idArrecadador;
	}

	/**
	 * @return idRecebimentoSituacao {@link Long}
	 */
	public Long getIdRecebimentoSituacao() {
		return idRecebimentoSituacao;
	}

	/**
	 * @param idRecebimentoSituacao {@link Long}
	 */
	public void setIdRecebimentoSituacao(Long idRecebimentoSituacao) {
		this.idRecebimentoSituacao = idRecebimentoSituacao;
	}

	/**
	 * @return idTipoDocumento {@link Long}
	 */
	public Long getIdTipoDocumento() {
		return idTipoDocumento;
	}

	/**
	 * @param idTipoDocumento {@link Long}
	 */
	public void setIdTipoDocumento(Long idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	/**
	 * @return idCliente {@link Long}
	 */
	public Long getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente {@link Long}
	 */
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return idPontoConsumo {@link Long}
	 */
	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	/**
	 * @param idPontoConsumo {@link Long}
	 */
	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}
	
	/**
	 * @return idImovel {@link Long}
	 */
	public Long getIdImovel() {
		return idImovel;
	}

	/**
	 * @param idImovel {@link Long}
	 */
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	
	/**
	 * @return idFormaArrecadacao {@link Long}
	 */
	public Long getIdFormaArrecadacao() {
		return idFormaArrecadacao;
	}

	/**
	 * @param idFormaArrecadacao {@link Long}
	 */
	public void setIdFormaArrecadacao(Long idFormaArrecadacao) {
		this.idFormaArrecadacao = idFormaArrecadacao;
	}
}
