/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 14:48:20
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte;

import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados com a emissão de aviso de corte.
 *
 */
public interface AvisoCorteEmissao extends EntidadeNegocio {

	String BEAN_ID_AVISO_CORTE_EMISSAO = "avisoCorteEmissao";

	/**
	 * @return PontoConsumo - Retorna objeto ponto consumo.
	 */
	public PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo - Set Ponto consumo.
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return Date - Retorna data do processamento.
	 */
	public Date getDataProcessamento();

	/**
	 * @param dataProcessamento - Set Data Processamento.
	 */
	public void setDataProcessamento(Date dataProcessamento);

	/**
	 * @return Boolean - Retorna indicador emissão.
	 */
	public Boolean getIndicadorEmissao();

	/**
	 * @param indicadorEmissao - Set indicador emissão.
	 */
	public void setIndicadorEmissao(Boolean indicadorEmissao);

	/**
	 * @return Fatura - Retorna objeto fatura.
	 */
	public Fatura getFatura();

	/**
	 * @param fatura - Set Fatura.
	 */
	public void setFatura(Fatura fatura);

}
