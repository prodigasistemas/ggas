/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 14:48:20
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte;

import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Aviso de corte 
 *
 */
public interface AvisoCorte extends EntidadeNegocio {

	String BEAN_ID_AVISO_CORTE = "avisoCorte";

	/**
	 * @return PontoConsumo - Retorna objeto ponto consumo.
	 */
	public PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo - Set objeto ponto consumo.
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return Cliente - Retorna o objeto cliente.
	 */
	public Cliente getCliente();

	/**
	 * @param cliente - Set objeto cliente.
	 */
	public void setCliente(Cliente cliente);

	/**
	 * @return Fatura - Retorna o objeto fatura.
	 */
	public Fatura getFatura();

	/**
	 * @param fatura - Set objeto fatura.
	 */
	public void setFatura(Fatura fatura);

	/**
	 * @return Date - Retorna data de aviso
	 */
	public Date getDataAviso();

	/**
	 * @param dataAviso - Set data de aviso.
	 */
	public void setDataAviso(Date dataAviso);

	/**
	 * @return Date - Retorna data de notificação
	 */
	public Date getDataNotificacao();

	/**
	 * @param dataNotificacao - Set data de notificação.
	 */
	public void setDataNotificacao(Date dataNotificacao);

	/**
	 * @return Date - Retrona data de corte.
	 */
	public Date getDataCorte();

	/**
	 * @param dataCorte - Set data de corte.
	 */
	public void setDataCorte(Date dataCorte);

	/**
	 * @return Integer - Retorna quantidade de dias de atraso.
	 */
	public Integer getDiasAtraso();

	/**
	 * @param diasAtraso - Set dias de atraso.
	 */
	void setDiasAtraso(Integer diasAtraso);
	
	/**
	 * @return Boolean - Retorna se a proxima fatura vai ser notificada.
	 */
	public Boolean getIndicadorFaturaNotificada();

	/**
	 * @param diasAtraso - Set indicador se a proxima fatura vai ser notificada.
	 */
	void setIndicadorFaturaNotificada(Boolean indicadorFaturaNotificada);
	
	/**
	 * 
	 * @return Numero de Checagens corte
	 */
	public Integer getNumeroChecagens();
	
	/**
	 * 
	 * @param numeroChecagens - Set numero de checagens corte
	 */
	public void setNumeroChecagens(Integer numeroChecagens);
	
	/**
	 * 
	 * @return produto - Indica valor para priorizacao corte
	 */
	Integer getProdutoCorte();

	/**
	 * 
	 * @param produtoCorte - {@link Integer}
	 */
	void setProdutoCorte(Integer produtoCorte);
	
	Integer getOrdemRoteirizar();
	
	void setOrdemRoteirizar(Integer ordemRoteirizar);

	/**
	 * 
	 * @return criticidade
	 */
	Integer getCriticidade();
	
	/**
	 * 
	 * @param criticidade
	 */
	void setCriticidade(Integer criticidade);

	/**
	 * 
	 * @param distanciaEntrePontos - {@link Double}
	 */
	void setDistanciaEntrePontos(Double distanciaEntrePontos);

	/**
	 * 
	 * @return distancia
	 */
	Double getDistanciaEntrePontos();

	/**
	 * 
	 * @return numeroTitulo - Retorna o numero de titulo
	 */
	Long getNumeroTitulo();

	/**
	 * 
	 * @param numeroTitulo - Set numero de titulo
	 */
	void setNumeroTitulo(Long numeroTitulo);
	
	/**
	 * 
	 * @param indicadorFaturaPriorizada - Set indicador se a fatura foi priorizada
	 */
	Boolean getIndicadorFaturaPriorizada();

	/**
	 * 
	 * @param indicadorFaturaPriorizada - Set indicador se a fatura foi priorizada
	 */
	void setIndicadorFaturaPriorizada(Boolean indicadorFaturaPriorizada);
	
}
