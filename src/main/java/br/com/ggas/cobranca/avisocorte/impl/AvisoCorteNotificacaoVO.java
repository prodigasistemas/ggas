package br.com.ggas.cobranca.avisocorte.impl;

/**
 * Classe para Aviso e Notificacao de Corte VO
 *
 */
public class AvisoCorteNotificacaoVO {

	private Integer diasAtraso;
	private Long chavePrimaria;
	private String tipo;
	private Long idCliente;
	private Long idFatura;
	private Long idsSegmento;
	private Long idImovel;
	private Boolean habilitado;
	private Boolean habilitar;
	private Long idComando;
	private Integer numeroPontos = 1;
	private Long idGrupoFaturamento;
	
	public Integer getDiasAtraso() {
		return diasAtraso;
	}
	public void setDiasAtraso(Integer diasAtraso) {
		this.diasAtraso = diasAtraso;
	}
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Long getIdFatura() {
		return idFatura;
	}
	public void setIdFatura(Long idFatura) {
		this.idFatura = idFatura;
	}
	public Long getIdsSegmento() {
		return idsSegmento;
	}
	public void setIdsSegmento(Long idsSegmento) {
		this.idsSegmento = idsSegmento;
	}
	public Long getIdImovel() {
		return idImovel;
	}
	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	public Boolean getHabilitar() {
		return habilitar;
	}
	public void setHabilitar(Boolean habilitar) {
		this.habilitar = habilitar;
	}
	public Long getIdComando() {
		return idComando;
	}
	public void setIdComando(Long idComando) {
		this.idComando = idComando;
	}
	public Integer getNumeroPontos() {
		return numeroPontos;
	}
	public void setNumeroPontos(Integer numeroPontos) {
		this.numeroPontos = numeroPontos;
	}
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
}
