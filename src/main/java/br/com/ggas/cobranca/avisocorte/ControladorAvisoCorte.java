/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 15:04:20
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVOCobranca;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 *  Interface responsável pela assinatura de métodos relacionados ao controlador de Aviso de Corte.
 *
 */
public interface ControladorAvisoCorte extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_AVISO_CORTE = "controladorAvisoCorte";

	/**
	 * Listar.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<AvisoCorte> listar(Map<String, Object> filtro);

	/**
	 * Emitir2 via notificacao corte.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] emitir2ViaNotificacaoCorte(Long chavePrimaria) throws IOException, GGASException;

	/**
	 * Emitir2 via aviso corte.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] emitir2ViaAvisoCorte(Long chavePrimaria) throws IOException, GGASException;

	/**
	 * Criar lista servico autorizacao vo cobranca.
	 * 
	 * @param avisosCorte - {@link Collection}
	 * @param idPontoConsumo - {@link Long}
	 * @return listaServicoVO - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<ServicoAutorizacaoVOCobranca> criarListaServicoAutorizacaoVOCobranca(Collection<AvisoCorte> avisosCorte, Long idPontoConsumo)
			throws NegocioException;

	/**
	 * Consultar aviso corte acompanhamento cobranca.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	public Collection<AvisoCorte> consultarAvisoCorteAcompanhamentoCobranca(Map<String, Object> filtro);

	
	/**
	 * Consultar Avisos Cortes
	 * @param chavesAvisoCorte - {@link Long}
	 * @return Coleção Aviso Corte - {@link AvisoCorte}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<AvisoCorte> consultarAvisosCorte(Long[] chavesAvisoCorte) throws NegocioException;

	/**
	 * Consulta lista aviso de corte a serem priorizadas
	 * @return Coleção aviso corte - {@link AvisoCorte}
	 * @throws GGASException - {@link GGASException}
	 */
	Collection<AvisoCorte> obterListaAvisoCortePriorizar() throws GGASException;

	/**
	 * Verifica Obrigatoriedade do Protocolo
	 * @return boolean - {@link Boolean}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Boolean verificarObrigatoriedadeProtocolo() throws NegocioException;

	/**
	 * Remover Aviso de Corte da lista de Priorizaao
	 * @param idsAviso - {@link Long}
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @return Colecao AvisoCorte - {@link AvisoCorte}
	 */
	Collection<AvisoCorte> removerAvisosDeCorte(Long[] idsAviso, Collection<AvisoCorte> listaAvisoCorte);

	/**
	 * Consulta Avisos de Corte Priorizado
	 * @return lista aviso de corte priorizado - {@link AvisoCorte}
	 * @throws NegocioException the negocio exception
	 */
	Collection<AvisoCorte> consultarAvisosCortePriorizados() throws NegocioException;

	/**
	 * Seleciona numero de avisos de corte
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @param numeroAviso - {@link Integer}
	 * @return lista aviso corte - {@link AvisoCorte}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<AvisoCorte> selecionarNumeroAvisoCorte(Collection<AvisoCorte> listaAvisoCorte, Integer numeroAviso)
			throws NegocioException;

	/**
	 * Salva lista de corte priorizada
	 * @param idsAviso - {@link Long}
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	void salvarListaPriorizada(Long[] idsAviso, Collection<AvisoCorte> listaAvisoCorte) throws ConcorrenciaException, NegocioException;

}
