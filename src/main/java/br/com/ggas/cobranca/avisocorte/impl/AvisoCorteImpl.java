/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 14:50:06
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela implementação da classe Aviso de Corte 
 *
 */
public class AvisoCorteImpl extends EntidadeNegocioImpl implements AvisoCorte {

	/**
	 * 
	 */
	private static final long serialVersionUID = -425932292174043100L;

	private PontoConsumo pontoConsumo;

	private Cliente cliente;

	private Fatura fatura;

	private Date dataAviso;

	private Date dataNotificacao;

	private Date dataCorte;

	private Integer diasAtraso;

	private Boolean indicadorFaturaNotificada;
	
	private Long numeroTitulo;
	
	private Integer produtoCorte = 0;

	private Integer numeroChecagens;

	private Integer ordemRoteirizar;

	private Integer criticidade;

	private Double distanciaEntrePontos;
	
	private Boolean indicadorFaturaPriorizada;

	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	@Override
	public Cliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public Date getDataAviso() {
		Date data = null;
		if (this.dataAviso != null) {
			data = (Date) dataAviso.clone();
		}
		return data;
	}

	@Override
	public void setDataAviso(Date dataAviso) {
		if(dataAviso != null) {
			this.dataAviso = (Date) dataAviso.clone();
		} else {
			this.dataAviso = null;
		}
	}

	@Override
	public Date getDataNotificacao() {
		Date data = null;
		if (this.dataNotificacao != null) {
			data = (Date) dataNotificacao.clone();
		}
		return data;
	}

	@Override
	public void setDataNotificacao(Date dataNotificacao) {
		if(dataNotificacao != null) {
			this.dataNotificacao = (Date) dataNotificacao.clone();
		} else {
			this.dataNotificacao = null;
		}
	}

	@Override
	public Date getDataCorte() {
		Date data = null;
		if (this.dataCorte != null) {
			data = (Date) dataCorte.clone();
		}
		return data;
	}

	@Override
	public void setDataCorte(Date dataCorte) {
		if(dataCorte != null) {
			this.dataCorte = (Date) dataCorte.clone();
		} else {
			this.dataCorte = null;
		}
		
	}

	@Override
	public Fatura getFatura() {

		return fatura;
	}

	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public Integer getDiasAtraso() {

		return diasAtraso;
	}

	@Override
	public void setDiasAtraso(Integer diasAtraso) {

		this.diasAtraso = diasAtraso;

	}

	@Override
	public Boolean getIndicadorFaturaNotificada() {
		return indicadorFaturaNotificada;
	}

	@Override
	public void setIndicadorFaturaNotificada(Boolean indicadorFaturaNotificada) {
		this.indicadorFaturaNotificada = indicadorFaturaNotificada;
		
	}

	@Override
	public Integer getNumeroChecagens() {
		return this.numeroChecagens;
	}

	@Override
	public void setNumeroChecagens(Integer numeroChecagens) {
		this.numeroChecagens = numeroChecagens;	
	}
	
	@Override
	public Long getNumeroTitulo() {
		return numeroTitulo;
	}

	@Override
	public void setNumeroTitulo(Long numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}

	@Override
	public Integer getProdutoCorte() {
		return produtoCorte;
	}

	@Override
	public void setProdutoCorte(Integer produtoCorte) {
		this.produtoCorte = produtoCorte;
	}

	@Override
	public Integer getOrdemRoteirizar() {
		return this.ordemRoteirizar;
	}

	@Override
	public void setOrdemRoteirizar(Integer ordemRoteirizar) {
		this.ordemRoteirizar = ordemRoteirizar;	
	}
	
	@Override
	public Integer getCriticidade() {
		return criticidade;
	}

	@Override
	public void setCriticidade(Integer criticidade) {
		this.criticidade = criticidade;
		
	}
	
	@Override
	public void setDistanciaEntrePontos(Double distanciaEntrePontos) {
		this.distanciaEntrePontos = distanciaEntrePontos;
	}

	@Override
	public Double getDistanciaEntrePontos() {
		return distanciaEntrePontos;
	}

	@Override
	public Boolean getIndicadorFaturaPriorizada() {
		return indicadorFaturaPriorizada;
	}

	@Override
	public void setIndicadorFaturaPriorizada(Boolean indicadorFaturaPriorizada) {
		this.indicadorFaturaPriorizada = indicadorFaturaPriorizada;
	}

}
