/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 14:50:06
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.avisocorte.AvisoCorteEmissao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela implementação dos métodos relacionados a emissão do aviso de corte.
 *
 */
public class AvisoCorteEmissaoImpl extends EntidadeNegocioImpl implements AvisoCorteEmissao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -425932292174043100L;

	private Fatura fatura;

	private PontoConsumo pontoConsumo;

	private Date dataProcessamento;

	private Boolean indicadorEmissao;

	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	@Override
	public Date getDataProcessamento() {
		Date data = null;
		if (this.dataProcessamento != null) {
			data = (Date) dataProcessamento.clone();
		}
		return data;
	}

	@Override
	public void setDataProcessamento(Date dataProcessamento) {
		if (dataProcessamento != null) {
			this.dataProcessamento = (Date) dataProcessamento.clone();
		} else {
			this.dataProcessamento = null;
		}
	}

	@Override
	public Boolean getIndicadorEmissao() {

		return indicadorEmissao;
	}

	@Override
	public void setIndicadorEmissao(Boolean indicadorEmissao) {

		this.indicadorEmissao = indicadorEmissao;
	}

	@Override
	public Fatura getFatura() {

		return fatura;
	}

	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
