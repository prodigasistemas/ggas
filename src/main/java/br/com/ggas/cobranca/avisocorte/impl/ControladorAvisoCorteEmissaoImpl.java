/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 15:04:52
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cobranca.avisocorte.AvisoCorteEmissao;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao;
import br.com.ggas.cobranca.dominio.DadosCicloCorte;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pela implementação dos métodos de consulta do comtrolador de emissão de aviso de corte. 
 *
 */
public class ControladorAvisoCorteEmissaoImpl extends ControladorNegocioImpl implements ControladorAvisoCorteEmissao {

	private static final String CAMPO_INDICADOR_EMISSAO = "indicadorEmissao";
	private static final String CAMPO_DATA_PROCESSAMENTO = "dataProcessamento";
	
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AvisoCorteEmissao.BEAN_ID_AVISO_CORTE_EMISSAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(AvisoCorteEmissao.BEAN_ID_AVISO_CORTE_EMISSAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao#consultar(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AvisoCorteEmissao> consultar(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long idFatura = (Long) filtro.get("idFatura");
			Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
			Boolean indicadorEmissao = (Boolean) filtro.get(CAMPO_INDICADOR_EMISSAO);
			String dataProcessamento = (String) filtro.get(CAMPO_DATA_PROCESSAMENTO);

			if(idFatura != null) {
				criteria.add(Restrictions.eq("fatura.chavePrimaria", idFatura));
			}

			if(idPontoConsumo != null) {
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
			}

			if(indicadorEmissao != null) {
				criteria.add(Restrictions.eq(CAMPO_INDICADOR_EMISSAO, indicadorEmissao));
			}

			if(dataProcessamento == null) {
				criteria.add(Restrictions.isNull(CAMPO_DATA_PROCESSAMENTO));
			}

		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao#consultarFaturasVencidasPontoConsumoNotificacaoCorte(java.util.Map,
	 * br.com.ggas.batch.acaocomando.dominio.AcaoComando, boolean, boolean)
	 */
	private Collection<Fatura> consultarFaturasVencidasPontoConsumoNotificacaoCorte(Map<String, Object> filtro, AcaoComando acaoComando) 
			throws NegocioException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		filtro.put("contratoAtivo", true);
		
		return controladorFatura.consultarFaturasVencidasPontoConsumo(filtro, acaoComando);

	}
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao#consultarFaturasVencidasPontoConsumoNotificacaoCorte(java.util.Map,
	 * br.com.ggas.batch.acaocomando.dominio.AcaoComando, boolean, boolean)
	 */
	@Override
	public Collection<Fatura> consultarFaturasValidasAvisoCorte(Map<String, Object> filtro, AcaoComando acaoComando) 
			throws NegocioException {
		String habilitadoString = (String) filtro.get(Constantes.HABILITADO);
		Boolean habilitado = null;
		if(!StringUtils.isEmpty(habilitadoString)) {
			habilitado = Boolean.parseBoolean(habilitadoString);
		}
		
		Collection<Fatura> faturasVencidas = consultarFaturasVencidasPontoConsumoNotificacaoCorte(filtro, acaoComando);
		desabilitarCobrancaFaturasComEmissaoAvisoCorte(faturasVencidas);
		Collection<Fatura> faturasInvalidasCobranca = 
				DadosCicloCorte.obterFaturasInvalidasEmissaoNotificacaoCorte(faturasVencidas, habilitado, acaoComando);
		faturasVencidas.removeAll(faturasInvalidasCobranca);
		
		Collection<Fatura> faturasComServicosAutorizacao = validarSeTemServicoAutorizacao(faturasVencidas);
		if(!faturasComServicosAutorizacao.isEmpty()) {
			faturasVencidas.removeAll(faturasComServicosAutorizacao);
		}

		return faturasVencidas;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao#consultarFaturasVencidasPontoConsumoNotificacaoCorte(java.util.Map,
	 * br.com.ggas.batch.acaocomando.dominio.AcaoComando, boolean, boolean)
	 */
	@Override
	public Collection<Fatura> consultarFaturasValidasNotificacaoCorte(Map<String, Object> filtro, AcaoComando acaoComando) 
			throws NegocioException {
		String habilitadoString = (String) filtro.get(Constantes.HABILITADO);
		Boolean habilitado = null;
		if(!StringUtils.isEmpty(habilitadoString)) {
			habilitado = Boolean.parseBoolean(habilitadoString);
		}
		
		Collection<Fatura> faturasVencidas = consultarFaturasVencidasPontoConsumoNotificacaoCorte(filtro, acaoComando);
		desabilitarCobrancaFaturasComEmissaoAvisoCorte(faturasVencidas);
		Collection<Fatura> faturasInvalidasCobranca = 
				DadosCicloCorte.obterFaturasInvalidasEmissaoNotificacaoCorte(faturasVencidas, habilitado, acaoComando);
		faturasVencidas.removeAll(faturasInvalidasCobranca);
		return faturasVencidas;
	}
	
	private void desabilitarCobrancaFaturasComEmissaoAvisoCorte(Collection<Fatura> faturas ) {
		
		Map<String, Object> filtroAvisoCorteEmissao = new HashMap<>();
		filtroAvisoCorteEmissao.put(CAMPO_INDICADOR_EMISSAO, Boolean.FALSE);
		Collection<AvisoCorteEmissao> listaAvisoCorteEmissao = this.consultar(filtroAvisoCorteEmissao);
		Map<Long, AvisoCorteEmissao> mapaAvisoCorteEmissao = new HashMap<>();
		for (AvisoCorteEmissao avisoCorteEmissao : listaAvisoCorteEmissao) {
			mapaAvisoCorteEmissao.put(avisoCorteEmissao.getFatura().getChavePrimaria(), avisoCorteEmissao);
		}
		
		for (Fatura fatura : faturas) {
			if(mapaAvisoCorteEmissao.containsKey(fatura.getChavePrimaria())) {
				fatura.setHabilitadoCobranca(Boolean.FALSE);
			}
		}
	}
	
	private Collection<Fatura> validarSeTemServicoAutorizacao(Collection<Fatura> faturas) throws NegocioException {
		Collection<Fatura> listaFaturaComServicoAutorizacao = new HashSet<>();

		for (Fatura fatura : faturas) {
			if (validarSeTemServicoAutorizacaoPorFatura(fatura)) {
				listaFaturaComServicoAutorizacao.add(fatura);
			}
		}

		return listaFaturaComServicoAutorizacao;

	}
	
	@Override
	public Boolean validarSeTemServicoAutorizacaoPorFatura(Fatura fatura) throws NegocioException {
		ControladorServicoAutorizacao controladorServicoAutorizacao = (ControladorServicoAutorizacao) ServiceLocator
				.getInstancia().getBeanPorID(ControladorServicoAutorizacao.BEAN_ID_CONTROLADOR_SERVICO_AUTORIZACAO);

		Long pontoConsumoChave = fatura.getPontoConsumo().getChavePrimaria();
		
		Collection<ServicoAutorizacao> listaServicoAutorizacao;

		listaServicoAutorizacao = controladorServicoAutorizacao.consultarServicoAutorizacaoPorClienteOuImovel(null,
				null, pontoConsumoChave);

		for (ServicoAutorizacao servicoAutorizacaoAux : listaServicoAutorizacao) {
			if ("CORTE".equals(servicoAutorizacaoAux.getServicoTipo().getDescricao())
					&& ("EM EXECUCAO".equals(servicoAutorizacaoAux.getStatus().getDescricao())
							|| "EXECUTADO".equals(servicoAutorizacaoAux.getStatus().getDescricao())
							|| "ABERTA".equals(servicoAutorizacaoAux.getStatus().getDescricao())
							|| "PENDENTE DE ATUALIZACAO".equals(servicoAutorizacaoAux.getStatus().getDescricao()))) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}

}
