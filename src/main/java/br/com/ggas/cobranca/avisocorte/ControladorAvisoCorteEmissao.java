/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 15:04:20
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de emissão do aviso de corte. 
 *
 */
public interface ControladorAvisoCorteEmissao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_AVISO_CORTE_EMISSAO = "controladorAvisoCorteEmissao";

	/**
	 * Consultar.
	 * 
	 * @param filtroAvisoCorteEmissao
	 *            the filtro aviso corte emissao
	 * @return the collection
	 */
	Collection<AvisoCorteEmissao> consultar(Map<String, Object> filtroAvisoCorteEmissao);

	/**
	 * Consultar faturas validas para emissao da notificacao corte.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param acaoComando
	 *            the acao comando
	 * @param isNotificacao
	 *            the is notificacao
	 * @param isBatchAvisoCorte
	 *            the is batch aviso corte
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasValidasNotificacaoCorte(Map<String, Object> filtro, AcaoComando acaoComando) throws NegocioException;
	
	/**
	 * Consultar faturas validas para emissao do aviso corte.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param acaoComando
	 *            the acao comando
	 * @param isNotificacao
	 *            the is notificacao
	 * @param isBatchAvisoCorte
	 *            the is batch aviso corte
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasValidasAvisoCorte(Map<String, Object> filtro, AcaoComando acaoComando) throws NegocioException;

	/**
	 * Valida se tem Servico Autorizacao Aberto Por Fatura
	 * @param fatura - {@link Fatura}
	 * @return Boolean - {@link Boolean}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Boolean validarSeTemServicoAutorizacaoPorFatura(Fatura fatura) throws NegocioException;
	
}
