/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 15:04:52
 @author wcosta
 */

package br.com.ggas.cobranca.avisocorte.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVOCobranca;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.batch.acao.negocio.ControladorAcao;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao;
import br.com.ggas.cobranca.dominio.DadosCicloCorte;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.cobranca.acaocobranca.AvisoCorteVO;
import br.com.ggas.web.cobranca.acaocobranca.SubRelatorioFaturasVencidasCobrancaVO;
/**
 * Classe responsável pela implementação do controlador do aviso de corte. 
 *
 */
public class ControladorAvisoCorteImpl extends ControladorNegocioImpl implements ControladorAvisoCorte {

	private static final String ENDERECO_EMPRESA = "enderecoEmpresa";

	private static final String CONTATO_EMPRESA = "contatoEmpresa";
	
	

	@Autowired
	private ControladorDocumentoImpressaoLayout controladorDocumentoImpressaoLayout;
	
	@Autowired 
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorEntregaDocumento controladorEntregaDocumento;
	
	@Autowired
	private ControladorAcao controladorAcao;
	
	@Autowired
	private ControladorAvisoCorteEmissao controladorAvisoCorteEmissao;
		
	private static final String ID_FATURA = "idFatura";	
	
	private static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";
	
	private static final Logger LOG = Logger.getLogger(ControladorAvisoCorteImpl.class);

	private ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
					ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AvisoCorte.BEAN_ID_AVISO_CORTE);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(AvisoCorte.BEAN_ID_AVISO_CORTE);
	}

	public Class<?> getClasseEntidadeEntregaDocumento() {

		return ServiceLocator.getInstancia().getClassPorID(EntregaDocumento.BEAN_ID_ENTREGA_DOCUMENTO);

	}

	public Class<?> getClasseEntidadeContrato() {

		return ServiceLocator.getInstancia().getClassPorID(Contrato.BEAN_ID_CONTRATO);

	}

	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte#listar(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AvisoCorte> listar(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();
		
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacaoRecebimentoPago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		EntidadeConteudo situacaoPagamentoQuitado = controladorEntidadeConteudo.obter(Long.valueOf(situacaoRecebimentoPago));

		if (filtro != null) {

			Long idCliente = (Long) filtro.get("idCliente");
			Long idFatura = (Long) filtro.get(ID_FATURA);
			Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
			Date dataAviso = (Date) filtro.get("dataAviso");
			Date dataNotificacao = (Date) filtro.get("dataNotificacao");
			String tipo = (String) filtro.get("tipo");
			Boolean entregaDocumento = (Boolean) filtro.get("entregaDocumento");
			Long idImovel = (Long) filtro.get("idImovel");
			Boolean mensagemNotificacao = (Boolean) filtro.get("menssagemNotificacao");
			Boolean priorizarCorte = (Boolean) filtro.get("priorizarCorte");

			criteria.createAlias("cliente", "cliente");
			criteria.createAlias("fatura", "fatura");


			if (idCliente != null) {
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}

			if (idFatura != null) {
				criteria.add(Restrictions.eq("fatura.chavePrimaria", idFatura));
			}

			if (idPontoConsumo != null) {
				criteria.createAlias("pontoConsumo", "pontoConsumo");
				if ((idImovel != null) && (idImovel > 0)) {
					criteria.add(Restrictions.eq("pontoConsumo.imovel.chavePrimaria", idImovel));
				}
				criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
			}

			if (dataAviso != null) {
				criteria.add(Restrictions.le("dataAviso", dataAviso));
			}

			if (dataNotificacao != null) {
				criteria.add(Restrictions.le("dataNotificacao", dataNotificacao));
			}

			if (tipo != null) {
				if ("notificacaoCorte".equalsIgnoreCase(tipo)) {
					criteria.add(Restrictions.isNotNull("dataNotificacao"));
				}

				if ("avisoCorte".equalsIgnoreCase(tipo)) {
					criteria.add(Restrictions.isNotNull("dataNotificacao"));
					criteria.add(Restrictions.isNotNull("dataAviso"));
				}
			}
			
			if(priorizarCorte != null && priorizarCorte) {
				criteria.add(Restrictions.ne("fatura.situacaoPagamento.chavePrimaria", situacaoPagamentoQuitado.getChavePrimaria()));
				criteria.add(Restrictions.isNull("dataCorte"));
			}

			if (entregaDocumento != null && entregaDocumento) {

				DetachedCriteria subconsulta = DetachedCriteria.forClass(this.getClasseEntidadeEntregaDocumento(), "entregaDocumento");
				subconsulta.createAlias("entregaDocumento.avisoCorte", "avisoCorte");
				subconsulta.setProjection(Property.forName("avisoCorte.chavePrimaria"));
				criteria.add(Subqueries.propertyNotIn("chavePrimaria", subconsulta));

			}
			
			if(mensagemNotificacao != null && mensagemNotificacao) {
				criteria.addOrder(Order.desc("dataNotificacao"));
			}else {
				criteria.addOrder(Order.asc("dataNotificacao"));
			}



		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte#emitir2ViaNotificacaoCorte(java.lang.Long)
	 */
	@Override
	public byte[] emitir2ViaNotificacaoCorte(Long chavePrimaria) throws IOException, GGASException {

		String codigoUnico;

		AvisoCorte aviso = (AvisoCorte) this.obter(chavePrimaria);

		List<AvisoCorteVO> listaAvisoCorte = new ArrayList<>();

		List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturasVencidas = new ArrayList<>();

		SubRelatorioFaturasVencidasCobrancaVO faturaVO = new SubRelatorioFaturasVencidasCobrancaVO();

		faturaVO.setAtraso(String.valueOf(Util.intervaloDatas(aviso.getFatura().getDataVencimento(), Util.getDataCorrente(false))));
		faturaVO.setDeposito("");
		faturaVO.setEmissao(aviso.getFatura().getDataEmissaoFormatada());
		faturaVO.setDescricaoPontoConsumo(aviso.getFatura().getPontoConsumo().getDescricaoFormatada());

		DebitosACobrar calculo = controladorArrecadacao.calcularAcrescimentoImpontualidade(aviso.getFatura(), Util.getDataCorrente(false),
						aviso.getFatura().getValorTotal(), false, true);
		if (calculo != null && calculo.getJurosMora() != null) {
			faturaVO.setJuros(calculo.getJurosMora());
		} else {
			faturaVO.setJuros(BigDecimal.ZERO);
		}
		faturaVO.setTitulo(String.valueOf(aviso.getFatura().getChavePrimaria()));
		faturaVO.setValor(aviso.getFatura().getValorTotal());
		faturaVO.setSaldo(aviso.getFatura().getValorSaldoConciliado());
		if (calculo != null && calculo.getJurosMora() != null) {
			faturaVO.setValorPagar(aviso.getFatura().getValorTotal().add(calculo.getJurosMora()));
		} else {
			faturaVO.setValorPagar(aviso.getFatura().getValorTotal());
		}
		faturaVO.setVencimento(aviso.getFatura().getDataVencimentoFormatada());

		colecaoFaturasVencidas.add(faturaVO);

		if (aviso.getFatura().getPontoConsumo().getCodigoLegado() != null) {
			codigoUnico = String.valueOf(aviso.getFatura().getPontoConsumo().getCodigoLegado());
		} else {
			codigoUnico = String.valueOf(aviso.getFatura().getPontoConsumo().getChavePrimaria());
		}

		AvisoCorteVO aCVo = new AvisoCorteVO();

		aCVo.setCodigo(String.valueOf(aviso.getCliente().getChavePrimaria()));
		aCVo.setCodigoUnico(codigoUnico);
		aCVo.setData(Util.dataAtualPorExtenso());
		aCVo.setNome(aviso.getCliente().getNome());

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setEndereco(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
			aCVo.setCep(aviso.getCliente().getEnderecoPrincipal().getCep().getCep());
		} else {
			aCVo.setEndereco("");
			aCVo.setCep("");
		}

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setBairro(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairro());
		} else {
			aCVo.setBairro("");
		}

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setCidade(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUFCEP());
			aCVo.setMunicipioUf(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
		} else {
			aCVo.setCidade("");
			aCVo.setMunicipioUf("");
		}

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setPontoEntrega(aviso.getFatura().getPontoConsumo().getDescricao());
		} else {
			aCVo.setPontoEntrega("");
		}

		if (aviso.getFatura().getContrato() != null) {

			aCVo.setCodigoContrato(aviso.getFatura().getContrato().getNumeroCompletoContrato());
		}
		if (aviso.getPontoConsumo().getSegmento() != null) {

			aCVo.setTipoResidencia(aviso.getPontoConsumo().getSegmento().getDescricao());
		} else {
			aCVo.setTipoResidencia("");
		}
		aCVo.setEnderecoPontoConsumoRelatorioCorteSergas(aviso.getPontoConsumo().getEnderecoFormatadoRelatorioCorteSergas());
		aCVo.setColecaoFaturaVencida(colecaoFaturasVencidas);

		listaAvisoCorte.add(aCVo);

		byte[] bytes = null;

		String arquivoRelatorio = controladorDocumentoImpressaoLayout
						.obterDocumentoImpressaoLayoutPorConstante(Constantes.NOTIFICACAO_CORTE);
		if (arquivoRelatorio == null || arquivoRelatorio.isEmpty()) {

			throw new GGASException("Cadastre um Layout em Documento Impressao Layout");

		}

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		Map<String, Object> parametros = new HashMap<>();
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		if (empresa != null) {

			if (empresa.getCliente().getEnderecoPrincipal() != null) {
				parametros.put(ENDERECO_EMPRESA, empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento()
								+ empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairroMunicipioUFCEP());
			} else {
				parametros.put(ENDERECO_EMPRESA, "");
			}
			if (empresa.getCliente().getContatos() != null && !empresa.getCliente().getContatos().isEmpty()) {
				parametros.put(CONTATO_EMPRESA, empresa.getCliente().getContatos().iterator().next().getFone()
								+ empresa.getCliente().getContatos().iterator().next().getEmail());
			} else {
				parametros.put(CONTATO_EMPRESA, "");
			}

			if (empresa.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}

		}
		String local = "";
		if (empresa != null && empresa.getCliente() != null && empresa.getCliente().getEnderecoPrincipal() != null) {
			local = empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF();
		}

		parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		bytes = RelatorioUtil.gerarRelatorioPDF(listaAvisoCorte, parametros, arquivoRelatorio);

		return bytes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte#emitir2ViaAvisoCorte(java.lang.Long)
	 */
	@Override
	public byte[] emitir2ViaAvisoCorte(Long chavePrimaria) throws IOException, GGASException {

		AvisoCorte aviso = (AvisoCorte) this.obter(chavePrimaria);

		List<AvisoCorteVO> listaAvisoCorte = new ArrayList<>();

		String codigoUnico;

		List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturasVencidas = new ArrayList<>();

		SubRelatorioFaturasVencidasCobrancaVO faturaVO = new SubRelatorioFaturasVencidasCobrancaVO();

		faturaVO.setAtraso(String.valueOf(Util.intervaloDatas(aviso.getFatura().getDataVencimento(), Util.getDataCorrente(false))));
		faturaVO.setDeposito("");
		faturaVO.setEmissao(aviso.getFatura().getDataEmissaoFormatada());
		faturaVO.setDescricaoPontoConsumo(aviso.getFatura().getPontoConsumo().getDescricaoFormatada());

		DebitosACobrar calculo = controladorArrecadacao.calcularAcrescimentoImpontualidade(aviso.getFatura(), Util.getDataCorrente(false),
						aviso.getFatura().getValorTotal(), false, true);
		if (calculo != null && calculo.getJurosMora() != null) {
			faturaVO.setJuros(calculo.getJurosMora());
		} else {
			faturaVO.setJuros(BigDecimal.ZERO);
		}
		faturaVO.setTitulo(String.valueOf(aviso.getFatura().getChavePrimaria()));
		faturaVO.setValor(aviso.getFatura().getValorTotal());
		faturaVO.setSaldo(aviso.getFatura().getValorSaldoConciliado());
		if (calculo != null && calculo.getJurosMora() != null) {
			faturaVO.setValorPagar(aviso.getFatura().getValorTotal().add(calculo.getJurosMora()));
		} else {
			faturaVO.setValorPagar(aviso.getFatura().getValorTotal());
		}
		faturaVO.setVencimento(aviso.getFatura().getDataVencimentoFormatada());

		colecaoFaturasVencidas.add(faturaVO);

		if (aviso.getFatura().getPontoConsumo().getCodigoLegado() != null) {
			codigoUnico = String.valueOf(aviso.getFatura().getPontoConsumo().getCodigoLegado());
		} else {
			codigoUnico = String.valueOf(aviso.getFatura().getPontoConsumo().getChavePrimaria());
		}

		AvisoCorteVO aCVo = new AvisoCorteVO();

		aCVo.setCodigo(String.valueOf(aviso.getCliente().getChavePrimaria()));
		aCVo.setCodigoUnico(codigoUnico);
		aCVo.setNome(aviso.getCliente().getNome());
		aCVo.setData(Util.dataAtualPorExtenso());
		if (aviso.getFatura().getContrato() != null) {

			aCVo.setCodigoContrato(aviso.getFatura().getContrato().getNumeroCompletoContrato());
		} else {
			aCVo.setCodigoContrato("");
		}
		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setEndereco(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
			aCVo.setCep(aviso.getCliente().getEnderecoPrincipal().getCep().getCep());
		} else {
			aCVo.setEndereco("");
			aCVo.setCep("");
		}

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setBairro(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairro());
		} else {
			aCVo.setBairro("");
		}

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setCidade(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUFCEP());
			aCVo.setMunicipioUf(aviso.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
		} else {
			aCVo.setCidade("");
			aCVo.setMunicipioUf("");
		}

		if (aviso.getCliente().getEnderecoPrincipal() != null) {
			aCVo.setPontoEntrega(aviso.getFatura().getPontoConsumo().getDescricao());
		} else {
			aCVo.setPontoEntrega("");
		}
		if (aviso.getPontoConsumo().getSegmento() != null) {

			aCVo.setTipoResidencia(aviso.getPontoConsumo().getSegmento().getDescricao());
		} else {
			aCVo.setTipoResidencia("");
		}
		aCVo.setEnderecoPontoConsumoRelatorioCorteSergas(aviso.getPontoConsumo().getEnderecoFormatadoRelatorioCorteSergas());			
		aCVo.setColecaoFaturaVencida(colecaoFaturasVencidas);

		listaAvisoCorte.add(aCVo);

		byte[] bytes = null;

		String arquivoRelatorio = controladorDocumentoImpressaoLayout.obterDocumentoImpressaoLayoutPorConstante(Constantes.AVISO_CORTE);
		if (arquivoRelatorio == null || arquivoRelatorio.isEmpty()) {

			throw new GGASException("Cadastre um Layout em Documento Impressao Layout");

		}

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		Map<String, Object> parametros = new HashMap<>();
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		if (empresa != null) {

			if (empresa.getCliente().getEnderecoPrincipal() != null) {
				parametros.put(ENDERECO_EMPRESA, empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento()
								+ empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairroMunicipioUFCEP());
			} else {
				parametros.put(ENDERECO_EMPRESA, "");
			}
			if (empresa.getCliente().getContatos() != null && !empresa.getCliente().getContatos().isEmpty()) {
				parametros.put(CONTATO_EMPRESA, empresa.getCliente().getContatos().iterator().next().getFone()
								+ empresa.getCliente().getContatos().iterator().next().getEmail());
			} else {
				parametros.put(CONTATO_EMPRESA, "");
			}

			if (empresa.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}

		}
		String local = "";
		if (empresa != null && empresa.getCliente() != null && empresa.getCliente().getEnderecoPrincipal() != null) {
			local = empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF();
		}

		parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		bytes = RelatorioUtil.gerarRelatorioPDF(listaAvisoCorte, parametros, arquivoRelatorio);

		return bytes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte#criarListaServicoAutorizacaoVOCobranca(java.util.Collection)
	 */
	@Override
	public Collection<ServicoAutorizacaoVOCobranca> criarListaServicoAutorizacaoVOCobranca(Collection<AvisoCorte> avisosCorte,
			Long idPontoConsumo) throws NegocioException {

		Long chavePrimariaPontoConsumo = null;
		Date dataAvisoCorte = null;
		List<ServicoAutorizacaoVOCobranca> listaServicoVO = new ArrayList<>();
		List<Long> listaIdPontoConsumo = null;
		ControladorServicoAutorizacao controladorServicoAutorizacao = ServiceLocator.getInstancia().getControladorServicoAutorizacao();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		for (AvisoCorte avisoCorte : avisosCorte) {
			if (idPontoConsumo == null) {
				if (avisoCorte.getPontoConsumo() == null) {
					listaIdPontoConsumo = populaListaIdPontoConsumoParaFaturaAgrupada(avisoCorte);
				} else {
					chavePrimariaPontoConsumo = avisoCorte.getPontoConsumo().getChavePrimaria();
				}
			}else{
				chavePrimariaPontoConsumo = idPontoConsumo;
			}
			dataAvisoCorte = avisoCorte.getDataAviso();

			String constanteServicoTipoCorte = Fachada.getInstancia().obterConstantePorCodigo(Constantes.C_SERVICO_TIPO_CORTE).getValor();

			if (constanteServicoTipoCorte == null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CONSTANTE_NAO_CONFIGURADA, Constantes.C_SERVICO_TIPO_CORTE);
			}

			Collection<ServicoAutorizacao> listaAutorizacaoCorte = null;

			ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_CANCELADO);
			EntidadeConteudo statusCancelado = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

			if (listaIdPontoConsumo != null && !listaIdPontoConsumo.isEmpty()) {

				listaAutorizacaoCorte =
						controladorServicoAutorizacao.obterServicoAutorizacao(null, dataAvisoCorte,
								Long.valueOf(constanteServicoTipoCorte), listaIdPontoConsumo, statusCancelado);
				List<String> listaData = new ArrayList<>();
				
				if(listaAutorizacaoCorte == null){
					listaAutorizacaoCorte = new ArrayList<>();
				}
				for (Iterator<ServicoAutorizacao> iterator = listaAutorizacaoCorte.iterator(); iterator.hasNext();) {

					ServicoAutorizacao servicoAutorizacao = iterator.next();
					String dataServicoAutorizacao = DataUtil.converterDataParaString(servicoAutorizacao.getDataGeracao(), false);

					if (listaData.contains(dataServicoAutorizacao)) {
						iterator.remove();
					} else {
						listaData.add(dataServicoAutorizacao);
					}

				}
			} else {
				listaAutorizacaoCorte =
						controladorServicoAutorizacao.obterServicoAutorizacao(chavePrimariaPontoConsumo, dataAvisoCorte,
								Long.valueOf(constanteServicoTipoCorte), null, statusCancelado);
			}

			if (listaAutorizacaoCorte != null && listaAutorizacaoCorte.isEmpty()) {
				ServicoAutorizacaoVOCobranca serVoCobranca = new ServicoAutorizacaoVOCobranca();
				serVoCobranca.setAvisoCorte(avisoCorte);
				listaServicoVO.add(serVoCobranca);
			} else {
				for (Iterator<ServicoAutorizacao> iterator = listaAutorizacaoCorte.iterator(); iterator.hasNext();) {
					ServicoAutorizacaoVOCobranca serVoCobranca = new ServicoAutorizacaoVOCobranca();
					serVoCobranca.setAvisoCorte(avisoCorte);

					ServicoAutorizacao seAutorizacao = iterator.next();

					// Verifica se a data de autorizacao de corte que esta sendo recuperada já foi setada em
					// ServicoAutorizacaoVOCobranca da lista, se já foi, não seta mais.
					if (listaServicoVO.isEmpty()) {
						serVoCobranca.setDataAutorizacao(seAutorizacao.getDataGeracao());
						serVoCobranca.setNomeEquipeCorte(seAutorizacao.getEquipe().getNome());
						serVoCobranca.setNomeOperadorCorte(seAutorizacao.getUsuarioSistema().getLogin());
						serVoCobranca.setPrioridadeCorte(seAutorizacao.getServicoTipoPrioridade().getDescricao());
						serVoCobranca.setServicoTipoCorte(seAutorizacao.getServicoTipo().getDescricao());
						serVoCobranca.setStatusCorte(seAutorizacao.getStatus().getDescricao());

					}

					if (!listaServicoVO.isEmpty()) {
						serVoCobranca.setDataAutorizacao(seAutorizacao.getDataGeracao());
						serVoCobranca.setNomeEquipeCorte(seAutorizacao.getEquipe().getNome());
						serVoCobranca.setNomeOperadorCorte(seAutorizacao.getUsuarioSistema().getLogin());
						serVoCobranca.setPrioridadeCorte(seAutorizacao.getServicoTipoPrioridade().getDescricao());
						serVoCobranca.setServicoTipoCorte(seAutorizacao.getServicoTipo().getDescricao());
						serVoCobranca.setStatusCorte(seAutorizacao.getStatus().getDescricao());
					}
					
					List<HistoricoOperacaoMedidor> listaHistoricoMedidorBloqueados = null;
					try {

						listaHistoricoMedidorBloqueados =
								(ArrayList<HistoricoOperacaoMedidor>) Fachada.getInstancia().obterHistoricoOperacaoMedidor(
										chavePrimariaPontoConsumo,
										serVoCobranca.getDataAutorizacao(),
										Long.valueOf(Fachada.getInstancia().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_BLOQUEIO)
												.getValor()));
					} catch (Exception e) {
						LOG.error(e);
					}

					Date dataCorte = null;
					if (listaHistoricoMedidorBloqueados!= null && CollectionUtils.isNotEmpty(listaHistoricoMedidorBloqueados)) {

						HistoricoOperacaoMedidor hisMedidorDataMaisProxima = listaHistoricoMedidorBloqueados.get(0);

						serVoCobranca.setDataCorte(hisMedidorDataMaisProxima.getDataRealizada());

						dataCorte = hisMedidorDataMaisProxima.getDataRealizada();

						Collection<ServicoAutorizacao> listaAutorizacaoReligacao =
								controladorServicoAutorizacao.obterServicoAutorizacao(
										chavePrimariaPontoConsumo,
										dataCorte,
										Long.valueOf(Fachada.getInstancia().obterConstantePorCodigo(Constantes.C_SERVICO_TIPO_RELIGACAO)
												.getValor()), null, null);

						if (listaAutorizacaoReligacao != null && !listaAutorizacaoReligacao.isEmpty()) {

							ServicoAutorizacao servicoAutorizacao = listaAutorizacaoReligacao.iterator().next();
							serVoCobranca.setNomeEquipeReligacao(servicoAutorizacao.getEquipe().getNome());
							serVoCobranca.setNomeOperadorReligacao(servicoAutorizacao.getUsuarioSistema().getLogin());
							serVoCobranca.setPrioridadeReligacao(servicoAutorizacao.getServicoTipoPrioridade().getDescricao());
							serVoCobranca.setServicoTipoReativacao(servicoAutorizacao.getServicoTipo().getDescricao());
							serVoCobranca.setStatusReligacao(servicoAutorizacao.getStatus().getDescricao());

							List<HistoricoOperacaoMedidor> listaHistoricoMedidorReligados = null;
							try {

								listaHistoricoMedidorReligados =
										(ArrayList<HistoricoOperacaoMedidor>) Fachada.getInstancia().obterHistoricoOperacaoMedidor(
												chavePrimariaPontoConsumo,
												serVoCobranca.getDataAutorizacao(),
												Long.valueOf(Fachada.getInstancia()
														.obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_REATIVACAO).getValor()));
							} catch (Exception e) {
								LOG.error(e);
							}

							if (listaHistoricoMedidorReligados != null && CollectionUtils.isNotEmpty(listaHistoricoMedidorReligados)) {
								HistoricoOperacaoMedidor hisOperacaoMedidor = listaHistoricoMedidorReligados.get(0);
								serVoCobranca.setDataReativacao(hisOperacaoMedidor.getDataRealizada());
							}
						}
					}
					listaServicoVO.add(serVoCobranca);
				}
			}
		}

		return listaServicoVO;
	}

	/**
	 * Popula lista de chaves de pontos de consumo de uma fatura agrupada.
	 * 
	 * @param avisoCorte
	 * @return idsPontoConsumo
	 */
	private List<Long> populaListaIdPontoConsumoParaFaturaAgrupada(AvisoCorte avisoCorte) {
		// Quando a fatura referente ao aviso de corte é agrupada, deve-se recuperar as chaves de todos os pontos de consumo desta
		// fatura para posteriormente obter as autorizações de serviço referentes a cada ponto de consumo.
		List<Long> idsPontoConsumo = new ArrayList<>();
		Collection<Fatura> listaFatura = Fachada.getInstancia().listarFaturaPorFaturaAgrupada(avisoCorte.getFatura().getChavePrimaria());

		for (Fatura fatura : listaFatura) {
			idsPontoConsumo.add(fatura.getPontoConsumo().getChavePrimaria());
		}

		return idsPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte#consultarAvisoCorteAcompanhamentoCobranca(java.util.Map)
	 */
	@Override
	public Collection<AvisoCorte> consultarAvisoCorteAcompanhamentoCobranca(Map<String, Object> filtro) {

		
		Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
		Long idCliente = (Long) filtro.get("idCliente");
		Long idImovel = (Long) filtro.get("idImovel");
		Long idFatura = (Long) filtro.get(ID_FATURA);


		Criteria criteria = this.createCriteria(getClasseEntidade());
		criteria.createAlias("fatura", "fatura", Criteria.INNER_JOIN);
		criteria.setFetchMode("fatura", FetchMode.JOIN);
		
		criteria.createAlias("fatura.contratoAtual", "contratoAtual", Criteria.INNER_JOIN);
		criteria.setFetchMode("contratoAtual", FetchMode.JOIN);

		DetachedCriteria subconsultaFaturaAgrupada = null;
		DetachedCriteria subconsultaFaturaComum = null;
				
		if((idImovel!=null && idImovel>0)||(idPontoConsumo!=null && idPontoConsumo>0)){
			
			subconsultaFaturaAgrupada = montaSubSelectImovelOuPontoConsumoQuandoFaturaAgrupada(idImovel, idPontoConsumo);
			Criterion criterionFaturaAgrupada = Subqueries.propertyIn("fatura.chavePrimaria", subconsultaFaturaAgrupada);
			
			subconsultaFaturaComum = montaSubSelectPontoConsumoOuImovel(idPontoConsumo, idImovel);
			Criterion criterionFaturaComum = Subqueries.propertyIn("fatura.pontoConsumo.chavePrimaria", subconsultaFaturaComum);
			
			criteria.add(Restrictions.or(criterionFaturaAgrupada, criterionFaturaComum));
			
		}
		
		if (idCliente != null && idCliente > 0) {
			criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
		}
		if (idFatura != null && idFatura > 0) {
			criteria.add(Restrictions.eq("fatura.chavePrimaria", idFatura));
		}
		
		DetachedCriteria subconsulta = montaSubSelectContrato();
		criteria.add(Subqueries.propertyIn("contratoAtual.numeroCompletoContrato", subconsulta));
		
		return criteria.list();
	}

	/**
	 * Subselect para ponto de consumo ou imovel de faturas agrupadas.
	 * 
	 * @param idImovel
	 * @param idPontoConsumo
	 * @return subconsulta
	 */
	private DetachedCriteria montaSubSelectImovelOuPontoConsumoQuandoFaturaAgrupada(Long idImovel, Long idPontoConsumo) {

		Class<?> classeFatura = ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
		DetachedCriteria subconsulta = DetachedCriteria.forClass(classeFatura, "fatura");
		subconsulta.createAlias("pontoConsumo", "pontoConsumo", Criteria.INNER_JOIN);
		
		corpoSubSelect(idImovel, idPontoConsumo, subconsulta);

		subconsulta.setProjection(Property.forName("faturaAgrupada.chavePrimaria"));

		return subconsulta;
	}

	/**
	 * Subselect para ponto de consumo de faturas que não são agrupadas.
	 * 
	 * @param idImovel
	 * @param idPontoConsumo
	 * @return subconsulta
	 */
	private DetachedCriteria montaSubSelectPontoConsumoOuImovel(Long idPontoConsumo, Long idImovel) {
		Class<?> classePontoConsumo = ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
		DetachedCriteria subconsulta = DetachedCriteria.forClass(classePontoConsumo, "pontoConsumo");

		corpoSubSelect(idImovel, idPontoConsumo, subconsulta);
		
		subconsulta.setProjection(Property.forName(PONTO_CONSUMO_CHAVE_PRIMARIA));
		return subconsulta;
	}
	
	/**
	 * Subselect para ponto de consumo de faturas que não são agrupadas.
	 * 
	 * @return subconsulta
	 */
	private DetachedCriteria montaSubSelectContrato() {
		DetachedCriteria subconsulta = DetachedCriteria.forClass(getClasseEntidadeContrato(), "contrato");
		subconsulta.add(Restrictions.eqProperty("contrato.numeroCompletoContrato", "contratoAtual.numeroCompletoContrato"));
		subconsulta.add(Restrictions.eq("contrato.habilitado", true));
		subconsulta.setProjection(Property.forName("contrato.numeroCompletoContrato"));
		return subconsulta;
	}
	
	private void corpoSubSelect(Long idImovel, Long idPontoConsumo, DetachedCriteria subconsulta) {
		subconsulta.createAlias("pontoConsumo.imovel", "imovel", Criteria.INNER_JOIN);

		SimpleExpression restricaoPontoConsumo = null;
		SimpleExpression restricaoImovel = null;

		if (idImovel != null && idImovel > 0) {
			restricaoImovel = Restrictions.eq("imovel.chavePrimaria", idImovel);
		}
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			restricaoPontoConsumo = Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo);
		}

		if (restricaoPontoConsumo != null && restricaoImovel != null) {
			subconsulta.add(Restrictions.or(restricaoPontoConsumo, restricaoImovel));
		} else if (restricaoPontoConsumo != null) {
			subconsulta.add(restricaoPontoConsumo);
		} else if (restricaoImovel != null) {
			subconsulta.add(restricaoImovel);
		}
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * consultarFaturas(java
	 * .lang.Long[])
	 */
	@Override
	public Collection<AvisoCorte> consultarAvisosCorte(Long[] chavesAvisoCorte) throws NegocioException {

		Collection<AvisoCorte> listaAvisoCorte = null;

		if ((chavesAvisoCorte != null) && (chavesAvisoCorte.length > 0)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" FROM ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" avisoCorte ");
			hql.append(" WHERE ");

			hql.append(" avisoCorte.chavePrimaria in (:chavesAvisoCorte) ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameterList("chavesAvisoCorte", chavesAvisoCorte);

			listaAvisoCorte = query.list();
		}

		return listaAvisoCorte;
	}
	
	@Override
	public Boolean verificarObrigatoriedadeProtocolo () throws NegocioException {

		ParametroSistema parametroObrigatorioRetornoProtocolo = 
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_OBRIGATORIO_RETORNO_PROTOCOLO);

		if (parametroObrigatorioRetornoProtocolo.getValorLong() == 0) {
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;

	}
	
	@Override
	public Collection<AvisoCorte> obterListaAvisoCortePriorizar() throws GGASException{
		ControladorServicoAutorizacao controladorServicoAutorizacao = (ControladorServicoAutorizacao) ServiceLocator
				.getInstancia().getBeanPorID(ControladorServicoAutorizacao.BEAN_ID_CONTROLADOR_SERVICO_AUTORIZACAO);
		
		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put("tipo", "avisoCorte");
		filtro.put("priorizarCorte", Boolean.TRUE);
		
		Collection<AvisoCorte> listaAvisoCorte = this.listar(filtro);
		Collection<AvisoCorte> retirarAvisoSemProtocoloEntregue = new HashSet<>();
		EntregaDocumento documentoProtocolo;
		Boolean isObrigatorioProtocolo = verificarObrigatoriedadeProtocolo();
		Integer diasVencimentos = consultarDiasVencimentoAcaoCorte();
		Date dataAtual = new Date();
		
		controladorServicoAutorizacao.checagemExecucaoServicoCorte(listaAvisoCorte);
		
		for(AvisoCorte avisoCorte : listaAvisoCorte) {
			Integer diasVencido = DataUtil.diferencaDiasEntreDatas(avisoCorte.getFatura().getDataVencimento(), dataAtual);
			
			if (!DadosCicloCorte.validarFaturaEmissaoNotificacaoCorte(avisoCorte.getFatura(), null, null)
					|| diasVencimentos > diasVencido
					|| controladorAvisoCorteEmissao.validarSeTemServicoAutorizacaoPorFatura(avisoCorte.getFatura())) {
				retirarAvisoSemProtocoloEntregue.add(avisoCorte);
			}
						
			if (isObrigatorioProtocolo) {
				Integer diasSituacao = null;
				documentoProtocolo = controladorEntregaDocumento
						.obterEntregaDocumentoProtocoloRetorno(avisoCorte.getChavePrimaria());
				if(documentoProtocolo != null) {
					diasSituacao = DataUtil.diferencaDiasEntreDatas(documentoProtocolo.getDataSituacao(), dataAtual);
				}
				if (documentoProtocolo == null || (diasSituacao != null && diasSituacao < 3)) {
					retirarAvisoSemProtocoloEntregue.add(avisoCorte);
				}
			}
			
			Integer vencimentoTituloAvisoCorte = DataUtil.diferencaDiasEntreDatas(avisoCorte.getFatura().getDataVencimento(), new Date());
			avisoCorte.getFatura().setDiasAtraso(vencimentoTituloAvisoCorte);
		}
		
		listaAvisoCorte.removeAll(retirarAvisoSemProtocoloEntregue);
		
		return listaAvisoCorte;
	}
	
	@Override
	public Collection<AvisoCorte> removerAvisosDeCorte (Long[] idsAviso, Collection<AvisoCorte> listaAvisoCorte){
		Collection<AvisoCorte> listaSerRemovida = new HashSet<>();
		List<Long> chavesAviso = Arrays.asList(idsAviso);
		
		for(AvisoCorte avisoAux : listaAvisoCorte) {
			if(chavesAviso.contains(avisoAux.getChavePrimaria())) {
				listaSerRemovida.add(avisoAux);
			}
		}
		
		listaAvisoCorte.removeAll(listaSerRemovida);
		
		return listaAvisoCorte;
	}
	
	private Integer consultarDiasVencimentoAcaoCorte() throws NegocioException {
		Acao acaoAux = new Acao();
		acaoAux.setDescricao("AUTORIZACAO DE SERVICO (CORTE)");
		
		Acao acao = (Acao) controladorAcao.consultarAcao(acaoAux, Boolean.TRUE).iterator().next();
		
		return acao.getNumeroDiasVencimento();
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * consultarFaturas(java
	 * .lang.Long[])
	 */
	@Override
	public Collection<AvisoCorte> consultarAvisosCortePriorizados() throws NegocioException {
		
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacaoRecebimentoPago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		EntidadeConteudo situacaoPagamentoQuitado = controladorEntidadeConteudo.obter(Long.valueOf(situacaoRecebimentoPago));
		Collection<AvisoCorte> listaAvisoCorte = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" FROM ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" avisoCorte ");
			hql.append(" inner join fetch avisoCorte.fatura fatura ");
			hql.append(" WHERE ");

			hql.append(" avisoCorte.indicadorFaturaPriorizada = true");
			hql.append(" and fatura.situacaoPagamento.chavePrimaria <> :cdSituacao");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("cdSituacao", situacaoPagamentoQuitado.getChavePrimaria());

			listaAvisoCorte = query.list();

		return listaAvisoCorte;
	}
	
	@Override
	public Collection<AvisoCorte> selecionarNumeroAvisoCorte(Collection<AvisoCorte> listaAvisoCorte,
			Integer numeroAviso) throws NegocioException {
		Collection<AvisoCorte> listaAvisoCorteRetorno = new HashSet<>();
		Iterator<AvisoCorte> iteratorAvisoCorte = listaAvisoCorte.iterator();

		if (numeroAviso != null && numeroAviso > 0) {
			while (numeroAviso > 0) {
				if (!iteratorAvisoCorte.hasNext()) {
					break;
				}
				listaAvisoCorteRetorno.add(iteratorAvisoCorte.next());
				numeroAviso--;
			}
			
			listaAvisoCorte.removeAll(listaAvisoCorteRetorno);
			
			for(AvisoCorte aux : listaAvisoCorte) {
				if(Double.compare(0.0, aux.getDistanciaEntrePontos()) == 0) {
					listaAvisoCorteRetorno.add(aux);
					continue;
				}
				break;
			}
					
			return Util.ordenarColecaoPorAtributo(listaAvisoCorteRetorno, "ordemRoteirizar", Boolean.TRUE);
		}
		return listaAvisoCorte;
	}
	
	@Override
	public void salvarListaPriorizada (Long[] idsAviso, Collection<AvisoCorte> listaAvisoCorte) throws ConcorrenciaException, NegocioException{
		Collection<AvisoCorte> listaSerRemovida = new HashSet<>();
		Collection<AvisoCorte> listaCopiada = new HashSet<AvisoCorte>();
		List<Long> chavesAviso = Arrays.asList(idsAviso);
		
		for(AvisoCorte avisoAux : listaAvisoCorte) {
			listaCopiada.add(avisoAux);
			if(chavesAviso.contains(avisoAux.getChavePrimaria())) {
				avisoAux.setIndicadorFaturaPriorizada(Boolean.TRUE);
				listaSerRemovida.add(avisoAux);
			} else if (avisoAux.getIndicadorFaturaPriorizada()) {
				avisoAux.setIndicadorFaturaPriorizada(Boolean.FALSE);
			} else {
				continue;
			}
			this.atualizar(avisoAux);
		}
		listaCopiada.removeAll(listaSerRemovida);
		verificarPontoNaoPriorizado(listaSerRemovida, listaCopiada);
			
	}
	
	private void verificarPontoNaoPriorizado(Collection<AvisoCorte> listaAvisoCortePriorizado,
			Collection<AvisoCorte> listaAvisoCorteNaoPriorizado) throws NegocioException, ConcorrenciaException {
		Collection<AvisoCorte> listaRetorno = new HashSet<>();
		List<Long> chavesImovel = new ArrayList<Long>();
		List<Long> chavesImovelCondomonio = new ArrayList<Long>();
		
		for (AvisoCorte avisoCorteAux : listaAvisoCortePriorizado) {
			if (avisoCorteAux.getPontoConsumo().getImovel() != null) {
				chavesImovel.add(avisoCorteAux.getPontoConsumo().getImovel().getChavePrimaria());
			}
			if (avisoCorteAux.getPontoConsumo().getImovel().getImovelCondominio() != null) {
				chavesImovelCondomonio
						.add(avisoCorteAux.getPontoConsumo().getImovel().getImovelCondominio().getChavePrimaria());
			}
		}
		for (AvisoCorte avisoCorte : listaAvisoCorteNaoPriorizado) {
			if ((avisoCorte.getPontoConsumo().getImovel() != null
					&& chavesImovel.contains(avisoCorte.getPontoConsumo().getImovel().getChavePrimaria()))
					|| (avisoCorte.getPontoConsumo().getImovel().getImovelCondominio() != null
							&& chavesImovelCondomonio.contains(avisoCorte.getPontoConsumo().getImovel()
									.getImovelCondominio().getChavePrimaria()))) {
				listaRetorno.add(avisoCorte);
			}
		}
		
		for(AvisoCorte avisoCortePriorizar : listaRetorno) {
			avisoCortePriorizar.setIndicadorFaturaPriorizada(Boolean.TRUE);
			this.atualizar(avisoCortePriorizar);
		}
	}
}
