package br.com.ggas.cobranca.boletobancario.codigobarras;

import com.google.common.base.Strings;

import br.com.ggas.cobranca.boletobancario.codigobarras.dados.DadosCodigoBarras;
import br.com.ggas.cobranca.boletobancario.codigobarras.excecoes.TamanhoCampoExcedido;
import br.com.ggas.util.Util;

/**
 *	Classe responsável pelo código de barras Santander 
 *
 */
public class CodigoBarrasSantander extends CodigoBarrasFebraban {

	private static final int TAMANHO_CONTA = 7;
	private static final int TAMANHO_NOSSO_NUMERO = 12;
	private static final int TAMANHO_FIXO = 1;
	private static final int TAMANHO_IOF = 1;
	private static final int TAMANHO_TIPO_MODALIDADE_CARTEIRA = 3;
	private static final String VAZIO = "";
	private static final char ZERO = '0';

	/**
	 * Cria um {@link CodigoBarras} que atende o {@link LeiauteCodigoBarras} do
	 * santander.
	 *
	 * @param os dados para o preenchimento dos campos da Febraban e da Santander
	 */
	CodigoBarrasSantander(DadosCodigoBarras dados) {
		super(dados);
	}

	@Override
	String campoLivre(DadosCodigoBarras dados) {
		StringBuilder construtor = new StringBuilder();
		construtor.append(encher("9", TAMANHO_FIXO));
		construtor.append(encherCodigoConvenio(dados.arrecadadorContratoConvenio().getCodigoConvenio(), TAMANHO_CONTA));
		construtor.append(encher(dados.nossoNumero(), TAMANHO_NOSSO_NUMERO));
		construtor.append(
				encher(DigitoAutoConferencia.modulo11(autoConferenciaCampoLivre(dados)).toString(),
						TAMANHO_AUTO_CONFERENCIA));
		construtor.append(encher("0",TAMANHO_IOF));
		construtor.append(encher("101",TAMANHO_TIPO_MODALIDADE_CARTEIRA));
		return construtor.toString();
	}

	private String autoConferenciaCampoLivre(DadosCodigoBarras dados) {
		return new StringBuilder().append(encher(dados.nossoNumero(), TAMANHO_NOSSO_NUMERO)).toString();
	}
	
	String encherCodigoConvenio(String campo, int casas) {
		String valorCampo = Util.coalescenciaNula(campo, VAZIO);
		if(valorCampo.length() > casas) {
			throw new TamanhoCampoExcedido(valorCampo, casas);
		} else {
			return Strings.padEnd(valorCampo, casas, ZERO);
		}
	}


}
