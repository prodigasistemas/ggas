/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario.codigobarras;

import java.math.BigDecimal;

import br.com.ggas.util.Constantes;

/**
 * Agrupa métodos utilitários para o cálculo de dígitos de auto
 * conferência/dígitos de verificação. Para mais detalhes consultar a
 * documentação oficial disponibilizada pela Febraban ou pelos bancos.
 **/
public final class DigitoAutoConferencia {

	private static final int PARAMETRO = 2;
	private static final int MULTIPLICADOR = 2;
	private static final int TAMANHO_MAX_PARAMETRO = 9;

	/**
	 * Efetua o cálculo do dígito de auto conferência do código de barras. Este
	 * cálculo só pode ser realizado após o preenchimento de todos os campos
	 * envolvidos, o que varia de acordo com o seu propósito.
	 *
	 * @param numero
	 *            a composiçao de todos os campos envolvidos, na ordem
	 * @return o dígito resultante
	 */
	public static final Integer modulo10(final String numero) {
		Integer dac = 0;
		Integer valorResultado = 0;
		Integer multiplicador = MULTIPLICADOR;

		int posicao = 1;
		for (int i = 0; i < numero.length(); i++) {
			Integer digito = Integer.valueOf(numero.substring(numero.length() - posicao, numero.length() - i));
			Integer resultado = digito * multiplicador;
			String resultadoStr = String.valueOf(resultado);
			for (int j = 0; j < resultadoStr.length(); j++) {
				valorResultado = valorResultado + Integer.parseInt(resultadoStr.substring(j, j + 1));
			}
			if (multiplicador == MULTIPLICADOR) {
				multiplicador = 1;
			} else {
				multiplicador = MULTIPLICADOR;
			}
			posicao++;
		}

		Integer restoDiv = valorResultado % BigDecimal.TEN.intValue();
		if (restoDiv != 0) {
			dac = BigDecimal.TEN.intValue() - restoDiv;
		}
		return dac;
	}
	
	/**
	 * Efetua o cálculo do dígito de auto conferência do código de barras. Este
	 * cálculo só pode ser realizado após o preenchimento de todos os campos
	 * envolvidos, o que, para o caso do dígito de auto conferência do código de
	 * barras, é o código de barras inteiro menos o próprio dígito.
	 *
	 * @param numero
	 *            a composiçao de todos os campos do código de barras menos o
	 *            próprio dígito, na ordem
	 * @return o dígito resultante
	 */
	public static final Integer modulo11(final String numero) {
		String aux = numero;
		int param = PARAMETRO;
		int soma = 0;

		for (int ind = aux.length() - 1; ind >= 0; ind--) {
			if (param > TAMANHO_MAX_PARAMETRO) {
				param = PARAMETRO;
			}
			soma = soma + (Integer.parseInt(aux.substring(ind, ind + 1)) * param);
			param = param + 1;
		}

		int resto = soma % Constantes.DIVISOR_MODULO11;
		int digito;

		if ((resto == 0) || (resto == 1)) {
			digito = 0;
		} else {
			digito = Constantes.DIVISOR_MODULO11 - resto;
		}
		return digito;
	}
	
}
