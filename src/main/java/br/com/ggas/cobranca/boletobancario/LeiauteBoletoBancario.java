/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario;

import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoleto;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoletoBancoDoBrasil;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoletoCaixa;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoletoDaycoval;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoletoItau;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoletoSantander;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.NegocioException;

/**
 * Representa a coleção de leiautes disponíveis para geração de
 * {@link ConstrutorRelatorioBoleto}.
 */
enum LeiauteBoletoBancario {

	BB {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoBancoDoBrasil(documentoCobranca);
		}
	},
	SANTANDER {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoSantander(documentoCobranca);
		}
	},
	CAIXA {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoCaixa(documentoCobranca);
		}
	},
	ITAU {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoItau(documentoCobranca);
		}
	},
	BN {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoSantander(documentoCobranca);
		}
	},
	BRADESCO {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoSantander(documentoCobranca);
		}
	},
	DAYCOVAL {
		@Override
		ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
			return new ConstrutorRelatorioBoletoDaycoval(documentoCobranca);
		}
	};
	
	/**
	 * Cria um {@link ConstrutorRelatorioBoleto} cujo banco está associado ao
	 * tipo da enumeração.
	 *
	 * @param documentoCobranca o {@link DocumentoCobranca}
	 * @return o {@link ConstrutorRelatorioBoleto} do banco selecionado
	 * @throws NegocioException 
	 */
	abstract ConstrutorRelatorioBoleto construtorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException;
	
}
