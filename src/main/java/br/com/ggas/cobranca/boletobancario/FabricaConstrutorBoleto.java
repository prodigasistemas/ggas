/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario;

import com.google.common.collect.ImmutableMap;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoleto;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Esta classe é responsável pela seleção do leiaute usado no boleto bancário.
 * Os leiautes disponíveis estão definidos estaticamente, bem como em
 * {@link LeiauteBoletoBancario}. Para a adição de um novo leiaute, considerar:
 * <ul>
 * <li>adicionar um tipo em {@link LeiauteBoletoBancario};</li>
 * <li>criar uma classe que estenda de ConstrutorRelatorioBoleto</li>
 * <li>adicionar uma entrada no registro interno desta classe</li>
 * </ul>
 * Para mais detalhes consultar a documentação oficial disponibilizada pela
 * Febraban ou pelos bancos.
 */
public final class FabricaConstrutorBoleto {

	private static final ImmutableMap<String, LeiauteBoletoBancario> LEIAUTES = leiautesDisponiveis();
	
	private FabricaConstrutorBoleto() {
	}

	private static final ImmutableMap<String, LeiauteBoletoBancario> leiautesDisponiveis() {
		return ImmutableMap.<String, LeiauteBoletoBancario>builder()
				.put("001", LeiauteBoletoBancario.BB)
				.put("033", LeiauteBoletoBancario.SANTANDER)
				.put("341", LeiauteBoletoBancario.ITAU)
				.put("104", LeiauteBoletoBancario.CAIXA)
				.put("237", LeiauteBoletoBancario.BRADESCO)
				.put("004", LeiauteBoletoBancario.BN)
				.put("707", LeiauteBoletoBancario.DAYCOVAL)
				.build();
	}
	
	/**
	 * Gera um novo {@link ConstrutorRelatorioBoleto} com os dados passados como
	 * argumento.
	 *
	 * @param documentoCobranca o {@link DocumentoCobranca}
	 * @return o código de barras preenchido
	 * @throws NegocioException the negocio exception
	 */
	public static final ConstrutorRelatorioBoleto novoConstrutorBoleto(DocumentoCobranca documentoCobranca) throws NegocioException {
		return LEIAUTES
				.get(convenioParaBoleto(documentoCobranca)
						.getArrecadadorContrato()
						.getArrecadador()
						.getBanco()
						.getCodigoBanco())
				.construtorBoleto(documentoCobranca);
	}
	
	/**
	 * Convenio para boleto.
	 *
	 * @param documentoCobranca the documento cobranca
	 * @return the arrecadador contrato convenio
	 */
	public static ArrecadadorContratoConvenio convenioParaBoleto(DocumentoCobranca documentoCobranca) {
		ArrecadadorContratoConvenio arrecadadorConvenioPadrao = convenioPadrao();
		if (documentoCobranca != null && documentoCobranca.getChavePrimaria() > 0) {
			ArrecadadorContratoConvenio arrecadadorContratoConvenio = ServiceLocator.getInstancia()
					.getControladorArrecadadorConvenio()
					.obterArrecadadorContratoConvenioPorDocumentoCobranca(documentoCobranca);

			if(arrecadadorContratoConvenio != null){
				return Util.coalescenciaNula(arrecadadorContratoConvenio, arrecadadorConvenioPadrao);
			}else{
				return arrecadadorConvenioPadrao;
			}
		}
		return arrecadadorConvenioPadrao;
	}

	/**
	 * Convenio padrao.
	 *
	 * @return the arrecadador contrato convenio
	 */
	private static ArrecadadorContratoConvenio convenioPadrao() {
		return ServiceLocator
				.getInstancia()
				.getControladorArrecadadorConvenio()
				.obterArrecadadorContratoConvenioPadrao();
	}
	
}
