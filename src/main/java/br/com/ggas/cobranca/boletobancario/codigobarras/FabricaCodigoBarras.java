/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario.codigobarras;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import br.com.ggas.cobranca.boletobancario.codigobarras.dados.ChaveLeiauteCodigoBarras;
import br.com.ggas.cobranca.boletobancario.codigobarras.dados.DadosCodigoBarras;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Esta classe é responsável pela seleção do leiaute usado no código de barras.
 * Os leiautes disponíveis estão definidos estaticamente, bem como em {@link LeiauteCodigoBarras}. Para a adição de um novo leiaute, considerar:
 * <ul>
 * <li>adicionar um tipo em {@link LeiauteCodigoBarras};</li>
 * <li>criar uma classe que estenda de CodigoBarrasFebraban</li>
 * <li>adicionar uma entrada no registro interno desta classe</li>
 * </ul>
 * Para mais detalhes consultar a documentação oficial disponibilizada pela
 * Febraban ou pelos bancos.
 **/
public final class FabricaCodigoBarras {

	private static final String BANCO_ARRECADACAO = "001";

	private static final String CAIXA = "104";

	private static final String ITAU = "341";

	private static final String BANCO_DO_BRASIL = "001";
	
	private static final String SANTANDER = "033";
	
	private static final String DAYCOVAL = "707";

	private static final String BANCO_NORDESTE = "004";

	private static final String BANCO_BRADESCO = "237";

	private static final String ARRECADACAO = "Arrecadação";

	private static final String BOLETO_BANCARIO = "Boleto Bancário";

	private static final String DEBITO_AUTOMATICO = "Débito Automático";

	public static final ImmutableMap<ChaveLeiauteCodigoBarras, LeiauteCodigoBarras> LEIAUTES = leiautesDisponiveis();

	private FabricaCodigoBarras() {

	}

	
	/**
	 * @return
	 */
	private static final ImmutableMap<ChaveLeiauteCodigoBarras, LeiauteCodigoBarras> leiautesDisponiveis() {

		return ImmutableMap.<ChaveLeiauteCodigoBarras, LeiauteCodigoBarras> builder()
						.put(new ChaveLeiauteCodigoBarras(BANCO_DO_BRASIL, BOLETO_BANCARIO), LeiauteCodigoBarras.BB7POSICOES)
						.put(new ChaveLeiauteCodigoBarras(BANCO_DO_BRASIL, DEBITO_AUTOMATICO), LeiauteCodigoBarras.BB7POSICOES)
						.put(new ChaveLeiauteCodigoBarras(ITAU, BOLETO_BANCARIO), LeiauteCodigoBarras.ITAU)
						.put(new ChaveLeiauteCodigoBarras(ITAU, DEBITO_AUTOMATICO), LeiauteCodigoBarras.ITAU)
						.put(new ChaveLeiauteCodigoBarras(DAYCOVAL, BOLETO_BANCARIO), LeiauteCodigoBarras.DAYCOVAL)
				        .put(new ChaveLeiauteCodigoBarras(DAYCOVAL, DEBITO_AUTOMATICO), LeiauteCodigoBarras.DAYCOVAL)
						.put(new ChaveLeiauteCodigoBarras(CAIXA, BOLETO_BANCARIO), LeiauteCodigoBarras.CAIXA)
				        .put(new ChaveLeiauteCodigoBarras(CAIXA, DEBITO_AUTOMATICO), LeiauteCodigoBarras.CAIXA)
						.put(new ChaveLeiauteCodigoBarras(BANCO_NORDESTE, BOLETO_BANCARIO), LeiauteCodigoBarras.BANCO_NORDESTE)
				        .put(new ChaveLeiauteCodigoBarras(BANCO_NORDESTE, DEBITO_AUTOMATICO), LeiauteCodigoBarras.BANCO_NORDESTE)
						.put(new ChaveLeiauteCodigoBarras(BANCO_BRADESCO, DEBITO_AUTOMATICO), LeiauteCodigoBarras.BANCO_BRADESCO)
				        .put(new ChaveLeiauteCodigoBarras(BANCO_BRADESCO, BOLETO_BANCARIO), LeiauteCodigoBarras.BANCO_BRADESCO)
						.put(new ChaveLeiauteCodigoBarras(SANTANDER, BOLETO_BANCARIO), LeiauteCodigoBarras.SANTANDER)
				        .put(new ChaveLeiauteCodigoBarras(SANTANDER, DEBITO_AUTOMATICO), LeiauteCodigoBarras.SANTANDER)
						.put(new ChaveLeiauteCodigoBarras(BANCO_ARRECADACAO, ARRECADACAO), LeiauteCodigoBarras.ARRECADACAO).build();
	}

	/**
	 * Gera um novo {@link CodigoBarras} já preenchido com os dados passados como
	 * argumento.
	 *
	 * @param dados
	 *            os dados usados no preenchimento do {@link CodigoBarras}
	 * @return o código de barras preenchido
	 * @throws NegocioException the negocio exception
	 */
	public static final CodigoBarras novoCodigoBarras(DadosCodigoBarras dados) throws NegocioException {

		// validacao temporaria, essa validacao sera feita preventivamente
		if (!LEIAUTES.containsKey(dados.chaveLeiaute())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_BANCO_CODIGO_BARRAS_NAO_IMPLEMENTADO, dados.chaveLeiaute());
		}


		return LEIAUTES.get(dados.chaveLeiaute()).codigoBarras(dados);
	}

	/**
	 * Gera uma string separados com virgula com os bancos suportados pelo sistema.
	 *
	 * @return os codigos dos bancos suportados pelo sistema
	 */
	public static final String obterCodigoBancosSuportados() {

		StringBuilder codigoBancosSuportados = new StringBuilder();
		ImmutableSet<ChaveLeiauteCodigoBarras> bancos = FabricaCodigoBarras.LEIAUTES.keySet();

		int cont = 0;
		for (ChaveLeiauteCodigoBarras banco : bancos) {
			cont++;
			if (banco.getCodigoBanco()!=null) {
				codigoBancosSuportados.append(banco.getCodigoBanco());
			}
			if (cont < bancos.size()) {
				codigoBancosSuportados.append(", ");
			}
		}

		return codigoBancosSuportados.toString();
	}
}
