/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario.codigobarras;

/**
 * A representação numérica/linha digitável exibe algumas seções do
 * {@link CodigoBarras} organizadas de outra forma. Para mais detalhes consultar
 * a documentação oficial disponibilizada pela Febraban ou pelos bancos.
 **/
public class RepresentacaoNumerica {

	private static final int FIM_QUINTO_CAMPO = 19;
	private static final int INICIO_QUINTO_CAMPO = 5;
	private static final int FIM_TERCEIRO_CAMPO = 44;
	private static final int INICIO_TERCEIRO_CAMPO = 34;
	private static final int FIM_SEGUNDO_CAMPO = 34;
	private static final int INICIO_SEGUNDO_CAMPO = 24;
	private static final int FIM_SEGUNDA_PARTE_PRIMEIRO_CAMPO = 24;
	private static final int INICIO_SEGUNDA_PARTE_PRIMEIRO_CAMPO = 19;
	private static final int FIM_PRIMEIRA_PARTE_PRIMEIRO_CAMPO = 4;
	private CodigoBarras codigoBarras;
	private static final char SEPARADOR = '.';
	private static final char ESPACO = ' ';
	private static final int POSICAO_SEPARADOR = 5;
	private static final int POSICAO_AUTO_CONFERENCIA = 4;
	
	/**
	 * Cria uma representação numérica usando como base o {@link CodigoBarras}
	 * passado como parâmetro.
	 *
	 * @param codigoBarras
	 *            o {@link CodigoBarras} preenchido
	 */
	public RepresentacaoNumerica(CodigoBarras codigoBarras) {
		this.codigoBarras = codigoBarras;
	}
	
	/**
	 * Gera o primeiro campo da representação numérica a partir do
	 * {@link CodigoBarras} passado no construtor desta instância.
	 *
	 * @return o primeiro campo
	 */
	public String primeiroCampo() {
		StringBuilder construtor = new StringBuilder();
		return construtor.append(this.codigoBarras.toString().substring(0, FIM_PRIMEIRA_PARTE_PRIMEIRO_CAMPO))
				.append(this.codigoBarras.toString().substring(INICIO_SEGUNDA_PARTE_PRIMEIRO_CAMPO,
						FIM_SEGUNDA_PARTE_PRIMEIRO_CAMPO))
				.append(DigitoAutoConferencia.modulo10(construtor.toString())).insert(POSICAO_SEPARADOR, SEPARADOR)
				.toString();
	}
	
	/**
	 * Gera o segundo campo da representação numérica a partir do
	 * {@link CodigoBarras} passado no construtor desta instância.
	 *
	 * @return o segundo campo
	 */
	public String segundoCampo() {
		StringBuilder construtor = new StringBuilder();
		return construtor
			.append(this.codigoBarras.toString().substring(INICIO_SEGUNDO_CAMPO, FIM_SEGUNDO_CAMPO))
			.append(DigitoAutoConferencia.modulo10(construtor.toString()))
			.insert(POSICAO_SEPARADOR, SEPARADOR)
			.toString();
	}
	
	/**
	 * Gera o terceiro campo da representação numérica a partir do
	 * {@link CodigoBarras} passado no construtor desta instância.
	 *
	 * @return o terceiro campo
	 */
	public String terceiroCampo() {
		StringBuilder construtor = new StringBuilder();
		return construtor
			.append(this.codigoBarras.toString().substring(INICIO_TERCEIRO_CAMPO, FIM_TERCEIRO_CAMPO))
			.append(DigitoAutoConferencia.modulo10(construtor.toString()))
			.insert(POSICAO_SEPARADOR, SEPARADOR)
			.toString();
	}
	
	/**
	 * Gera o quarto campo da representação numérica a partir do
	 * {@link CodigoBarras} passado no construtor desta instância.
	 *
	 * @return o quarto campo
	 */
	public String quartoCampo() {
		return String.valueOf(
			this.codigoBarras
				.toString()
				.charAt(POSICAO_AUTO_CONFERENCIA));
	}
	
	/**
	 * Gera o quinto campo da representação numérica a partir do
	 * {@link CodigoBarras} passado no construtor desta instância.
	 *
	 * @return o quinto campo
	 */
	public String quintoCampo() {
		return this.codigoBarras.toString().substring(INICIO_QUINTO_CAMPO, FIM_QUINTO_CAMPO);
	}
	/**
	 * converte resultado para String
	 */
	@Override
	public String toString() {
		return new StringBuilder()
			.append(primeiroCampo())
			.append(ESPACO)
			.append(segundoCampo())
			.append(ESPACO)
			.append(terceiroCampo())
			.append(ESPACO)
			.append(quartoCampo())
			.append(ESPACO)
			.append(quintoCampo())
			.toString();
	}

}
