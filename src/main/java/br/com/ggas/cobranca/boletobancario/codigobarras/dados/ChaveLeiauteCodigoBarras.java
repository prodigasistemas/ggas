/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario.codigobarras.dados;

/**
 * A Classe ChaveLeiauteCodigoBarras.
 */
public class ChaveLeiauteCodigoBarras {

	private String codigoBanco;

	private String tipoConvenio;
	
	private static final String ARRECADACAO = "Arrecadação";
	
	/**
	 * Instantiates a new chave leiaute codigo barras.
	 *
	 * @param codigoBanco the codigo banco
	 * @param tipoConvenio the tipo convenio
	 */
	public ChaveLeiauteCodigoBarras(String codigoBanco, String tipoConvenio) {
		this.codigoBanco = codigoBanco;
		this.tipoConvenio = tipoConvenio;
	}
	
	public String getCodigoBanco() {
		
		return codigoBanco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoBanco == null) ? 0 : codigoBanco.hashCode());
		result = prime * result + ((tipoConvenio == null) ? 0 : tipoConvenio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ChaveLeiauteCodigoBarras other = (ChaveLeiauteCodigoBarras) obj;
		if (tipoConvenio == null) {
			if (other.tipoConvenio != null) {
				return false;
			}
		} else if (!tipoConvenio.equals(other.tipoConvenio)) {
			return false;
		}
		if (tipoConvenio != null && !tipoConvenio.equals(ARRECADACAO)) {
			if (codigoBanco == null) {
				if (other.codigoBanco != null) {
					return false;
				}
			} else if (!codigoBanco.equals(other.codigoBanco)) {
				return false;
			}
		}
		return true;
	}
	
}
