/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.boletobancario.codigobarras.dados;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.ReadableInstant;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.cobranca.boletobancario.codigobarras.CodigoBarras;
import br.com.ggas.cobranca.boletobancario.codigobarras.excecoes.ConvenioDesatualizado;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.DadosAuditoriaUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Esta classe é responsável por extrair todos os dados necessários para geração
 * de {@link CodigoBarras}, para todos os leiautes. Esta classe não garante
 * tratamentos para informação incompleta.
 */
public class DadosPadraoCodigoBarras implements DadosCodigoBarras {

	private static final String COMPLEMENTO = "";
	private static final char MOEDA = '9';
	private static final ReadableInstant DATA_BASE_VENCIMENTO = new DateTime(1997, 10, 07, 0, 0, 0, 0);
	private final DocumentoCobranca cobranca;
	private final ArrecadadorContratoConvenio convenio;
	
	/**
	 * Cria uma nova instância desta classe, usando um {@link DocumentoCobranca}.
	 * Esta classe não garante tratamentos para informação incompleta.
	 *
	 * @param cobranca
	 *            o {@link DocumentoCobranca} contendo os dados necessários
	 */
	public DadosPadraoCodigoBarras(DocumentoCobranca cobranca) {
		this.cobranca = cobranca;
		this.convenio = convenioParaBoleto();
	}

	@Override
	public ChaveLeiauteCodigoBarras chaveLeiaute() {
		return new ChaveLeiauteCodigoBarras(
				banco(),
				this.convenio.getTipoConvenio().getDescricao());
	}

	@Override
	public String banco() {
		return this.convenio
				.getContaConvenio()
				.getAgencia()
				.getBanco()
				.getCodigoBanco();
	}

	@Override
	public String moeda() {
		return String.valueOf(MOEDA);
	}

	@Override
	public String convenio() {
		return this.convenio.getCodigoConvenio();
	}

	@Override
	public String vencimento() {
		return String.valueOf(
				Days.daysBetween(
						DATA_BASE_VENCIMENTO, 
						new DateTime(this.cobranca.getDataVencimento()))
				.getDays());
	}

	@Override
	public String valor() {
		BigDecimal valor = this.cobranca.getValorTotal();
		return valor.movePointRight(
				valor.scale())
				.toString();
	}

	@Override
	public String agencia() {
		return this.convenio
				.getContaConvenio()
				.getAgencia()
				.getCodigo();
	}

	@Override
	public String conta() {
		return this.convenio
				.getContaConvenio()
				.getNumeroConta();
	}

	@Override
	public String carteira() {
		return this.convenio
				.getArrecadadorCarteiraCobranca()
				.getNumero()
				.toString();
	}

	@Override
	public String nossoNumero() {
		Long nossoNumero = this.cobranca
				.getNossoNumero();
		if(nossoNumero == null) {
			try {
				nossoNumero = sequencialDaFaixa();
			} catch (NegocioException e) {
				throw new ConvenioDesatualizado(e);
			}
		}
		return nossoNumero.toString();
	}

	@Override
	public String complemento() {
		return COMPLEMENTO;
	}

	@Override
	public DocumentoCobranca documentoCobranca() {
		return this.cobranca;
	}

	@Override
	public ArrecadadorContratoConvenio arrecadadorContratoConvenio() {
		return this.convenio;
	}
	
	private Long sequencialDaFaixa() throws NegocioException {
		return ServiceLocator
				.getInstancia()
				.getControladorArrecadacao()
				.calcularNossoNumeroComValidacao(
						this.convenio,
						DadosAuditoriaUtil.getDadosAuditoria());
	}
	
	private ArrecadadorContratoConvenio convenioParaBoleto() {
		ArrecadadorContratoConvenio arrecadadorConvenioPadrao = convenioPadrao();
		if (cobranca != null && cobranca.getChavePrimaria() > 0) {
			ArrecadadorContratoConvenio arrecadadorContratoConvenio = ServiceLocator.getInstancia()
					.getControladorArrecadadorConvenio()
					.obterArrecadadorContratoConvenioPorDocumentoCobranca(cobranca);

			if(arrecadadorContratoConvenio != null){
				return Util.coalescenciaNula(arrecadadorContratoConvenio, arrecadadorConvenioPadrao);
			}else{
				return arrecadadorConvenioPadrao;
			}


		}
		return arrecadadorConvenioPadrao;
	}

	private ArrecadadorContratoConvenio convenioPadrao() {
		return ServiceLocator
				.getInstancia()
				.getControladorArrecadadorConvenio()
				.obterArrecadadorContratoConvenioPadrao();
	}

}
