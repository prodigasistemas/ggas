/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe <nome> representa uma <nome> no sistema.
 *
 * @since data atual
 * 
 */

package br.com.ggas.cobranca.perfilparcelamento.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSegmento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSituacaoPontoConsumo;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelo Perfil de parcelamento
 *
 */
public class ParcelamentoPerfilImpl extends EntidadeNegocioImpl implements ParcelamentoPerfil {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5373907084128366823L;

	private EntidadeConteudo tipoTabela;

	private EntidadeConteudo tipoValor;

	private String descricao;

	private String descricaoAbreviada;

	private Integer maximoParcelas;

	private boolean indicadorDesconto;

	private BigDecimal valorDesconto;

	private boolean indicadorJuros;

	private BigDecimal taxaJuros;

	private Date vigenciaInicial;

	private Date vigenciaFinal;

	private boolean indicadorCorrecaoMonetaria;

	private IndiceFinanceiro indiceFinanceiro;

	private Collection<ParcelamentoPerfilSegmento> listaParcPerfilSegmento = new HashSet<>();

	private Collection<ParcelamentoPerfilSituacaoPontoConsumo> listaParcPerfilSituacaoPontoConsumo = new HashSet<>();

	/**
	 * @return the vigenciaInicial
	 */
	@Override
	public Date getVigenciaInicial() {

		return vigenciaInicial;
	}

	/**
	 * @param vigenciaInicial
	 *            the vigenciaInicial to set
	 */
	@Override
	public void setVigenciaInicial(Date vigenciaInicial) {

		this.vigenciaInicial = vigenciaInicial;
	}

	/**
	 * @return the vigenciaFinal
	 */
	@Override
	public Date getVigenciaFinal() {

		return vigenciaFinal;
	}

	/**
	 * @param vigenciaFinal
	 *            the vigenciaFinal to set
	 */
	@Override
	public void setVigenciaFinal(Date vigenciaFinal) {

		this.vigenciaFinal = vigenciaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.perfilparcelamento
	 * .impl.ParcelamentoPerfil#getTipoTabela()
	 */
	@Override
	public EntidadeConteudo getTipoTabela() {

		return tipoTabela;
	}

	/*
	 * (non-Javadoc) * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setTipoTabela(br.com .ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoTabela(EntidadeConteudo tipoTabela) {

		this.tipoTabela = tipoTabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.perfilparcelamento
	 * .impl.ParcelamentoPerfil#getTipoValor()
	 */
	@Override
	public EntidadeConteudo getTipoValor() {

		return tipoValor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setTipoValor(br.com .ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoValor(EntidadeConteudo tipoValor) {

		this.tipoValor = tipoValor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento
	 * .impl.ParcelamentoPerfil#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setDescricao(java.lang .String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setDescricaoAbreviada (java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#getMaximoParcelas()
	 */
	@Override
	public Integer getMaximoParcelas() {

		return maximoParcelas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setMaximoParcelas(java .lang.Integer)
	 */
	@Override
	public void setMaximoParcelas(Integer maximoParcelas) {

		this.maximoParcelas = maximoParcelas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#isIndicadorDesconto()
	 */
	@Override
	public boolean isIndicadorDesconto() {

		return indicadorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setIndicadorDesconto (boolean)
	 */
	@Override
	public void setIndicadorDesconto(boolean indicadorDesconto) {

		this.indicadorDesconto = indicadorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento
	 * .impl.ParcelamentoPerfil#getValorDesconto()
	 */
	@Override
	public BigDecimal getValorDesconto() {

		return valorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setValorDesconto(java .math.BigDecimal)
	 */
	@Override
	public void setValorDesconto(BigDecimal valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento
	 * .impl.ParcelamentoPerfil#isIndicadorJuros()
	 */
	@Override
	public boolean isIndicadorJuros() {

		return indicadorJuros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setIndicadorJuros(boolean )
	 */
	@Override
	public void setIndicadorJuros(boolean indicadorJuros) {

		this.indicadorJuros = indicadorJuros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento
	 * .impl.ParcelamentoPerfil#getTaxaJuros()
	 */
	@Override
	public BigDecimal getTaxaJuros() {

		return taxaJuros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.perfilparcelamento .impl
	 * .ParcelamentoPerfil#setTaxaJuros(java.math .BigDecimal)
	 */
	@Override
	public void setTaxaJuros(BigDecimal taxaJuros) {

		this.taxaJuros = taxaJuros;
	}

	/**
	 * @return the listaParcPerfilSituacaoPontoConsumo
	 */
	@Override
	public Collection<ParcelamentoPerfilSituacaoPontoConsumo> getListaParcPerfilSituacaoPontoConsumo() {

		return listaParcPerfilSituacaoPontoConsumo;
	}

	/**
	 * @param listaParcPerfilSituacaoPontoConsumo
	 *            the listaParcPerfilSituacaoPontoConsumo to set
	 */
	@Override
	public void setListaParcPerfilSituacaoPontoConsumo(
			Collection<ParcelamentoPerfilSituacaoPontoConsumo> listaParcPerfilSituacaoPontoConsumo) {

		this.listaParcPerfilSituacaoPontoConsumo = listaParcPerfilSituacaoPontoConsumo;
	}

	/**
	 * @return the listaParcPerfilSegmento
	 */
	@Override
	public Collection<ParcelamentoPerfilSegmento> getListaParcPerfilSegmento() {

		return listaParcPerfilSegmento;
	}

	/**
	 * @param listaParcPerfilSegmento
	 *            the listaParcPerfilSegmento to set
	 */
	@Override
	public void setListaParcPerfilSegmento(Collection<ParcelamentoPerfilSegmento> listaParcPerfilSegmento) {

		this.listaParcPerfilSegmento = listaParcPerfilSegmento;
	}

	@Override
	public boolean isIndicadorCorrecaoMonetaria() {

		return indicadorCorrecaoMonetaria;
	}

	@Override
	public void setIndicadorCorrecaoMonetaria(boolean indicadorCorrecaoMonetaria) {

		this.indicadorCorrecaoMonetaria = indicadorCorrecaoMonetaria;
	}

	@Override
	public IndiceFinanceiro getIndiceFinanceiro() {

		return indiceFinanceiro;
	}

	@Override
	public void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro) {

		this.indiceFinanceiro = indiceFinanceiro;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.comum.negocio.impl. EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(descricaoAbreviada)) {
			stringBuilder.append(DESCRICAO_ABREVIADA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (maximoParcelas == null) {
			stringBuilder.append(MAXIMO_PARCELAS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (indicadorDesconto && valorDesconto == null) {
			stringBuilder.append(DESCONTO_MAXIMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (indicadorJuros && taxaJuros == null) {
			stringBuilder.append(TAXA_JUROS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (listaParcPerfilSegmento == null || listaParcPerfilSegmento.isEmpty()) {
			stringBuilder.append(LISTA_PARC_PERFIL_SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (listaParcPerfilSituacaoPontoConsumo == null || listaParcPerfilSituacaoPontoConsumo.isEmpty()) {
			stringBuilder.append(LISTA_PARC_PERFIL_SITUACAO_PONTO_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (vigenciaInicial == null) {
			stringBuilder.append(VIGENCIA_INICIAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
