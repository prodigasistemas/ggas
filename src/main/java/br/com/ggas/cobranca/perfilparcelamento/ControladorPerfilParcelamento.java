/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.perfilparcelamento;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável assinatura dos métodos relacionados ao Controlador do perfil de parcelamento.
 *
 */
public interface ControladorPerfilParcelamento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PERFIL_PARCELAMENTO = "controladorPerfilParcelamento";

	String ERRO_NEGOCIO_TIPO_VALOR = "ERRO_NEGOCIO_TIPO_VALOR";

	String ERRO_NEGOCIO_PERFIL_PARCELAMENTO_EXISTENTE = "ERRO_NEGOCIO_PERFIL_PARCELAMENTO_EXISTENTE";

	String ERRO_NUMERO_MAXIMO_PARCELAS = "ERRO_NUMERO_MAXIMO_PARCELAS";

	String ERRO_NEGOCIO_DATA_VIGENCIA_INTERVALO_INVALIDO = "ERRO_NEGOCIO_DATA_VIGENCIA_INTERVALO_INVALIDO";

	String ERRO_NEGOCIO_DESCONTO_SUPERIOR_INVALIDO = "ERRO_NEGOCIO_DESCONTO_SUPERIOR_INVALIDO";

	String ERRO_NEGOCIO_DESCONTO_INFERIOR_INVALIDO = "ERRO_NEGOCIO_DESCONTO_INFERIOR_INVALIDO";

	String ERRO_NEGOCIO_JUROS_MENOR_INVALIDO = "ERRO_NEGOCIO_JUROS_MENOR_INVALIDO";

	String ERRO_NEGOCIO_JUROS_MAIOR_INVALIDO = "ERRO_NEGOCIO_JUROS_MAIOR_INVALIDO";

	String ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA = "ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA";

	String ERRO_NEGOCIO_DADOS_SEM_ALTERACAO = "ERRO_NEGOCIO_DADOS_SEM_ALTERACAO";

	String ERRO_NEGOCIO_PERFIL_VINCULADO_PARCELAMENTO = "ERRO_NEGOCIO_PERFIL_VINCULADO_PARCELAMENTO";

	String NEGOCIO_DATA_VIGENTE = "NEGOCIO_DATA_VIGENTE";

	String RELATORIO_INSTRUMENTO_CONFISSAO_DIVIDAS = "relatorioConfissaoDividas.jasper";

	String RELATORIO_NOTA_PROMISSORIA = "relatorioNotaPromissoria.jasper";

	/**
	 * Criar parcelamento perfil segmento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarParcelamentoPerfilSegmento();

	/**
	 * Criar parcelamento perfil situacao ponto consumo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarParcelamentoPerfilSituacaoPontoConsumo();

	/**
	 * Método responsável por consultar o perfil
	 * do parcelamento através do filtro
	 * informado.
	 * 
	 * @param filtro
	 *            Mapa contendo os filtros
	 * @return lista de perfis de parcelamento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ParcelamentoPerfil> consultarPerfilParcelamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por remover os perfis de
	 * parcelamento selecionados.
	 * 
	 * @param chavesPrimarias
	 *            As chaves dos perfis a serem
	 *            excluídos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerPerfisParcelamento(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por comparar duas datas
	 * de vigencia incial de perfil parcelamento,
	 * para validação de alteração.
	 * 
	 * @param dataVigenciaTela
	 *            the data vigencia tela
	 * @param dataVigenciaBanco
	 *            the data vigencia banco
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDataVigenciaInicialPerfilParcelamento(Date dataVigenciaTela, Date dataVigenciaBanco) throws NegocioException;

	/**
	 * Método responsável por verificar se a
	 * vigencia final do perfil parcelamento já
	 * foi alterado em um perfil já vinculado.
	 * 
	 * @param dataVigenciaFinal
	 *            the data vigencia final
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarPerfilParcelamentoVigenciaFinal(Date dataVigenciaFinal) throws NegocioException;

	/**
	 * Obter segmento perfil parcelamento.
	 * 
	 * @param idPerfilParcelamento
	 *            the id perfil parcelamento
	 * @param idSegmento
	 *            the id segmento
	 * @return the parcelamento perfil segmento
	 */
	ParcelamentoPerfilSegmento obterSegmentoPerfilParcelamento(Long idPerfilParcelamento, Long idSegmento);

	/**
	 * Obter situacao consumo perfil parcelamento.
	 * 
	 * @param idPerfilParcelamento
	 *            the id perfil parcelamento
	 * @param idSituacaoConsumo
	 *            the id situacao consumo
	 * @return the parcelamento perfil situacao ponto consumo
	 */
	ParcelamentoPerfilSituacaoPontoConsumo obterSituacaoConsumoPerfilParcelamento(Long idPerfilParcelamento, Long idSituacaoConsumo);

}
