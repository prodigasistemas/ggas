/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe <nome> representa uma <nome> no sistema.
 *
 * @since data atual
 *
 */

package br.com.ggas.cobranca.perfilparcelamento.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSegmento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfilSituacaoPontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 *
 */
class ControladorPerfilParcelamentoImpl extends ControladorNegocioImpl implements ControladorPerfilParcelamento {


	private static final String MAXIMO_DESCONTO = "100";

	private static final String MINIMO_DESCONTO = "0";

	private static final String MINIMO_JUROS = "0";

	private static final String MAXIMO_JUROS = "100";
	
	private static final String CAMPO_VIGENCIA_INICIAL = "vigenciaInicial";
	
	private static final String CAMPO_VIGENCIA_FINAL = "vigenciaFinal";
	
	private static final String CAMPO_PARCELAMENTO_PERFIL = "parcelamentoPerfil"; 
	
	private static final String CAMPO_SEGMENTO = "segmento";
	

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ParcelamentoPerfil.BEAN_ID_PARCELAMENTO_PERFIL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento#criarParcelamentoPerfilSegmento()
	 */
	@Override
	public EntidadeNegocio criarParcelamentoPerfilSegmento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia()
						.getBeanPorID(ParcelamentoPerfilSegmento.BEAN_ID_PARCELAMENTO_PERFIL_SEGMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento#criarParcelamentoPerfilSituacaoPontoConsumo()
	 */
	@Override
	public EntidadeNegocio criarParcelamentoPerfilSituacaoPontoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ParcelamentoPerfilSituacaoPontoConsumo.BEAN_ID_PARCELAMENTO_PERFIL_SEGMENTO_PONTO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ParcelamentoPerfilImpl.BEAN_ID_PARCELAMENTO_PERFIL);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.perfilparcelamento.
	 * ControladorPerfilParcelamento#
	 * consultarPerfilParcelamento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ParcelamentoPerfil> consultarPerfilParcelamento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			String descricaoAbreviada = (String) filtro.get("descricaoAbreviada");
			if(!StringUtils.isEmpty(descricaoAbreviada)) {
				criteria.add(Restrictions.ilike("descricaoAbreviada", Util.formatarTextoConsulta(descricaoAbreviada)));
			}

			Long idSegmento = (Long) filtro.get("idSegmento");
			if(idSegmento != null && idSegmento > 0) {
				criteria.createCriteria("listaParcPerfilSegmento").add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

			criteria.createCriteria("tipoTabela", "tipoTabela", Criteria.LEFT_JOIN);

			Date vigenciaInicial = (Date) filtro.get(CAMPO_VIGENCIA_INICIAL);
			Date vigenciaFinal = (Date) filtro.get(CAMPO_VIGENCIA_FINAL);

			if(vigenciaInicial != null && vigenciaFinal == null) {
				criteria.add(Restrictions.eq(CAMPO_VIGENCIA_INICIAL, vigenciaInicial));
			} else if(vigenciaInicial == null && vigenciaFinal != null) {
				criteria.add(Restrictions.eq(CAMPO_VIGENCIA_FINAL, vigenciaFinal));
			} else if(vigenciaInicial != null && vigenciaFinal != null) {
				criteria.add(Restrictions.ge(CAMPO_VIGENCIA_INICIAL, vigenciaInicial));
				criteria.add(Restrictions.le(CAMPO_VIGENCIA_FINAL, vigenciaFinal));
			}

			Boolean exibirVencidos = (Boolean) filtro.get("exibirVencidos");
			if(exibirVencidos != null && !exibirVencidos) {

				criteria.add(Restrictions.or(Restrictions.ge(CAMPO_VIGENCIA_FINAL, DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime())),
								Restrictions.isNull(CAMPO_VIGENCIA_FINAL)));

			}

			criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		ParcelamentoPerfil perfil = (ParcelamentoPerfil) entidadeNegocio;
		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);

		Collection<Parcelamento> listaParcelamento = controladorParcelamento.consultarParcelamentosVinculadosPerfilParcelamento(perfil
						.getChavePrimaria());

		if(listaParcelamento != null && !listaParcelamento.isEmpty()) {
			throw new NegocioException(ControladorPerfilParcelamento.ERRO_NEGOCIO_PERFIL_VINCULADO_PARCELAMENTO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento#removerPerfisParcelamento(java.lang.Long[])
	 */
	@Override
	public void removerPerfisParcelamento(Long[] chavesPrimarias) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put("chavesPrimarias", chavesPrimarias);
			Collection<ParcelamentoPerfil> listaPerfil = this.consultarPerfilParcelamento(filtro);
			if((listaPerfil != null) && (!listaPerfil.isEmpty())) {
				for (ParcelamentoPerfil perfil : listaPerfil) {
					super.remover(perfil, false);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.perfilparcelamento.
	 * ControladorPerfilParcelamento
	 * #validarDataVigenciaInicialPerfilParcelamento
	 * (java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public void validarDataVigenciaInicialPerfilParcelamento(Date dataVigenciaTela, Date dataVigenciaBanco) throws NegocioException {

		Date dataSemHora = DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime());
		if(dataVigenciaTela == null || Util.compararDatas(dataVigenciaTela, dataVigenciaBanco) != 0 && dataVigenciaTela.before(dataSemHora)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.perfilparcelamento.
	 * ControladorPerfilParcelamento
	 * #validarPerfilParcelamentoVigenciaFinal(java
	 * .util.Date)
	 */
	@Override
	public void validarPerfilParcelamentoVigenciaFinal(Date dataVigenciaFinal) throws NegocioException {

		if(dataVigenciaFinal != null) {
			throw new NegocioException(ERRO_NEGOCIO_DADOS_SEM_ALTERACAO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		ParcelamentoPerfil parcelamentoPerfil = (ParcelamentoPerfil) entidadeNegocio;
		validarPerfilParcelamento(parcelamentoPerfil);
		validarPerfilParcelamentoExistente(parcelamentoPerfil);
		validarVigenciaInicial(parcelamentoPerfil);
		validarIntervaloVigencia(parcelamentoPerfil);
		validarPerfilParcelamentoConcedeDesconto(parcelamentoPerfil);
		validarPerfilParcelamentoJuros(parcelamentoPerfil);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		ParcelamentoPerfil parcelamentoPerfil = (ParcelamentoPerfil) entidadeNegocio;
		validarPerfilParcelamento(parcelamentoPerfil);
		validarPerfilParcelamentoExistente(parcelamentoPerfil);
		validarIntervaloVigencia(parcelamentoPerfil);
		validarPerfilParcelamentoConcedeDesconto(parcelamentoPerfil);
		validarPerfilParcelamentoJuros(parcelamentoPerfil);
	}

	/**
	 * Validar perfil parcelamento.
	 *
	 * @param parcelamentoPerfil
	 *            the parcelamento perfil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPerfilParcelamento(ParcelamentoPerfil parcelamentoPerfil) throws NegocioException {

		if (parcelamentoPerfil != null && parcelamentoPerfil.getMaximoParcelas() == 0) {
			throw new NegocioException(ERRO_NUMERO_MAXIMO_PARCELAS, true);
		}
	}

	/**
	 * Validar perfil parcelamento existente.
	 *
	 * @param parcelamentoPerfil the parcelamento perfil
	 * @throws NegocioException the negocio exception
	 */
	private void validarPerfilParcelamentoExistente(ParcelamentoPerfil parcelamentoPerfil) throws NegocioException {

		List<ParcelamentoPerfil> listaPerfil = this.consultarPerfilExistente(parcelamentoPerfil.getDescricao());
		if (!listaPerfil.isEmpty()) {
			ParcelamentoPerfil perfilExistente = listaPerfil.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(perfilExistente);

			if (perfilExistente.getChavePrimaria() != parcelamentoPerfil.getChavePrimaria()) {
				throw new NegocioException(ControladorPerfilParcelamento.ERRO_NEGOCIO_PERFIL_PARCELAMENTO_EXISTENTE,
						parcelamentoPerfil.getDescricao());
			}
		}
	}

	/**
	 * Validar perfil parcelamento concede desconto.
	 *
	 * @param parcelamentoPerfil
	 *            the parcelamento perfil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPerfilParcelamentoConcedeDesconto(ParcelamentoPerfil parcelamentoPerfil) throws NegocioException {

		if(parcelamentoPerfil.getValorDesconto() != null
						&& parcelamentoPerfil.getValorDesconto().compareTo(new BigDecimal(MAXIMO_DESCONTO)) > 0) {
			throw new NegocioException(ERRO_NEGOCIO_DESCONTO_SUPERIOR_INVALIDO, true);
		} else if(parcelamentoPerfil.getValorDesconto() != null
						&& parcelamentoPerfil.getValorDesconto().compareTo(new BigDecimal(MINIMO_DESCONTO)) < 0) {
			throw new NegocioException(ERRO_NEGOCIO_DESCONTO_INFERIOR_INVALIDO, true);
		}
	}

	/**
	 * Validar perfil parcelamento juros.
	 *
	 * @param parcelamentoPerfil
	 *            the parcelamento perfil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPerfilParcelamentoJuros(ParcelamentoPerfil parcelamentoPerfil) throws NegocioException {

		if(parcelamentoPerfil.getTaxaJuros() != null) {
			if(parcelamentoPerfil.getTaxaJuros().compareTo(new BigDecimal(MINIMO_JUROS)) < 0) {
				throw new NegocioException(ERRO_NEGOCIO_JUROS_MENOR_INVALIDO, true);
			} else if(parcelamentoPerfil.getTaxaJuros().compareTo(new BigDecimal(MAXIMO_JUROS)) > 0) {
				throw new NegocioException(ERRO_NEGOCIO_JUROS_MAIOR_INVALIDO, true);
			}
		}
	}

	/**
	 * Consultar perfil existente.
	 *
	 * @param descricao
	 *            the descricao
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	private List<ParcelamentoPerfil> consultarPerfilExistente(String descricao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidade WHERE ");
		hql.append(" LOWER(entidade.descricao) = :DESCRICAO ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("DESCRICAO", descricao.trim().toLowerCase());
		return query.list();
	}

	/**
	 * Validar intervalo vigencia.
	 *
	 * @param parcelamentoPerfil
	 *            the parcelamento perfil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarIntervaloVigencia(ParcelamentoPerfil parcelamentoPerfil) throws NegocioException {

		if((parcelamentoPerfil.getVigenciaInicial() != null && parcelamentoPerfil.getVigenciaFinal() != null)
						&& (parcelamentoPerfil.getVigenciaInicial().after(parcelamentoPerfil.getVigenciaFinal()))) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VIGENCIA_INTERVALO_INVALIDO, true);
		}

	}

	/**
	 * Validar vigencia inicial.
	 *
	 * @param parcelamentoPerfil
	 *            the parcelamento perfil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarVigenciaInicial(ParcelamentoPerfil parcelamentoPerfil) throws NegocioException {

		Date dataSemHora = DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime());
		if(parcelamentoPerfil.getVigenciaInicial() != null && parcelamentoPerfil.getVigenciaInicial().before(dataSemHora)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento#obterSegmentoPerfilParcelamento(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public ParcelamentoPerfilSegmento obterSegmentoPerfilParcelamento(Long idPerfilParcelamento, Long idSegmento) {

		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession()
						.createCriteria(ParcelamentoPerfilSegmentoImpl.class);

		criteria.createCriteria(CAMPO_SEGMENTO, CAMPO_SEGMENTO, Criteria.INNER_JOIN);
		criteria.createCriteria(CAMPO_PARCELAMENTO_PERFIL, CAMPO_PARCELAMENTO_PERFIL, Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
		criteria.add(Restrictions.eq("parcelamentoPerfil.chavePrimaria", idPerfilParcelamento));

		Collection<ParcelamentoPerfilSegmento> listaParcelamentoPerfilSegmento = criteria.list();

		if(listaParcelamentoPerfilSegmento.isEmpty()) {
			return null;
		} else {
			return listaParcelamentoPerfilSegmento.iterator().next();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento#obterSituacaoConsumoPerfilParcelamento(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public ParcelamentoPerfilSituacaoPontoConsumo obterSituacaoConsumoPerfilParcelamento(Long idPerfilParcelamento, Long idSituacaoConsumo) {

		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession()
						.createCriteria(ParcelamentoPerfilSituacaoPontoConsumoImpl.class);

		criteria.createCriteria("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
		criteria.createCriteria(CAMPO_PARCELAMENTO_PERFIL, CAMPO_PARCELAMENTO_PERFIL, Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("situacaoConsumo.chavePrimaria", idSituacaoConsumo));
		criteria.add(Restrictions.eq("parcelamentoPerfil.chavePrimaria", idPerfilParcelamento));

		Collection<ParcelamentoPerfilSituacaoPontoConsumo> listaParcelamentoPerfilSituacao = criteria.list();

		if(listaParcelamentoPerfilSituacao.isEmpty()) {
			return null;
		} else {
			return listaParcelamentoPerfilSituacao.iterator().next();
		}
	}

}
