/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.perfilparcelamento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao perfil de parcelamento. 
 *
 */
public interface ParcelamentoPerfil extends EntidadeNegocio {

	String BEAN_ID_PARCELAMENTO_PERFIL = "parcelamentoPerfil";

	String PARCELAMENTO_PERFIL = "ROTULO_PARCELAMENTO_PERFIL";

	String DESCRICAO = "ROTULO_DESCRICAO";

	String DESCRICAO_ABREVIADA = "ROTULO_DESCRICAO_ABREVIADA";

	String MAXIMO_PARCELAS = "ROTULO_MAXIMO_PARCELAS";

	String TAXA_JUROS = "ROTULO_TAXA_JUROS";

	String SISTEMA_AMORTIZACAO = "ROTULO_SISTEMA_AMORTIZACAO";

	String DESCONTO_MAXIMO = "ROTULO_DESCONTO_MAXIMO";

	String LISTA_PARC_PERFIL_SEGMENTO = "ROTULO_LISTA_PARC_PERFIL_SEGMENTO";

	String LISTA_PARC_PERFIL_SITUACAO_PONTO_CONSUMO = "ROTULO_LISTA_PARC_PERFIL_SITUACAO_PONTO_CONSUMO";

	String VIGENCIA_INICIAL = "ROTULO_VIGENCIA_INICIAL";

	String VIGENCIA_FINAL = "ROTULO_VIGENCIA_FINAL";

	/**
	 * @return the vigenciaInicial
	 */
	Date getVigenciaInicial();

	/**
	 * @param vigenciaInicial
	 *            the vigenciaInicial to set
	 */
	void setVigenciaInicial(Date vigenciaInicial);

	/**
	 * @return the vigenciaFinal
	 */
	Date getVigenciaFinal();

	/**
	 * @param vigenciaFinal
	 *            the vigenciaFinal to set
	 */
	void setVigenciaFinal(Date vigenciaFinal);

	/**
	 * @return the tipoTabela
	 */
	EntidadeConteudo getTipoTabela();

	/**
	 * @param tipoTabela
	 *            the tipoTabela to set
	 */
	void setTipoTabela(EntidadeConteudo tipoTabela);

	/**
	 * @return the tipoValor
	 */
	EntidadeConteudo getTipoValor();

	/**
	 * @param tipoValor
	 *            the tipoValor to set
	 */
	void setTipoValor(EntidadeConteudo tipoValor);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * @param descricaoAbreviada
	 *            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * @return the maximoParcelas
	 */
	Integer getMaximoParcelas();

	/**
	 * @param maximoParcelas
	 *            the maximoParcelas to set
	 */
	void setMaximoParcelas(Integer maximoParcelas);

	/**
	 * @return the indicadorDesconto
	 */
	boolean isIndicadorDesconto();

	/**
	 * @param indicadorDesconto
	 *            the indicadorDesconto to set
	 */
	void setIndicadorDesconto(boolean indicadorDesconto);

	/**
	 * @return the valorDesconto
	 */
	BigDecimal getValorDesconto();

	/**
	 * @param valorDesconto
	 *            the valorDesconto to set
	 */
	void setValorDesconto(BigDecimal valorDesconto);

	/**
	 * @return the indicadorJuros
	 */
	boolean isIndicadorJuros();

	/**
	 * @param indicadorJuros
	 *            the indicadorJuros to set
	 */
	void setIndicadorJuros(boolean indicadorJuros);

	/**
	 * @return the taxaJuros
	 */
	BigDecimal getTaxaJuros();

	/**
	 * @param taxaJuros
	 *            the taxaJuros to set
	 */
	void setTaxaJuros(BigDecimal taxaJuros);

	/**
	 * @return the listaParcPerfilSegmento
	 */
	Collection<ParcelamentoPerfilSegmento> getListaParcPerfilSegmento();

	/**
	 * @param listaParcPerfilSegmento
	 *            the listaParcPerfilSegmento to
	 *            set
	 */
	void setListaParcPerfilSegmento(Collection<ParcelamentoPerfilSegmento> listaParcPerfilSegmento);

	/**
	 * @return the
	 *         listaParcPerfilSituacaoPontoConsumo
	 */
	Collection<ParcelamentoPerfilSituacaoPontoConsumo> getListaParcPerfilSituacaoPontoConsumo();

	/**
	 * @param listaParcPerfilSituacaoPontoConsumo
	 *            the
	 *            listaParcPerfilSituacaoPontoConsumo
	 *            to set
	 */
	void setListaParcPerfilSituacaoPontoConsumo(Collection<ParcelamentoPerfilSituacaoPontoConsumo> listaParcPerfilSituacaoPontoConsumo);

	/**
	 * @return Retorna isIndicadorCorrecaoMonetaria
	 */
	boolean isIndicadorCorrecaoMonetaria();

	/**
	 * @param indicadorCorrecaoMonetaria - Set indicador correção monetária.
	 */
	void setIndicadorCorrecaoMonetaria(boolean indicadorCorrecaoMonetaria);

	/**
	 * @return Retorna IndiceFinanceiro
	 */
	IndiceFinanceiro getIndiceFinanceiro();

	/**
	 * @param indiceFinanceiro - Set indice Financeiro.
	 */
	void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro);

}
