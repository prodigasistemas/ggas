/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import br.com.ggas.cobranca.DadosExtratoDebito;
import br.com.ggas.web.faturamento.fatura.FaturaVO;

class DadosExtratoDebitoImpl implements DadosExtratoDebito {
	private static final long serialVersionUID = 118986038027041179L;

	// Atributos referente ao cliente
	private String nomeCliente;

	private String identificacao;

	private String docIdentificador;

	private String endereco;

	private String codigoCliente;

	private String inscricaoEstadual;

	private String tipoCliente;

	// Atributos referente ao ponto de consumo
	private Long codigoPontoConsumo;
	
	private String pontoConsumo;

	private String segmento;

	private String situacao;

	private String cep;

	// Atributos referente a tabela.
	private String numero;

	private String descricao;

	private String tipo;

	private String vencimento;

	private String valor;

	private String quantidade;

	private String valorUnitario;

	private String descricaoConciliacao;

	private String descricaoRecebimentos;

	private BigDecimal valorDoc;

	private String valorPorExtenso;

	private String documentoFiscal;

	private String diasAtraso;
	
	private Collection<FaturaVO> listaFaturasVO = new ArrayList<>();
	
	@Override
	public String getValorPorExtenso() {

		return valorPorExtenso;
	}

	@Override
	public void setValorPorExtenso(String valorPorExtenso) {

		this.valorPorExtenso = valorPorExtenso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getNomeCliente()
	 */
	@Override
	public String getNomeCliente() {

		return nomeCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setNomeCliente(java.lang.String)
	 */
	@Override
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getIdentificacao()
	 */
	@Override
	public String getIdentificacao() {

		return identificacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setIdentificacao(java.lang.String)
	 */
	@Override
	public void setIdentificacao(String identificacao) {

		this.identificacao = identificacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getDocIdentificador()
	 */
	@Override
	public String getDocIdentificador() {

		return docIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setDocIdentificador(java.lang.String)
	 */
	@Override
	public void setDocIdentificador(String docIdentificador) {

		this.docIdentificador = docIdentificador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getEndereco()
	 */
	@Override
	public String getEndereco() {

		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setEndereco(java.lang.String)
	 */
	@Override
	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getCodigoCliente()
	 */
	@Override
	public String getCodigoCliente() {

		return codigoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setCodigoCliente(java.lang.String)
	 */
	@Override
	public void setCodigoCliente(String codigoCliente) {

		this.codigoCliente = codigoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getInscricaoEstadual()
	 */
	@Override
	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setInscricaoEstadual(java.lang.String)
	 */
	@Override
	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getTipoCliente()
	 */
	@Override
	public String getTipoCliente() {

		return tipoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setTipoCliente(java.lang.String)
	 */
	@Override
	public void setTipoCliente(String tipoCliente) {

		this.tipoCliente = tipoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getPontoConsumo()
	 */
	@Override
	public String getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setPontoConsumo(java.lang.String)
	 */
	@Override
	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getSegmento()
	 */
	@Override
	public String getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setSegmento(java.lang.String)
	 */
	@Override
	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getSituacao()
	 */
	@Override
	public String getSituacao() {

		return situacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setSituacao(java.lang.String)
	 */
	@Override
	public void setSituacao(String situacao) {

		this.situacao = situacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getCep()
	 */
	@Override
	public String getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setCep(java.lang.String)
	 */
	@Override
	public void setCep(String cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getNumero()
	 */
	@Override
	public String getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setNumero(java.lang.String)
	 */
	@Override
	public void setNumero(String numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getTipo()
	 */
	@Override
	public String getTipo() {

		return tipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setTipo(java.lang.String)
	 */
	@Override
	public void setTipo(String tipo) {

		this.tipo = tipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getVencimento()
	 */
	@Override
	public String getVencimento() {

		return vencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setVencimento(java.lang.String)
	 */
	@Override
	public void setVencimento(String vencimento) {

		this.vencimento = vencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getValor()
	 */
	@Override
	public String getValor() {

		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setValor(java.lang.String)
	 */
	@Override
	public void setValor(String valor) {

		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getQuantidade()
	 */
	@Override
	public String getQuantidade() {

		return quantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setQuantidade(java.lang.String)
	 */
	@Override
	public void setQuantidade(String quantidade) {

		this.quantidade = quantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getValorUnitario()
	 */
	@Override
	public String getValorUnitario() {

		return valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setValorUnitario(java.lang.String)
	 */
	@Override
	public void setValorUnitario(String valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getDescricaoConciliacao()
	 */
	@Override
	public String getDescricaoConciliacao() {

		return descricaoConciliacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setDescricaoConciliacao(java.lang.String)
	 */
	@Override
	public void setDescricaoConciliacao(String descricaoConciliacao) {

		this.descricaoConciliacao = descricaoConciliacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #getDescricaoRecebimentos()
	 */
	@Override
	public String getDescricaoRecebimentos() {

		return descricaoRecebimentos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.DadosExtratoDebito
	 * #setDescricaoRecebimentos(java.lang.String)
	 */
	@Override
	public void setDescricaoRecebimentos(String descricaoRecebimentos) {

		this.descricaoRecebimentos = descricaoRecebimentos;
	}

	@Override
	public BigDecimal getValorDoc() {

		
		return valorDoc;
	}

	@Override
	public void setValorDoc(BigDecimal valorDoc) {

		this.valorDoc = valorDoc;

	}

	@Override
	public String getDocumentoFiscal() {

		return documentoFiscal;
	}

	@Override
	public void setDocumentoFiscal(String documentoFiscal) {

		this.documentoFiscal = documentoFiscal;
	}

	@Override
	public String getDiasAtraso() {

		return diasAtraso;
	}

	@Override
	public void setDiasAtraso(String diasAtraso) {

		this.diasAtraso = diasAtraso;
	}

	/**
	 * @return the listaFaturasVO
	 */
	@Override
	public Collection<FaturaVO> getListaFaturasVO() {

		return listaFaturasVO;
	}

	/**
	 * @param listaFaturasVO the listaFaturasVO to set
	 */
	@Override
	public void setListaFaturasVO(Collection<FaturaVO> listaFaturasVO) {

		this.listaFaturasVO = listaFaturasVO;
	}

	/**
	 * @return the codigoPontoConsumo
	 */
	@Override
	public Long getCodigoPontoConsumo() {

		return codigoPontoConsumo;
	}

	/**
	 * @param codigoPontoConsumo the codigoPontoConsumo to set
	 */
	@Override
	public void setCodigoPontoConsumo(Long codigoPontoConsumo) {

		this.codigoPontoConsumo = codigoPontoConsumo;
	}

}
