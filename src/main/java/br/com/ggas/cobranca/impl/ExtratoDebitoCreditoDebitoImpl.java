/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cobranca.impl;

import br.com.ggas.cobranca.ExtratoDebitoCreditoDebito;

class ExtratoDebitoCreditoDebitoImpl implements ExtratoDebitoCreditoDebito {

	private static final long serialVersionUID = 5323709991482893000L;

	private long idCreditoDebitoPrincipal;

	private String tipoDocumento;

	private String descricaoRubrica;

	private String inicioCobranca;

	private String parcelasCobradas;

	private String totalParcelas;

	private String valorAVista;

	private String valorTotal;

	private long idPontoConsumo;

	private int quantidadeNaoCobrada;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getIdCreditoDebitoPrincipal()
	 */
	@Override
	public long getIdCreditoDebitoPrincipal() {

		return idCreditoDebitoPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setIdCreditoDebitoPrincipal(long[])
	 */
	@Override
	public void setIdCreditoDebitoPrincipal(long idCreditoDebitoPrincipal) {

		this.idCreditoDebitoPrincipal = idCreditoDebitoPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getTipoDocumento()
	 */
	@Override
	public String getTipoDocumento() {

		return tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setTipoDocumento(java.lang.String)
	 */
	@Override
	public void setTipoDocumento(String tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getDescricaoRubrica()
	 */
	@Override
	public String getDescricaoRubrica() {

		return descricaoRubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setDescricaoRubrica(java.lang.String)
	 */
	@Override
	public void setDescricaoRubrica(String descricaoRubrica) {

		this.descricaoRubrica = descricaoRubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getInicioCobranca()
	 */
	@Override
	public String getInicioCobranca() {

		return inicioCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setInicioCobranca(java.lang.String)
	 */
	@Override
	public void setInicioCobranca(String inicioCobranca) {

		this.inicioCobranca = inicioCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getParcelasCobradas()
	 */
	@Override
	public String getParcelasCobradas() {

		return parcelasCobradas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setParcelasCobradas(java.lang.String)
	 */
	@Override
	public void setParcelasCobradas(String parcelasCobradas) {

		this.parcelasCobradas = parcelasCobradas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getTotalParcelas()
	 */
	@Override
	public String getTotalParcelas() {

		return totalParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setTotalParcelas(java.lang.String)
	 */
	@Override
	public void setTotalParcelas(String totalParcelas) {

		this.totalParcelas = totalParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito#getValorAVista()
	 */
	@Override
	public String getValorAVista() {

		return valorAVista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setValorAVista(java.lang.String)
	 */
	@Override
	public void setValorAVista(String valorAVista) {

		this.valorAVista = valorAVista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito#getValorTotal()
	 */
	@Override
	public String getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setValorTotal(java.lang.String)
	 */
	@Override
	public void setValorTotal(String valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #getIdPontoConsumo()
	 */
	@Override
	public long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * ExtratoDebitoCreditoDebito
	 * #setIdPontoConsumo(long[])
	 */
	@Override
	public void setIdPontoConsumo(long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ExtratoDebitoCreditoDebito
	 * #getQuantidadeNaoCobrada()
	 */
	@Override
	public int getQuantidadeNaoCobrada() {

		return quantidadeNaoCobrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ExtratoDebitoCreditoDebito
	 * #setQuantidadeNaoCobrada(int)
	 */
	@Override
	public void setQuantidadeNaoCobrada(int quantidadeNaoCobrada) {

		this.quantidadeNaoCobrada = quantidadeNaoCobrada;
	}

}
