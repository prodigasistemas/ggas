package br.com.ggas.cobranca.impl;

import java.util.ArrayList;
import java.util.Collection;

import br.com.ggas.atendimento.chamado.dominio.ClienteVO;

/**
 * Classe responsável pelos dados do relatório de extrato de débito e quitação anual. 
 *
 */
public class DadosRelatorioExtratoDebitoQuitacaoAnual {

	private String nomeCliente;
	private String pontoConsumo;
	private String descricao;
	private String endereco;
	private String codigoCliente;
	private String inscricaoEstadual;
	private String tipoCliente;
	private String cep;
	private String docIdentificador;
	private String situacao;
	private String segmento;
	private String valor;
	private String vencimento;
	private String tipo;
	private String numero;
	private Collection<ClienteVO> clientesVO = new ArrayList<>();

	
	
	public Collection<ClienteVO> getClientesVO() {
	
		return clientesVO;
	}
	
	public void setClientesVO(Collection<ClienteVO> clientesVO) {
	
		this.clientesVO = clientesVO;
	}

	public String getNomeCliente() {
	
		return nomeCliente;
	}

	
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}

	
	public String getPontoConsumo() {
	
		return pontoConsumo;
	}

	
	public void setPontoConsumo(String pontoConsumo) {
	
		this.pontoConsumo = pontoConsumo;
	}

	
	public String getDescricao() {
	
		return descricao;
	}

	
	public void setDescricao(String descricao) {
	
		this.descricao = descricao;
	}

	
	public String getEndereco() {
	
		return endereco;
	}

	
	public void setEndereco(String endereco) {
	
		this.endereco = endereco;
	}

	
	public String getCodigoCliente() {
	
		return codigoCliente;
	}

	
	public void setCodigoCliente(String codigoCliente) {
	
		this.codigoCliente = codigoCliente;
	}

	
	public String getInscricaoEstadual() {
	
		return inscricaoEstadual;
	}

	
	public void setInscricaoEstadual(String inscricaoEstadual) {
	
		this.inscricaoEstadual = inscricaoEstadual;
	}

	
	public String getTipoCliente() {
	
		return tipoCliente;
	}

	
	public void setTipoCliente(String tipoCliente) {
	
		this.tipoCliente = tipoCliente;
	}

	
	public String getCep() {
	
		return cep;
	}

	
	public void setCep(String cep) {
	
		this.cep = cep;
	}

	
	public String getDocIdentificador() {
	
		return docIdentificador;
	}

	
	public void setDocIdentificador(String docIdentificador) {
	
		this.docIdentificador = docIdentificador;
	}

	
	public String getSituacao() {
	
		return situacao;
	}

	
	public void setSituacao(String situacao) {
	
		this.situacao = situacao;
	}

	
	public String getSegmento() {
	
		return segmento;
	}

	
	public void setSegmento(String segmento) {
	
		this.segmento = segmento;
	}

	
	public String getValor() {
	
		return valor;
	}

	
	public void setValor(String valor) {
	
		this.valor = valor;
	}

	
	public String getVencimento() {
	
		return vencimento;
	}

	
	public void setVencimento(String vencimento) {
	
		this.vencimento = vencimento;
	}

	
	public String getTipo() {
	
		return tipo;
	}

	
	public void setTipo(String tipo) {
	
		this.tipo = tipo;
	}

	
	public String getNumero() {
	
		return numero;
	}

	
	public void setNumero(String numero) {
	
		this.numero = numero;
	}
}
