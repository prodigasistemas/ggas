/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCobrancaImpl representa uma ControladorCobrancaImpl no sistema.
 *
 * @since 22/02/2010
 *
 */

package br.com.ggas.cobranca.impl;

import java.awt.Image;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.ValorParcela;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.atendimento.chamado.dominio.ClienteVO;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.DadosExtratoDebito;
import br.com.ggas.cobranca.ExtratoDebitoCreditoDebito;
import br.com.ggas.cobranca.acao.AcaoCobranca;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.AvisoCorteEmissao;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorteEmissao;
import br.com.ggas.cobranca.boletobancario.FabricaConstrutorBoleto;
import br.com.ggas.cobranca.boletobancario.codigobarras.CodigoBarras;
import br.com.ggas.cobranca.boletobancario.codigobarras.DigitoAutoConferencia;
import br.com.ggas.cobranca.boletobancario.codigobarras.FabricaCodigoBarras;
import br.com.ggas.cobranca.boletobancario.codigobarras.dados.DadosCodigoBarras;
import br.com.ggas.cobranca.boletobancario.codigobarras.dados.DadosPadraoCodigoBarras;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoleto;
import br.com.ggas.cobranca.declaracaoquitacaoanual.DeclaracaoQuitacaoAnual;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.EncargoTributario;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.DocumentoCobrancaVO;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.FaturaConciliacao;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Extenso;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.cobranca.acaocobranca.AvisoCorteVO;
import br.com.ggas.web.cobranca.acaocobranca.SubRelatorioFaturasVencidasCobrancaVO;
import br.com.ggas.web.faturamento.fatura.FaturaVO;

/**
 * Classe responsável pela implementação dos métodos do controlador de cobrança.
 *
 */
@SuppressWarnings("unchecked")
public class ControladorCobrancaImpl extends ControladorNegocioImpl implements ControladorCobranca {


	private static final int TAMANHO_MAX_COD_VALIDACAO = 11;

	private static final int CONSTANTE_NOVECENTOS_NOVENTA_NOVE = 999;

	private static final String ENDERECOS = "enderecos";

	private static final int NUMERO_QUARENTA_QUATRO = 44;

	private static final int NUMERO_TRINTA_TRES = 33;

	private static final int NUMERO_VINTE_DOIS = 22;

	private static final int NUMERO_ZERO = 0;

	private static final String TIPO_PESSOA = "tipo_pessoa";

	private static final String CNPJ_CPF_CLIENTE = "cnpj_cpf_cliente";

	private static final String ENDERECO_EMPRESA = "enderecoEmpresa";

	private static final String IMAGEM_LOGOMARCA_EMPRESA = "imagem";

	private static final int CENTENA = 100;

	private static final int QUATRO_ZEROS = 4;

	private static final int ONZE_ZEROS = 11;

	private static final int LIMITE_DESCRICAO = 2;

	private static final int QUATRO_CASAS_DECIMAIS = 4;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int TRINTA_DIAS = 30;

	private static final int DEZOITO_CASAS = 18;

	private static final String PROTOCOLOS = "protocolos";

	private static final Logger LOG = Logger.getLogger(ControladorCobrancaImpl.class);

	private static final String RELATORIO_BOLETO_BANCARIO_JASPER = "relatorioBoletoBancario.jasper";

	private static final String RELATORIO_DECLARACAO_QUITACAO_DEBITOS_ANUAL_LOTE_JASPER =
			"relatorioDeclaracaoQuitacaoDebitosAnualLote.jasper";

	private static final String RELATORIO_DECLARACAO_QUITACAO_DEBITOS_ANUAL_NOVO = "relatorioDeclaracaoQuitacaoDebitosAnualNovo.jasper";

	private static final String RELATORIO_DECLARACAO_DEBITOS_EM_ABERTO_LOTE_JASPER = "relatorioDeclaracaoDebitosEmAbertoLote.jasper";

	private static final String TIPO_CLIENTE = "tipoCliente";

	private static final String INSCRICAO_ESTADUAL = "inscricaoEstadual";

	private static final String CODIGO_CLIENTE = "codigoCliente";

	private static final String ENDERECO = "endereco";

	private static final String DOC_IDENTIFICADOR = "docIdentificador";

	private static final String NOME_CLIENTE = "nomeCliente";

	private static final String CODIGO_BARRA = "codigoBarra";

	private static final String NUMERO_CODIGO_BARRA = "numeroCodigoBarra";

	private static final String CODIGO_BARRA_SEM_DIGITOS = "codigoBarraSemDigitos";

	private static final String CODIGO_BARRA_COM_DIGITOS = "codigoBarraComDigitos";

	private static final String VALOR_TOTAL = "valorTotal";

	private static final String TOTAL_CREDITOS = "totalCreditos";

	private static final String CREDITOS = "creditos";

	private static final String TOTAL_DEBITOS = "totalDebitos";

	private static final String DEBITOS = "debitos";

	private static final String FATURANOTASDEBITO = "faturaNotasDebito";

	private static final String NOTASCREDITO = "notasCredito";

	private static final String TOTAL_DOCUMENTOS = "totalDocumentos";

	private static final String DOCUMENTOS = "documentos";

	private static final String RELATORIO_EXTRATO_DEBITO = "relatorioExtratoDebito.jasper";

	private static final String RELATORIO_DECLARACAO_DEBITOS_EM_ABERTO = "relatorioDeclaracaoDebitosEmAberto.jasper";

	private static final String RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES = "relatorioDeclaracaoDebitosAnosAnteriores.jasper";

	private static final String RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES_EM_ABERTO = "relatorioDeclaracaoDebitosAnosAnteriores";

	private static final String CODIGO_EMPRESA_FEBRABAN = "CODIGO_EMPRESA_FEBRABAN";

	private static final String NOTA_CREDITO = "DC";

	private static final String CREDITO = "C";
	public static final String COBRANCA_DEBITO_SITUACAO = "cobrancaDebitoSituacao";

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresas;

	@Autowired
	private ControladorDocumentoImpressaoLayout controladorDocumentoImpressaoLayout;
	
	@Autowired
	@Qualifier("controladorTipoDocumento")
	private ControladorTipoDocumento controladorTipoDocumento;
	
	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	private static final String NOTA_DEBITO_PENALIDADE_REFERENTE = "Nota de Débito de penalidade Referente ao ponto de consumo: ";

	private static final String QNR = "QNR";

	private static final String QRET = "QRET";

	private static final String QREC = "QREC";

	private static final String QUEBRA_LINHA = "\r\n";
	
	private static final String VALOR_PARAMETRO_APENAS_ARQUIVO_DE_NOTIFICACAO = "2";
	
	private static final String VALOR_PARAMETRO_ARQUIVO_DE_NOTIFICACAO_E_CONTA = "3";

	private Fachada fachada = Fachada.getInstancia();


	private static final String FROM = " from ";
	private static final String WHERE = " where ";

	private ControladorConstanteSistema getControladorConstanteSistema() {
		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#criarValorParcela()
	 */
	@Override
	public Object criarValorParcela() {

		return ServiceLocator.getInstancia().getBeanPorID(ValorParcela.BEAN_ID_VALOR_PARCELA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#criarDocumentoCobranca()
	 */
	@Override
	public EntidadeNegocio criarDocumentoCobranca() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(DocumentoCobranca.BEAN_ID_DOCUMENTO_COBRANCA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#criarDocumentoCobrancaItem()
	 */
	@Override
	public EntidadeNegocio criarDocumentoCobrancaItem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(DocumentoCobrancaItem.BEAN_ID_DOCUMENTO_COBRANCA_ITEM);
	}

	public Class<?> getClasseEntidadeEntregaDocumento() {

		return ServiceLocator.getInstancia().getClassPorID(EntregaDocumento.BEAN_ID_ENTREGA_DOCUMENTO);
	}

	/**
	 * Criar extrato debito credito debito.
	 *
	 * @return the extrato debito credito debito
	 */
	public ExtratoDebitoCreditoDebito criarExtratoDebitoCreditoDebito() {

		return (ExtratoDebitoCreditoDebito) ServiceLocator.getInstancia().getBeanPorID(
						ExtratoDebitoCreditoDebito.BEAN_ID_EXTRATO_DEBITO_CREDITO_DEBITO);
	}

	/**
	 * Criar dados extrato debito.
	 *
	 * @return the dados extrato debito
	 */
	public DadosExtratoDebito criarDadosExtratoDebito() {

		return (DadosExtratoDebito) ServiceLocator.getInstancia().getBeanPorID(DadosExtratoDebito.BEAN_ID_DADOS_EXTRATO_DEBITO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeDeclaracaoQuitacaoAnual() {

		return ServiceLocator.getInstancia().getClassPorID(DeclaracaoQuitacaoAnual.BEAN_ID_DECLARACAO_QUITACAO_ANUAL);
	}

	public Class<?> getClasseEntidadeCreditoDebitoARealizar() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	public Class<?> getClasseEntidadeFaturaGeral() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaGeral.BEAN_ID_FATURA_GERAL);
	}

	public Class<?> getClasseEntidadeRecebimento() {

		return ServiceLocator.getInstancia().getClassPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	public Class<?> getClasseEntidadeFaturaConciliacao() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaConciliacao.BEAN_ID_FATURA_CONCILIACAO);
	}

	public Class<?> getClasseEntidadeCobrancaDebitoSituacao() {

		return ServiceLocator.getInstancia().getClassPorID(CobrancaDebitoSituacao.BEAN_ID_COBRANCA_DEBITO_SITUACAO);
	}

	public Class<?> getClasseEntidadeCreditoDebitoSituacao() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoSituacao.BEAN_ID_CREDITO_DEBITO_SITUACAO);
	}

	@Override
	public Class<?> getClasseEntidadeDocumentoCobranca() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoCobranca.BEAN_ID_DOCUMENTO_COBRANCA);
	}

	public Class<?> getClasseEntidadeDocumentoCobrancaItem() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoCobrancaItem.BEAN_ID_DOCUMENTO_COBRANCA_ITEM);
	}

	public Class<?> getClasseEntidadeTipoDocumento() {

		return ServiceLocator.getInstancia().getClassPorID(TipoDocumento.BEAN_ID_TIPO_DOCUMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#obterCobrancaDebitoSituacao(java.lang.Long)
	 */
	@Override
	public EntidadeNegocio obterCobrancaDebitoSituacao(Long idCobrancaDebitoSituacao) throws NegocioException {

		return super.obter(idCobrancaDebitoSituacao, getClasseEntidadeCobrancaDebitoSituacao());
	}

	public Class<?> getClasseEncargoTributario() {

		return ServiceLocator.getInstancia().getClassPorID(EncargoTributario.BEAN_ID_ENCARGO_TRIBUTARIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#obterCreditoDebitoSituacao(java.lang.Long)
	 */
	@Override
	public CreditoDebitoSituacao obterCreditoDebitoSituacao(Long idCreditoDebitoSituacao) throws NegocioException {

		return (CreditoDebitoSituacao) super.obter(idCreditoDebitoSituacao, getClasseEntidadeCreditoDebitoSituacao());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#obterFatura(java.lang.Long, java.lang.String[])
	 */
	@Override
	public EntidadeNegocio obterFatura(Long chavePrimaria, String... propriedadesEager) throws NegocioException {

		return super.obter(chavePrimaria, getClasseEntidadeFatura(), propriedadesEager);
	}

	public Class<?> getClasseEntidadeAcaoCobranca() {

		return ServiceLocator.getInstancia().getClassPorID(AcaoCobranca.BEAN_ID_ACAOCOBRANCA);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeArrecadadorContratoConvenio() {

		return ServiceLocator.getInstancia().getClassPorID(ArrecadadorContratoConvenio.BEAN_ID_ARRECADADOR_CONTRATO_CONVENIO);
	}

	public Class<?> getClasseEntidadeBanco() {

		return ServiceLocator.getInstancia().getClassPorID(Banco.BEAN_ID_BANCO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * obterDocumentoCobranca
	 * (java.lang.Long)
	 */
	@Override
	public DocumentoCobranca obterDocumentoCobranca(Long chavePrimaria) throws NegocioException {

		return (DocumentoCobranca) super.obter(chavePrimaria, getClasseEntidadeDocumentoCobranca(), COBRANCA_DEBITO_SITUACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * obterDocumentoCobrancaItem
	 * (java.lang.Long)
	 */
	@Override
	public DocumentoCobrancaItem obterDocumentoCobrancaItem(Long chavePrimaria) throws NegocioException {

		return (DocumentoCobrancaItem) super.obter(chavePrimaria, getClasseEntidadeDocumentoCobrancaItem());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * obterDocumentoCobranca
	 * (java.lang.Long)
	 */
	@Override
	public TipoDocumento obterTipoDocumento(Long chavePrimaria) throws NegocioException {

		return (TipoDocumento) super.obter(chavePrimaria, getClasseEntidadeTipoDocumento());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * obterCreditoDebitoARealizar
	 * (java.lang.Long)
	 */
	@Override
	public CreditoDebitoARealizar obterCreditoDebitoARealizar(Long chavePrimaria) throws NegocioException {

		return (CreditoDebitoARealizar) super.obter(chavePrimaria, getClasseEntidadeCreditoDebitoARealizar());
	}

	/**
	 * Método responsável para calcular o valor
	 * das parcelas do PRICE.
	 *
	 * @param valor
	 *            the valor
	 * @param numeroPrestacoes
	 *            the numero prestacoes
	 * @param taxaJuros
	 *            the taxa juros
	 * @param dataInicial
	 *            the data inicial
	 * @param periodicidade
	 *            the periodicidade
	 * @param periodicidadeJuros
	 *            the periodicidade juros
	 * @param isParcelamento
	 *            the is parcelamento
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public List<ValorParcela> calcularPrice(BigDecimal valor, int numeroPrestacoes, BigDecimal taxaJuros, Date dataInicial,
					Integer periodicidade, Integer periodicidadeJuros, boolean isParcelamento) throws NegocioException {

		BigDecimal taxaJurosAplicada = BigDecimal.ZERO;
		BigDecimal taxaJurosCalculada = BigDecimal.ZERO;

		if (isParcelamento) {

			BigDecimal taxaJurosDiaria = taxaJuros.divide(new BigDecimal(periodicidadeJuros), DEZOITO_CASAS, RoundingMode.HALF_UP);

			taxaJurosDiaria = taxaJurosDiaria.divide(new BigDecimal(CENTENA), DEZOITO_CASAS, BigDecimal.ROUND_DOWN);

			if (taxaJurosDiaria.compareTo(BigDecimal.ZERO) == 0) {
				throw new NegocioException(Constantes.ERRO_PERIODICIDADE_PARCELAMENTO_INVALIDA, true);
			}

			Date dataAtual = Calendar.getInstance().getTime();
			Date dataVencimentoInicial = dataInicial;

			int qtdeDias = Util.intervaloDatas(dataAtual, dataVencimentoInicial);
			if (qtdeDias > 0) {
				if (qtdeDias > TRINTA_DIAS) {
					qtdeDias = TRINTA_DIAS;
				}
				taxaJurosAplicada = taxaJurosDiaria.multiply(new BigDecimal(qtdeDias));
			}
			taxaJurosCalculada = taxaJurosDiaria.multiply(new BigDecimal(periodicidade));
		} else {
			taxaJurosCalculada = taxaJuros.divide(new BigDecimal(CENTENA), DEZOITO_CASAS, BigDecimal.ROUND_DOWN);
		}

		BigDecimal juros = BigDecimal.ONE;
		BigDecimal saldo = valor;
		BigDecimal amortizacao = BigDecimal.ONE;
		BigDecimal prestacao = BigDecimal.ONE;
		List<ValorParcela> listaRetorno = new ArrayList<>();
		ValorParcela valorParcela = null;

		BigDecimal fatorPrestacao = BigDecimal.ONE;
		BigDecimal fatorPrestacaoAuxiliar = BigDecimal.ONE;

		fatorPrestacao = fatorPrestacaoAuxiliar.add(taxaJurosCalculada);
		fatorPrestacao = fatorPrestacao.pow(numeroPrestacoes);
		fatorPrestacao = fatorPrestacaoAuxiliar.divide(fatorPrestacao, DEZOITO_CASAS, RoundingMode.HALF_UP);
		fatorPrestacao = fatorPrestacaoAuxiliar.subtract(fatorPrestacao);
		fatorPrestacao = fatorPrestacao.divide(taxaJurosCalculada, DEZOITO_CASAS, RoundingMode.HALF_UP);
		prestacao = saldo.divide(fatorPrestacao, DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);

		BigDecimal amortizacaoAcumulada = BigDecimal.ZERO;
		for (int i = 0; i < numeroPrestacoes; i++) {
			valorParcela = (ValorParcela) criarValorParcela();

			if (i != 0) {
				saldo = saldo.subtract(amortizacao);
			}
			if (isParcelamento) {
				juros = saldo.multiply(taxaJurosCalculada);

				DateTime novaData = new DateTime(dataInicial);
				// Tratar mês civil
				novaData = novaData.plusDays(i * periodicidade);
				valorParcela.setDataVencimento(novaData.toDate());
			} else {
				juros = saldo.multiply(taxaJurosCalculada);
			}

			amortizacao = prestacao.subtract(juros);
			amortizacaoAcumulada = amortizacaoAcumulada.add(amortizacao);

			BigDecimal valorPrestacao = prestacao.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
			if (i == 0) {
				juros = saldo.multiply(taxaJurosAplicada);
				valorPrestacao = juros.add(amortizacao.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
			}

			if (saldo.intValue() <= 0) {
				saldo = BigDecimal.ZERO;
			}

			if ((i == (numeroPrestacoes - 1)) && (amortizacaoAcumulada.compareTo(valor) != 0)) {
				valorPrestacao = saldo.add(juros).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
				amortizacao = valorPrestacao.subtract(juros);
			} else {
				amortizacao = amortizacao.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
			}

			valorParcela.setPeriodo(i);
			valorParcela.setJuros(juros.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
			valorParcela.setAmortizacao(amortizacao.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
			valorParcela.setSaldo(saldo.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
			valorParcela.setValorPagar(valorPrestacao.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));

			listaRetorno.add(valorParcela);
		}

		return listaRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * obterBanco(java.lang
	 * .Long)
	 */
	@Override
	public Banco obterBanco(Long chavePrimaria) throws NegocioException {

		return (Banco) super.obter(chavePrimaria, getClasseEntidadeBanco());
	}

	/**
	 * Método responsável para calcular o valor
	 * das parcelas no SAC
	 *
	 * @param parcelas
	 * @param valor
	 * @param taxaJuros
	 * @return
	 * @throws NegocioException
	 */
	@Override
	public List<ValorParcela> calcularSAC(BigDecimal valor, int parcelas, BigDecimal taxaJuros, Date dataInicial, Integer periodicidade,
					Integer periodicidadeJuros, boolean isParcelamento) throws NegocioException {

		return this.calcularSAC(valor, parcelas, taxaJuros, dataInicial, periodicidade, periodicidadeJuros, isParcelamento, false);
	}

	/**
	 * Método responsável para calcular o valor
	 * das parcelas no SAC
	 *
	 * @param valor
	 *            the valor
	 * @param parcelas
	 *            the parcelas
	 * @param taxaJuros
	 *            the taxa juros
	 * @param dataInicial
	 *            the data inicial
	 * @param periodicidade
	 *            the periodicidade
	 * @param periodicidadeJuros
	 *            the periodicidade juros
	 * @param isParcelamento
	 *            the is parcelamento
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public List<ValorParcela> calcularSAC(BigDecimal valor, int parcelas, BigDecimal taxaJuros, Date dataInicial, Integer periodicidade,
					Integer periodicidadeJuros, boolean isParcelamento, boolean isJurosCalculados) throws NegocioException {

		List<ValorParcela> listaRetorno = new ArrayList<>();

		BigDecimal taxaJurosAplicada = BigDecimal.ZERO;
		BigDecimal taxaJurosCalculada = BigDecimal.ZERO;
		Date dataVencimentoInicial = dataInicial;
		Date dataAtual = Calendar.getInstance().getTime();
		if (isParcelamento) {

			BigDecimal taxaJurosDiaria = taxaJuros.divide(new BigDecimal(periodicidadeJuros), DEZOITO_CASAS, RoundingMode.HALF_UP);
			taxaJurosDiaria = taxaJurosDiaria.divide(new BigDecimal(CENTENA), DEZOITO_CASAS, BigDecimal.ROUND_DOWN);

			int qtdeDias = Util.intervaloDatas(dataAtual, dataVencimentoInicial);
			if (qtdeDias > 0) {
				taxaJurosAplicada = taxaJurosDiaria.multiply(new BigDecimal(qtdeDias));
			}
			taxaJurosCalculada = taxaJurosDiaria.multiply(new BigDecimal(periodicidade));
		} else {
			taxaJurosCalculada = taxaJuros.divide(new BigDecimal(CENTENA), DEZOITO_CASAS, BigDecimal.ROUND_DOWN);
		}

		BigDecimal juros = BigDecimal.ZERO;
		BigDecimal saldo = valor;
		BigDecimal amortizacao = valor.divide(new BigDecimal(parcelas), DEZOITO_CASAS, BigDecimal.ROUND_DOWN);
		amortizacao = amortizacao.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);

		ValorParcela valorParcela = null;

		BigDecimal amortizacaoAcumulada = amortizacao.multiply(new BigDecimal(parcelas));
		// Recupera as perdas pelo arredondamento
		BigDecimal diferenca = valor.subtract(amortizacaoAcumulada);

		for (int i = 0; i < parcelas; i++) {

			valorParcela = (ValorParcela) criarValorParcela();

			if (i != 0) {
				saldo = saldo.subtract(amortizacao);
			}
			if (isParcelamento) {
				if (!isJurosCalculados) {
					if (i == 0) {
						juros = saldo.multiply(taxaJurosAplicada);
					} else {
						juros = saldo.multiply(taxaJurosCalculada);
					}
				}

				DateTime novaData = new DateTime(dataInicial);
				novaData = novaData.plusDays(i * periodicidade);
				valorParcela.setDataVencimento(novaData.toDate());

			} else {
				if (!isJurosCalculados) {
					juros = saldo.multiply(taxaJurosCalculada);
				}
			}

			// Garante recuperar as perdas no arredondamento. Soma ao valor da última parcela.
			if (i == (parcelas - 1)) {
				amortizacao = amortizacao.add(diferenca);
			}

			valorParcela.setPeriodo(i);
			valorParcela.setJuros(juros.setScale(DEZOITO_CASAS, BigDecimal.ROUND_HALF_UP));
			valorParcela.setAmortizacao(amortizacao.setScale(DEZOITO_CASAS, BigDecimal.ROUND_HALF_UP));
			valorParcela.setValorPagar(amortizacao.setScale(DEZOITO_CASAS, BigDecimal.ROUND_HALF_UP)
					.add(juros.setScale(DUAS_CASAS_DECIMAIS, BigDecimal.ROUND_HALF_UP)));
			valorParcela.setSaldo(saldo.setScale(DEZOITO_CASAS, BigDecimal.ROUND_HALF_UP));

			listaRetorno.add(valorParcela);
		}

		return listaRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * consultarExtratoDebitosClientePontoConsumo(java
	 * .lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Fatura> consultarFaturasClientePontoConsumo(Long idCliente, Long idPontoConsumo) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorParametroSistema();
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append( " fatura ");
		hql.append(" inner join fetch fatura.tipoDocumento tipoDocumento ");
		hql.append(" inner join fetch fatura.situacaoPagamento situacaoPagamento ");

		hql.append(WHERE);
		if ((idCliente != null) && (idCliente > 0)) {
			hql.append(" fatura.cliente.chavePrimaria = :idCliente and ");
		} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			hql.append(" fatura.pontoConsumo.chavePrimaria = :idPontoConsumo and ");
		}

		String codigoCreditoDebitoNormal = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);
		String codigoCreditoDebitoRetificada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_RETIFICADO);
		String codigoCreditoDebitoIncluida = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_INCLUIDO);

		Long[] arraySituacao = {Long.valueOf(codigoCreditoDebitoNormal), Long.valueOf(codigoCreditoDebitoRetificada), Long
						.valueOf(codigoCreditoDebitoIncluida)};
		// Alterar para verificar se a situação é válida (CRDS_IN_VALIDO=1 - valido is true)
		hql.append(" fatura.creditoDebitoSituacao.chavePrimaria in (:arraySituacao) ");
		hql.append(" and fatura.devolucao is null ");

		// Refeactoring de Faturamento agrupado
		hql.append(" and fatura.faturaAgrupada is null ");
		hql.append(" and fatura.contrato is null ");

		String codigoSituacaoPagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

		Long[] arrayPagamento = {Long.valueOf(codigoSituacaoPagamentoPendente), Long.valueOf(codigoSituacaoPagamentoParcialmentePago)};

		// refactory da query abaixo, solução imediata para homologação.
		hql.append(" and fatura.situacaoPagamento.chavePrimaria in (:arrayPagamento) ");
		hql.append(" and ( fatura.valorTotal >= ( ");
		hql.append(" select sum(recebimento.valorRecebimento) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRecebimento().getSimpleName());
		hql.append(" recebimento ");
		hql.append(WHERE);
		hql.append(" recebimento.faturaGeral.faturaAtual.chavePrimaria = fatura.chavePrimaria ");
		hql.append(" ) ");
		hql.append(" or 0 = ( ");
		hql.append(" select count(receb.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRecebimento().getSimpleName());
		hql.append(" receb ");
		hql.append(WHERE);
		hql.append(" receb.faturaGeral.faturaAtual.chavePrimaria = fatura.chavePrimaria ");
		hql.append(" ) ) ");
		hql.append(" order by fatura.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((idCliente != null) && (idCliente > 0)) {
			query.setLong("idCliente", idCliente);
		} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}
		query.setParameterList("arraySituacao", arraySituacao);
		query.setParameterList("arrayPagamento", arrayPagamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * consultarFaturasAbertasClientePontoConsumo(java
	 * .lang.Long,
	 * java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Fatura> consultarFaturasAbertasClientePontoConsumo(Long idCliente, Long idPontoConsumo, Boolean quitada)
					throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" inner join fetch fatura.tipoDocumento tipoDocumento ");
		hql.append(" inner join fetch fatura.situacaoPagamento situacaoPagamento ");

		hql.append(WHERE);

		// Refeactoring de Faturamento agrupado e Ver quem realizou o refactoring abaixo e analisar a necessidade da clausula
		// abaixo e ajustar ela // devidamente.
		hql.append(" fatura.faturaAgrupada is null ");
		hql.append(" and ");

		if ((idCliente != null) && (idCliente > 0)) {
			hql.append(" fatura.cliente.chavePrimaria = :idCliente and ");
		} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			hql.append(" (fatura.pontoConsumo.chavePrimaria = :idPontoConsumo or ");
			hql.append(" fatura.chavePrimaria in ");
			hql.append(" (select fatura2.faturaAgrupada.chavePrimaria from ");
			hql.append(getClasseEntidadeFatura().getSimpleName());
			hql.append(" fatura2 where ");
			hql.append(" fatura2.pontoConsumo.chavePrimaria =:idPontoConsumo and");
			hql.append(" fatura2.faturaAgrupada is not null )");
			hql.append(") and");
		}

		String codigoCreditoDebitoNormal = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);
		String codigoCreditoDebitoRetificada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_RETIFICADO);
		String codigoCreditoDebitoIncluida = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_INCLUIDO);

		Long[] arraySituacao = {Long.valueOf(codigoCreditoDebitoNormal), Long.valueOf(codigoCreditoDebitoRetificada), Long
						.valueOf(codigoCreditoDebitoIncluida)};

		hql.append(" fatura.creditoDebitoSituacao.chavePrimaria in (:arraySituacao) ");

		String codigoSituacaoPagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

		String codigoSituacaoPagamentoQuitado = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		Collection<Long> chavesPagamento = new ArrayList<Long>();
		chavesPagamento.add(Long.valueOf(codigoSituacaoPagamentoPendente));
		chavesPagamento.add(Long.valueOf(codigoSituacaoPagamentoParcialmentePago));

		if (quitada) {
			chavesPagamento.add(Long.valueOf(codigoSituacaoPagamentoQuitado));
		}

		hql.append(" and fatura.situacaoPagamento.chavePrimaria in (:arrayPagamento) ");

		String codigoDocumentoTipoFatura = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		String codigoDocumentoTipoNotaDebito = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO);
		Long codigoDocumentoTipoNotaDebitoPenalidade = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE));

		Long[] arrayTipoDocumento = {Long.parseLong(codigoDocumentoTipoFatura), Long.parseLong(codigoDocumentoTipoNotaDebito),
				codigoDocumentoTipoNotaDebitoPenalidade};

		hql.append(" and fatura.tipoDocumento.chavePrimaria in (:arrayTipoDocumento) ");
		hql.append(" order by fatura.dataEmissao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((idCliente != null) && (idCliente > 0)) {
			query.setLong("idCliente", idCliente);
		} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}
		query.setParameterList("arraySituacao", arraySituacao);
		query.setParameterList("arrayPagamento", chavesPagamento.toArray());
		query.setParameterList("arrayTipoDocumento", arrayTipoDocumento);

		return query.list();
	}

	/**
	 * Consultar creditos debitos cliente ponto consumo.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * consultarCreditosDebitosClientePontoConsumo(
	 * java.lang.Long)
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosClientePontoConsumo(Long idCliente, Long idPontoConsumo)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCreditoDebitoARealizar().getSimpleName());
		hql.append(" creditoDebito ");
		hql.append(" inner join fetch creditoDebito.rubrica rubrica ");
		hql.append(" inner join fetch creditoDebito.lancamentoItemContabil lancamentoItemContabil ");
		hql.append(" inner join fetch creditoDebito.lancamentoItemContabil.tipoCreditoDebito tipoCreditoDebito ");
		hql.append(WHERE);
		if ((idCliente != null) && (idCliente > 0)) {
			hql.append(" creditoDebito.cliente.chavePrimaria = :idCliente ");
		} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			hql.append(" creditoDebito.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}
		hql.append(" and creditoDebito.devolucao is null ");
		hql.append(" order by creditoDebito.rubrica.chavePrimaria, ");
		hql.append(" creditoDebito.creditoDebitoPrincipal.chavePrimaria, ");
		hql.append(" creditoDebito.numeroPrestacaoCobrada, ");
		hql.append(" creditoDebito.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((idCliente != null) && (idCliente > 0)) {
			query.setLong("idCliente", idCliente);
		} else if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca
	 * #calcularValorPresente
	 * (br.com.ggas.faturamento
	 * .CreditoDebitoARealizar, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public BigDecimal calcularValorPresente(CreditoDebitoARealizar creditoDebito, Integer qtdeParcelasCobradas,
					Integer qtdeParcelasAntecipadas) throws NegocioException {

		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		BigDecimal valorPresente = BigDecimal.ZERO;
		Long codigoLAICJurosParcelamento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO));

		Long codigoLAICJurosFinanciamento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_FINANCIAMENTO));

		Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
						.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebito.getChavePrimaria());

		for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCDDetalhamento) {

			if (creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() != codigoLAICJurosParcelamento
							&& creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() != codigoLAICJurosFinanciamento) {
				valorPresente = valorPresente.add(creditoDebitoDetalhamento.getValor());

			}
		}
		return valorPresente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * consultarFaturas(java
	 * .lang.Long[])
	 */
	@Override
	public Collection<Fatura> consultarFaturas(Long[] chavesFatura) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<Fatura> listaFatura = null;

		//[pburlamaqui]: remover as situações desta consulta, como já possui os ids das faturas não precisa obter pelas situações.
		if ((chavesFatura != null) && (chavesFatura.length > 0)) {
			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidadeFatura().getSimpleName());
			hql.append(" fatura ");
			hql.append(WHERE);

			// Refeactoring de Faturamento agrupado
			hql.append(" fatura.faturaAgrupada is null and");

			hql.append(" fatura.chavePrimaria in (:chavesFatura) ");
			hql.append(" and fatura.creditoDebitoSituacao.valido = 1 ");

			String codigoSituacaoPagamentoPendente = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
			String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

			Long[] arrayPagamento = {Long.valueOf(codigoSituacaoPagamentoPendente), Long.valueOf(codigoSituacaoPagamentoParcialmentePago)};

			hql.append(" and fatura.situacaoPagamento.chavePrimaria in (:arrayPagamento) ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameterList("chavesFatura", chavesFatura);
			query.setParameterList("arrayPagamento", arrayPagamento);

			listaFatura = query.list();
		}

		return listaFatura;
	}
	
	/**
	 * Consultar Creditos Debitos Parcelados.
	 *
	 * @param chavePrincipal
	 *            the Chave Principal
	 * @param numerosPrestacoesCobradas
	 *            the Numeros Prestacoes Cobradas
	 * @return the collection
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * consultarCreditosDebitos
	 * (java.lang.Long[])
	 */
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosParcelados(Long chavePrincipal, String[] numerosPrestacoesCobradas)
					throws NegocioException {

		Collection<CreditoDebitoARealizar> listaCreditoDebito = null;

		if (chavePrincipal != null) {

			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidadeCreditoDebitoARealizar().getSimpleName());
			hql.append(" creditoDebitoARealizar ");
			hql.append(WHERE);
			hql.append(" ( creditoDebitoARealizar.chavePrimaria =:chavePrincipal ");
			hql.append(" or creditoDebitoARealizar.creditoDebitoPrincipal.chavePrimaria =:chavePrincipal ) ");
			hql.append(" and creditoDebitoARealizar.numeroPrestacaoCobrada in (:numerosPrestacoesCobradas) ");
			hql.append(" order by creditoDebitoARealizar.rubrica.chavePrimaria, ");
			hql.append(" creditoDebitoARealizar.creditoDebitoPrincipal.chavePrimaria, ");
			hql.append(" creditoDebitoARealizar.numeroPrestacaoCobrada, ");
			hql.append(" creditoDebitoARealizar.chavePrimaria ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameter("chavePrincipal", chavePrincipal);
			query.setParameterList("numerosPrestacoesCobradas", Util.arrayStringParaArrayInteger(numerosPrestacoesCobradas));

			listaCreditoDebito = query.list();
		}

		return listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * obterFaturaGeralPelaFatura
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public FaturaGeral obterFaturaGeralPelaFatura(Long idFatura) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select faturaGeral from ");
		hql.append(getClasseEntidadeFaturaGeral().getSimpleName());
		hql.append(" faturaGeral ");
		hql.append(WHERE);
		hql.append(" faturaGeral.faturaAtual.chavePrimaria = :idFatura ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idFatura", idFatura);

		return (FaturaGeral) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * inserirDocumentoCobranca
	 * (java.lang.Long[], java.lang.Long[])
	 */
	@Override
	public DocumentoCobranca[] inserirDocumentoCobranca(Long[] chavesFatura, Long[] chavesCreditoDebito, Long idCliente,
					DocumentoCobranca documentoCobranca, boolean indicadorExtratoDebito, Long idTipoDocumento)
					throws NegocioException {

		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// Documento de cobrança que usará a forma de cobrança de Boleto Bancário.
		// O documento de cobrança que vem do parâmetro usará a forma de cobrança de Código de Barras.
		DocumentoCobranca documentoCobrancaBoleto = (DocumentoCobranca) this.criarDocumentoCobranca();
		Collection<Fatura> listaFatura = null;

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		if (chavesFatura != null && chavesFatura.length > 0) {
			Map<String, Object> filtro = new LinkedHashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesFatura);
			listaFatura = controladorFatura.consultarDadosFatura(filtro);
		}

		Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
		Map<Long, List<CreditoDebitoARealizar>> mapaParcelasAntecipadas = new LinkedHashMap<Long, List<CreditoDebitoARealizar>>();

		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitosPorChaves(chavesCreditoDebito);
			mapaParcelasAntecipadas = this.montarMapaParcelasAntecipadas(listaCreditoDebito);
		}

		Collection<ContratoPontoConsumo> listaContratoPonto = controladorContrato.obterContratoPontoConsumoPorCliente(idCliente);

		String idCobrancaDebitoSituacao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_COBRANCA_PENDENTE);

		Long idSituacao = null;
		try {
			idSituacao = Util.converterCampoStringParaValorLong(DocumentoCobranca.COBRANCA_DEBITO_SITUACAO, idCobrancaDebitoSituacao);
		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, DocumentoCobranca.COBRANCA_DEBITO_SITUACAO);
		}

		CobrancaDebitoSituacao cobrancaDebitoSituacao = (CobrancaDebitoSituacao) this.obterCobrancaDebitoSituacao(idSituacao);

		DocumentoCobrancaItem documentoCobrancaItem = null;

		Long idFaturaParcialmentePaga = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));
		Long idBoletoBancario = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_BOLETO_BANCARIO));
		Long idCodigoBarras = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_CODIGO_BARRAS));

		Map<Long, Collection<FaturaItem>> mapalistaFaturaItemPorFatura = controladorFatura.consultarFaturaItemPorChaveFatura(chavesFatura);
		// Populando DocumentoCobrancaItem a partir das faturas.
		Contrato contrato = null;
		if (listaFatura != null) {
			for (Fatura fatura : listaFatura) {
				contrato = fatura.getContratoAtual();
				documentoCobrancaItem = (DocumentoCobrancaItem) this.criarDocumentoCobrancaItem();
				documentoCobrancaItem.setCobrancaDebitoSituacao(cobrancaDebitoSituacao);
				FaturaGeral faturaGeral = obterFaturaGeralPelaFatura(fatura.getChavePrimaria());
				documentoCobrancaItem.setFaturaGeral(faturaGeral);

				BigDecimal valorFatura = fatura.getValorTotal();
				BigDecimal valorDocumentoitem =  BigDecimal.ZERO;
				if (indicadorExtratoDebito) {
					// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
					valorFatura = fatura.getValorSaldoConciliado();
					if ((idFaturaParcialmentePaga != null) && (fatura.getSituacaoPagamento() != null)
							&& (idFaturaParcialmentePaga == fatura.getSituacaoPagamento().getChavePrimaria())) {
						BigDecimal valorRecebimento = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
						if (valorRecebimento != null) {
							valorFatura = valorFatura.subtract(valorRecebimento);
						}
					}
				}
				Collection<FaturaItem> listaFaturaItem = mapalistaFaturaItemPorFatura.getOrDefault(
					fatura.getChavePrimaria(), Collections.emptyList());
				
				for (FaturaItem faturaItem : listaFaturaItem) {
					if(faturaItem.getCreditoDebitoARealizar() == null){
						valorDocumentoitem = faturaItem.getValorTotal();
					}
				}

				documentoCobrancaItem.setValor(valorDocumentoitem);

				ControladorRubrica controladorRubrica
						= (ControladorRubrica) ServiceLocator.getInstancia().getBeanPorID(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);
				String idRubricaTipoProduto = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PRODUTO);
				long idTipoProduto = Long.parseLong(idRubricaTipoProduto);
				boolean isTipoProduto = false;
				BigDecimal totalCredito = BigDecimal.ZERO;
				BigDecimal totalDebito = BigDecimal.ZERO;

				for (FaturaItem faturaItem : listaFaturaItem) {
					Rubrica rubrica = controladorRubrica.obterRubricaPorFaturaItem(faturaItem.getChavePrimaria());
					if (rubrica.getFinanciamentoTipo().getChavePrimaria() == idTipoProduto) {
						isTipoProduto = true;
					}
					CreditoDebitoARealizar creditoDebitoARealizar = faturaItem.getCreditoDebitoARealizar();
					if (creditoDebitoARealizar != null) {
						if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() == null) {
							totalDebito = totalDebito.add(creditoDebitoARealizar.getValor());
						} else {
							totalCredito = totalCredito.add(creditoDebitoARealizar.getValor());
						}
					}
				}
				if (isTipoProduto) {
					documentoCobrancaItem.setValorAcrecimos(totalDebito);
					documentoCobrancaItem.setValorDescontos(totalCredito);
				}

				if (fatura.getPontoConsumo() != null) {
					documentoCobrancaItem.setPontoConsumo(fatura.getPontoConsumo());
				}
				Long idFormaCobrancaContrato = obtemFormaCobrancaContrato(listaContratoPonto, fatura);

				documentoCobrancaItem.setVersao(0);
				documentoCobrancaItem.setDadosAuditoria(documentoCobranca.getDadosAuditoria());
				documentoCobrancaItem.setUltimaAlteracao(Calendar.getInstance().getTime());

				// A forma de cobranca indica em qual documento de cobrança o item será adicionado.
				if (idBoletoBancario.equals(idFormaCobrancaContrato)) {
					documentoCobrancaItem.setDocumentoCobranca(documentoCobrancaBoleto);
				} else if (idCodigoBarras.equals(idFormaCobrancaContrato)) {
					documentoCobrancaItem.setDocumentoCobranca(documentoCobranca);
					documentoCobranca.getItens().add(documentoCobrancaItem);
				} else {
					documentoCobrancaItem.setDocumentoCobranca(documentoCobrancaBoleto);
				}
				documentoCobrancaBoleto.getItens().add(documentoCobrancaItem);
			}
		}

		// Populando DocumentoCobrancaItem a partir dos CreditoDebitoARealizar.
		Entry<Long, List<CreditoDebitoARealizar>> entry = null;
		for (Iterator<Entry<Long, List<CreditoDebitoARealizar>>> it = mapaParcelasAntecipadas.entrySet().iterator(); it.hasNext();) {

			entry = it.next();

			List<CreditoDebitoARealizar> listaCDParcelas = new ArrayList<>(entry.getValue());
			Integer qtdeParcelasCobradas = controladorCreditoDebito.obterQtdCreditoDebitoCobrados(entry.getKey());

			if ((listaCreditoDebito != null) && (qtdeParcelasCobradas != null)) {
				for (CreditoDebitoARealizar creditoDebitoARealizar : listaCDParcelas) {

					documentoCobrancaItem = (DocumentoCobrancaItem) this.criarDocumentoCobrancaItem();
					documentoCobrancaItem.setCreditoDebitoARealizar(creditoDebitoARealizar);
					documentoCobrancaItem
									.setNumeroPrestacao(creditoDebitoARealizar.getCreditoDebitoNegociado().getNumeroPrestacaoCobrada());
					documentoCobrancaItem.setNumeroTotalPrestacao(creditoDebitoARealizar.getNumeroPrestacao());
					documentoCobrancaItem.setValorAcrecimos(BigDecimal.ZERO);
					documentoCobrancaItem.setValorDescontos(BigDecimal.ZERO);
					documentoCobrancaItem.setCobrancaDebitoSituacao(cobrancaDebitoSituacao);

					Long idFormaCobrancaContrato = null;
					if (creditoDebitoARealizar.getCreditoDebitoNegociado().getPontoConsumo() != null) {
						documentoCobrancaItem.setPontoConsumo(creditoDebitoARealizar.getCreditoDebitoNegociado().getPontoConsumo());
						idFormaCobrancaContrato = this.obterIdFormaCobrancaDoContrato(creditoDebitoARealizar.getCreditoDebitoNegociado()
										.getPontoConsumo(), listaContratoPonto);
					}
					BigDecimal valor = creditoDebitoARealizar.getValor();
					if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
						valor = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebitoARealizar);
					}
					if (creditoDebitoARealizar.getCreditoDebitoNegociado().getPercentualJuros() != null) {
						valor = this.calcularValorPresente(creditoDebitoARealizar, qtdeParcelasCobradas, entry.getValue().size());
					}
					documentoCobrancaItem.setValor(valor);
					documentoCobrancaItem.setVersao(0);
					documentoCobrancaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
					documentoCobrancaItem.setDadosAuditoria(documentoCobranca.getDadosAuditoria());

					if (idFormaCobrancaContrato != null && idFormaCobrancaContrato.equals(idBoletoBancario)) {
						documentoCobrancaItem.setDocumentoCobranca(documentoCobrancaBoleto);
					} else if (idFormaCobrancaContrato != null && idFormaCobrancaContrato.equals(idCodigoBarras)) {
						documentoCobrancaItem.setDocumentoCobranca(documentoCobranca);
						documentoCobranca.getItens().add(documentoCobrancaItem);
					} else {
						documentoCobrancaItem.setDocumentoCobranca(documentoCobrancaBoleto);
					}
					documentoCobrancaBoleto.getItens().add(documentoCobrancaItem);
					this.atualizarIndicadorAntecipacao(creditoDebitoARealizar, documentoCobranca.getDadosAuditoria());
				}
			}
		}

		documentoCobranca.setCobrancaDebitoSituacao(cobrancaDebitoSituacao);
		documentoCobrancaBoleto.setCobrancaDebitoSituacao(cobrancaDebitoSituacao);

		ArrecadadorContratoConvenio arrecadadorContratoConvenio;
		if (contrato != null && contrato.getArrecadadorContratoConvenio() != null) {
			arrecadadorContratoConvenio = contrato.getArrecadadorContratoConvenio();
		} else {
			arrecadadorContratoConvenio = controladorArrecadacao.obterArrecadadorContratoConvenioParaBoletoBancario();
		}
		if (arrecadadorContratoConvenio != null) {
			Long nossoNumeroCalculado = controladorArrecadacao.calcularNossoNumeroComValidacao(arrecadadorContratoConvenio,
					documentoCobranca.getDadosAuditoria());
			documentoCobranca.setNossoNumero(nossoNumeroCalculado);
			documentoCobrancaBoleto.setNossoNumero(nossoNumeroCalculado);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_NAO_CADASTRADO, true);
		}
		if ((documentoCobranca.getItens() != null) && (!documentoCobranca.getItens().isEmpty())) {
			this.popularDocumentoCobranca(idCliente, documentoCobranca, idTipoDocumento);
			inserir(documentoCobranca);
		}
		if ((documentoCobrancaBoleto.getItens() != null) && (!documentoCobrancaBoleto.getItens().isEmpty())) {
			documentoCobrancaBoleto.setDataVencimento(documentoCobranca.getDataVencimento());
			documentoCobrancaBoleto.setDadosAuditoria(documentoCobranca.getDadosAuditoria());
			popularDocumentoCobranca(idCliente, documentoCobrancaBoleto, idTipoDocumento);
			inserir(documentoCobrancaBoleto);
		}

		return new DocumentoCobranca[] {documentoCobranca, documentoCobrancaBoleto};
	}

	private Long obtemFormaCobrancaContrato(Collection<ContratoPontoConsumo> listaContratoPonto, Fatura fatura) {

		Long idFormaCobrancaContrato = 0L;
		if (fatura.getPontoConsumo() != null) {
			idFormaCobrancaContrato = this.obterIdFormaCobrancaDoContrato(fatura.getPontoConsumo(), listaContratoPonto);
		} else {
			Collection<Fatura> listaFaturasAgrupadas = fachada.listarFaturaPorFaturaAgrupada(fatura.getChavePrimaria());
			if (listaFaturasAgrupadas != null && !listaFaturasAgrupadas.isEmpty()) {
				idFormaCobrancaContrato = this.obterIdFormaCobrancaDoContrato(listaFaturasAgrupadas.iterator().next().getPontoConsumo(),
								listaContratoPonto);
			}
		}
		return idFormaCobrancaContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca
	 * #gerarExtratoDebito(java.lang.Long[],
	 * java.lang.Long[], java.lang.Long,
	 * br.com.ggas.faturamento.DocumentoCobranca)
	 */
	@Override
	public byte[] gerarExtratoDebito(Long[] chavesFatura, Long[] chavesCreditoDebito, Long idCliente, DocumentoCobranca documentoCobranca)
			throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idTipoDocumento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_EXTRATO_DEBITO));
		DocumentoCobranca[] documentosParaRelatorio = inserirDocumentoCobranca(chavesFatura, chavesCreditoDebito, idCliente,
						documentoCobranca, Boolean.TRUE, idTipoDocumento);

		DocumentoCobranca documentoCobrancaCodigoBarras = documentosParaRelatorio[0];
		DocumentoCobranca documentoCobrancaBoletoBancario = documentosParaRelatorio[1];

		return gerarRelatoriosDebito(documentoCobrancaCodigoBarras, documentoCobrancaBoletoBancario, idCliente, RELATORIO_EXTRATO_DEBITO,
						RELATORIO_BOLETO_BANCARIO_JASPER, Boolean.FALSE, null);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#gerarRelatoriosDebito(br.com.ggas.faturamento.DocumentoCobranca,
	 * br.com.ggas.faturamento.DocumentoCobranca, java.lang.Long, java.lang.String, java.lang.String, boolean, boolean,
	 * br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public byte[] gerarRelatoriosDebito(DocumentoCobranca documentoCobranca, DocumentoCobranca documentoCobrancaBoleto, Long idCliente,
					String arquivoRelatorioCodigoBarras, String arquivoRelatorioBoleto, boolean isSegundaVia,
					Fatura fatura) throws GGASException {

		List<byte[]> relatorios = new ArrayList<byte[]>();
		byte[] relatorioExtratoDebito = null;

		if ((documentoCobranca.getItens() != null) && (!documentoCobranca.getItens().isEmpty())) {
			relatorioExtratoDebito = this.gerarRelatorioExtratoDebito(documentoCobranca, arquivoRelatorioCodigoBarras, Boolean.TRUE,
							isSegundaVia, null);
			if (relatorioExtratoDebito != null) {
				relatorios.add(relatorioExtratoDebito);
			}
		}
		relatorios.add(construtorRelatorioBoleto(documentoCobrancaBoleto).arrayBytes());

		byte[] relatorioCobranca = null;
		if (!relatorios.isEmpty()) {
			relatorioCobranca = RelatorioUtil.unificarRelatoriosPdf(relatorios);
		}

		return relatorioCobranca;
	}

	private ConstrutorRelatorioBoleto construtorRelatorioBoleto(DocumentoCobranca documentoCobrancaBoleto) throws NegocioException {
		return FabricaConstrutorBoleto.novoConstrutorBoleto(documentoCobrancaBoleto)
				.preencherAgenciaCodigo()
				.preencherNossoNumero()
				.preencherValorDocumento()
				.preencherNumeroCodigoBarras()
				.preencherNumeroDocumento()
				.preencherCedente()
				.preencherImagemLogoBanco()
				.preencherNomeBanco()
				.preencherNumeroBanco()
				.preencherNomeClienteBoleto()
				.preencherEnderecoCliente()
				.preencherDataDocumento()
				.preencherDataVencimento()
				.preencherInstrucoes()
				.preencherDataProcessamento()
				.preencherOutrasDeducoes()
				.preencherDescontosAbatimentos()
				.preencherOutrosAcrescimos()
				.preencherMoraMulta()
				.preencherCarteira()
				.preencherEspecieDocumento()
				.preencherLocalDePagamento()
				.preencherValorTotalBoleto();
	}

	/**
	 * Método responsável por montar um mapa das
	 * parcelas selecionadas para antecipação,
	 * onde a chave do mapa é a chave primária
	 * do creditoDebitoNegociado e o valor é uma
	 * lista de creditoDebitoARealizar que contém
	 * o creditoDebitoNegociado.
	 *
	 * @param listaCreditoDebito
	 *            Lista de creditoDebitoARealizar
	 *            selecionados na tela.
	 * @return Um mapa das parcelas antecipadas.
	 */
	private Map<Long, List<CreditoDebitoARealizar>> montarMapaParcelasAntecipadas(Collection<CreditoDebitoARealizar> listaCreditoDebito) {

		Map<Long, List<CreditoDebitoARealizar>> mapaParcelasAntecipadas = new HashMap<>();
		for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {
			if (mapaParcelasAntecipadas.containsKey(creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria())) {
				List<CreditoDebitoARealizar> listaCDARealizar = mapaParcelasAntecipadas.get(creditoDebitoARealizar
								.getCreditoDebitoNegociado().getChavePrimaria());
				listaCDARealizar.add(creditoDebitoARealizar);
				mapaParcelasAntecipadas.put(creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria(), listaCDARealizar);
			} else {
				List<CreditoDebitoARealizar> listaCDARealizar = new ArrayList<>();
				listaCDARealizar.add(creditoDebitoARealizar);
				mapaParcelasAntecipadas.put(creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria(), listaCDARealizar);
			}
		}
		return mapaParcelasAntecipadas;
	}

	/**
	 * Método responsável por popular um documento
	 * cobranca.
	 *
	 * @param idCliente
	 *            cliente do documento de cobranca
	 * @param docCobranca
	 *            o documento de cobranca
	 * @param idTipoDocumento
	 *            the id tipo documento
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	private void popularDocumentoCobranca(Long idCliente, DocumentoCobranca docCobranca, Long idTipoDocumento)
					throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorArrecadacao controladorArrecadacao =  ServiceLocator.getInstancia().getControladorArrecadacao();

		docCobranca.setDataEmissao(Calendar.getInstance().getTime());
		Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
		docCobranca.setCliente(cliente);
		TipoDocumento tipoDocumento = controladorArrecadacao.obterTipoDocumento(idTipoDocumento);
		docCobranca.setTipoDocumento(tipoDocumento);
		BigDecimal valorTotal = (BigDecimal) obterValoresExtrato(docCobranca).get(VALOR_TOTAL);
		BigDecimal valorCredito = (BigDecimal) obterValoresExtrato(docCobranca).get(NOTASCREDITO);
		BigDecimal valorDebito = (BigDecimal) obterValoresExtrato(docCobranca).get(FATURANOTASDEBITO);
		docCobranca.setValorTotal(valorTotal);
		docCobranca.setValorCredito(valorCredito);
		docCobranca.setValorDebito(valorDebito);
		docCobranca.setVersao(0);
		docCobranca.setUltimaAlteracao(Calendar.getInstance().getTime());
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * obterIdFormaCobrancaDoContrato
	 * (br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Collection)
	 */
	@Override
	public Long obterIdFormaCobrancaDoContrato(PontoConsumo pontoConsumo, Collection<ContratoPontoConsumo> listaContratoPonto) {

		Long idFormaCobrancaContrato = null;

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPonto) {
			if ((pontoConsumo.getChavePrimaria() == contratoPontoConsumo.getPontoConsumo().getChavePrimaria())
							&& (contratoPontoConsumo.getContrato().getFormaCobranca() != null)) {
				idFormaCobrancaContrato = contratoPontoConsumo.getContrato().getFormaCobranca().getChavePrimaria();
				break;
			}
		}
		return idFormaCobrancaContrato;
	}

	/**
	 * Atualizar indicador antecipacao.
	 *
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarIndicadorAntecipacao(CreditoDebitoARealizar creditoDebitoARealizar, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		creditoDebitoARealizar.setDadosAuditoria(dadosAuditoria);
		creditoDebitoARealizar.setIndicadorAntecipacao(Boolean.TRUE);
		creditoDebitoARealizar.setDataAntecipacao(Calendar.getInstance().getTime());
		try {
			super.atualizar(creditoDebitoARealizar, getClasseEntidadeCreditoDebitoARealizar());
		} catch (Exception e) {
			throw new NegocioException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * gerarRelatorioDeclaracaoQuitacaoAnual(java.lang
	 * .String, java.lang.Long,
	 * java.lang.Long, java.lang.String)
	 */
	@Override
	public byte[] gerarRelatorioDeclaracaoQuitacaoAnual(String anoFaturas, Long idCliente, Long idPontoConsumo, String numeroContrato)
					throws GGASException {

		byte[] relatorioDeclaracaoQuitacaoAnual = null;

		if (((idCliente != null) && (idCliente > 0)) || ((idPontoConsumo != null) && (idPontoConsumo > 0))) {
			relatorioDeclaracaoQuitacaoAnual = this.gerarRelatorioQuitacaoAnualPorClienteOuPontoConsumo(idCliente, idPontoConsumo,
							anoFaturas);
		} else if (!StringUtils.isEmpty(numeroContrato)) {
			relatorioDeclaracaoQuitacaoAnual = this.gerarRelatorioDeclaracaoQuitacaoDebitoPorContrato(numeroContrato, anoFaturas);
		}

		return relatorioDeclaracaoQuitacaoAnual;
	}

	/**
	 * Gerar relatorio declaracao quitacao debito por contrato.
	 *
	 * @param numeroContrato
	 *            the numero contrato
	 * @param anoFaturas
	 *            the ano faturas
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private byte[] gerarRelatorioDeclaracaoQuitacaoDebitoPorContrato(String numeroContrato, String anoFaturas) throws GGASException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		byte[] relatorioDeclaracaoQuitacaoAnual = null;

		Collection<Fatura> listaFaturasNaoPagasAnosAnteriores = this.consultarFaturasNaoPagasPorContrato(numeroContrato, anoFaturas,
						Boolean.FALSE);
		Collection<Fatura> listaFaturasNaoPagasAnoAtual = this
						.consultarFaturasNaoPagasPorContrato(numeroContrato, anoFaturas, Boolean.TRUE);
		Collection<Fatura> listaFaturasPagas = null;
		Cliente clienteAssinatura = controladorCliente.obterClienteAssinaturaPeloNumeroContrato(numeroContrato);

		Long idSituacaoParcialmentePaga = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));

		if ((listaFaturasNaoPagasAnosAnteriores != null) && (!listaFaturasNaoPagasAnosAnteriores.isEmpty())) {
			Collection<DadosExtratoDebito> listaDadosExtrato = new ArrayList<>();
			for (Fatura fatura : listaFaturasNaoPagasAnosAnteriores) {
				popularDadosDeclaracaoQuitacaoAnual(fatura, listaDadosExtrato, Boolean.FALSE, idSituacaoParcialmentePaga);
			}
			Map<String, Object> parametros = new HashMap<>();
			parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_GGAS);
			byte[] relatorioDebitosEmAberto = RelatorioUtil.gerarRelatorioPDF(listaDadosExtrato, parametros,
							RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES);
			Util.salvarRelatorioNoDiretorio(relatorioDebitosEmAberto, RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES);
		}
		if ((listaFaturasNaoPagasAnoAtual != null) && (!listaFaturasNaoPagasAnoAtual.isEmpty())) {
			DocumentoCobranca documentoCobranca = this.popularDocumentoCobrancaNaoPersistido(listaFaturasNaoPagasAnoAtual,
							clienteAssinatura.getChavePrimaria(), Boolean.FALSE);
			relatorioDeclaracaoQuitacaoAnual = this.gerarRelatorioExtratoDebito(documentoCobranca, RELATORIO_DECLARACAO_DEBITOS_EM_ABERTO,
							Boolean.FALSE, Boolean.FALSE, null);
		} else if ((listaFaturasNaoPagasAnosAnteriores == null) || (listaFaturasNaoPagasAnosAnteriores.isEmpty())) {
			listaFaturasPagas = this.consultarFaturasPagasPorContrato(numeroContrato, anoFaturas);
			if ((listaFaturasPagas != null) && (!listaFaturasPagas.isEmpty())) {
				gerarRelatorioDeclaracaoQuitacaoAnualGeral(clienteAssinatura.getChavePrimaria(), Long.valueOf(anoFaturas));
			} else {
				throw new NegocioException(ERRO_NEGOCIO_FATURAS_NAO_ENCONTRADAS_ANO_SELECIONADO, true);
			}
		}
		return relatorioDeclaracaoQuitacaoAnual;
	}

	/**
	 * Método responsável por popular os dados do relatório de declaração de quitação anual.
	 *
	 * @param fatura Uma das faturas que será listada no relatório.
	 * @param listaDadosExtrato Lista de VO das faturas do relatório.
	 * @param indicadorPago Indicador para saber se a fatura já foi paga ou não e se será preciso obter o valor dos recebimento da fatura.
	 * @param situacaoParcialmentePaga the situacao parcialmente paga
	 * @throws GGASException the GGAS exception
	 */
	private void popularDadosDeclaracaoQuitacaoAnual(Fatura fatura, Collection<DadosExtratoDebito> listaDadosExtrato,
					boolean indicadorPago, Long situacaoParcialmentePaga) throws GGASException {

		DadosExtratoDebito dadoExtrato = this.criarDadosExtratoDebito();
		dadoExtrato.setNomeCliente(fatura.getCliente().getNome());
		dadoExtrato.setIdentificacao(fatura.getCliente().getNumeroDocumentoFormatado());
		dadoExtrato.setNumero(String.valueOf(fatura.getChavePrimaria()));
		String descricao = this.obterDescricaoFatura(fatura);
		dadoExtrato.setDescricao(descricao);
		dadoExtrato.setVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
		// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
		BigDecimal valorTotal = fatura.getValorSaldoConciliado();
		if ((!indicadorPago) && (fatura.getSituacaoPagamento() != null)
				&& (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoParcialmentePaga)) {
			BigDecimal valorRecebimento = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
			if (valorRecebimento != null) {
				valorTotal = valorTotal.subtract(valorRecebimento);
			}
		}

		dadoExtrato.setValor(Util.converterCampoCurrencyParaString(valorTotal, Constantes.LOCALE_PADRAO));
		listaDadosExtrato.add(dadoExtrato);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca
	 * #gerarRelatorioExtratoDebito
	 * (br.com.ggas.faturamento.DocumentoCobranca,
	 * java.lang.String, boolean)
	 */
	@Override
	public byte[] gerarRelatorioExtratoDebito(DocumentoCobranca documentoCobranca, String arquivoJasper, boolean isExtratoDebito,
					boolean isSegundaVia, Fatura fatura) throws GGASException {

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		byte[] bytes = null;
		if (documentoCobranca != null) {
			Collection<DadosExtratoDebito> listaDadosExtrato = new ArrayList<DadosExtratoDebito>();
			Map<String, Object> parametros = new HashMap<String, Object>();
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("principal", Boolean.TRUE);
			Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);
			Empresa empresa = null;
			if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
				empresa = ((List<Empresa>) listaEmpresas).get(0);
			}
			if (empresa != null && empresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
				parametros.put("razao_social_CDL", empresa.getCliente().getNome());
				parametros.put("endereco_CDL", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroBairroMunicipioUF());
				parametros.put("cep_CDL", empresa.getCliente().getEnderecoPrincipal().getCep().getCep());
				parametros.put("municipio_CDL", empresa.getCliente().getEnderecoPrincipal().getMunicipio().getDescricao() + ", ");
				parametros.put("cnpj_CDL", empresa.getCliente().getCnpjFormatado());
				parametros.put("uf_CDL", empresa.getCliente().getEnderecoPrincipal().getMunicipio().getUnidadeFederacao().getSigla());
				parametros.put("inscricao_estadual_CDL", empresa.getCliente().getInscricaoEstadual());
				
				Boolean rodapeAlgas = Boolean.FALSE;
				
				if("ALGAS - GAS DE ALAGOAS SA".equals(empresa.getCliente().getNome())) {
					rodapeAlgas = Boolean.TRUE;
				}
				
				parametros.put("rodapeAlgas", rodapeAlgas);
				
			}
			this.popularRelatorioMapaCliente(parametros, documentoCobranca, isSegundaVia);
			this.popularRelatorioDetalhamento(parametros, documentoCobranca, listaDadosExtrato, isExtratoDebito);
			if (isExtratoDebito) {
				this.popularRelatorioValoresExtrato(parametros, this.obterValoresExtrato(documentoCobranca));
				DadosCodigoBarras dados = new DadosPadraoCodigoBarras(documentoCobranca);
				CodigoBarras codigoBarras = FabricaCodigoBarras.novoCodigoBarras(dados);
				parametros.put(CODIGO_BARRA, codigoBarras.toString());
				parametros.put(NUMERO_CODIGO_BARRA, codigoBarras.representacaoNumerica().toString());
			}

			Cliente cliente = (Cliente) ServiceLocator.getInstancia().getControladorCliente()
							.obter(documentoCobranca.getCliente().getChavePrimaria(), ENDERECOS, "enderecoPrincipal.cep");

			if (!cliente.getEnderecos().isEmpty()) {
				parametros.put("endereco_cliente", cliente.getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
				parametros.put("cep_cliente", cliente.getEnderecoPrincipal().getCep().getCep());
				parametros.put("cidade", cliente.getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
			}

			parametros.put("destinatario_cliente", cliente.getNome());

			if (!StringUtils.EMPTY.equals(cliente.getCnpjFormatado())) {
				parametros.put(CNPJ_CPF_CLIENTE, cliente.getCnpjFormatado());
				parametros.put(TIPO_PESSOA, "CNPJ:");
			} else {
				parametros.put(CNPJ_CPF_CLIENTE, cliente.getCpfFormatado());
				parametros.put(TIPO_PESSOA, "CPF:");
			}

			if (empresa != null && empresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}

			if (fatura != null) {
				parametros.put("nr_fatura", fatura.getChavePrimaria());
				parametros.put("data_emissao", Util.converterDataParaString(fatura.getDataEmissao()));
				parametros.put("observacao", fatura.getObservacaoNota());
				parametros.put("referencia", Util.formatarAnoMesComMascara(fatura.getAnoMesReferencia()));
			}

			bytes = RelatorioUtil.gerarRelatorioPDF(listaDadosExtrato, parametros, arquivoJasper);
		}
		return bytes;
	}

	/**
	 * Popular relatorio detalhamento.
	 *
	 * @param parametros
	 *            the parametros
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @param listaDadosExtrato
	 *            the lista dados extrato
	 * @param isExtratoDebito
	 *            the is extrato debito
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularRelatorioDetalhamento(Map<String, Object> parametros, DocumentoCobranca documentoCobranca,
					Collection<DadosExtratoDebito> listaDadosExtrato, boolean isExtratoDebito) throws GGASException {

		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		if (documentoCobranca.getDataVencimento() != null) {
			parametros.put("dataVencimento",
							Util.converterDataParaStringSemHora(documentoCobranca.getDataVencimento(), Constantes.FORMATO_DATA_BR));
		}

		ParametroSistema parametroSistema = fachada.obterParametroPorCodigo(Constantes.PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO);

		if (parametroSistema != null) {
			parametros.put("responsavel_CDL", parametroSistema.getValor());
		} else {
			parametros.put("responsavel_CDL", StringUtils.EMPTY);
		}
		Date dataAtual = new Date();
		Date dataVencimentoDocumeto = documentoCobranca.getDataVencimento();

		if (Util.compararDatas(dataAtual, dataVencimentoDocumeto) > 0) {
			int dias = DataUtil.diferencaDiasEntreDatas(dataVencimentoDocumeto, dataAtual);
			parametros.put("diasAtraso", String.valueOf(dias));
		}
		Long idNotaCredito = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));

		Long idNotaDebito = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		Long idNotaDebitoPenalidade = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE));

		List<DocumentoCobrancaItem> listaDocumentoCobrancaFatura = new ArrayList<DocumentoCobrancaItem>();
		List<DocumentoCobrancaItem> listaDocumentoCobrancaCredDebito = new ArrayList<DocumentoCobrancaItem>();
		List<DocumentoCobrancaItem> listaDocumentoCobranca = new ArrayList<DocumentoCobrancaItem>();
		for (DocumentoCobrancaItem item : documentoCobranca.getItens()) {
			if (item.getFaturaGeral() != null) {
				listaDocumentoCobrancaFatura.add(item);
			} else if (item.getCreditoDebitoARealizar() != null) {
				listaDocumentoCobrancaCredDebito.add(item);
			}
		}

		ComparatorChain comparador = new ComparatorChain();
		comparador.addComparator(new BeanComparator("creditoDebitoARealizar.chavePrimaria"));
		Collections.sort(listaDocumentoCobrancaCredDebito, comparador);

		listaDocumentoCobranca.addAll(listaDocumentoCobrancaFatura);
		listaDocumentoCobranca.addAll(listaDocumentoCobrancaCredDebito);

		for (DocumentoCobrancaItem item : listaDocumentoCobranca) {
			DadosExtratoDebito dadoExtrato = this.criarDadosExtratoDebito();

			// Informações do Ponto de Consumo.
			if (item.getPontoConsumo() != null) {
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(item.getPontoConsumo().getChavePrimaria());
				if (pontoConsumo != null) {
					this.popularInfoPontoConsumoRelatorio(dadoExtrato, pontoConsumo, isExtratoDebito);
				}
			}

			// Informações dos documentos
			if (item.getFaturaGeral() != null) {
				Fatura faturaAtual = item.getFaturaGeral().getFaturaAtual();

				Collection<DocumentoFiscal> documentosFiscais = fachada.obterDocumentoFiscalPorFatura(faturaAtual.getChavePrimaria());

				if (documentosFiscais != null && !documentosFiscais.isEmpty()) {
					DocumentoFiscal documentoFiscal = documentosFiscais.iterator().next();
					dadoExtrato.setDocumentoFiscal(String.valueOf(documentoFiscal.getChavePrimaria()));
				} else {
					dadoExtrato.setDocumentoFiscal(StringUtils.EMPTY);
				}

				ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
								ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

				faturaAtual = (Fatura) controladorFatura.obter(faturaAtual.getChavePrimaria(), "listaFaturaItem");

				if (faturaAtual != null) {
					BigDecimal valor = BigDecimal.ZERO;

					if (faturaAtual.getTipoDocumento().getChavePrimaria() == idNotaCredito) {
						parametros.put(VALOR_TOTAL, Util.converterCampoValorDecimalParaString(Fatura.VALOR_TOTAL,
										documentoCobranca.getValorTotal(), Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS));
					}

					if (faturaAtual.getDiasAtraso() != null) {
						dadoExtrato.setDiasAtraso(String.valueOf(faturaAtual.getDiasAtraso()));
					} else {
						dadoExtrato.setDiasAtraso(StringUtils.EMPTY);
					}

					String descricaoConciliacao = StringUtils.EMPTY;
					if (((faturaAtual.getTipoDocumento().getChavePrimaria() == idNotaCredito)
									|| (faturaAtual.getTipoDocumento().getChavePrimaria() == idNotaDebito) || (faturaAtual
									.getTipoDocumento().getChavePrimaria() == idNotaDebitoPenalidade))
									&& (faturaAtual.getValorConciliado().compareTo(BigDecimal.ZERO) > 0)) {

						Collection<FaturaConciliacao> listaFaturasConciliacao = controladorFatura.consultarFaturaConciliacao(faturaAtual
										.getChavePrimaria());

						descricaoConciliacao = obterDescricaoConciliacao(listaFaturasConciliacao, faturaAtual);

					}

					dadoExtrato.setDescricaoConciliacao(descricaoConciliacao);
					dadoExtrato.setDescricaoRecebimentos(obterDescricaoRecebimentos(faturaAtual));

					dadoExtrato.setNumero(String.valueOf(faturaAtual.getChavePrimaria()));
					String descricao = this.obterDescricaoFatura(faturaAtual);

					if ((faturaAtual.getListaFaturaItem() != null) && (!faturaAtual.getListaFaturaItem().isEmpty())) {

						dadoExtrato.setDescricao(descricao);

						// Se for uma antecipação
						// de pagamento, deve
						// retirar o juros de
						// financiamento
						if ((documentoCobranca.getDataVencimento() != null)
										&& (DataUtil.gerarDataHmsZerados(documentoCobranca.getDataVencimento()).before(Util
														.gerarDataSemHora(faturaAtual.getDataVencimento())))) {

							BigDecimal valorTotal = BigDecimal.ZERO;

							for (FaturaItem faturaItem : faturaAtual.getListaFaturaItem()) {

								if (faturaItem != null) {
									dadoExtrato.setQuantidade(String.valueOf(faturaItem.getQuantidade()));
									dadoExtrato.setValorUnitario(String.valueOf(faturaItem.getValorUnitario().setScale(QUATRO_CASAS_DECIMAIS,
													BigDecimal.ROUND_HALF_DOWN)));
								}

								if (faturaItem != null
												&& faturaItem.getCreditoDebitoARealizar() != null
												&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getPercentualJuros() != null) {
									Integer qtdCreditoDebitoCobrado = controladorCreditoDebito.obterQtdCreditoDebitoCobrados(faturaItem
													.getCreditoDebitoARealizar().getChavePrimaria());
									int qtdParcelasAntecipadas = obterQtdParcelasAntecipadas(documentoCobranca.getItens(), faturaItem
													.getCreditoDebitoARealizar().getCreditoDebitoNegociado());
									valor = this.calcularValorPresente(faturaItem.getCreditoDebitoARealizar(), qtdCreditoDebitoCobrado,
													qtdParcelasAntecipadas);

									valorTotal = valorTotal.add(valor);

								} else {
									if (faturaItem != null) {
										valorTotal = valorTotal.add(faturaItem.getValorTotal());
									}
								}

							}

							dadoExtrato.setValor(Util.converterCampoCurrencyParaString(valorTotal, Constantes.LOCALE_PADRAO));
							Extenso valorTotalExtenso = new Extenso(valorTotal);
							dadoExtrato.setValorPorExtenso(valorTotalExtenso.toString());

						} else {
							FaturaItem faturaItem = null;
							for (FaturaItem faturaItemLista : faturaAtual.getListaFaturaItem()) {
								faturaItem = faturaItemLista;
								break;
							}

							if (faturaItem != null) {
								dadoExtrato.setQuantidade(String.valueOf(faturaItem.getQuantidade()));
								dadoExtrato.setValorUnitario(String.valueOf(faturaItem.getValorUnitario().setScale(QUATRO_CASAS_DECIMAIS,
												BigDecimal.ROUND_HALF_DOWN)));
							}

							dadoExtrato.setValor(Util.converterCampoCurrencyParaString(item.getValor(), Constantes.LOCALE_PADRAO));
							dadoExtrato.setValorDoc(item.getValor());
							Extenso valorTotalExtenso = new Extenso(item.getValor());
							dadoExtrato.setValorPorExtenso(valorTotalExtenso.toString());
						}

					}
					if (faturaAtual.getDataVencimento() != null) {
						dadoExtrato.setVencimento(Util.converterDataParaStringSemHora(faturaAtual.getDataVencimento(),
										Constantes.FORMATO_DATA_BR));
					}
					dadoExtrato.setTipo(faturaAtual.getTipoDocumento().getDescricao());
					listaDadosExtrato.add(dadoExtrato);
				}
			} else if (item.getCreditoDebitoARealizar() != null) {
				CreditoDebitoARealizar creditoDebitoARealizar = item.getCreditoDebitoARealizar();

				dadoExtrato.setNumero(String.valueOf(creditoDebitoARealizar.getChavePrimaria()));
				StringBuilder descricao = new StringBuilder();
				descricao.append(creditoDebitoARealizar.getCreditoDebitoNegociado().getRubrica().getDescricao());

				descricao.append("(");
				descricao.append(creditoDebitoARealizar.getNumeroPrestacao());
				descricao.append("/");
				descricao.append(creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes());
				descricao.append(")");
				dadoExtrato.setDescricao(descricao.toString());

				Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
								.obterCDDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				// verificar se pode pegar o primeiro desta lista.
				CreditoDebitoDetalhamento creditoDebitoDetalhamento = ((List<CreditoDebitoDetalhamento>) listaCDDetalhamento).get(0);
				dadoExtrato.setTipo(creditoDebitoDetalhamento.getLancamentoItemContabil().getTipoCreditoDebito().getDescricao());
				if (creditoDebitoARealizar.getDataVencimento() != null) {
					dadoExtrato.setVencimento(Util.converterDataParaStringSemHora(creditoDebitoARealizar.getDataVencimento(),
									Constantes.FORMATO_DATA_BR));
				}

				dadoExtrato.setQuantidade(creditoDebitoARealizar.getQuantidade().toString());
				
				BigDecimal valor = creditoDebitoARealizar.getValor();
				if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
					valor = controladorCreditoDebito.obterSaldoCreditoDebito(creditoDebitoARealizar);
				}

				dadoExtrato.setValor(Util.converterCampoCurrencyParaString(valor, Constantes.LOCALE_PADRAO));
				dadoExtrato.setValorDoc(valor);
				listaDadosExtrato.add(dadoExtrato);
			}
		}

		// Ordenando a lista pelo ponto de consumo para agrupar no relatório de declaração de quitação anual.
		Collections.sort((List<DadosExtratoDebito>) listaDadosExtrato, new Comparator<DadosExtratoDebito>(){

			@Override
			public int compare(DadosExtratoDebito o1, DadosExtratoDebito o2) {

				int retorno = 0;
				if ((o1.getPontoConsumo() != null) && (o2.getPontoConsumo() != null)) {
					retorno = o1.getPontoConsumo().compareTo(o2.getPontoConsumo());
				}
				return retorno;
			}
		});

	}

	/**
	 * Obter descricao recebimentos.
	 *
	 * @param faturaAtual the fatura atual
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String obterDescricaoRecebimentos(Fatura faturaAtual) throws NegocioException {

		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		Collection<Recebimento> listaRecebimentos = controladorRecebimento.consultarRecebimento(faturaAtual.getFaturaGeral());

		StringBuilder descricaoRecebimentos = new StringBuilder();
		if ((listaRecebimentos != null) && (!listaRecebimentos.isEmpty())) {
			for (Recebimento recebimento : listaRecebimentos) {
				if (recebimento.getDataRecebimento() != null) {
					descricaoRecebimentos.append(Util.converterDataParaStringSemHora(recebimento.getDataRecebimento(),
									Constantes.FORMATO_DATA_BR));
					descricaoRecebimentos.append(" - R$ ");

					descricaoRecebimentos.append(Util.converterCampoCurrencyParaString(recebimento.getValorRecebimento(),
									Constantes.LOCALE_PADRAO));
					descricaoRecebimentos.append("; ");
				}
			}
		}

		return descricaoRecebimentos.toString();
	}

	/**
	 * Obter descricao conciliacao.
	 *
	 * @param listaFaturasConciliacao the lista faturas conciliacao
	 * @param faturaAtual the fatura atual
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String obterDescricaoConciliacao(Collection<FaturaConciliacao> listaFaturasConciliacao, Fatura faturaAtual) {

		StringBuilder descricaoConciliacao = new StringBuilder();
		if ((listaFaturasConciliacao != null) && (!listaFaturasConciliacao.isEmpty())) {
			for (FaturaConciliacao faturaConciliacao : listaFaturasConciliacao) {
				if (faturaConciliacao.getCredito().getFaturaAtual().getChavePrimaria() != faturaAtual.getChavePrimaria()) {
					descricaoConciliacao.append(String.valueOf(faturaConciliacao.getCredito().getFaturaAtual().getChavePrimaria()));
				} else if (faturaConciliacao.getDebito().getFaturaAtual().getChavePrimaria() != faturaAtual.getChavePrimaria()) {
					descricaoConciliacao.append(String.valueOf(faturaConciliacao.getDebito().getFaturaAtual().getChavePrimaria()));
				}
				descricaoConciliacao.append(" - R$ ");
				descricaoConciliacao.append(Util.converterCampoCurrencyParaString(faturaConciliacao.getValorConciliacao(),
								Constantes.LOCALE_PADRAO));
				descricaoConciliacao.append("; ");
			}
		}

		return descricaoConciliacao.toString();
	}

	/**
	 * Método responsável por obter a quantodade de parcelas selecionadas na tela para antecipação.
	 *
	 * @param listaItens the lista itens
	 * @param creditoDebitoNegociado CreditoDebitoNegociado.
	 * @return A quantidade de creditoDebito selecionados para antecipação.
	 */
	private int obterQtdParcelasAntecipadas(Collection<DocumentoCobrancaItem> listaItens, CreditoDebitoNegociado creditoDebitoNegociado) {

		int count = 0;
		for (DocumentoCobrancaItem item : listaItens) {
			if ((item.getCreditoDebitoARealizar() != null)
							&& (item.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getChavePrimaria() == creditoDebitoNegociado
											.getChavePrimaria())) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Obter descricao fatura.
	 *
	 * @param faturaAtual
	 *            the fatura atual
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private String obterDescricaoFatura(Fatura faturaAtual) throws GGASException {

		StringBuilder desc = new StringBuilder();
		// : quando o fatura for implementado
		// poderá ter mais de uma fatura item e a
		// observação será repetida, se houver.

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		Long idNotaDebitoPenalidade = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE));

		// se o tipo de documento fatura for nota de debito penalidade... vai listar todas as penalidades e jogar na descrição
		if (faturaAtual.getTipoDocumento().getChavePrimaria() == idNotaDebitoPenalidade) {
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoPenalidadePeri = controladorApuracaoPenalidade
							.listarApuracaoQuantidadePenalidadePeriodicidadePorChaveFatura(new Long[] {faturaAtual.getChavePrimaria()});
			if (listaApuracaoPenalidadePeri != null) {
				BigDecimal saldoQpnr = BigDecimal.ZERO;
				desc.append(NOTA_DEBITO_PENALIDADE_REFERENTE);
				desc.append(faturaAtual.getPontoConsumo().getDescricao());
				desc.append("  ");
				desc.append(QUEBRA_LINHA);
				for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : listaApuracaoPenalidadePeri) {
					BigDecimal qnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada();

					String qnrTexto;

					if (qnr != null) {
						qnrTexto = Util.converterCampoValorDecimalParaString("QNR", qnr, Constantes.LOCALE_PADRAO);
					} else {
						qnrTexto = Util.converterCampoValorDecimalParaString("QNR", BigDecimal.ZERO, Constantes.LOCALE_PADRAO);
					}

					BigDecimal qRet = apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetirada();

					String qretTexto;

					if (qRet != null) {
						qretTexto = Util.converterCampoValorDecimalParaString("Quantidade Retirada", qRet, Constantes.LOCALE_PADRAO);
					} else {
						qretTexto = Util.converterCampoValorDecimalParaString("Quantidade Retirada", BigDecimal.ZERO,
										Constantes.LOCALE_PADRAO);
					}

					BigDecimal qRecuperada = apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada();

					String qRecuperadaTexto ;

					if (qRecuperada != null) {
						qRecuperadaTexto = Util.converterCampoValorDecimalParaString("quantidade Recuperada", qRecuperada,
										Constantes.LOCALE_PADRAO);
					} else {
						qRecuperadaTexto = Util.converterCampoValorDecimalParaString("quantidade Recuperada", BigDecimal.ZERO,
										Constantes.LOCALE_PADRAO);
					}


					BigDecimal qpnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
					if (qpnr == null) {
						qpnr = BigDecimal.ZERO;
					}

					Date dataInicial = apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao();
					Date dataFim = apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao();
					String dataInicialTexto = Util.converterDataParaString(dataInicial);

					String dataFimTexto = Util.converterDataParaString(dataFim);

					saldoQpnr = qpnr.add(saldoQpnr);

					desc.append("*");
					desc.append("No periodo de: ");
					desc.append(dataInicialTexto);
					desc.append(" a ");
					desc.append(dataFimTexto);
					desc.append("  ");
					desc.append(QNR);
					desc.append(": ");
					desc.append(qnrTexto);
					desc.append(Constantes.STRING_VIRGULA_ESPACO);
					desc.append(QRET);
					desc.append(": ");
					desc.append(qretTexto);
					desc.append(Constantes.STRING_VIRGULA_ESPACO);
					desc.append(QREC);
					desc.append(": ");
					desc.append(qRecuperadaTexto);
					desc.append(".");
					desc.append(QUEBRA_LINHA);

				}
				String saldoTexto = Util.converterCampoValorDecimalParaString("Saldo QPNR", saldoQpnr, Constantes.LOCALE_PADRAO);
				desc.append("Saldo da QPNR:");
				desc.append(saldoTexto);
			}
		} else {

			for (FaturaItem faturaItem : faturaAtual.getListaFaturaItem()) {
				if (faturaItem.getRubrica() != null) {
					if (!StringUtils.isEmpty(faturaItem.getRubrica().getDescricaoImpressao())) {
						desc.append(faturaItem.getRubrica().getDescricaoImpressao());
					}
					if ((faturaItem.getRubrica().getPermiteDescricaoComplementar())
									&& (!StringUtils.isEmpty(faturaAtual.getObservacaoNota()))) {
						desc.append(" - ");
						desc.append(faturaAtual.getObservacaoNota());
					}
					desc.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		String descricao = desc.toString();
		if (descricao.length() > 0) {
			descricao = descricao.substring(0, descricao.length() - LIMITE_DESCRICAO);
		}

		return descricao;
	}

	/**
	 * Popular info ponto consumo relatorio.
	 *
	 * @param dadoExtrato
	 *            the dado extrato
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param isExtratoDebito
	 *            the is extrato debito
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularInfoPontoConsumoRelatorio(DadosExtratoDebito dadoExtrato, PontoConsumo pontoConsumo, boolean isExtratoDebito)
					throws NegocioException {

		ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();
		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();
		ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		dadoExtrato.setPontoConsumo(pontoConsumo.getImovel().getNome() + " - " + pontoConsumo.getDescricao());

		if (pontoConsumo.getSegmento() != null) {
			Segmento segmento = (Segmento) controladorSegmento.obter(pontoConsumo.getSegmento().getChavePrimaria());
			dadoExtrato.setSegmento(segmento.getDescricao());
		}

		if (pontoConsumo.getSituacaoConsumo() != null) {
			SituacaoConsumo situacaoConsumo = controladorImovel.obterSituacaoConsumo(pontoConsumo.getSituacaoConsumo().getChavePrimaria());
			dadoExtrato.setSituacao(situacaoConsumo.getDescricao());
		}

		if (pontoConsumo.getQuadraFace().getEndereco().getCep() != null) {
			Cep cep = controladorEndereco.obterCepPorChave(pontoConsumo.getQuadraFace().getEndereco().getCep().getChavePrimaria());
			dadoExtrato.setCep(cep.getCep());
		}

		if (!isExtratoDebito) {
			ContratoPontoConsumo contratoPonto = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo
							.getChavePrimaria());
			if (contratoPonto != null) {
				if (contratoPonto.getCep() != null) {
					dadoExtrato.setEndereco(contratoPonto.getEnderecoFormatado());
				} else {
					Cliente cliente = contratoPonto.getContrato().getClienteAssinatura();
					if (cliente.getEnderecoPrincipal() != null) {
						dadoExtrato.setEndereco(cliente.getEnderecoPrincipal().getEnderecoFormatado());
					}
				}
			}
		}
	}

	/**
	 * Popular relatorio valores extrato.
	 *
	 * @param parametros the parametros
	 * @param valoresExtrato the valores extrato
	 * @throws NegocioException the negocio exception
	 */
	private void popularRelatorioValoresExtrato(Map<String, Object> parametros, Map<String, Object> valoresExtrato) throws NegocioException {

		// verificar onde podem ser setadas
		// as
		// constantes para os parâmetros do
		// relatório.
		BigDecimal documentos = (BigDecimal) valoresExtrato.get(DOCUMENTOS);
		parametros.put(TOTAL_DOCUMENTOS, Util.converterCampoCurrencyParaString(documentos, Constantes.LOCALE_PADRAO));

		try {
			BigDecimal debitos = (BigDecimal) valoresExtrato.get(DEBITOS);
			parametros.put(TOTAL_DEBITOS, Util.converterCampoCurrencyParaString(debitos, Constantes.LOCALE_PADRAO));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, new String[] { "Débitos" });
		}
		try {
			BigDecimal creditos = (BigDecimal) valoresExtrato.get(CREDITOS);
			parametros.put(TOTAL_CREDITOS, Util.converterCampoCurrencyParaString(creditos, Constantes.LOCALE_PADRAO));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, new String[] { "Créditos" });
		}
		try {
			BigDecimal valorTotal = (BigDecimal) valoresExtrato.get(VALOR_TOTAL);
			parametros.put(VALOR_TOTAL, Util.converterCampoCurrencyParaString(valorTotal, Constantes.LOCALE_PADRAO));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, new String[] { "Valor Total" });
		}
	}

	/**
	 * Popular relatorio mapa cliente.
	 *
	 * @param parametros
	 *            the parametros
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @param isSegundaVia
	 *            the is segunda via
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularRelatorioMapaCliente(Map<String, Object> parametros, DocumentoCobranca documentoCobranca, boolean isSegundaVia)
					throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		parametros.put("indicadorSegundaVia", String.valueOf(isSegundaVia));

		Cliente cliente = null;
		if (documentoCobranca.getCliente() != null) {
			cliente = (Cliente) controladorCliente.obter(documentoCobranca.getCliente().getChavePrimaria(), ENDERECOS);

			parametros.put(NOME_CLIENTE, cliente.getNome());

			if (!StringUtils.isEmpty(cliente.getCnpjFormatado())) {
				parametros.put(DOC_IDENTIFICADOR, cliente.getCnpjFormatado());
			} else if (!StringUtils.isEmpty(cliente.getCpfFormatado())) {
				parametros.put(DOC_IDENTIFICADOR, cliente.getCpfFormatado());
			}

			String endereco = obterEnderecoRelatorio(documentoCobranca, cliente);
			parametros.put(ENDERECO, endereco);

			parametros.put(CODIGO_CLIENTE, String.valueOf(cliente.getChavePrimaria()));

			if (cliente.getInscricaoEstadual() != null) {
				parametros.put(INSCRICAO_ESTADUAL, cliente.getInscricaoEstadual());
			} else if (cliente.getRg() != null) {
				parametros.put(INSCRICAO_ESTADUAL, cliente.getRg());
			}
			if (cliente.getTipoCliente() != null) {
				parametros.put(TIPO_CLIENTE, cliente.getTipoCliente().getDescricao());
			}

			if (cliente.getContatos() != null && !cliente.getContatos().isEmpty()) {
				ContatoCliente contato = cliente.getContatos().iterator().next();
				parametros.put("contato_cliente_nome", contato.getNome());
				parametros.put("contato_cliente_email", contato.getEmail());
				parametros.put("contato_cliente_telefone1", String.valueOf(contato.getFone() + " / "));
				parametros.put("contato_cliente_telefone2", String.valueOf(contato.getFone2()));
				parametros.put("contato_cliente_DDD", "(" + contato.getCodigoDDD() + ")");
				parametros.put("contato_cliente_DDD2", "(" + contato.getCodigoDDD2() + ")");
			}

		}
	}

	/**
	 * Obter endereco relatorio.
	 *
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @param cliente
	 *            the cliente
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String obterEnderecoRelatorio(DocumentoCobranca documentoCobranca, Cliente cliente) throws NegocioException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		Collection<DocumentoCobrancaItem> listaDocumentoCobranca = documentoCobranca.getItens();
		PontoConsumo pontoConsumo = null;
		if (listaDocumentoCobranca != null) {
			for (DocumentoCobrancaItem documentoCobrancaItem : listaDocumentoCobranca) {
				if (documentoCobrancaItem.getPontoConsumo() != null) {
					if ((pontoConsumo == null) || documentoCobrancaItem.getPontoConsumo().equals(pontoConsumo)) {
						pontoConsumo = documentoCobrancaItem.getPontoConsumo();

						// Se tiver mais de um
						// ponto de consumo no
						// documento de cobrança
						// vai usar o endereço do
						// cliente.
					} else {
						pontoConsumo = null;
						break;
					}

					// Se tiver algum item de
					// documento de cobrança sem
					// ponto de consumo vai usar o
					// endereço do cliente.
				} else {
					pontoConsumo = null;
					break;
				}
			}
		}

		String endereco = StringUtils.EMPTY;
		if (pontoConsumo != null) {
			ContratoPontoConsumo contratoPontoConsumo = null;
			contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
			// TKT 3332 - OK
			if (contratoPontoConsumo == null) {
				contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
								.getChavePrimaria());
			}

			if ((contratoPontoConsumo != null) && (!StringUtils.isEmpty(contratoPontoConsumo.getEnderecoFormatado()))) {
				endereco = contratoPontoConsumo.getEnderecoFormatado();
			}
		}
		if (StringUtils.isEmpty(endereco) && cliente.getEnderecoPrincipal() != null
						&& cliente.getEnderecoPrincipal().getEnderecoFormatado() != null) {
			endereco = cliente.getEnderecoPrincipal().getEnderecoFormatado();
		}
		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca
	 * #gerarNumeroCodigoBarra
	 * (br.com.ggas.faturamento.DocumentoCobranca,
	 * boolean)
	 */
	@Override
	public Map<String, Object> gerarNumeroCodigoBarra(DocumentoCobranca documentoCobranca, BigDecimal acumudado)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Map<String, Object> mapaCodigoBarra = new HashMap<String, Object>();
		String codigoBarra = StringUtils.EMPTY;

		// identificação do Produto
		String identProduto = Constantes.ID_PRODUTO_ARRECADACAO;
		codigoBarra += identProduto;

		// Identificação do Segmento
		String identSegmento = Constantes.ID_SEGMENTO_GAS;
		codigoBarra += identSegmento;

		// Identificação do valor real ou
		// referência
		String identValorReal = Constantes.ID_VALOR_REAL;
		codigoBarra += identValorReal;

		// Valor
		BigDecimal valorExtrato = (BigDecimal) this.obterValoresExtrato(documentoCobranca).get(VALOR_TOTAL);
		valorExtrato = valorExtrato.add(acumudado);
		String valorTotalFormatado = Util.adicionarZerosEsquerdaNumero(String.valueOf(valorExtrato).replace(".", StringUtils.EMPTY), ONZE_ZEROS);
		codigoBarra += valorTotalFormatado;

		// Identificação da Empresa/Órgão
		String identEmpresaOrgao = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(CODIGO_EMPRESA_FEBRABAN);
		identEmpresaOrgao = Util.adicionarZerosEsquerdaNumero(identEmpresaOrgao, QUATRO_ZEROS);
		codigoBarra += identEmpresaOrgao;

		// O 3° e 4° bloco do código de barra são compostos pelo número do documento, cada um com 11 digito.
		codigoBarra += "000";

		// Número do documento 3° bloco
		String numeroDocumento = Util.adicionarZerosEsquerdaNumero(String.valueOf(documentoCobranca.getChavePrimaria()), ONZE_ZEROS);
		codigoBarra += numeroDocumento;

		// o Número do documento se repete no 4° bloco na composição do código de barras para Algas.
		codigoBarra += numeroDocumento;

		// DAC(Dígito de Auto-Conferência) geral.
		String dacGeral = String.valueOf(DigitoAutoConferencia.modulo10(codigoBarra));

		codigoBarra = identProduto + identSegmento + identValorReal + dacGeral + valorTotalFormatado + identEmpresaOrgao + "000"
						+ numeroDocumento + numeroDocumento;

		String cbBloco1 = codigoBarra.substring(NUMERO_ZERO, ONZE_ZEROS);
		String dacBloco1 = String.valueOf(DigitoAutoConferencia.modulo10(cbBloco1));

		String cbBloco2 = codigoBarra.substring(ONZE_ZEROS, NUMERO_VINTE_DOIS);
		String dacBloco2 = String.valueOf(DigitoAutoConferencia.modulo10(cbBloco2));

		String cbBloco3 = codigoBarra.substring(NUMERO_VINTE_DOIS);
		String dacBolco3 = String.valueOf(DigitoAutoConferencia.modulo10(cbBloco3));

		String cbBloco4 = codigoBarra.substring(NUMERO_TRINTA_TRES, NUMERO_QUARENTA_QUATRO);
		String dacBloco4 = String.valueOf(DigitoAutoConferencia.modulo10(cbBloco4));

		String cbComDigitos = cbBloco1 + "-" + dacBloco1 + " " + cbBloco2 + "-" + dacBloco2 + " " + cbBloco3 + "-" + dacBolco3 + " "
						+ cbBloco4 + "-" + dacBloco4;

		String cbSemDigitos = cbBloco1 + cbBloco2 + cbBloco3 + cbBloco4;

		mapaCodigoBarra.put(CODIGO_BARRA_COM_DIGITOS, cbComDigitos);
		mapaCodigoBarra.put(CODIGO_BARRA_SEM_DIGITOS, cbSemDigitos);

		return mapaCodigoBarra;
	}

	/**
	 * Obter valores extrato.
	 *
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<String, Object> obterValoresExtrato(DocumentoCobranca documentoCobranca) throws NegocioException {

		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Map<String, Object> valoresExtrato = new HashMap<String, Object>();

		BigDecimal faturasNotasDebito = BigDecimal.ZERO;
		BigDecimal notasCredito = BigDecimal.ZERO;
		BigDecimal debitos = BigDecimal.ZERO;
		BigDecimal creditos = BigDecimal.ZERO;

		String chaveTipoDocumentoFatura = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		String chaveTipoDocumentoNotasDebito = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO);
		String chaveTipoDocumentoNotasDebitoPenalidade = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE);
		String chaveTipoDocumentoNotasCredito = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO);

		TipoDocumento tipoDocumentoFatura = controladorArrecadacao.obterTipoDocumento(Long.valueOf(chaveTipoDocumentoFatura));
		TipoDocumento tipoDocumentoNotasDebito = controladorArrecadacao.obterTipoDocumento(Long.valueOf(chaveTipoDocumentoNotasDebito));
		TipoDocumento tipoDocumentoNotasDebitoPenalidade = controladorArrecadacao.obterTipoDocumento(Long
						.valueOf(chaveTipoDocumentoNotasDebitoPenalidade));
		TipoDocumento tipoDocumentoNotasCredito = controladorArrecadacao.obterTipoDocumento(Long.valueOf(chaveTipoDocumentoNotasCredito));

		String chaveTipoDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);
		String chaveTipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);

		EntidadeConteudo tipoDebito = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(chaveTipoDebito));
		EntidadeConteudo tipoCredito = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(chaveTipoCredito));

		for (DocumentoCobrancaItem item : documentoCobranca.getItens()) {
			if (item.getFaturaGeral() != null) {
				Fatura faturaAtual = item.getFaturaGeral().getFaturaAtual();
				if (faturaAtual != null && faturaAtual.getListaFaturaItem() != null) {
					// : diminuir valor de recebimento.
					BigDecimal valorTotal = item.getFaturaGeral().getFaturaAtual().getValorTotal();

					// Se for uma antecipação de pagamento, deve retirar o juros de financiamento
					if ((documentoCobranca.getDataVencimento() != null)
									&& (DataUtil.gerarDataHmsZerados(documentoCobranca.getDataVencimento()).before(Util
													.gerarDataSemHora(faturaAtual.getDataVencimento())))) {

						BigDecimal valor = BigDecimal.ZERO;
						Collection<FaturaItem> listaFaturaItem = controladorFatura.listarFaturaItemPorChaveFatura(faturaAtual
										.getChavePrimaria());
						for (FaturaItem faturaItem : listaFaturaItem) {
							if (faturaItem.getCreditoDebitoARealizar() != null
											&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getPercentualJuros() != null) {
								Integer qtdCreditoDebitoCobrado = controladorCreditoDebito.obterQtdCreditoDebitoCobrados(faturaItem
												.getCreditoDebitoARealizar().getChavePrimaria());
								int qtdParcelasAntecipadas = obterQtdParcelasAntecipadas(documentoCobranca.getItens(), faturaItem
												.getCreditoDebitoARealizar().getCreditoDebitoNegociado());
								BigDecimal valorAux = this.calcularValorPresente(faturaItem.getCreditoDebitoARealizar(),
												qtdCreditoDebitoCobrado, qtdParcelasAntecipadas);

								valor = valor.add(valorAux);

							} else {
								valor = valor.add(faturaItem.getValorTotal());
							}

						}

						valorTotal = valor;

					}

					// Se for uma Fatura ou notas de débito
					if (tipoDocumentoFatura.getChavePrimaria() == faturaAtual.getTipoDocumento().getChavePrimaria()
									|| tipoDocumentoNotasDebito.getChavePrimaria() == faturaAtual.getTipoDocumento().getChavePrimaria()
									|| tipoDocumentoNotasDebitoPenalidade.getChavePrimaria() == faturaAtual.getTipoDocumento()
													.getChavePrimaria()) {
						faturasNotasDebito = faturasNotasDebito.add(valorTotal);
					}
					// Se for notas de crédito.
					if (tipoDocumentoNotasCredito.getChavePrimaria() == faturaAtual.getTipoDocumento().getChavePrimaria()) {
						notasCredito = notasCredito.add(valorTotal);
					}

				}

			} else if (item.getCreditoDebitoARealizar() != null) {
				Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
								.obterCDDetalhamentoPeloCDARealizar(item.getCreditoDebitoARealizar().getChavePrimaria());
				//[pburlamaqui]: verificar com luciene se pode pegar o primeiro desta lista.
				CreditoDebitoDetalhamento creditoDebitoDetalhamento = ((List<CreditoDebitoDetalhamento>) listaCDDetalhamento).get(0);
				EntidadeConteudo tipoDebitoCredito = creditoDebitoDetalhamento.getLancamentoItemContabil().getTipoCreditoDebito();
				// Se for débito
				if (tipoDebito.getChavePrimaria() == tipoDebitoCredito.getChavePrimaria()) {
					// verificar com analistas se aqui o valor também pode ser recalculado. / agrupar os débitos de creditoDebitoPrincipal.
					debitos = debitos.add(item.getValor());
				}
				// se for crédito
				if (tipoCredito.getChavePrimaria() == tipoDebitoCredito.getChavePrimaria()) {
					creditos = creditos.add(item.getValor());
				}

			}
		}

		// Cálculo documentos
		BigDecimal documentos = faturasNotasDebito.subtract(notasCredito);
		// Cálculo valor Total
		BigDecimal valorTotal = BigDecimal.ZERO;
		BigDecimal valor = documentos.add(debitos);
		if (valor.compareTo(creditos) >= 0) {
			valorTotal = (documentos.add(debitos)).subtract(creditos);
		} else {
			throw new NegocioException(ERRO_NEGOCIO_VALOR_CREDITO_MAIOR_DEBITO, true);
		}

		// Adicionando valores no mapa.
		valoresExtrato.put(DOCUMENTOS, documentos.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
		valoresExtrato.put(DEBITOS, debitos.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
		valoresExtrato.put(CREDITOS, creditos.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
		valoresExtrato.put(FATURANOTASDEBITO, faturasNotasDebito.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
		valoresExtrato.put(NOTASCREDITO, notasCredito.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
		valoresExtrato.put(VALOR_TOTAL, valorTotal.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));

		return valoresExtrato;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * validarFiltroPesquisaExtratoDebito(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public void validarFiltroPesquisaExtratoDebito(Long idCliente, Long idPontoConsumo) throws NegocioException {

		if (((idCliente == null) || (idCliente <= 0)) && ((idPontoConsumo == null) || (idPontoConsumo <= 0))) {
			throw new NegocioException(ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * obterValorRecebimentoPelaFatura(java.lang.Long
	 * )
	 */
	@Override
	public BigDecimal obterValorRecebimentoPelaFatura(Long idFatura) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder hql = new StringBuilder();
		hql.append(" select sum(recebimento.valorRecebimento) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRecebimento().getSimpleName());
		hql.append(" recebimento ");
		hql.append(WHERE);
		hql.append(" recebimento.faturaGeral.faturaAtual.chavePrimaria = :idFatura ");
		hql.append(" and recebimento.recebimentoSituacao.chavePrimaria != :idSituacaoRecebimento ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idFatura", idFatura);
		query.setLong("idSituacaoRecebimento",
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_ESTORNADO)));

		return (BigDecimal) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * obterUltimaDataRecebimentoPelaFatura(java.lang
	 * .Date)
	 */
	@Override
	public Date obterUltimaDataRecebimentoPelaFatura(Long idFatura) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(recebimento.dataRecebimento) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRecebimento().getSimpleName());
		hql.append(" recebimento ");
		hql.append(WHERE);
		hql.append(" recebimento.faturaGeral.faturaAtual.chavePrimaria = :idFatura ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idFatura", idFatura);

		return (Date) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * obterUltimaDataRecebimentoPelaFatura(java.lang
	 * .Date)
	 */
	@Override
	public Date obterUltimaDataConciliacaoPelaFatura(Long idFatura) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(faturaConciliacao.dataConciliacao) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeFaturaConciliacao().getSimpleName());
		hql.append(" faturaConciliacao ");
		hql.append(WHERE);
		hql.append(" faturaConciliacao.debito.faturaAtual.chavePrimaria = :idFatura ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idFatura", idFatura);

		return (Date) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Recebimento> obterRecebimentoClassificadoPelaFatura(Long idFatura) throws NegocioException {
		Collection<String> nomesConstantesSituacaoRecebimento = obterNomesConstantes();
		Collection<Long> valoresConstantesSituacaoRecebimento = obterValoresDasConstantes(nomesConstantesSituacaoRecebimento);
		Criteria criteria = construirCriterio(idFatura, valoresConstantesSituacaoRecebimento);
		return criteria.list();
	}

	private Collection<String> obterNomesConstantes() {
		Collection<String> constantesSituacaoRecebimento = new ArrayList<String>();
		constantesSituacaoRecebimento.add(Constantes.C_RECEBIMENTO_CLASSIFICADO);
		constantesSituacaoRecebimento.add(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE);
		constantesSituacaoRecebimento.add(Constantes.C_RECEBIMENTO_BAIXA_POR_DACAO);
		return constantesSituacaoRecebimento;
	}

	private Collection<Long> obterValoresDasConstantes(Collection<String> nomesConstantesSituacaoRecebimento) throws NegocioException {
		Collection<Long> valores = new ArrayList<Long>();
		ControladorConstanteSistema controladorConstanteSistema = getControladorConstanteSistema();
		for (String nomeConstante : nomesConstantesSituacaoRecebimento) {
			String valorStr = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(nomeConstante);
			try {
				Long valor = Long.valueOf(valorStr);
				valores.add(valor);
			} catch (NumberFormatException e) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CONSTANTE_NAO_CONFIGURADA, valorStr);
			}
		}
		return valores;
	}

	private Criteria construirCriterio(Long idFatura, Collection<Long> valoresConstantesSituacaoRecebimento) {
		Criteria criteria = createCriteria(getClasseEntidadeRecebimento(), "recebimento");
		criteria.createAlias("recebimento.recebimentoSituacao", "recebimentoSituacao");
		criteria.createAlias("recebimento.faturaGeral", "faturaGeral");
		criteria.createAlias("faturaGeral.faturaAtual", "faturaAtual");

		Criterion restricaoIdFaturaAtual = Restrictions.eq("faturaAtual.chavePrimaria", idFatura);
		Criterion restricaoSituacao = Restrictions
						.in("recebimento.recebimentoSituacao.chavePrimaria", valoresConstantesSituacaoRecebimento);

		criteria.add(restricaoIdFaturaAtual);
		criteria.add(restricaoSituacao);
		criteria.addOrder(Order.asc("recebimento.dataRecebimento"));
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * consultarFaturasPorClienteOuPontoConsumo(java
	 * .lang.Long, java.lang.Long,
	 * java.lang.String, java.lang.Long,
	 * java.lang.Long[], java.lang.Boolean)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Fatura> consultarFaturasPorClienteOuPontoConsumo(Long idCliente, Long idPontoConsumo, String anoFatura,
					Long chaveSituacaoPagamento, Long[] chavesTipoDocumento, Boolean anoAtual) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select fatura ");
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" inner join fetch fatura.cliente ");
		hql.append(" inner join fetch fatura.pontoConsumo ");
		hql.append(" inner join fetch fatura.situacaoPagamento ");
		hql.append(" inner join fetch fatura.tipoDocumento ");
		hql.append(WHERE);

		// Refeactoring de Faturamento
		// agrupado
		hql.append(" fatura.faturaAgrupada is null ");
		hql.append(" and fatura.contrato is null and ");

		hql.append(" fatura.tipoDocumento.chavePrimaria in (:chavesTipoDocumento)");

		// a consulta so podera ser feita por
		// cliente ou por ponto de consumo :
		// eles sao mutualmente exclusivos entre
		// si.
		if ((idCliente != null) && (idCliente.intValue() > 0)) {
			hql.append(" and fatura.cliente.chavePrimaria = :idCliente ");
		} else if ((idPontoConsumo != null) && (idPontoConsumo.intValue() > 0)) {
			hql.append("and fatura.pontoConsumo.chavePrimaria = :idPontoConsumo");
		}

		// pesquisar no ano atual ou em anos
		// anteriores
		if (anoAtual) {
			hql.append(" and cast(year(fatura.dataVencimento) as string) = :anoFatura ");
		} else {
			hql.append(" and cast(year(fatura.dataVencimento) as string) < :anoFatura ");
		}

		// Situacao do Pagamento :
		// Paga,Pendente,Parcialmente Paga, etc.
		if (chaveSituacaoPagamento != null && chaveSituacaoPagamento.intValue() > 0) {
			hql.append(" and fatura.situacaoPagamento.chavePrimaria = :chaveSituacaoPagamento");
		}

		hql.append(" order by fatura.pontoConsumo.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesTipoDocumento", chavesTipoDocumento);
		query.setParameter("anoFatura", anoFatura);

		if ((chaveSituacaoPagamento != null) && (chaveSituacaoPagamento.intValue() > 0)) {
			query.setLong("chaveSituacaoPagamento", chaveSituacaoPagamento);
		}

		if ((idCliente != null) && (idCliente.intValue() > 0)) {
			query.setLong("idCliente", idCliente);
		} else if ((idPontoConsumo != null) && (idPontoConsumo.intValue() > 0)) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}

		return query.list();
	}

	/**
	 * Consultar debitos a cobrar.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @param idEntidadeConteudo
	 *            the id entidade conteudo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<CreditoDebitoARealizar> consultarDebitosACobrar(Long idCliente, Long idEntidadeConteudo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select debitosPagos ");
		hql.append(FROM);
		hql.append(getClasseEntidadeCreditoDebitoARealizar().getSimpleName());
		hql.append(" debitosPagos ");
		hql.append(" inner join fetch debitosPagos.cliente cliente");
		hql.append(" inner join fetch debitosPagos.lancamentoItemContabil itemContabil");
		hql.append(" inner join fetch itemContabil.tipoCreditoDebito debito");
		hql.append(WHERE);
		hql.append(" cliente.chavePrimaria = :idCliente ");
		hql.append(" and debito.chavePrimaria = :idEntidadeConteudo");
		hql.append(" order by debitos.pontoConsumo.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCliente", idCliente);
		query.setLong("idEntidadeConteudo", idEntidadeConteudo);

		return query.list();
	}

	/**
	 * Gerar relatorio quitacao anual por cliente ou ponto consumo.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param anoFatura
	 *            the ano fatura
	 * @return the byte[]
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public byte[] gerarRelatorioQuitacaoAnualPorClienteOuPontoConsumo(Long idCliente, Long idPontoConsumo, String anoFatura)
					throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<Fatura> listaFaturasPendentes = null;
		Collection<Fatura> listaFaturasParcialPaga = null;
		Collection<Fatura> listaFaturasAnosAnterioresPendentes = null;
		Collection<Fatura> listaFaturasAnosAnterioresParcialPaga = null;

		String pagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		String parcialmentePaga = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
		String pagamentoEfetivado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);
		String tipoFatura = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		String tipoNotaDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO);

		Long[] chavesTipoDocumento = new Long[] {Long.parseLong(tipoFatura), Long.parseLong(tipoNotaDebito)};

		listaFaturasPendentes = this.consultarFaturasPorClienteOuPontoConsumo(idCliente, idPontoConsumo, anoFatura,
						Long.valueOf(pagamentoPendente), chavesTipoDocumento, Boolean.TRUE);

		listaFaturasParcialPaga = this.consultarFaturasPorClienteOuPontoConsumo(idCliente, idPontoConsumo, anoFatura,
						Long.valueOf(parcialmentePaga), chavesTipoDocumento, Boolean.TRUE);

		listaFaturasAnosAnterioresPendentes = this.consultarFaturasPorClienteOuPontoConsumo(idCliente, idPontoConsumo, anoFatura,
						Long.valueOf(pagamentoPendente), chavesTipoDocumento, Boolean.FALSE);

		listaFaturasAnosAnterioresParcialPaga = this.consultarFaturasPorClienteOuPontoConsumo(idCliente, idPontoConsumo, anoFatura,
						Long.valueOf(parcialmentePaga), chavesTipoDocumento, Boolean.FALSE);

		byte[] relatorioExtratoDebito = null;

		if ((!listaFaturasAnosAnterioresPendentes.isEmpty()) || (!listaFaturasAnosAnterioresParcialPaga.isEmpty())) {
			listaFaturasAnosAnterioresPendentes.addAll(listaFaturasAnosAnterioresParcialPaga);
			Collection<DadosExtratoDebito> listaDadosExtrato = new ArrayList<DadosExtratoDebito>();
			for (Fatura fatura : listaFaturasAnosAnterioresPendentes) {
				popularDadosDeclaracaoQuitacaoAnual(fatura, listaDadosExtrato, Boolean.FALSE, Long.valueOf(parcialmentePaga));
			}
			Map<String, Object> parametros = new HashMap<>();
			parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_GGAS);
			byte[] relatorioDebitosEmAberto = RelatorioUtil.gerarRelatorioPDF(listaDadosExtrato, parametros,
							RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES);
			Util.salvarRelatorioNoDiretorio(relatorioDebitosEmAberto, RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES_EM_ABERTO);
			relatorioExtratoDebito = relatorioDebitosEmAberto;
		}

		if ((!listaFaturasPendentes.isEmpty()) || (!listaFaturasParcialPaga.isEmpty())) {
			Collection<Fatura> listaFaturas = listaFaturasPendentes;
			listaFaturas.addAll(listaFaturasParcialPaga);

			DocumentoCobranca documentoCobranca = this.popularDocumentoCobrancaNaoPersistido(listaFaturas, idCliente, Boolean.TRUE);
			relatorioExtratoDebito = this.gerarRelatorioExtratoDebito(documentoCobranca, RELATORIO_DECLARACAO_DEBITOS_EM_ABERTO,
							Boolean.FALSE, Boolean.FALSE, null);
		} else if ((listaFaturasAnosAnterioresPendentes.isEmpty()) || (listaFaturasAnosAnterioresParcialPaga.isEmpty())) {

			Collection<Fatura> listaFaturasPagas = this.consultarFaturasPorClienteOuPontoConsumo(idCliente, idPontoConsumo, anoFatura,
							Long.valueOf(pagamentoEfetivado), chavesTipoDocumento, Boolean.TRUE);

			if ((listaFaturasPagas != null) && (!listaFaturasPagas.isEmpty())) {
				this.popularDocumentoCobrancaNaoPersistido(listaFaturasPagas, idCliente, Boolean.FALSE);
				byte[] relatorioDebitosEmAberto = gerarRelatorioDeclaracaoQuitacaoAnualGeral(idCliente, Long.valueOf(anoFatura));
				
				if(relatorioDebitosEmAberto != null) {
					relatorioExtratoDebito = relatorioDebitosEmAberto;
				}
			} else {
				throw new NegocioException(ERRO_NEGOCIO_FATURAS_NAO_ENCONTRADAS_ANO_SELECIONADO, true);
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_FATURAS_PENDENTES_ANO_ANTERIOR, true);
		}

		return relatorioExtratoDebito;

	}


	/**
	 * Consultar acoes cobranca.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<AcaoCobranca> consultarAcoesCobranca(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAcaoCobranca());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			AcaoCobranca acaoPrecedente = (AcaoCobranca) filtro.get("cobrancaAcao");
			if (acaoPrecedente != null) {
				criteria.add(Restrictions.eq(StringUtils.EMPTY, acaoPrecedente.getChavePrimaria()));
			}

			Segmento segmento = (Segmento) filtro.get("segmento");
			if (segmento != null) {
				criteria.add(Restrictions.eq(StringUtils.EMPTY, segmento.getChavePrimaria()));
			}

			String descricao = (String) filtro.get(EntidadeConteudo.ATRIBUTO_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(EntidadeConteudo.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Integer numeroDiasValidade = (Integer) filtro.get("numeroDiasValidade");
			if ((numeroDiasValidade != null) && (numeroDiasValidade > 0)) {
				criteria.add(Restrictions.ilike("numeroDiasValidade", Util.formatarValorNumerico(numeroDiasValidade)));
			}

			Integer numeroDiasVencimento = (Integer) filtro.get("numeroDiasVencimento");
			if ((numeroDiasVencimento != null) && (numeroDiasVencimento > 0)) {
				criteria.add(Restrictions.eq("numeroDiasVencimento", Util.formatarValorNumerico(numeroDiasVencimento)));
			}

			Boolean indicadorGerarOrdemServico = (Boolean) filtro.get("indicadorGerarOrdemServico");
			if (indicadorGerarOrdemServico != null) {
				criteria.add(Restrictions.eq("indicadorGerarOrdemServico", indicadorGerarOrdemServico));
			}

			Boolean indicadorConsolidarDebitosAVencer = (Boolean) filtro.get("indicadorConsolidarDebitosAVencer");
			if (indicadorConsolidarDebitosAVencer != null) {
				criteria.add(Restrictions.eq("indicadorConsolidarDebitosAVencer", indicadorConsolidarDebitosAVencer));
			}

			Boolean indicadorBloquearDocumentosPagaveis = (Boolean) filtro.get("indicadorBloquearDocumentosPagaveis");
			if (indicadorBloquearDocumentosPagaveis != null) {
				criteria.add(Restrictions.eq("indicadorBloquearDocumentosPagaveis", indicadorBloquearDocumentosPagaveis));
			}

		}

		return criteria.list();
	}

	/**
	 * Popular documento cobranca nao persistido.
	 *
	 * @param listaFatura
	 *            Uma colecao de faturas
	 * @param idCliente
	 *            the id cliente
	 * @param indicadorPago
	 *            the indicador pago
	 * @return DocumentoCobranca - Uma entidade do
	 *         sistema para montar o
	 *         relatorio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private DocumentoCobranca popularDocumentoCobrancaNaoPersistido(Collection<Fatura> listaFatura, Long idCliente, boolean indicadorPago)
					throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		DocumentoCobranca documentoCobranca = null;

		if ((listaFatura != null) && (!listaFatura.isEmpty())) {
			documentoCobranca = (DocumentoCobranca) this.criarDocumentoCobranca();
			DocumentoCobrancaItem documentoCobrancaItem = null;
			Cliente clienteAux = null;
			for (Fatura fatura : listaFatura) {

				if (clienteAux == null) {
					clienteAux = fatura.getCliente();
				}
				documentoCobrancaItem = (DocumentoCobrancaItem) this.criarDocumentoCobrancaItem();
				FaturaGeral faturaGeral = obterFaturaGeralPelaFatura(fatura.getChavePrimaria());
				documentoCobrancaItem.setFaturaGeral(faturaGeral);
				// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
				BigDecimal valorFatura = fatura.getValorSaldoConciliado();
				if (!indicadorPago) {
					BigDecimal valorRecebimento = obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
					if (valorRecebimento != null) {
						valorFatura = valorFatura.subtract(valorRecebimento);
					}
				}
				documentoCobrancaItem.setValor(valorFatura);
				documentoCobrancaItem.setPontoConsumo(fatura.getPontoConsumo());
				documentoCobranca.getItens().add(documentoCobrancaItem);
			}
			Cliente cliente = null;
			if (idCliente != null && idCliente > 0) {
				cliente = (Cliente) controladorCliente.obter(idCliente);
			} else {
				cliente = clienteAux;
			}
			documentoCobranca.setCliente(cliente);
		}
		return documentoCobranca;
	}

	/**
	 * Consultar faturas nao pagas por contrato.
	 *
	 * @param numeroContrato
	 *            the numero contrato
	 * @param anoFaturas
	 *            the ano faturas
	 * @param anoAtual
	 *            the ano atual
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("squid:S1192")
	private Collection<Fatura> consultarFaturasNaoPagasPorContrato(String numeroContrato, String anoFaturas, boolean anoAtual)
					throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// já foi feito refactory deste método no controlador de fatura, remover após homologação de arc/cbr.
		// e fazer Refeactoring de Faturamento agrupado

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(WHERE);

		hql.append(" fatura.faturaAgrupada is null ");
		hql.append(" and fatura.contrato is null and ");

		// Gera para o ano escolhido ou para os anos anteriores ao escolhido.
		if (anoAtual) {
			hql.append(" cast(year(fatura.dataVencimento) as string) = :anoFaturas ");
		} else {
			hql.append(" cast(year(fatura.dataVencimento) as string) < :anoFaturas ");
		}

		String idSituacaoPagamentoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
		String idSituacaoPagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		hql.append(" and (fatura.situacaoPagamento.chavePrimaria = :idSituacaoPagamentoParcialmentePago ");
		hql.append(" or fatura.situacaoPagamento.chavePrimaria = :idSituacaoPagamentoPendente) ");
		completarQueryFaturasPorContrato(hql);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("anoFaturas", anoFaturas);
		query.setString("numeroContrato", numeroContrato);
		query.setLong("idSituacaoPagamentoParcialmentePago", Long.parseLong(idSituacaoPagamentoParcialmentePago));
		query.setLong("idSituacaoPagamentoPendente", Long.parseLong(idSituacaoPagamentoPendente));

		return query.list();
	}

	/**
	 * Consultar faturas pagas por contrato.
	 *
	 * @param numeroContrato
	 *            the numero contrato
	 * @param anoFaturas
	 *            the ano faturas
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<Fatura> consultarFaturasPagasPorContrato(String numeroContrato, String anoFaturas) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// já foi feito refactory deste método no controlador de fatura, remover após homologação de arc/cbr.
				// e fazer Refeactoring de Faturamento agrupado

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(WHERE);

		hql.append(" fatura.faturaAgrupada is null ");
		hql.append(" and fatura.contrato is null and ");

		hql.append(" cast(year(fatura.dataVencimento) as string) = :anoFaturas ");

		String idSituacaoPagamentoPago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		hql.append(" and fatura.situacaoPagamento.chavePrimaria = :idSituacaoPagamento ");
		completarQueryFaturasPorContrato(hql);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("anoFaturas", anoFaturas);
		query.setString("numeroContrato", numeroContrato);
		query.setLong("idSituacaoPagamento", Long.parseLong(idSituacaoPagamentoPago));

		return query.list();
	}

	/**
	 * Completar query faturas por contrato.
	 *
	 * @param hql
	 *            the hql
	 */
	private void completarQueryFaturasPorContrato(StringBuilder hql) {

		hql.append(" and fatura.pontoConsumo.chavePrimaria in ( ");
		hql.append(" select contratoPonto.pontoConsumo.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(WHERE);
		hql.append(" cast((contrato.anoContrato*100000)+contrato.numero, string) = :numeroContrato ");
		hql.append(")");
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * validarFiltroDeclaracaoQuitacaoAnual(java.lang
	 * .String, java.lang.Long,
	 * java.lang.Long, java.lang.String)
	 */
	@Override
	public void validarFiltroDeclaracaoQuitacaoAnual(String anoFaturas, Long idCliente, Long idPontoConsumo, String numeroContrato,
					Boolean indicadorGeral) throws NegocioException {

		if (StringUtils.isEmpty(anoFaturas)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							new String[] {ControladorCobranca.DECLARACAO_QUITACAO_ANUAL_ANO});
		}

		if (((idCliente == null) || (idCliente <= 0)) && ((idPontoConsumo == null) || (idPontoConsumo <= 0))
						&& (StringUtils.isEmpty(numeroContrato)) && (!indicadorGeral)) {
			throw new NegocioException(ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * gerarRelatorioDeclaracaoQuitacaoAnualGeral(java
	 * .util.Collection,
	 * java.lang.String)
	 */
	@Override
	public byte[] gerarRelatorioDeclaracaoQuitacaoAnualGeral(Collection<Cliente> clientes, String anoGeracao, boolean indicadorPago,
					Integer qtdClientes, Map<String, Long> parametrosConsulta) throws GGASException {

		Collection<ClienteVO> listaClienteVO = new ArrayList<ClienteVO>();
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_GGAS);

		Long pagamentoPendente = parametrosConsulta.get("pagamentoPendente");
		Long parcialmentePaga = parametrosConsulta.get("parcialmentePaga");
		Long tipoFatura = parametrosConsulta.get("tipoFatura");
		Long tipoNotaDebito = parametrosConsulta.get("tipoNotaDebito");
		Long pagamentoEfetivado = parametrosConsulta.get("pagamentoEfetivado");
		Long[] chavesTipoDocumento = new Long[] {tipoFatura, tipoNotaDebito};
		Long[] situacaoPagamentoNaoPaga = new Long[] {pagamentoPendente, parcialmentePaga};
		Long[] situacaoPagamentoPaga = new Long[] {pagamentoEfetivado};

		Long[] chavesClientes = Util.collectionParaArrayChavesPrimarias(clientes);

		Collection<Fatura> listaFatura = null;
		if (indicadorPago) {
			listaFatura = this.consultarFaturasPorClientes(chavesClientes, anoGeracao, situacaoPagamentoPaga, chavesTipoDocumento,
							Boolean.TRUE);
		} else {
			listaFatura = this.consultarFaturasPorClientes(chavesClientes, anoGeracao, situacaoPagamentoNaoPaga, chavesTipoDocumento,
							Boolean.TRUE);
		}

		byte[] relatorioDeclaracaoAnual = null;
		if (listaFatura != null) {
			Map<Long, ClienteVO> mapaCliente = new HashMap<Long, ClienteVO>();
			for (Fatura fatura : listaFatura) {
				popularRelatorioDeclaracaoQuitacaoAnualDebitos(fatura.getCliente(), fatura, indicadorPago, parcialmentePaga, mapaCliente);
			}
			Entry<?, ?> entry = null;
			for (Iterator<?> it = mapaCliente.entrySet().iterator(); it.hasNext();) {
				entry = (Entry<?, ?>) it.next();
				listaClienteVO.add((ClienteVO) entry.getValue());
			}
			mapaCliente = null;

			String arquivoRelatorio = StringUtils.EMPTY;
			if (indicadorPago) {
				arquivoRelatorio = RELATORIO_DECLARACAO_QUITACAO_DEBITOS_ANUAL_LOTE_JASPER;
			} else {
				arquivoRelatorio = RELATORIO_DECLARACAO_DEBITOS_EM_ABERTO_LOTE_JASPER;
			}

			Collection<DadosRelatorioExtratoDebitoQuitacaoAnual> dadosRelatorio = new ArrayList<DadosRelatorioExtratoDebitoQuitacaoAnual>();
			DadosRelatorioExtratoDebitoQuitacaoAnual dadosRelatorioQuitacao = new DadosRelatorioExtratoDebitoQuitacaoAnual();
			dadosRelatorioQuitacao.setClientesVO(listaClienteVO);
			dadosRelatorio.add(dadosRelatorioQuitacao);
			relatorioDeclaracaoAnual = RelatorioUtil.gerarRelatorioPDF(dadosRelatorio, parametros, arquivoRelatorio);
		}

		return relatorioDeclaracaoAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#gerarRelatorioDeclaracaoQuitacaoAnualGeral(java.lang.Long, java.lang.Long)
	 */
	@Override
	public byte[] gerarRelatorioDeclaracaoQuitacaoAnualGeral(Long idCliente, Long anoReferencia) throws GGASException {

		Collection<DadosExtratoDebito> dadosExtratoDebito = new ArrayList<DadosExtratoDebito>();
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_GGAS);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long parcialmentePaga = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));
		Long pagamentoEfetivado = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO));
		Long tipoFatura = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
		Long tipoNotaDebito = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));

		Long[] chavesTipoDocumento = new Long[] {tipoFatura, tipoNotaDebito};
		Long[] situacaoPagamentoPaga = new Long[] {pagamentoEfetivado};

		Long[] chavesClientes = new Long[1];
		chavesClientes[0] = idCliente;

		// inicio da verificação se existe fatura em aberto
		Long[] situacaoPagamentoPagaEmAberto = new Long[] {Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE))};
		Collection<Fatura> listaFaturaEmAberto = this.consultarFaturasPorClientes(chavesClientes, String.valueOf(anoReferencia),
						situacaoPagamentoPagaEmAberto, chavesTipoDocumento, Boolean.TRUE);

		if (listaFaturaEmAberto != null && !listaFaturaEmAberto.isEmpty()) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Não foi possivel emitir a certidaos pois a fatura de vencimento do dia ");
			for (Fatura fatura : listaFaturaEmAberto) {
				stringBuilder.append(Util.converterDataParaString(fatura.getDataVencimento()));

			}
			stringBuilder.append(" se encontra em aberto.");
			throw new NegocioException(stringBuilder.toString());
			// final da verificação se existe fatura em aberto
		}

		Collection<Fatura> listaFatura = this.consultarFaturasPorClientes(chavesClientes, String.valueOf(anoReferencia),
						situacaoPagamentoPaga, chavesTipoDocumento, Boolean.TRUE);

		byte[] relatorioDeclaracaoAnual = null;
		if (listaFatura != null && !listaFatura.isEmpty()) {
			for (Fatura fatura : listaFatura) {

				DadosExtratoDebito dadosExtrato = this.criarDadosExtratoDebito();
				this.popularDadosExtratoRelatorioGeral(fatura.getCliente(), dadosExtrato, fatura, Boolean.TRUE, parcialmentePaga);
				dadosExtratoDebito.add(dadosExtrato);
			}

			String arquivoRelatorio = RELATORIO_DECLARACAO_QUITACAO_DEBITOS_ANUAL_NOVO;
			// gerando o codigo de validação
			Long numeroMaximo = Long.valueOf("99999999999");
			// indica o numero maximo que pode chagar com 11 posições
			Long numero = (long) (Math.random() * numeroMaximo); // NOSONAR
			String numeroString = numero + StringUtils.EMPTY;
			numeroString = Util.adicionarZerosEsquerdaNumero(numeroString,
					TAMANHO_MAX_COD_VALIDACAO - numeroString.length());
			String codigoValidacao = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(new Date());
			codigoValidacao += numeroString;

			Empresa empresa = controladorEmpresas.obterEmpresaPrincipal();
			parametros.put("nomeEmpresa", empresa.getCliente().getNome());
			parametros.put(ENDERECO_EMPRESA, empresa.getCliente().getEnderecoPrincipal().getEnderecoLogradouro());
			parametros.put("cepEmpresa", empresa.getCliente().getEnderecoPrincipal().getCep().getCep());
			parametros.put("cidadeEstadoEmpresa", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
			parametros.put("anoReferencia", anoReferencia + StringUtils.EMPTY);
			Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
			parametros.put("logo", logo);
			parametros.put("codigoValidacao", codigoValidacao);

			relatorioDeclaracaoAnual = RelatorioUtil.gerarRelatorioPDF(dadosExtratoDebito, parametros, arquivoRelatorio);
			this.inserirDeclaracaoQuitacaoAnual(idCliente, anoReferencia, relatorioDeclaracaoAnual, codigoValidacao);
		}

		return relatorioDeclaracaoAnual;
	}

	/**
	 * Inserir declaracao quitacao anual.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @param anoReferencia
	 *            the ano referencia
	 * @param relatorio
	 *            the relatorio
	 * @param codigoValidacao
	 *            the codigo validacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void inserirDeclaracaoQuitacaoAnual(Long idCliente, Long anoReferencia, byte[] relatorio, String codigoValidacao)
					throws NegocioException {

		DeclaracaoQuitacaoAnual declaracaoQuitacaoAnual = (DeclaracaoQuitacaoAnual) this.criarDeclaracaoQuitacaoAnual();

		Cliente cliente = (Cliente) this.obter(idCliente, ClienteImpl.class);
		declaracaoQuitacaoAnual.setCliente(cliente);

		declaracaoQuitacaoAnual.setAnoReferencia(anoReferencia);
		declaracaoQuitacaoAnual.setRelatorio(relatorio);

		// inserir CodigoValidacao na declaracaoQuitacaoAnual

		declaracaoQuitacaoAnual.setCodigoValidacao(codigoValidacao);

		this.inserir(declaracaoQuitacaoAnual);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#consultarDeclaracaoQuitacao(java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public byte[] consultarDeclaracaoQuitacao(String codigoValidacao, Long idCliente, Long anoReferencia) {

		byte[] relatorio = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDeclaracaoQuitacaoAnual().getSimpleName());
		hql.append(" declaracao ");
		hql.append(" where 1=1 ");

		if (codigoValidacao != null && !codigoValidacao.isEmpty() && !"null".equals(codigoValidacao)) {

			hql.append(" and declaracao.codigoValidacao = :codigoValidacao ");
		}

		if (idCliente != null && idCliente > 0) {

			hql.append(" and declaracao.cliente.chavePrimaria = :idCliente ");
		}

		if (anoReferencia != null && anoReferencia > 0) {

			hql.append(" and declaracao.anoReferencia = :anoReferencia ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoValidacao != null && !codigoValidacao.isEmpty() && !"null".equals(codigoValidacao)) {

			query.setString("codigoValidacao", codigoValidacao);
		}

		if (idCliente != null && idCliente > 0) {

			query.setLong("idCliente", idCliente);
		}

		if (anoReferencia != null && anoReferencia > 0) {

			query.setLong("anoReferencia", anoReferencia);
		}

		DeclaracaoQuitacaoAnual declaracaoQuitacaoAnual = (DeclaracaoQuitacaoAnual) query.uniqueResult();

		if (declaracaoQuitacaoAnual != null) {

			relatorio = declaracaoQuitacaoAnual.getRelatorio();
		}

		return relatorio;

	}

	/**
	 * Criar declaracao quitacao anual.
	 *
	 * @return the object
	 */
	public Object criarDeclaracaoQuitacaoAnual() {

		return ServiceLocator.getInstancia().getBeanPorID(DeclaracaoQuitacaoAnual.BEAN_ID_DECLARACAO_QUITACAO_ANUAL);
	}

	/**
	 * Popular dados extrato relatorio geral.
	 *
	 * @param cliente
	 *            the cliente
	 * @param dadosExtrato
	 *            the dados extrato
	 * @param fatura
	 *            the fatura
	 * @param indicadorPago
	 *            the indicador pago
	 * @param situacaoParcialmentePaga
	 *            the situacao parcialmente paga
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularDadosExtratoRelatorioGeral(Cliente cliente, DadosExtratoDebito dadosExtrato, Fatura fatura, boolean indicadorPago,
					Long situacaoParcialmentePaga) throws GGASException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		dadosExtrato.setNomeCliente(cliente.getNome());
		dadosExtrato.setDocIdentificador(cliente.getNumeroDocumentoFormatado());
		dadosExtrato.setCodigoCliente(String.valueOf(cliente.getChavePrimaria()));
		dadosExtrato.setInscricaoEstadual(cliente.getInscricaoEstadual());
		dadosExtrato.setTipoCliente(cliente.getTipoCliente().getDescricao());

		PontoConsumo pontoConsumo = fatura.getPontoConsumo();
		dadosExtrato.setPontoConsumo(pontoConsumo.getDescricao());
		if (pontoConsumo.getSegmento() != null) {
			dadosExtrato.setSegmento(pontoConsumo.getSegmento().getDescricao());
		}
		if (pontoConsumo.getCep() != null) {
			dadosExtrato.setCep(pontoConsumo.getCep().getCep());
		}

		Map<Long, ContratoPontoConsumo> mapaContratoPonto = new HashMap<Long, ContratoPontoConsumo>();
		ContratoPontoConsumo contratoPonto = null;
		if (!mapaContratoPonto.containsKey(pontoConsumo.getChavePrimaria())) {
			contratoPonto = controladorContrato.obterContratoPontoConsumoParaEnderecoEntrega(pontoConsumo.getChavePrimaria());
			mapaContratoPonto.put(pontoConsumo.getChavePrimaria(), contratoPonto);
		} else {
			contratoPonto = mapaContratoPonto.get(pontoConsumo.getChavePrimaria());
		}

		if ((contratoPonto != null) && (contratoPonto.getCep() != null)) {
			dadosExtrato.setEndereco(contratoPonto.getEnderecoFormatado());
		} else if (cliente.getEnderecoPrincipal() != null) {
			dadosExtrato.setEndereco(cliente.getEnderecoPrincipal().getEnderecoFormatado());
		}

		dadosExtrato.setSituacao(pontoConsumo.getSituacaoConsumo().getDescricao());

		dadosExtrato.setNumero(String.valueOf(fatura.getChavePrimaria()));
		dadosExtrato.setDescricao(this.obterDescricaoFatura(fatura));
		dadosExtrato.setTipo(fatura.getTipoDocumento().getDescricao());
		dadosExtrato.setVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));

		// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
		BigDecimal valorFatura = fatura.getValorSaldoConciliado();
		if ((!indicadorPago) && (fatura.getSituacaoPagamento() != null)
						&& (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoParcialmentePaga)) {
			BigDecimal valorRecebimento = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
			if (valorRecebimento != null) {
				valorFatura = valorFatura.subtract(valorRecebimento);
			}
		}

		dadosExtrato.setValor(Util.converterCampoCurrencyParaString(valorFatura, Constantes.LOCALE_PADRAO));
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * gerarRelatorioDebitosAnosAnterioresLote(java
	 * .util.Collection,
	 * java.lang.String)
	 */
	@Override
	public void gerarRelatorioDebitosAnosAnterioresLote(Collection<Cliente> listaClientes, String anoGeracao,
			Map<String, Long> parametrosConsulta) throws GGASException {

		Collection<DadosExtratoDebito> listaDadosExtrato = new ArrayList<>();
		Map<String, Object> parametros = new HashMap<>();
		parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_GGAS);
		List<Fatura> listaFaturas = new ArrayList<>();

		Long pagamentoPendente = parametrosConsulta.get("pagamentoPendente");
		Long parcialmentePaga = parametrosConsulta.get("parcialmentePaga");
		Long tipoFatura = parametrosConsulta.get("tipoFatura");
		Long tipoNotaDebito = parametrosConsulta.get("tipoNotaDebito");
		Long[] chavesTipoDocumento = new Long[] { tipoFatura, tipoNotaDebito };
		Long[] chavesSituacaoPagamento = new Long[] { pagamentoPendente, parcialmentePaga };

		List<Cliente> clientesRelatorio = new ArrayList<Cliente>();
		for (Cliente cliente : listaClientes) {
			clientesRelatorio.add(cliente);
			if ((clientesRelatorio.size() == CONSTANTE_NOVECENTOS_NOVENTA_NOVE)
					|| (((List<Cliente>) listaClientes).indexOf(cliente) == listaClientes.size() - 1)) {
				Long[] chavesClientes = Util.collectionParaArrayChavesPrimarias(clientesRelatorio);
				listaFaturas.addAll(this.consultarFaturasPorClientes(chavesClientes, anoGeracao,
						chavesSituacaoPagamento, chavesTipoDocumento, Boolean.FALSE));
				clientesRelatorio.clear();
			}
		}

		for (Fatura fatura : listaFaturas) {
			popularDadosDeclaracaoQuitacaoAnual(fatura, listaDadosExtrato, Boolean.FALSE, parcialmentePaga);
		}

		byte[] relatorioDebitosEmAberto = null;

		if (!listaFaturas.isEmpty()) {
			relatorioDebitosEmAberto = RelatorioUtil.gerarRelatorioPDF(listaDadosExtrato, parametros,
					RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES);
			Util.salvarRelatorioNoDiretorio(relatorioDebitosEmAberto,
					RELATORIO_DECLARACAO_DEBITOS_ANOS_ANTERIORES_EM_ABERTO);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca
	 * #gerarRelatorioBoletoBancario
	 * (br.com.ggas.faturamento.DocumentoCobranca,
	 * java.lang.String, br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio,
	 * java.lang.String)
	 */
	@Override
	public byte[] gerarRelatorioBoletoBancario(DocumentoCobranca documentoCobranca, String nossoNumero,
					ArrecadadorContratoConvenio arrecadadorContratoConvenio, String arquivoRelatorio, boolean isSegundaVia, Fatura fatura)
					throws GGASException {

		// Consultar a apuração de penalidade associada a esta fatura... a partir dessa penalidade... retornar todas as outras penalidades
		// que possuam a mesma caracteristica e setar no objeto de imprimir
		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		List<DadosExtratoDebito> listaDadosExtrato = new ArrayList<DadosExtratoDebito>();
		Map<String, Object> parametros = new HashMap<String, Object>();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);
		Empresa empresa = null;
		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}
		if (empresa != null && empresa.getLogoEmpresa() != null) {
			parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		}
		this.popularRelatorioMapaCliente(parametros, documentoCobranca, isSegundaVia);
		this.popularRelatorioDetalhamento(parametros, documentoCobranca, listaDadosExtrato, Boolean.TRUE);

		parametros.putAll(this.construtorRelatorioBoleto(documentoCobranca).mapaParametros());

		Collections.sort(listaDadosExtrato, new Comparator<DadosExtratoDebito>(){

			@Override
			public int compare(DadosExtratoDebito o1, DadosExtratoDebito o2) {

				Date data1 = new Date();
				Date data2 = new Date();
				try {
					if (o1.getVencimento() != null && o2.getVencimento() != null) {
						data1 = Util.converterCampoStringParaData("data", o1.getVencimento(), Constantes.FORMATO_DATA_BR);
						data2 = Util.converterCampoStringParaData("data", o2.getVencimento(), Constantes.FORMATO_DATA_BR);
					}
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
				}

				return data1.compareTo(data2);
			}

		});

		Cliente cliente = (Cliente) ServiceLocator.getInstancia().getControladorCliente()
						.obter(documentoCobranca.getCliente().getChavePrimaria(), ENDERECOS, "enderecoPrincipal.cep");

		if (!cliente.getEnderecos().isEmpty()) {
			parametros.put("endereco_cliente", cliente.getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
			parametros.put("cep_cliente", cliente.getEnderecoPrincipal().getCep().getCep());
			parametros.put("cidade", cliente.getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
		}

		if (cliente.getContatos() != null && !cliente.getContatos().isEmpty()) {
			ContatoCliente contato = cliente.getContatos().iterator().next();
			parametros.put("contato_cliente_nome", contato.getNome());
			parametros.put("contato_cliente_email", contato.getEmail());
			parametros.put("contato_cliente_telefone1", String.valueOf(contato.getFone() + " / "));
			parametros.put("contato_cliente_telefone2", String.valueOf(contato.getFone2()));
			parametros.put("contato_cliente_DDD", "(" + contato.getCodigoDDD() + ")");
			parametros.put("contato_cliente_DDD2", "(" + contato.getCodigoDDD2() + ")");
		}

		parametros.put("destinatario_cliente", cliente.getNome());

		if (!StringUtils.EMPTY.equals(cliente.getCnpjFormatado())) {
			parametros.put(CNPJ_CPF_CLIENTE, cliente.getCnpjFormatado());
			parametros.put(TIPO_PESSOA, "CNPJ:");
		} else {
			parametros.put(CNPJ_CPF_CLIENTE, cliente.getCpfFormatado());
			parametros.put(TIPO_PESSOA, "CPF:");
		}

		parametros.put("dataAtual", Util.converterDataParaString(new Date()));

		if (fatura != null) {
			ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
							ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

			parametros.put("numeroNotaFiscal", fatura.getChavePrimaria());
			parametros.put("nr_fatura", "Nº " + fatura.getChavePrimaria());
			parametros.put("data_emissao", Util.converterDataParaString(fatura.getDataEmissao()));

			Collection<Recebimento> recebimentos = fatura.getFaturaGeral().getListaRecebimento();

			if (recebimentos != null && !recebimentos.isEmpty()) {
				Recebimento recebimento = recebimentos.iterator().next();
				parametros.put("dataPagamento", recebimento.getDataRecebimentoFormatada());
			}
			BigDecimal valorTotal = BigDecimal.ZERO;
			if (fatura.getContratoAtual() != null) {

				Contrato contrato = fatura.getContratoAtual();
				if (fatura.getContratoAtual().getIndiceFinanceiro() != null) {
					String indiceFinanceiro = fatura.getContratoAtual().getIndiceFinanceiro().getDescricaoAbreviada();
					parametros.put("indiceFinanceiro", indiceFinanceiro);
					valorTotal = Util.atualizatValorIGPM(fatura.getDataEmissao(), fatura.getValorTotal(), indiceFinanceiro);

					if (valorTotal.compareTo(fatura.getValorTotal()) > 0) {
						BigDecimal correcaoMonetaria = valorTotal.subtract(fatura.getValorTotal()).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
						parametros.put("correcaoMonetaria", correcaoMonetaria.toString().replace(".", ","));
					}

				} else {
					valorTotal = fatura.getValorTotal();
				}
				DebitosACobrar debitosACobrar = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, new Date(),
								fatura.getValorTotal(), true, true);

				if (debitosACobrar != null) {
					if (debitosACobrar.getJurosMora() != null) {
						parametros.put("valorJuros", debitosACobrar.getJurosMora().toString().replace(".", ","));
						valorTotal = valorTotal.add(debitosACobrar.getJurosMora());
					}

					if (debitosACobrar.getMulta() != null) {
						parametros.put("valorMulta", debitosACobrar.getMulta().toString().replace(".", ","));
						valorTotal = valorTotal.add(debitosACobrar.getMulta());
					}
				}
				parametros.put("valorTotal", valorTotal.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP).toString().replace(".", ","));

				if (contrato.getPercentualMulta() != null) {
					parametros.put("percentualMulta", contrato.getPercentualMulta().multiply(new BigDecimal(CENTENA)) + "%");
				}

				if (contrato.getPercentualJurosMora() != null) {
					parametros.put("percentualJuros", contrato.getPercentualJurosMora().multiply(new BigDecimal(CENTENA)) + "%");
				}
			}
		}

		if (empresa != null) {
			parametros.put("razao_social_CDL", empresa.getCliente().getNome());
			parametros.put("endereco_CDL", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
			parametros.put("cep_CDL", empresa.getCliente().getEnderecoPrincipal().getCep().getCep());
			parametros.put("municipio_CDL", empresa.getCliente().getEnderecoPrincipal().getMunicipio().getDescricao() + ", ");
			parametros.put("cnpj_CDL", empresa.getCliente().getCnpjFormatado());
			parametros.put("uf_CDL", empresa.getCliente().getEnderecoPrincipal().getMunicipio().getUnidadeFederacao().getSigla());
			parametros.put("inscricao_estadual_CDL", empresa.getCliente().getInscricaoEstadual());
		}

		return RelatorioUtil.gerarRelatorioPDF(listaDadosExtrato, parametros, arquivoRelatorio);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * atualizarFatura(br.
	 * com.procenge.ggas.faturamento.Fatura)
	 */
	@Override
	public void atualizarFatura(Fatura faturaAtual) throws ConcorrenciaException, NegocioException {

		atualizar(faturaAtual, getClasseEntidadeFatura());
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.ControladorCobranca
	 * #
	 * atualizarCreditoDebitoARealizar
	 * (br.com.ggas.faturamento.CreditoDebitoARealizar
	 * )
	 */
	@Override
	public void atualizarCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) throws ConcorrenciaException,
					NegocioException {

		atualizar(creditoDebitoARealizar,
						ServiceLocator.getInstancia().getClassPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR));

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.ControladorCobranca#
	 * consultarFaturasPorClientes
	 * (java.lang.Long[], java.lang.String,
	 * java.lang.Long[], java.lang.Long[])
	 */
	@Override
	public Collection<Fatura> consultarFaturasPorClientes(Long[] chavesCliente, String anoFatura, Long[] chavesSituacaoPagamento,
					Long[] chavesTipoDocumento, boolean anoAtual) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" inner join fetch fatura.cliente ");
		hql.append(" inner join fetch fatura.pontoConsumo ");
		hql.append(" inner join fetch fatura.tipoDocumento ");
		hql.append(WHERE);

		// Refeactoring de Faturamento
		// agrupado
		hql.append(" fatura.faturaAgrupada is null ");
		hql.append(" and fatura.contrato is null and ");

		hql.append(" fatura.tipoDocumento.chavePrimaria in (:chavesTipoDocumento) ");
		hql.append(" and fatura.cliente.chavePrimaria in (:chavesCliente) ");
		if (anoAtual) {
			hql.append(" and cast(year(fatura.dataVencimento) as string) = :anoFatura ");
		} else {
			hql.append(" and cast(year(fatura.dataVencimento) as string) < :anoFatura ");
		}

		hql.append(" and fatura.situacaoPagamento.chavePrimaria in (:chavesSituacaoPagamento) ");
		hql.append(" order by fatura.pontoConsumo.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesTipoDocumento", chavesTipoDocumento);
		query.setParameter("anoFatura", anoFatura);
		query.setParameterList("chavesSituacaoPagamento", chavesSituacaoPagamento);
		query.setParameterList("chavesCliente", chavesCliente);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#obterValorFatura(java.lang.Long)
	 */
	@Override
	public BigDecimal obterValorFatura(Long chaveFatura) throws NegocioException {

		BigDecimal valor = BigDecimal.ZERO;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT fatura.valorTotal");
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append( " fatura ");
		hql.append(WHERE);
		hql.append(" fatura.chavePrimaria =:CHAVES_FATURA ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVES_FATURA", chaveFatura);

		valor = (BigDecimal) query.uniqueResult();
		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#listarCobrancaDebitoSituacao()
	 */
	@Override
	public Collection<CobrancaDebitoSituacao> listarCobrancaDebitoSituacao() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCobrancaDebitoSituacao());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#consultarDocumentoCobrancaItem(java.util.Map, java.lang.String, java.lang.String[])
	 */
	@Override
	public Collection<DocumentoCobrancaItem> consultarDocumentoCobrancaItem(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		Query query = null;
		Long idFaturaGeral = null;
		Long idDocumentoCobranca = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" doci ");
		hql.append(" where 1 = 1 ");

		if (filtro != null && !filtro.isEmpty()) {
			idFaturaGeral = (Long) filtro.get("idFaturaGeral");
			idDocumentoCobranca = (Long) filtro.get("idDocumentoCobranca");

			if (idDocumentoCobranca != null) {
				hql.append(" and doci.documentoCobranca = :idDocumentoCobranca ");
			}

			if (idFaturaGeral != null) {
				hql.append(" and doci.faturaGeral.chavePrimaria = :idFaturaGeral ");
			}

		}

		if (ordenacao != null) {

			hql.append(" order by  ");
			hql.append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null && !filtro.isEmpty()) {

			if (idFaturaGeral != null) {
				query.setLong("idFaturaGeral", idFaturaGeral);
			}

			if (idDocumentoCobranca != null) {
				query.setLong("idDocumentoCobranca", idDocumentoCobranca);
			}

		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null) {

			lista = inicializarLazyColecao(lista, propriedadesLazy);
		}

		return (Collection<DocumentoCobrancaItem>) (Collection<?>) lista;
	}

	/**
	 * Consulta uma coleção de entidades do tipo {@link DocumentoCobrancaItem} que possuem uma entidade do tipo {@link FaturaGeral}, da
	 * lista de entidades passada por parâmetro.
	 *
	 * @param faturasGerais - {@link List}
	 * @return coleção de {@link DocumentoCobrancaItem} por chave primária de {@link FaturaGeral}
	 */
	@Override
	public Map<Long, Collection<DocumentoCobrancaItem>> consultarDocumentoCobrancaItemPorFaturaGeral(List<FaturaGeral> faturasGerais) {

		Map<Long, Collection<DocumentoCobrancaItem>> documentoPorFatura = new HashMap<>();

		if (faturasGerais != null && !faturasGerais.isEmpty()) {

			StringBuilder hql = new StringBuilder();

			hql.append(" select faturaGeral.chavePrimaria, documentoCobrancaItem.chavePrimaria, ");
			hql.append(" documentoCobranca.chavePrimaria, documentoCobranca.nossoNumero ");
			hql.append(FROM);
			hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
			hql.append(" documentoCobrancaItem ");
			hql.append(" inner join documentoCobrancaItem.faturaGeral faturaGeral ");
			hql.append(" inner join documentoCobrancaItem.documentoCobranca documentoCobranca ");
			hql.append(WHERE);
			hql.append(" faturaGeral IN (:faturasGerais) ");
			hql.append(" order by documentoCobranca.chavePrimaria desc ");

			Query query = getSession().createQuery(hql.toString());

			query.setParameterList("faturasGerais", faturasGerais);

			documentoPorFatura.putAll(this.agruparDocumentoCobrancaItemPorFaturaGeral(query.list()));
		}

		return documentoPorFatura;
	}

	/**
	 * Mapeia uma coleção de entidades do tipo {@link DocumentoCobrancaItem} pelo atributo {@code chavePrimaria} de sua {@link FaturaGeral}.
	 *
	 * @param resultadoConsulta - {@link List}
	 * @return coleção de {@link DocumentoCobrancaItem} por chave primária de {@link FaturaGeral}
	 */
	private Map<Long, Collection<DocumentoCobrancaItem>> agruparDocumentoCobrancaItemPorFaturaGeral(List<Object[]> resultadoConsulta) {

		Map<Long, Collection<DocumentoCobrancaItem>> documentosPorFatura = new HashMap<>();

		Collection<DocumentoCobrancaItem> documentos = null;

		for (Object[] linhaConsulta : resultadoConsulta) {

			Long chaveFaturaGeral = (Long) linhaConsulta[0];
			Long chaveDocumentoCobrancaItem = (Long) linhaConsulta[1];
			Long chaveDocumentoCobranca = (Long) linhaConsulta[2];
			Long numeroDocumentoCobranca = (Long) linhaConsulta[3];

			DocumentoCobrancaItem documentoCobrancaItem = this.criarDocumentoCobrancaItem(chaveDocumentoCobranca,
							chaveDocumentoCobrancaItem, numeroDocumentoCobranca);

			if (!documentosPorFatura.containsKey(chaveFaturaGeral)) {
				documentos = new HashSet<>();
				documentosPorFatura.put(chaveFaturaGeral, documentos);
			} else {
				documentos = documentosPorFatura.get(chaveFaturaGeral);
			}

			documentos.add(documentoCobrancaItem);
		}
		return documentosPorFatura;
	}

	/**
	 * Cria uma entidade do tipo {@link DocumentoCobrancaItem} e popula com os atributos: {@code nossoNumero}, {@code chavePrimaria},
	 * {@code documentoCobranca}.
	 *
	 * @param chaveDocumentoCobranca - {@link Long}
	 * @param chaveDocumentoCobrancaItem - {@link Long}
	 * @param numeroDocumentoCobranca - {@link Long}
	 * @return {@link DocumentoCobrancaItem}
	 */
	private DocumentoCobrancaItem criarDocumentoCobrancaItem(Long chaveDocumentoCobranca, Long chaveDocumentoCobrancaItem,
					Long numeroDocumentoCobranca) {

		DocumentoCobranca documentoCobranca = (DocumentoCobranca) criarDocumentoCobranca();
		documentoCobranca.setChavePrimaria(chaveDocumentoCobranca);
		documentoCobranca.setNossoNumero(numeroDocumentoCobranca);

		DocumentoCobrancaItem documentoCobrancaItem = (DocumentoCobrancaItem) criarDocumentoCobrancaItem();
		documentoCobrancaItem.setChavePrimaria(chaveDocumentoCobrancaItem);
		documentoCobrancaItem.setDocumentoCobranca(documentoCobranca);

		return documentoCobrancaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#listarDocumentoCobranca(java.util.Map)
	 */
	@Override
	public Collection<DocumentoCobranca> listarDocumentoCobranca(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeDocumentoCobranca());
		criteria.createCriteria("itens", "itens", Criteria.INNER_JOIN);
		criteria.createAlias("itens.pontoConsumo", "pontoConsumo");
		criteria.createAlias("cliente", "cliente");
		criteria.createAlias("tipoDocumento", "tipoDocumento");
		criteria.createAlias(COBRANCA_DEBITO_SITUACAO, COBRANCA_DEBITO_SITUACAO);

		if (filtro != null) {

			Long idCliente = (Long) filtro.get("idCliente");
			if (filtro.get("idCliente") != null) {
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}

			Long idTipoDocumento = (Long) filtro.get("idTipoDocumento");
			if (filtro.get("idTipoDocumento") != null) {
				criteria.add(Restrictions.eq("tipoDocumento.chavePrimaria", idTipoDocumento));
			}

			Boolean habilitado = null;
			if (filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO) != null) {
				habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			}

			Boolean entregaDocumento = (Boolean) filtro.get("entregaDocumento");
			if (entregaDocumento) {

				DetachedCriteria subconsulta = DetachedCriteria.forClass(this.getClasseEntidadeEntregaDocumento(), "entregaDocumento");
				subconsulta.createAlias("entregaDocumento.documentoCobranca", "documentoCobranca");
				subconsulta.setProjection(Property.forName("documentoCobranca.chavePrimaria"));
				criteria.add(Subqueries.propertyNotIn("chavePrimaria", subconsulta));

			}

			if (habilitado != null) {
				if (habilitado) {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
				} else {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.FALSE));
				}
			}

		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#processarEmissaoAvisoCorteBatch(java.lang.StringBuilder,
	 * br.com.ggas.batch.acaocomando.dominio.AcaoComando, boolean, boolean)
	 */
	@Override
	public Map<Cliente, byte[]> processarEmissaoAvisoCorteBatch(StringBuilder logProcessamento, AcaoComando acaoComando) 
			throws IOException, GGASException {

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		Map<String, Object> parametros = new HashMap<String, Object>();

		ControladorAvisoCorteEmissao controladorAvisoCorteEmissao = (ControladorAvisoCorteEmissao) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorAvisoCorteEmissao.BEAN_ID_CONTROLADOR_AVISO_CORTE_EMISSAO);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Constantes.COMANDO_COBRANCA_NOTIFICACAO_CORTE, Boolean.FALSE);
		filtro.put(Constantes.COMANDO_COBRANCA_AVISO_CORTE, Boolean.TRUE);
		filtro.put(Fatura.INDICADOR_AVISO_CORTE_FATURA, Boolean.TRUE);
		
		List<Fatura> listaFatura = (List<Fatura>) controladorAvisoCorteEmissao.consultarFaturasValidasAvisoCorte(filtro, acaoComando);

		logProcessamento.append(Constantes.PULA_LINHA + Constantes.PULA_LINHA
						+ "Lista dos [Clientes] - [Pontos de consumo] e quantidade de faturas vencidas:");

		Map<Cliente, Collection<AvisoCorteVO>> clientesRelatorio = processarEmissaoNotificacaoAvisoCorte(logProcessamento, listaFatura,
						Boolean.FALSE);

		// atualiza os registros não processados referente ao controle de emissão de notificação de corte
		Map<String, Object> filtroAvisoCorteAtualizacao = new HashMap<>();
		filtroAvisoCorteAtualizacao.put("indicadorEmissao", Boolean.FALSE);
		filtroAvisoCorteAtualizacao.put("dataProcessamento", null);
		Collection<AvisoCorteEmissao> listaAvisoCorteEmissao = controladorAvisoCorteEmissao.consultar(filtroAvisoCorteAtualizacao);

		Iterator<AvisoCorteEmissao> iterator = listaAvisoCorteEmissao.iterator();

		while (iterator.hasNext()) {
			AvisoCorteEmissao ace = iterator.next();
			ace.setDataProcessamento(Util.getDataCorrente(true));
			controladorAvisoCorteEmissao.atualizar(ace);
		}
		// fim do bloco de atualização de registro não processados na notificação do corte

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		if (empresa != null) {

			if (empresa.getCliente().getEnderecoPrincipal() != null) {
				parametros.put(ENDERECO_EMPRESA, empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento()
								+ empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairroMunicipioUFCEP());
			} else {
				parametros.put(ENDERECO_EMPRESA, StringUtils.EMPTY);
			}
			if (empresa.getCliente().getContatos() != null && !empresa.getCliente().getContatos().isEmpty()) {
				parametros.put("contatoEmpresa", empresa.getCliente().getContatos().iterator().next().getFone()
								+ empresa.getCliente().getContatos().iterator().next().getEmail());
			} else {
				parametros.put("contatoEmpresa", StringUtils.EMPTY);
			}

			if (empresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}

		}
		String local = StringUtils.EMPTY;
		if (empresa != null && empresa.getCliente() != null && empresa.getCliente().getEnderecoPrincipal() != null) {
			local = empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF();
		}

		parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());
		ControladorParametroSistema controSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ParametroSistema gerenteFinanceiro = controSistema.obterParametroPorCodigo(Constantes.PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO);
		if (gerenteFinanceiro != null) {
			parametros.put("gerenteFinanceiro", gerenteFinanceiro.getValor());
		}
		byte[] relatorioAvisoCorte = null;

		String arquivoRelatorio = controladorDocumentoImpressaoLayout.obterDocumentoImpressaoLayoutPorConstante(Constantes.AVISO_CORTE);
		if (arquivoRelatorio == null || arquivoRelatorio.isEmpty()) {

			throw new GGASException(Constantes.ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO);

		}

		Map<Cliente, byte[]> mapaRelatorioAvicoCorteCliente = new HashMap<Cliente, byte[]>();
		parametros.put(PROTOCOLOS, criarParametroProtocolos(clientesRelatorio));
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		for (Entry<Cliente, Collection<AvisoCorteVO>> mapa : clientesRelatorio.entrySet()) {
			if (!mapa.getValue().isEmpty()) {
				relatorioAvisoCorte = RelatorioUtil.gerarRelatorioPDF(mapa.getValue(), parametros, arquivoRelatorio);
				mapaRelatorioAvicoCorteCliente.put(mapa.getKey(), relatorioAvisoCorte);
			}
		}

		return mapaRelatorioAvicoCorteCliente;

	}

	private Collection<SubRelatorioFaturasVencidasCobrancaVO> criarParametroProtocolos(
			Map<Cliente, Collection<AvisoCorteVO>> clientesRelatorio) {
		Collection<SubRelatorioFaturasVencidasCobrancaVO> protocolos = new ArrayList<SubRelatorioFaturasVencidasCobrancaVO>();
		for (Entry<Cliente, Collection<AvisoCorteVO>> mapa : clientesRelatorio.entrySet()) {
			Collection<AvisoCorteVO> camposDocCorte = mapa.getValue();
			for (AvisoCorteVO avisoCorteVO : camposDocCorte) {
				protocolos.addAll(avisoCorteVO.getColecaoFaturaVencida());
			}
		}
		return protocolos;
	}

	/**
	 * Processar emissao notificacao aviso corte.
	 *
	 * @param logProcessamento
	 *            the log processamento
	 * @param listaFatura
	 *            the lista fatura
	 * @param ehNotificacao
	 *            the eh notificacao
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<Cliente, Collection<AvisoCorteVO>> processarEmissaoNotificacaoAvisoCorte(StringBuilder logProcessamento,
					List<Fatura> listaFatura, Boolean ehNotificacao) throws GGASException {

		ControladorAvisoCorte controladorAvisoCorte = (ControladorAvisoCorte) ServiceLocator.getInstancia().getBeanPorID(
						ControladorAvisoCorte.BEAN_ID_CONTROLADOR_AVISO_CORTE);

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String isEnvioEmissaoCortePorEmail = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ENVIO_RELATORIO_EMISSAO_CORTE_POR_EMAIL);

		// agrupar por cliente
		Map<Long, Collection<Fatura>> mapaClienteFatura = new HashMap<Long, Collection<Fatura>>();
		Collection<Cliente> listaCliente = new ArrayList<Cliente>();

		if (listaFatura != null && !listaFatura.isEmpty()) {
			for (Fatura fatura : listaFatura) {
				if (fatura.getPontoConsumo() != null) {

					if (mapaClienteFatura.containsKey(fatura.getCliente().getChavePrimaria())) {
						mapaClienteFatura.get(fatura.getCliente().getChavePrimaria()).add(fatura);
					} else {
						Collection<Fatura> novaListaFatura = new ArrayList<Fatura>();
						novaListaFatura.add(fatura);
						mapaClienteFatura.put(fatura.getCliente().getChavePrimaria(), novaListaFatura);
						listaCliente.add(fatura.getCliente());
					}

				}
			}
		}

		Map<Cliente, Collection<AvisoCorteVO>> mapaAvisoCorteCliente = new HashMap<Cliente, Collection<AvisoCorteVO>>();
		List<AvisoCorteVO> listaAvisoCorteVO = new ArrayList<AvisoCorteVO>();

		int contDocumentosGeradas = 0;
		for (Cliente cliente : listaCliente) {

			Collection<Fatura> listaFaturaCliente = mapaClienteFatura.get(cliente.getChavePrimaria());

			// Popular lista de faturas de um ponto de consumo cliente
			Map<Fatura, Collection<Fatura>> faturasAgrupadas = new HashMap<Fatura, Collection<Fatura>>();
			Map<Long, Collection<Fatura>> mapaPontoConsumoFatura = new HashMap<Long, Collection<Fatura>>();
			Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
			for (Fatura fatura : listaFaturaCliente) {
				if(fatura.getFaturaAgrupada() != null) {
					if(!faturasAgrupadas.containsKey(fatura.getFaturaAgrupada())) {
						faturasAgrupadas.put(fatura.getFaturaAgrupada(), new HashSet<Fatura>());
					}
					faturasAgrupadas.get(fatura.getFaturaAgrupada()).add(fatura);
					continue;
				}
				if (mapaPontoConsumoFatura.containsKey(fatura.getPontoConsumo().getChavePrimaria())) {
					mapaPontoConsumoFatura.get(fatura.getPontoConsumo().getChavePrimaria()).add(fatura);
				} else {
					Collection<Fatura> novaListaFatura = new ArrayList<Fatura>();
					novaListaFatura.add(fatura);
					mapaPontoConsumoFatura.put(fatura.getPontoConsumo().getChavePrimaria(), novaListaFatura);
					listaPontoConsumo.add(fatura.getPontoConsumo());
				}
			}

			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				String codigoUnico = StringUtils.EMPTY;
				Collection<Fatura> listaFaturaPontoConsumo = mapaPontoConsumoFatura.get(pontoConsumo.getChavePrimaria());
				AvisoCorteVO acvo = new AvisoCorteVO();
				StringBuilder pontoEntrega = new StringBuilder();
				StringBuilder codigosUnicos = new StringBuilder();
				Set<PontoConsumo> pontosConsumoAgrupado = new HashSet<PontoConsumo>();
				
				List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturasVencidas = new ArrayList<SubRelatorioFaturasVencidasCobrancaVO>();
				for (Fatura fatura : listaFaturaPontoConsumo) {
					colecaoFaturasVencidas.add(montarVOFaturaVencida(fatura, faturasAgrupadas.get(fatura)));
					
					Collection<Fatura> faturasAgrupadasAux = faturasAgrupadas.get(fatura);
					if (faturasAgrupadas != null && !faturasAgrupadas.isEmpty() && faturasAgrupadasAux != null) {
						for (Fatura faturaAgrupada : faturasAgrupadasAux) {
							pontosConsumoAgrupado.add(faturaAgrupada.getPontoConsumo());
						}
					}

					if (fatura.getPontoConsumo() != null) {
						atualizarAvisoCorte(ehNotificacao, controladorAvisoCorte, cliente, fatura);
						if (fatura.getPontoConsumo().getCodigoLegado() != null) {
							codigoUnico = String.valueOf(fatura.getPontoConsumo().getCodigoLegado());
						} else {
							codigoUnico = String.valueOf(fatura.getPontoConsumo().getChavePrimaria());
						}
					}

					if (fatura.getContrato() != null) {

						acvo.setCodigoContrato(fatura.getContrato().getNumeroCompletoContrato());
					} else {
						acvo.setCodigoContrato(StringUtils.EMPTY);
					}

					if (fatura.getPontoConsumo().getSegmento() != null) {

						acvo.setTipoResidencia(fatura.getPontoConsumo().getSegmento().getDescricao());
					} else {
						acvo.setTipoResidencia(StringUtils.EMPTY);
					}

				}
				for(PontoConsumo pontoAgrupada : pontosConsumoAgrupado) {
					pontoEntrega.append(pontoAgrupada.getDescricao());
					pontoEntrega.append("\n");
					
					if (pontoAgrupada.getCodigoLegado() != null) {
						codigosUnicos.append(String.valueOf(pontoAgrupada.getCodigoLegado()));
					} else {
						codigosUnicos.append(String.valueOf(pontoAgrupada.getChavePrimaria()));
					}
					codigosUnicos.append(" - ");
				}
				
				acvo.setCodigo(String.valueOf(cliente.getChavePrimaria()));
				if(codigosUnicos != null && !"".equals(codigosUnicos.toString())) {
					acvo.setCodigoUnico(codigosUnicos.toString());
				} else {
					acvo.setCodigoUnico(codigoUnico);
				}
				
				acvo.setNome(cliente.getNome());
				acvo.setData(Util.dataAtualPorExtenso());

				acvo.setNome(cliente.getNome());
				

				if (pontoConsumo.getEnderecoFormatado() != null) {
					acvo.setEndereco(pontoConsumo.getEnderecoFormatadoRuaNumeroComplemento());
					acvo.setCep(pontoConsumo.getCep().getCep());
					acvo.setBairro(pontoConsumo.getEnderecoFormatadoBairro());
					acvo.setCidade(pontoConsumo.getEnderecoFormatadoMunicipioUFCEP());
					acvo.setMunicipioUf(pontoConsumo.getEnderecoFormatadoMunicipioUF());
					if(pontoEntrega != null && !"".equals(pontoEntrega.toString())) {
						acvo.setPontoEntrega(pontoEntrega.toString());
					} else {
						acvo.setPontoEntrega(pontoConsumo.getDescricao());
					}
					acvo.setCepCidadeEstado(pontoConsumo.getEnderecoFormatadoCepMunicipioUF());
					acvo.setEnderecoPontoConsumoRelatorioCorteSergas(pontoConsumo.getEnderecoFormatadoRelatorioCorteSergas());
				} else {
					acvo.setEndereco(StringUtils.EMPTY);
					acvo.setCep(StringUtils.EMPTY);
					acvo.setBairro(StringUtils.EMPTY);
					acvo.setCidade(StringUtils.EMPTY);
					acvo.setMunicipioUf(StringUtils.EMPTY);
					acvo.setPontoEntrega(StringUtils.EMPTY);
					acvo.setEnderecoPontoConsumoRelatorioCorteSergas(StringUtils.EMPTY);
				}

				acvo.setColecaoFaturaVencida(colecaoFaturasVencidas);
				listaAvisoCorteVO.add(acvo);

				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("[");
				logProcessamento.append(cliente.getNome());
				logProcessamento.append("] - [");
				logProcessamento.append(pontoConsumo.getDescricao());
				logProcessamento.append("] ");
				logProcessamento.append(colecaoFaturasVencidas.size());
				logProcessamento.append(" fatura(s) vencida(s).");

				contDocumentosGeradas += colecaoFaturasVencidas.size();
			}

			// agrupa os relatórios de aviso de corte por cliente.
			if ("1".equals(isEnvioEmissaoCortePorEmail)) {
				mapaAvisoCorteCliente.put(cliente, listaAvisoCorteVO);
				listaAvisoCorteVO = new ArrayList<>();
			}
		}
		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append(contDocumentosGeradas);
		logProcessamento.append(" fatura(s) vencida(s)");

		// retorna todos os relatórios em um único documento
		if ("0".equals(isEnvioEmissaoCortePorEmail)) {
			mapaAvisoCorteCliente.put(new ClienteImpl(), listaAvisoCorteVO);
		}

		return mapaAvisoCorteCliente;
	}

	private void atualizarAvisoCorte(Boolean ehNotificacao, ControladorAvisoCorte controladorAvisoCorte, Cliente cliente, Fatura fatura)
					throws NegocioException, ConcorrenciaException {

		if (ehNotificacao) {
			AvisoCorte avisoCorte = (AvisoCorte) controladorAvisoCorte.criar();
			avisoCorte.setCliente(cliente);
			avisoCorte.setFatura(fatura);
			avisoCorte.setPontoConsumo(fatura.getPontoConsumo());
			avisoCorte.setDataNotificacao(Util.getDataCorrente(false));
			avisoCorte.setNumeroChecagens(0);
			avisoCorte.setIndicadorFaturaPriorizada(Boolean.FALSE);
			controladorAvisoCorte.inserir(avisoCorte);
		} else {
			Map<String, Object> filtroAvisoCorte = new HashMap<>();
			filtroAvisoCorte.put("idCliente", cliente.getChavePrimaria());
			filtroAvisoCorte.put("idFatura", fatura.getChavePrimaria());
			if (fatura.getPontoConsumo() != null) {
				filtroAvisoCorte.put("idPontoConsumo", fatura.getPontoConsumo().getChavePrimaria());
			}
			Collection<AvisoCorte> listaAvisoCorteBanco = controladorAvisoCorte.listar(filtroAvisoCorte);
			Iterator<AvisoCorte> iterator = listaAvisoCorteBanco.iterator();

			while (iterator.hasNext()) {
				AvisoCorte ac = iterator.next();
				ac.setDataAviso(Util.getDataCorrente(false));
				controladorAvisoCorte.atualizar(ac);
				
				EntregaDocumento entregaDocumento = fachada.criarEntregaDocumento();
				entregaDocumento.setAvisoCorte(ac);
				entregaDocumento.setCliente(ac.getCliente());
				entregaDocumento.setImovel(ac.getFatura().getPontoConsumo().getImovel());
				entregaDocumento.setDataEmissao(Calendar.getInstance().getTime());
				entregaDocumento.setDataSituacao(Calendar.getInstance().getTime());
				entregaDocumento.setIndicadorRetornado(false);
				entregaDocumento.setSituacaoEntrega(
						controladorEntidadeConteudo.listarEntidadeConteudoPorEntidadeClasseEDescricao(
								"Situação da Entrega Documentos", "Pendente"));
				entregaDocumento.setTipoDocumento(controladorTipoDocumento.obterTipoDocumentoDescricao("PROTOCOLO"));
				
				fachada.inserirEntregaDocumento(entregaDocumento);
				
			}
		}
	}


	@Override
	public SubRelatorioFaturasVencidasCobrancaVO montarVOFaturaVencida(Fatura fatura, Collection<Fatura> faturasComponentes)
					throws GGASException {

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		SubRelatorioFaturasVencidasCobrancaVO faturaVO = new SubRelatorioFaturasVencidasCobrancaVO();

		faturaVO.setAtraso(String.valueOf(Util.intervaloDatas(fatura.getDataVencimento(), Util.getDataCorrente(false))));
		faturaVO.setDeposito(StringUtils.EMPTY);
		faturaVO.setEmissao(fatura.getDataEmissaoFormatada());
		if (faturasComponentes == null) {
			faturaVO.setDescricaoPontoConsumo(fatura.getPontoConsumo().getDescricaoFormatada());
		} else {
			Collection<String> descricoesFormatadas = CollectionUtils.collect(faturasComponentes, new Transformer() {

				@Override
				public Object transform(Object input) {
					Fatura fatura = (Fatura) input;
					return fatura.getPontoConsumo().getDescricaoFormatada();
				}

			});
			String separador = Constantes.STRING_ESPACO.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO);
			faturaVO.setDescricaoPontoConsumo(Util.montarStringSemNulos(separador,
					descricoesFormatadas.toArray(new String[descricoesFormatadas.size()])));
		}
		DebitosACobrar calculo = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, Util.getDataCorrente(false),
						fatura.getValorTotal(), false, true);
		if (calculo != null && calculo.getJurosMora() != null) {
			faturaVO.setJuros(calculo.getJurosMora());
		} else {
			faturaVO.setJuros(BigDecimal.ZERO);
		}

		DocumentoFiscal df = controladorFatura.obterDocumentoFiscalPorFaturaMaisRecente(fatura.getChavePrimaria());
		if (df != null) {
			faturaVO.setTitulo(String.valueOf(df.getNumero()));
		} else {
			faturaVO.setTitulo(String.valueOf(fatura.getChavePrimaria()));
		}

		faturaVO.setValor(fatura.getValorTotal());
		faturaVO.setSaldo(fatura.getValorSaldoConciliado());
		if (calculo != null && calculo.getJurosMora() != null) {
			faturaVO.setValorPagar(fatura.getValorTotal().add(calculo.getJurosMora()));
		} else {
			faturaVO.setValorPagar(fatura.getValorTotal());
		}
		faturaVO.setVencimento(fatura.getDataVencimentoFormatada());
		faturaVO.setNomeRota(fatura.getPontoConsumo().getRota().getNumeroRota());
		return faturaVO;
	}

	/**
	 * Montar aviso corte v os.
	 *
	 * @param logProcessamento
	 * @param faturasAgrupadas the faturas agrupadas
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	private Collection<AvisoCorteVO> montarAvisoCorteVOs(StringBuilder logProcessamento, Map<Fatura, Collection<Fatura>> faturasAgrupadas)
					throws GGASException {

		Iterator<Entry<Fatura, Collection<Fatura>>> entradas = faturasAgrupadas.entrySet().iterator();
		Collection<AvisoCorteVO> avisosCorteDeFaturasAgrupadas = new HashSet<AvisoCorteVO>();
		while(entradas.hasNext()) {
			Entry<Fatura, Collection<Fatura>> entrada = entradas.next();
			Fatura faturaAgrupada = entrada.getKey();
			Collection<Fatura> faturasParticipantes = entrada.getValue();
			AvisoCorteVO avisoCorteVO = montarAvisoCorteVO(faturaAgrupada, faturasParticipantes);
			avisosCorteDeFaturasAgrupadas.add(avisoCorteVO);
			String descricaoAgrupadaPontosConsumo = agruparDescricoesPontoConsumo(faturasParticipantes);
			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("[");
			logProcessamento.append(faturaAgrupada.getCliente().getNome());
			logProcessamento.append("] - [");
			logProcessamento.append(descricaoAgrupadaPontosConsumo);
			logProcessamento.append("] ");
			logProcessamento.append(avisoCorteVO.getColecaoFaturaVencida().size());
			logProcessamento.append(" fatura(s) vencida(s).");
		}
		return avisosCorteDeFaturasAgrupadas;
	}

	private String agruparDescricoesPontoConsumo(Collection<Fatura> faturasParticipantes) {
		StringBuilder descricaoAgrupada = new StringBuilder();
		for (Fatura faturaComponente : faturasParticipantes) {
			if (descricaoAgrupada.length() > 0) {
				descricaoAgrupada.append(Constantes.STRING_ESPACO).append(Constantes.STRING_BARRA).append(Constantes.STRING_ESPACO);
			}
			descricaoAgrupada.append(faturaComponente.getPontoConsumo().getDescricao());
		}
		return descricaoAgrupada.toString();
	}

	/**
	 * Montar aviso corte vo.
	 *
	 * @param faturaAgrupada the fatura agrupada
	 * @param faturasComponentes the faturas componentes
	 * @return the aviso corte vo
	 * @throws GGASException the GGAS exception
	 */
	private AvisoCorteVO montarAvisoCorteVO(Fatura faturaAgrupada, Collection<Fatura> faturasComponentes) throws GGASException {
		AvisoCorteVO acvo = new AvisoCorteVO();
		acvo.setCodigoContrato(faturaAgrupada.getContrato().getNumeroCompletoContrato());
		Cliente cliente = faturaAgrupada.getCliente();
		acvo.setCodigo(String.valueOf(cliente.getChavePrimaria()));
		acvo.setNome(cliente.getNome());
		acvo.setData(Util.dataAtualPorExtenso());
		acvo.setNome(cliente.getNome());

		Set<String> tipoResidencia = new HashSet<String>();
		Set<String> codigoUnico = new HashSet<String>();
		Set<String> endereco = new HashSet<String>();
		Set<String> cep = new HashSet<String>();
		Set<String> bairro = new HashSet<String>();
		Set<String> cidade = new HashSet<String>();
		Set<String> municipioUf = new HashSet<String>();
		Set<String> pontoEntrega = new HashSet<String>();
		Set<String> cepCidadeEstado = new HashSet<String>();
		Set<String> enderecoPontoConsumoRelatorioCorteSergas = new HashSet<String>();

		for (Fatura faturaComponente : faturasComponentes) {
			tipoResidencia.add(faturaComponente.getPontoConsumo().getSegmento().getDescricao());
			if (faturaComponente.getPontoConsumo().getCodigoLegado() != null) {
				codigoUnico.add(String.valueOf(faturaComponente.getPontoConsumo().getCodigoLegado()));
			} else {
				codigoUnico.add(String.valueOf(faturaComponente.getPontoConsumo().getChavePrimaria()));
			}
			endereco.add(faturaComponente.getPontoConsumo().getEnderecoFormatadoRuaNumeroComplemento());
			cep.add(faturaComponente.getPontoConsumo().getCep().getCep());
			bairro.add(faturaComponente.getPontoConsumo().getEnderecoFormatadoBairro());
			cidade.add(faturaComponente.getPontoConsumo().getEnderecoFormatadoMunicipioUFCEP());
			municipioUf.add(faturaComponente.getPontoConsumo().getEnderecoFormatadoMunicipioUF());
			pontoEntrega.add(faturaComponente.getPontoConsumo().getDescricao());
			cepCidadeEstado.add(faturaComponente.getPontoConsumo().getEnderecoFormatadoCepMunicipioUF());
			enderecoPontoConsumoRelatorioCorteSergas.add(faturaComponente.getPontoConsumo().getEnderecoFormatadoRelatorioCorteSergas());
		}
		String separador = Constantes.STRING_ESPACO.concat(Constantes.STRING_BARRA).concat(Constantes.STRING_ESPACO);
		acvo.setTipoResidencia(Util.montarStringSemNulos(separador, tipoResidencia.toArray(new String[tipoResidencia.size()])));
		acvo.setCodigoUnico(Util.montarStringSemNulos(separador, codigoUnico.toArray(new String[codigoUnico.size()])));
		acvo.setEndereco(Util.montarStringSemNulos(separador, endereco.toArray(new String[endereco.size()])));
		acvo.setCep(Util.montarStringSemNulos(separador, cep.toArray(new String[cep.size()])));
		acvo.setBairro(Util.montarStringSemNulos(separador, bairro.toArray(new String[bairro.size()])));
		acvo.setCidade(Util.montarStringSemNulos(separador, cidade.toArray(new String[cidade.size()])));
		acvo.setMunicipioUf(Util.montarStringSemNulos(separador, municipioUf.toArray(new String[municipioUf.size()])));
		acvo.setPontoEntrega(Util.montarStringSemNulos(separador, pontoEntrega.toArray(new String[pontoEntrega.size()])));
		acvo.setCepCidadeEstado(Util.montarStringSemNulos(separador, cepCidadeEstado.toArray(new String[cepCidadeEstado.size()])));
		acvo.setEnderecoPontoConsumoRelatorioCorteSergas(Util.montarStringSemNulos(separador,
						enderecoPontoConsumoRelatorioCorteSergas.toArray(new String[enderecoPontoConsumoRelatorioCorteSergas.size()])));

		acvo.setColecaoFaturaVencida(Arrays.asList(montarVOFaturaVencida(faturaAgrupada, faturasComponentes)));
		return acvo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#processarEmissaoNotificacaoCorteBatch(java.lang.StringBuilder,
	 * br.com.ggas.batch.acaocomando.dominio.AcaoComando, boolean, boolean)
	 */
	@Override
	public Map<Cliente, byte[]> processarEmissaoNotificacaoCorteBatch(StringBuilder logProcessamento, AcaoComando acaoComando) 
			throws IOException, GGASException {

		ControladorAvisoCorteEmissao controladorAvisoCorteEmissao = (ControladorAvisoCorteEmissao) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorAvisoCorteEmissao.BEAN_ID_CONTROLADOR_AVISO_CORTE_EMISSAO);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(Constantes.COMANDO_COBRANCA_NOTIFICACAO_CORTE, Boolean.TRUE);
		filtro.put(Constantes.COMANDO_COBRANCA_AVISO_CORTE, Boolean.FALSE);
		
		List<Fatura> listaFatura = (List<Fatura>) controladorAvisoCorteEmissao.consultarFaturasValidasNotificacaoCorte(filtro,
						acaoComando);

		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append("Lista dos [Clientes] - [Pontos de consumo] e quantidade de faturas vencidas:");

		Map<Cliente, Collection<AvisoCorteVO>> clientesRelatorio = processarEmissaoNotificacaoAvisoCorte(logProcessamento, listaFatura,
						Boolean.TRUE);

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Map<String, Object> parametros = new HashMap<String, Object>();

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		if (empresa != null) {

			if (empresa.getCliente().getEnderecoPrincipal() != null) {
				parametros.put(ENDERECO_EMPRESA, empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento()
								+ empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairroMunicipioUFCEP());
			} else {
				parametros.put(ENDERECO_EMPRESA, StringUtils.EMPTY);
			}
			if (empresa.getCliente().getContatos() != null && !empresa.getCliente().getContatos().isEmpty()) {
				parametros.put("contatoEmpresa", empresa.getCliente().getContatos().iterator().next().getFone()
								+ empresa.getCliente().getContatos().iterator().next().getEmail());
			} else {
				parametros.put("contatoEmpresa", StringUtils.EMPTY);
			}

			if (empresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}

		}
		String local = StringUtils.EMPTY;
		if (empresa != null && empresa.getCliente() != null && empresa.getCliente().getEnderecoPrincipal() != null) {
			local = empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF();
		}

		parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());

		byte[] relatorioAvisoCorte = null;

		String arquivoRelatorio = controladorDocumentoImpressaoLayout
						.obterDocumentoImpressaoLayoutPorConstante(Constantes.NOTIFICACAO_CORTE);
		if (arquivoRelatorio == null || arquivoRelatorio.isEmpty()) {

			throw new GGASException("Cadastre um Layout em Documento Impressao Layout");

		}

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ParametroSistema gerenteFinanceiro = 
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO);
		if (gerenteFinanceiro != null) {
			parametros.put("gerenteFinanceiro", gerenteFinanceiro.getValor());
		}

		logProcessamento.append(Constantes.PULA_LINHA + Constantes.PULA_LINHA);
		logProcessamento.append( "[");
		logProcessamento.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()));
		logProcessamento.append("] ");
		logProcessamento.append( "Gerando relatórios de notificação de corte.");
		
		ParametroSistema parametroExibirMensagemContaEOuNotificacao = 
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO);
		
		Map<Cliente, byte[]> mapaRelatorioAvicoCorteCliente = new HashMap<Cliente, byte[]>();
		
		if (parametroExibirMensagemContaEOuNotificacao != null
				&& verificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorte(
						parametroExibirMensagemContaEOuNotificacao)) {
			parametros.put(PROTOCOLOS, criarParametroProtocolos(clientesRelatorio));
			parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
			for (Entry<Cliente, Collection<AvisoCorteVO>> mapa : clientesRelatorio.entrySet()) {
				if (!mapa.getValue().isEmpty()) {
					relatorioAvisoCorte = RelatorioUtil.gerarRelatorioPDF(mapa.getValue(), parametros,
							arquivoRelatorio);
					mapaRelatorioAvicoCorteCliente.put(mapa.getKey(), relatorioAvisoCorte);
				}
			}
		}
		
		return mapaRelatorioAvicoCorteCliente;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#calcularValorTotalDebitos(java.lang.Long[], java.lang.Long[], java.util.Date)
	 */
	@Override
	public BigDecimal calcularValorTotalDebitos(Long[] chavesFatura, Long[] chavesCreditoDebito, Date dataVencimento) throws GGASException {

		validarDados(chavesFatura, chavesCreditoDebito);

		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		BigDecimal valorCorrigido = null;
		BigDecimal valorCorrigidoAtualizado = BigDecimal.ZERO;

		if (chavesCreditoDebito.length > 0) {

			Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorCreditoDebito
							.consultarCreditosDebitosPorChaves(chavesCreditoDebito);

			for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {
				Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
								.obterCDDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				if ((listaCDDetalhamento != null) && (!listaCDDetalhamento.isEmpty())) {
					CreditoDebitoDetalhamento creditoDebitoDetalhamento = ((List<CreditoDebitoDetalhamento>) listaCDDetalhamento).get(0);
					if (!creditoDebitoDetalhamento.getLancamentoItemContabil().getTipoCreditoDebito().getDescricaoAbreviada()
									.equals(CREDITO)) {

						valorCorrigido = creditoDebitoARealizar.getValor();
						valorCorrigidoAtualizado = valorCorrigidoAtualizado.add(valorCorrigido);

					}

				}
			}

		}

		if (chavesFatura.length > 0) {

			Collection<Fatura> listaFatura = controladorFatura.consultarFaturasPorChaves(chavesFatura);
			for (Fatura fatura : listaFatura) {

				if (!fatura.getTipoDocumento().getDescricaoAbreviada().equals(NOTA_CREDITO)) {

					BigDecimal valorPago = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
					DebitosACobrar debitosACobrarVO = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, dataVencimento,
									valorPago, Boolean.TRUE, Boolean.TRUE);

					valorCorrigido = fatura.getValorTotal();
					if (debitosACobrarVO != null) {
						if (debitosACobrarVO.getJurosMora() != null) {
							valorCorrigido = valorCorrigido.add(debitosACobrarVO.getJurosMora());
						}

						if (debitosACobrarVO.getMulta() != null) {
							valorCorrigido = valorCorrigido.add(debitosACobrarVO.getMulta());
						}
					}

					valorCorrigidoAtualizado = valorCorrigidoAtualizado.add(valorCorrigido);
				}

			}
		}

		return valorCorrigidoAtualizado;
	}

	/**
	 * Validar dados.
	 *
	 * @param chavesFatura
	 *            the chaves fatura
	 * @param chavesCreditoDebito
	 *            the chaves credito debito
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDados(Long[] chavesFatura, Long[] chavesCreditoDebito) throws NegocioException {

		if ((chavesFatura == null || chavesFatura.length == 0) && (chavesCreditoDebito == null || chavesCreditoDebito.length == 0)) {
			throw new NegocioException(Constantes.ERRO_SELECIONE_ALGUMA_FATURA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.ControladorCobranca#consultarFaturasNaoPagasCliente(java.lang.Long, java.lang.String)
	 */
	@Override
	public Collection<Fatura> consultarFaturasNaoPagasCliente(Long chaveCliente, String anoFaturas) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idSituacaoPagamentoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

		String idSituacaoPagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(WHERE);

		// Refeactoring de Faturamento
		// agrupado
		hql.append(" fatura.faturaAgrupada is null ");

		hql.append(" and fatura.cliente.chavePrimaria = :chaveCliente ");

		hql.append(" and cast(year(fatura.dataVencimento) as string) < :anoFaturas ");

		hql.append(" and (fatura.situacaoPagamento.chavePrimaria = :idSituacaoPagamentoParcialmentePago ");
		hql.append(" or fatura.situacaoPagamento.chavePrimaria = :idSituacaoPagamentoPendente) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("anoFaturas", anoFaturas);
		query.setLong("chaveCliente", chaveCliente);

		query.setLong("idSituacaoPagamentoParcialmentePago", Long.parseLong(idSituacaoPagamentoParcialmentePago));
		query.setLong("idSituacaoPagamentoPendente", Long.parseLong(idSituacaoPagamentoPendente));

		return query.list();
	}

	private ClienteVO popularRelatorioDeclaracaoQuitacaoAnualDebitos(Cliente cliente, Fatura fatura, boolean indicadorPago,
					Long situacaoParcialmentePaga, Map<Long, ClienteVO> mapaCliente) throws GGASException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		ClienteVO clienteVO = new ClienteVO();

		DadosExtratoDebito dadosExtrato = null;

		if (mapaCliente.containsKey(cliente.getChavePrimaria())) {
			clienteVO = mapaCliente.get(cliente.getChavePrimaria());
			for (DadosExtratoDebito dados : clienteVO.getListaDadosExtratoDebito()) {
				if (dados.getCodigoPontoConsumo() == fatura.getPontoConsumo().getChavePrimaria()) {
					dadosExtrato = dados;
					break;
				}
			}
			if (dadosExtrato != null) {
				FaturaVO faturaVO = new FaturaVO();
				faturaVO.setNumero(String.valueOf(fatura.getChavePrimaria()));
				faturaVO.setDescricao(this.obterDescricaoFatura(fatura));
				faturaVO.setTipo(fatura.getTipoDocumento().getDescricao());
				faturaVO.setVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
				// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
				BigDecimal valorFatura = fatura.getValorSaldoConciliado();
				if ((!indicadorPago) && (fatura.getSituacaoPagamento() != null)
								&& (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoParcialmentePaga)) {
					BigDecimal valorRecebimento = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
					if (valorRecebimento != null) {
						valorFatura = valorFatura.subtract(valorRecebimento);
					}
				}
				faturaVO.setValor(Util.converterCampoCurrencyParaString(valorFatura, Constantes.LOCALE_PADRAO));
				dadosExtrato.getListaFaturasVO().add(faturaVO);
			} else {
				dadosExtrato = this.criarDadosExtratoDebito();
				PontoConsumo pontoConsumo = fatura.getPontoConsumo();
				dadosExtrato.setPontoConsumo(pontoConsumo.getDescricao());
				dadosExtrato.setCodigoPontoConsumo(pontoConsumo.getChavePrimaria());
				if (pontoConsumo.getSegmento() != null) {
					dadosExtrato.setSegmento(pontoConsumo.getSegmento().getDescricao());
				}
				if (pontoConsumo.getCep() != null) {
					dadosExtrato.setCep(pontoConsumo.getCep().getCep());
				}
				dadosExtrato.setSituacao(pontoConsumo.getSituacaoConsumo().getDescricao());

				FaturaVO faturaVO = new FaturaVO();
				faturaVO.setNumero(String.valueOf(fatura.getChavePrimaria()));
				faturaVO.setDescricao(this.obterDescricaoFatura(fatura));
				faturaVO.setTipo(fatura.getTipoDocumento().getDescricao());
				faturaVO.setVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
				// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
				BigDecimal valorFatura = fatura.getValorSaldoConciliado();
				if ((!indicadorPago) && (fatura.getSituacaoPagamento() != null)
								&& (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoParcialmentePaga)) {
					BigDecimal valorRecebimento = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
					if (valorRecebimento != null) {
						valorFatura = valorFatura.subtract(valorRecebimento);
					}
				}

				faturaVO.setValor(Util.converterCampoCurrencyParaString(valorFatura, Constantes.LOCALE_PADRAO));
			}

		} else {

			clienteVO.setNomeCliente(cliente.getNome());
			clienteVO.setDocIdentificador(cliente.getNumeroDocumentoFormatado());
			clienteVO.setCodigoCliente(String.valueOf(cliente.getChavePrimaria()));
			clienteVO.setInscricaoEstadual(cliente.getInscricaoEstadual());
			clienteVO.setTipoCliente(cliente.getTipoCliente().getDescricao());
			PontoConsumo pontoConsumo = fatura.getPontoConsumo();
			dadosExtrato = this.criarDadosExtratoDebito();
			dadosExtrato.setPontoConsumo(pontoConsumo.getDescricao());
			dadosExtrato.setCodigoPontoConsumo(pontoConsumo.getChavePrimaria());
			if (pontoConsumo.getSegmento() != null) {
				dadosExtrato.setSegmento(pontoConsumo.getSegmento().getDescricao());
			}
			if (pontoConsumo.getCep() != null) {
				dadosExtrato.setCep(pontoConsumo.getCep().getCep());
			}
			dadosExtrato.setSituacao(pontoConsumo.getSituacaoConsumo().getDescricao());

			Map<Long, ContratoPontoConsumo> mapaContratoPonto = new HashMap<>();
			ContratoPontoConsumo contratoPonto = null;
			if (!mapaContratoPonto.containsKey(pontoConsumo.getChavePrimaria())) {
				contratoPonto = controladorContrato.obterContratoPontoConsumoParaEnderecoEntrega(pontoConsumo.getChavePrimaria());
				mapaContratoPonto.put(pontoConsumo.getChavePrimaria(), contratoPonto);
			} else {
				contratoPonto = mapaContratoPonto.get(pontoConsumo.getChavePrimaria());
			}

			if ((contratoPonto != null) && (contratoPonto.getCep() != null)) {
				clienteVO.setEndereco(contratoPonto.getEnderecoFormatado());
			} else if (cliente.getEnderecoPrincipal() != null) {
				clienteVO.setEndereco(cliente.getEnderecoPrincipal().getEnderecoFormatado());
			}

			FaturaVO faturaVO = new FaturaVO();
			faturaVO.setNumero(String.valueOf(fatura.getChavePrimaria()));
			faturaVO.setDescricao(this.obterDescricaoFatura(fatura));
			faturaVO.setTipo(fatura.getTipoDocumento().getDescricao());
			faturaVO.setVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
			// Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
			BigDecimal valorFatura = fatura.getValorSaldoConciliado();
			if ((!indicadorPago) && (fatura.getSituacaoPagamento() != null)
							&& (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoParcialmentePaga)) {
				BigDecimal valorRecebimento = this.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
				if (valorRecebimento != null) {
					valorFatura = valorFatura.subtract(valorRecebimento);
				}
			}
			faturaVO.setValor(Util.converterCampoCurrencyParaString(valorFatura, Constantes.LOCALE_PADRAO));
			dadosExtrato.getListaFaturasVO().add(faturaVO);
			clienteVO.getListaDadosExtratoDebito().add(dadosExtrato);
			mapaCliente.put(cliente.getChavePrimaria(), clienteVO);
		}

		return clienteVO;
	}

	@Override
	public void validarExibirExtratoDebito(Long idCliente, Long idPontoConsumo, Long idImovel) throws NegocioException {

		if ((idCliente == null || idCliente <= 0) && (idImovel == null || idImovel <= 0)) {
			throw new NegocioException(ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA, true);
		} else if ((idImovel != null && idImovel > 0) && (idPontoConsumo == null || idPontoConsumo <= 0)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_PONTO_CONSUMO_NAO_SELECIONADO, true);
		}
	}

	/**
	 * Listar as cobrancas por cliente.
	 *
	 * @param cliente
	 * return Listas de Documentos de Cobranca de um cliente
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoCobrancaVO> listarCobrancasPorCliente(Cliente cliente) {
		Criteria criteria = createCriteria(getClasseEntidadeDocumentoCobranca(), "doco");
		criteria.createAlias("doco.cliente", "clie");
		criteria.createAlias("doco.tipoDocumento", "tido");
		criteria.createAlias("doco.cobrancaDebitoSituacao", "cods");

		ProjectionList projections = Projections.projectionList();
		projections.add(Projections.property("doco.chavePrimaria"), DocumentoCobrancaVO.Field.chavePrimaria.toString());
		projections.add(Projections.property("doco.dataEmissao"), DocumentoCobrancaVO.Field.dataEmissao.toString());
		projections.add(Projections.property("doco.valorTotal"), DocumentoCobrancaVO.Field.valorTotal.toString());
		projections.add(Projections.property("doco.dataVencimento"), DocumentoCobrancaVO.Field.dataVencimento.toString());
		projections.add(Projections.property("cods.descricao"), DocumentoCobrancaVO.Field.situacaoDescricao.toString());
		projections.add(Projections.property("tido.descricao"), DocumentoCobrancaVO.Field.tipoDocDescricao.toString());
		criteria.setProjection(projections);

		if(cliente.getChavePrimaria() != 0){
			criteria.add(Restrictions.eq("clie.chavePrimaria", cliente.getChavePrimaria()));
		}

		criteria.setResultTransformer(Transformers.aliasToBean(DocumentoCobrancaVO.class));
		return (List<DocumentoCobrancaVO>) criteria.addOrder(Order.asc("doco.chavePrimaria")).list();
	}

	/**
	 * Listar os Recebimentos por cobranca.
	 *
	 * @param cobranca
	 * return Listas de Documentos de Cobranca de um cliente
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoCobrancaVO> listarRecebimentosPorCobranca(DocumentoCobrancaVO cobranca) {
		Criteria criteria = createCriteria(getClasseEntidadeRecebimento(), "rece");
		criteria.createAlias("rece.documentoCobrancaItem", "doci", Criteria.FULL_JOIN);
		criteria.createAlias("doci.documentoCobranca", "doco", Criteria.FULL_JOIN);
		
		ProjectionList projections2 = Projections.projectionList();
		projections2.add(Projections.property("rece.dataRecebimento"), DocumentoCobrancaVO.Field.dataPagamento.toString());
		criteria.setProjection(projections2);
		if(cobranca.getChavePrimaria() != null){
			criteria.add(Restrictions.eq("doco.chavePrimaria", cobranca.getChavePrimaria()));
		}
		criteria.setResultTransformer(Transformers.aliasToBean(DocumentoCobrancaVO.class));
		
		return criteria.list();
	}
	 
	/**
	 * Obter fatura com o Documento De Cobranca Item.
	 * @param documentoCobrancaVO
	 * return Documentos de Cobranca VO
	 */
	@Override
	public DocumentoCobrancaVO obterFaturaPorDocCobrancaItem(DocumentoCobrancaVO documentoCobrancaVO) {
		Criteria criteria = createCriteria(getClasseEntidadeDocumentoCobrancaItem(), "doci"); 
		criteria.createAlias("doci.documentoCobranca", "doco", Criteria.INNER_JOIN);
		criteria.createAlias("doci.faturaGeral", "fage", Criteria.INNER_JOIN);
		criteria.createAlias("fage.faturaAtual", "fatu", Criteria.INNER_JOIN);
		
		ProjectionList projections = Projections.projectionList();
		projections.add(Projections.property("doci.chavePrimaria"), DocumentoCobrancaVO.Field.docCobrancaItemID.toString());
		projections.add(Projections.property("fatu.chavePrimaria"), DocumentoCobrancaVO.Field.faturaID.toString());
		criteria.setProjection(projections);
		
		if(documentoCobrancaVO != null){
			criteria.add(Restrictions.eq("doco.chavePrimaria", documentoCobrancaVO.getChavePrimaria()));
		}
		criteria.setResultTransformer(Transformers.aliasToBean(DocumentoCobrancaVO.class));
		DocumentoCobrancaVO resultado = (DocumentoCobrancaVO) criteria.uniqueResult();
		if(resultado != null){
			return resultado;
		}
		return null;
	}
	
	
	private boolean verificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorte(ParametroSistema parametroSistema) {
		return VALOR_PARAMETRO_APENAS_ARQUIVO_DE_NOTIFICACAO.equals(parametroSistema.getValor()) || 
				VALOR_PARAMETRO_ARQUIVO_DE_NOTIFICACAO_E_CONTA.equals(parametroSistema.getValor());  
	}
	
	
	@Override
	public byte[] gerarRelatorioCreditoDebitoRealizar(DocumentoCobranca documentoCobranca, String arquivoJasper, boolean isExtratoDebito,
					boolean isSegundaVia, CreditoDebitoNegociado creditoNegociado) throws GGASException {

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		byte[] bytes = null;
		if (documentoCobranca != null) {
			Collection<DadosExtratoDebito> listaDadosExtrato = new ArrayList<DadosExtratoDebito>();
			Map<String, Object> parametros = new HashMap<String, Object>();
			Map<String, Object> filtro = new HashMap<String, Object>();
			String tipoNota = "";
			filtro.put("principal", Boolean.TRUE);
			Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);
			Empresa empresa = null;
			if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
				empresa = ((List<Empresa>) listaEmpresas).get(0);
			}
			if (empresa != null && empresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
				parametros.put("razao_social_CDL", empresa.getCliente().getNome());
				parametros.put("endereco_CDL", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroBairroMunicipioUF());
				parametros.put("cep_CDL", empresa.getCliente().getEnderecoPrincipal().getCep().getCep());
				parametros.put("municipio_CDL", empresa.getCliente().getEnderecoPrincipal().getMunicipio().getDescricao() + ", ");
				parametros.put("cnpj_CDL", empresa.getCliente().getCnpjFormatado());
				parametros.put("uf_CDL", empresa.getCliente().getEnderecoPrincipal().getMunicipio().getUnidadeFederacao().getSigla());
				parametros.put("inscricao_estadual_CDL", empresa.getCliente().getInscricaoEstadual());
				
				Boolean rodapeAlgas = Boolean.FALSE;
				
				if("ALGAS - GAS DE ALAGOAS SA".equals(empresa.getCliente().getNome())) {
					rodapeAlgas = Boolean.TRUE;
				}
				
				parametros.put("rodapeAlgas", rodapeAlgas);
				
			}
			this.popularRelatorioMapaCliente(parametros, documentoCobranca, isSegundaVia);
			this.popularRelatorioDetalhamento(parametros, documentoCobranca, listaDadosExtrato, isExtratoDebito);

			Cliente cliente = (Cliente) ServiceLocator.getInstancia().getControladorCliente()
							.obter(documentoCobranca.getCliente().getChavePrimaria(), ENDERECOS, "enderecoPrincipal.cep");

			if (!cliente.getEnderecos().isEmpty()) {
				parametros.put("endereco_cliente", cliente.getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento());
				parametros.put("cep_cliente", cliente.getEnderecoPrincipal().getCep().getCep());
				parametros.put("cidade", cliente.getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF());
			}

			parametros.put("destinatario_cliente", cliente.getNome());

			if (!StringUtils.EMPTY.equals(cliente.getCnpjFormatado())) {
				parametros.put(CNPJ_CPF_CLIENTE, cliente.getCnpjFormatado());
				parametros.put(TIPO_PESSOA, "CNPJ:");
			} else {
				parametros.put(CNPJ_CPF_CLIENTE, cliente.getCpfFormatado());
				parametros.put(TIPO_PESSOA, "CPF:");
			}

			if (empresa != null && empresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM_LOGOMARCA_EMPRESA, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}

			if (creditoNegociado != null) {
				parametros.put("data_emissao", Util.converterDataParaString(creditoNegociado.getDataFinanciamento()));
				parametros.put("observacao", creditoNegociado.getDescricao());
				parametros.put("referencia", DataUtil.converterAnoMesEmData(creditoNegociado.getAnoMesInclusao()));
				parametros.put("nr_fatura", String.valueOf(creditoNegociado.getChavePrimaria()));
				
			}
			
			if(creditoNegociado.getCreditoOrigem() != null) {
				tipoNota = "Crédito";
			} else {
				tipoNota = "Débito";
			}
			
			parametros.put("tipoNota", tipoNota);
			parametros.put("listaCreditoDebito", listaDadosExtrato);
			parametros.put("quantidadeTotal", creditoNegociado.getQuantidadeTotalPrestacoes());
			parametros.put("valorTotalizado", Util.converterCampoCurrencyParaString(creditoNegociado.getValor(), Constantes.LOCALE_PADRAO));
			parametros.put("descricaoCredito", creditoNegociado.getDescricao());


			bytes = RelatorioUtil.gerarRelatorioPDF(listaDadosExtrato, parametros, arquivoJasper);
		}
		return bytes;
	}
}
