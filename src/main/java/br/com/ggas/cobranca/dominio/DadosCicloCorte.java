package br.com.ggas.cobranca.dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;

import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.faturamento.Fatura;

/**
 * Classe responsável pela validacao das faturas que entrarao no ciclo de corte
 * 
 */
public class DadosCicloCorte {
	
	/**
	 * Retorna a lista de faturas que ao estao aptas a entrarem no ciclo de corte, de acordo com o comando.
	 *
	 * @param faturas - a lista de todas as faturas
	 * @param filtroTelaHabilitado - {@link Boolean}
	 * @param comando - {@link AcaoComando}}
	 * @return faturasInvalidas 
	 */
	public static Collection<Fatura> obterFaturasInvalidasEmissaoNotificacaoCorte( Collection<Fatura> faturas, 
			Boolean filtroTelaHabilitado, AcaoComando comando) {
		Collection<Fatura> faturasInvalidas = new ArrayList<Fatura>();
		
		faturas.forEach(fatura -> {
			if (!validarFaturaEmissaoNotificacaoCorte(fatura, filtroTelaHabilitado, comando)) {
				faturasInvalidas.add(fatura);
			}
		});

		return faturasInvalidas;
	}

	/**
	 * Valida se a fatura esta apta para a emissao na notificacao de corte 
	 *
	 * @param fatura - {@link Fatura}}
	 * @param filtroTelaHabilitado - {@link Boolean}
	 * @param comando - {@link AcaoComando}}
	 * @return {@link Boolean}} 
	 */
	public static boolean validarFaturaEmissaoNotificacaoCorte(Fatura fatura, Boolean filtroTelaHabilitado, AcaoComando comando) {
		
		if(!faturaValidaEmRevisao(fatura) 
				&& validarFiltroHabilitado(fatura, filtroTelaHabilitado)) {
			
			if (comando != null && !fatura.getHabilitadoCobranca()) {
				return false;
			}
			return true;
		}

		return false;
	}
	
	/**
	 * Valida se a fatura esta em processo de revisao 
	 *
	 * @param fatura - {@link Fatura}}
	 * @return {@link Boolean}} 
	 */
	private static boolean faturaValidaEmRevisao(Fatura fatura) {
		
		Date dataAtual = Calendar.getInstance().getTime();
		
		if(fatura.emRevisao()) {
			DateTime dataRevisaoCalculada = new DateTime(fatura.getDataRevisao());
			Integer dias = fatura.getMotivoRevisao().getPrazoMaximoDias();
			dataRevisaoCalculada = dataRevisaoCalculada.plusDays(dias);

			if(dataRevisaoCalculada.toDate().after(dataAtual)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Valida se o filtro e a fatura estao habilitados para cobranca 
	 *
	 * @param fatura - {@link Fatura}}
	 * @param filtroTela - {@link Boolean}
	 * @return {@link Boolean}} 
	 */
	private static boolean validarFiltroHabilitado(Fatura fatura, Boolean filtroTela) {
		if(filtroTela != null) {
			if(filtroTela) {
				if(!fatura.getHabilitadoCobranca()) {
					return false;
				}
			} else {
				if(fatura.getHabilitadoCobranca()) {
					return false;
				}
			}
		}
		
		return true;
	}

}
