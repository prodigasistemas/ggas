/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 16/01/2014 17:57:06
 @author ccavalcanti
 */

package br.com.ggas.cobranca.declaracaoquitacaoanual.impl;

import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.declaracaoquitacaoanual.DeclaracaoQuitacaoAnual;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela implementação da declaração de quitação anual. 
 *
 */
public class DeclaracaoQuitacaoAnualImpl extends EntidadeNegocioImpl implements DeclaracaoQuitacaoAnual {

	private static final long serialVersionUID = -1563125257078715906L;

	private Cliente cliente;

	private String codigoValidacao;

	private Long anoReferencia;

	private byte[] relatorio;

	@Override
	public Cliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public String getCodigoValidacao() {

		return codigoValidacao;
	}

	@Override
	public void setCodigoValidacao(String codigoValidacao) {

		this.codigoValidacao = codigoValidacao;
	}

	@Override
	public Long getAnoReferencia() {

		return anoReferencia;
	}

	@Override
	public void setAnoReferencia(Long anoReferencia) {

		this.anoReferencia = anoReferencia;
	}

	@Override
	public byte[] getRelatorio() {
		byte[] retorno = null;
		if (this.relatorio != null) {
			retorno = relatorio.clone();
		}
		return retorno;
	}

	@Override
	public void setRelatorio(byte[] relatorio) {
		if (relatorio != null) {
			this.relatorio = relatorio.clone();
		} else {
			this.relatorio = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
