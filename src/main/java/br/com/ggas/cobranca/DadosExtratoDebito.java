/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.web.faturamento.fatura.FaturaVO;
/**
 * Interface responsável pela assinatura dos métodos relacionados aos Dados de Extrato de Débito 
 *
 */
public interface DadosExtratoDebito extends Serializable {

	String BEAN_ID_DADOS_EXTRATO_DEBITO = "dadosExtratoDebito";

	/**
	 * @return the nomeCliente
	 */
	String getNomeCliente();

	/**
	 * @param nomeCliente
	 *            the nomeCliente to set
	 */
	void setNomeCliente(String nomeCliente);

	/**
	 * @return the identificacao
	 */
	String getIdentificacao();

	/**
	 * @param identificacao
	 *            the identificacao to set
	 */
	void setIdentificacao(String identificacao);

	/**
	 * @return the docIdentificador
	 */
	String getDocIdentificador();

	/**
	 * @param docIdentificador
	 *            the docIdentificador to set
	 */
	void setDocIdentificador(String docIdentificador);

	/**
	 * @return the endereco
	 */
	String getEndereco();

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	void setEndereco(String endereco);

	/**
	 * @return the codigoCliente
	 */
	String getCodigoCliente();

	/**
	 * @param codigoCliente
	 *            the codigoCliente to set
	 */
	void setCodigoCliente(String codigoCliente);

	/**
	 * @return the inscricaoEstadual
	 */
	String getInscricaoEstadual();

	/**
	 * @param inscricaoEstadual
	 *            the inscricaoEstadual to set
	 */
	void setInscricaoEstadual(String inscricaoEstadual);

	/**
	 * @return the tipoCliente
	 */
	String getTipoCliente();

	/**
	 * @param tipoCliente
	 *            the tipoCliente to set
	 */
	void setTipoCliente(String tipoCliente);

	/**
	 * @return the pontoConsumo
	 */
	String getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(String pontoConsumo);

	/**
	 * @return the segmento
	 */
	String getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(String segmento);

	/**
	 * @return the situacao
	 */
	String getSituacao();

	/**
	 * @param situacao
	 *            the situacao to set
	 */
	void setSituacao(String situacao);

	/**
	 * @return the cep
	 */
	String getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(String cep);

	/**
	 * @return the numero
	 */
	String getNumero();

	/**
	 * @param numero
	 *            the numero to set
	 */
	void setNumero(String numero);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the tipo
	 */
	String getTipo();

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	void setTipo(String tipo);

	/**
	 * @return the vencimento
	 */
	String getVencimento();

	/**
	 * @param vencimento
	 *            the vencimento to set
	 */
	void setVencimento(String vencimento);

	/**
	 * @return the valor
	 */
	String getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	void setValor(String valor);

	/**
	 * @return String - Valor por extenso.
	 */
	String getValorPorExtenso();

	/**
	 * @param valorPorExtenso - Set Valor por extenso.
	 */
	void setValorPorExtenso(String valorPorExtenso);

	/**
	 * @return the quantidade
	 */
	String getQuantidade();

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	void setQuantidade(String quantidade);

	/**
	 * @return the valorUnitario
	 */
	String getValorUnitario();

	/**
	 * @param valorUnitario
	 *            the valorUnitario to set
	 */
	void setValorUnitario(String valorUnitario);

	/**
	 * @return the descricaoConciliacao
	 */
	String getDescricaoConciliacao();

	/**
	 * @param descricaoConciliacao
	 *            the descricaoConciliacao to set
	 */
	void setDescricaoConciliacao(String descricaoConciliacao);

	/**
	 * @return the descricaoRecebimentos
	 */
	String getDescricaoRecebimentos();

	/**
	 * @param descricaoRecebimentos
	 *            the descricaoRecebimentos to set
	 */
	void setDescricaoRecebimentos(String descricaoRecebimentos);

	/**
	 * @return BigDecimal - Retorna Valor Doc.
	 */
	BigDecimal getValorDoc();

	/**
	 * @param valorDoc - Set Valor Doc.
	 */
	void setValorDoc(BigDecimal valorDoc);

	/**
	 * @param documentoFiscal - Set Documento Fiscal.
	 */
	void setDocumentoFiscal(String documentoFiscal);

	/**
	 * @return String - Retorna Documento fiscal.
	 */
	String getDocumentoFiscal();

	/**
	 * @return String - Retorna Dias atraso.
	 */
	String getDiasAtraso();

	/**
	 * @param diasAtraso - Set Dias atraso.
	 */
	void setDiasAtraso(String diasAtraso);
	
	/**
	 * @return the listaFaturasVO
	 */
	public Collection<FaturaVO> getListaFaturasVO();

	/**
	 * @param listaFaturasVO the listaFaturasVO to set
	 */
	public void setListaFaturasVO(Collection<FaturaVO> listaFaturasVO);
	
	/**
	 * @return the codigoPontoConsumo
	 */
	public Long getCodigoPontoConsumo();

	/**
	 * @param codigoPontoConsumo the codigoPontoConsumo to set
	 */
	public void setCodigoPontoConsumo(Long codigoPontoConsumo);
}
