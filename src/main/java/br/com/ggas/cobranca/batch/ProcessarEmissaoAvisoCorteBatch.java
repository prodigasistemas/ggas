/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/12/2013 15:22:32
 @author wcosta
 */

package br.com.ggas.cobranca.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Classe responsável pela implementação dos métodos para emissão do aviso de corte. 
 *
 */
@Component
public class ProcessarEmissaoAvisoCorteBatch implements Batch {

	private static final String PROCESSO = "processo";

	private static final String ID_COMANDO = "idComando";

	private static final String RELATORIO_AVISO_CORTE = "AvisoCorte";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String isEnvioEmissaoCortePorEmail = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ENVIO_RELATORIO_EMISSAO_CORTE_POR_EMAIL);
		ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia().getBeanPorID(
						ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
		AtividadeSistema atividadeSistema = controladorProcesso.obterAtividadeSistemaPorChaveOperacao(processo.getOperacao()
						.getChavePrimaria());

		ControladorAcaoComando controladorAcaoComando = ServiceLocator.getInstancia().getControladorAcaoComando();
		AcaoComando acaoComando = null;
		if (parametros.containsKey(ID_COMANDO) && Long.parseLong(parametros.get(ID_COMANDO).toString()) > 0) {
			acaoComando = controladorAcaoComando.obterAcaoComando(Long.parseLong(parametros.get(ID_COMANDO).toString()), "acao",
							"grupoFaturamento", "localidade", "setorComercial", "municipio", "marcaMedidor", "modeloMedidor",
							"marcaCorretor", "modeloCorretor", "situacaoConsumo", "segmento", "acao.servicoTipo.servicoTipoPrioridade",
							"acao.servicoTipo.listaEquipamentoEspecial", "acao.servicoTipo.listaMateriais",
							"acao.servicoTipo.indicadorGeraLote", "acao.servicoTipoPesquisa.indicadorPesquisaSatisfacao",
							"acao.chamadoAssuntoPesquisa.indicadorGeraServicoAutorizacao", "acao.chamadoAssuntoPesquisa");
		}

		try {

			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("[");
			logProcessamento.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()));
			logProcessamento.append("] ");
			logProcessamento.append("Iniciando o processo batch de geração de aviso de corte.");

			ServiceLocator.getInstancia().getBeanPorID(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

			ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
							ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);

			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("[");
			logProcessamento.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()));
			logProcessamento.append("] ");
			logProcessamento.append("Consultando todos os clientes com débitos no ano selecionado.");

			Map<Cliente, byte[]> mapaBytes = controladorCobranca.processarEmissaoAvisoCorteBatch(logProcessamento, acaoComando);

			if (mapaBytes != null && !mapaBytes.isEmpty()) {
				ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia().getControladorProcessoDocumento();

				ByteArrayInputStream byteArrayInputStream = null;
				File pdf = null;

				for (Entry<Cliente, byte[]> mapa : mapaBytes.entrySet()) {
					byte[] bytes = mapa.getValue();
					Cliente cliente = mapa.getKey();
					byteArrayInputStream = new ByteArrayInputStream(bytes);

					ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
									.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

					String diretorioRelatorioGerados = (String) controladorParametrosSistema
									.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);
					pdf = Util.getFile(diretorioRelatorioGerados);
					if (!pdf.exists()) {
						pdf.mkdirs();
					}

					final String nomeDocumento = RELATORIO_AVISO_CORTE + String.valueOf(processo.getChavePrimaria())
														+ Calendar.getInstance().getTime().getTime();
					
					final String diretorioDocumento = diretorioRelatorioGerados + nomeDocumento + Constantes.EXTENSAO_ARQUIVO_PDF;
					controladorProcesso.salvarRelatorio(processo, controladorProcessoDocumento, byteArrayInputStream, nomeDocumento,
							diretorioDocumento);

					if ("1".equals(isEnvioEmissaoCortePorEmail) && cliente.getEmailPrincipal() != null) {
						if (atividadeSistema.getDescricaoEmailRemetente() == null) {
							throw new NegocioException(Constantes.ERRO_ATIVIDADE_SISTEMA_EMAIL_REMETENTE, atividadeSistema.getDescricao());
						}

						JavaMailUtil javaMailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(
										JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

						ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(bytes, "application/pdf");

						StringBuilder conteudoEmail = new StringBuilder();
						
						conteudoEmail.append("Código do Processo: ");
						conteudoEmail.append(processo.getChavePrimaria());
						conteudoEmail.append("\r\n");
						
						conteudoEmail.append("Descrição do Processo: ");
						conteudoEmail.append(processo.getOperacao().getDescricao());
						conteudoEmail.append("\r\n");

						javaMailUtil.enviar(atividadeSistema.getDescricaoEmailRemetente(), cliente.getEmailPrincipal(),
										"Relatório de Aviso de Corte", conteudoEmail.toString(),
										"Relatorio_Aviso_Corte" + processo.getChavePrimaria() + Constantes.EXTENSAO_ARQUIVO_PDF,
										byteArrayDataSource);

					}
				}
			}

		} catch (IOException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		}

		return logProcessamento.toString();

	}
}
