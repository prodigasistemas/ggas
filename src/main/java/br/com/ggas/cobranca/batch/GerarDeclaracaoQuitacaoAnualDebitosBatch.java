/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável por implementar os métodos para gerar a declaracao de quitação anual de débitos
 *
 */
@Component
public class GerarDeclaracaoQuitacaoAnualDebitosBatch implements Batch {

	private static final String CLIENTES_ENCONTRADOS = " clientes encontrados.";

	private static final String PROCESSO = "processo";

	private static final String SALVANDO_RELATORIO_DEBITOS_EM_PDF_NO_SERVIDOR = 
					"\r\nSalvando relatório de débitos em aberto em pdf no servidor.";

	private static final String SALVANDO_RELATORIO_DECLARACAO_EM_PDF_NO_SERVIDOR = 
					"\r\nSalvando relatório de declaração de quitação anual de débitos em pdf no servidor.";

	private static final String ARQUIVO_RELATORIO_DECLARACAO_QUITACAO_ANUAL_DEBITOS = "RelatorioDeclaracaoQuitacaoAnualDebitos";

	private static final String ARQUIVO_RELATORIO_DEBITOS_EM_ABERTO = "RelatorioDeclaracaoDebitosEmAberto";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(final Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);				

		try {

			logProcessamento.append("Iniciando o processo batch de geração de relatório de declaração de quitação anual de débitos");

			ServiceLocator serviceLocator = ServiceLocator.getInstancia();
			ControladorCliente controladorCliente = (ControladorCliente) serviceLocator
							.getBeanPorID(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
			ControladorCobranca controladorCobranca = (ControladorCobranca) serviceLocator
							.getBeanPorID(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
			ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) serviceLocator
							.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			String anoGeracao = processo.getParametros().get("anoGeracao");
			logProcessamento.append("\r\nGerar relatório para o ano ");
			logProcessamento.append(anoGeracao);

			String parametroPaginas = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.QUANTIDADE_CLIENTES_RELATORIO_LOTE);
			logProcessamento.append("\r\nParâmetro de número de página por relatório: ");
			logProcessamento.append(parametroPaginas);
			Integer quantidadePaginas = Util.converterCampoStringParaValorInteger("\r\nParâmetro de número de clientes por relatório",
							parametroPaginas);

			Long pagamentoPendente = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
			Long parcialmentePaga = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));
			Long pagamentoEfetivado = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO));
			Long tipoFatura = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
			Long tipoNotaDebito = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));

			// Montando mapa de parâmetro que serão
			// usados nas consultas das faturas dos
			// clientes.
			Map<String, Long> parametrosConsulta = new HashMap<>();
			parametrosConsulta.put("pagamentoPendente", pagamentoPendente);
			parametrosConsulta.put("parcialmentePaga", parcialmentePaga);
			parametrosConsulta.put("tipoFatura", tipoFatura);
			parametrosConsulta.put("pagamentoEfetivado", pagamentoEfetivado);
			parametrosConsulta.put("tipoNotaDebito", tipoNotaDebito);

			Long[] situacaoPagamentoPaga = {pagamentoEfetivado};
			Long[] situacaoPagamentoNaoPaga = {pagamentoPendente, parcialmentePaga};
			Long[] tiposDocumento = {tipoFatura, tipoNotaDebito};

			logProcessamento.append("\r\nConsultando todos os clientes com débitos nos anos anteriores ao selecionado.");
			Collection<Cliente> listaClientesDebitosAnteriores = controladorCliente.listarClientesParaFaturasSituacaoPagamento(anoGeracao,
							situacaoPagamentoNaoPaga, tiposDocumento, true);
			
			logProcessamento.append("\r\n");
			logProcessamento.append(listaClientesDebitosAnteriores.size());
			logProcessamento.append(CLIENTES_ENCONTRADOS);

			logProcessamento.append("\r\nConsultando todos os clientes com débitos no ano selecionado.");
			Collection<Cliente> listaClientesDebitos = controladorCliente.listarClientesParaFaturasSituacaoPagamento(anoGeracao,
							situacaoPagamentoNaoPaga, tiposDocumento, false);
			
			logProcessamento.append("\r\n");
			logProcessamento.append(listaClientesDebitos.size());
			logProcessamento.append(CLIENTES_ENCONTRADOS);

			logProcessamento.append("\r\nConsultando todos os clientes com faturas quitadas até o ano selecionado.");
			Collection<Cliente> listaClientesDeclaracao = controladorCliente.listarClientesParaFaturasSituacaoPagamento(anoGeracao,
							situacaoPagamentoPaga, tiposDocumento, null);
			
			logProcessamento.append("\r\n");
			logProcessamento.append(listaClientesDeclaracao.size());
			logProcessamento.append(CLIENTES_ENCONTRADOS);

			logProcessamento.append("\r\nGerando relatórios de declaração de quitação anual de débitos");
			List<Cliente> listaClientesRelatorio = new ArrayList<>();
			for (Cliente cliente : listaClientesDeclaracao) {

				listaClientesRelatorio.add(cliente);
				// Se o tamanho da lista atingir o
				// parametro de quantidade de páginas
				// ou se for o último cliente da
				// lista, gera o relatório.
				if ((listaClientesRelatorio.size() == quantidadePaginas)
								|| (((List<Cliente>) listaClientesDeclaracao).indexOf(cliente) == listaClientesDeclaracao.size() - 1)) {

					byte[] bytes = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnualGeral(listaClientesRelatorio, anoGeracao,
									Boolean.TRUE, quantidadePaginas, parametrosConsulta);

					if (bytes != null) {
						
						logProcessamento.append(SALVANDO_RELATORIO_DECLARACAO_EM_PDF_NO_SERVIDOR);
						trySavarrelatorioQuitacaoAnualPdf(processo, bytes);
					}
					listaClientesRelatorio = new ArrayList<>();
				}
			}

			listaClientesRelatorio = new ArrayList<>();
			logProcessamento.append("\r\nGerando relatórios de débitos em aberto");
			for (Cliente cliente : listaClientesDebitos) {

				listaClientesRelatorio.add(cliente);
				// Se o tamanho da lista atingir o
				// parametro de quantidade de páginas
				// ou se for o último cliente da
				// lista, gera o relatório.
				if ((listaClientesRelatorio.size() == quantidadePaginas)
								|| (((List<Cliente>) listaClientesDebitos).indexOf(cliente) == listaClientesDebitos.size() - 1)) {

					byte[] bytes = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnualGeral(listaClientesRelatorio, anoGeracao,
									Boolean.FALSE, quantidadePaginas, parametrosConsulta);

					if (bytes != null) {
						logProcessamento.append(SALVANDO_RELATORIO_DEBITOS_EM_PDF_NO_SERVIDOR);
						trySalvarRelatorioDebitosPdf(processo, bytes);
					}

					listaClientesRelatorio = new ArrayList<>();
				}
			}

			logProcessamento.append("\r\nGerando relatório de clientes que tem débitos em aberto nos anos anteriores a ");
			logProcessamento.append(anoGeracao);
			
			controladorCobranca.gerarRelatorioDebitosAnosAnterioresLote(listaClientesDebitosAnteriores, anoGeracao, parametrosConsulta);

		} catch (HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		} catch (NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new NegocioException(e);
		} catch (GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		} catch (Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

	private void trySalvarRelatorioDebitosPdf(Processo processo, byte[] bytes) {
		ByteArrayInputStream byteArrayInputStream;
		File pdf;
		FileOutputStream saida;
		try {
			byteArrayInputStream = new ByteArrayInputStream(bytes);

			ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
							.getInstancia().getControladorNegocio(
											ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			pdf = Util.getFile(
							(String) controladorParametrosSistema
											.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF)
											+ ARQUIVO_RELATORIO_DEBITOS_EM_ABERTO
											+ String.valueOf(processo.getChavePrimaria())
											+ Calendar.getInstance().getTime().getTime() + Constantes.EXTENSAO_ARQUIVO_PDF);
			saida = new FileOutputStream(pdf);
			int data;
			while ((data = byteArrayInputStream.read()) != -1) {
				char ch = (char) data;
				saida.write(ch);
			}
			saida.flush();
			saida.close();
		} catch (Exception e) {
			throw new InfraestruturaException(e.getMessage(), e);
		}
	}

	private void trySavarrelatorioQuitacaoAnualPdf(Processo processo, byte[] bytes) {
		ByteArrayInputStream byteArrayInputStream;
		File pdf;
		FileOutputStream saida;
		try {
			byteArrayInputStream = new ByteArrayInputStream(bytes);

			ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
							.getInstancia().getControladorNegocio(
											ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			pdf = Util.getFile(
							(String) controladorParametrosSistema
											.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF)
											+ ARQUIVO_RELATORIO_DECLARACAO_QUITACAO_ANUAL_DEBITOS
											+ String.valueOf(processo.getChavePrimaria())
											+ Calendar.getInstance().getTime().getTime() + Constantes.EXTENSAO_ARQUIVO_PDF);
			saida = new FileOutputStream(pdf);
			int data;
			while ((data = byteArrayInputStream.read()) != -1) {
				char ch = (char) data;
				saida.write(ch);
			}
			saida.flush();
			saida.close();
		} catch (Exception e) {
			throw new InfraestruturaException(e.getMessage(), e);
		}
	}

}
