/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca;

import java.io.Serializable;
/**
 * Interface responsável pela assinatura dos métodos relacionados aos extratos de credito debito. 
 *
 */
public interface ExtratoDebitoCreditoDebito extends Serializable {

	String BEAN_ID_EXTRATO_DEBITO_CREDITO_DEBITO = "extratoDebitoCreditoDebito";

	/**
	 * @return the idCreditoDebitoPrincipal
	 */
	long getIdCreditoDebitoPrincipal();

	/**
	 * @param idCreditoDebitoPrincipal
	 *            the idCreditoDebitoPrincipal to
	 *            set
	 */
	void setIdCreditoDebitoPrincipal(long idCreditoDebitoPrincipal);

	/**
	 * @return the tipoDocumento
	 */
	String getTipoDocumento();

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	void setTipoDocumento(String tipoDocumento);

	/**
	 * @return the descricaoRubrica
	 */
	String getDescricaoRubrica();

	/**
	 * @param descricaoRubrica
	 *            the descricaoRubrica to set
	 */
	void setDescricaoRubrica(String descricaoRubrica);

	/**
	 * @return the inicioCobranca
	 */
	String getInicioCobranca();

	/**
	 * @param inicioCobranca
	 *            the inicioCobranca to set
	 */
	void setInicioCobranca(String inicioCobranca);

	/**
	 * @return the parcelasCobradas
	 */
	String getParcelasCobradas();

	/**
	 * @param parcelasCobradas
	 *            the parcelasCobradas to set
	 */
	void setParcelasCobradas(String parcelasCobradas);

	/**
	 * @return the totalParcelas
	 */
	String getTotalParcelas();

	/**
	 * @param totalParcelas
	 *            the totalParcelas to set
	 */
	void setTotalParcelas(String totalParcelas);

	/**
	 * @return the valorAVista
	 */
	String getValorAVista();

	/**
	 * @param valorAVista
	 *            the valorAVista to set
	 */
	void setValorAVista(String valorAVista);

	/**
	 * @return the valorTotal
	 */
	String getValorTotal();

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	void setValorTotal(String valorTotal);

	/**
	 * @return the idPontoConsumo
	 */
	long getIdPontoConsumo();

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	void setIdPontoConsumo(long idPontoConsumo);

	/**
	 * @return the quantidadeNaoCobrada
	 */
	int getQuantidadeNaoCobrada();

	/**
	 * @param quantidadeNaoCobrada
	 *            the quantidadeNaoCobrada to set
	 */
	void setQuantidadeNaoCobrada(int quantidadeNaoCobrada);
}
