/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCobrancaImpl representa uma ControladorCobrancaImpl no sistema.
 *
 * @since 22/02/2010
 * 
 */

package br.com.ggas.cobranca.acao.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cobranca.acao.AcaoCobranca;
import br.com.ggas.cobranca.acao.ControladorAcaoCobranca;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 *
 */
class ControladorAcaoCobrancaImpl extends ControladorNegocioImpl implements ControladorAcaoCobranca {

	private static final int LIMITE_DESCRICAO = 100;
	private static final String ERRO_NEGOCIO_ACAO_VINCULADA = "ERRO_NEGOCIO_ACAO_VINCULADA";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (AcaoCobranca) ServiceLocator.getInstancia().getBeanPorID(AcaoCobranca.BEAN_ID_ACAOCOBRANCA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(AcaoCobranca.BEAN_ID_ACAOCOBRANCA);
	}

	public Class<?> getClasseEntidadeAcaoCobranca() {

		return ServiceLocator.getInstancia().getClassPorID(AcaoCobranca.BEAN_ID_ACAOCOBRANCA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.acao.ControladorAcaoCobranca#consultarAcoesCobranca(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoCobranca> consultarAcoesCobranca(Map<String, Object> filtro) throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeAcaoCobranca());

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			AcaoCobranca acaoPrecedente = (AcaoCobranca) filtro.get("cobrancaAcao");
			if(acaoPrecedente != null) {
				criteria.add(Restrictions.eq("", acaoPrecedente.getChavePrimaria()));
			}

			Long idDocumento = (Long) filtro.get("tipoDocumentoGerado");
			if(idDocumento != null) {
				criteria.add(Restrictions.eq("documento.chavePrimaria", idDocumento));
			}

			String descricao = (String) filtro.get(EntidadeClasse.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(EntidadeClasse.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Integer numeroDiasValidade = (Integer) filtro.get("numeroDiasValidade");
			if((numeroDiasValidade != null) && (numeroDiasValidade > 0)) {
				criteria.add(Restrictions.eq("numeroDiasValidade", numeroDiasValidade));
			}

			Integer numeroDiasVencimento = (Integer) filtro.get("numeroDiasVencimento");
			if((numeroDiasVencimento != null) && (numeroDiasVencimento > 0)) {
				criteria.add(Restrictions.eq("numeroDiasVencimento", numeroDiasVencimento));
			}

			Boolean indicadorGerarOrdemServico = (Boolean) filtro.get("indicadorGerarOrdemServico");
			if(indicadorGerarOrdemServico != null) {
				criteria.add(Restrictions.eq("indicadorGerarOrdemServico", indicadorGerarOrdemServico));
			}

			Boolean indicadorConsolidarDebitosAVencer = (Boolean) filtro.get("indicadorConsolidarDebitosAVencer");
			if(indicadorConsolidarDebitosAVencer != null) {
				criteria.add(Restrictions.eq("indicadorConsolidarDebitosAVencer", indicadorConsolidarDebitosAVencer));
			}

			Boolean indicadorBloquearDocumentosPagaveis = (Boolean) filtro.get("indicadorBloquearDocumentosPagaveis");
			if(indicadorBloquearDocumentosPagaveis != null) {
				criteria.add(Restrictions.eq("indicadorBloquearDocumentosPagaveis", indicadorBloquearDocumentosPagaveis));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.medicao.anormalidade.
	 * ControladorAcaoCobranca#
	 * listarAcaoCobranca()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoCobranca> listarAcaoCobranca() throws GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeAcaoCobranca().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorAcaoCobranca
	 * #obterAcaoCobranca(java.lang.Long)
	 */
	@Override
	public AcaoCobranca obterAcaoCobranca(Long chavePrimaria) throws GGASException {

		AcaoCobranca acaoCobranca = null;
		if(chavePrimaria != null) {
			acaoCobranca = (AcaoCobranca) this.obter(chavePrimaria, getClasseEntidadeAcaoCobranca());
		}
		return acaoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		AcaoCobranca acaoCobranca = (AcaoCobranca) entidadeNegocio;

		validarDescricaoAcaoCobranca(acaoCobranca);

		// consultar a ação de cobrança
		Collection<AcaoCobranca> listaAcaoesExistente = this.consultarAcaoExistente(acaoCobranca.getDescricao(),
						acaoCobranca.getChavePrimaria());
		if(listaAcaoesExistente != null && !listaAcaoesExistente.isEmpty()) {
			throw new NegocioException("Já existe uma Ação de Cobrança com a mesma descrição");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		AcaoCobranca acaoCobranca = (AcaoCobranca) entidadeNegocio;

		validarDescricaoAcaoCobranca(acaoCobranca);

		// consultar a ação de cobrança
		Collection<AcaoCobranca> listaAcaoesExistente = this.consultarAcaoExistente(acaoCobranca.getDescricao(),
						acaoCobranca.getChavePrimaria());
		if(listaAcaoesExistente != null && !listaAcaoesExistente.isEmpty()) {
			throw new NegocioException("Já existe uma Ação de Cobrança com a mesma descrição");
		}

		if(acaoCobranca.getAcaoPrecedente() != null
						&& acaoCobranca.getAcaoPrecedente().getChavePrimaria() == acaoCobranca.getChavePrimaria()) {
			throw new NegocioException("Uma Ação de Cobrança não pode ter ela mesma como predecente.");
		}
	}

	/**
	 * Validar descricao acao cobranca.
	 * 
	 * @param acaoCobranca
	 *            the acao cobranca
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDescricaoAcaoCobranca(AcaoCobranca acaoCobranca) throws NegocioException {

		if(acaoCobranca.getDescricao() != null && acaoCobranca.getDescricao().length() > LIMITE_DESCRICAO) {
			throw new NegocioException(ERRO_DESCRICAO_MAIOR_100_CARACTERES, true);
		}
	}

	/**
	 * Consultar acao existente.
	 * 
	 * @param nomeAcao
	 *            the nome acao
	 * @param idAcaoCobranca
	 *            the id acao cobranca
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<AcaoCobranca> consultarAcaoExistente(String nomeAcao, Long idAcaoCobranca) {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq(EntidadeClasse.ATRIBUTO_DESCRICAO, nomeAcao));
		criteria.add(Restrictions.ne(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idAcaoCobranca));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.acao.ControladorAcaoCobranca#consultarQuantidadeAcoesCobrancaPorAcaoPredecessora(java.lang.Long)
	 */
	@Override
	public Integer consultarQuantidadeAcoesCobrancaPorAcaoPredecessora(Long chaveAcao) throws NegocioException {

		Query query = null;
		Long resultado = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(" count(acaoCobranca.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeAcaoCobranca().getSimpleName());
		hql.append(" acaoCobranca ");
		hql.append(" where ");
		hql.append(" acaoCobranca.acaoPrecedente.chavePrimaria = :idAcaoPredecessora ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idAcaoPredecessora", chaveAcao);
		resultado = (Long) query.uniqueResult();
		return resultado.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		AcaoCobranca acaoCobranca = (AcaoCobranca) entidadeNegocio;
		Integer quantidade = consultarQuantidadeAcoesCobrancaPorAcaoPredecessora(acaoCobranca.getChavePrimaria());
		if(quantidade > 0) {
			throw new NegocioException(ERRO_NEGOCIO_ACAO_VINCULADA, true);
		}

	}
}
