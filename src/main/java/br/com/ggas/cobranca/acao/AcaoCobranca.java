/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cobranca.acao;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados a Ação de cobrança. 
 *
 */
public interface AcaoCobranca extends EntidadeNegocio {

	String ACAO_COBRANCA = "ACAO_COBRANCA";

	String ACOES_COBRANCA = "ACOES_COBRANCA";

	String BEAN_ID_ACAOCOBRANCA = "acaoCobranca";

	String ACAO_PRECEDENTE = "ACAO_PRECEDENTE";

	String DOCUMENTO = "DOCUMENTO";

	String DESCRICAO = "DESCRICAO";

	String NUMERO_DIAS_VALIDADE = "NUMERO_DIAS_VALIDADE";

	String NUMERO_DIAS_VENCIMENTO = "NUMERO_DIAS_VENCIMENTO";

	String INDICADOR_GERAR_ORDEM_SERVICO = "INDICADOR_GERAR_ORDEM_SERVICO";

	String INDICADOR_CONSOLIDAR_DEBITOS_A_VENCER = "INDICADOR_CONSOLIDAR_DEBITOS_A_VENCER";

	String INDICADOR_BLOQUEAR_DOCUMENTOS_PAGAVEIS = "INDICADOR_BLOQUEAR_DOCUMENTOS_PAGAVEIS";

	String TIPO_NUMERO_DIAS_VALIDADE = "TIPO_NUMERO_DIAS_VALIDADE";

	String TIPO_NUMERO_DIAS_VENCIMENTO = "TIPO_NUMERO_DIAS_VENCIMENTO";

	/**
	 * @return the acaoPrecedente
	 */
	AcaoCobranca getAcaoPrecedente();

	/**
	 * @param cobrancaAcao
	 *            the cobrancaAcao to set
	 */
	void setAcaoPrecedente(AcaoCobranca cobrancaAcao);

	/**
	 * @return the documento
	 */
	TipoDocumento getDocumento();

	/**
	 * @param documento
	 *            the documento to set
	 */
	void setDocumento(TipoDocumento documento);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the numeroDiasValidade
	 */
	Integer getNumeroDiasValidade();

	/**
	 * @param numeroDiasValidade
	 *            the numeroDiasValidade to set
	 */
	void setNumeroDiasValidade(Integer numeroDiasValidade);

	/**
	 * @return the numeroDiasVencimento
	 */
	Integer getNumeroDiasVencimento();

	/**
	 * @param numeroDiasVencimento
	 *            the numeroDiasVencimento to set
	 */
	void setNumeroDiasVencimento(Integer numeroDiasVencimento);

	/**
	 * Checks if is gerar ordem servico.
	 * 
	 * @return the gerarOrdemServico
	 */
	Boolean isGerarOrdemServico();

	/**
	 * @param gerarOrdemServico
	 *            the gerarOrdemServico to set
	 */
	void setGerarOrdemServico(Boolean gerarOrdemServico);

	/**
	 * Checks if is considerar debitos a vencer.
	 * 
	 * @return the consolidarDebitosAVencer
	 */
	Boolean isConsiderarDebitosAVencer();

	/**
	 * @param considerarDebitosAVencer - Set considerar debistos a vencer.
	 */
	void setConsiderarDebitosAVencer(Boolean considerarDebitosAVencer);

	/**
	 * Checks if is bloquear documentos pagaveis.
	 * 
	 * @return the bloquearDocumentosPagaveis
	 */
	Boolean isBloquearDocumentosPagaveis();

	/**
	 * @param bloquearDocumentosPagaveis
	 *            the bloquearDocumentosPagaveis
	 *            to set
	 */
	void setBloquearDocumentosPagaveis(Boolean bloquearDocumentosPagaveis);

	/**
	 * @return the TipoNumeroDiasValidade
	 */
	Long getTipoNumeroDiasValidade();

	/**
	 * @param tipoNumeroDiasValidade - Set tipo número de dias validade.
	 */
	void setTipoNumeroDiasValidade(Long tipoNumeroDiasValidade);

	/**
	 * @return the numeroDiasVencimento
	 */
	Long getTipoNumeroDiasVencimento();

	/**
	 * @param tipoNumeroDiasVencimento - Set número de dias do vencimento.
	 */
	void setTipoNumeroDiasVencimento(Long tipoNumeroDiasVencimento);
}
