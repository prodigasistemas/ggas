/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.acao;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados com o Controlador de Ação de Cobrança
 * 
 *
 */
public interface ControladorAcaoCobranca extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ACAO_COBRANCA = "controladorAcaoCobranca";

	String ERRO_DESCRICAO_MAIOR_100_CARACTERES = "ERRO_DESCRICAO_MAIOR_100_CARACTERES";

	/**
	 * Método responsável por criar um documento
	 * de acao de cobranca.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Um DocumentoCobranca.
	 * @throws GGASException
	 *             the GGAS exception
	 */

	/**
	 * Método responsável por consultar as Ações e
	 * Cobranças de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de Ações e Cobranças.
	 * @return Uma coleção de Ações e Cobranças.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<AcaoCobranca> consultarAcoesCobranca(Map<String, Object> filtro) throws GGASException;

	/**
	 * Método responsável por listar as Ações
	 * Cobrança.
	 * 
	 * @return Uma coleção de Ações e Cobranças.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<AcaoCobranca> listarAcaoCobranca() throws GGASException;

	/**
	 * Método responsável por obter uma Acao
	 * Cobranca pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma Ação de Cobrança
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	AcaoCobranca obterAcaoCobranca(Long chavePrimaria) throws GGASException;

	/**
	 * Método responsável por consultar a
	 * quantidade de ações que possuem a ação
	 * passada por parâmetro como ação
	 * predecessora.
	 * 
	 * @param chaveAcao
	 *            Chave da Ação Predecessora.
	 * @return Quantidade de ações que possuem a
	 *         ação predecessora.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Integer consultarQuantidadeAcoesCobrancaPorAcaoPredecessora(Long chaveAcao) throws NegocioException;

}
