/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cobranca.acao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cobranca.acao.AcaoCobranca;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

class AcaoCobrancaImpl extends EntidadeNegocioImpl implements AcaoCobranca {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 2472638432785352020L;

	private AcaoCobranca acaoPrecedente;

	private TipoDocumento documento;

	private String descricao;

	private Integer numeroDiasValidade;

	private Integer numeroDiasVencimento;

	private Boolean gerarOrdemServico;

	private Boolean considerarDebitosAVencer;

	private Boolean bloquearDocumentosPagaveis;

	private Long tipoNumeroDiasVencimento;

	private Long tipoNumeroDiasValidade;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * getAcaoPrecedente()
	 */
	@Override
	public AcaoCobranca getAcaoPrecedente() {

		return acaoPrecedente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setAcaoPrecedente(br.
	 * com.procenge.ggas.cobranca.acao.AcaoCobranca
	 * )
	 */
	@Override
	public void setAcaoPrecedente(AcaoCobranca acaoPrecedente) {

		this.acaoPrecedente = acaoPrecedente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setDescricao(java.lang
	 * .String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * getNumeroDiasValidade()
	 */
	@Override
	public Integer getNumeroDiasValidade() {

		return numeroDiasValidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setNumeroDiasValidade
	 * (Integer)
	 */
	@Override
	public void setNumeroDiasValidade(Integer numeroDiasValidade) {

		this.numeroDiasValidade = numeroDiasValidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * getNumeroDiasVencimento()
	 */
	@Override
	public Integer getNumeroDiasVencimento() {

		return numeroDiasVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setNumeroDiasVencimento
	 * (Integer)
	 */
	@Override
	public void setNumeroDiasVencimento(Integer numeroDiasVencimento) {

		this.numeroDiasVencimento = numeroDiasVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * isGerarOrdemServico()
	 */
	@Override
	public Boolean isGerarOrdemServico() {

		return gerarOrdemServico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setGerarOrdemServico(
	 * java.lang.Boolean)
	 */
	@Override
	public void setGerarOrdemServico(Boolean gerarOrdemServico) {

		this.gerarOrdemServico = gerarOrdemServico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * isConsolidarDebitosAVencer
	 * ()
	 */
	@Override
	public Boolean isConsiderarDebitosAVencer() {

		return considerarDebitosAVencer;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setConsolidarDebitosAVencer
	 * (java.lang.Boolean)
	 */
	@Override
	public void setConsiderarDebitosAVencer(Boolean considerarDebitosAVencer) {

		this.considerarDebitosAVencer = considerarDebitosAVencer;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * isBloquearDocumentosPagaveis
	 * ()
	 */
	@Override
	public Boolean isBloquearDocumentosPagaveis() {

		return bloquearDocumentosPagaveis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setBloquearDocumentosPagaveis
	 * (java.lang.Boolean)
	 */
	@Override
	public void setBloquearDocumentosPagaveis(Boolean bloquearDocumentosPagaveis) {

		this.bloquearDocumentosPagaveis = bloquearDocumentosPagaveis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * getDocumento()
	 */
	@Override
	public TipoDocumento getDocumento() {

		return documento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.acao.AcaoCobranca#
	 * setDocumento(br.com.procenge
	 * .ggas.cobranca.acao.DocumentoAcaoCobranca)
	 */
	@Override
	public void setDocumento(TipoDocumento documento) {

		this.documento = documento;
	}

	@Override
	public Long getTipoNumeroDiasVencimento() {

		return tipoNumeroDiasVencimento;
	}

	@Override
	public void setTipoNumeroDiasVencimento(Long tipoNumeroDiasVencimento) {

		this.tipoNumeroDiasVencimento = tipoNumeroDiasVencimento;
	}

	@Override
	public Long getTipoNumeroDiasValidade() {

		return tipoNumeroDiasValidade;
	}

	@Override
	public void setTipoNumeroDiasValidade(Long tipoNumeroDiasValidade) {

		this.tipoNumeroDiasValidade = tipoNumeroDiasValidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados
	 * ()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(documento == null) {
			stringBuilder.append(DOCUMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(numeroDiasValidade != null && (tipoNumeroDiasValidade == null || tipoNumeroDiasValidade < 0)) {
			stringBuilder.append(TIPO_NUMERO_DIAS_VALIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(numeroDiasVencimento != null && (tipoNumeroDiasVencimento == null || tipoNumeroDiasVencimento < 0)) {
			stringBuilder.append(TIPO_NUMERO_DIAS_VENCIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(numeroDiasValidade == null && (tipoNumeroDiasValidade != null && tipoNumeroDiasValidade > 0)) {
			stringBuilder.append(NUMERO_DIAS_VALIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(numeroDiasVencimento == null && (tipoNumeroDiasVencimento != null && tipoNumeroDiasVencimento > 0)) {
			stringBuilder.append(NUMERO_DIAS_VENCIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}
}
