package br.com.ggas.cobranca.negativacao.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.negativacao.ClienteNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class ClienteNegativacaoImpl extends EntidadeNegocioImpl implements ClienteNegativacao {

	private static final long serialVersionUID = -861031886392573470L;
	
	private ComandoNegativacao comando;
	private Cliente cliente;
	private Date dataNotificacao;
	private Collection<FaturaClienteNegativacao> listaFaturasNegativacao = new HashSet<>();
	
	@Override
	public ComandoNegativacao getComando() {
		return this.comando;
	}

	@Override
	public void setComando(ComandoNegativacao comando) {
		this.comando = comando;
	}

	@Override
	public Cliente getCliente() {
		return this.cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public Date getDataNotificacao() {
		return this.dataNotificacao;
	}

	@Override
	public void setDataNotificacao(Date dataNotificacao) {
		this.dataNotificacao = dataNotificacao;
	}

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	@Override
	public Collection<FaturaClienteNegativacao> getListaFaturasNegativacao() {
		return listaFaturasNegativacao;
	}

	@Override
	public void setListaFaturasNegativacao(Collection<FaturaClienteNegativacao> listaFaturasNegativacao) {
		this.listaFaturasNegativacao = listaFaturasNegativacao;
	}
	
	public String getDescricaoReferenciaFaturas() {
		StringBuilder descricao = new StringBuilder();
		
		for (FaturaClienteNegativacao fatura : listaFaturasNegativacao) {
			if (fatura.getFatura() != null) {
				descricao.append(fatura.getFatura().getAnoMesReferencia())
						.append(" / ")
						.append(fatura.getFatura().getNumeroCiclo())
						.append("\n");
			}
		}
		return descricao.toString();
	}
	
}
