package br.com.ggas.cobranca.negativacao.batch;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.negativacao.ClienteNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoCliente;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoRota;
import br.com.ggas.cobranca.negativacao.ControladorNegativacao;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

@Component
public class ProcessarEmissaoNotificacaoNegativacaoBatch implements Batch {
	
	@Autowired
	private ControladorNegativacao controladorNegativacao;
	
	private static final String RELATORIO_NOTIFICACAO_NEGATIVACAO = "NotificacaoNegativacao";
	private static final String PROCESSO = "processo";

	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {
		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);
		ComandoNegativacao comandoNegativacao = controladorNegativacao
				.obterComandoNegativacao(Long.valueOf((String) parametros.get("idComando")));
		
		Collection<ComandoNegativacaoRota> listaComandoNegativacaoRota = controladorNegativacao
				.obterComandoNegativacaoRota(comandoNegativacao.getChavePrimaria());
		
		if(listaComandoNegativacaoRota != null && !listaComandoNegativacaoRota.isEmpty()) {
			Long[] chavesRota = new Long[listaComandoNegativacaoRota.size()];
			int i = 0;
			for(ComandoNegativacaoRota comandoNegativacaoRota : listaComandoNegativacaoRota) {
				chavesRota[i] = comandoNegativacaoRota.getRota().getChavePrimaria();
			}
			
			comandoNegativacao.setChavesRotas(chavesRota);
		}

		ControladorNegativacao controladorNegativacao = ServiceLocator.getInstancia().getControladorNegativacao();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
				.getControladorParametroSistema();
		ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
				.getControladorProcessoDocumento();
		ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);

		try {
			Map<Cliente, byte[]> mapaBytes = controladorNegativacao
					.processarEmissaoNotificacaoNegativacao(logProcessamento, comandoNegativacao);

			if (mapaBytes != null && !mapaBytes.isEmpty()) {
				ByteArrayInputStream byteArrayInputStream = null;
				File pdf = null;
				for (Entry<Cliente, byte[]> mapa : mapaBytes.entrySet()) {
					
					byte[] bytes = mapa.getValue();
					byteArrayInputStream = new ByteArrayInputStream(bytes);
					
					String diretorioRelatorioGerados = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);
					pdf = Util.getFile(diretorioRelatorioGerados);
					if (!pdf.exists()) {
						pdf.mkdirs();
					}

					final String nomeDocumento = RELATORIO_NOTIFICACAO_NEGATIVACAO
							+ String.valueOf(processo.getChavePrimaria()) + Calendar.getInstance().getTime().getTime();

					final String diretorioDocumento = diretorioRelatorioGerados + nomeDocumento
							+ Constantes.EXTENSAO_ARQUIVO_PDF;

					controladorProcesso.salvarRelatorio(processo, controladorProcessoDocumento, byteArrayInputStream,
							nomeDocumento, diretorioDocumento);

				}

			}
		} catch (IOException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		}

		return logProcessamento.toString();
	}

}
