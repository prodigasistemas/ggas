/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cobranca.negativacao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.negativacao.AgenteNegativador;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

public class AgenteNegativadorImpl extends EntidadeNegocioImpl implements AgenteNegativador {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4241608698799979602L;
	
	private static final int FINAL_CAMPO = 2;
	
	private Cliente cliente;
	
	private String numeroContrato;
	
	private Integer prazoNegativacao;
	
	private Date dataFimContrato;
	
	private Date dataInicioContrato;
	
	private BigDecimal valorTarifaInclusao;
	
	private BigDecimal valorContrato;
	
	private boolean indicadorPrioritario;
	

	@Override
	public Cliente getCliente() {
		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public String getNumeroContrato() {
		return numeroContrato;
	}

	@Override
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
		
	}

	@Override
	public BigDecimal getValorContrato() {
		return valorContrato;
	}

	@Override
	public void setValorContrato(BigDecimal valorContrato) {
		this.valorContrato = valorContrato;
		
	}

	@Override
	public BigDecimal getValorTarifaInclusao() {
		return valorTarifaInclusao;
	}

	@Override
	public void setValorTarifaInclusao(BigDecimal valorTarifaInclusao) {
		this.valorTarifaInclusao = valorTarifaInclusao;
		
	}

	@Override
	public Date getDataInicioContrato() {
		Date data = null;
		if (dataInicioContrato != null) {
			data = (Date) dataInicioContrato.clone();
		}
		return data;
	}

	@Override
	public void setDataInicioContrato(Date dataInicioContrato) {
		if(dataInicioContrato != null){
			this.dataInicioContrato = (Date) dataInicioContrato.clone();
		} else {
			this.dataInicioContrato = null; 
		}
		
	}

	@Override
	public Date getDataFimContrato() {
		Date data = null;
		if (dataFimContrato != null) {
			data = (Date) dataFimContrato.clone();
		}
		return data;
	}

	@Override
	public void setDataFimContrato(Date dataFimContrato) {
		if(dataFimContrato != null){
			this.dataFimContrato = (Date) dataFimContrato.clone();
		} else {
			this.dataFimContrato = null; 
		}
	}

	@Override
	public Integer getPrazoNegativacao() {
		return prazoNegativacao;
	}

	@Override
	public void setPrazoNegativacao(Integer prazoNegativacao) {
		this.prazoNegativacao = prazoNegativacao;
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {
		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(dataInicioContrato == null) {
			stringBuilder.append("Data Ínicio Contrato");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(dataFimContrato == null) {
			stringBuilder.append("Data Fim Contrato");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(cliente == null) {
			stringBuilder.append("Cliente");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(numeroContrato == null || numeroContrato.isEmpty()) {
			stringBuilder.append("Número Contrato");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - FINAL_CAMPO));
		}

		return erros;
	}

	@Override
	public boolean isIndicadorPrioritario() {
		return indicadorPrioritario;
	}

	@Override
	public void setIndicadorPrioritario(boolean indicadorPrioritario) {
		this.indicadorPrioritario = indicadorPrioritario;
	}

}
