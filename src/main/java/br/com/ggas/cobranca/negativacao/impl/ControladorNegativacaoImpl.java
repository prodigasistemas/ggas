/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 15:04:20
 @author wcosta
 */

package br.com.ggas.cobranca.negativacao.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.atendimento.chamado.dominio.ClienteVO;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.DocumentoNaoEntregue;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.cobranca.negativacao.AgenteNegativador;
import br.com.ggas.cobranca.negativacao.ClienteNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoCliente;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoRota;
import br.com.ggas.cobranca.negativacao.ControladorNegativacao;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.cobranca.negativacao.dominio.ClienteNegativacaoVO;
import br.com.ggas.cobranca.negativacao.dominio.ComandoNegativacaoVO;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.cobranca.acaocobranca.SubRelatorioFaturasVencidasCobrancaVO;
import br.com.ggas.web.cobranca.negativacao.NotificacaoNegativacaoVO;

/**
 * Classe responsável pela implementação do controlador da negativação.
 *
 */
public class ControladorNegativacaoImpl extends ControladorNegocioImpl implements ControladorNegativacao {

	private static final String ENDERECO_EMPRESA = "enderecoEmpresa";

	private static final String IMAGEM_LOGOMARCA_EMPRESA = "imagem";
	
	private static final String PROTOCOLOS = "protocolos";
	
	private static final Integer SITUACAO_NEGATIVACAO_INCLUIDO = 1;
	
	private static final Integer SITUACAO_NEGATIVACAO_CONFIRMADO = 2;
	
	private static final Integer SITUACAO_NEGATIVACAO_EXCLUIDO = 3;
	
	private static final Integer SITUACAO_NEGATIVACAO_A_INCLUIR = 5;
	
	private static final Integer SITUACAO_NEGATIVACAO_CANCELADO = 4;
	
	@Autowired
	@Qualifier(value = "controladorProcesso")
	private ControladorProcesso controladorProcesso;
	
	@Autowired
	@Qualifier(value = "controladorCobranca")
	private ControladorCobranca controladorCobranca;

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(value = "controladorAcesso")
	private ControladorAcesso controladorAcesso;
	
	@Autowired
	@Qualifier("controladorTipoDocumento")
	private ControladorTipoDocumento controladorTipoDocumento;
	
	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	@Qualifier("controladorAvisoCorte")
	private ControladorAvisoCorte controladorAvisoCorte;
	
	@Autowired
	private ControladorEntregaDocumento controladorEntregaDocumento;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	private Fachada fachada = Fachada.getInstancia();
	
	@Autowired
	@Qualifier(value = "controladorFatura")
	private ControladorFatura controladorFatura;
	
	@Autowired
	@Qualifier(value = "controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {
		return null;
	}

	@Override
	public EntidadeNegocio criarComandoNegativacaoCliente() {
		return (EntidadeNegocio) ServiceLocator.getInstancia()
				.getBeanPorID(ComandoNegativacaoCliente.BEAN_ID_COMANDO_NEGATIVACAO_CLIENTE);
	}

	@Override
	public EntidadeNegocio criarFaturaClienteNegativacao() {
		return (EntidadeNegocio) ServiceLocator.getInstancia()
				.getBeanPorID(FaturaClienteNegativacao.BEAN_ID_FATURA_CLIENTE_NEGATIVACAO);
	}

	@Override
	public EntidadeNegocio criarClienteNegativacao() {
		return (EntidadeNegocio) ServiceLocator.getInstancia()
				.getBeanPorID(ClienteNegativacao.BEAN_ID_CLIENTE_NEGATIVACAO);
	}
	
	@Override
	public EntidadeNegocio criarComandoNegativacaoRota() {
		return (EntidadeNegocio) ServiceLocator.getInstancia()
				.getBeanPorID(ComandoNegativacaoRota.BEAN_ID_COMANDO_NEGATIVACAO_ROTA);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.geral.negocio.impl. ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(AgenteNegativador.BEAN_ID_AGENTE_NEGATIVADOR);
	}

	public Class<?> getClasseEntidadeAgenteNegativador() {

		return ServiceLocator.getInstancia().getClassPorID(AgenteNegativador.BEAN_ID_AGENTE_NEGATIVADOR);
	}

	public Class<?> getClasseEntidadeComandoNegativacao() {

		return ServiceLocator.getInstancia().getClassPorID(ComandoNegativacao.BEAN_ID_COMANDO_NEGATIVACAO);
	}

	public Class<?> getClasseEntidadeClienteNegativacao() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteNegativacao.BEAN_ID_CLIENTE_NEGATIVACAO);
	}
	
	public Class<?> getClasseEntidadeComandoNegativacaoRota() {

		return ServiceLocator.getInstancia().getClassPorID(ComandoNegativacaoRota.BEAN_ID_COMANDO_NEGATIVACAO_ROTA);
	}

	/**
	 * Gets the classe entidade fatura.
	 *
	 * @return the classe entidade fatura
	 */
	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeComandoNegativacaoCliente() {

		return ServiceLocator.getInstancia()
				.getClassPorID(ComandoNegativacaoCliente.BEAN_ID_COMANDO_NEGATIVACAO_CLIENTE);
	}
	
	public Class<?> getClasseEntidadeFaturaClienteNegativacao() {

		return ServiceLocator.getInstancia()
				.getClassPorID(FaturaClienteNegativacao.BEAN_ID_FATURA_CLIENTE_NEGATIVACAO);
	}
	
	public Class<?> getClasseEntidadeDocumentoEntregue() {

		return ServiceLocator.getInstancia().getClassPorID(EntregaDocumento.BEAN_ID_ENTREGA_DOCUMENTO);
	}
	
	@Override
	public Collection<AgenteNegativador> listarAgenteNegativador() {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeAgenteNegativador().getSimpleName());
		hql.append(" agenteNegativador ");
		hql.append( " order by indicadorPrioritario desc " );

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	@Override
	public AgenteNegativador obterAgenteNegativador(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeAgenteNegativador().getSimpleName());
		hql.append(" agenteNegativador ");
		hql.append(" WHERE ");
		hql.append(" agenteNegativador.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", chavePrimaria);

		return (AgenteNegativador) query.uniqueResult();
	}

	@Override
	public Collection<Fatura> obterFaturasComando(ComandoNegativacao comando) throws NegocioException {
		StringBuilder hql = new StringBuilder();
		String parametro = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MESES_PRESCRICAO);
		
		hql.append(" FROM ");
		hql.append(getClasseEntidadeFatura().getSimpleName()).append(" fatura ");
		hql.append(" INNER JOIN FETCH fatura.pontoConsumo pontoConsumo ");
		hql.append(" INNER JOIN FETCH fatura.situacaoPagamento situacaoPagamento ");
		hql.append(" INNER JOIN FETCH pontoConsumo.segmento segmento ");
		hql.append(" INNER JOIN FETCH pontoConsumo.rota rota ");
		hql.append(" INNER JOIN FETCH rota.grupoFaturamento grupoFaturamento ");
		hql.append(" INNER JOIN FETCH fatura.contratoAtual contrato ");
		hql.append(" INNER JOIN FETCH fatura.cliente cliente ");
		hql.append(" INNER JOIN FETCH fatura.situacaoPagamento situacaoPagamento ");
		hql.append(" WHERE ");
		hql.append(" fatura.habilitado = true ");
		hql.append(" and fatura.dataCancelamento is null ");
		hql.append(" and (fatura.situacaoPagamento.chavePrimaria <> :cdSituacao ");
		hql.append(" and fatura.situacaoPagamento.chavePrimaria <> :cdSituacaoPrescrito ");
		hql.append(" and fatura.dataVencimento >= :dataPrescricao) ");
		hql.append(" and fatura.chavePrimaria not in ( ");
		hql.append(" select faturaClienteNegativacao.fatura.chavePrimaria ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeFaturaClienteNegativacao().getSimpleName()).append(" faturaClienteNegativacao) ");
		

		if(comando.getIndicadorCliente() == 1){
		    hql.append(" and (cliente.clientePublico is null or cliente.clientePublico = false) ");
		} else if (comando.getIndicadorCliente() == 2){
		    hql.append(" and cliente.clientePublico = true ");
		}
		
		if (comando.getReferenciaInicialDebitos() != null) {
			hql.append(" and fatura.anoMesReferencia BETWEEN :referenciaInicial AND :referenciaFinal ");
		}

		if (comando.getDataVencimentoInicial() != null) {
			hql.append(" and fatura.dataVencimento BETWEEN :dataVencimentoInicial AND :dataVencimentoFinal ");
		} else if(comando.getDiasAtrasoInicial() != null && comando.getDiasAtrasoFim() != null) {
			hql.append(" and fatura.dataVencimento between :diasAtrasoFim and :diasAtrasoInicial ");
		} else {
			hql.append(" and fatura.dataVencimento < :dataAtual");
		}

		if (comando.getValorInicialDebitos() != null) {
			hql.append(" and fatura.valorTotal BETWEEN :valorInicialDebitos AND :valorFinalDebitos ");
		}

		if (comando.getSituacaoConsumo() != null) {
			hql.append(" and pontoConsumo.situacaoConsumo.chavePrimaria = :situacaoConsumo ");
		}

		if (comando.getSegmento() != null) {
			hql.append(" and segmento.chavePrimaria = :segmento ");
		}

		if (comando.getGrupoFaturamento() != null) {
			hql.append(" and grupoFaturamento.chavePrimaria = :grupoFaturamento ");
		}

		if (comando.getSituacaoContrato() != null) {
			hql.append(" and contrato.situacao.chavePrimaria = :situacaoContrato ");
		}
		
		if (comando.getChavesRotas() != null && comando.getChavesRotas().length > 0) {
			hql.append(" and rota.chavePrimaria in (:arrayRota) ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (comando.getReferenciaInicialDebitos() != null) {
			query.setInteger("referenciaInicial", comando.getReferenciaInicialDebitos());
			query.setInteger("referenciaFinal", comando.getReferenciaFinalDebitos());
		}
		
		if (comando.getDataVencimentoInicial() != null) {
			query.setDate("dataVencimentoInicial", comando.getDataVencimentoInicial());
			query.setDate("dataVencimentoFinal", comando.getDataVencimentoFinal());
		} else if(comando.getDiasAtrasoInicial() != null && comando.getDiasAtrasoFim() != null) {
			query.setDate("diasAtrasoInicial",
					DataUtil.decrementarDia(Calendar.getInstance().getTime(), comando.getDiasAtrasoInicial()));
			query.setDate("diasAtrasoFim",
					DataUtil.decrementarDia(Calendar.getInstance().getTime(), comando.getDiasAtrasoFim()));	
		}else{
			query.setDate("dataAtual", Calendar.getInstance().getTime());
		}

		if (comando.getValorInicialDebitos() != null) {
			query.setBigDecimal("valorInicialDebitos", comando.getValorInicialDebitos());
			query.setBigDecimal("valorFinalDebitos", comando.getValorFinalDebitos());
		}

		if (comando.getSituacaoConsumo() != null) {
			query.setLong("situacaoConsumo", comando.getSituacaoConsumo().getChavePrimaria());
		}

		if (comando.getSegmento() != null) {
			query.setLong("segmento", comando.getSegmento().getChavePrimaria());
		}

		if (comando.getGrupoFaturamento() != null) {
			query.setLong("grupoFaturamento", comando.getGrupoFaturamento().getChavePrimaria());
		}

		if (comando.getSituacaoContrato() != null) {
			query.setLong("situacaoContrato", comando.getSituacaoContrato().getChavePrimaria());
		}
		
		if(comando.getQuantidadeMaximaInclusao () != null) {
			query.setMaxResults(comando.getQuantidadeMaximaInclusao());
		}
		
		if (comando.getChavesRotas() != null && comando.getChavesRotas().length > 0) {
			query.setParameterList("arrayRota", comando.getChavesRotas());
		}

		query.setLong("cdSituacao", obterSituacaoPagamentoQuitado());
		query.setLong("cdSituacaoPrescrito", obterSituacaoPagamentoPrescrito());
		
		
		DateTime dataPrescricao = new DateTime().minusMonths(Integer.valueOf(parametro));
		query.setDate("dataPrescricao", dataPrescricao.toDate());
		
		return query.list();
	}

	private Long obterSituacaoPagamentoQuitado() {
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPagamentoQuitado = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia()
				.getControladorEntidadeConteudo();

		EntidadeConteudo situacaoPagamentoQuitado = controladorEntidadeConteudo
				.obter(Long.valueOf(codigoSituacaoPagamentoQuitado));

		return situacaoPagamentoQuitado.getChavePrimaria();
	}
	
	private Long obterSituacaoPagamentoPrescrito() {
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPagamentoQuitado = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PRESCRITO);

		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia()
				.getControladorEntidadeConteudo();

		EntidadeConteudo situacaoPagamentoPrescrito = controladorEntidadeConteudo
				.obter(Long.valueOf(codigoSituacaoPagamentoQuitado));

		return situacaoPagamentoPrescrito.getChavePrimaria();
	}

	@Override
	public ComandoNegativacao obterComandoNegativacao(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacao().getSimpleName());
		hql.append(" comandoNegativacao ");
		hql.append(" WHERE ");
		hql.append(" comandoNegativacao.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", chavePrimaria);

		return (ComandoNegativacao) query.uniqueResult();
	}

	@Override
	public Processo inserirProcesso(Usuario usuario, DadosAuditoria dadosAuditoria, ComandoNegativacao comando) throws GGASException {
		Processo processo = (Processo) controladorProcesso.criar();
		processo.setOperacao(getOperacao());
		processo.setAgendado(Boolean.FALSE);
		processo.setVersao(0);
		processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
		processo.setDataFinalAgendamento(null);
		processo.setHabilitado(Boolean.TRUE);
		processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
		processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
		processo.setUsuario(usuario);
		processo.setDiaNaoUtil(Boolean.TRUE);
		processo.setDadosAuditoria(dadosAuditoria);
		processo.setDescricao("Processar Emissão Notificação Negativação");
		
		Map<String, String> parametros = new HashMap<String, String>();
		parametros.put("idComando", String.valueOf(comando.getChavePrimaria()));
		processo.setParametros(parametros);

		processo.setChavePrimaria(controladorProcesso.inserir(processo));

		return processo;
	}

	private Operacao getOperacao() {
		ConstanteSistema constante = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_OPERACAO_EMITIR_NOTIFICACAO_NEGATIVACAO);
		return controladorAcesso.obterOperacaoSistema(Long.parseLong(constante.getValor()));
	}

	@Override
	public Collection<ComandoNegativacao> listarComandos(ComandoNegativacaoVO comandoVO) throws ParseException {
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacao().getSimpleName()).append(" comando ");
		hql.append(" WHERE ");
		hql.append(" comando.habilitado = true ");
		
		if (comandoVO.getChaveComando() != null && !comandoVO.getChaveComando().isEmpty()) {
			hql.append(" and comando.chavePrimaria = :chaveComando ");
		}
		
		if (comandoVO.getExecutado() != null) {
			if (comandoVO != null) {
				if (comandoVO.getExecutado()) {
					hql.append(" and comando.chavePrimaria in ");
					hql.append(" (select distinct cliente.comando.chavePrimaria ");
					hql.append(" FROM ");
					hql.append(getClasseEntidadeClienteNegativacao().getSimpleName()).append(" cliente ");
					hql.append(" WHERE ");
					hql.append(" cliente.dataNotificacao is not null) ");
				} else {
					hql.append(" and comando.chavePrimaria not in ");
					hql.append(" (select distinct cliente.comando.chavePrimaria ");
					hql.append(" FROM ");
					hql.append(getClasseEntidadeClienteNegativacao().getSimpleName()).append(" cliente ");
					hql.append(" WHERE ");
					hql.append(" cliente.dataNotificacao is not null) ");
				}
			}
		}
		
		if (comandoVO.getNomeComando() != null && !"".equals(comandoVO.getNomeComando())) {
			 hql.append(" and upper(comando.nomeComando) like :nomeComando ");
		}

		if ((comandoVO.getDataInicioExecucao() != null && !"".equals(comandoVO.getDataInicioExecucao()))
				&& (comandoVO.getDataFimExecucao() != null && !"".equals(comandoVO.getDataFimExecucao()))) {
			hql.append(" and comando.chavePrimaria in ");
			hql.append(" (select distinct cliente.comando.chavePrimaria ");
			hql.append(" FROM ");
			hql.append(getClasseEntidadeClienteNegativacao().getSimpleName()).append(" cliente ");	
			hql.append(" WHERE ");
			hql.append(" cliente.dataNotificacao BETWEEN :datainicioExecucao AND :dataFimExecucao) ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		if (comandoVO.getChaveComando() != null && !comandoVO.getChaveComando().isEmpty()) {
			query.setLong("chaveComando", Long.valueOf(comandoVO.getChaveComando()));
		}
		
		if (comandoVO.getNomeComando() != null && !"".equals(comandoVO.getNomeComando())) {
			query.setString("nomeComando", "%" + comandoVO.getNomeComando() + "%");
		}
		
		if ((comandoVO.getDataInicioExecucao() != null && !"".equals(comandoVO.getDataInicioExecucao()))
				&& (comandoVO.getDataFimExecucao() != null && !"".equals(comandoVO.getDataFimExecucao()))) {
			query.setDate("datainicioExecucao", new SimpleDateFormat("dd/MM/yyyy").parse(comandoVO.getDataInicioExecucao()));
			query.setDate("dataFimExecucao", DataUtil
					.incrementarDia(new SimpleDateFormat("dd/MM/yyyy").parse(comandoVO.getDataFimExecucao()), 1));
		}

		return query.list();
	}

	@Override
	public Collection<ComandoNegativacaoCliente> obterComandoNegativacaoCliente(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacaoCliente().getSimpleName());
		hql.append(" comandoNegativacaoCliente ");
		hql.append(" WHERE ");
		hql.append(" comandoNegativacaoCliente.comando.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", chavePrimaria);

		return query.list();
	}

	@Override
	public ClienteNegativacao obterClienteNegativacao(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeClienteNegativacao().getSimpleName());
		hql.append(" clienteNegativacao ");
		hql.append(" WHERE ");
		hql.append(" clienteNegativacao.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", chavePrimaria);

		return (ClienteNegativacao) query.uniqueResult();
	}

	@Override
	public Map<Cliente, byte[]> processarEmissaoNotificacaoNegativacao(StringBuilder logProcessamento,
			ComandoNegativacao comandoNegativacao) throws GGASException {

		logProcessamento.append("\r\n Iniciando Batch de Emissão Notificação de Negativação \r\n\r\n");
		
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		Map<Cliente, byte[]> mapaRelatorioAvicoCorteCliente = new HashMap<Cliente, byte[]>();

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		
		ControladorDocumentoImpressaoLayout controladorDocumentoImpressaoLayout = ServiceLocator.getInstancia().getControladorDocumentoImpressaoLayout();

		if (comandoNegativacao != null) {
			logProcessamento.append("\r\n   -- COMANDO: " + comandoNegativacao.getNomeComando());
			
			Map<Cliente, Collection<Fatura>> mapaClienteFatura = obterListaFaturasGerarNotificacao(comandoNegativacao, logProcessamento);

			
			if (mapaClienteFatura != null && !mapaClienteFatura.isEmpty()) {
				logProcessamento.append("\r\n    ---- INÍCIO DA LISTA DE CLIENTES ---- ");
				for (Map.Entry<Cliente, Collection<Fatura>> entry : mapaClienteFatura.entrySet()) {
					ClienteNegativacao clienteNegativacao = (ClienteNegativacao) this.criarClienteNegativacao();
					clienteNegativacao.setCliente(entry.getKey());
					clienteNegativacao.setComando(comandoNegativacao);
					clienteNegativacao.setDataNotificacao(new Date());

					logProcessamento.append("\r\n    -- CLIENTE: " + clienteNegativacao.getCliente().getNome() + " ("
							+ clienteNegativacao.getCliente().getChavePrimaria() + ")");

					long chaveClienteNegativacao = this.inserir(clienteNegativacao);

					for (Fatura fatura : entry.getValue()) {
						FaturaClienteNegativacao faturaClienteNegativacao = (FaturaClienteNegativacao) this
								.criarFaturaClienteNegativacao();

						faturaClienteNegativacao.setFatura(fatura);
						faturaClienteNegativacao
								.setClienteNegativacao(this.obterClienteNegativacao(chaveClienteNegativacao));

						logProcessamento.append(
								"\n -- FATURA: REFERENCIA " + faturaClienteNegativacao.getFatura().getAnoMesReferencia()
										+ " / CICLO " + faturaClienteNegativacao.getFatura().getNumeroCiclo());
						long chaveFaturaClienteNegativacao = this.inserir(faturaClienteNegativacao);
						
						EntregaDocumento entregaDocumento = fachada.criarEntregaDocumento();
						entregaDocumento.setCliente(entry.getKey());
						entregaDocumento.setImovel(fatura.getPontoConsumo().getImovel());
						entregaDocumento.setDataEmissao(Calendar.getInstance().getTime());
						entregaDocumento.setDataSituacao(Calendar.getInstance().getTime());
						entregaDocumento.setIndicadorRetornado(false);
						entregaDocumento.setSituacaoEntrega(
								controladorEntidadeConteudo.listarEntidadeConteudoPorEntidadeClasseEDescricao(
										"Situação da Entrega Documentos", "Pendente"));
						entregaDocumento.setTipoDocumento(controladorTipoDocumento.obterTipoDocumentoDescricao("PROTOCOLO"));
						entregaDocumento.setFaturaClienteNegativacao(
								this.obterFaturaClienteNegativacao(chaveFaturaClienteNegativacao));
						fachada.inserirEntregaDocumento(entregaDocumento);
					}
				}
				logProcessamento.append("\r\n    ---- FIM DA LISTA DE CLIENTES ---- \r\n\r\n");
			} else {
				logProcessamento.append("\r\n    ---- PARA ESSE COMANDO NÃO FOI ENCONTRADO NENHUM CLIENTE/FATURA ---- \r\n\r\n");
			}

			Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
			if (empresa != null) {

				if (empresa.getCliente().getEnderecoPrincipal() != null) {
					parametros.put(ENDERECO_EMPRESA,
							empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoRuaNumeroComplemento()
									+ empresa.getCliente().getEnderecoPrincipal()
											.getEnderecoFormatadoBairroMunicipioUFCEP());
				} else {
					parametros.put(ENDERECO_EMPRESA, StringUtils.EMPTY);
				}
				if (empresa.getCliente().getContatos() != null && !empresa.getCliente().getContatos().isEmpty()) {
					parametros.put("contatoEmpresa", empresa.getCliente().getContatos().iterator().next().getFone()
							+ empresa.getCliente().getContatos().iterator().next().getEmail());
				} else {
					parametros.put("contatoEmpresa", StringUtils.EMPTY);
				}

				if (empresa.getLogoEmpresa() != null) {
					parametros.put(IMAGEM_LOGOMARCA_EMPRESA,
							Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
				}

			}
			
			String local = StringUtils.EMPTY;
			if (empresa != null && empresa.getCliente() != null && empresa.getCliente().getEnderecoPrincipal() != null) {
				local = empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF();
			}

			parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());
			ControladorParametroSistema controSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
			ParametroSistema gerenteFinanceiro = controSistema.obterParametroPorCodigo(Constantes.PARAMETRO_GERENTE_FINANCEIRO_FATURAMENTO);
			if (gerenteFinanceiro != null) {
				parametros.put("gerenteFinanceiro", gerenteFinanceiro.getValor());
			}
			
			String arquivoRelatorio = controladorDocumentoImpressaoLayout.obterDocumentoImpressaoLayoutPorConstante(Constantes.NOTIFICACAO_NEGATIVACAO);

			if (arquivoRelatorio == null || arquivoRelatorio.isEmpty()) {

				throw new GGASException(Constantes.ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO);

			}
			
			byte[] relatorioNotificacaoNegativacao = null;
			Map<Cliente, Collection<NotificacaoNegativacaoVO>> mapaNotificacaoNegativacao = popularObjetoNotificacaoNegativacaoVO(
					mapaClienteFatura);
			
			parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
			parametros.put(PROTOCOLOS, criarParametroProtocolos(mapaNotificacaoNegativacao));
			for (Entry<Cliente, Collection<NotificacaoNegativacaoVO>> mapa : mapaNotificacaoNegativacao.entrySet()) {
				if (!mapa.getValue().isEmpty()) {
					relatorioNotificacaoNegativacao = RelatorioUtil.gerarRelatorioPDF(mapa.getValue(), parametros, arquivoRelatorio);
					mapaRelatorioAvicoCorteCliente.put(mapa.getKey(), relatorioNotificacaoNegativacao);
				}
			}

		} else {
			logProcessamento.append("\r\n  NÃO EXISTE COMANDO A SER EXECUTADO ");
			logProcessamento.append("\r\n  É NECESSÁRIO INSERIR E EXECUTAR A PARTIR DA TELA DE COMANDO DE NEGATIVAÇÃO ");
		}

		return mapaRelatorioAvicoCorteCliente;
	}
	
	
	private Map<Cliente, Collection<NotificacaoNegativacaoVO>> popularObjetoNotificacaoNegativacaoVO(
			Map<Cliente, Collection<Fatura>> mapaClienteFatura) throws GGASException {
		
		Map<Cliente, Collection<NotificacaoNegativacaoVO>> mapaNotificacaoNegativacao = new HashMap<Cliente, Collection<NotificacaoNegativacaoVO>>();
		List<NotificacaoNegativacaoVO> listaNotificacaoNegativacaoVO = new ArrayList<NotificacaoNegativacaoVO>();
		for (Map.Entry<Cliente, Collection<Fatura>> entry : mapaClienteFatura.entrySet()) {
			List<SubRelatorioFaturasVencidasCobrancaVO> colecaoFaturasVencidas = new ArrayList<SubRelatorioFaturasVencidasCobrancaVO>();
			PontoConsumo pontoConsumo = null;
			Cliente cliente = entry.getKey();
			NotificacaoNegativacaoVO notificacaoNegativacaoVO = new NotificacaoNegativacaoVO();

			
			for(Fatura fatura : entry.getValue()) {
				colecaoFaturasVencidas.add(controladorCobranca.montarVOFaturaVencida(fatura, null));
				if (pontoConsumo == null) {
					pontoConsumo = fatura.getPontoConsumo();
				}
				
				if (fatura.getContrato() != null) {
					notificacaoNegativacaoVO.setCodigoContrato(fatura.getContrato().getNumeroCompletoContrato());
				} else {
					notificacaoNegativacaoVO.setCodigoContrato(StringUtils.EMPTY);
				}
				
				if (fatura.getPontoConsumo().getSegmento() != null) {
					notificacaoNegativacaoVO.setTipoResidencia(fatura.getPontoConsumo().getSegmento().getDescricao());
				} else {
					notificacaoNegativacaoVO.setTipoResidencia(StringUtils.EMPTY);
				}
				
				if (fatura.getRota() != null) {
					notificacaoNegativacaoVO.setNumeroRota(fatura.getRota().getChavePrimaria());
				}
			}
			
			notificacaoNegativacaoVO.setColecaoFaturaVencida(colecaoFaturasVencidas);
			
			if (pontoConsumo.getCodigoLegado() != null) {
				notificacaoNegativacaoVO.setCodigoUnico(String.valueOf(pontoConsumo.getCodigoLegado()));
			} else {
				notificacaoNegativacaoVO.setCodigoUnico(String.valueOf(pontoConsumo.getChavePrimaria()));
			}
			
			notificacaoNegativacaoVO.setCodigo(String.valueOf(cliente.getChavePrimaria()));
			notificacaoNegativacaoVO.setNome(cliente.getNome());
			notificacaoNegativacaoVO.setData(Util.dataAtualPorExtenso());
			notificacaoNegativacaoVO.setNome(cliente.getNome());
			
			if (pontoConsumo.getEnderecoFormatado() != null) {
				notificacaoNegativacaoVO.setEndereco(pontoConsumo.getEnderecoFormatadoRuaNumeroComplemento());
				notificacaoNegativacaoVO.setCep(pontoConsumo.getCep().getCep());
				notificacaoNegativacaoVO.setBairro(pontoConsumo.getEnderecoFormatadoBairro());
				notificacaoNegativacaoVO.setCidade(pontoConsumo.getEnderecoFormatadoMunicipioUFCEP());
				notificacaoNegativacaoVO.setMunicipioUf(pontoConsumo.getEnderecoFormatadoMunicipioUF());
				notificacaoNegativacaoVO.setPontoEntrega(pontoConsumo.getDescricao());
				notificacaoNegativacaoVO.setCepCidadeEstado(pontoConsumo.getEnderecoFormatadoCepMunicipioUF());
				notificacaoNegativacaoVO.setEnderecoPontoConsumoRelatorioCorteSergas(pontoConsumo.getEnderecoFormatadoRelatorioCorteSergas());
			} else {
				notificacaoNegativacaoVO.setEndereco(StringUtils.EMPTY);
				notificacaoNegativacaoVO.setCep(StringUtils.EMPTY);
				notificacaoNegativacaoVO.setBairro(StringUtils.EMPTY);
				notificacaoNegativacaoVO.setCidade(StringUtils.EMPTY);
				notificacaoNegativacaoVO.setMunicipioUf(StringUtils.EMPTY);
				notificacaoNegativacaoVO.setPontoEntrega(StringUtils.EMPTY);
				notificacaoNegativacaoVO.setEnderecoPontoConsumoRelatorioCorteSergas(StringUtils.EMPTY);
			}
			
			listaNotificacaoNegativacaoVO.add(notificacaoNegativacaoVO);
			
		}
		
		mapaNotificacaoNegativacao.put(new ClienteImpl(), Util.ordenarColecaoPorAtributo(listaNotificacaoNegativacaoVO, "numeroRota", Boolean.TRUE));

		return mapaNotificacaoNegativacao;
	}
	
	private Collection<SubRelatorioFaturasVencidasCobrancaVO> criarParametroProtocolos(
			Map<Cliente, Collection<NotificacaoNegativacaoVO>> clientesRelatorio) {
		Collection<SubRelatorioFaturasVencidasCobrancaVO> protocolos = new ArrayList<SubRelatorioFaturasVencidasCobrancaVO>();
		for (Entry<Cliente, Collection<NotificacaoNegativacaoVO>> mapa : clientesRelatorio.entrySet()) {
			Collection<NotificacaoNegativacaoVO> camposDocNegativacao = mapa.getValue();
			for (NotificacaoNegativacaoVO notificacaoNegativacaoVO : camposDocNegativacao) {
				protocolos.addAll(notificacaoNegativacaoVO.getColecaoFaturaVencida());
			}
		}
		return protocolos;
	}
	
	private Map<Cliente, Collection<Fatura>> obterListaFaturasGerarNotificacao(ComandoNegativacao comando, StringBuilder logProcessamento) throws NegocioException {
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		
		Collection<Fatura> listaFaturaNegativacao = new HashSet<>();
		Map<Cliente, Collection<Fatura>> mapaClienteFatura = new HashMap<Cliente, Collection<Fatura>>();
		Collection<ComandoNegativacaoCliente> listaComandoNegativacaoCliente = new HashSet<>();
		Integer quantidadeFaturas = 0;
		
		if (comando.getIndicadorComandoCliente()) {
			listaComandoNegativacaoCliente = this
					.obterComandoNegativacaoCliente(comando.getChavePrimaria());

			for (ComandoNegativacaoCliente comandoCliente : listaComandoNegativacaoCliente) {
				Collection<Fatura> colecaoFaturaVencidaCliente = controladorFatura
						.consultarFaturasVencidasCliente(comandoCliente.getCliente().getChavePrimaria());
				
				if (colecaoFaturaVencidaCliente != null && !colecaoFaturaVencidaCliente.isEmpty()) {
					quantidadeFaturas = quantidadeFaturas + colecaoFaturaVencidaCliente.size();
					mapaClienteFatura.put(comandoCliente.getCliente(), colecaoFaturaVencidaCliente);
				}
			}
		} else {
			listaFaturaNegativacao = this.obterFaturasComando(comando);
			
			if (listaFaturaNegativacao != null && !listaFaturaNegativacao.isEmpty()) {
				quantidadeFaturas = quantidadeFaturas + listaFaturaNegativacao.size();
			}
			
			for (Fatura fatura : listaFaturaNegativacao) {
				
				if (mapaClienteFatura.containsKey(fatura.getCliente())) {
					mapaClienteFatura.get(fatura.getCliente()).add(fatura);
				} else {
					Collection<Fatura> novaListaFatura = new ArrayList<Fatura>();
					novaListaFatura.add(fatura);
					mapaClienteFatura.put(fatura.getCliente(), novaListaFatura);
				}
			}
		}
		
		logProcessamento.append("\r\n  - QTD DE CLIENTES: " + mapaClienteFatura.size());
		logProcessamento.append("\n  - QTD DE FATURAS: " + quantidadeFaturas + "");
		
		return mapaClienteFatura;
	}	
	
	@Override
	public Collection<FaturaClienteNegativacao> listarFaturaClientesNegativacao(ClienteNegativacaoVO vo) throws NegocioException {
		Boolean isObrigatorioProtocolo = controladorAvisoCorte.verificarObrigatoriedadeProtocolo();
		
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT faturaNegativacao ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeFaturaClienteNegativacao().getSimpleName()).append(" faturaNegativacao ");
		hql.append(" INNER JOIN FETCH faturaNegativacao.clienteNegativacao clienteNegativacao ");
		hql.append(" INNER JOIN FETCH faturaNegativacao.fatura fatura ");
		hql.append(" LEFT JOIN FETCH fatura.situacaoPagamento situacaoPagamento ");
		
		hql.append(" WHERE ");
		hql.append(" faturaNegativacao.habilitado = true ");
		
		if (isObrigatorioProtocolo) {
			hql.append(montarConsultaRetornoProtocolo());
		}
		
		if (vo != null) {
			if (vo.getSituacaoFaturas() != null && vo.getSituacaoFaturas() == 1) {
				hql.append(" and fatura.situacaoPagamento.chavePrimaria = :cdSituacao ");
			} else if (vo.getSituacaoFaturas() == 2)  {
				hql.append(" and fatura.situacaoPagamento.chavePrimaria <> :cdSituacao ");
			} else if (vo.getSituacaoFaturas() == 3)  {
				hql.append(" and fatura.situacaoPagamento.chavePrimaria = :cdSituacaoPrescrita ");
			} 
			

			if (vo.getSituacaoNegativacao().equals(SITUACAO_NEGATIVACAO_INCLUIDO)) {
				hql.append(" and faturaNegativacao.dataInclusaoNegativacao is not null ");
				hql.append(" and faturaNegativacao.dataConfirmacaoNegativacao is null ");
				hql.append(" and faturaNegativacao.dataExclusaoNegativacao is null ");
				hql.append(" and faturaNegativacao.dataCancelamentoNegativacao is null ");
			} else if (vo.getSituacaoNegativacao().equals(SITUACAO_NEGATIVACAO_EXCLUIDO)) {
				hql.append(" and faturaNegativacao.dataExclusaoNegativacao is not null ");
				hql.append(" and faturaNegativacao.dataCancelamentoNegativacao is null ");
			} else if (vo.getSituacaoNegativacao().equals(SITUACAO_NEGATIVACAO_CONFIRMADO)) {
				hql.append(" and faturaNegativacao.dataConfirmacaoNegativacao is not null ");
				hql.append(" and faturaNegativacao.dataExclusaoNegativacao is null ");
				hql.append(" and faturaNegativacao.dataCancelamentoNegativacao is null ");
			} else if (vo.getSituacaoNegativacao().equals(SITUACAO_NEGATIVACAO_A_INCLUIR)) {
				hql.append(" and faturaNegativacao.dataInclusaoNegativacao is null ");
				hql.append(" and faturaNegativacao.dataCancelamentoNegativacao is null ");
			} else if (vo.getSituacaoNegativacao().equals(SITUACAO_NEGATIVACAO_CANCELADO)) {
				hql.append(" and faturaNegativacao.dataCancelamentoNegativacao is not null ");
			}
			
			if (vo.getNomeComando() != null && !vo.getNomeComando().isEmpty()) {
				hql.append(" and upper(clienteNegativacao.comando.nomeComando) like :nomeComando ");
			}
		}
		
		hql.append(" order by clienteNegativacao.cliente.nome, faturaNegativacao.dataInclusaoNegativacao, ");
		hql.append(" faturaNegativacao.dataConfirmacaoNegativacao, faturaNegativacao.dataExclusaoNegativacao ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		if (vo.getSituacaoFaturas() != null && (vo.getSituacaoFaturas() == 1 || vo.getSituacaoFaturas() == 2)) { 
				query.setLong("cdSituacao", obterSituacaoPagamentoQuitado());
		}
		
		if (vo.getSituacaoFaturas() != null && vo.getSituacaoFaturas() == 3) { 
			query.setLong("cdSituacaoPrescrita", obterSituacaoPagamentoPrescrito());
	}
		
		if (vo.getNomeComando() != null && !vo.getNomeComando().isEmpty()) {
			query.setString("nomeComando", "%"  + vo.getNomeComando() + "%");
		}
		
		return query.list();
	
	}

	
	
	private StringBuilder montarConsultaRetornoProtocolo() {
		StringBuilder hql = new StringBuilder();
		
		hql.append(" and faturaNegativacao.chavePrimaria in ");
		hql.append(" ( select entregaDocumento.faturaClienteNegativacao.chavePrimaria from ");
		hql.append(getClasseEntidadeDocumentoEntregue().getSimpleName());
		hql.append(" entregaDocumento ");
		hql.append(" where ");
		hql.append(" entregaDocumento.faturaClienteNegativacao.chavePrimaria is not null ");
		hql.append(" and entregaDocumento.habilitado = true" );
		hql.append(" and entregaDocumento.tipoDocumento.descricao like 'PROTOCOLO' ");
		hql.append(" and entregaDocumento.indicadorRetornado = true) ");
		
		return hql;
	}

	@Override
	public Collection<FaturaClienteNegativacao> obterListaClientesNegativacao(ClienteNegativacaoVO vo)
			throws NegocioException {

		Collection<FaturaClienteNegativacao> listaFaturas = this.listarFaturaClientesNegativacao(vo);
		Collection<FaturaClienteNegativacao> listaClienteNegativacao = new HashSet<>();

		Boolean isObrigatorioProtocolo = controladorAvisoCorte.verificarObrigatoriedadeProtocolo();
		for (FaturaClienteNegativacao faturaClienteNegativacao : listaFaturas) {
			Boolean inserir = Boolean.TRUE;
			if (isObrigatorioProtocolo) {
				inserir = Boolean.FALSE;

				EntregaDocumento entregaDocumento = controladorEntregaDocumento
						.obterEntregaDocumentoProtocoloNegativacaoRetorno(faturaClienteNegativacao.getChavePrimaria());
				if (entregaDocumento != null) {
					AgenteNegativador agenteNegativador = faturaClienteNegativacao.getClienteNegativacao().getComando()
							.getAgenteNegativador();

					if (agenteNegativador.getPrazoNegativacao() != null) {
						DateTime dataAtual = new DateTime(Calendar.getInstance().getTime());
						Date dataAtualMenosProtocolo = new DateTime(dataAtual)
								.minusDays(agenteNegativador.getPrazoNegativacao()).toDate();

						if (entregaDocumento.getDataSituacao().before(dataAtualMenosProtocolo)) {
							inserir = Boolean.TRUE;
						}
					}
				}

			}

			if (inserir) {
				listaClienteNegativacao.add(faturaClienteNegativacao);
			}
		}

		return listaClienteNegativacao;
	}
		
	private Collection<FaturaClienteNegativacao> selecionarFaturasSemRetornoProtocolo(
			Collection<FaturaClienteNegativacao> listaTodasFaturas) throws NegocioException {
		
		Collection<FaturaClienteNegativacao> listaSemProtocolo = new HashSet<>();
		for (FaturaClienteNegativacao faturaClienteNegativacao : listaTodasFaturas) {

			EntregaDocumento entregaDocumento = controladorEntregaDocumento
					.obterEntregaDocumentoProtocoloNegativacaoRetorno(
							faturaClienteNegativacao.getChavePrimaria());
			if (entregaDocumento == null) {
				listaSemProtocolo.add(faturaClienteNegativacao);
			}
		}
		return listaSemProtocolo;
	}
	
	private boolean isFaturaAptaInclusao(FaturaClienteNegativacao faturaClienteNegativacao) throws NegocioException {
		Boolean isObrigatorioProtocolo = controladorAvisoCorte.verificarObrigatoriedadeProtocolo();
		
		Boolean inserir = Boolean.TRUE;
		if (isObrigatorioProtocolo) {
			inserir = Boolean.FALSE;
			
			EntregaDocumento entregaDocumento = controladorEntregaDocumento
					.obterEntregaDocumentoProtocoloNegativacaoRetorno(
							faturaClienteNegativacao.getChavePrimaria());
			
			if (entregaDocumento != null) {
				AgenteNegativador agenteNegativador = faturaClienteNegativacao.getClienteNegativacao()
						.getComando().getAgenteNegativador();

				if (agenteNegativador.getPrazoNegativacao() != null) {
					DateTime dataAtual = new DateTime(Calendar.getInstance().getTime());
					Date dataAtualMenosProtocolo = new DateTime(dataAtual)
							.minusDays(agenteNegativador.getPrazoNegativacao()).toDate();

					if (entregaDocumento.getDataSituacao().before(dataAtualMenosProtocolo)) {
						inserir = Boolean.TRUE;
					}
				}
			}
		}
		return inserir;
		
		
	}
	
	
	private FaturaClienteNegativacao obterFaturaClienteNegativacao(Long chavePrimaria) {
			StringBuilder hql = new StringBuilder();

			hql.append(" FROM ");
			hql.append(getClasseEntidadeFaturaClienteNegativacao().getSimpleName());
			hql.append(" faturaClienteNegativacao ");
			hql.append(" WHERE ");
			hql.append(" faturaClienteNegativacao.chavePrimaria = :chavePrimaria ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("chavePrimaria", chavePrimaria);

			return (FaturaClienteNegativacao) query.uniqueResult();
	}
	
	@Override
	public Collection<AgenteNegativador> consultaAgenteNegativador(Map<String,Object> filtro){
		StringBuilder hql = new StringBuilder();
		String nomeCliente = null;
		String numeroContrato = null;
		Boolean indicadorPrioritario = null;
		
		if (filtro != null) {
			nomeCliente = (String) filtro.get("nomeCliente");
			numeroContrato = (String) filtro.get("numeroContrato");
			indicadorPrioritario = (Boolean) filtro.get("indicadorPrioritario");
		}
		
		hql.append(" FROM ");
		hql.append(getClasseEntidadeAgenteNegativador().getSimpleName());
		hql.append(" agenteNegativador ");
		hql.append(" WHERE ");
		hql.append(" agenteNegativador.habilitado = true ");
		
		if(nomeCliente != null && !nomeCliente.isEmpty()) {
			hql.append(" and agenteNegativador.cliente.nome like :nomeCliente ");
			
		}
		
		if(numeroContrato != null && !numeroContrato.isEmpty()) {
			hql.append(" and agenteNegativador.numeroContrato like :numeroContrato ");
		}
		
		if(indicadorPrioritario != null) {
			hql.append(" and agenteNegativador.indicadorPrioritario = :indicadorPrioritario ");
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(nomeCliente != null && !nomeCliente.isEmpty()) {
			query.setString("nomeCliente", "%"+nomeCliente+"%");
		}
		
		if(numeroContrato != null && !numeroContrato.isEmpty()) {
			query.setString("numeroContrato",  "%"+numeroContrato+"%");
		}
		
		if(indicadorPrioritario != null) {
			query.setBoolean("indicadorPrioritario",  indicadorPrioritario);
		}
		
		return query.list();
	}
	
	@Override
	public AgenteNegativador obterAgenteNegativadorPrioritario() {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeAgenteNegativador().getSimpleName());
		hql.append(" agenteNegativador ");
		hql.append(" WHERE ");
		hql.append(" agenteNegativador.indicadorPrioritario = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		
		return (AgenteNegativador) query.uniqueResult();
	}
	
	@Override
	public void incluirClientesNegativacao(Long[] idsClientesNegativacao, Date dataInclusao) throws NegocioException {
		
		Long situacaoPrescrito = obterSituacaoPagamentoPrescrito();
		Long situacaoQuitado = obterSituacaoPagamentoQuitado();
		String parametro = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MESES_PRESCRICAO);
		
		DateTime dataPrescricao = new DateTime().minusMonths(Integer.valueOf(parametro));
		
		Collection<FaturaClienteNegativacao> faturasNegativacao = consultarListaFaturasNegativacao(idsClientesNegativacao);
		for (FaturaClienteNegativacao faturaNegativacao : faturasNegativacao) {
			try {
				if (faturaNegativacao.getDataInclusaoNegativacao() == null
						&& faturaNegativacao.getDataCancelamentoNegativacao() == null
						&& (faturaNegativacao.getFatura().getSituacaoPagamento().getChavePrimaria() != situacaoPrescrito
						&& faturaNegativacao.getFatura().getSituacaoPagamento().getChavePrimaria() != situacaoQuitado)
						&& faturaNegativacao.getFatura().getDataVencimento().after(dataPrescricao.toDate())) {
					faturaNegativacao.setDataInclusaoNegativacao(dataInclusao);
					faturaNegativacao.setUltimaAlteracao(new Date());
					atualizar(faturaNegativacao);
				}
			} catch (ConcorrenciaException | NegocioException e) {
				LOG.info(e.getStackTrace(), e);
			}
		}
	}
	
	@Override
	public void confirmarInclusaoClientesNegativacao(Long[] idsClientesNegativacao, DadosAuditoria dadosAuditoria, Date dataConfirmacao) {
		Collection<FaturaClienteNegativacao> faturasNegativacao = consultarListaFaturasNegativacao(idsClientesNegativacao);
		for (FaturaClienteNegativacao faturaNegativacao : faturasNegativacao) {
			try {
				if (faturaNegativacao.getDataConfirmacaoNegativacao() == null
						&& faturaNegativacao.getDataInclusaoNegativacao() != null) {
					faturaNegativacao.setDataConfirmacaoNegativacao(dataConfirmacao);
					faturaNegativacao.setUltimaAlteracao(new Date());
					
					Cliente cliente = faturaNegativacao.getClienteNegativacao().getCliente();
					cliente.setIndicadorNegativado(true);
					controladorCliente.atualizar(cliente);
					
					atualizar(faturaNegativacao);
				}
			} catch (GGASException e) {
				LOG.info(e.getStackTrace(), e);
			}
		}
	}

	@Override
	public void excluirClientesNegativacao(Long[] idsClientesNegativacao, DadosAuditoria dadosAuditoria, Date dataExclusao) {
		Collection<FaturaClienteNegativacao> faturasNegativacao = consultarListaFaturasNegativacao(idsClientesNegativacao);
		for (FaturaClienteNegativacao faturaNegativacao : faturasNegativacao) {
			try {
				if (faturaNegativacao.isFaturaAptaExclusaoNegativacao()) {
					faturaNegativacao.setDataExclusaoNegativacao(dataExclusao);
					faturaNegativacao.setUltimaAlteracao(new Date());
					
					if (!verificarSeClientePossuiOutrasFaturasNegativadas(faturaNegativacao)) {
						Cliente cliente = faturaNegativacao.getClienteNegativacao().getCliente();
						cliente.setIndicadorNegativado(false);
						controladorCliente.atualizar(cliente);
					}
					
					atualizar(faturaNegativacao);
				}

			} catch (GGASException e) {
				LOG.info(e.getStackTrace(), e);
			}
		}
	}
	
	private boolean verificarSeClientePossuiOutrasFaturasNegativadas(FaturaClienteNegativacao faturaClienteNegativacao) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeFaturaClienteNegativacao().getSimpleName());
		hql.append(" faturaClienteNegativacao ");
		hql.append(" inner join fetch faturaClienteNegativacao.fatura fatura ");
		hql.append(" inner join fetch faturaClienteNegativacao.clienteNegativacao clienteNegativacao ");
		hql.append(" inner join fetch clienteNegativacao.cliente cliente ");
		hql.append(" WHERE");
		hql.append(" cliente.chavePrimaria = :idCliente ");
		hql.append(" and faturaClienteNegativacao.chavePrimaria <> :idFatura ");
		hql.append(" and faturaClienteNegativacao.dataExclusaoNegativacao is null ");
		hql.append(" and faturaClienteNegativacao.dataCancelamentoNegativacao is null ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("idCliente", faturaClienteNegativacao.getClienteNegativacao().getCliente().getChavePrimaria());
		query.setParameter("idFatura", faturaClienteNegativacao.getChavePrimaria());

		Collection<FaturaClienteNegativacao> faturas = query.list();

		if (faturas != null && faturas.size() > 0) {
			return true;
		}

		return false;
	}
	
	private Collection<FaturaClienteNegativacao> consultarListaFaturasNegativacao(Long[] ids) {
		Collection<FaturaClienteNegativacao> listaCLientes = null;
		if ((ids != null) && (ids.length > 0)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" FROM ");
			hql.append(getClasseEntidadeFaturaClienteNegativacao().getSimpleName());
			hql.append(" faturasClientes ");
			hql.append(" WHERE ");

			hql.append(" faturasClientes.chavePrimaria in (:ids) ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameterList("ids", ids);

			listaCLientes= query.list();
		}

		return listaCLientes;
	}
	
	@Override
	public Collection<ComandoNegativacaoRota> obterComandoNegativacaoRota(Long chaveComando) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacaoRota().getSimpleName());
		hql.append(" comandoNegativacaoRota ");
		hql.append(" WHERE ");
		hql.append(" comandoNegativacaoRota.comandoNegativacao.chavePrimaria = :chaveComando ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveComando", chaveComando);
		
		return query.list();
	}
	
	
	@Override
	public Boolean verificarCadastroAgenteNegativador(AgenteNegativador agenteNegativador) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeAgenteNegativador().getSimpleName());
		hql.append(" agenteNegativador ");
		hql.append(" WHERE ");
		hql.append(" agenteNegativador.cliente.chavePrimaria = :chaveCliente ");
		hql.append(" and (:dataInicio BETWEEN agenteNegativador.dataInicioContrato and agenteNegativador.dataFimContrato ");
		hql.append(" or :dataFim BETWEEN agenteNegativador.dataInicioContrato and agenteNegativador.dataFimContrato) ");
		hql.append(" and agenteNegativador.habilitado = true ");
		
		if(agenteNegativador.getChavePrimaria() != 0) {
			hql.append(" and agenteNegativador.chavePrimaria <> :chavePrimariaAgente ");
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveCliente", agenteNegativador.getCliente().getChavePrimaria());
		query.setDate("dataInicio", agenteNegativador.getDataInicioContrato());
		query.setDate("dataFim", agenteNegativador.getDataFimContrato());
		
		if(agenteNegativador.getChavePrimaria() != 0) {
			query.setLong("chavePrimariaAgente", agenteNegativador.getChavePrimaria());
		}

		Collection<AgenteNegativador> listaAgenteNegativador = query.list();
		return listaAgenteNegativador.size() > 0;
	}
	
	
	@Override
	public void cancelarClientesNegativacao(Long[] idsClientesNegativacao, DadosAuditoria dadosAuditoria, Date dataCancelamento) {
		Collection<FaturaClienteNegativacao> faturasNegativacao = consultarListaFaturasNegativacao(idsClientesNegativacao);
		for (FaturaClienteNegativacao faturaNegativacao : faturasNegativacao) {
			try {
				if (faturaNegativacao.getDataCancelamentoNegativacao() == null
						&& faturaNegativacao.getDataConfirmacaoNegativacao() == null
						&& faturaNegativacao.getDataExclusaoNegativacao() == null) {
					faturaNegativacao.setDataCancelamentoNegativacao(dataCancelamento);
					faturaNegativacao.setUltimaAlteracao(new Date());
					atualizar(faturaNegativacao);
				}
			} catch (GGASException e) {
				LOG.info(e.getStackTrace(), e);
			}
		}
	}
	
	@Override
	public ComandoNegativacao obterComandoNegativacaoPeloNomeEChave(String nomeComando, Long chaveComando) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacao().getSimpleName());
		hql.append(" comandoNegativacao ");
		hql.append(" WHERE ");
		hql.append(" upper(comandoNegativacao.nomeComando) like :nomeComando ");
		hql.append(" and comandoNegativacao.chavePrimaria <> :chaveComando ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("nomeComando", nomeComando.toUpperCase());
		query.setLong("chaveComando", chaveComando);

		return (ComandoNegativacao) query.uniqueResult();
	}
	
	@Override
	public Collection<ComandoNegativacaoCliente> obterComandoCliente(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		
		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacaoCliente().getSimpleName());
		hql.append(" comandoNegativacaoCliente ");
		hql.append(" WHERE ");
		hql.append(" comandoNegativacaoCliente.comando.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", chavePrimaria);

		return query.list();
	}
	
	
	@Override
	public Collection<ClienteVO> popularListaClienteVOComandoCliente(Long chavePrimaria) throws NegocioException {
		Collection<ClienteVO> listaClienteVO = new ArrayList<>();
		Collection<ComandoNegativacaoCliente> listaComandoCliente = this.obterComandoCliente(chavePrimaria);

		for(ComandoNegativacaoCliente comandoCliente : listaComandoCliente) {
			ClienteVO clienteVO = new ClienteVO();
			Cliente cliente = comandoCliente.getCliente();
			Integer quantidadeFaturasVencidas = 0;
			BigDecimal valorTotalFaturasVencidas = BigDecimal.ZERO;
			
			clienteVO.setCliente(cliente);

			Collection<Fatura> faturasVencidas = controladorFatura
					.consultarFaturasVencidasCliente(cliente.getChavePrimaria());

			if (faturasVencidas != null && !faturasVencidas.isEmpty()) {

				for (Fatura fatura : faturasVencidas) {
					quantidadeFaturasVencidas = quantidadeFaturasVencidas + 1;
					valorTotalFaturasVencidas = valorTotalFaturasVencidas.add(fatura.getValorTotal());
				}
			}

			clienteVO.setQuantidadeFaturasVencidas(quantidadeFaturasVencidas);
			clienteVO.setValorTotalFaturasVencidas(valorTotalFaturasVencidas);
			
			
			listaClienteVO.add(clienteVO);
		}
		
		return listaClienteVO;
	}
	
	@Override
	public Boolean verificaSeComandoFoiExecutado (Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeComandoNegativacao().getSimpleName()).append(" comando ");
		hql.append(" WHERE ");
		hql.append(" comando.habilitado = true ");
		hql.append(" and comando.chavePrimaria = :chavePrimaria ");
		hql.append(" and comando.chavePrimaria in ");
		hql.append(" (select distinct cliente.comando.chavePrimaria ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeClienteNegativacao().getSimpleName()).append(" cliente ");
		hql.append(" WHERE ");
		hql.append(" cliente.dataNotificacao is not null) ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", chavePrimaria);
		
		return query.list().size() == 0;
		
	}
	
	@Override
	public void removerComandoNegativacao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {
		for (Long chavePrimariaComando : chavesPrimarias) {
			Collection<ComandoNegativacaoCliente> listaComandoCliente = this.obterComandoCliente(chavePrimariaComando);
			if(listaComandoCliente != null && !listaComandoCliente.isEmpty()) {
				Long[] chavesPrimariasComandoCliente = Util.collectionParaArrayChavesPrimarias(listaComandoCliente);
				this.remover(chavesPrimariasComandoCliente, ComandoNegativacaoCliente.class.getName(), dadosAuditoria);
			}
			
			Collection<ComandoNegativacaoRota> listaComandoRota = this.obterComandoNegativacaoRota(chavePrimariaComando);
			if(listaComandoRota != null && !listaComandoRota.isEmpty()) {
				Long[] chavesPrimariasComandoRota = Util.collectionParaArrayChavesPrimarias(listaComandoRota);
				this.remover(chavesPrimariasComandoRota, ComandoNegativacaoRota.class.getName(), dadosAuditoria);
			}
			
		}
		
		this.remover(chavesPrimarias, ComandoNegativacao.class.getName(), dadosAuditoria);
	}
}
