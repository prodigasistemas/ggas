package br.com.ggas.cobranca.negativacao;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ComandoNegativacaoCliente extends EntidadeNegocio {

	String BEAN_ID_COMANDO_NEGATIVACAO_CLIENTE= "comandoNegativacaoCliente";
	
	/**
	 * @return ComandoNegativacao - {@link ComandoNegativacao}
	 */
	ComandoNegativacao getComando();

	/**
	 * @param comando - {@link ComandoNegativacao}
	 */
	void setComando(ComandoNegativacao comando);

	/**
	 * @return Cliente - {@link Cliente}
	 */
	Cliente getCliente();

	/**
	 * @param cliente - {@link Cliente}
	 */
	void setCliente(Cliente cliente);
}
