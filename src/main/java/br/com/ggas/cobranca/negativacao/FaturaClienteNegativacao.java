package br.com.ggas.cobranca.negativacao;

import java.util.Date;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface FaturaClienteNegativacao extends EntidadeNegocio {

	String BEAN_ID_FATURA_CLIENTE_NEGATIVACAO= "faturaClienteNegativacao";


	/**
	 * @return ClienteNegativacao - {@link ClienteNegativacao}
	 */
	ClienteNegativacao getClienteNegativacao();

	/**
	 * @param clienteNegativacao - {@link ClienteNegativacao}
	 */
	void setClienteNegativacao(ClienteNegativacao clienteNegativacao);

	
	/**
	 * @return Fatura - {@link Fatura}
	 */
	Fatura getFatura();

	/**
	 * @param fatura - {@link Fatura}
	 */
	void setFatura(Fatura fatura);
	
	/**
	 * @return Date - {@link Date}
	 */
	Date getDataInclusaoNegativacao();
	
	/**
	 * @param Date - {@link Date}
	 */
	void setDataInclusaoNegativacao(Date dataInclusaoNegativacao);
	
	/**
	 * @return Date - {@link Date}
	 */
	Date getDataExclusaoNegativacao();
	
	/**
	 * @param Date - {@link Date}
	 */
	void setDataExclusaoNegativacao(Date dataExclusaoNegativacao);
	
	
	/**
	 * @return Date - {@link Date}
	 */
	Date getDataConfirmacaoNegativacao();

	/**
	 * @param Date - {@link Date}
	 */
	void setDataConfirmacaoNegativacao(Date dataConfirmacaoNegativacao);	

	Date getDataCancelamentoNegativacao();

	void setDataCancelamentoNegativacao(Date dataCancelamentoNegativacao);

	boolean isFaturaAptaExclusaoNegativacao();
}
