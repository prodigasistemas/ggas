package br.com.ggas.cobranca.negativacao.dominio;

public class ClienteNegativacaoVO {

	private Integer situacaoNegativacao;
	private Integer situacaoFaturas;
	private Long chaveComando;
	private String nomeComando;

	public Integer getSituacaoNegativacao() {
		return situacaoNegativacao;
	}

	public void setSituacaoNegativacao(Integer situacaoNegativacao) {
		this.situacaoNegativacao = situacaoNegativacao;
	}

	public Integer getSituacaoFaturas() {
		return situacaoFaturas;
	}
	
	public void setSituacaoFaturas(Integer situacaoFaturas) {
		this.situacaoFaturas = situacaoFaturas;
	}
	
	public Long getChaveComando() {
		return chaveComando;
	}
	
	public void setChaveComando(Long chaveComando) {
		this.chaveComando = chaveComando;
	}

	public String getNomeComando() {
		return nomeComando;
	}

	public void setNomeComando(String nomeComando) {
		this.nomeComando = nomeComando;
	}
	
	
}
