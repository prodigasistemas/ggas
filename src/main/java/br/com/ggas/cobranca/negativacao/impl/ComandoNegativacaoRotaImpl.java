package br.com.ggas.cobranca.negativacao.impl;

import java.util.Map;

import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoRota;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.Rota;

public class ComandoNegativacaoRotaImpl extends EntidadeNegocioImpl implements ComandoNegativacaoRota{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6819805203728187906L;
	
	private Rota rota;
	private ComandoNegativacao comandoNegativacao;
	
	
	
	public Map<String, Object> validarDados() {
		return null;
	}


	@Override
	public Rota getRota() {
		return rota;
	}


	@Override
	public void setRota(Rota rota) {
		this.rota = rota;
	}


	@Override
	public ComandoNegativacao getComandoNegativacao() {
		return comandoNegativacao;
	}


	@Override
	public void setComandoNegativacao(ComandoNegativacao comandoNegativacao) {
		this.comandoNegativacao = comandoNegativacao;
	}

}
