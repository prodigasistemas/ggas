/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/01/2014 15:04:20
 @author wcosta
 */

package br.com.ggas.cobranca.negativacao;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.negativacao.dominio.ClienteNegativacaoVO;
import br.com.ggas.cobranca.negativacao.dominio.ComandoNegativacaoVO;
import br.com.ggas.atendimento.chamado.dominio.ClienteVO;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 *  Interface responsável pela assinatura de métodos relacionados ao controlador de Negativação.
 *
 */
public interface ControladorNegativacao extends ControladorNegocio{
	String BEAN_ID_CONTROLADOR_NEGATIVACAO = "controladorNegativacao";

	/**
	 * Método responsável por obter a lista de
	 * periodicidade SOP.
	 * 
	 * @return coleção de periodicidade SOP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<AgenteNegativador> listarAgenteNegativador() ;

	/**
	 * Obter agente negativador pela chave primaria
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @return AgenteNegativador - {@link AgenteNegativador}
	 */
	AgenteNegativador obterAgenteNegativador(Long chavePrimaria);
	
	/**
	 * Obter a lista de faturs selecionadas de acordo com
	 * o comando de negativacao
	 * 
	 * @param comando - {@link ComandoNegativacao}
	 * @return lista de faturas - {@link Collection}
	 * @throws NegocioException 
	 */
	Collection<Fatura> obterFaturasComando(ComandoNegativacao comando) throws NegocioException;

	EntidadeNegocio criarComandoNegativacaoCliente();

	ComandoNegativacao obterComandoNegativacao(Long chavePrimaria);

	Processo inserirProcesso(Usuario usuario, DadosAuditoria dadosAuditoria, ComandoNegativacao comando) throws GGASException;

	/**
	 * Obter Comando Negativacao cliente pelo Comando de Negativacao
	 * @param chavePrimaria - {@link Long}
	 * @return lista comando negativacao cliente - {@link ComandoNegativacaoCliente}
	 */
	Collection<ComandoNegativacaoCliente> obterComandoNegativacaoCliente(Long chavePrimaria);

	EntidadeNegocio criarFaturaClienteNegativacao();

	EntidadeNegocio criarClienteNegativacao();

	ClienteNegativacao obterClienteNegativacao(Long chavePrimaria);

	Map<Cliente, byte[]> processarEmissaoNotificacaoNegativacao(StringBuilder logProcessamento, ComandoNegativacao comandoNegativacao) throws GGASException;	
	
	Collection<ComandoNegativacao> listarComandos(ComandoNegativacaoVO comando) throws ParseException;
	
	/**
	 * A lista de faturas a serem negativadas
	 * @param vo - {@link ClienteNegativacaoVO}
	 * @return lista de faturas - {@link Collection<FaturaClienteNegativacao>}
	 * @throws NegocioException 
	 */
	Collection<FaturaClienteNegativacao> listarFaturaClientesNegativacao(ClienteNegativacaoVO vo) throws NegocioException;

	Collection<AgenteNegativador> consultaAgenteNegativador(Map<String, Object> filtro);

	AgenteNegativador obterAgenteNegativadorPrioritario();
	
	/**
	 * incluir a lista de clientes na negativacao
	 * @param clientes - {@link Collection<ClienteNegativacao>}
	 * @throws NegocioException 
	 */
	void incluirClientesNegativacao(Long[] idsClientesNegativacao, Date dataInclusao) throws NegocioException;
	
	/**
	 * Confirmar a inclusao de clientes na negativacao
	 * @param clientes - {@link Collection<ClienteNegativacao>}
	 */
	void confirmarInclusaoClientesNegativacao(Long[] idsClientesNegativacao, DadosAuditoria dadosAuditoria, Date dataConfirmacao);
	
	/**
	 * Excluir clientes da negativacao
	 * @param clientes - {@link Collection<ClienteNegativacao>}
	 */
	void excluirClientesNegativacao(Long[] idsClientesNegativacao, DadosAuditoria dadosAuditoria, Date dataExclusao);
	

	EntidadeNegocio criarComandoNegativacaoRota();

	Collection<ComandoNegativacaoRota> obterComandoNegativacaoRota(Long chaveComando);

	Collection<FaturaClienteNegativacao> obterListaClientesNegativacao(ClienteNegativacaoVO vo) throws NegocioException;

	Boolean verificarCadastroAgenteNegativador(AgenteNegativador agenteNegativador);

	void cancelarClientesNegativacao(Long[] idsClientesNegativacao, DadosAuditoria dadosAuditoria, Date dataCancelamento);

	ComandoNegativacao obterComandoNegativacaoPeloNomeEChave(String nomeComando, Long chaveComando);

	Collection<ComandoNegativacaoCliente> obterComandoCliente(Long chavePrimaria);

	Collection<ClienteVO> popularListaClienteVOComandoCliente(Long chavePrimaria) throws NegocioException;

	Boolean verificaSeComandoFoiExecutado(Long chavePrimaria);

	void removerComandoNegativacao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;
}
