package br.com.ggas.cobranca.negativacao;

import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.rota.Rota;

public interface ComandoNegativacaoRota extends EntidadeNegocio{
	
	String BEAN_ID_COMANDO_NEGATIVACAO_ROTA= "comandoNegativacaoRota";
	
	Rota getRota();

	void setRota(Rota rota);

	ComandoNegativacao getComandoNegativacao();

	void setComandoNegativacao(ComandoNegativacao comandoNegativacao);

}
