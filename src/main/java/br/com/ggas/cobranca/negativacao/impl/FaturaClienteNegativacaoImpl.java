package br.com.ggas.cobranca.negativacao.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cobranca.negativacao.ClienteNegativacao;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class FaturaClienteNegativacaoImpl extends EntidadeNegocioImpl implements FaturaClienteNegativacao {

	private static final long serialVersionUID = 3940843846646002186L;

	private ClienteNegativacao clienteNegativacao;
	private Fatura fatura;
	private Date dataInclusaoNegativacao;
	private Date dataConfirmacaoNegativacao;
	private Date dataExclusaoNegativacao;
	private Date dataCancelamentoNegativacao;
	
	@Override
	public ClienteNegativacao getClienteNegativacao() {
		return this.clienteNegativacao;
	}

	@Override
	public void setClienteNegativacao(ClienteNegativacao clienteNegativacao) {
		this.clienteNegativacao = clienteNegativacao;
	}

	@Override
	public Fatura getFatura() {
		return this.fatura;
	}

	@Override
	public void setFatura(Fatura fatura) {
		this.fatura = fatura;
	}

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	public String getDescricaoReferenciaFatura() {
		StringBuilder descricao = new StringBuilder();
		
		if (fatura != null) {
			descricao.append(fatura.getAnoMesReferencia())
					.append(" / ")
					.append(fatura.getNumeroCiclo())
					.append("\n");
		}
		return descricao.toString();
	}

	@Override
	public Date getDataInclusaoNegativacao() {
		return dataInclusaoNegativacao;
	}

	@Override
	public void setDataInclusaoNegativacao(Date dataInclusaoNegativacao) {
		this.dataInclusaoNegativacao = dataInclusaoNegativacao;
	}

	@Override
	public Date getDataConfirmacaoNegativacao() {
		return dataConfirmacaoNegativacao;
	}

	@Override
	public void setDataConfirmacaoNegativacao(Date dataConfirmacaoNegativacao) {
		this.dataConfirmacaoNegativacao = dataConfirmacaoNegativacao;
	}

	@Override
	public Date getDataExclusaoNegativacao() {
		return dataExclusaoNegativacao;
	}

	@Override
	public void setDataExclusaoNegativacao(Date dataExclusaoNegativacao) {
		this.dataExclusaoNegativacao = dataExclusaoNegativacao;
	}

	@Override
	public Date getDataCancelamentoNegativacao() {
		return dataCancelamentoNegativacao;
	}

	@Override
	public void setDataCancelamentoNegativacao(Date dataCancelamentoNegativacao) {
		this.dataCancelamentoNegativacao = dataCancelamentoNegativacao;
	}
	
	public boolean isFaturaAptaExclusaoNegativacao() {
		return dataExclusaoNegativacao == null && dataConfirmacaoNegativacao != null;
	}
}
