/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.negativacao;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface Comando Negativacao
 */
public interface ComandoNegativacao extends EntidadeNegocio{
	
	String BEAN_ID_COMANDO_NEGATIVACAO = "comandoNegativacao";
	
	/** The comando negativacao. */
	String COMANDO_NEGATIVACAO = "Comando(s) Negativador(es)";
	
	/**
	 * @return AgenteNegativador - {@link AgenteNegativador}
	 */
	AgenteNegativador getAgenteNegativador();
	
	/**
	 * @param cliente - {@link Cliente}
	 */
	void setAgenteNegativador(AgenteNegativador agenteNegativador);
	
	/**
	 * @return nomeComando - {@link String}
	 */
	String getNomeComando();
	
	/**
	 * @param nomeComando - {@link String}
	 */
	void setNomeComando(String nomeComando);
	
	
	/**
	 * @return dataPrevistaExecucao - {@link Date}
	 */
	Date getDataPrevistaExecucao();
	
	/**
	 * @param dataPrevistaExecucao - {@link Date}
	 */
	void setDataPrevistaExecucao(Date dataPrevistaExecucao);
	
	/**
	 * @return indicadorSimulacao - {@link boolean}
	 */
	boolean getIndicadorSimulacao();
	
	/**
	 * @param indicadorSimulacao - {@link boolean}
	 */
	void setIndicadorSimulacao(boolean indicadorSimulacao);
	
	/**
	 * @return quantidadeMaximaInclusao - {@link Integer}
	 */
	Integer getQuantidadeMaximaInclusao();
	
	/**
	 * @param quantidadeMaximaInclusao - {@link Integer}
	 */
	void setQuantidadeMaximaInclusao(Integer quantidadeMaximaInclusao);
	
	/**
	 * @return referenciaInicialDebitos - {@link Integer}
	 */
	Integer getReferenciaInicialDebitos();
	
	/**
	 * @param referenciaInicialDebitos - {@link Integer}
	 */
	void setReferenciaInicialDebitos(Integer referenciaInicialDebitos);
	
	/**
	 * @return referenciaFinalDebitos - {@link Integer}
	 */
	Integer getReferenciaFinalDebitos();
	
	/**
	 * @param referenciaFinalDebitos - {@link Integer}
	 */
	void setReferenciaFinalDebitos(Integer referenciaFinalDebitos);
	
	/**
	 * @return dataVencimentoInicial - {@link Date}
	 */
	Date getDataVencimentoInicial();
	
	/**
	 * @param dataVencimentoInicial - {@link Date}
	 */
	void setDataVencimentoInicial(Date dataVencimentoInicial);

	/**
	 * @return dataVencimentoFinal - {@link Date}
	 */
	Date getDataVencimentoFinal();
	
	/**
	 * @param dataVencimentoFinal - {@link Date}
	 */
	void setDataVencimentoFinal(Date dataVencimentoFinal);
	
	/**
	 * @return valorInicialDebitos - {@link BigDecimal}
	 */
	BigDecimal getValorInicialDebitos();
	
	/**
	 * @param valorInicialDebitos - {@link BigDecimal}
	 */
	void setValorInicialDebitos(BigDecimal valorInicialDebitos);
	
	/**
	 * @return valorFinallDebitos - {@link BigDecimal}
	 */
	BigDecimal getValorFinalDebitos();
	
	/**
	 * @param valorFinalnicialDebitos - {@link BigDecimal}
	 */
	void setValorFinalDebitos(BigDecimal valorFinalDebitos);
	
	/**
	 * @return indicadorConsiderarContasEmAnalise - {@link boolean}
	 */
	boolean getIndicadorConsiderarContasEmAnalise();
	
	/**
	 * @param indicadorConsiderarContasEmAnalise - {@link boolean}
	 */
	void setIndicadorConsiderarContasEmAnalise(boolean indicadorConsiderarContasEmAnalise);
	
	/**
	 * @return situacaoConsumo - {@link SituacaoConsumo}
	 */
	SituacaoConsumo getSituacaoConsumo();
	
	/**
	 * @param situacaoConsumo - {@link SituacaoConsumo}
	 */
	void setSituacaoConsumo(SituacaoConsumo situacaoConsumo);
	
	/**
	 * @return segmento - {@link Segmento}
	 */
	Segmento getSegmento();
	
	/**
	 * @param segmento - {@link Segmento}
	 */
	void setSegmento(Segmento segmento);
	
	/**
	 * @return indicadorCliente - {@link int}
	 */
	int getIndicadorCliente();
	
	/**
	 * @param indicadorCliente - {@link int}
	 */
	void setIndicadorCliente(int indicadorCliente);
	
	
	/**
	 * @return situacaoContrato - {@link SituacaoContrato}
	 */
	SituacaoContrato getSituacaoContrato();
	
	/**
	 * @param situacaoContrato - {@link SituacaoContrato}
	 */
	void setSituacaoContrato(SituacaoContrato situacaoContrato);
	
	/**
	 * @return grupoFaturamento - {@link GrupoFaturamento}
	 */
	GrupoFaturamento getGrupoFaturamento();
	
	/**
	 * @param grupoFaturamento - {@link GrupoFaturamento}
	 */
	void setGrupoFaturamento(GrupoFaturamento grupoFaturamento);
	
	/**
	 * @return indicadorComandoCliente - {@link boolean}
	 */
	boolean getIndicadorComandoCliente();
	
	/**
	 * @param indicadorComandoCliente - {@link boolean}
	 */
	void setIndicadorComandoCliente(boolean indicadorComandoCliente);
	
	/**
	 * @param indicadorComandoCliente - {@link boolean}
	 */

	Long[] getChavesRotas();

	void setChavesRotas(Long[] chavesRotas);

	Integer getDiasAtrasoInicial();

	void setDiasAtrasoInicial(Integer diasAtrasoInicial);

	Integer getDiasAtrasoFim();

	void setDiasAtrasoFim(Integer diasAtrasoFim);

	
	
}
