package br.com.ggas.cobranca.negativacao.dominio;

import java.math.BigDecimal;

/**
 * Classe que representa a consulta de comando de negativacao
 */
public class ComandoNegativacaoVO {
	
	private String chaveComando;

	private String nomeComando;
	
	private String nomeComandoAtualizar;
	
	private Boolean executado;
	
	private String dataInicioExecucao;
	
	private String dataFimExecucao;
	
	/**
	 * recupera o nomeComando 
         * nomeComando
	 */
	public String getNomeComando() {
		return nomeComando;
	}

	/**
	 * @param nomeComando
	 * define o nomeComando do comando
	 */
	public void setNomeComando(String nomeComando) {
		this.nomeComando = nomeComando;
	}

	/**
	 * recupera a informacao se o comando foi executado 
         * @return executado
	 */
	public Boolean getExecutado() {
		return executado;
	}

	public void setExecutado(Boolean executado) {
		this.executado = executado;
	}

	/**
	 * recupera a data inicial da execucao 
         * @return dataInicioExecucao
	 */
	public String getDataInicioExecucao() {
		return dataInicioExecucao;
	}

	/**
	 * @param dataInicioExecucao
	 * define a data inicial da execucao
	 */
	public void setDataInicioExecucao(String dataInicioExecucao) {
		this.dataInicioExecucao = dataInicioExecucao;
	}
	
	/**
	 * recupera a data final da execucao 
         * @return dataUltimaExecucao
	 */
	public String getDataFimExecucao() {
		return dataFimExecucao;
	}

	/**
	 * @param dataFimExecucao
	 * define a data final da execucao
	 */
	public void setDataFimExecucao(String dataFimExecucao) {
		this.dataFimExecucao = dataFimExecucao;
	}

	/**
	 * 
	 * @return chaveComando - {@link String}
	 */
	public String getChaveComando() {
		return chaveComando;
	}

	/**
	 * 
	 * @param chaveComando - {@link String}
	 */
	public void setChaveComando(String chaveComando) {
		this.chaveComando = chaveComando;
	}

	public String getNomeComandoAtualizar() {
		return nomeComandoAtualizar;
	}

	public void setNomeComandoAtualizar(String nomeComandoAtualizar) {
		this.nomeComandoAtualizar = nomeComandoAtualizar;
	}

}