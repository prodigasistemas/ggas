package br.com.ggas.cobranca.negativacao.impl;

import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.cobranca.negativacao.ComandoNegativacaoCliente;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class ComandoNegativacaoClienteImpl extends EntidadeNegocioImpl implements ComandoNegativacaoCliente{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5407786852204451083L;
	private ComandoNegativacao comando;
	private Cliente cliente;
	
	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	public ComandoNegativacao getComando() {
		return comando;
	}

	public void setComando(ComandoNegativacao comando) {
		this.comando = comando;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	

}
