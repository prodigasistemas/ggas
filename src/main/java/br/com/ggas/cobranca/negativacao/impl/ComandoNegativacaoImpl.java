package br.com.ggas.cobranca.negativacao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cobranca.negativacao.AgenteNegativador;
import br.com.ggas.cobranca.negativacao.ComandoNegativacao;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

public class ComandoNegativacaoImpl extends EntidadeNegocioImpl implements ComandoNegativacao{

	private static final long serialVersionUID = 1958519156364611301L;
	
	private static final int FINAL_CAMPO = 2;

	private AgenteNegativador agenteNegativador;
	private String nomeComando;
	private boolean  indicadorSimulacao;
	private Date dataPrevistaExecucao;
	private Integer quantidadeMaximaInclusao;
	private Integer referenciaInicialDebitos;
	private Integer referenciaFinalDebitos;
	private Date dataVencimentoInicial;
	private Date dataVencimentoFinal;
	private BigDecimal valorInicialDebitos;
	private BigDecimal valorFinalDebitos;
	private boolean  indicadorConsiderarContasEmAnalise;
	private Integer diasAtrasoInicial;
	private Integer diasAtrasoFim;
	
	private SituacaoConsumo situacaoConsumo;
	private Segmento segmento;
	private int indicadorCliente;
	private SituacaoContrato situacaoContrato;
	private GrupoFaturamento grupoFaturamento;

	private boolean indicadorComandoCliente;
	private Long[] chavesRotas;
	
	@Override
	public AgenteNegativador getAgenteNegativador() {
		return agenteNegativador;
	}
	
	@Override
	public void setAgenteNegativador(AgenteNegativador agenteNegativador) {
		this.agenteNegativador = agenteNegativador;
	}
	
	@Override
	public String getNomeComando() {
		return nomeComando;
	}

	@Override
	public void setNomeComando(String nomeComando) {
		this.nomeComando = nomeComando;
		
	}
	
	@Override
	public Date getDataPrevistaExecucao() {
		return dataPrevistaExecucao;
	}
	
	@Override
	public void setDataPrevistaExecucao(Date dataPrevistaExecucao) {
		this.dataPrevistaExecucao = dataPrevistaExecucao;
	}
	
	@Override
	public boolean  getIndicadorSimulacao() {
		return indicadorSimulacao;
	}
	
	@Override
	public void setIndicadorSimulacao(boolean  indicadorSimulacao) {
		this.indicadorSimulacao = indicadorSimulacao;
	}
	
	@Override
	public Integer getQuantidadeMaximaInclusao() {
		return quantidadeMaximaInclusao;
	}
	
	@Override
	public void setQuantidadeMaximaInclusao(Integer quantidadeMaximaInclusao) {
		this.quantidadeMaximaInclusao = quantidadeMaximaInclusao;
	}
	
	@Override
	public Integer getReferenciaInicialDebitos() {
		return referenciaInicialDebitos;
	}
	
	@Override
	public void setReferenciaInicialDebitos(Integer referenciaInicialDebitos) {
		this.referenciaInicialDebitos = referenciaInicialDebitos;
	}
	
	@Override
	public Integer getReferenciaFinalDebitos() {
		return referenciaFinalDebitos;
	}
	
	@Override
	public void setReferenciaFinalDebitos(Integer referenciaFinalDebitos) {
		this.referenciaFinalDebitos = referenciaFinalDebitos;	
	}
	
	@Override
	public Date getDataVencimentoInicial() {
		return dataVencimentoInicial;
	}
	
	@Override
	public void setDataVencimentoInicial(Date dataVencimentoInicial) {
		this.dataVencimentoInicial = dataVencimentoInicial;
	}
	
	@Override
	public Date getDataVencimentoFinal() {
		return dataVencimentoFinal;
	}
	
	@Override
	public void setDataVencimentoFinal(Date dataVencimentoFinal) {
		this.dataVencimentoFinal = dataVencimentoFinal;
	}
	
	@Override
	public BigDecimal getValorInicialDebitos() {
		return valorInicialDebitos;
	}
	
	@Override
	public void setValorInicialDebitos(BigDecimal valorInicialDebitos) {
		this.valorInicialDebitos = valorInicialDebitos;
	}
	
	@Override
	public BigDecimal getValorFinalDebitos() {
		return valorFinalDebitos;
	}
	
	@Override
	public void setValorFinalDebitos(BigDecimal valorFinalDebitos) {
		this.valorFinalDebitos = valorFinalDebitos;
	}
	
	@Override
	public boolean  getIndicadorConsiderarContasEmAnalise() {
		return indicadorConsiderarContasEmAnalise;
	}
	
	@Override
	public void setIndicadorConsiderarContasEmAnalise(boolean  indicadorConsiderarContasEmAnalise) {
		this.indicadorConsiderarContasEmAnalise = indicadorConsiderarContasEmAnalise;
	}
	
	@Override
	public SituacaoConsumo getSituacaoConsumo() {
		return situacaoConsumo;
	}
	
	@Override
	public void setSituacaoConsumo(SituacaoConsumo situacaoConsumo) {
		this.situacaoConsumo = situacaoConsumo;
	}
	
	@Override
	public Segmento getSegmento() {
		return segmento;
	}
	
	@Override
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	
	@Override
	public int getIndicadorCliente() {
		return indicadorCliente;
	}
	
	@Override
	public void setIndicadorCliente(int indicadorCliente) {
		this.indicadorCliente = indicadorCliente;
	}
	
	@Override
	public SituacaoContrato getSituacaoContrato() {
		return situacaoContrato;
	}
	
	@Override
	public void setSituacaoContrato(SituacaoContrato situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}
	
	@Override
	public GrupoFaturamento getGrupoFaturamento() {
		return grupoFaturamento;
	}
	
	@Override
	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {
		this.grupoFaturamento = grupoFaturamento;
	}
	
	@Override
	public Map<String, Object> validarDados() {
		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(nomeComando == null || nomeComando.isEmpty()) {
			stringBuilder.append("Nome Comando");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - FINAL_CAMPO));
		}

		return erros;
	}

	@Override
	public boolean getIndicadorComandoCliente() {
		return indicadorComandoCliente;
	}

	@Override
	public void setIndicadorComandoCliente(boolean indicadorComandoCliente) {
		this.indicadorComandoCliente = indicadorComandoCliente;
		
	}

	@Override
	public Long[] getChavesRotas() {
		return chavesRotas;
	}

	@Override
	public void setChavesRotas(Long[] chavesRotas) {
		this.chavesRotas = chavesRotas;
	}

	@Override
	public Integer getDiasAtrasoInicial() {
		return diasAtrasoInicial;
	}

	@Override
	public void setDiasAtrasoInicial(Integer diasAtrasoInicial) {
		this.diasAtrasoInicial = diasAtrasoInicial;
	}

	
	@Override
	public Integer getDiasAtrasoFim() {
		return diasAtrasoFim;
	}

	@Override
	public void setDiasAtrasoFim(Integer diasAtrasoFim) {
		this.diasAtrasoFim = diasAtrasoFim;
	}

	
}
