package br.com.ggas.cobranca.negativacao;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ClienteNegativacao extends EntidadeNegocio {

	String BEAN_ID_CLIENTE_NEGATIVACAO= "clienteNegativacao";
	
	/**
	 * @return ComandoNegativacao - {@link ComandoNegativacao}
	 */
	ComandoNegativacao getComando();

	/**
	 * @param comando - {@link ComandoNegativacao}
	 */
	void setComando(ComandoNegativacao comando);

	/**
	 * @return Cliente - {@link Cliente}
	 */
	Cliente getCliente();

	/**
	 * @param cliente - {@link Cliente}
	 */
	void setCliente(Cliente cliente);
	
	/**
	 * @return Date - {@link Date}
	 */
	Date getDataNotificacao();
	
	/**
	 * @param Date - {@link Date}
	 */
	void setDataNotificacao(Date dataNotificacao);
		
	/**
	 * @param listaFaturasNegativacao - {@link Collection<FaturaClienteNegativacao>}
	 */
	Collection<FaturaClienteNegativacao> getListaFaturasNegativacao();

	/**
	 * return listaFaturasNegativacao - {@link Collection<FaturaClienteNegativacao>}
	 */
	void setListaFaturasNegativacao(Collection<FaturaClienteNegativacao> listaFaturasNegativacao);

}
