/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.ValorParcela;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.DocumentoCobrancaVO;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.web.cobranca.acaocobranca.SubRelatorioFaturasVencidasCobrancaVO;

/**
 * The Interface ControladorCobranca.
 */
public interface ControladorCobranca extends ControladorNegocio {

	/** The bean id controlador cobranca. */
	String BEAN_ID_CONTROLADOR_COBRANCA = "controladorCobranca";

	/** The erro negocio campos obrigatorios pesquisa. */
	String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA_EXTRATO_DEBITO";

	/** The declaracao quitacao anual ano. */
	String DECLARACAO_QUITACAO_ANUAL_ANO = "DECLARACAO_QUITACAO_ANUAL_ANO";

	/** The sucesso gerar declaracao quitacao anual. */
	String SUCESSO_GERAR_DECLARACAO_QUITACAO_ANUAL = "SUCESSO_GERAR_DECLARACAO_QUITACAO_ANUAL";

	/** The erro negocio valor credito maior debito. */
	String ERRO_NEGOCIO_VALOR_CREDITO_MAIOR_DEBITO = "ERRO_NEGOCIO_VALOR_CREDITO_MAIOR_DEBITO";

	/** The erro negocio arrecadador contrato convenio. */
	String ERRO_NEGOCIO_ARRECADADOR_CONTRATO_CONVENIO = "ERRO_NEGOCIO_ARRECADADOR_CONTRATO_CONVENIO";

	/** The erro negocio faturas nao encontradas ano selecionado. */
	String ERRO_NEGOCIO_FATURAS_NAO_ENCONTRADAS_ANO_SELECIONADO = "ERRO_NEGOCIO_FATURAS_NAO_ENCONTRADAS_ANO_SELECIONADO";

	/** The erro negocio faturas pendentes ano anterior. */
	String ERRO_NEGOCIO_FATURAS_PENDENTES_ANO_ANTERIOR = "ERRO_NEGOCIO_FATURAS_PENDENTES_ANO_ANTERIOR";

	/** The erro negocio geracao codigo barras. */
	String ERRO_NEGOCIO_GERACAO_CODIGO_BARRAS = "ERRO_NEGOCIO_GERACAO_CODIGO_BARRAS";

	/** The erro negocio carteira inexistente. */
	String ERRO_NEGOCIO_CARTEIRA_INEXISTENTE = "ERRO_NEGOCIO_CARTEIRA_INEXISTENTE";

	/** The erro negocio convenio sem conta. */
	String ERRO_NEGOCIO_CONVENIO_SEM_CONTA = "ERRO_NEGOCIO_CONVENIO_SEM_CONTA";

	/** The erro negocio arrecadador sem banco. */
	String ERRO_NEGOCIO_ARRECADADOR_SEM_BANCO = "ERRO_NEGOCIO_ARRECADADOR_SEM_BANCO";

	/**
	 * Método responsável por criar um documento
	 * de cobrancaç.
	 * 
	 * @return Um DocumentoCobranca.
	 */
	EntidadeNegocio criarDocumentoCobranca();

	/**
	 * Método responsável por obter o nome da
	 * classe documento de cobrança.
	 * 
	 * @return A classe Documento Cobrança.
	 */
	Class<?> getClasseEntidadeDocumentoCobranca();

	/**
	 * Método responsável por criar um item de
	 * documento de cobrança.
	 * 
	 * @return Um DocumentoCobrancaItem.
	 */
	EntidadeNegocio criarDocumentoCobrancaItem();

	/**
	 * Método responsável por criar um VO Valor
	 * Parcela.
	 * 
	 * @return Um ValorParcela.
	 */
	Object criarValorParcela();

	/**
	 * Método responsável por obter um tipo de
	 * documento.
	 * 
	 * @param chavePrimaria
	 *            Chave primária do tipo de
	 *            documento.
	 * @return Um TipoDocumento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoDocumento obterTipoDocumento(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um
	 * CreditoDebitoARealizar.
	 * 
	 * @param chavePrimaria
	 *            Chave primária do
	 *            CreditoDebitoARealizar.
	 * @return Um CreditoDebitoARealizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CreditoDebitoARealizar obterCreditoDebitoARealizar(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável para calcular o valor
	 * das parcelas do PRICE.
	 *
	 * @param valor the valor
	 * @param numeroPrestacoes the numero prestacoes
	 * @param taxaJuros the taxa juros
	 * @param dataInicial the data inicial
	 * @param periodicidade the periodicidade
	 * @param periodicidadeJuros the periodicidade juros
	 * @param isParcelamento the is parcelamento
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<ValorParcela> calcularPrice(BigDecimal valor, int numeroPrestacoes, BigDecimal taxaJuros, Date dataInicial, Integer periodicidade,
					Integer periodicidadeJuros, boolean isParcelamento) throws NegocioException;

	/**
	 * Calcular sac.
	 *
	 * @param valor the valor
	 * @param parcelas the parcelas
	 * @param taxaJuros the taxa juros
	 * @param dataInicial the data inicial
	 * @param periodicidade the periodicidade
	 * @param periodicidadeJuros the periodicidade juros
	 * @param isParcelamento the is parcelamento
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<ValorParcela> calcularSAC(BigDecimal valor, int parcelas, BigDecimal taxaJuros, Date dataInicial, Integer periodicidade,
					Integer periodicidadeJuros, boolean isParcelamento) throws NegocioException;

	/**
	 * Calcular sac.
	 *
	 * @param valor the valor
	 * @param parcelas the parcelas
	 * @param taxaJuros the taxa juros
	 * @param dataInicial the data inicial
	 * @param periodicidade the periodicidade
	 * @param periodicidadeJuros the periodicidade juros
	 * @param isParcelamento the is parcelamento
	 * @param isJurosCalculados the is juros calculados
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<ValorParcela> calcularSAC(BigDecimal valor, int parcelas, BigDecimal taxaJuros, Date dataInicial, Integer periodicidade,
					Integer periodicidadeJuros, boolean isParcelamento, boolean isJurosCalculados) throws NegocioException;

	/**
	 * Método responsável por consultar as faturas
	 * de um cliente ou de um ponto de consumo com
	 * situação de creditoDebito normal,
	 * retificada ou incluída e com situação de
	 * pagamento pendente ou parcialmente paga.
	 * 
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @return Uma coleção de Fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturasClientePontoConsumo(Long idCliente, Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por consultar as faturas
	 * ABERTAS de um cliente ou de um ponto de
	 * consumo que sejam do tipo FATURA ou NOTA DE
	 * DEBITO.
	 *
	 * @param idCliente Chave primária do cliente.
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param quitada the quitada
	 * @return Uma coleção de Fatura.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturasAbertasClientePontoConsumo(Long idCliente, Long idPontoConsumo, Boolean quitada)
					throws NegocioException;

	/**
	 * Método responsável por inserir um documento
	 * de cobrança.
	 *
	 * @param chavesFatura Chaves primárias das faturas.
	 * @param chavesCreditoDebito Chaves primárias dos créditos e
	 *            débitos.
	 * @param idCliente Chave primária do cliente.
	 * @param documentoCobranca Documento de cobrança a ser
	 *            populado e inserido.
	 * @param indicadorExtratoDebito Indica se o relatório é de
	 *            extrato de débito.
	 * @param idTipoDocumento the id tipo documento
	 * @return Um array com o documento de
	 *         cobrança de código de barras e o
	 *         documento de cobrança de boleto
	 *         bancário.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DocumentoCobranca[] inserirDocumentoCobranca(Long[] chavesFatura, Long[] chavesCreditoDebito, Long idCliente,
					DocumentoCobranca documentoCobranca, boolean indicadorExtratoDebito, Long idTipoDocumento) throws NegocioException;

	/**
	 * Método responsável por validar se algum
	 * filtro foi selecionado para a pesquisa.
	 * 
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @throws NegocioException
	 *             Caso o usuário não tenha
	 *             selecionado algum filtro para a
	 *             pesquisa.
	 */
	void validarFiltroPesquisaExtratoDebito(Long idCliente, Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter a soma do
	 * valor dos recebimentos de uma fatura.
	 * 
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return A soma do valor dos recebimentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal obterValorRecebimentoPelaFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por obter a Ultima Data
	 * dos recebimentos de uma fatura.
	 * 
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return A soma do valor dos recebimentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date obterUltimaDataRecebimentoPelaFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por obter os recebimentos
	 * classificados da fatura.
	 * 
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return Os recebimentos
	 *         classificados da fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Recebimento> obterRecebimentoClassificadoPelaFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por obter a Ultima Data
	 * das Conciliações de uma fatura.
	 * 
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return A soma do valor dos recebimentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date obterUltimaDataConciliacaoPelaFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por obter o documento de
	 * cobrança.
	 *
	 * @param chavePrimaria Chave primária do documento de
	 *            cobrança.
	 * @return O documento de cobrança.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DocumentoCobranca obterDocumentoCobranca(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o documento de
	 * cobrança.
	 *
	 * @param chavePrimaria Chave primária do documento de
	 *            cobrança.
	 * @return O documento de cobrança.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DocumentoCobrancaItem obterDocumentoCobrancaItem(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por consultar as Faturas
	 * do ano do cliente ou do ponto de consumo
	 * informado.
	 * 
	 * @param idCliente
	 *            A Chave primária do cliente.
	 * @param idPontoConsumo
	 *            A chave primaria do ponto
	 *            consumo
	 * @param anoFatura
	 *            O ano das faturas
	 * @param chaveSituacaoPagamento
	 *            A chave primaria da Entidade
	 *            Conteudo referente a situação do
	 *            pagamento
	 * @param chaveTipoDocumento
	 *            Array informando os tipos de
	 *            documentos das faturas
	 *            pesquisadas
	 * @param anoAtual
	 *            Informa se a pesquisa é feita no
	 *            ano corrido ou em anos
	 *            anteriores
	 * @return Uma Coleção de faturas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturasPorClienteOuPontoConsumo(Long idCliente, Long idPontoConsumo, String anoFatura,
					Long chaveSituacaoPagamento, Long[] chaveTipoDocumento, Boolean anoAtual) throws NegocioException;

	/**
	 * Método responsável por obter a fatura pela
	 * fatura geral.
	 * 
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return Uma FaturaGeral.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	FaturaGeral obterFaturaGeralPelaFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por obter as faturas
	 * pelas chaves primárias.
	 * 
	 * @param chavesFatura
	 *            Chave primária das faturas.
	 * @return Uma coleção de Fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturas(Long[] chavesFatura) throws NegocioException;

	/**
	 * Método responsável por gerar relatório de
	 * declaração de quitação anual para um ponto
	 * de consumo, para
	 * todos os pontos de consumo de um cliente ou
	 * todos os pontos de consumo de um contrato.
	 *
	 * @param anoFaturas Ano para geração da declaração.
	 * @param idCliente Chave primária do cliente.
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param numeroContrato Número do contrato.
	 * @return Um array de bytes do relatório.
	 * @throws GGASException the GGAS exception
	 */
	byte[] gerarRelatorioDeclaracaoQuitacaoAnual(String anoFaturas, Long idCliente, Long idPontoConsumo, String numeroContrato)
					throws GGASException;

	/**
	 * Método responsável por validar o filtro da
	 * pesquisa para geração de declaração de
	 * quitação anual.
	 *
	 * @param anoFaturas Ano para geração das faturas.
	 * @param idCliente Chave primária do cliente.
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param numeroContrato Número do contrato.
	 * @param indicadorGeral the indicador geral
	 * @throws NegocioException Caso o usuário não informe o
	 *             ano para geração da declaração
	 *             ou caso
	 *             ele não preencha algum dos
	 *             campos da pesquisa.
	 */
	void validarFiltroDeclaracaoQuitacaoAnual(String anoFaturas, Long idCliente, Long idPontoConsumo, String numeroContrato,
					Boolean indicadorGeral) throws NegocioException;

	/**
	 * Método responsável por obter a situação do
	 * crédito débito pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave Primária do débito
	 *            crédito.
	 * @return CreditoDebitoSituacao A entidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CreditoDebitoSituacao obterCreditoDebitoSituacao(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o banco
	 * utilizado pela empresa para fazer a
	 * cobrança.
	 * 
	 * @param chavePrimaria
	 *            Chave primária do banco.
	 * @return Um banco.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Banco obterBanco(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por gerar relátorio de
	 * declaração de quitação anual para vários
	 * clientes.
	 *
	 * @param clientes Lista de clientes para gerar
	 *            relatório.
	 * @param anoGeracao Ano para geração do relatório.
	 * @param indicadorPago Indicador para gerar relatório
	 *            de declaração de quitação ou de
	 *            faturas em aberto.
	 * @param qtdClientes Quantidade de clientes por
	 *            relatório.
	 * @param parametrosConsulta Parâmetros do sistema usados nas
	 *            consultas.
	 * @return Um array de bytes do relatório.
	 * @throws GGASException the GGAS exception
	 */
	byte[] gerarRelatorioDeclaracaoQuitacaoAnualGeral(Collection<Cliente> clientes, String anoGeracao, boolean indicadorPago,
					Integer qtdClientes, Map<String, Long> parametrosConsulta) throws GGASException;

	/**
	 * Método responsável por gerar relatório de
	 * débitos em aberto para os anos anteriores
	 * ao selecionado.
	 *
	 * @param listaClientes Lista de cliente para gerar
	 *            relatório.
	 * @param anoGeracao Ano para geração do relatório.
	 * @param parametrosConsulta Parâmetros do sistemas usados
	 *            nas consultas das faturas.
	 * @throws GGASException the GGAS exception
	 */
	void
					gerarRelatorioDebitosAnosAnterioresLote(Collection<Cliente> listaClientes, String anoGeracao,
									Map<String, Long> parametrosConsulta) throws GGASException;

	/**
	 * Obter fatura.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesEager the propriedades eager
	 * @return the entidade negocio
	 * @throws NegocioException the negocio exception
	 */
	EntidadeNegocio obterFatura(Long chavePrimaria, String... propriedadesEager) throws NegocioException;

	/**
	 * Método responsável por gerar o relatório de
	 * boleto bancário.
	 *
	 * @param documentoCobranca O documento de cobrança.
	 * @param nossoNumero Nosso número gerado na inclusão
	 *            do documento de cobrança.
	 * @param arrecadadorContratoConvenio ArrecadadorContratoConvenio
	 *            usado para a geração do boleto
	 *            bancário.
	 * @param arquivoRelatorio the arquivo relatorio
	 * @param isSegundaVia indica se o relatório de nota de
	 *            débito ou crédito a ser gerado é
	 *            segunda via.
	 * @param fatura the fatura
	 * @return Um array de bytes do relatório de
	 *         boleto bancário.
	 * @throws GGASException the GGAS exception
	 */
	byte[] gerarRelatorioBoletoBancario(DocumentoCobranca documentoCobranca, String nossoNumero,
					ArrecadadorContratoConvenio arrecadadorContratoConvenio, String arquivoRelatorio, boolean isSegundaVia, Fatura fatura)
					throws GGASException;

	/**
	 * Método responsável por atualizar a fatura.
	 *
	 * @param faturaAtual A fatura que será atualizada
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void atualizarFatura(Fatura faturaAtual) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por atualizar o credito
	 * débito a realizar.
	 *
	 * @param creditoDebitoARealizar o credito débito
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void atualizarCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável pelo cálculo do valor
	 * presente.
	 *
	 * @param creditoDebito the credito debito
	 * @param qtdeParcelasCobradas the qtde parcelas cobradas
	 * @param qtdeParcelasAntecipadas the qtde parcelas antecipadas
	 * @return O valor presente
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal calcularValorPresente(CreditoDebitoARealizar creditoDebito, Integer qtdeParcelasCobradas, Integer qtdeParcelasAntecipadas)
					throws NegocioException;

	/**
	 * Obter cobranca debito situacao.
	 *
	 * @param idCobrancaDebitoSituacao the id cobranca debito situacao
	 * @return the entidade negocio
	 * @throws NegocioException the negocio exception
	 */
	EntidadeNegocio obterCobrancaDebitoSituacao(Long idCobrancaDebitoSituacao) throws NegocioException;

	/**
	 * Método responsável por consultar as faturas
	 * dos clientes de acordo o ano de vencimento
	 * da fatura, a situação de pagamento
	 * e o tipo da fatura.
	 * 
	 * @param chavesCliente
	 *            Chaves primárias do clientes.
	 * @param anoFatura
	 *            Ano de vencimento da fatura.
	 * @param chavesSituacaoPagamento
	 *            Chaves primárias das situações
	 *            de pagamento.
	 * @param chavesTipoDocumento
	 *            Chaves primárias dos tipo de
	 *            documento (fatura, nota de
	 *            débito).
	 * @param anoAtual
	 *            Indicador para consultar as
	 *            faturas com vencimento no ano
	 *            selecionado ou em anos
	 *            anteriores ao selecionado.
	 * @return Uma coleção de fatuas.
	 * @throws NegocioException
	 *             Caso ocorra algume erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturasPorClientes(Long[] chavesCliente, String anoFatura, Long[] chavesSituacaoPagamento,
					Long[] chavesTipoDocumento, boolean anoAtual) throws NegocioException;

	/**
	 * Método responsável por obter a forma de
	 * cobrança do contrato pelo ponto de consumo.
	 * 
	 * @param pontoConsumo
	 *            Ponto de consumo do contrato.
	 * @param listaContratoPonto
	 *            Lista de contratoPontoConsumo.
	 * @return A chave primária da forma de
	 *         cobrança.
	 */
	Long obterIdFormaCobrancaDoContrato(PontoConsumo pontoConsumo, Collection<ContratoPontoConsumo> listaContratoPonto);

	/**
	 * Obter valor fatura.
	 *
	 * @param chaveFatura the chave fatura
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal obterValorFatura(Long chaveFatura) throws NegocioException;

	/**
	 * Método responsável por gerar um relátorio
	 * de extrato de débito.
	 *
	 * @param documentoCobranca Documento de cobrança gerado.
	 * @param arquivoJasper Nome do arquivo do relatório.
	 * @param isExtratoDebito Indicador se o relatório está
	 *            sendo gerado da tela de extrato
	 *            de débito ou não.
	 * @param isSegundaVia indica se o relatório de nota de
	 *            débito ou crédito a ser gerado é
	 *            segunda via.
	 * @param fatura the fatura
	 * @return Um array de bytes com o relatório
	 *         gerado.
	 * @throws GGASException the GGAS exception
	 */
	byte[] gerarRelatorioExtratoDebito(DocumentoCobranca documentoCobranca, String arquivoJasper, boolean isExtratoDebito,
					boolean isSegundaVia, Fatura fatura) throws GGASException;

	/**
	 * Método responsável por gerar os relatório
	 * de extrato de débito.
	 *
	 * @param chavesFatura Chaves primárias das faturas.
	 * @param chavesCreditoDebito Chaves primárias dos créditos e
	 *            débitos.
	 * @param idCliente Chave primária do cliente.
	 * @param documentoCobranca Documento de cobrança a ser
	 *            populado e inserido.
	 * @return Um array de bytes com o relatório
	 *         gerado.
	 * @throws GGASException the GGAS exception
	 */
	byte[] gerarExtratoDebito(Long[] chavesFatura, Long[] chavesCreditoDebito, Long idCliente, DocumentoCobranca documentoCobranca)
					throws GGASException;

	/**
	 * Método responsável por gerar relatório de
	 * extrato de débito e relatório de boleto
	 * bancário.
	 *
	 * @param documentoCobranca Documento de cobrança para
	 *            relatório de extrato de débito.
	 * @param documentoCobrancaBoleto Documento de cobrança para
	 *            relatório de boleto bancário.
	 * @param idCliente Chave primária do cliente.
	 * @param arquivoRelatorioCodigoBarras the arquivo relatorio codigo barras
	 * @param arquivoRelatorioBoleto the arquivo relatorio boleto
	 * @param isSegundaVia indica se o relatório de nota de
	 *            débito ou crédito a ser gerado é
	 *            segunda via.
	 * @param fatura the fatura
	 * @return Um array de bytes com os relátorio
	 *         concatenados, caso haja mais de um.
	 * @throws GGASException the GGAS exception
	 */
	byte[] gerarRelatoriosDebito(DocumentoCobranca documentoCobranca, DocumentoCobranca documentoCobrancaBoleto, Long idCliente,
					String arquivoRelatorioCodigoBarras, String arquivoRelatorioBoleto, boolean isSegundaVia, Fatura fatura)
					throws GGASException;

	/**
	 * Método responsável por gerar um número que
	 * alimenta um código de barras.
	 *
	 * @param documentoCobranca Documento de Cobrança utilizado
	 *            para gerar o código de barras
	 * @param acumudado the acumudado
	 * @return Mapa com os números para gerar o
	 *         relatório o os números formatados
	 *         para exibição
	 * @throws NegocioException caso ocorra algum erro
	 */
	Map<String, Object> gerarNumeroCodigoBarra(DocumentoCobranca documentoCobranca, BigDecimal acumudado) throws NegocioException;

	/**
	 * Método responsável por popular o relatório
	 * de boleto bancário.
	 *
	 * @return Collection<CobrancaDebitoSituacao>
	 * @throws NegocioException caso ocorra algum erro
	 */
	Collection<CobrancaDebitoSituacao> listarCobrancaDebitoSituacao() throws NegocioException;

	/**
	 * Consultar documento cobranca item.
	 *
	 * @param filtro the filtro
	 * @param ordenacao the ordenacao
	 * @param propriedadesLazy the propriedades lazy
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<DocumentoCobrancaItem> consultarDocumentoCobrancaItem(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException;

	/**
	 * Listar documento cobranca.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<DocumentoCobranca> listarDocumentoCobranca(Map<String, Object> filtro);

	/**
	 * Processar emissao aviso corte batch.
	 *
	 * @param logProcessamento the log processamento
	 * @param acaoComando the acao comando
	 * @return the map
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 */
	Map<Cliente, byte[]> processarEmissaoAvisoCorteBatch(StringBuilder logProcessamento, AcaoComando acaoComando) 
			throws IOException, GGASException;

	/**
	 * Processar emissao notificacao corte batch.
	 *
	 * @param logProcessamento the log processamento
	 * @param acaoComando the acao comando
	 * @return the map
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 */
	Map<Cliente, byte[]> processarEmissaoNotificacaoCorteBatch(StringBuilder logProcessamento, AcaoComando acaoComando) 
			throws IOException, GGASException;

	/**
	 * Calcular valor total debitos.
	 *
	 * @param chavesFatura the chaves fatura
	 * @param chavesCreditoDebito the chaves credito debito
	 * @param data the data
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	BigDecimal calcularValorTotalDebitos(Long[] chavesFatura, Long[] chavesCreditoDebito, Date data) throws GGASException;

	/**
	 * Consultar declaracao quitacao.
	 *
	 * @param codigoValidacao the codigo validacao
	 * @param idCliente the id cliente
	 * @param anoReferencia the ano referencia
	 * @return the byte[]
	 */
	byte[] consultarDeclaracaoQuitacao(String codigoValidacao, Long idCliente, Long anoReferencia);

	/**
	 * Gerar relatorio declaracao quitacao anual geral.
	 *
	 * @param idCliente the id cliente
	 * @param anoReferencia the ano referencia
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	public byte[] gerarRelatorioDeclaracaoQuitacaoAnualGeral(Long idCliente, Long anoReferencia) throws GGASException;

	/**
	 * Consultar faturas nao pagas cliente.
	 *
	 * @param chaveCliente the chave cliente
	 * @param anoFaturas the ano faturas
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Fatura> consultarFaturasNaoPagasCliente(Long chaveCliente, String anoFaturas) throws NegocioException;

	/**
	 * Consulta uma coleção de entidades do tipo {@link DocumentoCobrancaItem} que possuem uma entidade do tipo {@link FaturaGeral}, da
	 * lista de entidades passada por parâmetro.
	 * 
	 * @param faturasGerais - {@link List}
	 * @return coleção de {@link DocumentoCobrancaItem} por chave primária de {@link FaturaGeral}
	 */
	Map<Long, Collection<DocumentoCobrancaItem>> consultarDocumentoCobrancaItemPorFaturaGeral(List<FaturaGeral> faturasGerais);

	/**
	 * Método responsável por validar se um dos filtros foi selecionado (Ponto de Consumo ou Cliente)
	 * 
	 * @param idCliente {@link Long}
	 * @param idPontoConsumo {@link Long}
	 * @param idImovel {@link Long}
	 * @throws NegocioException {@link NegocioException}
	 */
	void validarExibirExtratoDebito(Long idCliente, Long idPontoConsumo, Long idImovel) throws NegocioException;

	/**
	 * Método responsável por listar as cobrancas dos clientes
	 * 
	 * @param cliente {@link Object}

	 * @return Lista de DocumentoCobrancaVO
	 */
	List<DocumentoCobrancaVO> listarCobrancasPorCliente(Cliente cliente);

	/**
	 * Método responsável por listar os recebimentos passando como parametro a cobranca
	 * 
	 * @param cliente {@link Object}
	 * @param documentoCobrancaVO the Documento Cobranca VO
	 * @return Lista de DocumentoCobrancaVO
	 */
	List<DocumentoCobrancaVO> listarRecebimentosPorCobranca(DocumentoCobrancaVO documentoCobrancaVO);
	
	/**
	 * Método responsável por Obter a fatura por Documento de Cobranca Item
	 * @param documentoCobrancaVO {@link Object}
	 * @return DocumentoCobrancaVO
	 */
	DocumentoCobrancaVO obterFaturaPorDocCobrancaItem(DocumentoCobrancaVO documentoCobrancaVO);

	/**
	 * Montar vo fatura vencida.
	 *
	 * @param fatura the fatura
	 * @param faturasComponentes the faturas componentes
	 * @return the sub relatorio aviso corte faturas vencidas vo
	 * @throws GGASException the GGAS exception
	 */
	SubRelatorioFaturasVencidasCobrancaVO montarVOFaturaVencida(Fatura fatura, Collection<Fatura> faturasComponentes)
			throws GGASException;

	byte[] gerarRelatorioCreditoDebitoRealizar(DocumentoCobranca documentoCobranca, String arquivoJasper,
			boolean isExtratoDebito, boolean isSegundaVia, CreditoDebitoNegociado creditoNegociado)
			throws GGASException;

}
