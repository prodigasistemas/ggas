/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
/**
 * Classe responsável pela representação dos itens de parcela 
 *
 */
public class ItemParcelaVO implements Serializable {

	private static final long serialVersionUID = -2196394615489865077L;

	private LancamentoItemContabil lancamentoItemContabil;

	private BigDecimal valor;

	private Set<FaturaItem> listaFaturas = new HashSet<>();

	private Set<CreditoDebitoARealizar> listaCreditoDebito = new HashSet<>();

	/**
	 * @return the lancamentoItemContabil
	 */
	public LancamentoItemContabil getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	/**
	 * @param lancamentoItemContabil
	 *            the lancamentoItemContabil to
	 *            set
	 */
	public void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * @return the listaFaturas
	 */
	public Set<FaturaItem> getListaFaturas() {

		return listaFaturas;
	}

	/**
	 * @param listaFaturas
	 *            the listaFaturas to set
	 */
	public void setListaFaturas(Set<FaturaItem> listaFaturas) {

		this.listaFaturas = listaFaturas;
	}

	/**
	 * @return the listaCreditoDebito
	 */
	public Set<CreditoDebitoARealizar> getListaCreditoDebito() {

		return listaCreditoDebito;
	}

	/**
	 * @param listaCreditoDebito
	 *            the listaCreditoDebito to set
	 */
	public void setListaCreditoDebito(Set<CreditoDebitoARealizar> listaCreditoDebito) {

		this.listaCreditoDebito = listaCreditoDebito;
	}

}
