/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.io.Serializable;

/**
 * Interface responsável pela assinatura dos métodos relacionados aos dados de relatório confissão de dívidas. 
 *
 */
public interface DadosRelatorioConfissaoDividas extends Serializable {

	String BEAN_ID_DADOS_RELATORIO_CONFISSAO_DIVIDAS = "DadosRelatorioConfissaoDividas";

	/**
	 * Gets the cnpj empresa.
	 *
	 * @return the cnpjEmpresa
	 */
	String getCnpjEmpresa();

	/**
	 * Sets the cnpj empresa.
	 *
	 * @param cnpjEmpresa            the cnpjEmpresa to set
	 */
	void setCnpjEmpresa(String cnpjEmpresa);

	/**
	 * Gets the nome cliente.
	 *
	 * @return the nomeCliente
	 */
	String getNomeCliente();

	/**
	 * Sets the nome cliente.
	 *
	 * @param nomeCliente            the nomeCliente to set
	 */
	void setNomeCliente(String nomeCliente);

	/**
	 * Gets the gets the endereco.
	 *
	 * @return the endereco
	 */
	String getGetEndereco();

	/**
	 * Sets the gets the endereco.
	 *
	 * @param getEndereco the new gets the endereco
	 */
	void setGetEndereco(String getEndereco);

	/**
	 * Gets the cpf cnpj cliente.
	 *
	 * @return the cpfCnpjCliente
	 */
	String getCpfCnpjCliente();

	/**
	 * Sets the cpf cnpj cliente.
	 *
	 * @param cpfCnpjCliente            the cpfCnpjCliente set
	 */
	void setCpfCnpjCliente(String cpfCnpjCliente);

	/**
	 * Gets the total debitos.
	 *
	 * @return the totalDebitos
	 */
	String getTotalDebitos();

	/**
	 * Sets the total debitos.
	 *
	 * @param totalDebitos the new total debitos
	 */
	void setTotalDebitos(String totalDebitos);

	/**
	 * Gets the total debitos extenso.
	 *
	 * @return the total debitos extenso
	 */
	String getTotalDebitosExtenso();

	/**
	 * Sets the total debitos extenso.
	 *
	 * @param totalDebitosExtenso the new total debitos extenso
	 */
	void setTotalDebitosExtenso(String totalDebitosExtenso);

	/**
	 * Gets the valor entrada.
	 *
	 * @return the valor entrada
	 */
	String getValorEntrada();

	/**
	 * Sets the valor entrada.
	 *
	 * @param valorEntrada the new valor entrada
	 */
	void setValorEntrada(String valorEntrada);

	/**
	 * Gets the valor entrada extenso.
	 *
	 * @return the valor entrada extenso
	 */
	String getValorEntradaExtenso();

	/**
	 * Sets the valor entrada extenso.
	 *
	 * @param valorEntradaExtenso the new valor entrada extenso
	 */
	void setValorEntradaExtenso(String valorEntradaExtenso);

	/**
	 * Gets the data parcelamento.
	 *
	 * @return the data parcelamento
	 */
	String getDataParcelamento();

	/**
	 * Sets the data parcelamento.
	 *
	 * @param dataParcelamento the new data parcelamento
	 */
	void setDataParcelamento(String dataParcelamento);

	/**
	 * Gets the numero parcelas.
	 *
	 * @return the numero parcelas
	 */
	String getNumeroParcelas();

	/**
	 * Sets the numero parcelas.
	 *
	 * @param numeroParcelas the new numero parcelas
	 */
	void setNumeroParcelas(String numeroParcelas);

	/**
	 * Gets the numero parcelas extenso.
	 *
	 * @return the numero parcelas extenso
	 */
	String getNumeroParcelasExtenso();

	/**
	 * Sets the numero parcelas extenso.
	 *
	 * @param numeroParcelasExtenso the new numero parcelas extenso
	 */
	void setNumeroParcelasExtenso(String numeroParcelasExtenso);

	/**
	 * Gets the valor parcela.
	 *
	 * @return the valor parcela
	 */
	String getValorParcela();

	/**
	 * Sets the valor parcela.
	 *
	 * @param valorParcela the new valor parcela
	 */
	void setValorParcela(String valorParcela);

	/**
	 * Gets the taxa multa.
	 *
	 * @return the taxa multa
	 */
	String getTaxaMulta();

	/**
	 * Sets the taxa multa.
	 *
	 * @param taxaMulta the new taxa multa
	 */
	void setTaxaMulta(String taxaMulta);

	/**
	 * Gets the taxa multa extenso.
	 *
	 * @return the taxa multa extenso
	 */
	String getTaxaMultaExtenso();

	/**
	 * Sets the taxa multa extenso.
	 *
	 * @param taxaMultaExtenso the new taxa multa extenso
	 */
	void setTaxaMultaExtenso(String taxaMultaExtenso);

	/**
	 * Gets the taxa juros mora.
	 *
	 * @return the taxa juros mora
	 */
	String getTaxaJurosMora();

	/**
	 * Sets the taxa juros mora.
	 *
	 * @param taxaJurosMora the new taxa juros mora
	 */
	void setTaxaJurosMora(String taxaJurosMora);

	/**
	 * Gets the taxa juros mora extenso.
	 *
	 * @return the taxa juros mora extenso
	 */
	String getTaxaJurosMoraExtenso();

	/**
	 * Sets the taxa juros mora extenso.
	 *
	 * @param taxaJurosMoraExtenso the new taxa juros mora extenso
	 */
	void setTaxaJurosMoraExtenso(String taxaJurosMoraExtenso);

	/**
	 * Gets the imagem.
	 *
	 * @return the imagem
	 */
	String getImagem();

	/**
	 * Sets the imagem.
	 *
	 * @param imagem the new imagem
	 */
	void setImagem(String imagem);

	/**
	 * Gets the endereco empresa.
	 *
	 * @return the endereco empresa
	 */
	String getEnderecoEmpresa();

	/**
	 * Sets the endereco empresa.
	 *
	 * @param enderecoEmpresa the new endereco empresa
	 */
	void setEnderecoEmpresa(String enderecoEmpresa);

	/**
	 * Gets the mes portugues.
	 *
	 * @return the mes portugues
	 */
	String getMesPortugues();

	/**
	 * Sets the mes portugues.
	 *
	 * @param mesPortugues the new mes portugues
	 */
	void setMesPortugues(String mesPortugues);

	/**
	 * Gets the nome empresa 1.
	 *
	 * @return the nome empresa 1
	 */
	String getNomeEmpresa1();

	/**
	 * Sets the nome empresa 1.
	 *
	 * @param nomeEmpresa1 the new nome empresa 1
	 */
	void setNomeEmpresa1(String nomeEmpresa1);

	/**
	 * Gets the nome empresa 2.
	 *
	 * @return the nome empresa 2
	 */
	String getNomeEmpresa2();

	/**
	 * Sets the nome empresa 2.
	 *
	 * @param nomeEmpresa2 the new nome empresa 2
	 */
	void setNomeEmpresa2(String nomeEmpresa2);

	/**
	 * Gets the nome empresa 3.
	 *
	 * @return the nome empresa 3
	 */
	String getNomeEmpresa3();

	/**
	 * Sets the nome empresa 3.
	 *
	 * @param nomeEmpresa3 the new nome empresa 3
	 */
	void setNomeEmpresa3(String nomeEmpresa3);

	/**
	 * Gets the nome empresa 4.
	 *
	 * @return the nome empresa 4
	 */
	String getNomeEmpresa4();

	/**
	 * Sets the nome empresa 4.
	 *
	 * @param nomeEmpresa4 the new nome empresa 4
	 */
	void setNomeEmpresa4(String nomeEmpresa4);

	/**
	 * Gets the imovel dia vencimento.
	 *
	 * @return the imovel dia vencimento
	 */
	String getImovelDiaVencimento();

	/**
	 * Sets the imovel dia vencimento.
	 *
	 * @param imovelDiaVencimento the new imovel dia vencimento
	 */
	void setImovelDiaVencimento(String imovelDiaVencimento);

	/**
	 * Gets the descricao localidade.
	 *
	 * @return the descricao localidade
	 */
	String getDescricaoLocalidade();

	/**
	 * Sets the descricao localidade.
	 *
	 * @param descricaoLocalidade the new descricao localidade
	 */
	void setDescricaoLocalidade(String descricaoLocalidade);

	/**
	 * Gets the colecao ano mes referencia.
	 *
	 * @return the colecao ano mes referencia
	 */
	String getColecaoAnoMesReferencia();

	/**
	 * Sets the colecao ano mes referencia.
	 *
	 * @param colecaoAnoMesReferencia the new colecao ano mes referencia
	 */
	void setColecaoAnoMesReferencia(String colecaoAnoMesReferencia);

	/**
	 * Gets the colecao ano mes referencia sobra.
	 *
	 * @return the colecao ano mes referencia sobra
	 */
	String getColecaoAnoMesReferenciaSobra();

	/**
	 * Sets the colecao ano mes referencia sobra.
	 *
	 * @param colecaoAnoMesReferenciaSobra the new colecao ano mes referencia sobra
	 */
	void setColecaoAnoMesReferenciaSobra(String colecaoAnoMesReferenciaSobra);

}
