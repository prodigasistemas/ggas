/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe <nome> representa uma <nome> no sistema.
 *
 * @since data atual
 * 
 */

package br.com.ggas.cobranca.parcelamento.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cobranca.parcelamento.DadosItemParcela;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;

class DadosParcelasImpl implements DadosParcelas {

	private static final long serialVersionUID = -6631075660312760512L;

	private Integer quantidadeParcelas;

	private Integer numeroParcela;

	private BigDecimal saldoInicial;

	private BigDecimal juros;

	private BigDecimal amortizacao;

	private BigDecimal total;

	private BigDecimal saldoFinal;

	private Date dataVencimento;

	private Date dataPagamento;

	private Collection<DadosItemParcela> listaItemParcela = new ArrayList<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.DadosParcelas
	 * #getNumeroParcela()
	 */
	@Override
	public Integer getNumeroParcela() {

		return numeroParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.DadosParcelas
	 * #setNumeroParcela(java.lang.Integer)
	 */
	@Override
	public void setNumeroParcela(Integer numeroParcela) {

		this.numeroParcela = numeroParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.DadosParcelas
	 * #getDataVencimento()
	 */
	@Override
	public Date getDataVencimento() {

		return dataVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.DadosParcelas
	 * #setDataVencimento(java.util.Date)
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas#getQuantidadeParcelas()
	 */
	@Override
	public Integer getQuantidadeParcelas() {

		return quantidadeParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas
	 * #setQuantidadeParcelas(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeParcelas(Integer quantidadeParcelas) {

		this.quantidadeParcelas = quantidadeParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas#getSaldoInicial()
	 */
	@Override
	public BigDecimal getSaldoInicial() {

		return saldoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas
	 * #setSaldoInicial(java.math.BigDecimal)
	 */
	@Override
	public void setSaldoInicial(BigDecimal saldoInicial) {

		this.saldoInicial = saldoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas#getJuros()
	 */
	@Override
	public BigDecimal getJuros() {

		return juros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas
	 * #setJuros(java.math.BigDecimal)
	 */
	@Override
	public void setJuros(BigDecimal juros) {

		this.juros = juros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas#getAmortizacao()
	 */
	@Override
	public BigDecimal getAmortizacao() {

		return amortizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas
	 * #setAmortizacao(java.math.BigDecimal)
	 */
	@Override
	public void setAmortizacao(BigDecimal amortizacao) {

		this.amortizacao = amortizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas#getTotal()
	 */
	@Override
	public BigDecimal getTotal() {

		return total;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas
	 * #setTotal(java.math.BigDecimal)
	 */
	@Override
	public void setTotal(BigDecimal total) {

		this.total = total;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas#getSaldoFinal()
	 */
	@Override
	public BigDecimal getSaldoFinal() {

		return saldoFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelas
	 * #setSaldoFinal(java.math.BigDecimal)
	 */
	@Override
	public void setSaldoFinal(BigDecimal saldoFinal) {

		this.saldoFinal = saldoFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.DadosParcelas
	 * #getDataPagamento()
	 */
	@Override
	public Date getDataPagamento() {

		return dataPagamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.DadosParcelas
	 * #setDataPagamento(java.util.Date)
	 */
	@Override
	public void setDataPagamento(Date dataPagamento) {

		this.dataPagamento = dataPagamento;
	}

	/**
	 * @return the listaItemParcela
	 */
	@Override
	public Collection<DadosItemParcela> getListaItemParcela() {

		return listaItemParcela;
	}

	/**
	 * @param listaItemParcela
	 *            the listaItemParcela to set
	 */
	@Override
	public void setListaItemParcela(Collection<DadosItemParcela> listaItemParcela) {

		this.listaItemParcela = listaItemParcela;
	}

}
