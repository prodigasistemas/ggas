/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe DadosParcelamento representa uma DadosParcelamento no sistema.
 *
 * @since 05/04/2010
 * 
 */

package br.com.ggas.cobranca.parcelamento.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.parcelamento.DadosParcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;

/**
 * 
 *
 */
class DadosParcelamentoImpl implements DadosParcelamento {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -9032112682182604325L;

	private PontoConsumo pontoConsumo;

	private boolean bloquearCobrancaFatura;

	private Collection<Map<String, Object>> listaFatura = new ArrayList<>();

	private Collection<CreditoDebitoARealizar> listaCreditoDebito = new ArrayList<>();

	private Collection<ParcelamentoPerfil> listaPerfil = new ArrayList<>();

	/**
	 * @return the bloquearCobrancaFatura
	 */
	@Override
	public boolean isBloquearCobrancaFatura() {

		return bloquearCobrancaFatura;
	}

	/**
	 * @param bloquearCobrancaFatura
	 *            the bloquearCobrancaFatura to
	 *            set
	 */
	@Override
	public void setBloquearCobrancaFatura(boolean bloquearCobrancaFatura) {

		this.bloquearCobrancaFatura = bloquearCobrancaFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento
	 * #setPontoConsumo(br.com.ggas
	 * .cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento#getListaFatura()
	 */
	@Override
	public Collection<Map<String, Object>> getListaFatura() {

		return listaFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento
	 * #setListaFatura(java.util.Collection)
	 */
	@Override
	public void setListaFatura(Collection<Map<String, Object>> listaFatura) {

		this.listaFatura = listaFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento#getListaCreditoDebito()
	 */
	@Override
	public Collection<CreditoDebitoARealizar> getListaCreditoDebito() {

		return listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento
	 * #setListaCreditoDebito(java
	 * .util.Collection)
	 */
	@Override
	public void setListaCreditoDebito(Collection<CreditoDebitoARealizar> listaCreditoDebito) {

		this.listaCreditoDebito = listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento#getListaPerfil()
	 */
	@Override
	public Collection<ParcelamentoPerfil> getListaPerfil() {

		return listaPerfil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosParcelamento
	 * #setListaPerfil(java.util.Collection)
	 */
	@Override
	public void setListaPerfil(Collection<ParcelamentoPerfil> listaPerfil) {

		this.listaPerfil = listaPerfil;
	}

}
