/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.io.Serializable;

/**
 * Interface responsável pela assinatura dos métodos relacionados aos dados do relatório de nota promissória.  
 *
 */
public interface DadosRelatorioNotaPromissoria extends Serializable {

	String BEAN_ID_DADOS_RELATORIO_NOTA_PROMISSORIA = "DadosRelatorioNotaPromissoria";

	/**
	 * Gets the param codigo nota.
	 *
	 * @return the param codigo nota
	 */
	String getParamCodigoNota();

	/**
	 * Sets the param codigo nota.
	 *
	 * @param paramCodigoNota the new param codigo nota
	 */
	void setParamCodigoNota(String paramCodigoNota);

	/**
	 * Gets the param ano.
	 *
	 * @return the param ano
	 */
	String getParamAno();

	/**
	 * Sets the param ano.
	 *
	 * @param paramAno the new param ano
	 */
	void setParamAno(String paramAno);

	/**
	 * Gets the param total prestacoes.
	 *
	 * @return the param total prestacoes
	 */
	String getParamTotalPrestacoes();

	/**
	 * Sets the param total prestacoes.
	 *
	 * @param paramTotalPrestacoes the new param total prestacoes
	 */
	void setParamTotalPrestacoes(String paramTotalPrestacoes);

	/**
	 * Gets the param data.
	 *
	 * @return the param data
	 */
	String getParamData();

	/**
	 * Sets the param data.
	 *
	 * @param paramData the new param data
	 */
	void setParamData(String paramData);

	/**
	 * Gets the param valor.
	 *
	 * @return the param valor
	 */
	String getParamValor();

	/**
	 * Sets the param valor.
	 *
	 * @param paramValor the new param valor
	 */
	void setParamValor(String paramValor);

	/**
	 * Gets the param dia extenso.
	 *
	 * @return the param dia extenso
	 */
	String getParamDiaExtenso();

	/**
	 * Sets the param dia extenso.
	 *
	 * @param paramDiaExtenso the new param dia extenso
	 */
	void setParamDiaExtenso(String paramDiaExtenso);

	/**
	 * Gets the param mes extenso.
	 *
	 * @return the param mes extenso
	 */
	String getParamMesExtenso();

	/**
	 * Sets the param mes extenso.
	 *
	 * @param paramMesExtenso the new param mes extenso
	 */
	void setParamMesExtenso(String paramMesExtenso);

	/**
	 * Gets the param nome empresa extenso.
	 *
	 * @return the param nome empresa extenso
	 */
	String getParamNomeEmpresaExtenso();

	/**
	 * Sets the param nome empresa extenso.
	 *
	 * @param paramNomeEmpresaExtenso the new param nome empresa extenso
	 */
	void setParamNomeEmpresaExtenso(String paramNomeEmpresaExtenso);

	/**
	 * Gets the param nome empresa abreviacao.
	 *
	 * @return the param nome empresa abreviacao
	 */
	String getParamNomeEmpresaAbreviacao();

	/**
	 * Sets the param nome empresa abreviacao.
	 *
	 * @param paramNomeEmpresaAbreviacao the new param nome empresa abreviacao
	 */
	void setParamNomeEmpresaAbreviacao(String paramNomeEmpresaAbreviacao);

	/**
	 * Gets the param CNPJ empresa.
	 *
	 * @return the param CNPJ empresa
	 */
	String getParamCNPJEmpresa();

	/**
	 * Sets the param CNPJ empresa.
	 *
	 * @param paramCNPJEmpresa the new param CNPJ empresa
	 */
	void setParamCNPJEmpresa(String paramCNPJEmpresa);

	/**
	 * Gets the param valor extenso 1.
	 *
	 * @return the param valor extenso 1
	 */
	String getParamValorExtenso1();

	/**
	 * Sets the param valor extenso 1.
	 *
	 * @param paramValorExtenso1 the new param valor extenso 1
	 */
	void setParamValorExtenso1(String paramValorExtenso1);

	/**
	 * Gets the param valor extenso 2.
	 *
	 * @return the param valor extenso 2
	 */
	String getParamValorExtenso2();

	/**
	 * Sets the param valor extenso 2.
	 *
	 * @param paramValorExtenso2 the new param valor extenso 2
	 */
	void setParamValorExtenso2(String paramValorExtenso2);

	/**
	 * Gets the param cidade.
	 *
	 * @return the param cidade
	 */
	String getParamCidade();

	/**
	 * Sets the param cidade.
	 *
	 * @param paramCidade the new param cidade
	 */
	void setParamCidade(String paramCidade);

	/**
	 * Gets the param estado.
	 *
	 * @return the param estado
	 */
	String getParamEstado();

	/**
	 * Sets the param estado.
	 *
	 * @param paramEstado the new param estado
	 */
	void setParamEstado(String paramEstado);

	/**
	 * Gets the param data atual extenso.
	 *
	 * @return the param data atual extenso
	 */
	String getParamDataAtualExtenso();

	/**
	 * Sets the param data atual extenso.
	 *
	 * @param paramDataAtualExtenso the new param data atual extenso
	 */
	void setParamDataAtualExtenso(String paramDataAtualExtenso);

	/**
	 * Gets the param nome cliente.
	 *
	 * @return the param nome cliente
	 */
	String getParamNomeCliente();

	/**
	 * Sets the param nome cliente.
	 *
	 * @param paramNomeCliente the new param nome cliente
	 */
	void setParamNomeCliente(String paramNomeCliente);

	/**
	 * Gets the param CNPJ cliente.
	 *
	 * @return the param CNPJ cliente
	 */
	String getParamCNPJCliente();

	/**
	 * Sets the param CNPJ cliente.
	 *
	 * @param paramCNPJCliente the new param CNPJ cliente
	 */
	void setParamCNPJCliente(String paramCNPJCliente);

	/**
	 * Gets the param endereco cliente.
	 *
	 * @return the param endereco cliente
	 */
	String getParamEnderecoCliente();

	/**
	 * Sets the param endereco cliente.
	 *
	 * @param paramEnderecoCliente the new param endereco cliente
	 */
	void setParamEnderecoCliente(String paramEnderecoCliente);

	/**
	 * Gets the param nome avalista 1.
	 *
	 * @return the param nome avalista 1
	 */
	String getParamNomeAvalista1();

	/**
	 * Sets the param nome avalista 1.
	 *
	 * @param paramNomeAvalista1 the new param nome avalista 1
	 */
	void setParamNomeAvalista1(String paramNomeAvalista1);

	/**
	 * Gets the param CPF avalista 1.
	 *
	 * @return the param CPF avalista 1
	 */
	String getParamCPFAvalista1();

	/**
	 * Sets the param CPF avalista 1.
	 *
	 * @param paramCPFAvalista1 the new param CPF avalista 1
	 */
	void setParamCPFAvalista1(String paramCPFAvalista1);

	/**
	 * Gets the param nome avalista 2.
	 *
	 * @return the param nome avalista 2
	 */
	String getParamNomeAvalista2();

	/**
	 * Sets the param nome avalista 2.
	 *
	 * @param paramNomeAvalista2 the new param nome avalista 2
	 */
	void setParamNomeAvalista2(String paramNomeAvalista2);

	/**
	 * Gets the param CPF avalista 2.
	 *
	 * @return the param CPF avalista 2
	 */
	String getParamCPFAvalista2();

	/**
	 * Sets the param CPF avalista 2.
	 *
	 * @param paramCPFAvalista2 the new param CPF avalista 2
	 */
	void setParamCPFAvalista2(String paramCPFAvalista2);

	/**
	 * Gets the param nome avalista 3.
	 *
	 * @return the param nome avalista 3
	 */
	String getParamNomeAvalista3();

	/**
	 * Sets the param nome avalista 3.
	 *
	 * @param paramNomeAvalista3 the new param nome avalista 3
	 */
	void setParamNomeAvalista3(String paramNomeAvalista3);

	/**
	 * Gets the param CPF avalista 3.
	 *
	 * @return the param CPF avalista 3
	 */
	String getParamCPFAvalista3();

	/**
	 * Sets the param CPF avalista 3.
	 *
	 * @param paramCPFAvalista3 the new param CPF avalista 3
	 */
	void setParamCPFAvalista3(String paramCPFAvalista3);

}
