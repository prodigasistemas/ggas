/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
/**
 *  Interface responsável pela assinatura dos métodos de dados das parcelas
 *
 */
public interface DadosParcelas extends Serializable {

	String BEAN_ID_DADOS_PARCELAS = "dadosParcelas";

	/**
	 * @return the numeroParcela
	 */
	Integer getNumeroParcela();

	/**
	 * @param numeroParcela
	 *            the numeroParcela to set
	 */
	void setNumeroParcela(Integer numeroParcela);

	/**
	 * @return the dataVencimento
	 */
	Date getDataVencimento();

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * @return the quantidadeParcelas
	 */
	Integer getQuantidadeParcelas();

	/**
	 * @param quantidadeParcelas
	 *            the quantidadeParcelas to set
	 */
	void setQuantidadeParcelas(Integer quantidadeParcelas);

	/**
	 * @return the saldoInicial
	 */
	BigDecimal getSaldoInicial();

	/**
	 * @param saldoInicial
	 *            the saldoInicial to set
	 */
	void setSaldoInicial(BigDecimal saldoInicial);

	/**
	 * @return the juros
	 */
	BigDecimal getJuros();

	/**
	 * @param juros
	 *            the juros to set
	 */
	void setJuros(BigDecimal juros);

	/**
	 * @return the amortizacao
	 */
	BigDecimal getAmortizacao();

	/**
	 * @param amortizacao
	 *            the amortizacao to set
	 */
	void setAmortizacao(BigDecimal amortizacao);

	/**
	 * @return the total
	 */
	BigDecimal getTotal();

	/**
	 * @param total
	 *            the total to set
	 */
	void setTotal(BigDecimal total);

	/**
	 * @return the saldoFinal
	 */
	BigDecimal getSaldoFinal();

	/**
	 * @param saldoFinal
	 *            the saldoFinal to set
	 */
	void setSaldoFinal(BigDecimal saldoFinal);

	/**
	 * @return the dataPagamento
	 */
	Date getDataPagamento();

	/**
	 * @param dataPagamento
	 *            the dataPagamento to set
	 */
	void setDataPagamento(Date dataPagamento);

	/**
	 * @return the listaItemParcela
	 */
	Collection<DadosItemParcela> getListaItemParcela();

	/**
	 * @param listaItemParcela
	 *            the listaItemParcela to set
	 */
	void setListaItemParcela(Collection<DadosItemParcela> listaItemParcela);

}
