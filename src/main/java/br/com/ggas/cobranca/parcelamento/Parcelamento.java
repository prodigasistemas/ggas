/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao parcelamento. 
 *
 */
public interface Parcelamento extends EntidadeNegocio {

	String BEAN_ID_PARCELAMENTO = "parcelamento";

	String ROTULO_PARCELAMENTO = "PARCELAMENTO";

	String ROTULO_PARCELAMENTO_PERFIL_PARCELAMENTO = "PARCELAMENTO_PERFIL_PARCELAMENTO";

	String ROTULO_PARCELAMENTO_VALOR_DEBITO = "PARCELAMENTO_VALOR_DEBITO";

	String ROTULO_PARCELAMENTO_TOTAL_LIQUIDO = "PARCELAMENTO_TOTAL_LIQUIDO";

	String ROTULO_PARCELAMENTO_NUMERO_PARCELAS = "PARCELAMENTO_NUMERO_PARCELAS";

	String ROTULO_PARCELAMENTO_VALOR_DESCONTO = "PARCELAMENTO_VALOR_DESCONTO";

	String ROTULO_PARCELAMENTO_VENCIMENTO_INICIAL = "PARCELAMENTO_VENCIMENTO_INICIAL";

	String ROTULO_PARCELAMENTO_PERIODICIDADE_PARCELAS = "PARCELAMENTO_PERIODICIDADE_PARCELAS";

	/**
	 * @return ValorParcelado
	 */
	String getValorParcelado();

	/**
	 * @return
	 */
	String getValorEntradaFormatado();

	/**
	 * @return the parcelamentoPerfil
	 */
	ParcelamentoPerfil getParcelamentoPerfil();

	/**
	 * @param parcelamentoPerfil
	 *            the parcelamentoPerfil to set
	 */
	void setParcelamentoPerfil(ParcelamentoPerfil parcelamentoPerfil);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the fiador
	 */
	Cliente getFiador();

	/**
	 * @param fiador
	 *            the fiador to set
	 */
	void setFiador(Cliente fiador);

	/**
	 * @return the dataParcelamento
	 */
	Date getDataParcelamento();

	/**
	 * @param dataParcelamento
	 *            the dataParcelamento to set
	 */
	void setDataParcelamento(Date dataParcelamento);

	/**
	 * @return the valorFatura
	 */
	BigDecimal getValorFatura();

	/**
	 * @param valorFatura
	 *            the valorFatura to set
	 */
	void setValorFatura(BigDecimal valorFatura);

	/**
	 * @return the valorDebito
	 */
	BigDecimal getValorDebito();

	/**
	 * @param valorDebito
	 *            the valorDebito to set
	 */
	void setValorDebito(BigDecimal valorDebito);

	/**
	 * @return the valorCredito
	 */
	BigDecimal getValorCredito();

	/**
	 * @param valorCredito
	 *            the valorCredito to set
	 */
	void setValorCredito(BigDecimal valorCredito);

	/**
	 * @return the valorCorrecao
	 */
	BigDecimal getValorCorrecao();

	/**
	 * @param valorCorrecao
	 *            the valorCorrecao to set
	 */
	void setValorCorrecao(BigDecimal valorCorrecao);

	/**
	 * @return the valorJurosMora
	 */
	BigDecimal getValorJurosMora();

	/**
	 * @param valorJurosMora
	 *            the valorJurosMora to set
	 */
	void setValorJurosMora(BigDecimal valorJurosMora);

	/**
	 * @return the percentualJurosMora
	 */
	BigDecimal getPercentualJurosMora();

	/**
	 * @param percentualJurosMora
	 *            the percentualJurosMora to set
	 */
	void setPercentualJurosMora(BigDecimal percentualJurosMora);

	/**
	 * @return the valorMulta
	 */
	BigDecimal getValorMulta();

	/**
	 * @param valorMulta
	 *            the valorMulta to set
	 */
	void setValorMulta(BigDecimal valorMulta);

	/**
	 * @return the percentualMulta
	 */
	BigDecimal getPercentualMulta();

	/**
	 * @param percentualMulta
	 *            the percentualMulta to set
	 */
	void setPercentualMulta(BigDecimal percentualMulta);

	/**
	 * @return the valorAtualizado
	 */
	BigDecimal getValorAtualizado();

	/**
	 * @param valorAtualizado
	 *            the valorAtualizado to set
	 */
	void setValorAtualizado(BigDecimal valorAtualizado);

	/**
	 * @return the valorDesconto
	 */
	BigDecimal getValorDesconto();

	/**
	 * @param valorDesconto
	 *            the valorDesconto to set
	 */
	void setValorDesconto(BigDecimal valorDesconto);

	/**
	 * @return the percentualDesconto
	 */
	BigDecimal getPercentualDesconto();

	/**
	 * @param percentualDesconto
	 *            the percentualDesconto to set
	 */
	void setPercentualDesconto(BigDecimal percentualDesconto);

	/**
	 * @return the valorEntrada
	 */
	BigDecimal getValorEntrada();

	/**
	 * @param valorEntrada
	 *            the valorEntrada to set
	 */
	void setValorEntrada(BigDecimal valorEntrada);

	/**
	 * @return the percentualEntrada
	 */
	BigDecimal getPercentualEntrada();

	/**
	 * @param percentualEntrada
	 *            the percentualEntrada to set
	 */
	void setPercentualEntrada(BigDecimal percentualEntrada);

	/**
	 * @return the valorJurosParcelamento
	 */
	BigDecimal getValorJurosParcelamento();

	/**
	 * @param valorJurosParcelamento
	 *            the valorJurosParcelamento to
	 *            set
	 */
	void setValorJurosParcelamento(BigDecimal valorJurosParcelamento);

	/**
	 * @return the percentualJurosParcelamento
	 */
	BigDecimal getPercentualJurosParcelamento();

	/**
	 * @param percentualJurosParcelamento
	 *            the percentualJurosParcelamento
	 *            to set
	 */
	void setPercentualJurosParcelamento(BigDecimal percentualJurosParcelamento);

	/**
	 * @return the numeroPrestacoes
	 */
	Integer getNumeroPrestacoes();

	/**
	 * @param numeroPrestacoes
	 *            the numeroPrestacoes to set
	 */
	void setNumeroPrestacoes(Integer numeroPrestacoes);

	/**
	 * @return the formaCobrancaParcelamento
	 */
	EntidadeConteudo getFormaCobrancaParcelamento();

	/**
	 * @param formaCobrancaParcelamento
	 *            the formaCobrancaParcelamento to
	 *            set
	 */
	void setFormaCobrancaParcelamento(EntidadeConteudo formaCobrancaParcelamento);

	/**
	 * @return the dataVencimentoInicial
	 */
	Date getDataVencimentoInicial();

	/**
	 * @param dataVencimentoInicial
	 *            the dataVencimentoInicial to set
	 */
	void setDataVencimentoInicial(Date dataVencimentoInicial);

	/**
	 * @return the numeroDiasPeriodicidade
	 */
	Integer getNumeroDiasPeriodicidade();

	/**
	 * @param numeroDiasPeriodicidade
	 *            the numeroDiasPeriodicidade to
	 *            set
	 */
	void setNumeroDiasPeriodicidade(Integer numeroDiasPeriodicidade);

	/**
	 * @return the emissaoNotasPromissorias
	 */
	boolean isEmissaoNotasPromissorias();

	/**
	 * @param emissaoNotasPromissorias
	 *            the emissaoNotasPromissorias to
	 *            set
	 */
	void setEmissaoNotasPromissorias(boolean emissaoNotasPromissorias);

	/**
	 * @return the emissaoTermoParcelamento
	 */
	boolean isEmissaoTermoParcelamento();

	/**
	 * @param emissaoTermoParcelamento
	 *            the emissaoTermoParcelamento to
	 *            set
	 */
	void setEmissaoTermoParcelamento(boolean emissaoTermoParcelamento);

	/**
	 * @return the listaParcelamentoItem
	 */
	Collection<ParcelamentoItem> getListaParcelamentoItem();

	/**
	 * @param listaParcelamentoItem
	 *            the listaParcelamentoItem to set
	 */
	void setListaParcelamentoItem(Collection<ParcelamentoItem> listaParcelamentoItem);

	/**
	 * @return
	 */
	EntidadeConteudo getStatus();

	/**
	 * @param status
	 */
	void setStatus(EntidadeConteudo status);

	/**
	 * @return
	 */
	Usuario getUltimoUsuarioAlteracao();

	/**
	 * @param ultimoUsuarioAlteracao
	 */
	void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao);

	/**
	 * @return
	 */
	public EntidadeConteudo getTipoCobranca();

	/**
	 * @param tipoCobranca
	 */
	public void setTipoCobranca(EntidadeConteudo tipoCobranca);

	/**
	 * @return ValorLiquido
	 */
	String getValorLiquido();

}
