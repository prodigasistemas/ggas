/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;
import br.com.ggas.cobranca.parcelamento.ItemParcelaVO;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.EntidadeConteudo;

class DadosGeraisParcelasImpl implements DadosGeraisParcelas {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 98418370402853124L;

	private ParcelamentoPerfil parcelamentoPerfil;

	private boolean cobrancaEmFatura;

	private EntidadeConteudo formaCobranca;

	private Date vencimentoInicial;

	private boolean indicadorVencimentoInicial;

	private Integer periodicidadeParcelas;

	private boolean emiteNotaPromissoria;

	private boolean emiteTermoConfissaoDivida;

	private boolean indicarJuros;

	private Long idFiador;

	private Long idPontoConsumo;

	private Integer numeroPrestacoes;

	private BigDecimal valorDesconto;

	private BigDecimal percentualDesconto;

	private DadosAuditoria dadosAuditoria;

	private BigDecimal valorTotalJuros;

	private BigDecimal valorTotalAPagar;

	private Collection<DadosParcelas> listaDadosParcelas = new ArrayList<>();

	private Collection<Fatura> listaFatura = new ArrayList<>();

	private Collection<CreditoDebitoARealizar> listaCreditoDebito = new ArrayList<>();

	private Long idCliente;

	private String melhorDiaVencimento;

	private EntidadeConteudo tipoCobranca;

	// - valores usados em credito/debito a
	// realizar
	private BigDecimal valorEntrada;

	private Long idRubrica;

	private Long idCreditoOrigem;

	private BigDecimal taxaJuros;

	private Date dataInicioCobranca;

	private Date dataInicioCredito;

	private String descricao;

	private BigDecimal quantidade;

	private String quantidadeDiasCobranca;

	private Long idInicioCobranca;

	private Long idPeriodicidadeJuros;

	private Long idPeriodicidade;

	private String motivoCancelamento;

	private BigDecimal valorUnitario;

	// - mapa lancamento contabil
	private Map<LancamentoItemContabil, ItemParcelaVO> mapaValoresPorRubrica = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #getMelhorDiaVencimento()
	 */
	@Override
	public String getMelhorDiaVencimento() {

		return melhorDiaVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setMelhorDiaVencimento(java.lang.String)
	 */
	@Override
	public void setMelhorDiaVencimento(String melhorDiaVencimento) {

		this.melhorDiaVencimento = melhorDiaVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getValorTotalAPagar()
	 */
	@Override
	public BigDecimal getValorTotalAPagar() {

		return valorTotalAPagar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setValorTotalAPagar(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalAPagar(BigDecimal valorTotalAPagar) {

		this.valorTotalAPagar = valorTotalAPagar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getFormaCobranca()
	 */
	@Override
	public EntidadeConteudo getFormaCobranca() {

		return formaCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setFormaCobranca(br.com.
	 * ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setFormaCobranca(EntidadeConteudo formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getValorTotalJuros()
	 */
	@Override
	public BigDecimal getValorTotalJuros() {

		return valorTotalJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setValorTotalJuros(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalJuros(BigDecimal valorTotalJuros) {

		this.valorTotalJuros = valorTotalJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#isIndicarJuros()
	 */
	@Override
	public boolean isIndicarJuros() {

		return indicarJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setIndicarJuros(boolean)
	 */
	@Override
	public void setIndicarJuros(boolean indicarJuros) {

		this.indicarJuros = indicarJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getVencimentoInicial()
	 */
	@Override
	public Date getVencimentoInicial() {

		return vencimentoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setVencimentoInicial(java.util.Date)
	 */
	@Override
	public void setVencimentoInicial(Date vencimentoInicial) {

		this.vencimentoInicial = vencimentoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #isIndicadorVencimentoInicial()
	 */
	@Override
	public boolean isIndicadorVencimentoInicial() {

		return indicadorVencimentoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setIndicadorVencimentoInicial(boolean)
	 */
	@Override
	public void setIndicadorVencimentoInicial(boolean indicadorVencimentoInicial) {

		this.indicadorVencimentoInicial = indicadorVencimentoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #getPeriodicidadeParcelas()
	 */
	@Override
	public Integer getPeriodicidadeParcelas() {

		return periodicidadeParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setPeriodicidadeParcelas
	 * (java.lang.Integer)
	 */
	@Override
	public void setPeriodicidadeParcelas(Integer periodicidadeParcelas) {

		this.periodicidadeParcelas = periodicidadeParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas#getParcelamentoPerfil()
	 */
	@Override
	public ParcelamentoPerfil getParcelamentoPerfil() {

		return parcelamentoPerfil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #setParcelamentoPerfil(br
	 * .com.ggas.cobranca.
	 * perfilparcelamento.ParcelamentoPerfil)
	 */
	@Override
	public void setParcelamentoPerfil(ParcelamentoPerfil parcelamentoPerfil) {

		this.parcelamentoPerfil = parcelamentoPerfil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas#isCobrancaEmFatura()
	 */
	@Override
	public boolean isCobrancaEmFatura() {

		return cobrancaEmFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #setCobrancaEmFatura(boolean)
	 */
	@Override
	public void setCobrancaEmFatura(boolean cobrancaEmFatura) {

		this.cobrancaEmFatura = cobrancaEmFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #isEmiteNotaPromissoria()
	 */
	@Override
	public boolean isEmiteNotaPromissoria() {

		return emiteNotaPromissoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #setEmiteNotaPromissoria(boolean)
	 */
	@Override
	public void setEmiteNotaPromissoria(boolean emiteNotaPromissoria) {

		this.emiteNotaPromissoria = emiteNotaPromissoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #isEmiteTermoConfissaoDivida()
	 */
	@Override
	public boolean isEmiteTermoConfissaoDivida() {

		return emiteTermoConfissaoDivida;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #setEmiteTermoConfissaoDivida(boolean)
	 */
	@Override
	public void setEmiteTermoConfissaoDivida(boolean emiteTermoConfissaoDivida) {

		this.emiteTermoConfissaoDivida = emiteTermoConfissaoDivida;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas#getIdFiador()
	 */
	@Override
	public Long getIdFiador() {

		return idFiador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #setIdFiador(java.lang.Long)
	 */
	@Override
	public void setIdFiador(Long idFiador) {

		this.idFiador = idFiador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas#getListaDadosParcelas()
	 */
	@Override
	public Collection<DadosParcelas> getListaDadosParcelas() {

		return listaDadosParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getIdPontoConsumo()
	 */
	@Override
	public Long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setIdPontoConsumo(java.lang.Long)
	 */
	@Override
	public void setIdPontoConsumo(Long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getDadosAuditoria()
	 */
	@Override
	public DadosAuditoria getDadosAuditoria() {

		return dadosAuditoria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setDadosAuditoria(br.com
	 * .ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {

		this.dadosAuditoria = dadosAuditoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * DadosGeraisParcelas
	 * #setListaDadosParcelas(java
	 * .util.Collection)
	 */
	@Override
	public void setListaDadosParcelas(Collection<DadosParcelas> listaDadosParcelas) {

		this.listaDadosParcelas = listaDadosParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getListaFatura()
	 */
	@Override
	public Collection<Fatura> getListaFatura() {

		return listaFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setListaFatura(java.util.Collection)
	 */
	@Override
	public void setListaFatura(Collection<Fatura> listaFatura) {

		this.listaFatura = listaFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getListaCreditoDebito()
	 */
	@Override
	public Collection<CreditoDebitoARealizar> getListaCreditoDebito() {

		return listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setListaCreditoDebito(java
	 * .util.Collection)
	 */
	@Override
	public void setListaCreditoDebito(Collection<CreditoDebitoARealizar> listaCreditoDebito) {

		this.listaCreditoDebito = listaCreditoDebito;
	}

	/**
	 * @return the numeroPrestacoes
	 */
	@Override
	public Integer getNumeroPrestacoes() {

		return numeroPrestacoes;
	}

	/**
	 * @param numeroPrestacoes
	 *            the numeroPrestacoes to set
	 */
	@Override
	public void setNumeroPrestacoes(Integer numeroPrestacoes) {

		this.numeroPrestacoes = numeroPrestacoes;
	}

	/**
	 * @return the valorDesconto
	 */
	@Override
	public BigDecimal getValorDesconto() {

		return valorDesconto;
	}

	/**
	 * @param valorDesconto
	 *            the valorDesconto to set
	 */
	@Override
	public void setValorDesconto(BigDecimal valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	/**
	 * @return the percentualDesconto
	 */
	@Override
	public BigDecimal getPercentualDesconto() {

		return percentualDesconto;
	}

	/**
	 * @param percentualDesconto
	 *            the percentualDesconto to set
	 */
	@Override
	public void setPercentualDesconto(BigDecimal percentualDesconto) {

		this.percentualDesconto = percentualDesconto;
	}

	/**
	 * @return the idCliente
	 */
	@Override
	public Long getIdCliente() {

		return idCliente;
	}

	/**
	 * @param idCliente
	 *            the idCliente to set
	 */
	@Override
	public void setIdCliente(Long idCliente) {

		this.idCliente = idCliente;
	}

	/**
	 * @return the valorEntrada
	 */
	@Override
	public BigDecimal getValorEntrada() {

		return valorEntrada;
	}

	/**
	 * @param valorEntrada
	 *            the valorEntrada to set
	 */
	@Override
	public void setValorEntrada(BigDecimal valorEntrada) {

		this.valorEntrada = valorEntrada;
	}

	/**
	 * @return the idRubrica
	 */
	@Override
	public Long getIdRubrica() {

		return idRubrica;
	}

	/**
	 * @param idRubrica
	 *            the idRubrica to set
	 */
	@Override
	public void setIdRubrica(Long idRubrica) {

		this.idRubrica = idRubrica;
	}

	/**
	 * @return the idCreditoOrigem
	 */
	@Override
	public Long getIdCreditoOrigem() {

		return idCreditoOrigem;
	}

	/**
	 * @param idCreditoOrigem
	 *            the idCreditoOrigem to set
	 */
	@Override
	public void setIdCreditoOrigem(Long idCreditoOrigem) {

		this.idCreditoOrigem = idCreditoOrigem;
	}

	/**
	 * @return the taxaJuros
	 */
	@Override
	public BigDecimal getTaxaJuros() {

		return taxaJuros;
	}

	/**
	 * @param taxaJuros
	 *            the taxaJuros to set
	 */
	@Override
	public void setTaxaJuros(BigDecimal taxaJuros) {

		this.taxaJuros = taxaJuros;
	}

	/**
	 * @return the dataInicioCobranca
	 */
	@Override
	public Date getDataInicioCobranca() {

		return dataInicioCobranca;
	}

	/**
	 * @param dataInicioCobranca
	 *            the dataInicioCobranca to set
	 */
	@Override
	public void setDataInicioCobranca(Date dataInicioCobranca) {

		this.dataInicioCobranca = dataInicioCobranca;
	}

	/**
	 * @return the dataInicioCredito
	 */
	@Override
	public Date getDataInicioCredito() {

		return dataInicioCredito;
	}

	/**
	 * @param dataInicioCredito
	 *            the dataInicioCredito to set
	 */
	@Override
	public void setDataInicioCredito(Date dataInicioCredito) {

		this.dataInicioCredito = dataInicioCredito;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the quantidade
	 */
	@Override
	public BigDecimal getQuantidade() {

		return quantidade;
	}

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	@Override
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	/**
	 * @return the quantidadeDiasCobranca
	 */
	@Override
	public String getQuantidadeDiasCobranca() {

		return quantidadeDiasCobranca;
	}

	/**
	 * @param quantidadeDiasCobranca
	 *            the quantidadeDiasCobranca to
	 *            set
	 */
	@Override
	public void setQuantidadeDiasCobranca(String quantidadeDiasCobranca) {

		this.quantidadeDiasCobranca = quantidadeDiasCobranca;
	}

	/**
	 * @return the idInicioCobranca
	 */
	@Override
	public Long getIdInicioCobranca() {

		return idInicioCobranca;
	}

	/**
	 * @param idInicioCobranca
	 *            the idInicioCobranca to set
	 */
	@Override
	public void setIdInicioCobranca(Long idInicioCobranca) {

		this.idInicioCobranca = idInicioCobranca;
	}

	/**
	 * @return the idPeriodicidadeJuros
	 */
	@Override
	public Long getIdPeriodicidadeJuros() {

		return idPeriodicidadeJuros;
	}

	/**
	 * @param idPeriodicidadeJuros
	 *            the idPeriodicidadeJuros to set
	 */
	@Override
	public void setIdPeriodicidadeJuros(Long idPeriodicidadeJuros) {

		this.idPeriodicidadeJuros = idPeriodicidadeJuros;
	}

	/**
	 * @return the idPeriodicidade
	 */
	@Override
	public Long getIdPeriodicidade() {

		return idPeriodicidade;
	}

	/**
	 * @param idPeriodicidade
	 *            the idPeriodicidade to set
	 */
	@Override
	public void setIdPeriodicidade(Long idPeriodicidade) {

		this.idPeriodicidade = idPeriodicidade;
	}

	/**
	 * @return the motivoCancelamento
	 */
	@Override
	public String getMotivoCancelamento() {

		return motivoCancelamento;
	}

	/**
	 * @param motivoCancelamento
	 *            the motivoCancelamento to set
	 */
	@Override
	public void setMotivoCancelamento(String motivoCancelamento) {

		this.motivoCancelamento = motivoCancelamento;
	}

	/**
	 * @return the valorUnitario
	 */
	@Override
	public BigDecimal getValorUnitario() {

		return valorUnitario;
	}

	/**
	 * @param valorUnitario
	 *            the valorUnitario to set
	 */
	@Override
	public void setValorUnitario(BigDecimal valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #getMapaValoresPorRubrica()
	 */
	@Override
	public Map<LancamentoItemContabil, ItemParcelaVO> getMapaValoresPorRubrica() {

		return mapaValoresPorRubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setMapaValoresPorRubrica(java.util.Map)
	 */
	@Override
	public void setMapaValoresPorRubrica(Map<LancamentoItemContabil, ItemParcelaVO> mapaValoresPorRubrica) {

		this.mapaValoresPorRubrica = mapaValoresPorRubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas#getTipoCobranca()
	 */
	@Override
	public EntidadeConteudo getTipoCobranca() {

		return tipoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas
	 * #setTipoCobranca(br.com.ggas
	 * .geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoCobranca(EntidadeConteudo tipoCobranca) {

		this.tipoCobranca = tipoCobranca;
	}

}
