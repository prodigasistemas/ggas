/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ParcelamentoItemImpl representa uma ParcelamentoItemImpl no sistema.
 *
 * @since 08/04/2010
 * 
 */

package br.com.ggas.cobranca.parcelamento.impl;

import java.util.Map;

import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.parcelamento.ParcelamentoItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 *
 */
class ParcelamentoItemImpl extends EntidadeNegocioImpl implements ParcelamentoItem {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5877493429963325877L;

	private Parcelamento parcelamento;

	private CreditoDebitoARealizar creditoDebitoARealizar;

	private FaturaGeral faturaGeral;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * ParcelamentoItem#getParcelamento()
	 */
	@Override
	public Parcelamento getParcelamento() {

		return parcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * ParcelamentoItem
	 * #setParcelamento(br.com.ggas
	 * .cobranca.parcelamento.Parcelamento)
	 */
	@Override
	public void setParcelamento(Parcelamento parcelamento) {

		this.parcelamento = parcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * ParcelamentoItem
	 * #getCreditoDebitoARealizar()
	 */
	@Override
	public CreditoDebitoARealizar getCreditoDebitoARealizar() {

		return creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * ParcelamentoItem
	 * #setCreditoDebitoARealizar(br
	 * .com.ggas.faturamento
	 * .CreditoDebitoARealizar)
	 */
	@Override
	public void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) {

		this.creditoDebitoARealizar = creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * ParcelamentoItem#getFaturaGeral()
	 */
	@Override
	public FaturaGeral getFaturaGeral() {

		return faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * ParcelamentoItem
	 * #setFaturaGeral(br.com.ggas.
	 * faturamento.FaturaGeral)
	 */
	@Override
	public void setFaturaGeral(FaturaGeral faturaGeral) {

		this.faturaGeral = faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
