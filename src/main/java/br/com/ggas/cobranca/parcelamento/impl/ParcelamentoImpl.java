/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.parcelamento.ParcelamentoItem;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pela implementação dos métodos relacionados ao parcelamento
 *
 */
public class ParcelamentoImpl extends EntidadeNegocioImpl implements Parcelamento {

	private static final int NUMERO_DECIMAIS = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1067683384302677161L;

	private ParcelamentoPerfil parcelamentoPerfil;

	private PontoConsumo pontoConsumo;

	private Cliente cliente;

	private Cliente fiador;

	private Date dataParcelamento;

	private BigDecimal valorFatura;

	private BigDecimal valorDebito;

	private BigDecimal valorCredito;

	private BigDecimal valorCorrecao;

	private BigDecimal valorJurosMora;

	private BigDecimal percentualJurosMora;

	private BigDecimal valorMulta;

	private BigDecimal percentualMulta;

	private BigDecimal valorAtualizado;

	private BigDecimal valorDesconto;

	private BigDecimal percentualDesconto;

	private BigDecimal valorEntrada;

	private BigDecimal percentualEntrada;

	private BigDecimal valorJurosParcelamento;

	private BigDecimal percentualJurosParcelamento;

	private Integer numeroPrestacoes;

	private EntidadeConteudo formaCobrancaParcelamento;

	private Date dataVencimentoInicial;

	private Integer numeroDiasPeriodicidade;

	private boolean emissaoNotasPromissorias;

	private boolean emissaoTermoParcelamento;

	private EntidadeConteudo status;

	private Usuario ultimoUsuarioAlteracao;

	private EntidadeConteudo tipoCobranca;

	private Collection<ParcelamentoItem> listaParcelamentoItem = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #getValorParcelado()
	 */
	@Override
	public String getValorParcelado() {

		BigDecimal valorParcelado = BigDecimal.ZERO;
		if(this.valorAtualizado != null) {
			valorParcelado = valorAtualizado;
		}
		/*
		 * TODO: TKT 4064
		 * if (valorDesconto!=null).
		 * valorParcelado =
		 * valorParcelado.subtract(valorDesconto)
		 * 
		 * if (valorJurosParcelamento !=null)
		 * valorParcelado =
		 * valorParcelado.add(valorJurosParcelamento
		 * )
		 * 
		 */
		return Util.converterCampoValorDecimalParaString("", valorParcelado, Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #getValorEntradaFormatado()
	 */
	@Override
	public String getValorEntradaFormatado() {

		if(valorEntrada != null) {
			return Util.converterCampoValorDecimalParaString("", this.valorEntrada, Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
		} else {
			return Util.converterCampoValorDecimalParaString("", BigDecimal.ZERO, Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getParcelamentoPerfil()
	 */
	@Override
	public ParcelamentoPerfil getParcelamentoPerfil() {

		return parcelamentoPerfil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setParcelamentoPerfil(br.com.ggas
	 * .cobranca.
	 * perfilparcelamento.ParcelamentoPerfil)
	 */
	@Override
	public void setParcelamentoPerfil(ParcelamentoPerfil parcelamentoPerfil) {

		this.parcelamentoPerfil = parcelamentoPerfil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setPontoConsumo(br.com.ggas.cadastro
	 * .imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setCliente(br.com.ggas.cadastro
	 * .cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getFiador()
	 */
	@Override
	public Cliente getFiador() {

		return fiador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setFiador(br.com.ggas.cadastro.
	 * cliente.Cliente)
	 */
	@Override
	public void setFiador(Cliente fiador) {

		this.fiador = fiador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getDataParcelamento()
	 */
	@Override
	public Date getDataParcelamento() {
		Date data = null;
		if(dataParcelamento != null) {
			data = (Date) dataParcelamento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setDataParcelamento(java.util.Date)
	 */
	@Override
	public void setDataParcelamento(Date dataParcelamento) {
		if(dataParcelamento != null) {
			this.dataParcelamento = (Date) dataParcelamento.clone();
		} else {
			this.dataParcelamento = null;
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorFatura()
	 */
	@Override
	public BigDecimal getValorFatura() {

		return valorFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorFatura(java.math.BigDecimal)
	 */
	@Override
	public void setValorFatura(BigDecimal valorFatura) {

		this.valorFatura = valorFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorDebito()
	 */
	@Override
	public BigDecimal getValorDebito() {

		return valorDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorDebito(java.math.BigDecimal)
	 */
	@Override
	public void setValorDebito(BigDecimal valorDebito) {

		this.valorDebito = valorDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorCredito()
	 */
	@Override
	public BigDecimal getValorCredito() {

		return valorCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorCredito(java.math.BigDecimal)
	 */
	@Override
	public void setValorCredito(BigDecimal valorCredito) {

		this.valorCredito = valorCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorCorrecao()
	 */
	@Override
	public BigDecimal getValorCorrecao() {

		return valorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorCorrecao(java.math.BigDecimal)
	 */
	@Override
	public void setValorCorrecao(BigDecimal valorCorrecao) {

		this.valorCorrecao = valorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorJurosMora()
	 */
	@Override
	public BigDecimal getValorJurosMora() {

		return valorJurosMora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorJurosMora(java.math.BigDecimal)
	 */
	@Override
	public void setValorJurosMora(BigDecimal valorJurosMora) {

		this.valorJurosMora = valorJurosMora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getPercentualJurosMora()
	 */
	@Override
	public BigDecimal getPercentualJurosMora() {

		return percentualJurosMora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setPercentualJurosMora(java.math
	 * .BigDecimal)
	 */
	@Override
	public void setPercentualJurosMora(BigDecimal percentualJurosMora) {

		this.percentualJurosMora = percentualJurosMora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorMulta()
	 */
	@Override
	public BigDecimal getValorMulta() {

		return valorMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorMulta(java.math.BigDecimal)
	 */
	@Override
	public void setValorMulta(BigDecimal valorMulta) {

		this.valorMulta = valorMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getPercentualMulta()
	 */
	@Override
	public BigDecimal getPercentualMulta() {

		return percentualMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setPercentualMulta(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualMulta(BigDecimal percentualMulta) {

		this.percentualMulta = percentualMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorAtualizado()
	 */
	@Override
	public BigDecimal getValorAtualizado() {

		return valorAtualizado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorAtualizado(java.math.BigDecimal)
	 */
	@Override
	public void setValorAtualizado(BigDecimal valorAtualizado) {

		this.valorAtualizado = valorAtualizado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorDesconto()
	 */
	@Override
	public BigDecimal getValorDesconto() {

		return valorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorDesconto(java.math.BigDecimal)
	 */
	@Override
	public void setValorDesconto(BigDecimal valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getPercentualDesconto()
	 */
	@Override
	public BigDecimal getPercentualDesconto() {

		return percentualDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setPercentualDesconto(java.math
	 * .BigDecimal)
	 */
	@Override
	public void setPercentualDesconto(BigDecimal percentualDesconto) {

		this.percentualDesconto = percentualDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorEntrada()
	 */
	@Override
	public BigDecimal getValorEntrada() {

		return valorEntrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorEntrada(java.math.BigDecimal)
	 */
	@Override
	public void setValorEntrada(BigDecimal valorEntrada) {

		this.valorEntrada = valorEntrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getPercentualEntrada()
	 */
	@Override
	public BigDecimal getPercentualEntrada() {

		return percentualEntrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setPercentualEntrada(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualEntrada(BigDecimal percentualEntrada) {

		this.percentualEntrada = percentualEntrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getValorJurosParcelamento()
	 */
	@Override
	public BigDecimal getValorJurosParcelamento() {

		return valorJurosParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setValorJurosParcelamento(java.
	 * math.BigDecimal)
	 */
	@Override
	public void setValorJurosParcelamento(BigDecimal valorJurosParcelamento) {

		this.valorJurosParcelamento = valorJurosParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #getPercentualJurosParcelamento()
	 */
	@Override
	public BigDecimal getPercentualJurosParcelamento() {

		return percentualJurosParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setPercentualJurosParcelamento(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setPercentualJurosParcelamento(BigDecimal percentualJurosParcelamento) {

		this.percentualJurosParcelamento = percentualJurosParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getNumeroPrestacoes()
	 */
	@Override
	public Integer getNumeroPrestacoes() {

		return numeroPrestacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setNumeroPrestacoes(java.math.BigDecimal)
	 */
	@Override
	public void setNumeroPrestacoes(Integer numeroPrestacoes) {

		this.numeroPrestacoes = numeroPrestacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getFormaCobrancaParcelamento()
	 */
	@Override
	public EntidadeConteudo getFormaCobrancaParcelamento() {

		return formaCobrancaParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setFormaCobrancaParcelamento(br
	 * .com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setFormaCobrancaParcelamento(EntidadeConteudo formaCobrancaParcelamento) {

		this.formaCobrancaParcelamento = formaCobrancaParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getDataVencimentoInicial()
	 */
	@Override
	public Date getDataVencimentoInicial() {
		Date data = null;
		if(dataVencimentoInicial != null) {
			data = (Date) dataVencimentoInicial.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setDataVencimentoInicial(java.util.Date)
	 */
	@Override
	public void setDataVencimentoInicial(Date dataVencimentoInicial) {
		if(dataVencimentoInicial != null) {
			this.dataVencimentoInicial = (Date) dataVencimentoInicial.clone();
		} else {
			this.dataVencimentoInicial = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#getNumeroDiasPeriodicidade()
	 */
	@Override
	public Integer getNumeroDiasPeriodicidade() {

		return numeroDiasPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setNumeroDiasPeriodicidade(java
	 * .lang.Integer)
	 */
	@Override
	public void setNumeroDiasPeriodicidade(Integer numeroDiasPeriodicidade) {

		this.numeroDiasPeriodicidade = numeroDiasPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#isEmissaoNotasPromissorias()
	 */
	@Override
	public boolean isEmissaoNotasPromissorias() {

		return emissaoNotasPromissorias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setEmissaoNotasPromissorias(boolean)
	 */
	@Override
	public void setEmissaoNotasPromissorias(boolean emissaoNotasPromissorias) {

		this.emissaoNotasPromissorias = emissaoNotasPromissorias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento#isEmissaoTermoParcelamento()
	 */
	@Override
	public boolean isEmissaoTermoParcelamento() {

		return emissaoTermoParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.impl.
	 * Parcelamento
	 * #setEmissaoTermoParcelamento(boolean)
	 */
	@Override
	public void setEmissaoTermoParcelamento(boolean emissaoTermoParcelamento) {

		this.emissaoTermoParcelamento = emissaoTermoParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #getStatus()
	 */
	@Override
	public EntidadeConteudo getStatus() {

		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #getUltimoUsuarioAlteracao()
	 */
	@Override
	public Usuario getUltimoUsuarioAlteracao() {

		return ultimoUsuarioAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #setUltimoUsuarioAlteracao(br.com.ggas.
	 * controleacesso.Usuario)
	 */
	@Override
	public void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao) {

		this.ultimoUsuarioAlteracao = ultimoUsuarioAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #
	 * setStatus(br.com.ggas.geral.EntidadeConteudo
	 * )
	 */
	@Override
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #getTipoCobranca()
	 */
	@Override
	public EntidadeConteudo getTipoCobranca() {

		return tipoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #setTipoCobranca(br.com.ggas.geral.
	 * EntidadeConteudo)
	 */
	@Override
	public void setTipoCobranca(EntidadeConteudo tipoCobranca) {

		this.tipoCobranca = tipoCobranca;
	}

	/**
	 * @return the listaParcelamentoItem
	 */
	@Override
	public Collection<ParcelamentoItem> getListaParcelamentoItem() {

		return listaParcelamentoItem;
	}

	/**
	 * @param listaParcelamentoItem
	 *            the listaParcelamentoItem to set
	 */
	@Override
	public void setListaParcelamentoItem(Collection<ParcelamentoItem> listaParcelamentoItem) {

		this.listaParcelamentoItem = listaParcelamentoItem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * #getValorLiquido()
	 */
	@Override
	public String getValorLiquido() {

		BigDecimal valorLiquido = BigDecimal.ZERO;
		if(valorDesconto != null) {
			valorLiquido = valorAtualizado.subtract(valorDesconto);
		} else {
			valorLiquido = valorAtualizado;
		}
		return Util.converterCampoValorDecimalParaString("", valorLiquido, Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
	}
}
