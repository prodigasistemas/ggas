/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorParcelamentoImpl representa uma ControladorParcelamentoImpl no sistema.
 *
 * @since 05/04/2010
 *
 */

package br.com.ggas.cobranca.parcelamento.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.LazyInitializationException;
import org.hibernate.Query;
import org.joda.time.DateTime;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.ValorParcela;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.cobranca.parcelamento.DadosItemParcela;
import br.com.ggas.cobranca.parcelamento.DadosParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;
import br.com.ggas.cobranca.parcelamento.DadosRelatorioNotaPromissoria;
import br.com.ggas.cobranca.parcelamento.ItemParcelaVO;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.cobranca.parcelamento.ParcelamentoItem;
import br.com.ggas.cobranca.parcelamento.SubRelatorioParcelaVO;
import br.com.ggas.cobranca.perfilparcelamento.ControladorPerfilParcelamento;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.contabil.LancamentoContabilAnaliticoVO;
import br.com.ggas.contabil.LancamentoContabilVO;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.EncargoTributario;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoARealizarImpl;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DadosAuditoriaUtil;
import br.com.ggas.util.DiaOrdinal;
import br.com.ggas.util.Extenso;
import br.com.ggas.util.Pair;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.cobranca.parcelamento.ParcelaVO;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 *
 */
class ControladorParcelamentoImpl extends ControladorNegocioImpl implements ControladorParcelamento {


	private static final int TRINTA_DIAS = 30;

	private static final String WHERE = " where ";

	private static final String FROM = " from ";

	private static final int ESCALA_PARCELAS = 6;

	private static final int NUMERO_DIGITOS = 2;

	private static final int LIMITE_OBSERVACAO = 2;

	private static final int POR_CENTAGEM = 100;

	private static final int PERIODO_PARCELAS = 30;

	private static final int ESCALA = 30;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final Logger LOG = Logger.getLogger(ControladorParcelamentoImpl.class);

	private static final String REFLECTION_GET = "get";
	
	private static final String CAMPO_PARCELAMENTO_PERFIL = "parcelamentoPerfil";
	
	private static final String CAMPO_ENDERECOS = "enderecos";
	
	private static final String CAMPO_CLIENTE = "cliente";
	
	private static final String CAMPO_ID_CLIENTE = "idCliente";
	
	private static final String CAMPO_STATUS = "status";
	
	private static final String CAMPO_ID_PERFIL_PARCELAMENTO = "idPerfilParcelamento";
	
	private static final String CAMPO_NUMERO_PARCELAS = "numeroParcelas";
	
	private static final String CAMPO_VENCIMENTO_INICIAL = "vencimentoInicial";
	
	private static final String CAMPO_SEGMENTO = "segmento";

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (Parcelamento) ServiceLocator.getInstancia().getBeanPorID(Parcelamento.BEAN_ID_PARCELAMENTO);
	}

	/**
	 * Criar parcelamento item.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarParcelamentoItem() {

		return (ParcelamentoItem) ServiceLocator.getInstancia().getBeanPorID(ParcelamentoItem.BEAN_ID_PARCELAMENTO_ITEM);
	}

	/**
	 * Criar dados parcelamento.
	 *
	 * @return the dados parcelamento
	 */
	public DadosParcelamento criarDadosParcelamento() {

		return (DadosParcelamento) ServiceLocator.getInstancia().getBeanPorID(DadosParcelamento.BEAN_ID_DADOS_PARCELAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#criarValorParcela()
	 */
	@Override
	public Object criarValorParcela() {

		return ServiceLocator.getInstancia().getBeanPorID(ValorParcela.BEAN_ID_VALOR_PARCELA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#criarDadosParcelas()
	 */
	@Override
	public Object criarDadosParcelas() {

		return ServiceLocator.getInstancia().getBeanPorID(DadosParcelas.BEAN_ID_DADOS_PARCELAS);
	}

	/**
	 * Criar dados item parcela.
	 *
	 * @return the object
	 */
	public Object criarDadosItemParcela() {

		return ServiceLocator.getInstancia().getBeanPorID(DadosItemParcela.BEAN_ID_DADOS_ITEM_PARCELA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#criarDadosGeraisParcelas()
	 */
	@Override
	public Object criarDadosGeraisParcelas() {

		return ServiceLocator.getInstancia().getBeanPorID(DadosGeraisParcelas.BEAN_ID_DADOS_GERAIS_PARCELAS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#criarFatura()
	 */
	@Override
	public EntidadeNegocio criarFatura() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Fatura.BEAN_ID_FATURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#criarFaturaGeral()
	 */
	@Override
	public EntidadeNegocio criarFaturaGeral() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FaturaGeral.BEAN_ID_FATURA_GERAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#criarFaturaItem()
	 */
	@Override
	public EntidadeNegocio criarFaturaItem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FaturaItem.BEAN_ID_FATURA_ITEM);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Parcelamento.BEAN_ID_PARCELAMENTO);
	}

	public Class<?> getClasseEntidadeParcelamentoItem() {

		return ServiceLocator.getInstancia().getClassPorID(ParcelamentoItem.BEAN_ID_PARCELAMENTO_ITEM);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeTipoDocumento() {

		return ServiceLocator.getInstancia().getClassPorID(TipoDocumento.BEAN_ID_TIPO_DOCUMENTO);
	}

	public Class<?> getClasseEntidadeFinanciamentoTipo() {

		return ServiceLocator.getInstancia().getClassPorID(FinanciamentoTipo.BEAN_ID_FINANCIAMENTO_TIPO);
	}

	public Class<?> getClasseEntidadeLancamentoItemContabil() {

		return ServiceLocator.getInstancia().getClassPorID(LancamentoItemContabil.BEAN_ID_LANCAMENTO_ITEM_CONTABIL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#obterLancamentoItemContabil(java.lang.Long)
	 */
	@Override
	public LancamentoItemContabil obterLancamentoItemContabil(Long chavePrimaria) throws NegocioException {

		return (LancamentoItemContabil) super.obter(chavePrimaria, getClasseEntidadeLancamentoItemContabil());
	}

	private ControladorIntegracao getControladorIntegracao() {

		return (ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
	}

	private ControladorFatura getControladorFatura() {

		return (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Credito Debito.
	 *
	 * @return ControladorCreditoDebito O
	 *         controlador de Credito Debito.
	 */
	private ControladorCreditoDebito getControladorCreditoDebito() {

		return (ControladorCreditoDebito) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * obterDadosExibicaoEfetuarParcelamento(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public DadosParcelamento obterDadosExibicaoEfetuarParcelamento(Long idCliente, Long idPontoConsumo) throws IllegalAccessException,
					InvocationTargetException, NoSuchMethodException, GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		DadosParcelamento dadosParcelamento = this.criarDadosParcelamento();
		Map<String, Object> filtro = new HashMap<>();

		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorPerfilParcelamento controladorPerfilParcelamento = ServiceLocator.getInstancia().getControladorPerfilParcelamento();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		Long idDocumentoFatura = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
		Long idDocumentoNotaDebito = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		PontoConsumo pontoConsumo = null;
		ContratoPontoConsumo contratoPontoConsumo = null;
		if ((idCliente == null || idCliente == 0) && (idPontoConsumo == null || idPontoConsumo == 0)) {
			throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_CLIENTE_IMOVEL_SELECIONADO, true);
		} else {
			Collection<Fatura> listaFatura = null;
			Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
			List<Map<String, Object>> dados = null;

			Map<String, Object[]> parametros = new HashMap<>();

			String codigoSituacaoPagamentoPendente = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
			String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

			Long[] arrayPagamentoFatura = {Long.valueOf(codigoSituacaoPagamentoPendente), Long
							.valueOf(codigoSituacaoPagamentoParcialmentePago)};
			Long[] arrayTipoDocumento = {idDocumentoFatura, idDocumentoNotaDebito};

			parametros.put("arrayPagamento", arrayPagamentoFatura);
			parametros.put("arrayTipoDocumento", arrayTipoDocumento);

			if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				listaFatura = controladorFatura.consultarFaturasParaParcelamento(idCliente, idPontoConsumo, parametros, null);
				dados = this.obterListaFaturas(listaFatura, idDocumentoNotaDebito);

				Map<String, Object> filtroCreditoDebito = new HashMap<>();
				filtroCreditoDebito.put("chavesPontoConsumo", new Long[] {idPontoConsumo});

				Long[] arraySituacao = (Long[]) parametros.get("arraySituacao");
				filtroCreditoDebito.put("arraySituacao", arraySituacao);

				Long[] arrayPagamento = (Long[]) parametros.get("arrayPagamento");
				filtroCreditoDebito.put("arrayPagamento", arrayPagamento);

				if ((idCliente != null) && (idCliente > 0)) {
					filtroCreditoDebito.put("idCliente", idCliente);
				}

				// Restrição de tarifa autorizada (Alçada)

				String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
				if (statusAutorizado != null) {
					filtroCreditoDebito.put(CAMPO_STATUS, Long.valueOf(statusAutorizado));
				}
				// FIXME: Os valores dos débitos/créditos devem vir com o valor presente (desconsiderar os juros de
				// financiamento/parcelamento)
				listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtroCreditoDebito);

				pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, "cep", "imovel", CAMPO_SEGMENTO);
				contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
									.getChavePrimaria());
				}

				dadosParcelamento.setPontoConsumo(pontoConsumo);
				filtro.put("idSegmento", pontoConsumo.getSegmento().getChavePrimaria());
			}
			if (dados != null && !dados.isEmpty()) {
				dadosParcelamento.setListaFatura(dados);
			}
			if (listaCreditoDebito != null && !listaCreditoDebito.isEmpty()) {
				dadosParcelamento.getListaCreditoDebito().addAll(listaCreditoDebito);
			}

			filtro.put("exibirVencidos", false);
			filtro.put("habilitado", true);

			Collection<ParcelamentoPerfil> listaPerfil = controladorPerfilParcelamento.consultarPerfilParcelamento(filtro);
			if (listaPerfil != null && !listaPerfil.isEmpty()) {
				dadosParcelamento.getListaPerfil().addAll(listaPerfil);
			}
		}

		if (contratoPontoConsumo != null) {
			ContratoPontoConsumoItemFaturamento cpcItemFaturamento = this.obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);
			if (cpcItemFaturamento == null) {
				dadosParcelamento.setBloquearCobrancaFatura(Boolean.TRUE);
			}
		}

		// evitando o lazy nessa propriedade.
		BeanUtils.getProperty(dadosParcelamento.getPontoConsumo(), "quadraFace");

		return dadosParcelamento;
	}

	/**
	 * Obter lista faturas.
	 *
	 * @param listaFaturas
	 *            the lista faturas
	 *            the id documento fatura
	 * @param idDocumentoNotaDebito
	 *            the id documento nota debito
	 * @return the list
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private List<Map<String, Object>> obterListaFaturas(Collection<Fatura> listaFaturas, Long idDocumentoNotaDebito)
			throws GGASException {

		List<Map<String, Object>> listaFaturaParcelamento = new ArrayList<>();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		if ((listaFaturas != null) && (!listaFaturas.isEmpty())) {
			for (Fatura fatura : listaFaturas) {
				Map<String, Object> dados = new HashMap<>();

				dados.put("chavePrimaria", fatura.getChavePrimaria());
				dados.put("idTipoDocumento", fatura.getTipoDocumento().getChavePrimaria());
				dados.put("tipoDocumento", fatura.getTipoDocumento().getDescricao());
				if (fatura.getDataEmissao() != null) {
					dados.put("dataEmissao", fatura.getDataEmissao());
				}
				if (fatura.getDataVencimento() != null) {
					dados.put("dataVencimento", fatura.getDataVencimento());
				}
				dados.put("valorOriginal", fatura.getValorTotal());

				Pair<BigDecimal, BigDecimal> calcularSaldoFatura =
								controladorFatura.calcularSaldoFatura(fatura, true, true);

				BigDecimal valorSaldo = calcularSaldoFatura.getFirst();
				BigDecimal valorSaldoCorrigido = calcularSaldoFatura.getSecond();
				BigDecimal valorCorrecao = valorSaldoCorrigido.subtract(valorSaldo);

				dados.put("valorSaldo", valorSaldo);
				dados.put("valorTotal", valorSaldoCorrigido);
				dados.put("valorCorrecao", valorCorrecao);

				if (fatura.getSituacaoPagamento() != null) {
					dados.put("situacaoPagamento", fatura.getSituacaoPagamento().getDescricao());
				}
				if (fatura.getPontoConsumo() != null) {
					dados.put("idPontoConsumo", fatura.getPontoConsumo().getChavePrimaria());
				}
				listaFaturaParcelamento.add(dados);
			}
		}

		return listaFaturaParcelamento;
	}

	/**
	 * Validar dados perfil parcelamento.
	 *
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosPerfilParcelamento(Map<String, Object> dados) throws NegocioException {

		this.validarCamposObrigatorios(dados);
		ControladorPerfilParcelamento controladorPerfilParcelamento = ServiceLocator.getInstancia().getControladorPerfilParcelamento();

		ParcelamentoPerfil perfil = null;
		Long idPerfilParcelamento = (Long) dados.get(CAMPO_ID_PERFIL_PARCELAMENTO);
		if (idPerfilParcelamento != null && idPerfilParcelamento > 0) {
			perfil = (ParcelamentoPerfil) controladorPerfilParcelamento.obter(idPerfilParcelamento);
		}

		if (perfil != null) {
			BigDecimal percentualDesconto = (BigDecimal) dados.get("percentualDesconto");
			if (percentualDesconto != null && perfil.isIndicadorDesconto() && percentualDesconto.compareTo(perfil.getValorDesconto()) > 0) {
				throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_VALOR_DESCONTO, true);
			}
			Integer numeroParcelas = (Integer) dados.get(CAMPO_NUMERO_PARCELAS);
			if ((numeroParcelas != null) && (numeroParcelas > perfil.getMaximoParcelas())) {
				throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_NUMERO_PARCELAS, true);
			}
		}

		Date vencimentoInicial = (Date) dados.get(CAMPO_VENCIMENTO_INICIAL);

		if (Util.compararDatas(vencimentoInicial, Calendar.getInstance().getTime()) < 0) {
			throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_VENCIMENTO_INICIAL_MENOR, true);
		}
	}

	/**
	 * Validar campos obrigatorios.
	 *
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarCamposObrigatorios(Map<String, Object> dados) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		Long idPerfilParcelamento = (Long) dados.get(CAMPO_ID_PERFIL_PARCELAMENTO);
		if (idPerfilParcelamento == null || idPerfilParcelamento == 0) {
			stringBuilder.append(Parcelamento.ROTULO_PARCELAMENTO_PERFIL_PARCELAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		BigDecimal valorTotalDebito = (BigDecimal) dados.get("valorTotalDebito");
		if (valorTotalDebito == null || valorTotalDebito.compareTo(BigDecimal.ZERO) == 0) {
			stringBuilder.append(Parcelamento.ROTULO_PARCELAMENTO_VALOR_DEBITO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		BigDecimal valorTotalLiquido = (BigDecimal) dados.get("valorTotalLiquido");
		if (valorTotalLiquido == null || valorTotalLiquido.compareTo(BigDecimal.ZERO) == 0) {
			stringBuilder.append(Parcelamento.ROTULO_PARCELAMENTO_TOTAL_LIQUIDO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		Integer numeroParcelas = (Integer) dados.get(CAMPO_NUMERO_PARCELAS);
		if (numeroParcelas == null || numeroParcelas == 0) {
			stringBuilder.append(Parcelamento.ROTULO_PARCELAMENTO_NUMERO_PARCELAS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		Boolean cobrancaEmConta = (Boolean) dados.get("cobrancaEmConta");
		if (!cobrancaEmConta) {
			Date vencimentoInicial = (Date) dados.get(CAMPO_VENCIMENTO_INICIAL);
			if (vencimentoInicial == null) {
				stringBuilder.append(Parcelamento.ROTULO_PARCELAMENTO_VENCIMENTO_INICIAL);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			Integer periodicidadeParcelas = (Integer) dados.get("periodicidadeParcelas");
			if (periodicidadeParcelas == null || periodicidadeParcelas == 0) {
				stringBuilder.append(Parcelamento.ROTULO_PARCELAMENTO_PERIODICIDADE_PARCELAS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
			throw new NegocioException(erros);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * gerarParcelas(java.util.Map)
	 */
	@Override
	public DadosGeraisParcelas gerarParcelas(Map<String, Object> dados, Long[] chavesFatura, Long[] chavesCreditoDebito)
					throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Map<LancamentoItemContabil, ItemParcelaVO> mapaValoresPorRubrica = new HashMap<>();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorPerfilParcelamento controladorPerfilParcelamento = ServiceLocator.getInstancia().getControladorPerfilParcelamento();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		String chaveTipoCredito = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.TIPO_CREDITO);
		EntidadeConteudo tipoCredito = controladorEntidadeConteudo.obter(Long.valueOf(chaveTipoCredito));

		this.validarDadosPerfilParcelamento(dados);

		Collection<ValorParcela> listaParcelas = new ArrayList<>();
		DadosGeraisParcelas dadosGerais = (DadosGeraisParcelas) this.criarDadosGeraisParcelas();

		Boolean cobrancaEmConta = (Boolean) dados.get("cobrancaEmConta");
		dadosGerais.setCobrancaEmFatura(cobrancaEmConta.booleanValue());

		Boolean indicadorNotaPromissoria = (Boolean) dados.get("indicadorNotaPromissoria");
		dadosGerais.setEmiteNotaPromissoria(indicadorNotaPromissoria.booleanValue());

		Boolean indicadorConfissaoDivida = (Boolean) dados.get("indicadorConfissaoDivida");
		dadosGerais.setEmiteTermoConfissaoDivida(indicadorConfissaoDivida.booleanValue());

		Long idCliente = (Long) dados.get(CAMPO_ID_CLIENTE);
		dadosGerais.setIdFiador(idCliente);

		Long idPontoConsumo = (Long) dados.get("idPontoConsumo");
		dadosGerais.setIdPontoConsumo(idPontoConsumo);

		Date vencimentoInicial = (Date) dados.get(CAMPO_VENCIMENTO_INICIAL);
		dadosGerais.setVencimentoInicial(vencimentoInicial);

		Integer periodicidadeParcelas = (Integer) dados.get("periodicidadeParcelas");
		dadosGerais.setPeriodicidadeParcelas(periodicidadeParcelas);
		BigDecimal percDesconto = (BigDecimal) dados.get("percentualDesconto");
		dadosGerais.setPercentualDesconto(percDesconto);

		BigDecimal valorDesconto = (BigDecimal) dados.get("valorDesconto");
		dadosGerais.setValorDesconto(valorDesconto);

		BigDecimal somatorioTodosDebitos = BigDecimal.ZERO;
		BigDecimal somatorioTodosCreditos = BigDecimal.ZERO;
		Set<LancamentoItemContabil> listaCreditos = new HashSet<>();

		if (chavesFatura != null && chavesFatura.length > 0) {
			Collection<Fatura> listaFatura = controladorFatura.consultarFaturasPorChaves(chavesFatura);

			for (Fatura fatura : listaFatura) {
				Collection<FaturaItem> listaFaturaItem = fatura.getListaFaturaItem();
				for (FaturaItem faturaItem : listaFaturaItem) {
					// Agrupa os valores de acordo com o Lancamento Contabil
					if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getLancamentoItemContabil() != null) {
						if (mapaValoresPorRubrica.containsKey(faturaItem.getRubrica().getLancamentoItemContabil())) {
							ItemParcelaVO item = mapaValoresPorRubrica.get(faturaItem.getRubrica().getLancamentoItemContabil());
							item = mapaValoresPorRubrica.get(faturaItem.getRubrica().getLancamentoItemContabil());
							item.getListaFaturas().add(faturaItem);
							item.setValor(item.getValor().add(faturaItem.getValorTotal()));
							mapaValoresPorRubrica.put(faturaItem.getRubrica().getLancamentoItemContabil(), item);

						} else {
							ItemParcelaVO item = new ItemParcelaVO();
							item.setLancamentoItemContabil(faturaItem.getRubrica().getLancamentoItemContabil());
							item.setValor(faturaItem.getValorTotal());
							item.getListaFaturas().add(faturaItem);
							mapaValoresPorRubrica.put(faturaItem.getRubrica().getLancamentoItemContabil(), item);
						}

						if (faturaItem.getRubrica().getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == tipoCredito
										.getChavePrimaria()) {
							somatorioTodosCreditos = somatorioTodosCreditos.add(faturaItem.getValorTotal());
							listaCreditos.add(faturaItem.getRubrica().getLancamentoItemContabil());
						} else {
							somatorioTodosDebitos = somatorioTodosDebitos.add(faturaItem.getValorTotal());
						}
					} else {
						LancamentoItemContabil lancamentoItemContabil = faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado()
										.getRubrica().getLancamentoItemContabil();
						if (mapaValoresPorRubrica.containsKey(lancamentoItemContabil)) {
							ItemParcelaVO item = mapaValoresPorRubrica.get(lancamentoItemContabil);
							item = mapaValoresPorRubrica.get(lancamentoItemContabil);
							item.getListaFaturas().add(faturaItem);
							item.setValor(item.getValor().add(faturaItem.getValorTotal()));
							mapaValoresPorRubrica.put(lancamentoItemContabil, item);

						} else {
							ItemParcelaVO item = new ItemParcelaVO();
							item.setLancamentoItemContabil(lancamentoItemContabil);
							item.setValor(faturaItem.getValorTotal());
							item.getListaFaturas().add(faturaItem);
							mapaValoresPorRubrica.put(lancamentoItemContabil, item);
						}

						if (faturaItem.getCreditoDebitoARealizar() != null
										&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado() != null
										&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getCreditoOrigem() != null) {

							somatorioTodosCreditos = somatorioTodosCreditos.add(faturaItem.getValorTotal());
							listaCreditos.add(faturaItem.getRubrica().getLancamentoItemContabil());
						} else {
							somatorioTodosDebitos = somatorioTodosDebitos.add(faturaItem.getValorTotal());
						}
					}
				}
				fatura.getListaFaturaItem().addAll(listaFaturaItem);
			}
			dadosGerais.getListaFatura().addAll(listaFatura);
		}

		if (chavesCreditoDebito != null && chavesCreditoDebito.length > 0) {

			Map<String, Object> filtro = new HashMap<>();
			filtro.put("chavesCreditoDebitoNegociado", chavesCreditoDebito);

			filtro.put("chavesPontoConsumo", new Long[] {dadosGerais.getIdPontoConsumo()});

			String codigoSituacaoPagamentoPendente = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_SITUACAO_PAGAMENTO_PENDENTE);
			String codigoSituacaoPagamentoParcialmentePago = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_SITUACAO_PAGAMENTO_PARCIALMENTE_PAGO);

			Long[] arrayPagamento = {Long.valueOf(codigoSituacaoPagamentoPendente), Long.valueOf(codigoSituacaoPagamentoParcialmentePago)};
			filtro.put("arrayPagamento", arrayPagamento);

			// Restrição de tarifa autorizada (Alçada)
			String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
			if (statusAutorizado != null) {
				filtro.put(CAMPO_STATUS, Long.valueOf(statusAutorizado));
			}

			Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorCreditoDebito.consultarCreditosDebitos(filtro);

			for (CreditoDebitoARealizar creditoDebito : listaCreditoDebito) {
				// Se for credito
				if (creditoDebito.getCreditoDebitoNegociado() != null) {
					if (creditoDebito.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
						somatorioTodosCreditos = somatorioTodosCreditos.add(creditoDebito.getValor());
					} else {
						somatorioTodosDebitos = somatorioTodosDebitos.add(creditoDebito.getValor());
					}

					if (creditoDebito.getCreditoDebitoNegociado().getRubrica() != null
									&& creditoDebito.getCreditoDebitoNegociado().getRubrica().getLancamentoItemContabil() != null) {

						LancamentoItemContabil lancContabil = creditoDebito.getCreditoDebitoNegociado().getRubrica()
										.getLancamentoItemContabil();

						if (mapaValoresPorRubrica.containsKey(lancContabil)) {
							ItemParcelaVO item = mapaValoresPorRubrica.get(lancContabil);
							item.getListaCreditoDebito().add(creditoDebito);
							item.setValor(item.getValor().add(creditoDebito.getValor()));
							mapaValoresPorRubrica.put(lancContabil, item);
						} else {
							ItemParcelaVO item = new ItemParcelaVO();
							item.setLancamentoItemContabil(lancContabil);
							item.setValor(creditoDebito.getValor());
							item.getListaCreditoDebito().add(creditoDebito);
							mapaValoresPorRubrica.put(lancContabil, item);
						}
					}
				}
			}

			dadosGerais.getListaCreditoDebito().addAll(listaCreditoDebito);
		}

		// Guarda as faturas itens e os créditos/débitos que compoem o parcelamento agrupados por lançamento contábil.
		// Usado durante o salvamento do parcelamento para registrar os lançamentos contábeis.

		dadosGerais.setMapaValoresPorRubrica(mapaValoresPorRubrica);

		Long idPerfilParcelamento = (Long) dados.get(CAMPO_ID_PERFIL_PARCELAMENTO);
		if (idPerfilParcelamento != null && idPerfilParcelamento > 0) {

			ParcelamentoPerfil perfil = (ParcelamentoPerfil) controladorPerfilParcelamento.obter(idPerfilParcelamento);

			if ((perfil.getValorDesconto() != null) && (percDesconto != null) && (percDesconto.compareTo(perfil.getValorDesconto()) > 0)) {
				throw new NegocioException(PERCENTUAL_POR_PARCELAMENTO_MAIOR_PERMITIDO_PERFIL, true);
			}

			dadosGerais.setParcelamentoPerfil(perfil);
			dadosGerais.setIndicarJuros(perfil.isIndicadorJuros());
			BigDecimal valorTotalLiquido = (BigDecimal) dados.get("valorTotalLiquido");
			Integer numeroParcelas = (Integer) dados.get(CAMPO_NUMERO_PARCELAS);
			dadosGerais.setNumeroPrestacoes(numeroParcelas);

			if (cobrancaEmConta) {
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(dadosGerais.getIdPontoConsumo(),  CAMPO_SEGMENTO);
				ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
									.getChavePrimaria());
				}

				if (contratoPontoConsumo != null) {
					ContratoPontoConsumoItemFaturamento cpcItemFaturamento = this
									.obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);

					if (cpcItemFaturamento != null) {
						DateTime dataAtual = new DateTime(Calendar.getInstance().getTime());
						Integer numeroDiaVencimento = cpcItemFaturamento.getNumeroDiaVencimento();
						// FIXME : Tratar opção de data: dias corridos, dias úteis ou dia fixo
						if (numeroDiaVencimento != null) {
							// FIXME: verificar parâmetro de quantidade de dias de antecedência para definição do vencimento da
							// fatura no módulo de faturamento.
							if (numeroDiaVencimento <= dataAtual.getDayOfMonth()) {
								Calendar calendar = new GregorianCalendar();
								calendar.set(dataAtual.getYear(), dataAtual.getMonthOfYear() - 1, numeroDiaVencimento);
								dataAtual = new DateTime(calendar.getTime());
								dataAtual = dataAtual.plusMonths(1);
								vencimentoInicial = dataAtual.toDate();
								dadosGerais.setVencimentoInicial(vencimentoInicial);
							} else {
								Calendar calendar = new GregorianCalendar();
								calendar.set(dataAtual.getYear(), dataAtual.getMonthOfYear(), numeroDiaVencimento);
								dataAtual = new DateTime(calendar.getTime());
							}
						} else {
							vencimentoInicial = dataAtual.toDate();
							dadosGerais.setVencimentoInicial(vencimentoInicial);
						}
						// TODO: Tratar mês civil
						periodicidadeParcelas = contratoPontoConsumo.getPeriodicidade().getQuantidadeDias();
						dadosGerais.setPeriodicidadeParcelas(periodicidadeParcelas);
					} else {
						throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_ITEM_FATURA, Boolean.TRUE);
					}
				}
			}

			if (perfil.isIndicadorJuros()) {

				EntidadeConteudo periodicidadeJuros = controladorEntidadeConteudo
								.obterEntidadeConteudoPorConstanteSistema(Constantes.C_JUROS_MENSAL);

				listaParcelas = controladorCobranca.calcularSAC(valorTotalLiquido, numeroParcelas, perfil.getTaxaJuros(),
								dadosGerais.getVencimentoInicial(), periodicidadeParcelas,
								Integer.parseInt(periodicidadeJuros.getCodigo()), Boolean.TRUE, Boolean.TRUE);
			} else {
				// Não aplicará juros nas parcelas
				BigDecimal parcela = (valorTotalLiquido.divide(new BigDecimal(numeroParcelas), ESCALA_PARCELAS,
						RoundingMode.HALF_UP)).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
				BigDecimal valorInicial = BigDecimal.ZERO;
				for (int i = 0; i < numeroParcelas; i++) {
					ValorParcela valorParcela = (ValorParcela) this.criarValorParcela();
					if (valorInicial.compareTo(BigDecimal.ZERO) == 0) {
						valorInicial = valorTotalLiquido;
					} else {
						valorInicial = valorInicial.subtract(parcela);
					}
					if (i == (numeroParcelas - 1)) {
						parcela = valorInicial;
					}
					valorParcela.setValorPagar(parcela);
					valorParcela.setAmortizacao(parcela);
					valorParcela.setSaldo(valorInicial);

					DateTime novaData = new DateTime(dadosGerais.getVencimentoInicial());
					// TODO: Tratar mês civil
					novaData = novaData.plusDays(i * dadosGerais.getPeriodicidadeParcelas());
					valorParcela.setDataVencimento(novaData.toDate());

					listaParcelas.add(valorParcela);
				}
			}

			// FIXME: O cálculo de percentual não está correto, está considerando débito e crédito como sendo só débito

			Map<String, BigDecimal> valores = new HashMap<>();
			BigDecimal percentPartValorOrigem = this.calcularPercentualParticipacaoOriginal(dadosGerais, valores);

			if (listaParcelas != null && !listaParcelas.isEmpty()) {

				String codigoLAICJurosParcelamento = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO);

				LancamentoItemContabil laicJurosParcelamento = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICJurosParcelamento));

				String codigoLAICJurosMora = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_MORA);

				LancamentoItemContabil laicJurosMora = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICJurosMora));

				String codigoLAICParcelamento = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_PARCELAMENTO);

				LancamentoItemContabil laicParcelamento = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICParcelamento));

				BigDecimal acumularValor = new BigDecimal(0);
				BigDecimal acumularValorJurosMora = new BigDecimal(0);
				Collection<DadosParcelas> listaDadosParcelas = new ArrayList<>();
				int qtde = 1;
				BigDecimal valorTotalJuros = BigDecimal.ZERO;
				BigDecimal valorTotalAPagar = BigDecimal.ZERO;
				for (ValorParcela valorParcela : listaParcelas) {
					DadosParcelas dadosParcela = (DadosParcelas) this.criarDadosParcelas();
					dadosParcela.setNumeroParcela(qtde);

					if (valorParcela.getAmortizacao() != null) {
						dadosParcela.setAmortizacao(valorParcela.getAmortizacao().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));

						BigDecimal valor = (dadosParcela.getAmortizacao().multiply(percentPartValorOrigem)).setScale(DUAS_CASAS_DECIMAIS,
										RoundingMode.HALF_UP);
						BigDecimal valorJurosMora = dadosParcela.getAmortizacao().subtract(valor);

						acumularValor = acumularValor.add(valor);
						acumularValorJurosMora = acumularValorJurosMora.add(valorJurosMora);
						this.criarEAdicionarDadosItemParcela(dadosParcela, laicParcelamento, valor, null, null, null);
						if (valorJurosMora.compareTo(new BigDecimal(0)) > 0) {
							this.criarEAdicionarDadosItemParcela(dadosParcela, laicJurosMora, valorJurosMora, null, null, null);
						}

					} else {
						dadosParcela.setAmortizacao(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getJuros() != null) {
						dadosParcela.setJuros(valorParcela.getJuros().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
						this.criarEAdicionarDadosItemParcela(dadosParcela, laicJurosParcelamento, dadosParcela.getJuros(), null, null, null);
						valorTotalJuros = valorTotalJuros.add(dadosParcela.getJuros());
					} else {
						dadosParcela.setJuros(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getSaldo() != null) {
						dadosParcela.setSaldoInicial(valorParcela.getSaldo().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
					} else {
						dadosParcela.setSaldoInicial(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getValorPagar() != null) {
						dadosParcela.setTotal(valorParcela.getValorPagar().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
						valorTotalAPagar = valorTotalAPagar.add(dadosParcela.getTotal());
					} else {
						dadosParcela.setTotal(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getDataVencimento() != null) {
						dadosParcela.setDataVencimento(valorParcela.getDataVencimento());
					}

					listaDadosParcelas.add(dadosParcela);
					qtde++;
				}

				dadosGerais.setValorTotalAPagar(valorTotalAPagar);
				dadosGerais.setValorTotalJuros(valorTotalJuros);
				dadosGerais.getListaDadosParcelas().addAll(listaDadosParcelas);
			}

			String parametroCodigoCobranca = Constantes.C_FORMA_COBRANCA_FATURA;
			if (!dadosGerais.isCobrancaEmFatura()) {
				parametroCodigoCobranca = Constantes.C_FORMA_COBRANCA_NOTA_DEBITO;
			}

			controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
			dadosGerais.setFormaCobranca((EntidadeConteudo) controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(parametroCodigoCobranca))));

		}

		return dadosGerais;
	}

	/**
	 * Método responsável por calcular o
	 * percentual de participação original do
	 * parcelamento.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param valores
	 *            the valores
	 * @return the big decimal
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private BigDecimal calcularPercentualParticipacaoOriginal(DadosGeraisParcelas dadosGerais, Map<String, BigDecimal> valores)
					throws GGASException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String chaveTipoDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);
		String chaveTipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);
		EntidadeConteudo tipoDebito = controladorEntidadeConteudo.obter(Long.valueOf(chaveTipoDebito));
		EntidadeConteudo tipoCredito = controladorEntidadeConteudo.obter(Long.valueOf(chaveTipoCredito));

		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		BigDecimal valorTotalFatura = BigDecimal.ZERO;
		BigDecimal valorTotalDebitos = BigDecimal.ZERO;
		BigDecimal valorTotalCredito = BigDecimal.ZERO;
		BigDecimal valorCorrecao = BigDecimal.ZERO;
		BigDecimal valorJurosMora = BigDecimal.ZERO;
		BigDecimal valorMulta = BigDecimal.ZERO;

		// calcula valor total das faturas
		if (dadosGerais.getListaFatura() != null && !dadosGerais.getListaFatura().isEmpty()) {
			Long idDocumentoFatura = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
			Long idDocumentoNotaDebito = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));

			for (Fatura fatura : dadosGerais.getListaFatura()) {
				if ((fatura.getTipoDocumento().getChavePrimaria() == idDocumentoFatura)
								|| (fatura.getTipoDocumento().getChavePrimaria() == idDocumentoNotaDebito)) {
					valorTotalFatura = valorTotalFatura.add(controladorFatura.calcularSaldoFatura(fatura));

					DebitosACobrar debitosACobrar = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, Calendar
									.getInstance().getTime(), fatura.getValorTotal(), Boolean.TRUE, Boolean.TRUE);
					if (debitosACobrar != null && debitosACobrar.getMulta() != null) {
						valorMulta = valorMulta.add(debitosACobrar.getMulta());
					}

					if (debitosACobrar != null && debitosACobrar.getJurosMora() != null) {
						valorJurosMora = valorJurosMora.add(debitosACobrar.getJurosMora());
					}

				} else {
					valorTotalFatura = valorTotalFatura.subtract(controladorFatura.calcularSaldoFatura(fatura));
				}
			}
		}

		// calcula o valor total dos creditos e debitos
		if (dadosGerais.getListaCreditoDebito() != null && !dadosGerais.getListaCreditoDebito().isEmpty()) {
			for (CreditoDebitoARealizar creditoDebitoARealizar : dadosGerais.getListaCreditoDebito()) {
				Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
								.obterCDDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				CreditoDebitoDetalhamento debitoDetalhamento = ((List<CreditoDebitoDetalhamento>) listaCDDetalhamento).get(0);
				EntidadeConteudo tipoDebitoCredito = debitoDetalhamento.getLancamentoItemContabil().getTipoCreditoDebito();

				if (tipoDebito.getChavePrimaria() == tipoDebitoCredito.getChavePrimaria()) {
					valorTotalDebitos = valorTotalDebitos.add(creditoDebitoARealizar.getValor());
				}

				if (tipoCredito.getChavePrimaria() == tipoDebitoCredito.getChavePrimaria()) {
					valorTotalCredito = valorTotalCredito.add(creditoDebitoARealizar.getValor());
				}
			}
		}

		BigDecimal valorOrigem = (valorTotalFatura.add(valorTotalDebitos)).subtract(valorTotalCredito);
		BigDecimal valorAtualizado = ((valorTotalFatura.add(valorTotalDebitos)).subtract(valorTotalCredito)).add(valorCorrecao
						.add(valorMulta).add(valorJurosMora));

		valores.put("valorOrigem", valorOrigem);
		valores.put("valorAtualizado", valorAtualizado);

		return valorOrigem.divide(valorAtualizado, ESCALA, RoundingMode.HALF_UP);
	}

	/**
	 * Método responsável por criar Um ou Mais
	 * DadosItemParcela e adiciona-lo(s) ao
	 * DadosParcela.
	 *
	 * @param dadosParcela
	 *            the dados parcela
	 * @param lancamentoItemContabil
	 *            the lancamento item contabil
	 * @param valor
	 *            the valor
	 * @param mapaPercentuais
	 *            the mapa percentuais
	 * @param percentualParticipacao
	 *            the percentual participacao
	 * @param listaCreditos
	 *            the lista creditos
	 */
	private void criarEAdicionarDadosItemParcela(DadosParcelas dadosParcela, LancamentoItemContabil lancamentoItemContabil,
					BigDecimal valor, Map<LancamentoItemContabil, BigDecimal> mapaPercentuais, BigDecimal percentualParticipacao,
					Set<LancamentoItemContabil> listaCreditos) {

		DadosItemParcela dadosItemParcela = null;

		if (mapaPercentuais != null) {
			BigDecimal soma = BigDecimal.ZERO;
			Iterator<LancamentoItemContabil> itLaic = mapaPercentuais.keySet().iterator();
			for (Map.Entry<LancamentoItemContabil, BigDecimal> entry : mapaPercentuais.entrySet()) {

				LancamentoItemContabil laic = entry.getKey();
				dadosItemParcela = (DadosItemParcela) this.criarDadosItemParcela();
				dadosItemParcela.setLancamentoItemContabil(laic);

				if (itLaic.hasNext()) {
					BigDecimal valorBruto = valor.divide(percentualParticipacao, ESCALA, RoundingMode.HALF_DOWN);
					dadosItemParcela.setValor(valorBruto.multiply(entry.getValue()));
					if (listaCreditos != null && listaCreditos.contains(laic)) {
						soma = soma.subtract(dadosItemParcela.getValor());
					} else {
						soma = soma.add(dadosItemParcela.getValor());
					}
				} else {
					dadosItemParcela.setValor(valor.subtract(soma));
				}

				if (percentualParticipacao != null) {
					dadosItemParcela.setPercentualParticipacao(percentualParticipacao);
				}

				if (dadosItemParcela.getValor().compareTo(BigDecimal.ZERO) > 0) {
					dadosParcela.getListaItemParcela().add(dadosItemParcela);
				}

			}
		} else if (lancamentoItemContabil != null) {
			dadosItemParcela = (DadosItemParcela) this.criarDadosItemParcela();
			dadosItemParcela.setLancamentoItemContabil(lancamentoItemContabil);
			dadosItemParcela.setValor(valor);
			if (dadosItemParcela.getValor().compareTo(BigDecimal.ZERO) > 0) {
				dadosParcela.getListaItemParcela().add(dadosItemParcela);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * obterContratoPontoConsumoItemFaturamento
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo)
	 */
	@Override
	public ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamento(ContratoPontoConsumo contratoPontoConsumo)
					throws NegocioException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		ContratoPontoConsumoItemFaturamento cpcItemFaturamento = controladorContrato.consultarContratoPontoConsumoItemFaturamento(
						contratoPontoConsumo.getChavePrimaria(), itemFaturaGas);

		if (cpcItemFaturamento == null) {

			Long itemFaturaMargeDistribuicao = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO));

			cpcItemFaturamento = controladorContrato.consultarContratoPontoConsumoItemFaturamento(contratoPontoConsumo.getChavePrimaria(),
							itemFaturaMargeDistribuicao);

			if (cpcItemFaturamento == null) {
				Long itemFaturaTransporte = Long.valueOf(controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE));

				cpcItemFaturamento = controladorContrato.consultarContratoPontoConsumoItemFaturamento(
								contratoPontoConsumo.getChavePrimaria(), itemFaturaTransporte);
			}
		}

		return cpcItemFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #obterDebitoSituacao(java
	 * .lang.String)
	 */
	@Override
	public CreditoDebitoSituacao obterDebitoSituacao(String codigoSituacao) throws NegocioException {

		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();

		return controladorCobranca.obterCreditoDebitoSituacao(Long.valueOf(codigoSituacao));
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #obterTipoDocumentoNotasDebito
	 * ()
	 */
	@Override
	public TipoDocumento obterTipoDocumentoNotasDebito() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long tipoFatura = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		return (TipoDocumento) super.obter(tipoFatura, getClasseEntidadeTipoDocumento());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #salvarParcelamento(br.
	 * com.procenge.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas, boolean, boolean)
	 */
	@Override
	public byte[] salvarParcelamento(DadosGeraisParcelas dadosGerais,
					boolean indicadorNotaPromissoria, boolean indicadorConfissaoDivida)
					throws GGASException {

		Collection<Long> listaChaves = new ArrayList<>();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		// O sistema deverá gerar parcelas do tipo débitos a cobrar

		String codigoRubricaParcelamento = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_PARCELAMENTO_DEBITO);

		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		Rubrica rubrica = (Rubrica) controladorRubrica.obter(Long.parseLong(codigoRubricaParcelamento));
		CreditoDebitoNegociado debitoNegociado = null;

		if (dadosGerais != null && dadosGerais.getListaDadosParcelas() != null && !dadosGerais.getListaDadosParcelas().isEmpty()) {

			// Prepara e insere o crédito débito negociado.

			int qtdeTotalPrestacoes = dadosGerais.getListaDadosParcelas().size();
			debitoNegociado = this.carregarCreditoDebitoNegociado(dadosGerais, rubrica, qtdeTotalPrestacoes);
			super.inserir(debitoNegociado);
			int numeroPrestacao = 1;

			// regra para definir a periodicidade
			int ciclo = 1;
			// ciclo atual na contagem
			int valorReferenciaFaturamento = 0;
			// ultimo ano mes faturado

			int numeroMaximoCiclo = 1;
			String referenciaFaturamento = null;

			if (debitoNegociado != null && debitoNegociado.getPontoConsumo() != null) {

				PontoConsumo pontoConsumo = debitoNegociado.getPontoConsumo();

				ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
									.getChavePrimaria());
				}

				if (contratoPontoConsumo != null) {
					numeroMaximoCiclo = contratoPontoConsumo.getPeriodicidade().getQuantidadeCiclo();
				}

				if (pontoConsumo.getRota() != null && pontoConsumo.getRota().getGrupoFaturamento() != null) {
					referenciaFaturamento = String.valueOf(pontoConsumo.getRota().getGrupoFaturamento().getAnoMesReferencia());

				} else {
					referenciaFaturamento = (String) controladorParametroSistema
									.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);
				}

				String codigoPeriodicidadeContrato = controladorCreditoDebito.obterPeriodicidadeContratoPontoConsumo(
								dadosGerais.getIdPeriodicidade(), pontoConsumo.getChavePrimaria());
				// periodicidade informada no contrato(Se houver)

				valorReferenciaFaturamento = controladorCreditoDebito.obterValorReferenciaFaturamento(Calendar.getInstance().getTime(),
								referenciaFaturamento);

				// -----verifica o ciclo -------
				DateTime dataCobranca = new DateTime(Calendar.getInstance().getTime());
				// data de inicio de cobrança (informada na tela)

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(Calendar.getInstance().getTime());

				int diaBaseCobranca = dataCobranca.getDayOfMonth();
				if (codigoPeriodicidadeContrato != null) {
					int basePeriodicidade = Integer.parseInt(codigoPeriodicidadeContrato);

					// FIXME: URGENTE está misturando periodicidade de cobrança com dia do mês
					while (diaBaseCobranca > basePeriodicidade) {
						ciclo = ciclo + 1;
						basePeriodicidade = basePeriodicidade + Integer.parseInt(codigoPeriodicidadeContrato);
					}
				}
			}

			Map<String, Integer> mapaReferenciaCiclo = null;

			for (DadosParcelas dadoParcela : dadosGerais.getListaDadosParcelas()) {
				if (mapaReferenciaCiclo != null) {
					valorReferenciaFaturamento = mapaReferenciaCiclo.get("referencia");
					ciclo = mapaReferenciaCiclo.get("ciclo");
				}
				if (controladorParametroSistema.obterValorParametroUtilizacaoMultiplosCiclos()) {
					mapaReferenciaCiclo = Util.gerarProximaReferenciaCiclo(valorReferenciaFaturamento, ciclo, PERIODO_PARCELAS, new DateTime());
				} else {
					mapaReferenciaCiclo = Util.rolarReferenciaCiclo(valorReferenciaFaturamento, ciclo, numeroMaximoCiclo);
				}

				// Prepara e insere o crédito débito a realizar.

				BigDecimal valor = dadoParcela.getJuros().add(dadoParcela.getAmortizacao());
				CreditoDebitoARealizar debitoARealizar = this.carregarCreditoDebitoARealizar(dadosGerais, numeroPrestacao, dadoParcela,
								valor, debitoNegociado, valorReferenciaFaturamento, ciclo);
				Long chaveDebitoARealizar = super.inserir(debitoARealizar);

				listaChaves.add(chaveDebitoARealizar);

				for (DadosItemParcela dadoItemParcela : dadoParcela.getListaItemParcela()) {
					// Prepara e insere o crédito débito detalhamento.

					CreditoDebitoDetalhamento debitoJurosDetalhamento = this.carregarCreditoDebitoDetalhamento(debitoARealizar,
									dadoItemParcela);
					super.inserir(debitoJurosDetalhamento);
				}
				// Criando notas de débito para os débitos a realizar, caso a cobrança não seja via fatura.
				if (!dadosGerais.isCobrancaEmFatura()) {

					Fatura fatura = this.carregarFatura(dadosGerais, dadoParcela, debitoARealizar);

					// necessario pois os titulos a receber deverao constar todas as faturas que foram parceladas
					StringBuilder observacaoTituloReceber = new StringBuilder("Código das Faturas canceladas: ");
					for (Fatura fat : dadosGerais.getListaFatura()) {
						observacaoTituloReceber.append(fat.getChavePrimaria());
						observacaoTituloReceber.append(", ");
					}

					fatura.setObservacaoNota(observacaoTituloReceber.toString().substring(0,
									observacaoTituloReceber.toString().length() - LIMITE_OBSERVACAO));
					// necessario pois os titulos a receber deverao constar todas as faturas que foram parceladas

					Fatura retornoInsercaoNota =
									controladorFatura
									.inserirNotaDebitoCredito(fatura, new Long[] {}, null)
									.getSecond();

					Long chaveFatura = retornoInsercaoNota.getChavePrimaria();
					listaChaves.add(chaveFatura);
				}
				numeroPrestacao++;
			}

		}
		if (dadosGerais != null && listaChaves != null && debitoNegociado != null) {
			Long chaveParcelamento = this.inserirParcelamento(dadosGerais, listaChaves, debitoNegociado);

			byte[] retorno = gerarRelatorioUnificadoConfissaoDividasNotasPromissorias(dadosGerais, indicadorNotaPromissoria,
							indicadorConfissaoDivida, chaveParcelamento);

			this.registrarLancamentoContabil(dadosGerais, chaveParcelamento);

			if (dadosGerais.getValorDesconto() != null && dadosGerais.getValorDesconto().compareTo(new BigDecimal(0)) > 0) {
				this.registrarLancamentoContabilDesconto(dadosGerais, chaveParcelamento);
			}
			return retorno;
		}

		return null;
	}

	/**
	 * Registrar lancamento contabil desconto.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param chaveParcelamento
	 *            the chave parcelamento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void registrarLancamentoContabilDesconto(DadosGeraisParcelas dadosGerais, Long chaveParcelamento) throws NegocioException,
					ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoLAICDEsconto = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_DESCONTO);
		LancamentoItemContabil laicDesconto = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICDEsconto));

		salvarLancamentoContabilVO(dadosGerais, chaveParcelamento, dadosGerais.getValorDesconto(), false, laicDesconto, true);

	}

	/**
	 * Registra os lancçamentos contábeis
	 * referentes ao parcelamento cuja chave foi
	 * informada.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param chaveParcelamento
	 *            Identificador do parcelamento
	 *            que originou o lançamento
	 *            contábil.
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void registrarLancamentoContabil(DadosGeraisParcelas dadosGerais, Long chaveParcelamento) throws NegocioException,
					ConcorrenciaException {

		int limiteCurtoPrazo = ControladorCreditoDebito.QTD_MES_CONTABILIDADE_CURTO_PRAZO
				* (TRINTA_DIAS / dadosGerais.getPeriodicidadeParcelas());

		Map<Long, BigDecimal> acumularCurtoPrazo = new LinkedHashMap<>();
		Map<Long, BigDecimal> acumularLongoPrazo = new LinkedHashMap<>();
		List<LancamentoItemContabil> listaLancamentoitemContabil = new ArrayList<>();

		for (DadosParcelas dadosParcelas : dadosGerais.getListaDadosParcelas()) {

			if (dadosParcelas.getNumeroParcela() <= limiteCurtoPrazo) {

				for (DadosItemParcela dadosItemParcela : dadosParcelas.getListaItemParcela()) {
					BigDecimal valorAcumulado = acumularCurtoPrazo.get(dadosItemParcela.getLancamentoItemContabil().getChavePrimaria());
					if (valorAcumulado == null) {
						acumularCurtoPrazo
										.put(dadosItemParcela.getLancamentoItemContabil().getChavePrimaria(), dadosItemParcela.getValor());
						listaLancamentoitemContabil.add(dadosItemParcela.getLancamentoItemContabil());
					} else {
						valorAcumulado = valorAcumulado.add(dadosItemParcela.getValor());
						acumularCurtoPrazo.put(dadosItemParcela.getLancamentoItemContabil().getChavePrimaria(), valorAcumulado);
					}
				}
			} else {
				for (DadosItemParcela dadosItemParcela : dadosParcelas.getListaItemParcela()) {
					BigDecimal valorAcumulado = acumularLongoPrazo.get(dadosItemParcela.getLancamentoItemContabil().getChavePrimaria());
					if (valorAcumulado == null) {
						acumularLongoPrazo
										.put(dadosItemParcela.getLancamentoItemContabil().getChavePrimaria(), dadosItemParcela.getValor());
					} else {
						valorAcumulado = valorAcumulado.add(dadosItemParcela.getValor());
						acumularLongoPrazo.put(dadosItemParcela.getLancamentoItemContabil().getChavePrimaria(), valorAcumulado);
					}
				}
			}

		}

		for (LancamentoItemContabil lancamentoItemContabil : listaLancamentoitemContabil) {
			BigDecimal valorCurtoPrazo = acumularCurtoPrazo.get(lancamentoItemContabil.getChavePrimaria());
			if (valorCurtoPrazo != null) {
				salvarLancamentoContabilVO(dadosGerais, chaveParcelamento, valorCurtoPrazo, true, lancamentoItemContabil, false);
			}

			BigDecimal valorLongoPrazo = acumularLongoPrazo.get(lancamentoItemContabil.getChavePrimaria());
			if (valorLongoPrazo != null) {
				salvarLancamentoContabilVO(dadosGerais, chaveParcelamento, valorLongoPrazo, false, lancamentoItemContabil, false);
			}

		}

	}

	/**
	 * Salvar lancamento contabil vo.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param chaveParcelamento
	 *            the chave parcelamento
	 * @param valor
	 *            the valor
	 * @param isCurtoPrazo
	 *            the is curto prazo
	 * @param lancamentoItemContabil
	 *            the lancamento item contabil
	 * @param isDesconto
	 *            the is desconto
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito#
	 * salvarLancamentoContabilVO
	 * (br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado,
	 * java.math.BigDecimal,
	 * boolean,
	 * br.com.ggas.contabil.impl.OperacaoContabil,
	 * br.com.ggas.faturamento.LancamentoItemContabil
	 * )
	 */
	public void salvarLancamentoContabilVO(DadosGeraisParcelas dadosGerais, Long chaveParcelamento, BigDecimal valor, boolean isCurtoPrazo,
					LancamentoItemContabil lancamentoItemContabil, boolean isDesconto) throws NegocioException, ConcorrenciaException {

		ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();

		lancamentoContabilVO.setDataRealizacaoEvento(Calendar.getInstance().getTime());
		lancamentoContabilVO.setContaBancaria(null);

		if (dadosGerais.getIdPontoConsumo() != null) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(dadosGerais.getIdPontoConsumo(),  CAMPO_SEGMENTO);
			lancamentoContabilVO.setSegmento(pontoConsumo.getSegmento());
		}

		if (dadosGerais.getIdCliente() != null) {
			Cliente cliente = (Cliente) controladorCliente.obter(dadosGerais.getIdCliente());
			lancamentoContabilVO.setCnpj(cliente.getCnpj());
		}

		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setLancamentoItemContabil(lancamentoItemContabil);

		String chaveEvento = this.montarChaveEventoComercialFatura(lancamentoItemContabil, isCurtoPrazo, isDesconto);
		lancamentoContabilVO.setEventoComercial(controladorContabil.obterEventoComercial(controladorContabil
						.obterChaveEventoComercial(chaveEvento)));

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVO.setCodigoObjeto(chaveParcelamento);
		lancamentoContabilAnaliticoVO.setValor(valor);

		lancamentoContabilVO.setListaLancamentosContabeisAnaliticos(new ArrayList<LancamentoContabilAnaliticoVO>());
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

		lancamentoContabilVO.setDadosAuditoria(dadosGerais.getDadosAuditoria());

		Parcelamento parcelamento = (Parcelamento) this.obter(chaveParcelamento, "listaParcelamentoItem");

		// ------------ popular conta Auxiliar e historico contabil
		List<Object> listaObjetos = new ArrayList<>();
		listaObjetos.add(parcelamento.getCliente());
		listaObjetos.add(parcelamento);
		controladorContabil.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
		// ---------------------------------------------------

		// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
		Map<String, Object> filtro = controladorContabil.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
		EventoComercialLancamento eventoComercialLancamento = controladorContabil.consultarEventoComercialLancamentoPorFiltro(filtro);
		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		// ----------------------------------------------------------------------------------------------------------------------------

		controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);

		if (parcelamento.getListaParcelamentoItem() != null) {

			for (ParcelamentoItem parcelamentoItem : parcelamento.getListaParcelamentoItem()) {

				try {
					if (parcelamentoItem.getFaturaGeral() != null && parcelamentoItem.getFaturaGeral().getFaturaAtual() != null) {
						controladorContabil.relizarBaixaContasPDD(parcelamentoItem.getFaturaGeral().getFaturaAtual().getChavePrimaria(),
										Constantes.C_PROV_DEV_DUV_MOTIVO_BAIXA_PARCELAMENTO, lancamentoContabilVO.getEventoComercial());
					}
				} catch (LazyInitializationException e) {
					LOG.error(e.getMessage(), e);
				}
			}

		}

	}

	/**
	 * Método responsável por obter a chave do evento comercial para operações de
	 * Fatura.
	 *
	 * @param lancamentoItemContabil the lancamento item contabil
	 * @param isCurtoPrazo           the is curto prazo
	 * @param isDesconto             the is desconto
	 * @return chave do evento comercial
	 */
	private String montarChaveEventoComercialFatura(LancamentoItemContabil lancamentoItemContabil,
			boolean isCurtoPrazo, boolean isDesconto) {

		String retorno;

		if (isDesconto) {
			return "EVENTO_COMERCIAL_PARCELAMENTO_DESCONTO";
		}

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();

		String codigoLAICJurosParcelamento = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO);
		String codigoLAICJurosMora = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_MORA);
		String codigoLAICParcelamento = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_PARCELAMENTO);

		String chaveEvento = "";
		if (lancamentoItemContabil.getChavePrimaria() == Long.parseLong(codigoLAICJurosParcelamento)) {
			chaveEvento = "PARCELAMENTO_JUROS_";
		} else if (lancamentoItemContabil.getChavePrimaria() == Long.parseLong(codigoLAICJurosMora)) {
			chaveEvento = "PARCELAMENTO_CORRECAO_";
		} else if (lancamentoItemContabil.getChavePrimaria() == Long.parseLong(codigoLAICParcelamento)) {
			chaveEvento = "PARCELAMENTO_DEBITO_ORIGINAL_";
		}

		if (isCurtoPrazo) {
			chaveEvento = chaveEvento + "CURTO_PRAZO";
		} else {
			chaveEvento = chaveEvento + "LONGO_PRAZO";
		}

		retorno = "EVENTO_COMERCIAL_" + chaveEvento;
		return retorno;
	}

	/**
	 * Inserir parcelamento.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param listaChaves
	 *            the lista chaves
	 * @param debitoGerado
	 *            the debito gerado
	 * @return the long
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Long inserirParcelamento(DadosGeraisParcelas dadosGerais, Collection<Long> listaChaves,
			CreditoDebitoNegociado debitoGerado) throws GGASException {

		Parcelamento parcelamento = montarParcelamento(dadosGerais);
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		Long chaveParcelamento = super.inserir(parcelamento);

		// Atualiza débito gerado com dados de alçada.
		debitoGerado.setStatus(parcelamento.getStatus());
		debitoGerado.setUltimoUsuarioAlteracao(parcelamento.getUltimoUsuarioAlteracao());

		// Se o parcelamento não estiver autorizado, o débito fica associado a ele a fim de poder ser recuperado
		// e atualizado em caso de futura autorização do parcelamento.

		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		if (parcelamento.getStatus().getChavePrimaria() != Long.parseLong(statusAutorizado)) {
			debitoGerado.setParcelamento(parcelamento);
		}

		debitoGerado.setParcelamento(parcelamento);
		controladorCreditoDebito.atualizar(debitoGerado, controladorCreditoDebito.getClasseEntidadeCreditoDebitoNegociado());

		// Atualiza as Notas de crédito/débito com o parcelamento.

		if (listaChaves != null && !listaChaves.isEmpty()) {

			Parcelamento parcelamentoBD = (Parcelamento) super.obter(chaveParcelamento);
			if (dadosGerais.isCobrancaEmFatura()) {
				Collection<CreditoDebitoARealizar> listaCreditoDebito = controladorCreditoDebito
								.consultarCreditosDebitosPorChaves(listaChaves.toArray());
				for (CreditoDebitoARealizar debitoARealizar : listaCreditoDebito) {
					debitoARealizar.getCreditoDebitoNegociado().setParcelamento(parcelamentoBD);
					super.atualizar(debitoARealizar, CreditoDebitoARealizarImpl.class);
				}
			} else {
				Collection<Fatura> listaFaturas = controladorFatura.consultarFaturasPorChaves(listaChaves.toArray());
				for (Fatura fatura : listaFaturas) {
					fatura.setParcelamento(parcelamentoBD);
					super.atualizar(fatura, getClasseEntidadeFatura());
				}
			}
		}

		return chaveParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #montarParcelamento(br.
	 * com.procenge.ggas.cobranca.parcelamento.
	 * DadosGeraisParcelas)
	 */
	@Override
	public Parcelamento montarParcelamento(DadosGeraisParcelas dadosGerais) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Parcelamento parcelamento = (Parcelamento) this.criar();

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		String chaveTipoDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);
		String chaveTipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);
		EntidadeConteudo tipoDebito = controladorEntidadeConteudo.obter(Long.valueOf(chaveTipoDebito));
		EntidadeConteudo tipoCredito = controladorEntidadeConteudo.obter(Long.valueOf(chaveTipoCredito));
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		BigDecimal valorTotalFatura = BigDecimal.ZERO;
		BigDecimal valorTotalDebitos = BigDecimal.ZERO;
		BigDecimal valorTotalCredito = BigDecimal.ZERO;
		BigDecimal valorCorrecao = BigDecimal.ZERO;
		BigDecimal valorJurosMora = BigDecimal.ZERO;
		BigDecimal valorMulta = BigDecimal.ZERO;
		BigDecimal valorJurosParcelamento = BigDecimal.ZERO;

		if (dadosGerais.getListaFatura() != null && !dadosGerais.getListaFatura().isEmpty()) {
			Long idDocumentoFatura = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
			Long idDocumentoNotaDebito = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));

			// necessario pois os titulos a receber deverao constar todas as faturas que foram parceladas
			StringBuilder observacaoTituloReceber = new StringBuilder("Código das Faturas canceladas: ");
			for (Fatura fatura : dadosGerais.getListaFatura()) {
				observacaoTituloReceber.append(fatura.getChavePrimaria());
				observacaoTituloReceber.append(", ");
			}
			// necessario pois os titulos a receber deverao constar todas as faturas que foram parceladas

			for (Fatura fatura : dadosGerais.getListaFatura()) {
				if ((fatura.getTipoDocumento().getChavePrimaria() == idDocumentoFatura)
								|| (fatura.getTipoDocumento().getChavePrimaria() == idDocumentoNotaDebito)) {
					valorTotalFatura = valorTotalFatura.add(controladorFatura.calcularSaldoFatura(fatura));

					DebitosACobrar debitosACobrar = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, Calendar
									.getInstance().getTime(), fatura.getValorTotal(), Boolean.TRUE, Boolean.TRUE);
					if (debitosACobrar != null && debitosACobrar.getMulta() != null) {
						valorMulta = valorMulta.add(debitosACobrar.getMulta());
					}

					if (debitosACobrar != null && debitosACobrar.getJurosMora() != null) {
						valorJurosMora = valorJurosMora.add(debitosACobrar.getJurosMora());
					}

				} else {
					valorTotalFatura = valorTotalFatura.subtract(controladorFatura.calcularSaldoFatura(fatura));
				}

				ParcelamentoItem parcelamentoItem = (ParcelamentoItem) this.criarParcelamentoItem();
				parcelamentoItem.setUltimaAlteracao(Calendar.getInstance().getTime());
				parcelamentoItem.setHabilitado(Boolean.TRUE);
				parcelamentoItem.setDadosAuditoria(dadosGerais.getDadosAuditoria());
				parcelamentoItem.setFaturaGeral(fatura.getFaturaGeral());
				parcelamento.getListaParcelamentoItem().add(parcelamentoItem);
				fatura.setDadosAuditoria(dadosGerais.getDadosAuditoria());
				String codigoSituacaoParcelada = (String) controladorParametroSistema
								.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_PARCELADA);

				fatura.setCreditoDebitoSituacao(this.obterDebitoSituacao(codigoSituacaoParcelada));

				this.atualizar(fatura, getClasseEntidadeFatura());
				ControladorIntegracao controladorIntegracao = getControladorIntegracao();

				fatura.setObservacaoNota(observacaoTituloReceber.toString().substring(0, observacaoTituloReceber.toString().length() - 2));

				controladorIntegracao.cancelarTituloReceber(fatura);
			}
		}

		parcelamento.setValorFatura(valorTotalFatura);

		if (dadosGerais.getListaCreditoDebito() != null && !dadosGerais.getListaCreditoDebito().isEmpty()) {
			for (CreditoDebitoARealizar creditoDebitoARealizar : dadosGerais.getListaCreditoDebito()) {
				Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = controladorCreditoDebito
								.obterCDDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				CreditoDebitoDetalhamento debitoDetalhamento = ((List<CreditoDebitoDetalhamento>) listaCDDetalhamento).get(0);
				EntidadeConteudo tipoDebitoCredito = debitoDetalhamento.getLancamentoItemContabil().getTipoCreditoDebito();

				if (tipoDebito.getChavePrimaria() == tipoDebitoCredito.getChavePrimaria()) {
					valorTotalDebitos = valorTotalDebitos.add(creditoDebitoARealizar.getValor());
				}

				if (tipoCredito.getChavePrimaria() == tipoDebitoCredito.getChavePrimaria()) {
					valorTotalCredito = valorTotalCredito.add(creditoDebitoARealizar.getValor());
				}

				ParcelamentoItem parcelamentoItem = (ParcelamentoItem) this.criarParcelamentoItem();
				parcelamentoItem.setUltimaAlteracao(Calendar.getInstance().getTime());
				parcelamentoItem.setHabilitado(Boolean.TRUE);
				parcelamentoItem.setDadosAuditoria(dadosGerais.getDadosAuditoria());
				parcelamentoItem.setCreditoDebitoARealizar(creditoDebitoARealizar);
				parcelamento.getListaParcelamentoItem().add(parcelamentoItem);
				creditoDebitoARealizar.setDadosAuditoria(dadosGerais.getDadosAuditoria());
				this.atualizarCreditoDebito(creditoDebitoARealizar, true);
			}
		}

		parcelamento.setValorEntrada(BigDecimal.ZERO);
		if (dadosGerais.getListaDadosParcelas() != null && !dadosGerais.getListaDadosParcelas().isEmpty()) {
			for (DadosParcelas dadosParcela : dadosGerais.getListaDadosParcelas()) {
				valorJurosParcelamento = valorJurosParcelamento.add(dadosParcela.getJuros());
				if ((dadosParcela.getNumeroParcela().compareTo(1) == 0)
								&& (Util.compararDatas(dadosGerais.getVencimentoInicial(), Calendar.getInstance().getTime()) == 0)) {
					parcelamento.setValorEntrada(dadosParcela.getTotal());
				}
			}
		}
		parcelamento.setValorJurosParcelamento(valorJurosParcelamento);
		parcelamento.setValorDebito(valorTotalDebitos);
		parcelamento.setValorCredito(valorTotalCredito);
		parcelamento.setValorCorrecao(valorCorrecao);
		parcelamento.setValorJurosMora(valorJurosMora);
		parcelamento.setValorMulta(valorMulta);
		BigDecimal valorAtualizado = ((valorTotalFatura.add(valorTotalDebitos)).subtract(valorTotalCredito)).add(valorCorrecao
						.add(valorMulta).add(valorJurosMora));
		parcelamento.setValorAtualizado(valorAtualizado);
		parcelamento.setValorDesconto(dadosGerais.getValorDesconto());
		parcelamento.setPercentualDesconto(dadosGerais.getPercentualDesconto());
		parcelamento.setPercentualEntrada(BigDecimal.ZERO);
		parcelamento.setPercentualJurosParcelamento(dadosGerais.getParcelamentoPerfil().getTaxaJuros());
		parcelamento.setDataVencimentoInicial(dadosGerais.getVencimentoInicial());
		parcelamento.setNumeroDiasPeriodicidade(dadosGerais.getPeriodicidadeParcelas());
		parcelamento.setEmissaoNotasPromissorias(dadosGerais.isEmiteNotaPromissoria());
		parcelamento.setEmissaoTermoParcelamento(dadosGerais.isEmiteTermoConfissaoDivida());

		parcelamento.setParcelamentoPerfil(dadosGerais.getParcelamentoPerfil());
		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(dadosGerais.getIdPontoConsumo(),  CAMPO_SEGMENTO);
		parcelamento.setPontoConsumo(pontoConsumo);
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(dadosGerais
						.getIdPontoConsumo());

		Cliente cliente = (Cliente) controladorCliente.obter(contratoPontoConsumo.getContrato().getClienteAssinatura().getChavePrimaria());
		parcelamento.setCliente(cliente);

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String formaCobrancaNotaDebito = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_NOTA_DEBITO);

		EntidadeConteudo formaCobrancaParcelamento = controladorEntidadeConteudo.obter(Long
						.valueOf(formaCobrancaNotaDebito));
		parcelamento.setFormaCobrancaParcelamento(formaCobrancaParcelamento);
		parcelamento.setNumeroPrestacoes(dadosGerais.getNumeroPrestacoes());
		Cliente fiador = null;
		if (dadosGerais.getIdFiador() != null) {
			fiador = (Cliente) controladorCliente.obter(dadosGerais.getIdFiador());
		}
		parcelamento.setFiador(fiador);
		parcelamento.setDataParcelamento(Calendar.getInstance().getTime());
		parcelamento.setDadosAuditoria(dadosGerais.getDadosAuditoria());

		parcelamento.setStatus(definirStatusParcelamento(dadosGerais, parcelamento));
		parcelamento.setUltimoUsuarioAlteracao(DadosAuditoriaUtil.getDadosAuditoria().getUsuario());

		if (dadosGerais.getTipoCobranca() != null) {
			parcelamento.setTipoCobranca(dadosGerais.getTipoCobranca());
		}

		return parcelamento;
	}

	/**
	 * Definir status parcelamento.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param parcelamento
	 *            the parcelamento
	 * @return the entidade conteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private EntidadeConteudo definirStatusParcelamento(DadosGeraisParcelas dadosGerais, Parcelamento parcelamento) throws NegocioException {

		// Valida se existe controle de alçada
		// para o parcelamento
		EntidadeConteudo status = null;
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();

		ControladorAlcada controladorAlcada = (ControladorAlcada) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);
		ParametroSistema parametroTabelaParcelamento = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_PARCELAMENTO);
		Long chaveTabelaParc = null;
		if (parametroTabelaParcelamento != null) {
			chaveTabelaParc = Long.valueOf(parametroTabelaParcelamento.getValor());
		}

		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		Collection<Papel> papeis = null;
		if (dadosGerais.getDadosAuditoria() != null && dadosGerais.getDadosAuditoria().getUsuario() != null
						&& dadosGerais.getDadosAuditoria().getUsuario().getFuncionario() != null) {
			papeis = controladorPapel.consultarPapeisPorFuncionario(dadosGerais.getDadosAuditoria().getUsuario().getFuncionario()
							.getChavePrimaria());
		}

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		if (chaveTabelaParc != null && papeis != null && !papeis.isEmpty()) {
			Collection<Alcada> colecaoAlcadas = controladorAlcada.obterListaAlcadasVigentes(chaveTabelaParc, papeis, Calendar.getInstance()
							.getTime());
			if (colecaoAlcadas != null && !colecaoAlcadas.isEmpty()) {
				for (Alcada alcada : colecaoAlcadas) {
					//
					try {

						String propriedade = alcada.getColuna().getNomePropriedade();
						propriedade = propriedade.substring(0, 1).toUpperCase() + propriedade.subSequence(1, propriedade.length());
						String nomeMetodo = REFLECTION_GET + propriedade;

						Object valorMetodoObj = parcelamento.getClass().getDeclaredMethod(nomeMetodo, new Class[] {}).invoke(parcelamento);
						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());
							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)) {

								status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
								break;
							}
						}
					} catch (IllegalArgumentException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (SecurityException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (IllegalAccessException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (InvocationTargetException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (NoSuchMethodException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					}
				}
			}
		}

		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		if (status != null) {
			parcelamento.setStatus(status);
			if (parcelamento.getUltimoUsuarioAlteracao() == null) {
				parcelamento.setUltimoUsuarioAlteracao(dadosGerais.getDadosAuditoria().getUsuario());
			}

			boolean isUnicoUsuarioAutorizado = controladorAlcada.possuiAlcadaAutorizacaoParcelamento(dadosGerais.getDadosAuditoria()
							.getUsuario(), parcelamento);

			if (isUnicoUsuarioAutorizado) {
				status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusAutorizada));
			} else {
				status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
			}
		} else {
			status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusAutorizada));
		}

		return status;
	}

	/**
	 * Gerar relatorio unificado confissao dividas notas promissorias.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param indicadorNotaPromissoria
	 *            the indicador nota promissoria
	 * @param indicadorConfissaoDivida
	 *            the indicador confissao divida
	 * @param chaveParcelamento
	 *            the chave parcelamento
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private byte[] gerarRelatorioUnificadoConfissaoDividasNotasPromissorias(DadosGeraisParcelas dadosGerais,
					boolean indicadorNotaPromissoria, boolean indicadorConfissaoDivida, Long chaveParcelamento) throws GGASException {

		List<byte[]> retornoRelatorios = new ArrayList<>();
		byte[] relatorioConfissaoDividas = null;
		byte[] relatorioNotasPromissorias = null;

		if (indicadorConfissaoDivida) {
			// Gera relatorio de confissao de
			// dividas
			relatorioConfissaoDividas = gerarRelatorioConfissaoDividas(chaveParcelamento, dadosGerais.getListaDadosParcelas(),
							dadosGerais.getListaFatura());
		}

		if (relatorioConfissaoDividas != null && relatorioConfissaoDividas.length != 0) {
			retornoRelatorios.add(relatorioConfissaoDividas);
		}
		if (indicadorNotaPromissoria) {
			// Gera relatório de notas
			// promissórias
			relatorioNotasPromissorias = gerarRelatorioNotaPromissoria(chaveParcelamento, dadosGerais.getListaDadosParcelas());
		}

		if (relatorioNotasPromissorias != null && relatorioNotasPromissorias.length != 0) {
			retornoRelatorios.add(relatorioNotasPromissorias);
		}

		byte[] retornoRelatorio = null;
		if (!retornoRelatorios.isEmpty()) {
			retornoRelatorio = RelatorioUtil.unificarRelatoriosPdf(retornoRelatorios);
		}
		return retornoRelatorio;
	}

	/**
	 * Atualizar credito debito.
	 *
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param mudaSituacao
	 *            the muda situacao
	 * @throws NegocioException
	 *             the negocio exception
	 * @deprecated
	 */
	@Deprecated
	private void atualizarCreditoDebito(CreditoDebitoARealizar creditoDebitoARealizar, boolean mudaSituacao) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if (mudaSituacao) {
			String codigoSituacaoParcelada = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_PARCELADA);
			creditoDebitoARealizar.setCreditoDebitoSituacao(this.obterDebitoSituacao(codigoSituacaoParcelada));
		}

		CreditoDebitoNegociado debitoNegociado = creditoDebitoARealizar.getCreditoDebitoNegociado();
		debitoNegociado.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(debitoNegociado);

		creditoDebitoARealizar.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(creditoDebitoARealizar);
	}

	/**
	 * Carregar fatura.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param dadoParcela
	 *            the dado parcela
	 * @param debitoARealizar
	 *            the debito a realizar
	 * @return the fatura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Fatura carregarFatura(DadosGeraisParcelas dadosGerais, DadosParcelas dadoParcela, CreditoDebitoARealizar debitoARealizar)
					throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Fatura fatura = (Fatura) this.criarFatura();

		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		fatura.setValorTotal(dadoParcela.getTotal());
		fatura.setDataVencimento(dadoParcela.getDataVencimento());
		fatura.setTipoDocumento(this.obterTipoDocumentoNotasDebito());
		fatura.setDataEmissao(Util.getDataCorrente(false));
		fatura.setDadosAuditoria(dadosGerais.getDadosAuditoria());
		fatura.setCobrada(Boolean.TRUE);
		fatura.setProvisaoDevedoresDuvidosos(Boolean.FALSE);

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(dadosGerais.getIdPontoConsumo(),  CAMPO_SEGMENTO);
		fatura.setPontoConsumo(pontoConsumo);
		fatura.setSegmento(pontoConsumo.getSegmento());

		Cliente cliente = this.obterClienteParcelamento(dadosGerais.getIdFiador(), pontoConsumo);
		fatura.setCliente(cliente);

		String codigoSituacaoNormal = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL);
		fatura.setCreditoDebitoSituacao(this.obterDebitoSituacao(codigoSituacaoNormal));

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPendente));
		fatura.setSituacaoPagamento(situacaoPagamento);

		// Criando e adicionando valores da Fatura Item.

		FaturaItem faturaItem = (FaturaItem) this.criarFaturaItem();

		faturaItem.setCreditoDebitoARealizar(debitoARealizar);
		if (debitoARealizar != null && debitoARealizar.getCreditoDebitoNegociado() != null) {
			faturaItem.setRubrica(debitoARealizar.getCreditoDebitoNegociado().getRubrica());
		}
		faturaItem.setQuantidade(BigDecimal.ONE);

		BigDecimal valorUnitario = dadoParcela.getAmortizacao().add(dadoParcela.getJuros());
		faturaItem.setValorUnitario(valorUnitario);

		BigDecimal valorTotal = faturaItem.getQuantidade().multiply(faturaItem.getValorUnitario());
		faturaItem.setValorTotal(valorTotal);

		faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
		faturaItem.setDadosAuditoria(dadosGerais.getDadosAuditoria());
		faturaItem.setHabilitado(Boolean.TRUE);
		faturaItem.setNumeroSequencial(1);
		fatura.setValorConciliado(BigDecimal.ZERO);

		fatura.getListaFaturaItem().add(faturaItem);

		return fatura;
	}

	/**
	 * Carregar credito debito negociado.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param rubrica
	 *            the rubrica
	 * @param qtdeTotalPrestacoes
	 *            the qtde total prestacoes
	 * @return the credito debito negociado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private CreditoDebitoNegociado carregarCreditoDebitoNegociado(DadosGeraisParcelas dadosGerais, Rubrica rubrica,
					Integer qtdeTotalPrestacoes) throws NegocioException {

		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoPeriodicidadeJurosMensal = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_JUROS_MENSAL);

		EntidadeConteudo periodicidadeJuros = controladorEntidadeConteudo.obter(Long
						.valueOf(codigoPeriodicidadeJurosMensal));
		// Cria o Crédito Débito Negociado.
		CreditoDebitoNegociado debitoNegociado = (CreditoDebitoNegociado) controladorCreditoDebito.criarCreditoDebitoNegociado();
		debitoNegociado.setAmortizacao(dadosGerais.getParcelamentoPerfil().getTipoTabela());
		debitoNegociado.setPeriodicidadeJuros(periodicidadeJuros);
		debitoNegociado.setDataInicioCobranca(Calendar.getInstance().getTime());
		debitoNegociado.setPercentualJuros(dadosGerais.getParcelamentoPerfil().getTaxaJuros());
		debitoNegociado.setRubrica(rubrica);
		debitoNegociado.setNumeroPrestacaoCobrada(0);
		debitoNegociado.setFinanciamentoTipo(rubrica.getFinanciamentoTipo());
		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(dadosGerais.getIdPontoConsumo(), CAMPO_SEGMENTO);
		debitoNegociado.setSegmento(pontoConsumo.getSegmento());
		Cliente cliente = this.obterClienteParcelamento(dadosGerais.getIdFiador(), pontoConsumo);
		debitoNegociado.setCliente(cliente);
		debitoNegociado.setQuantidadeTotalPrestacoes(qtdeTotalPrestacoes);
		debitoNegociado.setDataFinanciamento(Calendar.getInstance().getTime());
		debitoNegociado.setAnoMesInclusao(Util.obterAnoMesCorrente());
		debitoNegociado.setValorJuros(dadosGerais.getValorTotalJuros());
		debitoNegociado.setValor(dadosGerais.getValorTotalAPagar().subtract(dadosGerais.getValorTotalJuros()));
		debitoNegociado.setValorEntrada(BigDecimal.ZERO);
		debitoNegociado.setFormaCobranca(dadosGerais.getFormaCobranca());

		debitoNegociado.setPontoConsumo(pontoConsumo);

		String codigoPeriodicidadeCobranca = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_A_CADA_FATURAMENTO);
		EntidadeConteudo periodicidadeCobranca = controladorEntidadeConteudo.obter(Long
						.valueOf(codigoPeriodicidadeCobranca));
		debitoNegociado.setPeriodicidadeCobranca(periodicidadeCobranca);

		return debitoNegociado;
	}

	/**
	 * Carregar credito debito a realizar.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param numeroPrestacao
	 *            the numero prestacao
	 * @param dadoParcela
	 *            the dado parcela
	 * @param valor
	 *            the valor
	 * @param debitoNegociado
	 *            the debito negociado
	 * @param referencia
	 *            the referencia
	 * @param ciclo
	 *            the ciclo
	 * @return the credito debito a realizar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private CreditoDebitoARealizar carregarCreditoDebitoARealizar(DadosGeraisParcelas dadosGerais, int numeroPrestacao,
					DadosParcelas dadoParcela, BigDecimal valor, CreditoDebitoNegociado debitoNegociado, Integer referencia, Integer ciclo)
					throws NegocioException {

		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPendente));

		String codigoSituacaoNormal = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL);

		// Cria o Crédito Débito a realizar.
		CreditoDebitoARealizar debitoCredito = (CreditoDebitoARealizar) controladorCreditoDebito.criar();

		debitoCredito.setSituacaoPagamento(situacaoPagamento);
		debitoCredito.setNumeroPrestacao(numeroPrestacao);
		debitoCredito.setIndicadorAntecipacao(Boolean.FALSE);
		debitoCredito.setCreditoDebitoSituacao(this.obterDebitoSituacao(codigoSituacaoNormal));
		debitoCredito.setCreditoDebitoNegociado(debitoNegociado);
		debitoCredito.setAnoMesFaturamento(referencia);
		debitoCredito.setNumeroCiclo(ciclo);

		if (!dadosGerais.isCobrancaEmFatura()) {
			debitoCredito.setAnoMesCobranca(referencia);
		}

		if (dadosGerais.isIndicarJuros()) {
			debitoCredito.setValor(valor);
		} else {
			debitoCredito.setValor(dadoParcela.getTotal());
		}

		return debitoCredito;
	}

	/**
	 * Carregar credito debito detalhamento.
	 *
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param dadoItemParcela
	 *            the dado item parcela
	 * @return the credito debito detalhamento
	 */
	private CreditoDebitoDetalhamento carregarCreditoDebitoDetalhamento(CreditoDebitoARealizar creditoDebitoARealizar,
					DadosItemParcela dadoItemParcela) {

		// Cria o Crédito Débito Detalhamento.
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		CreditoDebitoDetalhamento debitoDetalhamento = (CreditoDebitoDetalhamento) controladorCreditoDebito
						.criarCreditoDebitoDetalhamento();
		debitoDetalhamento.setLancamentoItemContabil(dadoItemParcela.getLancamentoItemContabil());
		debitoDetalhamento.setCreditoDebitoARealizar(creditoDebitoARealizar);
		debitoDetalhamento.setValor(dadoItemParcela.getValor());

		return debitoDetalhamento;
	}

	/**
	 * Obter cliente parcelamento.
	 *
	 * @param idFiador
	 *            the id fiador
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Cliente obterClienteParcelamento(Long idFiador, PontoConsumo pontoConsumo) throws NegocioException {

		Cliente cliente = null;
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		if (idFiador != null) {
			cliente = (Cliente) controladorCliente.obter(idFiador);
		} else {
			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

			if (contratoPontoConsumo == null) {
				contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
								.getChavePrimaria());
			}
			cliente = contratoPontoConsumo.getContrato().getClienteAssinatura();
		}
		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * consultarParcelamentosVinculadosPerfilParcelamento
	 * (java.lang.Long)
	 */
	@Override
	public Collection<Parcelamento> consultarParcelamentosVinculadosPerfilParcelamento(Long idParcelamentoPerfil) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" parcelamento WHERE ");
		hql.append(" parcelamento.parcelamentoPerfil.chavePrimaria = :parcelamentoPerfil ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CAMPO_PARCELAMENTO_PERFIL, idParcelamentoPerfil);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * consultarParcelamentoCliente(java.lang.Long)
	 */
	@Override
	public Collection<Parcelamento> consultarParcelamentoCliente(Long idCliente, Long idImovel, Date dataInicial, Date dataFinal,
					Long statusParcelamento, Long[] chavesPrimarias) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" parcelamento ");
		hql.append(" inner join fetch parcelamento.cliente ");
		hql.append(" inner join fetch parcelamento.status ");
		hql.append(" inner join fetch parcelamento.pontoConsumo ");
		hql.append(" inner join fetch parcelamento.pontoConsumo.imovel ");
		hql.append(" where 1=1 ");

		if (dataInicial != null && dataFinal != null) {
			hql.append(" and (parcelamento.dataParcelamento between :dataInicial and :dataFinal) ");
		}

		if (idCliente != null && idCliente > 0) {
			hql.append(" and parcelamento.cliente.chavePrimaria = :idCliente ");
		} else if (idImovel != null && idImovel > 0) {
			hql.append(" and parcelamento.pontoConsumo.imovel.chavePrimaria = :idImovel ");
		}

		if (statusParcelamento != null && statusParcelamento > 0) {
			hql.append(" and parcelamento.status.chavePrimaria = :statusParcelamento ");
		}

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			hql.append(" and parcelamento.chavePrimaria in (:chavesPrimarias) ");
		}

		hql.append(" order by parcelamento.dataParcelamento desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idCliente != null && idCliente > 0) {
			query.setLong("idCliente", idCliente);
		} else if (idImovel != null && idImovel > 0) {
			query.setLong("idImovel", idImovel);
		}

		if (dataInicial != null && dataFinal != null) {

			Util.adicionarRestricaoDataSemHora(query, dataInicial, "dataInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataFinal, "dataFinal", Boolean.FALSE);

		}

		if (statusParcelamento != null && statusParcelamento > 0) {
			query.setLong("statusParcelamento", statusParcelamento);
		}

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			query.setParameterList("chavesPrimarias", chavesPrimarias);
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * validarFiltroPesquisaParcelamentoDebitos(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public void validarFiltroPesquisaParcelamentoDebitos(String dataInicial, String dataFinal) throws NegocioException {

		if ((StringUtils.isEmpty(dataInicial)) && (StringUtils.isEmpty(dataFinal))) {
			throw new NegocioException(PARCELAMENTO_ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA, true);
		}

		if(StringUtils.isEmpty(dataInicial) && (!StringUtils.isEmpty(dataFinal)) || (!StringUtils.isEmpty(dataInicial) && StringUtils
						.isEmpty(dataFinal))) {
			throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, false);
		}
		if(StringUtils.isEmpty(dataInicial) && (!StringUtils.isEmpty(dataFinal)) || (!StringUtils.isEmpty(dataInicial) && StringUtils
						.isEmpty(dataFinal))) {
			throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, false);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * validarFiltroPesquisaPontosConsumo(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public void validarFiltroPesquisaPontosConsumo(Long idImovel, Long idCliente) throws NegocioException {

		if (((idCliente == null) || (idCliente == 0)) && ((idImovel == null) || (idImovel == 0))) {
			throw new NegocioException(PARCELAMENTO_ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA_PONTO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento#
	 * listaParcelamentoItemPorChaveParcelamento(java
	 * .lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ParcelamentoItem> listarParcelamentoItemPorChaveParcelamento(Long idParcelamento) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeParcelamentoItem().getSimpleName());
		hql.append(" parcelamentoItem ");
		hql.append(" inner join fetch parcelamentoItem.faturaGeral ");
		hql.append(" inner join fetch parcelamentoItem.creditoDebitoARealizar ");
		hql.append(" inner join fetch parcelamentoItem.parcelamento ");
		hql.append(WHERE);
		hql.append(" parcelamentoItem.parcelamento.chavePrimaria = :idParcelamento ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idParcelamento", idParcelamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #gerarRelatorioConfissaoDividas
	 * (java.lang.Long, java.util.Collection,
	 * java.util.Collection)
	 */
	@Override
	public byte[] gerarRelatorioConfissaoDividas(Long idParcelamento, Collection<DadosParcelas> colecaoDadosParcelas,
					Collection<Fatura> listaFatura) throws NegocioException {

		Parcelamento parcelamento = (Parcelamento) obter(idParcelamento, CAMPO_CLIENTE, CAMPO_PARCELAMENTO_PERFIL, "pontoConsumo",
						"formaCobrancaParcelamento");

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		Map<String, Object> filtroEmpresa = new HashMap<>();
		filtroEmpresa.put("principal", Boolean.TRUE);
		Collection<Empresa> colecaoEmpresa = controladorEmpresa.consultarEmpresas(filtroEmpresa);

		Empresa empresa = null;
		if (!colecaoEmpresa.isEmpty()) {
			empresa = colecaoEmpresa.iterator().next();
		}

		if (empresa != null) {
			Cliente clienteEmpresa = (Cliente) controladorCliente.obter(empresa.getCliente().getChavePrimaria(), CAMPO_ENDERECOS);
			empresa.setCliente(clienteEmpresa);
		}

		StringBuilder datasFaturas = new StringBuilder();
		if (listaFatura != null && !listaFatura.isEmpty()) {
			Iterator<Fatura> iterator = listaFatura.iterator();

			while (iterator.hasNext()) {
				Fatura fatura = iterator.next();
				datasFaturas.append(fatura.getDataVencimentoFormatada().substring(fatura.getDataVencimentoFormatada().indexOf('/') + 1));
				// remove
				// o
				// dia
				// da
				// data
				if (iterator.hasNext()) {
					datasFaturas.append(" ,");
				}
			}
		}

		StringBuilder valoresParcelas = new StringBuilder();
		if (colecaoDadosParcelas != null && !colecaoDadosParcelas.isEmpty()) {
			Iterator<DadosParcelas> iterator = colecaoDadosParcelas.iterator();

			while (iterator.hasNext()) {
				DadosParcelas dadosParcelas = iterator.next();
				valoresParcelas.append("R$");
				valoresParcelas.append(dadosParcelas.getTotal());

				if (iterator.hasNext()) {
					valoresParcelas.append("; ");
				}
			}
		}

		byte[] bytes = null;
		if (parcelamento != null) {
			// Recupera as Parcelas do
			// Parcelamento
			List<SubRelatorioParcelaVO> colecaoParcelas = new ArrayList<>();
			Collection<Fatura> listaParcelasFatura;
			listaParcelasFatura = controladorFatura.consultarFaturasPorParcelamento(parcelamento.getChavePrimaria());

			SubRelatorioParcelaVO voParcela = null;
			if (!listaParcelasFatura.isEmpty()) {
				int contador = 1;
				for (Fatura fatura : listaParcelasFatura) {
					voParcela = new SubRelatorioParcelaVO();
					voParcela.setNumeroParcela(String.valueOf(contador));
					BigDecimal valorSaldoFatura = controladorFatura.calcularSaldoFatura(fatura);
					if (valorSaldoFatura != null) {
						voParcela.setValor(Util.converterCampoCurrencyParaString(valorSaldoFatura, Constantes.LOCALE_PADRAO));
					}
					if (fatura.getDataVencimento() != null) {
						voParcela.setDataVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(),
										Constantes.FORMATO_DATA_BR));
					}
					colecaoParcelas.add(voParcela);
					contador++;
				}
			} else {
				Collection<CreditoDebitoARealizar> listaParcelasCreditoDebito =
						controladorCreditoDebito.consultarCreditoDebitoARealizarPorParcelamento(parcelamento.getChavePrimaria());
				Integer ultimoNumeroParcela = 0;
				for (CreditoDebitoARealizar creditoDebitoARealizar : listaParcelasCreditoDebito) {
					voParcela = new SubRelatorioParcelaVO();
					if (!ultimoNumeroParcela.equals(creditoDebitoARealizar.getNumeroPrestacao())) {
						ultimoNumeroParcela = creditoDebitoARealizar.getNumeroPrestacao();
						voParcela.setNumeroParcela(String.valueOf(creditoDebitoARealizar.getNumeroPrestacao()));
						voParcela.setValor(
								Util.converterCampoCurrencyParaString(creditoDebitoARealizar.getValor(), Constantes.LOCALE_PADRAO));

						colecaoParcelas.add(voParcela);
					}
				}
			}

			Map<String, Object> parametros = new HashMap<>();
			if (empresa.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}
			this.popularRelatorioConfissaoDividaParcelamento(parametros, parcelamento, empresa, datasFaturas.toString(),
							valoresParcelas.toString());

			JRBeanCollectionDataSource listaParcelas = new JRBeanCollectionDataSource(colecaoParcelas);
			parametros.put("colecaoParcelamento", listaParcelas);

			List<SubRelatorioParcelaVO> teste = new ArrayList<>();
			teste.add(new SubRelatorioParcelaVO());

			bytes = RelatorioUtil.gerarRelatorioPDF(teste, parametros, RELATORIO_INSTRUMENTO_CONFISSAO_DIVIDAS);
		}
		return bytes;
	}

	/**
	 * Popular relatorio confissao divida parcelamento.
	 *
	 * @param parametros
	 *            the parametros
	 * @param parcelamento
	 *            the parcelamento
	 * @param empresa
	 *            the empresa
	 * @param datasFaturas
	 *            the datas faturas
	 * @param valoresParcelas
	 *            the valores parcelas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularRelatorioConfissaoDividaParcelamento(Map<String, Object> parametros, Parcelamento parcelamento, Empresa empresa,
					String datasFaturas, String valoresParcelas) throws NegocioException {

		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String chaveIndiceMora = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_JUROS_MORA);
		String chaveIndiceMultaAtraso = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MULTA_ATRASO);

		EncargoTributario encargoMora = controladorArrecadacao.obterValorNominalEncargoTributario(Long.valueOf(chaveIndiceMora));
		EncargoTributario encargoMultaAtraso = controladorArrecadacao.obterValorNominalEncargoTributario(Long
						.valueOf(chaveIndiceMultaAtraso));

		BigDecimal jurosMora = encargoMora.getPercentualNominal().multiply(new BigDecimal(POR_CENTAGEM));
		BigDecimal multaAtraso = encargoMultaAtraso.getPercentualNominal().multiply(new BigDecimal(POR_CENTAGEM));

		parametros.put(CAMPO_RELATORIO_CNPJ_EMPRESA, empresa.getCliente().getNumeroDocumentoFormatado());
		parametros.put(CAMPO_RELATORIO_NOME_CLIENTE, parcelamento.getCliente().getNome());
		ClienteEndereco enderecoPrincipal = parcelamento.getCliente().getEnderecoPrincipal();
		if (enderecoPrincipal != null) {
			parametros.put(CAMPO_RELATORIO_ENDERECO, enderecoPrincipal.getEnderecoFormatado());
		} else {
			parametros.put(CAMPO_RELATORIO_ENDERECO, "");
		}

		if (!StringUtils.isEmpty(parcelamento.getCliente().getCpfFormatado())) {
			parametros.put(CAMPO_RELATORIO_CPF_CNPJ_CLIENTE, parcelamento.getCliente().getCpfFormatado());
		} else {
			parametros.put(CAMPO_RELATORIO_CPF_CNPJ_CLIENTE, parcelamento.getCliente().getCnpjFormatado());
		}
		BigDecimal valorRelatorio = parcelamento.getValorAtualizado();

		NumberFormat valorFormatoReal = NumberFormat.getNumberInstance();
		valorFormatoReal.setMinimumFractionDigits(NUMERO_DIGITOS);

		parametros.put(CAMPO_RELATORIO_TOTAL_FATURA, valorFormatoReal.format(valorRelatorio));
		parametros.put(CAMPO_RELATORIO_TOTAL_FATURA_EXTENSO, new Extenso(valorRelatorio).toString());
		if (parcelamento.getFiador() != null) {

			parametros.put(CAMPO_RELATORIO_INDICADOR_FIADOR, Boolean.TRUE);
			parametros.put(CAMPO_RELATORIO_NOME_FIADOR, parcelamento.getFiador().getNome());

			if (!StringUtils.isEmpty(parcelamento.getFiador().getCpfFormatado())) {
				parametros.put(CAMPO_RELATORIO_CPF_CNPJ_FIADOR, parcelamento.getFiador().getCpfFormatado());
			} else {
				parametros.put(CAMPO_RELATORIO_CPF_CNPJ_FIADOR, parcelamento.getFiador().getCnpjFormatado());
			}
		}

		parametros.put(CAMPO_RELATORIO_VALOR_ENTRADA, parcelamento.getValorEntradaFormatado());

		parametros.put(CAMPO_RELATORIO_VALOR_ENTRADA_EXTENSO, new Extenso(parcelamento.getValorEntrada()).toString());
		if (parcelamento.getDataVencimentoInicial() != null) {
			parametros.put(CAMPO_RELATORIO_DATA_PARCELAMENTO,
							Util.converterDataParaStringSemHora(parcelamento.getDataVencimentoInicial(), Constantes.FORMATO_DATA_BR));
		} else {
			parametros.put(CAMPO_RELATORIO_DATA_PARCELAMENTO, "");
		}
		parametros.put(CAMPO_RELATORIO_NUMERO_PARCELAS, parcelamento.getNumeroPrestacoes().toString());

		String numeroParcelasExtenso = new Extenso(parcelamento.getNumeroPrestacoes()).toString();
		numeroParcelasExtenso = numeroParcelasExtenso.replaceAll("reais", "");
		numeroParcelasExtenso = numeroParcelasExtenso.replaceAll("real", "");
		numeroParcelasExtenso = numeroParcelasExtenso.replaceAll("centavos", "");
		numeroParcelasExtenso = numeroParcelasExtenso.replaceAll("centavo", "");
		numeroParcelasExtenso = numeroParcelasExtenso.trim();

		parametros.put(CAMPO_RELATORIO_NUMERO_PARCELAS_EXTENSO, numeroParcelasExtenso);
		parametros.put(CAMPO_RELATORIO_VALORES_PARCELAS, valoresParcelas);
		parametros.put(CAMPO_RELATORIO_TAXA_MULTA, jurosMora.toString());
		parametros.put(CAMPO_RELATORIO_TAXA_JUROS_MORA, multaAtraso.toString());

		if (empresa.getCliente().getEnderecoPrincipal() != null) {
			parametros.put(CAMPO_RELATORIO_ENDERECO_EMPRESA, empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatado());
			parametros.put(CAMPO_RELATORIO_DESCRICAO_LOCALIDADE, empresa.getCliente().getEnderecoPrincipal().getCep().getNomeMunicipio());
		} else {
			parametros.put(CAMPO_RELATORIO_ENDERECO_EMPRESA, "");
			parametros.put(CAMPO_RELATORIO_DESCRICAO_LOCALIDADE, "");
		}

		parametros.put(CAMPO_RELATORIO_NOME_EMPRESA1, empresa.getCliente().getNome());
		parametros.put(CAMPO_RELATORIO_NOME_EMPRESA2, empresa.getCliente().getNome());
		parametros.put(CAMPO_RELATORIO_NOME_EMPRESA3, empresa.getCliente().getNome());
		parametros.put(CAMPO_RELATORIO_NOME_EMPRESA4, empresa.getCliente().getNome());
		parametros.put(CAMPO_RELATORIO_IMOVEL_DIA_VENCIMENTO,
						Util.converterDataParaStringSemHora(parcelamento.getDataParcelamento(), Constantes.FORMATO_DATA_BR));
		parametros.put(CAMPO_RELATORIO_COLECAO_ANO_MES_REFERENCIA, datasFaturas);
		Calendar calendar = Calendar.getInstance(Constantes.LOCALE_PADRAO);
		calendar.setTime(Calendar.getInstance().getTime());
		String mes = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Constantes.LOCALE_PADRAO);
		parametros.put(CAMPO_RELATORIO_MES_PORTUGUES, mes);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #gerarRelatorioNotaPromissoria
	 * (java.lang.Long, java.util.Collection)
	 */
	@Override
	public byte[] gerarRelatorioNotaPromissoria(Long idParcelamento, Collection<DadosParcelas> colecaoDadosParcelas)
					throws GGASException {

		byte[] bytes = null;
		Parcelamento parcelamento = (Parcelamento) obter(idParcelamento, CAMPO_CLIENTE, CAMPO_PARCELAMENTO_PERFIL, "pontoConsumo",
						"formaCobrancaParcelamento");
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		Map<String, Object> filtroEmpresa = new HashMap<>();
		filtroEmpresa.put("principal", Boolean.TRUE);
		Collection<Empresa> colecaoEmpresa = controladorEmpresa.consultarEmpresas(filtroEmpresa);
		Empresa empresa = null;
		if (!colecaoEmpresa.isEmpty()) {
			empresa = colecaoEmpresa.iterator().next();
		}

		if (empresa != null) {
			Cliente clienteEmpresa = (Cliente) controladorCliente.obter(empresa.getCliente().getChavePrimaria(), CAMPO_ENDERECOS);
			empresa.setCliente(clienteEmpresa);
		}

		if (empresa != null) {
			Cliente clienteEmpresa = (Cliente) controladorCliente.obter(empresa.getCliente().getChavePrimaria(), CAMPO_ENDERECOS);
			empresa.setCliente(clienteEmpresa);
		}

		if (parcelamento != null) {
			Collection<Parcelamento> listaDadosExtrato = new ArrayList<>();

			listaDadosExtrato.add(parcelamento);

			Map<String, Object> parametros = new HashMap<>();

			Collection<DadosRelatorioNotaPromissoria> colecaoDadosRelatorio = new ArrayList<>();

			for (DadosParcelas dadosParcelas : colecaoDadosParcelas) {
				this.popularColecaoRelatorioNotaPromissoria(colecaoDadosRelatorio, parcelamento, empresa, dadosParcelas);
			}

			bytes = RelatorioUtil.gerarRelatorioPDF(colecaoDadosRelatorio, parametros, RELATORIO_NOTA_PROMISSORIA);
		}
		return bytes;
	}

	/**
	 * Popular colecao relatorio nota promissoria.
	 *
	 * @param colecaoDadosRelatorio
	 *            the colecao dados relatorio
	 * @param parcelamento
	 *            the parcelamento
	 * @param empresa
	 *            the empresa
	 * @param dadoParcela
	 *            the dado parcela
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularColecaoRelatorioNotaPromissoria(Collection<DadosRelatorioNotaPromissoria> colecaoDadosRelatorio,
					Parcelamento parcelamento, Empresa empresa, DadosParcelas dadoParcela) throws GGASException {

		DadosRelatorioNotaPromissoria dadosRelatorio = new DadosRelatorioNotaPromissoriaImpl();
		String valorEntradaExtensoStringPrincipal = "";
		String valorEntradaExtensoStringSecundario = "";

		dadosRelatorio.setParamCodigoNota(dadoParcela.getNumeroParcela().toString());

		dadosRelatorio.setParamTotalPrestacoes(parcelamento.getNumeroPrestacoes().toString());

		if (dadoParcela.getDataVencimento() != null) {
			dadosRelatorio.setParamData(Util.converterDataParaStringSemHora(dadoParcela.getDataVencimento(), Constantes.FORMATO_DATA_BR));
		} else {
			dadosRelatorio.setParamData("");
		}

		dadosRelatorio.setParamValor(Util.converterCampoCurrencyParaString(dadoParcela.getTotal(), Constantes.LOCALE_PADRAO));

		Calendar calendar = Calendar.getInstance(Constantes.LOCALE_PADRAO);
		if (dadoParcela.getDataVencimento() != null) {
			calendar.setTime(dadoParcela.getDataVencimento());
		} else {
			calendar.setTime(Calendar.getInstance().getTime());
		}
		String mesExtenso = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Constantes.LOCALE_PADRAO);

		dadosRelatorio.setParamAno(String.valueOf(calendar.get(Calendar.YEAR)));

		DiaOrdinal diaOrdinal = new DiaOrdinal(Integer.valueOf(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
		dadosRelatorio.setParamDiaExtenso(diaOrdinal.toString().toUpperCase());

		if (!StringUtils.isEmpty(mesExtenso)) {
			dadosRelatorio.setParamMesExtenso(StringUtils.upperCase(mesExtenso));
		}

		dadosRelatorio.setParamNomeEmpresaExtenso(empresa.getCliente().getNome());
		dadosRelatorio.setParamNomeEmpresaAbreviacao(empresa.getCliente().getNomeAbreviado());
		dadosRelatorio.setParamCNPJEmpresa(empresa.getCliente().getNumeroDocumentoFormatado());

		Extenso valorEntradaExtenso = new Extenso(dadoParcela.getTotal());
		String valorEntradaExtensoString = valorEntradaExtenso.toString();
		if (valorEntradaExtensoString.length() > TAMANHO_MAXIMO_VALOR_EXTENSO) {
			String valorEntradaExtensoStringPrincipalAux = valorEntradaExtensoString.substring(0, TAMANHO_MAXIMO_VALOR_EXTENSO);
			for (int i = valorEntradaExtensoStringPrincipalAux.length() - 1; i > 0; i--) {
				if (valorEntradaExtensoStringPrincipalAux.charAt(i) == ' ') {
					valorEntradaExtensoStringPrincipal = valorEntradaExtensoStringPrincipalAux.substring(0, i);
					valorEntradaExtensoStringSecundario = valorEntradaExtensoString.substring(i + 1, valorEntradaExtenso.toString()
									.length());
					break;
				}
			}
		} else {
			valorEntradaExtensoStringPrincipal = valorEntradaExtensoString;
			valorEntradaExtensoStringSecundario = "X.X.X.X.X.X.X.X.X.X.X.X.X";
		}

		dadosRelatorio.setParamValorExtenso1(valorEntradaExtensoStringPrincipal);
		dadosRelatorio.setParamValorExtenso2(valorEntradaExtensoStringSecundario);

		if (empresa.getCliente().getEnderecoPrincipal() != null) {
			dadosRelatorio.setParamCidade(empresa.getCliente().getEnderecoPrincipal().getCep().getNomeMunicipio());
			dadosRelatorio.setParamEstado(empresa.getCliente().getEnderecoPrincipal().getCep().getUf());
		} else {
			dadosRelatorio.setParamCidade("");
			dadosRelatorio.setParamEstado("");
		}
		dadosRelatorio.setParamNomeCliente(parcelamento.getCliente().getNome());

		if (!StringUtils.isEmpty(parcelamento.getCliente().getCpfFormatado())) {
			dadosRelatorio.setParamCNPJCliente(parcelamento.getCliente().getCpfFormatado());
		} else {
			dadosRelatorio.setParamCNPJCliente(parcelamento.getCliente().getCnpjFormatado());
		}

		if (parcelamento.getCliente().getEnderecoPrincipal() != null) {
			dadosRelatorio.setParamEnderecoCliente(parcelamento.getCliente().getEnderecoPrincipal().getEnderecoFormatado());
		} else {
			dadosRelatorio.setParamEnderecoCliente("");
		}

		dadosRelatorio.setParamNomeAvalista1("");
		dadosRelatorio.setParamCPFAvalista1("");
		dadosRelatorio.setParamNomeAvalista2("");
		dadosRelatorio.setParamCPFAvalista2("");
		dadosRelatorio.setParamNomeAvalista3("");
		dadosRelatorio.setParamCPFAvalista3("");

		colecaoDadosRelatorio.add(dadosRelatorio);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.ControladorParcelamento#montarDadosGeraisParcelas(br.com.ggas.cobranca.parcelamento.Parcelamento)
	 */
	@Override
	public DadosGeraisParcelas montarDadosGeraisParcelas(Parcelamento parcelamento) throws GGASException {

		// FIXME: corrigir este método de acordo com o gerarParcelas(). Verificar a possibilidade de substituí-lo por ele.
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Map<LancamentoItemContabil, ItemParcelaVO> mapaValoresPorRubrica = new HashMap<>();
		BigDecimal somaValoresParcelamento = BigDecimal.ZERO;
		DadosGeraisParcelas dadosGerais = (DadosGeraisParcelas) this.criarDadosGeraisParcelas();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		if (parcelamento.getTipoCobranca() != null) {
			String paramCobrancaFatura = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_FATURA);

			EntidadeConteudo tipoCobrancaFatura = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramCobrancaFatura));
			if (parcelamento.getTipoCobranca().equals(tipoCobrancaFatura)) {
				dadosGerais.setCobrancaEmFatura(Boolean.TRUE);
			} else {
				dadosGerais.setCobrancaEmFatura(Boolean.FALSE);
			}
		}

		dadosGerais.setEmiteNotaPromissoria(parcelamento.isEmissaoNotasPromissorias());

		dadosGerais.setEmiteTermoConfissaoDivida(parcelamento.isEmissaoTermoParcelamento());

		if ((parcelamento.getCliente()) != null) {
			dadosGerais.setIdFiador(parcelamento.getFiador().getChavePrimaria());
		}

		if (parcelamento.getPontoConsumo() != null) {
			dadosGerais.setIdPontoConsumo(parcelamento.getPontoConsumo().getChavePrimaria());
		}

		Calendar dataVencimento = Calendar.getInstance();
		String paramTIpoCobrancaNotaAtualMes = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_NOTA_MES_POSTERIOR);

		EntidadeConteudo tipoCobrancaAtualMes = controladorEntidadeConteudo.obterEntidadeConteudo(Long
						.valueOf(paramTIpoCobrancaNotaAtualMes));
		if (parcelamento.getTipoCobranca() != null && parcelamento.getTipoCobranca().equals(tipoCobrancaAtualMes)) {
			dataVencimento.add(Calendar.MONTH, 1);
			// soma um mes
		}
		DateTime vencimentoInicial = new DateTime(dataVencimento.getTime());

		dadosGerais.setVencimentoInicial(vencimentoInicial.toDate());
		BigDecimal percDesconto = parcelamento.getPercentualDesconto();
		dadosGerais.setPercentualDesconto(percDesconto);
		dadosGerais.setValorDesconto(parcelamento.getValorDesconto());
		dadosGerais.setFormaCobranca(parcelamento.getFormaCobrancaParcelamento());
		dadosGerais.setPeriodicidadeParcelas(PERIODO_PARCELAS);

		if (parcelamento.getCliente() != null) {
			dadosGerais.setIdCliente(parcelamento.getCliente().getChavePrimaria());
		}

		if (parcelamento.getFiador() != null) {
			dadosGerais.setIdFiador(parcelamento.getFiador().getChavePrimaria());
		}

		if (parcelamento.getPontoConsumo() != null) {
			dadosGerais.setIdPontoConsumo(parcelamento.getPontoConsumo().getChavePrimaria());
		}

		BigDecimal somatorioTodosCreditos = BigDecimal.ZERO;
		BigDecimal somatorioTodosDebitos = BigDecimal.ZERO;
		Set<LancamentoItemContabil> listaCreditos = new HashSet<>();

		if (parcelamento.getListaParcelamentoItem() != null && !parcelamento.getListaParcelamentoItem().isEmpty()) {

			List<Fatura> listaFatura = new ArrayList<>();
			Collection<CreditoDebitoARealizar> listaCreditoDebito = new ArrayList<>();

			String codigoSituacaoPagamentoPendente = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
			EntidadeConteudo situacaoPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long
							.valueOf(codigoSituacaoPagamentoPendente));

			for (ParcelamentoItem parcelamentoItem : parcelamento.getListaParcelamentoItem()) {

				if (parcelamentoItem.getFaturaGeral() != null && parcelamentoItem.getFaturaGeral().getFaturaAtual() != null
								&& parcelamentoItem.getFaturaGeral().getFaturaAtual().getSituacaoPagamento().equals(situacaoPendente)) {
					// se
					// for
					// fatura

					Fatura fatura = parcelamentoItem.getFaturaGeral().getFaturaAtual();
					Collection<FaturaItem> listaFaturaItem = controladorFatura.listarFaturaItemPorChaveFatura(fatura.getChavePrimaria());

					for (FaturaItem faturaItem : listaFaturaItem) {
						// Agrupa os valores de acordo com o Lancamento Contabil
						if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getLancamentoItemContabil() != null) {
							if (mapaValoresPorRubrica.containsKey(faturaItem.getRubrica().getLancamentoItemContabil())) {
								ItemParcelaVO item = mapaValoresPorRubrica.get(faturaItem.getRubrica().getLancamentoItemContabil());
								item.getListaFaturas().add(faturaItem);
								item.setValor(item.getValor().add(faturaItem.getValorTotal()));
								mapaValoresPorRubrica.put(faturaItem.getRubrica().getLancamentoItemContabil(), item);

								if (faturaItem.getCreditoDebitoARealizar() != null
												&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado() != null
												&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getCreditoOrigem() != null) {

									somatorioTodosCreditos = somatorioTodosCreditos.add(faturaItem.getValorTotal());
									listaCreditos.add(faturaItem.getRubrica().getLancamentoItemContabil());
								} else {
									somaValoresParcelamento = somaValoresParcelamento.add(faturaItem.getValorTotal());
								}
							} else {
								ItemParcelaVO item = new ItemParcelaVO();
								item.setLancamentoItemContabil(faturaItem.getRubrica().getLancamentoItemContabil());
								item.setValor(faturaItem.getValorTotal());
								item.getListaFaturas().add(faturaItem);
								mapaValoresPorRubrica.put(faturaItem.getRubrica().getLancamentoItemContabil(), item);

								if (faturaItem.getCreditoDebitoARealizar() != null
												&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado() != null
												&& faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado().getCreditoOrigem() != null) {

									somatorioTodosCreditos = somatorioTodosCreditos.add(faturaItem.getValorTotal());
									listaCreditos.add(faturaItem.getRubrica().getLancamentoItemContabil());
								} else {
									somaValoresParcelamento = somaValoresParcelamento.add(faturaItem.getValorTotal());
								}
							}
						}
					}
					listaFatura.add(fatura);
				} else if (parcelamentoItem.getCreditoDebitoARealizar() != null
								&& parcelamentoItem.getCreditoDebitoARealizar().getSituacaoPagamento().equals(situacaoPendente)) {

					// Se for credito
					CreditoDebitoARealizar creditoDebito = parcelamentoItem.getCreditoDebitoARealizar();
					if (creditoDebito.getCreditoDebitoNegociado().getCreditoOrigem() != null) {
						somatorioTodosCreditos = somatorioTodosCreditos.add(creditoDebito.getValor());
					} else {
						somatorioTodosDebitos = somatorioTodosDebitos.add(creditoDebito.getValor());
					}

					if (creditoDebito.getCreditoDebitoNegociado().getRubrica() != null
									&& creditoDebito.getCreditoDebitoNegociado().getRubrica().getLancamentoItemContabil() != null) {

						LancamentoItemContabil lancContabil = creditoDebito.getCreditoDebitoNegociado().getRubrica()
										.getLancamentoItemContabil();

						if (mapaValoresPorRubrica.containsKey(lancContabil)) {
							ItemParcelaVO item = mapaValoresPorRubrica.get(lancContabil);
							item.getListaCreditoDebito().add(creditoDebito);
							item.setValor(item.getValor().add(creditoDebito.getValor()));

							mapaValoresPorRubrica.put(lancContabil, item);
						} else {
							ItemParcelaVO item = new ItemParcelaVO();
							item.setLancamentoItemContabil(lancContabil);
							item.setValor(creditoDebito.getValor());
							item.getListaCreditoDebito().add(creditoDebito);

							mapaValoresPorRubrica.put(lancContabil, item);
						}
					}
					listaCreditoDebito.add(creditoDebito);
				}
			}

			dadosGerais.getListaCreditoDebito().addAll(listaCreditoDebito);
			dadosGerais.getListaFatura().addAll(listaFatura);
		}

		somatorioTodosDebitos = somatorioTodosDebitos.add(somaValoresParcelamento);

		// Guarda as fatutas itens e os créditos/débitos que compoem o parcelamento agrupados por lançamento contábil.
		// Usado durante o salvamento do parcelamento para registrar os lançamentos contábeis.
		dadosGerais.setMapaValoresPorRubrica(mapaValoresPorRubrica);

		if (parcelamento.getParcelamentoPerfil() != null) {
			ParcelamentoPerfil perfil = parcelamento.getParcelamentoPerfil();

			if ((perfil.getValorDesconto() != null) && (percDesconto != null) && (percDesconto.compareTo(perfil.getValorDesconto()) > 0)) {
				throw new NegocioException(PERCENTUAL_POR_PARCELAMENTO_MAIOR_PERMITIDO_PERFIL, true);
			}

			dadosGerais.setParcelamentoPerfil(perfil);
			dadosGerais.setIndicarJuros(perfil.isIndicadorJuros());
			BigDecimal valorTotalLiquido = somatorioTodosDebitos.subtract(somatorioTodosCreditos);

			Integer numeroParcelas = parcelamento.getNumeroPrestacoes();
			dadosGerais.setNumeroPrestacoes(numeroParcelas);
			Integer periodicidadeParcelas = parcelamento.getNumeroDiasPeriodicidade();

			boolean cobrancaEmConta = Boolean.FALSE;
			String paramCobrancaConta = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_FATURA);

			EntidadeConteudo tipoCobrancaFatura = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramCobrancaConta));
			if (parcelamento.getTipoCobranca() != null && parcelamento.getTipoCobranca().equals(tipoCobrancaFatura)) {
				cobrancaEmConta = Boolean.TRUE;
			}

			if (cobrancaEmConta) {
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(dadosGerais.getIdPontoConsumo(), CAMPO_SEGMENTO);
				ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
									.getChavePrimaria());
				}

				if (contratoPontoConsumo != null) {
					ContratoPontoConsumoItemFaturamento cpcItemFaturamento = this
									.obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);

					if (cpcItemFaturamento != null) {
						dadosGerais.setVencimentoInicial(parcelamento.getDataVencimentoInicial());
						dadosGerais.setPeriodicidadeParcelas(periodicidadeParcelas);
					} else {
						throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_ITEM_FATURA, Boolean.TRUE);
					}
				}
			}
			Collection<ValorParcela> listaParcelas = new ArrayList<>();
			if (perfil.isIndicadorJuros()) {

				Long tipoPrice = Long.valueOf(controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_AMORTIZACAO_PRICE));

				String codigoPeriodicidadeJurosMensal = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_JUROS_MENSAL);
				EntidadeConteudo periodicidadeJuros = controladorEntidadeConteudo.obter(Long
								.valueOf(codigoPeriodicidadeJurosMensal));

				EntidadeConteudo tipoTabela = perfil.getTipoTabela();
				if (tipoTabela != null && tipoTabela.getChavePrimaria() == tipoPrice) {// Price
					listaParcelas = controladorCobranca.calcularPrice(valorTotalLiquido, numeroParcelas, perfil.getTaxaJuros(),
									dadosGerais.getVencimentoInicial(), periodicidadeParcelas,
									Integer.parseInt(periodicidadeJuros.getCodigo()), Boolean.TRUE);
				} else {// SAC
					listaParcelas = controladorCobranca.calcularSAC(valorTotalLiquido, numeroParcelas, perfil.getTaxaJuros(),
									dadosGerais.getVencimentoInicial(), periodicidadeParcelas,
									Integer.parseInt(periodicidadeJuros.getCodigo()), Boolean.TRUE);
				}
			} else {
				// Não aplicará juros as parcelas
				BigDecimal parcela = valorTotalLiquido.divide(new BigDecimal(numeroParcelas), ESCALA_PARCELAS, RoundingMode.HALF_UP);
				BigDecimal valorInicial = BigDecimal.ZERO;
				for (int i = 0; i < numeroParcelas; i++) {
					ValorParcela valorParcela = (ValorParcela) this.criarValorParcela();
					valorParcela.setValorPagar(parcela.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));

					if (valorInicial.compareTo(BigDecimal.ZERO) == 0) {
						valorInicial = valorTotalLiquido;
						valorParcela.setSaldo(valorInicial.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
					} else {
						valorInicial = valorInicial.subtract(parcela);
						valorParcela.setSaldo(valorInicial.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
					}

					DateTime novaData = new DateTime(dadosGerais.getVencimentoInicial());
					// TODO: Tratar mês civil
					novaData = novaData.plusDays(i * dadosGerais.getPeriodicidadeParcelas());
					valorParcela.setDataVencimento(novaData.toDate());

					listaParcelas.add(valorParcela);
				}
			}

			Map<LancamentoItemContabil, BigDecimal> mapaPercentuais = new HashMap<>();
			BigDecimal somatorioPercentuais = BigDecimal.ZERO;
			int count = 0;
			int tamanhoMapa = mapaValoresPorRubrica.entrySet().size();
			BigDecimal percentual = null;
			for (Map.Entry<LancamentoItemContabil, ItemParcelaVO> entry : mapaValoresPorRubrica.entrySet()) {
				LancamentoItemContabil laic = entry.getKey();
				count++;
				if (count == tamanhoMapa) {
					// verifica se é o último item
					ItemParcelaVO item = mapaValoresPorRubrica.get(laic);
					percentual = item.getValor().divide(somatorioTodosDebitos, ESCALA, RoundingMode.HALF_DOWN);

					somatorioPercentuais = somatorioPercentuais.add(percentual);
					mapaPercentuais.put(laic, percentual);
				} else {
					percentual = BigDecimal.ONE.subtract(somatorioPercentuais);
					mapaPercentuais.put(laic, percentual);
				}
			}

			// calcular Percentual de participação
			Map<String, BigDecimal> valores = new HashMap<>();
			BigDecimal percentPartValorOrigem = this.calcularPercentualParticipacaoOriginal(dadosGerais, valores);
			BigDecimal valorOrigem = valores.get("valorOrigem");
			BigDecimal valorAtualizado = valores.get("valorAtualizado");

			if (listaParcelas != null && !listaParcelas.isEmpty()) {

				String codigoLAICJurosParcelamento = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO);
				LancamentoItemContabil laicJurosParcelamento = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICJurosParcelamento));

				String codigoLAICJurosMora = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_MORA);
				LancamentoItemContabil laicJurosMora = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICJurosMora));

				String codigoLAICParcelamento = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_PARCELAMENTO);
				LancamentoItemContabil laicParcelamento = this.obterLancamentoItemContabil(Long.valueOf(codigoLAICParcelamento));

				BigDecimal acumularValor = new BigDecimal(0);
				BigDecimal acumularValorJurosMora = new BigDecimal(0);
				Collection<DadosParcelas> listaDadosParcelas = new ArrayList<>();
				int qtde = 1;
				BigDecimal valorTotalJuros = BigDecimal.ZERO;
				BigDecimal valorTotalAPagar = BigDecimal.ZERO;
				for (ValorParcela valorParcela : listaParcelas) {
					DadosParcelas dadosParcela = (DadosParcelas) this.criarDadosParcelas();
					dadosParcela.setNumeroParcela(qtde);

					if (valorParcela.getAmortizacao() != null) {
						dadosParcela.setAmortizacao(valorParcela.getAmortizacao().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));

						BigDecimal valor = (dadosParcela.getAmortizacao().multiply(percentPartValorOrigem)).setScale(DUAS_CASAS_DECIMAIS,
										RoundingMode.HALF_UP);
						BigDecimal valorJurosMora = dadosParcela.getAmortizacao().subtract(valor);

						// acumular
						acumularValor = acumularValor.add(valor);
						acumularValorJurosMora = acumularValorJurosMora.add(valorJurosMora);
						// atualizar perdas de arredondamento na ultima parcela
						if (qtde == listaParcelas.size()) {
							valor = valor.add(valorOrigem.subtract(acumularValor));

							BigDecimal valorJurosMoraOrigem = valorAtualizado.subtract(valorOrigem);
							valorJurosMora = valorJurosMora.add(valorJurosMoraOrigem.subtract(acumularValorJurosMora));
						}
						this.criarEAdicionarDadosItemParcela(dadosParcela, laicParcelamento, valor, null, null, null);
						if (valorJurosMora.compareTo(new BigDecimal(0)) > 0) {
							this.criarEAdicionarDadosItemParcela(dadosParcela, laicJurosMora, valorJurosMora, null, null, null);
						}

					} else {
						dadosParcela.setAmortizacao(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getJuros() != null) {
						dadosParcela.setJuros(valorParcela.getJuros().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
						this.criarEAdicionarDadosItemParcela(dadosParcela, laicJurosParcelamento, dadosParcela.getJuros(), null, null, null);
						valorTotalJuros = valorTotalJuros.add(dadosParcela.getJuros());
					} else {
						dadosParcela.setJuros(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getSaldo() != null) {
						dadosParcela.setSaldoInicial(valorParcela.getSaldo().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
					} else {
						dadosParcela.setSaldoInicial(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getValorPagar() != null) {
						dadosParcela.setTotal(valorParcela.getValorPagar().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
						valorTotalAPagar = valorTotalAPagar.add(dadosParcela.getTotal());
					} else {
						dadosParcela.setTotal(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
					}

					if (valorParcela.getDataVencimento() != null) {
						dadosParcela.setDataVencimento(valorParcela.getDataVencimento());
					}

					listaDadosParcelas.add(dadosParcela);
					qtde++;
				}

				dadosGerais.setValorTotalAPagar(valorTotalAPagar);
				dadosGerais.setValorTotalJuros(valorTotalJuros);
				dadosGerais.getListaDadosParcelas().addAll(listaDadosParcelas);
			}

			String parametroCodigoCobranca = Constantes.C_FORMA_COBRANCA_FATURA;
			if (!dadosGerais.isCobrancaEmFatura()) {
				parametroCodigoCobranca = Constantes.C_FORMA_COBRANCA_NOTA_DEBITO;
			}

			controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			dadosGerais.setFormaCobranca((EntidadeConteudo) controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(parametroCodigoCobranca))));

		}

		return dadosGerais;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.parcelamento.
	 * ControladorParcelamento
	 * #autorizarParcelamento(
	 * java.lang.Long,
	 * br.com.ggas.geral.EntidadeConteudo,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Parcelamento autorizarParcelamento(Long idParcelamento, EntidadeConteudo status, DadosAuditoria dadosAuditoria)
					throws GGASException {

		Parcelamento parcelamento = (Parcelamento) this.obter(idParcelamento, CAMPO_STATUS, "listaParcelamentoItem", CAMPO_PARCELAMENTO_PERFIL);
		DadosGeraisParcelas voParcela = this.montarDadosGeraisParcelas(parcelamento);
		voParcela.setDadosAuditoria(dadosAuditoria);
		Parcelamento novoParcelamento = this.montarParcelamento(voParcela);
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		if (novoParcelamento != null) {

			// Atualização do débito gerado pelo parcelamento.
			Map<String, Object> filtro = new HashMap<>();
			filtro.put("idParcelamento", parcelamento.getChavePrimaria());
			filtro.put(CAMPO_STATUS, parcelamento.getStatus().getChavePrimaria());

			Collection<CreditoDebitoNegociado> debitosParcelas = controladorCreditoDebito.consultarCreditoDebitoNegociado(filtro);
			if (!debitosParcelas.isEmpty()) {
				CreditoDebitoNegociado debitoGerado = debitosParcelas.iterator().next();
				debitoGerado.setParcelamento(null);
				debitoGerado.setStatus(status);
				debitoGerado.setUltimoUsuarioAlteracao(dadosAuditoria.getUsuario());
				debitoGerado.setDadosAuditoria(dadosAuditoria);

				controladorCreditoDebito.atualizar(debitoGerado, controladorCreditoDebito.getClasseEntidadeCreditoDebitoNegociado());
			}

			getSessionFactory().getCurrentSession().evict(parcelamento);

			// Copiando valores do parcelamento antigo
			novoParcelamento.setChavePrimaria(parcelamento.getChavePrimaria());
			novoParcelamento.setHabilitado(parcelamento.isHabilitado());
			novoParcelamento.setVersao(parcelamento.getVersao());

			// Removendo itens antigos
			for (ParcelamentoItem itemAntigo : parcelamento.getListaParcelamentoItem()) {
				this.remover(itemAntigo, getClasseEntidadeParcelamentoItem());
			}

			novoParcelamento.setStatus(status);
			novoParcelamento.setUltimoUsuarioAlteracao(dadosAuditoria.getUsuario());
			novoParcelamento.setDadosAuditoria(dadosAuditoria);

			this.atualizar(novoParcelamento, getClasseEntidade());
		}

		return novoParcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#buscarParcelamentoPorFaturaGeral(java.lang.Long)
	 */
	@Override
	public Parcelamento buscarParcelamentoPorFaturaGeral(Long idFaturaGeral) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select parcelamentoItem.parcelamento ");
		hql.append(FROM);
		hql.append(getClasseEntidadeParcelamentoItem().getSimpleName());
		hql.append(" parcelamentoItem ");
		hql.append(WHERE);
		hql.append(" parcelamentoItem.faturaGeral.chavePrimaria = :idFaturaGeral ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idFaturaGeral", idFaturaGeral);
		return (Parcelamento) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#consultarParcelamentosNotaDebito(java.lang.Long)
	 */
	@Override
	public Collection<ParcelaVO> consultarParcelamentosNotaDebito(Long idParcelamento) throws GGASException {

		Collection<ParcelaVO> listaParcelas = new ArrayList<>();
		Collection<Fatura> listaParcelasFatura = getControladorFatura().consultarFaturasPorParcelamento(idParcelamento);
		ParcelaVO voParcela = null;
		if (!listaParcelasFatura.isEmpty()) {
			int contador = 1;
			for (Fatura fatura : listaParcelasFatura) {
				voParcela = new ParcelaVO();
				voParcela.setNumeroParcela(String.valueOf(contador));
				voParcela.setNumeroDocumento(String.valueOf(fatura.getChavePrimaria()));
				BigDecimal valorSaldoFatura = getControladorFatura().calcularSaldoFatura(fatura);
				if (valorSaldoFatura != null) {
					voParcela.setValor(Util.converterCampoCurrencyParaString(valorSaldoFatura, Constantes.LOCALE_PADRAO));
				}
				if (fatura.getDataVencimento() != null) {
					voParcela.setDataVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
				}
				listaParcelas.add(voParcela);
				contador++;
			}

		}
		return listaParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.parcelamento.ControladorParcelamento#consultarParcelamentosCreditoARealizar(java.lang.Long)
	 */
	@Override
	public Collection<ParcelaVO> consultarParcelamentosCreditoARealizar(Long idParcelamento) throws GGASException {

		Collection<ParcelaVO> listaParcelas = new ArrayList<>();
		ParcelaVO voParcela = null;
		Collection<CreditoDebitoARealizar> listaParcelasCreditoDebito = getControladorCreditoDebito()
						.consultarCreditoDebitoARealizarPorParcelamento(idParcelamento);
		Integer ultimoNumeroParcela = 0;
		for (CreditoDebitoARealizar creditoDebitoARealizar : listaParcelasCreditoDebito) {
			voParcela = new ParcelaVO();
			if (!ultimoNumeroParcela.equals(creditoDebitoARealizar.getNumeroPrestacao())) {
				ultimoNumeroParcela = creditoDebitoARealizar.getNumeroPrestacao();
				voParcela.setNumeroParcela(String.valueOf(creditoDebitoARealizar.getNumeroPrestacao()));
				voParcela.setValor(Util.converterCampoCurrencyParaString(creditoDebitoARealizar.getValor(), Constantes.LOCALE_PADRAO));
				listaParcelas.add(voParcela);
			}
		}
		return listaParcelas;
	}
}
