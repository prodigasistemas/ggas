/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento.impl;

import br.com.ggas.cobranca.parcelamento.DadosRelatorioNotaPromissoria;

class DadosRelatorioNotaPromissoriaImpl implements DadosRelatorioNotaPromissoria {

	private static final long serialVersionUID = 6154996936339580983L;

	private String paramCodigoNota;

	private String paramAno;

	private String paramTotalPrestacoes;

	private String paramData;

	private String paramValor;

	private String paramDiaExtenso;

	private String paramMesExtenso;

	private String paramNomeEmpresaExtenso;

	private String paramNomeEmpresaAbreviacao;

	private String paramCNPJEmpresa;

	private String paramValorExtenso1;

	private String paramValorExtenso2;

	private String paramCidade;

	private String paramEstado;

	private String paramDataAtualExtenso;

	private String paramNomeCliente;

	private String paramCNPJCliente;

	private String paramEnderecoCliente;

	private String paramNomeAvalista1;

	private String paramCPFAvalista1;

	private String paramNomeAvalista2;

	private String paramCPFAvalista2;

	private String paramNomeAvalista3;

	private String paramCPFAvalista3;

	/**
	 * @return the paramCodigoNota
	 */
	@Override
	public String getParamCodigoNota() {

		return paramCodigoNota;
	}

	/**
	 * @param paramCodigoNota
	 *            the paramCodigoNota to set
	 */
	@Override
	public void setParamCodigoNota(String paramCodigoNota) {

		this.paramCodigoNota = paramCodigoNota;
	}

	/**
	 * @return the paramAno
	 */
	@Override
	public String getParamAno() {

		return paramAno;
	}

	/**
	 * @param paramAno
	 *            the paramAno to set
	 */
	@Override
	public void setParamAno(String paramAno) {

		this.paramAno = paramAno;
	}

	/**
	 * @return the paramTotalPrestacoes
	 */
	@Override
	public String getParamTotalPrestacoes() {

		return paramTotalPrestacoes;
	}

	/**
	 * @param paramTotalPrestacoes
	 *            the paramTotalPrestacoes to set
	 */
	@Override
	public void setParamTotalPrestacoes(String paramTotalPrestacoes) {

		this.paramTotalPrestacoes = paramTotalPrestacoes;
	}

	/**
	 * @return the paramData
	 */
	@Override
	public String getParamData() {

		return paramData;
	}

	/**
	 * @param paramData
	 *            the paramData to set
	 */
	@Override
	public void setParamData(String paramData) {

		this.paramData = paramData;
	}

	/**
	 * @return the paramValor
	 */
	@Override
	public String getParamValor() {

		return paramValor;
	}

	/**
	 * @param paramValor
	 *            the paramValor to set
	 */
	@Override
	public void setParamValor(String paramValor) {

		this.paramValor = paramValor;
	}

	/**
	 * @return the paramDiaExtenso
	 */
	@Override
	public String getParamDiaExtenso() {

		return paramDiaExtenso;
	}

	/**
	 * @param paramDiaExtenso
	 *            the paramDiaExtenso to set
	 */
	@Override
	public void setParamDiaExtenso(String paramDiaExtenso) {

		this.paramDiaExtenso = paramDiaExtenso;
	}

	/**
	 * @return the paramMesExtenso
	 */
	@Override
	public String getParamMesExtenso() {

		return paramMesExtenso;
	}

	/**
	 * @param paramMesExtenso
	 *            the paramMesExtenso to set
	 */
	@Override
	public void setParamMesExtenso(String paramMesExtenso) {

		this.paramMesExtenso = paramMesExtenso;
	}

	/**
	 * @return the paramNomeEmpresaExtenso
	 */
	@Override
	public String getParamNomeEmpresaExtenso() {

		return paramNomeEmpresaExtenso;
	}

	/**
	 * @param paramNomeEmpresaExtenso
	 *            the paramNomeEmpresaExtenso to
	 *            set
	 */
	@Override
	public void setParamNomeEmpresaExtenso(String paramNomeEmpresaExtenso) {

		this.paramNomeEmpresaExtenso = paramNomeEmpresaExtenso;
	}

	/**
	 * @return the paramNomeEmpresaAbreviacao
	 */
	@Override
	public String getParamNomeEmpresaAbreviacao() {

		return paramNomeEmpresaAbreviacao;
	}

	/**
	 * @param paramNomeEmpresaAbreviacao
	 *            the paramNomeEmpresaAbreviacao
	 *            to set
	 */
	@Override
	public void setParamNomeEmpresaAbreviacao(String paramNomeEmpresaAbreviacao) {

		this.paramNomeEmpresaAbreviacao = paramNomeEmpresaAbreviacao;
	}

	/**
	 * @return the paramCNPJEmpresa
	 */
	@Override
	public String getParamCNPJEmpresa() {

		return paramCNPJEmpresa;
	}

	/**
	 * @param paramCNPJEmpresa
	 *            the paramCNPJEmpresa to set
	 */
	@Override
	public void setParamCNPJEmpresa(String paramCNPJEmpresa) {

		this.paramCNPJEmpresa = paramCNPJEmpresa;
	}

	/**
	 * @return the paramValorExtenso1
	 */
	@Override
	public String getParamValorExtenso1() {

		return paramValorExtenso1;
	}

	/**
	 * @param paramValorExtenso1
	 *            the paramValorExtenso1 to set
	 */
	@Override
	public void setParamValorExtenso1(String paramValorExtenso1) {

		this.paramValorExtenso1 = paramValorExtenso1;
	}

	/**
	 * @return the paramValorExtenso2
	 */
	@Override
	public String getParamValorExtenso2() {

		return paramValorExtenso2;
	}

	/**
	 * @param paramValorExtenso2
	 *            the paramValorExtenso2 to set
	 */
	@Override
	public void setParamValorExtenso2(String paramValorExtenso2) {

		this.paramValorExtenso2 = paramValorExtenso2;
	}

	/**
	 * @return the paramCidade
	 */
	@Override
	public String getParamCidade() {

		return paramCidade;
	}

	/**
	 * @param paramCidade
	 *            the paramCidade to set
	 */
	@Override
	public void setParamCidade(String paramCidade) {

		this.paramCidade = paramCidade;
	}

	/**
	 * @return the paramEstado
	 */
	@Override
	public String getParamEstado() {

		return paramEstado;
	}

	/**
	 * @param paramEstado
	 *            the paramEstado to set
	 */
	@Override
	public void setParamEstado(String paramEstado) {

		this.paramEstado = paramEstado;
	}

	/**
	 * @return the paramDataAtualExtenso
	 */
	@Override
	public String getParamDataAtualExtenso() {

		return paramDataAtualExtenso;
	}

	/**
	 * @param paramDataAtualExtenso
	 *            the paramDataAtualExtenso to set
	 */
	@Override
	public void setParamDataAtualExtenso(String paramDataAtualExtenso) {

		this.paramDataAtualExtenso = paramDataAtualExtenso;
	}

	/**
	 * @return the paramNomeCliente
	 */
	@Override
	public String getParamNomeCliente() {

		return paramNomeCliente;
	}

	/**
	 * @param paramNomeCliente
	 *            the paramNomeCliente to set
	 */
	@Override
	public void setParamNomeCliente(String paramNomeCliente) {

		this.paramNomeCliente = paramNomeCliente;
	}

	/**
	 * @return the paramCNPJCliente
	 */
	@Override
	public String getParamCNPJCliente() {

		return paramCNPJCliente;
	}

	/**
	 * @param paramCNPJCliente
	 *            the paramCNPJCliente to set
	 */
	@Override
	public void setParamCNPJCliente(String paramCNPJCliente) {

		this.paramCNPJCliente = paramCNPJCliente;
	}

	/**
	 * @return the paramEnderecoCliente
	 */
	@Override
	public String getParamEnderecoCliente() {

		return paramEnderecoCliente;
	}

	/**
	 * @param paramEnderecoCliente
	 *            the paramEnderecoCliente to set
	 */
	@Override
	public void setParamEnderecoCliente(String paramEnderecoCliente) {

		this.paramEnderecoCliente = paramEnderecoCliente;
	}

	/**
	 * @return the paramNomeAvalista1
	 */
	@Override
	public String getParamNomeAvalista1() {

		return paramNomeAvalista1;
	}

	/**
	 * @param paramNomeAvalista1
	 *            the paramNomeAvalista1 to set
	 */
	@Override
	public void setParamNomeAvalista1(String paramNomeAvalista1) {

		this.paramNomeAvalista1 = paramNomeAvalista1;
	}

	/**
	 * @return the paramCPFAvalista1
	 */
	@Override
	public String getParamCPFAvalista1() {

		return paramCPFAvalista1;
	}

	/**
	 * @param paramCPFAvalista1
	 *            the paramCPFAvalista1 to set
	 */
	@Override
	public void setParamCPFAvalista1(String paramCPFAvalista1) {

		this.paramCPFAvalista1 = paramCPFAvalista1;
	}

	/**
	 * @return the paramNomeAvalista2
	 */
	@Override
	public String getParamNomeAvalista2() {

		return paramNomeAvalista2;
	}

	/**
	 * @param paramNomeAvalista2
	 *            the paramNomeAvalista2 to set
	 */
	@Override
	public void setParamNomeAvalista2(String paramNomeAvalista2) {

		this.paramNomeAvalista2 = paramNomeAvalista2;
	}

	/**
	 * @return the paramCPFAvalista2
	 */
	@Override
	public String getParamCPFAvalista2() {

		return paramCPFAvalista2;
	}

	/**
	 * @param paramCPFAvalista2
	 *            the paramCPFAvalista2 to set
	 */
	@Override
	public void setParamCPFAvalista2(String paramCPFAvalista2) {

		this.paramCPFAvalista2 = paramCPFAvalista2;
	}

	/**
	 * @return the paramNomeAvalista3
	 */
	@Override
	public String getParamNomeAvalista3() {

		return paramNomeAvalista3;
	}

	/**
	 * @param paramNomeAvalista3
	 *            the paramNomeAvalista3 to set
	 */
	@Override
	public void setParamNomeAvalista3(String paramNomeAvalista3) {

		this.paramNomeAvalista3 = paramNomeAvalista3;
	}

	/**
	 * @return the paramCPFAvalista3
	 */
	@Override
	public String getParamCPFAvalista3() {

		return paramCPFAvalista3;
	}

	/**
	 * @param paramCPFAvalista3
	 *            the paramCPFAvalista3 to set
	 */
	@Override
	public void setParamCPFAvalista3(String paramCPFAvalista3) {

		this.paramCPFAvalista3 = paramCPFAvalista3;
	}

}
