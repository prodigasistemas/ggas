/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cobranca.perfilparcelamento.ParcelamentoPerfil;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.EntidadeConteudo;
/**
 * Interface responsável pela assinatura dos métodos relacionados aos dados gerais das parcelas. 
 *
 */
public interface DadosGeraisParcelas extends Serializable {

	String BEAN_ID_DADOS_GERAIS_PARCELAS = "dadosGeraisParcelas";

	/**
	 * @return the valorTotalAPagar
	 */
	BigDecimal getValorTotalAPagar();

	/**
	 * @param valorTotalAPagar
	 *            the valorTotalAPagar to set
	 */
	void setValorTotalAPagar(BigDecimal valorTotalAPagar);

	/**
	 * @return the formaCobranca
	 */
	EntidadeConteudo getFormaCobranca();

	/**
	 * @param formaCobranca
	 *            the formaCobranca to set
	 */
	void setFormaCobranca(EntidadeConteudo formaCobranca);

	/**
	 * @return the valorTotalJuros
	 */
	BigDecimal getValorTotalJuros();

	/**
	 * @param valorTotalJuros
	 *            the valorTotalJuros to set
	 */
	void setValorTotalJuros(BigDecimal valorTotalJuros);

	/**
	 * @return the indicarJuros
	 */
	boolean isIndicarJuros();

	/**
	 * @param indicarJuros
	 *            the indicarJuros to set
	 */
	void setIndicarJuros(boolean indicarJuros);

	/**
	 * @return the vencimentoInicial
	 */
	Date getVencimentoInicial();

	/**
	 * @param vencimentoInicial
	 *            the vencimentoInicial to set
	 */
	void setVencimentoInicial(Date vencimentoInicial);

	/**
	 * @return the indicadorVencimentoInicial
	 */
	boolean isIndicadorVencimentoInicial();

	/**
	 * @param indicadorVencimentoInicial
	 *            the indicadorVencimentoInicial
	 *            to set
	 */
	void setIndicadorVencimentoInicial(boolean indicadorVencimentoInicial);

	/**
	 * @return the periodicidadeParcelas
	 */
	Integer getPeriodicidadeParcelas();

	/**
	 * @param periodicidadeParcelas
	 *            the periodicidadeParcelas to set
	 */
	void setPeriodicidadeParcelas(Integer periodicidadeParcelas);

	/**
	 * @return the parcelamentoPerfil
	 */
	ParcelamentoPerfil getParcelamentoPerfil();

	/**
	 * @param parcelamentoPerfil
	 *            the parcelamentoPerfil to set
	 */
	void setParcelamentoPerfil(ParcelamentoPerfil parcelamentoPerfil);

	/**
	 * @return the valorEntrada
	 */
	BigDecimal getValorEntrada();

	/**
	 * @param valorEntrada
	 *            the valorEntrada to set
	 */
	void setValorEntrada(BigDecimal valorEntrada);

	/**
	 * @return the idRubrica
	 */
	Long getIdRubrica();

	/**
	 * @param idRubrica
	 *            the idRubrica to set
	 */
	void setIdRubrica(Long idRubrica);

	/**
	 * @return the idCreditoOrigem
	 */
	Long getIdCreditoOrigem();

	/**
	 * @param idCreditoOrigem
	 *            the idCreditoOrigem to set
	 */
	void setIdCreditoOrigem(Long idCreditoOrigem);

	/**
	 * @return the taxaJuros
	 */
	BigDecimal getTaxaJuros();

	/**
	 * @param taxaJuros
	 *            the taxaJuros to set
	 */
	void setTaxaJuros(BigDecimal taxaJuros);

	/**
	 * @return the dataInicioCobranca
	 */
	Date getDataInicioCobranca();

	/**
	 * @param dataInicioCobranca
	 *            the dataInicioCobranca to set
	 */
	void setDataInicioCobranca(Date dataInicioCobranca);

	/**
	 * @return the dataInicioCredito
	 */
	Date getDataInicioCredito();

	/**
	 * @param dataInicioCredito
	 *            the dataInicioCredito to set
	 */
	void setDataInicioCredito(Date dataInicioCredito);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the quantidade
	 */
	BigDecimal getQuantidade();

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	void setQuantidade(BigDecimal quantidade);

	/**
	 * @return the quantidadeDiasCobranca
	 */
	String getQuantidadeDiasCobranca();

	/**
	 * @param quantidadeDiasCobranca
	 *            the quantidadeDiasCobranca to
	 *            set
	 */
	void setQuantidadeDiasCobranca(String quantidadeDiasCobranca);

	/**
	 * @return the idInicioCobranca
	 */
	Long getIdInicioCobranca();

	/**
	 * @param idInicioCobranca
	 *            the idInicioCobranca to set
	 */
	void setIdInicioCobranca(Long idInicioCobranca);

	/**
	 * @return the idPeriodicidadeJuros
	 */
	Long getIdPeriodicidadeJuros();

	/**
	 * @param idPeriodicidadeJuros
	 *            the idPeriodicidadeJuros to set
	 */
	void setIdPeriodicidadeJuros(Long idPeriodicidadeJuros);

	/**
	 * @return the idPeriodicidade
	 */
	Long getIdPeriodicidade();

	/**
	 * @param idPeriodicidade
	 *            the idPeriodicidade to set
	 */
	void setIdPeriodicidade(Long idPeriodicidade);

	/**
	 * @return the cobrancaEmFatura
	 */
	boolean isCobrancaEmFatura();

	/**
	 * @param cobrancaEmFatura
	 *            the cobrancaEmFatura to set
	 */
	void setCobrancaEmFatura(boolean cobrancaEmFatura);

	/**
	 * @return the emiteNotaPromissoria
	 */
	boolean isEmiteNotaPromissoria();

	/**
	 * @param emiteNotaPromissoria
	 *            the emiteNotaPromissoria to set
	 */
	void setEmiteNotaPromissoria(boolean emiteNotaPromissoria);

	/**
	 * @return the emiteTermoConfissaoDivida
	 */
	boolean isEmiteTermoConfissaoDivida();

	/**
	 * @param emiteTermoConfissaoDivida
	 *            the emiteTermoConfissaoDivida to
	 *            set
	 */
	void setEmiteTermoConfissaoDivida(boolean emiteTermoConfissaoDivida);

	/**
	 * @return the idFiador
	 */
	Long getIdFiador();

	/**
	 * @param idFiador
	 *            the idFiador to set
	 */
	void setIdFiador(Long idFiador);

	/**
	 * @return the listaDadosParcelas
	 */
	Collection<DadosParcelas> getListaDadosParcelas();

	/**
	 * @return the idPontoConsumo
	 */
	Long getIdPontoConsumo();

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	void setIdPontoConsumo(Long idPontoConsumo);

	/**
	 * @return the dadosAuditoria
	 */
	DadosAuditoria getDadosAuditoria();

	/**
	 * @param dadosAuditoria
	 *            the dadosAuditoria to set
	 */
	void setDadosAuditoria(DadosAuditoria dadosAuditoria);

	/**
	 * @param listaDadosParcelas
	 *            the listaDadosParcelas to set
	 */
	void setListaDadosParcelas(Collection<DadosParcelas> listaDadosParcelas);

	/**
	 * @return the listaFatura
	 */
	Collection<Fatura> getListaFatura();

	/**
	 * @param listaFatura
	 *            the listaFatura to set
	 */
	void setListaFatura(Collection<Fatura> listaFatura);

	/**
	 * @return the listaCreditoDebito
	 */
	Collection<CreditoDebitoARealizar> getListaCreditoDebito();

	/**
	 * @param listaCreditoDebito
	 *            the listaCreditoDebito to set
	 */
	void setListaCreditoDebito(Collection<CreditoDebitoARealizar> listaCreditoDebito);

	/**
	 * @return the numeroPrestacoes
	 */
	Integer getNumeroPrestacoes();

	/**
	 * @param numeroPrestacoes
	 *            the numeroPrestacoes to set
	 */
	void setNumeroPrestacoes(Integer numeroPrestacoes);

	/**
	 * @return the valorDesconto
	 */
	BigDecimal getValorDesconto();

	/**
	 * @param valorDesconto
	 *            the valorDesconto to set
	 */
	void setValorDesconto(BigDecimal valorDesconto);

	/**
	 * @return the percentualDesconto
	 */
	BigDecimal getPercentualDesconto();

	/**
	 * @param percentualDesconto
	 *            the percentualDesconto to set
	 */
	void setPercentualDesconto(BigDecimal percentualDesconto);

	/**
	 * @return the idCliente
	 */
	Long getIdCliente();

	/**
	 * @param idCliente
	 *            the idCliente to set
	 */
	void setIdCliente(Long idCliente);

	/**
	 * @return the motivoCancelamento
	 */
	String getMotivoCancelamento();

	/**
	 * @param motivoCancelamento
	 *            the motivoCancelamento to set
	 */
	void setMotivoCancelamento(String motivoCancelamento);

	/**
	 * @return the valorUnitario
	 */
	BigDecimal getValorUnitario();

	/**
	 * @param valorUnitario
	 *            the valorUnitario to set
	 */
	void setValorUnitario(BigDecimal valorUnitario);

	/**
	 * @return the melhorDiaVencimento
	 */
	String getMelhorDiaVencimento();

	/**
	 * @param melhorDiaVencimento
	 *            the melhorDiaVencimento to set
	 */
	void setMelhorDiaVencimento(String melhorDiaVencimento);

	/**
	 * @return the mapaValoresPorRubrica
	 */
	Map<LancamentoItemContabil, ItemParcelaVO> getMapaValoresPorRubrica();

	/**
	 * @param mapaValoresPorRubrica
	 *            the mapaValoresPorRubrica to set
	 */
	void setMapaValoresPorRubrica(Map<LancamentoItemContabil, ItemParcelaVO> mapaValoresPorRubrica);

	/**
	 * @return
	 */
	public EntidadeConteudo getTipoCobranca();

	/**
	 * @param tipoCobranca
	 */
	public void setTipoCobranca(EntidadeConteudo tipoCobranca);

}
