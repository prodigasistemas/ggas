/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.web.cobranca.parcelamento.ParcelaVO;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Parcelamento. 
 *
 */
public interface ControladorParcelamento extends ControladorNegocio {

	int TAMANHO_MAXIMO_VALOR_EXTENSO = 59;

	String BEAN_ID_CONTROLADOR_PARCELAMENTO = "controladorParcelamento";

	String ERRO_NEGOCIO_CLIENTE_IMOVEL_SELECIONADO = "ERRO_NEGOCIO_CLIENTE_IMOVEL_SELECIONADO";

	String ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA = "ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA";

	String ERRO_NEGOCIO_NUMERO_PARCELAS = "ERRO_NEGOCIO_NUMERO_PARCELAS";

	String ERRO_NEGOCIO_VENCIMENTO_INICIAL_MENOR = "ERRO_NEGOCIO_VENCIMENTO_INICIAL_MENOR";

	String ERRO_NEGOCIO_VALOR_DESCONTO = "ERRO_NEGOCIO_VALOR_DESCONTO";

	String ERRO_NEGOCIO_ITEM_FATURA = "ERRO_NEGOCIO_ITEM_FATURA";

	String RELATORIO_INSTRUMENTO_CONFISSAO_DIVIDAS = "relatorioConfissaoDividas.jasper";

	String RELATORIO_NOTA_PROMISSORIA = "relatorioNotaPromissoria.jasper";

	String PERCENTUAL_POR_PARCELAMENTO_MAIOR_PERMITIDO_PERFIL = "PERCENTUAL_POR_PARCELAMENTO_MAIOR_PERMITIDO_PERFIL";

	String PARCELAMENTO_ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA = "PARCELAMENTO_ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA";

	String PARCELAMENTO_ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA_PONTO = "PARCELAMENTO_ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA_PONTO";

	// -------- campos do relatório de confissao
	// de divida
	String CAMPO_RELATORIO_CNPJ_EMPRESA = "cnpjEmpresa";

	String CAMPO_RELATORIO_NOME_CLIENTE = "nomeCliente";

	String CAMPO_RELATORIO_ENDERECO = "getEndereco";

	String CAMPO_RELATORIO_CPF_CNPJ_CLIENTE = "cpfCnpjCliente";

	String CAMPO_RELATORIO_TOTAL_FATURA = "totalFatura";

	String CAMPO_RELATORIO_TOTAL_FATURA_EXTENSO = "totalFaturaExtenso";

	String CAMPO_RELATORIO_VALOR_ENTRADA = "valorEntrada";

	String CAMPO_RELATORIO_VALOR_ENTRADA_EXTENSO = "valorEntradaExtenso";

	String CAMPO_RELATORIO_DATA_PARCELAMENTO = "dataParcelamento";

	String CAMPO_RELATORIO_NUMERO_PARCELAS = "numeroParcelas";

	String CAMPO_RELATORIO_NUMERO_PARCELAS_EXTENSO = "numeroParcelasExtenso";

	String CAMPO_RELATORIO_VALORES_PARCELAS = "valoresParcelas";

	String CAMPO_RELATORIO_TAXA_MULTA = "taxaMulta";

	String CAMPO_RELATORIO_TAXA_MULTA_EXTENSO = "taxaMultaExtenso";

	String CAMPO_RELATORIO_TAXA_JUROS_MORA = "taxaJurosMora";

	String CAMPO_RELATORIO_TAXA_JUROS_MORA_EXTENSO = "taxaJurosMoraExtenso";

	String CAMPO_RELATORIO_IMAGEM = "imagem";

	String CAMPO_RELATORIO_ENDERECO_EMPRESA = "enderecoEmpresa";

	String CAMPO_RELATORIO_MES_PORTUGUES = "mesPortugues";

	String CAMPO_RELATORIO_NOME_EMPRESA1 = "nomeEmpresa1";

	String CAMPO_RELATORIO_NOME_EMPRESA2 = "nomeEmpresa2";

	String CAMPO_RELATORIO_NOME_EMPRESA3 = "nomeEmpresa3";

	String CAMPO_RELATORIO_NOME_EMPRESA4 = "nomeEmpresa4";

	String CAMPO_RELATORIO_IMOVEL_DIA_VENCIMENTO = "imovelDiaVencimento";

	String CAMPO_RELATORIO_DESCRICAO_LOCALIDADE = "descricaoLocalidade";

	String CAMPO_RELATORIO_COLECAO_ANO_MES_REFERENCIA = "colecaoAnoMesReferencia";

	String CAMPO_RELATORIO_COLECAO_ANO_MES_REFERENCIA_SOBRA = "colecaoAnoMesReferenciaSobra";

	String CAMPO_RELATORIO_CPF_CNPJ_FIADOR = "cpfCnpjFiador";

	String CAMPO_RELATORIO_NOME_FIADOR = "nomeFiador";

	String CAMPO_RELATORIO_INDICADOR_FIADOR = "paramFiador";

	// ------ campos do relatório de nota
	// promissoria
	String CAMPO_RELATORIO_PARAM_CODIGO_NOTA = "paramCodigoNota";

	String CAMPO_RELATORIO_PARAM_ANO = "paramAno";

	String CAMPO_RELATORIO_PARAM_DATA = "paramData";

	String CAMPO_RELATORIO_PARAM_TOTAL_PRESTACOES = "paramTotalPrestacoes";

	String CAMPO_RELATORIO_PARAM_VALOR = "paramValor";

	String CAMPO_RELATORIO_PARAM_DIA_EXTENSO = "paramDiaExtenso";

	String CAMPO_RELATORIO_PARAM_MES_EXTENSO = "paramMesExtenso";

	String CAMPO_RELATORIO_PARAM_NOME_EMPRESA_EXTENSO = "paramNomeEmpresaExtenso";

	String CAMPO_RELATORIO_PARAM_NOME_EMPRESA_ABREVIACAO = "paramNomeEmpresaAbreviacao";

	String CAMPO_RELATORIO_PARAM_CNPJ_EMPRESA = "paramCNPJEmpresa";

	String CAMPO_RELATORIO_PARAM_VALOR_EXTENSO1 = "paramValorExtenso1";

	String CAMPO_RELATORIO_PARAM_VALOR_EXTENSO2 = "paramValorExtenso2";

	String CAMPO_RELATORIO_PARAM_CIDADE = "paramCidade";

	String CAMPO_RELATORIO_PARAM_ESTADO = "paramEstado";

	String CAMPO_RELATORIO_PARAM_DATA_ATUAL_EXTENSO = "paramDataAtualExtenso";

	String CAMPO_RELATORIO_PARAM_NOME_CLIENTE = "paramNomeCliente";

	String CAMPO_RELATORIO_PARAM_CNPJ_CLIENTE = "paramCNPJCliente";

	String CAMPO_RELATORIO_PARAM_ENDERECO_CLIENTE = "paramEnderecoCliente";

	String CAMPO_RELATORIO_PARAM_NOME_AVALISTA_1 = "paramNomeAvalista1";

	String CAMPO_RELATORIO_PARAM_CPF_AVALISTA_1 = "paramCPFAvalista1";

	String CAMPO_RELATORIO_PARAM_NOME_AVALISTA_2 = "paramNomeAvalista2";

	String CAMPO_RELATORIO_PARAM_CPF_AVALISTA_2 = "paramCPFAvalista2";

	String CAMPO_RELATORIO_PARAM_NOME_AVALISTA_3 = "paramNomeAvalista3";

	String CAMPO_RELATORIO_PARAM_CPF_AVALISTA_3 = "paramNomeAvalista3";

	/**
	 * Método responsável por obter os dados
	 * (Faturas/Notas de Débtos/Notas de
	 * Créditos/Débitos a Cobrar/Créditos a
	 * realizar)
	 * para exibição da tela de efetuar
	 * parcelamento de débitos.
	 * 
	 * @param idCliente
	 *            A chave primária do cliente.
	 * @param idPontoConsumo
	 *            A chave primária do ponto de
	 *            consumo
	 * @return DadosParcelamento Os dados para
	 *         exibição na tela.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DadosParcelamento obterDadosExibicaoEfetuarParcelamento(Long idCliente, Long idPontoConsumo) throws
					IllegalAccessException, InvocationTargetException, NoSuchMethodException, GGASException;

	/**
	 * Método responsável por consultar os
	 * parcelamentos do cliente.
	 * 
	 * @param idCliente
	 *            Id do cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @param statusParcelamento
	 *            Status do Parcelamento
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return Lista de Parcelamentos.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Parcelamento> consultarParcelamentoCliente(Long idCliente, Long idPontoConsumo, Date dataInicial, Date dataFinal,
					Long statusParcelamento, Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por validar os filtros
	 * que serão usados para pesquisar um
	 * parcelamento de débito.
	 * 
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarFiltroPesquisaParcelamentoDebitos(String dataInicial, String dataFinal) throws NegocioException;

	/**
	 * Método responsável por gerar as parcelas.
	 * 
	 * @param dados
	 *            Um mapa contendo as informações
	 *            do parcelamento
	 * @param chavesFatura
	 *            the chaves fatura
	 * @param chavesCreditoDebito
	 *            the chaves credito debito
	 * @return the dados gerais parcelas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro na
	 *             formatação dos números.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DadosGeraisParcelas gerarParcelas(Map<String, Object> dados, Long[] chavesFatura, Long[] chavesCreditoDebito) throws GGASException;

	/**
	 * Método responsável por salvar o
	 * parcelamento do débito.
	 * 
	 * @param dadosGerais
	 *            VO contendo as informações
	 *            necessárias para salvar o
	 *            parcelamento
	 * @param indicadorNotaPromissoria
	 *            the indicador nota promissoria
	 * @param indicadorConfissaoDivida
	 *            the indicador confissao divida
	 * @return the byte[]
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] salvarParcelamento(DadosGeraisParcelas dadosGerais, boolean indicadorNotaPromissoria, boolean indicadorConfissaoDivida)
					throws GGASException;

	/**
	 * Método Para retornar os Parcelamentos
	 * vinculados a um determinado
	 * PerfilParcelamento.
	 * 
	 * @param idPerfilParcelamento
	 *            the id perfil parcelamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Parcelamento> consultarParcelamentosVinculadosPerfilParcelamento(Long idPerfilParcelamento) throws NegocioException;

	/**
	 * Método responsável por listar os itens de
	 * parcelamento pela chave do parcelamento.
	 * 
	 * @param idParcelamento
	 *            Chave primária do parcelamento.
	 * @return Uma coleção de ParcelamentoItem.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ParcelamentoItem> listarParcelamentoItemPorChaveParcelamento(Long idParcelamento) throws NegocioException;

	/**
	 * Método responsável por instanciar um VO de
	 * DadosParcelas;.
	 * 
	 * @return object
	 */
	Object criarDadosParcelas();

	/**
	 * Método responsável por instanciar um VO de
	 * DadosGeraisParcelas.
	 * 
	 * @return Object
	 */
	Object criarDadosGeraisParcelas();

	/**
	 * Método responsável por instanciar um VO de
	 * ValorParcela.
	 * 
	 * @return Object
	 */
	Object criarValorParcela();

	/**
	 * Método responsável por instanciar uma
	 * Fatura.
	 * 
	 * @return EntidadeNegocio
	 */
	EntidadeNegocio criarFatura();

	/**
	 * Método responsável por instanciar um Tipo
	 * Documento Notas Debito.
	 * 
	 * @return TipoDocumento
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	TipoDocumento obterTipoDocumentoNotasDebito() throws NegocioException;

	/**
	 * Método responsável por instanciar um Débito
	 * Situação.
	 * 
	 * @param codigoSituacao
	 *            the codigo situacao
	 * @return CreditoDebitoSituacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CreditoDebitoSituacao obterDebitoSituacao(String codigoSituacao) throws NegocioException;

	/**
	 * Método responsável por instanciar uma
	 * Fatura Item.
	 * 
	 * @return EntidadeNegocio
	 */
	EntidadeNegocio criarFaturaItem();

	/**
	 * Método responsável por instanciar uma
	 * Fatura geral.
	 * 
	 * @return EntidadeNegocio
	 */
	EntidadeNegocio criarFaturaGeral();

	/**
	 * Método responsável por gerar relatório de
	 * notas promissórias.
	 * 
	 * @param idParcelamento
	 *            the id parcelamento
	 * @param colecaoDadosParcelas
	 *            the colecao dados parcelas
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorioNotaPromissoria(Long idParcelamento, Collection<DadosParcelas> colecaoDadosParcelas) throws
					GGASException;

	/**
	 * Método responsável por gerar um relatório
	 * de confissão de dívidas.
	 * 
	 * @param idParcelamento
	 *            the id parcelamento
	 * @param colecaoDadosParcelas
	 *            the colecao dados parcelas
	 * @param listaFatura
	 *            the lista fatura
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] gerarRelatorioConfissaoDividas(Long idParcelamento, Collection<DadosParcelas> colecaoDadosParcelas,
					Collection<Fatura> listaFatura) throws NegocioException;

	/**
	 * Método responsável por obter um Contrato
	 * Ponto de Consumo Item Faturamento.
	 * 
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @return the contrato ponto consumo item faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamento(ContratoPontoConsumo contratoPontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por obter um Lançamento
	 * Item Contábil.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the lancamento item contabil
	 * @throws NegocioException
	 *             the negocio exception
	 */
	LancamentoItemContabil obterLancamentoItemContabil(Long chavePrimaria) throws NegocioException;

	/**
	 * Validar filtro pesquisa pontos consumo.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @param idCliente
	 *            the id cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFiltroPesquisaPontosConsumo(Long idImovel, Long idCliente) throws NegocioException;

	/**
	 * Montar parcelamento.
	 * 
	 * @param dadosGerais
	 *            the dados gerais
	 * @return the parcelamento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Parcelamento montarParcelamento(DadosGeraisParcelas dadosGerais) throws GGASException;

	/**
	 * Montar dados gerais parcelas.
	 * 
	 * @param parcelamento
	 *            the parcelamento
	 * @return the dados gerais parcelas
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DadosGeraisParcelas montarDadosGeraisParcelas(Parcelamento parcelamento) throws GGASException;

	/**
	 * Autorizar parcelamento.
	 * 
	 * @param idParcelamento
	 *            the id parcelamento
	 * @param status
	 *            the status
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the parcelamento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Parcelamento autorizarParcelamento(Long idParcelamento, EntidadeConteudo status, DadosAuditoria dadosAuditoria)
					throws GGASException;

	/**
	 * Busca o parcelamento através do relacionamento de item de parcelamento com fatura geral. Para cada fatura parcelada haverá um item de
	 * parcelamento com uma fatura geral associada.
	 * 
	 * @param idFaturaGeral
	 *            - Chave primaria da fatura geral.
	 * @return the parcelamento
	 */
	public Parcelamento buscarParcelamentoPorFaturaGeral(Long idFaturaGeral);

	/**
	 * Consulta os parcelamentos que geraram notas de débito.
	 * 
	 * @param idParcelamento
	 *            - Id do parcelamento
	 * @return - Uma coleção de ParcelaVO que representa as parcelas geradas no parcelamento.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<ParcelaVO> consultarParcelamentosNotaDebito(Long idParcelamento) throws GGASException;

	/**
	 * Consulta os parcelamentos que geraram débitos a cobrar.
	 * 
	 * @param idParcelamento
	 *            the id parcelamento
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Collection<ParcelaVO> consultarParcelamentosCreditoARealizar(Long idParcelamento) throws GGASException;
}
