/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cobranca.parcelamento.impl;

import br.com.ggas.cobranca.parcelamento.DadosRelatorioConfissaoDividas;

class DadosRelatorioConfissaoDividasImpl implements DadosRelatorioConfissaoDividas {

	private static final long serialVersionUID = -4254273448752567549L;

	private String cnpjEmpresa;

	private String nomeCliente;

	private String getEndereco;

	private String cpfCnpjCliente;

	private String totalDebitos;

	private String totalDebitosExtenso;

	private String valorEntrada;

	private String valorEntradaExtenso;

	private String dataParcelamento;

	private String numeroParcelas;

	private String numeroParcelasExtenso;

	private String valorParcela;

	private String taxaMulta;

	private String taxaMultaExtenso;

	private String taxaJurosMora;

	private String taxaJurosMoraExtenso;

	private String imagem;

	private String enderecoEmpresa;

	private String mesPortugues;

	private String nomeEmpresa1;

	private String nomeEmpresa2;

	private String nomeEmpresa3;

	private String nomeEmpresa4;

	private String imovelDiaVencimento;

	private String descricaoLocalidade;

	private String colecaoAnoMesReferencia;

	private String colecaoAnoMesReferenciaSobra;

	/**
	 * @return the cnpjEmpresa
	 */
	@Override
	public String getCnpjEmpresa() {

		return cnpjEmpresa;
	}

	/**
	 * @param cnpjEmpresa
	 *            the cnpjEmpresa to set
	 */
	@Override
	public void setCnpjEmpresa(String cnpjEmpresa) {

		this.cnpjEmpresa = cnpjEmpresa;
	}

	/**
	 * @return the nomeCliente
	 */
	@Override
	public String getNomeCliente() {

		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 *            the nomeCliente to set
	 */
	@Override
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/**
	 * @return the getEndereco
	 */
	@Override
	public String getGetEndereco() {

		return getEndereco;
	}

	/**
	 * @param getEndereco
	 *            the getEndereco to set
	 */
	@Override
	public void setGetEndereco(String getEndereco) {

		this.getEndereco = getEndereco;
	}

	/**
	 * @return the cpfCnpjCliente
	 */
	@Override
	public String getCpfCnpjCliente() {

		return cpfCnpjCliente;
	}

	/**
	 * @param cpfCnpjCliente
	 *            the cpfCnpjCliente to set
	 */
	@Override
	public void setCpfCnpjCliente(String cpfCnpjCliente) {

		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	/**
	 * @return the totalDebitos
	 */
	@Override
	public String getTotalDebitos() {

		return totalDebitos;
	}

	/**
	 * @param totalDebitos
	 *            the totalDebitos to set
	 */
	@Override
	public void setTotalDebitos(String totalDebitos) {

		this.totalDebitos = totalDebitos;
	}

	/**
	 * @return the totalDebitosExtenso
	 */
	@Override
	public String getTotalDebitosExtenso() {

		return totalDebitosExtenso;
	}

	/**
	 * @param totalDebitosExtenso
	 *            the totalDebitosExtenso to set
	 */
	@Override
	public void setTotalDebitosExtenso(String totalDebitosExtenso) {

		this.totalDebitosExtenso = totalDebitosExtenso;
	}

	/**
	 * @return the valorEntrada
	 */
	@Override
	public String getValorEntrada() {

		return valorEntrada;
	}

	/**
	 * @param valorEntrada
	 *            the valorEntrada to set
	 */
	@Override
	public void setValorEntrada(String valorEntrada) {

		this.valorEntrada = valorEntrada;
	}

	/**
	 * @return the valorEntradaExtenso
	 */
	@Override
	public String getValorEntradaExtenso() {

		return valorEntradaExtenso;
	}

	/**
	 * @param valorEntradaExtenso
	 *            the valorEntradaExtenso to set
	 */
	@Override
	public void setValorEntradaExtenso(String valorEntradaExtenso) {

		this.valorEntradaExtenso = valorEntradaExtenso;
	}

	/**
	 * @return the dataParcelamento
	 */
	@Override
	public String getDataParcelamento() {

		return dataParcelamento;
	}

	/**
	 * @param dataParcelamento
	 *            the dataParcelamento to set
	 */
	@Override
	public void setDataParcelamento(String dataParcelamento) {

		this.dataParcelamento = dataParcelamento;
	}

	/**
	 * @return the numeroParcelas
	 */
	@Override
	public String getNumeroParcelas() {

		return numeroParcelas;
	}

	/**
	 * @param numeroParcelas
	 *            the numeroParcelas to set
	 */
	@Override
	public void setNumeroParcelas(String numeroParcelas) {

		this.numeroParcelas = numeroParcelas;
	}

	/**
	 * @return the numeroParcelasExtenso
	 */
	@Override
	public String getNumeroParcelasExtenso() {

		return numeroParcelasExtenso;
	}

	/**
	 * @param numeroParcelasExtenso
	 *            the numeroParcelasExtenso to set
	 */
	@Override
	public void setNumeroParcelasExtenso(String numeroParcelasExtenso) {

		this.numeroParcelasExtenso = numeroParcelasExtenso;
	}

	/**
	 * @return the valorParcela
	 */
	@Override
	public String getValorParcela() {

		return valorParcela;
	}

	/**
	 * @param valorParcela
	 *            the valorParcela to set
	 */
	@Override
	public void setValorParcela(String valorParcela) {

		this.valorParcela = valorParcela;
	}

	/**
	 * @return the taxaMulta
	 */
	@Override
	public String getTaxaMulta() {

		return taxaMulta;
	}

	/**
	 * @param taxaMulta
	 *            the taxaMulta to set
	 */
	@Override
	public void setTaxaMulta(String taxaMulta) {

		this.taxaMulta = taxaMulta;
	}

	/**
	 * @return the taxaMultaExtenso
	 */
	@Override
	public String getTaxaMultaExtenso() {

		return taxaMultaExtenso;
	}

	/**
	 * @param taxaMultaExtenso
	 *            the taxaMultaExtenso to set
	 */
	@Override
	public void setTaxaMultaExtenso(String taxaMultaExtenso) {

		this.taxaMultaExtenso = taxaMultaExtenso;
	}

	/**
	 * @return the taxaJurosMora
	 */
	@Override
	public String getTaxaJurosMora() {

		return taxaJurosMora;
	}

	/**
	 * @param taxaJurosMora
	 *            the taxaJurosMora to set
	 */
	@Override
	public void setTaxaJurosMora(String taxaJurosMora) {

		this.taxaJurosMora = taxaJurosMora;
	}

	/**
	 * @return the taxaJurosMoraExtenso
	 */
	@Override
	public String getTaxaJurosMoraExtenso() {

		return taxaJurosMoraExtenso;
	}

	/**
	 * @param taxaJurosMoraExtenso
	 *            the taxaJurosMoraExtenso to set
	 */
	@Override
	public void setTaxaJurosMoraExtenso(String taxaJurosMoraExtenso) {

		this.taxaJurosMoraExtenso = taxaJurosMoraExtenso;
	}

	/**
	 * @return the imagem
	 */
	@Override
	public String getImagem() {

		return imagem;
	}

	/**
	 * @param imagem
	 *            the imagem to set
	 */
	@Override
	public void setImagem(String imagem) {

		this.imagem = imagem;
	}

	/**
	 * @return the enderecoEmpresa
	 */
	@Override
	public String getEnderecoEmpresa() {

		return enderecoEmpresa;
	}

	/**
	 * @param enderecoEmpresa
	 *            the enderecoEmpresa to set
	 */
	@Override
	public void setEnderecoEmpresa(String enderecoEmpresa) {

		this.enderecoEmpresa = enderecoEmpresa;
	}

	/**
	 * @return the mesPortugues
	 */
	@Override
	public String getMesPortugues() {

		return mesPortugues;
	}

	/**
	 * @param mesPortugues
	 *            the mesPortugues to set
	 */
	@Override
	public void setMesPortugues(String mesPortugues) {

		this.mesPortugues = mesPortugues;
	}

	/**
	 * @return the nomeEmpresa1
	 */
	@Override
	public String getNomeEmpresa1() {

		return nomeEmpresa1;
	}

	/**
	 * @param nomeEmpresa1
	 *            the nomeEmpresa1 to set
	 */
	@Override
	public void setNomeEmpresa1(String nomeEmpresa1) {

		this.nomeEmpresa1 = nomeEmpresa1;
	}

	/**
	 * @return the nomeEmpresa2
	 */
	@Override
	public String getNomeEmpresa2() {

		return nomeEmpresa2;
	}

	/**
	 * @param nomeEmpresa2
	 *            the nomeEmpresa2 to set
	 */
	@Override
	public void setNomeEmpresa2(String nomeEmpresa2) {

		this.nomeEmpresa2 = nomeEmpresa2;
	}

	/**
	 * @return the nomeEmpresa3
	 */
	@Override
	public String getNomeEmpresa3() {

		return nomeEmpresa3;
	}

	/**
	 * @param nomeEmpresa3
	 *            the nomeEmpresa3 to set
	 */
	@Override
	public void setNomeEmpresa3(String nomeEmpresa3) {

		this.nomeEmpresa3 = nomeEmpresa3;
	}

	/**
	 * @return the nomeEmpresa4
	 */
	@Override
	public String getNomeEmpresa4() {

		return nomeEmpresa4;
	}

	/**
	 * @param nomeEmpresa4
	 *            the nomeEmpresa4 to set
	 */
	@Override
	public void setNomeEmpresa4(String nomeEmpresa4) {

		this.nomeEmpresa4 = nomeEmpresa4;
	}

	/**
	 * @return the imovelDiaVencimento
	 */
	@Override
	public String getImovelDiaVencimento() {

		return imovelDiaVencimento;
	}

	/**
	 * @param imovelDiaVencimento
	 *            the imovelDiaVencimento to set
	 */
	@Override
	public void setImovelDiaVencimento(String imovelDiaVencimento) {

		this.imovelDiaVencimento = imovelDiaVencimento;
	}

	/**
	 * @return the descricaoLocalidade
	 */
	@Override
	public String getDescricaoLocalidade() {

		return descricaoLocalidade;
	}

	/**
	 * @param descricaoLocalidade
	 *            the descricaoLocalidade to set
	 */
	@Override
	public void setDescricaoLocalidade(String descricaoLocalidade) {

		this.descricaoLocalidade = descricaoLocalidade;
	}

	/**
	 * @return the colecaoAnoMesReferencia
	 */
	@Override
	public String getColecaoAnoMesReferencia() {

		return colecaoAnoMesReferencia;
	}

	/**
	 * @param colecaoAnoMesReferencia
	 *            the colecaoAnoMesReferencia to
	 *            set
	 */
	@Override
	public void setColecaoAnoMesReferencia(String colecaoAnoMesReferencia) {

		this.colecaoAnoMesReferencia = colecaoAnoMesReferencia;
	}

	/**
	 * @return the colecaoAnoMesReferenciaSobra
	 */
	@Override
	public String getColecaoAnoMesReferenciaSobra() {

		return colecaoAnoMesReferenciaSobra;
	}

	/**
	 * @param colecaoAnoMesReferenciaSobra
	 *            the colecaoAnoMesReferenciaSobra
	 *            to set
	 */
	@Override
	public void setColecaoAnoMesReferenciaSobra(String colecaoAnoMesReferenciaSobra) {

		this.colecaoAnoMesReferenciaSobra = colecaoAnoMesReferenciaSobra;
	}
}
