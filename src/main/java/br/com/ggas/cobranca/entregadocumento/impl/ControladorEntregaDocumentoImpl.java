/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 09:52:09
 @author wcosta
 */

package br.com.ggas.cobranca.entregadocumento.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.jvnet.hk2.internal.DescriptorComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Classe responsável por implementar os métodos relacionados ao Controlador de EntregaDocumento
 *
 */
public class ControladorEntregaDocumentoImpl extends ControladorNegocioImpl implements ControladorEntregaDocumento {

	private static final String MOTIVO_NAO_ENTREGA = "motivoNaoEntrega";
	private static final String SITUACAO_ENTREGA = "situacaoEntrega";
	private static final String DATA_SITUACAO = "dataSituacao";
	private static final String CAMPO_TIPO_DOCUMENTO = "tipoDocumento";

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EntregaDocumento.BEAN_ID_ENTREGA_DOCUMENTO);

	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(EntregaDocumento.BEAN_ID_ENTREGA_DOCUMENTO);
	}
	
	public EntregaDocumento obterEntregaDocumento(final Long idEntregaDocumento, final String[] propriedadesLazy) throws GGASException {

		return (EntregaDocumento) obter(idEntregaDocumento, EntregaDocumentoImpl.class, propriedadesLazy);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento#listarEntregaDocumento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EntregaDocumento> listarEntregaDocumento(Map<String, Object> filtro) throws GGASException {

		Criteria criteria = getCriteria();

		criteria.createAlias(CAMPO_TIPO_DOCUMENTO, "tipoDocumento");
		criteria.createAlias("situacaoEntrega", "situacaoEntrega");
		criteria.createAlias("cliente", "cliente", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("imovel", "imovel", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("motivoNaoEntrega", "motivoNaoEntrega", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("documentoCobranca", "documentoCobranca", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("avisoCorte", "avisoCorte", CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias("avisoCorte.fatura", "fatura", CriteriaSpecification.LEFT_JOIN);

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			String dataSituacao = (String) filtro.get(DATA_SITUACAO);
			if(dataSituacao != null && !"".equalsIgnoreCase(dataSituacao)) {
				DateTime dataSituacaoConsulta = new DateTime(Util.converterCampoStringParaData("Data da situação", dataSituacao,
								Constantes.FORMATO_DATA_BR));
				criteria.add(Restrictions.eq(DATA_SITUACAO, dataSituacaoConsulta.toDate()));
			}

			String dataVencimento = (String) filtro.get("dataVencimento");
			if(dataVencimento != null && !"".equalsIgnoreCase(dataVencimento)) {
				DateTime dataVencimentoConsulta = new DateTime(Util.converterCampoStringParaData("Data do vencimento", dataVencimento,
								Constantes.FORMATO_DATA_BR));
				criteria.add(Restrictions.eq("documentoCobranca.dataVencimento", dataVencimentoConsulta.toDate()));
			}

			Long situacaoEntrega = (Long) filtro.get(SITUACAO_ENTREGA);
			if(situacaoEntrega != null && situacaoEntrega.intValue() > 0) {
				criteria.add(Restrictions.eq("situacaoEntrega.chavePrimaria", situacaoEntrega));
			}

			Long motivoNaoEntrega = (Long) filtro.get(MOTIVO_NAO_ENTREGA);
			if(motivoNaoEntrega != null && motivoNaoEntrega.intValue() > 0) {
				criteria.add(Restrictions.eq("motivoNaoEntrega.chavePrimaria", motivoNaoEntrega));
			}

			Long tipoDocumento = (Long) filtro.get(CAMPO_TIPO_DOCUMENTO);
			if(tipoDocumento != null && tipoDocumento.intValue() > 0) {
				criteria.add(Restrictions.eq("tipoDocumento.chavePrimaria", tipoDocumento));
			}

			Long idCliente = (Long) filtro.get("idCliente");
			if(idCliente != null && idCliente.intValue() > 0) {
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}
			
			Long idImovel = (Long) filtro.get("idImovel");
			if(idImovel != null && idImovel.intValue() > 0) {
				criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
			}

			Boolean habilitado = null;
			if(filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO) != null) {
				habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			}

			if(habilitado != null) {
				if(habilitado) {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
				} else if(!habilitado) {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.FALSE));
				}
			}

		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if(filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if(StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if(paginacaoPadrao) {
			criteria.addOrder(Order.asc("cliente.nome"));
			criteria.addOrder(Order.asc("tipoDocumento.descricao"));
			criteria.addOrder(Order.asc("documentoCobranca.dataVencimento"));
			criteria.addOrder(Order.asc("situacaoEntrega.descricao"));
			criteria.addOrder(Order.asc(DATA_SITUACAO));
		}

		return criteria.list();

	}
	
	/**
	 * Obtém uma entrega de documento por chave primaria.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @return entregaDocumento - {@link EntregaDocumento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public EntregaDocumento obterEntregaDocumento(Long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");
		hql.append(" inner join fetch entrega.cliente ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" left join fetch entrega.motivoNaoEntrega ");
		hql.append(" inner join fetch entrega.tipoDocumento ");
		hql.append(" where entrega.chavePrimaria = :chavePrimaria ");

		Query query = super.getSession().createQuery(hql.toString());

		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		EntregaDocumento entregaDocumento = (EntregaDocumento) query.uniqueResult();

		if (entregaDocumento == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, getClass().getName());
		}

		return entregaDocumento;
	}
	
	@Override
	public EntregaDocumento obterEntregaDocumentoProtocolo(Long chavePrimariaAvisoCorte) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");
		hql.append(" inner join fetch entrega.avisoCorte ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" inner join fetch entrega.tipoDocumento ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" where entrega.avisoCorte.chavePrimaria = :chavePrimariaAvisoCorte ");
		hql.append(" and entrega.tipoDocumento.descricao like 'PROTOCOLO' ");
		hql.append(" and (entrega.situacaoEntrega.descricao like 'Realizada' ");
		hql.append(" or entrega.situacaoEntrega.descricao like 'Retornado') ");

		Query query = super.getSession().createQuery(hql.toString());

		query.setLong("chavePrimariaAvisoCorte", chavePrimariaAvisoCorte);

		return (EntregaDocumento) query.uniqueResult();
	}

	@Override
	public EntregaDocumento obterEntregaDocumentoProtocoloRetorno(Long chavePrimariaAvisoCorte) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");
		hql.append(" inner join fetch entrega.avisoCorte ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" inner join fetch entrega.tipoDocumento ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" where entrega.avisoCorte.chavePrimaria = :chavePrimariaAvisoCorte ");
		hql.append(" and entrega.tipoDocumento.descricao like 'PROTOCOLO' ");
		hql.append(" and entrega.situacaoEntrega.descricao like 'Retornado'");

		Query query = super.getSession().createQuery(hql.toString());

		query.setLong("chavePrimariaAvisoCorte", chavePrimariaAvisoCorte);

		return (EntregaDocumento) query.uniqueResult();

	}
	
	@Override
	public EntregaDocumento obterEntregaDocumentoProtocoloNegativacaoRetorno(Long chavePrimariaFaturaNegativacao) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");
		hql.append(" inner join fetch entrega.faturaClienteNegativacao faturaClienteNegativacao ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" inner join fetch entrega.tipoDocumento ");
		hql.append(" inner join fetch entrega.situacaoEntrega ");
		hql.append(" where entrega.faturaClienteNegativacao.chavePrimaria = :chavePrimariaFaturaNegativacao ");
		hql.append(" and entrega.tipoDocumento.descricao like 'PROTOCOLO' ");
		hql.append(" and entrega.situacaoEntrega.descricao like 'Retornado'");

		Query query = super.getSession().createQuery(hql.toString());

		query.setLong("chavePrimariaFaturaNegativacao", chavePrimariaFaturaNegativacao);

		return (EntregaDocumento) query.uniqueResult();

	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento#listarProtocolos(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EntregaDocumento> listarProtocolos(Map<String, Object> filtro) throws GGASException {
		
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacaoRecebimentoPago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		EntidadeConteudo situacaoPagamentoQuitado = controladorEntidadeConteudo.obter(Long.valueOf(situacaoRecebimentoPago));
		
		StringBuilder hql = new StringBuilder();
		
		Boolean indicadorRetornado = null;
		Long idGrupoFaturamento = null;
		Date dataEmissaoInicial = null;
		Date dataEmissaoFinal = null;
		Date dataVencimentoInicial = null;
		Date dataVencimentoFinal = null;
		String tipoProtocolo = null;
		String descricaPontoConsumo = null;
		Long chaveServicoTipo = null;
		Long idLoteComunicacao = null;
		
		if(!filtro.isEmpty()) {
			indicadorRetornado = (Boolean) filtro.get("indicadorRetornado");
			idGrupoFaturamento =  (Long) filtro.get("idGrupoFaturamento");
			dataEmissaoInicial = (Date) filtro.get("dataInicioEmissao");
			dataEmissaoFinal = (Date) filtro.get("dataFimEmissao");
			dataVencimentoInicial = (Date) filtro.get("dataInicioVencimento");
			dataVencimentoFinal = (Date) filtro.get("dataFimVencimento");
			tipoProtocolo = (String) filtro.get("tipoProtocolo");
			descricaPontoConsumo = (String) filtro.get("descricaoPontoConsumo");
			chaveServicoTipo = (Long) filtro.get("chaveServicoTipo");
			idLoteComunicacao = (Long) filtro.get("idLoteComunicacao");
			
		}
		

		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");

		hql.append(" inner join fetch entrega.situacaoEntrega situacaoEntrega ");
		hql.append(" inner join fetch entrega.tipoDocumento documentoTipo ");
		
		if (tipoProtocolo != null && !"".equals(tipoProtocolo)) {
			if ("Aviso de Corte".equals(tipoProtocolo)) {
				hql.append(" left join fetch entrega.avisoCorte avisoCorte ");
				hql.append(" left join fetch avisoCorte.fatura fatura");
			} else if ("Negativação".equals(tipoProtocolo)) {
				hql.append(" left join fetch entrega.faturaClienteNegativacao faturaClienteNegativacao ");
				hql.append(" left join fetch faturaClienteNegativacao.fatura fatura");
			} else {
				hql.append(" left join fetch entrega.itemLoteComunicacao itemLoteComunicacao ");
			}
		}
		if ("Comunicação".equals(tipoProtocolo)) {
			hql.append("left join fetch itemLoteComunicacao.pontoConsumo pontoConsumo ");
			hql.append("left join fetch pontoConsumo.rota rota ");
			hql.append("left join fetch itemLoteComunicacao.loteComunicacao loteComunicacao ");
		}else {
			hql.append(" left join fetch fatura.pontoConsumo pontoConsumo ");
			hql.append(" left join fetch fatura.rota rota ");
		}
		hql.append(" WHERE ");
		
		
		if ("Comunicação".equals(tipoProtocolo)) {
			hql.append(" entrega.tipoDocumento.descricao like 'COMUNICAÇÃO' ");
		} else {
			hql.append(" entrega.tipoDocumento.descricao like 'PROTOCOLO' ");
			hql.append(" and fatura.situacaoPagamento.chavePrimaria <> :cdSituacaoPagamento");
		}
		
		if (indicadorRetornado != null) {
			hql.append(" and entrega.indicadorRetornado = :indicadorRetornado");
		}
		
		if (dataEmissaoInicial != null && dataEmissaoFinal != null) {
			hql.append(" and entrega.dataEmissao BETWEEN :dataEmissaoInicial and :dataEmissaoFinal ");
		}
		
		if (dataVencimentoInicial != null && dataVencimentoFinal != null) {
			hql.append(" and fatura.dataVencimento BETWEEN :dataVencimentoInicial and :dataVencimentoFinal ");
		}
		
		if (idGrupoFaturamento != null) {
			hql.append(" and rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
		}
		
		if (tipoProtocolo != null  && !"".equals(tipoProtocolo)) {
			if("Aviso de Corte".equals(tipoProtocolo)) {
				hql.append(" and entrega.avisoCorte is not null ");
			} else if ("Negativação".equals(tipoProtocolo)) {
				hql.append(" and entrega.faturaClienteNegativacao is not null ");
			} else {
				hql.append(" and entrega.itemLoteComunicacao is not null ");
			}
		}
		
		if (descricaPontoConsumo != null && !descricaPontoConsumo.isEmpty()) {
			hql.append(" and upper(pontoConsumo.descricao) like :descricaoPontoConsumo " );
		}
		
		if (tipoProtocolo != null  && !"".equals(tipoProtocolo)) {
			if("Comunicação".equals(tipoProtocolo)) {
				hql.append(" and loteComunicacao.chavePrimaria = :idLoteComunicacao ");
				if(chaveServicoTipo != null) {
					hql.append(" and loteComunicacao.servicoTipo.chavePrimaria = :chaveServicoTipo " );
				}
			}
		}
		
		if ("Comunicação".equals(tipoProtocolo)) {
			hql.append(" order by  itemLoteComunicacao.chavePrimaria asc ");
		}
		
		Query query = super.getSession().createQuery(hql.toString());
		
		if (!"Comunicação".equals(tipoProtocolo)) {
			query.setLong("cdSituacaoPagamento", situacaoPagamentoQuitado.getChavePrimaria());
		}
		
		if (indicadorRetornado != null) {
			query.setBoolean("indicadorRetornado", indicadorRetornado);
		}
		
		if (dataEmissaoInicial != null && dataEmissaoFinal != null) {
			query.setDate("dataEmissaoInicial", dataEmissaoInicial);
			query.setDate("dataEmissaoFinal", dataEmissaoFinal);
		}
		
		if (dataVencimentoInicial != null && dataVencimentoInicial != null) {
			query.setDate("dataVencimentoInicial", dataVencimentoInicial);
			query.setDate("dataVencimentoFinal", dataVencimentoFinal);
		}
		
		if (idGrupoFaturamento != null) {
			query.setLong("idGrupoFaturamento", idGrupoFaturamento);
		}
		
		if (descricaPontoConsumo != null && !descricaPontoConsumo.isEmpty()) {
			query.setString("descricaoPontoConsumo",
					Util.removerAcentuacao(Util.formatarTextoConsulta(descricaPontoConsumo).toUpperCase()));
		}
		
		if (tipoProtocolo != null && !"".equals(tipoProtocolo)) {
			if ("Comunicação".equals(tipoProtocolo)) {
				query.setLong("idLoteComunicacao", idLoteComunicacao);
				if (chaveServicoTipo != null) {
					query.setLong("chaveServicoTipo", chaveServicoTipo);
				}
			}
		}

		return query.list();
	}
	
	@Override
	public Boolean popularDataMensagemRetornoProtocolo (Long[] idsProtocolos, Collection<EntregaDocumento> protocolos, String dataRetorno, String descricaoObservacaoRetorno, Funcionario funcionario, Boolean isMesmaMensagem) throws GGASException{
		List<Long> chavesProtocolos = Arrays.asList(idsProtocolos);

		for (EntregaDocumento protocoloAux : protocolos) {
			
			if(protocoloAux.getDescricaoObservacaoRetorno() == null) {
				protocoloAux.setDescricaoObservacaoRetorno("");
			} else {
				protocoloAux.setDescricaoObservacaoRetorno(protocoloAux.getDescricaoObservacaoRetorno() + "\n");
			}

			if (chavesProtocolos.contains(protocoloAux.getChavePrimaria())) {
				
				if(protocoloAux.isIndicadorRetornado()) {
					continue;
				}
				
				if (descricaoObservacaoRetorno != null && !descricaoObservacaoRetorno.isEmpty()) {
					if(isMesmaMensagem) {
						protocoloAux.setDescricaoObservacaoRetorno(descricaoObservacaoRetorno);
					} else {
						protocoloAux.setDescricaoObservacaoRetorno(protocoloAux.getDescricaoObservacaoRetorno() + descricaoObservacaoRetorno);
					}
					
				}
				if (dataRetorno != null && !"".equals(dataRetorno)) {
					Date dataGeracao = DataUtil.gerarDataHmsZerados(protocoloAux.getDataEmissao());
					Date dataRetornoConvertida = Util.converterCampoStringParaData("Data Retorno", dataRetorno,
							Constantes.FORMATO_DATA_BR);

					Boolean isDataRetornoDepoisDataAtual = DataUtil.depoisHoje(dataRetornoConvertida);
					Boolean isDataRetornoAntesDataEmissao = DataUtil.menorQue(dataRetornoConvertida, dataGeracao);
					
					if(isDataRetornoAntesDataEmissao && protocoloAux.getItemLoteComunicacao() != null) {
						return Boolean.FALSE;
					}
					
					if (!isDataRetornoDepoisDataAtual && !isDataRetornoAntesDataEmissao
							&& !protocoloAux.isIndicadorRetornado()) {
						protocoloAux.setIndicadorRetornado(true);
						protocoloAux.setDataSituacao(dataRetornoConvertida);
						protocoloAux.setSituacaoEntrega(
								controladorEntidadeConteudo.listarEntidadeConteudoPorEntidadeClasseEDescricao(
										"Situação da Entrega Documentos", "Retornado"));
					}
				}
				
				protocoloAux.setFuncionario(funcionario);
				this.atualizar(protocoloAux);
			}
		}
		
		return Boolean.TRUE;
		
	}
	
	@Override
	public Collection<EntregaDocumento> consultarProtocolosPorListaChaves(Long[] chaves) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");
		hql.append(" WHERE ");
		
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "entrega.chavePrimaria", "ED_CONS", Arrays.asList(chaves));
				
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		
		return query.list();
	}
	
	@Override
	public EntregaDocumento consultarProtocoloPorAutorizacaoServicoProtocolo(Long chaveAutorizacaoServico) {
		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(this.getClasseEntidade().getSimpleName());
		hql.append(" as entrega ");
		hql.append(" inner join fetch entrega.itemLoteComunicacao");
		hql.append(" as itemLote ");

		hql.append(" WHERE ");
		hql.append(" itemLote.servicoAutorizacaoProtocolo.chavePrimaria = :chaveAutorizacaoServico ");
		hql.append(" AND entrega.indicadorRetornado = false ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);
		
		query.setParameter("chaveAutorizacaoServico", chaveAutorizacaoServico);
		
		return (EntregaDocumento) query.uniqueResult();
		
	}
}

