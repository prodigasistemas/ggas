/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 09:27:35
 @author wcosta
 */

package br.com.ggas.cobranca.entregadocumento;

import java.util.Date;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Documento não Entregue
 *
 */
public interface DocumentoNaoEntregue extends EntidadeNegocio {

	String BEAN_ID_DOCUMENTO_NAO_ENTREGUE = "documentoNaoEntregue";

	/**
	 * @return Date - Retorna a data da tentativa.
	 */
	public Date getDataTentativa();

	/**
	 * @param dataTentativa - Set Data de tentativa.
	 */
	public void setDataTentativa(Date dataTentativa);

	/**
	 * @return Integer - Retorna um valor da quantidade de tentativa.
	 */
	public Integer getQuantidadeTentativa();

	/**
	 * @param quantidadeTentativa - Set valor da quantidade de tentativa.
	 */
	public void setQuantidadeTentativa(Integer quantidadeTentativa);

	/**
	 * @return String - Retorna descrição.
	 */
	public String getDescricao();

	/**
	 * @param descricao - Set descrição.
	 */
	public void setDescricao(String descricao);

	/**
	 * @return EntidadeConteudo - Retorna uma entidade conteudo com informações sobre motivo.
	 */
	public EntidadeConteudo getMotivo();

	/**
	 * @param motivo - Set objeto motivo.
	 */
	public void setMotivo(EntidadeConteudo motivo);

	/**
	 * @return TipoDocumento - Retorno objeto tipo documento.
	 */
	public TipoDocumento getTipoDocumento();

	/**
	 * @param tipoDocumento - Set objeto Tipo documento.
	 */
	public void setTipoDocumento(TipoDocumento tipoDocumento);

	/**
	 * @return Cliente - Retorna objeto cliente.
	 */
	public Cliente getCliente();

	/**
	 * @param cliente - Set objeto Cliente.
	 */
	public void setCliente(Cliente cliente);

	/**
	 * @return DocumentoCobranca - Retorna objeto Documento Cobrança.
	 */
	public DocumentoCobranca getDocumentoCobranca();

	/**
	 * @param documentoCobranca - Set objeto Documento cobrança.
	 */
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca);

}
