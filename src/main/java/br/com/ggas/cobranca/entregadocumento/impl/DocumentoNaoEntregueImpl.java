/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 09:29:39
 @author wcosta
 */

package br.com.ggas.cobranca.entregadocumento.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.entregadocumento.DocumentoNaoEntregue;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelos atributos e métodos relacionados aos Documentos Não Entreges
 *
 */
public class DocumentoNaoEntregueImpl extends EntidadeNegocioImpl implements DocumentoNaoEntregue {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7078791632890431755L;

	private Date dataTentativa;

	private Integer quantidadeTentativa;

	private String descricao;

	private EntidadeConteudo motivo;

	private TipoDocumento tipoDocumento;

	private Cliente cliente;

	private DocumentoCobranca documentoCobranca;

	@Override
	public Date getDataTentativa() {
		Date data = null;
		if(this.dataTentativa != null) {
			data = (Date) dataTentativa.clone();
		}
		return data;
	}

	@Override
	public void setDataTentativa(Date dataTentativa) {
		if (dataTentativa != null) {
			this.dataTentativa = (Date) dataTentativa.clone();
		} else {
			this.dataTentativa = null;
		}
	}

	@Override
	public Integer getQuantidadeTentativa() {

		return quantidadeTentativa;
	}

	@Override
	public void setQuantidadeTentativa(Integer quantidadeTentativa) {

		this.quantidadeTentativa = quantidadeTentativa;
	}

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	@Override
	public EntidadeConteudo getMotivo() {

		return motivo;
	}

	@Override
	public void setMotivo(EntidadeConteudo motivo) {

		this.motivo = motivo;
	}

	@Override
	public TipoDocumento getTipoDocumento() {

		return tipoDocumento;
	}

	@Override
	public void setTipoDocumento(TipoDocumento tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public Cliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public DocumentoCobranca getDocumentoCobranca() {

		return documentoCobranca;
	}

	@Override
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca) {

		this.documentoCobranca = documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
