/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 09:50:34
 @author wcosta
 */

package br.com.ggas.cobranca.entregadocumento;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao controlador de entrega de documento. 
 *
 */
public interface ControladorEntregaDocumento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ENTRAGA_DOCUMENTO = "controladorEntregaDocumento";

	/**
	 * Listar entrega documento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<EntregaDocumento> listarEntregaDocumento(Map<String, Object> filtro) throws GGASException;

	/**
	 * Obter EntregaDocumento
	 * 
	 * @param idEntregaDocumento - {@link Long}
	 * @param propriedadesLazy - {@link String}
	 * @return entrega documento
	 * @throws GGASException the GGAS Exception
	 */
	EntregaDocumento obterEntregaDocumento(Long idEntregaDocumento, String[] propriedadesLazy) throws GGASException;

	/**
	 * Obtém uma entrega de documento por chave primaria.
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @return entregaDocumento - {@link EntregaDocumento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	EntregaDocumento obterEntregaDocumento(Long chavePrimaria) throws NegocioException;

	
	/**
	 * Obtém uma entrega de documento do tipo protocolo.
	 * 
	 * @param chavePrimariaAvisoCorte - {@link Long}
	 * @return entregaDocumento - {@link EntregaDocumento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	EntregaDocumento obterEntregaDocumentoProtocolo(Long chavePrimariaAvisoCorte) throws NegocioException;

	/**
	 * Obter entrega documento protocolo para retorno
	 * @param chavePrimariaAvisoCorte - {@link Long}
	 * @return EntregaDocumento - {@link EntregaDocumento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	EntregaDocumento obterEntregaDocumentoProtocoloRetorno(Long chavePrimariaAvisoCorte) throws NegocioException;

	/**
	 * Lista os documentos do tipo PROTOCOLO
	 * 
	 * @param filtro - Filtro para retorno dos protocolos
	 * @return coleção dos protocolos
	 * @throws GGASException - {@link GGASException}
	 */
	Collection<EntregaDocumento> listarProtocolos(Map<String, Object> filtro) throws GGASException;

	Boolean popularDataMensagemRetornoProtocolo(Long[] idsProtocolos, Collection<EntregaDocumento> protocolos, String dataRetorno, String descricaoObservacaoRetorno, Funcionario funcionario, Boolean isMesmaMensagem)
			throws GGASException;

	EntregaDocumento obterEntregaDocumentoProtocoloNegativacaoRetorno(Long chavePrimariaFaturaNegativacao)
			throws NegocioException;

	Collection<EntregaDocumento> consultarProtocolosPorListaChaves(Long[] chaves);

	EntregaDocumento consultarProtocoloPorAutorizacaoServicoProtocolo(Long chavePontoConsumo);

}
