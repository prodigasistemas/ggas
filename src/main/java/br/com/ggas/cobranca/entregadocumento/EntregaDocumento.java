/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 09:27:35
 @author wcosta
 */

package br.com.ggas.cobranca.entregadocumento;

import java.util.Date;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a Entrega de documento.
 *
 */
public interface EntregaDocumento extends EntidadeNegocio {

	String BEAN_ID_ENTREGA_DOCUMENTO = "entregaDocumento";

	/**
	 * @return Date - Situação da data.
	 */
	public Date getDataSituacao();

	/**
	 * @param dataSituacao - Set Situação da data.
	 */
	public void setDataSituacao(Date dataSituacao);

	/**
	 * @return EntidadeConteudo - Situação Entrega.
	 */
	public EntidadeConteudo getSituacaoEntrega();

	/**
	 * @param situacaoEntrega - Set Situação Entrega
	 */
	public void setSituacaoEntrega(EntidadeConteudo situacaoEntrega);

	/**
	 * @return EntidadeConteudo - Motivo não entrega.
	 */
	public EntidadeConteudo getMotivoNaoEntrega();

	/**
	 * @param motivoNaoEntrega - Set Motivo Não Entrega
	 */
	public void setMotivoNaoEntrega(EntidadeConteudo motivoNaoEntrega);

	/**
	 * @return TipoDocumento - Tipo Documento
	 */
	public TipoDocumento getTipoDocumento();

	/**
	 * @param tipoDocumento - Set Tipo Documento
	 */
	public void setTipoDocumento(TipoDocumento tipoDocumento);

	/**
	 * @return Cliente - Cliente
	 */
	public Cliente getCliente();

	/**
	 * @param cliente - Set Cliente
	 */
	public void setCliente(Cliente cliente);

	/**
	 * @return DocumentoCobranca - Documento cobrança
	 */
	public DocumentoCobranca getDocumentoCobranca();

	/**
	 * @param documentoCobranca - Set Documento Cobrança
	 */
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca);

	/**
	 * @return AvisoCorte - Aviso Corte.
	 */
	public AvisoCorte getAvisoCorte();

	/**
	 * @param avisoCorte - Set Aviso Corte.
	 */
	public void setAvisoCorte(AvisoCorte avisoCorte);

	/**
	 * 
	 * @return Imovel - Imovel
	 */
	Imovel getImovel();

	/**
	 * 
	 * @param imovel - Set Imovel
	 */
	void setImovel(Imovel imovel);
	
	/**
	 * @return Date - Data de emissao.
	 */
	public Date getDataEmissao();

	/**
	 * @param dataSituacao - Set Situação da data.
	 */
	public void setDataEmissao(Date dataEmissao);
	
	/**
	 * 
	 * @return Boolean - indicador retornado
	 */
	boolean isIndicadorRetornado();

	/**
	 * 
	 * @param imovel - Set Imovel
	 */
	void setIndicadorRetornado(boolean indicadorRetornado);

	FaturaClienteNegativacao getFaturaClienteNegativacao();

	void setFaturaClienteNegativacao(FaturaClienteNegativacao faturaClienteNegativacao);

	void setNumeroTitulo(String numeroTitulo);

	String getNumeroTitulo();

	void setFatura(Fatura fatura);

	Fatura getFatura();
	
	/**
	 * @return itemLoteComunicacao - return the ItemLoteComunicacao
	 */
	public ItemLoteComunicacao getItemLoteComunicacao();

	/**
	 * @param itemLoteComunicacao - Set the itemLoteComunicacao
	 */
	public void setItemLoteComunicacao(ItemLoteComunicacao itemLoteComunicacao);

	String getDescricaoObservacaoRetorno();

	void setDescricaoObservacaoRetorno(String descricaoObservacaoRetorno);
	
	Funcionario getFuncionario();
	
	void setFuncionario(Funcionario funcionario);

}
