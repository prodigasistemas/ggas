/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/12/2013 09:29:39
 @author wcosta
 */

package br.com.ggas.cobranca.entregadocumento.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.cobranca.negativacao.FaturaClienteNegativacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos atributos e métodos relacionados a Entrega do Documento. 
 *
 */
public class EntregaDocumentoImpl extends EntidadeNegocioImpl implements EntregaDocumento {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;
	/**
	 * 
	 */
	private static final long serialVersionUID = -7078791632890431755L;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataSituacao;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataEmissao;

	private EntidadeConteudo situacaoEntrega;

	private EntidadeConteudo motivoNaoEntrega;

	private TipoDocumento tipoDocumento;

	private Cliente cliente;

	private DocumentoCobranca documentoCobranca;

	private AvisoCorte avisoCorte;
	
	private Imovel imovel;
	
	private Boolean indicadorRetornado;
	
	private FaturaClienteNegativacao faturaClienteNegativacao;
	
	private Fatura fatura;
	
	private String numeroTitulo;

	private ItemLoteComunicacao itemLoteComunicacao;
	
	private String descricaoObservacaoRetorno;
	
	private Funcionario funcionario;
	
	@Override
	public Date getDataSituacao() {
		Date data = null;
		if (this.dataSituacao != null) {
			data = (Date) dataSituacao.clone();
		}
		return data;
	}

	@Override
	public void setDataSituacao(Date dataSituacao) {

		this.dataSituacao = dataSituacao;

	}

	@Override
	public EntidadeConteudo getSituacaoEntrega() {

		return situacaoEntrega;
	}

	@Override
	public void setSituacaoEntrega(EntidadeConteudo situacaoEntrega) {

		this.situacaoEntrega = situacaoEntrega;
	}

	@Override
	public EntidadeConteudo getMotivoNaoEntrega() {

		return motivoNaoEntrega;
	}

	@Override
	public void setMotivoNaoEntrega(EntidadeConteudo motivoNaoEntrega) {

		this.motivoNaoEntrega = motivoNaoEntrega;
	}

	@Override
	public TipoDocumento getTipoDocumento() {

		return tipoDocumento;
	}

	@Override
	public void setTipoDocumento(TipoDocumento tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public Cliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public DocumentoCobranca getDocumentoCobranca() {

		return documentoCobranca;
	}

	@Override
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca) {

		this.documentoCobranca = documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(cliente == null && imovel == null) {
			stringBuilder.append("Cliente/Imovel");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoDocumento == null) {
			stringBuilder.append("Tipo de Documento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(situacaoEntrega == null) {
			stringBuilder.append("Situação da Entrega");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataSituacao == null) {
			stringBuilder.append("Data da Situação");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;

	}

	@Override
	public AvisoCorte getAvisoCorte() {

		return avisoCorte;
	}

	@Override
	public void setAvisoCorte(AvisoCorte avisoCorte) {

		this.avisoCorte = avisoCorte;
	}

	@Override
	public Imovel getImovel() {
		return imovel;
	}

	@Override
	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	@Override
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	@Override
	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
		
	}

	@Override
	public boolean isIndicadorRetornado() {
		return indicadorRetornado;
	}

	@Override
	public void setIndicadorRetornado(boolean indicadorRetornado) {
		this.indicadorRetornado = indicadorRetornado;
		
	}

	@Override
	public FaturaClienteNegativacao getFaturaClienteNegativacao() {
		return faturaClienteNegativacao;
	}

	@Override
	public void setFaturaClienteNegativacao(FaturaClienteNegativacao faturaClienteNegativacao) {
		this.faturaClienteNegativacao = faturaClienteNegativacao;
		
	}

	@Override
	public Fatura getFatura() {
		return fatura;
	}

	@Override
	public void setFatura(Fatura fatura) {
		this.fatura = fatura;
	}

	@Override
	public String getNumeroTitulo() {
		return numeroTitulo;
	}

	@Override
	public void setNumeroTitulo(String numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}

	@Override
	public ItemLoteComunicacao getItemLoteComunicacao() {
		return this.itemLoteComunicacao;
	}

	@Override
	public void setItemLoteComunicacao(ItemLoteComunicacao itemLoteComunicacao) {
		this.itemLoteComunicacao = itemLoteComunicacao;
	}

	@Override
	public String getDescricaoObservacaoRetorno() {
		return descricaoObservacaoRetorno;
	}

	@Override
	public void setDescricaoObservacaoRetorno(String descricaoObservacaoRetorno) {
		this.descricaoObservacaoRetorno = descricaoObservacaoRetorno;
	}

	@Override
	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
		
	}

}
