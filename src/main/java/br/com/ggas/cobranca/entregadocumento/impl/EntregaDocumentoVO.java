package br.com.ggas.cobranca.entregadocumento.impl;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;

public class EntregaDocumentoVO {

	private String dataInicioEmissao;
	
	private String dataFimEmissao;
	
	private String dataRetorno;
	
	private String dataInicioVencimento;
	
	private String dataFimVencimento;
	
	private Boolean indicadorRetornado;
	
	private Long idGrupoFaturamento;
	
	private String tipoProtocolo;
	
	private ServicoTipo servicoTipo;
	
	private String descricaoPontoConsumo;
	
	private String descricaoObservacaoRetorno;
	
	private Boolean lotesCartasCanceladas;
	

	public Boolean getIndicadorRetornado() {
		return indicadorRetornado;
	}

	public void setIndicadorRetornado(Boolean indicadorRetornado) {
		this.indicadorRetornado = indicadorRetornado;
	}

	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}

	public String getDataInicioVencimento() {
		return dataInicioVencimento;
	}

	public void setDataInicioVencimento(String dataInicioVencimento) {
		this.dataInicioVencimento = dataInicioVencimento;
	}

	public String getDataFimVencimento() {
		return dataFimVencimento;
	}

	public void setDataFimVencimento(String dataFimVencimento) {
		this.dataFimVencimento = dataFimVencimento;
	}

	public String getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(String dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public String getDataInicioEmissao() {
		return dataInicioEmissao;
	}

	public void setDataInicioEmissao(String dataInicioEmissao) {
		this.dataInicioEmissao = dataInicioEmissao;
	}

	public String getDataFimEmissao() {
		return dataFimEmissao;
	}

	public void setDataFimEmissao(String dataFimEmissao) {
		this.dataFimEmissao = dataFimEmissao;
	}

	public String getTipoProtocolo() {
		return tipoProtocolo;
	}

	public void setTipoProtocolo(String tipoProtocolo) {
		this.tipoProtocolo = tipoProtocolo;
	}

	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}

	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getDescricaoObservacaoRetorno() {
		return descricaoObservacaoRetorno;
	}

	public void setDescricaoObservacaoRetorno(String descricaoObservacaoRetorno) {
		this.descricaoObservacaoRetorno = descricaoObservacaoRetorno;
	}

	public Boolean getLotesCartasCanceladas() {
		return lotesCartasCanceladas;
	}

	public void setLotesCartasCanceladas(Boolean lotesCartasCanceladas) {
		this.lotesCartasCanceladas = lotesCartasCanceladas;
	}
	
}
