/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente.impl;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.cliente.IntegracaoClienteEndereco;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;

/**
 * Classe de implementação Integracao Cliente Endereco.
 *
 */
public class IntegracaoClienteEnderecoImpl extends IntegracaoImpl implements IntegracaoClienteEndereco {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7015283399534821711L;

	private IntegracaoCliente cliente;

	private Long cep;

	private Long tipoEndereco;

	private String logradouro;

	private String numero;

	private String complemento;

	private String bairro;

	private Long caixaPostal;

	private Long correspondencia;

	private String enderecoReferencia;

	private Long municipio;

	private Long clienteEndereco;

	private Integer indicadorPrincipal;

	/**
	 * @return the clienteEndereco
	 */
	@Override
	public Long getClienteEndereco() {

		return clienteEndereco;
	}

	/**
	 * @param clienteEndereco
	 *            the clienteEndereco to set
	 */
	@Override
	public void setClienteEndereco(Long clienteEndereco) {

		this.clienteEndereco = clienteEndereco;
	}

	/**
	 * @return the cliente
	 */
	@Override
	public IntegracaoCliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	@Override
	public void setCliente(IntegracaoCliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return the cep
	 */
	@Override
	public Long getCep() {

		return cep;
	}

	/**
	 * @param cep
	 *            the cep to set
	 */
	@Override
	public void setCep(Long cep) {

		this.cep = cep;
	}

	/**
	 * @return the tipoEndereco
	 */
	@Override
	public Long getTipoEndereco() {

		return tipoEndereco;
	}

	/**
	 * @param tipoEndereco
	 *            the tipoEndereco to set
	 */
	@Override
	public void setTipoEndereco(Long tipoEndereco) {

		this.tipoEndereco = tipoEndereco;
	}

	/**
	 * @return the logradouro
	 */
	@Override
	public String getLogradouro() {

		return logradouro;
	}

	/**
	 * @param logradouro
	 *            the logradouro to set
	 */
	@Override
	public void setLogradouro(String logradouro) {

		this.logradouro = logradouro;
	}

	/**
	 * @return the numero
	 */
	@Override
	public String getNumero() {

		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	@Override
	public void setNumero(String numero) {

		this.numero = numero;
	}

	/**
	 * @return the complemento
	 */
	@Override
	public String getComplemento() {

		return complemento;
	}

	/**
	 * @param complemento
	 *            the complemento to set
	 */
	@Override
	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	/**
	 * @return the bairro
	 */
	@Override
	public String getBairro() {

		return bairro;
	}

	/**
	 * @param bairro
	 *            the bairro to set
	 */
	@Override
	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	/**
	 * @return the caixaPostal
	 */
	@Override
	public Long getCaixaPostal() {

		return caixaPostal;
	}

	/**
	 * @param caixaPostal
	 *            the caixaPostal to set
	 */
	@Override
	public void setCaixaPostal(Long caixaPostal) {

		this.caixaPostal = caixaPostal;
	}

	/**
	 * @return the correspondencia
	 */
	@Override
	public Long getCorrespondencia() {

		return correspondencia;
	}

	/**
	 * @param correspondencia
	 *            the correspondencia to set
	 */
	@Override
	public void setCorrespondencia(Long correspondencia) {

		this.correspondencia = correspondencia;
	}

	/**
	 * @return the enderecoReferencia
	 */
	@Override
	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	/**
	 * @param enderecoReferencia
	 *            the enderecoReferencia to set
	 */
	@Override
	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	/**
	 * @return the municipio
	 */
	@Override
	public Long getMunicipio() {

		return municipio;
	}

	/**
	 * @param municipio
	 *            the municipio to set
	 */
	@Override
	public void setMunicipio(Long municipio) {

		this.municipio = municipio;
	}

	@Override
	public Integer getIndicadorPrincipal() {

		return indicadorPrincipal;
	}

	@Override
	public void setIndicadorPrincipal(Integer indicadorPrincipal) {

		this.indicadorPrincipal = indicadorPrincipal;
	}
}
