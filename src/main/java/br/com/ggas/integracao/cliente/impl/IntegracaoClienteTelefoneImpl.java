/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente.impl;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.cliente.IntegracaoClienteTelefone;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;


/**
 * Classe Integracao Cliente Telefone.
 * 
 *
 */
public class IntegracaoClienteTelefoneImpl extends IntegracaoImpl implements IntegracaoClienteTelefone {

	/**
	 * 
	 */
	private static final long serialVersionUID = 550687321959756477L;

	private IntegracaoCliente cliente;

	private Long tipoFone;

	private Long codigoDDD;

	private String numero;

	private String ramal;

	private Long indicadorPrincipal;

	private Long clienteTelefone;

	/**
	 * @return the clienteTelefone
	 */
	@Override
	public Long getClienteTelefone() {

		return clienteTelefone;
	}

	/**
	 * @param clienteTelefone
	 *            the clienteTelefone to set
	 */
	@Override
	public void setClienteTelefone(Long clienteTelefone) {

		this.clienteTelefone = clienteTelefone;
	}

	/**
	 * @return the cliente
	 */
	@Override
	public IntegracaoCliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	@Override
	public void setCliente(IntegracaoCliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return the tipoFone
	 */
	@Override
	public Long getTipoFone() {

		return tipoFone;
	}

	/**
	 * @param tipoFone
	 *            the tipoFone to set
	 */
	@Override
	public void setTipoFone(Long tipoFone) {

		this.tipoFone = tipoFone;
	}

	/**
	 * @return the codigoDDD
	 */
	@Override
	public Long getCodigoDDD() {

		return codigoDDD;
	}

	/**
	 * @param codigoDDD
	 *            the codigoDDD to set
	 */
	@Override
	public void setCodigoDDD(Long codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	/**
	 * @return the numero
	 */
	@Override
	public String getNumero() {

		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	@Override
	public void setNumero(String numero) {

		this.numero = numero;
	}

	/**
	 * @return the ramal
	 */
	@Override
	public String getRamal() {

		return ramal;
	}

	/**
	 * @param ramal
	 *            the ramal to set
	 */
	@Override
	public void setRamal(String ramal) {

		this.ramal = ramal;
	}

	/**
	 * @return the indicadorPrincipal
	 */
	@Override
	public Long getIndicadorPrincipal() {

		return indicadorPrincipal;
	}

	/**
	 * @param indicadorPrincipal
	 *            the indicadorPrincipal to set
	 */
	@Override
	public void setIndicadorPrincipal(Long indicadorPrincipal) {

		this.indicadorPrincipal = indicadorPrincipal;
	}
}
