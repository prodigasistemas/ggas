/*
v Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente.impl;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.cliente.IntegracaoClienteContato;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;

/**
 * Classe Integracao Cliente Contato.
 *
 *
 */
public class IntegracaoClienteContatoImpl extends IntegracaoImpl implements IntegracaoClienteContato {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2992159152635490538L;

	private IntegracaoCliente cliente;

	private Long tipoContato;

	private String nome;

	private Long codigoDDD;

	private String fone;

	private String ramal;

	private String descricaoArea;

	private String email;

	private Long principal;

	private Long profissao;

	private Long clienteContato;

	private Long contatoDestino;

	/**
	 * @return the clienteContato
	 */
	@Override
	public Long getClienteContato() {

		return clienteContato;
	}

	/**
	 * @param clienteContato
	 *            the clienteContato to set
	 */
	@Override
	public void setClienteContato(Long clienteContato) {

		this.clienteContato = clienteContato;
	}

	/**
	 * @return the cliente
	 */
	@Override
	public IntegracaoCliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	@Override
	public void setCliente(IntegracaoCliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return the tipoContato
	 */
	@Override
	public Long getTipoContato() {

		return tipoContato;
	}

	/**
	 * @param tipoContato
	 *            the tipoContato to set
	 */
	@Override
	public void setTipoContato(Long tipoContato) {

		this.tipoContato = tipoContato;
	}

	/**
	 * @return the nome
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return the codigoDDD
	 */
	@Override
	public Long getCodigoDDD() {

		return codigoDDD;
	}

	/**
	 * @param codigoDDD
	 *            the codigoDDD to set
	 */
	@Override
	public void setCodigoDDD(Long codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	/**
	 * @return the fone
	 */
	@Override
	public String getFone() {

		return fone;
	}

	/**
	 * @param fone
	 *            the fone to set
	 */
	@Override
	public void setFone(String fone) {

		this.fone = fone;
	}

	/**
	 * @return the ramal
	 */
	@Override
	public String getRamal() {

		return ramal;
	}

	/**
	 * @param ramal
	 *            the ramal to set
	 */
	@Override
	public void setRamal(String ramal) {

		this.ramal = ramal;
	}

	/**
	 * @return the descricaoArea
	 */
	@Override
	public String getDescricaoArea() {

		return descricaoArea;
	}

	/**
	 * @param descricaoArea
	 *            the descricaoArea to set
	 */
	@Override
	public void setDescricaoArea(String descricaoArea) {

		this.descricaoArea = descricaoArea;
	}

	/**
	 * @return the email
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/**
	 * @return the principal
	 */
	@Override
	public Long getPrincipal() {

		return principal;
	}

	/**
	 * @param principal
	 *            the principal to set
	 */
	@Override
	public void setPrincipal(Long principal) {

		this.principal = principal;
	}

	/**
	 * @return the profissao
	 */
	@Override
	public Long getProfissao() {

		return profissao;
	}

	/**
	 * @param profissao
	 *            the profissao to set
	 */
	@Override
	public void setProfissao(Long profissao) {

		this.profissao = profissao;
	}

	/**
	 * @return the contatoDestino
	 */
	@Override
	public Long getContatoDestino() {

		return contatoDestino;
	}

	/**
	 * @param contatoDestino
	 *            the contatoDestino to set
	 */
	@Override
	public void setContatoDestino(Long contatoDestino) {

		this.contatoDestino = contatoDestino;
	}
}
