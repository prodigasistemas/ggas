/**
 * 
 */

package br.com.ggas.integracao.cliente;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Map;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de 
 * @author vpessoa
 */
@Component
public class ProcessarReintegracaoClientes implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		try {

			ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

			logProcessamento.append(Constantes.PULA_LINHA).append("[")
					.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
					.append("] ").append("Iniciando o tratamento dos registros da integração de cliente.").append(Constantes.PULA_LINHA);

			controladorIntegracao.reintegrarClientes(logProcessamento);

			logProcessamento.append(Constantes.PULA_LINHA).append("[")
					.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
					.append("] ").append("Finalizando o tratamento dos registros da integração de cliente.");

		} catch(HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

}
