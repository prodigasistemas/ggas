/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente.impl;

import java.util.Date;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;

/**
 * Classe Integracao Cliente.
 * 
 *
 */
public class IntegracaoClienteImpl extends IntegracaoImpl implements IntegracaoCliente {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7874459283226924071L;

	private Long clienteOrigem;

	private Long clienteResponsavel;

	private String nome;

	private String nomeAbreviado;

	private Long tipoCliente;

	private String emailPrincipal;

	private String emailSecundario;

	private Long clienteSituacao;

	private Long nacionalidade;

	private String cpf;

	private String rg;

	private Date dataEmissaoRG;

	private Long orgaoExpedidor;

	private Long unidadeFederacao;

	private String numeroPassaporte;

	private Date dataNascimento;

	private Long profissao;

	private Long sexo;

	private String nomeMae;

	private String nomePai;

	private Long rendaFamiliar;

	private String nomeFantasia;

	private String cnpj;

	private String inscricaoEstadual;

	private Long atividadeEconomica;

	private String inscricaoMunicipal;

	private String inscricaoRural;

	private Long regimeRecolhimento;

	private Long clienteDestino;

	private String contaAuxiliar;

	private Long fornecedorDestino;

	private Long segmento;

	/**
	 * @return the clienteOrigem
	 */
	@Override
	public Long getClienteOrigem() {

		return clienteOrigem;
	}

	/**
	 * @param clienteOrigem
	 *            the clienteOrigem to set
	 */
	@Override
	public void setClienteOrigem(Long clienteOrigem) {

		this.clienteOrigem = clienteOrigem;
	}

	/**
	 * @return the clienteResponsavel
	 */
	@Override
	public Long getClienteResponsavel() {

		return clienteResponsavel;
	}

	/**
	 * @param clienteResponsavel
	 *            the clienteResponsavel to set
	 */
	@Override
	public void setClienteResponsavel(Long clienteResponsavel) {

		this.clienteResponsavel = clienteResponsavel;
	}

	/**
	 * @return the nome
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return the nomeAbreviado
	 */
	@Override
	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	/**
	 * @param nomeAbreviado
	 *            the nomeAbreviado to set
	 */
	@Override
	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	/**
	 * @return the tipoCliente
	 */
	@Override
	public Long getTipoCliente() {

		return tipoCliente;
	}

	/**
	 * @param tipoCliente
	 *            the tipoCliente to set
	 */
	@Override
	public void setTipoCliente(Long tipoCliente) {

		this.tipoCliente = tipoCliente;
	}

	/**
	 * @return the emailPrincipal
	 */
	@Override
	public String getEmailPrincipal() {

		return emailPrincipal;
	}

	/**
	 * @param emailPrincipal
	 *            the emailPrincipal to set
	 */
	@Override
	public void setEmailPrincipal(String emailPrincipal) {

		this.emailPrincipal = emailPrincipal;
	}

	/**
	 * @return the emailSecundario
	 */
	@Override
	public String getEmailSecundario() {

		return emailSecundario;
	}

	/**
	 * @param emailSecundario
	 *            the emailSecundario to set
	 */
	@Override
	public void setEmailSecundario(String emailSecundario) {

		this.emailSecundario = emailSecundario;
	}

	/**
	 * @return the clienteSituacao
	 */
	@Override
	public Long getClienteSituacao() {

		return clienteSituacao;
	}

	/**
	 * @param clienteSituacao
	 *            the clienteSituacao to set
	 */
	@Override
	public void setClienteSituacao(Long clienteSituacao) {

		this.clienteSituacao = clienteSituacao;
	}

	/**
	 * @return the nacionalidade
	 */
	@Override
	public Long getNacionalidade() {

		return nacionalidade;
	}

	/**
	 * @param nacionalidade
	 *            the nacionalidade to set
	 */
	@Override
	public void setNacionalidade(Long nacionalidade) {

		this.nacionalidade = nacionalidade;
	}

	/**
	 * @return the cpf
	 */
	@Override
	public String getCpf() {

		return cpf;
	}

	/**
	 * @param cpf
	 *            the cpf to set
	 */
	@Override
	public void setCpf(String cpf) {

		this.cpf = cpf;
	}

	/**
	 * @return the rg
	 */
	@Override
	public String getRg() {

		return rg;
	}

	/**
	 * @param rg
	 *            the rg to set
	 */
	@Override
	public void setRg(String rg) {

		this.rg = rg;
	}

	/**
	 * @return the dataEmissaoRG
	 */
	@Override
	public Date getDataEmissaoRG() {
		Date data = null;
		if (this.dataEmissaoRG != null) {
			data = (Date) dataEmissaoRG.clone();
		}
		return data;
	}

	/**
	 * @param dataEmissaoRG
	 *            the dataEmissaoRG to set
	 */
	@Override
	public void setDataEmissaoRG(Date dataEmissaoRG) {
		if (dataEmissaoRG != null) {
			this.dataEmissaoRG = (Date) dataEmissaoRG.clone();
		} else {
			this.dataEmissaoRG = null;
		}
	}

	/**
	 * @return the orgaoExpedidor
	 */
	@Override
	public Long getOrgaoExpedidor() {

		return orgaoExpedidor;
	}

	/**
	 * @param orgaoExpedidor
	 *            the orgaoExpedidor to set
	 */
	@Override
	public void setOrgaoExpedidor(Long orgaoExpedidor) {

		this.orgaoExpedidor = orgaoExpedidor;
	}

	/**
	 * @return the unidadeFederacao
	 */
	@Override
	public Long getUnidadeFederacao() {

		return unidadeFederacao;
	}

	/**
	 * @param unidadeFederacao
	 *            the unidadeFederacao to set
	 */
	@Override
	public void setUnidadeFederacao(Long unidadeFederacao) {

		this.unidadeFederacao = unidadeFederacao;
	}

	/**
	 * @return the numeroPassaporte
	 */
	@Override
	public String getNumeroPassaporte() {

		return numeroPassaporte;
	}

	/**
	 * @param numeroPassaporte
	 *            the numeroPassaporte to set
	 */
	@Override
	public void setNumeroPassaporte(String numeroPassaporte) {

		this.numeroPassaporte = numeroPassaporte;
	}

	/**
	 * @return the dataNascimento
	 */
	@Override
	public Date getDataNascimento() {
		Date data = null;
		if (this.dataNascimento != null) {
			data = (Date) dataNascimento.clone();
		}
		return data;
	}

	/**
	 * @param dataNascimento
	 *            the dataNascimento to set
	 */
	@Override
	public void setDataNascimento(Date dataNascimento) {
		if (dataNascimento != null) {
			this.dataNascimento = (Date) dataNascimento.clone();
		} else {
			this.dataNascimento = null;
		}
	}

	/**
	 * @return the profissao
	 */
	@Override
	public Long getProfissao() {

		return profissao;
	}

	/**
	 * @param profissao
	 *            the profissao to set
	 */
	@Override
	public void setProfissao(Long profissao) {

		this.profissao = profissao;
	}

	/**
	 * @return the sexo
	 */
	@Override
	public Long getSexo() {

		return sexo;
	}

	/**
	 * @param sexo
	 *            the sexo to set
	 */
	@Override
	public void setSexo(Long sexo) {

		this.sexo = sexo;
	}

	/**
	 * @return the nomeMae
	 */
	@Override
	public String getNomeMae() {

		return nomeMae;
	}

	/**
	 * @param nomeMae
	 *            the nomeMae to set
	 */
	@Override
	public void setNomeMae(String nomeMae) {

		this.nomeMae = nomeMae;
	}

	/**
	 * @return the nomePai
	 */
	@Override
	public String getNomePai() {

		return nomePai;
	}

	/**
	 * @param nomePai
	 *            the nomePai to set
	 */
	@Override
	public void setNomePai(String nomePai) {

		this.nomePai = nomePai;
	}

	/**
	 * @return the rendaFamiliar
	 */
	@Override
	public Long getRendaFamiliar() {

		return rendaFamiliar;
	}

	/**
	 * @param rendaFamiliar
	 *            the rendaFamiliar to set
	 */
	@Override
	public void setRendaFamiliar(Long rendaFamiliar) {

		this.rendaFamiliar = rendaFamiliar;
	}

	/**
	 * @return the nomeFantasia
	 */
	@Override
	public String getNomeFantasia() {

		return nomeFantasia;
	}

	/**
	 * @param nomeFantasia
	 *            the nomeFantasia to set
	 */
	@Override
	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	/**
	 * @return the cnpj
	 */
	@Override
	public String getCnpj() {

		return cnpj;
	}

	/**
	 * @param cnpj
	 *            the cnpj to set
	 */
	@Override
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	/**
	 * @return the inscricaoEstadual
	 */
	@Override
	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	/**
	 * @param inscricaoEstadual
	 *            the inscricaoEstadual to set
	 */
	@Override
	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	/**
	 * @return the atividadeEconomica
	 */
	@Override
	public Long getAtividadeEconomica() {

		return atividadeEconomica;
	}

	/**
	 * @param atividadeEconomica
	 *            the atividadeEconomica to set
	 */
	@Override
	public void setAtividadeEconomica(Long atividadeEconomica) {

		this.atividadeEconomica = atividadeEconomica;
	}

	/**
	 * @return the inscricaoMunicipal
	 */
	@Override
	public String getInscricaoMunicipal() {

		return inscricaoMunicipal;
	}

	/**
	 * @param inscricaoMunicipal
	 *            the inscricaoMunicipal to set
	 */
	@Override
	public void setInscricaoMunicipal(String inscricaoMunicipal) {

		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	/**
	 * @return the inscricaoRural
	 */
	@Override
	public String getInscricaoRural() {

		return inscricaoRural;
	}

	/**
	 * @param inscricaoRural
	 *            the inscricaoRural to set
	 */
	@Override
	public void setInscricaoRural(String inscricaoRural) {

		this.inscricaoRural = inscricaoRural;
	}

	/**
	 * @return the regimeRecolhimento
	 */
	@Override
	public Long getRegimeRecolhimento() {

		return regimeRecolhimento;
	}

	/**
	 * @param regimeRecolhimento
	 *            the regimeRecolhimento to set
	 */
	@Override
	public void setRegimeRecolhimento(Long regimeRecolhimento) {

		this.regimeRecolhimento = regimeRecolhimento;
	}

	/**
	 * @return the clienteDestino
	 */
	@Override
	public Long getClienteDestino() {

		return clienteDestino;
	}

	/**
	 * @param clienteDestino
	 *            the clienteDestino to set
	 */
	@Override
	public void setClienteDestino(Long clienteDestino) {

		this.clienteDestino = clienteDestino;
	}

	@Override
	public String getContaAuxiliar() {

		return contaAuxiliar;
	}

	@Override
	public void setContaAuxiliar(String contaAuxiliar) {

		this.contaAuxiliar = contaAuxiliar;
	}

	@Override
	public Long getFornecedorDestino() {

		return fornecedorDestino;
	}

	@Override
	public void setFornecedorDestino(Long fornecedorDestino) {

		this.fornecedorDestino = fornecedorDestino;
	}

	@Override
	public Long getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Long segmento) {

		this.segmento = segmento;
	}

}
