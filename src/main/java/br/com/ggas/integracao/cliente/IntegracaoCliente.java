/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente;

import java.util.Date;

import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoCliente.
 */
public interface IntegracaoCliente extends Integracao {

	/** The bean id integracao cliente. */
	String BEAN_ID_INTEGRACAO_CLIENTE = "integracaoCliente";

	/**
	 * Gets the cliente origem.
	 *
	 * @return the cliente origem
	 */
	Long getClienteOrigem();

	/**
	 * Sets the cliente origem.
	 *
	 * @param clienteOrigem the new cliente origem
	 */
	void setClienteOrigem(Long clienteOrigem);

	/**
	 * Gets the cliente responsavel.
	 *
	 * @return the cliente responsavel
	 */
	Long getClienteResponsavel();

	/**
	 * Sets the cliente responsavel.
	 *
	 * @param clienteResponsavel the new cliente responsavel
	 */
	void setClienteResponsavel(Long clienteResponsavel);

	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	String getNome();

	/**
	 * Sets the nome.
	 *
	 * @param nome the new nome
	 */
	void setNome(String nome);

	/**
	 * Gets the nome abreviado.
	 *
	 * @return the nome abreviado
	 */
	String getNomeAbreviado();

	/**
	 * Sets the nome abreviado.
	 *
	 * @param nomeAbreviado the new nome abreviado
	 */
	void setNomeAbreviado(String nomeAbreviado);

	/**
	 * Gets the tipo cliente.
	 *
	 * @return the tipo cliente
	 */
	Long getTipoCliente();

	/**
	 * Sets the tipo cliente.
	 *
	 * @param tipoCliente the new tipo cliente
	 */
	void setTipoCliente(Long tipoCliente);

	/**
	 * Gets the email principal.
	 *
	 * @return the email principal
	 */
	String getEmailPrincipal();

	/**
	 * Sets the email principal.
	 *
	 * @param emailPrincipal the new email principal
	 */
	void setEmailPrincipal(String emailPrincipal);

	/**
	 * Gets the email secundario.
	 *
	 * @return the email secundario
	 */
	String getEmailSecundario();

	/**
	 * Sets the email secundario.
	 *
	 * @param emailSecundario the new email secundario
	 */
	void setEmailSecundario(String emailSecundario);

	/**
	 * Gets the cliente situacao.
	 *
	 * @return the cliente situacao
	 */
	Long getClienteSituacao();

	/**
	 * Sets the cliente situacao.
	 *
	 * @param clienteSituacao the new cliente situacao
	 */
	void setClienteSituacao(Long clienteSituacao);

	/**
	 * Gets the nacionalidade.
	 *
	 * @return the nacionalidade
	 */
	Long getNacionalidade();

	/**
	 * Sets the nacionalidade.
	 *
	 * @param nacionalidade the new nacionalidade
	 */
	void setNacionalidade(Long nacionalidade);

	/**
	 * Gets the cpf.
	 *
	 * @return the cpf
	 */
	String getCpf();

	/**
	 * Sets the cpf.
	 *
	 * @param cpf the new cpf
	 */
	void setCpf(String cpf);

	/**
	 * Gets the rg.
	 *
	 * @return the rg
	 */
	String getRg();

	/**
	 * Sets the rg.
	 *
	 * @param rg the new rg
	 */
	void setRg(String rg);

	/**
	 * Gets the data emissao rg.
	 *
	 * @return the data emissao rg
	 */
	Date getDataEmissaoRG();

	/**
	 * Sets the data emissao rg.
	 *
	 * @param dataEmissaoRG the new data emissao rg
	 */
	void setDataEmissaoRG(Date dataEmissaoRG);

	/**
	 * Gets the orgao expedidor.
	 *
	 * @return the orgao expedidor
	 */
	Long getOrgaoExpedidor();

	/**
	 * Sets the orgao expedidor.
	 *
	 * @param orgaoExpedidor the new orgao expedidor
	 */
	void setOrgaoExpedidor(Long orgaoExpedidor);

	/**
	 * Gets the unidade federacao.
	 *
	 * @return the unidade federacao
	 */
	Long getUnidadeFederacao();

	/**
	 * Sets the unidade federacao.
	 *
	 * @param unidadeFederacao the new unidade federacao
	 */
	void setUnidadeFederacao(Long unidadeFederacao);

	/**
	 * Gets the numero passaporte.
	 *
	 * @return the numero passaporte
	 */
	String getNumeroPassaporte();

	/**
	 * Sets the numero passaporte.
	 *
	 * @param numeroPassaporte the new numero passaporte
	 */
	void setNumeroPassaporte(String numeroPassaporte);

	/**
	 * Gets the data nascimento.
	 *
	 * @return the data nascimento
	 */
	Date getDataNascimento();

	/**
	 * Sets the data nascimento.
	 *
	 * @param dataNascimento the new data nascimento
	 */
	void setDataNascimento(Date dataNascimento);

	/**
	 * Gets the profissao.
	 *
	 * @return the profissao
	 */
	Long getProfissao();

	/**
	 * Sets the profissao.
	 *
	 * @param profissao the new profissao
	 */
	void setProfissao(Long profissao);

	/**
	 * Gets the sexo.
	 *
	 * @return the sexo
	 */
	Long getSexo();

	/**
	 * Sets the sexo.
	 *
	 * @param sexo the new sexo
	 */
	void setSexo(Long sexo);

	/**
	 * Gets the nome mae.
	 *
	 * @return the nome mae
	 */
	String getNomeMae();

	/**
	 * Sets the nome mae.
	 *
	 * @param nomeMae the new nome mae
	 */
	void setNomeMae(String nomeMae);

	/**
	 * Gets the nome pai.
	 *
	 * @return the nome pai
	 */
	String getNomePai();

	/**
	 * Sets the nome pai.
	 *
	 * @param nomePai the new nome pai
	 */
	void setNomePai(String nomePai);

	/**
	 * Gets the renda familiar.
	 *
	 * @return the renda familiar
	 */
	Long getRendaFamiliar();

	/**
	 * Sets the renda familiar.
	 *
	 * @param rendaFamiliar the new renda familiar
	 */
	void setRendaFamiliar(Long rendaFamiliar);

	/**
	 * Gets the nome fantasia.
	 *
	 * @return the nome fantasia
	 */
	String getNomeFantasia();

	/**
	 * Sets the nome fantasia.
	 *
	 * @param nomeFantasia the new nome fantasia
	 */
	void setNomeFantasia(String nomeFantasia);

	/**
	 * Gets the cnpj.
	 *
	 * @return the cnpj
	 */
	String getCnpj();

	/**
	 * Sets the cnpj.
	 *
	 * @param cnpj the new cnpj
	 */
	void setCnpj(String cnpj);

	/**
	 * Gets the inscricao estadual.
	 *
	 * @return the inscricao estadual
	 */
	String getInscricaoEstadual();

	/**
	 * Sets the inscricao estadual.
	 *
	 * @param inscricaoEstadual the new inscricao estadual
	 */
	void setInscricaoEstadual(String inscricaoEstadual);

	/**
	 * Gets the atividade economica.
	 *
	 * @return the atividade economica
	 */
	Long getAtividadeEconomica();

	/**
	 * Sets the atividade economica.
	 *
	 * @param atividadeEconomica the new atividade economica
	 */
	void setAtividadeEconomica(Long atividadeEconomica);

	/**
	 * Gets the inscricao municipal.
	 *
	 * @return the inscricao municipal
	 */
	String getInscricaoMunicipal();

	/**
	 * Sets the inscricao municipal.
	 *
	 * @param inscricaoMunicipal the new inscricao municipal
	 */
	void setInscricaoMunicipal(String inscricaoMunicipal);

	/**
	 * Gets the inscricao rural.
	 *
	 * @return the inscricao rural
	 */
	String getInscricaoRural();

	/**
	 * Sets the inscricao rural.
	 *
	 * @param inscricaoRural the new inscricao rural
	 */
	void setInscricaoRural(String inscricaoRural);

	/**
	 * Gets the regime recolhimento.
	 *
	 * @return the regime recolhimento
	 */
	Long getRegimeRecolhimento();

	/**
	 * Sets the regime recolhimento.
	 *
	 * @param regimeRecolhimento the new regime recolhimento
	 */
	void setRegimeRecolhimento(Long regimeRecolhimento);

	/**
	 * Gets the cliente destino.
	 *
	 * @return the cliente destino
	 */
	Long getClienteDestino();

	/**
	 * Sets the cliente destino.
	 *
	 * @param clienteDestino the new cliente destino
	 */
	void setClienteDestino(Long clienteDestino);

	/**
	 * Gets the conta auxiliar.
	 *
	 * @return the conta auxiliar
	 */
	String getContaAuxiliar();

	/**
	 * Sets the conta auxiliar.
	 *
	 * @param contaAuxiliar the new conta auxiliar
	 */
	void setContaAuxiliar(String contaAuxiliar);

	/**
	 * Gets the fornecedor destino.
	 *
	 * @return the fornecedor destino
	 */
	Long getFornecedorDestino();

	/**
	 * Sets the fornecedor destino.
	 *
	 * @param fornecedorDestino the new fornecedor destino
	 */
	void setFornecedorDestino(Long fornecedorDestino);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Long getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	void setSegmento(Long segmento);
}
