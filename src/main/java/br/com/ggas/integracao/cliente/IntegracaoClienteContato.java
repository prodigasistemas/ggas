/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente;

import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoClienteContato.
 */
public interface IntegracaoClienteContato extends Integracao {

	/** The bean id integracao cliente contato. */
	String BEAN_ID_INTEGRACAO_CLIENTE_CONTATO = "integracaoClienteContato";

	/**
	 * Gets the cliente contato.
	 *
	 * @return the cliente contato
	 */
	Long getClienteContato();

	/**
	 * Sets the cliente contato.
	 *
	 * @param clienteContato the new cliente contato
	 */
	void setClienteContato(Long clienteContato);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	IntegracaoCliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(IntegracaoCliente cliente);

	/**
	 * Gets the tipo contato.
	 *
	 * @return the tipo contato
	 */
	Long getTipoContato();

	/**
	 * Sets the tipo contato.
	 *
	 * @param tipoContato the new tipo contato
	 */
	void setTipoContato(Long tipoContato);

	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	String getNome();

	/**
	 * Sets the nome.
	 *
	 * @param nome the new nome
	 */
	void setNome(String nome);

	/**
	 * Gets the codigo DDD.
	 *
	 * @return the codigo DDD
	 */
	Long getCodigoDDD();

	/**
	 * Sets the codigo DDD.
	 *
	 * @param codigoDDD the new codigo DDD
	 */
	void setCodigoDDD(Long codigoDDD);

	/**
	 * Gets the fone.
	 *
	 * @return the fone
	 */
	String getFone();

	/**
	 * Sets the fone.
	 *
	 * @param fone the new fone
	 */
	void setFone(String fone);

	/**
	 * Gets the ramal.
	 *
	 * @return the ramal
	 */
	String getRamal();

	/**
	 * Sets the ramal.
	 *
	 * @param ramal the new ramal
	 */
	void setRamal(String ramal);

	/**
	 * Gets the descricao area.
	 *
	 * @return the descricao area
	 */
	String getDescricaoArea();

	/**
	 * Sets the descricao area.
	 *
	 * @param descricaoArea the new descricao area
	 */
	void setDescricaoArea(String descricaoArea);

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	String getEmail();

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	void setEmail(String email);

	/**
	 * Gets the principal.
	 *
	 * @return the principal
	 */
	Long getPrincipal();

	/**
	 * Sets the principal.
	 *
	 * @param principal the new principal
	 */
	void setPrincipal(Long principal);

	/**
	 * Gets the profissao.
	 *
	 * @return the profissao
	 */
	Long getProfissao();

	/**
	 * Sets the profissao.
	 *
	 * @param profissao the new profissao
	 */
	void setProfissao(Long profissao);

	/**
	 * Gets the contato destino.
	 *
	 * @return the contato destino
	 */
	Long getContatoDestino();

	/**
	 * Sets the contato destino.
	 *
	 * @param contatoDestino the new contato destino
	 */
	void setContatoDestino(Long contatoDestino);

}
