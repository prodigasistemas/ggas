/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente;

import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoClienteEndereco.
 */
public interface IntegracaoClienteEndereco extends Integracao {

	/** The bean id integracao cliente endereco. */
	String BEAN_ID_INTEGRACAO_CLIENTE_ENDERECO = "integracaoClienteEndereco";

	/**
	 * Gets the cliente endereco.
	 *
	 * @return the cliente endereco
	 */
	Long getClienteEndereco();

	/**
	 * Sets the cliente endereco.
	 *
	 * @param clienteEndereco the new cliente endereco
	 */
	void setClienteEndereco(Long clienteEndereco);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	IntegracaoCliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(IntegracaoCliente cliente);

	/**
	 * Gets the cep.
	 *
	 * @return the cep
	 */
	Long getCep();

	/**
	 * Sets the cep.
	 *
	 * @param cep the new cep
	 */
	void setCep(Long cep);

	/**
	 * Gets the tipo endereco.
	 *
	 * @return the tipo endereco
	 */
	Long getTipoEndereco();

	/**
	 * Sets the tipo endereco.
	 *
	 * @param tipoEndereco the new tipo endereco
	 */
	void setTipoEndereco(Long tipoEndereco);

	/**
	 * Gets the logradouro.
	 *
	 * @return the logradouro
	 */
	String getLogradouro();

	/**
	 * Sets the logradouro.
	 *
	 * @param logradouro the new logradouro
	 */
	void setLogradouro(String logradouro);

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	String getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero the new numero
	 */
	void setNumero(String numero);

	/**
	 * Gets the complemento.
	 *
	 * @return the complemento
	 */
	String getComplemento();

	/**
	 * Sets the complemento.
	 *
	 * @param complemento the new complemento
	 */
	void setComplemento(String complemento);

	/**
	 * Gets the bairro.
	 *
	 * @return the bairro
	 */
	String getBairro();

	/**
	 * Sets the bairro.
	 *
	 * @param bairro the new bairro
	 */
	void setBairro(String bairro);

	/**
	 * Gets the caixa postal.
	 *
	 * @return the caixa postal
	 */
	Long getCaixaPostal();

	/**
	 * Sets the caixa postal.
	 *
	 * @param caixaPostal the new caixa postal
	 */
	void setCaixaPostal(Long caixaPostal);

	/**
	 * Gets the correspondencia.
	 *
	 * @return the correspondencia
	 */
	Long getCorrespondencia();

	/**
	 * Sets the correspondencia.
	 *
	 * @param correspondencia the new correspondencia
	 */
	void setCorrespondencia(Long correspondencia);

	/**
	 * Gets the endereco referencia.
	 *
	 * @return the endereco referencia
	 */
	String getEnderecoReferencia();

	/**
	 * Sets the endereco referencia.
	 *
	 * @param enderecoReferencia the new endereco referencia
	 */
	void setEnderecoReferencia(String enderecoReferencia);

	/**
	 * Gets the municipio.
	 *
	 * @return the municipio
	 */
	Long getMunicipio();

	/**
	 * Sets the municipio.
	 *
	 * @param municipio the new municipio
	 */
	void setMunicipio(Long municipio);

	/**
	 * Gets the indicador principal.
	 *
	 * @return the indicador principal
	 */
	Integer getIndicadorPrincipal();

	/**
	 * Sets the indicador principal.
	 *
	 * @param indicadorPrincipal the new indicador principal
	 */
	void setIndicadorPrincipal(Integer indicadorPrincipal);

}
