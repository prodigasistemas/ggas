/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 12:08:57
 @author eguilherme
 */

package br.com.ggas.integracao.cliente;

import br.com.ggas.integracao.geral.Integracao;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a integração cliente / telefone
 *
 */
public interface IntegracaoClienteTelefone extends Integracao {

	String BEAN_ID_INTEGRACAO_CLIENTE_TELEFONE = "integracaoClienteTelefone";

	/**
	 * @return Long - Retorna Telefone do cliente.
	 */
	Long getClienteTelefone();
	/**
	 * @param clienteTelefone - Set telefone cliente.
	 */
	void setClienteTelefone(Long clienteTelefone);

	/**
	 * @return IntegracaoCliente - Retorna objeto Integração Cliente.
	 */
	IntegracaoCliente getCliente();

	/**
	 * @param cliente - Set cliente.
	 */
	void setCliente(IntegracaoCliente cliente);

	/**
	 * @return Long - Retorno Tipo Fone.
	 */
	Long getTipoFone();

	/**
	 * @param tipoFone - Set tipo fone.
	 */
	void setTipoFone(Long tipoFone);

	/**
	 * @return Long - Retorno Código DDD.
	 */
	Long getCodigoDDD();

	/**
	 * @param codigoDDD - Set código DDD.
	 */
	void setCodigoDDD(Long codigoDDD);

	/**
	 * @return String - Retorna Número.
	 */
	String getNumero();

	/**
	 * @param numero - Set número.
	 */
	void setNumero(String numero);

	/**
	 * @return String - Retorna Ramal.
	 */
	String getRamal();

	/**
	 * @param ramal - Set ramal.
	 */
	void setRamal(String ramal);

	/**
	 * @return Long - Retorna Indicador Principal.
	 */
	Long getIndicadorPrincipal();

	/**
	 * @param indicadorPrincipal - Set indicador principal.
	 */
	void setIndicadorPrincipal(Long indicadorPrincipal);

}
