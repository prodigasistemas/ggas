/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 20/02/2013 09:24:02
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;

public class SupervisorioMedicaoVO implements Serializable {

	private static final int INICIO_CAMPO_MES = 4;

	private static final int LIMITE_CAMPO_ANO = 4;

	private static final long serialVersionUID = 1L;

	private SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade;

	private Integer numeroCiclo;

	private String codigoPontoConsumoSupervisorio;

	private Date dataRealizacaoLeitura;

	private BigDecimal leituraSemCorrecaoFatorPTZ;

	private BigDecimal leituraComCorrecaoFatorPTZ;

	private BigDecimal consumoSemCorrecaoFatorPTZ;

	private BigDecimal consumoComCorrecaoFatorPTZ;

	private BigDecimal pressao;

	private BigDecimal temperatura;

	private BigDecimal fatorPTZ;

	private BigDecimal fatorZ;

	private Integer quantidadeSupervisorioMedicaoHoraria;

	private String comentario;

	private String pontoconsumo;

	private Integer dataReferencia;

	private String dataReferenciaFormatado;

	private Integer qtdMedicao;

	private Long chavePrimaria;

	private String anormalidade;

	private String transferido;

	private String integrado;

	private Boolean habilitado;

	private Boolean possuiAutorizacaoAlcada;

	private EntidadeConteudo status;

	private Boolean isStatusPendente;

	private Usuario ultimoUsuarioAlteracao;

	private int versao;

	private Boolean indicadorIntegrado;

	private Boolean indicadorProcessado;

	private Boolean indicadorConsolidada;

	private Date dataRegistroLeitura;

	private Date ultimaAlteracao;
	
	private String medidor;

	/**
	 * @return the comentario
	 */
	public String getComentario() {

		return comentario;
	}

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	/**
	 * @return the supervisorioMedicaoAnormalidade
	 */
	public SupervisorioMedicaoAnormalidade getSupervisorioMedicaoAnormalidade() {

		return supervisorioMedicaoAnormalidade;
	}

	/**
	 * @param supervisorioMedicaoAnormalidade
	 *            the supervisorioMedicaoAnormalidade to set
	 */
	public void setSupervisorioMedicaoAnormalidade(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade) {

		this.supervisorioMedicaoAnormalidade = supervisorioMedicaoAnormalidade;
	}

	/**
	 * @return the numeroCiclo
	 */
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	public String getCodigoPontoConsumoSupervisorio() {

		return codigoPontoConsumoSupervisorio;
	}

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	public void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio) {

		this.codigoPontoConsumoSupervisorio = codigoPontoConsumoSupervisorio;
	}

	/**
	 * @return the dataRealizacaoLeitura
	 */
	public Date getDataRealizacaoLeitura() {
		Date data = null;
		if (this.dataRealizacaoLeitura != null) {
			data = (Date) dataRealizacaoLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataRealizacaoLeitura
	 *            the dataRealizacaoLeitura to set
	 */
	public void setDataRealizacaoLeitura(Date dataRealizacaoLeitura) {
		if (dataRealizacaoLeitura != null) {
			this.dataRealizacaoLeitura = (Date) dataRealizacaoLeitura.clone();
		} else {
			this.dataRealizacaoLeitura = null;
		}
	}

	/**
	 * @return the leituraSemCorrecaoFatorPTZ
	 */
	public BigDecimal getLeituraSemCorrecaoFatorPTZ() {

		return leituraSemCorrecaoFatorPTZ;
	}

	/**
	 * @param leituraSemCorrecaoFatorPTZ
	 *            the leituraSemCorrecaoFatorPTZ to set
	 */
	public void setLeituraSemCorrecaoFatorPTZ(BigDecimal leituraSemCorrecaoFatorPTZ) {

		this.leituraSemCorrecaoFatorPTZ = leituraSemCorrecaoFatorPTZ;
	}

	/**
	 * @return the leituraComCorrecaoFatorPTZ
	 */
	public BigDecimal getLeituraComCorrecaoFatorPTZ() {

		return leituraComCorrecaoFatorPTZ;
	}

	/**
	 * @param leituraComCorrecaoFatorPTZ
	 *            the leituraComCorrecaoFatorPTZ to set
	 */
	public void setLeituraComCorrecaoFatorPTZ(BigDecimal leituraComCorrecaoFatorPTZ) {

		this.leituraComCorrecaoFatorPTZ = leituraComCorrecaoFatorPTZ;
	}

	/**
	 * @return the consumoSemCorrecaoFatorPTZ
	 */
	public BigDecimal getConsumoSemCorrecaoFatorPTZ() {

		return consumoSemCorrecaoFatorPTZ;
	}

	/**
	 * @param consumoSemCorrecaoFatorPTZ
	 *            the consumoSemCorrecaoFatorPTZ to set
	 */
	public void setConsumoSemCorrecaoFatorPTZ(BigDecimal consumoSemCorrecaoFatorPTZ) {

		this.consumoSemCorrecaoFatorPTZ = consumoSemCorrecaoFatorPTZ;
	}

	/**
	 * @return the consumoComCorrecaoFatorPTZ
	 */
	public BigDecimal getConsumoComCorrecaoFatorPTZ() {

		return consumoComCorrecaoFatorPTZ;
	}

	/**
	 * @param consumoComCorrecaoFatorPTZ
	 *            the consumoComCorrecaoFatorPTZ to set
	 */
	public void setConsumoComCorrecaoFatorPTZ(BigDecimal consumoComCorrecaoFatorPTZ) {

		this.consumoComCorrecaoFatorPTZ = consumoComCorrecaoFatorPTZ;
	}

	/**
	 * @return the pressao
	 */
	public BigDecimal getPressao() {

		return pressao;
	}

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	public void setPressao(BigDecimal pressao) {

		this.pressao = pressao;
	}

	/**
	 * @return the temperatura
	 */
	public BigDecimal getTemperatura() {

		return temperatura;
	}

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	public void setTemperatura(BigDecimal temperatura) {

		this.temperatura = temperatura;
	}

	/**
	 * @return the fatorPTZ
	 */
	public BigDecimal getFatorPTZ() {

		return fatorPTZ;
	}

	/**
	 * @param fatorPTZ
	 *            the fatorPTZ to set
	 */
	public void setFatorPTZ(BigDecimal fatorPTZ) {

		this.fatorPTZ = fatorPTZ;
	}

	/**
	 * @return the fatorZ
	 */
	public BigDecimal getFatorZ() {

		return fatorZ;
	}

	/**
	 * @param fatorZ
	 *            the fatorZ to set
	 */
	public void setFatorZ(BigDecimal fatorZ) {

		this.fatorZ = fatorZ;
	}

	/**
	 * @return the quantidadeSupervisorioMedicaoHoraria
	 */
	public Integer getQuantidadeSupervisorioMedicaoHoraria() {

		return quantidadeSupervisorioMedicaoHoraria;
	}

	/**
	 * @param quantidadeSMH
	 *            the quantidadeSMH to set
	 */
	public void setQuantidadeSupervisorioMedicaoHoraria(Integer quantidadeSupervisorioMedicaoHoraria) {

		this.quantidadeSupervisorioMedicaoHoraria = quantidadeSupervisorioMedicaoHoraria;
	}

	/**
	 * @return the pontoconsumo
	 */
	public String getPontoconsumo() {

		return pontoconsumo;
	}

	/**
	 * @param pontoconsumo
	 *            the pontoconsumo to set
	 */
	public void setPontoconsumo(String pontoconsumo) {

		this.pontoconsumo = pontoconsumo;
	}

	/**
	 * @return the dataReferencia
	 */
	public Integer getDataReferencia() {

		return dataReferencia;
	}

	public String getDataReferenciaFormatado() {

		String referencia = null;
		String ano = String.valueOf(dataReferencia).substring(0, LIMITE_CAMPO_ANO);
		String mes = String.valueOf(dataReferencia).substring(INICIO_CAMPO_MES, String.valueOf(dataReferencia).length());
		if(numeroCiclo == null) {
			referencia = "";
		} else {
			referencia = mes + "/" + ano + "-" + numeroCiclo;
		}
		return referencia;
	}

	/**
	 * @param dataReferencia
	 *            the dataReferencia to set
	 */
	public void setDataReferencia(Integer dataReferencia) {

		this.dataReferencia = dataReferencia;
	}

	/**
	 * @return the qtdMedicao
	 */
	public Integer getQtdMedicao() {

		return qtdMedicao;
	}

	/**
	 * @param qtdMedicao
	 *            the qtdMedicao to set
	 */
	public void setQtdMedicao(Integer qtdMedicao) {

		this.qtdMedicao = qtdMedicao;
	}

	/**
	 * @return the chavePrimaria
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 *            the chavePrimaria to set
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return the transferido
	 */
	public String getTransferido() {

		return transferido;
	}

	/**
	 * @param transferido
	 *            the transferido to set
	 */
	public void setTransferido(String transferido) {

		this.transferido = transferido;
	}

	/**
	 * @return the integrado
	 */
	public String getIntegrado() {

		return integrado;
	}

	/**
	 * @param integrado
	 *            the integrado to set
	 */
	public void setIntegrado(String integrado) {

		this.integrado = integrado;
	}

	/**
	 * @return the anormalidade
	 */
	public String getAnormalidade() {

		return anormalidade;
	}

	/**
	 * @param anormalidade
	 *            the anormalidade to set
	 */
	public void setAnormalidade(String anormalidade) {

		this.anormalidade = anormalidade;
	}

	/**
	 * @return the habilitado
	 */
	public Boolean getHabilitado() {

		return habilitado;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	public void setHabilitado(Boolean habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * @return the possuiAutorizacaoAlcada
	 */
	public Boolean getPossuiAutorizacaoAlcada() {

		return possuiAutorizacaoAlcada;
	}

	/**
	 * @param possuiAutorizacaoAlcada
	 *            the possuiAutorizacaoAlcada to set
	 */
	public void setPossuiAutorizacaoAlcada(Boolean possuiAutorizacaoAlcada) {

		this.possuiAutorizacaoAlcada = possuiAutorizacaoAlcada;
	}

	public EntidadeConteudo getStatus() {

		return status;
	}

	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/**
	 * @return the isStatusPendente
	 */
	public Boolean getIsStatusPendente() {

		return isStatusPendente;
	}

	/**
	 * @param isStatusPendente
	 *            the isStatusPendente to set
	 */
	public void setIsStatusPendente(Boolean isStatusPendente) {

		this.isStatusPendente = isStatusPendente;
	}

	public Usuario getUltimoUsuarioAlteracao() {

		return ultimoUsuarioAlteracao;
	}

	public void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao) {

		this.ultimoUsuarioAlteracao = ultimoUsuarioAlteracao;
	}

	public int getVersao() {

		return versao;
	}

	public void setVersao(int versao) {

		this.versao = versao;
	}

	/**
	 * @return the indicadorIntegrado
	 */
	public Boolean getIndicadorIntegrado() {

		return indicadorIntegrado;
	}

	/**
	 * @param indicadorIntegrado
	 *            the indicadorIntegrado to set
	 */
	public void setIndicadorIntegrado(Boolean indicadorIntegrado) {

		this.indicadorIntegrado = indicadorIntegrado;
	}

	/**
	 * @return the indicadorProcessado
	 */
	public Boolean getIndicadorProcessado() {

		return indicadorProcessado;
	}

	/**
	 * @param processado
	 *            the indicadorProcessado to set
	 */
	public void setIndicadorProcessado(Boolean indicadorProcessado) {

		this.indicadorProcessado = indicadorProcessado;
	}

	/**
	 * @return the indicadorConsolidada
	 */
	public Boolean getIndicadorConsolidada() {

		return indicadorConsolidada;
	}

	/**
	 * @param indicadorConsolidada
	 *            the indicadorConsolidada to set
	 */
	public void setIndicadorConsolidada(Boolean indicadorConsolidada) {

		this.indicadorConsolidada = indicadorConsolidada;
	}

	/**
	 * @return the dataRegistroLeitura
	 */
	public Date getDataRegistroLeitura() {
		Date data = null;
		if (this.dataRegistroLeitura != null) {
			data = (Date) dataRegistroLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataRegistroLeitura
	 *            the dataRegistroLeitura to set
	 */
	public void setDataRegistroLeitura(Date dataRegistroLeitura) {
		if (dataRegistroLeitura != null) {
			this.dataRegistroLeitura = (Date) dataRegistroLeitura.clone();
		} else {
			this.dataRegistroLeitura = null;
		}
	}

	/**
	 * @param dataReferenciaFormatado
	 *            the dataReferenciaFormatado to set
	 */
	public void setDataReferenciaFormatado(String dataReferenciaFormatado) {

		this.dataReferenciaFormatado = dataReferenciaFormatado;
	}

	public Date getUltimaAlteracao() {
		Date data = null;
		if (this.ultimaAlteracao != null) {
			data = (Date) ultimaAlteracao.clone();
		}
		return data;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		if (ultimaAlteracao != null) {
			this.ultimaAlteracao = (Date) ultimaAlteracao.clone();
		} else {
			this.ultimaAlteracao = null;
		}
	}

	public String getMedidor() {

		return medidor;
	}

	public void setMedidor(String medidor) {

		this.medidor = medidor;
	}
}
