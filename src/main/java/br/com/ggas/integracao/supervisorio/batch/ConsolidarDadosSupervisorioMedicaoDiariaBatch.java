/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/01/2013 17:01:21
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.batch;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.ggas.batch.Batch;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import org.springframework.stereotype.Component;

/**
 * Classe Consolidar Dados Supervisorio Medicao Diaria Batch.
 * 
 *
 */
@Component
public class ConsolidarDadosSupervisorioMedicaoDiariaBatch implements Batch {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();

		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

		String tipoIntegracao = (String) controladorParametrosSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);

		Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria = new LinkedHashMap<String, List<Object[]>>();

		ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO = new ResumoSupervisorioMedicaoVO();
		
		String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);
		if(tipoIntegracao.equals(tipoIntegracaoHoraria)) {

			
			mapaAnormalidadeSupervisorioMedicaoDiaria = controladorSupervisorio.tratarMedicoesHorarias(null, null, null, logProcessamento,
							mapaAnormalidadeSupervisorioMedicaoDiaria, resumoSupervisorioMedicaoVO, Boolean.TRUE, null);
		}

		controladorSupervisorio.tratarMedicoesDiarias(null, null, null, logProcessamento, mapaAnormalidadeSupervisorioMedicaoDiaria,
						resumoSupervisorioMedicaoVO, null);

		return logProcessamento.toString();
	}

}
