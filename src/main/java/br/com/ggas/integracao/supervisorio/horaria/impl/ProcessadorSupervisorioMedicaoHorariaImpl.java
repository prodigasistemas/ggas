/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.integracao.supervisorio.horaria.impl;
/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

Este programa é um software livre; você pode redistribuí-lo e/ou
modificá-lo sob os termos de Licença Pública Geral GNU, conforme
publicada pela Free Software Foundation; versão 2 da Licença.

O GGAS é distribuído na expectativa de ser útil,
mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
Consulte a Licença Pública Geral GNU para obter mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU
junto com este programa; se não, escreva para Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

This file is part of GGAS, a commercial management system for Gas Distribution Services

GGAS is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

GGAS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
*/

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;

/**
 * This is a Javadoc comment
 * @author pamelagatinho
 */
public class ProcessadorSupervisorioMedicaoHorariaImpl {

	/**
	 * Calcular a média horaria do consumo sem correcao, baseado em uma lista de SupervisorioMedicaoHoraria
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return a media {@link BigDecimal}
	 */
	public BigDecimal getMediaHorariaConsumoSemCorrecao(List<SupervisorioMedicaoHoraria> medicoes) {
		SupervisorioMedicaoHoraria primeiraMedicao = medicoes.get(0);
		SupervisorioMedicaoHoraria ultimaMedicao = medicoes.get(medicoes.size() - 1);
		
		return getMediaHorariaConsumo(primeiraMedicao.getLeituraSemCorrecaoFatorPTZ(), 
				ultimaMedicao.getLeituraSemCorrecaoFatorPTZ(), 
				primeiraMedicao.getDataRealizacaoLeitura(), 
				ultimaMedicao.getDataRealizacaoLeitura());
	}
	
	/**
	 * Calcular a média horaria do consumo com correcao, baseado em uma lista de SupervisorioMedicaoHoraria
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return a media {@link BigDecimal}
	 */
	public BigDecimal getMediaHorariaConsumoComCorrecao(List<SupervisorioMedicaoHoraria> medicoes) {
		SupervisorioMedicaoHoraria primeiraMedicao = medicoes.get(0);
		SupervisorioMedicaoHoraria ultimaMedicao = medicoes.get(medicoes.size() - 1);
		
		return getMediaHorariaConsumo(primeiraMedicao.getLeituraComCorrecaoFatorPTZ(), 
				ultimaMedicao.getLeituraComCorrecaoFatorPTZ(), 
				primeiraMedicao.getDataRealizacaoLeitura(), 
				ultimaMedicao.getDataRealizacaoLeitura());
		
	}
	
	/**
	 * Calcular a média de consumo
	 *
	 * @param primeiraLeitura BigDecimal {@link BigDecimal}
	 * @param ultimaLeitura BigDecimal {@link BigDecimal}
	 * @param primeiroHorario Date {@link Date}
	 * @param ultimoHorario Date {@link Date}
	 * @return a media {@link BigDecimal}
	 */
	private BigDecimal getMediaHorariaConsumo(BigDecimal primeiraLeitura, BigDecimal ultimaLeitura,Date primeiroHorario, Date ultimoHorario) {
		BigDecimal diferencaLeitura = ultimaLeitura.subtract(primeiraLeitura);
		Integer diferencaHora = DataUtil.getHora(ultimoHorario) - DataUtil.getHora(primeiroHorario);
		
		return diferencaLeitura.divide(new BigDecimal(diferencaHora), 0, BigDecimal.ROUND_DOWN);
	}
	
	/**
	 * Gerar um SupervisorioMedicaoHoraria posterior ao horario repassado
	 *
	 * @param shAnterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @param leituraSemCorrecao BigDecimal {@link BigDecimal}
	 * @param leituraComCorrecao BigDecimal {@link BigDecimal}
	 * @return SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */
	public SupervisorioMedicaoHoraria gerarMedicaoPosterior(SupervisorioMedicaoHoraria shAnterior, BigDecimal leituraSemCorrecao,
			BigDecimal leituraComCorrecao) {
		SupervisorioMedicaoHoraria shPosterior = copiarMedicao(shAnterior);

		shPosterior.setDataRealizacaoLeitura(Util.incrementarHoraData(shAnterior.getDataRealizacaoLeitura(), 1));
		shPosterior.setIndicadorTipoInclusaoMedicao(Constantes.SUPERVISORIO_TIPO_MEDICAO_INCLUSAO_MANUAL);
		shPosterior.setLeituraSemCorrecaoFatorPTZ(shAnterior.getLeituraSemCorrecaoFatorPTZ().add(leituraSemCorrecao));
		shPosterior.setLeituraComCorrecaoFatorPTZ(shAnterior.getLeituraComCorrecaoFatorPTZ().add(leituraComCorrecao));
		shPosterior.setIndicadorConsolidada(Constantes.INDICADOR_CONSOLIDADO);

		return shPosterior;
	}
	
	/**
	 * Gerar um SupervisorioMedicaoHoraria anterior ao horario repassado
	 *
	 * @param shPosterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @param leituraSemCorrecao BigDecimal {@link BigDecimal}
	 * @param leituraComCorrecao BigDecimal {@link BigDecimal}
	 * @return SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */
	public SupervisorioMedicaoHoraria gerarMedicaoAnterior(SupervisorioMedicaoHoraria shPosterior, BigDecimal leituraSemCorrecao,
			BigDecimal leituraComCorrecao) {
		SupervisorioMedicaoHoraria shAnterior = copiarMedicao(shPosterior);

		shAnterior.setDataRealizacaoLeitura(Util.incrementarHoraData(shPosterior.getDataRealizacaoLeitura(), -1));
		shAnterior.setIndicadorTipoInclusaoMedicao(Constantes.SUPERVISORIO_TIPO_MEDICAO_INCLUSAO_MANUAL);
		shAnterior.setLeituraSemCorrecaoFatorPTZ(shPosterior.getLeituraSemCorrecaoFatorPTZ().subtract(leituraSemCorrecao));
		shAnterior.setLeituraComCorrecaoFatorPTZ(shPosterior.getLeituraComCorrecaoFatorPTZ().subtract(leituraComCorrecao));
		shAnterior.setIndicadorConsolidada(Constantes.INDICADOR_CONSOLIDADO);

		return shAnterior;
	}
	
	/**
	 * Gerar um SupervisorioMedicaoHoraria anterior ao horario repassado
	 *
	 * @param shPosterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @param leituraSemCorrecao BigDecimal {@link BigDecimal}
	 * @param leituraComCorrecao BigDecimal {@link BigDecimal}
	 * @param medicoes SupervisorioMedicaoHoraria {@link SupervisorioMedicaoDiaria}
	 * @return SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */	
	public List<SupervisorioMedicaoHoraria> validarLeiturasGeradas(List<SupervisorioMedicaoHoraria> medicoes) {
		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = getMapMedicoes(medicoes);
		int indice = BigDecimal.ONE.intValue();
		SupervisorioMedicaoHoraria medicaoAtual = mapMedicoes.get(indice);
		
		while (indice < Constantes.QUANTIDADE_MAXIMA_MEDICOES_HORARIAS) {
			
			SupervisorioMedicaoHoraria medicaoAnterior = mapMedicoes.get(indice-1);
			
			if (!validarLeituraSemCorrecao(medicaoAnterior, medicaoAtual)) {
				medicaoAnterior.setLeituraSemCorrecaoFatorPTZ(medicaoAtual.getLeituraSemCorrecaoFatorPTZ());
			}
			
			if (!validarLeituraComCorrecao(medicaoAnterior, medicaoAtual)) {
				medicaoAnterior.setLeituraComCorrecaoFatorPTZ(medicaoAtual.getLeituraComCorrecaoFatorPTZ());
			}
			
			medicaoAtual = mapMedicoes.get(++indice);
		}
		
		return medicoes;
	}
	
	/**
	 * Preenche a lista de SupervisorioMedicaoHoraria com as medicoes geradas com a media horaria 
	 *
	 * @param medicoesExistentes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return lista completa de SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */
	public List<SupervisorioMedicaoHoraria> preencherMedicoesMediaHoraria(List<SupervisorioMedicaoHoraria> medicoesExistentes) {
		BigDecimal leituraSemCorrecao = getMediaHorariaConsumoSemCorrecao(medicoesExistentes);
		BigDecimal leituraComCorrecao = getMediaHorariaConsumoComCorrecao(medicoesExistentes);
		
		preencherMedicoes(medicoesExistentes, leituraSemCorrecao, leituraComCorrecao);
		
		return validarLeiturasGeradas(medicoesExistentes);
	}
	
	/**
	 * Preenche a lista de SupervisorioMedicaoHoraria com as medicoes geradas com a media diaria
	 *
	 * @param smhExistentes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @param smdExistentes List<SupervisorioMedicaoDiaria> {@link SupervisorioMedicaoDiaria}
	 * @return lista completa de SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */
	public List<SupervisorioMedicaoHoraria> preencherMedicoesMediaDiaria(List<SupervisorioMedicaoHoraria> smhExistentes,
			List<SupervisorioMedicaoDiaria> smdExistentes) {
		BigDecimal leituraSemCorrecao = getMediaDiariaConsumoSemCorrecao(smdExistentes);
		BigDecimal leituraComCorrecao = getMediaDiariaConsumoSemCorrecao(smdExistentes);

		preencherMedicoes(smhExistentes, leituraSemCorrecao, leituraComCorrecao);

		return validarLeiturasGeradas(smhExistentes);
	}

	/**
	 * Preenche a lista de SupervisorioMedicaoHoraria com as medicoes que faltam
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @param leituraSemCorrecao BigDecimal {@link BigDecimal}
	 * @param leituraComCorrecao BigDecimal {@link BigDecimal}
	 */
	private void preencherMedicoes(List<SupervisorioMedicaoHoraria> medicoes, BigDecimal leituraSemCorrecao,
			BigDecimal leituraComCorrecao) {
		medicoes.addAll(gerarPrimeirasMedicoes(medicoes));

		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = getMapMedicoes(medicoes);
		int indice = BigDecimal.ONE.intValue();
		
		while (indice < Constantes.QUANTIDADE_MAXIMA_MEDICOES_HORARIAS) {
			
			if (!mapMedicoes.containsKey(indice)) {
				SupervisorioMedicaoHoraria medicaoAnterior = mapMedicoes.get(indice - 1);
				SupervisorioMedicaoHoraria medicaoAtual = gerarMedicaoPosterior(medicaoAnterior, leituraSemCorrecao, leituraComCorrecao);
				mapMedicoes.put(indice, medicaoAtual);
				
				medicoes.add(medicaoAtual);
			}
			
			indice++;
		}
	}
	
	/**
	 * Preenche a lista de SupervisorioMedicaoHoraria com as medicoes das primeiras horas do dia
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return lista SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */
	public List<SupervisorioMedicaoHoraria> gerarPrimeirasMedicoes(List<SupervisorioMedicaoHoraria> medicoes) {
		List<SupervisorioMedicaoHoraria> medicoesCompletas = new ArrayList<SupervisorioMedicaoHoraria>();
		
		if (isPrimeiraMedicaoFaltando(medicoes)) {
			BigDecimal leituraSemCorrecao = getMediaHorariaConsumoSemCorrecao(medicoes);
			BigDecimal leituraComCorrecao = getMediaHorariaConsumoComCorrecao(medicoes);
			
			SupervisorioMedicaoHoraria smh1 = medicoes.get(0); 
			
			Integer primeiraHora = DataUtil.getHora(smh1.getDataRealizacaoLeitura());
			
			SupervisorioMedicaoHoraria medicaoPosterior = smh1;
			
			while (primeiraHora != 0) {
				medicaoPosterior = gerarMedicaoAnterior(medicaoPosterior, leituraSemCorrecao, leituraComCorrecao);
				medicoesCompletas.add(medicaoPosterior);
				primeiraHora = primeiraHora -1;
			}
		}
		
		return medicoesCompletas;
	}
	
	/**
	 * Verificar se a medicao horaria que falta e das primeiras horas do dia
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return true, se for das primieiras hora do dia
	 */
	private boolean isPrimeiraMedicaoFaltando(List<SupervisorioMedicaoHoraria> medicoesHoraria) {
		SupervisorioMedicaoHoraria smh1 = medicoesHoraria.get(0);
		
		return DataUtil.getHora(smh1.getDataRealizacaoLeitura()) != 0 ? true : false;
	}
	
	/**
	 * Gera um map com as medicoes, tendo como chave a hora da medicao
	 *
	 * @param medicoesHoraria List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return map Map<Integer, SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 */
	public Map<Integer, SupervisorioMedicaoHoraria> getMapMedicoes(List<SupervisorioMedicaoHoraria> medicoesHoraria) {
		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = new HashMap<Integer, SupervisorioMedicaoHoraria>();
		
		for (SupervisorioMedicaoHoraria medicao : medicoesHoraria) {
			mapMedicoes.put(DataUtil.getHora(medicao.getDataRealizacaoLeitura()), medicao);
		}
		
		return mapMedicoes;
	}
	
	/**
	 * Cria uma copia da medicao 
	 *
	 * @param medicao SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @return medicao SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 */
	private SupervisorioMedicaoHoraria copiarMedicao(SupervisorioMedicaoHoraria medicao) {
		SupervisorioMedicaoHoraria novaMedicao = new SupervisorioMedicaoHorariaImpl();

		novaMedicao.setChavePrimaria(0);
		novaMedicao.setCodigoPontoConsumoSupervisorio(medicao.getCodigoPontoConsumoSupervisorio());
		novaMedicao.setIndicadorTipoMedicao(medicao.getIndicadorTipoMedicao());
		novaMedicao.setPressao(medicao.getPressao());
		novaMedicao.setTemperatura(medicao.getTemperatura());
		novaMedicao.setDataRegistroLeitura(Calendar.getInstance().getTime());
		novaMedicao.setUltimaAlteracao(Calendar.getInstance().getTime());
		novaMedicao.setVersao(medicao.getVersao());
		novaMedicao.setHabilitado(Constantes.INDICADOR_HABILITADO);
		novaMedicao.setSupervisorioMedicaoDiaria(medicao.getSupervisorioMedicaoDiaria());
	
		return novaMedicao;
	}
	
	
	/**
	 * Calcular a media diaria do consumo sem correcao, baseado em uma lista de SupervisorioMedicaoHoraria
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return a media {@link BigDecimal}
	 */
	public BigDecimal getMediaDiariaConsumoSemCorrecao(List<SupervisorioMedicaoDiaria> medicoes) {
		SupervisorioMedicaoDiaria smdPrimeiro = medicoes.get(0);
		SupervisorioMedicaoDiaria smdUltimo = medicoes.get(medicoes.size() - 1);

		BigDecimal diferencaLeitura = smdUltimo.getLeituraSemCorrecaoFatorPTZ().subtract(smdPrimeiro.getLeituraSemCorrecaoFatorPTZ());
		Integer diferencaDia =
				(DataUtil.getDia(smdUltimo.getDataRealizacaoLeitura()) - DataUtil.getDia(smdPrimeiro.getDataRealizacaoLeitura())) + 1;

		BigDecimal valor = diferencaLeitura.divide(new BigDecimal(diferencaDia), 0, BigDecimal.ROUND_DOWN);

		return valor.divide(new BigDecimal(Constantes.QUANTIDADE_MAXIMA_MEDICOES_HORARIAS), 0, BigDecimal.ROUND_DOWN);
	}
	
	/**
	 * Calcular a media diaria do consumo com correcao, baseado em uma lista de SupervisorioMedicaoHoraria
	 *
	 * @param medicoes List<SupervisorioMedicaoHoraria> {@link SupervisorioMedicaoHoraria}
	 * @return a media {@link BigDecimal}
	 */
	public BigDecimal getMediaDiariaConsumoComCorrecao(List<SupervisorioMedicaoDiaria> medicoes) {
		SupervisorioMedicaoDiaria smdPrimeiro = medicoes.get(0);
		SupervisorioMedicaoDiaria smdUltimo = medicoes.get(medicoes.size() - 1);

		BigDecimal diferencaLeitura = smdUltimo.getLeituraComCorrecaoFatorPTZ().subtract(smdPrimeiro.getLeituraComCorrecaoFatorPTZ());
		Integer diferencaDia =
				DataUtil.getDia(smdUltimo.getDataRealizacaoLeitura()) - DataUtil.getDia(smdPrimeiro.getDataRealizacaoLeitura());

		BigDecimal valor = diferencaLeitura.divide(new BigDecimal(diferencaDia), 0, BigDecimal.ROUND_DOWN);

		return valor.divide(new BigDecimal(Constantes.QUANTIDADE_MAXIMA_MEDICOES_HORARIAS), 0, BigDecimal.ROUND_DOWN);
	}
	
	/**
	 * Validar se a leitura sem correcao e valida
	 *
	 * @param medicaoAnterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @param medicaoPosterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @return true, se for valida
	 */
	public boolean validarLeituraSemCorrecao(SupervisorioMedicaoHoraria medicaoAnterior, SupervisorioMedicaoHoraria medicaoPosterior) {
		return medicaoAnterior.getLeituraSemCorrecaoFatorPTZ().compareTo(medicaoPosterior.getLeituraSemCorrecaoFatorPTZ()) <= 0 ;
	}
	
	/**
	 * Validar se a leitura com correcao e valida
	 *
	 * @param medicaoAnterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @param medicaoPosterior SupervisorioMedicaoHoraria {@link SupervisorioMedicaoHoraria}
	 * @return true, se for valida
	 */
	public boolean validarLeituraComCorrecao(SupervisorioMedicaoHoraria medicaoAnterior, SupervisorioMedicaoHoraria medicaoPosterior) {
		return medicaoAnterior.getLeituraComCorrecaoFatorPTZ().compareTo(medicaoPosterior.getLeituraComCorrecaoFatorPTZ()) <= 0 ;
	}
}
