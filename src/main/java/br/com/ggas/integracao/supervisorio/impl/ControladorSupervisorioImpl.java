/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/01/2013 17:31:43
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.DateType;
import org.hibernate.type.StandardBasicTypes;
import org.jfree.util.Log;
import org.joda.time.DateTime;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorGrupoFaturamento;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.SupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario;
import br.com.ggas.integracao.supervisorio.comentario.impl.SupervisorioMedicaoComentarioImpl;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.diaria.impl.SupervisorioMedicaoDiariaImpl;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.integracao.supervisorio.horaria.impl.ProcessadorSupervisorioMedicaoHorariaImpl;
import br.com.ggas.integracao.supervisorio.horaria.impl.SupervisorioMedicaoHorariaImpl;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.UnidadeConversao;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.GenericUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 *
 * Classe responsável por implementar os métodos que gerenciam
 * o supervisório
 *
 */
public class ControladorSupervisorioImpl extends ControladorNegocioImpl implements ControladorSupervisorio {

	private static final int CONSTANTE_NUMERO_ONZE = 11;

	private static final int CONSTANTE_NUMERO_SEIS = 6;

	private static final int CONSTANTE_NUMERO_NOVE = 9;

	private static final int CONSTANTE_NUMERO_OITO = 8;

	private static final int CONSTANTE_NUMERO_SETE = 7;

	private static final int CONSTANTE_NUMERO_ZERO = 0;

	private static final int CONSTANTE_NUMERO_QUATRO = 4;

	private static final int CONSTANTE_NUMERO_TRES = 3;

	private static final int CONSTANTE_NUMERO_UM = 1;

	private static final int CONSTANTE_NUMERO_DOIS = 2;

	private static final String HABILITADO = "habilitado";

	private static final String INDICADOR_INTEGRADO = "indicadorIntegrado";

	private static final String INDICADOR_CONSOLIDADA = "indicadorConsolidada";

	private static final String INDICADOR_PROCESSADO = "indicadorProcessado";

	private static final String INDICADOR_TIPO_MEDICAO = "indicadorTipoMedicao";

	private static final String CODIGO_PONTO_CONSUMO_SUPERVISORIO = "codigoPontoConsumoSupervisorio";

	private static final String CODIGO_MEDIDOR_SUPERVISORIO = "codigoMedidorSupervisorio";

	private static final String DESCRICAO_CODIGO_PONTO_CONSUMO_SUPERVISORIO = "Código Ponto de Consumo Supervisório";

	private static final String DATA_INICIAL_REALIZACAO_LEITURA = "dataInicialRealizacaoLeitura";

	private static final String DATA_FINAL_REALIZACAO_LEITURA = "dataFinalRealizacaoLeitura";

	private static final String DATA_REALIZACAO_LEITURA = "dataRealizacaoLeitura";

	private static final String DESCRICAO_DATA_REALIZACAO_LEITURA = "Data da Realização da Leitura";

	private static final String DESCRICAO_HORA_NAO_FRACIONADA = "Hora não Fracionada";

	private static final String DESCRICAO_DATA_REGISTRO_LEITURA = "Data do Registro da Leitura";

	private static final String DESCRICAO_DATA_REFERENCIA = "Mês/Ano Referência-Ciclo";

	private static final String DESCRICAO_LEITURA_NAO_CORRIGIDA = "Leitura não Corrigida";

	private static final String DESCRICAO_LEITURA_CORRIGIDA = "Leitura Corrigida";

	private static final String DESCRICAO_CONSUMO_NAO_CORRIGIDA = "Volume não Corrigido";

	private static final String DESCRICAO_CONSUMO_CORRIGIDA = "Volume Corrigido";

	private static final String DESCRICAO_FATOR_Z = "Fator Z";

	private static final String DESCRICAO_FATOR_PTZ = "Fator PTZ";

	private static final String DESCRICAO_TEMPERATURA = "Temperatura";

	private static final String DESCRICAO_PRESSAO = "Pressão";

	private static final String DATA_REFERENCIA = "dataReferencia";

	private static final String NUMERO_CICLO = "numeroCiclo";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String SUPERVISORIO_MEDICAO_HORARIA = "supervisorioMedicaoHoraria";

	private static final String SUPERVISORIO_MEDICAO_DIARIA = "supervisorioMedicaoDiaria";

	private static final String CODIGO_STATUS_ALCADA = "codigoStatusAlcada";

	private static final String CODIGO_STATUS_ALCADA_DIFERENTE = "codigoStatusAlcadaDiferente";

	private static final String OCORRENCIA = "ocorrencia";

	private static final String IDS_OCORRENCIA_MEDICAO = "idsOcorrenciaMedicao";

	private static final String ANALISADA = "analisada";

	private static final String TRANSFERENCIA = "transferencia";

	private static final String ANO_MES_REFERENCIA_LIDO = "anoMesReferenciaLido";

	private static final String ID_CICLO = "idCiclo";

	private static final String ENDERECO_REMOTO_LIDO = "enderecoRemotoLido";

	private static final String DATA_LEITURA_INICIAL = "dataLeituraInicial";

	private static final String DATA_LEITURA_FINAL = "dataLeituraFinal";

	private static final String DATA_INCLUSAO_INICIAL = "dataInclusaoInicial";

	private static final String DATA_INCLUSAO_FINAL = "dataInclusaoFinal";

	private static final int ESCALA_SUPERVISORIO = CONSTANTE_NUMERO_OITO;

	private static final int CONSTANTE_NUMERO_DEZ = 10;

	private static final String ATIVO = "ativo";

	private static final String STATUS_AUTORIZADO = "statusAutorizado";

	private static final String REFLECTION_GET = "get";

	private static final String DATA_INTEGRACAO_INICIAL = "dataIntegracaoInicial";

	private static final String DATA_INTEGRACAO_FINAL = "dataIntegracaoFinal";

	private static final String NAO_CONSOLIDADO_OU_POSSUI_ANORMALIDADE = "naoConsolidadoOuPossuiAnormalidadeImpeditiva";

	private static final String ORDENACAO_POR_DATA_REALIZACAO_LEITURA = " order by smh.dataRealizacaoLeitura desc ";

	private static final String DATA_REFERENCIA_CICLO_INICIAL = "dataReferenciaCicloInicial";

	private static final String DATA_REFERENCIA_CICLO_FINAL = "dataReferenciaCicloFinal";

	private static final String ULTIMA_ALTERACAO = "ultimaAlteracao";

	private static final String CONSUMO_SEM_CORRECAO_FATOR_PTZ = "getConsumoSemCorrecaoFatorPTZ";

	private static final String CONSUMO_COM_CORRECAO_FATOR_PTZ = "getConsumoComCorrecaoFatorPTZ";

	private static final String PRESSAO = "getPressao";

	private static final String TEMPERATURA = "getTemperatura";

	private static final String SUPERVISORIO_MEDIA_CALCULADA_COMENTARIO = "SUPERVISORIO_MEDIA_CALCULADA_COMENTARIO";

	private static final Long ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE = 17L;

	private Map<Long, SupervisorioMedicaoAnormalidade> mapaSupervisorioMedicaoAnormalidade;

	private static final Logger LOG = Logger.getLogger(ControladorSupervisorioImpl.class);

	public Class<?> getClasseEntidadeSupervisorioMedicaoHoraria() {

		return ServiceLocator.getInstancia().getClassPorID(SupervisorioMedicaoHoraria.BEAN_ID_SUPERVISORIO_MEDICAO_HORARIA);
	}

	public Class<?> getClasseEntidadeSupervisorioMedicaoDiaria() {

		return ServiceLocator.getInstancia().getClassPorID(SupervisorioMedicaoDiaria.BEAN_ID_SUPERVISORIO_MEDICAO_DIARIA);
	}

	public Class<?> getClasseEntidadeSupervisorioMedicaoAnormalidade() {

		return ServiceLocator.getInstancia().getClassPorID(SupervisorioMedicaoAnormalidade.BEAN_ID_SUPERVISORIO_MEDICAO_ANORMALIDADE);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(Medidor.BEAN_ID_MEDIDOR);
	}

	public Class<?> getClasseEntidadeSupervisorioMedicaoComentario() {

		return ServiceLocator.getInstancia().getClassPorID(SupervisorioMedicaoComentario.BEAN_ID_SUPERVISORIO_MEDICAO_COMENTARIO);
	}

	/**
	 * Criar supervisorio medicao anormalidade.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarSupervisorioMedicaoAnormalidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						SupervisorioMedicaoAnormalidade.BEAN_ID_SUPERVISORIO_MEDICAO_ANORMALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#criarSupervisorioMedicaoDiaria()
	 */
	@Override
	public EntidadeNegocio criarSupervisorioMedicaoDiaria() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(SupervisorioMedicaoDiaria.BEAN_ID_SUPERVISORIO_MEDICAO_DIARIA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#criarSupervisorioMedicaoHoraria()
	 */
	@Override
	public EntidadeNegocio criarSupervisorioMedicaoHoraria() {

		return (EntidadeNegocio) ServiceLocator.getInstancia()
						.getBeanPorID(SupervisorioMedicaoHoraria.BEAN_ID_SUPERVISORIO_MEDICAO_HORARIA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#criarSupervisorioMedicaoComentario()
	 */
	@Override
	public EntidadeNegocio criarSupervisorioMedicaoComentario() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						SupervisorioMedicaoComentario.BEAN_ID_SUPERVISORIO_MEDICAO_COMENTARIO);
	}

	/**
	 * Criar atividade sistema.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAtividadeSistema() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AtividadeSistema.BEAN_ID_ATIVIDADE_SISTEMA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#tratarMedicoesHorarias(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Date, java.util.Date, java.lang.StringBuilder, java.util.Map,
	 * br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO,
	 * java.lang.Boolean)
	 */
	@Override
	public Map<String, List<Object[]>> tratarMedicoesHorarias(PontoConsumo pontoConsumo, Date dataInicialRealizacaoLeitura,
					Date dataFinalRealizacaoLeitura, StringBuilder logProcessamento,
					Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoHoraria,
					ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO, Boolean isBatch, Medidor medidor) throws GGASException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoOperacaoMedidorInstalacao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_INSTALACAO);

		String codigoOperacaoMedidorRetirada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_RETIRADA);

		logProcessamento.append(Constantes.PULA_LINHA).append("[")
				.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime())).append("] ")
				.append(Constantes.INICIANDO_TRATAMENTO_MEDICOES_HORARIAS);

		Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria = new LinkedHashMap<>();

		// Monta filtro para a consulta
		Map<String, Object> filtro = new HashMap<>();

		if (pontoConsumo != null) {
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, pontoConsumo.getCodigoPontoConsumoSupervisorio());
		}
		if (medidor != null) {
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, medidor.getCodigoMedidorSupervisorio());
		}

		filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
		filtro.put(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);

		filtro.put(NAO_CONSOLIDADO_OU_POSSUI_ANORMALIDADE, Boolean.TRUE);

		filtro.put(HABILITADO, Boolean.TRUE);
		filtro.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);

		String paramStatusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		filtro.put(STATUS_AUTORIZADO, Long.valueOf(paramStatusAutorizado));

		// Monta a ordenação da consulta
		String ordenacao = "smh." + CODIGO_PONTO_CONSUMO_SUPERVISORIO + ", smh." + DATA_REALIZACAO_LEITURA;

		// Recupera as medições horárias ainda não processadas ordenadas por codigoPontoConsumoSupervisorio e dataRealizacaoLeitura
		List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaNaoConsolidada = (List<SupervisorioMedicaoHoraria>) this
						.consultarSupervisorioMedicaoHoraria(filtro, ordenacao);

		if (listaSupervisorioMedicaoHorariaNaoConsolidada != null && !listaSupervisorioMedicaoHorariaNaoConsolidada.isEmpty()) {

			// processa os dados das medições horárias não consolidadas
			mapaAnormalidadeSupervisorioMedicaoDiaria.putAll(this.processarListaSupervisorioMedicaoHoraria(
							listaSupervisorioMedicaoHorariaNaoConsolidada, codigoOperacaoMedidorRetirada, codigoOperacaoMedidorInstalacao,
							pontoConsumo, resumoSupervisorioMedicaoVO, logProcessamento, medidor));
			logProcessamento.append("[")
					.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
					.append("] ").append(Constantes.CONCLUINDO_TRATAMENTO_MEDICOES_HORARIAS);
		} else {
			logProcessamento.append("[")
					.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
					.append("] ").append(Constantes.ERRO_NEGOCIO_SEM_DADOS);
		}

		return mapaAnormalidadeSupervisorioMedicaoDiaria;
	}

	private void finalizarGeracaoMedicaoHoraria(List<SupervisorioMedicaoHoraria> medicoes, PontoConsumo pontoConsumo)
			throws  GGASException {
		int quantidadeMinimaMedicoes = obterParametro(Constantes.PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA);

		List<SupervisorioMedicaoHoraria> medicoesInseridas = new ArrayList<SupervisorioMedicaoHoraria>();
		if (!medicoes.isEmpty() && medicoes.size() < Constantes.NUMERO_HORAS_NO_DIA) {

			if (medicoes.size() >= quantidadeMinimaMedicoes) {
				medicoesInseridas.addAll(gerarMedicoesHorariasExtrasMediaHoraria(medicoes));

			} else {
				medicoesInseridas.addAll(gerarMedicoesHorariasExtrasMediaDiaria(medicoes, pontoConsumo));
			}
			inserirMedicoesGeradas(medicoesInseridas);
		}
	}

	private void inserirMedicoesGeradas(List<SupervisorioMedicaoHoraria> medicoes) {
		for (SupervisorioMedicaoHoraria medicao :medicoes) {
			if (medicao.getChavePrimaria() == CONSTANTE_NUMERO_ZERO) {
				try {
					inserir(medicao);
				} catch (NegocioException e) {
					Log.error(e);
				}
			}
		}
	}

	/**
	 * Processar lista supervisorio medicao horaria.
	 *
	 * @param listaSupervisorioMedicaoHorariaNaoConsolidada {@link List}
	 * @param codigoOperacaoMedidorRetirada {@link String}
	 * @param codigoOperacaoMedidorInstalacao {@link String}
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param resumoSupervisorioMedicaoVO {@link ResumoSupervisorioMedicaoVO}
	 * @param logProcessamento {@link StringBuilder}
	 * @param medidorAux {@link Medidor}
	 * @return um mapa com as anormalidades da medicao diária do supervisório {@link }
	 * @throws GGASException {@link GGASException}
	 */
	public Map<String, List<Object[]>> processarListaSupervisorioMedicaoHoraria(
			List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaNaoConsolidada, String codigoOperacaoMedidorRetirada,
			String codigoOperacaoMedidorInstalacao, PontoConsumo pontoConsumo, ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO,
			StringBuilder logProcessamento, Medidor medidorAux) throws GGASException {

		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		SimpleDateFormat d = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
		logProcessamento.append("processarListaSupervisorioMedicaoHoraria INICIO: ").append(d.format(new Date()))
				.append(Constantes.PULA_LINHA);

		List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = null;
		List<VazaoCorretorHistoricoOperacao> listarHistoricoCorretorVazao = null;
		Integer quantidadeSupervisoriMedicaoHorariaProcessados = CONSTANTE_NUMERO_ZERO;
		Map<Long, Integer> mapaAnormalidade = new LinkedHashMap<>();
		PontoConsumo pontoConsumoAux = pontoConsumo;
		Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria = new LinkedHashMap<>();
		Medidor medidor = null;

		String horaInicioDia = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_HORA_INICIO_DIA);

		/*
		 * 1) obtém todos os códigos do PC no supervisório e coloca em uma lista
		 * 2) obtém uma lista das medições não consolidadas por PC
		 * 3) verifica se existem registros já consolidados por PC e pela data inicial da primeira leitura da medição
		 * 4) caso existam registros já consolidados unir os registros não consolidados com os consolidados e ordena por
		 * codigoPontoConsumoSupervisorio e dataRealizacaoLeitura
		 */

		// retorna um mapa com uma lista de medições no supervisório de acordo com o código do ponto de consumo
		Map<String, Collection<SupervisorioMedicaoHoraria>> lista = this
						.gerarListaMedicaoHoraria(listaSupervisorioMedicaoHorariaNaoConsolidada);

		Map<String, PontoConsumo> mapaPontoConsumo = null;
		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumo = null;
		Map<Long, VazaoCorretor> mapaVazaoCorretor = null;
		if (!lista.isEmpty()) {

			Set<String> keys = lista.keySet();
			String[] codigosSupervisorio = keys.toArray(new String[keys.size()]);

			mapaPontoConsumo = controladorPontoConsumo.consultarPontoConsumoPorCodigoSupervisorio(codigosSupervisorio);
			mapaContratoPontoConsumo = controladorContrato.consultarContratoFaturavelPorCodigoSupervisorio(codigosSupervisorio);
			mapaVazaoCorretor = controladorMedidor.obterCorretoresDeVazaoPorCodigosDoSupervisorio(codigosSupervisorio);

		}

		for (Entry<String, Collection<SupervisorioMedicaoHoraria>> entry : lista.entrySet()) {
			String codigoSupervisorio = entry.getKey();

			Integer numeroHoraInicial = Integer.valueOf(horaInicioDia);

			// obtém a lista de medições no supervisório de acordo com o código do ponto de consumo
			List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaPorCodigoSupervisorio = (List<SupervisorioMedicaoHoraria>) lista
							.get(codigoSupervisorio);

			VazaoCorretor vazaoCorretor = null;
			ContratoPontoConsumo contratoPontoConsumo = null;
			Date dataRealizacaoLeitura = null;

			dataRealizacaoLeitura = Util.primeiroElemento(listaSupervisorioMedicaoHorariaPorCodigoSupervisorio).getDataRealizacaoLeitura();

			// Recupera o pontoConsumo pelo codigoPontoConsumoSupervisorio
			if (pontoConsumoAux == null) {

				// Monta filtro para a consulta
				Map<String, Object> filtro = new HashMap<>();
				filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoSupervisorio);

				// Recupera o pontoConsumo pelo codigoPontoConsumoSupervisorio
				if (mapaPontoConsumo != null && !mapaPontoConsumo.isEmpty()) {
					pontoConsumoAux = mapaPontoConsumo.get(codigoSupervisorio);
				}
			}

			Pair<Medidor, Boolean> medidorIndicador = definirMedidor(
							medidorAux, controladorMedidor, pontoConsumoAux,
							medidor, codigoSupervisorio);

			medidor = medidorIndicador.getFirst();
			Boolean indicadorMedidor = medidorIndicador.getSecond();

			if (Boolean.TRUE.equals(indicadorMedidor)) {
				Collection<PontoConsumo> listaPontoConsumoMedidor =
								obtemListaPontoConsumoMedidor(controladorMedidor, medidor);
				if (!listaPontoConsumoMedidor.isEmpty()) {
					pontoConsumoAux = listaPontoConsumoMedidor.iterator().next();
				}
			}

			if (pontoConsumoAux != null) {

				Long chavePrimariaPontoConsumo = pontoConsumoAux.getChavePrimaria();
				if (mapaContratoPontoConsumo != null && !mapaContratoPontoConsumo.isEmpty()) {
					contratoPontoConsumo = mapaContratoPontoConsumo.get(chavePrimariaPontoConsumo);
				}

				// obtém a instalação do medidor para obter o números de dígitos do corretor de vazão
				InstalacaoMedidor instalacaoMedidor = null;
				if (Boolean.TRUE.equals(indicadorMedidor)) {
					instalacaoMedidor = medidor.getInstalacaoMedidor();
					listarHistoricoCorretorVazao = (List<VazaoCorretorHistoricoOperacao>) controladorVazaoCorretor
									.consultarHistoricoOperacaoCorretorVazao(null, Long.valueOf(codigoOperacaoMedidorInstalacao),
													Long.valueOf(codigoOperacaoMedidorRetirada), null, null, medidor.getChavePrimaria());
					if (instalacaoMedidor != null) {
						vazaoCorretor = instalacaoMedidor.getVazaoCorretor();
					}

				} else {
					if (mapaVazaoCorretor != null && !mapaVazaoCorretor.isEmpty()) {
						vazaoCorretor = mapaVazaoCorretor.get(chavePrimariaPontoConsumo);
					}

					// Recupera os registros de operação de Corretor de vazão no Ponto de Consumo
					listarHistoricoCorretorVazao = (List<VazaoCorretorHistoricoOperacao>) controladorVazaoCorretor
									.consultarHistoricoOperacaoCorretorVazao(pontoConsumoAux.getChavePrimaria(),
													Long.valueOf(codigoOperacaoMedidorInstalacao),
													Long.valueOf(codigoOperacaoMedidorRetirada), null, null, null);
				}

				// valida se tem contrato ativo para aquele Ponto de Consumo
				Map<String, Object> mapaContratoAtivo = controladorContrato.validarContratoAtivoPorDataLeitura(dataRealizacaoLeitura,
								contratoPontoConsumo, pontoConsumoAux);

				if (mapaContratoAtivo != null && !mapaContratoAtivo.isEmpty()) {

					Boolean possuiContrato = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);

					// Pega a hora inicial do contrato ativo se tiver
					if (possuiContrato && mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO) != null) {

						numeroHoraInicial = (Integer) mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO);
					}
				}

			}

			Map<String, Date> listaDatas = Util.incrementarDataComHoraEQuantidadeDias(dataRealizacaoLeitura, numeroHoraInicial,
							Constantes.NUMERO_DIAS_INCREMENTAR_DATA);

			listaSupervisorioMedicaoHoraria = listaSupervisorioMedicaoHorariaPorCodigoSupervisorio;

			// incrementa a quantidade de SupervisoriMedicaoHoraria consolidados e não consolidados processados
			quantidadeSupervisoriMedicaoHorariaProcessados = quantidadeSupervisoriMedicaoHorariaProcessados
							+ listaSupervisorioMedicaoHoraria.size();

			this.processarDadosMedicaoHoraria(listaSupervisorioMedicaoHoraria, numeroHoraInicial, listarHistoricoCorretorVazao,
							Long.valueOf(codigoOperacaoMedidorRetirada), Long.valueOf(codigoOperacaoMedidorInstalacao), vazaoCorretor,
							mapaAnormalidade, pontoConsumoAux, listaDatas, mapaAnormalidadeSupervisorioMedicaoDiaria, logProcessamento,
							indicadorMedidor, medidor);

		}

		resumoSupervisorioMedicaoVO.setQuantidadeSupervisorioMedicaoHoraria(quantidadeSupervisoriMedicaoHorariaProcessados);
		resumoSupervisorioMedicaoVO.setMapaAnormalidade(mapaAnormalidade);

		logProcessamento.append("processarListaSupervisorioMedicaoHoraria FIM: ").append(d.format(new Date()))
				.append(Constantes.PULA_LINHA);

		return mapaAnormalidadeSupervisorioMedicaoDiaria;
	}

	private Pair<Medidor, Boolean> definirMedidor(Medidor medidorAux, ControladorMedidor controladorMedidor,
					PontoConsumo pontoConsumoAux, Medidor medidorDefinir, String codigoSupervisorio)
									throws NegocioException {
		Medidor medidor = medidorDefinir;
		Boolean indicadorMedidor = false;

		if(pontoConsumoAux == null){
			if (medidorAux != null) {
				medidor = medidorAux;
				indicadorMedidor = true;
			} else {
				Map<String, Object> filtro = new HashMap<>();
				filtro.put(CODIGO_MEDIDOR_SUPERVISORIO, codigoSupervisorio);
				Collection<Medidor> listaMedidor = controladorMedidor.consultarMedidor(filtro);
				if (!listaMedidor.isEmpty()) {
					medidor = listaMedidor.iterator().next();
					indicadorMedidor = true;
				}
			}
		}

		return new Pair<>(medidor, indicadorMedidor);
	}

	/**
	 * Validar anormalidade supervisorio medicao horaria.
	 *
	 * @param listaDataSupervisorioMedicaoHoraria {@link List}
	 * @param listaDatas {@link Map}
	 * @param mapaAnormalidadeSupervisorioMedicaoDiaria {@link Map}
	 * @param dataPrimeiraMedicao {@link Date}
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param codigoSupervisorio {@link String}
	 * @param listaDataAnormalidade {@link List}
	 * @param medidor {@link Medidor}
	 * @return o mapa com as anormalidades preenchido
	 * @throws NegocioException  {@link NegocioException}
	 */
	public Map<String, List<Object[]>> validarAnormalidadeSupervisorioMedicaoHoraria(List<Date> listaDataSupervisorioMedicaoHoraria,
			Map<String, Date> listaDatas, Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria, Date dataPrimeiraMedicao,
			PontoConsumo pontoConsumo, String codigoSupervisorio, List<Object[]> listaDataAnormalidade, Medidor medidor)
			throws NegocioException {

		Date dataInicial = listaDatas.get(Constantes.DATA_INICIAL);

		int tam = CONSTANTE_NUMERO_ZERO;
		int tamLista = listaDataSupervisorioMedicaoHoraria.size();
		while (tamLista != CONSTANTE_NUMERO_ZERO) {
			tamLista--;
			if (listaDataSupervisorioMedicaoHoraria.contains(dataInicial)) {
				tam++;
			}
			dataInicial = Util.incrementarHoraData(dataInicial, CONSTANTE_NUMERO_UM);
		}

		// Caso a quantidade de registros horários seja menor que 24
		if (listaDataSupervisorioMedicaoHoraria.size() < Constantes.NUMERO_HORAS_NO_DIA || tam < Constantes.NUMERO_HORAS_NO_DIA) {

			// Caso a quantidade de registros horários seja menor que 24 e seja a primeira medição
			if (this.validarPrimeiraLeituraEPrimeiraMedicao(dataPrimeiraMedicao, pontoConsumo, codigoSupervisorio, medidor)) {
				// Gera anormalidade
				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
								.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_PRIMEIRO_DIA_MEDICAO);

				Object[] arrayAnormalidade = new Object[CONSTANTE_NUMERO_DOIS];
				arrayAnormalidade[CONSTANTE_NUMERO_ZERO] = dataPrimeiraMedicao;
				arrayAnormalidade[CONSTANTE_NUMERO_UM] = supervisorioMedicaoAnormalidade;

				listaDataAnormalidade.add(arrayAnormalidade);
				mapaAnormalidadeSupervisorioMedicaoDiaria.put(codigoSupervisorio, listaDataAnormalidade);

			} else {
				// Caso a quantidade de registros horários seja menor que 24 e não seja a primeira medição

				// Gera anormalidade
				int quantidadeMinimaRegistros = obterParametro(Constantes.PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA);

				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
						.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MENOR_24);

				if(tam >= quantidadeMinimaRegistros) {
					supervisorioMedicaoAnormalidade = this
							.obterSupervisorioMedicaoAnormalidade(ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE);
				}


				Object[] arrayAnormalidade = new Object[CONSTANTE_NUMERO_DOIS];
				arrayAnormalidade[CONSTANTE_NUMERO_ZERO] = listaDatas.get(Constantes.DATA_INICIAL);
				arrayAnormalidade[CONSTANTE_NUMERO_UM] = supervisorioMedicaoAnormalidade;

				listaDataAnormalidade.add(arrayAnormalidade);
				mapaAnormalidadeSupervisorioMedicaoDiaria.put(codigoSupervisorio, listaDataAnormalidade);

			}

		} else if (listaDataSupervisorioMedicaoHoraria.size() > Constantes.NUMERO_HORAS_NO_DIA) {
			// Caso a quantidade de registros horários seja maior que 24

			// Gera anormalidade
			SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
							.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_QUANTIDADE_REGISTROS_MAIOR_24);

			Object[] arrayAnormalidade = new Object[CONSTANTE_NUMERO_DOIS];
			arrayAnormalidade[CONSTANTE_NUMERO_ZERO] = listaDatas.get(Constantes.DATA_INICIAL);
			arrayAnormalidade[CONSTANTE_NUMERO_UM] = supervisorioMedicaoAnormalidade;

			listaDataAnormalidade.add(arrayAnormalidade);
			mapaAnormalidadeSupervisorioMedicaoDiaria.put(codigoSupervisorio, listaDataAnormalidade);

		}

		return mapaAnormalidadeSupervisorioMedicaoDiaria;
	}

	private int obterParametro(String nomeParametro) throws NegocioException {
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String quantidadeMinimaRegistrosMedicoesHora = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(nomeParametro);

		return Integer.parseInt(quantidadeMinimaRegistrosMedicoesHora);
	}

	/**
	 * Validar primeira leitura e primeira medicao.
	 *
	 * @param dataPrimeiraMedicao data da Primeira Medicao
	 * @param pontoConsumo the ponto Consumo
	 * @param codigoSupervisorio the codigo supervisorio
	 * @param medidor the medidor
	 * @return um valor booleano indicando se é a primeira medição ou não
	 * @throws NegocioException Lanca um Negocio Exception
	 */
	public Boolean validarPrimeiraLeituraEPrimeiraMedicao(Date dataPrimeiraMedicao, PontoConsumo pontoConsumo, String codigoSupervisorio,
			Medidor medidor) throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia().getControladorHistoricoMedicao();

		Boolean isPrimeiraMedicao = Boolean.FALSE;

		Long quantidadeSupervisorioMedicaoHorariaConsolidado = this
						.contarSupervisorioMedicaoHorariaConsolidado(codigoSupervisorio, dataPrimeiraMedicao, Boolean.TRUE,
										Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);

		if (quantidadeSupervisorioMedicaoHorariaConsolidado == null || quantidadeSupervisorioMedicaoHorariaConsolidado == CONSTANTE_NUMERO_ZERO) {
			Long quantidadeHistorico = null;
			if (pontoConsumo != null) {
				quantidadeHistorico = controladorHistoricoMedicao.obterQuantidadeDoHistoricoMedicaoPorPontoConsumoOuMedidor(
								pontoConsumo.getChavePrimaria(), dataPrimeiraMedicao, null);
			}
			if (medidor != null) {
				quantidadeHistorico = controladorHistoricoMedicao.obterQuantidadeDoHistoricoMedicaoPorPontoConsumoOuMedidor(null,
								dataPrimeiraMedicao, medidor);
			}

			if (quantidadeHistorico == null || quantidadeHistorico <= CONSTANTE_NUMERO_ZERO) {
				isPrimeiraMedicao = Boolean.TRUE;
			}
		}

		return isPrimeiraMedicao;
	}

	/**
	 * Gerar lista medicao pontoconsumo.
	 *
	 * @param listaSupervisorioMedicaoHorariaNaoConsolidada
	 *            the lista supervisorio medicao horaria nao consolidada
	 * @return the map
	 */
	public Map<String, Collection<SupervisorioMedicaoHoraria>> gerarListaMedicaoHoraria(
					List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaNaoConsolidada) {

		Map<String, Collection<SupervisorioMedicaoHoraria>> lista = new LinkedHashMap<>();
		Collection<SupervisorioMedicaoHoraria> listaMedicaoHoraria = new ArrayList<>();
		String codigoPontoConsumoSupervisorio = Util.primeiroElemento(listaSupervisorioMedicaoHorariaNaoConsolidada)
						.getCodigoPontoConsumoSupervisorio();

		for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : listaSupervisorioMedicaoHorariaNaoConsolidada) {

			if (supervisorioMedicaoHoraria != null) {
				if (!codigoPontoConsumoSupervisorio.equalsIgnoreCase(supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio())) {
					lista.put(codigoPontoConsumoSupervisorio, listaMedicaoHoraria);
					listaMedicaoHoraria = new ArrayList<>();
					codigoPontoConsumoSupervisorio = supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio();
				}

				listaMedicaoHoraria.add(supervisorioMedicaoHoraria);
			}
		}

		lista.put(codigoPontoConsumoSupervisorio, listaMedicaoHoraria);

		return lista;
	}

	/**
	 * Gerar lista diaria pontoconsumo.
	 *
	 * @param listaSupervisorioMedicaoDiariaNaoConsolidada
	 *            the lista supervisorio medicao diaria nao consolidada
	 * @return the map
	 */
	public Map<String, Collection<SupervisorioMedicaoDiaria>> gerarListaDiaria(
					List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaNaoConsolidada) {

		Map<String, Collection<SupervisorioMedicaoDiaria>> lista = new HashMap<>();
		Collection<SupervisorioMedicaoDiaria> listaMedicaoDiaria = new ArrayList<>();
		String codigoPontoConsumoSupervisorio = Util.primeiroElemento(listaSupervisorioMedicaoDiariaNaoConsolidada)
						.getCodigoPontoConsumoSupervisorio();

		for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiariaNaoConsolidada) {

			if (!codigoPontoConsumoSupervisorio.equalsIgnoreCase(supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio())) {
				lista.put(codigoPontoConsumoSupervisorio, listaMedicaoDiaria);
				listaMedicaoDiaria = new ArrayList<>();
				codigoPontoConsumoSupervisorio = supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio();
			}

			listaMedicaoDiaria.add(supervisorioMedicaoDiaria);
		}

		lista.put(codigoPontoConsumoSupervisorio, listaMedicaoDiaria);

		return lista;
	}

	/**
	 * Definir referencia cronograma atividade faturamento.
	 *
	 * @param listaCronogramaAtividadeFaturamento
	 *            the lista cronograma atividade faturamento
	 * @param mapaComListaReferenciaLeitura
	 *            the mapa com lista referencia leitura
	 * @return the map
	 */
	public Map<Long, List<Map<String, Object>>> definirReferenciaCronogramaAtividadeFaturamento(
					List<CronogramaAtividadeFaturamento> listaCronogramaAtividadeFaturamento,
					Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura) {

		CronogramaFaturamento cronogramaFaturamento = null;
		Map<String, Object> mapaReferenciasLeitura;
		List<Map<String, Object>> listaMapaReferenciaLeitura = new ArrayList<Map<String, Object>>();
		Long idGrupoFaturamento = Util.primeiroElemento(listaCronogramaAtividadeFaturamento).getCronogramaFaturamento().getGrupoFaturamento()
						.getChavePrimaria();

		for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : listaCronogramaAtividadeFaturamento) {

			cronogramaFaturamento = cronogramaAtividadeFaturamento.getCronogramaFaturamento();
			mapaReferenciasLeitura = new LinkedHashMap<>();
			mapaReferenciasLeitura.put("anoMesFaturamento", cronogramaFaturamento.getAnoMesFaturamento());
			mapaReferenciasLeitura.put("numeroCiclo", cronogramaFaturamento.getNumeroCiclo());
			mapaReferenciasLeitura.put("dataFim", cronogramaAtividadeFaturamento.getDataFim());

			listaMapaReferenciaLeitura.add(mapaReferenciasLeitura);

		}

		mapaComListaReferenciaLeitura.put(idGrupoFaturamento, listaMapaReferenciaLeitura);

		return mapaComListaReferenciaLeitura;
	}

	/**
	 * Processar dados medicao horaria.
	 *
	 * @param listaSupervisorioMedicaoHoraria  {@link List}
	 * @param numeroHoraInicial {@link Integer}
	 * @param listarHistoricoCorretorVazao {@link List}
	 * @param codigoOperacaoMedidorRetirada {@link Long}
	 * @param codigoOperacaoMedidorInstalacao {@link Long}
	 * @param vazaoCorretor {@link VazaoCorretor}
	 * @param mapaAnormalidade {@link Map}
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param listaDatas {@link Map}
	 * @param mapaAnormalidadeSupervisorioMedicaoDiaria {@link List}
	 * @param logProcessamento {@link StringBuilder}
	 * @param indicadorMedidor {@link Boolean}
	 * @param medidor {@link Medidor}
	 * @return Map {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	public Map<String, List<Object[]>> processarDadosMedicaoHoraria(List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria,
					Integer numeroHoraInicial, List<VazaoCorretorHistoricoOperacao> listarHistoricoCorretorVazao,
					Long codigoOperacaoMedidorRetirada, Long codigoOperacaoMedidorInstalacao, VazaoCorretor vazaoCorretor,
					Map<Long, Integer> mapaAnormalidade, PontoConsumo pontoConsumo, Map<String, Date> listaDatas,
					Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria, StringBuilder logProcessamento,
					Boolean indicadorMedidor, Medidor medidor) throws GGASException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia().getControladorHistoricoMedicao();

		Map<String, Date> listaDatasAux = listaDatas;

		SimpleDateFormat d = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
		logProcessamento.append("processarDadosMedicaoHoraria INICIO: ").append(d.format(new Date())).append(Constantes.PULA_LINHA);

		Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaPorDia = new ArrayList<>();
		List<Date> listaDataSupervisorioMedicaoHoraria = new ArrayList<>();
		Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaPorDiaAux = new ArrayList<>();
		List<Date> listaDataSupervisorioMedicaoHorariaAux = new ArrayList<>();
		Boolean isPrimeiraHoraDoDiaMedicao = Boolean.FALSE;
		Date dataPrimeiraMedicao = null;
		Date dataInicialAux = new Timestamp(listaDatasAux.get(Constantes.DATA_INICIAL).getTime());
		Date dataFinalAux = new Timestamp(listaDatasAux.get(Constantes.DATA_FINAL).getTime());
		Date ultimaDataHoraDoDia = DataUtil.gerarDataHmsZerados(dataInicialAux);
		Boolean isSubtituicaoCorretorVazao = Boolean.FALSE;
		Map<String, Object> retornoSubtituicaoCorretorVazao = null;
		Map<String, Object> retornoViradaCorretorVazao = null;
		Date dataAtual = new Date(System.currentTimeMillis());
		List<Object[]> listaDataAnormalidade = new ArrayList<>();

		BigDecimal quantidadeRegistros = BigDecimal.ZERO;
		int index = CONSTANTE_NUMERO_ZERO;
		int tamanho = CONSTANTE_NUMERO_ZERO;
		int posicao = CONSTANTE_NUMERO_ZERO;
		String codigoSupervisorio = null;

		BigDecimal consumoSemCorrecaoFatorPTZ;
		BigDecimal consumoComCorrecaoFatorPTZ;

		// acumuladores
		BigDecimal somatorioConsumo = BigDecimal.ZERO;
		BigDecimal somatorioConsumoCorrigido = BigDecimal.ZERO;
		BigDecimal somatorioPressao = BigDecimal.ZERO;
		BigDecimal somatorioTemperatura = BigDecimal.ZERO;
		SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAnterior = null;
		Boolean possuiHoraFracionada = Boolean.FALSE;
		List<Date> listaData = new ArrayList<>();

		if (listaSupervisorioMedicaoHoraria != null) {
			codigoSupervisorio = Util.primeiroElemento(listaSupervisorioMedicaoHoraria).getCodigoPontoConsumoSupervisorio();

			for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : listaSupervisorioMedicaoHoraria) {

				supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(null);

				consumoSemCorrecaoFatorPTZ = BigDecimal.ZERO;
				consumoComCorrecaoFatorPTZ = BigDecimal.ZERO;

				if (Boolean.TRUE.equals(indicadorMedidor)) {
					verificaHoraInicialConsumo(medidor, controladorMedidor, controladorContrato, supervisorioMedicaoHoraria,
									mapaAnormalidadeSupervisorioMedicaoDiaria, listaDataAnormalidade);
				}

				// verifica registros em duplicidade
				if (listaSupervisorioMedicaoHoraria.size() > CONSTANTE_NUMERO_UM) {

					if (listaData.isEmpty() || !(listaData.contains(supervisorioMedicaoHoraria.getDataRealizacaoLeitura()))) {

						listaData.add(supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
					}

					// compara a data de leitura do resgistro atual com a data de leitura do próximo registro
					int indexProximoRegistro = posicao;
					indexProximoRegistro++;
					if (indexProximoRegistro < listaSupervisorioMedicaoHoraria.size()) {
						//Possivel Implementação Futura.
					}

					// compara a data de leitura do resgistro atual com a data de leitura do registro anterior
					if (posicao > CONSTANTE_NUMERO_ZERO) {
						//Possivel Implementação Futura.
					}
				}

				// acumula dados da medição horária sendo que os dados do primeiro registro não são considerados nas operações de acúmulo
				if (supervisorioMedicaoHoraria.getDataRealizacaoLeitura().compareTo(dataInicialAux) > CONSTANTE_NUMERO_ZERO) {

					// incrementa os acumuladores
					quantidadeRegistros = quantidadeRegistros.add(BigDecimal.ONE);
					somatorioPressao = somatorioPressao.add(supervisorioMedicaoHoraria.getPressao());
					somatorioTemperatura = somatorioTemperatura.add(supervisorioMedicaoHoraria.getTemperatura());

					// verifica se houve substituição de corretor de vazão
					if (listarHistoricoCorretorVazao != null && listarHistoricoCorretorVazao.size() > CONSTANTE_NUMERO_UM) {

						retornoSubtituicaoCorretorVazao = this.tratarSubtituicaoCorretorVazao(supervisorioMedicaoHoraria,
										listarHistoricoCorretorVazao, codigoOperacaoMedidorRetirada, codigoOperacaoMedidorInstalacao,
										dataInicialAux, dataFinalAux, mapaAnormalidade);

						isSubtituicaoCorretorVazao = (Boolean) retornoSubtituicaoCorretorVazao.get(Constantes.SUBSTITUICAO_CORRETOR_VAZAO);

					}

					// se não houve substituição de corretor de vazão
					if (!isSubtituicaoCorretorVazao) {

						// caso não possua medição do supervisório anterior
						if (supervisorioMedicaoHorariaAnterior == null) {

							// procura nas SupervisorioMediçãoHorária já consolidadas por uma medição imediatamente anterior a data da medição
							// atual
							List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaConsolidado = (List<SupervisorioMedicaoHoraria>) this
											.consultarSupervisorioMedicaoHorariaConsolidado(
															supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio(), dataInicialAux,
															Boolean.TRUE, Constantes.SUPERVISORIO_TIPO_MEDICAO_HORARIA);

							if (listaSupervisorioMedicaoHorariaConsolidado != null && !listaSupervisorioMedicaoHorariaConsolidado.isEmpty()) {

								supervisorioMedicaoHorariaAnterior = Util.primeiroElemento(listaSupervisorioMedicaoHorariaConsolidado);
							} else {

								if (pontoConsumo != null || medidor != null) {
									Collection<HistoricoMedicao> listarHistoricoMedicaoPorPontoConsumo = null;

									// caso não tenha medição anterior consolidada e não consolidada procura no histórico de medição
									if (Boolean.TRUE.equals(indicadorMedidor)) {
										listarHistoricoMedicaoPorPontoConsumo = controladorHistoricoMedicao
														.listarHistoricoMedicaoPorMedidor(medidor,
																		supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
									} else {
										listarHistoricoMedicaoPorPontoConsumo = controladorHistoricoMedicao
														.listarHistoricoMedicaoPorPontoConsumo(pontoConsumo,
																		supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
									}

									if (listarHistoricoMedicaoPorPontoConsumo != null && !listarHistoricoMedicaoPorPontoConsumo.isEmpty()) {

										BigDecimal leituraInformada = listarHistoricoMedicaoPorPontoConsumo.iterator().next()
														.getNumeroLeituraInformada();

										BigDecimal leituraFaturada = listarHistoricoMedicaoPorPontoConsumo.iterator().next()
														.getNumeroLeituraFaturada();

										if (leituraInformada != null && leituraFaturada != null) {

											supervisorioMedicaoHorariaAnterior = (SupervisorioMedicaoHoraria) this
															.criarSupervisorioMedicaoHoraria();

											supervisorioMedicaoHorariaAnterior.setLeituraSemCorrecaoFatorPTZ(leituraInformada);
											supervisorioMedicaoHorariaAnterior.setLeituraComCorrecaoFatorPTZ(leituraFaturada);
										}

									}

									if (supervisorioMedicaoHorariaAnterior == null && vazaoCorretor != null) {

										// caso não tenha medição anterior consolidada, não consolidada nem no histórico de medição
										// procura na instalação do corretor de vazão

										ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia()
														.getControladorVazaoCorretor();

										Collection<VazaoCorretorHistoricoOperacao> listaHistoricoCorretorVazao = null;
										if (Boolean.TRUE.equals(indicadorMedidor)) {
											listaHistoricoCorretorVazao = controladorVazaoCorretor.consultarHistoricoOperacaoCorretorVazao(null,
															codigoOperacaoMedidorInstalacao, null,
															vazaoCorretor.getChavePrimaria(),
															supervisorioMedicaoHoraria.getDataRealizacaoLeitura(),
															medidor.getChavePrimaria());
										} else {
											listaHistoricoCorretorVazao = controladorVazaoCorretor.consultarHistoricoOperacaoCorretorVazao(
															pontoConsumo.getChavePrimaria(), codigoOperacaoMedidorInstalacao,
															null, vazaoCorretor.getChavePrimaria(),
															supervisorioMedicaoHoraria.getDataRealizacaoLeitura(), null);
										}

										if (listaHistoricoCorretorVazao != null && !listaHistoricoCorretorVazao.isEmpty()) {

											BigDecimal leituraHistoricoCorretorVazao = listaHistoricoCorretorVazao.iterator().next()
															.getLeitura();

											if (leituraHistoricoCorretorVazao != null) {
												supervisorioMedicaoHorariaAnterior = (SupervisorioMedicaoHoraria) this
																.criarSupervisorioMedicaoHoraria();

												// está faltando a leitura sem correção
												supervisorioMedicaoHorariaAnterior
																.setLeituraComCorrecaoFatorPTZ(leituraHistoricoCorretorVazao);
											}
										}
									}
								}
							}
						}

						if (supervisorioMedicaoHorariaAnterior != null) {

							// caso possua medição do supervisório anterior

							// obtém o tipo de integração
							String tipoIntegracao = (String) controladorParametroSistema
											.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);

							if (supervisorioMedicaoHorariaAnterior.getLeituraSemCorrecaoFatorPTZ() != null) {

								// Caso a leitura atual seja maior ou igual à leitura anterior calcula o consumo
								if (supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ().compareTo(
												supervisorioMedicaoHorariaAnterior.getLeituraSemCorrecaoFatorPTZ()) >= CONSTANTE_NUMERO_ZERO) {

									consumoSemCorrecaoFatorPTZ = supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ().subtract(
													supervisorioMedicaoHorariaAnterior.getLeituraSemCorrecaoFatorPTZ());

								} else {

									// chama o fluxo de virada de mostrador
									if (vazaoCorretor != null) {
										retornoViradaCorretorVazao = this.tratarViradaMostradorCorretorVazao(
														supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ(),
														supervisorioMedicaoHorariaAnterior.getLeituraSemCorrecaoFatorPTZ(), vazaoCorretor,
														tipoIntegracao, mapaAnormalidade);

										if (supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade() == null
														|| supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade()
																		.getIndicadorImpedeProcessamento()
																		.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

											supervisorioMedicaoHoraria
															.setSupervisorioMedicaoAnormalidade((SupervisorioMedicaoAnormalidade) retornoViradaCorretorVazao
																			.get(Constantes.SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA));
										}

										// se a medição for horária e a leitura for sem correção calcula o consumo

										consumoSemCorrecaoFatorPTZ = (BigDecimal) retornoViradaCorretorVazao.get(Constantes.CONSUMO);

									} else {
										// caso a leitura anterior seja menor que a leitura atual e o ponto de consumo não possua corretor de
										// vazão
										// uma anomalia é gerada para o SupervisorioMedicaoHoraria

										// Gera anormalidade

										if (supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade() == null
														|| supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade()
																		.getIndicadorImpedeProcessamento()
																		.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

											SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
															.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_POSSIVEL_VIRADA_SEM_CORRETOR_VAZAO);
											supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
										}
									}
								}
							}

							// Caso a leitura atual corrigida seja maior ou igual à leitura anterior corrigida calcula o consumo corrigido
							if (supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ().compareTo(
											supervisorioMedicaoHorariaAnterior.getLeituraComCorrecaoFatorPTZ()) >= CONSTANTE_NUMERO_ZERO) {

								consumoComCorrecaoFatorPTZ = supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ().subtract(
												supervisorioMedicaoHorariaAnterior.getLeituraComCorrecaoFatorPTZ());
							} else {

								// chama o fluxo de virada de mostrador
								if (vazaoCorretor != null) {

									retornoViradaCorretorVazao = this.tratarViradaMostradorCorretorVazao(
													supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ(),
													supervisorioMedicaoHorariaAnterior.getLeituraComCorrecaoFatorPTZ(), vazaoCorretor,
													tipoIntegracao, mapaAnormalidade);

									if (supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade() == null
													|| supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade()
																	.getIndicadorImpedeProcessamento()
																	.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {
										supervisorioMedicaoHoraria
														.setSupervisorioMedicaoAnormalidade((SupervisorioMedicaoAnormalidade) retornoViradaCorretorVazao
																		.get(Constantes.SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA));

									}

									// se a medição for horária e a leitura for com correção calcula o consumo com correção
									consumoComCorrecaoFatorPTZ = (BigDecimal) retornoViradaCorretorVazao.get(Constantes.CONSUMO);

								} else {
									// caso a leitura anterior seja menor que a leitura atual e o ponto de consumo não possua corretor de vazão
									// uma anomalia é gerada para o SupervisorioMedicaoHoraria

									// Gera anormalidade
									if (supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade() == null
													|| supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade()
																	.getIndicadorImpedeProcessamento()
																	.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

										SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
														.obterSupervisorioMedicaoAnormalidade(Constantes
																.ANORMALIDADE_SUPERVISORIO_POSSIVEL_VIRADA_SEM_CORRETOR_VAZAO);
										supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

									}

								}
							}
						}
					} else {

						// se houve substituição de corretor de vazão
						consumoSemCorrecaoFatorPTZ = (BigDecimal) retornoSubtituicaoCorretorVazao.get(Constantes.CONSUMO_SEM_CORRECAO);
						consumoComCorrecaoFatorPTZ = (BigDecimal) retornoSubtituicaoCorretorVazao.get(Constantes.CONSUMO_COM_CORRECAO);
					}

					// incrementa os acumuladores
					somatorioConsumo = somatorioConsumo.add(consumoSemCorrecaoFatorPTZ);
					somatorioConsumoCorrigido = somatorioConsumoCorrigido.add(consumoComCorrecaoFatorPTZ);

					// verifica se a hora do dia está fracionada
					if (!possuiHoraFracionada) {
						possuiHoraFracionada = this.verificarHoraFracionada(supervisorioMedicaoHoraria,
										mapaAnormalidadeSupervisorioMedicaoDiaria, codigoSupervisorio, listaDataAnormalidade);

						if (possuiHoraFracionada) {
							//Possivel Implementação Futura.
						}
					}

				}

				// obtém primeira hora do dia de medicao
				if (supervisorioMedicaoHoraria.getDataRealizacaoLeitura().compareTo(dataInicialAux) >= CONSTANTE_NUMERO_ZERO
								&& supervisorioMedicaoHoraria.getDataRealizacaoLeitura().compareTo(dataFinalAux) < CONSTANTE_NUMERO_ZERO) {
					listaSupervisorioMedicaoHorariaPorDia.add(supervisorioMedicaoHoraria);
					listaDataSupervisorioMedicaoHoraria.add(supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
				}

				// obtém primeira hora do dia de medicao
				if (!isPrimeiraHoraDoDiaMedicao) {
					dataPrimeiraMedicao = supervisorioMedicaoHoraria.getDataRealizacaoLeitura();
					isPrimeiraHoraDoDiaMedicao = Boolean.TRUE;
				}

				supervisorioMedicaoHorariaAnterior = supervisorioMedicaoHoraria;
				tamanho++;
				index++;
				posicao++;

				// se for o último registro da lista ou se a data que estiver processando for a última hora do dia consolida o dia
				if ((listaSupervisorioMedicaoHoraria.size() == tamanho || listaSupervisorioMedicaoHoraria.get(index)
								.getDataRealizacaoLeitura().compareTo(dataFinalAux) > CONSTANTE_NUMERO_ZERO)
								&& quantidadeRegistros.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {



					Date dataRealizacaoLeitura = null;
					if ((listaSupervisorioMedicaoHoraria.size() > index && listaSupervisorioMedicaoHoraria.get(index) != null)
									&&( listaSupervisorioMedicaoHoraria.get(index).getDataRealizacaoLeitura() != null
									&& listaSupervisorioMedicaoHoraria.get(index).getDataRealizacaoLeitura().
									compareTo(dataFinalAux)<=CONSTANTE_NUMERO_ZERO)) {
						dataRealizacaoLeitura = listaSupervisorioMedicaoHoraria.get(index).getDataRealizacaoLeitura();
					} else {
						dataRealizacaoLeitura = dataFinalAux;
					}
					this.consolidarDadosSupervisorioMedicaoHoraria(codigoSupervisorio, supervisorioMedicaoHoraria, ultimaDataHoraDoDia,
									dataAtual, listaSupervisorioMedicaoHorariaPorDia, mapaAnormalidade, somatorioConsumo,
									somatorioConsumoCorrigido, somatorioPressao, somatorioTemperatura, quantidadeRegistros);

					if (Boolean.TRUE.equals(indicadorMedidor)) {
						this.validarAnormalidadeSupervisorioMedicaoHoraria(listaDataSupervisorioMedicaoHoraria, listaDatasAux,
										mapaAnormalidadeSupervisorioMedicaoDiaria, dataPrimeiraMedicao, null, codigoSupervisorio,
										listaDataAnormalidade, medidor);
					} else {
						this.validarAnormalidadeSupervisorioMedicaoHoraria(listaDataSupervisorioMedicaoHoraria, listaDatasAux,
										mapaAnormalidadeSupervisorioMedicaoDiaria, dataPrimeiraMedicao, pontoConsumo, codigoSupervisorio,
										listaDataAnormalidade, null);
					}

					listaDatasAux = Util.incrementarDataComHoraEQuantidadeDias(dataRealizacaoLeitura, numeroHoraInicial,
									Constantes.NUMERO_DIAS_INCREMENTAR_DATA);

					dataInicialAux = listaDatasAux.get(Constantes.DATA_INICIAL);
					dataFinalAux = listaDatasAux.get(Constantes.DATA_FINAL);

					// inicializar as variáveis
					somatorioConsumo = BigDecimal.ZERO;
					somatorioConsumoCorrigido = BigDecimal.ZERO;
					somatorioPressao = BigDecimal.ZERO;
					somatorioTemperatura = BigDecimal.ZERO;

					ultimaDataHoraDoDia = DataUtil.gerarDataHmsZerados(dataInicialAux);
					isSubtituicaoCorretorVazao = Boolean.FALSE;
					retornoSubtituicaoCorretorVazao = null;
					listaSupervisorioMedicaoHorariaPorDia = new LinkedHashSet<>();
					listaDataSupervisorioMedicaoHoraria = new ArrayList<>();

					if (listaSupervisorioMedicaoHorariaPorDiaAux != null && !listaSupervisorioMedicaoHorariaPorDiaAux.isEmpty()) {

						listaSupervisorioMedicaoHorariaPorDia.addAll(listaSupervisorioMedicaoHorariaPorDiaAux);
						listaDataSupervisorioMedicaoHoraria.addAll(listaDataSupervisorioMedicaoHorariaAux);

						listaSupervisorioMedicaoHorariaPorDiaAux = new LinkedHashSet<>();
						listaDataSupervisorioMedicaoHorariaAux = new ArrayList<>();

					}

					if (supervisorioMedicaoHoraria.getDataRealizacaoLeitura().compareTo(dataInicialAux) >= CONSTANTE_NUMERO_ZERO) {
						listaSupervisorioMedicaoHorariaPorDia.add(supervisorioMedicaoHoraria);
						listaDataSupervisorioMedicaoHoraria.add(supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
					}
					quantidadeRegistros = BigDecimal.ZERO;
					possuiHoraFracionada = Boolean.FALSE;
					isPrimeiraHoraDoDiaMedicao = Boolean.FALSE;
					listaData = new ArrayList<>();
				}

				if ((supervisorioMedicaoHoraria.getDataRealizacaoLeitura().compareTo(dataFinalAux) == CONSTANTE_NUMERO_ZERO)
								&& quantidadeRegistros.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {

					if (!listaSupervisorioMedicaoHorariaPorDiaAux.contains(supervisorioMedicaoHoraria)) {

						listaSupervisorioMedicaoHorariaPorDiaAux.add(supervisorioMedicaoHoraria);
					}

					if (!listaDataSupervisorioMedicaoHorariaAux.contains(supervisorioMedicaoHoraria.getDataRealizacaoLeitura())) {

						listaDataSupervisorioMedicaoHorariaAux.add(supervisorioMedicaoHoraria.getDataRealizacaoLeitura());
					}

				}

			}

		}
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		logProcessamento.append("processarDadosMedicaoHoraria FIM: ").append(d.format(new Date())).append(Constantes.PULA_LINHA);

		return mapaAnormalidadeSupervisorioMedicaoDiaria;
	}

	private void verificaHoraInicialConsumo(Medidor medidor, ControladorMedidor controladorMedidor,
					ControladorContrato controladorContrato, SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria, List<Object[]> listaDataAnormalidade)
					throws GGASException {

		Collection<PontoConsumo> listaPontoConsumoMedidor = obtemListaPontoConsumoMedidor(controladorMedidor, medidor);
		Integer horaInicial = null;
		for (PontoConsumo pontoConsumoAux : listaPontoConsumoMedidor) {

			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoFaturavelPorPontoConsumo(
							pontoConsumoAux.getChavePrimaria(), Boolean.TRUE);
			Map<String, Object> mapaContratoAtivo = controladorContrato.validarContratoAtivoPorDataLeitura(
							supervisorioMedicaoHoraria.getDataRealizacaoLeitura(), contratoPontoConsumo, pontoConsumoAux);

			if (mapaContratoAtivo != null && !mapaContratoAtivo.isEmpty()) {

				Boolean possuiContrato = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);

				// Pega a hora inicial do contrato ativo se tiver
				if (possuiContrato && mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO) != null) {
					if (horaInicial == null) {
						horaInicial = (Integer) mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO);
					} else {
						if (!horaInicial.equals((Integer) mapaContratoAtivo.get(Constantes.HORA_INICIAL_CONTRATO))) {
							SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
											.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_PC_HORA_INICIAL_LEITURA_DIFERENTES);
							supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

							Object[] arrayAnormalidade = new Object[CONSTANTE_NUMERO_DOIS];
							arrayAnormalidade[CONSTANTE_NUMERO_ZERO] = supervisorioMedicaoHoraria.getDataRealizacaoLeitura();
							arrayAnormalidade[CONSTANTE_NUMERO_UM] = supervisorioMedicaoAnormalidade;

							listaDataAnormalidade.add(arrayAnormalidade);

							mapaAnormalidadeSupervisorioMedicaoDiaria.put(supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio(),
											listaDataAnormalidade);
						}
					}
				}
			}
		}
	}

	/**
	 * Consolidar dados supervisorio medicao horaria.
	 *
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigo ponto consumo supervisorio
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param ultimaDataHoraDoDia
	 *            the ultima data hora do dia
	 * @param dataAtual
	 *            the data atual
	 * @param listaSupervisorioMedicaoHorariaPorDia
	 *            the lista supervisorio medicao horaria por dia
	 * @param mapaAnormalidade
	 *            the mapa anormalidade
	 * @param somatorioConsumo
	 *            the somatorio consumo
	 * @param somatorioConsumoCorrigido
	 *            the somatorio consumo corrigido
	 * @param somatorioPressao
	 *            the somatorio pressao
	 * @param somatorioTemperatura
	 *            the somatorio temperatura
	 * @param quantidadeRegistros
	 *            the quantidade registros
	 * @throws GGASException the ggas exception
	 */
	public void consolidarDadosSupervisorioMedicaoHoraria(String codigoPontoConsumoSupervisorio,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria, Date ultimaDataHoraDoDia, Date dataAtual,
					Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHorariaPorDia, Map<Long, Integer> mapaAnormalidade,
					BigDecimal somatorioConsumo, BigDecimal somatorioConsumoCorrigido, BigDecimal somatorioPressao,
					BigDecimal somatorioTemperatura, BigDecimal quantidadeRegistros) throws GGASException {

		this.validarMapaAnormalidades(mapaAnormalidade, supervisorioMedicaoHoraria.getSupervisorioMedicaoAnormalidade());

		PontoConsumo pontoConsumo = new PontoConsumoImpl();
		pontoConsumo.setCodigoPontoConsumoSupervisorio(codigoPontoConsumoSupervisorio);
		this.finalizarGeracaoMedicaoHoraria(converterCollectionEmList(listaSupervisorioMedicaoHorariaPorDia), pontoConsumo);

		BigDecimal leituraSemCorrecaoFatorPTZ = supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ();
		BigDecimal leituraComCorrecaoFatorPTZ = supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ();

		// obter registro de medição diária caso ele já exista

		// Monta filtro para a consulta
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(DATA_REALIZACAO_LEITURA, ultimaDataHoraDoDia);
		filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
		filtro.put(HABILITADO, Boolean.TRUE);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String paramStatusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		filtro.put(STATUS_AUTORIZADO, Long.valueOf(paramStatusAutorizado));

		List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariasAux = (List<SupervisorioMedicaoDiaria>) this
						.consultarSupervisorioMedicaoDiaria(filtro, null, "supervisorioMedicaoAnormalidade");

		SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = null;

		if (listaSupervisorioMedicaoDiariasAux != null && !listaSupervisorioMedicaoDiariasAux.isEmpty()) {

			supervisorioMedicaoDiaria = Util.primeiroElemento(listaSupervisorioMedicaoDiariasAux);
		}

		// se já existir um registro do supervisório medição diária de acordo com o dia ultimaDataHoraDoDia ele será alterado
		if (supervisorioMedicaoDiaria != null) {

			this.preencherDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, codigoPontoConsumoSupervisorio, ultimaDataHoraDoDia,
							leituraSemCorrecaoFatorPTZ, leituraComCorrecaoFatorPTZ, somatorioConsumo, somatorioConsumoCorrigido,
							somatorioPressao, somatorioTemperatura, quantidadeRegistros, dataAtual);

			supervisorioMedicaoDiaria = this.preencherEscalaSupervisorio(supervisorioMedicaoDiaria);
			super.atualizarEmBatch(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);

		} else {
			// caso não exista um registro do supervisório medição diária de acordo com o dia ultimaDataHoraDoDia ele será inserido
			supervisorioMedicaoDiaria = (SupervisorioMedicaoDiaria) this.criarSupervisorioMedicaoDiaria();

			this.preencherDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, codigoPontoConsumoSupervisorio, ultimaDataHoraDoDia,
							leituraSemCorrecaoFatorPTZ, leituraComCorrecaoFatorPTZ, somatorioConsumo, somatorioConsumoCorrigido,
							somatorioPressao, somatorioTemperatura, quantidadeRegistros, dataAtual);

			supervisorioMedicaoDiaria = this.preencherEscalaSupervisorio(supervisorioMedicaoDiaria);
			super.inserirEmBatch(supervisorioMedicaoDiaria, Boolean.TRUE);

		}

		// Adiciona comentario para medicao calculada automaticamente pelo sistema quando nao houver registros minimos para o calculo da media
		inserirSupervisorioMedicaoComentarioCalculoMediaDiaria(supervisorioMedicaoDiaria);

		// Atualizar as medições horárias como consolidadas
		Long[] chaves = new Long[listaSupervisorioMedicaoHorariaPorDia.size()];
		int i=CONSTANTE_NUMERO_ZERO;
		for (SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAux : listaSupervisorioMedicaoHorariaPorDia) {
			chaves[i] = supervisorioMedicaoHorariaAux.getChavePrimaria();
			i++;
		}
		if(chaves.length > CONSTANTE_NUMERO_ZERO){
			this.atualizarSupervisorioMedicaoHorariasPorDia(supervisorioMedicaoDiaria, chaves, dataAtual);
		}
	}

	private List<SupervisorioMedicaoHoraria> converterCollectionEmList(Collection<SupervisorioMedicaoHoraria> colecao) {
		List<SupervisorioMedicaoHoraria> lista = new ArrayList<SupervisorioMedicaoHoraria>();

		for (SupervisorioMedicaoHoraria medicao : colecao) {
			lista.add(medicao);
		}

		return lista;
	}

	private void inserirSupervisorioMedicaoComentarioCalculoMediaDiaria(
			SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		if(supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null
				&& supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade().getChavePrimaria()
				== ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE) {

			SupervisorioMedicaoComentario supervisorioMedicaoComentario = (SupervisorioMedicaoComentario) this.criarSupervisorioMedicaoComentario();

			ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

			String quantidadeMinimaRegistrosMedicoesHora = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA);
			String quantidadeMedicaoDiariaParaMedia = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA);

			supervisorioMedicaoComentario.setSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
			supervisorioMedicaoComentario.setDescricao(MensagemUtil.obterMensagem(SUPERVISORIO_MEDIA_CALCULADA_COMENTARIO,
					quantidadeMinimaRegistrosMedicoesHora, quantidadeMedicaoDiariaParaMedia));
			supervisorioMedicaoComentario.setSequenciaComentario(Integer.valueOf(CONSTANTE_NUMERO_UM));

			ControladorUsuario controladorUsuario = (ControladorUsuario) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);
			Usuario usuario = controladorUsuario.buscar(Constantes.USUARIO_AUDITORIA);
			supervisorioMedicaoComentario.setUsuario(usuario);

			this.inserirSupervisorioMedicaoDiariaComentario(supervisorioMedicaoComentario, supervisorioMedicaoDiaria);
		}
	}

	private void atualizarSupervisorioMedicaoHorariasPorDia(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					Long[] chavesSupervisorioMedicaoHoraria, Date dataAtual) {

		StringBuilder hql = new StringBuilder();

		hql.append(" UPDATE ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" sumh ");
		hql.append(" SET ");
		hql.append(" sumh.indicadorConsolidada = true, ");
		hql.append(" sumh.dataRegistroLeitura = :dataAtual, ");
		hql.append(" sumh.supervisorioMedicaoDiaria = :supervisorioMedicaoDiaria ");
		hql.append(" WHERE ");
		hql.append(" sumh.chavePrimaria in (:chavesSupervisorioMedicaoHoraria) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setEntity("supervisorioMedicaoDiaria", supervisorioMedicaoDiaria);
		query.setParameterList("chavesSupervisorioMedicaoHoraria", chavesSupervisorioMedicaoHoraria);
		query.setDate("dataAtual", dataAtual);

		query.executeUpdate();
	}

	/**
	 * Verificar hora fracionada.
	 *
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param mapaAnormalidadeSupervisorioMedicaoDiaria
	 *            the mapa anormalidade supervisorio medicao diaria
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigo ponto consumo supervisorio
	 * @param listaDataAnormalidade
	 *            the lista data anormalidade
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Boolean verificarHoraFracionada(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria, String codigoPontoConsumoSupervisorio,
					List<Object[]> listaDataAnormalidade) throws NegocioException {

		Boolean possuiHoraFracionada = Boolean.FALSE;

		// Caso exista algum registro com hora fracionada
		if (Util.verificarHoraFracionada(supervisorioMedicaoHoraria.getDataRealizacaoLeitura())) {

			possuiHoraFracionada = Boolean.TRUE;

			// Gera anormalidade
			SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
							.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_HORA_FRACIONADA);

			// seta o supervisorioMedicaoAnormalidade ao supervisorioMedicaoHoraria
			supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

			Object[] arrayAnormalidade = new Object[CONSTANTE_NUMERO_DOIS];
			arrayAnormalidade[CONSTANTE_NUMERO_ZERO] = supervisorioMedicaoHoraria.getDataRealizacaoLeitura();
			arrayAnormalidade[CONSTANTE_NUMERO_UM] = supervisorioMedicaoAnormalidade;

			listaDataAnormalidade.add(arrayAnormalidade);
			mapaAnormalidadeSupervisorioMedicaoDiaria.put(codigoPontoConsumoSupervisorio, listaDataAnormalidade);

		}

		return possuiHoraFracionada;
	}

	/**
	 * Preencher dados supervisorio medicao diaria.
	 *
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigo ponto consumo supervisorio
	 * @param ultimaDataHoraDoDia
	 *            the ultima data hora do dia
	 * @param leituraSemCorrecaoFatorPTZ
	 *            the leitura sem correcao fator ptz
	 * @param leituraComCorrecaoFatorPTZ
	 *            the leitura com correcao fator ptz
	 * @param somatorioConsumo
	 *            the somatorio consumo
	 * @param somatorioConsumoCorrigido
	 *            the somatorio consumo corrigido
	 * @param somatorioPressao
	 *            the somatorio pressao
	 * @param somatorioTemperatura
	 *            the somatorio temperatura
	 * @param quantidadeRegistros
	 *            the quantidade registros
	 * @param dataAtual
	 *            the data atual
	 * @return the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public SupervisorioMedicaoDiaria preencherDadosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					String codigoPontoConsumoSupervisorio, Date ultimaDataHoraDoDia, BigDecimal leituraSemCorrecaoFatorPTZ,
					BigDecimal leituraComCorrecaoFatorPTZ, BigDecimal somatorioConsumo, BigDecimal somatorioConsumoCorrigido,
					BigDecimal somatorioPressao, BigDecimal somatorioTemperatura, BigDecimal quantidadeRegistros, Date dataAtual)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String escalaParametro = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);
		int escala = Integer.parseInt(escalaParametro);

		String quantidadeMinimaRegistrosMedicoesHora = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MINIMA_REGISTROS_MEDICOES_HORA);
		int quantidadeMinimaRegistros = Integer.parseInt(quantidadeMinimaRegistrosMedicoesHora);


		supervisorioMedicaoDiaria.setCodigoPontoConsumoSupervisorio(codigoPontoConsumoSupervisorio);
		supervisorioMedicaoDiaria.setDataRealizacaoLeitura(ultimaDataHoraDoDia);
		supervisorioMedicaoDiaria.setLeituraSemCorrecaoFatorPTZ(leituraSemCorrecaoFatorPTZ);
		supervisorioMedicaoDiaria.setLeituraComCorrecaoFatorPTZ(leituraComCorrecaoFatorPTZ);

		if((quantidadeRegistros.compareTo(new BigDecimal(quantidadeMinimaRegistros))) > CONSTANTE_NUMERO_ZERO) {
			supervisorioMedicaoDiaria.setConsumoSemCorrecaoFatorPTZ(somatorioConsumo);
			supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(somatorioConsumoCorrigido);
			supervisorioMedicaoDiaria.setPressao(somatorioPressao.divide(quantidadeRegistros, escala, BigDecimal.ROUND_HALF_UP));
			supervisorioMedicaoDiaria.setTemperatura(somatorioTemperatura.divide(quantidadeRegistros, escala, BigDecimal.ROUND_HALF_UP));

		} else {

			String quantidadeMedicaoDiariaParaMedia = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA);
			int quantidadeMedicaoDiariaMedia = Integer.parseInt(quantidadeMedicaoDiariaParaMedia);

			Date dataRealizacaoLeitura = supervisorioMedicaoDiaria.getDataRealizacaoLeitura();
			Date dataInicio = Util.decrementarDataComQuantidadeDias(dataRealizacaoLeitura, quantidadeMedicaoDiariaMedia);
			Date dataFinal = Util.decrementarDataComQuantidadeDias(dataRealizacaoLeitura, CONSTANTE_NUMERO_UM);

			Map<String, Object> filtro = new HashMap<>();
			filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, dataInicio);
			filtro.put(DATA_FINAL_REALIZACAO_LEITURA, dataFinal);
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			filtro.put(INDICADOR_CONSOLIDADA, Boolean.TRUE);
			filtro.put(HABILITADO, Boolean.TRUE);

			Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = 
					this.consultarSupervisorioMedicaoDiaria(filtro, null,StringUtils.trim(null));

			somatorioConsumo = new GenericUtil<SupervisorioMedicaoDiaria>().calcularMediaLista(listaSupervisorioMedicaoDiaria,
					CONSUMO_SEM_CORRECAO_FATOR_PTZ, escala);
			somatorioConsumoCorrigido = new GenericUtil<SupervisorioMedicaoDiaria>().calcularMediaLista(listaSupervisorioMedicaoDiaria,
					CONSUMO_COM_CORRECAO_FATOR_PTZ, escala);
			BigDecimal mediaPressao = new GenericUtil<SupervisorioMedicaoDiaria>().calcularMediaLista(listaSupervisorioMedicaoDiaria,
					PRESSAO, escala);
			BigDecimal mediaTemperatura = new GenericUtil<SupervisorioMedicaoDiaria>().calcularMediaLista(listaSupervisorioMedicaoDiaria,
					TEMPERATURA, escala);


			supervisorioMedicaoDiaria.setConsumoSemCorrecaoFatorPTZ(somatorioConsumo);
			supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(somatorioConsumoCorrigido);
			supervisorioMedicaoDiaria.setPressao(mediaPressao);
			supervisorioMedicaoDiaria.setTemperatura(mediaTemperatura);
			supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(
					this.obterSupervisorioMedicaoAnormalidade(ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE));
		}

		if (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ().compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {
			supervisorioMedicaoDiaria.setFatorPTZ(supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ().divide(
					supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ(), escala, BigDecimal.ROUND_HALF_UP));
		} else {
			supervisorioMedicaoDiaria.setFatorPTZ(BigDecimal.ONE);
		}


		supervisorioMedicaoDiaria.setIndicadorIntegrado(Boolean.FALSE);
		supervisorioMedicaoDiaria.setIndicadorProcessado(Boolean.FALSE);
		supervisorioMedicaoDiaria.setIndicadorConsolidada(Boolean.FALSE);
		supervisorioMedicaoDiaria.setDataRegistroLeitura(dataAtual);

		return supervisorioMedicaoDiaria;
	}

	/**
	 * Tratar subtituicao corretor vazao.
	 *
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param listarHistoricoCorretorVazao
	 *            the listar historico corretor vazao
	 * @param codigoOperacaoMedidorRetirada
	 *            the codigo operacao medidor retirada
	 * @param codigoOperacaoMedidorInstalacao
	 *            the codigo operacao medidor instalacao
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param mapaAnormalidade
	 *            the mapa anormalidade
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Map<String, Object> tratarSubtituicaoCorretorVazao(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					List<VazaoCorretorHistoricoOperacao> listarHistoricoCorretorVazao, Long codigoOperacaoMedidorRetirada,
					Long codigoOperacaoMedidorInstalacao, Date dataInicial, Date dataFinal, Map<Long, Integer> mapaAnormalidade)
					throws NegocioException {

		Map<String, Object> retornoSubtituicaoCorretorVazao = new LinkedHashMap<>();
		BigDecimal consumoSemCorrecao = BigDecimal.ZERO;
		BigDecimal consumoComCorrecao;
		Boolean isSubstituicaoCorretorVazao = Boolean.FALSE;

		// acumulador
		BigDecimal somatorioConsumoAnterior = BigDecimal.ZERO;
		BigDecimal somatorioConsumoAtual = BigDecimal.ZERO;

		for (VazaoCorretorHistoricoOperacao vazaoCorretorHistoricoOperacao : listarHistoricoCorretorVazao) {

			if (!(vazaoCorretorHistoricoOperacao.getDataRealizada().compareTo(dataInicial) < CONSTANTE_NUMERO_ZERO)
							&& !(vazaoCorretorHistoricoOperacao.getDataRealizada().compareTo(dataFinal) > CONSTANTE_NUMERO_ZERO)) {

				BigDecimal leituraCorretorVazao = null;
				BigDecimal leituraSupervisorio = null;

				BigDecimal potencia = BigDecimal
								.valueOf(Math.pow(CONSTANTE_NUMERO_DEZ, vazaoCorretorHistoricoOperacao.getVazaoCorretor().getNumeroDigitos()));

				if (vazaoCorretorHistoricoOperacao.getLeitura() != null) {

					leituraCorretorVazao = vazaoCorretorHistoricoOperacao.getLeitura();
				}

				if (supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ() != null) {

					leituraSupervisorio = supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ();
				}

				if (leituraCorretorVazao != null && leituraSupervisorio != null) {

					isSubstituicaoCorretorVazao = Boolean.TRUE;

					// Gera anormalidade
					SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
									.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_SUBSTITUICAO_CORRETOR_VAZAO);

					supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

					// retirada corretor de vazão
					if (vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getChavePrimaria() == codigoOperacaoMedidorRetirada) {

						// se maior ou igual
						if (leituraCorretorVazao.compareTo(leituraSupervisorio) >= CONSTANTE_NUMERO_ZERO) {
							somatorioConsumoAnterior = somatorioConsumoAnterior.add(leituraCorretorVazao.subtract(leituraSupervisorio));
						} else {

							somatorioConsumoAnterior = somatorioConsumoAnterior.add(potencia.subtract(leituraCorretorVazao
											.add(leituraSupervisorio)));
						}

						// instalação corretor de vazão
					} else if (vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getChavePrimaria() == codigoOperacaoMedidorInstalacao) {

						// se maior ou igual
						if (leituraSupervisorio.compareTo(leituraCorretorVazao) >= CONSTANTE_NUMERO_ZERO) {
							somatorioConsumoAtual = somatorioConsumoAtual.add(leituraSupervisorio.subtract(leituraCorretorVazao));
						} else {

							somatorioConsumoAtual = somatorioConsumoAtual.add(potencia.subtract(leituraCorretorVazao
											.add(leituraSupervisorio)));
						}

					}
				}

			}
		}

		consumoComCorrecao = somatorioConsumoAnterior.add(somatorioConsumoAtual);

		// verificar dps
		retornoSubtituicaoCorretorVazao.put(Constantes.CONSUMO_SEM_CORRECAO, consumoSemCorrecao);
		retornoSubtituicaoCorretorVazao.put(Constantes.CONSUMO_COM_CORRECAO, consumoComCorrecao);
		retornoSubtituicaoCorretorVazao.put(Constantes.SUBSTITUICAO_CORRETOR_VAZAO, isSubstituicaoCorretorVazao);

		return retornoSubtituicaoCorretorVazao;
	}

	/**
	 * Tratar virada mostrador corretor vazao.
	 *
	 * @param leituraAtual
	 *            the leitura atual
	 * @param leituraAnterior
	 *            the leitura anterior
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @param tipoIntegracao
	 *            the tipo integracao
	 * @param mapaAnormalidade
	 *            the mapa anormalidade
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Map<String, Object> tratarViradaMostradorCorretorVazao(BigDecimal leituraAtual, BigDecimal leituraAnterior,
					VazaoCorretor vazaoCorretor, String tipoIntegracao, Map<Long, Integer> mapaAnormalidade) throws NegocioException {

		Map<String, Object> retornoViradaCorretorVazao = new LinkedHashMap<>();
		BigDecimal potencia = BigDecimal.valueOf(Math.pow(CONSTANTE_NUMERO_DEZ, vazaoCorretor.getNumeroDigitos()));

		// Gera anormalidade
		SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
						.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_VIRADA_CORRETOR_VAZAO);

		retornoViradaCorretorVazao.put(Constantes.SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA, supervisorioMedicaoAnormalidade);

		String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

		String tipoIntegracaoDiaria = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA);

		// se a medição for horária calcula o consumo e o consumo corrigido
		if (tipoIntegracao.equals(tipoIntegracaoHoraria)) {
			BigDecimal consumo = leituraAtual.add(potencia).subtract(leituraAnterior);

			retornoViradaCorretorVazao.put(Constantes.CONSUMO, consumo);

		} else if (tipoIntegracao.equals(tipoIntegracaoDiaria)) {

			// se a medição for diária calcula a leitura e o leitura corrigida
			BigDecimal leitura = leituraAtual.subtract(potencia);

			retornoViradaCorretorVazao.put(Constantes.LEITURA, leitura);

		}

		return retornoViradaCorretorVazao;
	}

	/**
	 * Ordenar listas por codigo ponto consumo supervisorio e data realizacao leitura.
	 *
	 * @param listaSMHConsolidada
	 *            the lista smh consolidada
	 * @param listaSMHNaoConsolidada
	 *            the lista smh nao consolidada
	 * @return the list
	 */
	public List<SupervisorioMedicaoHoraria> ordenarListasPorCodigoPontoConsumoSupervisorioEDataRealizacaoLeitura(
					List<SupervisorioMedicaoHoraria> listaSMHConsolidada, List<SupervisorioMedicaoHoraria> listaSMHNaoConsolidada) {

		List<SupervisorioMedicaoHoraria> listaSMHConsolidadaENaoConsolidada = new ArrayList<>();

		// reune os elementos em uma única lista
		listaSMHConsolidadaENaoConsolidada.addAll(listaSMHNaoConsolidada);
		listaSMHConsolidadaENaoConsolidada.addAll(listaSMHConsolidada);

		// ordena as listas por codigoPontoConsumoSupervisorio e por dataRealizacaoLeitura
		Collections.sort(listaSMHConsolidadaENaoConsolidada, new Comparator<SupervisorioMedicaoHoraria>(){

			@Override
			public int compare(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria1,
							SupervisorioMedicaoHoraria supervisorioMedicaoHoraria2) {

				// compara os registro por codigoPontoConsumoSupervisorio
				int retorno = supervisorioMedicaoHoraria1.getCodigoPontoConsumoSupervisorio().compareTo(
								supervisorioMedicaoHoraria2.getCodigoPontoConsumoSupervisorio());

				// se os codigoPontoConsumoSupervisorio forem iguais compara os registro por dataRealizacaoLeitura
				if (retorno == CONSTANTE_NUMERO_ZERO) {
					retorno = supervisorioMedicaoHoraria1.getDataRealizacaoLeitura().compareTo(
									supervisorioMedicaoHoraria2.getDataRealizacaoLeitura());
				}
				return retorno;
			}
		});
		return listaSMHConsolidadaENaoConsolidada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#obterAnoMesReferenciaCiclo(java.util.Map, java.util.Date,
	 * br.com.ggas.cadastro.imovel.PontoConsumo, br.com.ggas.medicao.rota.Periodicidade, java.lang.Boolean)
	 */
	@Override
	public Map<String, Object> obterAnoMesReferenciaCiclo(Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura,
					Date dataRealizacaoLeitura, PontoConsumo pontoConsumo, Periodicidade periodicidade,
					Boolean calcularAnoMesReferenciaCiclo) throws GGASException {

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getBeanPorID(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		Map<String, Object> mapaAnoMesReferenciaCiclo = new LinkedHashMap<>();

		List<Map<String, Object>> listaMapaReferenciaLeitura = null;
		Rota rotaPC = pontoConsumo.getRota();
		if (rotaPC != null && rotaPC.getGrupoFaturamento() != null) {
			listaMapaReferenciaLeitura = mapaComListaReferenciaLeitura.get(rotaPC.getGrupoFaturamento().getChavePrimaria());
		}
		Integer anoMesFaturamento = null;
		Integer numeroCiclo = null;
		Date dataFim = null;

		if (listaMapaReferenciaLeitura != null) {
			for (Map<String, Object> mapa : listaMapaReferenciaLeitura) {

				dataFim = (Date) mapa.get("dataFim");
				anoMesFaturamento = (Integer) mapa.get("anoMesFaturamento");
				numeroCiclo = (Integer) mapa.get("numeroCiclo");

				if (dataFim.compareTo(dataRealizacaoLeitura) > CONSTANTE_NUMERO_ZERO) {
					mapaAnoMesReferenciaCiclo.put("anoMesFaturamento", anoMesFaturamento);
					mapaAnoMesReferenciaCiclo.put("numeroCiclo", numeroCiclo);
					mapaAnoMesReferenciaCiclo.put("dataFim", dataFim);

					Rota rota = rotaPC;

					DateTime dataPartidaDateTime = new DateTime(dataFim);

					Map<String, Object> mapaDatas = controladorCronogramaFaturamento.obterDatasSugestaoCronograma(dataPartidaDateTime,
									periodicidade, null, null, null, Boolean.TRUE, rota.getTipoLeitura(), numeroCiclo, null);

					mapaAnoMesReferenciaCiclo.put("dataFimComPeriodicidade", mapaDatas.get("dataPrevista"));

					break;
				}
			}

			GrupoFaturamento grupoFaturamento = rotaPC.getGrupoFaturamento();

			if (mapaAnoMesReferenciaCiclo.isEmpty() && (calcularAnoMesReferenciaCiclo != null && calcularAnoMesReferenciaCiclo)) {

				listaMapaReferenciaLeitura.addAll(this.calcularAnoMesReferenciaCiclo(anoMesFaturamento, numeroCiclo, periodicidade,
								dataFim, dataRealizacaoLeitura, grupoFaturamento));

				Map<String, Object> mapa = listaMapaReferenciaLeitura.get(listaMapaReferenciaLeitura.size() - CONSTANTE_NUMERO_UM);

				mapaAnoMesReferenciaCiclo.put("anoMesFaturamento", mapa.get("anoMesFaturamento"));
				mapaAnoMesReferenciaCiclo.put("numeroCiclo", mapa.get("numeroCiclo"));
				mapaAnoMesReferenciaCiclo.put("dataFimComPeriodicidade", mapa.get("dataFim"));

			}

			if (calcularAnoMesReferenciaCiclo != null && !calcularAnoMesReferenciaCiclo) {

				DateTime dataFimLocal = new DateTime(dataFim);

				Map<String, Object> mapaDatas = controladorCronogramaFaturamento.obterDatasSugestaoCronograma(dataFimLocal, periodicidade,
								null, null, null, Boolean.TRUE, grupoFaturamento.getTipoLeitura(), numeroCiclo, null);

				dataFim = (Date) mapaDatas.get("dataPrevista");
			}

			mapaAnoMesReferenciaCiclo.put("dataFim", dataFim);
		}
		return mapaAnoMesReferenciaCiclo;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#tratarMedicoesDiarias(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Date, java.util.Date, java.lang.StringBuilder, java.util.Map,
	 * br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO)
	 */
	@Override
	public void tratarMedicoesDiarias(PontoConsumo pontoConsumo, Date dataInicialRealizacaoLeitura, Date dataFinalRealizacaoLeitura,
					StringBuilder logProcessamento, Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria,
					ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO, Medidor medidor) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia().getControladorHistoricoConsumo();
		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Integer quantidadeSupervisorioMedicaoDiariaProcessadas = CONSTANTE_NUMERO_ZERO;
		Map<Long, Integer> mapaAnormalidade = null;
		PontoConsumo pontoConsumoLocal = null;
		Medidor medidorLocal = null;
		List<Date> listaData = null;
		StringBuilder pontosConsumoSemRota = new StringBuilder();

		String tipoIntegracao = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);

		if (resumoSupervisorioMedicaoVO.getMapaAnormalidade() != null && !resumoSupervisorioMedicaoVO.getMapaAnormalidade().isEmpty()) {

			mapaAnormalidade = resumoSupervisorioMedicaoVO.getMapaAnormalidade();
		} else {

			mapaAnormalidade = new LinkedHashMap<>();
		}

		logProcessamento.append(Constantes.PULA_LINHA).append("[")
				.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime())).append("] ")
				.append(Constantes.INICIANDO_TRATAMENTO_MEDICOES_DIARIAS + Constantes.PULA_LINHA + Constantes.PULA_LINHA);

		// Monta filtro para a consulta
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
		filtro.put(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);
		filtro.put(INDICADOR_PROCESSADO, Boolean.FALSE);
		filtro.put("naoConsolidadaOuPossuiAnormalidadeImpeditiva", Boolean.TRUE);

		filtro.put(HABILITADO, Boolean.TRUE);

		if (pontoConsumo != null) {

			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, pontoConsumo.getCodigoPontoConsumoSupervisorio());
		}
		if (medidor != null) {
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, medidor.getCodigoMedidorSupervisorio());
		}

		String paramStatusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		filtro.put(STATUS_AUTORIZADO, Long.valueOf(paramStatusAutorizado));

		// Monta a ordenação da consulta
		String ordenacao = " smd." + CODIGO_PONTO_CONSUMO_SUPERVISORIO + ", smd." + DATA_REALIZACAO_LEITURA;

		List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaNaoConsolidada = (List<SupervisorioMedicaoDiaria>) this
						.consultarSupervisorioMedicaoDiaria(filtro, ordenacao, "supervisorioMedicaoAnormalidade");

		List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = new ArrayList<>();

		if (listaSupervisorioMedicaoDiariaNaoConsolidada != null && !listaSupervisorioMedicaoDiariaNaoConsolidada.isEmpty()) {

			listaSupervisorioMedicaoDiaria.addAll(listaSupervisorioMedicaoDiariaNaoConsolidada);
		}

		if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {

			quantidadeSupervisorioMedicaoDiariaProcessadas = listaSupervisorioMedicaoDiaria.size();

			// retorna um mapa com uma lista de medições no supervisório de acordo com o código do ponto de consumo ou do medidor independente.
			Map<String, Collection<SupervisorioMedicaoDiaria>> mapaSupervisorioMedicaoDiaria = this
							.gerarListaDiaria(listaSupervisorioMedicaoDiaria);

			Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura = new LinkedHashMap<>();

			UnidadeConversao unidadeConversao = controladorUnidade.obterUnidadeConversaoDeCelsiusParaPadrao();

			Map<String, PontoConsumo> mapaPontoConsumo = null;
			Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumo = null;
			Map<Long, VazaoCorretor> mapaVazaoCorretor = null;
			if (!mapaSupervisorioMedicaoDiaria.isEmpty()) {

				Set<String> keys = mapaSupervisorioMedicaoDiaria.keySet();
				String[] codigosSupervisorio = keys.toArray(new String[keys.size()]);

				mapaPontoConsumo = controladorPontoConsumo.consultarPontoConsumoPorCodigoSupervisorio(codigosSupervisorio);
				mapaContratoPontoConsumo = controladorContrato.consultarContratoFaturavelPorCodigoSupervisorio(codigosSupervisorio);
				mapaVazaoCorretor = controladorMedidor.obterCorretoresDeVazaoPorCodigosDoSupervisorio(codigosSupervisorio);

			}

			for (Entry<String, Collection<SupervisorioMedicaoDiaria>> entry : mapaSupervisorioMedicaoDiaria.entrySet()) {

				String codigoSupervisorio = entry.getKey();
				Map<String, Object> retornoViradaCorretorVazao = null;
				SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAnterior = null;
				Boolean possuiContrato = Boolean.FALSE;
				listaData = new ArrayList<>();
				pontoConsumoLocal = null;
				Periodicidade periodicidade = null;
				filtro = new HashMap<>();

				List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaPorCodigoSupervisorio;
				listaSupervisorioMedicaoDiariaPorCodigoSupervisorio = (List<SupervisorioMedicaoDiaria>) mapaSupervisorioMedicaoDiaria
								.get(codigoSupervisorio);
				Boolean indicadorMedidor = false;
				Collection<PontoConsumo> listaPontoConsumoMedidor = new ArrayList<>();
				if (pontoConsumo == null) {
					if (mapaPontoConsumo != null && !mapaPontoConsumo.isEmpty()) {
						pontoConsumoLocal = mapaPontoConsumo.get(codigoSupervisorio);
					}
				} else {
					pontoConsumoLocal = pontoConsumo;
				}

				if (medidor == null) {
					filtro.put(CODIGO_MEDIDOR_SUPERVISORIO, codigoSupervisorio);
					Collection<Medidor> listaMedidor = controladorMedidor.consultarMedidor(filtro);
					if (!listaMedidor.isEmpty()) {
						medidorLocal = listaMedidor.iterator().next();
						indicadorMedidor = true;

					}
				} else {
					medidorLocal = medidor;
				}

				/*
				 * Apesar dos medidores independentes poderem ter mais de um ponto de consumo (indiretamente),
				 * todos os pontos de consumo devem ter o mesmo cronograma e rota
				 */
				if (Boolean.TRUE.equals(indicadorMedidor)) {
					/*
					 * Medidores independentes podem ter mais de um ponto de consumo indiretamente por
					 * fazer parte de mais de uma composicao virtual.
					 */
					listaPontoConsumoMedidor = obtemListaPontoConsumoMedidor(controladorMedidor, medidorLocal);
					if (listaPontoConsumoMedidor != null && !listaPontoConsumoMedidor.isEmpty()) {
						pontoConsumoLocal = listaPontoConsumoMedidor.iterator().next();
					}
				}

				if (pontoConsumoLocal != null) {
					Long chavePrimariaPontoConsumo = pontoConsumoLocal.getChavePrimaria();
					ContratoPontoConsumo contratoPontoConsumo = null;
					if (mapaContratoPontoConsumo != null && !mapaContratoPontoConsumo.isEmpty()) {
						contratoPontoConsumo = mapaContratoPontoConsumo.get(chavePrimariaPontoConsumo);
					}
					Map<String, Object> mapaContratoAtivo = this.preencherMapaComListaReferenciaLeitura(pontoConsumoLocal,
									contratoPontoConsumo, Util.primeiroElemento(listaSupervisorioMedicaoDiariaPorCodigoSupervisorio).getDataRealizacaoLeitura(),
									mapaComListaReferenciaLeitura);
					possuiContrato = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);

					// obtém a hora inicial e a periodicidade do contrato se tiver
					if (possuiContrato != null && possuiContrato && mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO) != null) {
						// Recupera a periodicidade de faturamento do contrato ativo
						periodicidade = (Periodicidade) mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO);

					} else {
						// Recupera a periodicidade de faturamento do ramo de atividade se o PC não tiver contrato ativo
						if (pontoConsumoLocal.getRamoAtividade().getPeriodicidade() != null) {
							periodicidade = pontoConsumoLocal.getRamoAtividade().getPeriodicidade();

						} else if (pontoConsumoLocal.getSegmento().getPeriodicidade() != null) {

							// Recupera a periodicidade de faturamento do segmento se o ramo de atividade não for informado
							periodicidade = pontoConsumoLocal.getSegmento().getPeriodicidade();
						}
					}
				}
				
				if(pontoConsumoLocal != null) {
					removerMedicoesSemCronograma(listaSupervisorioMedicaoDiariaPorCodigoSupervisorio,
							mapaComListaReferenciaLeitura, pontoConsumoLocal.getRota().getGrupoFaturamento().getChavePrimaria());					
				}

				for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiariaPorCodigoSupervisorio) {

					supervisorioMedicaoDiaria.setIndicadorMedidor(indicadorMedidor);
					if (supervisorioMedicaoDiaria.getIndicadorConsolidada() == false
							|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
							|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
									.getChavePrimaria() == ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE) {

						Boolean possuiAnormalidadeImpeditiva = false;
						
						if(supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null ) {
							supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(null);
						}

						if (medidorLocal == null && pontoConsumoLocal == null) {
							// Gera anormalidade endereco remoto não encontrado.
							SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
											.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_ENDERECO_REMOTO_NAO_LOCALIZADO);
							supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
						} else {
							if (Boolean.TRUE.equals(indicadorMedidor)) {
								possuiAnormalidadeImpeditiva = verificaAnormalidadeImpeditivaMedidor(controladorMedidor,
										medidorLocal, supervisorioMedicaoDiaria, pontosConsumoSemRota);
							}

							if (!possuiAnormalidadeImpeditiva) {
								ContratoPontoConsumo contratoPontoConsumo = null;
								if (Boolean.TRUE.equals(indicadorMedidor)) {
									for (PontoConsumo pontoConsumoAux : listaPontoConsumoMedidor) {
										contratoPontoConsumo = controladorContrato.obterContratoFaturavelPorPontoConsumo(
														pontoConsumoAux.getChavePrimaria(), Boolean.TRUE);
										verificaAnormalidadesPontoConsumo(controladorMedidor, controladorContrato,
												pontoConsumoAux, mapaComListaReferenciaLeitura, periodicidade,
												supervisorioMedicaoDiaria, possuiContrato, contratoPontoConsumo,
												indicadorMedidor, medidorLocal);
									}
								} else {
									if (pontoConsumoLocal != null) {
										Long chavePontoConsumo = pontoConsumoLocal.getChavePrimaria();
										if(pontoConsumoLocal.getDadosResumoPontoConsumo().
														getContratoPontoConsumo() != null){
											contratoPontoConsumo = pontoConsumoLocal.
															getDadosResumoPontoConsumo().getContratoPontoConsumo();
										} else if (mapaContratoPontoConsumo != null && mapaContratoPontoConsumo.containsKey(chavePontoConsumo)){
											contratoPontoConsumo = mapaContratoPontoConsumo.get(chavePontoConsumo);
										} else {
											contratoPontoConsumo = controladorContrato.obterContratoFaturavelPorPontoConsumo(
															chavePontoConsumo, Boolean.TRUE);
										}
									}
									verificaAnormalidadesPontoConsumo(controladorMedidor, controladorContrato,
											pontoConsumoLocal, mapaComListaReferenciaLeitura, periodicidade,
											supervisorioMedicaoDiaria, possuiContrato, contratoPontoConsumo,
											indicadorMedidor, medidorLocal);
								}
							}
						}

						if (listaSupervisorioMedicaoDiariaNaoConsolidada.size() > CONSTANTE_NUMERO_UM
								&& (listaData.isEmpty() || !(listaData.contains(supervisorioMedicaoDiaria.getDataRealizacaoLeitura())))) {

							listaData.add(supervisorioMedicaoDiaria.getDataRealizacaoLeitura());
						}

						String escalaParametro = (String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);

						String codigoOperacaoMedidorInstalacao = controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_INSTALACAO);

						String temperaturaReferenciada = (String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA);

						BigDecimal temperaturaReferenciadaConvertida = Util.converterCampoStringParaValorBigDecimal("",
										temperaturaReferenciada, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

						String pressaoReferenciada = (String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PRESSAO_CONDICAO_REFERENCIA);

						BigDecimal pressaoReferenciadaConvertida = Util.converterCampoStringParaValorBigDecimal("", pressaoReferenciada,
										Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

						int escala = Integer.parseInt(escalaParametro);
						BigDecimal pressao = BigDecimal.ONE;
						BigDecimal temperatura = BigDecimal.ONE;

						VazaoCorretor vazaoCorretor = null;

						// obtém a instalação do medidor para obter o números de dígitos do corretor de vazão
						if (pontoConsumoLocal != null) {
							if (mapaVazaoCorretor != null && !mapaVazaoCorretor.isEmpty()) {
								Long chavePrimariaPontoConsumo = pontoConsumoLocal.getChavePrimaria();
								vazaoCorretor = mapaVazaoCorretor.get(chavePrimariaPontoConsumo);
							}
							// Caso seja medidor independente, o ponto de consumo não possuirá corretor de vazão pois o seu medidor é virtual.
							if (vazaoCorretor == null && indicadorMedidor.equals(true)) {
								// pega o corretor de vazao do medidor independente.
								vazaoCorretor = medidorLocal.getInstalacaoMedidor().getVazaoCorretor();
							}

						}

						String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
								.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

						String tipoIntegracaoDiaria = ServiceLocator.getInstancia().getControladorConstanteSistema()
								.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_DIARIA);

						// Caso o tipo de integração indique medição horária
						if (tipoIntegracao.equals(tipoIntegracaoHoraria)) {

							BigDecimal leituraMedicaoHorariaAtual = null;
							BigDecimal leituraMedicaoHorariaAnterior = null;
							BigDecimal consumoCorrigido = null;

							// Monta filtro para a consulta
							filtro = new HashMap<>();
							filtro.put(DATA_REALIZACAO_LEITURA, supervisorioMedicaoDiaria.getDataRealizacaoLeitura());
							filtro.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_DIARIA);
							filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoSupervisorio);
							filtro.put(HABILITADO, Boolean.TRUE);

							filtro.put(STATUS_AUTORIZADO, Long.valueOf(paramStatusAutorizado));

							// consultar o registro anterior decrementando a data da realização da leitura
							Long quantidadeSupervisorioMedicaoHoraria =  this.contarSupervisorioMedicaoHoraria(filtro);

							if (quantidadeSupervisorioMedicaoHoraria == null || quantidadeSupervisorioMedicaoHoraria != CONSTANTE_NUMERO_UM) {

								if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
												|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
																.getIndicadorImpedeProcessamento()
																.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

									// Gera anormalidade
									SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
													.obterSupervisorioMedicaoAnormalidade(Constantes
															.ANORMALIDADE_SUPERVISORIO_REGISTRO_DIARIO_NAO_ENCONTRADO);
									supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
								}

							} else {

								// Monta filtro para a consulta
								filtro = new HashMap<>();
								filtro.put("decrementarDataInicial", Boolean.TRUE);
								filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, supervisorioMedicaoDiaria.getDataRealizacaoLeitura());
								filtro.put(DATA_FINAL_REALIZACAO_LEITURA, Util.incrementarDataComQuantidadeDias(
												supervisorioMedicaoDiaria.getDataRealizacaoLeitura(),
												Constantes.NUMERO_DIAS_INCREMENTAR_DATA));
								filtro.put(INDICADOR_TIPO_MEDICAO, Constantes.SUPERVISORIO_TIPO_MEDICAO_DIARIA);
								filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoSupervisorio);
								filtro.put(HABILITADO, Boolean.TRUE);

								filtro.put(STATUS_AUTORIZADO, Long.valueOf(paramStatusAutorizado));

								// Monta a ordenação da consulta
								ordenacao = DATA_REALIZACAO_LEITURA + " desc";

								// consultar o registro anterior decrementando a data da realização da leitura
								List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = (List<SupervisorioMedicaoHoraria>) this
												.consultarSupervisorioMedicaoHoraria(filtro, ordenacao);

								if (listaSupervisorioMedicaoHoraria != null && listaSupervisorioMedicaoHoraria.size() > CONSTANTE_NUMERO_UM) {
									leituraMedicaoHorariaAtual = Util.primeiroElemento(listaSupervisorioMedicaoHoraria)
													.getLeituraComCorrecaoFatorPTZ();

									leituraMedicaoHorariaAnterior = listaSupervisorioMedicaoHoraria.get(
													listaSupervisorioMedicaoHoraria.size() - CONSTANTE_NUMERO_UM).getLeituraComCorrecaoFatorPTZ();

									if (leituraMedicaoHorariaAtual.compareTo(leituraMedicaoHorariaAnterior) >= CONSTANTE_NUMERO_ZERO) {
										consumoCorrigido = leituraMedicaoHorariaAtual.subtract(leituraMedicaoHorariaAnterior);
									} else {

										// chama o fluxo de virada de mostrador
										if (vazaoCorretor != null) {
											retornoViradaCorretorVazao = this.tratarViradaMostradorCorretorVazao(
															leituraMedicaoHorariaAtual, leituraMedicaoHorariaAnterior, vazaoCorretor,
															tipoIntegracao, mapaAnormalidade);

											if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
															|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
																			.getIndicadorImpedeProcessamento()
																			.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

												supervisorioMedicaoDiaria
																.setSupervisorioMedicaoAnormalidade((SupervisorioMedicaoAnormalidade) retornoViradaCorretorVazao
																				.get(Constantes.SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA));

											}

											// se a medição for horária e a leitura for sem correção calcula o consumo
											consumoCorrigido = (BigDecimal) retornoViradaCorretorVazao.get(Constantes.CONSUMO);

										} else {
											// caso a leitura anterior seja menor que a leitura atual e o ponto de consumo não possua
											// corretor de
											// vazão
											// uma anomalia é gerada para o SMD

											if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
															|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
																			.getIndicadorImpedeProcessamento()
																			.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

												// Gera anormalidade
												SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
														.obterSupervisorioMedicaoAnormalidade(Constantes
																.ANORMALIDADE_SUPERVISORIO_POSSIVEL_VIRADA_SEM_CORRETOR_VAZAO);
												supervisorioMedicaoDiaria
																.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

											}

										}
									}
								}
							}

							BigDecimal fatorPTZ = null;
							if (supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() != null
											&& (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() != null && supervisorioMedicaoDiaria
															.getConsumoSemCorrecaoFatorPTZ().compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO)) {

								// Calcula o fatorPTZ
								fatorPTZ = supervisorioMedicaoDiaria
												.getConsumoComCorrecaoFatorPTZ()
												.setScale(escala, BigDecimal.ROUND_HALF_UP)
												.divide(supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ().setScale(escala,
																BigDecimal.ROUND_HALF_UP), BigDecimal.ROUND_HALF_UP);
							} else {

								fatorPTZ = BigDecimal.ONE;
							}

							supervisorioMedicaoDiaria.setFatorPTZ(fatorPTZ);

							if (supervisorioMedicaoDiaria.getPressao() != null && supervisorioMedicaoDiaria.getTemperatura() != null) {

								pressao = supervisorioMedicaoDiaria.getPressao();

								temperatura = controladorHistoricoConsumo.converterUnidadeMedida(supervisorioMedicaoDiaria.getTemperatura(),
												unidadeConversao);

								// Calcula o fator PT
								BigDecimal fatorPT = controladorHistoricoConsumo.obterFatorPT(temperaturaReferenciadaConvertida,
												pressaoReferenciadaConvertida, temperatura, pressaoReferenciadaConvertida, pressao);

								BigDecimal fatorZ = null;

								if (fatorPT.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {

									fatorZ = fatorPTZ.divide(fatorPT.setScale(escala, BigDecimal.ROUND_HALF_UP), BigDecimal.ROUND_HALF_UP);

									supervisorioMedicaoDiaria.setFatorZ(fatorZ.setScale(Constantes.ESCALA_FATOR_PTZ,
													BigDecimal.ROUND_HALF_UP));
								}

							}

							if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
											|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
															.getIndicadorImpedeProcessamento()
															.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

								// validar as anormalidades geradas no SupervisorioMedicaoHoraria
								this.validarAnomaliasSupervisorioMedicaoHoraria(mapaAnormalidadeSupervisorioMedicaoDiaria,
												supervisorioMedicaoDiaria, mapaAnormalidade);

							}

							if ((consumoCorrigido != null
											&& consumoCorrigido.compareTo(supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ()) != CONSTANTE_NUMERO_ZERO) &&
											(supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
											|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
											.getIndicadorImpedeProcessamento()
											.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO))) {

								// Gera anormalidade
								SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
												.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_VOLUME_DIVERGE);
								supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

							}
						} else if (tipoIntegracao.equals(tipoIntegracaoDiaria)) {

							// verificar a hora fracionada
							if (Util.verificarHoraFracionada(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) &&
											(supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
											|| !supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
											.getIndicadorImpedeProcessamento().equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO))) {

								// Gera anormalidade
								SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
												.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_REGISTRO_HORA_FRACIONADA);
								supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

							}

							Map<String, Object> mapa = null;
							Boolean possuiLeituraAnterior = Boolean.FALSE;

							// verifica se existe SupervisorioMedicaoDiaria anterior
							if (supervisorioMedicaoDiariaAnterior == null) {

								// verifica se existe SupervisorioMedicaoDiaria anterior para obter a data da leitura anterior
								mapa = this.verificarSupervisorioMedicaoDiariaAnterior(pontoConsumoLocal, Boolean.TRUE,
												supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), vazaoCorretor,
												Long.valueOf(codigoOperacaoMedidorInstalacao));

								if (mapa != null && !mapa.isEmpty()) {
									supervisorioMedicaoDiariaAnterior = (SupervisorioMedicaoDiaria) mapa
													.get(Constantes.SUPERVISORIO_MEDICAO_DIARIA_ANTERIOR);
									possuiLeituraAnterior = (Boolean) mapa.get(Constantes.POSSUI_LEITURA_ANTERIOR);
								}
							} else {

								possuiLeituraAnterior = Boolean.TRUE;
							}

							if (supervisorioMedicaoDiaria.getFatorZ() != null
											&& supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null) {
								// se o fator Z for informado será
								// preciso calcular os consumos e as
								// leituras

								if (supervisorioMedicaoDiaria.getPressao() != null) {
									pressao = supervisorioMedicaoDiaria.getPressao();
								}

								if (supervisorioMedicaoDiaria.getTemperatura() != null) {

									temperatura = controladorHistoricoConsumo
													.converterUnidadeMedida(supervisorioMedicaoDiaria.getTemperatura(), unidadeConversao);
								}

								// Calcula o fator PT
								BigDecimal fatorPT = controladorHistoricoConsumo.obterFatorPT(temperaturaReferenciadaConvertida,
												pressaoReferenciadaConvertida, temperatura, pressaoReferenciadaConvertida, pressao);

								// Calcula o consumo sem correção
								BigDecimal consumo = null;

								consumo = supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ().divide(
												fatorPT.setScale(escala, BigDecimal.ROUND_HALF_UP), CONSTANTE_NUMERO_ZERO, BigDecimal.ROUND_DOWN);

								// consumo sem correção não precisa ter decimal
								supervisorioMedicaoDiaria.setConsumoSemCorrecaoFatorPTZ(consumo);

								// Calcula o consumo corrigido
								BigDecimal consumoCorrigido = supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ().multiply(
												supervisorioMedicaoDiaria.getFatorZ());
								supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(consumoCorrigido.setScale(ESCALA_SUPERVISORIO,
												BigDecimal.ROUND_HALF_UP));

								BigDecimal fatorPTZ = null;
								// Calcula o Fator PTZ
								if (supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() != null
												&& (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() != null && supervisorioMedicaoDiaria
																.getConsumoSemCorrecaoFatorPTZ().compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO)) {

									fatorPTZ = supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ().divide(
													supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ(), escala,
													BigDecimal.ROUND_HALF_UP);
								} else {

									fatorPTZ = BigDecimal.ONE;
								}

								supervisorioMedicaoDiaria.setFatorPTZ(fatorPTZ);

								BigDecimal leituraAtualSemCorrecao = null;
								BigDecimal leituraAtualComCorrecao = null;

								BigDecimal leituraAnteriorSemCorrecao = BigDecimal.ZERO;
								BigDecimal leituraAnteriorComCorrecao = BigDecimal.ZERO;

								if (possuiLeituraAnterior) {
									leituraAnteriorSemCorrecao = supervisorioMedicaoDiariaAnterior.getLeituraSemCorrecaoFatorPTZ();
									leituraAnteriorComCorrecao = supervisorioMedicaoDiariaAnterior.getLeituraComCorrecaoFatorPTZ();

									// Calcula a leitura sem correção
									leituraAtualSemCorrecao = leituraAnteriorSemCorrecao.add(supervisorioMedicaoDiaria
													.getConsumoSemCorrecaoFatorPTZ());

									// Calcula a leitura com correção
									leituraAtualComCorrecao = leituraAnteriorComCorrecao.add(supervisorioMedicaoDiaria
													.getConsumoComCorrecaoFatorPTZ());
								} else {
									leituraAtualSemCorrecao = supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ();
									leituraAtualComCorrecao = supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ();
								}

								if (vazaoCorretor != null) {
									BigDecimal potencia = BigDecimal.valueOf(Math.pow(CONSTANTE_NUMERO_DEZ, vazaoCorretor.getNumeroDigitos()));

									// verifica se houve virada de mostrador para a leitura sem correção
									if (leituraAtualSemCorrecao != null && leituraAtualSemCorrecao.compareTo(potencia) > CONSTANTE_NUMERO_ZERO) {
										retornoViradaCorretorVazao = this.tratarViradaMostradorCorretorVazao(leituraAtualSemCorrecao,
														leituraAnteriorSemCorrecao, vazaoCorretor, tipoIntegracao, mapaAnormalidade);

										if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
														|| !supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
																		.getIndicadorImpedeProcessamento().equals
																			(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

											supervisorioMedicaoDiaria
															.setSupervisorioMedicaoAnormalidade((SupervisorioMedicaoAnormalidade) retornoViradaCorretorVazao
																			.get(Constantes.SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA));

										}

										// se a medição for diária e a leitura for sem correção calcula a leitura
										leituraAtualSemCorrecao = (BigDecimal) retornoViradaCorretorVazao.get(Constantes.LEITURA);
									}

									// verifica se houve virada de mostrador para a leitura com correção
									if (leituraAtualComCorrecao != null && leituraAtualComCorrecao.compareTo(potencia) > CONSTANTE_NUMERO_ZERO) {
										retornoViradaCorretorVazao = this.tratarViradaMostradorCorretorVazao(leituraAtualComCorrecao,
														leituraAnteriorComCorrecao, vazaoCorretor, tipoIntegracao, mapaAnormalidade);

										if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
														|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
																		.getIndicadorImpedeProcessamento().equals
																			(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

											supervisorioMedicaoDiaria
															.setSupervisorioMedicaoAnormalidade((SupervisorioMedicaoAnormalidade) retornoViradaCorretorVazao
																			.get(Constantes.SUPERVISORIO_MEDICAO_ANORMALIDADE_VIRADA));

										}

										// se a medição for diária e a leitura for com correção calcula a leitura com correção
										leituraAtualComCorrecao = (BigDecimal) retornoViradaCorretorVazao.get(Constantes.LEITURA);

									}

								}
								if (leituraAtualSemCorrecao != null) {
									supervisorioMedicaoDiaria.setLeituraSemCorrecaoFatorPTZ(
											leituraAtualSemCorrecao.setScale(CONSTANTE_NUMERO_ZERO, BigDecimal.ROUND_HALF_UP));
								}
								if (leituraAtualComCorrecao != null) {
									supervisorioMedicaoDiaria.setLeituraComCorrecaoFatorPTZ(
											leituraAtualComCorrecao.setScale(CONSTANTE_NUMERO_ZERO, BigDecimal.ROUND_HALF_UP));
								}
							}
							
							if(pontoConsumoLocal != null) {
								verificarViradaMedidor(supervisorioMedicaoDiaria, supervisorioMedicaoDiariaAnterior,
										pontoConsumoLocal.getInstalacaoMedidor().getMedidor().getDigito());
							}
							
							supervisorioMedicaoDiariaAnterior = supervisorioMedicaoDiaria;
						}

						supervisorioMedicaoDiaria.setIndicadorIntegrado(Boolean.FALSE);
						supervisorioMedicaoDiaria.setIndicadorProcessado(Boolean.FALSE);
						supervisorioMedicaoDiaria.setIndicadorConsolidada(Boolean.TRUE);
						supervisorioMedicaoDiaria.setDataRegistroLeitura(new Date(System.currentTimeMillis()));

						this.validarMapaAnormalidades(mapaAnormalidade, supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade());
						supervisorioMedicaoDiaria = this.preencherEscalaSupervisorio(supervisorioMedicaoDiaria);
												
						
						if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null
								&& supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
										.getChavePrimaria() == Constantes.ANORMALIDADE_SUPERVISORIO_ENDERECO_REMOTO_NAO_LOCALIZADO) {
							supervisorioMedicaoDiaria.setIndicadorConsolidada(Boolean.FALSE);
						}
						
						
						if (supervisorioMedicaoDiariaAnterior != null && supervisorioMedicaoDiaria
								.getConsumoComCorrecaoFatorPTZ().compareTo(BigDecimal.ZERO) < 0
								&& pontoConsumoLocal != null) {
							logProcessamento.append(pontoConsumoLocal.getDescricao());
							logProcessamento.append("( ");
							logProcessamento.append(DataUtil.converterDataParaString(supervisorioMedicaoDiariaAnterior.getDataRealizacaoLeitura(), false));
							logProcessamento.append(" ) com consumo medido negativo:");
							logProcessamento.append(supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ());
							logProcessamento.append("\n");
							
							if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
									|| (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null
											&& !supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade()
													.getIndicadorImpedeProcessamento())) {
								SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
										.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_CONSUMO_NEGATIVO);
								supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
							}
						}
						
						if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null
								&& pontoConsumoLocal != null) {
							logProcessamento.append(pontoConsumoLocal.getDescricao());
							logProcessamento.append("( ");
							logProcessamento.append(DataUtil.converterDataParaString(supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), false));
							logProcessamento.append(" ) com anormalidade:");
							logProcessamento.append(supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ());
							logProcessamento.append("\n");
							
						}
						
						super.atualizarEmBatch(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);											
						
						possuiContrato = Boolean.FALSE;
					}
				}
				getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
			}

			resumoSupervisorioMedicaoVO.setQuantidadeSupervisorioMedicaoDiaria(quantidadeSupervisorioMedicaoDiariaProcessadas);
			resumoSupervisorioMedicaoVO.setMapaAnormalidade(mapaAnormalidade);

			Integer quantidadeRegistro;
			if(resumoSupervisorioMedicaoVO.getQuantidadeSupervisorioMedicaoHoraria() != null){
				quantidadeRegistro = resumoSupervisorioMedicaoVO.getQuantidadeSupervisorioMedicaoHoraria();
			}else{
				quantidadeRegistro = CONSTANTE_NUMERO_ZERO;
			}

			logProcessamento.append(Constantes.PULA_LINHA)
					.append("[")
					.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
					.append("] ").append(Constantes.REGISTRO_MEDICOES_HORARIA_PROCESSADOS).append(quantidadeRegistro)
					.append(Constantes.PULA_LINHA);

			if(resumoSupervisorioMedicaoVO.getQuantidadeSupervisorioMedicaoDiaria() != null){
				quantidadeRegistro = resumoSupervisorioMedicaoVO.getQuantidadeSupervisorioMedicaoDiaria();
			}else{
				quantidadeRegistro = CONSTANTE_NUMERO_ZERO;
			}

			logProcessamento.append("[")
					.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
					.append("] ").append(Constantes.REGISTRO_MEDICOES_DIARIA_PROCESSADOS).append(quantidadeRegistro).append(Constantes.PULA_LINHA);

			this.preencherLogMapaAnormalidade(logProcessamento, resumoSupervisorioMedicaoVO);
			logProcessamento.append(pontosConsumoSemRota.toString());

		} else {
			logProcessamento.append("[")
					.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
					.append("] ").append(Constantes.ERRO_NEGOCIO_SEM_DADOS + Constantes.PULA_LINHA);
		}

		logProcessamento.append("[")
				.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true)).append("] ")
				.append(Constantes.CONCLUINDO_TRATAMENTO_MEDICOES_DIARIAS).append(Constantes.PULA_LINHA);
		
	}

	private void verificarViradaMedidor(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAnterior, int digito) throws NegocioException {
		if (supervisorioMedicaoDiariaAnterior != null
				&& supervisorioMedicaoDiariaAnterior.getCodigoPontoConsumoSupervisorio()
						.equals(supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio())
				&& supervisorioMedicaoDiariaAnterior.getLeituraComCorrecaoFatorPTZ()
						.compareTo(supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ()) > 0) {
			BigDecimal calculo = (supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ()
					.add(BigDecimal.valueOf(Math.pow(10, digito))))
							.subtract(supervisorioMedicaoDiariaAnterior.getLeituraComCorrecaoFatorPTZ());

			if (calculo != null) {
				calculo = calculo.setScale(8, RoundingMode.HALF_UP);
			}
			
			BigDecimal calculoVirada = BigDecimal.valueOf(Math.pow(10, digito)).subtract(BigDecimal.valueOf(1));
			
			if (calculo != null && calculo.compareTo(calculoVirada) <= CONSTANTE_NUMERO_ZERO) {
				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
						.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_VIRADA_MEDIDOR);
				supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
				supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(calculo);
			}
		}
		
	}

	private void removerMedicoesSemCronograma(
			List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaPorCodigoSupervisorio,
			Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura, Long chavePrimariaGrupo) {
		
		Date dataFim = null;
		List<Map<String, Object>> listaMapaReferenciaLeitura = new ArrayList<>();
		listaMapaReferenciaLeitura = mapaComListaReferenciaLeitura.get(chavePrimariaGrupo);
		Boolean remover = Boolean.FALSE;
		List<SupervisorioMedicaoDiaria> listaSupervisorioMedicao =  new ArrayList<>();
		
		for (SupervisorioMedicaoDiaria medicaoDiaria : listaSupervisorioMedicaoDiariaPorCodigoSupervisorio) {
			
			Date dataRealizacaoLeitura = medicaoDiaria.getDataRealizacaoLeitura();
			if (listaMapaReferenciaLeitura != null) {
				for (Map<String, Object> mapa : listaMapaReferenciaLeitura) {

					dataFim = (Date) mapa.get("dataFim");

					if (dataFim.compareTo(dataRealizacaoLeitura) > CONSTANTE_NUMERO_ZERO) {
						remover = Boolean.TRUE;
						break;
					}
				}
				
				if(!remover) {
					listaSupervisorioMedicao.add(medicaoDiaria);
				}
				
				remover = Boolean.FALSE;
			} else {
				listaSupervisorioMedicao.add(medicaoDiaria);
			}
		}
		
		listaSupervisorioMedicaoDiariaPorCodigoSupervisorio.removeAll(listaSupervisorioMedicao);
		
	}

	private Collection<PontoConsumo> obtemListaPontoConsumoMedidor(ControladorMedidor controladorMedidor, Medidor medidor)
					throws GGASException {

		Collection<PontoConsumo> listaPontoConsumoMedidor = new ArrayList<>();
		Collection<Medidor> listaMedidorVirtual = controladorMedidor.consultarMedidorVirtualPorMedidorIndependente(medidor);
		for (Medidor medidorVirtual : listaMedidorVirtual) {
			if (medidorVirtual.getInstalacaoMedidor() != null) {
				listaPontoConsumoMedidor.add(medidorVirtual.getInstalacaoMedidor().getPontoConsumo());
			}
		}
		return listaPontoConsumoMedidor;
	}

	private void verificaAnormalidadesPontoConsumo(ControladorMedidor controladorMedidor,
			ControladorContrato controladorContrato, PontoConsumo pontoConsumoLocal,
			Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura, Periodicidade periodicidadeTmp,
			SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, Boolean possuiContratoTmp,
			ContratoPontoConsumo contratoPontoConsumo, Boolean indicadorMedidor, Medidor medidor) throws GGASException {
		Periodicidade periodicidade = periodicidadeTmp;
		Boolean possuiContrato;
		if (pontoConsumoLocal != null) {
			//final if ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE
			if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null ||
					supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade().getChavePrimaria() !=
					ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE) {
				// verifica como estava o status do ponto de consumo na data que a leitura foi realizada
				Map<String, Object> mapaInstalacaoMedidor = controladorMedidor
						.verificarStatusPontoConsumo(supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), pontoConsumoLocal);

				if (mapaInstalacaoMedidor != null && !mapaInstalacaoMedidor.isEmpty()) {

					Boolean isStatusAtivo = (Boolean) mapaInstalacaoMedidor.get("isStatusAtivo");

					if (isStatusAtivo != null && !isStatusAtivo) {

						// Gera anormalidade
						SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade =
								this.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_PC_INATIVO);
						supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
					}
				}

				Map<String, Object> mapaContratoAtivo = controladorContrato
						.validarContratoAtivoPorDataLeitura(supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), contratoPontoConsumo,
								pontoConsumoLocal);
				possuiContrato = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);

				if (possuiContrato == null || !possuiContrato) {

					// Gera anormalidade
					SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade =
							this.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_PC_SEM_CONTRATO_ATIVO);
					supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

				}

				// Verifica se tem um corretor de vazão associado
				if (indicadorMedidor == null || Boolean.FALSE.equals(indicadorMedidor)) {
					verificaCorretorVazaoAssociado(controladorMedidor, pontoConsumoLocal.getChavePrimaria(), supervisorioMedicaoDiaria,
							null, Constantes.ANORMALIDADE_SUPERVISORIO_PC_SEM_CORRETOR_VAZAO_INSTALADO);
				} else {
					verificaCorretorVazaoAssociado(controladorMedidor, null, supervisorioMedicaoDiaria, medidor.getChavePrimaria(),
							Constantes.ANORMALIDADE_SUPERVISORIO_MEDIDOR_SEM_CORRETOR_VAZAO_INSTALADO);
				}

				// obtém a hora inicial e a periodicidade do contrato se tiver
				if ((possuiContrato != null) && (possuiContrato && mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO) != null)) {
					// Recupera a periodicidade de faturamento do contrato ativo
					periodicidade = (Periodicidade) mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO);

				} else {
					// Recupera a periodicidade de faturamento do ramo de atividade se o PC não tiver contrato ativo
					if (pontoConsumoLocal.getRamoAtividade().getPeriodicidade() != null) {
						periodicidade = pontoConsumoLocal.getRamoAtividade().getPeriodicidade();

					} else if (pontoConsumoLocal.getSegmento().getPeriodicidade() != null) {

						// Recupera a periodicidade de faturamento do segmento se o ramo de atividade não for informado
						periodicidade = pontoConsumoLocal.getSegmento().getPeriodicidade();
					}
				}
			} 

			Map<String, Object> mapaAnoMesReferenciaCiclo = this.obterAnoMesReferenciaCiclo(mapaComListaReferenciaLeitura,
							supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), pontoConsumoLocal, periodicidade, Boolean.TRUE);

			supervisorioMedicaoDiaria.setDataReferencia((Integer) mapaAnoMesReferenciaCiclo.get("anoMesFaturamento"));
			supervisorioMedicaoDiaria.setNumeroCiclo((Integer) mapaAnoMesReferenciaCiclo.get("numeroCiclo"));

			if (supervisorioMedicaoDiaria.getDataReferencia() == null || supervisorioMedicaoDiaria.getNumeroCiclo() == null) {
				// Gera anormalidade
				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade(
						Constantes.ANORMALIDADE_SUPERVISORIO_CRONOGRAMA_FATURAMENTO_NAO_ENCONTRADO);
				supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
			}

		} else {
			// Gera anormalidade
			SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
							.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_ENDERECO_REMOTO_NAO_LOCALIZADO);
			supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

		}
	}

	private void verificaCorretorVazaoAssociado(ControladorMedidor controladorMedidor, Long idPontoConsumo,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, Long idMedidor, Long constante) throws NegocioException {

		Timestamp dataInstalacaoVazaoCorretorHistoricoOperacao = controladorMedidor.consultarDataInstalacaoRetiradaAnteriorDataLeitura(
						supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), idPontoConsumo, idMedidor);

		if (dataInstalacaoVazaoCorretorHistoricoOperacao == null) {

			// Gera anormalidade
			SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade(constante);
			supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
		}
	}

	private Boolean verificaAnormalidadeImpeditivaMedidor(ControladorMedidor controladorMedidor, Medidor medidor,
			SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, StringBuilder pontosConsumoSemRota)
			throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		List<PontoConsumo> listaPontoConsumo = new ArrayList<>();

		if (medidor != null) {
			Collection<Medidor> listaMedidorVirtual = controladorMedidor.consultarMedidorVirtualPorMedidorIndependente(medidor);
			Long codigoMedidorProntoInstalacao = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_PRONTO_INSTALAR));
			if ((medidor.getInstalacaoMedidor() == null) || (medidor.getInstalacaoMedidor() == null
					&& medidor.getSituacaoMedidor().getChavePrimaria() == codigoMedidorProntoInstalacao)) {
				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade =
						this.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_MEDIDOR_NAO_INSTALADO);
				supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
				return true;
			} else if (medidor.getInstalacaoMedidor().getVazaoCorretor() == null) {
				// Gera anormalidade pois o medidor independente não possui corretor de vazão.
				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade(
						Constantes.ANORMALIDADE_SUPERVISORIO_MEDIDOR_SEM_CORRETOR_VAZAO_INSTALADO);
				supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
				return true;
			} else if (listaMedidorVirtual == null || listaMedidorVirtual.isEmpty()) {
				// Gera anormalidade pois o medidor independente não compõe um medidor virtual.
				SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade(
						Constantes.ANORMALIDADE_SUPERVISORIO_NAO_EXISTE_MEDIDOR_VIRTUAL_PARA_MEDIDOR_INDEPENDENTE);
				supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
				return true;
			} else {
				for (Medidor medidorVirtual : listaMedidorVirtual) {
					InstalacaoMedidor instalacaoMedidor = medidorVirtual.getInstalacaoMedidor();
					if (instalacaoMedidor == null || instalacaoMedidor.getPontoConsumo() == null) {
						// Gera anormalidade pois o medidor virtual não esta "instalado" num ponto de consumo.
						SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade(
								Constantes.ANORMALIDADE_SUPERVISORIO_NAO_EXISTE_PONTO_CONSUMO_PARA_MEDIDOR_VIRTUAL);
						supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
						return true;
					} else {
						listaPontoConsumo.add(instalacaoMedidor.getPontoConsumo());
					}
				}
			}
		}

		if (!listaPontoConsumo.isEmpty()) {
			// Verifica se todos os pontos de consumo tem rota.
			Rota rota;
			StringBuilder pontosConsumoSemRotaMI = new StringBuilder();
			Boolean possuiPontoConsumoSemRota = false;
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				rota = pontoConsumo.getRota();
				if (rota == null) {
					pontosConsumoSemRotaMI.append(pontosConsumoSemRotaMI).append("                      ")
							.append(pontoConsumo.getDescricao()).append(Constantes.PULA_LINHA);
					possuiPontoConsumoSemRota = true;
				}
			}

			String mensagemLog = "["
							+ DataUtil.converterDataParaString(Calendar.getInstance().getTime(),  true) + "] "
							+ Constantes.PONTOS_CONSUMO_SEM_ROTA + pontosConsumoSemRotaMI.toString();

			int index = pontosConsumoSemRota.indexOf(Constantes.PONTOS_CONSUMO_SEM_ROTA + pontosConsumoSemRotaMI.toString());

			if (index == -CONSTANTE_NUMERO_UM && !"".equals(pontosConsumoSemRotaMI.toString())) {
				pontosConsumoSemRota.append(mensagemLog);
			}

			if (!possuiPontoConsumoSemRota) {
				// Verifica se os pontos de consumo possuem grupos faturamento iguais.
				GrupoFaturamento grupoFaturamento = Util.primeiroElemento(listaPontoConsumo).getRota().getGrupoFaturamento();
				for (PontoConsumo pontoConsumo : listaPontoConsumo) {
					long idGrupoFaturamento = pontoConsumo.getRota().getGrupoFaturamento().getChavePrimaria();
					if (idGrupoFaturamento != grupoFaturamento.getChavePrimaria()) {
						// Gera anormalidade pois os pontos de consumo possuem grupos faturamento diferentes.
						SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade(
								Constantes.ANORMALIDADE_SUPERVISORIO_PC_GRUPOS_FATURAMENTO_DIFERENTES_PARA_MI);
						supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
						return true;
					}
				}
			}
		}
		return false;
	}



	/**
	 * Preencher log mapa anormalidade.
	 *
	 * @param logProcessamento
	 *            the log processamento
	 * @param resumoSupervisorioMedicaoVO
	 *            the resumo supervisorio medicao vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void preencherLogMapaAnormalidade(StringBuilder logProcessamento, ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO)
					throws NegocioException {

		Map<Long, Integer> mapaAnormalidade = resumoSupervisorioMedicaoVO.getMapaAnormalidade();

		if (mapaAnormalidade != null && !mapaAnormalidade.isEmpty()) {

			logProcessamento.append("[")
					.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
					.append("] ").append(Constantes.ANORMALIDADES_REGISTRADAS);

			for (Map.Entry<Long, Integer> entry : resumoSupervisorioMedicaoVO.getMapaAnormalidade().entrySet()) {

				// mostra o filtro para fazer a pesquisa
				Map<String, Object> filtro = new HashMap<>();
				filtro.put(CHAVE_PRIMARIA, entry.getKey());
				filtro.put(HABILITADO, Boolean.TRUE);

				List<SupervisorioMedicaoAnormalidade> listaSupervisorioMedicaoDiariaPorIndicadorProcessado =
								(List<SupervisorioMedicaoAnormalidade>) this
								.consultarSupervisorioMedicaoAnormalidade(filtro);

				String descricaoAnormalidade = Util.primeiroElemento(listaSupervisorioMedicaoDiariaPorIndicadorProcessado).getDescricao();
				Integer quantidadeAnormalidades = entry.getValue();

				logProcessamento.append("                      ").append(descricaoAnormalidade).append(" - ")
						.append(quantidadeAnormalidades).append(Constantes.PULA_LINHA);

			}
		}
	}

	/**
	 * Verificar supervisorio medicao diaria anterior.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param indicadorProcessado
	 *            the indicador processado
	 * @param dataLeitura
	 *            the data leitura
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @param codigoOperacaoMedidorInstalacao
	 *            the codigo operacao medidor instalacao
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Map<String, Object> verificarSupervisorioMedicaoDiariaAnterior(PontoConsumo pontoConsumo, Boolean indicadorProcessado,
					Date dataLeitura, VazaoCorretor vazaoCorretor, Long codigoOperacaoMedidorInstalacao) throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia().getControladorHistoricoMedicao();

		// mostra o filtro para fazer a pesquisa
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(DATA_FINAL_REALIZACAO_LEITURA, dataLeitura);
		filtro.put(INDICADOR_PROCESSADO, indicadorProcessado);
		filtro.put(HABILITADO, Boolean.TRUE);

		SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAnterior = null;
		Map<String, Object> mapa = new HashMap<>();

		// Monta a ordenação da consulta
		String ordenacao = " smd." + DATA_REALIZACAO_LEITURA + " desc";

		// procura no SMD já processados, retornando o registro mais recente
		List<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaPorIndicadorProcessado = (List<SupervisorioMedicaoDiaria>) this
						.consultarSupervisorioMedicaoDiaria(filtro, ordenacao);

		if (listaSupervisorioMedicaoDiariaPorIndicadorProcessado != null
				&& !listaSupervisorioMedicaoDiariaPorIndicadorProcessado.isEmpty()) {

			mapa.put(Constantes.SUPERVISORIO_MEDICAO_DIARIA_ANTERIOR,
					Util.primeiroElemento(listaSupervisorioMedicaoDiariaPorIndicadorProcessado));
			mapa.put(Constantes.POSSUI_LEITURA_ANTERIOR, Boolean.TRUE);

		} else {

			if (pontoConsumo != null) {

				// caso não encontre no SMD já processados procura no histórico de medição
				HistoricoMedicao historicoMedicaoAtual = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(
								pontoConsumo.getChavePrimaria(), null, null, Boolean.FALSE);

				supervisorioMedicaoDiariaAnterior = (SupervisorioMedicaoDiaria) this.criarSupervisorioMedicaoDiaria();

				BigDecimal leituraSemCorrecao = BigDecimal.ZERO;

				if (historicoMedicaoAtual != null && historicoMedicaoAtual.getNumeroLeituraFaturada() != null
								&& historicoMedicaoAtual.getNumeroLeituraCorretor() != null) {
					supervisorioMedicaoDiariaAnterior.setLeituraComCorrecaoFatorPTZ(historicoMedicaoAtual.getNumeroLeituraFaturada());


					if (historicoMedicaoAtual.getFatorPTZCorretor() != null) {
						leituraSemCorrecao = historicoMedicaoAtual.getNumeroLeituraFaturada().divide(
										historicoMedicaoAtual.getFatorPTZCorretor(), RoundingMode.HALF_DOWN);
					}
					supervisorioMedicaoDiariaAnterior.setLeituraSemCorrecaoFatorPTZ(leituraSemCorrecao);
					mapa.put(Constantes.SUPERVISORIO_MEDICAO_DIARIA_ANTERIOR, supervisorioMedicaoDiariaAnterior);
					mapa.put(Constantes.POSSUI_LEITURA_ANTERIOR, Boolean.TRUE);

				}

			}

		}

		return mapa;
	}

	/**
	 * Validar anomalias supervisorio medicao horaria.
	 *
	 * @param mapaAnormalidadeSupervisorioMedicaoDiaria
	 *            the mapa anormalidade supervisorio medicao diaria
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param mapaAnormalidade
	 *            the mapa anormalidade
	 */
	public void validarAnomaliasSupervisorioMedicaoHoraria(Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, Map<Long, Integer> mapaAnormalidade) {

		List<Object[]> listaDataAnormalidade = mapaAnormalidadeSupervisorioMedicaoDiaria.get(supervisorioMedicaoDiaria
						.getCodigoPontoConsumoSupervisorio());

		if (listaDataAnormalidade != null && !listaDataAnormalidade.isEmpty()) {

			for (Object[] arrayDataAnormalidade : listaDataAnormalidade) {

				Date dataSupervisorioMedicaoHoraria = (Date) arrayDataAnormalidade[CONSTANTE_NUMERO_ZERO];

				if (DataUtil.gerarDataHmsZerados(dataSupervisorioMedicaoHoraria).compareTo(
								DataUtil.gerarDataHmsZerados(supervisorioMedicaoDiaria.getDataRealizacaoLeitura())) == CONSTANTE_NUMERO_ZERO) {

					if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null || !supervisorioMedicaoDiaria
							.getSupervisorioMedicaoAnormalidade().getIndicadorImpedeProcessamento()
							.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

						SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade =
								(SupervisorioMedicaoAnormalidade) arrayDataAnormalidade[CONSTANTE_NUMERO_UM];
						supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);

					}

					if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() != null
									&& supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade().getIndicadorImpedeProcessamento()
													.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {
						break;
					}

				}
			}
		}
	}

	/**
	 * Validar mapa anormalidades.
	 *
	 * @param mapaAnormalidade
	 *            the mapa anormalidade
	 * @param supervisorioMedicaoAnormalidade
	 *            the supervisorio medicao anormalidade
	 */
	public void validarMapaAnormalidades(Map<Long, Integer> mapaAnormalidade,
					SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade) {

		if (supervisorioMedicaoAnormalidade != null) {
			Boolean possuiAnormalidade = Boolean.FALSE;

			if (!mapaAnormalidade.isEmpty()) {
				for (Map.Entry<Long, Integer> entry : mapaAnormalidade.entrySet()) {

					if (entry.getKey() == supervisorioMedicaoAnormalidade.getChavePrimaria()) {

						possuiAnormalidade = Boolean.TRUE;

						Integer quantidadeAnormalidades = entry.getValue();
						++quantidadeAnormalidades;
						entry.setValue(quantidadeAnormalidades);

					}
				}

				if (!possuiAnormalidade) {
					mapaAnormalidade.put(supervisorioMedicaoAnormalidade.getChavePrimaria(), CONSTANTE_NUMERO_UM);
				}

			} else {
				mapaAnormalidade.put(supervisorioMedicaoAnormalidade.getChavePrimaria(), CONSTANTE_NUMERO_UM);
			}
		}
	}

	/**
	 * Verificar ciclo por data para primeira medicao.
	 *
	 * @param data
	 *            the data
	 * @param quantidadeCiclo
	 *            the quantidade ciclo
	 * @return the integer
	 */
	public Integer verificarCicloPorDataParaPrimeiraMedicao(Date data, Integer quantidadeCiclo) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		int qtdDiasMesAtual = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		int diaAtual = calendar.get(Calendar.DAY_OF_MONTH);
		int tamanhoCiclo = qtdDiasMesAtual / quantidadeCiclo;
		Integer ciclo = CONSTANTE_NUMERO_UM;

		while (diaAtual > tamanhoCiclo) {
			ciclo++;
			tamanhoCiclo = tamanhoCiclo + tamanhoCiclo;
		}

		return ciclo;
	}

	/**
	 * Listar supervisorios por rota.
	 *
	 * @param chaveRota
	 *            the chave rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<SupervisorioMedicaoDiaria> listarSupervisoriosPorRota(Long[] chaveRota, Integer anoMesReferencia, Integer numeroCiclo)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select  sumd ");
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName());
		hql.append(" sumd, ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName()).append(" pocn ");
		hql.append(" where pocn.codigoPontoConsumoSupervisorio = sumd.codigoPontoConsumoSupervisorio ");
		hql.append(" and pocn.rota.chavePrimaria in (:chaveRota) ");
		hql.append(" and sumd.indicadorIntegrado is true ");
		hql.append(" and sumd.indicadorProcessado is false ");
		hql.append(" and sumd.dataReferencia = :anoMesReferencia ");
		hql.append(" and sumd.numeroCiclo = :numeroCiclo ");
		hql.append(" order by sumd.dataRealizacaoLeitura desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chaveRota", chaveRota);
		query.setInteger("numeroCiclo", numeroCiclo);
		query.setInteger("anoMesReferencia", anoMesReferencia);

		Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = query.list();
		ParametroSistema temFatorZFixo = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1);
		if ((temFatorZFixo != null) && (temFatorZFixo.getValor() != null && "1".equals(temFatorZFixo.getValor()))) {
			for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
				if (supervisorioMedicaoDiaria.getFatorZ() == null
						|| supervisorioMedicaoDiaria.getFatorZ().compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO) {
					supervisorioMedicaoDiaria.setFatorZ(BigDecimal.ONE);
				}
			}
		}

		return listaSupervisorioMedicaoDiaria;
	}

	/**
	 * Listar supervisorio medicao horaria.
	 *
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigo ponto consumo supervisorio
	 * @param dataRealizacaoLeitura
	 *            the data realizacao leitura
	 * @param indicadorTipoMedicao
	 *            the indicador tipo medicao
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<SupervisorioMedicaoHoraria> listarSupervisorioMedicaoHoraria(String codigoPontoConsumoSupervisorio,
					Date dataRealizacaoLeitura, String indicadorTipoMedicao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" smh ");
		hql.append(" where ");

		if (codigoPontoConsumoSupervisorio != null && !codigoPontoConsumoSupervisorio.isEmpty()) {
			hql.append(" smh.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio and ");
		}

		if (indicadorTipoMedicao != null) {
			hql.append(" smh.indicadorTipoMedicao = :indicadorTipoMedicao and ");
		}

		if (dataRealizacaoLeitura != null) {
			hql.append(" smh.dataRealizacaoLeitura  in (:dataAtualRealizacaoLeitura, :dataAnteriorRealizacaoLeitura) ");
		}

		hql.append(ORDENACAO_POR_DATA_REALIZACAO_LEITURA);

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoPontoConsumoSupervisorio != null && !codigoPontoConsumoSupervisorio.isEmpty()) {
			query.setString("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio);
		}

		if (indicadorTipoMedicao != null) {
			query.setString("indicadorTipoMedicao", indicadorTipoMedicao);
		}

		if (dataRealizacaoLeitura != null) {
			query.setDate("dataAtualRealizacaoLeitura", dataRealizacaoLeitura);
			query.setDate("dataAnteriorRealizacaoLeitura",
							Util.decrementarDataComQuantidadeDias(dataRealizacaoLeitura, Constantes.NUMERO_DIAS_INCREMENTAR_DATA));

		}

		return query.list();
	}

	/**
	 * Calcular ano mes referencia ciclo.
	 *
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param periodicidade
	 *            the periodicidade
	 * @param dataPartida
	 *            the data partida
	 * @param dataLeitura
	 *            the data leitura
	 * @param grupoFaturamento
	 *            the grupo faturamento
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public List<Map<String, Object>> calcularAnoMesReferenciaCiclo(Integer anoMesFaturamento, Integer numeroCiclo,
					Periodicidade periodicidade, Date dataPartida, Date dataLeitura, GrupoFaturamento grupoFaturamento)
					throws GGASException {

		Integer anoMesFaturamentoLocal = anoMesFaturamento;
		Integer numeroCicloLocal = numeroCiclo;
		Date dataPartidaComPeriocididade;
		List<Map<String, Object>> listaMapaReferenciaLeitura = new ArrayList<>();
		Map<String, Object> mapaReferenciasLeitura = new LinkedHashMap<>();
		Boolean dataLeituraMaior = Boolean.FALSE;

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getBeanPorID(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		ControladorGrupoFaturamento controladorGrupoFaturamento = ServiceLocator.getInstancia().getControladorGrupoFaturamento();
		
		Map<String, Integer> referenciaCiclo = null;
		if (controladorGrupoFaturamento.isPeriodicidadeGrupoMultiplosCiclos(grupoFaturamento)) {
			referenciaCiclo = Util.gerarProximaReferenciaCiclo(anoMesFaturamentoLocal, numeroCicloLocal,
					periodicidade.getQuantidadeDias(),  new DateTime(dataLeitura.getTime()));
		} else {
			referenciaCiclo =  Util.rolarReferenciaCiclo(anoMesFaturamentoLocal, numeroCicloLocal,
					periodicidade.getQuantidadeCiclo());
		}

		numeroCicloLocal = referenciaCiclo.get("ciclo");
		anoMesFaturamentoLocal = referenciaCiclo.get("referencia");

		DateTime dataPartidaDateTime = new DateTime(dataPartida);

		// incrementa a data de partida de acordo com a quantidade de dias da periodicidade
		Map<String, Object> mapaDatas = controladorCronogramaFaturamento.obterDatasSugestaoCronograma(dataPartidaDateTime, periodicidade,
						null, null, null, Boolean.TRUE, grupoFaturamento.getTipoLeitura(), numeroCicloLocal, null);

		dataPartidaComPeriocididade = (Date) mapaDatas.get("dataPrevista");

		// verifica se a data da realização da leitura está entre o período de referência do último faturamento
		while (dataLeitura.compareTo(dataPartidaComPeriocididade) > CONSTANTE_NUMERO_ZERO) {

			dataLeituraMaior = Boolean.TRUE;

			// obtém o próximo ano/mês e ciclo
			referenciaCiclo = Util.gerarProximaReferenciaCiclo(anoMesFaturamentoLocal, numeroCicloLocal,
							periodicidade.getQuantidadeDias(), new DateTime(dataLeitura.getTime())) ;

			numeroCicloLocal = referenciaCiclo.get("ciclo");
			anoMesFaturamentoLocal = referenciaCiclo.get("referencia");

			DateTime dataPartidaComPeriocididadeDateTime = new DateTime(dataPartidaComPeriocididade);

			mapaDatas = controladorCronogramaFaturamento.obterDatasSugestaoCronograma(dataPartidaComPeriocididadeDateTime, periodicidade,
							null, null, null, Boolean.TRUE, grupoFaturamento.getTipoLeitura(), numeroCicloLocal, null);

			dataPartidaComPeriocididade = (Date) mapaDatas.get("dataPrevista");

		}

		if (!dataLeituraMaior && periodicidade.getConsideraMesCivil()) {

			anoMesFaturamentoLocal = (Integer) mapaDatas.get("anoMesInicial");
			numeroCicloLocal = (Integer) mapaDatas.get("ciclo");
		}

		mapaReferenciasLeitura.put("anoMesFaturamento", anoMesFaturamentoLocal);
		mapaReferenciasLeitura.put("numeroCiclo", numeroCicloLocal);
		mapaReferenciasLeitura.put("dataFim", dataPartidaComPeriocididade);
		listaMapaReferenciaLeitura.add(mapaReferenciasLeitura);

		return listaMapaReferenciaLeitura;
	}

	/**
	 * Validar corretor vazao instalado.
	 *
	 * @param listarHistoricoCorretorVazao
	 *            the listar historico corretor vazao
	 * @param codigoOperacaoMedidorRetirada
	 *            the codigo operacao medidor retirada
	 * @param codigoOperacaoMedidorInstalacao
	 *            the codigo operacao medidor instalacao
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarCorretorVazaoInstalado(List<VazaoCorretorHistoricoOperacao> listarHistoricoCorretorVazao,
					Long codigoOperacaoMedidorRetirada, Long codigoOperacaoMedidorInstalacao,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		Boolean isMedicaoAntesInstalacao = Boolean.FALSE;
		Boolean isMedicaoDepoisRetirada = Boolean.FALSE;

		for (VazaoCorretorHistoricoOperacao vazaoCorretorHistoricoOperacao : listarHistoricoCorretorVazao) {

			// instalação corretor de vazão
			if (vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getChavePrimaria() == codigoOperacaoMedidorInstalacao) {
				if (supervisorioMedicaoDiaria.getDataRealizacaoLeitura().compareTo(vazaoCorretorHistoricoOperacao.getDataRealizada())
						< CONSTANTE_NUMERO_ZERO) {

					isMedicaoAntesInstalacao = Boolean.TRUE;
					break;
				}
			} else
				// retirada corretor de vazão
			if (vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getChavePrimaria() == codigoOperacaoMedidorRetirada
							&& supervisorioMedicaoDiaria.getDataRealizacaoLeitura().compareTo(
											vazaoCorretorHistoricoOperacao.getDataRealizada()) > CONSTANTE_NUMERO_ZERO) {

				isMedicaoDepoisRetirada = Boolean.TRUE;
				break;
			}
		}

		if (isMedicaoAntesInstalacao || isMedicaoDepoisRetirada) {

			// Gera anormalidade
			SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this
							.obterSupervisorioMedicaoAnormalidade(Constantes.ANORMALIDADE_SUPERVISORIO_PC_SEM_CORRETOR_VAZAO_INSTALADO);
			supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
		}

	}

	/**
	 * Consulta o primeiro registro do supervisorio medicao horaria, filtrando de acordo com os parametros
	 *
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigo ponto consumo supervisorio
	 * @param dataRealizacaoLeitura
	 *            the data realizacao leitura
	 * @param indicadorConsolidada
	 *            the indicador consolidada
	 * @param indicadorTipoMedicao
	 *            the indicador tipo medicao
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<SupervisorioMedicaoHoraria> consultarSupervisorioMedicaoHorariaConsolidado(String codigoPontoConsumoSupervisorio,
					Date dataRealizacaoLeitura, Boolean indicadorConsolidada, String indicadorTipoMedicao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" smh ");
		hql.append(" where smh.habilitado = true ");

		if (codigoPontoConsumoSupervisorio != null) {
			hql.append(" and smh.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio ");
		}

		if (dataRealizacaoLeitura != null) {
			hql.append(" and smh.dataRealizacaoLeitura < :dataInicialRealizacaoLeitura ");
		}

		hql.append(" and smh.indicadorConsolidada = :indicadorConsolidada ");
		hql.append(" and smh.indicadorTipoMedicao = :indicadorTipoMedicao ");
		hql.append(ORDENACAO_POR_DATA_REALIZACAO_LEITURA);

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoPontoConsumoSupervisorio != null) {
			query.setString("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio);
		}

		if (dataRealizacaoLeitura != null) {
			query.setDate(DATA_INICIAL_REALIZACAO_LEITURA, dataRealizacaoLeitura);

		}
		query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
		query.setString("indicadorTipoMedicao", indicadorTipoMedicao);
		query.setMaxResults(CONSTANTE_NUMERO_UM);

		return query.list();
	}

	/**
	 * Realiza a contagem da quantidade de entidades de SupervisorioMedicaoHoraria,
	 * filtrando de acordo com os parametros.
	 *
	 * @param codigoPontoConsumoSupervisorio {@link String}
	 * @param dataRealizacaoLeitura {@link Date}
	 * @param indicadorConsolidada {@link Boolean}
	 * @param indicadorTipoMedicao {@link String}
	 * @return Long - quantidade de registros da entidade SupervisorioMedicaoHoraria
	 */
	public Long contarSupervisorioMedicaoHorariaConsolidado(String codigoPontoConsumoSupervisorio,
					Date dataRealizacaoLeitura, Boolean indicadorConsolidada, String indicadorTipoMedicao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(smh) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" smh ");
		hql.append(" where smh.habilitado = true ");

		if (codigoPontoConsumoSupervisorio != null) {
			hql.append(" and smh.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio ");
		}

		if (dataRealizacaoLeitura != null) {
			hql.append(" and smh.dataRealizacaoLeitura < :dataInicialRealizacaoLeitura ");
		}

		hql.append(" and smh.indicadorConsolidada = :indicadorConsolidada ");
		hql.append(" and smh.indicadorTipoMedicao = :indicadorTipoMedicao ");
		hql.append(ORDENACAO_POR_DATA_REALIZACAO_LEITURA);

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoPontoConsumoSupervisorio != null) {
			query.setString("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio);
		}

		if (dataRealizacaoLeitura != null) {
			query.setDate(DATA_INICIAL_REALIZACAO_LEITURA, dataRealizacaoLeitura);

		}

		query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
		query.setString("indicadorTipoMedicao", indicadorTipoMedicao);

		return (Long) query.uniqueResult();
	}

	/**
	 * Consultar supervisorio medicao anormalidade.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<SupervisorioMedicaoAnormalidade> consultarSupervisorioMedicaoAnormalidade(Map<String, Object> filtro)
					throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeSupervisorioMedicaoAnormalidade());

		if (filtro != null) {
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

		}

		return criteria.list();
	}

	/**
	 * Consultar supervisorio medicao horaria.
	 *
	 * @param filtro
	 *            podendo ser: habilitado, indicadorConsolidada, indicadorTipoMedicao,
	 *            dataInicialRealizacaoLeitura e dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Lista de Medições Horárias do Supervisório
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<SupervisorioMedicaoHoraria> consultarSupervisorioMedicaoHoraria(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		Query query = null;
		Boolean habilitado = null;
		Boolean possuiAnormalidadeImpeditiva = null;
		Boolean indicadorConsolidada = null;
		Boolean naoConsolidadoOuPossuiAnormalidadeImpeditiva = null;
		String indicadorTipoMedicao = null;
		String codigoPontoConsumoSupervisorio = null;
		Date dataInicialRealizacaoLeitura = null;
		Date dataFinalRealizacaoLeitura = null;
		Date dataRealizacaoLeitura = null;
		Long supervisorioMedicaoDiaria = null;
		Long codigoStatusAlcada = null;
		Boolean decrementarDataInicial = null;
		Long statusAutorizado = null;
		Long chavePrimaria = null;
		String dataReferenciaCicloInicial = null;
		String dataReferenciaCicloFinal = null;
		Boolean isDesconsolidarSupervisorio = null;

		if (filtro != null) {
			possuiAnormalidadeImpeditiva = (Boolean) filtro.get("possuiAnormalidadeImpeditiva");
			dataReferenciaCicloInicial = (String) filtro.get(DATA_REFERENCIA_CICLO_INICIAL);
			dataReferenciaCicloFinal = (String) filtro.get(DATA_REFERENCIA_CICLO_FINAL);
			isDesconsolidarSupervisorio = (Boolean) filtro.get("isDesconsolidarSupervisorio");
			naoConsolidadoOuPossuiAnormalidadeImpeditiva = (Boolean) filtro.
							get(NAO_CONSOLIDADO_OU_POSSUI_ANORMALIDADE);
		}

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" smh ");

		if ((dataReferenciaCicloInicial != null && dataReferenciaCicloFinal != null) || (possuiAnormalidadeImpeditiva != null)
						|| (isDesconsolidarSupervisorio != null)) {
			hql.append("inner join fetch smh.supervisorioMedicaoDiaria supervisorioMedicaoDiaria ");
		}

		if (possuiAnormalidadeImpeditiva != null) {
			hql.append("left join fetch smh.supervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade ");

			hql.append("left join fetch supervisorioMedicaoDiaria.supervisorioMedicaoAnormalidade sma ");
		}

		if (naoConsolidadoOuPossuiAnormalidadeImpeditiva != null) {

			hql.append("left join fetch smh.supervisorioMedicaoDiaria supervisorioMedicaoDiaria ");

			hql.append("left join fetch smh.supervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade ");

			hql.append("left join fetch supervisorioMedicaoDiaria.supervisorioMedicaoAnormalidade sma ");
		}

		hql.append(" where 1 = 1 ");

		if (filtro != null && !filtro.isEmpty()) {
			habilitado = (Boolean) filtro.get(HABILITADO);
			indicadorConsolidada = (Boolean) filtro.get(INDICADOR_CONSOLIDADA);
			indicadorTipoMedicao = (String) filtro.get(INDICADOR_TIPO_MEDICAO);
			codigoPontoConsumoSupervisorio = (String) filtro.get(CODIGO_PONTO_CONSUMO_SUPERVISORIO);
			dataInicialRealizacaoLeitura = (Date) filtro.get(DATA_INICIAL_REALIZACAO_LEITURA);
			dataFinalRealizacaoLeitura = (Date) filtro.get(DATA_FINAL_REALIZACAO_LEITURA);
			dataRealizacaoLeitura = (Date) filtro.get(DATA_REALIZACAO_LEITURA);
			supervisorioMedicaoDiaria = (Long) filtro.get(SUPERVISORIO_MEDICAO_DIARIA);
			codigoStatusAlcada = (Long) filtro.get(CODIGO_STATUS_ALCADA);
			decrementarDataInicial = (Boolean) filtro.get("decrementarDataInicial");
			statusAutorizado = (Long) filtro.get(STATUS_AUTORIZADO);
			chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura != null) {
				hql.append(" and smh.dataRealizacaoLeitura between :dataInicialRealizacaoLeitura and :dataFinalRealizacaoLeitura ");
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura == null) {
				hql.append(" and smh.dataRealizacaoLeitura >= :dataInicialRealizacaoLeitura ");
			}

			if (dataRealizacaoLeitura != null) {
				hql.append(" and smh.dataRealizacaoLeitura = :dataRealizacaoLeitura ");
			}

			if (indicadorConsolidada != null) {

				hql.append(" and smh.indicadorConsolidada = :indicadorConsolidada ");
			}

			if (statusAutorizado != null) {
				hql.append(" and (smh.status = :statusAutorizado or smh.status is null) ");
			}

			if (indicadorTipoMedicao != null) {
				hql.append(" and smh.indicadorTipoMedicao = :indicadorTipoMedicao ");
			}

			if (habilitado != null) {
				hql.append(" and smh.habilitado = :habilitado ");
			}

			if (codigoPontoConsumoSupervisorio != null) {
				hql.append(" and  smh.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio");
			}

			if (supervisorioMedicaoDiaria != null && supervisorioMedicaoDiaria > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and smh.supervisorioMedicaoDiaria.chavePrimaria = :supervisorioMedicaoDiaria ");
			}

			if (codigoStatusAlcada != null) {
				hql.append(" and  smh.status.chavePrimaria = :codigoStatusAlcada");
			}

			if (chavePrimaria != null) {
				hql.append(" and  smh.chavePrimaria <> :chavePrimaria");
			}

			if (possuiAnormalidadeImpeditiva != null) {
				hql.append(" and  ( (supervisorioMedicaoAnormalidade is not null and supervisorioMedicaoAnormalidade."
								+ "indicadorImpedeProcessamento = :possuiAnormalidadeImpeditiva ) or "
								+ "(sma is not null and sma.indicadorImpedeProcessamento = :possuiAnormalidadeImpeditiva)) ");
			}

			if (naoConsolidadoOuPossuiAnormalidadeImpeditiva != null) {
				hql.append(" and  ( "
					+ "		( "
					+ " 		(supervisorioMedicaoAnormalidade is not null and "
					+ " 		supervisorioMedicaoAnormalidade.indicadorImpedeProcessamento = true ) "
					+ " 		or "
					+ " 		(sma is not null and sma.indicadorImpedeProcessamento = true) "
					+ " 	) "
					+ " or "
					+ " 	(smh.indicadorConsolidada = false) "
					+ ") ");
			}

			if (dataReferenciaCicloInicial != null && dataReferenciaCicloFinal != null) {

				hql.append(" AND (concat(str(supervisorioMedicaoDiaria.dataReferencia),str(supervisorioMedicaoDiaria.numeroCiclo)))"
								+ " between :dataReferenciaCicloInicial AND :dataReferenciaCicloFinal ");
			}

			if (filtro.get("status") != null && (Long) filtro.get("status") > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and  smh.status.chavePrimaria = ").append(filtro.get("status")).append(" ");
			}

			if (filtro.get("chavesPrimariasPendentes") != null) {
				hql.append(" and smh.chavePrimaria in (").append(filtro.get("chavesPrimariasPendentes").toString()).append(")");
			}

			if (isDesconsolidarSupervisorio != null) {
				hql.append(" and supervisorioMedicaoDiaria.indicadorIntegrado = :indicadorIntegrado ");
				hql.append(" and supervisorioMedicaoDiaria.indicadorProcessado = :indicadorProcessado ");
				if (filtro.get("numeroCiclo") != null) {
					hql.append(" and supervisorioMedicaoDiaria.numeroCiclo = :numeroCiclo ");
				}
				if (filtro.get("anoMesReferenciaLido") != null) {
					hql.append(" and supervisorioMedicaoDiaria.dataReferencia = :anoMesReferenciaLido ");
				}
			}
		}

		if (ordenacao != null) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null && !filtro.isEmpty()) {

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura != null) {

				if (decrementarDataInicial != null && decrementarDataInicial) {
					Util.adicionarRestricaoDataSemHora(query, dataInicialRealizacaoLeitura, DATA_INICIAL_REALIZACAO_LEITURA, Boolean.TRUE);
					Util.adicionarRestricaoDataSemHora(query, dataFinalRealizacaoLeitura, DATA_FINAL_REALIZACAO_LEITURA, Boolean.FALSE);

				} else {

					query.setTimestamp(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
					query.setTimestamp(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);

				}

			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura == null) {
				query.setDate(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
			}

			if (dataRealizacaoLeitura != null) {
				query.setTimestamp(DATA_REALIZACAO_LEITURA, dataRealizacaoLeitura);
			}

			if (indicadorConsolidada != null) {
				query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
			}

			if (statusAutorizado != null) {
				query.setLong(STATUS_AUTORIZADO, statusAutorizado);
			}

			if (indicadorTipoMedicao != null) {
				query.setString(INDICADOR_TIPO_MEDICAO, indicadorTipoMedicao);
			}

			if (habilitado != null) {
				query.setBoolean(HABILITADO, habilitado);
			}

			if (codigoPontoConsumoSupervisorio != null) {
				query.setString(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			}

			if (supervisorioMedicaoDiaria != null && supervisorioMedicaoDiaria > CONSTANTE_NUMERO_ZERO) {
				query.setLong(SUPERVISORIO_MEDICAO_DIARIA, supervisorioMedicaoDiaria);
			}

			if (codigoStatusAlcada != null) {
				query.setLong(CODIGO_STATUS_ALCADA, codigoStatusAlcada);
			}

			if (chavePrimaria != null) {
				query.setLong(CHAVE_PRIMARIA, chavePrimaria);
			}

			if (possuiAnormalidadeImpeditiva != null) {
				query.setBoolean("possuiAnormalidadeImpeditiva", possuiAnormalidadeImpeditiva);
			}

			if (dataReferenciaCicloInicial != null && dataReferenciaCicloFinal != null) {
				query.setString(DATA_REFERENCIA_CICLO_INICIAL, dataReferenciaCicloInicial);
				query.setString(DATA_REFERENCIA_CICLO_FINAL, dataReferenciaCicloFinal);
			}

			if (isDesconsolidarSupervisorio != null) {
				query.setParameter(INDICADOR_INTEGRADO, Boolean.FALSE);
				query.setParameter("indicadorProcessado", Boolean.FALSE);
				if (filtro.get("numeroCiclo") != null) {
					query.setLong("numeroCiclo", (Long) filtro.get("numeroCiclo"));
				}
				if (filtro.get("anoMesReferenciaLido") != null) {
					query.setLong("anoMesReferenciaLido", (Long) filtro.get("anoMesReferenciaLido"));
				}
			}

			// Paginação em banco dados
			ColecaoPaginada colecaoPaginada = null;
			if (filtro.containsKey("colecaoPaginada")) {
				colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

				HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query,
								getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName(), hql.toString());

			}

			// Quantidade mínima para autorizações pendentes
			if (filtro.containsKey("isQtdeMinimaPendente") && filtro.get("isQtdeMinimaPendente") != null) {

				Boolean isQtdeMinimaPendente = (Boolean) filtro.get("isQtdeMinimaPendente");

				if (isQtdeMinimaPendente) {

					query.setMaxResults(CONSTANTE_NUMERO_DEZ);

				}
			}


			if (colecaoPaginada != null) {
				if(colecaoPaginada.getIndex() == CONSTANTE_NUMERO_UM){
					query.setFirstResult(CONSTANTE_NUMERO_ZERO);
				}else{
					query.setFirstResult(colecaoPaginada.getIndex());
				}
				query.setMaxResults(colecaoPaginada.getObjectsPerPage());
			}

		}



		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > CONSTANTE_NUMERO_ZERO) {

			lista = super.inicializarLazyColecao(lista, propriedadesLazy);

		}

		return (Collection<SupervisorioMedicaoHoraria>) (Collection<?>) lista;
	}

	/**
	 * Realiza a contagem da quantidade de entidades de SupervisorioMedicaoHoraria,
	 * filtrando de acordo com as entradas do {@code filtro}
	 * @param filtro
	 *            podendo ser: habilitado, indicadorConsolidada, indicadorTipoMedicao,
	 *            dataInicialRealizacaoLeitura e dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio
	 * @return Long - quantidade de entidades SupervisorioMedicaoHoraria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Long contarSupervisorioMedicaoHoraria(Map<String, Object> filtro) throws NegocioException {

		Query query = null;

		Boolean habilitado = null;
		String indicadorTipoMedicao = null;
		String codigoPontoConsumoSupervisorio = null;
		Date dataRealizacaoLeitura = null;
		Long statusAutorizado = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(smh) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" smh ");

		hql.append(" where 1 = 1 ");

		if (filtro != null && !filtro.isEmpty()) {
			habilitado = (Boolean) filtro.get(HABILITADO);
			indicadorTipoMedicao = (String) filtro.get(INDICADOR_TIPO_MEDICAO);
			codigoPontoConsumoSupervisorio = (String) filtro.get(CODIGO_PONTO_CONSUMO_SUPERVISORIO);
			dataRealizacaoLeitura = (Date) filtro.get(DATA_REALIZACAO_LEITURA);
			statusAutorizado = (Long) filtro.get(STATUS_AUTORIZADO);


			if (dataRealizacaoLeitura != null) {
				hql.append(" and smh.dataRealizacaoLeitura = :dataRealizacaoLeitura ");
			}

			if (statusAutorizado != null) {
				hql.append(" and (smh.status = :statusAutorizado or smh.status is null) ");
			}

			if (indicadorTipoMedicao != null) {
				hql.append(" and smh.indicadorTipoMedicao = :indicadorTipoMedicao ");
			}

			if (habilitado != null) {
				hql.append(" and smh.habilitado = :habilitado ");
			}

			if (codigoPontoConsumoSupervisorio != null) {
				hql.append(" and  smh.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio");
			}
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null && !filtro.isEmpty()) {

			if (dataRealizacaoLeitura != null) {
				query.setTimestamp(DATA_REALIZACAO_LEITURA, dataRealizacaoLeitura);
			}

			if (statusAutorizado != null) {
				query.setLong(STATUS_AUTORIZADO, statusAutorizado);
			}

			if (indicadorTipoMedicao != null) {
				query.setString(INDICADOR_TIPO_MEDICAO, indicadorTipoMedicao);
			}

			if (habilitado != null) {
				query.setBoolean(HABILITADO, habilitado);
			}

			if (codigoPontoConsumoSupervisorio != null) {
				query.setString(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			}
		}

		return (Long) query.uniqueResult();
	}




	/**
	 * Consultar supervisorio medicao diaria.
	 *
	 * @param filtro
	 *            podendo ser: habilitado, indicadorConsolidada, indicadorTipoMedicao,
	 *            dataInicialRealizacaoLeitura e dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Lista de Medições Horárias do Supervisório
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<SupervisorioMedicaoDiaria> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Query query = null;
		Boolean possuiAnormalidadeImpeditiva = null;
		Boolean naoConsolidadaOuPossuiAnormalidadeImpeditiva = null;
		Boolean habilitado = null;
		Boolean indicadorProcessado = null;
		String indicadorIntegrado = null;
		String codigoPontoConsumoSupervisorio = null;
		Date dataInicialRealizacaoLeitura = null;
		Date dataFinalRealizacaoLeitura = null;
		Date dataRealizacaoLeitura = null;
		Long codigoStatusAlcada = null;
		Long chavePrimaria = null;
		String referencia = null;
		Long ciclo = null;
		Long[] chaveRota = null;
		Boolean ocorrencia = null;
		Long[] idsOcorrenciaMedicao = null;
		Boolean analisada = null;
		Boolean transferencia = null;
		Long anoMesReferenciaLido = null;
		Long idCiclo = null;
		String enderecoRemotoLido = null;
		Date dataLeituraInicial = null;
		Date dataLeituraFinal = null;
		Date dataInclusaoInicial = null;
		Date dataInclusaoFinal = null;
		Boolean ativo = null;
		Boolean isReferenciaNull = null;
		Boolean isCicloNull = null;
		Long codigoStatusAlcadaDiferente = null;
		Boolean isReferenciaNotNull = null;
		Boolean isCicloNotNull = null;
		Long statusAutorizado = null;
		Date dataMaiorRealizacaoLeitura = null;
		Boolean indicadorConsolidada = null;
		Long status = null;
		Date dataIntegracaoInicial = null;
		Date dataIntegracaoFinal = null;
		Long[] chavesSupervisorioMedicaoDiaria = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName());
		hql.append(" smd ");

		if (!filtro.isEmpty()) {
			possuiAnormalidadeImpeditiva = (Boolean) filtro.get("possuiAnormalidadeImpeditiva");
			naoConsolidadaOuPossuiAnormalidadeImpeditiva = (Boolean) filtro.get("naoConsolidadaOuPossuiAnormalidadeImpeditiva");
			ativo = (Boolean) filtro.get(ATIVO);
			habilitado = (Boolean) filtro.get(HABILITADO);
			indicadorConsolidada = (Boolean) filtro.get(INDICADOR_CONSOLIDADA);
			indicadorProcessado = (Boolean) filtro.get(INDICADOR_PROCESSADO);
			indicadorIntegrado = (String) filtro.get(INDICADOR_INTEGRADO);
			dataRealizacaoLeitura = (Date) filtro.get(DATA_REALIZACAO_LEITURA);
			codigoPontoConsumoSupervisorio = (String) filtro.get(CODIGO_PONTO_CONSUMO_SUPERVISORIO);
			dataInicialRealizacaoLeitura = (Date) filtro.get(DATA_INICIAL_REALIZACAO_LEITURA);
			dataFinalRealizacaoLeitura = (Date) filtro.get(DATA_FINAL_REALIZACAO_LEITURA);
			codigoStatusAlcada = (Long) filtro.get(CODIGO_STATUS_ALCADA);
			chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			referencia = (String) filtro.get(DATA_REFERENCIA);
			ciclo = (Long) filtro.get(NUMERO_CICLO);
			chaveRota = (Long[]) filtro.get("chaveRota");
			ocorrencia = (Boolean) filtro.get(OCORRENCIA);
			idsOcorrenciaMedicao = (Long[]) filtro.get(IDS_OCORRENCIA_MEDICAO);
			analisada = (Boolean) filtro.get(ANALISADA);
			transferencia = (Boolean) filtro.get(TRANSFERENCIA);
			anoMesReferenciaLido = (Long) filtro.get(ANO_MES_REFERENCIA_LIDO);
			idCiclo = (Long) filtro.get(ID_CICLO);
			enderecoRemotoLido = (String) filtro.get(ENDERECO_REMOTO_LIDO);
			dataLeituraInicial = (Date) filtro.get(DATA_LEITURA_INICIAL);
			dataLeituraFinal = (Date) filtro.get(DATA_LEITURA_FINAL);
			dataInclusaoInicial = (Date) filtro.get(DATA_INCLUSAO_INICIAL);
			dataInclusaoFinal = (Date) filtro.get(DATA_INCLUSAO_FINAL);
			isReferenciaNull = (Boolean) filtro.get("isReferenciaNull");
			isCicloNull = (Boolean) filtro.get("isCicloNull");
			codigoStatusAlcadaDiferente = (Long) filtro.get(CODIGO_STATUS_ALCADA_DIFERENTE);
			isReferenciaNotNull = (Boolean) filtro.get("isReferenciaNotNull");
			isCicloNotNull = (Boolean) filtro.get("isCicloNotNull");
			statusAutorizado = (Long) filtro.get(STATUS_AUTORIZADO);
			dataMaiorRealizacaoLeitura = (Date) filtro.get("dataMaiorRealizacaoLeitura");
			status = (Long) filtro.get("status");
			dataIntegracaoInicial = (Date) filtro.get(DATA_INTEGRACAO_INICIAL);
			dataIntegracaoFinal = (Date) filtro.get(DATA_INTEGRACAO_FINAL);
			chavesSupervisorioMedicaoDiaria = (Long[]) filtro.get("chavesSupervisorioMedicaoDiaria");

			if (dataIntegracaoInicial != null) {
				dataLeituraInicial = dataIntegracaoInicial;
			}
			if (dataIntegracaoFinal != null) {
				dataLeituraFinal = dataIntegracaoFinal;
			}

			if ((idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO)
							|| naoConsolidadaOuPossuiAnormalidadeImpeditiva != null ) {

				hql.append("left join fetch smd.supervisorioMedicaoAnormalidade suma ");

			}

			hql.append(" where 1 = 1 ");

			if (chaveRota != null) {
				hql.append(" and smd.codigoPontoConsumoSupervisorio in (");
				hql.append(" select pocn.codigoPontoConsumoSupervisorio ");
				hql.append(" from ");
				hql.append(getClasseEntidadePontoConsumo().getSimpleName()).append(" pocn ");
				hql.append(" where pocn.codigoPontoConsumoSupervisorio = smd.codigoPontoConsumoSupervisorio ");
				hql.append(" and pocn.rota.chavePrimaria in (:chaveRota) ");
				hql.append(") ");
			}

			if (referencia != null) {
				hql.append(" and smd.dataReferencia = :dataReferencia ");
			}

			if (ciclo != null) {
				hql.append(" and smd.numeroCiclo = :numeroCiclo ");
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura != null) {
				hql.append(" and smd.dataRealizacaoLeitura between :dataInicialRealizacaoLeitura and :dataFinalRealizacaoLeitura ");
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura == null) {
				hql.append(" and smd.dataRealizacaoLeitura between :dataAnteriorRealizacaoLeituraInicial and :dataAtualRealizacaoLeituraFinal ");
			}

			if (dataInicialRealizacaoLeitura == null && dataFinalRealizacaoLeitura != null) {
				hql.append(" and smd.dataRealizacaoLeitura < :dataFinalRealizacaoLeitura  ");
			}

			if (dataRealizacaoLeitura != null) {
				hql.append(" and smd.dataRealizacaoLeitura = :dataRealizacaoLeitura ");
			}

			if (dataMaiorRealizacaoLeitura != null) {
				hql.append(" and smd.dataRealizacaoLeitura > :dataMaiorRealizacaoLeitura ");
			}

			if (indicadorProcessado != null) {
				hql.append(" and smd.indicadorProcessado = :indicadorProcessado ");
			}

			if (indicadorIntegrado != null) {
				hql.append(" and smd.indicadorIntegrado = :indicadorIntegrado ");
			}

			if (indicadorConsolidada != null) {
				hql.append(" and smd.indicadorConsolidada = :indicadorConsolidada ");
			}

			if (ativo != null) {
				hql.append(" and smd.habilitado = :ativo ");
			}

			if (habilitado != null) {
				hql.append(" and smd.habilitado = :habilitado ");
			}

			if (codigoPontoConsumoSupervisorio != null) {
				hql.append(" and  smd.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio");
			}

			if (codigoStatusAlcada != null) {
				hql.append(" and  smd.status.chavePrimaria = :codigoStatusAlcada");
			}

			if (codigoStatusAlcadaDiferente != null) {
				hql.append(" and  (smd.status.chavePrimaria <> :codigoStatusAlcadaDiferente or smd.status.chavePrimaria is null)");
			}

			if (chavePrimaria != null) {
				hql.append(" and  smd.chavePrimaria <> :chavePrimaria");
			}

			if (ocorrencia != null) {
				if (ocorrencia.equals(Boolean.TRUE)) {
					hql.append(" and smd.supervisorioMedicaoAnormalidade is not null ");
				} else if (ocorrencia.equals(Boolean.FALSE)) {
					hql.append(" and smd.supervisorioMedicaoAnormalidade is null ");
				}
			}

			if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and suma.chavePrimaria in (:idsOcorrenciaMedicao) ");
			}

			if (possuiAnormalidadeImpeditiva != null) {
				hql.append(" and  (smd.supervisorioMedicaoAnormalidade is not null and "
								+ "smd.supervisorioMedicaoAnormalidade.indicadorImpedeProcessamento = :possuiAnormalidadeImpeditiva ) ");
			}

			if (naoConsolidadaOuPossuiAnormalidadeImpeditiva != null) {
				hql.append(" and ( "
						+ " (suma is not null and "
						+ "suma.indicadorImpedeProcessamento = true ) "
						+ " or "
						+ " ( smd.indicadorConsolidada = false ) "
						+ " ) ");
			}

			if (analisada != null) {
				hql.append(" and smd.indicadorIntegrado = :analisada ");
			}

			if (transferencia != null) {
				hql.append(" and smd.indicadorProcessado = :transferencia ");
			}

			if (anoMesReferenciaLido != null && anoMesReferenciaLido > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and smd.dataReferencia = :anoMesReferenciaLido ");
			}

			if (isReferenciaNull != null && isReferenciaNull) {
				hql.append(" and smd.dataReferencia is null ");
			}

			if (isReferenciaNotNull != null && isReferenciaNotNull) {
				hql.append(" and smd.dataReferencia is not null ");
			}

			if (idCiclo != null && idCiclo > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and smd.numeroCiclo = :idCiclo ");
			}

			if (isCicloNull != null && isCicloNull) {
				hql.append(" and smd.numeroCiclo is null ");
			}

			if (isCicloNotNull != null && isCicloNotNull) {
				hql.append(" and smd.numeroCiclo is not null ");
			}

			if (!StringUtils.isEmpty(enderecoRemotoLido)) {
				hql.append(" and upper(smd.codigoPontoConsumoSupervisorio) like upper(:enderecoRemotoLido) ");
			}

			if (dataLeituraInicial != null && dataLeituraFinal != null) {
				hql.append(" and (smd.dataRealizacaoLeitura between :dataLeituraInicial and :dataLeituraFinal) ");
			}

			if (dataInclusaoInicial != null && dataInclusaoFinal != null) {
				hql.append(" and (smd.dataRegistroLeitura between :dataInclusaoInicial and :dataInclusaoFinal) ");
			}

			if (statusAutorizado != null) {

				hql.append(" and (smd.status = :statusAutorizado or smd.status is null) ");
			}

			if (status != null && status > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and  smd.status.chavePrimaria = :statusSupervisorio ");
			}

			if (filtro.get("chavePrimariaDiaria") != null && !((Map<Long, Long>) filtro.get("chavePrimariaDiaria")).isEmpty()) {
				Map<Long, Long> chavesPrimarias = (Map<Long, Long>) filtro.get("chavePrimariaDiaria");

				hql.append(" and smd.chavePrimaria in (").append(retornaChavesPrimarias(chavesPrimarias)).append(")");
			}

			if (chavesSupervisorioMedicaoDiaria != null && chavesSupervisorioMedicaoDiaria.length > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and smd.chavePrimaria in ( :chavesSupervisorioMedicaoDiaria ) ");
			}

		}

		if (ordenacao != null) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!filtro.isEmpty()) {

			if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO) {
				query.setParameterList(IDS_OCORRENCIA_MEDICAO, idsOcorrenciaMedicao);
			}
			if (analisada != null) {
				query.setBoolean(ANALISADA, analisada);
			}
			if (transferencia != null) {
				query.setBoolean(TRANSFERENCIA, transferencia);
			}
			if (anoMesReferenciaLido != null && anoMesReferenciaLido > CONSTANTE_NUMERO_ZERO) {
				query.setLong(ANO_MES_REFERENCIA_LIDO, anoMesReferenciaLido);
			}
			if (idCiclo != null && idCiclo > CONSTANTE_NUMERO_ZERO) {
				query.setLong(ID_CICLO, idCiclo);
			}
			if (!StringUtils.isEmpty(enderecoRemotoLido)) {
				query.setString(ENDERECO_REMOTO_LIDO, enderecoRemotoLido);
			}
			if ((dataLeituraInicial != null) && (dataLeituraFinal != null)) {
				dataLeituraFinal = (Util.ultimoHorario(new DateTime(dataLeituraFinal))).toDate();
				Util.adicionarRestricaoDataSemHora(query, dataLeituraInicial, DATA_LEITURA_INICIAL, Boolean.TRUE);
				Util.adicionarRestricaoDataSemHora(query, dataLeituraFinal, DATA_LEITURA_FINAL, Boolean.FALSE);
			}
			if ((dataInclusaoInicial != null) && (dataInclusaoFinal != null)) {
				dataInclusaoFinal = (Util.ultimoHorario(new DateTime(dataInclusaoFinal))).toDate();
				Util.adicionarRestricaoDataSemHora(query, dataInclusaoInicial, DATA_INCLUSAO_INICIAL, Boolean.TRUE);
				Util.adicionarRestricaoDataSemHora(query, dataInclusaoFinal, DATA_INCLUSAO_FINAL, Boolean.FALSE);
			}

			if (dataMaiorRealizacaoLeitura != null) {
				query.setDate("dataMaiorRealizacaoLeitura", dataMaiorRealizacaoLeitura);
			}

			if (chaveRota != null) {
				query.setParameterList("chaveRota", chaveRota);
			}

			if (referencia != null) {
				query.setString(DATA_REFERENCIA, referencia);
			}

			if (ciclo != null) {
				query.setLong(NUMERO_CICLO, ciclo);
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura != null) {
				query.setDate(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
				query.setDate(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);

			}

			if (dataInicialRealizacaoLeitura == null && dataFinalRealizacaoLeitura != null) {
				query.setDate(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);

			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura == null) {

				Util.adicionarRestricaoDataSemHora(query, Util.decrementarDataComQuantidadeDias(dataInicialRealizacaoLeitura,
								Constantes.NUMERO_DIAS_INCREMENTAR_DATA), "dataAnteriorRealizacaoLeituraInicial", Boolean.TRUE);

				Util.adicionarRestricaoDataSemHora(query, dataInicialRealizacaoLeitura, "dataAtualRealizacaoLeituraFinal", Boolean.FALSE);

			}

			if (dataRealizacaoLeitura != null) {
				query.setDate(DATA_REALIZACAO_LEITURA, dataRealizacaoLeitura);
			}

			if (indicadorProcessado != null) {
				query.setBoolean(INDICADOR_PROCESSADO, indicadorProcessado);
			}

			if (indicadorIntegrado != null) {
				query.setString(INDICADOR_INTEGRADO, indicadorIntegrado);
			}

			if (indicadorConsolidada != null) {
				query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
			}

			if (ativo != null) {
				query.setBoolean(ATIVO, ativo);
			}

			if (habilitado != null) {
				query.setBoolean(HABILITADO, habilitado);
			}

			if (codigoPontoConsumoSupervisorio != null) {
				query.setString(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			}

			if (codigoStatusAlcada != null) {
				query.setLong(CODIGO_STATUS_ALCADA, codigoStatusAlcada);
			}

			if (codigoStatusAlcadaDiferente != null) {
				query.setLong(CODIGO_STATUS_ALCADA_DIFERENTE, codigoStatusAlcadaDiferente);
			}

			if (chavePrimaria != null) {
				query.setLong(CHAVE_PRIMARIA, chavePrimaria);
			}

			if (statusAutorizado != null) {
				query.setLong(STATUS_AUTORIZADO, statusAutorizado);
			}

			if (status != null && status > CONSTANTE_NUMERO_ZERO) {
				query.setLong("statusSupervisorio", status);
			}

			if (chavesSupervisorioMedicaoDiaria != null && chavesSupervisorioMedicaoDiaria.length > CONSTANTE_NUMERO_ZERO) {
				query.setParameterList("chavesSupervisorioMedicaoDiaria", chavesSupervisorioMedicaoDiaria);
			}

			if (possuiAnormalidadeImpeditiva != null) {
				query.setBoolean("possuiAnormalidadeImpeditiva", possuiAnormalidadeImpeditiva);
			}

			//Filtrar o mínimo(10) de registros supervisórios pendentes (Login)
			if (filtro.containsKey("isQtdeMinimaPendente") && filtro.get("isQtdeMinimaPendente") != null) {

				Boolean isQtdeMinimaPendente = (Boolean) filtro.get("isQtdeMinimaPendente");

				if (isQtdeMinimaPendente) {

					query.setMaxResults(CONSTANTE_NUMERO_DEZ);

				}
			}

		}


		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > CONSTANTE_NUMERO_ZERO) {

			lista = super.inicializarLazyColecao(lista, propriedadesLazy);

		}

		Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = (Collection<SupervisorioMedicaoDiaria>) (Collection<?>) lista;

		String temFatorZFixo = (String) controladorParametroSistema
			.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1);
		if ("1".equals(temFatorZFixo)) {
			for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
				if (supervisorioMedicaoDiaria.getFatorZ() == null
						|| supervisorioMedicaoDiaria.getFatorZ().compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO) {
					supervisorioMedicaoDiaria.setFatorZ(BigDecimal.ONE);
				}
			}
		}
		return listaSupervisorioMedicaoDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#listarSupervisorioMedicaoAnormalidade()
	 */
	@Override
	public Collection<SupervisorioMedicaoAnormalidade> listarSupervisorioMedicaoAnormalidade(String ocorrencia) {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoAnormalidade().getSimpleName());
		if (ocorrencia != null && !ocorrencia.isEmpty()) {
			hql.append(" where ");
			if ("true".equalsIgnoreCase(ocorrencia)) {
				hql.append(" habilitado = true ");
			} else {
				hql.append(" habilitado = false ");
			}
		}
		hql.append(" order by descricao");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#consultarSupervisorioMedicaoDiaria(java.util.Map, java.lang.Boolean)
	 */
	@Override
	public Collection<SupervisorioMedicaoVO> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro,
					Boolean isConsultaPorPontoConsumo) throws NegocioException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		Collection<SupervisorioMedicaoVO> retorno = new ArrayList<>();

		Class<?> entidadePontoConsumo = ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);

		Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
		Long idGrupoFaturamento = (Long) filtro.get("idGrupoFaturamento");
		Long[] idsRota = (Long[]) filtro.get("idsRota");
		Long idCliente = (Long) filtro.get("idCliente");
		Long matriculaImovel = (Long) filtro.get("matriculaImovel");
		String nomeImovel = (String) filtro.get("nomeImovel");
		String complementoImovel = (String) filtro.get("complementoImovel");
		String numeroImovel = (String) filtro.get("numeroImovel");
		Long idLocalidade = (Long) filtro.get("idLocalidade");
		Long idSetorComercial = (Long) filtro.get("idSetorComercial");
		String cepImovel = (String) filtro.get("cepImovel");

		StringBuilder hql = new StringBuilder();

		hql.append("select  (select distinct ponto.descricao from  ");
		hql.append(entidadePontoConsumo.getSimpleName());
		hql.append(" ponto ");
		hql.append("where sumd.codigoPontoConsumoSupervisorio = ponto.codigoPontoConsumoSupervisorio ");
		hql.append(" ) as pontoConsumo, ");

		hql.append("sumd.codigoPontoConsumoSupervisorio as codigoPontoConsumoSupervisorio, ");
		hql.append("sumd.dataReferencia as dataReferencia, ");
		hql.append("sumd.numeroCiclo as numeroCiclo, ");
		hql.append("count(distinct sumd.chavePrimaria) as qtdMedicaoDiaria, ");
		hql.append("max(suma.indicadorImpedeProcessamento) as anormalidadeImpeditiva, ");
		hql.append("min(enco.chavePrimaria) as statusDiaria, ");
		hql.append("min(sumd.indicadorProcessado) as transferido, ");
		hql.append("min(sumd.indicadorIntegrado) as indicadorIntegrado, ");
		hql.append("min(encoHoraria.chavePrimaria) as statusHoraria, ");
		hql.append("(select pontoConsumo.chavePrimaria from  ");
		hql.append(entidadePontoConsumo.getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append("where sumd.codigoPontoConsumoSupervisorio = pontoConsumo.codigoPontoConsumoSupervisorio ");
		hql.append(" ) as chavePontoConsumo ");
		hql.append("from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" sumh ");

		if (isConsultaPorPontoConsumo) {

			hql.append(" , ");
			hql.append(entidadePontoConsumo.getSimpleName());
			hql.append(" pocn ");

			if (idGrupoFaturamento != null && idGrupoFaturamento > CONSTANTE_NUMERO_ZERO) {
				hql.append("inner join pocn.rota rota ");
				hql.append("inner join rota.grupoFaturamento grupoFaturamento ");
			}

			hql.append("inner join pocn.imovel imovel ");
			if (indicadorCondominio != null && indicadorCondominio) {
				hql.append("inner join imovel.imovelCondominio imovelCondominio ");
			}

			if ((idCliente != null) && (idCliente > CONSTANTE_NUMERO_ZERO)) {
				hql.append("inner join imovel.listaClienteImovel clienteImovel ");
			}
		}

		hql.append("right join sumh.supervisorioMedicaoDiaria sumd ");
		hql.append("left join sumd.supervisorioMedicaoAnormalidade suma ");
		hql.append("left join sumd.status enco ");
		hql.append("left join sumh.status encoHoraria ");
		hql.append("where (sumd.indicadorMedidor is null or sumd.indicadorMedidor = false) ");

		// filtros do ponto de consumo
		if (isConsultaPorPontoConsumo) {

			hql.append(" and sumd.codigoPontoConsumoSupervisorio = pocn.codigoPontoConsumoSupervisorio ");

			if ((idCliente != null) && (idCliente > CONSTANTE_NUMERO_ZERO)) {
				hql.append(" and clienteImovel.cliente.chavePrimaria = :idCliente  ");
				hql.append(" and clienteImovel.relacaoFim is null ");
			}

			if (!StringUtils.isEmpty(cepImovel)) {
				hql.append(" and pocn.cep.cep = :cepImovel ");
			}

			if (indicadorCondominio != null) {
				if (indicadorCondominio) {
					hql.append(" and imovelCondominio.condominio = :indicadorCondominio ");
				} else {
					hql.append(" and imovel.condominio = :indicadorCondominio ");
				}
			}

			if (indicadorCondominio != null && indicadorCondominio) {
				if ((matriculaImovel != null) && (matriculaImovel > CONSTANTE_NUMERO_ZERO)) {
					hql.append(" and imovelCondominio.chavePrimaria = :matriculaImovel ");
				}
				if (!StringUtils.isEmpty(nomeImovel)) {
					hql.append(" and upper(imovelCondominio.nome) like upper(:nomeImovel) ");
				}

				if (!StringUtils.isEmpty(complementoImovel)) {
					hql.append(" and upper(imovelCondominio.descricaoComplemento) like upper(:complementoImovel) ");
				}
				if (!StringUtils.isEmpty(numeroImovel)) {
					hql.append(" and imovelCondominio.numeroImovel like :numeroImovel ");
				}

				if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
					hql.append(" and imovelCondominio.quadraFace.quadra.setorComercial.localidade.chavePrimaria = :idLocalidade ");
				}

				if (idSetorComercial != null && idSetorComercial > CONSTANTE_NUMERO_ZERO) {
					hql.append(" and imovelCondominio.quadraFace.quadra.setorComercial.chavePrimaria = :idSetorComercial ");
				}

			} else {

				if ((matriculaImovel != null) && (matriculaImovel > CONSTANTE_NUMERO_ZERO)) {
					hql.append(" and imovel.chavePrimaria = :matriculaImovel ");
				}

				if (!StringUtils.isEmpty(nomeImovel)) {
					hql.append(" and upper(imovel.nome) like upper(:nomeImovel) ");
				}

				if (!StringUtils.isEmpty(complementoImovel)) {
					hql.append(" and upper(imovel.descricaoComplemento) like upper(:complementoImovel) ");
				}

				if (!StringUtils.isEmpty(numeroImovel)) {
					hql.append(" and imovel.numeroImovel like :numeroImovel ");
				}

				if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
					hql.append(" and imovel.quadraFace.quadra.setorComercial.localidade.chavePrimaria = :idLocalidade ");
				}

				if (idSetorComercial != null && idSetorComercial > CONSTANTE_NUMERO_ZERO) {
					hql.append(" and imovel.quadraFace.quadra.setorComercial.chavePrimaria = :idSetorComercial ");
				}

			}

			if (idGrupoFaturamento != null && idGrupoFaturamento > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and pocn.rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
			}

			if (idsRota != null && idsRota.length > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and pocn.rota.chavePrimaria in (:idsRota) ");
			}

		}

		Boolean ocorrencia = (Boolean) filtro.get("ocorrencia");
		if (ocorrencia != null) {
			if (ocorrencia.equals(Boolean.TRUE)) {
				hql.append(" and sumd.supervisorioMedicaoAnormalidade is not null ");
			} else if (ocorrencia.equals(Boolean.FALSE)) {
				hql.append(" and sumd.supervisorioMedicaoAnormalidade is null ");
			}
		}

		Long[] idsOcorrenciaMedicao = (Long[]) filtro.get("idsOcorrenciaMedicao");
		if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and suma.chavePrimaria in (:idsOcorrenciaMedicao) ");
		}

		Boolean analisada = (Boolean) filtro.get("analisada");
		if (analisada != null) {
			hql.append(" and sumd.indicadorIntegrado = :analisada ");
		}

		Boolean indicadorConsolidada = (Boolean) filtro.get(INDICADOR_CONSOLIDADA);
		if (indicadorConsolidada != null) {
			hql.append(" and sumd.indicadorConsolidada = :indicadorConsolidada ");
		}

		Boolean transferencia = (Boolean) filtro.get("transferencia");
		if (transferencia != null) {
			hql.append(" and sumd.indicadorProcessado = :transferencia ");
		}

		Boolean ativo = (Boolean) filtro.get("ativo");
		if (ativo != null) {
			hql.append(" and sumd.habilitado = :ativo ");
		}

		Long status = (Long) filtro.get("status");
		if (status != null && status > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and sumd.status.chavePrimaria = :statusSupervisorio ");
		}

		Long anoMesReferenciaLido = (Long) filtro.get("anoMesReferenciaLido");
		if (anoMesReferenciaLido != null && anoMesReferenciaLido > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and sumd.dataReferencia = :anoMesReferenciaLido ");
		}

		Long idCiclo = (Long) filtro.get("idCiclo");
		if (idCiclo != null && idCiclo > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and sumd.numeroCiclo = :idCiclo ");
		}

		String enderecoRemotoLido = (String) filtro.get("enderecoRemotoLido");
		if (!StringUtils.isEmpty(enderecoRemotoLido)) {
			hql.append(" and upper(sumd.codigoPontoConsumoSupervisorio) like upper(:enderecoRemotoLido) ");
		}

		Date dataLeituraInicial = (Date) filtro.get("dataLeituraInicial");
		Date dataLeituraFinal = (Date) filtro.get("dataLeituraFinal");
		if (dataLeituraInicial != null && dataLeituraFinal != null) {
			hql.append(" and (sumd.dataRealizacaoLeitura between :dataLeituraInicial and :dataLeituraFinal) ");
		}

		Date dataInclusaoInicial = (Date) filtro.get("dataInclusaoInicial");
		Date dataInclusaoFinal = (Date) filtro.get("dataInclusaoFinal");
		if (dataInclusaoInicial != null && dataInclusaoFinal != null) {
			hql.append(" and (sumd.dataRegistroLeitura between :dataInclusaoInicial and :dataInclusaoFinal) ");
		}

		if (filtro.get("chavePrimariaDiaria") != null && !((Map<Long, Long>) filtro.get("chavePrimariaDiaria")).isEmpty()) {
			Map<Long, Long> chavePrimaria = (Map<Long, Long>) filtro.get("chavePrimariaDiaria");

			hql.append(" and sumd.chavePrimaria in (").append(retornaChavesPrimarias(chavePrimaria)).append(")");
		}

		if (filtro.get("chavesPrimariasPendentes") != null) {
			hql.append(" and sumd.chavePrimaria in (").append(filtro.get("chavesPrimariasPendentes").toString()).append(")");
		}

		hql.append("group by sumd.codigoPontoConsumoSupervisorio, sumd.dataReferencia, sumd.numeroCiclo ");
		hql.append("order by sumd.codigoPontoConsumoSupervisorio, sumd.dataReferencia desc, sumd.numeroCiclo desc, pontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		// filtros do ponto de consumo
		if (isConsultaPorPontoConsumo) {

			if ((idCliente != null) && (idCliente > CONSTANTE_NUMERO_ZERO)) {
				query.setLong("idCliente", idCliente);
			}
			if (!StringUtils.isEmpty(cepImovel)) {
				query.setString("cepImovel", cepImovel);
			}
			if ((matriculaImovel != null) && (matriculaImovel > CONSTANTE_NUMERO_ZERO)) {
				query.setLong("matriculaImovel", matriculaImovel);
			}
			if (indicadorCondominio != null) {
				query.setBoolean("indicadorCondominio", indicadorCondominio);
			}

			if (!StringUtils.isEmpty(nomeImovel)) {
				query.setString("nomeImovel", Util.formatarTextoConsulta(nomeImovel));
			}
			if (!StringUtils.isEmpty(complementoImovel)) {
				query.setString("complementoImovel", Util.formatarTextoConsulta(complementoImovel));
			}
			if (!StringUtils.isEmpty(numeroImovel)) {
				query.setString("numeroImovel", numeroImovel);
			}
			if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
				query.setLong("idLocalidade", idLocalidade);
			}
			if (idSetorComercial != null && idSetorComercial > CONSTANTE_NUMERO_ZERO) {
				query.setLong("idSetorComercial", idSetorComercial);
			}
			if (idGrupoFaturamento != null && idGrupoFaturamento > CONSTANTE_NUMERO_ZERO) {
				query.setLong("idGrupoFaturamento", idGrupoFaturamento);
			}
			if (idsRota != null && idsRota.length > CONSTANTE_NUMERO_ZERO) {
				query.setParameterList("idsRota", idsRota);
			}
		}

		if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO) {
			query.setParameterList("idsOcorrenciaMedicao", idsOcorrenciaMedicao);
		}
		if (analisada != null) {
			query.setBoolean("analisada", analisada);
		}
		if (indicadorConsolidada != null) {
			query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
		}
		if (transferencia != null) {
			query.setBoolean("transferencia", transferencia);
		}
		if (ativo != null) {
			query.setBoolean("ativo", ativo);
		}

		if (status != null && status > CONSTANTE_NUMERO_ZERO) {
			query.setLong("statusSupervisorio", status);
		}

		if (anoMesReferenciaLido != null && anoMesReferenciaLido > CONSTANTE_NUMERO_ZERO) {
			query.setLong("anoMesReferenciaLido", anoMesReferenciaLido);
		}
		if (idCiclo != null && idCiclo > CONSTANTE_NUMERO_ZERO) {
			query.setLong("idCiclo", idCiclo);
		}
		if (!StringUtils.isEmpty(enderecoRemotoLido)) {
			query.setString("enderecoRemotoLido", enderecoRemotoLido);
		}
		if ((dataLeituraInicial != null) && (dataLeituraFinal != null)) {
			dataLeituraFinal = (Util.ultimoHorario(new DateTime(dataLeituraFinal))).toDate();
			Util.adicionarRestricaoDataSemHora(query, dataLeituraInicial, "dataLeituraInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataLeituraFinal, "dataLeituraFinal", Boolean.FALSE);
		}
		if ((dataInclusaoInicial != null) && (dataInclusaoFinal != null)) {
			dataInclusaoFinal = (Util.ultimoHorario(new DateTime(dataInclusaoFinal))).toDate();
			Util.adicionarRestricaoDataSemHora(query, dataInclusaoInicial, "dataInclusaoInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataInclusaoFinal, "dataInclusaoFinal", Boolean.FALSE);
		}

		// Paginação em banco dados
		ColecaoPaginada colecaoPaginada = null;
		if (filtro.containsKey("colecaoPaginada")) {
			colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query, getClasseEntidadeSupervisorioMedicaoHoraria

			().getSimpleName(), hql.toString());
		}

		if (colecaoPaginada != null) {
			if(colecaoPaginada.getIndex() == CONSTANTE_NUMERO_UM){
				query.setFirstResult(CONSTANTE_NUMERO_ZERO);
			}else{
				query.setFirstResult(colecaoPaginada.getIndex());
			}
			query.setMaxResults(colecaoPaginada.getObjectsPerPage());
		}

		List<Object[]> listaSupervisorioMedicaoDiaria = query.list();

		// contador da chave primária
		Long i = 1L;

		for (Object[] supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
			SupervisorioMedicaoVO supervisorioMedicaoVO = new SupervisorioMedicaoVO();
			supervisorioMedicaoVO.setChavePrimaria(i);
			i++;

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_ZERO] != null) {
				supervisorioMedicaoVO.setPontoconsumo(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_ZERO].toString());
			}

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_UM] != null) {
				supervisorioMedicaoVO.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_UM].toString());
			}

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DOIS] != null) {
				supervisorioMedicaoVO.setDataReferencia(Integer.valueOf(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DOIS].toString()));
			}

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_TRES] != null) {
				supervisorioMedicaoVO.setNumeroCiclo(Integer.valueOf(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_TRES].toString()));
			}

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_QUATRO] != null) {
				supervisorioMedicaoVO.setQtdMedicao(Integer.valueOf(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_QUATRO].toString()));
			}


			//Verificando o Status para determinar Pendência ou Autorização
			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			EntidadeConteudo statusPendente = null;
			ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

			String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

			statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));


			/*
			 * supervisorioMedicaoDiaria[5] = retorna o valor máximo que a coluna suma.indicadorImpedeProcessamento pode receber
			 * para os registros
			 * diários agrupados
			 * supervisorioMedicaoDiaria[6] = retorna o valor mínimo que a coluna sumd.status pode receber para os registros
			 * diários agrupados
			 * supervisorioMedicaoDiaria[9] = retorna o valor mínimo que a coluna sumh.status pode receber para os registros
			 * horários
			 */

			if (supervisorioMedicaoDiaria[5] != null) {

				Boolean anormalidade = (Boolean) supervisorioMedicaoDiaria[5];

				// verifica se a anormalidade é impeditiva ou se os registros diários/horários estão autorizados
				if (anormalidade.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

					// seta anormalidade impeditiva
					supervisorioMedicaoVO.setAnormalidade("1");
				} else {

					// seta anormalidade não impeditiva
					supervisorioMedicaoVO.setAnormalidade("0");
				}

			}

			Long statusDiaria = (Long) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_SEIS];
			Long statusHoraria = (Long) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_NOVE];


			// verifica se os registros diários/horários estão autorizados pois não é permitido transferir um registro que não está
			// autorizado
			if ((statusDiaria != null && statusDiaria == statusPendente.getChavePrimaria())
							|| (statusHoraria != null && statusHoraria == statusPendente.getChavePrimaria())) {

				// seta anormalidade impeditiva
				supervisorioMedicaoVO.setAnormalidade("1");
			}

			// supervisorioMedicaoDiaria[7] = retorna o valor mínimo que a coluna sumd.indicadorProcessado pode receber para os registros
			// diários agrupados
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_SETE] != null) {

				Boolean transferido = (Boolean) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_SETE];

				if (transferido.equals(Constantes.INDICADOR_PROCESSADO)) {

					supervisorioMedicaoVO.setTransferido(Constantes.SIM);
					supervisorioMedicaoVO.setIndicadorProcessado(Boolean.TRUE);
				} else {

					supervisorioMedicaoVO.setTransferido(Constantes.NAO);
					supervisorioMedicaoVO.setIndicadorProcessado(Boolean.FALSE);
				}
			}

			// supervisorioMedicaoDiaria[8] = retorna o valor mínimo que a coluna sumd.indicadorProcessado pode receber para os registros
			// diários agrupados
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_OITO] != null) {

				Boolean indicadorIntegrado = (Boolean) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_OITO];

				if (indicadorIntegrado.equals(Constantes.INDICADOR_INTEGRADO)) {

					supervisorioMedicaoVO.setIntegrado(Constantes.SIM);
					supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.TRUE);
				} else {

					supervisorioMedicaoVO.setIntegrado(Constantes.NAO);
					supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.FALSE);
				}
			}

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DEZ] != null) {
				PontoConsumo pontoConsumo = controladorPontoConsumo.buscarPontoConsumo((Long) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DEZ]);
				Periodicidade periodicidade = controladorContrato.obterPeriodicidadePorPontoConsumo(pontoConsumo, null);
				if (periodicidade != null) {
					if ((periodicidade.getQuantidadeDias() <= supervisorioMedicaoVO.getQtdMedicao())
									&& supervisorioMedicaoVO.getIndicadorIntegrado()) {
						supervisorioMedicaoVO.setIntegrado(Constantes.SIM);
						supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.TRUE);
					} else {
						supervisorioMedicaoVO.setIntegrado(Constantes.NAO);
						supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.FALSE);
					}
				}
			}

			retorno.add(supervisorioMedicaoVO);
		}
		return retorno;
	}

	@Override
	public Collection<SupervisorioMedicaoVO> consultarSupervisorioMedicaoDiariaPorMedidor(Map<String, Object> filtro) throws GGASException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		Collection<SupervisorioMedicaoVO> retorno = new ArrayList<>();

		Class<?> entidadeMedidor = ServiceLocator.getInstancia().getClassPorID(Medidor.BEAN_ID_MEDIDOR);
		Class<?> entidadePontoConsumo = ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
		Class<?> entidadeInstalacaoMedidor = ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);


		StringBuilder hql = new StringBuilder();

		hql.append("select  (select distinct medidorIndependente.numeroSerie from  ");
		hql.append(entidadeMedidor.getSimpleName());
		hql.append(" medidorIndependente ");
		hql.append("where sumd.codigoPontoConsumoSupervisorio = medidorIndependente.codigoMedidorSupervisorio ");
		hql.append(" ) as medidor, ");

		hql.append("sumd.codigoPontoConsumoSupervisorio as codigoPontoConsumoSupervisorio, ");
		hql.append("sumd.dataReferencia as dataReferencia, ");
		hql.append("sumd.numeroCiclo as numeroCiclo, ");
		hql.append("count(distinct sumd.chavePrimaria) as qtdMedicaoDiaria, ");
		hql.append("max(suma.indicadorImpedeProcessamento) as anormalidadeImpeditiva, ");
		hql.append("min(enco.chavePrimaria) as statusDiaria, ");
		hql.append("min(sumd.indicadorProcessado) as transferido, ");
		hql.append("min(sumd.indicadorIntegrado) as indicadorIntegrado, ");
		hql.append("min(encoHoraria.chavePrimaria) as statusHoraria, ");

		hql.append("(select distinct medidor.chavePrimaria from  ");
		hql.append(entidadeMedidor.getSimpleName());
		hql.append(" medidor ");
		hql.append("where sumd.codigoPontoConsumoSupervisorio = medidor.codigoMedidorSupervisorio ");
		hql.append(" ) as chaveMedidor, ");

		hql.append("(select ponto.descricao from ");
		hql.append(entidadePontoConsumo.getSimpleName());
		hql.append(" ponto, ");
		hql.append(entidadeInstalacaoMedidor.getSimpleName());
		hql.append(" medi_instalacao1_");
		hql.append(" where medi_instalacao1_.pontoConsumo = ponto.chavePrimaria and medi_instalacao1_.medidor =(");
		hql.append("select max(medidor1_.chavePrimaria) from ");
		hql.append(entidadeMedidor.getSimpleName());
		hql.append(" medidor1_ ");
		hql.append("where medidor1_.composicaoVirtual like concat(concat( '%',medidorInd.chavePrimaria),'%')");
		hql.append(" )) as descricaoPonto ");

		hql.append("from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" sumh ");

		hql.append("right join sumh.supervisorioMedicaoDiaria sumd ");
		hql.append("left join sumd.supervisorioMedicaoAnormalidade suma ");
		hql.append("left join sumd.status enco ");
		hql.append("left join sumh.status encoHoraria, ");
		hql.append(entidadeMedidor.getSimpleName());
		hql.append(" medidorInd ");
		hql.append("where sumd.indicadorMedidor=true ");
		hql.append("and medidorInd.codigoMedidorSupervisorio = sumd.codigoPontoConsumoSupervisorio ");

		Boolean ocorrencia = (Boolean) filtro.get("ocorrencia");
		if (ocorrencia != null) {
			if (ocorrencia.equals(Boolean.TRUE)) {
				hql.append(" and sumd.supervisorioMedicaoAnormalidade is not null ");
			} else if (ocorrencia.equals(Boolean.FALSE)) {
				hql.append(" and sumd.supervisorioMedicaoAnormalidade is null ");
			}
		}

		Long[] idsOcorrenciaMedicao = (Long[]) filtro.get("idsOcorrenciaMedicao");
		if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and suma.chavePrimaria in (:idsOcorrenciaMedicao) ");
		}

		Boolean analisada = (Boolean) filtro.get("analisada");
		if (analisada != null) {
			hql.append(" and sumd.indicadorIntegrado = :analisada ");
		}

		Boolean indicadorConsolidada = (Boolean) filtro.get(INDICADOR_CONSOLIDADA);
		if (indicadorConsolidada != null) {
			hql.append(" and sumd.indicadorConsolidada = :indicadorConsolidada ");
		}

		Boolean transferencia = (Boolean) filtro.get("transferencia");
		if (transferencia != null) {
			hql.append(" and sumd.indicadorProcessado = :transferencia ");
		}

		Boolean ativo = (Boolean) filtro.get("ativo");
		if (ativo != null) {
			hql.append(" and sumd.habilitado = :ativo ");
		}

		Long status = (Long) filtro.get("status");
		if (status != null && status > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and sumd.status.chavePrimaria = :statusSupervisorio ");
		}

		Long anoMesReferenciaLido = (Long) filtro.get("anoMesReferenciaLido");
		if (anoMesReferenciaLido != null && anoMesReferenciaLido > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and sumd.dataReferencia = :anoMesReferenciaLido ");
		}

		Long idCiclo = (Long) filtro.get("idCiclo");
		if (idCiclo != null && idCiclo > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and sumd.numeroCiclo = :idCiclo ");
		}

		String enderecoRemotoLido = (String) filtro.get("enderecoRemotoLido");
		if (!StringUtils.isEmpty(enderecoRemotoLido)) {
			hql.append(" and upper(sumd.codigoPontoConsumoSupervisorio) like upper(:enderecoRemotoLido) ");
		}

		Date dataLeituraInicial = (Date) filtro.get("dataLeituraInicial");
		Date dataLeituraFinal = (Date) filtro.get("dataLeituraFinal");
		if (dataLeituraInicial != null && dataLeituraFinal != null) {
			hql.append(" and (sumd.dataRealizacaoLeitura between :dataLeituraInicial and :dataLeituraFinal) ");
		}

		Date dataInclusaoInicial = (Date) filtro.get("dataInclusaoInicial");
		Date dataInclusaoFinal = (Date) filtro.get("dataInclusaoFinal");
		if (dataInclusaoInicial != null && dataInclusaoFinal != null) {
			hql.append(" and (sumd.dataRegistroLeitura between :dataInclusaoInicial and :dataInclusaoFinal) ");
		}

		if (filtro.get("chavePrimariaDiaria") != null && !((Map<Long, Long>) filtro.get("chavePrimariaDiaria")).isEmpty()) {
			Map<Long, Long> chavePrimaria = (Map<Long, Long>) filtro.get("chavePrimariaDiaria");

			hql.append(" and sumd.chavePrimaria in (").append(retornaChavesPrimarias(chavePrimaria)).append(")");
		}

		if (filtro.get("chavesPrimariasPendentes") != null) {
			hql.append(" and sumd.chavePrimaria in (").append(filtro.get("chavesPrimariasPendentes").toString()).append(")");
		}

		hql.append("group by sumd.codigoPontoConsumoSupervisorio, sumd.dataReferencia, sumd.numeroCiclo, medidorInd.chavePrimaria ");
		hql.append("order by sumd.codigoPontoConsumoSupervisorio, sumd.dataReferencia desc, sumd.numeroCiclo desc, medidor ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idsOcorrenciaMedicao != null && idsOcorrenciaMedicao.length > CONSTANTE_NUMERO_ZERO) {
			query.setParameterList("idsOcorrenciaMedicao", idsOcorrenciaMedicao);
		}
		if (analisada != null) {
			query.setBoolean("analisada", analisada);
		}
		if (indicadorConsolidada != null) {
			query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
		}
		if (transferencia != null) {
			query.setBoolean("transferencia", transferencia);
		}
		if (ativo != null) {
			query.setBoolean("ativo", ativo);
		}

		if (status != null && status > CONSTANTE_NUMERO_ZERO) {
			query.setLong("statusSupervisorio", status);
		}

		if (anoMesReferenciaLido != null && anoMesReferenciaLido > CONSTANTE_NUMERO_ZERO) {
			query.setLong("anoMesReferenciaLido", anoMesReferenciaLido);
		}
		if (idCiclo != null && idCiclo > CONSTANTE_NUMERO_ZERO) {
			query.setLong("idCiclo", idCiclo);
		}
		if (!StringUtils.isEmpty(enderecoRemotoLido)) {
			query.setString("enderecoRemotoLido", enderecoRemotoLido);
		}
		if ((dataLeituraInicial != null) && (dataLeituraFinal != null)) {
			dataLeituraFinal = (Util.ultimoHorario(new DateTime(dataLeituraFinal))).toDate();
			Util.adicionarRestricaoDataSemHora(query, dataLeituraInicial, "dataLeituraInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataLeituraFinal, "dataLeituraFinal", Boolean.FALSE);
		}
		if ((dataInclusaoInicial != null) && (dataInclusaoFinal != null)) {
			dataInclusaoFinal = (Util.ultimoHorario(new DateTime(dataInclusaoFinal))).toDate();
			Util.adicionarRestricaoDataSemHora(query, dataInclusaoInicial, "dataInclusaoInicial", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataInclusaoFinal, "dataInclusaoFinal", Boolean.FALSE);
		}

		// Paginação em banco dados
		ColecaoPaginada colecaoPaginada = null;
		if (filtro.containsKey("colecaoPaginada")) {
			colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query, getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName(),
							hql.toString());
		}

		if (colecaoPaginada != null) {
			if(colecaoPaginada.getIndex() == CONSTANTE_NUMERO_UM){
				query.setFirstResult(CONSTANTE_NUMERO_ZERO);
			}else{
				query.setFirstResult(colecaoPaginada.getIndex());
			}
			query.setMaxResults(colecaoPaginada.getObjectsPerPage());
		}

		List<Object[]> listaSupervisorioMedicaoDiaria = query.list();

		// contador da chave primária
		Long i = 1L;

		for (Object[] supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
			SupervisorioMedicaoVO supervisorioMedicaoVO = new SupervisorioMedicaoVO();
			supervisorioMedicaoVO.setChavePrimaria(i);
			i++;

			// supervisorioMedicaoDiaria[0] = descrição do Medidor
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_ZERO] != null) {
				supervisorioMedicaoVO.setMedidor(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_ZERO].toString());
			}

			// supervisorioMedicaoDiaria[1] = código do supervisório
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_UM] != null) {
				supervisorioMedicaoVO.setCodigoPontoConsumoSupervisorio(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_UM].toString());
			}

			// supervisorioMedicaoDiaria[2] = ano/mês referência
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DOIS] != null) {
				supervisorioMedicaoVO.setDataReferencia(Integer.valueOf(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DOIS].toString()));
			}

			// supervisorioMedicaoDiaria[3] = número do ciclo
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_TRES] != null) {
				supervisorioMedicaoVO.setNumeroCiclo(Integer.valueOf(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_TRES].toString()));
			}

			// supervisorioMedicaoDiaria[4] = quantidade de registros diários agrupados
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_QUATRO] != null) {
				supervisorioMedicaoVO.setQtdMedicao(Integer.valueOf(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_QUATRO].toString()));
			}

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

			EntidadeConteudo statusPendente = null;
			ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
							.getControladorNegocio

							(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

			statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));

			/*
			 * supervisorioMedicaoDiaria[5] = retorna o valor máximo que a coluna suma.indicadorImpedeProcessamento pode receber
			 * para os registros
			 * diários agrupados
			 * supervisorioMedicaoDiaria[6] = retorna o valor mínimo que a coluna sumd.status pode receber para os registros
			 * diários agrupados
			 * supervisorioMedicaoDiaria[9] = retorna o valor mínimo que a coluna sumh.status pode receber para os registros
			 * horários
			 */

			if (supervisorioMedicaoDiaria[5] != null) {

				Boolean anormalidade = (Boolean) supervisorioMedicaoDiaria[5];

				// verifica se a anormalidade é impeditiva ou se os registros diários/horários estão autorizados
				if (anormalidade.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

					// seta anormalidade impeditiva
					supervisorioMedicaoVO.setAnormalidade("1");
				} else {

					// seta anormalidade não impeditiva
					supervisorioMedicaoVO.setAnormalidade("0");
				}

			}

			Long statusDiaria = (Long) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_SEIS];
			Long statusHoraria = (Long) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_NOVE];

			// verifica se os registros diários/horários estão autorizados pois não é permitido transferir um registro que não está autorizado
			if ((statusDiaria != null && statusDiaria == statusPendente.getChavePrimaria())
							|| (statusHoraria != null && statusHoraria == statusPendente.getChavePrimaria())) {

				// seta anormalidade impeditiva
				supervisorioMedicaoVO.setAnormalidade("1");
			}

			// supervisorioMedicaoDiaria[7] = retorna o valor mínimo que a coluna sumd.indicadorProcessado pode receber para os registros
			// diários agrupados
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_SETE] != null) {

				Boolean transferido = (Boolean) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_SETE];

				if (transferido.equals(Constantes.INDICADOR_PROCESSADO)) {

					supervisorioMedicaoVO.setTransferido(Constantes.SIM);
					supervisorioMedicaoVO.setIndicadorProcessado(Boolean.TRUE);
				} else {

					supervisorioMedicaoVO.setTransferido(Constantes.NAO);
					supervisorioMedicaoVO.setIndicadorProcessado(Boolean.FALSE);
				}
			}

			// supervisorioMedicaoDiaria[8] = retorna o valor mínimo que a coluna sumd.indicadorProcessado pode receber para os registros
			// diários agrupados
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_OITO] != null) {

				Boolean indicadorIntegrado = (Boolean) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_OITO];

				if (indicadorIntegrado.equals(Constantes.INDICADOR_INTEGRADO)) {

					supervisorioMedicaoVO.setIntegrado(Constantes.SIM);
					supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.TRUE);
				} else {

					supervisorioMedicaoVO.setIntegrado(Constantes.NAO);
					supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.FALSE);
				}
			}

			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DEZ] != null) {
				Medidor medidor = (Medidor) controladorMedidor.obter((Long) supervisorioMedicaoDiaria[CONSTANTE_NUMERO_DEZ]);
				List<Medidor> listaMedidorVirtual = (List<Medidor>) controladorMedidor
								.consultarMedidorVirtualPorMedidorIndependente(medidor);
				PontoConsumo pontoConsumo = null;
				if (!listaMedidorVirtual.isEmpty()) {
					InstalacaoMedidor instalacaoMedidor = Util.primeiroElemento(listaMedidorVirtual).getInstalacaoMedidor();
					if (instalacaoMedidor != null) {
						pontoConsumo = instalacaoMedidor.getPontoConsumo();
					}
				}
				if (pontoConsumo != null) {
					Periodicidade periodicidade = controladorContrato.obterPeriodicidadePorPontoConsumo(pontoConsumo, null);
					if (periodicidade != null) {
						if ((periodicidade.getQuantidadeDias() <= supervisorioMedicaoVO.getQtdMedicao())
										&& supervisorioMedicaoVO.getIndicadorIntegrado()) {
							supervisorioMedicaoVO.setIntegrado(Constantes.SIM);
							supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.TRUE);
						} else {
							supervisorioMedicaoVO.setIntegrado(Constantes.NAO);
							supervisorioMedicaoVO.setIndicadorIntegrado(Boolean.FALSE);
						}
					}
				}
			}


			// supervisorioMedicaoDiaria[11] = quantidade de registros diários agrupados
			if (supervisorioMedicaoDiaria[CONSTANTE_NUMERO_ONZE] != null) {
				supervisorioMedicaoVO.setPontoconsumo(supervisorioMedicaoDiaria[CONSTANTE_NUMERO_ONZE].toString());

			}

			retorno.add(supervisorioMedicaoVO);
		}
		return retorno;
	}

	/**
	 * Retorna chaves primarias.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the string
	 */
	private String retornaChavesPrimarias(Map<Long, Long> chavePrimaria) {

		StringBuilder s = new StringBuilder();

		for (Long l : chavePrimaria.values()) {
			s.append(l).append(",");
		}

		return s.toString().substring(CONSTANTE_NUMERO_ZERO, s.toString().length() - CONSTANTE_NUMERO_UM);
	}

	/**
	 * Valida de existe controle de alçada para a medição diária.
	 *
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param supervisorioMedicaoDiariaAnterior
	 *            the supervisorio medicao diaria anterior
	 * @param parametroTabelaSuperMedicaoDiaria
	 *            the parametro tabela super medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void validarAlcada(DadosAuditoria dadosAuditoria, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAnterior, ParametroSistema parametroTabelaSuperMedicaoDiaria)
					throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		EntidadeConteudo status = null;

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorAlcada controladorAlcada = (ControladorAlcada) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
		EntidadeConteudo statusAutorizado = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusAutorizada));

		if (supervisorioMedicaoDiariaAnterior == null || supervisorioMedicaoDiariaAnterior.getStatus() == null
						|| (!supervisorioMedicaoDiariaAnterior.getStatus().equals(statusPendente))) {
			Long chaveTabelaSuperMedicaoDiaria = null;
			if (parametroTabelaSuperMedicaoDiaria != null) {
				chaveTabelaSuperMedicaoDiaria = Long.valueOf(parametroTabelaSuperMedicaoDiaria.getValor());
			}

			ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
			Collection<Papel> papeis = null;
			if (dadosAuditoria != null && dadosAuditoria.getUsuario() != null && dadosAuditoria.getUsuario().getFuncionario() != null) {
				papeis = controladorPapel.consultarPapeisPorFuncionario(dadosAuditoria.getUsuario().getFuncionario().getChavePrimaria());
			}

			if (chaveTabelaSuperMedicaoDiaria != null && papeis != null && !papeis.isEmpty()) {
				Collection<Alcada> colecaoAlcadas = controladorAlcada.obterListaAlcadasVigentes(chaveTabelaSuperMedicaoDiaria, papeis,
								Calendar.getInstance().getTime());
				if (colecaoAlcadas != null && !colecaoAlcadas.isEmpty()) {
					for (Alcada alcada : colecaoAlcadas) {
						try {

							String propriedade = alcada.getColuna().getNomePropriedade();
							propriedade = propriedade.substring(CONSTANTE_NUMERO_ZERO, CONSTANTE_NUMERO_UM).toUpperCase() + propriedade
									.subSequence(CONSTANTE_NUMERO_UM, propriedade.length());
							String nomeMetodo = REFLECTION_GET + propriedade;

							Object valorMetodoObj = supervisorioMedicaoDiaria.getClass().getDeclaredMethod(nomeMetodo, new Class[] {})
											.invoke(supervisorioMedicaoDiaria);

							// verifica se o campo avaliado foi alterado
							Boolean ehCampoAlterado = Boolean.FALSE;
							if (supervisorioMedicaoDiariaAnterior == null) {
								if (supervisorioMedicaoDiaria.getChavePrimaria() == 0L) {
									ehCampoAlterado = Boolean.TRUE;
								} else {
									SupervisorioMedicaoDiaria supervisorioMedicaoDiariaBanco = obterSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria
													.getChavePrimaria());
									Object valorMetodoObjBanco = supervisorioMedicaoDiariaBanco.getClass()
													.getDeclaredMethod(nomeMetodo, new Class[] {}).invoke(supervisorioMedicaoDiariaBanco);
									if (!(new BigDecimal(valorMetodoObjBanco.toString())
													.compareTo(new BigDecimal(valorMetodoObj.toString())) == CONSTANTE_NUMERO_ZERO)) {
										ehCampoAlterado = Boolean.TRUE;
									}
								}

							} else {
								Object valorMetodoObjBanco = supervisorioMedicaoDiariaAnterior.getClass()
												.getDeclaredMethod(nomeMetodo, new Class[] {}).invoke(supervisorioMedicaoDiariaAnterior);
								if (valorMetodoObjBanco == null
												|| valorMetodoObj == null
												|| !(new BigDecimal(valorMetodoObjBanco.toString()).compareTo(new BigDecimal(valorMetodoObj
																.toString())) == CONSTANTE_NUMERO_ZERO)) {
									ehCampoAlterado = Boolean.TRUE;
								}
							}
							// ---------------------------
							if (ehCampoAlterado && valorMetodoObj != null) {
								BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());
								if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
												&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= CONSTANTE_NUMERO_ZERO)
												&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= CONSTANTE_NUMERO_ZERO)) {

									status = statusPendente;

									break;
								}
							}
						} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException
								| NoSuchMethodException e) {
							LOG.error(e);
						}
					}
				}
			}
		} else {
			status = statusPendente;
		}
		if (status != null) {

			supervisorioMedicaoDiaria.setStatus(status);
			if (supervisorioMedicaoDiaria.getUltimoUsuarioAlteracao() == null) {
				supervisorioMedicaoDiaria.setUltimoUsuarioAlteracao(dadosAuditoria.getUsuario());
			}

			boolean isUnicoUsuarioAutorizado = controladorAlcada.possuiAlcadaAutorizacaoSupervisorioMedicaoDiaria(
							dadosAuditoria.getUsuario(), supervisorioMedicaoDiaria);

			if (isUnicoUsuarioAutorizado) {
				status = statusAutorizado;
			} else {
				status = statusPendente;
			}
		} else {
			status = statusAutorizado;
		}

		if (supervisorioMedicaoDiaria != null) {
			supervisorioMedicaoDiaria.setStatus(status);
			if (supervisorioMedicaoDiaria.getDadosAuditoria() != null && supervisorioMedicaoDiaria.getDadosAuditoria().getUsuario() != null) {
				supervisorioMedicaoDiaria.setUltimoUsuarioAlteracao(supervisorioMedicaoDiaria.getDadosAuditoria().getUsuario());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#consultarSupervisorioMedicaoComentario(java.util.Map,
	 * java.lang.String,
	 * java.lang.String[])
	 */
	@Override
	public Collection<SupervisorioMedicaoComentario> consultarSupervisorioMedicaoComentario(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		Query query = null;
		Long supervisorioMedicaoDiaria = null;
		Long supervisorioMedicaoHoraria = null;
		Long[] chavesSupervisorioMedicaoDiaria = null;
		Boolean habilitado = null;
		String codigoPontoConsumoSupervisorio = null;
		Boolean isDesconsolidarSupervisorio = null;
		if (filtro != null) {
			isDesconsolidarSupervisorio = (Boolean) filtro.get("isDesconsolidarSupervisorio");
		}

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoComentario().getSimpleName());
		hql.append(" smc ");

		if (isDesconsolidarSupervisorio != null) {
			hql.append(" inner join fetch smc.supervisorioMedicaoDiaria smd ");
		}

		hql.append(" where 1 = 1 ");

		if (filtro != null && !filtro.isEmpty()) {
			habilitado = (Boolean) filtro.get(HABILITADO);
			supervisorioMedicaoDiaria = (Long) filtro.get(SUPERVISORIO_MEDICAO_DIARIA);
			supervisorioMedicaoHoraria = (Long) filtro.get(SUPERVISORIO_MEDICAO_HORARIA);
			chavesSupervisorioMedicaoDiaria = (Long[]) filtro.get("chavesSupervisorioMedicaoDiaria");
			codigoPontoConsumoSupervisorio = (String) filtro.get(CODIGO_PONTO_CONSUMO_SUPERVISORIO);

			if (supervisorioMedicaoHoraria != null) {
				hql.append(" and smc.supervisorioMedicaoHoraria.chavePrimaria = :supervisorioMedicaoHoraria ");
			}

			if (supervisorioMedicaoDiaria != null && supervisorioMedicaoHoraria != null) {
				hql.append(" and smc.supervisorioMedicaoDiaria.chavePrimaria = :supervisorioMedicaoDiaria ");

			} else if (supervisorioMedicaoDiaria != null && supervisorioMedicaoHoraria == null) {
				hql.append(" and smc.supervisorioMedicaoDiaria.chavePrimaria = :supervisorioMedicaoDiaria ");
				hql.append(" and smc.supervisorioMedicaoHoraria is null ");
			}

			if (habilitado != null) {
				hql.append(" and smc.habilitado = :habilitado ");
			}

			if (chavesSupervisorioMedicaoDiaria != null && chavesSupervisorioMedicaoDiaria.length > CONSTANTE_NUMERO_ZERO) {
				hql.append(" and smc.supervisorioMedicaoDiaria.chavePrimaria in (:chavesSupervisorioMedicaoDiaria) ");
			}

			if (isDesconsolidarSupervisorio != null && codigoPontoConsumoSupervisorio != null) {
				hql.append(" and smd.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio ");
				if (filtro.get(NUMERO_CICLO) != null) {
					hql.append(" and smd.numeroCiclo = :numeroCiclo ");
				}
				if (filtro.get(ANO_MES_REFERENCIA_LIDO) != null) {
					hql.append(" and smd.dataReferencia = :anoMesReferenciaLido ");
				}
			}

		}

		if (ordenacao != null) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null && !filtro.isEmpty()) {

			if (supervisorioMedicaoDiaria != null) {
				query.setLong(SUPERVISORIO_MEDICAO_DIARIA, supervisorioMedicaoDiaria);
			}

			if (supervisorioMedicaoHoraria != null) {
				query.setLong(SUPERVISORIO_MEDICAO_HORARIA, supervisorioMedicaoHoraria);
			}

			if (habilitado != null) {
				query.setBoolean(HABILITADO, habilitado);
			}

			if (chavesSupervisorioMedicaoDiaria != null && chavesSupervisorioMedicaoDiaria.length > CONSTANTE_NUMERO_ZERO) {
				query.setParameterList("chavesSupervisorioMedicaoDiaria", chavesSupervisorioMedicaoDiaria);
			}

			if (isDesconsolidarSupervisorio != null && codigoPontoConsumoSupervisorio != null) {
				query.setParameter("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio);
				if (filtro.get(NUMERO_CICLO) != null) {
					query.setLong(NUMERO_CICLO, (Long) filtro.get(NUMERO_CICLO));
				}
				if (filtro.get(ANO_MES_REFERENCIA_LIDO) != null) {
					query.setLong(ANO_MES_REFERENCIA_LIDO, (Long) filtro.get(ANO_MES_REFERENCIA_LIDO));
				}
			}

			// Paginação em banco dados
			ColecaoPaginada colecaoPaginada = null;
			if (filtro.containsKey("colecaoPaginada")) {
				colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

				HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query, getClasseEntidadeSupervisorioMedicaoComentario()
								.getSimpleName(), hql.toString());

			}

			if (colecaoPaginada != null) {
				if(colecaoPaginada.getIndex() == CONSTANTE_NUMERO_UM){
					query.setFirstResult(CONSTANTE_NUMERO_ZERO);
				}else{
					query.setFirstResult(colecaoPaginada.getIndex());
				}
				query.setMaxResults(colecaoPaginada.getObjectsPerPage());
			}
		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null) {

			lista = inicializarLazyColecao(lista, propriedadesLazy);
		}

		return (Collection<SupervisorioMedicaoComentario>) (Collection<?>) lista;
	}

	/**
	 * Valida de existe controle de alçada para a medição diária.
	 *
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param supervisorioMedicaoHorariaAnterior
	 *            the supervisorio medicao horaria anterior
	 * @param parametroTabelaSuperMedicaoHoraria
	 *            the parametro tabela super medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void validarAlcada(DadosAuditoria dadosAuditoria, SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAnterior, ParametroSistema parametroTabelaSuperMedicaoHoraria)
					throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		EntidadeConteudo status = null;

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorAlcada controladorAlcada = (ControladorAlcada) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		EntidadeConteudo statusAutorizado = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusAutorizada));

		if (supervisorioMedicaoHorariaAnterior == null || supervisorioMedicaoHorariaAnterior.getStatus() == null
						|| (!supervisorioMedicaoHorariaAnterior.getStatus().equals(statusPendente))) {
			Long chaveTabelaSuperMedicaoHoraria = null;
			if (parametroTabelaSuperMedicaoHoraria != null) {
				chaveTabelaSuperMedicaoHoraria = Long.valueOf(parametroTabelaSuperMedicaoHoraria.getValor());
			}

			ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
			Collection<Papel> papeis = null;
			if (dadosAuditoria != null && dadosAuditoria.getUsuario() != null && dadosAuditoria.getUsuario().getFuncionario() != null) {
				papeis = controladorPapel.consultarPapeisPorFuncionario(dadosAuditoria.getUsuario().getFuncionario().getChavePrimaria());
			}

			if (chaveTabelaSuperMedicaoHoraria != null && papeis != null && !papeis.isEmpty()) {
				Collection<Alcada> colecaoAlcadas = controladorAlcada.obterListaAlcadasVigentes(chaveTabelaSuperMedicaoHoraria, papeis,
								Calendar.getInstance().getTime());
				if (colecaoAlcadas != null && !colecaoAlcadas.isEmpty()) {
					for (Alcada alcada : colecaoAlcadas) {
						try {

							String propriedade = alcada.getColuna().getNomePropriedade();
							propriedade = propriedade.substring(CONSTANTE_NUMERO_ZERO, CONSTANTE_NUMERO_UM).toUpperCase() + propriedade
									.subSequence(CONSTANTE_NUMERO_UM, propriedade.length());
							String nomeMetodo = REFLECTION_GET + propriedade;

							Object valorMetodoObj = supervisorioMedicaoHoraria.getClass().getDeclaredMethod(nomeMetodo, new Class[] {})
											.invoke(supervisorioMedicaoHoraria);

							// verifica se o campo avaliado foi alterado
							Boolean ehCampoAlterado = Boolean.FALSE;
							if (supervisorioMedicaoHorariaAnterior == null) {
								if (supervisorioMedicaoHoraria.getChavePrimaria() == 0L) {
									ehCampoAlterado = Boolean.TRUE;
								} else {
									SupervisorioMedicaoHoraria supervisorioMedicaoHorariaBanco = obterSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria
													.getChavePrimaria());
									Object valorMetodoObjBanco = supervisorioMedicaoHorariaBanco.getClass()
													.getDeclaredMethod(nomeMetodo, new Class[] {}).invoke(supervisorioMedicaoHorariaBanco);
									if (!(new BigDecimal(valorMetodoObjBanco.toString())
													.compareTo(new BigDecimal(valorMetodoObj.toString())) == CONSTANTE_NUMERO_ZERO)) {
										ehCampoAlterado = Boolean.TRUE;
									}
								}

							} else {
								Object valorMetodoObjBanco = supervisorioMedicaoHorariaAnterior.getClass()
												.getDeclaredMethod(nomeMetodo, new Class[] {}).invoke(supervisorioMedicaoHorariaAnterior);
								if (!(new BigDecimal(valorMetodoObjBanco.toString()).compareTo(new BigDecimal(valorMetodoObj.toString()))
										== CONSTANTE_NUMERO_ZERO)) {
									ehCampoAlterado = Boolean.TRUE;
								}
							}
							// ---------------------------
							if (ehCampoAlterado && valorMetodoObj != null) {
								BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());
								if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null) && (alcada.getValorFinal() == null
										|| alcada.getValorFinal().compareTo(valorMetodo) >= CONSTANTE_NUMERO_ZERO) && (
										alcada.getValorInicial() == null
												|| alcada.getValorInicial().compareTo(valorMetodo) <= CONSTANTE_NUMERO_ZERO)) {

									status = statusPendente;
									break;
								}
							}
						} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException
								| NoSuchMethodException e) {
							LOG.error(e);
						}
					}
				}
			}
		} else {
			status = statusPendente;
		}

		if (status != null) {

			supervisorioMedicaoHoraria.setStatus(status);
			if (supervisorioMedicaoHoraria.getUltimoUsuarioAlteracao() == null) {
				supervisorioMedicaoHoraria.setUltimoUsuarioAlteracao(dadosAuditoria.getUsuario());
			}

			boolean isUnicoUsuarioAutorizado = controladorAlcada.possuiAlcadaAutorizacaoSupervisorioMedicaoHoraria(
							dadosAuditoria.getUsuario(), supervisorioMedicaoHoraria);

			if (isUnicoUsuarioAutorizado) {
				status = statusAutorizado;
			} else {
				status = statusPendente;
			}
		} else {
			status = statusAutorizado;
		}

		if (supervisorioMedicaoHoraria != null) {
			supervisorioMedicaoHoraria.setStatus(status);
			if (supervisorioMedicaoHoraria.getDadosAuditoria() != null
							&& supervisorioMedicaoHoraria.getDadosAuditoria().getUsuario() != null) {
				supervisorioMedicaoHoraria.setUltimoUsuarioAlteracao(supervisorioMedicaoHoraria.getDadosAuditoria().getUsuario());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#obterSupervisorioMedicaoAnormalidade(long)
	 */
	@Override
	public SupervisorioMedicaoAnormalidade obterSupervisorioMedicaoAnormalidade(long chavePrimaria) throws NegocioException {

		SupervisorioMedicaoAnormalidade retorno = null;
		if (mapaSupervisorioMedicaoAnormalidade == null) {
			mapaSupervisorioMedicaoAnormalidade = new HashMap<>();
		}
		if (mapaSupervisorioMedicaoAnormalidade.containsKey(chavePrimaria)) {
			retorno = mapaSupervisorioMedicaoAnormalidade.get(chavePrimaria);
		} else {
			retorno = (SupervisorioMedicaoAnormalidade) super.obter(chavePrimaria, getClasseEntidadeSupervisorioMedicaoAnormalidade());
			mapaSupervisorioMedicaoAnormalidade.put(retorno.getChavePrimaria(), retorno);
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#obterSupervisorioMedicaoDiaria(long)
	 */
	@Override
	public SupervisorioMedicaoDiaria obterSupervisorioMedicaoDiaria(long chavePrimaria) throws NegocioException {

		SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = (SupervisorioMedicaoDiaria) super.obter(chavePrimaria,
						getClasseEntidadeSupervisorioMedicaoDiaria());
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ParametroSistema temFatorZFixo = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1);
		if (temFatorZFixo != null && temFatorZFixo.getValor() != null && "1".compareTo(temFatorZFixo.getValor())==CONSTANTE_NUMERO_ZERO
						&& (supervisorioMedicaoDiaria.getFatorZ() == null || (supervisorioMedicaoDiaria.getFatorZ() != null &&
						supervisorioMedicaoDiaria.getFatorZ().compareTo(BigDecimal.ZERO)==CONSTANTE_NUMERO_ZERO))) {
			supervisorioMedicaoDiaria.setFatorZ(BigDecimal.ONE);
		}

		return supervisorioMedicaoDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#obterSupervisorioMedicaoDiaria(long, java.lang.String[])
	 */
	@Override
	public SupervisorioMedicaoDiaria obterSupervisorioMedicaoDiaria(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = (SupervisorioMedicaoDiaria) super.obter(chavePrimaria,
						getClasseEntidadeSupervisorioMedicaoDiaria(), propriedadesLazy);
		ParametroSistema temFatorZFixo = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1);
		if (temFatorZFixo != null && temFatorZFixo.getValor() != null && "1".equals(temFatorZFixo.getValor())
						&& (supervisorioMedicaoDiaria.getFatorZ() == null || (supervisorioMedicaoDiaria.getFatorZ() != null &&
						supervisorioMedicaoDiaria.getFatorZ().compareTo(BigDecimal.ZERO)==CONSTANTE_NUMERO_ZERO))) {
			supervisorioMedicaoDiaria.setFatorZ(BigDecimal.ONE);
		}
		return supervisorioMedicaoDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#obterSupervisorioMedicaoHoraria(long)
	 */
	@Override
	public SupervisorioMedicaoHoraria obterSupervisorioMedicaoHoraria(long chavePrimaria) throws NegocioException {

		return (SupervisorioMedicaoHoraria) super.obter(chavePrimaria, getClasseEntidadeSupervisorioMedicaoHoraria());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#obterSupervisorioMedicaoHoraria(long, java.lang.String[])
	 */
	@Override
	public SupervisorioMedicaoHoraria obterSupervisorioMedicaoHoraria(long chavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		return (SupervisorioMedicaoHoraria) super.obter(chavePrimaria, getClasseEntidadeSupervisorioMedicaoHoraria(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#validarDadosObrigatoriosSupervisorioMedicaoDiaria(br.com.ggas.integracao.
	 * supervisorio
	 * .diaria.SupervisorioMedicaoDiaria, br.com.ggas.integracao.supervisorio.SupervisorioMedicaoVO)
	 */
	@Override
	public void validarDadosObrigatoriosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoVO supervisorioMedicaoDiariaVO) throws NegocioException {

		Map<String, Object> errosSupervisorioMedicaoDiaria = this.validarDadosObrigatorios(supervisorioMedicaoDiaria,
						supervisorioMedicaoDiariaVO);

		if (errosSupervisorioMedicaoDiaria != null && !errosSupervisorioMedicaoDiaria.isEmpty()) {
			throw new NegocioException(errosSupervisorioMedicaoDiaria);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#validarDadosSupervisorioMedicaoHoraria(br.com.ggas.integracao.supervisorio
	 * .horaria
	 * .SupervisorioMedicaoHoraria)
	 */
	@Override
	public void validarDadosSupervisorioMedicaoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException {

		Map<String, Object> errosSupervisorioMedicaoHoraria = this.validarDados(supervisorioMedicaoHoraria);

		if (errosSupervisorioMedicaoHoraria != null && !errosSupervisorioMedicaoHoraria.isEmpty()) {
			throw new NegocioException(errosSupervisorioMedicaoHoraria);
		}

	}

	/**
	 * Validar dados obrigatorios.
	 *
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param supervisorioMedicaoDiariaVO
	 *            the supervisorio medicao diaria vo
	 * @return the map
	 */
	public Map<String, Object> validarDadosObrigatorios(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoVO supervisorioMedicaoDiariaVO) {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		String codigoPontoConsumoSupervisorio = null;
		Date dataRealizacaoLeitura = null;
		Integer dataReferencia = null;

		if (supervisorioMedicaoDiaria != null) {

			codigoPontoConsumoSupervisorio = supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio();
			dataRealizacaoLeitura = supervisorioMedicaoDiaria.getDataRealizacaoLeitura();
			dataReferencia = supervisorioMedicaoDiaria.getDataReferencia();
		} else if (supervisorioMedicaoDiariaVO != null) {

			codigoPontoConsumoSupervisorio = supervisorioMedicaoDiariaVO.getCodigoPontoConsumoSupervisorio();
			dataRealizacaoLeitura = supervisorioMedicaoDiariaVO.getDataRealizacaoLeitura();
			dataReferencia = supervisorioMedicaoDiariaVO.getDataReferencia();
		}

		if (codigoPontoConsumoSupervisorio == null || StringUtils.isEmpty(codigoPontoConsumoSupervisorio)) {
			stringBuilder.append(DESCRICAO_CODIGO_PONTO_CONSUMO_SUPERVISORIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataRealizacaoLeitura == null) {
			stringBuilder.append(DESCRICAO_DATA_REALIZACAO_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataReferencia == null) {
			stringBuilder.append(DESCRICAO_DATA_REFERENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > CONSTANTE_NUMERO_ZERO) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(CONSTANTE_NUMERO_ZERO,
					stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
		}
		return erros;
	}

	/**
	 * Validar dados.
	 *
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @return the map
	 */
	public Map<String, Object> validarDados(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio() == null
						|| StringUtils.isEmpty(supervisorioMedicaoHoraria.getCodigoPontoConsumoSupervisorio())) {
			stringBuilder.append(DESCRICAO_CODIGO_PONTO_CONSUMO_SUPERVISORIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (supervisorioMedicaoHoraria.getDataRealizacaoLeitura() == null) {
			stringBuilder.append(DESCRICAO_DATA_REALIZACAO_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if (Util.verificarHoraFracionada(supervisorioMedicaoHoraria.getDataRealizacaoLeitura())) {
				stringBuilder.append(DESCRICAO_HORA_NAO_FRACIONADA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if (supervisorioMedicaoHoraria.getDataRegistroLeitura() == null) {
			stringBuilder.append(DESCRICAO_DATA_REGISTRO_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ() == null) {
			stringBuilder.append(DESCRICAO_LEITURA_CORRIGIDA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ() == null) {
			stringBuilder.append(DESCRICAO_LEITURA_NAO_CORRIGIDA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (supervisorioMedicaoHoraria.getTemperatura() == null) {
			stringBuilder.append(DESCRICAO_TEMPERATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (supervisorioMedicaoHoraria.getPressao() == null) {
			stringBuilder.append(DESCRICAO_PRESSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > CONSTANTE_NUMERO_ZERO) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(CONSTANTE_NUMERO_ZERO, stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
		}
		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#alterarSupervisorioMedicaoDiaria(br.com.ggas.integracao.supervisorio.
	 * diaria.
	 * SupervisorioMedicaoDiaria)
	 */
	@Override
	public void alterarSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorio) throws NegocioException, ConcorrenciaException {

		supervisorio.setIndicadorProcessado(true);
		supervisorio.setDataRegistroLeitura(new Date());
		super.atualizar(supervisorio, getClasseEntidadeSupervisorioMedicaoDiaria());
	}

	/**
	 * Consultar supervisorio medicao diaria.
	 *
	 * @param filtro
	 *            podendo ser: habilitado, indicadorConsolidada, indicadorTipoMedicao,
	 *            dataInicialRealizacaoLeitura e dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio
	 * @param ordenacao
	 *            the ordenacao
	 * @return Lista de Medicoes Horarias do Supervisorio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<SupervisorioMedicaoDiaria> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, String ordenacao)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Query query = null;
		Boolean habilitado = null;
		Boolean indicadorProcessado = null;
		Boolean indicadorConsolidada = null;
		Boolean indicadorIntegrado = null;
		String codigoPontoConsumoSupervisorio = null;
		Date dataInicialRealizacaoLeitura = null;
		Date dataFinalRealizacaoLeitura = null;
		Integer referencia = null;
		Integer ciclo = null;
		Long[] chaveRota = null;
		Long statusAutorizado = null;
		String[] listaCodigoSupervisorio = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName());
		hql.append(" smd ");
		hql.append(" where 1 = 1 ");

		if (!filtro.isEmpty()) {
			habilitado = (Boolean) filtro.get(HABILITADO);
			indicadorProcessado = (Boolean) filtro.get(INDICADOR_PROCESSADO);
			indicadorConsolidada = (Boolean) filtro.get(INDICADOR_CONSOLIDADA);
			indicadorIntegrado = (Boolean) filtro.get(INDICADOR_INTEGRADO);
			codigoPontoConsumoSupervisorio = (String) filtro.get(CODIGO_PONTO_CONSUMO_SUPERVISORIO);
			dataInicialRealizacaoLeitura = (Date) filtro.get(DATA_INICIAL_REALIZACAO_LEITURA);
			dataFinalRealizacaoLeitura = (Date) filtro.get(DATA_FINAL_REALIZACAO_LEITURA);
			referencia = (Integer) filtro.get(DATA_REFERENCIA);
			ciclo = (Integer) filtro.get(NUMERO_CICLO);
			chaveRota = (Long[]) filtro.get("chaveRota");
			statusAutorizado = (Long) filtro.get(STATUS_AUTORIZADO);
			listaCodigoSupervisorio = (String[]) filtro.get("listaCodigoSupervisorio");

			if (chaveRota != null) {
				hql.append(" and smd.codigoPontoConsumoSupervisorio in (");
				hql.append(" select pocn.codigoPontoConsumoSupervisorio ");
				hql.append(" from ");
				hql.append(getClasseEntidadePontoConsumo().getSimpleName()).append(" pocn ");
				hql.append(" where pocn.codigoPontoConsumoSupervisorio = smd.codigoPontoConsumoSupervisorio ");
				hql.append(" and pocn.rota.chavePrimaria in (:chaveRota) ");
				hql.append(") ");
			}

			if (referencia != null) {
				hql.append(" and smd.dataReferencia = :dataReferencia ");
			}

			if (ciclo != null) {
				hql.append(" and smd.numeroCiclo = :numeroCiclo ");
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura != null) {
				hql.append(" and smd.dataRealizacaoLeitura between :dataInicialRealizacaoLeitura and :dataFinalRealizacaoLeitura ");
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura == null) {
				hql.append(" and smd.dataRealizacaoLeitura between :dataAnteriorRealizacaoLeituraInicial and :dataAtualRealizacaoLeituraFinal ");
			}

			if (dataInicialRealizacaoLeitura == null && dataFinalRealizacaoLeitura != null) {
				hql.append(" and smd.dataRealizacaoLeitura < :dataFinalRealizacaoLeitura  ");
			}

			if (indicadorProcessado != null) {
				hql.append(" and smd.indicadorProcessado = :indicadorProcessado ");
			}

			if (indicadorIntegrado != null) {
				hql.append(" and smd.indicadorIntegrado = :indicadorIntegrado ");
			}

			if (habilitado != null) {
				hql.append(" and smd.habilitado = :habilitado ");
			}

			if (codigoPontoConsumoSupervisorio != null) {
				hql.append(" and  smd.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio");
			}

			if (indicadorConsolidada != null) {
				if (!indicadorConsolidada) {
					hql.append(" and (smd.indicadorConsolidada = :indicadorConsolidada or smd.supervisorioMedicaoAnormalidade <> null) ");
				} else {
					hql.append(" and smd.indicadorConsolidada = :indicadorConsolidada ");
				}
			}

			if (statusAutorizado != null) {

				hql.append(" and (smd.status = :statusAutorizado or smd.status is null) ");
			}

			if (listaCodigoSupervisorio != null) {
				hql.append(" and smd.codigoPontoConsumoSupervisorio in (:listaCodigoSupervisorio) ");
			}

		}

		if (ordenacao != null && !ordenacao.isEmpty()) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!filtro.isEmpty()) {

			if (chaveRota != null) {
				query.setParameterList("chaveRota", chaveRota);
			}

			if (referencia != null) {
				query.setInteger(DATA_REFERENCIA, referencia);
			}

			if (ciclo != null) {
				query.setInteger(NUMERO_CICLO, ciclo);
			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura != null) {
				query.setDate(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
				query.setDate(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);

			}

			if (dataInicialRealizacaoLeitura == null && dataFinalRealizacaoLeitura != null) {
				query.setDate(DATA_FINAL_REALIZACAO_LEITURA, dataFinalRealizacaoLeitura);

			}

			if (dataInicialRealizacaoLeitura != null && dataFinalRealizacaoLeitura == null) {

				Util.adicionarRestricaoDataSemHora(query, Util.decrementarDataComQuantidadeDias(dataInicialRealizacaoLeitura,
								Constantes.NUMERO_DIAS_INCREMENTAR_DATA), "dataAnteriorRealizacaoLeituraInicial", Boolean.TRUE);

				Util.adicionarRestricaoDataSemHora(query, dataInicialRealizacaoLeitura, "dataAtualRealizacaoLeituraFinal", Boolean.FALSE);

			}

			if (indicadorProcessado != null) {
				query.setBoolean(INDICADOR_PROCESSADO, indicadorProcessado);
			}

			if (indicadorIntegrado != null) {
				query.setBoolean(INDICADOR_INTEGRADO, indicadorIntegrado);
			}

			if (habilitado != null) {
				query.setBoolean(HABILITADO, habilitado);
			}

			if (indicadorConsolidada != null) {
				query.setBoolean(INDICADOR_CONSOLIDADA, indicadorConsolidada);
			}

			if (codigoPontoConsumoSupervisorio != null) {
				query.setString(CODIGO_PONTO_CONSUMO_SUPERVISORIO, codigoPontoConsumoSupervisorio);
			}

			if (statusAutorizado != null) {
				query.setLong(STATUS_AUTORIZADO, statusAutorizado);
			}

			if (listaCodigoSupervisorio != null) {
				query.setParameterList("listaCodigoSupervisorio", listaCodigoSupervisorio);
			}

		}
		Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = query.list();
		ParametroSistema temFatorZFixo = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_SUPERVISORIO_FATORZ_VALOR_FIXO_1);
		if (temFatorZFixo != null && temFatorZFixo.getValor() != null && "1".equals(temFatorZFixo.getValor())) {
			for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
				if (supervisorioMedicaoDiaria.getFatorZ() == null
						|| supervisorioMedicaoDiaria.getFatorZ().compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO) {
					supervisorioMedicaoDiaria.setFatorZ(BigDecimal.ONE);
				}
			}
		}
		return listaSupervisorioMedicaoDiaria;
	}

	/**
	 * Preencher escala supervisorio.
	 *
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @return the supervisorio medicao diaria
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public SupervisorioMedicaoDiaria preencherEscalaSupervisorio(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
					throws NegocioException {

		BigDecimal consumoSemCorrecaoFatorPTZ = supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ();

		if (consumoSemCorrecaoFatorPTZ != null) {

			// consumo sem correção não precisa ter decimal
			supervisorioMedicaoDiaria
					.setConsumoSemCorrecaoFatorPTZ(consumoSemCorrecaoFatorPTZ.setScale(CONSTANTE_NUMERO_ZERO, BigDecimal.ROUND_HALF_UP));
		}

		BigDecimal consumoComCorrecaoFatorPTZ = supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ();

		if (consumoComCorrecaoFatorPTZ != null) {

			supervisorioMedicaoDiaria.setConsumoComCorrecaoFatorPTZ(consumoComCorrecaoFatorPTZ.setScale(ESCALA_SUPERVISORIO,
							BigDecimal.ROUND_HALF_UP));
		}

		BigDecimal pressao = supervisorioMedicaoDiaria.getPressao();

		if (pressao != null) {

			supervisorioMedicaoDiaria.setPressao(pressao.setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
		}

		BigDecimal temperatura = supervisorioMedicaoDiaria.getTemperatura();

		if (temperatura != null) {

			supervisorioMedicaoDiaria.setTemperatura(temperatura.setScale(ESCALA_SUPERVISORIO, BigDecimal.ROUND_HALF_UP));
		}

		BigDecimal fatorPTZ = supervisorioMedicaoDiaria.getFatorPTZ();

		if (fatorPTZ != null) {

			fatorPTZ = fatorPTZ.setScale(Constantes.ESCALA_FATOR_PTZ, BigDecimal.ROUND_HALF_UP);

			int precisao = fatorPTZ.precision();
			int scala = fatorPTZ.scale();
			int parteInteira = precisao - scala;
			if (parteInteira <= Constantes.PARTE_INTEIRA_FATOR_PTZ_Z) {

				supervisorioMedicaoDiaria.setFatorPTZ(fatorPTZ);
			} else {

				supervisorioMedicaoDiaria.setFatorPTZ(null);

				if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
								|| supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade().getIndicadorImpedeProcessamento()
												.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

					// Gera anormalidade
					SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade

					(Constantes.ANORMALIDADE_SUPERVISORIO_NAO_E_POSSIVEL_CALCULAR_O_FATOR);
					supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
				}

			}
		}

		BigDecimal fatorZ = supervisorioMedicaoDiaria.getFatorZ();

		if (fatorZ != null) {

			fatorZ = fatorZ.setScale(Constantes.ESCALA_FATOR_PTZ, BigDecimal.ROUND_HALF_UP);

			int precisao = fatorZ.precision();
			int scala = fatorZ.scale();
			int parteInteira = precisao - scala;
			if (parteInteira <= Constantes.PARTE_INTEIRA_FATOR_PTZ_Z) {

				supervisorioMedicaoDiaria.setFatorZ(fatorZ);
			} else {

				supervisorioMedicaoDiaria.setFatorZ(null);

				if (supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade() == null
								|| !supervisorioMedicaoDiaria.getSupervisorioMedicaoAnormalidade().getIndicadorImpedeProcessamento()
									.equals(Constantes.ANORMALIDADE_IMPEDE_PROCESSAMENTO)) {

					// Gera anormalidade
					SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = this.obterSupervisorioMedicaoAnormalidade

					(Constantes.ANORMALIDADE_SUPERVISORIO_NAO_E_POSSIVEL_CALCULAR_O_FATOR);
					supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(supervisorioMedicaoAnormalidade);
				}

			}
		}

		return supervisorioMedicaoDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#validarDadosSupervisorioMedicaoDiaria(br.com.ggas.integracao.supervisorio.
	 * diaria
	 * .SupervisorioMedicaoDiaria)
	 */
	@Override
	public void validarDadosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		Map<String, Object> errosSupervisorioMedicaoDiaria = this.validarDados(supervisorioMedicaoDiaria);

		if (errosSupervisorioMedicaoDiaria != null && !errosSupervisorioMedicaoDiaria.isEmpty()) {
			throw new NegocioException(errosSupervisorioMedicaoDiaria);
		}

	}

	/**
	 * Validar dados.
	 *
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Map<String, Object> validarDados(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		Date dataRealizaoLeituraIncrementada = Util.incrementarDataComQuantidadeDias(supervisorioMedicaoDiaria.getDataRealizacaoLeitura(),
						Constantes.NUMERO_DIAS_INCREMENTAR_DATA);
		filtro.put(DATA_REALIZACAO_LEITURA, dataRealizaoLeituraIncrementada);
		filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio());
		filtro.put(HABILITADO, Boolean.TRUE);

		// consulta para obter o registro posterior a data de leitura que está sendo processada
		Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = this.consultarSupervisorioMedicaoDiaria(filtro, "");
		SupervisorioMedicaoDiaria supervisorioMedicaoDiariaPosterior = null;
		if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {
			supervisorioMedicaoDiariaPosterior = listaSupervisorioMedicaoDiaria.iterator().next();
		}

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if ((supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO)
				&& (supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null
				|| (supervisorioMedicaoDiaria.getFatorPTZ() == null
						&& (supervisorioMedicaoDiaria.getPressao() == null || supervisorioMedicaoDiaria.getTemperatura() == null
								|| supervisorioMedicaoDiaria.getFatorZ() == null)
						&& (supervisorioMedicaoDiaria.getFatorPTZ() == null || supervisorioMedicaoDiaria.getPressao() == null
								|| supervisorioMedicaoDiaria.getTemperatura() == null)))) {

			stringBuilder.append(DESCRICAO_LEITURA_NAO_CORRIGIDA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if ((supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO)
				&& (supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null
				|| (supervisorioMedicaoDiaria.getFatorPTZ() == null
						&& (supervisorioMedicaoDiaria.getPressao() == null || supervisorioMedicaoDiaria.getTemperatura() == null
								|| supervisorioMedicaoDiaria.getFatorZ() == null)
						&& (supervisorioMedicaoDiaria.getFatorPTZ() == null || supervisorioMedicaoDiaria.getPressao() == null
								|| supervisorioMedicaoDiaria.getTemperatura() == null)))) {

			stringBuilder.append(DESCRICAO_LEITURA_CORRIGIDA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if ((supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO)
				&& (supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() == null
				|| (supervisorioMedicaoDiaria.getFatorPTZ() == null
						&& (supervisorioMedicaoDiaria.getPressao() == null || supervisorioMedicaoDiaria.getTemperatura() == null
								|| supervisorioMedicaoDiaria.getFatorZ() == null)
						&& (supervisorioMedicaoDiaria.getFatorPTZ() == null || supervisorioMedicaoDiaria.getPressao() == null
								|| supervisorioMedicaoDiaria.getTemperatura() == null))
						&& supervisorioMedicaoDiariaPosterior == null
				|| supervisorioMedicaoDiariaPosterior.getLeituraSemCorrecaoFatorPTZ() == null
				|| supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null)) {

			stringBuilder.append(DESCRICAO_CONSUMO_NAO_CORRIGIDA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if ((supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO)
				&& (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() == null
				|| (supervisorioMedicaoDiaria.getFatorPTZ() == null
						&& (supervisorioMedicaoDiaria.getPressao() == null || supervisorioMedicaoDiaria.getTemperatura() == null
								|| supervisorioMedicaoDiaria.getFatorZ() == null)
						&& (supervisorioMedicaoDiaria.getFatorPTZ() == null || supervisorioMedicaoDiaria.getPressao() == null
								|| supervisorioMedicaoDiaria.getTemperatura() == null))
						&& supervisorioMedicaoDiariaPosterior == null
				|| supervisorioMedicaoDiariaPosterior.getLeituraComCorrecaoFatorPTZ() == null
				|| supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null)) {

			stringBuilder.append(DESCRICAO_CONSUMO_CORRIGIDA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}


		if (supervisorioMedicaoDiaria.getPressao() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO
				&& (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() == null
						|| supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() == null)
				&& supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null
				|| supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null && supervisorioMedicaoDiaria.getFatorPTZ() == null) {

			stringBuilder.append(DESCRICAO_PRESSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if ((supervisorioMedicaoDiaria.getTemperatura() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO
				&& supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() == null
				|| supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() == null)
						&& ((supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null
								|| supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null
										&& supervisorioMedicaoDiaria.getFatorPTZ() == null))) {

			stringBuilder.append(DESCRICAO_TEMPERATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if ((supervisorioMedicaoDiaria.getFatorZ() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO)
				&& ((supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() == null
						|| supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() == null)
				&& (supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null
				|| supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null && supervisorioMedicaoDiaria.getFatorPTZ() == null))) {

			stringBuilder.append(DESCRICAO_FATOR_Z);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (supervisorioMedicaoDiaria.getFatorPTZ() == null && stringBuilder.length() == CONSTANTE_NUMERO_ZERO
				&& (supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ() == null
						|| supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ() == null)
				&& supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ() == null
				|| supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() == null && (supervisorioMedicaoDiaria.getPressao() == null
						|| supervisorioMedicaoDiaria.getTemperatura() == null || supervisorioMedicaoDiaria.getFatorZ() == null)) {

			stringBuilder.append(DESCRICAO_FATOR_PTZ);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > CONSTANTE_NUMERO_ZERO) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(CONSTANTE_NUMERO_ZERO, stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
		}
		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#inserirSupervisorioMedicaoDiaria(br.com.ggas.auditoria.DadosAuditoria,
	 * br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria, br.com.ggas.parametrosistema.ParametroSistema,
	 * br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario, java.util.Collection, java.lang.Boolean,
	 * br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria, java.lang.Boolean)
	 */
	@Override
	public Long inserirSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					ParametroSistema parametroTabelaSuperMedicaoDiaria, SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, Boolean possuiComentariosAnteriores,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAntigo, Boolean validaAlcada) throws NegocioException {

		this.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
		this.validarDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
		if (validaAlcada) {
			this.validarAlcada(dadosAuditoria, supervisorioMedicaoDiaria, supervisorioMedicaoDiariaAntigo,
							parametroTabelaSuperMedicaoDiaria);
		}
		Long chavePrimaria = super.inserir(supervisorioMedicaoDiaria);

		if (supervisorioMedicaoDiariaAntigo != null) {
			supervisorioMedicaoComentarioNovo.setDescricao(gerarComentarioAutomaticoDiaria(supervisorioMedicaoDiariaAntigo,
							supervisorioMedicaoDiaria, supervisorioMedicaoComentarioNovo.getDescricao()));
			supervisorioMedicaoComentarioNovo.setDescricao(supervisorioMedicaoComentarioNovo.getDescricao() + " - Registro Alterado");
		} else {
			if (validaAlcada) {
				supervisorioMedicaoComentarioNovo.setDescricao(supervisorioMedicaoComentarioNovo.getDescricao() + " - Registro Incluido");
			} else {
				supervisorioMedicaoComentarioNovo.setDescricao("Registro Não Autorizado");
			}
		}
		// insere comentário
		if (possuiComentariosAnteriores != null && possuiComentariosAnteriores) {

			Integer numeroSequenciaComentario = this.salvarSupervisorioMedicaoComentariosAnterioresDiaria(
							listaSupervisorioMedicaoComentario, supervisorioMedicaoDiaria);
			supervisorioMedicaoComentarioNovo.setSequenciaComentario(numeroSequenciaComentario);
			this.inserir(supervisorioMedicaoComentarioNovo);
		} else {

			this.inserirSupervisorioMedicaoDiariaComentario(supervisorioMedicaoComentarioNovo, supervisorioMedicaoDiaria);
		}

		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#inserirSupervisorioMedicaoHoraria(br.com.ggas.auditoria.DadosAuditoria,
	 * br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria, br.com.ggas.parametrosistema.ParametroSistema,
	 * br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario, java.util.Collection, java.lang.Boolean,
	 * br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria, java.lang.Boolean)
	 */
	@Override
	public Long inserirSupervisorioMedicaoHoraria(DadosAuditoria dadosAuditoria, SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					ParametroSistema parametroTabelaSuperMedicaoHoraria, SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, Boolean possuiComentariosAnteriores,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAntigo, Boolean validaAlcada) throws NegocioException {

		this.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
		if (validaAlcada) {
			this.validarAlcada(dadosAuditoria, supervisorioMedicaoHoraria, supervisorioMedicaoHorariaAntigo,
							parametroTabelaSuperMedicaoHoraria);
		}
		Long chavePrimaria = super.inserir(supervisorioMedicaoHoraria);

		if (supervisorioMedicaoHorariaAntigo != null) {
			supervisorioMedicaoComentarioNovo.setDescricao(gerarComentarioAutomaticoHoraria(supervisorioMedicaoHorariaAntigo,
							supervisorioMedicaoHoraria, supervisorioMedicaoComentarioNovo.getDescricao()));
		}
		if (validaAlcada) {
			supervisorioMedicaoComentarioNovo.setDescricao(supervisorioMedicaoComentarioNovo.getDescricao() + " - Registro Incluido");
		} else {
			supervisorioMedicaoComentarioNovo.setDescricao("Registro Não Autorizado");
		}
		// insere comentário
		if (possuiComentariosAnteriores != null && possuiComentariosAnteriores) {

			Integer numeroSequenciaComentario = this.salvarSupervisorioMedicaoComentariosAnterioresHoraria(
							listaSupervisorioMedicaoComentario, supervisorioMedicaoHoraria);
			supervisorioMedicaoComentarioNovo.setSequenciaComentario(numeroSequenciaComentario);
			this.inserir(supervisorioMedicaoComentarioNovo);
		} else {

			this.inserirSupervisorioMedicaoHorariaComentario(supervisorioMedicaoComentarioNovo, supervisorioMedicaoHoraria);
		}

		return chavePrimaria;
	}

	/**
	 * Gerar comentario automatico diaria.
	 *
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param supervisorioMedicaoDiariaNovo
	 *            the supervisorio medicao diaria novo
	 * @param comentario
	 *            the comentario
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String gerarComentarioAutomaticoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo, String comentario) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		String comentarioAutomatico = "";
		if (comentario != null && !comentario.isEmpty()) {
			comentarioAutomatico = comentarioAutomatico + comentario;
		}
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.ANO_MES_REFERENCIA,
										supervisorioMedicaoDiaria.getDataReferencia(), supervisorioMedicaoDiariaNovo.getDataReferencia());
		String dataAtualLeituraInformada = null;
		String dataNovaLeituraInformada = null;

		if (supervisorioMedicaoDiaria.getDataRealizacaoLeitura() != null) {
			dataAtualLeituraInformada = "\"" + df.format(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) + "\"";
		}

		if (supervisorioMedicaoDiariaNovo.getDataRealizacaoLeitura() != null) {
			dataNovaLeituraInformada = "\"" + df.format(supervisorioMedicaoDiariaNovo.getDataRealizacaoLeitura()) + "\"";
		}
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.DATA_LEITURA, dataAtualLeituraInformada,
										dataNovaLeituraInformada);
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.LEITURA_NAO_CORRIGIDA,
										supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ(),
										supervisorioMedicaoDiariaNovo.getLeituraSemCorrecaoFatorPTZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.LEITURA_CORRIGIDA,
										supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ(),
										supervisorioMedicaoDiariaNovo.getLeituraComCorrecaoFatorPTZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.CONSUMO_NAO_CORRIGIDA,
										supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ(),
										supervisorioMedicaoDiariaNovo.getConsumoSemCorrecaoFatorPTZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.CONSUMO_CORRIGIDA,
										supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ(),
										supervisorioMedicaoDiariaNovo.getConsumoComCorrecaoFatorPTZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.PRESSAO, supervisorioMedicaoDiaria.getPressao(),
										supervisorioMedicaoDiariaNovo.getPressao());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.TEMPERATURA, supervisorioMedicaoDiaria.getTemperatura(),
										supervisorioMedicaoDiariaNovo.getTemperatura());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.FATOR_Z, supervisorioMedicaoDiaria.getFatorZ(),
										supervisorioMedicaoDiariaNovo.getFatorZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.FATOR_PTZ, supervisorioMedicaoDiaria.getFatorPTZ(),
										supervisorioMedicaoDiariaNovo.getFatorPTZ());
		return comentarioAutomatico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#atualizarRegistrosRemocaoLogicaSupervisorioMedicaoDiaria(br.com.ggas.
	 * integracao
	 * .supervisorio.diaria.SupervisorioMedicaoDiaria, java.lang.Long, java.lang.Boolean, java.lang.String)
	 */
	@Override
	public void atualizarRegistrosRemocaoLogicaSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					Long chavePrimariaSupervisorioMedicaoDiaria, Boolean ativo, String indicadorTipoMedicao) throws GGASException {

		this.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
		this.atualizar(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);

		// Desabilita todos os registro horários que estão relacionados o registro diário que foi removido
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(SUPERVISORIO_MEDICAO_DIARIA, chavePrimariaSupervisorioMedicaoDiaria);
		filtro.put(INDICADOR_TIPO_MEDICAO, indicadorTipoMedicao);
		filtro.put(ATIVO, ativo);
		this.atualizarSupervisorioMedicaoHoraria(filtro, null);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#atualizarRegistrosAlteradosSupervisorioMedicaoDiaria(br.com.ggas.auditoria
	 * .
	 * DadosAuditoria, br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria, br.com.ggas.parametrosistema.ParametroSistema,
	 * java.lang.Long, java.lang.Boolean, java.lang.String, br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria,
	 * java.util.Collection, br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario, java.lang.Boolean)
	 */
	@Override
	public void atualizarRegistrosAlteradosSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, ParametroSistema parametroTabelaSuperMedicaoDiaria,
					Long chavePrimariaSupervisorioMedicaoDiaria, Boolean ativo, String indicadorTipoMedicao,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo, Boolean possuiComentariosAnteriores)
					throws GGASException {

		this.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
		this.validarDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
		this.atualizar(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);

		this.inserirSupervisorioMedicaoDiaria(dadosAuditoria, supervisorioMedicaoDiariaNovo, parametroTabelaSuperMedicaoDiaria,
						supervisorioMedicaoComentarioNovo, listaSupervisorioMedicaoComentario, possuiComentariosAnteriores,
						supervisorioMedicaoDiaria, Boolean.TRUE);

		// Desabilita todos os registro horários que estão relacionados o registro diário que foi alterado e
		// atualiza os registros horários com o novo registro diário
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(SUPERVISORIO_MEDICAO_DIARIA, chavePrimariaSupervisorioMedicaoDiaria);
		filtro.put(INDICADOR_TIPO_MEDICAO, indicadorTipoMedicao);
		filtro.put(ATIVO, ativo);
		this.atualizarSupervisorioMedicaoHoraria(filtro, supervisorioMedicaoDiariaNovo);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#atualizarRegistrosAlcadaSupervisorioMedicaoDiaria(br.com.ggas.auditoria.
	 * DadosAuditoria
	 * , br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria, br.com.ggas.parametrosistema.ParametroSistema,
	 * br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria, java.util.Collection,
	 * br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario)
	 */
	@Override
	public void atualizarRegistrosAlcadaSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, ParametroSistema parametroTabelaSuperMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo) throws GGASException {

		this.validarDadosObrigatoriosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria, null);
		this.validarDadosSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
		this.validarAlcada(dadosAuditoria, supervisorioMedicaoDiaria, null, parametroTabelaSuperMedicaoDiaria);
		this.atualizar(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);

		if (supervisorioMedicaoDiariaNovo != null) {

			// Atualiza o comentário
			Integer numeroSequenciaComentario = this.salvarSupervisorioMedicaoComentariosAnterioresDiaria(
							listaSupervisorioMedicaoComentario, supervisorioMedicaoDiariaNovo);
			supervisorioMedicaoComentarioNovo.setSequenciaComentario(numeroSequenciaComentario);
			this.inserir(supervisorioMedicaoComentarioNovo);
		} else {

			this.inserirSupervisorioMedicaoDiariaComentario(supervisorioMedicaoComentarioNovo, supervisorioMedicaoDiaria);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#atualizarSupervisorioMedicaoHoraria(java.util.Map,
	 * br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria)
	 */
	@Override
	public void atualizarSupervisorioMedicaoHoraria(Map<String, Object> filtro, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
					throws GGASException {

		Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = this.consultarSupervisorioMedicaoHoraria(filtro, null);

		for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : listaSupervisorioMedicaoHoraria) {
			supervisorioMedicaoHoraria.setHabilitado(Boolean.FALSE);

			if (supervisorioMedicaoDiaria != null) {

				supervisorioMedicaoHoraria.setSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
			}
			this.atualizar(supervisorioMedicaoHoraria, SupervisorioMedicaoHorariaImpl.class);
		}
	}

	/**
	 * Inserir supervisorio medicao diaria comentario.
	 *
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirSupervisorioMedicaoDiariaComentario(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		this.validarDadosEntidade(supervisorioMedicaoComentario);
		this.customizarDescricaoComentario(supervisorioMedicaoComentario, supervisorioMedicaoDiaria);
		this.inserir(supervisorioMedicaoComentario);

	}

	/**
	 * Inserir supervisorio medicao horaria comentario.
	 *
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirSupervisorioMedicaoHorariaComentario(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException {

		this.validarDadosEntidade(supervisorioMedicaoComentario);
		this.customizarDescricaoComentario(supervisorioMedicaoComentario, supervisorioMedicaoHoraria);
		this.inserir(supervisorioMedicaoComentario);

	}

	/**
	 * Customizar descricao comentario.
	 *
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void customizarDescricaoComentario(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		StringBuilder sb = new StringBuilder();

		Criteria criteria = this.createCriteria(SupervisorioMedicaoDiaria.class);
		criteria.add(Restrictions.idEq(supervisorioMedicaoDiaria.getChavePrimaria()));

		SupervisorioMedicaoDiaria supervisorioMedicaoDiariaoBd = (SupervisorioMedicaoDiaria) criteria.uniqueResult();
		this.getSession().evict(supervisorioMedicaoDiariaoBd);

		String comentarioUsuario = supervisorioMedicaoComentario.getDescricao();

		if (comentarioUsuario != null && !comentarioUsuario.isEmpty()) {

			sb.append(comentarioUsuario);
		}

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.ANO_MES_REFERENCIA, supervisorioMedicaoDiariaoBd.getDataReferencia(),
						supervisorioMedicaoDiaria.getDataReferencia()));

		String dataAtualLeituraInformada = null;
		String dataNovaLeituraInformada = null;

		if (supervisorioMedicaoDiariaoBd.getDataRealizacaoLeitura() != null) {
			dataAtualLeituraInformada = "\"" + df.format(supervisorioMedicaoDiariaoBd.getDataRealizacaoLeitura()) + "\"";
		}

		if (supervisorioMedicaoDiaria.getDataRealizacaoLeitura() != null) {
			dataNovaLeituraInformada = "\"" + df.format(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) + "\"";
		}

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.DATA_LEITURA, dataAtualLeituraInformada, dataNovaLeituraInformada));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.LEITURA_NAO_CORRIGIDA,
						supervisorioMedicaoDiariaoBd.getLeituraSemCorrecaoFatorPTZ(),
						supervisorioMedicaoDiaria.getLeituraSemCorrecaoFatorPTZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.LEITURA_CORRIGIDA,
						supervisorioMedicaoDiariaoBd.getLeituraComCorrecaoFatorPTZ(),
						supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.CONSUMO_NAO_CORRIGIDA,
						supervisorioMedicaoDiariaoBd.getConsumoSemCorrecaoFatorPTZ(),
						supervisorioMedicaoDiaria.getConsumoSemCorrecaoFatorPTZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.CONSUMO_CORRIGIDA,
						supervisorioMedicaoDiariaoBd.getConsumoComCorrecaoFatorPTZ(),
						supervisorioMedicaoDiaria.getConsumoComCorrecaoFatorPTZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.PRESSAO, supervisorioMedicaoDiariaoBd.getPressao(),
						supervisorioMedicaoDiaria.getPressao()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.TEMPERATURA, supervisorioMedicaoDiariaoBd.getTemperatura(),
						supervisorioMedicaoDiaria.getTemperatura()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.FATOR_Z, supervisorioMedicaoDiariaoBd.getFatorZ(),
						supervisorioMedicaoDiaria.getFatorZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.FATOR_PTZ, supervisorioMedicaoDiariaoBd.getFatorPTZ(),
						supervisorioMedicaoDiaria.getFatorPTZ()));

		supervisorioMedicaoComentario.setDescricao(sb.toString());

	}

	/**
	 * Customizar descricao comentario.
	 *
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void customizarDescricaoComentario(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		StringBuilder sb = new StringBuilder();

		Criteria criteria = this.createCriteria(SupervisorioMedicaoDiaria.class);
		criteria.add(Restrictions.idEq(supervisorioMedicaoHoraria.getChavePrimaria()));

		SupervisorioMedicaoDiaria supervisorioMedicaoDiariaoBd = (SupervisorioMedicaoDiaria) criteria.uniqueResult();
		this.getSession().evict(supervisorioMedicaoDiariaoBd);

		String comentarioUsuario = supervisorioMedicaoComentario.getDescricao();

		if (comentarioUsuario != null && !comentarioUsuario.isEmpty()) {

			sb.append(comentarioUsuario);
		}

		String dataAtualLeituraInformada = null;
		String dataNovaLeituraInformada = null;

		if (supervisorioMedicaoDiariaoBd.getDataRealizacaoLeitura() != null) {
			dataAtualLeituraInformada = "\"" + df.format(supervisorioMedicaoDiariaoBd.getDataRealizacaoLeitura()) + "\"";
		}

		if (supervisorioMedicaoHoraria.getDataRealizacaoLeitura() != null) {
			dataNovaLeituraInformada = "\"" + df.format(supervisorioMedicaoHoraria.getDataRealizacaoLeitura()) + "\"";
		}

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.DATA_LEITURA, dataAtualLeituraInformada, dataNovaLeituraInformada));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.LEITURA_NAO_CORRIGIDA,
						supervisorioMedicaoDiariaoBd.getLeituraSemCorrecaoFatorPTZ(),
						supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.LEITURA_CORRIGIDA,
						supervisorioMedicaoDiariaoBd.getLeituraComCorrecaoFatorPTZ(),
						supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.PRESSAO, supervisorioMedicaoDiariaoBd.getPressao(),
						supervisorioMedicaoHoraria.getPressao()));

		sb.append(Util.gerarComentarioCampo(SupervisorioMedicaoDiaria.TEMPERATURA, supervisorioMedicaoDiariaoBd.getTemperatura(),
						supervisorioMedicaoHoraria.getTemperatura()));

		supervisorioMedicaoComentario.setDescricao(sb.toString());

	}

	/**
	 * Salvar supervisorio medicao comentarios anteriores diaria.
	 *
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @return the integer
	 */
	private Integer salvarSupervisorioMedicaoComentariosAnterioresDiaria(
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) {

		Integer numeroSequenciaComentario = Integer.valueOf(CONSTANTE_NUMERO_UM);
		if (listaSupervisorioMedicaoComentario != null && !listaSupervisorioMedicaoComentario.isEmpty()) {
			for (SupervisorioMedicaoComentario supervisorioMedicaoComentario : listaSupervisorioMedicaoComentario) {
				try {
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo = (SupervisorioMedicaoComentario) this
									.criarSupervisorioMedicaoComentario();

					supervisorioMedicaoComentarioNovo.setSupervisorioMedicaoDiaria(supervisorioMedicaoDiaria);
					supervisorioMedicaoComentarioNovo.setDescricao(supervisorioMedicaoComentario.getDescricao());
					supervisorioMedicaoComentarioNovo.setUsuario(supervisorioMedicaoComentario.getUsuario());
					supervisorioMedicaoComentarioNovo.setSequenciaComentario(numeroSequenciaComentario);

					this.inserir(supervisorioMedicaoComentarioNovo);
					numeroSequenciaComentario = numeroSequenciaComentario + CONSTANTE_NUMERO_UM;
				} catch (GGASException e) {
					LOG.error(e);
				}

			}
		}

		return numeroSequenciaComentario;
	}

	/**
	 * Salvar supervisorio medicao comentarios anteriores horaria.
	 *
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @return the integer
	 */
	private Integer salvarSupervisorioMedicaoComentariosAnterioresHoraria(
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) {

		Integer numeroSequenciaComentario = Integer.valueOf(CONSTANTE_NUMERO_UM);
		if (listaSupervisorioMedicaoComentario != null && !listaSupervisorioMedicaoComentario.isEmpty()) {
			for (SupervisorioMedicaoComentario supervisorioMedicaoComentario : listaSupervisorioMedicaoComentario) {
				try {
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo = (SupervisorioMedicaoComentario) this
									.criarSupervisorioMedicaoComentario();

					supervisorioMedicaoComentarioNovo.setSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
					supervisorioMedicaoComentarioNovo.setSupervisorioMedicaoDiaria(supervisorioMedicaoHoraria
									.getSupervisorioMedicaoDiaria());
					supervisorioMedicaoComentarioNovo.setDescricao(supervisorioMedicaoComentario.getDescricao());
					supervisorioMedicaoComentarioNovo.setUsuario(supervisorioMedicaoComentario.getUsuario());
					supervisorioMedicaoComentarioNovo.setSequenciaComentario(numeroSequenciaComentario);

					this.inserir(supervisorioMedicaoComentarioNovo);
					numeroSequenciaComentario = numeroSequenciaComentario + CONSTANTE_NUMERO_UM;
				} catch (GGASException e) {
					LOG.error(e);
				}

			}
		}

		return numeroSequenciaComentario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#inserirSupervisorioMedicaoHoraria(br.com.ggas.parametrosistema.
	 * ParametroSistema,
	 * br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria, br.com.ggas.auditoria.DadosAuditoria,
	 * br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario)
	 */
	@Override
	public SupervisorioMedicaoHoraria inserirSupervisorioMedicaoHoraria(ParametroSistema parametroTabelaSuperMedicaoHoraria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria, DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoComentario supervisorioMedicaoComentario) throws NegocioException {

		this.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
		this.validarAlcada(dadosAuditoria, supervisorioMedicaoHoraria, null, parametroTabelaSuperMedicaoHoraria);
		this.inserir(supervisorioMedicaoHoraria);
		supervisorioMedicaoComentario.setDescricao(supervisorioMedicaoComentario.getDescricao() + " - Registro Incluido");
		this.inserir(supervisorioMedicaoComentario);
		return supervisorioMedicaoHoraria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#removerSupervisorioMedicaoDiaria(br.com.ggas.integracao.supervisorio.
	 * comentario
	 * .SupervisorioMedicaoComentario, br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria)
	 */
	@Override
	public void removerSupervisorioMedicaoHoraria(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException {

		this.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
		supervisorioMedicaoHoraria.setHabilitado(Boolean.FALSE);
		try {
			this.atualizar(supervisorioMedicaoHoraria, SupervisorioMedicaoHorariaImpl.class);
		} catch (ConcorrenciaException e) {
			LOG.error(e);
		}
		supervisorioMedicaoComentario.setDescricao(supervisorioMedicaoComentario.getDescricao() + " - Registro removido");
		this.inserir(supervisorioMedicaoComentario);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#alterarSupervisorioMedicaoDiaria(java.util.Collection,
	 * br.com.ggas.auditoria.DadosAuditoria, br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria,
	 * br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario, br.com.ggas.parametrosistema.ParametroSistema,
	 * br.com.ggas.integracao.supervisorio.SupervisorioMedicaoVO, br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria)
	 */
	@Override
	public SupervisorioMedicaoHoraria atualizarSupervisorioMedicaoHoraria(
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo, SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					ParametroSistema parametroTabelaSuperMedicaoHoraria, SupervisorioMedicaoVO supervisorioMedicaoHorariaVO,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException {

		supervisorioMedicaoHorariaNovo.setSupervisorioMedicaoDiaria(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria());
		supervisorioMedicaoComentario.setSupervisorioMedicaoDiaria(supervisorioMedicaoHoraria.getSupervisorioMedicaoDiaria());
		this.validarDadosSupervisorioMedicaoHoraria(supervisorioMedicaoHorariaNovo);
		supervisorioMedicaoHoraria.setHabilitado(Boolean.FALSE);
		try {
			this.atualizar(supervisorioMedicaoHoraria, SupervisorioMedicaoHorariaImpl.class);
		} catch (ConcorrenciaException e) {
			LOG.error(e);
		}

		this.inserir(supervisorioMedicaoHorariaNovo);

		Integer numeroSequenciaComentario = salvarSupervisorioMedicaoComentariosAnteriores(listaSupervisorioMedicaoComentario,
						supervisorioMedicaoHorariaNovo);
		supervisorioMedicaoComentario.setSequenciaComentario(numeroSequenciaComentario);
		supervisorioMedicaoComentario.setDescricao(gerarComentarioAutomaticoHoraria(supervisorioMedicaoHoraria,
						supervisorioMedicaoHorariaNovo, supervisorioMedicaoComentario.getDescricao()));
		this.inserir(supervisorioMedicaoComentario);
		return supervisorioMedicaoHorariaNovo;
	}

	/**
	 * Gerar comentario automatico horaria.
	 *
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param supervisorioMedicaoHorariaNovo
	 *            the supervisorio medicao horaria novo
	 * @param comentario
	 *            the comentario
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String gerarComentarioAutomaticoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo, String comentario) throws NegocioException {

		String comentarioAutomatico = "";
		if (comentario != null && !comentario.isEmpty()) {
			comentarioAutomatico = comentarioAutomatico + comentario;
		}
		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
		String dataAtualLeituraInformada = null;
		String dataNovaLeituraInformada = null;

		if (supervisorioMedicaoHoraria.getDataRealizacaoLeitura() != null) {
			dataAtualLeituraInformada = "\"" + df.format(supervisorioMedicaoHoraria.getDataRealizacaoLeitura()) + "\"";
		}

		if (supervisorioMedicaoHorariaNovo.getDataRealizacaoLeitura() != null) {
			dataNovaLeituraInformada = "\"" + df.format(supervisorioMedicaoHorariaNovo.getDataRealizacaoLeitura()) + "\"";
		}
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoHoraria.DATA_HORA_LEITURA, dataAtualLeituraInformada,
										dataNovaLeituraInformada);
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoHoraria.LEITURA_NAO_CORRIGIDA,
										supervisorioMedicaoHoraria.getLeituraSemCorrecaoFatorPTZ(),
										supervisorioMedicaoHorariaNovo.getLeituraSemCorrecaoFatorPTZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoHoraria.LEITURA_CORRIGIDA,
										supervisorioMedicaoHoraria.getLeituraComCorrecaoFatorPTZ(),
										supervisorioMedicaoHorariaNovo.getLeituraComCorrecaoFatorPTZ());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoHoraria.PRESSAO, supervisorioMedicaoHoraria.getPressao(),
										supervisorioMedicaoHorariaNovo.getPressao());
		comentarioAutomatico = comentarioAutomatico
						+ Util.gerarComentarioCampo(SupervisorioMedicaoHoraria.TEMPERATURA, supervisorioMedicaoHoraria.getTemperatura(),
										supervisorioMedicaoHorariaNovo.getTemperatura());
		return comentarioAutomatico;
	}

	/**
	 * Salvar supervisorio medicao comentarios anteriores.
	 *
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @return numeroSequenciaComentario
	 */
	private Integer salvarSupervisorioMedicaoComentariosAnteriores(
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) {

		Integer numeroSequenciaComentario = Integer.valueOf(CONSTANTE_NUMERO_UM);
		if (listaSupervisorioMedicaoComentario != null && !listaSupervisorioMedicaoComentario.isEmpty()) {
			for (SupervisorioMedicaoComentario supervisorioMedicaoComentario : listaSupervisorioMedicaoComentario) {
				try {
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo;
					supervisorioMedicaoComentarioNovo = supervisorioMedicaoComentario;
					supervisorioMedicaoComentarioNovo.setSupervisorioMedicaoHoraria(supervisorioMedicaoHoraria);
					supervisorioMedicaoComentarioNovo.setSequenciaComentario(numeroSequenciaComentario);
					numeroSequenciaComentario = numeroSequenciaComentario + CONSTANTE_NUMERO_UM;
					this.inserir(supervisorioMedicaoComentarioNovo);
				} catch (GGASException e) {
					LOG.error(e);
				}

			}
		}

		return numeroSequenciaComentario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#transferirSupervisorioMedicoesDiaria(java.util.Collection,
	 * java.util.Map)
	 */
	@Override
	public Boolean transferirSupervisorioMedicoesDiaria(Collection<SupervisorioMedicaoVO> listaMedicoesDiarias, Map<String, Object> filtro)
					throws NegocioException, ConcorrenciaException, FormatoInvalidoException {

		Boolean indicadorRegistrosProcessados = Boolean.FALSE;
		Boolean indicadorRegistrosIncompletos = Boolean.FALSE;
		Boolean analisada = (Boolean) filtro.get(ANALISADA);
		Boolean excecaoRegistrosIncompletos = Boolean.FALSE;
		
		Map<String, Boolean> mapaAnaliseQtdDiasMedicao = new HashMap<>();
		for (SupervisorioMedicaoVO vo : listaMedicoesDiarias) {

			if (vo.getAnormalidade() != null && "1".equals(vo.getAnormalidade())) {
				throw new NegocioException("ERRO_NEGOCIO_MEDICAO_ANORMALIDADE_IMPEDITIVA", true);
			} else {

				// atualiza os registro de supervisório medições diárias
				filtro.put("colecaoPaginada", null);
				filtro.put("codigoPontoConsumoSupervisorio", vo.getCodigoPontoConsumoSupervisorio());
				filtro.put(ANO_MES_REFERENCIA_LIDO, Long.valueOf(vo.getDataReferencia()));
				filtro.put(ID_CICLO, Long.valueOf(vo.getNumeroCiclo()));			
				filtro.remove(ANALISADA);
				
				Date dataIntegracaoInicial = (Date) filtro.get(DATA_INTEGRACAO_INICIAL);
				Date dataIntegracaoFinal = (Date) filtro.get(DATA_INTEGRACAO_FINAL);
				
				if(dataIntegracaoInicial == null && dataIntegracaoFinal == null) {
					filtro.remove(TRANSFERENCIA);
				}

				Collection<SupervisorioMedicaoDiaria> listaCompleta = this.consultarSupervisorioMedicaoDiaria(filtro,
						null, "supervisorioMedicaoAnormalidade", "status", "ultimoUsuarioAlteracao");
				
				Collection<SupervisorioMedicaoDiaria> listaAlterar = listaCompleta.stream()
						.filter(p -> p.getIndicadorIntegrado() == Boolean.FALSE).collect(Collectors.toSet());
				
				
				if (listaAlterar.isEmpty()) {
					indicadorRegistrosProcessados = Boolean.FALSE;
				} else {
					indicadorRegistrosProcessados = Boolean.TRUE;


					String chaveMedicao = vo.getPontoconsumo() + vo.getDataReferenciaFormatado();
				
					if (!mapaAnaliseQtdDiasMedicao.containsKey(chaveMedicao)) {
						vo.setQtdMedicao(listaCompleta.size());
						mapaAnaliseQtdDiasMedicao.put(chaveMedicao, Boolean.TRUE);
						
						if(dataIntegracaoInicial != null && dataIntegracaoFinal != null) {
							indicadorRegistrosIncompletos = this.conferirQuantidadeMedicoesIntegracaoParcial(
									dataIntegracaoInicial, dataIntegracaoFinal, vo);
						} else {
							indicadorRegistrosIncompletos = this.conferirQuantidadeMedicoesSupervisorio(vo);
						}

					}
				}
				
				if(!excecaoRegistrosIncompletos && indicadorRegistrosIncompletos) {
					excecaoRegistrosIncompletos = indicadorRegistrosIncompletos;
				}
				
				if(!indicadorRegistrosIncompletos) {
					for (SupervisorioMedicaoDiaria superMedicaoDiaria : listaAlterar) {
						superMedicaoDiaria.setIndicadorIntegrado(true);
						super.atualizar(superMedicaoDiaria, getClasseEntidadeSupervisorioMedicaoDiaria());
					}
				}

				if (analisada != null) {
					filtro.put(ANALISADA, analisada);
				}

			}
			
		}
		if (!indicadorRegistrosProcessados) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REGISTROS_NAO_PROCESSADOS, true);
		}
		
		return excecaoRegistrosIncompletos;

	}

	/**
	 * Gerar ano mes referencia ciclo inicial final.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dataRealizacaoLeitura
	 *            the data realizacao leitura
	 * @param paramStatusAutorizado
	 *            the param status autorizado
	 * @param ultimoHistoricoConsumo
	 *            the ultimo historico consumo
	 * @param periodicidade
	 *            the periodicidade
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Map<String, Integer> gerarAnoMesReferenciaCicloInicialFinal(PontoConsumo pontoConsumo, Date dataRealizacaoLeitura,
					String paramStatusAutorizado, HistoricoConsumo ultimoHistoricoConsumo, Periodicidade periodicidade)
					throws GGASException {

		Map<String, Integer> mapaAnoMesReferenciaCicloInicialFinal = new LinkedHashMap<>();

		SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = null;

		supervisorioMedicaoDiaria = (SupervisorioMedicaoDiaria) this.criarSupervisorioMedicaoDiaria();
		supervisorioMedicaoDiaria.setCodigoPontoConsumoSupervisorio(pontoConsumo.getCodigoPontoConsumoSupervisorio());
		supervisorioMedicaoDiaria.setDataRealizacaoLeitura(dataRealizacaoLeitura);

		Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura = new LinkedHashMap<>();

		this.preencherMapaComListaReferenciaLeitura(pontoConsumo, null, dataRealizacaoLeitura, mapaComListaReferenciaLeitura);

		Map<String, Object> mapaAnoMesReferenciaCicloRetorno = this.obterAnoMesReferenciaCiclo(mapaComListaReferenciaLeitura,
						supervisorioMedicaoDiaria.getDataRealizacaoLeitura(), pontoConsumo, periodicidade, Boolean.TRUE);

		Map<String, Object> mapaAnoMesReferenciaCiclo = new LinkedHashMap<>();

		mapaAnoMesReferenciaCiclo.put("anoMesReferencia", mapaAnoMesReferenciaCicloRetorno.get("anoMesFaturamento"));
		mapaAnoMesReferenciaCiclo.put("ciclo", mapaAnoMesReferenciaCicloRetorno.get("numeroCiclo"));

		mapaAnoMesReferenciaCicloInicialFinal.put("anoMesReferenciaFinal", (Integer) mapaAnoMesReferenciaCiclo.get("anoMesReferencia"));
		mapaAnoMesReferenciaCicloInicialFinal.put("cicloFinal", (Integer) mapaAnoMesReferenciaCiclo.get("ciclo"));

		return mapaAnoMesReferenciaCicloInicialFinal;
	}

	/**
	 * Comparar ano mes referencia ciclo historico medicao supervisorio diario.
	 *
	 * @param historicoMedicao
	 *            the historico medicao
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param isObterAnoMesReferenciaCicloInicial
	 *            the is obter ano mes referencia ciclo inicial
	 * @return the map
	 */
	public Map<String, Integer> compararAnoMesReferenciaCicloHistoricoMedicaoSupervisorioDiario(HistoricoMedicao historicoMedicao,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, Boolean isObterAnoMesReferenciaCicloInicial) {

		Map<String, Integer> anoMesReferenciaCicloMap = null;
		Integer anoMesReferencia = null;
		Integer ciclo = null;

		if (historicoMedicao != null && supervisorioMedicaoDiaria != null) {

			Date dataLeituraUltimoHistoricoMedicao = historicoMedicao.getDataLeituraInformada();
			Integer anoMesReferenciaUltimoHistoricoMedicao = historicoMedicao.getAnoMesLeitura();
			Integer cicloUltimoHistoricoMedicao = historicoMedicao.getNumeroCiclo();

			Date dataLeituraSupervisorio = supervisorioMedicaoDiaria.getDataRealizacaoLeitura();
			Integer anoMesReferenciaSupervisorio = supervisorioMedicaoDiaria.getDataReferencia();
			Integer cicloSupervisorio = supervisorioMedicaoDiaria.getNumeroCiclo();

			if (dataLeituraUltimoHistoricoMedicao.compareTo(dataLeituraSupervisorio) >= CONSTANTE_NUMERO_ZERO) {

				if (isObterAnoMesReferenciaCicloInicial) {

					anoMesReferencia = anoMesReferenciaSupervisorio;
					ciclo = cicloSupervisorio;
				} else {

					anoMesReferencia = anoMesReferenciaUltimoHistoricoMedicao;
					ciclo = cicloUltimoHistoricoMedicao;
				}

			} else {

				if (isObterAnoMesReferenciaCicloInicial) {

					anoMesReferencia = anoMesReferenciaUltimoHistoricoMedicao;
					ciclo = cicloUltimoHistoricoMedicao;
				} else {

					anoMesReferencia = anoMesReferenciaSupervisorio;
					ciclo = cicloSupervisorio;
				}

			}

		} else {
			if (historicoMedicao != null) {
				anoMesReferencia = historicoMedicao.getAnoMesLeitura();
				ciclo = historicoMedicao.getNumeroCiclo();
			}

			if (supervisorioMedicaoDiaria != null) {
				anoMesReferencia = supervisorioMedicaoDiaria.getDataReferencia();
				ciclo = supervisorioMedicaoDiaria.getNumeroCiclo();
			}
		}

		if (anoMesReferencia != null && ciclo != null) {

			anoMesReferenciaCicloMap = new LinkedHashMap<>();
			anoMesReferenciaCicloMap.put("anoMesReferencia", anoMesReferencia);
			anoMesReferenciaCicloMap.put("ciclo", ciclo);
		}

		return anoMesReferenciaCicloMap;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#validarAnoMesReferenciaCiclo(java.lang.Integer, java.lang.Integer,
	 * br.com.ggas.cadastro.imovel.PontoConsumo, java.util.Date, java.lang.String, br.com.ggas.medicao.consumo.HistoricoConsumo,
	 * br.com.ggas.medicao.rota.Periodicidade)
	 */
	// validacao para tela
	@Override
	public Boolean validarAnoMesReferenciaCiclo(Integer anoMesReferenciaSupervisorio, Integer cicloSupervisorio, PontoConsumo pontoConsumo,
					Date dataRealizacaoLeitura, String paramStatusAutorizado, HistoricoConsumo ultimoHistoricoConsumo,
					Periodicidade periodicidade) throws GGASException {

		this.gerarAnoMesReferenciaCicloInicialFinal(pontoConsumo, dataRealizacaoLeitura, paramStatusAutorizado, ultimoHistoricoConsumo,
						periodicidade);

		Boolean isAnoMesReferenciaCicloValido = Boolean.FALSE;

		Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura = new LinkedHashMap<>();
		preencherMapaComListaReferenciaLeitura(pontoConsumo, null, dataRealizacaoLeitura, mapaComListaReferenciaLeitura);
		List<Map<String, Object>> listaMapaReferenciaLeitura = mapaComListaReferenciaLeitura.get(pontoConsumo.getRota()
						.getGrupoFaturamento().getChavePrimaria());
		Integer anoMesFaturamento = null;
		Integer numeroCiclo = null;
		Date dataFim = null;

		if (listaMapaReferenciaLeitura != null) {
			for (Map<String, Object> mapa : listaMapaReferenciaLeitura) {

				dataFim = (Date) mapa.get("dataFim");
				anoMesFaturamento = (Integer) mapa.get("anoMesFaturamento");
				numeroCiclo = (Integer) mapa.get("numeroCiclo");

				if (dataFim.compareTo(dataRealizacaoLeitura) >= CONSTANTE_NUMERO_ZERO) {
					break;
				}
			}
		}

		if (anoMesFaturamento != null && numeroCiclo != null && anoMesFaturamento.equals(anoMesReferenciaSupervisorio)
						&& numeroCiclo.equals(cicloSupervisorio)) {
			isAnoMesReferenciaCicloValido = Boolean.TRUE;
		}

		return isAnoMesReferenciaCicloValido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.supervisorio.ControladorSupervisorio#preencherMapaComListaReferenciaLeitura(br.com.ggas.cadastro.imovel.
	 * PontoConsumo,
	 * java.util.Date, java.util.Map)
	 */
	@Override
	public Map<String, Object> preencherMapaComListaReferenciaLeitura(PontoConsumo pontoConsumo,
					ContratoPontoConsumo contratoPontoConsumo, Date dataRealizacaoLeitura,
					Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura) throws GGASException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ContratoPontoConsumo contratoPontoConsumoFaturavel = null;
		if (contratoPontoConsumo == null) {
			contratoPontoConsumoFaturavel = controladorContrato.obterContratoFaturavelPorPontoConsumo(
							pontoConsumo.getChavePrimaria(), Boolean.TRUE);
		} else {
			contratoPontoConsumoFaturavel = contratoPontoConsumo;
		}

		Map<String, Object> mapaContratoAtivo = controladorContrato.validarContratoAtivoPorDataLeitura(dataRealizacaoLeitura,
						contratoPontoConsumoFaturavel, pontoConsumo);

		if (pontoConsumo.getRota() != null) {
			Long idGrupoFaturamento = pontoConsumo.getRota().getGrupoFaturamento().getChavePrimaria();

			if (mapaComListaReferenciaLeitura.isEmpty() || !mapaComListaReferenciaLeitura.containsKey(idGrupoFaturamento)) {

				ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
								.getInstancia().getBeanPorID(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

				ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
								.getBeanPorID(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
				ConstanteSistema constanteAtividadeRegistrarLeitura = controladorConstanteSistema
								.obterConstantePorCodigo(Constantes.C_CODIGO_ATIVIDADE_REGISTRAR_LEITURA);

				Map<String, Object> filtro = new HashMap<>();
				filtro.put("habilitado", Boolean.TRUE);
				filtro.put("idGrupoFaturamento", idGrupoFaturamento);
				filtro.put("idAtividade", Long.valueOf(constanteAtividadeRegistrarLeitura.getValor()));
				filtro.put("atividadeRealizada", Boolean.FALSE);

				String ordenacao = " cronogramaAtividadeFaturamento.dataFim asc ";

				List<CronogramaAtividadeFaturamento> listaCronogramaAtividadeFaturamento =
								(List<CronogramaAtividadeFaturamento>) controladorCronogramaFaturamento
								.consultarCronogramaAtividadeFaturamento(filtro, ordenacao);

				if (listaCronogramaAtividadeFaturamento != null && !listaCronogramaAtividadeFaturamento.isEmpty()) {

					this.definirReferenciaCronogramaAtividadeFaturamento(
									listaCronogramaAtividadeFaturamento, mapaComListaReferenciaLeitura);
				}

			}
		}

		return mapaContratoAtivo;
	}

	/**
	 * Validar desfazer supervisorio.
	 *
	 * @param supervisorioMedicaoVO
	 *            the supervisorio medicao vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDesfazerSupervisorio(SupervisorioMedicaoVO supervisorioMedicaoVO) throws NegocioException {

		if (supervisorioMedicaoVO.getMedidor() == null && supervisorioMedicaoVO.getPontoconsumo() == null) {
			throw new NegocioException(Constantes.ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_PONTO_CONSUMO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#desfazerSupervisorio(java.util.Collection)
	 */
	@Override
	public void desfazerSupervisorio(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO) throws ConcorrenciaException,
					NegocioException {

		Map<String, Object> parametros = new HashMap<>();
		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getBeanPorID(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoVO) {

			this.validarDesfazerSupervisorio(supervisorioMedicaoVO);

			parametros.put("numeroCiclo", supervisorioMedicaoVO.getNumeroCiclo());
			parametros.put("anoMesFaturamento", supervisorioMedicaoVO.getDataReferencia());
			parametros.put("codigoPontoConsumoSupervisorio", supervisorioMedicaoVO.getCodigoPontoConsumoSupervisorio());

			Collection<HistoricoMedicao> listaHistoricoMedicao = controladorCronogramaFaturamento
							.consultarDadosRegistrarLeitura(parametros);

			if (listaHistoricoMedicao.isEmpty()) {

				parametros.clear();
				parametros.put("colecaoPaginada", null);
				parametros.put("numeroCiclo", Long.parseLong(supervisorioMedicaoVO.getNumeroCiclo().toString()));
				parametros.put("anoMesReferenciaLido", Long.parseLong(supervisorioMedicaoVO.getDataReferencia().toString()));
				parametros.put("codigoPontoConsumoSupervisorio", supervisorioMedicaoVO.getCodigoPontoConsumoSupervisorio());
				parametros.put("indicadorProcessado", Boolean.FALSE);
				parametros.put(INDICADOR_INTEGRADO, "1");

				Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaConsulta = this.consultarSupervisorioMedicaoDiaria(
								parametros, null, StringUtils.trim(null));

				if (!listaSupervisorioMedicaoDiariaConsulta.isEmpty()) {
					for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiariaConsulta) {
						supervisorioMedicaoDiaria.setIndicadorIntegrado(Boolean.FALSE);
						super.atualizar(supervisorioMedicaoDiaria, getClasseEntidadeSupervisorioMedicaoDiaria());
					}
				} else {
					throw new NegocioException(Constantes.ERRO_DESFAZER_DADOS_SUPERVISORIO_SEM_DADOS, true);
				}

			} else {
				throw new NegocioException(Constantes.ERRO_DESFAZER_DADOS_SUPERVISORIO, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#desfazerConsolidarSupervisorio(java.util.Collection)
	 */
	@Override
	public void desfazerConsolidarSupervisorio(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO) throws ConcorrenciaException,
					NegocioException {

		Map<String, Object> parametros = new HashMap<>();

		for (SupervisorioMedicaoVO supervisorioMedicaoVO : listaSupervisorioMedicaoVO) {

			parametros.put("colecaoPaginada", null);
			if (supervisorioMedicaoVO.getNumeroCiclo() != null) {
				parametros.put("numeroCiclo", Long.parseLong(supervisorioMedicaoVO.getNumeroCiclo().toString()));
			}
			if (supervisorioMedicaoVO.getDataReferencia() != null) {
				parametros.put("anoMesReferenciaLido", Long.parseLong(supervisorioMedicaoVO.getDataReferencia().toString()));
			}
			parametros.put("codigoPontoConsumoSupervisorio", supervisorioMedicaoVO.getCodigoPontoConsumoSupervisorio());
			parametros.put(INDICADOR_INTEGRADO, "1");
			parametros.put("indicadorProcessado", Boolean.FALSE);

			Collection<SupervisorioMedicaoDiaria> listaSupervisorioDiariaConsulta = this.consultarSupervisorioMedicaoDiaria(parametros,
							null, StringUtils.trim(null));

			if (listaSupervisorioDiariaConsulta.isEmpty()) {

				parametros.put("colecaoPaginada", null);
				parametros.put("codigoPontoConsumoSupervisorio", supervisorioMedicaoVO.getCodigoPontoConsumoSupervisorio());
				parametros.put(INDICADOR_INTEGRADO, Boolean.FALSE);
				parametros.put("isDesconsolidarSupervisorio", Boolean.TRUE);

				Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = this.consultarSupervisorioMedicaoHoraria(
								parametros, null, StringUtils.trim(null));

				for (SupervisorioMedicaoHoraria supervisorioMedicaoHoraria : listaSupervisorioMedicaoHoraria) {
					supervisorioMedicaoHoraria.setSupervisorioMedicaoDiaria(null);
					supervisorioMedicaoHoraria.setSupervisorioMedicaoAnormalidade(null);
					supervisorioMedicaoHoraria.setIndicadorConsolidada(Boolean.FALSE);
					supervisorioMedicaoHoraria.setStatus(null);
					supervisorioMedicaoHoraria.setUltimoUsuarioAlteracao(null);
					supervisorioMedicaoHoraria.setHabilitado(Boolean.TRUE);
					this.atualizar(supervisorioMedicaoHoraria, SupervisorioMedicaoHorariaImpl.class);
				}

				parametros.put(INDICADOR_INTEGRADO, null);
				parametros.put("indicadorProcessado", null);
				Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario = this.consultarSupervisorioMedicaoComentario(
								parametros, null, StringUtils.trim(null));

				for (SupervisorioMedicaoComentario supervisorioMedicaoComentario : listaSupervisorioMedicaoComentario) {
					this.remover(supervisorioMedicaoComentario, SupervisorioMedicaoComentarioImpl.class);
				}

				removerOuAtualizarMedicoesDiarias(parametros);

			} else {
				throw new NegocioException(Constantes.ERRO_DESFAZER_CONSOLIDAR_SUPERVISORIO, true);
			}
		}
	}

	/**
	 * Esse método é responsável por remover ou atualizar as medições diárias.
	 * Caso o tipo de medição seja horário, este método remove todas as medições diárias.
	 * Caso o tipo de medição seja diário, este método atualiza as medições diárias
	 * fazendo com que elas retonem ao seu estado inicial para posterior consolidação dos dados.
	 *
	 *
	 * @param parametros
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	private void removerOuAtualizarMedicoesDiarias(Map<String, Object> parametros)
			throws NegocioException, ConcorrenciaException {

		String tipoIntegracao = obtemValorParametroTipoIntegracao();

		String tipoIntegracaoHoraria = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_INTEGRACAO_SUPERVISORIO_HORARIA);

		if(tipoIntegracao.equals(tipoIntegracaoHoraria)) {
			Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = this.consultarSupervisorioMedicaoDiaria(parametros,
					null, StringUtils.trim(null));
			for (Iterator<SupervisorioMedicaoDiaria> iterator = listaSupervisorioMedicaoDiaria.iterator(); iterator.hasNext();) {
				SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = iterator.next();
				this.remover(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);
			}
		}else{
			parametros.put(HABILITADO, Boolean.TRUE);
			Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = this.consultarSupervisorioMedicaoDiaria(parametros,
					null, StringUtils.trim(null));
			for (Iterator<SupervisorioMedicaoDiaria> iterator = listaSupervisorioMedicaoDiaria.iterator(); iterator.hasNext();) {
				SupervisorioMedicaoDiaria supervisorioMedicaoDiaria = iterator.next();

				Collection<SupervisorioMedicaoDiaria> listaMedicaoDiariaDesabilitada =
						consultarMedicaoDiariaDesabilitada(supervisorioMedicaoDiaria);

				if(listaMedicaoDiariaDesabilitada.isEmpty()){
					restaurarParaEstadoInicialEAtualizar(supervisorioMedicaoDiaria);
				}else{
					atualizaMedicaoOriginalERemovePosteriores(supervisorioMedicaoDiaria, listaMedicaoDiariaDesabilitada);
				}
			}
		}


	}

	/**
	 * Este método varre todas as medicoes desabilitadas para achar a primeira versão
	 * da medição e remove as medições posteriores que foram concebidas através de
	 * uma alteração da medição que foi integrada inicialmente.
	 *
	 * @param supervisorioMedicaoDiaria
	 * @param listaMedicaoDiariaDesabilitada
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	private void atualizaMedicaoOriginalERemovePosteriores(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			Collection<SupervisorioMedicaoDiaria> listaMedicaoDiariaDesabilitada) throws NegocioException, ConcorrenciaException {

		Iterator<SupervisorioMedicaoDiaria> iterator = listaMedicaoDiariaDesabilitada.iterator();
		restaurarParaEstadoInicialEAtualizar(iterator.next());

		while(iterator.hasNext()) {
			this.remover(iterator.next(), SupervisorioMedicaoDiariaImpl.class);
		}

		this.remover(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);
	}

	/**
	 * Este método é responsável por restaurar os dados de uma médicao diária
	 * deixado ela no estado inicial, quando foi integrada ao sistema.
	 *
	 * @param supervisorioMedicaoDiaria
	 * @throws ConcorrenciaException
	 * @throws NegocioException
	 */
	private void restaurarParaEstadoInicialEAtualizar(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
			throws ConcorrenciaException, NegocioException {
		supervisorioMedicaoDiaria.setSupervisorioMedicaoAnormalidade(null);
		supervisorioMedicaoDiaria.setHabilitado(Boolean.TRUE);
		supervisorioMedicaoDiaria.setIndicadorConsolidada(Boolean.FALSE);
		supervisorioMedicaoDiaria.setIndicadorProcessado(Boolean.FALSE);
		supervisorioMedicaoDiaria.setIndicadorIntegrado(Boolean.FALSE);
		this.atualizar(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);
	}

	private String obtemValorParametroTipoIntegracao() throws NegocioException {
		ControladorParametroSistema controladorParametrosSistema =
				ServiceLocator.getInstancia().getControladorParametroSistema();

		return (String) controladorParametrosSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);
	}

	private Collection<SupervisorioMedicaoDiaria> consultarMedicaoDiariaDesabilitada(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
			throws NegocioException {
		Map<String, Object>filtro = new HashMap<>();
		filtro.put(DATA_REALIZACAO_LEITURA, supervisorioMedicaoDiaria.getDataRealizacaoLeitura());
		filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, supervisorioMedicaoDiaria.getCodigoPontoConsumoSupervisorio());
		filtro.put(HABILITADO, Boolean.FALSE);
		return consultarSupervisorioMedicaoDiaria(filtro, ULTIMA_ALTERACAO, StringUtils.trim(null));
	}

	@Override
	public List<Object> validarComposicaoVirtualPorCodSupervisorio(String[] arrayEnderecoRemoto, Integer numeroCiclo, Integer anoMes) {

		Criteria criteria = this.createCriteria(SupervisorioMedicaoDiaria.class);
		criteria.add(Restrictions.eq(NUMERO_CICLO, numeroCiclo));
		criteria.add(Restrictions.eq(DATA_REFERENCIA, anoMes));
		criteria.add(Restrictions.eq(INDICADOR_INTEGRADO, true));
		criteria.add(Restrictions.eq(HABILITADO, true));
		criteria.add(Restrictions.in(CODIGO_PONTO_CONSUMO_SUPERVISORIO, arrayEnderecoRemoto)).setProjection(
						Projections.projectionList()
										.add(Projections.sqlGroupProjection("trunc(SUMD_TM_LEITURA) as dataRealizacaoLeitura",
														"SUMD_TM_LEITURA", new String[] {DATA_REALIZACAO_LEITURA},
														new DateType[] {StandardBasicTypes.DATE})).add(Projections.rowCount()));
		return criteria.list();
	}

	/**
	 * Atualiza as anormalidades de uma lista de entidades de SupervisorioMedicaoHoraria
	 * @param supervisorioMedicaoAnormalidade
	 * @param listaSupervisorioMedicaoHoraria
	 */
	@Override
	public void atualizarAnormalidadeDoSupervisorioMedicaoHoraria(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade,
					List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria) {

		final int tamanhoLimiteLista = 1000;

		StringBuilder hql = new StringBuilder();
		hql.append(" UPDATE ");
		hql.append(getClasseEntidadeSupervisorioMedicaoHoraria().getSimpleName());
		hql.append(" AS smh ");
		hql.append(" SET smh.supervisorioMedicaoAnormalidade = :anormalidade, ");
		hql.append(" smh.versao = smh.versao + 1 ");
		hql.append(" WHERE smh.chavePrimaria IN (:medicoes) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		while (listaSupervisorioMedicaoHoraria.size() > tamanhoLimiteLista) {
			List<SupervisorioMedicaoHoraria> subLista = listaSupervisorioMedicaoHoraria.subList(CONSTANTE_NUMERO_ZERO, tamanhoLimiteLista);
			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(subLista);
			query.setParameter("anormalidade", supervisorioMedicaoAnormalidade);
			query.setParameterList("medicoes", chavesPrimarias);
			query.executeUpdate();
			listaSupervisorioMedicaoHoraria.subList(CONSTANTE_NUMERO_ZERO, tamanhoLimiteLista).clear();
		}

		if (!listaSupervisorioMedicaoHoraria.isEmpty()) {
			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(listaSupervisorioMedicaoHoraria);
			query.setParameter("anormalidade", supervisorioMedicaoAnormalidade);
			query.setParameterList("medicoes", chavesPrimarias);
			query.executeUpdate();
		}

	}
	/**
	 * Consolida as anormalidades de um coleção de entidades de SupervisorioMedicaoHoraria mapeadas
	 * por SupervisorioMedicaoAnormalidade
	 * @param mapaAnormalidade
	 */
	@Override
	public void consolidarAnormalidadesDoSupervisorioMedicaoHoraria(Map<SupervisorioMedicaoAnormalidade,
					List<SupervisorioMedicaoHoraria>> mapaAnormalidade) {

		for (Entry<SupervisorioMedicaoAnormalidade, List<SupervisorioMedicaoHoraria>> entry : mapaAnormalidade.entrySet()) {
			SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade = entry.getKey();
			List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = mapaAnormalidade.get(supervisorioMedicaoAnormalidade);
			this.atualizarAnormalidadeDoSupervisorioMedicaoHoraria(supervisorioMedicaoAnormalidade, listaSupervisorioMedicaoHoraria);
		}

	}

	/**
	 * Agrupa uma lista de SupervisorioMedicaoHoraria por SupervisorioMedicaoAnormalidade
	 * @param listaSupervisorioMedicaoHoraria
	 * @return Map - lista de SupervisorioMedicaoHoraria por SupervisorioMedicaoAnormalidade
	 */
	@Override
	public  Map<SupervisorioMedicaoAnormalidade, List<SupervisorioMedicaoHoraria>> agruparSupervisorioMedicaoHorariaPorAnormalidade(
					Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria) {

		Map<SupervisorioMedicaoAnormalidade, List<SupervisorioMedicaoHoraria>> mapaAnormalidade = new HashMap<>();
		List<SupervisorioMedicaoHoraria> listaSupervisorioPorAnormalidade;
		for (SupervisorioMedicaoHoraria smh : listaSupervisorioMedicaoHoraria) {
			SupervisorioMedicaoAnormalidade suMedicaoAnormalidade = smh.getSupervisorioMedicaoAnormalidade();
			if (suMedicaoAnormalidade != null) {
				if (!mapaAnormalidade.containsKey(suMedicaoAnormalidade)) {
					listaSupervisorioPorAnormalidade = new ArrayList<>();
					mapaAnormalidade.put(suMedicaoAnormalidade, listaSupervisorioPorAnormalidade);
				} else {
					listaSupervisorioPorAnormalidade = mapaAnormalidade.get(suMedicaoAnormalidade);
				}
				listaSupervisorioPorAnormalidade.add(smh);
			}
		}
		return mapaAnormalidade;
	}
	/**
	 * Gerar medicoes horarias extras media horaria
	 * @param medicoes - {@link SupervisorioMedicaoDiaria}
	 * @return list SupervisorioMedicaoHorario - {@link SupervisorioMedicaoDiaria}
	 */
	public List<SupervisorioMedicaoHoraria> gerarMedicoesHorariasExtrasMediaHoraria(List<SupervisorioMedicaoHoraria> medicoes) {
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();

		return processador.preencherMedicoesMediaHoraria(medicoes);
	}
	
	/**
	 * Gerar medicoes horarias extras media diaria
	 * @param medicoesHorarias - {@link SupervisorioMedicaoHoraria}
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @return list SupervisorioMedicaoHorario - {@link SupervisorioMedicaoDiaria}
	 * @throws GGASException the GGAS Exception
	 */
	public List<SupervisorioMedicaoHoraria> gerarMedicoesHorariasExtrasMediaDiaria(List<SupervisorioMedicaoHoraria> medicoesHorarias,
			PontoConsumo pontoConsumo) throws GGASException {

		SupervisorioMedicaoHoraria primeiraMedicao = medicoesHorarias.get(CONSTANTE_NUMERO_ZERO);

		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		List<SupervisorioMedicaoDiaria> medicoesDiarias = 
				consultarMedicoesDiariasPreencherHorarios(pontoConsumo, primeiraMedicao.getDataRealizacaoLeitura(), null);
		
		return processador.preencherMedicoesMediaDiaria(medicoesHorarias, medicoesDiarias);
	}
	/**
	 * Consultar Medicoes Diarias Preencher Horarios
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @param dataInicialRealizacaoLeitura - {@link Date}
	 * @param medidor - {@link Medidor}
	 * @return list SupervisorioMedicaoHorario - {@link SupervisorioMedicaoDiaria}
	 * @throws GGASException the GGAS Exception
	 */
	private List<SupervisorioMedicaoDiaria> consultarMedicoesDiariasPreencherHorarios(PontoConsumo pontoConsumo, 
			Date dataInicialRealizacaoLeitura, Medidor medidor) throws GGASException {
		Map<String, Object> filtro = new HashMap<String, Object>();

		ServiceLocator.getInstancia().getControladorParametroSistema();
		
		int quantidadeDiasMedicosDiarias = obterParametro(Constantes.PARAMETRO_QUANTIDADE_MEDICAO_DIARIA_PARA_MEDIA);

		if (pontoConsumo != null) {
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, pontoConsumo.getCodigoPontoConsumoSupervisorio());
		}
		if (medidor != null) {
			filtro.put(CODIGO_PONTO_CONSUMO_SUPERVISORIO, medidor.getCodigoMedidorSupervisorio());
		}

		filtro.put(DATA_INICIAL_REALIZACAO_LEITURA, dataInicialRealizacaoLeitura);
		filtro.put(DATA_FINAL_REALIZACAO_LEITURA, DataUtil.incrementarDia(dataInicialRealizacaoLeitura, quantidadeDiasMedicosDiarias));

		filtro.put(HABILITADO, Boolean.TRUE);

		// Monta a ordenação da consulta
		String ordenacao = "smd." + DATA_REALIZACAO_LEITURA;

		// Recupera as medições horárias ainda não processadas ordenadas por codigoPontoConsumoSupervisorio e dataRealizacaoLeitura
		return (List<SupervisorioMedicaoDiaria>) this.consultarSupervisorioMedicaoDiaria(filtro, ordenacao);
	}

	/**
	 * Realizar a contagem de pontos de consumo na atividade do cronograma
	 * referente ao grupo de fatura no ano, mês e ciclo e qual o tipo de leitura
	 *
	 * @param grupoFaturamento
	 * @param param 
	 * @param anoMesFaturamento
	 * @param ciclo
	 * @param leituraTipo
	 */
	@Override
	public Long consultarQuantidadePontosConsumoTotaisNaAtividade(Long grupoFaturamento, Integer anoMesFaturamento, Integer ciclo,
			Long leituraTipo) {

		StringBuilder hql = new StringBuilder();
		
		hql.append("SELECT COUNT(DISTINCT smd.codigoPontoConsumoSupervisorio) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName()).append(" smd, ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName()).append(" pc ");
		hql.append(" JOIN pc.rota r ");
		hql.append(" WHERE ");		
		hql.append(" smd.habilitado = 1 ");
		hql.append(" AND smd.dataReferencia = :anoMesFaturamento ");
		hql.append(" AND smd.numeroCiclo = :ciclo ");
		hql.append(" AND r.tipoLeitura.chavePrimaria = :tipoLeitura ");
		hql.append(" AND r.grupoFaturamento.chavePrimaria = :grupoFaturamento ");
		hql.append(" AND smd.codigoPontoConsumoSupervisorio = pc.codigoPontoConsumoSupervisorio " );
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setLong("anoMesFaturamento", anoMesFaturamento);
		query.setLong("ciclo", ciclo);
		query.setLong("grupoFaturamento", grupoFaturamento);
		query.setLong("tipoLeitura",leituraTipo);

		return (Long) query.list().stream().findFirst().orElse(0L);
	}
	
	/**
	 * Consulta a quantidade de pontos de consumos que estão "presos" em uma determinada etapa
	 * do faturamaento
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param anoMesFaturamento ano mês faturamento
	 * @param ciclo número do ciclo
	 * @param leituraTipo tipo da leitura
	 * @return retorna a quantidade de pontos de consumo que estão em uma determinada situação
	 */
	@Override
	public Long consultarQuantidadePontosConsumoNaAtividade(Long grupoFaturamento, Integer anoMesFaturamento, Integer ciclo,
			Long leituraTipo) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT COUNT(DISTINCT smd.codigoPontoConsumoSupervisorio) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName()).append(" smd, ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName()).append(" pc ");
		hql.append(" JOIN pc.rota r ");
		hql.append(" WHERE ");
		hql.append(" smd.habilitado = 1 ");
		hql.append(" AND smd.dataReferencia = :anoMesFaturamento ");
		hql.append(" AND smd.numeroCiclo = :ciclo ");
		hql.append(" AND r.tipoLeitura.chavePrimaria = :tipoLeitura ");
		hql.append(" AND r.grupoFaturamento.chavePrimaria = :grupoFaturamento ");
		hql.append(" AND smd.indicadorProcessado = 0");
		hql.append(" AND smd.codigoPontoConsumoSupervisorio = pc.codigoPontoConsumoSupervisorio ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("anoMesFaturamento", anoMesFaturamento);
		query.setLong("ciclo", ciclo);
		query.setLong("grupoFaturamento", grupoFaturamento);
		query.setLong("tipoLeitura", leituraTipo);

		return (Long) query.list().stream().findFirst().orElse(0L);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.supervisorio.ControladorSupervisorio#transferirSupervisorioMedicoesDiaria(java.util.Collection,
	 * java.util.Map)
	 */
	@Override
	public void transferirSupervisorioMedicoesDiariaPorDia(Map<String, Object> filtro)
			throws NegocioException, ConcorrenciaException, FormatoInvalidoException {

		Boolean indicadorRegistrosProcessados = Boolean.FALSE;

		Collection<SupervisorioMedicaoDiaria> listaAlterar = this.consultarSupervisorioMedicaoDiaria(filtro, null,
				"supervisorioMedicaoAnormalidade", "status", "ultimoUsuarioAlteracao");
		if (listaAlterar.isEmpty()) {
			indicadorRegistrosProcessados = Boolean.FALSE;
		} else {
			indicadorRegistrosProcessados = Boolean.TRUE;
		}
		for (SupervisorioMedicaoDiaria superMedicaoDiaria : listaAlterar) {
			superMedicaoDiaria.setIndicadorIntegrado(true);
			super.atualizar(superMedicaoDiaria, getClasseEntidadeSupervisorioMedicaoDiaria());
		}

		if (!indicadorRegistrosProcessados) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REGISTROS_NAO_PROCESSADOS, true);
		}

	}
	
	
	@Override
	public Boolean conferirQuantidadeMedicoesSupervisorio(SupervisorioMedicaoVO superMedicaoVO)
			throws NegocioException {
		
		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
				.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		
		GrupoFaturamento grupoFaturamento = controladorCronogramaFaturamento
				.obterGrupoFaturamentoPorCodigoSupervisorio(superMedicaoVO.getCodigoPontoConsumoSupervisorio());
		
		
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("descricaoAtividadeSistema", "REGISTRAR LEITURA");
		filtro.put("habilitado", Boolean.TRUE);
		filtro.put("anoMesReferencia", superMedicaoVO.getDataReferencia());
		filtro.put("numeroCiclo", superMedicaoVO.getNumeroCiclo());
		filtro.put("codigoGrupoFaturamento", grupoFaturamento.getChavePrimaria());
		
		
		
		Collection<CronogramaAtividadeFaturamento> cronogramasAtuais = controladorCronogramaFaturamento
				.consultarCronogramaAtividadeFaturamento(filtro);
		
		
		filtro.remove("anoMesReferencia");
		filtro.remove("numeroCiclo");
		filtro.put("realizado", Boolean.TRUE);
		Integer periodoEntreCronogramas = 0;
		
		Collection<CronogramaAtividadeFaturamento> cronogramasAnteriores = controladorCronogramaFaturamento
				.consultarCronogramaAtividadeFaturamento(filtro);
		
		if ((cronogramasAtuais != null && !cronogramasAtuais.isEmpty())
				&& (cronogramasAnteriores != null && !cronogramasAnteriores.isEmpty())) {
			Date dataCronogramaAtividadeAtual = cronogramasAtuais.iterator().next().getDataFim();
			Date dataCronogramaAtividadeAnterior = cronogramasAnteriores.iterator().next().getDataFim();
			
			periodoEntreCronogramas = DataUtil.diferencaDiasEntreDatas(dataCronogramaAtividadeAnterior,
					dataCronogramaAtividadeAtual);								
		} else if (cronogramasAtuais != null && !cronogramasAtuais.isEmpty()){
			Date dataCronogramaAtividadeAtual = cronogramasAtuais.iterator().next().getDataFim();
			Date dataCronogramaAtividadeAnterior = DataUtil.gerarDataSemHoraPrimeiroDiaMes(DataUtil.decrementarDia(dataCronogramaAtividadeAtual, 1));
			
			periodoEntreCronogramas = DataUtil.diferencaDiasEntreDatas(dataCronogramaAtividadeAnterior,
					dataCronogramaAtividadeAtual);		
		}
		
		if(periodoEntreCronogramas.compareTo(superMedicaoVO.getQtdMedicao()) != 0) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	
	@Override 
	public Boolean conferirQuantidadeMedicoesIntegracaoParcial(Date dataInicio, Date dataFim, SupervisorioMedicaoVO medicao) {
		Integer periodoDias = DataUtil.diferencaDiasEntreDatas(dataInicio, dataFim) + 1;
		
		if(periodoDias.compareTo(medicao.getQtdMedicao()) == 0) {
			return Boolean.FALSE;
		}
		
		return Boolean.TRUE;
	}
	
}
