/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:09:42
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.horaria;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;

/**
 * Classe Supervisorio Medicao Horaria.
 * 
 *
 */
public interface SupervisorioMedicaoHoraria extends EntidadeNegocio {

	String BEAN_ID_SUPERVISORIO_MEDICAO_HORARIA = "supervisorioMedicaoHoraria";

	String SUPERVISORIO_MEDICAO_HORARIA = "SUPERVISORIO_MEDICAO_HORARIA";

	String DATA_HORA_LEITURA = "SUPERVISORIO_MEDICAO_HORARIA_DATA_LEITURA";

	String LEITURA_NAO_CORRIGIDA = "SUPERVISORIO_MEDICAO_HORARIA_LEITURA_NAO_CORRIGIDA";

	String LEITURA_CORRIGIDA = "SUPERVISORIO_MEDICAO_HORARIA_LEITURA_CORRIGIDA";

	String PRESSAO = "SUPERVISORIO_MEDICAO_HORARIA_PRESSAO";

	String TEMPERATURA = "SUPERVISORIO_MEDICAO_HORARIA_TEMPERATURA";

	/**
	 * @return SupervisorioMedicaoDiaria - Retorna supervisório diária.
	 */
	SupervisorioMedicaoDiaria getSupervisorioMedicaoDiaria();

	/**
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorioMedicaoDiaria to set
	 */
	void setSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria);

	/**
	 * @return the supervisorioMedicaoAnormalidade
	 */
	SupervisorioMedicaoAnormalidade getSupervisorioMedicaoAnormalidade();

	/**
	 * @param supervisorioMedicaoAnormalidade
	 *            the supervisorioMedicaoAnormalidade to set
	 */
	void setSupervisorioMedicaoAnormalidade(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade);

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	String getCodigoPontoConsumoSupervisorio();

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio);

	/**
	 * @return the indicadorTipoMedicao
	 */
	String getIndicadorTipoMedicao();

	/**
	 * @param indicadorTipoMedicao
	 *            the indicadorTipoMedicao to set
	 */
	void setIndicadorTipoMedicao(String indicadorTipoMedicao);

	/**
	 * @return the dataRealizacaoLeitura
	 */
	Date getDataRealizacaoLeitura();

	/**
	 * @param dataRealizacaoLeitura
	 *            the dataRealizacaoLeitura to set
	 */
	void setDataRealizacaoLeitura(Date dataRealizacaoLeitura);

	/**
	 * @return the leituraSemCorrecaoFatorPTZ
	 */
	BigDecimal getLeituraSemCorrecaoFatorPTZ();

	/**
	 * @param leituraSemCorrecaoFatorPTZ
	 *            the leituraSemCorrecaoFatorPTZ to set
	 */
	void setLeituraSemCorrecaoFatorPTZ(BigDecimal leituraSemCorrecaoFatorPTZ);

	/**
	 * @return the leituraComCorrecaoFatorPTZ
	 */
	BigDecimal getLeituraComCorrecaoFatorPTZ();

	/**
	 * @param leituraComCorrecaoFatorPTZ
	 *            the leituraComCorrecaoFatorPTZ to set
	 */
	void setLeituraComCorrecaoFatorPTZ(BigDecimal leituraComCorrecaoFatorPTZ);

	/**
	 * @return the pressao
	 */
	BigDecimal getPressao();

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	void setPressao(BigDecimal pressao);

	/**
	 * @return the temperatura
	 */
	BigDecimal getTemperatura();

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	void setTemperatura(BigDecimal temperatura);

	/**
	 * @return the indicadorConsolidada
	 */
	Boolean getIndicadorConsolidada();

	/**
	 * @param indicadorConsolidada
	 *            the indicadorConsolidada to set
	 */
	void setIndicadorConsolidada(Boolean indicadorConsolidada);

	/**
	 * @return the dataRegistroLeitura
	 */
	Date getDataRegistroLeitura();

	/**
	 * @param dataRegistroLeitura
	 *            the dataRegistroLeitura to set
	 */
	void setDataRegistroLeitura(Date dataRegistroLeitura);

	/**
	 * @return EntidadeConteudo - retorna status.
	 */
	EntidadeConteudo getStatus();

	/**
	 * @param status - Set status.
	 */
	void setStatus(EntidadeConteudo status);

	/**
	 * @return Usuário - Retorna ultima alteração do usuário.	
	 */
	Usuario getUltimoUsuarioAlteracao();

	/**
	 * @param ultimoUsuarioAlteracao - Set última alteração do usuário .
	 */
	void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao);
	
	/**
	 * @return indicadorTipoInclusaoMedicao - Retorna o indicador do tipo de inclusao da mdedicao..	
	 */
	String getIndicadorTipoInclusaoMedicao();

	/**
	 * @param indicadorTipoInclusaoMedicao - Set tipo de inclusao a medicao .
	 */
	void setIndicadorTipoInclusaoMedicao(String indicadorTipoInclusaoMedicao);

}
