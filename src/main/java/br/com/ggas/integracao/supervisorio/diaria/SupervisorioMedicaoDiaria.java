/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.diaria;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;

/**
 * Classe Supervisorio Medicao Diaria.
 * 
 *
 */
public interface SupervisorioMedicaoDiaria extends EntidadeNegocio {

	String SUPERVISORIO_MEDICAO_DIARIA_ROTULO = "SUPERVISORIO_MEDICAO_DIARIA_ROTULO";

	String BEAN_ID_SUPERVISORIO_MEDICAO_DIARIA = "supervisorioMedicaoDiaria";

	String SUPERVISORIO_MEDICAO_DIARIA = "SUPERVISORIO_MEDICAO_DIARIA";

	String ANO_MES_REFERENCIA = "SUPERVISORIO_MEDICAO_DIARIA_ANO_MES_REFERENCIA";

	String DATA_LEITURA = "SUPERVISORIO_MEDICAO_DIARIA_DATA_LEITURA";

	String LEITURA_NAO_CORRIGIDA = "SUPERVISORIO_MEDICAO_DIARIA_LEITURA_NAO_CORRIGIDA";

	String LEITURA_CORRIGIDA = "SUPERVISORIO_MEDICAO_DIARIA_LEITURA_CORRIGIDA";

	String CONSUMO_NAO_CORRIGIDA = "SUPERVISORIO_MEDICAO_DIARIA_CONSUMO_NAO_CORRIGIDA";

	String CONSUMO_CORRIGIDA = "SUPERVISORIO_MEDICAO_DIARIA_CONSUMO_CORRIGIDA";

	String PRESSAO = "SUPERVISORIO_MEDICAO_DIARIA_PRESSAO";

	String TEMPERATURA = "SUPERVISORIO_MEDICAO_DIARIA_TEMPERATURA";

	String FATOR_Z = "SUPERVISORIO_MEDICAO_DIARIA_FATOR_Z";

	String FATOR_PTZ = "SUPERVISORIO_MEDICAO_DIARIA_FATOR_PTZ";

	/**
	 * @return SupervisorioMedicaoAnormalidade - Retorna um objeto supervisório medição anormalidade.
	 */
	SupervisorioMedicaoAnormalidade getSupervisorioMedicaoAnormalidade();

	/**
	 * @param supervisorioMedicaoAnormalidade
	 *            the supervisorioMedicaoAnormalidade to set
	 */
	void setSupervisorioMedicaoAnormalidade(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade);

	/**
	 * @return the dataReferencia
	 */
	Integer getDataReferencia();

	/**
	 * @return String - Data referência formatada.
	 */
	String getDataReferenciaFormatado();

	/**
	 * @param dataReferencia
	 *            the dataReferencia to set
	 */
	void setDataReferencia(Integer dataReferencia);

	/**
	 * @return the numeroCiclo
	 */
	Integer getNumeroCiclo();

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	String getCodigoPontoConsumoSupervisorio();

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio);

	/**
	 * @return the dataRealizacaoLeitura
	 */
	Date getDataRealizacaoLeitura();

	/**
	 * @param dataRealizacaoLeitura
	 *            the dataRealizacaoLeitura to set
	 */
	void setDataRealizacaoLeitura(Date dataRealizacaoLeitura);

	/**
	 * @return the leituraSemCorrecaoFatorPTZ
	 */
	BigDecimal getLeituraSemCorrecaoFatorPTZ();

	/**
	 * @param leituraSemCorrecaoFatorPTZ
	 *            the leituraSemCorrecaoFatorPTZ to set
	 */
	void setLeituraSemCorrecaoFatorPTZ(BigDecimal leituraSemCorrecaoFatorPTZ);

	/**
	 * @return the leituraComCorrecaoFatorPTZ
	 */
	BigDecimal getLeituraComCorrecaoFatorPTZ();

	/**
	 * @param leituraComCorrecaoFatorPTZ
	 *            the leituraComCorrecaoFatorPTZ to set
	 */
	void setLeituraComCorrecaoFatorPTZ(BigDecimal leituraComCorrecaoFatorPTZ);

	/**
	 * @return the consumoSemCorrecaoFatorPTZ
	 */
	BigDecimal getConsumoSemCorrecaoFatorPTZ();

	/**
	 * @param consumoSemCorrecaoFatorPTZ
	 *            the consumoSemCorrecaoFatorPTZ to set
	 */
	void setConsumoSemCorrecaoFatorPTZ(BigDecimal consumoSemCorrecaoFatorPTZ);

	/**
	 * @return the consumoComCorrecaoFatorPTZ
	 */
	BigDecimal getConsumoComCorrecaoFatorPTZ();

	/**
	 * @param consumoComCorrecaoFatorPTZ
	 *            the consumoComCorrecaoFatorPTZ to set
	 */
	void setConsumoComCorrecaoFatorPTZ(BigDecimal consumoComCorrecaoFatorPTZ);

	/**
	 * @return the pressao
	 */
	BigDecimal getPressao();

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	void setPressao(BigDecimal pressao);

	/**
	 * @return the temperatura
	 */
	BigDecimal getTemperatura();

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	void setTemperatura(BigDecimal temperatura);

	/**
	 * @return the fatorPTZ
	 */
	BigDecimal getFatorPTZ();

	/**
	 * @param fatorPTZ
	 *            the fatorPTZ to set
	 */
	public void setFatorPTZ(BigDecimal fatorPTZ);

	/**
	 * @return the fatorZ
	 */
	BigDecimal getFatorZ();

	/**
	 * @param fatorZ
	 *            the fatorZ to set
	 */
	void setFatorZ(BigDecimal fatorZ);

	/**
	 * @return the indicadorIntegrado
	 */
	Boolean getIndicadorIntegrado();

	/**
	 * @param indicadorIntegrado
	 *            the indicadorIntegrado to set
	 */
	void setIndicadorIntegrado(Boolean indicadorIntegrado);

	/**
	 * @return the indicadorProcessado
	 */
	Boolean getIndicadorProcessado();

	/**
	 * @param indicadorProcessado
	 *            the indicadorProcessado to set
	 */
	void setIndicadorProcessado(Boolean indicadorProcessado);

	/**
	 * @return the dataRegistroLeitura
	 */
	Date getDataRegistroLeitura();

	/**
	 * @param dataRegistroLeitura
	 *            the dataRegistroLeitura to set
	 */
	void setDataRegistroLeitura(Date dataRegistroLeitura);

	/**
	 * @return Collection - Retorna uma coleção de SupervisorioMedicaoHoraria.
	 */
	Collection<SupervisorioMedicaoHoraria> getListaSupervisorioMedicaoHoraria();

	/**
	 * @param listaSupervisorioMedicaoHoraria
	 *            the listaSupervisorioMedicaoHoraria to set
	 */
	void setListaSupervisorioMedicaoHoraria(Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria);

	/**
	 * @return EntidadeConteudo - Retorna o status do objeto.
	 */
	EntidadeConteudo getStatus();

	/**
	 * @param status - Set Status.
	 */
	void setStatus(EntidadeConteudo status);

	/**
	 * @return Usuario - Retorna um usuário com informações último usuário alteração.
	 */
	Usuario getUltimoUsuarioAlteracao();

	/**
	 * @param ultimoUsuarioAlteracao - Set último usuário alteração.
	 */
	void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao);

	/**
	 * @return Boolean - indicador consolidada.
	 */
	Boolean getIndicadorConsolidada();

	/**
	 * @param indicadorConsolidado - Set indicador consolidada.
	 */
	void setIndicadorConsolidada(Boolean indicadorConsolidado);

	/**
	 * @return Boolean - Indicador medidor.
	 */
	Boolean getIndicadorMedidor();

	/**
	 * @param indicadorMedidor - Set Indicador medidor.
	 */
	void setIndicadorMedidor(Boolean indicadorMedidor);

}
