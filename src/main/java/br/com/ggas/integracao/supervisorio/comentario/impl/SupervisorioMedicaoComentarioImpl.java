/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:07:40
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.comentario.impl;

import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;

/**
 * Classe Supervisorio Medicao Comentario.
 * 
 *
 */
public class SupervisorioMedicaoComentarioImpl extends EntidadeNegocioImpl implements SupervisorioMedicaoComentario {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3519328751665919369L;

	private SupervisorioMedicaoHoraria supervisorioMedicaoHoraria;

	private SupervisorioMedicaoDiaria supervisorioMedicaoDiaria;

	private Usuario usuario;

	private Integer sequenciaComentario;

	private String descricao;

	/**
	 * @return the supervisorioMedicaoHoraria
	 */
	@Override
	public SupervisorioMedicaoHoraria getSupervisorioMedicaoHoraria() {

		return supervisorioMedicaoHoraria;
	}

	/**
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorioMedicaoHoraria to set
	 */
	@Override
	public void setSupervisorioMedicaoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) {

		this.supervisorioMedicaoHoraria = supervisorioMedicaoHoraria;
	}

	/**
	 * @return the supervisorioMedicaoDiaria
	 */
	@Override
	public SupervisorioMedicaoDiaria getSupervisorioMedicaoDiaria() {

		return supervisorioMedicaoDiaria;
	}

	/**
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorioMedicaoDiaria to set
	 */
	@Override
	public void setSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) {

		this.supervisorioMedicaoDiaria = supervisorioMedicaoDiaria;
	}

	/**
	 * @return the usuario
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/**
	 * @return the sequenciaComentario
	 */
	@Override
	public Integer getSequenciaComentario() {

		return sequenciaComentario;
	}

	/**
	 * @param sequenciaComentario
	 *            the sequenciaComentario to set
	 */
	@Override
	public void setSequenciaComentario(Integer sequenciaComentario) {

		this.sequenciaComentario = sequenciaComentario;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
