/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/02/2013 10:35:25
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio;

import java.io.Serializable;
import java.util.Map;

/**
 * Classe Resumo Supervisorio Medicao.
 * 
 *
 */
public class ResumoSupervisorioMedicaoVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer quantidadeSupervisorioMedicaoHoraria;

	private Integer quantidadeSupervisorioMedicaoDiaria;

	private Map<Long, Integer> mapaAnormalidade;

	/**
	 * Instantiates a new resumo supervisorio medicao vo.
	 * 
	 * @param quantidadeSupervisorioMedicaoHoraria
	 *            the quantidade supervisorio medicao horaria
	 * @param quantidadeSupervisorioMedicaoDiaria
	 *            the quantidade supervisorio medicao diaria
	 * @param mapaAnormalidade
	 *            the mapa anormalidade
	 */
	public ResumoSupervisorioMedicaoVO(Integer quantidadeSupervisorioMedicaoHoraria, Integer quantidadeSupervisorioMedicaoDiaria,
										Map<Long, Integer> mapaAnormalidade) {

		super();
		this.quantidadeSupervisorioMedicaoHoraria = quantidadeSupervisorioMedicaoHoraria;
		this.quantidadeSupervisorioMedicaoDiaria = quantidadeSupervisorioMedicaoDiaria;
		this.mapaAnormalidade = mapaAnormalidade;
	}

	/**
	 * Instantiates a new resumo supervisorio medicao vo.
	 */
	public ResumoSupervisorioMedicaoVO() {

	}

	/**
	 * @return the quantidadeSMH
	 */
	public Integer getQuantidadeSupervisorioMedicaoHoraria() {

		return quantidadeSupervisorioMedicaoHoraria;
	}

	/**
	 * @param quantidadeSMH
	 *            the quantidadeSMH to set
	 */
	public void setQuantidadeSupervisorioMedicaoHoraria(Integer quantidadeSupervisorioMedicaoHoraria) {

		this.quantidadeSupervisorioMedicaoHoraria = quantidadeSupervisorioMedicaoHoraria;
	}

	/**
	 * @return the quantidadeSMD
	 */
	public Integer getQuantidadeSupervisorioMedicaoDiaria() {

		return quantidadeSupervisorioMedicaoDiaria;
	}

	/**
	 * @param quantidadeSMD
	 *            the quantidadeSMD to set
	 */
	public void setQuantidadeSupervisorioMedicaoDiaria(Integer quantidadeSupervisorioMedicaoDiaria) {

		this.quantidadeSupervisorioMedicaoDiaria = quantidadeSupervisorioMedicaoDiaria;
	}

	/**
	 * @return the mapaAnormalidade
	 */
	public Map<Long, Integer> getMapaAnormalidade() {

		return mapaAnormalidade;
	}

	/**
	 * @param mapaAnormalidade
	 *            the mapaAnormalidade to set
	 */
	public void setMapaAnormalidade(Map<Long, Integer> mapaAnormalidade) {

		this.mapaAnormalidade = mapaAnormalidade;
	}

}
