/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/01/2013 17:32:04
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.comentario.SupervisorioMedicaoComentario;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.parametrosistema.ParametroSistema;

/**
 * The Interface ControladorSupervisorio.
 */
public interface ControladorSupervisorio extends ControladorNegocio {

	/** The bean id controlador supervisorio. */
	String BEAN_ID_CONTROLADOR_SUPERVISORIO = "controladorSupervisorio";

	/** The erro negocio tamanho campo. */
	String ERRO_NEGOCIO_TAMANHO_CAMPO = "ERRO_NEGOCIO_TAMANHO_CAMPO";

	/** The erro negocio ano mes referencia ciclo. */
	String ERRO_NEGOCIO_ANO_MES_REFERENCIA_CICLO = "ERRO_NEGOCIO_ANO_MES_REFERENCIA_CICLO";

	/**
	 * Tratar medicoes horarias.
	 *
	 * @param pontoConsumo            the ponto consumo
	 * @param dataInicialRealizacaoLeitura            the data inicial realizacao leitura
	 * @param dataFinalRealizacaoLeitura            the data final realizacao leitura
	 * @param logProcessamento            the log processamento
	 * @param mapaAnormalidadeSupervisorioMedicaoHoraria            the mapa anormalidade supervisorio medicao horaria
	 * @param resumoSupervisorioMedicaoVO            the resumo supervisorio medicao vo
	 * @param isBatch            the is batch
	 * @param medidor the medidor
	 * @return the map
	 * @throws GGASException             the GGAS exception
	 */
	Map<String, List<Object[]>> tratarMedicoesHorarias(PontoConsumo pontoConsumo, Date dataInicialRealizacaoLeitura,
					Date dataFinalRealizacaoLeitura, StringBuilder logProcessamento,
					Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoHoraria,
					ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO, Boolean isBatch, Medidor medidor) throws GGASException;

	/**
	 * Tratar medicoes diarias.
	 *
	 * @param pontoConsumo            the ponto consumo
	 * @param dataInicialRealizacaoLeitura            the data inicial realizacao leitura
	 * @param dataFinalRealizacaoLeitura            the data final realizacao leitura
	 * @param logProcessamento            the log processamento
	 * @param mapaAnormalidadeSMD            the mapa anormalidade smd
	 * @param resumoSupervisorioMedicaoVO            the resumo supervisorio medicao vo
	 * @param medidor the medidor
	 * @throws GGASException             the GGAS exception
	 */
	void tratarMedicoesDiarias(PontoConsumo pontoConsumo, Date dataInicialRealizacaoLeitura, Date dataFinalRealizacaoLeitura,
					StringBuilder logProcessamento, Map<String, List<Object[]>> mapaAnormalidadeSMD,
					ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO, Medidor medidor) throws GGASException;


	/**
	 * Consultar supervisorio medicao diaria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SupervisorioMedicaoDiaria> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, String ordenacao)
					throws NegocioException;

	/**
	 * Alterar supervisorio medicao diaria.
	 * 
	 * @param supervisorio
	 *            the supervisorio
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void alterarSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorio) throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar supervisorio medicao diaria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param isConsultaPorPontoConsumo
	 *            the is consulta por ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SupervisorioMedicaoVO> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, Boolean isConsultaPorPontoConsumo)
					throws NegocioException;

	/**
	 * Consultar supervisorio medicao diaria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SupervisorioMedicaoDiaria> consultarSupervisorioMedicaoDiaria(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException;

	/**
	 * Consultar supervisorio medicao horaria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SupervisorioMedicaoHoraria> consultarSupervisorioMedicaoHoraria(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException;

	/**
	 * Listar supervisorio medicao anormalidade.
	 * 
	 * @param ocorrencia
	 *            the ocorrencia
	 * @return the collection
	 */
	Collection<SupervisorioMedicaoAnormalidade> listarSupervisorioMedicaoAnormalidade(String ocorrencia);

	/**
	 * Consultar supervisorio medicao comentario.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SupervisorioMedicaoComentario> consultarSupervisorioMedicaoComentario(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException;

	/**
	 * Criar supervisorio medicao diaria.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSupervisorioMedicaoDiaria();

	/**
	 * Criar supervisorio medicao horaria.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSupervisorioMedicaoHoraria();

	/**
	 * Criar supervisorio medicao comentario.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSupervisorioMedicaoComentario();

	/**
	 * Obter supervisorio medicao diaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoDiaria obterSupervisorioMedicaoDiaria(long chavePrimaria) throws NegocioException;

	/**
	 * Obter supervisorio medicao diaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoDiaria obterSupervisorioMedicaoDiaria(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Obter supervisorio medicao horaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoHoraria obterSupervisorioMedicaoHoraria(long chavePrimaria) throws NegocioException;

	/**
	 * Obter supervisorio medicao horaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoHoraria obterSupervisorioMedicaoHoraria(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Validar alcada.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param supervisorioMedicaoHorariaAnterior
	 *            the supervisorio medicao horaria anterior
	 * @param parametroTabelaSuperMedicaoHoraria
	 *            the parametro tabela super medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAlcada(DadosAuditoria dadosAuditoria, SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAnterior, ParametroSistema parametroTabelaSuperMedicaoHoraria)
					throws NegocioException;

	/**
	 * Validar alcada.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param supervisorioMedicaoDiariaAnteior
	 *            the supervisorio medicao diaria anteior
	 * @param parametroTabelaSuperMedicaoDiaria
	 *            the parametro tabela super medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAlcada(DadosAuditoria dadosAuditoria, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAnteior, ParametroSistema parametroTabelaSuperMedicaoDiaria)
					throws NegocioException;

	/**
	 * Validar dados obrigatorios supervisorio medicao diaria.
	 * 
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param supervisorioMedicaoDiariaVO
	 *            the supervisorio medicao diaria vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosObrigatoriosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					SupervisorioMedicaoVO supervisorioMedicaoDiariaVO) throws NegocioException;

	/**
	 * Validar dados supervisorio medicao horaria.
	 * 
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosSupervisorioMedicaoHoraria(SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException;

	/**
	 * Validar dados supervisorio medicao diaria.
	 * 
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) throws NegocioException;

	/**
	 * Inserir supervisorio medicao diaria.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param parametroTabelaSuperMedicaoDiaria
	 *            the parametro tabela super medicao diaria
	 * @param supervisorioMedicaoComentarioNovo
	 *            the supervisorio medicao comentario novo
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param possuiComentariosAnteriores
	 *            the possui comentarios anteriores
	 * @param supervisorioMedicaoDiariaAntigo
	 *            the supervisorio medicao diaria antigo
	 * @param validaAlcada
	 *            the valida alcada
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserirSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					ParametroSistema parametroTabelaSuperMedicaoDiaria, SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, Boolean possuiComentariosAnteriores,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaAntigo, Boolean validaAlcada) throws NegocioException;

	/**
	 * Inserir supervisorio medicao horaria.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param parametroTabelaSuperMedicaoHoraria
	 *            the parametro tabela super medicao horaria
	 * @param supervisorioMedicaoComentarioNovo
	 *            the supervisorio medicao comentario novo
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param possuiComentariosAnteriores
	 *            the possui comentarios anteriores
	 * @param supervisorioMedicaoHorariaAntigo
	 *            the supervisorio medicao horaria antigo
	 * @param validaAlcada
	 *            the valida alcada
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserirSupervisorioMedicaoHoraria(DadosAuditoria dadosAuditoria, SupervisorioMedicaoHoraria supervisorioMedicaoHoraria,
					ParametroSistema parametroTabelaSuperMedicaoHoraria, SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, Boolean possuiComentariosAnteriores,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaAntigo, Boolean validaAlcada) throws NegocioException;

	/**
	 * Transferir supervisorio medicoes diaria.
	 * 
	 * @param listaMedicoesDiarias
	 *            the lista medicoes diarias
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	Boolean transferirSupervisorioMedicoesDiaria(Collection<SupervisorioMedicaoVO> listaMedicoesDiarias, Map<String, Object> filtro)
					throws NegocioException, ConcorrenciaException, FormatoInvalidoException;

	/**
	 * Atualizar registros remocao logica supervisorio medicao diaria.
	 * 
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param chavePrimariaSupervisorioMedicaoDiaria
	 *            the chave primaria supervisorio medicao diaria
	 * @param ativo
	 *            the ativo
	 * @param indicadorTipoMedicao
	 *            the indicador tipo medicao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarRegistrosRemocaoLogicaSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
					Long chavePrimariaSupervisorioMedicaoDiaria, Boolean ativo, String indicadorTipoMedicao) throws GGASException;

	/**
	 * Atualizar registros alterados supervisorio medicao diaria.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param parametroTabelaSuperMedicaoDiaria
	 *            the parametro tabela super medicao diaria
	 * @param chavePrimariaSupervisorioMedicaoDiaria
	 *            the chave primaria supervisorio medicao diaria
	 * @param ativo
	 *            the ativo
	 * @param indicadorTipoMedicao
	 *            the indicador tipo medicao
	 * @param supervisorioMedicaoDiariaNovo
	 *            the supervisorio medicao diaria novo
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoComentarioNovo
	 *            the supervisorio medicao comentario novo
	 * @param possuiComentariosAnteriores
	 *            the possui comentarios anteriores
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarRegistrosAlteradosSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, ParametroSistema parametroTabelaSuperMedicaoDiaria,
					Long chavePrimariaSupervisorioMedicaoDiaria, Boolean ativo, String indicadorTipoMedicao,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo, Boolean possuiComentariosAnteriores)
					throws GGASException;

	/**
	 * Atualizar supervisorio medicao horaria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarSupervisorioMedicaoHoraria(Map<String, Object> filtro, SupervisorioMedicaoDiaria supervisorioMedicaoDiaria)
					throws GGASException;

	/**
	 * Inserir supervisorio medicao horaria.
	 * 
	 * @param parametroTabelaSuperMedicaoHoraria
	 *            the parametro tabela super medicao horaria
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoHoraria inserirSupervisorioMedicaoHoraria(ParametroSistema parametroTabelaSuperMedicaoHoraria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria, DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoComentario supervisorioMedicaoComentario) throws NegocioException;

	/**
	 * Remover supervisorio medicao horaria.
	 * 
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerSupervisorioMedicaoHoraria(SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException;

	/**
	 * Atualizar supervisorio medicao horaria.
	 * 
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoHorariaNovo
	 *            the supervisorio medicao horaria novo
	 * @param supervisorioMedicaoComentario
	 *            the supervisorio medicao comentario
	 * @param parametroTabelaSuperMedicaoHoraria
	 *            the parametro tabela super medicao horaria
	 * @param supervisorioMedicaoHorariaVO
	 *            the supervisorio medicao horaria vo
	 * @param supervisorioMedicaoHoraria
	 *            the supervisorio medicao horaria
	 * @return the supervisorio medicao horaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoHoraria atualizarSupervisorioMedicaoHoraria(
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario, DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoHoraria supervisorioMedicaoHorariaNovo, SupervisorioMedicaoComentario supervisorioMedicaoComentario,
					ParametroSistema parametroTabelaSuperMedicaoHoraria, SupervisorioMedicaoVO supervisorioMedicaoHorariaVO,
					SupervisorioMedicaoHoraria supervisorioMedicaoHoraria) throws NegocioException;

	/**
	 * Atualizar registros alcada supervisorio medicao diaria.
	 * 
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorio medicao diaria
	 * @param parametroTabelaSuperMedicaoDiaria
	 *            the parametro tabela super medicao diaria
	 * @param supervisorioMedicaoDiariaNovo
	 *            the supervisorio medicao diaria novo
	 * @param listaSupervisorioMedicaoComentario
	 *            the lista supervisorio medicao comentario
	 * @param supervisorioMedicaoComentarioNovo
	 *            the supervisorio medicao comentario novo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarRegistrosAlcadaSupervisorioMedicaoDiaria(DadosAuditoria dadosAuditoria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, ParametroSistema parametroTabelaSuperMedicaoDiaria,
					SupervisorioMedicaoDiaria supervisorioMedicaoDiariaNovo,
					Collection<SupervisorioMedicaoComentario> listaSupervisorioMedicaoComentario,
					SupervisorioMedicaoComentario supervisorioMedicaoComentarioNovo) throws GGASException;

	/**
	 * Validar ano mes referencia ciclo.
	 *
	 * @param anoMesReferencia            the ano mes referencia
	 * @param ciclo            the ciclo
	 * @param pontoConsumo            the ponto consumo
	 * @param dataRealizacaoLeitura            the data realizacao leitura
	 * @param paramStatusAutorizado            the param status autorizado
	 * @param ultimoHistoricoConsumo            the ultimo historico consumo
	 * @param periodicidade            the periodicidade
	 * @return the boolean
	 * @throws GGASException             the GGAS exception
	 */
	Boolean validarAnoMesReferenciaCiclo(Integer anoMesReferencia, Integer ciclo, PontoConsumo pontoConsumo, Date dataRealizacaoLeitura,
					String paramStatusAutorizado, HistoricoConsumo ultimoHistoricoConsumo, Periodicidade periodicidade)
					throws GGASException;

	/**
	 * Obter supervisorio medicao anormalidade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the supervisorio medicao anormalidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SupervisorioMedicaoAnormalidade obterSupervisorioMedicaoAnormalidade(long chavePrimaria) throws NegocioException;

	/**
	 * Preencher mapa com lista referencia leitura.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param contratoPontoConsumo 
	 * 			  contratoPontoConsumo de contrato faturável
	 * @param dataRealizacaoLeitura
	 *            the data realizacao leitura
	 * @param mapaComListaReferenciaLeitura
	 *            the mapa com lista referencia leitura
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Map<String, Object> preencherMapaComListaReferenciaLeitura(PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo,
					Date dataRealizacaoLeitura, Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura) throws GGASException;

	/**
	 * Obter ano mes referencia ciclo.
	 *
	 * @param mapaComListaReferenciaLeitura            the mapa com lista referencia leitura
	 * @param dataRealizacaoLeitura            the data realizacao leitura
	 * @param pontoConsumo            the ponto consumo
	 * @param periodicidade            the periodicidade
	 * @param calcularAnoMesReferenciaCiclo            the calcular ano mes referencia ciclo
	 * @return the map
	 * @throws GGASException             the GGAS exception
	 */
	Map<String, Object> obterAnoMesReferenciaCiclo(Map<Long, List<Map<String, Object>>> mapaComListaReferenciaLeitura,
					Date dataRealizacaoLeitura, PontoConsumo pontoConsumo, Periodicidade periodicidade,
					Boolean calcularAnoMesReferenciaCiclo) throws GGASException;

	/**
	 * Desfazer supervisorio.
	 * 
	 * @param listaSupervisorioMedicaoVO
	 *            the lista supervisorio medicao vo
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void desfazerSupervisorio(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO) throws ConcorrenciaException, NegocioException;

	/**
	 * Desfazer consolidar supervisorio.
	 * 
	 * @param listaSupervisorioMedicaoVO
	 *            the lista supervisorio medicao vo
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void desfazerConsolidarSupervisorio(Collection<SupervisorioMedicaoVO> listaSupervisorioMedicaoVO) throws ConcorrenciaException,
					NegocioException;

	/**
	 * Consulta supervisorio medicao diaria por medidor.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 * @throws NumberFormatException the number format exception
	 */
	Collection<SupervisorioMedicaoVO> consultarSupervisorioMedicaoDiariaPorMedidor(Map<String, Object> filtro) throws GGASException;

	/**
	 * Validar composicao virtual por cod supervisorio.
	 *
	 * @param arrayEnderecoRemoto the array endereco remoto
	 * @param numeroCiclo the numero ciclo
	 * @param anoMes the ano mes
	 * @return the list
	 */
	List<Object> validarComposicaoVirtualPorCodSupervisorio(String[] arrayEnderecoRemoto, Integer numeroCiclo, Integer anoMes);

	/**
	 * Atualiza as anormalidades de uma lista de entidades de SupervisorioMedicaoHoraria.
	 *
	 * @param supervisorioMedicaoAnormalidade the supervisorio medicao anormalidade
	 * @param listaSupervisorioMedicaoHoraria the lista supervisorio medicao horaria
	 */
	void atualizarAnormalidadeDoSupervisorioMedicaoHoraria(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade,
					List<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria);

	/**
	 * Agrupa uma lista de SupervisorioMedicaoHoraria por SupervisorioMedicaoAnormalidade.
	 *
	 * @param listaSupervisorioMedicaoHoraria the lista supervisorio medicao horaria
	 * @return Map - lista de SupervisorioMedicaoHoraria por SupervisorioMedicaoAnormalidade
	 */
	Map<SupervisorioMedicaoAnormalidade, List<SupervisorioMedicaoHoraria>> agruparSupervisorioMedicaoHorariaPorAnormalidade(
					Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria);

	/**
	 * Consolida as anormalidades de um coleção de entidades de SupervisorioMedicaoHoraria mapeadas
	 * por SupervisorioMedicaoAnormalidade.
	 *
	 * @param mapaAnormalidade the mapa anormalidade
	 */
	void consolidarAnormalidadesDoSupervisorioMedicaoHoraria(
					Map<SupervisorioMedicaoAnormalidade, List<SupervisorioMedicaoHoraria>> mapaAnormalidade);

	/**
	 * Realiza a contagem da quantidade de entidades de SupervisorioMedicaoHoraria,
	 * filtrando de acordo com as entradas do {@code filtro}.
	 *
	 * @param filtro            podendo ser: habilitado, indicad  orConsolidada, indicadorTipoMedicao,
	 *            dataInicialRealizacaoLeitura e dataFinalRealizacaoLeitura, codigoPontoConsumoSupervisorio
	 * @return Long - quantidade de entidades SupervisorioMedicaoHoraria
	 * @throws NegocioException             the negocio exception
	 */
	Long contarSupervisorioMedicaoHoraria(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Realizar a contagem de pontos de consumo na atividade do cronograma
	 * referente ao grupo de fatura no ano, mês e ciclo e qual o tipo de leitura
	 * 
	 * @return Long 
	 * @param chaveSituacaoLeitura 
	 * @param grupoFaturamento the grupo Faturamento
	 * @param param 
	 * @param anoMesFaturamento the ano Mes Faturamento
	 * @param ciclo the ciclo
	 * @param leituraTipo the leitura Tipo
	 */
	Long consultarQuantidadePontosConsumoTotaisNaAtividade(Long grupoFaturamento, Integer anoMesFaturamento,
			Integer ciclo, Long leituraTipo);
	
	/**
	 * Consulta a quantidade de pontos de consumos que estão "presos" em uma determinada etapa
	 * do faturamaento
	 * @param chaveAtividadeCronograma chave da situação da leitura
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param anoMesFaturamento ano mês faturamento
	 * @param ciclo número do ciclo
	 * @param leituraTipo Tipo Leitura 
	 * @return retorna a quantidade de pontos de consumo que estão em uma determinada situação
	 */	
	Long consultarQuantidadePontosConsumoNaAtividade(Long grupoFaturamento,
			Integer anoMesFaturamento, Integer ciclo, Long leituraTipo);

	/**
	 * Transfere medicao por dia
	 * 
	 * @param filtro - {@link Map}
	 * @throws NegocioException - {@link NegocioException}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws FormatoInvalidoException - {@link FormatoInvalidoException}
	 */
	void transferirSupervisorioMedicoesDiariaPorDia(Map<String, Object> filtro)
			throws NegocioException, ConcorrenciaException, FormatoInvalidoException;

	/**
	 * Confere se a quantidade de dias de medição daquele ponto de consumo na referencia atual está como esperado
	 * @param superMedicaoVO - {@link SupervisorioMedicaoVO}
	 * @return Boolean - {@link Boolean}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Boolean conferirQuantidadeMedicoesSupervisorio(SupervisorioMedicaoVO superMedicaoVO) throws NegocioException;

	/**
	 * Confere se a quantidade de dias parcial de medição daquele ponto de consumo na referencia atual está como esperado
	 * @param dataInicio - {@link Date}
	 * @param dataFim - {@link Date}
	 * @param medicao - {@link SupervisorioMedicaoVO}
	 * @return Boolean - {@link Boolean}
	 */
	Boolean conferirQuantidadeMedicoesIntegracaoParcial(Date dataInicio, Date dataFim, SupervisorioMedicaoVO medicao);
	
}
