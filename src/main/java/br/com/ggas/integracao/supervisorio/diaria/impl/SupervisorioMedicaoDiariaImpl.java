/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:09:20
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.diaria.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.util.Util;

/**
 * 
 * Classe responsável por representar uma medição diária realizada no sistema supervisório.
 *
 */
public class SupervisorioMedicaoDiariaImpl extends EntidadeNegocioImpl implements SupervisorioMedicaoDiaria {

	private static final long serialVersionUID = -7983964813569591039L;

	private SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade;

	private Integer dataReferencia;

	private Integer numeroCiclo;

	private String codigoPontoConsumoSupervisorio;

	private Date dataRealizacaoLeitura;

	private BigDecimal leituraSemCorrecaoFatorPTZ;

	private BigDecimal leituraComCorrecaoFatorPTZ;

	private BigDecimal consumoSemCorrecaoFatorPTZ;

	private BigDecimal consumoComCorrecaoFatorPTZ;

	private BigDecimal pressao;

	private BigDecimal temperatura;

	private BigDecimal fatorPTZ;

	private BigDecimal fatorZ;

	private Boolean indicadorIntegrado;

	private Boolean indicadorProcessado;

	private Date dataRegistroLeitura;

	private Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria = new HashSet<SupervisorioMedicaoHoraria>();

	private Usuario ultimoUsuarioAlteracao;

	private EntidadeConteudo status;

	private Boolean indicadorConsolidada;

	private Boolean indicadorMedidor;

	/**
	 * @return the supervisorioMedicaoAnormalidade
	 */
	@Override
	public SupervisorioMedicaoAnormalidade getSupervisorioMedicaoAnormalidade() {

		return supervisorioMedicaoAnormalidade;
	}

	/**
	 * @param supervisorioMedicaoAnormalidade
	 *            the supervisorioMedicaoAnormalidade to set
	 */
	@Override
	public void setSupervisorioMedicaoAnormalidade(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade) {

		this.supervisorioMedicaoAnormalidade = supervisorioMedicaoAnormalidade;
	}

	/**
	 * @return the dataReferencia
	 */
	@Override
	public Integer getDataReferencia() {

		return dataReferencia;
	}

	@Override
	public String getDataReferenciaFormatado() {

		return Util.formatarAnoMes(dataReferencia);
	}

	/**
	 * @param dataReferencia
	 *            the dataReferencia to set
	 */
	@Override
	public void setDataReferencia(Integer dataReferencia) {

		this.dataReferencia = dataReferencia;
	}

	/**
	 * @return the numeroCiclo
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	@Override
	public String getCodigoPontoConsumoSupervisorio() {

		return codigoPontoConsumoSupervisorio;
	}

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	@Override
	public void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio) {

		this.codigoPontoConsumoSupervisorio = codigoPontoConsumoSupervisorio;
	}

	/**
	 * @return the dataRealizacaoLeitura
	 */
	@Override
	public Date getDataRealizacaoLeitura() {
		Date data = null;
		if (this.dataRealizacaoLeitura != null) {
			data = (Date) dataRealizacaoLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataRealizacaoLeitura
	 *            the dataRealizacaoLeitura to set
	 */
	@Override
	public void setDataRealizacaoLeitura(Date dataRealizacaoLeitura) {
		if (dataRealizacaoLeitura != null) {
			this.dataRealizacaoLeitura = (Date) dataRealizacaoLeitura.clone();
		} else {
			this.dataRealizacaoLeitura = null;
		}
	}

	/**
	 * @return the leituraSemCorrecaoFatorPTZ
	 */
	@Override
	public BigDecimal getLeituraSemCorrecaoFatorPTZ() {

		return leituraSemCorrecaoFatorPTZ;
	}

	/**
	 * @param leituraSemCorrecaoFatorPTZ
	 *            the leituraSemCorrecaoFatorPTZ to set
	 */
	@Override
	public void setLeituraSemCorrecaoFatorPTZ(BigDecimal leituraSemCorrecaoFatorPTZ) {

		this.leituraSemCorrecaoFatorPTZ = leituraSemCorrecaoFatorPTZ;
	}

	/**
	 * @return the leituraComCorrecaoFatorPTZ
	 */
	@Override
	public BigDecimal getLeituraComCorrecaoFatorPTZ() {

		return leituraComCorrecaoFatorPTZ;
	}

	/**
	 * @param leituraComCorrecaoFatorPTZ
	 *            the leituraComCorrecaoFatorPTZ to set
	 */
	@Override
	public void setLeituraComCorrecaoFatorPTZ(BigDecimal leituraComCorrecaoFatorPTZ) {

		this.leituraComCorrecaoFatorPTZ = leituraComCorrecaoFatorPTZ;
	}

	/**
	 * @return the consumoSemCorrecaoFatorPTZ
	 */
	@Override
	public BigDecimal getConsumoSemCorrecaoFatorPTZ() {

		return consumoSemCorrecaoFatorPTZ;
	}

	/**
	 * @param consumoSemCorrecaoFatorPTZ
	 *            the consumoSemCorrecaoFatorPTZ to set
	 */
	@Override
	public void setConsumoSemCorrecaoFatorPTZ(BigDecimal consumoSemCorrecaoFatorPTZ) {

		this.consumoSemCorrecaoFatorPTZ = consumoSemCorrecaoFatorPTZ;
	}

	/**
	 * @return the consumoComCorrecaoFatorPTZ
	 */
	@Override
	public BigDecimal getConsumoComCorrecaoFatorPTZ() {

		return consumoComCorrecaoFatorPTZ;
	}

	/**
	 * @param consumoComCorrecaoFatorPTZ
	 *            the consumoComCorrecaoFatorPTZ to set
	 */
	@Override
	public void setConsumoComCorrecaoFatorPTZ(BigDecimal consumoComCorrecaoFatorPTZ) {

		this.consumoComCorrecaoFatorPTZ = consumoComCorrecaoFatorPTZ;
	}

	/**
	 * @return the pressao
	 */
	@Override
	public BigDecimal getPressao() {

		return pressao;
	}

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	@Override
	public void setPressao(BigDecimal pressao) {

		this.pressao = pressao;
	}

	/**
	 * @return the temperatura
	 */
	@Override
	public BigDecimal getTemperatura() {

		return temperatura;
	}

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	@Override
	public void setTemperatura(BigDecimal temperatura) {

		this.temperatura = temperatura;
	}

	/**
	 * @return the fatorPTZ
	 */
	@Override
	public BigDecimal getFatorPTZ() {

		return fatorPTZ;
	}

	/**
	 * @param fatorPTZ
	 *            the fatorPTZ to set
	 */
	@Override
	public void setFatorPTZ(BigDecimal fatorPTZ) {

		this.fatorPTZ = fatorPTZ;
	}

	/**
	 * @return the fatorZ
	 */
	@Override
	public BigDecimal getFatorZ() {

		return fatorZ;
	}

	/**
	 * @param fatorZ
	 *            the fatorZ to set
	 */
	@Override
	public void setFatorZ(BigDecimal fatorZ) {

		this.fatorZ = fatorZ;
	}

	/**
	 * @return the indicadorIntegrado
	 */
	@Override
	public Boolean getIndicadorIntegrado() {

		return indicadorIntegrado;
	}

	/**
	 * @param analizado
	 *            the indicadorIntegrado to set
	 */
	@Override
	public void setIndicadorIntegrado(Boolean indicadorIntegrado) {

		this.indicadorIntegrado = indicadorIntegrado;
	}

	/**
	 * @return the indicadorProcessado
	 */
	@Override
	public Boolean getIndicadorProcessado() {

		return indicadorProcessado;
	}

	/**
	 * @param processado
	 *            the indicadorProcessado to set
	 */
	@Override
	public void setIndicadorProcessado(Boolean indicadorProcessado) {

		this.indicadorProcessado = indicadorProcessado;
	}

	/**
	 * @return the dataRegistroLeitura
	 */
	@Override
	public Date getDataRegistroLeitura() {
		Date data = null;
		if (this.dataRegistroLeitura != null) {
			data = (Date) dataRegistroLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataRegistroLeitura
	 *            the dataRegistroLeitura to set
	 */
	@Override
	public void setDataRegistroLeitura(Date dataRegistroLeitura) {
		if (dataRegistroLeitura != null) {
			this.dataRegistroLeitura = (Date) dataRegistroLeitura.clone();
		} else {
			this.dataRegistroLeitura = null;
		}
	}

	/**
	 * @return the listaSupervisorioMedicaoHoraria
	 */
	@Override
	public Collection<SupervisorioMedicaoHoraria> getListaSupervisorioMedicaoHoraria() {

		return listaSupervisorioMedicaoHoraria;
	}

	/**
	 * @param listaSupervisorioMedicaoHoraria
	 *            the listaSupervisorioMedicaoHoraria to set
	 */
	@Override
	public void setListaSupervisorioMedicaoHoraria(Collection<SupervisorioMedicaoHoraria> listaSupervisorioMedicaoHoraria) {

		this.listaSupervisorioMedicaoHoraria = listaSupervisorioMedicaoHoraria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public EntidadeConteudo getStatus() {

		return status;
	}

	@Override
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	@Override
	public Usuario getUltimoUsuarioAlteracao() {

		return ultimoUsuarioAlteracao;
	}

	@Override
	public void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao) {

		this.ultimoUsuarioAlteracao = ultimoUsuarioAlteracao;
	}

	/**
	 * @return the indicadorConsolidado
	 */
	@Override
	public Boolean getIndicadorConsolidada() {

		return indicadorConsolidada;
	}

	/**
	 * @param indicadorConsolidado
	 *            the indicadorConsolidado to set
	 */
	@Override
	public void setIndicadorConsolidada(Boolean indicadorConsolidada) {

		this.indicadorConsolidada = indicadorConsolidada;
	}

	@Override
	public Boolean getIndicadorMedidor() {

		return indicadorMedidor;
	}

	@Override
	public void setIndicadorMedidor(Boolean indicadorMedidor) {

		this.indicadorMedidor = indicadorMedidor;
	}

}
