/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:10:00
 @author ccavalcanti
 */

package br.com.ggas.integracao.supervisorio.horaria.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.supervisorio.anormalidade.SupervisorioMedicaoAnormalidade;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
/**
 * 
 * Classe responsável por representar uma medição horária realizada no sistema supervisório.
 *
 */
public class SupervisorioMedicaoHorariaImpl extends EntidadeNegocioImpl implements SupervisorioMedicaoHoraria {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9072205159838270331L;

	private SupervisorioMedicaoDiaria supervisorioMedicaoDiaria;

	private SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade;

	private String codigoPontoConsumoSupervisorio;

	private String indicadorTipoMedicao;

	private Date dataRealizacaoLeitura;

	private BigDecimal leituraSemCorrecaoFatorPTZ;

	private BigDecimal leituraComCorrecaoFatorPTZ;

	private BigDecimal pressao;

	private BigDecimal temperatura;

	private Boolean indicadorConsolidada;

	private Date dataRegistroLeitura;

	private Usuario ultimoUsuarioAlteracao;

	private EntidadeConteudo status;
	
	private String indicadorTipoInclusaoMedicao;

	/**
	 * @return the supervisorioMedicaoDiaria
	 */
	@Override
	public SupervisorioMedicaoDiaria getSupervisorioMedicaoDiaria() {

		return supervisorioMedicaoDiaria;
	}

	/**
	 * @param supervisorioMedicaoDiaria
	 *            the supervisorioMedicaoDiaria to set
	 */
	@Override
	public void setSupervisorioMedicaoDiaria(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria) {

		this.supervisorioMedicaoDiaria = supervisorioMedicaoDiaria;
	}

	/**
	 * @return the supervisorioMedicaoAnormalidade
	 */
	@Override
	public SupervisorioMedicaoAnormalidade getSupervisorioMedicaoAnormalidade() {

		return supervisorioMedicaoAnormalidade;
	}

	/**
	 * @param supervisorioMedicaoAnormalidade
	 *            the supervisorioMedicaoAnormalidade to set
	 */
	@Override
	public void setSupervisorioMedicaoAnormalidade(SupervisorioMedicaoAnormalidade supervisorioMedicaoAnormalidade) {

		this.supervisorioMedicaoAnormalidade = supervisorioMedicaoAnormalidade;
	}

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	@Override
	public String getCodigoPontoConsumoSupervisorio() {

		return codigoPontoConsumoSupervisorio;
	}

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	@Override
	public void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio) {

		this.codigoPontoConsumoSupervisorio = codigoPontoConsumoSupervisorio;
	}

	/**
	 * @return the indicadorTipoMedicao
	 */
	@Override
	public String getIndicadorTipoMedicao() {

		return indicadorTipoMedicao;
	}

	/**
	 * @param indicadorTipoMedicao
	 *            the indicadorTipoMedicao to set
	 */
	@Override
	public void setIndicadorTipoMedicao(String indicadorTipoMedicao) {

		this.indicadorTipoMedicao = indicadorTipoMedicao;
	}

	/**
	 * @return the dataRealizacaoLeitura
	 */
	@Override
	public Date getDataRealizacaoLeitura() {
		Date data = null;
		if (this.dataRealizacaoLeitura != null) {
			data = (Date) dataRealizacaoLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataRealizacaoLeitura
	 *            the dataRealizacaoLeitura to set
	 */
	@Override
	public void setDataRealizacaoLeitura(Date dataRealizacaoLeitura) {
		if (dataRealizacaoLeitura != null) {
			this.dataRealizacaoLeitura = (Date) dataRealizacaoLeitura.clone();
		} else {
			this.dataRealizacaoLeitura = null;
		}
	}

	/**
	 * @return the leituraSemCorrecaoFatorPTZ
	 */
	@Override
	public BigDecimal getLeituraSemCorrecaoFatorPTZ() {

		return leituraSemCorrecaoFatorPTZ;
	}

	/**
	 * @param leituraSemCorrecaoFatorPTZ
	 *            the leituraSemCorrecaoFatorPTZ to set
	 */
	@Override
	public void setLeituraSemCorrecaoFatorPTZ(BigDecimal leituraSemCorrecaoFatorPTZ) {

		this.leituraSemCorrecaoFatorPTZ = leituraSemCorrecaoFatorPTZ;
	}

	/**
	 * @return the leituraComCorrecaoFatorPTZ
	 */
	@Override
	public BigDecimal getLeituraComCorrecaoFatorPTZ() {

		return leituraComCorrecaoFatorPTZ;
	}

	/**
	 * @param leituraComCorrecaoFatorPTZ
	 *            the leituraComCorrecaoFatorPTZ to set
	 */
	@Override
	public void setLeituraComCorrecaoFatorPTZ(BigDecimal leituraComCorrecaoFatorPTZ) {

		this.leituraComCorrecaoFatorPTZ = leituraComCorrecaoFatorPTZ;
	}

	/**
	 * @return the pressao
	 */
	@Override
	public BigDecimal getPressao() {

		return pressao;
	}

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	@Override
	public void setPressao(BigDecimal pressao) {

		this.pressao = pressao;
	}

	/**
	 * @return the temperatura
	 */
	@Override
	public BigDecimal getTemperatura() {

		return temperatura;
	}

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	@Override
	public void setTemperatura(BigDecimal temperatura) {

		this.temperatura = temperatura;
	}

	/**
	 * @return the indicadorConsolidada
	 */
	@Override
	public Boolean getIndicadorConsolidada() {

		return indicadorConsolidada;
	}

	/**
	 * @param indicadorConsolidada
	 *            the indicadorConsolidada to set
	 */
	@Override
	public void setIndicadorConsolidada(Boolean indicadorConsolidada) {

		this.indicadorConsolidada = indicadorConsolidada;
	}

	/**
	 * @return the dataRegistroLeitura
	 */
	@Override
	public Date getDataRegistroLeitura() {
		Date data = null;
		if (this.dataRegistroLeitura != null) {
			data = (Date) dataRegistroLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataRegistroLeitura
	 *            the dataRegistroLeitura to set
	 */
	@Override
	public void setDataRegistroLeitura(Date dataRegistroLeitura) {
		if (dataRegistroLeitura != null) {
			this.dataRegistroLeitura = (Date) dataRegistroLeitura.clone();
		} else {
			this.dataRegistroLeitura = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public EntidadeConteudo getStatus() {

		return status;
	}

	@Override
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	@Override
	public Usuario getUltimoUsuarioAlteracao() {

		return ultimoUsuarioAlteracao;
	}

	@Override
	public void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao) {

		this.ultimoUsuarioAlteracao = ultimoUsuarioAlteracao;
	}

	@Override
	public String getIndicadorTipoInclusaoMedicao() {
		return indicadorTipoInclusaoMedicao;
	}

	@Override
	public void setIndicadorTipoInclusaoMedicao(String indicadorTipoInclusaoMedicao) {
		this.indicadorTipoInclusaoMedicao = indicadorTipoInclusaoMedicao;
	}
	
	

}
