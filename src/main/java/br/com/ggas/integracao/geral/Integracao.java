/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/06/2013 17:09:48
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface Integracao.
 */
public interface Integracao extends EntidadeNegocio {

	/**
	 * Gets the operacao.
	 *
	 * @return the operacao
	 */
	String getOperacao();

	/**
	 * Sets the operacao.
	 *
	 * @param operacao the new operacao
	 */
	void setOperacao(String operacao);

	/**
	 * Gets the situacao.
	 *
	 * @return the situacao
	 */
	String getSituacao();

	/**
	 * Sets the situacao.
	 *
	 * @param situacao the new situacao
	 */
	void setSituacao(String situacao);

	/**
	 * Gets the descricao erro.
	 *
	 * @return the descricao erro
	 */
	String getDescricaoErro();

	/**
	 * Sets the descricao erro.
	 *
	 * @param descricaoErro the new descricao erro
	 */
	void setDescricaoErro(String descricaoErro);

	/**
	 * Gets the sistema origem.
	 *
	 * @return the sistema origem
	 */
	String getSistemaOrigem();

	/**
	 * Sets the sistema origem.
	 *
	 * @param sistemaOrigem the new sistema origem
	 */
	void setSistemaOrigem(String sistemaOrigem);

	/**
	 * Gets the sistema destino.
	 *
	 * @return the sistema destino
	 */
	String getSistemaDestino();

	/**
	 * Sets the sistema destino.
	 *
	 * @param sistemaDestino the new sistema destino
	 */
	void setSistemaDestino(String sistemaDestino);

	/**
	 * Gets the usuario.
	 *
	 * @return the usuario
	 */
	String getUsuario();

	/**
	 * Sets the usuario.
	 *
	 * @param usuario the new usuario
	 */
	void setUsuario(String usuario);
}
