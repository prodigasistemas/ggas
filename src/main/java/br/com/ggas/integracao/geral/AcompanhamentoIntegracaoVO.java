/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 20/02/2013 09:24:02
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import java.io.Serializable;
import java.lang.reflect.Type;


/**
 * Classe Acompanhamento Integracao.
 * 
 *
 */
public class AcompanhamentoIntegracaoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8261029474505917712L;

	private String nomeColuna;

	private String nomePropriedade;

	private Object valorColuna;

	private Object valorColuna2;

	private Type tipoColuna;

	private Class<?> classe;

	/**
	 * @return the nomeColuna
	 */
	public String getNomeColuna() {

		return nomeColuna;
	}

	/**
	 * @param nomeColuna
	 *            the nomeColuna to set
	 */
	public void setNomeColuna(String nomeColuna) {

		this.nomeColuna = nomeColuna;
	}

	/**
	 * @return the valorColuna
	 */
	public Object getValorColuna() {

		return valorColuna;
	}

	/**
	 * @param valorColuna
	 *            the valorColuna to set
	 */
	public void setValorColuna(Object valorColuna) {

		this.valorColuna = valorColuna;
	}

	/**
	 * @return the tipoColuna
	 */
	public Type getTipoColuna() {

		return tipoColuna;
	}

	/**
	 * @param tipoColuna
	 *            the tipoColuna to set
	 */
	public void setTipoColuna(Type tipoColuna) {

		this.tipoColuna = tipoColuna;
	}

	/**
	 * @return the nomePropriedade
	 */
	public String getNomePropriedade() {

		return nomePropriedade;
	}

	/**
	 * @param nomePropriedade
	 *            the nomePropriedade to set
	 */
	public void setNomePropriedade(String nomePropriedade) {

		this.nomePropriedade = nomePropriedade;
	}

	/**
	 * @return the classe
	 */
	public Class<?> getClasse() {

		return classe;
	}

	/**
	 * @param classe
	 *            the classe to set
	 */
	public void setClasse(Class<?> classe) {

		this.classe = classe;
	}

	/**
	 * @return the valorColuna2
	 */
	public Object getValorColuna2() {

		return valorColuna2;
	}

	/**
	 * @param valorColuna2
	 *            the valorColuna2 to set
	 */
	public void setValorColuna2(Object valorColuna2) {

		this.valorColuna2 = valorColuna2;
	}
}
