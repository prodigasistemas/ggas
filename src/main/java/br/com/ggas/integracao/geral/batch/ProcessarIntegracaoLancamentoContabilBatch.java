/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 12/09/2013 14:35:45
 @author wcosta
 */

package br.com.ggas.integracao.geral.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.ResumoIntegracaoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.LogProcessosUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe Processar Integracao Lancamento Contabil Batch.
 *
 *
 */
@Component
public class ProcessarIntegracaoLancamentoContabilBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();
		Map<String, Object[]> mapaInconsistenciasLancamento = new LinkedHashMap<String, Object[]>();
		ResumoIntegracaoVO resumoIntegracaoVOLancamento = new ResumoIntegracaoVO();

		try {

			Long valorConstanteIntegracaoLancamentosContabeis = 
							getControladorConstanteSistema()
											.obterValorConstanteIntegracaoLancamentosContabeis();

			List<String> listaSistemasIntegrantes = 
							gerarListaSistemasIntegrantesPorFuncao(
											valorConstanteIntegracaoLancamentosContabeis);

			processo.setEmailResponsavel(
							getRemetenteEmails(processo, 
											valorConstanteIntegracaoLancamentosContabeis));

			if(CollectionUtils.isNotEmpty(listaSistemasIntegrantes)) {

				Collection<LancamentoContabilSintetico> listaLancamentos = getControladorContabil()
								.consultarLancamentoContabilSinteticoHabilitadoNaoIntegrado();

				// carrega as contas de contábeis de crédito e débito
				String detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico = 
								getControladorContabil().popularContaCreditoDebitoLancamentoSintetico(
												listaLancamentos);

				if(CollectionUtils.isNotEmpty(listaLancamentos)) {

					logProcessamento.append(LogProcessosUtil
									.gerarInicioValidacaoLancamentosContabeisSinteticos());
					logProcessamento.append(Constantes.PULA_LINHA);

					for (LancamentoContabilSintetico lancamentoContabilSintetico : listaLancamentos) {

						// condição para retirar os lancamentos contabeis de volume
						if(!lancamentoContabilSintetico.getEventoComercial().isDescricaoValorM3()) {
							if(getControladorIntegracao().validarProcessamentoLancamento(
											lancamentoContabilSintetico, mapaInconsistenciasLancamento)) {
								resumoIntegracaoVOLancamento.incrementarQuantidadeRegistrosIncluidos();
							} else {
								resumoIntegracaoVOLancamento.incrementarQuantidadeRegistrosInconsistentes();
							}
						} else {
							// lancamento contabil de volume não irá para integração 
							// com o ERP e será marcador como integrado
							lancamentoContabilSintetico.setIndicadorIntegrado(Boolean.TRUE);
							getControladorContabil().atualizar(lancamentoContabilSintetico);
						}

					}
					
					resumoIntegracaoVOLancamento.updateQuantidadeRegistrosProcessados();
					resumoIntegracaoVOLancamento.setMapaInconsistencias(mapaInconsistenciasLancamento);

					if(resumoIntegracaoVOLancamento.getQuantidadeRegistrosProcessados() > 0 
									|| !mapaInconsistenciasLancamento.isEmpty()) {

						getControladorIntegracao().preencherLogMapaInconsistencias(
										logProcessamento, resumoIntegracaoVOLancamento, true, 
										detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico);
					}
					
					logProcessamento.append(LogProcessosUtil
									.gerarFimValidacaoLancamentosContabeisSinteticos());
					
				} else {
					logProcessamento.append(LogProcessosUtil.gerarLogErroSemDados());
				}

			} else {
				logProcessamento.append(LogProcessosUtil
								.gerarLogErroIntegracaoLancamentosContabeisNaoLocalizado());
			}

		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}
		return logProcessamento.toString();
	}

	private List<String> gerarListaSistemasIntegrantesPorFuncao(Long valorConstanteIntegracaoLancamentosContabeis) {

		return getControladorIntegracao()
						.gerarListaSistemasIntegrantesPorFuncao(
										valorConstanteIntegracaoLancamentosContabeis);
	}

	private String getRemetenteEmails(Processo processo, Long valorConstanteIntegracaoLancamentosContabeis) {

		return Util.concatenaEmails(
						processo.getEmailResponsavel(),
						getControladorIntegracao().buscarEmailDestinatarioIntegracao(
										valorConstanteIntegracaoLancamentosContabeis));
	}

	private ControladorContabil getControladorContabil() {

		return ServiceLocator.getInstancia().getControladorContabil();
	}

	private ControladorConstanteSistema getControladorConstanteSistema() {

		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	private ControladorIntegracao getControladorIntegracao() {

		return ServiceLocator.getInstancia().getControladorIntegracao();
	}

}
