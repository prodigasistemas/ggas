/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/05/2013 17:14:25
 @author vpessoa
 */

package br.com.ggas.integracao.geral;

import br.com.ggas.geral.EntidadeConteudo;

/**
 * The Class IntegracaoSistemaFuncaoVO.
 */
public class IntegracaoSistemaFuncaoVO {

	/** The indicador envia email erro. */
	private Long indicadorEnviaEmailErro;

	/** The destinatarios email. */
	private String destinatariosEmail;

	/** The integracao sistema. */
	private IntegracaoSistema integracaoSistema;

	/** The integracao funcao. */
	private IntegracaoFuncao integracaoFuncao;

	/** The fluxo integracao. */
	private EntidadeConteudo fluxoIntegracao;

	/** The pendencia. */
	private String pendencia;

	/** The chave primaria. */
	private Long chavePrimaria;

	/** The habilitado. */
	private boolean habilitado;

	/**
	 * Checks if is habilitado.
	 *
	 * @return true, if is habilitado
	 */
	public boolean isHabilitado() {

		return habilitado;
	}

	/**
	 * Sets the habilitado.
	 *
	 * @param habilitado the new habilitado
	 */
	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * Gets the chave primaria.
	 *
	 * @return the chave primaria
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * Sets the chave primaria.
	 *
	 * @param chavePrimaria the new chave primaria
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * Gets the indicador envia email erro.
	 *
	 * @return the indicador envia email erro
	 */
	public Long getIndicadorEnviaEmailErro() {

		return indicadorEnviaEmailErro;
	}

	/**
	 * Sets the indicador envia email erro.
	 *
	 * @param indicadorEnviaEmailErro the new indicador envia email erro
	 */
	public void setIndicadorEnviaEmailErro(Long indicadorEnviaEmailErro) {

		this.indicadorEnviaEmailErro = indicadorEnviaEmailErro;
	}

	/**
	 * Gets the destinatarios email.
	 *
	 * @return the destinatarios email
	 */
	public String getDestinatariosEmail() {

		return destinatariosEmail;
	}

	/**
	 * Sets the destinatarios email.
	 *
	 * @param destinatariosEmail the new destinatarios email
	 */
	public void setDestinatariosEmail(String destinatariosEmail) {

		this.destinatariosEmail = destinatariosEmail;
	}

	/**
	 * Gets the integracao sistema.
	 *
	 * @return the integracao sistema
	 */
	public IntegracaoSistema getIntegracaoSistema() {

		return integracaoSistema;
	}

	/**
	 * Sets the integracao sistema.
	 *
	 * @param integracaoSistema the new integracao sistema
	 */
	public void setIntegracaoSistema(IntegracaoSistema integracaoSistema) {

		this.integracaoSistema = integracaoSistema;
	}

	/**
	 * Gets the integracao funcao.
	 *
	 * @return the integracao funcao
	 */
	public IntegracaoFuncao getIntegracaoFuncao() {

		return integracaoFuncao;
	}

	/**
	 * Sets the integracao funcao.
	 *
	 * @param integracaoFuncao the new integracao funcao
	 */
	public void setIntegracaoFuncao(IntegracaoFuncao integracaoFuncao) {

		this.integracaoFuncao = integracaoFuncao;
	}

	/**
	 * Gets the fluxo integracao.
	 *
	 * @return the fluxo integracao
	 */
	public EntidadeConteudo getFluxoIntegracao() {

		return fluxoIntegracao;
	}

	/**
	 * Sets the fluxo integracao.
	 *
	 * @param fluxoIntegracao the new fluxo integracao
	 */
	public void setFluxoIntegracao(EntidadeConteudo fluxoIntegracao) {

		this.fluxoIntegracao = fluxoIntegracao;
	}

	/**
	 * Gets the pendencia.
	 *
	 * @return the pendencia
	 */
	public String getPendencia() {

		return pendencia;
	}

	/**
	 * Sets the pendencia.
	 *
	 * @param pendencia the new pendencia
	 */
	public void setPendencia(String pendencia) {

		this.pendencia = pendencia;
	}

}
