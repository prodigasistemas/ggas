/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import java.math.BigDecimal;

/**
 * The Interface IntegracaoNotaFiscalItem.
 */
public interface IntegracaoNotaFiscalItem extends Integracao {

	/** The bean id integracao nota fiscal item. */
	String BEAN_ID_INTEGRACAO_NOTA_FISCAL_ITEM = "integracaoNotaFiscalItem";

	/**
	 * Gets the nota fiscal.
	 *
	 * @return the nota fiscal
	 */
	IntegracaoNotaFiscal getNotaFiscal();

	/**
	 * Sets the nota fiscal.
	 *
	 * @param notaFiscal the new nota fiscal
	 */
	void setNotaFiscal(IntegracaoNotaFiscal notaFiscal);

	/**
	 * Gets the valor total.
	 *
	 * @return the valor total
	 */
	BigDecimal getValorTotal();

	/**
	 * Sets the valor total.
	 *
	 * @param valorTotal the new valor total
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * Gets the valor unitario.
	 *
	 * @return the valor unitario
	 */
	BigDecimal getValorUnitario();

	/**
	 * Sets the valor unitario.
	 *
	 * @param valorUnitario the new valor unitario
	 */
	void setValorUnitario(BigDecimal valorUnitario);

	/**
	 * Gets the codigo item.
	 *
	 * @return the codigo item
	 */
	Long getCodigoItem();

	/**
	 * Sets the codigo item.
	 *
	 * @param codigoItem the new codigo item
	 */
	void setCodigoItem(Long codigoItem);

	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao the new descricao
	 */
	void setDescricao(String descricao);

	/**
	 * Gets the quantidade.
	 *
	 * @return the quantidade
	 */
	BigDecimal getQuantidade();

	/**
	 * Sets the quantidade.
	 *
	 * @param quantidade the new quantidade
	 */
	void setQuantidade(BigDecimal quantidade);

	/**
	 * Gets the cfop.
	 *
	 * @return the cfop
	 */
	Long getCfop();

	/**
	 * Sets the cfop.
	 *
	 * @param cfop the new cfop
	 */
	void setCfop(Long cfop);

	/**
	 * Gets the sequencia.
	 *
	 * @return the sequencia
	 */
	Integer getSequencia();

	/**
	 * Sets the sequencia.
	 *
	 * @param sequencia the new sequencia
	 */
	void setSequencia(Integer sequencia);

	/**
	 * Gets the codigo segmento.
	 *
	 * @return the codigo segmento
	 */
	Long getCodigoSegmento();

	/**
	 * Sets the codigo segmento.
	 *
	 * @param codigoSegmento the new codigo segmento
	 */
	void setCodigoSegmento(Long codigoSegmento);

	/**
	 * Gets the codigo situacao tributaria icms.
	 *
	 * @return the codigo situacao tributaria icms
	 */
	String getCodigoSituacaoTributariaIcms();

	/**
	 * Sets the codigo situacao tributaria icms.
	 *
	 * @param codigoSituacaoTributariaIcms the new codigo situacao tributaria icms
	 */
	void setCodigoSituacaoTributariaIcms(String codigoSituacaoTributariaIcms);

	/**
	 * Gets the codigo situacao tributaria pis.
	 *
	 * @return the codigo situacao tributaria pis
	 */
	String getCodigoSituacaoTributariaPis();

	/**
	 * Sets the codigo situacao tributaria pis.
	 *
	 * @param codigoSituacaoTributariaPis the new codigo situacao tributaria pis
	 */
	void setCodigoSituacaoTributariaPis(String codigoSituacaoTributariaPis);

	/**
	 * Gets the codigo situacao tributaria cofins.
	 *
	 * @return the codigo situacao tributaria cofins
	 */
	String getCodigoSituacaoTributariaCofins();

	/**
	 * Sets the codigo situacao tributaria cofins.
	 *
	 * @param codigoSituacaoTributariaCofins the new codigo situacao tributaria cofins
	 */
	void setCodigoSituacaoTributariaCofins(String codigoSituacaoTributariaCofins);

	/**
	 * Gets the valor desconto.
	 *
	 * @return the valor desconto
	 */
	BigDecimal getValorDesconto();

	/**
	 * Sets the valor desconto.
	 *
	 * @param valorDesconto the new valor desconto
	 */
	void setValorDesconto(BigDecimal valorDesconto);

	/**
	 * Gets the descricao classificacao fiscal.
	 *
	 * @return the descricao classificacao fiscal
	 */
	String getDescricaoClassificacaoFiscal();

	/**
	 * Sets the descricao classificacao fiscal.
	 *
	 * @param descricaoClassificacaoFiscal the new descricao classificacao fiscal
	 */
	void setDescricaoClassificacaoFiscal(String descricaoClassificacaoFiscal);
	
	/**
	 * Gets o codigo CEST
	 *
	 * @return o codigo CEST
	 */

	String getCodigoCEST();

	/**
	 * Sets o codigo CEST
	 *
	 * @param codigoCEST o codigo CEST
	 */
	void setCodigoCEST(String codigoCEST);
	
	/**
	 * Gets o codigo Ramo Atividade
	 *
	 * @return o codigo Ramo Atividade
	 */
	Long getCodigoRamoAtividade();

	/**
	 * Sets o codigo Ramo Atividade
	 *
	 * @param codigoRamoAtividade
	 *            o codigoRamoAtividade
	 */
	void setCodigoRamoAtividade(Long codigoRamoAtividade);
}
