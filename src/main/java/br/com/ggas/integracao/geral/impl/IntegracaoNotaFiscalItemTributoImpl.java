/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:16:20
 @author vtavares
 */

package br.com.ggas.integracao.geral.impl;

import java.math.BigDecimal;

import br.com.ggas.integracao.geral.IntegracaoNotaFiscalItem;
import br.com.ggas.integracao.geral.IntegracaoNotaFiscalItemTributo;

/**
 * Classe da Entidade de Integração de Itens de Notas fiscais
 * Documentos fiscais para as tabelas de integração
 *
 */
public class IntegracaoNotaFiscalItemTributoImpl extends IntegracaoImpl implements IntegracaoNotaFiscalItemTributo {

	
	private static final long serialVersionUID = -7874459283226924071L;

	private IntegracaoNotaFiscalItem notaFiscalItem;

	private Long tributo;

	private BigDecimal percentualAliquota;

	private BigDecimal baseCalculo;

	private BigDecimal valorTributo;

	private BigDecimal valorOutros;

	private BigDecimal baseCalculoSubstituto;

	private BigDecimal valorSubstituto;

	private Boolean indicadorIsencao;

	private Boolean indicadorProdesin;

	@Override
	public IntegracaoNotaFiscalItem getNotaFiscalItem() {

		return notaFiscalItem;
	}

	@Override
	public void setNotaFiscalItem(IntegracaoNotaFiscalItem notaFiscalItem) {

		this.notaFiscalItem = notaFiscalItem;
	}

	@Override
	public Long getTributo() {

		return tributo;
	}

	@Override
	public void setTributo(Long tributo) {

		this.tributo = tributo;
	}

	@Override
	public BigDecimal getPercentualAliquota() {

		return percentualAliquota;
	}

	@Override
	public void setPercentualAliquota(BigDecimal percentualAliquota) {

		this.percentualAliquota = percentualAliquota;
	}

	@Override
	public BigDecimal getBaseCalculo() {

		return baseCalculo;
	}

	@Override
	public void setBaseCalculo(BigDecimal baseCalculo) {

		this.baseCalculo = baseCalculo;
	}

	@Override
	public BigDecimal getValorTributo() {

		return valorTributo;
	}

	@Override
	public void setValorTributo(BigDecimal valorTributo) {

		this.valorTributo = valorTributo;
	}

	@Override
	public BigDecimal getBaseCalculoSubstituto() {

		return baseCalculoSubstituto;
	}

	@Override
	public void setBaseCalculoSubstituto(BigDecimal baseCalculoSubstituto) {

		this.baseCalculoSubstituto = baseCalculoSubstituto;
	}

	@Override
	public BigDecimal getValorSubstituto() {

		return valorSubstituto;
	}

	@Override
	public void setValorSubstituto(BigDecimal valorSubstituto) {

		this.valorSubstituto = valorSubstituto;
	}

	@Override
	public Boolean getIndicadorIsencao() {

		return indicadorIsencao;
	}

	@Override
	public void setIndicadorIsencao(Boolean indicadorIsencao) {

		this.indicadorIsencao = indicadorIsencao;
	}

	@Override
	public Boolean getIndicadorProdesin() {

		return indicadorProdesin;
	}

	@Override
	public void setIndicadorProdesin(Boolean indicadorProdesin) {

		this.indicadorProdesin = indicadorProdesin;
	}

	@Override
	public BigDecimal getValorOutros() {

		return valorOutros;
	}

	@Override
	public void setValorOutros(BigDecimal valorOutros) {

		this.valorOutros = valorOutros;
	}
}
