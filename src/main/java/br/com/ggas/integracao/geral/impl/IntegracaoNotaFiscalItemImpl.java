/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:16:20
 @author vtavares
 */

package br.com.ggas.integracao.geral.impl;

import java.math.BigDecimal;

import br.com.ggas.integracao.geral.IntegracaoNotaFiscal;
import br.com.ggas.integracao.geral.IntegracaoNotaFiscalItem;

/**
 * Classe da entidade de  
 * Integração dos itens das notas fiscais 
 *
 */
public class IntegracaoNotaFiscalItemImpl extends IntegracaoImpl implements IntegracaoNotaFiscalItem {

	private static final long serialVersionUID = -7874459283226924071L;

	private IntegracaoNotaFiscal notaFiscal;

	private BigDecimal valorTotal;

	private BigDecimal valorUnitario;

	private BigDecimal valorDesconto;

	private Long codigoItem;

	private String descricao;

	private BigDecimal quantidade;

	private Long codigoSegmento;

	private Long codigoRamoAtividade;

	private String codigoSituacaoTributariaIcms;

	private String codigoSituacaoTributariaPis;

	private String codigoSituacaoTributariaCofins;

	private Long cfop;

	private Integer sequencia;

	private String descricaoClassificacaoFiscal;
	
	private String codigoCEST;

	@Override
	public IntegracaoNotaFiscal getNotaFiscal() {

		return notaFiscal;
	}

	@Override
	public void setNotaFiscal(IntegracaoNotaFiscal notaFiscal) {

		this.notaFiscal = notaFiscal;
	}

	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	@Override
	public BigDecimal getValorUnitario() {

		return valorUnitario;
	}

	@Override
	public void setValorUnitario(BigDecimal valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	@Override
	public Long getCodigoItem() {

		return codigoItem;
	}

	@Override
	public void setCodigoItem(Long codigoItem) {

		this.codigoItem = codigoItem;
	}

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	@Override
	public BigDecimal getQuantidade() {

		return quantidade;
	}

	@Override
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	@Override
	public Long getCfop() {

		return cfop;
	}

	@Override
	public void setCfop(Long cfop) {

		this.cfop = cfop;
	}

	@Override
	public Integer getSequencia() {

		return sequencia;
	}

	@Override
	public void setSequencia(Integer sequencia) {

		this.sequencia = sequencia;
	}

	@Override
	public Long getCodigoSegmento() {

		return codigoSegmento;
	}

	@Override
	public void setCodigoSegmento(Long codigoSegmento) {

		this.codigoSegmento = codigoSegmento;
	}

	@Override
	public String getCodigoSituacaoTributariaIcms() {

		return codigoSituacaoTributariaIcms;
	}

	@Override
	public void setCodigoSituacaoTributariaIcms(String codigoSituacaoTributariaIcms) {

		this.codigoSituacaoTributariaIcms = codigoSituacaoTributariaIcms;
	}

	@Override
	public String getCodigoSituacaoTributariaPis() {

		return codigoSituacaoTributariaPis;
	}

	@Override
	public void setCodigoSituacaoTributariaPis(String codigoSituacaoTributariaPis) {

		this.codigoSituacaoTributariaPis = codigoSituacaoTributariaPis;
	}

	@Override
	public String getCodigoSituacaoTributariaCofins() {

		return codigoSituacaoTributariaCofins;
	}

	@Override
	public void setCodigoSituacaoTributariaCofins(String codigoSituacaoTributariaCofins) {

		this.codigoSituacaoTributariaCofins = codigoSituacaoTributariaCofins;
	}

	@Override
	public BigDecimal getValorDesconto() {

		return valorDesconto;
	}

	@Override
	public void setValorDesconto(BigDecimal valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	@Override
	public String getDescricaoClassificacaoFiscal() {

		return descricaoClassificacaoFiscal;
	}

	@Override
	public void setDescricaoClassificacaoFiscal(String descricaoClassificacaoFiscal) {

		this.descricaoClassificacaoFiscal = descricaoClassificacaoFiscal;
	}

	@Override
	public String getCodigoCEST() {

		return codigoCEST;
	}

	@Override
	public void setCodigoCEST(String codigoCEST) {

		this.codigoCEST = codigoCEST;
	}

	@Override
	public Long getCodigoRamoAtividade() {

		return codigoRamoAtividade;
	}

	@Override
	public void setCodigoRamoAtividade(Long codigoRamoAtividade) {

		this.codigoRamoAtividade = codigoRamoAtividade;
	}
	
}
