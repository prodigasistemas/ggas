/*
 Copyright (C) <2011> GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este arquivo Ã© parte do GGAS, um sistema de gestÃ£o comercial de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este programa Ã© um software livre; vocÃª pode redistribuÃ­-lo e/ou
 modificÃ¡-lo sob os termos de LicenÃ§a PÃºblica Geral GNU, conforme
 publicada pela Free Software Foundation; versÃ£o 2 da LicenÃ§a.

 O GGAS Ã© distribuÃ­do na expectativa de ser Ãºtil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implÃ­cita de
 COMERCIALIZAÃ‡ÃƒO ou de ADEQUAÃ‡ÃƒO A QUALQUER PROPÃ“SITO EM PARTICULAR.
 Consulte a LicenÃ§a PÃºblica Geral GNU para obter mais detalhes.

 VocÃª deve ter recebido uma cÃ³pia da LicenÃ§a PÃºblica Geral GNU
 junto com este programa; se nÃ£o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place â€“ Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:10:20
 @author vtavares
 */

package br.com.ggas.integracao.geral.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.geral.IntegracaoFuncao;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.integracao.geral.IntegracaoSistemaParametro;

/**
 * Classe Integracao Sistema Funcao.
 * 
 *
 */
public class IntegracaoSistemaFuncaoImpl extends EntidadeNegocioImpl implements IntegracaoSistemaFuncao {

	/**
	 * 
	 */

	private static final long serialVersionUID = -7983964813569591039L;

	private Long indicadorEnviaEmailErro;

	private String destinatariosEmail;

	private IntegracaoSistema integracaoSistema;

	private IntegracaoFuncao integracaoFuncao;

	private Collection<IntegracaoSistemaParametro> listaIntegracaoSistemaParametro = new HashSet<IntegracaoSistemaParametro>();

	@Override
	public Long getIndicadorEnviaEmailErro() {

		return indicadorEnviaEmailErro;
	}

	@Override
	public void setIndicadorEnviaEmailErro(Long indicadorEnviaEmailErro) {

		this.indicadorEnviaEmailErro = indicadorEnviaEmailErro;
	}

	@Override
	public String getDestinatariosEmail() {

		return destinatariosEmail;
	}

	@Override
	public void setDestinatariosEmail(String destinatariosEmail) {

		this.destinatariosEmail = destinatariosEmail;
	}

	@Override
	public IntegracaoSistema getIntegracaoSistema() {

		return integracaoSistema;
	}

	@Override
	public void setIntegracaoSistema(IntegracaoSistema integracaoSistema) {

		this.integracaoSistema = integracaoSistema;
	}

	@Override
	public IntegracaoFuncao getIntegracaoFuncao() {

		return integracaoFuncao;
	}

	@Override
	public void setIntegracaoFuncao(IntegracaoFuncao integracaoFuncao) {

		this.integracaoFuncao = integracaoFuncao;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public Collection<IntegracaoSistemaParametro> getListaIntegracaoSistemaParametro() {

		return listaIntegracaoSistemaParametro;
	}

	@Override
	public void setListaIntegracaoSistemaParametro(Collection<IntegracaoSistemaParametro> listaIntegracaoSistemaParametro) {

		this.listaIntegracaoSistemaParametro = listaIntegracaoSistemaParametro;
	}

}
