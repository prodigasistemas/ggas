/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**Classe Responsável pelas ações relacionadas à funcionalidade de  EntidadesIntegracaoEnum
 * 
 @since 03/07/2013 14:48:51
 @author vpessoa
 */

package br.com.ggas.integracao.geral;


/**
 * Enum Entidades Integracao.
 * 
 *
 */
public enum EntidadesIntegracaoEnum {

	MEDIDOR_SITUACAO("Situação de Medidor"), MEDIDOR_TIPO("Tipo de Medidor"), MEDIDOR_DIAMETRO("Diâmetro do Medidor"), MEDIDOR_MARCA(
					"Marca de Medidor"), MEDIDOR_MODELO("Modelo de Medidor"), MEDIDOR_CAPACIDADE("Capacidade do Medidor"),
	MEDIDOR_LOCAL_ARMAZENAGEM("Local Armazenagem do Medidor"), MEDIDOR_FATOR_K("Fator K do Medidor"), UNIDADE("Unidade de Medida"),
	TEMPERATURA_TRABALHO_FAIXA("Faixa de Temperatura de Trabalho"), TIPO_TRANSDUTOR_PRESSAO("Tipo de Transdutor de Pressão"),
	TEMPERATURA_MAXIMA_TRANSDUTOR("Temperatura Máxima de Transdutor do Corretor de Vazão"), MODELO_VAZAO_CORRETOR(
					"Modelo de Corretor de Vazão"), PROTOCOLO_COMUNICACAO("Protocolo de Comunicação"), TIPO_TRANSDUTOR_TEMPERATURA(
					"Tipo de Transdutor de Temperatura"), PRESSAO_MAXIMA_TRANSDUTOR("Pressão Máxima de Transdutor do Corretor de Vazão"),
	MARCA_CORRETOR("Marca de Corretor de Vazão");

	private final String valor;

	/**
	 * Instantiates a new entidades integracao enum.
	 * 
	 * @param valorOpcao
	 *            the valor opcao
	 */
	EntidadesIntegracaoEnum(String valorOpcao) {

		valor = valorOpcao;

	}

	public String getValor() {

		return valor;

	}

}
