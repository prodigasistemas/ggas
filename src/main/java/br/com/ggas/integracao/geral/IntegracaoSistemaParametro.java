/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface IntegracaoSistemaParametro.
 */
public interface IntegracaoSistemaParametro extends EntidadeNegocio {

	/** The bean id integracao sistema parametro. */
	String BEAN_ID_INTEGRACAO_SISTEMA_PARAMETRO = "integracaoSistemaParametro";

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	String getValor();

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	void setValor(String valor);

	/**
	 * Gets the integracao parametro.
	 *
	 * @return the integracao parametro
	 */
	IntegracaoParametro getIntegracaoParametro();

	/**
	 * Sets the integracao parametro.
	 *
	 * @param integracaoParametro the new integracao parametro
	 */
	void setIntegracaoParametro(IntegracaoParametro integracaoParametro);

	/**
	 * Gets the integracao sistema funcao.
	 *
	 * @return the integracao sistema funcao
	 */
	IntegracaoSistemaFuncao getIntegracaoSistemaFuncao();

	/**
	 * Sets the integracao sistema funcao.
	 *
	 * @param integracaoSistemaFuncao the new integracao sistema funcao
	 */
	void setIntegracaoSistemaFuncao(IntegracaoSistemaFuncao integracaoSistemaFuncao);

	/**
	 * Gets the integracao sistema.
	 *
	 * @return the integracao sistema
	 */
	IntegracaoSistema getIntegracaoSistema();

	/**
	 * Sets the integracao sistema.
	 *
	 * @param integracaoSistema the new integracao sistema
	 */
	void setIntegracaoSistema(IntegracaoSistema integracaoSistema);

	/**
	 * Gets the valor long.
	 *
	 * @return the valor long
	 * @throws NegocioException the negocio exception
	 */
	Long getValorLong() throws NegocioException;

}
