/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface IntegracaoMapeamento.
 */
public interface IntegracaoMapeamento extends EntidadeNegocio {

	/** The bean id integracao mapeamento. */
	String BEAN_ID_INTEGRACAO_MAPEAMENTO = "integracaoMapeamento";

	/**
	 * Gets the chave mapeamento.
	 *
	 * @return the chave mapeamento
	 */
	int getChaveMapeamento();

	/**
	 * Sets the chave mapeamento.
	 *
	 * @param chaveMapemanto the new chave mapeamento
	 */
	void setChaveMapeamento(int chaveMapemanto);

	/**
	 * Gets the codigo entidade sistema ggas.
	 *
	 * @return the codigo entidade sistema ggas
	 */
	String getCodigoEntidadeSistemaGgas();

	/**
	 * Sets the codigo entidade sistema ggas.
	 *
	 * @param codigoEntidadeSistemaGgas the new codigo entidade sistema ggas
	 */
	void setCodigoEntidadeSistemaGgas(String codigoEntidadeSistemaGgas);

	/**
	 * Gets the codigo entidade sistema integrante.
	 *
	 * @return the codigo entidade sistema integrante
	 */
	String getCodigoEntidadeSistemaIntegrante();

	/**
	 * Sets the codigo entidade sistema integrante.
	 *
	 * @param codigoEntidadeSistemaIntegrante the new codigo entidade sistema integrante
	 */
	void setCodigoEntidadeSistemaIntegrante(String codigoEntidadeSistemaIntegrante);

	/**
	 * Gets the tabela.
	 *
	 * @return the tabela
	 */
	Tabela getTabela();

	/**
	 * Sets the tabela.
	 *
	 * @param tabela the new tabela
	 */
	void setTabela(Tabela tabela);

	/**
	 * Gets the integracao sistema.
	 *
	 * @return the integracao sistema
	 */
	IntegracaoSistema getIntegracaoSistema();

	/**
	 * Sets the integracao sistema.
	 *
	 * @param integracaoSistema the new integracao sistema
	 */
	void setIntegracaoSistema(IntegracaoSistema integracaoSistema);

}
