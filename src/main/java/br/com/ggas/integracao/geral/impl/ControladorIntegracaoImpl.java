/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/01/2013 17:31:43
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral.impl;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.*;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl;
import br.com.ggas.cadastro.imovel.*;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.faturamento.*;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.FaturaItemTributacao;
import br.com.ggas.faturamento.fatura.FaturaTributacao;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.ExistenciaClasseVO;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.bens.*;
import br.com.ggas.integracao.bens.impl.IntegracaoBemImpl;
import br.com.ggas.integracao.bens.impl.IntegracaoBemMedidorImpl;
import br.com.ggas.integracao.bens.impl.IntegracaoBemMovimentacaoImpl;
import br.com.ggas.integracao.bens.impl.IntegracaoBemVazaoCorretorImpl;
import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.cliente.IntegracaoClienteContato;
import br.com.ggas.integracao.cliente.IntegracaoClienteEndereco;
import br.com.ggas.integracao.cliente.IntegracaoClienteTelefone;
import br.com.ggas.integracao.cliente.impl.IntegracaoClienteContatoImpl;
import br.com.ggas.integracao.cliente.impl.IntegracaoClienteEnderecoImpl;
import br.com.ggas.integracao.cliente.impl.IntegracaoClienteImpl;
import br.com.ggas.integracao.cliente.impl.IntegracaoClienteTelefoneImpl;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.*;
import br.com.ggas.integracao.titulos.*;
import br.com.ggas.integracao.titulos.impl.IntegracaoTituloImpl;
import br.com.ggas.integracao.titulos.impl.IntegracaoTituloPagarImpl;
import br.com.ggas.integracao.titulos.impl.IntegracaoTituloReceberImpl;
import br.com.ggas.medicao.medidor.*;
import br.com.ggas.medicao.medidor.impl.HistoricoOperacaoMedidorImpl;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.medidor.impl.SituacaoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.*;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorHistoricoOperacaoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;
import br.com.ggas.nfe.ControladorNfe;
import br.com.ggas.nfe.Nfe;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.*;
import com.google.common.base.Strings;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.Future;

/**
 * Classe com os métodos
 * para dados nas tabelas de integração
 *
 */
public class ControladorIntegracaoImpl extends ControladorNegocioImpl implements ControladorIntegracao {

	private static final int CONSTANTE_NUMERO_TRES = 3;
	private static final int CONSTANTE_NUMERO_DOIS = 2;
	private static final Logger LOG = Logger.getLogger(ControladorIntegracaoImpl.class);
	public static final String INTEGRACAO_SISTEMA_CHAVE_PRIMARIA = "integracaoSistema.chavePrimaria";
	public static final int CEM = 100;

	private ResourceBundle mensagens = ResourceBundle.getBundle("mensagens");

	private final ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
					ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

	private static final String ERRO_PROCESSO_INTEGRACAO_AGUARDANDO_RETORNO = "ERRO_PROCESSO_INTEGRACAO_AGUARDANDO_RETORNO";

	private static final String ERRO_PROCESSO_INTEGRACAO_TITULO_AGUARDANDO_RETORNO = "ERRO_PROCESSO_INTEGRACAO_TITULO_AGUARDANDO_RETORNO";

	private final ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

	private final ControladorParametroSistema controladorParamento = (ControladorParametroSistema) ServiceLocator.getInstancia()
			.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String NOME = "nome";

	private static final String SIGLA = "sigla";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String PARCELADA = "Parcelada";

	private static final String DATA_EMISSAO = "dataEmissao";

	private static String NUMERO_MSG_ENV_AUT = "100";

	private static String NUMERO_MSG_ENV_CAN = "101";

	private static String NUMERO_MSG_ENV_DEN = "110";

	private static String NUMERO_MSG_ENV_ERRO = "999";

	public static final String OPERACAO = "OPERACAO_REINTEGRAR";
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseIntegracaoSistema() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoSistema.BEAN_ID_INTEGRACAO_SISTEMA);
	}

	public Class<?> getClasseIntegracaoFuncao() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoFuncao.BEAN_ID_INTEGRACAO_FUNCAO);
	}

	public Class<?> getClasseIntegracaoSistemaFuncao() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoSistemaFuncao.BEAN_ID_INTEGRACAO_SISTEMA_FUNCAO);
	}

	public Class<?> getClasseEntidadeIntegracaoParametro() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoParametro.BEAN_ID_INTEGRACAO_PARAMETRO);
	}

	public Class<?> getClasseEntidadeIntegracaoSistemaParametro() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoSistemaParametro.BEAN_ID_INTEGRACAO_SISTEMA_PARAMETRO);
	}

	public Class<?> getClasseEntidadeIntegracaoContrato() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoContrato.BEAN_ID_INTEGRACAO_CONTRATO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	/**
	 * Gets the entidade funcionario.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade funcionario
	 */
	public EntidadeNegocio getEntidadeFuncionario(Long chavePrimaria) {

		return ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(Funcionario.BEAN_ID_FUNCIONARIO, chavePrimaria);
	}

	/**
	 * Gets the entidade medidor local armazenagem.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade medidor local armazenagem
	 */
	public EntidadeNegocio getEntidadeMedidorLocalArmazenagem(Long chavePrimaria) {

		return ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM,
						chavePrimaria);
	}

	/**
	 * Gets the entidade motivo movimentacao medidor.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade motivo movimentacao medidor
	 */
	public EntidadeNegocio getEntidadeMotivoMovimentacaoMedidor(Long chavePrimaria) {

		return ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(MotivoMovimentacaoMedidor.BEAN_ID_MOTIVO_MOVIMENTACAO_MEDIDOR,
						chavePrimaria);
	}

	/**
	 * Gets the entidade vazao corretor.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade vazao corretor
	 */
	public EntidadeNegocio getEntidadeVazaoCorretor(Long chavePrimaria) {

		return ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(VazaoCorretor.BEAN_ID_VAZAO_CORRETOR, chavePrimaria);
	}

	/**
	 * Gets the entidade situacao medidor.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade situacao medidor
	 */
	public EntidadeNegocio getEntidadeSituacaoMedidor(Long chavePrimaria) {

		return ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(SituacaoMedidor.BEAN_ID_SITUACAO_MEDIDOR, chavePrimaria);
	}

	public Class<?> getClasseEntidadeIntegracaoBem() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoBem.BEAN_ID_INTEGRACAO_BEM);
	}

	public Class<?> getClasseEntidadeRecurso() {

		return ServiceLocator.getInstancia().getClassPorID(Recurso.BEAN_ID_RECURSO);
	}

	public Class<?> getClasseEntidadeIntegracaoOperacaoInativa() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoOperacaoInativa.BEAN_ID_INTEGRACAO_OPERACAO_INATIVA);
	}
	/**
	 * Gets the classe entidade fatura item.
	 *
	 * @return the classe entidade fatura item
	 */
	public Class<?> getClasseEntidadeIntegracaoNotaFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoNotaFiscal.BEAN_ID_INTEGRACAO_NOTA_FISCAL);
	}
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultarIntegracaoFuncao(java.util.Map)
	 */
	@Override
	public Collection<IntegracaoFuncao> consultarIntegracaoFuncao(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseIntegracaoSistema());
		preencherCriteria(filtro, criteria);
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoFuncao()
	 */
	@Override
	public Collection<IntegracaoFuncao> listarIntegracaoFuncao() {

		Query query = criarQueryParaCombos(getClasseIntegracaoFuncao().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoFuncaoTabela()
	 */
	@Override
	public Collection<IntegracaoFuncaoTabela> listarIntegracaoFuncaoTabela() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseIntegracaoSistemaFuncao().getSimpleName());

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoSistemaFuncao(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao(Map<String, Object> filtro) {

		Query query = null;
		Long sistema = (Long) filtro.get("integracaoSistema");
		Long funcao = (Long) filtro.get("integracaoFuncao");
		String flag = null;
		if (filtro.get("integracaoFuncaoTabela") != null) {
			flag = (String) filtro.get("integracaoFuncaoTabela");
		}
		Boolean habilitado = (Boolean) filtro.get(HABILITADO);

		StringBuilder hql = new StringBuilder();
		hql.append("select sistemaFuncao ");
		hql.append(" from ");
		hql.append(getClasseIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" sistemaFuncao");
		hql.append(" inner join fetch sistemaFuncao.integracaoSistema sistema ");
		hql.append(" left join fetch sistemaFuncao.integracaoFuncao funcao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));

		if (funcao != null) {
			if ((sistema != null && sistema != -1) && funcao != -1) {
				HibernateHqlUtil.adicionarClausulaWhereOuAnd(hql);
				hql.append(" sistemaFuncao.integracaoSistema.chavePrimaria = ?");
				hql.append(" and sistemaFuncao.integracaoFuncao.chavePrimaria = ?");
				if (habilitado != null) {
					if (habilitado) {
						hql.append(" and sistemaFuncao.habilitado = true");
					} else {
						hql.append(" and sistemaFuncao.habilitado = false");
					}
				}

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));
				query.setLong(0, sistema);
				query.setLong(1, funcao);

			} else if ((sistema == null || sistema == -1) && funcao != -1) {
				HibernateHqlUtil.adicionarClausulaWhereOuAnd(hql);
				hql.append(" sistemaFuncao.integracaoFuncao.chavePrimaria = ?");
				if (habilitado != null) {
					if (habilitado) {
						hql.append(" and sistemaFuncao.habilitado = true");
					} else {
						hql.append(" and sistemaFuncao.habilitado = false");
					}
				}

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));
				query.setLong(0, funcao);

			} else if ((sistema != null && sistema != -1) && funcao == -1) {
				HibernateHqlUtil.adicionarClausulaWhereOuAnd(hql);
				hql.append(" sistemaFuncao.integracaoSistema.chavePrimaria = ?");
				if (habilitado != null) {
					if (habilitado) {
						hql.append(" and sistemaFuncao.habilitado = true");
					} else {
						hql.append(" and sistemaFuncao.habilitado = false");
					}
				}

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));
				query.setLong(0, sistema);

			} else if ((sistema == null || sistema == -1) && funcao == -1) {
				if (habilitado != null) {
					HibernateHqlUtil.adicionarClausulaWhereOuAnd(hql);
					if (habilitado) {
						hql.append(" sistemaFuncao.habilitado = true");
					} else {
						hql.append(" sistemaFuncao.habilitado = false");
					}
				}

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));

			}
		} else {
			if (sistema != null && sistema != -1) {
				HibernateHqlUtil.adicionarClausulaWhereOuAnd(hql);
				hql.append(" sistemaFuncao.integracaoSistema.chavePrimaria = ?");

				if (habilitado != null) {
					if (habilitado) {
						hql.append(" and sistemaFuncao.habilitado = true");
					} else {
						hql.append(" and sistemaFuncao.habilitado = false");
					}
				}

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));
				query.setLong(0, sistema);

			} else {
				HibernateHqlUtil.adicionarClausulaWhereOuAnd(hql);
				if (habilitado) {
					hql.append(" sistemaFuncao.habilitado = true");
				} else {
					hql.append(" sistemaFuncao.habilitado = false");
				}

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updateHQLtoFindFuncaoTabela(hql, flag));

			}
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral
	 * .ControladorIntegracao
	 * #listarIntegracaoSistemaFuncaoCadastroBens()
	 */
	@Override
	public Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncaoCadastroBens() throws NegocioException {

		Long idCadastroBens = getControladorConstanteSistema().obterConstanteContratoCadastroBens().getValorLong();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("integracaoFuncao", idCadastroBens);
		map.put(HABILITADO, Boolean.TRUE);

		return listarIntegracaoSistemaFuncao(map);
	}

	private ControladorConstanteSistema getControladorConstanteSistema() {

		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	private ControladorContrato getControladorContrato() {
		return (ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#removerSistemaFuncao(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerSistemaFuncao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		removerSistemaParametro(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Remover sistema parametro.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	// auxilia o removerSistemaFuncao
	public void removerSistemaParametro(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		for (Long chave : chavesPrimarias) {
			IntegracaoSistemaFuncao integracaoSistemaFuncao = obterIntegracaoSistemaFuncao(chave);
			integracaoSistemaFuncao.getListaIntegracaoSistemaParametro().clear();
			Collection<IntegracaoMapeamento> mapeamentos = this.localizarIntegracaoMapeamento(chave);
			for (IntegracaoMapeamento integracaoMapeamento : mapeamentos) {
				super.remover(integracaoMapeamento, IntegracaoMapeamentoImpl.class);
			}
			super.remover(integracaoSistemaFuncao);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#removerIntegracaoMapeamento(java.util.Collection)
	 */
	@Override
	public void removerIntegracaoMapeamento(Collection<IntegracaoMapeamento> listaIntegracaoMapeamento) throws NegocioException {

		for (IntegracaoMapeamento integracaoMapeamento : listaIntegracaoMapeamento) {
			this.remover(integracaoMapeamento);
		}

	}

	/**
	 * Preencher criteria.
	 *
	 * @param filtro
	 *            the filtro
	 * @param criteria
	 *            the criteria
	 */
	private void preencherCriteria(Map<String, Object> filtro, Criteria criteria) {

		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
			Long idIntegracaoSistema = (Long) filtro.get("integracaoSistema");
			if ((idIntegracaoSistema != null) && (idIntegracaoSistema > 0)) {
				criteria.createAlias("integracaoSistema", "integracaoSistema");
				criteria.add(Restrictions.eq(INTEGRACAO_SISTEMA_CHAVE_PRIMARIA, idIntegracaoSistema));
			} else {
				criteria.setFetchMode("integracaoSistema", FetchMode.JOIN);
			}
			Long idIntegracaoFuncao = (Long) filtro.get("integracaoFuncao");
			if ((idIntegracaoFuncao != null) && (idIntegracaoFuncao > 0)) {
				criteria.createAlias("integracaoFuncao", "integracaoFuncao");
				criteria.add(Restrictions.eq("integracaoFuncao.chavePrimaria", idIntegracaoFuncao));
			} else {
				criteria.setFetchMode("integracaoFuncao", FetchMode.JOIN);
			}
		}
	}

	/**
	 * Criar query para combos.
	 *
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaCombos(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#criarIntegracaoSistema()
	 */
	@Override
	public EntidadeNegocio criarIntegracaoSistema() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoSistema.BEAN_ID_INTEGRACAO_SISTEMA);
	}

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoCliente.
	 *
	 * @return the integracao cliente
	 */
	public IntegracaoCliente criarIntegracaoCliente() {

		return (IntegracaoCliente) ServiceLocator.getInstancia().getBeanPorID(IntegracaoCliente.BEAN_ID_INTEGRACAO_CLIENTE);
	}

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoClienteEndereco.
	 *
	 * @return the integracao cliente endereco
	 */
	public IntegracaoClienteEndereco criarIntegracaoClienteEndereco() {

		return (IntegracaoClienteEndereco) ServiceLocator.getInstancia().getBeanPorID(
						IntegracaoClienteEndereco.BEAN_ID_INTEGRACAO_CLIENTE_ENDERECO);
	}

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoClienteTelefone.
	 *
	 * @return the integracao cliente telefone
	 */
	public IntegracaoClienteTelefone criarIntegracaoClienteTelefone() {

		return (IntegracaoClienteTelefone) ServiceLocator.getInstancia().getBeanPorID(
						IntegracaoClienteTelefone.BEAN_ID_INTEGRACAO_CLIENTE_TELEFONE);
	}

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoClienteContato.
	 *
	 * @return the integracao cliente contato
	 */
	public IntegracaoClienteContato criarIntegracaoClienteContato() {

		return (IntegracaoClienteContato) ServiceLocator.getInstancia().getBeanPorID(
						IntegracaoClienteContato.BEAN_ID_INTEGRACAO_CLIENTE_CONTATO);
	}

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoContrato.
	 *
	 * @return the integracao contrato
	 */
	public IntegracaoContrato criarIntegracaoContrato() {

		return (IntegracaoContrato) ServiceLocator.getInstancia().getBeanPorID(IntegracaoContrato.BEAN_ID_INTEGRACAO_CONTRATO);
	}

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoMapeamento.
	 *
	 * @return integracao mapeamento
	 */
	@Override
	public IntegracaoMapeamento criarIntegracaoMapeamento() {

		return (IntegracaoMapeamento) ServiceLocator.getInstancia().getBeanPorID(IntegracaoMapeamento.BEAN_ID_INTEGRACAO_MAPEAMENTO);
	}

	/**
	 * Inserir integracao sistema.
	 *
	 * @param integracaoSistema
	 *            the integracao sistema
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long inserirIntegracaoSistema(IntegracaoSistema integracaoSistema) throws NegocioException {

		return (Long) super.inserir(integracaoSistema);
	}

	/**
	 * Método responsável por consultar a partir da chave primaria do Cliente todas as IntegracaoCliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @return listaIntegracaoCliente
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoCliente> consultarIntegracaoCliente(Long cliente) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoCliente().getSimpleName());
		hql.append(" integracaoCliente ");
		hql.append(" where ");
		hql.append(" integracaoCliente.habilitado = true and ");
		hql.append(" integracaoCliente.clienteOrigem = :cliente ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("cliente", cliente);

		return query.list();
	}

	@SuppressWarnings("squid:S1192")
	@Override
	public Boolean validarIntegracaoContratoImovel(Long idImovel) {

		Query query = null;

		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteNaoProcessado =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" integracaoContrato ");
		hql.append(" inner join integracaoContrato.contratoPontoConsumo cpc ");
		hql.append(" inner join integracaoContrato.contratoPontoConsumo.pontoConsumo pc ");
		hql.append(" where ");
		hql.append(" integracaoContrato.situacao = :situacao and ");
		hql.append(" pc.imovel.chavePrimaria = :idimovel ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idimovel", idImovel);
		query.setString("situacao", constanteNaoProcessado.getValor());

		return query.list().isEmpty();
	}

	@Override
	@SuppressWarnings("squid:S1192")
	public Boolean validarIntegracaoContrato(Long idContrato, Long idContratoPai) {

		Query query = null;

		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteNaoProcessado =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" integracaoContrato ");
		hql.append(" where ");
		hql.append(" (integracaoContrato.contratoPaiOrigem = :idContratoPai or integracaoContrato.contratoOrigem.chavePrimaria = :idContratoPai");
		hql.append(" or integracaoContrato.contratoOrigem.chavePrimaria = :idContrato) ");
		hql.append(" and (integracaoContrato.situacao = :situacao)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("idContrato", idContrato);
		query.setParameter("idContratoPai", idContratoPai);
		query.setParameter("situacao", constanteNaoProcessado.getValor());

		return query.list().isEmpty();

	}

	@Override
	@SuppressWarnings("squid:S1192")
	public Boolean validarIntegracaoContratoCliente(Long idCliente) {

		Query query = null;

		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteNaoProcessado =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" integracaoContrato ");
		hql.append(" where ");
		hql.append(" (integracaoContrato.chaveCliente = :idCliente) and ");
		hql.append(" (integracaoContrato.situacao = :situacao) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCliente", idCliente);
		query.setString("situacao", constanteNaoProcessado.getValor());

		return query.list().isEmpty();

	}

	/**
	 * Método responsável por consultar a partir da chave primaria da IntegracaoCliente todas as IntegracaoClienteEndereco.
	 *
	 * @param cliente
	 *            the cliente
	 * @param operacao
	 *            the operacao
	 * @param principal
	 *            the principal
	 * @return listaIntegracaoClienteEndereco
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoClienteEndereco> consultarIntegracaoClienteEndereco(Long cliente, String operacao, Long principal) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoClienteEndereco().getSimpleName());
		hql.append(" integracaoClienteEndereco ");
		hql.append(" where ");
		hql.append(" integracaoClienteEndereco.habilitado = true and ");
		hql.append(" integracaoClienteEndereco.cliente = :cliente ");
		if (operacao != null) {
			hql.append(" and integracaoClienteEndereco.operacao <> :operacao ");
		}
		if (principal != null) {
			hql.append(" and integracaoClienteEndereco.correspondencia = :correspondencia ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("cliente", cliente);

		if (operacao != null) {
			query.setString("operacao", operacao);
		}
		if (principal != null) {
			query.setLong("correspondencia", principal);
		}

		return query.list();
	}

	/**
	 * Método responsável por consultar a partir da chave primaria da IntegracaoCliente todas as IntegracaoClienteTelefone.
	 *
	 * @param cliente
	 *            the cliente
	 * @param operacao
	 *            the operacao
	 * @param principal
	 *            the principal
	 * @return listaIntegracaoClienteTelefone
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoClienteTelefone> consultarIntegracaoClienteTelefone(Long cliente, String operacao, Long principal) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoClienteTelefone().getSimpleName());
		hql.append(" integracaoClienteTelefone ");
		hql.append(" where ");
		hql.append(" integracaoClienteTelefone.habilitado = true and ");
		hql.append(" integracaoClienteTelefone.cliente = :cliente ");
		if (operacao != null) {
			hql.append(" and integracaoClienteTelefone.operacao <> :operacao ");
		}
		if (principal != null) {
			hql.append(" and integracaoClienteTelefone.indicadorPrincipal = :indicadorPrincipal ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("cliente", cliente);

		if (operacao != null) {
			query.setString("operacao", operacao);
		}
		if (principal != null) {
			query.setLong("indicadorPrincipal", principal);
		}

		return query.list();
	}

	/**
	 * Método responsável por consultar a partir da chave primaria da IntegracaoCliente todas as IntegracaoClienteContato.
	 *
	 * @param cliente
	 *            the cliente
	 * @param operacao
	 *            the operacao
	 * @param principal
	 *            the principal
	 * @return listaIntegracaoClienteContato
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoClienteContato> consultarIntegracaoClienteContato(Long cliente, String operacao, Long principal) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoClienteContato().getSimpleName());
		hql.append(" integracaoClienteContato ");
		hql.append(" where ");
		hql.append(" integracaoClienteContato.habilitado = true and ");
		hql.append(" integracaoClienteContato.cliente = :cliente ");
		if (operacao != null) {
			hql.append(" and integracaoClienteContato.operacao <> :operacao ");
		}
		if (principal != null) {
			hql.append(" and integracaoClienteContato.principal = :principal ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("cliente", cliente);

		if (operacao != null) {
			query.setString("operacao", operacao);
		}
		if (principal != null) {
			query.setLong("principal", principal);
		}

		return query.list();
	}

	/**
	 * Buscar codigo sistema integrante numerico.
	 *
	 * @param codigoGGAS - {@link Long}
	 * @param tabela - {@link String}
	 * @param sistemaOrigem {@link String}
	 * @return código da entidade no sistema integrante - {@link Long}
	 */
	private Long buscarCodigoSistemaIntegranteNumerico(Long codigoGGAS, String tabela, String sistemaOrigem) {

		String codigoSistemaIntegrante = buscarCodigoSistemaIntegrante(codigoGGAS, tabela, sistemaOrigem);

		Long codigoNumerico = NumeroUtil.stringParaLong(codigoSistemaIntegrante, codigoGGAS);

		if (codigoNumerico == null) {
			codigoNumerico = codigoGGAS;
		}

		return codigoNumerico;
	}

	/**
	 * Buscar codigo sistema integrante.
	 *
	 * @param codigoGGAS
	 *            the codigo ggas
	 * @param tabela
	 *            the tabela
	 * @param sistemaOrigem
	 *            the sistema origem
	 * @return the string
	 */
	private String buscarCodigoSistemaIntegrante(Long codigoGGAS, String tabela, String sistemaOrigem) {

		Query query = montarConsultaBuscarCodigoSistemaIntegrante(tabela, sistemaOrigem);
		query.setString("codigoGGAS", String.valueOf(codigoGGAS));

		String codigoSistemaIntegracao = (String) query.setMaxResults(1).uniqueResult();
		if (codigoSistemaIntegracao != null) {
			return codigoSistemaIntegracao;
		} else {
			return String.valueOf(codigoGGAS);
		}
	}

	/**
	 * Montar consulta buscar codigo sistema integrante.
	 *
	 * @param tabela
	 *            the tabela
	 * @param sistemaOrigem
	 *            the sistema origem
	 * @return the query
	 */
	private Query montarConsultaBuscarCodigoSistemaIntegrante(String tabela, String sistemaOrigem) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select integracaoMapeamento.codigoEntidadeSistemaIntegrante ");
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoMapeamento().getSimpleName());
		hql.append(" integracaoMapeamento");
		hql.append(" where integracaoMapeamento.integracaoSistema.chavePrimaria = (select chavePrimaria from ")
				.append(getClasseEntidadeIntegracaoSistema().getSimpleName()).append(" integracaoSistema where sigla = '")
				.append(sistemaOrigem).append("' )");
		hql.append(" and integracaoMapeamento.tabela.chavePrimaria = (select chavePrimaria from ")
				.append(getClasseEntidadeTabela().getSimpleName()).append(" tabela where tabela.nomeClasse = '").append(tabela)
				.append("' ) ");
		hql.append(" and integracaoMapeamento.codigoEntidadeSistemaGgas = :codigoGGAS ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query;
	}

	/**
	 * Consulta os mapeamentos dos sistemas integrantes, a partir das siglas dos sistemas
	 * passadas por parâmetro. Retorna um mapa com valor igual ao código da entidade, do
	 * tipo {@link String}, no sistema integrante e com chave composta da sigla do sistema
	 * integrante, do código da entidade no sistema GGAS e do nome da classe mapeada.
	 *
	 * @param siglasSistemasIntegrantes - {@link List}
	 * @return {@link MultiKeyMap}
	 */
	@Override
	public MultiKeyMap consultarMapeamentosPorSistemasIntegrantes(List<String> siglasSistemasIntegrantes) {

		MultiKeyMap multiKeyMap = new MultiKeyMap();

		if (siglasSistemasIntegrantes != null && !siglasSistemasIntegrantes.isEmpty()) {
			StringBuilder hql = new StringBuilder();
			hql.append(" select integracaoSistema.sigla, tabela.nomeClasse, ");
			hql.append(" integracaoMapeamento.codigoEntidadeSistemaGgas, ");
			hql.append(" integracaoMapeamento.codigoEntidadeSistemaIntegrante ");
			hql.append(" from ");
			hql.append(getClasseEntidadeIntegracaoMapeamento().getSimpleName());
			hql.append(" integracaoMapeamento");
			hql.append(" inner join integracaoMapeamento.integracaoSistema integracaoSistema ");
			hql.append(" inner join integracaoMapeamento.tabela tabela");
			hql.append(" where integracaoSistema.sigla IN (:siglasSistemasIntegrantes) ");

			Query query = getSession().createQuery(hql.toString());
			query.setParameterList("siglasSistemasIntegrantes", siglasSistemasIntegrantes);

			List<Object[]> list = query.list();

			for (Object[] object : list) {
				multiKeyMap.put(object[0], object[1], object[CONSTANTE_NUMERO_DOIS], object[CONSTANTE_NUMERO_TRES]);
			}
		}
		return multiKeyMap;
	}

	/**
	 * Obtém o código de uma entidade em um sistema integrante a partir dos mapeamentos dos sistemas integrantes, indexados por sigla, nome
	 * da entidade mapeada e código da entidade. Caso não exista mapeamento, retorna o código da entidade no sistema GGAS.
	 *
	 * @param mapeamentosPorIntegrantes - {@link MultiKeyMap}
	 * @param entidadeNegocio - {@link EntidadeNegocio}
	 * @param integracaoSistema - {@link IntegracaoSistema}
	 * @return codigoEntidadeSistemaIntegrante - {@link Long}
	 */
	@Override
	public Long getCodigoEntidadeSistemaIntegrante(MultiKeyMap mapeamentosPorIntegrantes, EntidadeNegocio entidadeNegocio,
					IntegracaoSistema integracaoSistema) {

		Long codigoEntidadeSistemaIntegrante = null;

		if (entidadeNegocio != null) {
			codigoEntidadeSistemaIntegrante = entidadeNegocio.getChavePrimaria();
		}

		if (Util.isAllNotNull(mapeamentosPorIntegrantes, entidadeNegocio, integracaoSistema)) {

			String codigo =
							(String) mapeamentosPorIntegrantes.get(integracaoSistema.getSigla(),
											entidadeNegocio.getClass().getInterfaces()[0].getName(),
											String.valueOf(entidadeNegocio.getChavePrimaria()));

			if (!StringUtils.isBlank(codigo)) {
				codigoEntidadeSistemaIntegrante = NumeroUtil.stringParaLong(codigo, entidadeNegocio.getChavePrimaria());
			}
		}
		return codigoEntidadeSistemaIntegrante;
	}

	/**
	 * Obtém as siglas das entidades do tipo {@link IntegracaoSistema}, que pertencem a uma coleção de entidades do tipo
	 * {@link IntegracaoSistemaFuncao}, passada por parâmetro.
	 *
	 * @param listaIntegracaoSistemaFuncao - {@link List}
	 * @return siglas - {@link List}
	 */
	private List<String> agruparSiglasIntegracaoSistema(Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao) {

		List<String> siglas = new ArrayList<>();

		if (!Util.isNullOrEmpty(listaIntegracaoSistemaFuncao)) {

			for (IntegracaoSistemaFuncao integracaoSistemaFuncao : listaIntegracaoSistemaFuncao) {

				String sigla = this.getSiglaIntegracaoSistema(integracaoSistemaFuncao);
				if (!siglas.contains(sigla)) {
					siglas.add(sigla);
				}
			}
		}
		return siglas;
	}

	/**
	 * Obtém o atributo {@code sigla} da entidade {@link IntegracaoSistema}, em {@link IntegracaoSistemaFuncao}.
	 *
	 * @param integracaoSistemaFuncao - {@link IntegracaoSistemaFuncao}
	 * @return sigla - {@link String}
	 */
	private String getSiglaIntegracaoSistema(IntegracaoSistemaFuncao integracaoSistemaFuncao) {

		String sigla = "";
		if (integracaoSistemaFuncao != null && integracaoSistemaFuncao.getIntegracaoSistema() != null) {

			IntegracaoSistema integracaoSistema = integracaoSistemaFuncao.getIntegracaoSistema();

			sigla = integracaoSistema.getSigla();
		}
		return sigla;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#atualizarIntegracaoCliente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void atualizarIntegracaoCliente(Cliente cliente) throws NegocioException, ConcorrenciaException {

		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Collection<IntegracaoCliente> listaIntegracaoCliente = this.consultarIntegracaoCliente(cliente.getChavePrimaria());
		if (listaIntegracaoCliente != null && !listaIntegracaoCliente.isEmpty()) {
			ParametroSistema parametroClienteEndereco =
							ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE);
			ParametroSistema parametroClienteTelefone =
							ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LIMITE_INTEGRACAO_TELEFONE_CLIENTE);
			ParametroSistema parametroClienteContato =
							ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LIMITE_INTEGRACAO_CONTATO_CLIENTE);
			long quantidadeClienteEndereco = Long.parseLong(parametroClienteEndereco.getValor());
			long quantidadeClienteTelefone = Long.parseLong(parametroClienteTelefone.getValor());
			long quantidadeClienteContato = Long.parseLong(parametroClienteContato.getValor());

			Map<String, ConstanteSistema> mapConstantesAtualizar = listarConstantesOperacaoAlteracao();
			Map<String, ConstanteSistema> mapConstantesSemOperacao = listarConstantesReintegrarClientes();
			
			ConstanteSistema constanteAguardandoRetorno = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO);
			ConstanteSistema constanteNaoProcessado = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
			ConstanteSistema constanteErro = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_ERRO);
			
			for (IntegracaoCliente integracaoCliente : listaIntegracaoCliente) {

				IntegracaoSistemaParametro inspClienteEndereco =
								this.obterIntegracaoSistemaParametro(integracaoCliente.getSistemaDestino(), parametroClienteEndereco);
				if (inspClienteEndereco != null) {
					quantidadeClienteEndereco = Long.parseLong(inspClienteEndereco.getValor());
				}
				IntegracaoSistemaParametro inspClienteTelefone =
								this.obterIntegracaoSistemaParametro(integracaoCliente.getSistemaDestino(), parametroClienteTelefone);
				if (inspClienteTelefone != null) {
					quantidadeClienteTelefone = Long.parseLong(inspClienteTelefone.getValor());
				}
				IntegracaoSistemaParametro inspClienteContato =
								this.obterIntegracaoSistemaParametro(integracaoCliente.getSistemaDestino(), parametroClienteContato);
				if (inspClienteContato != null) {
					quantidadeClienteContato = Long.parseLong(inspClienteContato.getValor());
				}

				if (integracaoCliente.getSituacao().equals(constanteAguardandoRetorno.getValor())) {
					throw new NegocioException(ERRO_PROCESSO_INTEGRACAO_AGUARDANDO_RETORNO, true);
				} else {

					if (integracaoCliente.getSituacao().equals(constanteNaoProcessado.getValor())
									|| integracaoCliente.getSituacao().equals(constanteErro.getValor())) {
						integracaoCliente =
										preencherIntegracaoCliente(integracaoCliente, cliente, integracaoCliente.getSistemaDestino(),
												mapConstantesSemOperacao);
					} else {
						integracaoCliente =
										preencherIntegracaoCliente(integracaoCliente, cliente, integracaoCliente.getSistemaDestino(),
												mapConstantesAtualizar);
					}
					super.atualizar(integracaoCliente, IntegracaoClienteImpl.class);

					atualizarIntegracaoClienteEndereco(cliente, integracaoCliente, integracaoCliente.getSistemaDestino(), quantidadeClienteEndereco);

					atualizarIntegracaoClienteTelefone(cliente, integracaoCliente, integracaoCliente.getSistemaDestino(), quantidadeClienteTelefone);

					atualizarIntegracaoClienteContato(cliente, integracaoCliente, integracaoCliente.getSistemaDestino(), quantidadeClienteContato);
				}
			}
		} else {
			this.inserirIntegracaoCliente(cliente);
		}
	}

	/**
	 * Método responsável por atualizar a IntegracaoClienteEndereco da IntegracaoCliente informada.
	 *
	 * @param cliente the cliente
	 * @param constanteSim the constante sim
	 * @param constanteNao the constante nao
	 * @param constanteInclusao the constante inclusao
	 * @param constanteAlteracao the constante alteracao
	 * @param constanteExclusao the constante exclusao
	 * @param constanteNaoProcessado the constante nao processado
	 * @param integracaoCliente the integracao cliente
	 * @param sistemaIntegrante the sistema integrante
	 * @param constanteErro the constante erro
	 * @param quantidadeClienteEndereco the quantidade cliente endereco
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarIntegracaoClienteEndereco(Cliente cliente, IntegracaoCliente integracaoCliente, String sistemaIntegrante,
					long quantidadeClienteEndereco) throws ConcorrenciaException, NegocioException {
		
		Map<String, ConstanteSistema> mapConstantesAtualizar = listarConstantesOperacaoAlteracao();
		Map<String, ConstanteSistema> mapConstantesExclusao = listarConstantesOperacaoRemover();
		Map<String, ConstanteSistema> mapConstantesSemOperacao = listarConstantesReintegrarClientes();
		Map<String, ConstanteSistema> mapConstantesInclusao = listarConstantesOperacaoIncluir();
		
		ConstanteSistema constanteExclusao = mapConstantesExclusao.get(ControladorIntegracaoImpl.OPERACAO);
		ConstanteSistema constanteInclusao = mapConstantesInclusao.get(ControladorIntegracaoImpl.OPERACAO);

		ConstanteSistema constanteNaoProcessado = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema constanteErro = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_ERRO);

		
		Collection<IntegracaoClienteEndereco> listaIntegracaoClienteEndereco =
						this.consultarIntegracaoClienteEndereco(integracaoCliente.getChavePrimaria(), null, null);

		for (IntegracaoClienteEndereco integracaoClienteEndereco : listaIntegracaoClienteEndereco) {
			if (!constanteExclusao.getValor().equals(integracaoClienteEndereco.getOperacao())) {
				ClienteEndereco clienteEndereco =
								existeClienteEndereco(integracaoClienteEndereco.getClienteEndereco(), cliente.getEnderecos());

				if (clienteEndereco != null) {

					if (integracaoClienteEndereco.getSituacao().equals(constanteNaoProcessado.getValor())
									|| integracaoClienteEndereco.getSituacao().equals(constanteErro.getValor())) {

						integracaoClienteEndereco =
										preencherIntegracaoClienteEndereco(integracaoCliente, integracaoClienteEndereco, clienteEndereco,
												mapConstantesSemOperacao, sistemaIntegrante);

					} else {

						integracaoClienteEndereco =
										preencherIntegracaoClienteEndereco(integracaoCliente, integracaoClienteEndereco, clienteEndereco,
														mapConstantesAtualizar, sistemaIntegrante);
					}

					super.atualizar(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);

				} else {

					if (constanteInclusao.getValor().equals(integracaoClienteEndereco.getOperacao())
									&& (constanteNaoProcessado.getValor().equals(integracaoClienteEndereco.getSituacao()) || constanteErro
													.getValor().equals(integracaoClienteEndereco.getSituacao()))) {

						super.remover(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);

					} else {

						integracaoClienteEndereco =
										preencherIntegracaoClienteEndereco(integracaoCliente, integracaoClienteEndereco, null,
														mapConstantesExclusao, sistemaIntegrante);
						super.atualizar(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);
					}
				}
			}
		}

		Collection<IntegracaoClienteEndereco> listaIntegracaoClienteEnderecoAtivosPrincipal =
						this.consultarIntegracaoClienteEndereco(integracaoCliente.getChavePrimaria(), constanteExclusao.getValor(), 1L);

		Collection<IntegracaoClienteEndereco> listaIntegracaoClienteEnderecoAtivos =
						this.consultarIntegracaoClienteEndereco(integracaoCliente.getChavePrimaria(), constanteExclusao.getValor(), null);
		long contadorClienteEndereco = listaIntegracaoClienteEnderecoAtivos.size();

		if ((listaIntegracaoClienteEnderecoAtivosPrincipal == null || listaIntegracaoClienteEnderecoAtivosPrincipal.isEmpty())
						&& contadorClienteEndereco >= quantidadeClienteEndereco && !cliente.getEnderecos().isEmpty()) {
			IntegracaoClienteEndereco integracaoClienteEndereco =
							((List<IntegracaoClienteEndereco>) listaIntegracaoClienteEnderecoAtivos).get(0);
			if (constanteInclusao.getValor().equals(integracaoClienteEndereco.getOperacao())
							&& (constanteNaoProcessado.getValor().equals(integracaoClienteEndereco.getSituacao()) || constanteErro
											.getValor().equals(integracaoClienteEndereco.getSituacao()))) {
				super.remover(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);
			} else {
				integracaoClienteEndereco.setOperacao(constanteExclusao.getValor());
				super.atualizar(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);
			}
			contadorClienteEndereco--;
		}

		if (contadorClienteEndereco < quantidadeClienteEndereco) {
			for (ClienteEndereco clienteEndereco : cliente.getEnderecos()) {
				if (clienteEndereco.getCorrespondencia()
								&& (existeIntegracaoClienteEndereco(clienteEndereco.getChavePrimaria(), listaIntegracaoClienteEndereco) == null || clienteEndereco
												.getChavePrimaria() == 0L)) {
					IntegracaoClienteEndereco integracaoClienteEndereco =
									preencherIntegracaoClienteEndereco(integracaoCliente, null, clienteEndereco, mapConstantesInclusao, sistemaIntegrante);
					super.inserir(integracaoClienteEndereco);
					contadorClienteEndereco++;
					break;
				}
			}

			for (ClienteEndereco clienteEndereco : cliente.getEnderecos()) {
				if (contadorClienteEndereco < quantidadeClienteEndereco) {
					if (!clienteEndereco.getCorrespondencia()
									&& (existeIntegracaoClienteEndereco(clienteEndereco.getChavePrimaria(), listaIntegracaoClienteEndereco) == null || clienteEndereco
													.getChavePrimaria() == 0L)) {
						IntegracaoClienteEndereco integracaoClienteEndereco =
										preencherIntegracaoClienteEndereco(integracaoCliente, null, clienteEndereco, mapConstantesInclusao, sistemaIntegrante);
						super.inserir(integracaoClienteEndereco);
						contadorClienteEndereco++;
					}
				} else {
					break;
				}
			}
		}
	}

	/**
	 * Método responsável por atualizar a IntegracaoClienteTelefone da IntegracaoCliente informada.
	 *
	 * @param cliente the cliente
	 * @param constanteSim the constante sim
	 * @param constanteNao the constante nao
	 * @param constanteInclusao the constante inclusao
	 * @param constanteAlteracao the constante alteracao
	 * @param constanteExclusao the constante exclusao
	 * @param constanteNaoProcessado the constante nao processado
	 * @param integracaoCliente the integracao cliente
	 * @param sistemaIntegrante the sistema integrante
	 * @param constanteErro the constante erro
	 * @param quantidadeClienteTelefone the quantidade cliente telefone
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarIntegracaoClienteTelefone(Cliente cliente, IntegracaoCliente integracaoCliente, String sistemaIntegrante,
					long quantidadeClienteTelefone) throws ConcorrenciaException, NegocioException {

		Map<String, ConstanteSistema> mapConstantesAtualizar = listarConstantesOperacaoAlteracao();
		Map<String, ConstanteSistema> mapConstantesExclusao = listarConstantesOperacaoRemover();
		Map<String, ConstanteSistema> mapConstantesSemOperacao = listarConstantesReintegrarClientes();
		Map<String, ConstanteSistema> mapConstantesInclusao = listarConstantesOperacaoIncluir();
		
		ConstanteSistema constanteExclusao = mapConstantesExclusao.get(ControladorIntegracaoImpl.OPERACAO);
		ConstanteSistema constanteInclusao = mapConstantesInclusao.get(ControladorIntegracaoImpl.OPERACAO);

		ConstanteSistema constanteNaoProcessado = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema constanteErro = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_ERRO);

		
		Collection<IntegracaoClienteTelefone> listaIntegracaoClienteTelefone =
						this.consultarIntegracaoClienteTelefone(integracaoCliente.getChavePrimaria(), null, null);

		for (IntegracaoClienteTelefone integracaoClienteTelefone : listaIntegracaoClienteTelefone) {

			if (!constanteExclusao.getValor().equals(integracaoClienteTelefone.getOperacao())) {

				ClienteFone clienteFone = existeClienteFone(integracaoClienteTelefone.getClienteTelefone(), cliente.getFones());

				if (clienteFone != null) {

					if (integracaoClienteTelefone.getSituacao().equals(constanteNaoProcessado.getValor())
									|| integracaoClienteTelefone.getSituacao().equals(constanteErro.getValor())) {

						integracaoClienteTelefone =
										preencherIntegracaoClienteTelefone(integracaoCliente, integracaoClienteTelefone, clienteFone,
													mapConstantesSemOperacao, sistemaIntegrante);

					} else {

						integracaoClienteTelefone =
										preencherIntegracaoClienteTelefone(integracaoCliente, integracaoClienteTelefone, clienteFone,
													mapConstantesAtualizar, sistemaIntegrante);
					}

					super.atualizar(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);

				} else {

					if (constanteInclusao.getValor().equals(integracaoClienteTelefone.getOperacao())
									&& (constanteNaoProcessado.getValor().equals(integracaoClienteTelefone.getSituacao()) || constanteErro
													.getValor().equals(integracaoClienteTelefone.getSituacao()))) {

						super.remover(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);

					} else {

						integracaoClienteTelefone =
										preencherIntegracaoClienteTelefone(integracaoCliente, integracaoClienteTelefone, null,
													mapConstantesExclusao, sistemaIntegrante);
						super.atualizar(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);
					}
				}
			}
		}

		Collection<IntegracaoClienteTelefone> listaIntegracaoClienteTelefoneAtivosPrincipal =
						this.consultarIntegracaoClienteTelefone(integracaoCliente.getChavePrimaria(), constanteExclusao.getValor(), 1L);

		Collection<IntegracaoClienteTelefone> listaIntegracaoClienteTelefoneAtivos =
						this.consultarIntegracaoClienteTelefone(integracaoCliente.getChavePrimaria(), constanteExclusao.getValor(), null);
		long contadorClienteTelefone = listaIntegracaoClienteTelefoneAtivos.size();

		if ((listaIntegracaoClienteTelefoneAtivosPrincipal == null || listaIntegracaoClienteTelefoneAtivosPrincipal.isEmpty())
						&& contadorClienteTelefone >= quantidadeClienteTelefone && !cliente.getFones().isEmpty()) {
			IntegracaoClienteTelefone integracaoClienteTelefone =
							((List<IntegracaoClienteTelefone>) listaIntegracaoClienteTelefoneAtivos).get(0);
			if (constanteInclusao.getValor().equals(integracaoClienteTelefone.getOperacao())
							&& (constanteNaoProcessado.getValor().equals(integracaoClienteTelefone.getSituacao()) || constanteErro
											.getValor().equals(integracaoClienteTelefone.getSituacao()))) {
				super.remover(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);
			} else {
				integracaoClienteTelefone.setOperacao(constanteExclusao.getValor());
				super.atualizar(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);
			}
			contadorClienteTelefone--;

		}
		if (contadorClienteTelefone < quantidadeClienteTelefone) {
			for (ClienteFone clienteFone : cliente.getFones()) {
				if (clienteFone.getIndicadorPrincipal()
								&& existeIntegracaoClienteTelefone(clienteFone.getChavePrimaria(), listaIntegracaoClienteTelefone) == null) {
					IntegracaoClienteTelefone integracaoClienteTelefone =
									preencherIntegracaoClienteTelefone(integracaoCliente, null, clienteFone, mapConstantesInclusao, 
											sistemaIntegrante);
					super.inserir(integracaoClienteTelefone);
					contadorClienteTelefone++;
					break;
				}
			}
			for (ClienteFone clienteFone : cliente.getFones()) {
				if (contadorClienteTelefone < quantidadeClienteTelefone) {
					if (!clienteFone.getIndicadorPrincipal()
									&& existeIntegracaoClienteTelefone(clienteFone.getChavePrimaria(), listaIntegracaoClienteTelefone) == null) {
						IntegracaoClienteTelefone integracaoClienteTelefone =
										preencherIntegracaoClienteTelefone(integracaoCliente, null, clienteFone, 
												mapConstantesInclusao, sistemaIntegrante);
						super.inserir(integracaoClienteTelefone);
						contadorClienteTelefone++;
					}
				} else {
					break;
				}
			}
		}
	}

	/**
	 * Método responsável por atualizar a IntegracaoClienteContato da IntegracaoCliente informada.
	 *
	 * @param cliente the cliente
	 * @param constanteSim the constante sim
	 * @param constanteNao the constante nao
	 * @param constanteInclusao the constante inclusao
	 * @param constanteAlteracao the constante alteracao
	 * @param constanteExclusao the constante exclusao
	 * @param constanteNaoProcessado the constante nao processado
	 * @param integracaoCliente the integracao cliente
	 * @param sistemaIntegrante the sistema integrante
	 * @param constanteErro the constante erro
	 * @param quantidadeClienteContato the quantidade cliente contato
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarIntegracaoClienteContato(Cliente cliente,IntegracaoCliente integracaoCliente, String sistemaIntegrante,
					long quantidadeClienteContato) throws ConcorrenciaException, NegocioException {

		Map<String, ConstanteSistema> mapConstantesAtualizar = listarConstantesOperacaoAlteracao();
		Map<String, ConstanteSistema> mapConstantesExclusao = listarConstantesOperacaoRemover();
		Map<String, ConstanteSistema> mapConstantesSemOperacao = listarConstantesReintegrarClientes();
		Map<String, ConstanteSistema> mapConstantesInclusao = listarConstantesOperacaoIncluir();
		
		ConstanteSistema constanteExclusao = mapConstantesExclusao.get(ControladorIntegracaoImpl.OPERACAO);
		ConstanteSistema constanteInclusao = mapConstantesInclusao.get(ControladorIntegracaoImpl.OPERACAO);

		ConstanteSistema constanteNaoProcessado = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema constanteErro = mapConstantesAtualizar.get(Constantes.C_INTEGRACAO_SITUACAO_ERRO);

		
		Collection<IntegracaoClienteContato> listaIntegracaoClienteContato =
						this.consultarIntegracaoClienteContato(integracaoCliente.getChavePrimaria(), null, null);
		for (IntegracaoClienteContato integracaoClienteContato : listaIntegracaoClienteContato) {
			if (!constanteExclusao.getValor().equals(integracaoClienteContato.getOperacao())) {

				ContatoCliente contatoCliente = existeContatoCliente(integracaoClienteContato.getClienteContato(), cliente.getContatos());

				if (contatoCliente != null) {
					if (integracaoClienteContato.getSituacao().equals(constanteNaoProcessado.getValor())
									|| integracaoClienteContato.getSituacao().equals(constanteErro.getValor())) {
						integracaoClienteContato =
										preencherIntegracaoClienteContato(integracaoCliente, integracaoClienteContato, contatoCliente,
														mapConstantesSemOperacao, sistemaIntegrante);
					} else {
						integracaoClienteContato =
										preencherIntegracaoClienteContato(integracaoCliente, integracaoClienteContato, contatoCliente,
													mapConstantesAtualizar, sistemaIntegrante);
						super.atualizar(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
					}
				} else {
					if (constanteInclusao.getValor().equals(integracaoClienteContato.getOperacao())
									&& (constanteNaoProcessado.getValor().equals(integracaoClienteContato.getSituacao()) || constanteErro
													.getValor().equals(integracaoClienteContato.getSituacao()))) {
						super.remover(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
					} else {
						integracaoClienteContato =
										preencherIntegracaoClienteContato(integracaoCliente, integracaoClienteContato, null,
												mapConstantesExclusao, sistemaIntegrante);
						super.atualizar(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
					}
				}
			}
		}

		Collection<IntegracaoClienteContato> listaIntegracaoClienteContatoAtivosPrincipal =
						this.consultarIntegracaoClienteContato(integracaoCliente.getChavePrimaria(), constanteExclusao.getValor(), 1L);
		Collection<IntegracaoClienteContato> listaIntegracaoClienteContatoAtivos =
						this.consultarIntegracaoClienteContato(integracaoCliente.getChavePrimaria(), constanteExclusao.getValor(), null);
		long contadorClienteContato = listaIntegracaoClienteContatoAtivos.size();

		if ((listaIntegracaoClienteContatoAtivosPrincipal == null || listaIntegracaoClienteContatoAtivosPrincipal.isEmpty())
						&& contadorClienteContato >= quantidadeClienteContato && !cliente.getContatos().isEmpty()) {
			IntegracaoClienteContato integracaoClienteContato =
							((List<IntegracaoClienteContato>) listaIntegracaoClienteContatoAtivos).get(0);
			if (constanteInclusao.getValor().equals(integracaoClienteContato.getOperacao())
							&& (constanteNaoProcessado.getValor().equals(integracaoClienteContato.getSituacao()) || constanteErro
											.getValor().equals(integracaoClienteContato.getSituacao()))) {
				super.remover(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
			} else {
				integracaoClienteContato.setOperacao(constanteExclusao.getValor());
				super.atualizar(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
			}
			contadorClienteContato--;

		}
		if (contadorClienteContato < quantidadeClienteContato) {
			for (ContatoCliente contatoCliente : cliente.getContatos()) {
				if (contatoCliente.isPrincipal()
								&& existeIntegracaoClienteContato(contatoCliente.getChavePrimaria(), listaIntegracaoClienteContato) == null) {
					IntegracaoClienteContato integracaoClienteContato =
									preencherIntegracaoClienteContato(integracaoCliente, null, contatoCliente, mapConstantesInclusao, sistemaIntegrante);
					super.inserir(integracaoClienteContato);
					contadorClienteContato++;
					break;
				}
			}
			for (ContatoCliente contatoCliente : cliente.getContatos()) {
				if (contadorClienteContato < quantidadeClienteContato) {
					if (!contatoCliente.isPrincipal()
									&& existeIntegracaoClienteContato(contatoCliente.getChavePrimaria(), listaIntegracaoClienteContato) == null) {
						IntegracaoClienteContato integracaoClienteContato =
										preencherIntegracaoClienteContato(integracaoCliente, null, contatoCliente, 
												mapConstantesInclusao, sistemaIntegrante);
						super.inserir(integracaoClienteContato);
						contadorClienteContato++;
					}
				} else {
					break;
				}
			}
		}
	}

	/**
	 * Método responsável por retornar se houver um clienteEndereco na lista de cliente.
	 *
	 * @param chavePrimariaClienteEndereco
	 *            the chave primaria cliente endereco
	 * @param enderecos
	 *            the enderecos
	 * @return retorno
	 */
	private ClienteEndereco existeClienteEndereco(Long chavePrimariaClienteEndereco, Collection<ClienteEndereco> enderecos) {

		ClienteEndereco retorno = null;
		for (ClienteEndereco clienteEndereco : enderecos) {
			if (chavePrimariaClienteEndereco.equals(clienteEndereco.getChavePrimaria())) {
				retorno = clienteEndereco;
				break;
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por retornar se houver um integracaoClienteEndereco na lista de integracaoCliente.
	 *
	 * @param chavePrimariaClienteEndereco
	 *            the chave primaria cliente endereco
	 * @param listaIntegracaoClienteEndereco
	 *            the lista integracao cliente endereco
	 * @return retorno
	 */
	private IntegracaoClienteEndereco existeIntegracaoClienteEndereco(Long chavePrimariaClienteEndereco,
					Collection<IntegracaoClienteEndereco> listaIntegracaoClienteEndereco) {

		IntegracaoClienteEndereco retorno = null;
		for (IntegracaoClienteEndereco integracaoClienteEndereco : listaIntegracaoClienteEndereco) {
			if (chavePrimariaClienteEndereco.equals(integracaoClienteEndereco.getClienteEndereco())) {
				retorno = integracaoClienteEndereco;
				break;
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por retornar se houver um clienteFone na lista de cliente.
	 *
	 * @param chavePrimariaClienteFone
	 *            the chave primaria cliente fone
	 * @param fones
	 *            the fones
	 * @return retorno
	 */
	private ClienteFone existeClienteFone(Long chavePrimariaClienteFone, Collection<ClienteFone> fones) {

		ClienteFone retorno = null;
		for (ClienteFone clienteFone : fones) {
			if (chavePrimariaClienteFone.equals(clienteFone.getChavePrimaria())) {
				retorno = clienteFone;
				break;
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por retornar se houver um integracaoClienteTelefone na lista de integracaoCliente.
	 *
	 * @param chavePrimariaClienteTelefone
	 *            the chave primaria cliente telefone
	 * @param listaIntegracaoClienteTelefone
	 *            the lista integracao cliente telefone
	 * @return retorno
	 */
	private IntegracaoClienteTelefone existeIntegracaoClienteTelefone(Long chavePrimariaClienteTelefone,
					Collection<IntegracaoClienteTelefone> listaIntegracaoClienteTelefone) {

		IntegracaoClienteTelefone retorno = null;
		for (IntegracaoClienteTelefone integracaoClienteTelefone : listaIntegracaoClienteTelefone) {
			if (chavePrimariaClienteTelefone.equals(integracaoClienteTelefone.getClienteTelefone())) {
				retorno = integracaoClienteTelefone;
				break;
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por retornar se houver um contatoCliente na lista de cliente.
	 *
	 * @param chavePrimariaContatoCliente
	 *            the chave primaria contato cliente
	 * @param contatos
	 *            the contatos
	 * @return retorno
	 */
	private ContatoCliente existeContatoCliente(Long chavePrimariaContatoCliente, Collection<ContatoCliente> contatos) {

		ContatoCliente retorno = null;
		for (ContatoCliente contatoCliente : contatos) {
			if (chavePrimariaContatoCliente.equals(contatoCliente.getChavePrimaria())) {
				retorno = contatoCliente;
				break;
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por retornar se houver um integracaoClienteContato na lista de integracaoCliente.
	 *
	 * @param chavePrimariaClienteContato
	 *            the chave primaria cliente contato
	 * @param listaIntegracaoClienteContato
	 *            the lista integracao cliente contato
	 * @return retorno
	 */
	private IntegracaoClienteContato existeIntegracaoClienteContato(Long chavePrimariaClienteContato,
					Collection<IntegracaoClienteContato> listaIntegracaoClienteContato) {

		IntegracaoClienteContato retorno = null;
		for (IntegracaoClienteContato integracaoClienteContato : listaIntegracaoClienteContato) {
			if (chavePrimariaClienteContato.equals(integracaoClienteContato.getClienteContato())) {
				retorno = integracaoClienteContato;
				break;
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#removerIntegracaoCliente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void removerIntegracaoCliente(Cliente cliente) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();

		Collection<IntegracaoCliente> listaIntegracaoCliente = this.consultarIntegracaoCliente(cliente.getChavePrimaria());
		if (listaIntegracaoCliente != null && !listaIntegracaoCliente.isEmpty()) {
			Map<String, ConstanteSistema> mapConstantes = listarConstantesOperacaoRemover();
			
			ConstanteSistema constanteInclusao = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
			ConstanteSistema constanteAguardandoRetorno = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO);
			ConstanteSistema constanteErro = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_ERRO);
			ConstanteSistema constanteNaoProcessado = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
			
			for (IntegracaoCliente integracaoCliente : listaIntegracaoCliente) {

				if (integracaoCliente.getSituacao().equals(constanteAguardandoRetorno.getValor())) {
					throw new NegocioException(ERRO_PROCESSO_INTEGRACAO_AGUARDANDO_RETORNO, true);
				} else {

					Collection<IntegracaoClienteEndereco> listaIntegracaoClienteEndereco =
									this.consultarIntegracaoClienteEndereco(integracaoCliente.getChavePrimaria(), null, null);
					for (IntegracaoClienteEndereco integracaoClienteEndereco : listaIntegracaoClienteEndereco) {
						if (constanteInclusao.getValor().equals(integracaoClienteEndereco.getOperacao())
										&& (constanteNaoProcessado.getValor().equals(integracaoClienteEndereco.getSituacao()) || constanteErro
														.getValor().equals(integracaoClienteEndereco.getSituacao()))) {
							super.remover(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);
						} else {
							integracaoClienteEndereco =
											preencherIntegracaoClienteEndereco(integracaoCliente, integracaoClienteEndereco, null,
															mapConstantes, integracaoCliente.getSistemaDestino());
							super.atualizar(integracaoClienteEndereco, IntegracaoClienteEnderecoImpl.class);
						}
					}
					Collection<IntegracaoClienteTelefone> listaIntegracaoClienteTelefone =
									this.consultarIntegracaoClienteTelefone(integracaoCliente.getChavePrimaria(), null, null);
					for (IntegracaoClienteTelefone integracaoClienteTelefone : listaIntegracaoClienteTelefone) {
						if (constanteInclusao.getValor().equals(integracaoClienteTelefone.getOperacao())
										&& (constanteNaoProcessado.getValor().equals(integracaoClienteTelefone.getSituacao()) || constanteErro
														.getValor().equals(integracaoClienteTelefone.getSituacao()))) {
							super.remover(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);
						} else {
							integracaoClienteTelefone =
											preencherIntegracaoClienteTelefone(integracaoCliente, integracaoClienteTelefone, null,
															mapConstantes, integracaoCliente.getSistemaDestino());

							super.atualizar(integracaoClienteTelefone, IntegracaoClienteTelefoneImpl.class);
						}
					}
					Collection<IntegracaoClienteContato> listaIntegracaoClienteContato =
									this.consultarIntegracaoClienteContato(integracaoCliente.getChavePrimaria(), null, null);
					for (IntegracaoClienteContato integracaoClienteContato : listaIntegracaoClienteContato) {
						if (constanteInclusao.getValor().equals(integracaoClienteContato.getOperacao())
										&& (constanteNaoProcessado.getValor().equals(integracaoClienteContato.getSituacao()) || constanteErro
														.getValor().equals(integracaoClienteContato.getSituacao()))) {
							super.remover(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
						} else {
							integracaoClienteContato =
											preencherIntegracaoClienteContato(integracaoCliente, integracaoClienteContato, null,
															mapConstantes, integracaoCliente.getSistemaDestino());
							super.atualizar(integracaoClienteContato, IntegracaoClienteContatoImpl.class);
						}
					}

					if (constanteInclusao.getValor().equals(integracaoCliente.getOperacao())
									&& (constanteNaoProcessado.getValor().equals(integracaoCliente.getSituacao()) || constanteErro
													.getValor().equals(integracaoCliente.getSituacao()))) {
						super.remover(integracaoCliente, IntegracaoClienteImpl.class);
					} else {
						
						
						integracaoCliente =
										preencherIntegracaoCliente(integracaoCliente, cliente, integracaoCliente.getSistemaDestino(),
														mapConstantes);
						super.atualizar(integracaoCliente, IntegracaoClienteImpl.class);
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#inserirIntegracaoCliente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public Long inserirIntegracaoCliente(Cliente cliente) throws NegocioException {

		Long chavePrimaria = cliente.getChavePrimaria();

		reintegrarCliente(cliente);

		return chavePrimaria;
	}

	@Override
	public void atualizarPontoConsumoIntegracaoContrato(PontoConsumo pontoConsumo) throws NegocioException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteContrato = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_CONTRATOS);
		Collection<IntegracaoSistema> consultarIntegracaoSistemaFuncao =
				this.consultarIntegracaoSistemaFuncao(Long.valueOf(constanteContrato.getValor()));
		if (!consultarIntegracaoSistemaFuncao.isEmpty()) {
			ConstanteSistema constanteProcessado =
					ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
			if (existeIntegracaoContratoNaoProcessadoParaPontoConsumo(pontoConsumo, constanteProcessado)) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_NAO_PROCESSADA, true);
			} else {
				ConstanteSistema constanteNaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
				ConstanteSistema constanteAlteracao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);
				ConstanteSistema constanteExclusao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
				persistirAtualizacaoPontoConsumoIntegracaoContrato(pontoConsumo, constanteNaoProcessado, constanteAlteracao,
						constanteExclusao);
			}
		}
	}

	/**
	 * Verifica se existe registro em integração contrato com status diferente de processado para o ponto de consumo informado
	 * @param pontoConsumo O ponto de consumo alterado
	 * @param constanteProcessado A constante representando a situação Processado
	 * @return True se o registro existe e false caso contrário
	 */
	@SuppressWarnings("squid:S1192")
	private boolean existeIntegracaoContratoNaoProcessadoParaPontoConsumo(PontoConsumo pontoConsumo, ConstanteSistema constanteProcessado) {
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" as integracaoContrato ");
		hql.append(" inner join fetch integracaoContrato.contratoPontoConsumo contratoPontoConsumo");
		hql.append(" where contratoPontoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo");
		hql.append(" and integracaoContrato.situacao <> :situacao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setString("situacao", constanteProcessado.getValor());

		return !query.list().isEmpty();
	}

	/**
	 * Atualiza a integracao Contrato com os dados do ponto de consumo
	 * @param pontoConsumo O ponto de consumo alterado
	 * @param constanteNaoProcessado A constante da situaçao Não processado
	 * @param constanteOperacaoAlteracao A constante da operação Alteração
	 */
	@SuppressWarnings("squid:S1192")
	private void persistirAtualizacaoPontoConsumoIntegracaoContrato(PontoConsumo pontoConsumo, ConstanteSistema constanteNaoProcessado,
			ConstanteSistema constanteOperacaoAlteracao, ConstanteSistema constanteOperacaoExclusao) throws NegocioException {
		StringBuilder hql = new StringBuilder();
		hql.append(" update ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" as ic ");
		hql.append(" set ic.pontoConsumo = :pontoConsumo, ic.logradouroFaturamento = :logradouroFaturamento");
		hql.append(" , ic.numeroEnderecoFaturamento = :numeroEnderecoFaturamento, ic.codPaisFaturamento = :codPaisFaturamento");
		hql.append(" , ic.complementoEnderecoFaturamento = :complementoEnderecoFaturamento, ic.bairroFaturamento = :bairroFaturamento");
		hql.append(" , ic.cepFaturamento = :cepFaturamento, ic.codMunicipioFaturamento = :codMunicipioFaturamento");
		hql.append(" , ic.municipioFaturamento = :municipioFaturamento, ic.siglaEstadoFaturamento = :siglaEstadoFaturamento");
		hql.append(" , situacao = :situacao, operacao = :operacao");
		hql.append(" where ic.contratoPontoConsumo.chavePrimaria in (");
		hql.append(" select chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" as cpc where cpc.pontoConsumo.chavePrimaria = :idPontoConsumo )");
		hql.append(" and ic.operacao <> :operacaoE");


		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("pontoConsumo", pontoConsumo.getDescricao());
		query.setString("logradouroFaturamento", pontoConsumo.getEnderecoLogradouro());
		query.setString("numeroEnderecoFaturamento", pontoConsumo.getNumeroImovel());
		UnidadeFederacaoImpl unidadeFederacaoFaturamento = (UnidadeFederacaoImpl) Fachada.getInstancia().obterEntidadePorClasse(
				pontoConsumo.getCep().getMunicipio().getUnidadeFederacao().getChavePrimaria(), UnidadeFederacaoImpl.class);
		query.setLong("codPaisFaturamento", unidadeFederacaoFaturamento.getPais().getCodigoBacen());
		query.setString("complementoEnderecoFaturamento", pontoConsumo.getDescricaoComplemento());
		query.setString("bairroFaturamento", pontoConsumo.getCep().getBairro());
		query.setLong("cepFaturamento", pontoConsumo.getCep().getChavePrimaria());
		query.setLong("codMunicipioFaturamento", pontoConsumo.getCep().getMunicipio().getUnidadeFederativaIBGE());
		query.setString("municipioFaturamento", pontoConsumo.getCep().getNomeMunicipio());
		query.setString("siglaEstadoFaturamento", pontoConsumo.getCep().getMunicipio().getUnidadeFederacao().getSigla());

		query.setString("situacao", constanteNaoProcessado.getValor());
		query.setString("operacao", constanteOperacaoAlteracao.getValor());
		query.setString("operacaoE", constanteOperacaoExclusao.getValor());
		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.executeUpdate();
	}

	@Override
	public void atualizarClienteIntegracaoContrato(Cliente cliente) throws NegocioException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteContrato = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_CONTRATOS);
		Collection<IntegracaoSistema> consultarIntegracaoSistemaFuncao =
				this.consultarIntegracaoSistemaFuncao(Long.valueOf(constanteContrato.getValor()));
		if (!consultarIntegracaoSistemaFuncao.isEmpty()) {
			ConstanteSistema constanteProcessado =
					ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
			if (existeIntegracaoContratoNaoProcessadoParaCliente(cliente, constanteProcessado)) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_NAO_PROCESSADA, true);
			} else {
				ConstanteSistema constanteNaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
				ConstanteSistema constanteAlteracao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);
				ConstanteSistema constanteExclusao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
				persistirAtualizacaoClienteIntegracaoContrato(cliente, constanteNaoProcessado, constanteAlteracao, constanteExclusao);
			}
		}
	}

	/**
	 * Verifica se existe alguma integracao que não está processada para um determinado cliente
	 * @param cliente O cliente que se deseja verificar
	 * @param constanteProcessada A constante representando a situação "Processada"
	 * @return True se existe integracao com situação diferente de "Processada" para esse cliente, false caso contrário
	 */
	@SuppressWarnings("squid:S1192")
	private boolean existeIntegracaoContratoNaoProcessadoParaCliente(Cliente cliente, ConstanteSistema constanteProcessada) {
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" where ");
		hql.append(" chaveCliente = :idCliente and situacao <> :situacao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCliente", cliente.getChavePrimaria());
		query.setString("situacao", constanteProcessada.getValor());

		return !query.list().isEmpty();
	}

	/**
	 * Atualiza a integracao Contrato com os dados do cliente
	 * @param cliente O cliente alterado
	 * @param constanteNaoProcessado A constante que representa a situacação "Não Processada"
	 * @param constanteOperacaoAlteracao A constante que representa a operação de alteração
	 */
	@SuppressWarnings("squid:S1192")
	private void persistirAtualizacaoClienteIntegracaoContrato(Cliente cliente, ConstanteSistema constanteNaoProcessado,
			ConstanteSistema constanteOperacaoAlteracao, ConstanteSistema constanteOperacaoExclusao) {
		StringBuilder hql = new StringBuilder();
		hql.append(" update ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" set tipoCliente = :tipo, nomeCliente = :nome, cpfCliente = :cpf, cnpjCliente = :cnpj");
		hql.append(" , inscricaoEstadualCliente = :ie, emailCliente = :email");
		hql.append(" , situacao = :situacao, operacao = :operacao");
		hql.append(" , ultimaAlteracao = :ultimaAlteracao");
		if (!cliente.getFones().isEmpty()) {
			hql.append(" , foneCliente = :fone, dddCliente = :ddd");
		}
		hql.append(" where chaveCliente = :idCliente and operacao <> :operacaoE");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("tipo", cliente.getTipoCliente().getChavePrimaria());
		query.setString("nome", cliente.getNome());
		query.setString("cpf", cliente.getCpf());
		query.setString("cnpj", cliente.getCnpj());

		if (!cliente.getFones().isEmpty()) {
			ClienteFone clienteFone = cliente.getFones().iterator().next();
			query.setInteger("ddd", clienteFone.getCodigoDDD());
			query.setInteger("fone", clienteFone.getNumero());
		}

		query.setString("ie", cliente.getInscricaoEstadual());
		query.setString("email", cliente.getEmailPrincipal());
		query.setString("situacao", constanteNaoProcessado.getValor());
		query.setString("operacao", constanteOperacaoAlteracao.getValor());
		query.setString("operacaoE", constanteOperacaoExclusao.getValor());
		query.setLong("idCliente", cliente.getChavePrimaria());
		query.setTimestamp("ultimaAlteracao", new Date());

		query.executeUpdate();
	}

	@Override
	public void inserirIntegracaoContrato(Contrato contrato) throws GGASException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteInclusao = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
		inserirRegistroIntegracaoContrato(constanteInclusao, new ArrayList<>(contrato.getListaContratoPontoConsumo()));
	}

	@Override
	public void excluirIntegracaoContratoPorPontoConsumo(Contrato contrato, List<ContratoPontoConsumo> pontosRemovidos)
			throws GGASException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteExclusao = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
		inserirRegistroIntegracaoContrato(constanteExclusao, pontosRemovidos);
	}

	@Override
	public void alterarTitularDeContratoAtivo(Contrato contrato, Contrato contratoAntigo) throws GGASException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteInclusao = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
		ConstanteSistema constanteExclusao = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
		List<ContratoPontoConsumo> pontos = new ArrayList<>(contrato.getListaContratoPontoConsumo());
		List<ContratoPontoConsumo> pontosAntigos = new ArrayList<>(contratoAntigo.getListaContratoPontoConsumo());
		inserirRegistroIntegracaoContrato(constanteExclusao, pontosAntigos);
		inserirRegistroIntegracaoContrato(constanteInclusao, pontos);
	}

	/**
	 * Registra todos os integracao contrato gerados a partir de uma lista de ContratoPontoConsumo
	 * @param operacao A operação de integração
	 * @param pontos A lista dos ContratoPontoConsumo
	 * @throws GGASException o GGASException
	 */
	private void inserirRegistroIntegracaoContrato(ConstanteSistema operacao, List<ContratoPontoConsumo> pontos) throws GGASException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteContrato = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_CONTRATOS);
		Collection<IntegracaoSistema> consultarIntegracaoSistemaFuncao =
				this.consultarIntegracaoSistemaFuncao(Long.valueOf(constanteContrato.getValor()));
		for (IntegracaoSistema sistemaIntegrante : consultarIntegracaoSistemaFuncao) {
			ArrayList<IntegracaoContrato> listaIntegracaoContrato = new ArrayList<>();
			for (ContratoPontoConsumo contratoPontoConsumo : pontos) {
				IntegracaoContrato integracaoContrato = preencherIntegracaoContrato(contratoPontoConsumo,
						sistemaIntegrante.getSigla().toUpperCase(), operacao);
				listaIntegracaoContrato.add(integracaoContrato);
			}
			for (IntegracaoContrato integracaoContrato : listaIntegracaoContrato) {
				persistirIntegracaoContrato(ctlConstanteSistema, integracaoContrato);
			}
		}
	}

	/**
	 * Persiste o objeto integracao contrato
	 * @param ctlConstanteSistema O controlador de constantes
	 * @param integracaoContrato O objeto IntegracaoContrato
	 * @throws ConcorrenciaException o ConcorrenciaException
	 * @throws NegocioException o NegocioException
	 */
	private void persistirIntegracaoContrato(ControladorConstanteSistema ctlConstanteSistema, IntegracaoContrato integracaoContrato)
			throws ConcorrenciaException, NegocioException {
		IntegracaoContrato integracaoExistente = obterIntegracaoContrato(integracaoContrato.getContratoOrigem(),
				integracaoContrato.getPontoConsumoObj());
		if (integracaoExistente == null) {
			super.inserir(integracaoContrato);
		} else {
			ConstanteSistema constanteProcessado =
					ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
			if (!constanteProcessado.getValor().equals(integracaoExistente.getSituacao())) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_NAO_PROCESSADA, true);
			}
			if (Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO.equals(integracaoContrato.getConstanteOperacao().getNome())) {
				ConstanteSistema constanteAlteracao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);
				integracaoContrato.setConstanteOperacao(constanteAlteracao);
				integracaoContrato.setOperacao(constanteAlteracao.getValor());
			}
			integracaoContrato.setChavePrimaria(integracaoExistente.getChavePrimaria());
			integracaoContrato.setContratoPontoConsumo(integracaoExistente.getContratoPontoConsumo());
			super.atualizar(integracaoContrato);
		}
	}

	/**
	 * Cria um objeto IntegracaoContrato a partir dos dados do contratoPontoConsumo
	 * @param contratoPontoConsumo o ContratoPontoConsumo
	 * @param sistemaIntegrante O sistemaIntegrante
	 * @param constanteOperacao A operação de integração
	 * @return O objeto IntegracaoContrato
	 * @throws GGASException O GGASException
	 */
	private IntegracaoContrato preencherIntegracaoContrato(ContratoPontoConsumo contratoPontoConsumo,
			String sistemaIntegrante, ConstanteSistema constanteOperacao) throws GGASException {
		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteIntegracaoSistemaGGAS = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema constanteNaoProcessado =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		IntegracaoContrato integracaoContrato = this.criarIntegracaoContrato();
		PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
		Contrato contrato = Fachada.getInstancia().obterContrato(contratoPontoConsumo.getContrato().getChavePrimaria());
		Cliente cliente = Fachada.getInstancia().obterCliente(contrato.getClienteAssinatura().getChavePrimaria());
		contratoPontoConsumo = getControladorContrato().obterContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());

		// Dados gerais
		integracaoContrato.setSistemaOrigem(constanteIntegracaoSistemaGGAS.getValor());
		integracaoContrato.setSistemaDestino(sistemaIntegrante);
		integracaoContrato.setContratoOrigem(contrato);
		integracaoContrato.setContratoPaiOrigem(contrato.getChavePrimariaPai());
		integracaoContrato.setContratoPontoConsumo(contratoPontoConsumo);
		integracaoContrato.setConstanteOperacao(constanteOperacao);
		integracaoContrato.setPontoConsumoObj(contratoPontoConsumo.getPontoConsumo());
		if (constanteOperacao != null && constanteOperacao.getValor() != null) {
			integracaoContrato.setOperacao(constanteOperacao.getValor());
		}
		integracaoContrato.setDescricaoErro(null);
		integracaoContrato.setSituacao(constanteNaoProcessado.getValor());
		integracaoContrato.setSistemaOrigem(constanteIntegracaoSistemaGGAS.getValor());
		integracaoContrato.setSegmento(Optional.ofNullable(pontoConsumo.getSegmento()).map(Segmento::getChavePrimaria).orElse(null));
		// Dados cobrança
		integracaoContrato.setCodMunicipioCobranca(contratoPontoConsumo.getCep().getMunicipio().getUnidadeFederativaIBGE());
		integracaoContrato.setMunicipioCobranca(contratoPontoConsumo.getCep().getMunicipio().getDescricao());
		integracaoContrato.setLogradouroCobranca(contratoPontoConsumo.getCep().getTipoLogradouro() + " "
				+ contratoPontoConsumo.getCep().getLogradouro());
		integracaoContrato.setNumeroEnderecoCobranca(contratoPontoConsumo.getNumeroImovel());
		integracaoContrato.setComplementoEnderecoCobranca(contratoPontoConsumo.getComplementoEndereco());
		integracaoContrato.setBairroCobranca(contratoPontoConsumo.getCep().getBairro());
		integracaoContrato.setMunicipioCobranca(contratoPontoConsumo.getCep().getNomeMunicipio());
		integracaoContrato.setSiglaEstadoCobranca(contratoPontoConsumo.getCep().getMunicipio().getUnidadeFederacao().getSigla());
		UnidadeFederacaoImpl unidadeFederacaoCobranca = (UnidadeFederacaoImpl) Fachada.getInstancia().obterEntidadePorClasse(
				contratoPontoConsumo.getCep().getMunicipio().getUnidadeFederacao().getChavePrimaria(), UnidadeFederacaoImpl.class);
		integracaoContrato.setCodPaisCobranca(unidadeFederacaoCobranca.getPais().getCodigoBacen());
		// Dados faturamento
		integracaoContrato.setPontoConsumo(pontoConsumo.getDescricao());
		integracaoContrato.setLogradouroFaturamento(pontoConsumo.getCep().getTipoLogradouro() + " "
				+ pontoConsumo.getCep().getLogradouro());
		integracaoContrato.setCodMunicipioFaturamento(pontoConsumo.getCep().getMunicipio().getUnidadeFederativaIBGE());
		integracaoContrato.setMunicipioFaturamento(pontoConsumo.getCep().getNomeMunicipio());
		integracaoContrato.setNumeroEnderecoFaturamento(pontoConsumo.getNumeroImovel());
		integracaoContrato.setComplementoEnderecoFaturamento(pontoConsumo.getDescricaoComplemento());
		integracaoContrato.setBairroFaturamento(pontoConsumo.getCep().getBairro());
		integracaoContrato.setCepFaturamento(pontoConsumo.getCep().getChavePrimaria());
		integracaoContrato.setSiglaEstadoFaturamento(pontoConsumo.getCep().getMunicipio().getUnidadeFederacao().getSigla());
		UnidadeFederacaoImpl unidadeFederacaoFaturamento = (UnidadeFederacaoImpl) Fachada.getInstancia().obterEntidadePorClasse(
				pontoConsumo.getCep().getMunicipio().getUnidadeFederacao().getChavePrimaria(), UnidadeFederacaoImpl.class);
		integracaoContrato.setCodPaisFaturamento(unidadeFederacaoFaturamento.getPais().getCodigoBacen());
		// Dados Cliente
		integracaoContrato.setChaveCliente(cliente.getChavePrimaria());
		integracaoContrato.setTipoCliente(cliente.getTipoCliente().getChavePrimaria());
		integracaoContrato.setNomeCliente(cliente.getNome());
		integracaoContrato.setCpfCliente(cliente.getCpf());
		integracaoContrato.setCnpjCliente(cliente.getCnpj());
		if (!cliente.getFones().isEmpty()) {
			ClienteFone fone = cliente.getFones().iterator().next();
			integracaoContrato.setDddCliente(fone.getCodigoDDD());
			integracaoContrato.setFoneCliente(fone.getNumero());
		}
		integracaoContrato.setInscricaoEstadualCliente(cliente.getInscricaoEstadual());
		integracaoContrato.setEmailCliente(cliente.getEmailPrincipal());
		// Dados bancários
		integracaoContrato.setNumeroBancoCliente(Optional.ofNullable(contrato.getBanco())
				.map(Banco::getCodigoBanco).orElse(null));
		integracaoContrato.setAgenciaCliente(contrato.getAgencia());
		integracaoContrato.setContaCorrenteCliente(contrato.getContaCorrente());
		integracaoContrato.setTipoConvenioCobrancaCliente(contrato.getFormaCobranca().getDescricao());

		return integracaoContrato;
	}

	/**
	 * Método responsável por inserir os contatos do cliente na integração IntegracaoClienteContato.
	 *
	 * @param cliente
	 *            the cliente
	 * @param quantidadeClienteContato
	 *            the quantidade cliente contato
	 * @param constanteSim
	 *            the constante sim
	 * @param constanteNao
	 *            the constante nao
	 * @param constanteInclusao
	 *            the constante inclusao
	 * @param constanteNaoProcessado
	 *            the constante nao processado
	 * @param sistemaIntegrante
	 *            the sistema integrante
	 * @param integracaoCliente
	 *            the integracao cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirIntegracaoClienteContato(Cliente cliente, long quantidadeClienteContato, Map<String, ConstanteSistema> mapConstantes, 
					String sistemaIntegrante, IntegracaoCliente integracaoCliente) throws NegocioException {

		if (quantidadeClienteContato > 0) {
			long contadorClienteContato = 0;
			for (ContatoCliente contatoCliente : cliente.getContatos()) {
				if (contatoCliente.isPrincipal()) {
					IntegracaoClienteContato integracaoClienteContato =
									preencherIntegracaoClienteContato(integracaoCliente, null, contatoCliente, mapConstantes, sistemaIntegrante);
					super.inserir(integracaoClienteContato);
					contadorClienteContato++;
					break;
				}
			}
			for (ContatoCliente contatoCliente : cliente.getContatos()) {
				if (contadorClienteContato < quantidadeClienteContato) {
					if (!contatoCliente.isPrincipal()) {
						IntegracaoClienteContato integracaoClienteContato =
										preencherIntegracaoClienteContato(integracaoCliente, null, contatoCliente, mapConstantes, sistemaIntegrante);
						super.inserir(integracaoClienteContato);
						contadorClienteContato++;
					}
				} else {
					break;
				}
			}
		}
	}

	/**
	 * Método responsável por inserir os telefones do cliente na integração IntegracaoClienteTelefone.
	 *
	 * @param cliente
	 *            the cliente
	 * @param quantidadeClienteTelefone
	 *            the quantidade cliente telefone
	 * @param constanteSim
	 *            the constante sim
	 * @param constanteNao
	 *            the constante nao
	 * @param constanteInclusao
	 *            the constante inclusao
	 * @param constanteNaoProcessado
	 *            the constante nao processado
	 * @param sistemaIntegrante
	 *            the sistema integrante
	 * @param integracaoCliente
	 *            the integracao cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirIntegracaoClienteTelefone(Cliente cliente, long quantidadeClienteTelefone,
			Map<String, ConstanteSistema> mapConstantes, String sistemaIntegrante, IntegracaoCliente integracaoCliente)
			throws NegocioException {

		if (quantidadeClienteTelefone > 0) {
			long contadorClienteTelefone = 0;
			for (ClienteFone clienteFone : cliente.getFones()) {
				if (clienteFone.getIndicadorPrincipal()) {
					IntegracaoClienteTelefone integracaoClienteTelefone =
							preencherIntegracaoClienteTelefone(integracaoCliente, null, clienteFone, mapConstantes, sistemaIntegrante);
					super.inserir(integracaoClienteTelefone);
					contadorClienteTelefone++;
					break;
				}
			}
			for (ClienteFone clienteFone : cliente.getFones()) {
				if (contadorClienteTelefone < quantidadeClienteTelefone) {
					if (!clienteFone.getIndicadorPrincipal()) {
						IntegracaoClienteTelefone integracaoClienteTelefone =
								preencherIntegracaoClienteTelefone(integracaoCliente, null, clienteFone, mapConstantes, sistemaIntegrante);
						super.inserir(integracaoClienteTelefone);
						contadorClienteTelefone++;
					}
				} else {
					break;
				}
			}
		}
	}

	/**
	 * Método responsável por inserir os endereços do cliente na integração IntegracaoClienteEndereco.
	 *
	 * @param cliente
	 *            the cliente
	 * @param quantidadeMaximaClienteEnderecoTmp
	 *            the quantidade cliente endereco
	 * @param constanteSim
	 *            the constante sim
	 * @param constanteNao
	 *            the constante nao
	 * @param constanteInclusao
	 *            the constante inclusao
	 * @param constanteNaoProcessado
	 *            the constante nao processado
	 * @param sistemaIntegrante
	 *            the sistema integrante
	 * @param integracaoCliente
	 *            the integracao cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirIntegracaoClienteEndereco(Cliente cliente, long quantidadeMaximaClienteEnderecoTmp,
			Map<String, ConstanteSistema> mapConstantes, IntegracaoSistema sistemaIntegrante, IntegracaoCliente integracaoCliente)
			throws NegocioException {

		long quantidadeMaximaClienteEndereco = quantidadeMaximaClienteEnderecoTmp;
		ControladorIntegracao controladorIntegracao = ServiceLocator.getInstancia().getControladorIntegracao();

		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ParametroSistema parametroIntegracaoLimiteEnderecoCliente = ctlParametroSistema.obterParametroIntegracaoLimiteEnderecoCliente();

		IntegracaoSistemaParametro integracaoSistemaParametro =
				controladorIntegracao.obterIntegracaoSistemaParametro(sistemaIntegrante, parametroIntegracaoLimiteEnderecoCliente);

		quantidadeMaximaClienteEndereco = integracaoSistemaParametro.getValorLong();

		long contadorClienteEndereco = 0;

		// integrando endereco principal
		if (contadorClienteEndereco < quantidadeMaximaClienteEndereco) {
			for (ClienteEndereco clienteEndereco : cliente.getEnderecos()) {
				if (clienteEndereco.getIndicadorPrincipal().equals(1)) {

					IntegracaoClienteEndereco integracaoClienteEndereco = preencherIntegracaoClienteEndereco(integracaoCliente, null,
							clienteEndereco, mapConstantes, sistemaIntegrante.getSigla().toUpperCase());

					super.inserir(integracaoClienteEndereco);
					contadorClienteEndereco++;
					break;

				}
			}
		}

		// integrando endereco de correspondencia
		if (contadorClienteEndereco < quantidadeMaximaClienteEndereco) {
			for (ClienteEndereco clienteEndereco : cliente.getEnderecos()) {

				if (!clienteEndereco.getIndicadorPrincipal().equals(1) && clienteEndereco.getCorrespondencia()) {

					IntegracaoClienteEndereco integracaoClienteEndereco = preencherIntegracaoClienteEndereco(integracaoCliente, null,
							clienteEndereco, mapConstantes, sistemaIntegrante.getSigla().toUpperCase());

					super.inserir(integracaoClienteEndereco);
					contadorClienteEndereco++;
					break;
				}
			}
		}

		// integrando enderecos restantes ate limite
		if (contadorClienteEndereco < quantidadeMaximaClienteEndereco) {
			for (ClienteEndereco clienteEndereco : cliente.getEnderecos()) {
				if (!clienteEndereco.getIndicadorPrincipal().equals(1) && !clienteEndereco.getCorrespondencia()) {

					IntegracaoClienteEndereco integracaoClienteEndereco = preencherIntegracaoClienteEndereco(integracaoCliente, null,
							clienteEndereco, mapConstantes, sistemaIntegrante.getSigla().toUpperCase());

					super.inserir(integracaoClienteEndereco);
					contadorClienteEndereco++;
					if (contadorClienteEndereco >= quantidadeMaximaClienteEndereco) {
						break;
					}
				}
			}
		}

	}

	/**
	 * Método responsável por preencher e retornar a IntegracaoCliente.
	 *

	 * @param cliente
	 *            the cliente
	 * @param sistemaIntegrante
	 *            the sistema integrante
	 * @param constanteSim
	 *            the constante sim
	 * @param constanteNao
	 *            the constante nao
	 * @param constanteOperacao
	 *            the constante operacao
	 * @param constanteNaoProcessado
	 *            the constante nao processado
	 * @param constanteIntegracaoSistemaGGAS
	 *            the constante integracao sistema ggas
	 * @return integracaoCliente
	 */
	private IntegracaoCliente preencherIntegracaoCliente(IntegracaoCliente integracaoClienteTmp, Cliente cliente, String sistemaIntegrante,
			Map<String, ConstanteSistema> mapConstantes) {
		ConstanteSistema constanteSim = mapConstantes.get(Constantes.C_SIM);
		ConstanteSistema constanteNao = mapConstantes.get(Constantes.C_NAO);
		ConstanteSistema constanteNaoProcessado = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema constanteIntegracaoSistemaGGAS = mapConstantes.get(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema constanteOperacao = mapConstantes.get(ControladorIntegracaoImpl.OPERACAO);
		
		IntegracaoCliente integracaoCliente = integracaoClienteTmp;
		if (integracaoCliente == null) {
			integracaoCliente = this.criarIntegracaoCliente();
		}
		integracaoCliente.setClienteOrigem(cliente.getChavePrimaria());
		if (cliente.getClienteResponsavel() != null) {
			integracaoCliente.setClienteResponsavel(cliente.getClienteResponsavel().getChavePrimaria());
		}
		integracaoCliente.setNome(cliente.getNome());
		integracaoCliente.setNomeAbreviado(cliente.getNomeAbreviado());
		integracaoCliente.setEmailPrincipal(cliente.getEmailPrincipal());
		integracaoCliente.setEmailSecundario(cliente.getEmailSecundario());
		if (cliente.getClienteSituacao() != null) {
			integracaoCliente.setClienteSituacao(buscarCodigoSistemaIntegranteNumerico(cliente.getClienteSituacao().getChavePrimaria(),
							cliente.getClienteSituacao().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		if (cliente.getTipoCliente() != null) {
			integracaoCliente.setTipoCliente(buscarCodigoSistemaIntegranteNumerico(cliente.getTipoCliente().getChavePrimaria(), cliente
							.getTipoCliente().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		integracaoCliente.setCpf(cliente.getCpf());
		integracaoCliente.setRg(cliente.getRg());
		integracaoCliente.setDataEmissaoRG(cliente.getDataEmissaoRG());
		if (cliente.getOrgaoExpedidor() != null) {
			integracaoCliente.setOrgaoExpedidor(buscarCodigoSistemaIntegranteNumerico(cliente.getOrgaoExpedidor().getChavePrimaria(),
							cliente.getOrgaoExpedidor().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		if (cliente.getUnidadeFederacao() != null) {
			integracaoCliente.setUnidadeFederacao(buscarCodigoSistemaIntegranteNumerico(cliente.getUnidadeFederacao().getChavePrimaria(),
							cliente.getUnidadeFederacao().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		if (cliente.getNacionalidade() != null) {
			integracaoCliente.setNacionalidade(buscarCodigoSistemaIntegranteNumerico(cliente.getNacionalidade().getChavePrimaria(), cliente
							.getNacionalidade().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		integracaoCliente.setNumeroPassaporte(cliente.getNumeroPassaporte());
		integracaoCliente.setDataNascimento(cliente.getDataNascimento());
		if (cliente.getProfissao() != null) {
			integracaoCliente.setProfissao(buscarCodigoSistemaIntegranteNumerico(cliente.getProfissao().getChavePrimaria(), cliente
							.getProfissao().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		if (cliente.getSexo() != null) {
			integracaoCliente.setSexo(buscarCodigoSistemaIntegranteNumerico(cliente.getSexo().getChavePrimaria(), cliente.getSexo()
							.getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		integracaoCliente.setNomeMae(cliente.getNomeMae());
		integracaoCliente.setNomePai(cliente.getNomePai());
		if (cliente.getRendaFamiliar() != null) {
			integracaoCliente.setRendaFamiliar(buscarCodigoSistemaIntegranteNumerico(cliente.getRendaFamiliar().getChavePrimaria(), cliente
							.getRendaFamiliar().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}
		integracaoCliente.setNomeFantasia(cliente.getNomeFantasia());
		integracaoCliente.setCnpj(cliente.getCnpj());
		if (cliente.getAtividadeEconomica() != null) {
			integracaoCliente
							.setAtividadeEconomica(buscarCodigoSistemaIntegranteNumerico(
											cliente.getAtividadeEconomica().getChavePrimaria(), cliente.getAtividadeEconomica().getClass()
															.getInterfaces()[0].getName(), sistemaIntegrante));
		}
		integracaoCliente.setInscricaoEstadual(cliente.getInscricaoEstadual());
		integracaoCliente.setInscricaoMunicipal(cliente.getInscricaoMunicipal());
		integracaoCliente.setInscricaoRural(cliente.getInscricaoRural());
		if (cliente.getRegimeRecolhimento() != null) {
			if ("1".equals(cliente.getRegimeRecolhimento())) {
				integracaoCliente.setRegimeRecolhimento(Long.valueOf(constanteSim.getValor()));
			} else {
				integracaoCliente.setRegimeRecolhimento(Long.valueOf(constanteNao.getValor()));
			}
		}
		if (constanteOperacao != null && constanteOperacao.getValor() != null) {
			integracaoCliente.setOperacao(constanteOperacao.getValor());
		}
		integracaoCliente.setSituacao(constanteNaoProcessado.getValor());
		integracaoCliente.setDescricaoErro(null);
		integracaoCliente.setSistemaOrigem(constanteIntegracaoSistemaGGAS.getValor());
		integracaoCliente.setSistemaDestino(sistemaIntegrante);
		integracaoCliente.setDadosAuditoria(cliente.getDadosAuditoria());
		if (cliente.getDadosAuditoria() != null && cliente.getDadosAuditoria().getUsuario() != null) {
			integracaoCliente.setUsuario(String.valueOf(cliente.getDadosAuditoria().getUsuario().getChavePrimaria()));
		}

		if (cliente.getSegmento() != null) {
			integracaoCliente.setSegmento(cliente.getSegmento().getChavePrimaria());
		}
		return integracaoCliente;
	}

	/**
	 * Método responsável por preencher e retornar a IntegracaoClienteEndereco.
	 *
	 * @param integracaoCliente the integracao cliente
	 * @param clienteEndereco the cliente endereco
	 * @param constanteSim the constante sim
	 * @param constanteNao the constante nao
	 * @param constanteOperacao the constante operacao
	 * @param constanteNaoProcessado the constante nao processado
	 * @param sistemaIntegrante the sistema integrante
	 * @return integracaoClienteEndereco
	 */
	private IntegracaoClienteEndereco preencherIntegracaoClienteEndereco(IntegracaoCliente integracaoCliente,
			IntegracaoClienteEndereco integracaoClienteEnderecoTmp, ClienteEndereco clienteEndereco,
			Map<String, ConstanteSistema> mapConstantes, String sistemaIntegrante) {
		ConstanteSistema constanteSim = mapConstantes.get(Constantes.C_SIM);
		ConstanteSistema constanteNao = mapConstantes.get(Constantes.C_NAO);
		ConstanteSistema constanteOperacao = mapConstantes.get(ControladorIntegracaoImpl.OPERACAO);
		ConstanteSistema constanteNaoProcessado = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		IntegracaoClienteEndereco integracaoClienteEndereco = integracaoClienteEnderecoTmp;
		if (clienteEndereco != null) {

			if (integracaoClienteEndereco == null) {
				integracaoClienteEndereco = this.criarIntegracaoClienteEndereco();
			}

			integracaoClienteEndereco.setCliente(integracaoCliente);

			if (clienteEndereco.getCep() != null) {
				if (clienteEndereco.getCep().getCep() != null) {
					integracaoClienteEndereco.setCep(Long.valueOf(Util.removerCaracteresEspeciais(clienteEndereco.getCep().getCep())));
				}
				integracaoClienteEndereco.setBairro(clienteEndereco.getCep().getBairro());
			}

			if (clienteEndereco.getTipoEndereco() != null) {
				integracaoClienteEndereco
						.setTipoEndereco(buscarCodigoSistemaIntegranteNumerico(clienteEndereco.getTipoEndereco().getChavePrimaria(),
								clienteEndereco.getTipoEndereco().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
			}

			integracaoClienteEndereco.setLogradouro(clienteEndereco.getEnderecoLogradouro());
			integracaoClienteEndereco.setNumero(clienteEndereco.getNumero());
			integracaoClienteEndereco.setComplemento(clienteEndereco.getComplemento());

			if (clienteEndereco.getCaixaPostal() != null) {
				integracaoClienteEndereco.setCaixaPostal(clienteEndereco.getCaixaPostal().longValue());
			}

			if (clienteEndereco.getCorrespondencia()) {
				integracaoClienteEndereco.setCorrespondencia(Long.valueOf(constanteSim.getValor()));
			} else {
				integracaoClienteEndereco.setCorrespondencia(Long.valueOf(constanteNao.getValor()));
			}

			integracaoClienteEndereco.setEnderecoReferencia(clienteEndereco.getEnderecoReferencia());

			// Município está ligado com o CEP
			if (clienteEndereco.getCep().getMunicipio().getDescricao() != null) {
				integracaoClienteEndereco.setMunicipio(clienteEndereco.getCep().getMunicipio().getUnidadeFederativaIBGE());
			}

			integracaoClienteEndereco.setSituacao(constanteNaoProcessado.getValor());
			integracaoClienteEndereco.setDescricaoErro(null);
			integracaoClienteEndereco.setDadosAuditoria(clienteEndereco.getDadosAuditoria());

			if (clienteEndereco.getDadosAuditoria() != null && clienteEndereco.getDadosAuditoria().getUsuario() != null) {
				integracaoClienteEndereco.setUsuario(String.valueOf(clienteEndereco.getDadosAuditoria().getUsuario().getChavePrimaria()));
			}

			integracaoClienteEndereco.setClienteEndereco(clienteEndereco.getChavePrimaria());
			integracaoClienteEndereco.setIndicadorPrincipal(clienteEndereco.getIndicadorPrincipal());
		}

		if (constanteOperacao != null && constanteOperacao.getValor() != null) {
			integracaoClienteEndereco.setOperacao(constanteOperacao.getValor());
		}

		if (constanteNaoProcessado != null && constanteNaoProcessado.getValor() != null) {
			integracaoClienteEndereco.setSituacao(constanteNaoProcessado.getValor());
		}

		return integracaoClienteEndereco;
	}

	/**
	 * Método responsável por preencher e retornar a IntegracaoClienteTelefone.
	 *
	 * @param integracaoCliente the integracao cliente
	 * @param constanteSim the constante sim
	 * @param constanteNao the constante nao
	 * @param constanteOperacao the constante operacao
	 * @param constanteNaoProcessado the constante nao processado
	 * @param sistemaIntegrante the sistema integrante
	 * @return integracaoClienteTelefone
	 */
	private IntegracaoClienteTelefone preencherIntegracaoClienteTelefone(IntegracaoCliente integracaoCliente,
			IntegracaoClienteTelefone integracaoClienteTelefoneTmp, ClienteFone clienteFoneTmp, Map<String, ConstanteSistema> mapConstantes,
			String sistemaIntegrante) {
		ConstanteSistema constanteSim = mapConstantes.get(Constantes.C_SIM);
		ConstanteSistema constanteNao = mapConstantes.get(Constantes.C_NAO);
		ConstanteSistema constanteOperacao = mapConstantes.get(ControladorIntegracaoImpl.OPERACAO);
		ConstanteSistema constanteNaoProcessado = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		IntegracaoClienteTelefone integracaoClienteTelefone = integracaoClienteTelefoneTmp;
		ClienteFone clienteFone = clienteFoneTmp;
		if (clienteFone != null) {
			if (integracaoClienteTelefone == null) {
				integracaoClienteTelefone = this.criarIntegracaoClienteTelefone();
			}
			integracaoClienteTelefone.setCliente(integracaoCliente);
			if (clienteFone.getTipoFone() != null) {
				integracaoClienteTelefone.setTipoFone(buscarCodigoSistemaIntegranteNumerico(clienteFone.getTipoFone().getChavePrimaria(),
						clienteFone.getTipoFone().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
			}
			if (clienteFone.getCodigoDDD() != null) {
				integracaoClienteTelefone.setCodigoDDD(clienteFone.getCodigoDDD().longValue());
			}
			if (clienteFone.getNumero() != null) {
				integracaoClienteTelefone.setNumero(clienteFone.getNumero().toString());
			}
			if (clienteFone.getRamal() != null) {
				integracaoClienteTelefone.setRamal(clienteFone.getRamal().toString());
			}
			if (clienteFone.getIndicadorPrincipal()) {
				integracaoClienteTelefone.setIndicadorPrincipal(Long.valueOf(constanteSim.getValor()));
			} else {
				integracaoClienteTelefone.setIndicadorPrincipal(Long.valueOf(constanteNao.getValor()));
			}
			integracaoClienteTelefone.setSituacao(constanteNaoProcessado.getValor());
			integracaoClienteTelefone.setDescricaoErro(null);
			integracaoClienteTelefone.setDadosAuditoria(clienteFone.getDadosAuditoria());
			if (clienteFone.getDadosAuditoria() != null && clienteFone.getDadosAuditoria().getUsuario() != null) {
				integracaoClienteTelefone.setUsuario(String.valueOf(clienteFone.getDadosAuditoria().getUsuario().getChavePrimaria()));
			}
			integracaoClienteTelefone.setClienteTelefone(clienteFone.getChavePrimaria());
		}
		if (constanteOperacao != null && constanteOperacao.getValor() != null) {
			integracaoClienteTelefone.setOperacao(constanteOperacao.getValor());
		}
		if (constanteNaoProcessado != null && constanteNaoProcessado.getValor() != null) {
			integracaoClienteTelefone.setSituacao(constanteNaoProcessado.getValor());
		}
		return integracaoClienteTelefone;
	}

	/**
	 * Método responsável por preencher e retornar a IntegracaoClienteContato.
	 *
	 * @param integracaoCliente the integracao cliente
	 * @param integracaoClienteContato the integracao cliente contato
	 * @param contatoCliente the contato cliente
	 * @param constanteSim the constante sim
	 * @param constanteNao the constante nao
	 * @param constanteOperacao the constante operacao
	 * @param constanteNaoProcessado the constante nao processado
	 * @param sistemaIntegrante the sistema integrante
	 * @return integracaoClienteContato
	 */
	private IntegracaoClienteContato preencherIntegracaoClienteContato(IntegracaoCliente integracaoCliente,
					IntegracaoClienteContato integracaoClienteContato, ContatoCliente contatoCliente, Map<String, ConstanteSistema> mapConstantes,
					String sistemaIntegrante) {

		ConstanteSistema constanteSim = mapConstantes.get(Constantes.C_SIM);
		ConstanteSistema constanteNao = mapConstantes.get(Constantes.C_NAO);
		ConstanteSistema constanteOperacao = mapConstantes.get(ControladorIntegracaoImpl.OPERACAO);
		ConstanteSistema constanteNaoProcessado = mapConstantes.get(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		
		if (contatoCliente != null) {
			if (integracaoClienteContato == null) {
				integracaoClienteContato = this.criarIntegracaoClienteContato();
			}
			integracaoClienteContato.setCliente(integracaoCliente);
			if (contatoCliente.getTipoContato() != null) {
				integracaoClienteContato.setTipoContato(buscarCodigoSistemaIntegranteNumerico(contatoCliente.getTipoContato()
								.getChavePrimaria(), contatoCliente.getTipoContato().getClass().getInterfaces()[0].getName(),
								sistemaIntegrante));
			}
			integracaoClienteContato.setNome(contatoCliente.getNome());
			if (contatoCliente.getCodigoDDD() != null) {
				integracaoClienteContato.setCodigoDDD(contatoCliente.getCodigoDDD().longValue());
			}
			if (contatoCliente.getFone() != null) {
				integracaoClienteContato.setFone(contatoCliente.getFone().toString());
			}
			if (contatoCliente.getRamal() != null) {
				integracaoClienteContato.setRamal(contatoCliente.getRamal().toString());
			}
			integracaoClienteContato.setDescricaoArea(contatoCliente.getDescricaoArea());
			integracaoClienteContato.setEmail(contatoCliente.getEmail());
			if (contatoCliente.isPrincipal()) {
				integracaoClienteContato.setPrincipal(Long.valueOf(constanteSim.getValor()));
			} else {
				integracaoClienteContato.setPrincipal(Long.valueOf(constanteNao.getValor()));
			}
			if (contatoCliente.getProfissao() != null) {
				integracaoClienteContato.setProfissao(buscarCodigoSistemaIntegranteNumerico(contatoCliente.getProfissao()
								.getChavePrimaria(), contatoCliente.getProfissao().getClass().getInterfaces()[0].getName(),
								sistemaIntegrante));
			}
			integracaoClienteContato.setSituacao(constanteNaoProcessado.getValor());
			integracaoClienteContato.setDescricaoErro(null);
			integracaoClienteContato.setDadosAuditoria(contatoCliente.getDadosAuditoria());
			if (contatoCliente.getDadosAuditoria() != null && contatoCliente.getDadosAuditoria().getUsuario() != null) {
				integracaoClienteContato.setUsuario(String.valueOf(contatoCliente.getDadosAuditoria().getUsuario().getChavePrimaria()));
			}
			integracaoClienteContato.setClienteContato(contatoCliente.getChavePrimaria());
		}
		if (constanteOperacao != null && constanteOperacao.getValor() != null) {
			integracaoClienteContato.setOperacao(constanteOperacao.getValor());
		}
		if (constanteNaoProcessado != null && constanteNaoProcessado.getValor() != null) {
			integracaoClienteContato.setSituacao(constanteNaoProcessado.getValor());
		}
		return integracaoClienteContato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#inserirIntegracaoSistemaParametro(br.com.ggas.integracao.geral.
	 * IntegracaoSistemaParametro)
	 */
	@Override
	public void inserirIntegracaoSistemaParametro(IntegracaoSistemaParametro integracaoSistemaParametro) throws NegocioException {

		this.inserir(integracaoSistemaParametro);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#inserirIntegracaoSistemaMapeamento(br.com.ggas.integracao.geral.IntegracaoMapeamento
	 * )
	 */
	@Override
	public void inserirIntegracaoSistemaMapeamento(IntegracaoMapeamento integracaoSistemaMapeamento) throws NegocioException {

		this.inserir(integracaoSistemaMapeamento);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#atualizarIntegracaoSistemaParametro(br.com.ggas.integracao.geral.
	 * IntegracaoSistemaParametro)
	 */
	@Override
	public void atualizarIntegracaoSistemaParametro(IntegracaoSistemaParametro integracaoSistemaParametro) throws NegocioException,
					ConcorrenciaException {

		this.atualizar(integracaoSistemaParametro, IntegracaoSistemaParametroImpl.class);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterIntegracaoSistema(long)
	 */
	@Override
	public IntegracaoSistema obterIntegracaoSistema(long chavePrimaria) throws NegocioException {

		return (IntegracaoSistema) super.obter(chavePrimaria, getClasseEntidadeIntegracaoSistema());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#atualizarIntegracaoSistema(br.com.ggas.integracao.geral.IntegracaoSistema)
	 */
	@Override
	public void atualizarIntegracaoSistema(IntegracaoSistema integracaoSistema) throws NegocioException, ConcorrenciaException {

		super.atualizar(integracaoSistema, getClasseEntidadeIntegracaoSistema());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#atualizarAcompanharIntegracao(br.com.ggas.geral.negocio.EntidadeNegocio,
	 * java.lang.Class)
	 */
	@Override
	public void atualizarAcompanharIntegracao(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException,
					ConcorrenciaException {

		super.atualizar(entidadeNegocio, classe);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#removerIntegracaoSistema(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerIntegracaoSistema(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		this.remover(chavesPrimarias, IntegracaoSistema.class.getName(), dadosAuditoria);
	}

	/*
	 * Metodo responsavel por pesquisar todos os Sistemas Integrantes
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultarIntegracaoSistema(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoSistema> consultarIntegracaoSistema(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoSistema());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String nome = (String) filtro.get(NOME);
			if (!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike(NOME, Util.formatarTextoConsulta(nome)));
			}

			String sigla = (String) filtro.get(SIGLA);
			if (!StringUtils.isEmpty(sigla)) {
				criteria.add(Restrictions.ilike(SIGLA, Util.formatarTextoConsulta(sigla)));
			}

			String descricao = (String) filtro.get(DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc(NOME));
			criteria.addOrder(Order.asc(SIGLA));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultarIntegracaoSistemaFuncao(java.lang.Long)
	 */
	@Override
	public Collection<IntegracaoSistema> consultarIntegracaoSistemaFuncao(Long integracaoFuncao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" integracaoSistemaFuncao ");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoSistema integracaoSistema");
		hql.append(" where ");
		hql.append(" integracaoSistemaFuncao.habilitado = true and ");
		hql.append(" integracaoSistemaFuncao.integracaoFuncao.chavePrimaria = :integracaoFuncao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("integracaoFuncao", integracaoFuncao);

		Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao = query.list();
		Collection<IntegracaoSistema> listaIntegracaoSistema = new ArrayList<IntegracaoSistema>();

		for (Iterator<IntegracaoSistemaFuncao> iterator = listaIntegracaoSistemaFuncao.iterator(); iterator.hasNext();) {
			IntegracaoSistemaFuncao integracaoSistemaFuncao = iterator.next();

			if (!listaIntegracaoSistema.contains(integracaoSistemaFuncao.getIntegracaoSistema())) {

				listaIntegracaoSistema.add(integracaoSistemaFuncao.getIntegracaoSistema());
			}
		}

		return listaIntegracaoSistema;
	}

	
	@Override
	public Collection<IntegracaoSistemaFuncao> consultarIntegracaoSistemaFuncaoPorIntegracaoFuncao(Long integracaoFuncao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" integracaoSistemaFuncao ");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoSistema integracaoSistema");
		hql.append(" where ");
		hql.append(" integracaoSistemaFuncao.habilitado = true and ");
		hql.append(" integracaoSistemaFuncao.integracaoFuncao.chavePrimaria = :integracaoFuncao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("integracaoFuncao", integracaoFuncao);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultarIntegracao(java.util.Map, java.lang.Class, java.lang.String,
	 * java.lang.String[])
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Class<?>> consultarIntegracao(Map<String, Object> filtro, Class<?> classe, String ordenacao,
					String... propriedadesLazy) {

		Query query = null;

		Object[] chavesSistemasIntegrantes = null;
		Boolean habilitado = null;
		String naoProcessado = null;
		String processado = null;
		Long chavePrimaria = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe.getSimpleName());
		hql.append(" integracao ");

		if (classe.getSimpleName().equals(Constantes.CLASSE_INTEGRACAO_BEM_MOVIMENTACAO)) {
			hql.append(" inner join fetch integracao.bem bem");
		}

		hql.append(" where 1=1 ");

		if (filtro != null && !filtro.isEmpty()) {

			chavesSistemasIntegrantes = (Object[]) filtro.get("chavesSistemasIntegrantes");
			if (chavesSistemasIntegrantes != null && chavesSistemasIntegrantes.length > 0) {

				hql.append(" and upper(integracao.sistemaOrigem) in (:chavesSistemasIntegrantes) ");
			}

			habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {

				hql.append(" and integracao.habilitado = :habilitado ");
			}

			naoProcessado = (String) filtro.get("naoProcessado");
			if (naoProcessado != null && !StringUtils.isEmpty(naoProcessado)) {

				hql.append(" and upper(integracao.situacao) = upper(:naoProcessado) ");
			}

			processado = (String) filtro.get("processado");
			if (processado != null && !StringUtils.isEmpty(processado)) {

				hql.append(" and upper(integracao.situacao) like upper(:processado) ");
			}

			chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {

				hql.append(" and integracao.chavePrimaria = :chavePrimaria ");
			}

		}

		if (ordenacao != null) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null) {
			if (chavesSistemasIntegrantes != null && chavesSistemasIntegrantes.length > 0) {

				query.setParameterList("chavesSistemasIntegrantes", chavesSistemasIntegrantes);
			}

			if (habilitado != null) {

				query.setBoolean(HABILITADO, Boolean.TRUE);
			}

			if (naoProcessado != null && !StringUtils.isEmpty(naoProcessado)) {

				query.setString("naoProcessado", naoProcessado);
			}

			if (processado != null && !StringUtils.isEmpty(processado)) {

				query.setString("processado", processado);
			}

			if (chavePrimaria != null && chavePrimaria > 0) {

				query.setLong(CHAVE_PRIMARIA, chavePrimaria);
			}

		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > 0) {

			lista = super.inicializarLazyColecao(lista, propriedadesLazy);

		}

		return (Collection<Class<?>>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#gerarListaSistemasIntegrantesPorFuncao(java.lang.Long)
	 */
	@Override
	public List<String> gerarListaSistemasIntegrantesPorFuncao(Long chaveFuncao) {

		Collection<IntegracaoSistema> listaIntegracaoSistema = this.consultarIntegracaoSistemaFuncao(chaveFuncao);
		List<String> listaSistemasIntegrantes = new ArrayList<String>();
		for (IntegracaoSistema integracaoSistema : listaIntegracaoSistema) {
			if (!listaSistemasIntegrantes.contains(integracaoSistema.getSigla())) {
				listaSistemasIntegrantes.add(integracaoSistema.getSigla().toUpperCase());
			}
		}

		return listaSistemasIntegrantes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#buscarEmailDestinatarioIntegracao(java.lang.Long)
	 */
	@Override
	public String buscarEmailDestinatarioIntegracao(Long chaveFuncao) {

		String emails = "";
		Collection<IntegracaoSistemaFuncao> listaIntegracaoSistema = this.consultarIntegracaoSistemaFuncaoPorIntegracaoFuncao(chaveFuncao);
		for (IntegracaoSistemaFuncao integracaoSistema : listaIntegracaoSistema) {
			emails = Util.concatenaEmails(emails, integracaoSistema.getDestinatariosEmail());
		}

		return emails;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#atualizarIntegracaoComErro(java.lang.Class,
	 * br.com.ggas.integracao.geral.Integracao,
	 * java.lang.String)
	 */
	@Override
	public void atualizarIntegracaoComErro(Class<?> classe, Integracao integracao, String descricaoErro) throws NegocioException,
					ConcorrenciaException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistemaSituacaoErro = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_ERRO);

		integracao.setSituacao(constanteSistemaSituacaoErro.getValor());
		integracao.setDescricaoErro(descricaoErro);

		super.atualizar(integracao, classe);
	}

	/**
	 * Atualizar integracao processado.
	 *
	 * @param classe
	 *            the classe
	 * @param integracaoBem
	 *            the integracao bem
	 * @param chavePrimaria
	 *            the chave primaria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void atualizarIntegracaoProcessado(Class<?> classe, IntegracaoBem integracaoBem, Long chavePrimaria) throws NegocioException,
					ConcorrenciaException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistemaSituacaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

		integracaoBem.setSituacao(constanteSistemaSituacaoProcessado.getValor());
		integracaoBem.setBemDestino(chavePrimaria);
		super.atualizar(integracaoBem, classe);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultarIntegracaoSistemaFuncao(java.util.Map)
	 */
	@Override
	public Collection<IntegracaoSistemaFuncao> consultarIntegracaoSistemaFuncao(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoSistemaFuncao());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long indicadorEnviaEmailErro = (Long) filtro.get("indicadorEnviaEmailErro");
			if ((indicadorEnviaEmailErro != null) && (indicadorEnviaEmailErro > 0)) {
				criteria.add(Restrictions.eq("indicadorEnviaEmailErro", indicadorEnviaEmailErro));
			}

			Long[] idIntegracaoSistemas = (Long[]) filtro.get("idIntegracaoSistemas");
			if (idIntegracaoSistemas != null && idIntegracaoSistemas.length > 0) {
				criteria.createAlias("integracaoSistema", "integracaoSistema");
				criteria.add(Restrictions.in(INTEGRACAO_SISTEMA_CHAVE_PRIMARIA, idIntegracaoSistemas));
			}

			Long idIntegracaoSistema = (Long) filtro.get("idIntegracaoSistema");
			if ((idIntegracaoSistema != null) && idIntegracaoSistema > 0) {
				criteria.createAlias("integracaoSistema", "integracaoSistema");
				criteria.add(Restrictions.eq(INTEGRACAO_SISTEMA_CHAVE_PRIMARIA, idIntegracaoSistema));
			}

			Long idIntegracaoFuncao = (Long) filtro.get("idIntegracaoFuncao");
			if ((idIntegracaoFuncao != null) && idIntegracaoFuncao > 0) {
				criteria.createAlias("integracaoFuncao", "integracaoFuncao");
				criteria.add(Restrictions.eq("integracaoFuncao.chavePrimaria", idIntegracaoFuncao));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoSistemaFuncao(br.com.ggas.integracao.geral.IntegracaoSistema)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao(IntegracaoSistema integracaoSistema) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" as integracaoSistemaFuncao ");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoFuncao funcao");
		hql.append(" where ");
		hql.append(" integracaoSistemaFuncao.integracaoSistema.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, integracaoSistema.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#verificarSistemaIntegranteExistente(java.util.Map)
	 */
	@Override
	public void verificarSistemaIntegranteExistente(Map<String, Object> filtro) throws NegocioException {

		Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);

		Collection<IntegracaoSistema> sistema = null;
		filtro.put(NOME, null);
		filtro.put(HABILITADO, null);
		filtro.put(CHAVE_PRIMARIA, null);
		sistema = this.consultarIntegracaoSistema(filtro);

		if (sistema != null && !sistema.isEmpty()) {
			IntegracaoSistema sistemaExistente = sistema.iterator().next();

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(sistemaExistente);
			if (sistemaExistente.getChavePrimaria() != chavePrimaria) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SISTEMA_INTEGRANTE_EXISTENTE, sistemaExistente.getNome());
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#verificarExistenciaIntegracoesAtivas(java.util.Map)
	 */
	@Override
	public void verificarExistenciaIntegracoesAtivas(Map<String, Object> filtro) throws NegocioException {

		Collection<IntegracaoSistemaFuncao> sistemas = null;
		filtro.put(HABILITADO, true);
		sistemas = this.consultarIntegracaoSistemaFuncao(filtro);

		if (sistemas != null && !sistemas.isEmpty()) {
			IntegracaoSistemaFuncao interacoesAtivas = sistemas.iterator().next();

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(interacoesAtivas);

			throw new NegocioException(Constantes.ERRO_NEGOCIO_INTERACOES_ATIVAS, new String[] {});
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#verificarExistenciaIntegracoesRelacionadas(java.util.Map)
	 */
	@Override
	public void verificarExistenciaIntegracoesRelacionadas(Map<String, Object> filtro) throws NegocioException {

		Collection<IntegracaoSistemaFuncao> sistemas = null;

		sistemas = this.consultarIntegracaoSistemaFuncao(filtro);

		if (sistemas != null && !sistemas.isEmpty()) {
			IntegracaoSistemaFuncao interacoesAtivas = sistemas.iterator().next();

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(interacoesAtivas);

			throw new NegocioException(Constantes.ERRO_NEGOCIO_INTERACOES_RELACIONADAS, new String[] {});
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#validarDadosSistemaIntegrante(br.com.ggas.integracao.geral.IntegracaoSistema)
	 */
	@Override
	public void validarDadosSistemaIntegrante(IntegracaoSistema integracaoSistema) throws NegocioException {

		Map<String, Object> errosIntegracaoSistema = integracaoSistema.validarDados();

		if (errosIntegracaoSistema != null && !errosIntegracaoSistema.isEmpty()) {
			throw new NegocioException(errosIntegracaoSistema);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarDadosIntegracaoSistemaParametro(br.com.ggas.integracao.geral.
	 * IntegracaoParametro)
	 */
	@Override
	public void validarDadosIntegracaoSistemaParametro(IntegracaoParametro integracaoParametro) throws NegocioException {

		Map<String, Object> errosIntegracaoSistemaParametro = integracaoParametro.validarDados();

		if (errosIntegracaoSistemaParametro != null && !errosIntegracaoSistemaParametro.isEmpty()) {
			throw new NegocioException(errosIntegracaoSistemaParametro);
		}

	}

	public Class<?> getClasseAgencia() {

		return ServiceLocator.getInstancia().getClassPorID(Agencia.BEAN_ID_AGENCIA);
	}

	/**
	 * Criar agencia.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAgencia() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Agencia.BEAN_ID_AGENCIA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#criarIntegracaoSistemaFuncao()
	 */
	@Override
	public EntidadeNegocio criarIntegracaoSistemaFuncao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoSistemaFuncao.BEAN_ID_INTEGRACAO_SISTEMA_FUNCAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#criarIntegracaoSistemaParametro()
	 */
	@Override
	public EntidadeNegocio criarIntegracaoSistemaParametro() {

		return (EntidadeNegocio) ServiceLocator.getInstancia()
						.getBeanPorID(IntegracaoSistemaParametro.BEAN_ID_INTEGRACAO_SISTEMA_PARAMETRO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#criarIntegracaoFuncao()
	 */
	@Override
	public EntidadeNegocio criarIntegracaoFuncao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoFuncao.BEAN_ID_INTEGRACAO_FUNCAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#criarIntegracaoFuncaoTabela()
	 */
	@Override
	public EntidadeNegocio criarIntegracaoFuncaoTabela() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoFuncaoTabela.BEAN_ID_INTEGRACAO_FUNCAO_TABELA);
	}

	/**
	 * Criar tabela.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarTabela() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Tabela.BEAN_ID_TABELA);
	}

	/**
	 * Criar coluna.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarColuna() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Coluna.BEAN_ID_COLUNA_TABELA);
	}

	public Class<?> getClasseEntidadeIntegracaoSistema() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoSistema.BEAN_ID_INTEGRACAO_SISTEMA);
	}

	public Class<?> getClasseEntidadeIntegracaoSistemaFuncao() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoSistemaFuncao.BEAN_ID_INTEGRACAO_SISTEMA_FUNCAO);
	}

	public Class<?> getClasseEntidadeIntegracaoFuncao() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoFuncao.BEAN_ID_INTEGRACAO_FUNCAO);
	}

	public Class<?> getClasseEntidadeIntegracaoFuncaoTabela() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoFuncaoTabela.BEAN_ID_INTEGRACAO_FUNCAO_TABELA);
	}

	public Class<?> getClasseEntidadeIntegracaoMapeamento() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoMapeamento.BEAN_ID_INTEGRACAO_MAPEAMENTO);
	}

	public Class<?> getClasseEntidadeTabela() {

		return ServiceLocator.getInstancia().getClassPorID(Tabela.BEAN_ID_TABELA);
	}

	public Class<?> getClasseEntidadeColuna() {

		return ServiceLocator.getInstancia().getClassPorID(Coluna.BEAN_ID_COLUNA_TABELA);
	}

	public Class<?> getClasseEntidadeIntegracaoBemMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoBemMedidor.BEAN_ID_INTEGRACAO_BEM_MEDIDOR);
	}

	public Class<?> getClasseEntidadeIntegracaoClienteEndereco() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoClienteEndereco.BEAN_ID_INTEGRACAO_CLIENTE_ENDERECO);
	}

	public Class<?> getClasseEntidadeIntegracaoClienteTelefone() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoClienteTelefone.BEAN_ID_INTEGRACAO_CLIENTE_TELEFONE);
	}

	public Class<?> getClasseEntidadeIntegracaoClienteContato() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoClienteContato.BEAN_ID_INTEGRACAO_CLIENTE_CONTATO);
	}

	public Class<?> getClasseEntidadeIntegracaoTitulo() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoTitulo.BEAN_ID_INTEGRACAO_TITULO);
	}

	public Class<?> getClasseEntidadeIntegracaoTituloReceber() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoTituloReceber.BEAN_ID_INTEGRACAO_TITULO_RECEBER);
	}

	public Class<?> getClasseEntidadeIntegracaoTituloBaixa() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoTituloImpl.BEAN_ID_INTEGRACAO_TITULO);
	}

	public Class<?> getClasseEntidadeIntegracaoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoCliente.BEAN_ID_INTEGRACAO_CLIENTE);
	}

	public Class<?> getClasseEntidadeIntegracaoTituloPagar() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoTituloPagar.BEAN_ID_INTEGRACAO_TITULO_PAGAR);
	}

	/**
	 * Criar integracao bem medidor.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoBemMedidor() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoBemMedidor.BEAN_ID_INTEGRACAO_BEM_MEDIDOR);
	}

	/**
	 * Criar integracao bem vazao corretor.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoBemVazaoCorretor() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						IntegracaoBemVazaoCorretor.BEAN_ID_INTEGRACAO_BEM_VAZAO_CORRETOR);
	}

	/**
	 * Criar movimentacao medidor.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarMovimentacaoMedidor() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(MovimentacaoMedidor.BEAN_ID_MOVIMENTACAO_MEDIDOR);
	}

	/**
	 * Criar corretor vazao movimentacao.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarCorretorVazaoMovimentacao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CorretorVazaoMovimentacao.BEAN_ID_CORRETOR_VAZAO_MOVIMENTACAO);
	}

	/**
	 * Criar integracao nota fiscal.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoNotaFiscal() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoNotaFiscal.BEAN_ID_INTEGRACAO_NOTA_FISCAL);
	}

	/**
	 * Criar integracao nota fiscal item.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoNotaFiscalItem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoNotaFiscalItem.BEAN_ID_INTEGRACAO_NOTA_FISCAL_ITEM);
	}

	/**
	 * Criar integracao nota fiscal tributo.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoNotaFiscalTributo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						IntegracaoNotaFiscalItemTributo.BEAN_ID_INTEGRACAO_NOTA_FISCAL_ITEM_TRIBUTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#criarIntegracaoTitulo()
	 */
	@Override
	public IntegracaoTitulo criarIntegracaoTitulo() {

		return (IntegracaoTitulo) ServiceLocator.getInstancia().getBeanPorID(IntegracaoTitulo.BEAN_ID_INTEGRACAO_TITULO);
	}

	/**
	 * Criar integracao titulo pagar.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoTituloPagar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(IntegracaoTituloPagar.BEAN_ID_INTEGRACAO_TITULO_PAGAR);
	}

	/**
	 * Criar integracao titulo pagar tributo.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarIntegracaoTituloPagarTributo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						IntegracaoTituloPagarTributo.BEAN_ID_INTEGRACAO_TITULO_PAGAR_TRIBUTO);
	}

	public Class<?> getClasseEntidadeIntegracaoLancamentoContabil() {

		return ServiceLocator.getInstancia().getClassPorID(IntegracaoLancamentoContabilImpl.BEAN_ID_INTEGRACAO_LANCAMENTO_CONTABIL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoSistema()
	 */
	@Override
	public Collection<IntegracaoSistema> listarIntegracaoSistema() {

		Query query = criarQueryParaComboIntegracaoSistema(getClasseEntidadeIntegracaoSistema().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarFuncao()
	 */
	@Override
	public Collection<IntegracaoFuncao> listarFuncao() {

		Query query = criarQueryParaCombos(getClasseEntidadeIntegracaoFuncao().getSimpleName());
		return query.list();
	}

	/**
	 * Criar query para combo integracao sistema.
	 *
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaComboIntegracaoSistema(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Método que lista funções por
	 * sistema selecionado.
	 *
	 * @param chavePrimariaSistema
	 *            the chave primaria sistema
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<IntegracaoSistemaFuncao> listarFuncaoPorSistema(Long chavePrimariaSistema) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" as sistemafuncao ");
		hql.append(" inner join fetch sistemafuncao.integracaoSistema sistema ");
		hql.append(" left join fetch sistemafuncao.integracaoFuncao funcao ");
		hql.append(" where sistemafuncao.integracaoSistema.chavePrimaria = ? ");
		hql.append(" and sistemafuncao.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimariaSistema);

		return query.list();
	}

	/**
	 * Método que lista todas as tabelas
	 * de integração com determinada função.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoFuncaoTabela> listarTabelas(Long chavePrimaria) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName());
		hql.append(" as funcaotabela ");
		hql.append(" inner join fetch funcaotabela.tabela tabela ");
		if (chavePrimaria != null && chavePrimaria != 0) {
			hql.append(" where funcaotabela.integracaoFuncao.chavePrimaria = ? ");
			hql.append(" and funcaotabela.tabela.chavePrimaria = tabela.chavePrimaria ");
			hql.append(" and tabela.integracao = true ");
			hql.append(" order by tabela.nome ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong(0, chavePrimaria);

		} else {
			hql.append(" where tabela.integracao = true ");
			hql.append(" order by tabela.nome ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoFuncaoTabela(java.util.Collection, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoFuncaoTabela> listarIntegracaoFuncaoTabela(
					Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao, Long idIntegracaoFuncao) throws NegocioException {

		Long[] chavesPrimaria = new Long[listaIntegracaoSistemaFuncao.size()];
		int i = 0;
		for (IntegracaoSistemaFuncao integracaoSistemaFuncao : listaIntegracaoSistemaFuncao) {
			chavesPrimaria[i] = Long.valueOf(integracaoSistemaFuncao.getIntegracaoFuncao().getChavePrimaria());
			i++;
		}
		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName());
		hql.append(" as funcaotabela ");
		hql.append(" inner join fetch funcaotabela.tabela tabela ");

		hql.append(" where funcaotabela.integracaoFuncao.chavePrimaria in (:chavesPrimariasIntegracaoFuncao) ");

		hql.append(" and tabela.integracao = true ");
		hql.append(" order by tabela.nome ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesPrimariasIntegracaoFuncao", chavesPrimaria);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarColunasTabela(java.lang.Long)
	 */
	@Override
	public Collection<Coluna> listarColunasTabela(Long chavePrimariaTabela) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeColuna().getSimpleName());
		hql.append(" as coluna ");
		hql.append(" inner join fetch coluna.tabela tabela ");
		hql.append(" where tabela.chavePrimaria = ? ");
		hql.append(" and tabela.integracao = true ");
		hql.append(" order by coluna.nome ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimariaTabela);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarClasse()
	 */
	@Override
	public Collection<?> listarClasse() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append("AgenciaImpl");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarTabelaColunas(java.lang.Long)
	 */
	@Override
	public Collection<Tabela> listarTabelaColunas(Long chavePrimariaTabela) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(" as tabela ");
		hql.append(" inner join fetch tabela.listaColunaTabela ");
		hql.append(" where tabela.chavePrimaria= ? ");
		hql.append(" and tabela.integracao = true ");
		hql.append(" order by coluna.nome ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimariaTabela);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarConteudoTabelaColuna(br.com.ggas.cadastro.imovel.Tabela,
	 * java.util.List)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public List<Object> listarConteudoTabelaColuna(Tabela tabela, List<AcompanhamentoIntegracaoVO> listaFiltros) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");

		try {
			Class<?> classe = Class.forName(tabela.getNomeClasse());
			if (classe.isInterface()) {
				hql.append(classe.getSimpleName()).append("Impl");
			} else {
				hql.append(classe.getSimpleName());
			}
		} catch (ClassNotFoundException e) {
			throw new NegocioException(e);
		}

		hql.append(" where 1=1 ");

		if (listaFiltros != null) {
			for (AcompanhamentoIntegracaoVO acompanhamentoIntegracaoVO : listaFiltros) {

				if (acompanhamentoIntegracaoVO.getValorColuna() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())
								&& acompanhamentoIntegracaoVO.getValorColuna2() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

					hql.append(" and ").append(acompanhamentoIntegracaoVO.getNomePropriedade()).append(" >= :")
							.append(acompanhamentoIntegracaoVO.getNomeColuna());

					hql.append(" and :").append(acompanhamentoIntegracaoVO.getNomeColuna()).append("2 >= ")
							.append(acompanhamentoIntegracaoVO.getNomePropriedade());

				} else if (acompanhamentoIntegracaoVO.getValorColuna() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())) {

					hql.append(" and ").append(acompanhamentoIntegracaoVO.getNomePropriedade()).append(" = :")
							.append(acompanhamentoIntegracaoVO.getNomeColuna());

				} else if (acompanhamentoIntegracaoVO.getValorColuna2() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

					hql.append(" and ").append(acompanhamentoIntegracaoVO.getNomePropriedade()).append(" = :")
							.append(acompanhamentoIntegracaoVO.getNomeColuna()).append("2");

				}
			}
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (listaFiltros != null) {
			try {
				for (AcompanhamentoIntegracaoVO acompanhamentoIntegracaoVO : listaFiltros) {

					if (acompanhamentoIntegracaoVO.getValorColuna() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())
									&& acompanhamentoIntegracaoVO.getValorColuna2() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna(), acompanhamentoIntegracaoVO
										.getValorColuna().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna() + "2", acompanhamentoIntegracaoVO
										.getValorColuna2().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

					} else if (acompanhamentoIntegracaoVO.getValorColuna() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())) {

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna(), acompanhamentoIntegracaoVO
										.getValorColuna().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

					} else if (acompanhamentoIntegracaoVO.getValorColuna2() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna() + "2", acompanhamentoIntegracaoVO
										.getValorColuna2().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

					}
				}
			} catch (Exception e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);

				throw new NegocioException(ControladorIntegracao.ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO, true);
			}
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterIntegracaoFuncaoTabela(long)
	 */
	@Override
	public Collection<IntegracaoFuncaoTabela> obterIntegracaoFuncaoTabela(long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName());
		hql.append(" as funcaoTabela ");
		hql.append(" inner join fetch funcaoTabela.integracaoFuncao ");
		hql.append(" where tabela.chavePrimaria= ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimaria);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarParametrosAssociados(br.com.ggas.integracao.geral.IntegracaoFuncao)
	 */
	@Override
	public Collection<IntegracaoParametro> listarParametrosAssociados(IntegracaoFuncao integracaoFuncao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoParametro().getSimpleName());
		hql.append(" as integracaoParametro ");
		hql.append(" inner join fetch integracaoParametro.parametroSistema parametro");
		hql.append(" where ");
		hql.append(" integracaoParametro.integracaoFuncao.chavePrimaria = ? ");
		hql.append(" and integracaoParametro.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, integracaoFuncao.getChavePrimaria());

		return query.list();
	}

	@Override
	public IntegracaoSistemaParametro
					obterIntegracaoSistemaParametro(IntegracaoSistema integracaoSistema, ParametroSistema parametroSistema)
									throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoSistemaParametro().getSimpleName());
		hql.append(" as integracaoSistemaParametro ");
		hql.append(" inner join fetch integracaoSistemaParametro.integracaoParametro.parametroSistema parametroSistema");
		hql.append(" inner join fetch integracaoSistemaParametro.integracaoSistemaFuncao.integracaoSistema integracaoSistema");
		hql.append(" where parametroSistema.chavePrimaria = ? ");
		hql.append(" and integracaoSistema.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, parametroSistema.getChavePrimaria());
		query.setLong(1, integracaoSistema.getChavePrimaria());

		return (IntegracaoSistemaParametro) query.uniqueResult();
	}

	/**
	 * Consulta um IntegracaoContrato no banco
	 * @param contrato O contrato ao qual essa integração está relacionada
	 * @param pontoConsumo O ponto de consumo ao qual essa integração está relacionada
	 * @return O IntegracaoContrato
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public IntegracaoContrato obterIntegracaoContrato(Contrato contrato, PontoConsumo pontoConsumo) {
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
		if (contratoPontoConsumo != null) {
			contrato = contratoPontoConsumo.getContrato();
		}

		ControladorConstanteSistema ctlConstanteSistema = getControladorConstanteSistema();
		ConstanteSistema constanteExclusao = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoContrato().getSimpleName());
		hql.append(" as ic ");
		hql.append(" inner join fetch ic.contratoPontoConsumo contratoPontoConsumo");
		hql.append(" where (ic.contratoPaiOrigem = :idContratoPai or ic.contratoOrigem.chavePrimaria = :idContratoPai");
		hql.append(" or ic.contratoOrigem.chavePrimaria = :idContrato)");
		hql.append(" and contratoPontoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo and ic.operacao <> :operacao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idContrato", contrato.getChavePrimaria());
		query.setParameter("idContratoPai", contrato.getChavePrimariaPai());
		query.setParameter("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setParameter("operacao", constanteExclusao.getValor());

		return (IntegracaoContrato) query.uniqueResult();
	}

	@Override
	@SuppressWarnings("squid:S1192")
	public IntegracaoSistemaParametro obterIntegracaoSistemaParametro(String siglaSistema, ParametroSistema parametroSistema)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoSistemaParametro().getSimpleName());
		hql.append(" as integracaoSistemaParametro ");
		hql.append(" inner join fetch integracaoSistemaParametro.integracaoParametro.parametroSistema parametroSistema");
		hql.append(" inner join fetch integracaoSistemaParametro.integracaoSistemaFuncao.integracaoSistema integracaoSistema");
		hql.append(" where parametroSistema.chavePrimaria = :codigoParametroSistema ");
		hql.append(" and integracaoSistema.sigla = :siglaSistema ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("codigoParametroSistema", parametroSistema.getChavePrimaria());
		query.setString("siglaSistema", siglaSistema);

		return (IntegracaoSistemaParametro) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarMapeamentosAssociados(br.com.ggas.integracao.geral.IntegracaoFuncao)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoFuncaoTabela> listarMapeamentosAssociados(IntegracaoFuncao integracaoFuncao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName());
		hql.append(" as integracaoFuncaoTabela ");
		hql.append(" inner join fetch integracaoFuncaoTabela.tabela tabela");
		hql.append(" where ");
		hql.append(" integracaoFuncaoTabela.integracaoFuncao.chavePrimaria = ? ");
		hql.append(" and tabela.mapeamento = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, integracaoFuncao.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarIntegracaoMapeamento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoMapeamento> listarIntegracaoMapeamento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoMapeamento());

		if (filtro != null) {

			Long idIntegracaoSistema = (Long) filtro.get("idIntegracaoSistema");

			if ((idIntegracaoSistema != null) && idIntegracaoSistema > 0) {
				criteria.createAlias("integracaoSistema", "integracaoSistema");
				criteria.add(Restrictions.eq(INTEGRACAO_SISTEMA_CHAVE_PRIMARIA, idIntegracaoSistema));
			}

		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("codigoEntidadeSistemaGgas"));
			criteria.addOrder(Order.asc("codigoEntidadeSistemaIntegrante"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#
	 * localizarIntegracaoMapeamento(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoMapeamento> localizarIntegracaoMapeamento(Long idIntegracaoSistemaFuncao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select integracaoMapeamento ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoMapeamento().getSimpleName());
		hql.append(" as integracaoMapeamento,  ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" as integracaoSistemaFuncao,  ");
		hql.append(getClasseEntidadeIntegracaoSistema().getSimpleName());
		hql.append(" as integracaoSistema  ");
		hql.append(" where integracaoMapeamento.tabela.chavePrimaria in ");
		hql.append(" (select integracaoFuncaoTabela.tabela.chavePrimaria from ")
				.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName()).append(" ");
		hql.append("  as integracaoFuncaoTabela, ");
		hql.append(" ").append(getClasseEntidadeIntegracaoFuncao().getSimpleName()).append(" ");
		hql.append("  as integracaoFuncao, ");
		hql.append(" ").append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName()).append(" ");
		hql.append("  as integracaoSistemaFuncao ");
		hql.append("  where integracaoFuncaoTabela.integracaoFuncao.chavePrimaria = integracaoFuncao.chavePrimaria  ");
		hql.append("  and integracaoSistemaFuncao.integracaoFuncao.chavePrimaria = integracaoFuncao.chavePrimaria ");
		hql.append("  and integracaoSistemaFuncao.chavePrimaria = ?) ");
		hql.append("  and integracaoSistemaFuncao.chavePrimaria = ? ");
		hql.append("  and integracaoSistemaFuncao.integracaoSistema.chavePrimaria = integracaoSistema.chavePrimaria ");
		hql.append("  and integracaoMapeamento.integracaoSistema.chavePrimaria = integracaoSistema.chavePrimaria ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idIntegracaoSistemaFuncao);
		query.setLong(1, idIntegracaoSistemaFuncao);
		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterIntegracaoFuncao(long)
	 */
	@Override
	public IntegracaoFuncao obterIntegracaoFuncao(long chavePrimaria) throws NegocioException {

		return (IntegracaoFuncao) super.obter(chavePrimaria, getClasseEntidadeIntegracaoFuncao());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterIntegracaoSistemaFuncao(long)
	 */
	@Override
	public IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(long chavePrimaria) throws NegocioException {

		return (IntegracaoSistemaFuncao) super.obter(chavePrimaria, getClasseEntidadeIntegracaoSistemaFuncao());
	}

	@Override
	@SuppressWarnings("squid:S1192")
	public IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(String siglaSistema) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" as integracaoSistemaFuncao ");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoSistema sistema");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoFuncao funcao");
		hql.append(" where ");
		hql.append(" funcao.sigla = ? ");
		hql.append(" and integracaoSistemaFuncao.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, siglaSistema);
		return (IntegracaoSistemaFuncao) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterIntegracaoSistemaFuncao(long, long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(long chaveSistema, long chaveFuncao) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" as integracaoSistemaFuncao ");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoSistema sistema");
		hql.append(" inner join fetch integracaoSistemaFuncao.integracaoFuncao funcao");
		hql.append(" where ");
		hql.append(" sistema.chavePrimaria = ? ");
		hql.append(" and funcao.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveSistema);
		query.setLong(1, chaveFuncao);
		return (IntegracaoSistemaFuncao) query.uniqueResult();

	}

	/**
	 * Update hq lto find funcao tabela.
	 *
	 * @param hql
	 *            the hql
	 * @param flag
	 *            the flag
	 * @return the string
	 */
	public String updateHQLtoFindFuncaoTabela(StringBuilder hql, String flag) {

		if (flag != null && "S".equals(flag)) {
			if (hql.toString().contains("where")) {
				hql.append(" and funcao.chavePrimaria in (select integracaoFuncaoTabela.integracaoFuncao.chavePrimaria from ")
						.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName()).append(" integracaoFuncaoTabela ) ");
			} else {
				hql.append(" where funcao.chavePrimaria in (select integracaoFuncaoTabela.integracaoFuncao.chavePrimaria from ")
						.append(getClasseEntidadeIntegracaoFuncaoTabela().getSimpleName()).append(" integracaoFuncaoTabela ) ");
			}
		} else {
			return hql.toString();
		}
		return hql.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterFuncao(java.lang.Long)
	 */
	@Override
	public Collection<IntegracaoFuncao> obterFuncao(Long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseIntegracaoFuncao().getSimpleName());
		if (chavePrimaria != null && chavePrimaria != 0) {
			hql.append(" where chavePrimaria = ? ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(0, chavePrimaria);
		} else {
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		}
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultaSistemaFuncao(java.lang.Long)
	 */
	@Override
	public IntegracaoSistemaFuncao consultaSistemaFuncao(Long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseIntegracaoSistemaFuncao().getSimpleName());
		if (chavePrimaria != null && chavePrimaria != 0) {
			hql.append(" sistemaFuncao ");
			hql.append(" inner join fetch sistemaFuncao.integracaoFuncao ");
			hql.append(" where sistemaFuncao.chavePrimaria = :chavePrimaria");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(CHAVE_PRIMARIA, chavePrimaria);
		} else {
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		}

		return (IntegracaoSistemaFuncao) query.setMaxResults(1).uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.exportacaodados.impl
	 * .ControladorConsultaImpl
	 * #listarEntidadeReferenciada()
	 */
	@Override
	public byte[] exportarTabelaIntegracaoParaCSV(List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO, String separador)
					throws NegocioException {

		StringBuilder sb = new StringBuilder();
		StringBuilder cabecalho = new StringBuilder();

		if (listaAcompanhamentoIntegracaoVO != null && !listaAcompanhamentoIntegracaoVO.isEmpty()) {
			AcompanhamentoIntegracaoVO[] arrayAcompanhamentoIntegracaoVO = Util.primeiroElemento(listaAcompanhamentoIntegracaoVO);

			for (AcompanhamentoIntegracaoVO acompanhamentoIntegracaoVO : arrayAcompanhamentoIntegracaoVO) {
				cabecalho.append("\"").append(acompanhamentoIntegracaoVO.getNomeColuna()).append("\"").append(separador);
			}
			sb.append(cabecalho).append("\r\n");

			for (AcompanhamentoIntegracaoVO[] aRrayAcompanhamentoIntegracaoVO : listaAcompanhamentoIntegracaoVO) {
				StringBuilder linha = new StringBuilder();
				for (AcompanhamentoIntegracaoVO dados : aRrayAcompanhamentoIntegracaoVO) {
					linha.append(dados.getValorColuna());
					linha.append(separador);
				}
				sb.append(linha).append("\r\n");
			}

		} else {
			throw new NegocioException(ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS, true);
		}

		return sb.toString().getBytes();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarDestinatariosEmail(br.com.ggas.integracao.geral.IntegracaoSistemaFuncao)
	 */
	@Override
	public void validarDestinatariosEmail(IntegracaoSistemaFuncao integracaoSistemaFuncao) throws NegocioException {

		if ((integracaoSistemaFuncao.getDestinatariosEmail() != null)
						&& (!StringUtils.isEmpty(integracaoSistemaFuncao.getDestinatariosEmail()))) {
			String destinatarioEmail = integracaoSistemaFuncao.getDestinatariosEmail().replaceAll(",", ";");
			String[] emails = destinatarioEmail.split(";");
			if (emails.length != 0) {
				for (String email : emails) {
					if (!Util.validarDominio(email.trim(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
						throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, IntegracaoSistemaFuncao.EMAIL);
					}
				}
			} else {
				if (!Util.validarDominio(destinatarioEmail, Constantes.EXPRESSAO_REGULAR_EMAIL)) {
					throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, IntegracaoSistemaFuncao.EMAIL);
				}
			}
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, IntegracaoSistemaFuncao.EMAIL);
		}
	}

	/**
	 * Validar existencia entidades.
	 *
	 * @param classeOrigem
	 *            the classe origem
	 * @param objetoValor
	 *            the objeto valor
	 * @param classe
	 *            the classe
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public String validarExistenciaEntidades(EntidadeNegocio classeOrigem, EntidadeNegocio objetoValor, Class<?> classe,
					Map<String, Object[]> mapaInconsistencias) throws NegocioException {

		return super.validarExistenciaEntidades(classeOrigem, objetoValor, mapaInconsistencias);
	}

	/**
	 * Validar integracao.
	 *
	 * @param objetoValor
	 *            the objeto valor
	 * @param classe
	 *            the classe
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public Boolean validarIntegracao(EntidadeNegocio objetoValor, Class<?> classe, Map<String, Object[]> mapaInconsistencias)
					throws NegocioException, ConcorrenciaException {

		Boolean possuiErro = Boolean.FALSE;
		String descricaoErro = Util.validacao(objetoValor, mapaInconsistencias);

		if (descricaoErro != null && !descricaoErro.isEmpty()) {
			possuiErro = Boolean.TRUE;
			this.atualizarIntegracaoComErro(classe, (Integracao) objetoValor, descricaoErro);
		}
		return possuiErro;
	}

	/**
	 * Validar integracao bem movimentacao.
	 *
	 * @param integracaoBemMovimentacao
	 *            the integracao bem movimentacao
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 */
	public Map<String, Object> validarIntegracaoBemMovimentacao(IntegracaoBemMovimentacao integracaoBemMovimentacao,
					Map<String, Object[]> mapaInconsistencias) throws NegocioException, ConcorrenciaException, IllegalAccessException,
					InvocationTargetException, NoSuchMethodException {

		Map<String, Object> mapaIntegracaoBemMovimentacao = new LinkedHashMap<String, Object>();
		Boolean possuiErro = this.validarIntegracao(integracaoBemMovimentacao, IntegracaoBemMovimentacaoImpl.class, mapaInconsistencias);
		String descricaoErro = null;

		if (possuiErro != null && !possuiErro) {

			MovimentacaoMedidor movimentacaoMedidor = null;
			CorretorVazaoMovimentacao corretorVazaoMovimentacao = null;

			IntegracaoBem integracaoBem = integracaoBemMovimentacao.getBem();
			mapaIntegracaoBemMovimentacao.put("integracaoBem", integracaoBem);

			if (integracaoBem instanceof IntegracaoBemMedidorImpl) {

				movimentacaoMedidor = (MovimentacaoMedidor) this.criarMovimentacaoMedidor();
				mapaIntegracaoBemMovimentacao.put("movimentacaoMedidor", movimentacaoMedidor);
				descricaoErro = super.validarExistenciaEntidades(movimentacaoMedidor, integracaoBemMovimentacao, mapaInconsistencias);
			} else {

				corretorVazaoMovimentacao = (CorretorVazaoMovimentacao) this.criarCorretorVazaoMovimentacao();
				mapaIntegracaoBemMovimentacao.put("corretorVazaoMovimentacao", corretorVazaoMovimentacao);
				descricaoErro = super.validarExistenciaEntidades(corretorVazaoMovimentacao, integracaoBemMovimentacao, mapaInconsistencias);
			}

			if (descricaoErro != null && !descricaoErro.isEmpty()) {
				possuiErro = Boolean.TRUE;
				this.atualizarIntegracaoComErro(IntegracaoBemMovimentacaoImpl.class, integracaoBemMovimentacao, descricaoErro);
			} else {

				// validar a entidade SituacaoBem
				Map<String, ExistenciaClasseVO> mapValidarEntidades = new HashMap<String, ExistenciaClasseVO>();
				Object objSituacaoBem =
								IntegracaoBemMovimentacaoImpl.class.getDeclaredMethod(Util.montarNomeGet("SituacaoBem")).invoke(
												integracaoBemMovimentacao);
				ExistenciaClasseVO vo = null;
				if (objSituacaoBem != null) {
					vo = new ExistenciaClasseVO();
					vo.setChavePrimaria((Long) objSituacaoBem);
					vo.setNomeClasse(Constantes.CLASSE_SITUACAO_MEDIDOR);
					mapValidarEntidades.put(Constantes.SITUACAO_MEDIDOR, vo);
				}
				Object objCodigoMovimentacao =
								IntegracaoBemMovimentacaoImpl.class.getDeclaredMethod(Util.montarNomeGet("CodigoMovimentacao")).invoke(
												integracaoBemMovimentacao);

				if (objCodigoMovimentacao != null) {

					vo = new ExistenciaClasseVO();
					vo.setChavePrimaria((Long) objCodigoMovimentacao);
					vo.setNomeClasse(Constantes.CLASSE_OPERACAO_MEDIDOR);
					mapValidarEntidades.put(Constantes.OPERACAO_MEDIDOR, vo);
				}

				descricaoErro = super.validarExistenciaEntidades(mapValidarEntidades, mapaInconsistencias);

				if (descricaoErro != null && !descricaoErro.isEmpty()) {
					possuiErro = Boolean.TRUE;
					this.atualizarIntegracaoComErro(IntegracaoBemMovimentacaoImpl.class, integracaoBemMovimentacao, descricaoErro);
				}
			}
		}

		mapaIntegracaoBemMovimentacao.put("possuiErro", possuiErro);

		return mapaIntegracaoBemMovimentacao;
	}

	/**
	 * Validar integracao usuario.
	 *
	 * @param integracao
	 *            the integracao
	 * @param descricaoErroTmp
	 *            the descricao erro
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @param classe
	 *            the classe
	 * @return the boolean
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public Boolean validarIntegracaoUsuario(Integracao integracao, String descricaoErroTmp, Map<String, Object[]> mapaInconsistencias,
					Class<?> classe) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, NegocioException,
					ConcorrenciaException {

		Boolean possuiErro = Boolean.FALSE;
		String descricaoErro = descricaoErroTmp;
		Map<String, ExistenciaClasseVO> mapValidarEntidades = new HashMap<String, ExistenciaClasseVO>();
		Object obj = IntegracaoImpl.class.getDeclaredMethod(Util.montarNomeGet("Usuario")).invoke(integracao);

		if (obj != null) {

			Long idUsuario = null;

			try {

				String objAux = String.valueOf(obj);
				idUsuario = Long.valueOf(objAux);

			} catch (NumberFormatException e) {

				descricaoErro = Constantes.USUARIO_INVALIDO;

				if (mapaInconsistencias != null && !mapaInconsistencias.isEmpty()) {

					for (Map.Entry<String, Object[]> entry : mapaInconsistencias.entrySet()) {

						Object[] listaAux = entry.getValue();
						String constante = entry.getKey();

						if (constante.equals(Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA)) {

							Integer contadorInconsistencias = (Integer) listaAux[0];
							contadorInconsistencias++;
							listaAux[0] = contadorInconsistencias;

							mapaInconsistencias.put(constante, listaAux);
						} else {

							listaAux = new Object[CONSTANTE_NUMERO_DOIS];
							listaAux[0] = 1;
							listaAux[1] = Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA;
							mapaInconsistencias.put(Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA, listaAux);
						}
					}
				} else {

					Object[] listaAux = new Object[CONSTANTE_NUMERO_DOIS];
					listaAux[0] = 1;
					listaAux[1] = Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA;
					if (mapaInconsistencias != null) {
						mapaInconsistencias.put(Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA, listaAux);
					}
				}

			}

			if (descricaoErro != null && !descricaoErro.isEmpty()) {

				possuiErro = Boolean.TRUE;
				this.atualizarIntegracaoComErro(classe, integracao, descricaoErro);

			} else {
				ExistenciaClasseVO vo = new ExistenciaClasseVO();
				vo.setChavePrimaria(idUsuario);
				vo.setNomeClasse(Constantes.CLASSE_USUARIO);
				mapValidarEntidades.put(Constantes.USUARIO, vo);
				descricaoErro = super.validarExistenciaEntidades(mapValidarEntidades, mapaInconsistencias);

				if (descricaoErro != null && !descricaoErro.isEmpty()) {
					possuiErro = Boolean.TRUE;
					this.atualizarIntegracaoComErro(classe, integracao, descricaoErro);
				}
			}
		}

		return possuiErro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#registrarMovimentacaoBens(java.util.Collection,
	 * br.com.ggas.constantesistema.ControladorConstanteSistema, br.com.ggas.batch.Processo, java.lang.StringBuilder, java.util.List)
	 */
	@Override
	public void registrarMovimentacaoBens(Collection<IntegracaoBemMovimentacao> listaIntegracaoBemMovimentacao,
					ControladorConstanteSistema controladorConstanteSistema, Processo processo, StringBuilder logProcessamento,
					List<String> listaSistemasIntegrantes) throws NegocioException, ConcorrenciaException, IOException,
					IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		ResumoIntegracaoVO resumoIntegracaoVO = new ResumoIntegracaoVO();
		Map<String, Object[]> mapaInconsistencias = new LinkedHashMap<String, Object[]>();
		Integer quantidadeRegistrosProcessados = 0;
		Integer quantidadeRegistrosIncluidos = 0;

		ConstanteSistema constanteSistemaSituacaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

		for (Iterator<IntegracaoBemMovimentacao> iterator = listaIntegracaoBemMovimentacao.iterator(); iterator.hasNext();) {

			IntegracaoBemMovimentacao integracaoBemMovimentacao = iterator.next();

			Map<String, Object> mapaIntegracaoBemMovimentacao =
							this.validarIntegracaoBemMovimentacao(integracaoBemMovimentacao, mapaInconsistencias);

			if (mapaIntegracaoBemMovimentacao != null && !mapaIntegracaoBemMovimentacao.isEmpty()) {

				Boolean possuiErro = (Boolean) mapaIntegracaoBemMovimentacao.get("possuiErro");

				if (possuiErro != null && !possuiErro) {

					IntegracaoBem integracaoBem = (IntegracaoBem) mapaIntegracaoBemMovimentacao.get("integracaoBem");

					MovimentacaoMedidor movimentacaoMedidor =
									(MovimentacaoMedidor) mapaIntegracaoBemMovimentacao.get("movimentacaoMedidor");

					CorretorVazaoMovimentacao corretorVazaoMovimentacao =
									(CorretorVazaoMovimentacao) mapaIntegracaoBemMovimentacao.get("corretorVazaoMovimentacao");

					ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

					Funcionario funcionario = null;
					if (integracaoBemMovimentacao.getFuncionario() != null) {
						funcionario = (Funcionario) this.getEntidadeFuncionario(integracaoBemMovimentacao.getFuncionario());
					}

					MedidorLocalArmazenagem medidorLocalArmazenagem =
									(MedidorLocalArmazenagem) this.getEntidadeMedidorLocalArmazenagem(integracaoBemMovimentacao
													.getLocalArmazenagemDestino());
					medidorLocalArmazenagem = controladorMedidor.obterMedidorLocalArmazenagem(medidorLocalArmazenagem.getChavePrimaria());

					MedidorLocalArmazenagem medidorLocalArmazenagemOrigem =
									(MedidorLocalArmazenagem) this.getEntidadeMedidorLocalArmazenagem(integracaoBemMovimentacao
													.getLocalArmazenagemOrigem());
					medidorLocalArmazenagemOrigem =
									controladorMedidor.obterMedidorLocalArmazenagem(medidorLocalArmazenagemOrigem.getChavePrimaria());

					MotivoMovimentacaoMedidor motivoMovimentacao =
									(MotivoMovimentacaoMedidor) this.getEntidadeMotivoMovimentacaoMedidor(integracaoBemMovimentacao
													.getMotivoMovimentacao());

					SituacaoMedidor situacaoMedidor = null;
					if (integracaoBemMovimentacao.getSituacaoBem() != null) {

						situacaoMedidor = (SituacaoMedidor) this.getEntidadeSituacaoMedidor(integracaoBemMovimentacao.getSituacaoBem());
					}

					if (movimentacaoMedidor != null) {

						movimentacaoMedidor.setDataMovimento(integracaoBemMovimentacao.getDataOperacao());

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("chavesSistemasIntegrantes", listaSistemasIntegrantes.toArray());
						filtro.put(HABILITADO, Boolean.TRUE);
						filtro.put("processado", constanteSistemaSituacaoProcessado.getValor());
						filtro.put(CHAVE_PRIMARIA, integracaoBem.getChavePrimaria());

						String ordenacao = "integracao.class asc, integracao.operacao asc";

						Collection<IntegracaoBem> listaIntegracaoBem =
										(Collection<IntegracaoBem>) (Collection<?>) this.consultarIntegracao(filtro,
														IntegracaoBemImpl.class, ordenacao, "");

						Long bemDestino = null;
						quantidadeRegistrosProcessados++;
						if (listaIntegracaoBem != null && !listaIntegracaoBem.isEmpty()) {
							IntegracaoBem aux = new IntegracaoBemImpl();
							BeanUtils.copyProperties(listaIntegracaoBem.iterator().next(), aux);
							bemDestino = aux.getBemDestino();
						} else {
							Util.atualizaMapaInconsistencias(mapaInconsistencias,
											concatenaMensagemErro(Constantes.ERRO_NEGOCIO_BEM_NAO_CADASTRADO, ""));
							String mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_BEM_NAO_CADASTRADO);
							this.atualizarIntegracaoComErro(IntegracaoBemMovimentacaoImpl.class, integracaoBemMovimentacao, mensagemErro);
						}

						if (bemDestino != null) {

							Medidor medidor = (Medidor) super.obter(bemDestino, MedidorImpl.class, Boolean.TRUE, "localArmazenagem");

							movimentacaoMedidor.setMedidor(medidor);
							movimentacaoMedidor.setMotivoMovimentacaoMedidor(motivoMovimentacao);
							movimentacaoMedidor.setDescricaoParecer(integracaoBemMovimentacao.getDescricao());
							movimentacaoMedidor.setLocalArmazenagemOrigem(medidorLocalArmazenagemOrigem);
							movimentacaoMedidor.setLocalArmazenagemDestino(medidorLocalArmazenagem);
							movimentacaoMedidor.setFuncionario(funcionario);

							super.inserir(movimentacaoMedidor);

							medidor.setLocalArmazenagem(medidorLocalArmazenagem);

							if (situacaoMedidor != null) {

								medidor.setSituacaoMedidor(situacaoMedidor);
							}

							logProcessamento.append(Constantes.PULA_LINHA).append("Número do Medidor: ").append(medidor.getNumeroSerie())
									.append(Constantes.PULA_LINHA).append("Local de Armazenagem Origem: ")
									.append(medidorLocalArmazenagemOrigem.getDescricao()).append(Constantes.PULA_LINHA)
									.append("Local de Armazenagem Destino: ").append(medidorLocalArmazenagem.getDescricao());
							if (situacaoMedidor != null) {
								logProcessamento.append(Constantes.PULA_LINHA).append("Situação: ").append(situacaoMedidor.getDescricao());
							} else {
								logProcessamento.append(Constantes.PULA_LINHA).append("Situação: ")
										.append(medidor.getSituacaoMedidor().getDescricao());
							}
							logProcessamento.append(Constantes.PULA_LINHA).append(" ");

							super.atualizar(medidor, MedidorImpl.class);

							integracaoBemMovimentacao.setSituacao(constanteSistemaSituacaoProcessado.getValor());
							super.atualizar(integracaoBemMovimentacao, IntegracaoBemMovimentacaoImpl.class);
							quantidadeRegistrosIncluidos++;
						}

					} else if (corretorVazaoMovimentacao != null) {

						corretorVazaoMovimentacao.setDataMovimento(integracaoBemMovimentacao.getDataOperacao());

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("chavesSistemasIntegrantes", listaSistemasIntegrantes.toArray());
						filtro.put(HABILITADO, Boolean.TRUE);
						filtro.put("processado", constanteSistemaSituacaoProcessado.getValor());
						filtro.put(CHAVE_PRIMARIA, integracaoBem.getChavePrimaria());

						String ordenacao = "integracao.class asc, integracao.operacao asc";

						Collection<IntegracaoBem> listaIntegracaoBem =
										(Collection<IntegracaoBem>) (Collection<?>) this.consultarIntegracao(filtro,
														IntegracaoBemImpl.class, ordenacao, "");
						Long bemDestino = null;
						quantidadeRegistrosProcessados++;
						if (listaIntegracaoBem != null && !listaIntegracaoBem.isEmpty()) {

							IntegracaoBem aux = new IntegracaoBemImpl();
							BeanUtils.copyProperties(listaIntegracaoBem.iterator().next(), aux);
							bemDestino = aux.getBemDestino();
						} else {
							Util.atualizaMapaInconsistencias(mapaInconsistencias,
											concatenaMensagemErro(Constantes.ERRO_NEGOCIO_BEM_NAO_CADASTRADO, ""));
							String mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_BEM_NAO_CADASTRADO);
							this.atualizarIntegracaoComErro(IntegracaoBemMovimentacaoImpl.class, integracaoBemMovimentacao, mensagemErro);
						}

						if (bemDestino != null) {

							VazaoCorretor vazaoCorretor =
											(VazaoCorretor) super.obter(integracaoBem.getBemDestino(), VazaoCorretorImpl.class,
															Boolean.TRUE, "localArmazenagem");

							corretorVazaoMovimentacao.setCorretorVazao(vazaoCorretor);
							corretorVazaoMovimentacao.setMotivoMovimentacao(motivoMovimentacao);
							corretorVazaoMovimentacao.setDescricaoParecer(integracaoBemMovimentacao.getDescricao());
							corretorVazaoMovimentacao.setLocalArmazenagemOrigem(medidorLocalArmazenagemOrigem);
							corretorVazaoMovimentacao.setLocalArmazenagemDestino(medidorLocalArmazenagem);
							corretorVazaoMovimentacao.setFuncionario(funcionario);

							super.inserir(corretorVazaoMovimentacao);

							vazaoCorretor.setLocalArmazenagem(medidorLocalArmazenagem);

							if (situacaoMedidor != null) {

								vazaoCorretor.setSituacaoMedidor(situacaoMedidor);
							}

							logProcessamento.append(Constantes.PULA_LINHA).append("Número do Corretor de Vazão: ")
											.append(vazaoCorretor.getNumeroSerie());
							logProcessamento.append(Constantes.PULA_LINHA).append("Local de Armazenagem Origem: ")
											.append(medidorLocalArmazenagemOrigem.getDescricao());
							logProcessamento.append(Constantes.PULA_LINHA).append("Local de Armazenagem Destino: ")
											.append(medidorLocalArmazenagem.getDescricao());
							if (situacaoMedidor != null) {
								logProcessamento.append(Constantes.PULA_LINHA).append("Situação: ")
											.append(situacaoMedidor.getDescricao());
							} else {
								logProcessamento.append(Constantes.PULA_LINHA).append("Situação: ")
											.append(vazaoCorretor.getSituacaoMedidor().getDescricao());
							}
							logProcessamento.append(Constantes.PULA_LINHA).append(" ");

							super.atualizar(vazaoCorretor, VazaoCorretorImpl.class);

							integracaoBemMovimentacao.setSituacao(constanteSistemaSituacaoProcessado.getValor());
							super.atualizar(integracaoBemMovimentacao, IntegracaoBemMovimentacaoImpl.class);
							quantidadeRegistrosIncluidos++;
						}

					}

				}

			}

		}

		resumoIntegracaoVO.setQuantidadeRegistrosProcessados(quantidadeRegistrosProcessados);
		resumoIntegracaoVO.setQuantidadeRegistrosIncluidos(quantidadeRegistrosIncluidos);
		resumoIntegracaoVO.setMapaInconsistencias(mapaInconsistencias);

		this.preencherLogMapaInconsistencias(logProcessamento, resumoIntegracaoVO, false);
	}

	@Override
	public void preencherLogMapaInconsistencias(StringBuilder logProcessamento, ResumoIntegracaoVO resumoIntegracaoVO,
					boolean logCadastroBens) throws NegocioException, IOException {

		preencherLogMapaInconsistencias(logProcessamento, resumoIntegracaoVO, logCadastroBens, null);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#preencherLogMapaInconsistencias(java.lang.StringBuilder,
	 * br.com.ggas.integracao.geral.ResumoIntegracaoVO, boolean)
	 */
	@Override
	public void preencherLogMapaInconsistencias(StringBuilder logProcessamento, ResumoIntegracaoVO resumoIntegracaoVO,
					boolean logCadastroBens, String detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico) throws NegocioException,
					IOException {

		Map<String, Object[]> mapaInconsistencias = resumoIntegracaoVO.getMapaInconsistencias();
		InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(Constantes.MENSAGEM_PROPRIEDADES);
		Properties propriedades = new Properties();
		propriedades.load(stream);

		StringBuilder logProcessamentoAux = new StringBuilder();

		logProcessamento.append(LogProcessosUtil.gerarLogRegistrosProcessados(resumoIntegracaoVO.getQuantidadeRegistrosProcessados()));
		logProcessamento.append(LogProcessosUtil.gerarLogRegistrosIncluidos(resumoIntegracaoVO.getQuantidadeRegistrosIncluidos()));

		if (logCadastroBens) {
			logProcessamento.append(LogProcessosUtil.gerarLogRegistrosAlterados(resumoIntegracaoVO.getQuantidadeRegistrosAlterados()));
			logProcessamento.append(LogProcessosUtil.gerarLogRegistrosExcluidos(resumoIntegracaoVO.getQuantidadeRegistrosExcluidos()));
		}
		int contadorInconsistencias = 0;
		if (mapaInconsistencias != null && !mapaInconsistencias.isEmpty()) {
			for (Map.Entry<String, Object[]> entry : mapaInconsistencias.entrySet()) {

				Object[] listaAux = entry.getValue();
				Integer contador = (Integer) listaAux[0];
				contadorInconsistencias = contadorInconsistencias + contador;

				String constante = entry.getKey();
				String mensagem = (String) propriedades.get(constante);

				logProcessamentoAux.append(Constantes.STRING_TABULACAO);
				logProcessamentoAux.append(mensagem);
				logProcessamentoAux.append(Constantes.STRING_DOIS_PONTOS_ESPACO);
				logProcessamentoAux.append(contador);
				logProcessamentoAux.append(Constantes.PULA_LINHA);

			}

			if (!logCadastroBens) {
				resumoIntegracaoVO.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVO.getQuantidadeRegistrosProcessados()
								- resumoIntegracaoVO.getQuantidadeRegistrosIncluidos());
			}

		}

		Integer quantidadeRegistrosInconsistentes = resumoIntegracaoVO.getQuantidadeRegistrosInconsistentes();

		String logInconsistencias =
						gerarLogInconsistencias(quantidadeRegistrosInconsistentes, contadorInconsistencias,
										detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico);

		logProcessamento.append(logInconsistencias);

	}

	private String gerarLogInconsistencias(Integer quantidadeRegistrosInconsistentes, int contadorInconsistencias,
			String detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico) {

		StringBuilder logInconsistencias = new StringBuilder();

		logInconsistencias.append(LogProcessosUtil.gerarLogRegistrosInconsistentes(quantidadeRegistrosInconsistentes));
		logInconsistencias.append(Constantes.PULA_LINHA);
		logInconsistencias.append(LogProcessosUtil
						.gerarLogListaEventosInconsistencias(detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico));
		logInconsistencias.append(LogProcessosUtil.gerarLogTotalInconsistencias(contadorInconsistencias));
		logInconsistencias.append(Constantes.PULA_LINHA);

		return logInconsistencias.toString();
	}

	/**
	 * Valida a existência de um medidor a partir do número de série e marca do medidor.
	 *
	 * @param integracaoBemMedidor
	 *            - bean com os valores para serem validados
	 * @return - true caso exista, false caso contrário.
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public boolean validarExistenciaMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor) throws NegocioException, ConcorrenciaException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		return controladorMedidor.validarMedidorExistentePorNumeroSerieEMarca(null, integracaoBemMedidor.getNumeroSerie(),
						integracaoBemMedidor.getMarcaMedidor());

	}

	/**
	 * Valida a existência de um corretor de vazão a partir do número de série e marca do medidor.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @return - true caso exista, false caso contrário.
	 * @throws GGASException
	 *             the GGas exception
	 */
	public boolean validarExistenciaVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor) throws GGASException {

		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();

		return controladorVazaoCorretor.validarVazaoCorretorExistentePorNumeroSerieEMarca(null,
						integracaoBemVazaoCorretor.getNumeroSerie(), integracaoBemVazaoCorretor.getMarcaCorretor());

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#validarInsercaoMedidor(
	 * br.com.ggas.integracao.geral.impl.IntegracaoBemMedidorImpl, br.com.ggas.integracao.geral.impl.IntegracaoWrapper)
	 */
	@Override
	public boolean validarInsercaoMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Map<String, Object[]> mapaInconsistencias)
					throws GGASException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		String mensagemErro = "";
		Boolean possuiErro = this.validarIntegracao(integracaoBemMedidor, IntegracaoBemMedidorImpl.class, mapaInconsistencias);

		if (!possuiErro) {

			boolean medidorExistente = validarExistenciaMedidor(integracaoBemMedidor);
			if (!medidorExistente) {

				ControladorConstanteSistema ctlConstanteSistema =
								(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				String situacaoCorretor = ctlConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL);

				if (!integracaoBemMedidor.getSituacaoBem().toString().equals(situacaoCorretor)) {

					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.SITUACAOMEDIDOR));
				}
				String entidadesInexistentes = validarExistenciaEntidades(new MedidorImpl(), integracaoBemMedidor, mapaInconsistencias);

				// valida o ano
				if (integracaoBemMedidor.getAnoFabricacao() != null
								&& (integracaoBemMedidor.getAnoFabricacao() > Calendar.getInstance().get(Calendar.YEAR))) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.ANOFABRICACAO));
				}

				// valida o ano maior que aquisição
				if (integracaoBemMedidor.getAnoFabricacao() != null
								&& integracaoBemMedidor.getDataAquisicao() != null
								&& (integracaoBemMedidor.getAnoFabricacao() > new DateTime(integracaoBemMedidor.getDataAquisicao())
												.getYear())) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.ANOFABRICACAO));
				}

				if ((integracaoBemMedidor.getPressaoMaxima() != null && integracaoBemMedidor.getUnidadePressaoMaximaTrabalho() == null)
								|| (integracaoBemMedidor.getPressaoMaxima() == null && integracaoBemMedidor
												.getUnidadePressaoMaximaTrabalho() != null)) {
					if (integracaoBemMedidor.getPressaoMaxima() == null) {
						Util.atualizaMapaInconsistencias(
										mapaInconsistencias,
										concatenaMensagemErro(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS,
														Constantes.PRESSAOMAXIMA));
					} else {
						Util.atualizaMapaInconsistencias(
										mapaInconsistencias,
										concatenaMensagemErro(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS,
														Constantes.UNIDADEPRESSAOMAXIMA));
					}
				}

				// valida o local de armazenagem
				if (integracaoBemMedidor.getLocalArmazenagem() == null || integracaoBemMedidor.getLocalArmazenagem().intValue() == 0) {
					Util.atualizaMapaInconsistencias(
									mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS,
													Constantes.LOCALARMAZENAGEM));
				}

				mensagemErro = Util.gerarMensagemErroConsolidada(mapaInconsistencias, mensagens);
				mensagemErro = mensagemErro.concat(entidadesInexistentes);
			} else {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO, " "));
				mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO);

			}
			if (!mensagemErro.isEmpty()) {
				atualizarIntegracaoComErro(IntegracaoBemMedidorImpl.class, integracaoBemMedidor, mensagemErro);
				return false;
			} else {
				Medidor medidor = converterIntegracaoBemMedidorParaMedidor(integracaoBemMedidor);

				Long idMedidor = controladorMedidor.inserir(medidor);

				atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemMedidor, idMedidor);
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Converter integracao bem medidor para medidor.
	 *
	 * @param integracaoBemMedidor
	 *            the integracao bem medidor
	 * @return the medidor
	 */
	private Medidor converterIntegracaoBemMedidorParaMedidor(IntegracaoBemMedidor integracaoBemMedidor) {

		ServiceLocator service = ServiceLocator.getInstancia();
		Medidor medidor = (Medidor) service.getBeanPorID(Medidor.BEAN_ID_MEDIDOR);
		medidor.setNumeroSerie(integracaoBemMedidor.getNumeroSerie());
		medidor.setSituacaoMedidor((SituacaoMedidor) service.getBeanPorIDeSeteChavePrimaria(SituacaoMedidor.BEAN_ID_SITUACAO_MEDIDOR,
						integracaoBemMedidor.getSituacaoBem()));

		medidor.setTipoMedidor((TipoMedidor) service.getBeanPorIDeSeteChavePrimaria(TipoMedidor.BEAN_ID_TIPO_MEDIDOR,
						integracaoBemMedidor.getTipoMedidor()));

		medidor.setMarcaMedidor((MarcaMedidor) service.getBeanPorIDeSeteChavePrimaria(MarcaMedidor.BEAN_ID_MARCA_MEDIDOR,
						integracaoBemMedidor.getMarcaMedidor()));
		if (integracaoBemMedidor.getDiametroMedidor() != null) {
			medidor.setDiametroMedidor((DiametroMedidor) service.getBeanPorIDeSeteChavePrimaria(DiametroMedidor.BEAN_ID_DIAMETRO_MEDIDOR,
							integracaoBemMedidor.getDiametroMedidor()));
		}
		medidor.setCapacidadeMinima((CapacidadeMedidor) service.getBeanPorIDeSeteChavePrimaria(
						CapacidadeMedidor.BEAN_ID_CAPACIDADE_MEDIDOR, integracaoBemMedidor.getCapacidadeMinima()));
		medidor.setCapacidadeMaxima((CapacidadeMedidor) service.getBeanPorIDeSeteChavePrimaria(
						CapacidadeMedidor.BEAN_ID_CAPACIDADE_MEDIDOR, integracaoBemMedidor.getCapacidadeMaxima()));
		if (integracaoBemMedidor.getAnoFabricacao() != null) {
			medidor.setAnoFabricacao(Integer.parseInt(integracaoBemMedidor.getAnoFabricacao().toString()));
		}
		if (integracaoBemMedidor.getDataAquisicao() != null) {
			medidor.setDataAquisicao(integracaoBemMedidor.getDataAquisicao());
		}
		if (integracaoBemMedidor.getDataMaximaInstalacao() != null) {
			medidor.setDataMaximaInstalacao(integracaoBemMedidor.getDataMaximaInstalacao());
		}
		if (integracaoBemMedidor.getDataUltimaCalibracao() != null) {
			medidor.setDataUltimaCalibracao(integracaoBemMedidor.getDataUltimaCalibracao());
		}
		if (integracaoBemMedidor.getNumeroAnosCalibracao() != null) {
			medidor.setAnoCalibracao(Util.converteLongParaInteger(integracaoBemMedidor.getNumeroAnosCalibracao()));
		}
		if (integracaoBemMedidor.getNumeroTombamento() != null) {
			medidor.setTombamento(integracaoBemMedidor.getNumeroTombamento());
		}
		if (integracaoBemMedidor.getLocalArmazenagem() != null) {
			medidor.setLocalArmazenagem((MedidorLocalArmazenagem) service.getBeanPorIDeSeteChavePrimaria(
							MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, integracaoBemMedidor.getLocalArmazenagem()));
		}
		medidor.setDigito(Util.converteLongParaInteger(integracaoBemMedidor.getNumeroDigito()));
		if (integracaoBemMedidor.getFaixaTemperaturaTrabalho() != null) {
			medidor.setFaixaTemperaturaTrabalho((FaixaTemperaturaTrabalho) service.getBeanPorIDeSeteChavePrimaria(
							FaixaTemperaturaTrabalho.BEAN_ID_FAIXA_TEMPERATURA_TRABALHO, integracaoBemMedidor.getFaixaTemperaturaTrabalho()));
		}
		if (integracaoBemMedidor.getPressaoMaxima() != null) {
			medidor.setPressaoMaximaTrabalho(integracaoBemMedidor.getPressaoMaxima());
		}
		if (integracaoBemMedidor.getUnidadePressaoMaximaTrabalho() != null) {
			medidor.setUnidadePressaoMaximaTrabalho((Unidade) service.getBeanPorIDeSeteChavePrimaria(Unidade.BEAN_ID_UNIDADE,
							integracaoBemMedidor.getUnidadePressaoMaximaTrabalho()));
		}
		if (integracaoBemMedidor.getModelo() != null) {
			medidor.setModelo((ModeloMedidor) service.getBeanPorIDeSeteChavePrimaria(ModeloMedidor.BEAN_ID_MODELO_MEDIDOR,
							integracaoBemMedidor.getModelo()));
		}
		if (integracaoBemMedidor.getFatorK() != null) {
			medidor.setFatorK((FatorK) service.getBeanPorIDeSeteChavePrimaria(FatorK.BEAN_ID_FATOR_K, integracaoBemMedidor.getFatorK()));
		}
		return medidor;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarAlteracaoMedidor(br.com.ggas.integracao.bens.impl.IntegracaoBemMedidorImpl,
	 * java.util.Map)
	 */
	@Override
	public boolean validarAlteracaoMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Map<String, Object[]> mapaInconsistencias)
					throws GGASException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		Medidor medidor = null;
		String mensagemErro = "";
		String mensagemErroNova = "";
		ServiceLocator service = ServiceLocator.getInstancia();
		Boolean possuiErro = this.validarIntegracao(integracaoBemMedidor, IntegracaoBemMedidorImpl.class, mapaInconsistencias);
		if (!possuiErro) {
			if (integracaoBemMedidor.getBemDestino() == null) {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO, ""));
				mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO);
			} else {
				EntidadeNegocio entidade = controladorMedidor.obter(integracaoBemMedidor.getBemDestino(), MedidorImpl.class, true);
				if (entidade != null) {
					medidor = (Medidor) entidade;
				}
				if (medidor == null) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO, ""));
					mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO);
				} else {
					boolean medidorExistente = validarExistenciaMedidor(integracaoBemMedidor);
					if (!medidorExistente || integracaoBemMedidor.getBemDestino().equals(medidor.getChavePrimaria())) {

						mensagemErro = validarExistenciaEntidades(new MedidorImpl(), integracaoBemMedidor, mapaInconsistencias);
						if (mensagemErro.isEmpty()) {
							// Valida Situação do Medidor
							if (!integracaoBemMedidor.getSituacaoBem().equals(medidor.getSituacaoMedidor().getChavePrimaria())) {
								ConstanteSistema situacaoInstalado =
												controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_MEDIDOR_INSTALADO);
								if (Long.valueOf(situacaoInstalado.getValor()).equals(medidor.getSituacaoMedidor().getChavePrimaria())) {
									Util.atualizaMapaInconsistencias(mapaInconsistencias,
													concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_SITUACAO_MEDIDOR_INVALIDA, ""));
								} else {
									EntidadeNegocio situacaoMedidor =
													controladorMedidor.obter(integracaoBemMedidor.getSituacaoBem(),
																	SituacaoMedidorImpl.class, true);
									if (situacaoMedidor == null) {
										Util.atualizaMapaInconsistencias(
														mapaInconsistencias,
														concatenaMensagemErro(Constantes.ERRO_NEGOCIO_ENTIDADE_NAO_CADASTRADA,
																		Constantes.SITUACAOMEDIDOR));
									} else {
										medidor.setSituacaoMedidor((SituacaoMedidor) service.getBeanPorIDeSeteChavePrimaria(
														SituacaoMedidor.BEAN_ID_SITUACAO_MEDIDOR, integracaoBemMedidor.getSituacaoBem()));
									}
								}
							}
							// Valida os atributos que foram alterados
							validarAtributosMedidor(integracaoBemMedidor, medidor);

							validarDatasAlteracao(integracaoBemMedidor, medidor, mapaInconsistencias);
							mensagemErroNova = Util.gerarMensagemErroConsolidada(mapaInconsistencias, mensagens);
							mensagemErro = concatenaMensagemErro(mensagemErro, mensagemErroNova);
						}

					} else {
						Util.atualizaMapaInconsistencias(mapaInconsistencias,
										concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO, " "));
						mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_JA_CADASTRADO);
					}
				}
			}
			if (mensagemErro != null && !mensagemErro.isEmpty()) {
				atualizarIntegracaoComErro(IntegracaoBemMedidorImpl.class, integracaoBemMedidor, mensagemErro);
				return false;
			} else {
				super.atualizar(medidor, MedidorImpl.class);
				atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemMedidor, medidor.getChavePrimaria());
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Validar atributos medidor.
	 *
	 * @param integracaoBemMedidor
	 *            the integracao bem medidor
	 * @param medidor
	 *            the medidor
	 */
	private void validarAtributosMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Medidor medidor) {

		ServiceLocator service = ServiceLocator.getInstancia();
		if (!integracaoBemMedidor.getNumeroSerie().equals(medidor.getNumeroSerie())) {
			medidor.setNumeroSerie(integracaoBemMedidor.getNumeroSerie());
		}

		if (!integracaoBemMedidor.getTipoMedidor().equals(medidor.getTipoMedidor().getChavePrimaria())) {
			medidor.setTipoMedidor((TipoMedidor) service.getBeanPorIDeSeteChavePrimaria(TipoMedidor.BEAN_ID_TIPO_MEDIDOR,
							integracaoBemMedidor.getTipoMedidor()));
		}

		// Valida Marca Medidor
		if (!integracaoBemMedidor.getMarcaMedidor().equals(medidor.getMarcaMedidor().getChavePrimaria())) {
			medidor.setMarcaMedidor((MarcaMedidor) service.getBeanPorIDeSeteChavePrimaria(MarcaMedidor.BEAN_ID_MARCA_MEDIDOR,
							integracaoBemMedidor.getMarcaMedidor()));
		}

		// Valida Diametro Medidor
		if (integracaoBemMedidor.getDiametroMedidor() != null) {
			if ((medidor.getDiametroMedidor() != null && (!integracaoBemMedidor.getDiametroMedidor().equals(
							medidor.getDiametroMedidor().getChavePrimaria())))
							|| medidor.getDiametroMedidor() == null) {
				medidor.setDiametroMedidor((DiametroMedidor) service.getBeanPorIDeSeteChavePrimaria(
								DiametroMedidor.BEAN_ID_DIAMETRO_MEDIDOR, integracaoBemMedidor.getDiametroMedidor()));
			}
		} else {
			medidor.setDiametroMedidor(null);
		}

		// Valida Capacidade Minima
		if (!integracaoBemMedidor.getCapacidadeMinima().equals(medidor.getCapacidadeMinima().getChavePrimaria())) {
			medidor.setCapacidadeMinima((CapacidadeMedidor) service.getBeanPorIDeSeteChavePrimaria(
							CapacidadeMedidor.BEAN_ID_CAPACIDADE_MEDIDOR, integracaoBemMedidor.getCapacidadeMinima()));
		}

		// Valida Capacidade Maxima
		if (!integracaoBemMedidor.getCapacidadeMaxima().equals(medidor.getCapacidadeMaxima().getChavePrimaria())) {
			medidor.setCapacidadeMaxima((CapacidadeMedidor) service.getBeanPorIDeSeteChavePrimaria(
							CapacidadeMedidor.BEAN_ID_CAPACIDADE_MEDIDOR, integracaoBemMedidor.getCapacidadeMaxima()));
		}

		// Valida Local Armazenagem
		if (integracaoBemMedidor.getLocalArmazenagem() != null) {
			if ((medidor.getLocalArmazenagem() != null && (!integracaoBemMedidor.getLocalArmazenagem().equals(
							medidor.getLocalArmazenagem().getChavePrimaria())))
							|| medidor.getLocalArmazenagem() == null) {
				medidor.setLocalArmazenagem((MedidorLocalArmazenagem) service.getBeanPorIDeSeteChavePrimaria(
								MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, integracaoBemMedidor.getLocalArmazenagem()));
			}
		} else {
			medidor.setLocalArmazenagem(null);
		}

		// Valida Temperatura Trabalho
		if (integracaoBemMedidor.getFaixaTemperaturaTrabalho() != null) {
			if ((medidor.getFaixaTemperaturaTrabalho() != null && (!integracaoBemMedidor.getFaixaTemperaturaTrabalho().equals(
							medidor.getFaixaTemperaturaTrabalho().getChavePrimaria())))
							|| medidor.getFaixaTemperaturaTrabalho() == null) {
				medidor.setFaixaTemperaturaTrabalho((FaixaTemperaturaTrabalho) service.getBeanPorIDeSeteChavePrimaria(
								FaixaTemperaturaTrabalho.BEAN_ID_FAIXA_TEMPERATURA_TRABALHO,
								integracaoBemMedidor.getFaixaTemperaturaTrabalho()));
			}
		} else {
			medidor.setFaixaTemperaturaTrabalho(null);
		}

		// Valida Unidade Pressão Máxima
		if (integracaoBemMedidor.getUnidadePressaoMaximaTrabalho() != null) {
			if ((medidor.getUnidadePressaoMaximaTrabalho() != null && (!integracaoBemMedidor.getUnidadePressaoMaximaTrabalho().equals(
							medidor.getUnidadePressaoMaximaTrabalho().getChavePrimaria())))
							|| medidor.getUnidadePressaoMaximaTrabalho() == null) {
				medidor.setUnidadePressaoMaximaTrabalho((Unidade) service.getBeanPorIDeSeteChavePrimaria(Unidade.BEAN_ID_UNIDADE,
								integracaoBemMedidor.getUnidadePressaoMaximaTrabalho()));
			}
		} else {
			medidor.setUnidadePressaoMaximaTrabalho(null);
		}

		// Valida Modelo do Medidor
		if (integracaoBemMedidor.getModelo() != null) {
			if ((medidor.getModelo() != null && (!integracaoBemMedidor.getModelo().equals(medidor.getModelo().getChavePrimaria())))
							|| medidor.getModelo() == null) {
				medidor.setModelo((ModeloMedidor) service.getBeanPorIDeSeteChavePrimaria(ModeloMedidor.BEAN_ID_MODELO_MEDIDOR,
								integracaoBemMedidor.getModelo()));
			}
		} else {
			medidor.setModelo(null);
		}

		// Valida Fator K
		if (integracaoBemMedidor.getFatorK() != null) {
			if ((medidor.getFatorK() != null && (!integracaoBemMedidor.getFatorK().equals(medidor.getFatorK().getChavePrimaria())))
							|| medidor.getFatorK() == null) {
				medidor.setFatorK((FatorK) service.getBeanPorIDeSeteChavePrimaria(FatorK.BEAN_ID_FATOR_K, integracaoBemMedidor.getFatorK()));
			}
		} else {
			medidor.setFatorK(null);
		}

	}

	/**
	 * Validar datas alteracao.
	 *
	 * @param integracaoBemMedidor
	 *            the integracao bem medidor
	 * @param medidor
	 *            the medidor
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return the string
	 */
	private String validarDatasAlteracao(IntegracaoBemMedidorImpl integracaoBemMedidor, Medidor medidor,
					Map<String, Object[]> mapaInconsistencias) {

		ResourceBundle.getBundle("mensagens");
		String mensagemErro = "";
		// Valida Ano Fabricacao
		if (integracaoBemMedidor.getAnoFabricacao() != null) {
			if ((medidor.getAnoFabricacao() != null && (!integracaoBemMedidor.getAnoFabricacao().equals(
							Long.valueOf(medidor.getAnoFabricacao()))))
							|| medidor.getAnoFabricacao() == null) {
				if (integracaoBemMedidor.getAnoFabricacao() > Calendar.getInstance().get(Calendar.YEAR)) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.ANOFABRICACAO));

				} else {
					medidor.setAnoFabricacao(Util.converteLongParaInteger(integracaoBemMedidor.getAnoFabricacao()));
				}
			}
		} else {
			medidor.setAnoFabricacao(null);
		}

		// Valida Data de Aquisição
		if (!integracaoBemMedidor.getDataAquisicao().equals(medidor.getDataAquisicao())) {
			medidor.setDataAquisicao(integracaoBemMedidor.getDataAquisicao());
		}

		// Valida Data Máxima de Instalação
		if (integracaoBemMedidor.getDataMaximaInstalacao() != null) {
			if ((medidor.getDataMaximaInstalacao() != null && (!integracaoBemMedidor.getDataMaximaInstalacao().equals(
							medidor.getDataMaximaInstalacao())))
							|| medidor.getDataMaximaInstalacao() == null) {
				medidor.setDataMaximaInstalacao(integracaoBemMedidor.getDataMaximaInstalacao());
			}
		} else {
			medidor.setDataMaximaInstalacao(null);
		}

		// Valida Data Ultima calibração
		if (integracaoBemMedidor.getDataUltimaCalibracao() != null) {
			if ((medidor.getDataUltimaCalibracao() != null && (!integracaoBemMedidor.getDataUltimaCalibracao().equals(
							medidor.getDataUltimaCalibracao())))
							|| medidor.getDataUltimaCalibracao() == null) {
				medidor.setDataUltimaCalibracao(integracaoBemMedidor.getDataUltimaCalibracao());
			}
		} else {
			medidor.setDataUltimaCalibracao(null);
		}

		// Valida Número de digitos
		if (!integracaoBemMedidor.getNumeroDigito().equals(Long.valueOf(medidor.getDigito()))) {
			medidor.setDigito(Util.converteLongParaInteger(integracaoBemMedidor.getNumeroDigito()));
		}

		if (integracaoBemMedidor.getNumeroTombamento() != null) {
			if ((medidor.getTombamento() != null && (!integracaoBemMedidor.getNumeroTombamento().equals(medidor.getTombamento())))
							|| medidor.getTombamento() == null) {
				medidor.setTombamento(integracaoBemMedidor.getNumeroTombamento());
			}
		} else {
			medidor.setTombamento(null);
		}
		return mensagemErro;
	}

	/**
	 * Concatena mensagem erro.
	 *
	 * @param mensagemErro
	 *            the mensagem erro
	 * @param valor
	 *            the valor
	 * @return the string
	 */
	private String concatenaMensagemErro(String mensagemErroTmp, String valor) {

		String mensagemErro = mensagemErroTmp;
		if (!mensagemErro.isEmpty()) {
			mensagemErro = mensagemErro.concat(", " + valor);
		} else {
			mensagemErro = mensagemErro.concat(valor);
		}
		return mensagemErro;
	}

	/**
	 * Converter integracao bem vazao corretor para vazao corretor.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @return the vazao corretor
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private VazaoCorretor converterIntegracaoBemVazaoCorretorParaVazaoCorretor(IntegracaoBemVazaoCorretor integracaoBemVazaoCorretor)
					throws GGASException {

		ServiceLocator.getInstancia().getControladorMedidor();
		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();

		ServiceLocator service = ServiceLocator.getInstancia();
		VazaoCorretor corretor = (VazaoCorretor) service.getBeanPorID(VazaoCorretor.BEAN_ID_VAZAO_CORRETOR);
		corretor.setNumeroSerie(integracaoBemVazaoCorretor.getNumeroSerie());
		corretor.setMarcaCorretor((MarcaCorretor) service.getBeanPorIDeSeteChavePrimaria(MarcaCorretor.BEAN_ID_MARCA_CORRETOR,
						integracaoBemVazaoCorretor.getMarcaCorretor()));
		if (integracaoBemVazaoCorretor.getAnoFabricacao() != null) {
			corretor.setNumeroAnoFabricacao(integracaoBemVazaoCorretor.getAnoFabricacao().intValue());
		}

		if (integracaoBemVazaoCorretor.getIndicadorTipoMostrador() != null) {
			corretor.setTipoMostrador(controladorVazaoCorretor.obterTipoMostrador(Integer.valueOf(integracaoBemVazaoCorretor
							.getIndicadorTipoMostrador().toString())));
		}

		if (integracaoBemVazaoCorretor.getIndicadorCorrecaoPressao() != null) {
			corretor.setCorrecaoPressao(Util.converteLongParaBoolean(integracaoBemVazaoCorretor.getIndicadorCorrecaoPressao()));
		}

		if (integracaoBemVazaoCorretor.getIndicadorCorrecaoTemperatura() != null) {
			corretor.setCorrecaoTemperatura(Util.converteLongParaBoolean(integracaoBemVazaoCorretor.getIndicadorCorrecaoTemperatura()));
		}

		if (integracaoBemVazaoCorretor.getIndicadorControleVazao() != null) {
			corretor.setControleVazao(Util.converteLongParaBoolean(integracaoBemVazaoCorretor.getIndicadorControleVazao()));
		}

		if (integracaoBemVazaoCorretor.getIndicadorLinearizacaoFatorK() != null) {
			corretor.setLinearizacaoFatorK(Util.converteLongParaBoolean(integracaoBemVazaoCorretor.getIndicadorLinearizacaoFatorK()));
		}

		if (integracaoBemVazaoCorretor.getProtocoloComunicacao() != null) {
			corretor.setProtocoloComunicacao((ProtocoloComunicacao) service.getBeanPorIDeSeteChavePrimaria(
							ProtocoloComunicacao.BEAN_ID_PROTOCOLO_COMUNICACAO, integracaoBemVazaoCorretor.getProtocoloComunicacao()));
		}

		if (integracaoBemVazaoCorretor.getTipoTransdutorPressao() != null) {
			corretor.setTipoTransdutorPressao((TipoTransdutorPressao) service.getBeanPorIDeSeteChavePrimaria(
							TipoTransdutorPressao.BEAN_ID_TIPO_TRANSDUTOR_PRESSAO, integracaoBemVazaoCorretor.getTipoTransdutorPressao()));
		}

		if (integracaoBemVazaoCorretor.getTipoTransdutorTemperatura() != null) {
			corretor.setTipoTransdutorTemperatura((TipoTransdutorTemperatura) service.getBeanPorIDeSeteChavePrimaria(
							TipoTransdutorTemperatura.BEAN_ID_TIPO_TRANSDUTOR_TEMPERATURA,
							integracaoBemVazaoCorretor.getTipoTransdutorTemperatura()));
		}

		if (integracaoBemVazaoCorretor.getPressaoMaximaTransdutor() != null) {
			corretor.setPressaoMaximaTransdutor((PressaoMaximaTransdutor) service.getBeanPorIDeSeteChavePrimaria(
							PressaoMaximaTransdutor.BEAN_ID_PRESSAO_MAXIMA_TRANSDUTOR,
							integracaoBemVazaoCorretor.getPressaoMaximaTransdutor()));
		}

		if (integracaoBemVazaoCorretor.getTemperaturaMaximaTransdutor() != null) {
			corretor.setTemperaturaMaximaTransdutor((TemperaturaMaximaTransdutor) service.getBeanPorIDeSeteChavePrimaria(
							TemperaturaMaximaTransdutor.BEAN_ID_TEMPERATURA_MAXIMA_TRANSDUTOR,
							integracaoBemVazaoCorretor.getTemperaturaMaximaTransdutor()));
		}

		if (integracaoBemVazaoCorretor.getModelo() != null) {
			corretor.setModelo((ModeloCorretor) service.getBeanPorIDeSeteChavePrimaria(ModeloCorretor.BEAN_ID_MODELO_CORRETOR,
							integracaoBemVazaoCorretor.getModelo()));
		}

		if (integracaoBemVazaoCorretor.getSituacaoBem() != null) {
			corretor.setSituacaoMedidor((SituacaoMedidor) service.getBeanPorIDeSeteChavePrimaria(SituacaoMedidor.BEAN_ID_SITUACAO_MEDIDOR,
							integracaoBemVazaoCorretor.getSituacaoBem()));
		}

		corretor.setNumeroDigitos(Util.converteLongParaInteger(integracaoBemVazaoCorretor.getNumeroDigito()));
		if (integracaoBemVazaoCorretor.getNumeroTombamento() != null) {
			corretor.setTombamento(integracaoBemVazaoCorretor.getNumeroTombamento());
		}

		if (integracaoBemVazaoCorretor.getLocalArmazenagem() != null) {
			corretor.setLocalArmazenagem((MedidorLocalArmazenagem) service.getBeanPorIDeSeteChavePrimaria(
							MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, integracaoBemVazaoCorretor.getLocalArmazenagem()));
		}

		return corretor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarExclusaoMedidor(br.com.ggas.integracao.bens.impl.IntegracaoBemMedidorImpl,
	 * java.util.Map)
	 */
	@Override
	public boolean validarExclusaoMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Map<String, Object[]> mapaInconsistencias)
					throws NegocioException, ConcorrenciaException {

		ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		String mensagemErro = "";
		Medidor medidor = null;
		if (integracaoBemMedidor.getBemDestino() == null) {
			Util.atualizaMapaInconsistencias(mapaInconsistencias,
							concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO, ""));
			mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO);
		} else {
			EntidadeNegocio entidade = controladorMedidor.obter(integracaoBemMedidor.getBemDestino(), MedidorImpl.class, true);
			if (entidade != null) {
				medidor = (Medidor) entidade;
			}
			if (medidor == null) {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO, ""));
				mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_MEDIDOR_NAO_CADASTRADO);
			} else {
				boolean existeRestricao = controladorMedidor.verificarRegistrosAssociados(medidor.getChavePrimaria());
				if (existeRestricao) {
					medidor.setHabilitado(false);
					super.atualizar(medidor, MedidorImpl.class);
					atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemMedidor, medidor.getChavePrimaria());
				} else {
					super.remover(medidor, MedidorImpl.class);
					atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemMedidor, medidor.getChavePrimaria());
				}
			}
		}
		if (mensagemErro != null && !mensagemErro.isEmpty()) {
			atualizarIntegracaoComErro(IntegracaoBemMedidorImpl.class, integracaoBemMedidor, mensagemErro);
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarInsercaoVazaoCorretor(br.com.ggas.integracao.bens.impl.
	 * IntegracaoBemVazaoCorretorImpl
	 * , java.util.Map)
	 */
	@Override
	public boolean validarInsercaoVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws GGASException {

		String mensagemErro = "";
		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		Boolean possuiErro = this.validarIntegracao(integracaoBemVazaoCorretor, IntegracaoBemVazaoCorretorImpl.class, mapaInconsistencias);
		if (!possuiErro) {
			boolean vazaoCorretorExistente = validarExistenciaVazaoCorretor(integracaoBemVazaoCorretor);
			if (!vazaoCorretorExistente) {

				ControladorConstanteSistema ctlConstanteSistema =
								(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				String situacaoCorretor = ctlConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL);

				if (!integracaoBemVazaoCorretor.getSituacaoBem().toString().equals(situacaoCorretor)) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.SITUACAOMEDIDOR));
				} else {

					if (integracaoBemVazaoCorretor.getAnoFabricacao() != null
									&& (integracaoBemVazaoCorretor.getAnoFabricacao() > Calendar.getInstance().get(Calendar.YEAR))) {
						Util.atualizaMapaInconsistencias(mapaInconsistencias,
										concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.ANOFABRICACAO));

					}

					String entidadesInexistentes =
									validarExistenciaEntidades(new VazaoCorretorImpl(), integracaoBemVazaoCorretor, mapaInconsistencias);

					ParametroSistema parametroSistema =
									ctlParametroSistema.obterParametroPorCodigo(ParametroSistema.NUMERO_MAXIMO_DIGITOS_CORRETOR);
					if (integracaoBemVazaoCorretor.getNumeroDigito() != null
									&& ((integracaoBemVazaoCorretor.getNumeroDigito() > Long.parseLong(parametroSistema.getValor())) || integracaoBemVazaoCorretor
													.getNumeroDigito() == 0)) {
						Util.atualizaMapaInconsistencias(mapaInconsistencias,
										concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.NUMERODIGITOS));

					}
					mensagemErro = Util.gerarMensagemErroConsolidada(mapaInconsistencias, mensagens);
					mensagemErro = mensagemErro.concat(entidadesInexistentes);
				}
			} else {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_CADASTRADO, " "));
				mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_CADASTRADO);
			}
			if (!mensagemErro.isEmpty()) {
				atualizarIntegracaoComErro(IntegracaoBemVazaoCorretorImpl.class, integracaoBemVazaoCorretor, mensagemErro);
				return false;
			} else {
				VazaoCorretor corretor = converterIntegracaoBemVazaoCorretorParaVazaoCorretor(integracaoBemVazaoCorretor);

				Long idCorretor = controladorVazaoCorretor.inserir(corretor);
				corretor.getMovimentacoes().add(controladorVazaoCorretor.criarMovimentacaoCorretorVazao(corretor));

				atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemVazaoCorretor, idCorretor);
			}
		} else {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarAlteracaoVazaoCorretor(br.com.ggas.integracao.bens.impl.
	 * IntegracaoBemVazaoCorretorImpl
	 * , java.util.Map)
	 */
	@Override
	public boolean validarAlteracaoVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws GGASException {

		VazaoCorretor vazaoCorretor = null;
		String mensagemErro = "";
		String mensagemErroNova = "";

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();

		ServiceLocator service = ServiceLocator.getInstancia();
		Boolean possuiErro = this.validarIntegracao(integracaoBemVazaoCorretor, IntegracaoBemVazaoCorretorImpl.class, mapaInconsistencias);
		if (!possuiErro) {
			if (integracaoBemVazaoCorretor.getBemDestino() == null) {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO, ""));
				mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO);
			} else {
				EntidadeNegocio entidade =
								controladorVazaoCorretor.obter(integracaoBemVazaoCorretor.getBemDestino(), VazaoCorretorImpl.class, true);
				if (entidade != null) {
					vazaoCorretor = (VazaoCorretor) entidade;
				}
				if (vazaoCorretor == null) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO, ""));
					mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO);
				} else {
					boolean vazaoCorretorExistente = validarExistenciaVazaoCorretor(integracaoBemVazaoCorretor);
					if (!vazaoCorretorExistente || integracaoBemVazaoCorretor.getBemDestino().equals(vazaoCorretor.getChavePrimaria())) {

						mensagemErro = validarExistenciaEntidades(new VazaoCorretorImpl(), integracaoBemVazaoCorretor, mapaInconsistencias);
						if (mensagemErro.isEmpty()) {
							if (!integracaoBemVazaoCorretor.getSituacaoBem().equals(vazaoCorretor.getSituacaoMedidor().getChavePrimaria())) {
								ConstanteSistema situacaoInstalado =
												controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_MEDIDOR_INSTALADO);
								if (Long.valueOf(situacaoInstalado.getValor())
												.equals(vazaoCorretor.getSituacaoMedidor().getChavePrimaria())) {
									Util.atualizaMapaInconsistencias(mapaInconsistencias,
													concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_SITUACAO_MEDIDOR_INVALIDA, ""));
								} else {
									EntidadeNegocio situacaoMedidor =
													controladorMedidor.obter(integracaoBemVazaoCorretor.getSituacaoBem(),
																	SituacaoMedidorImpl.class, true);
									if (situacaoMedidor == null) {
										Util.atualizaMapaInconsistencias(
														mapaInconsistencias,
														concatenaMensagemErro(Constantes.ERRO_NEGOCIO_ENTIDADE_NAO_CADASTRADA,
																		Constantes.SITUACAOMEDIDOR));
									} else {
										vazaoCorretor.setSituacaoMedidor((SituacaoMedidor) service.getBeanPorIDeSeteChavePrimaria(
														SituacaoMedidor.BEAN_ID_SITUACAO_MEDIDOR,
														integracaoBemVazaoCorretor.getSituacaoBem()));
									}

								}
							}

							// Valida os atributos que foram alterados
							validarAtributosNaoCadastradosVazaoCorretor(integracaoBemVazaoCorretor, vazaoCorretor);
							validarIndicadores(integracaoBemVazaoCorretor, vazaoCorretor, mapaInconsistencias);

							mensagemErroNova = Util.gerarMensagemErroConsolidada(mapaInconsistencias, mensagens);
							mensagemErro = concatenaMensagemErro(mensagemErro, mensagemErroNova);
						}
					} else {
						Util.atualizaMapaInconsistencias(mapaInconsistencias,
										concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_CADASTRADO, " "));
						mensagemErro =
										MensagemUtil.obterMensagem(mensagens,
														Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_JA_CADASTRADO);
					}
				}
			}

			if (mensagemErro != null && !mensagemErro.isEmpty()) {
				atualizarIntegracaoComErro(IntegracaoBemVazaoCorretorImpl.class, integracaoBemVazaoCorretor, mensagemErro);
				return false;
			} else {
				super.atualizar(vazaoCorretor, VazaoCorretorImpl.class);
				atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemVazaoCorretor, vazaoCorretor.getChavePrimaria());
			}
		} else {
			return false;
		}
		return true;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarExclusaoVazaoCorretor(br.com.ggas.integracao.bens.impl.
	 * IntegracaoBemVazaoCorretorImpl
	 * , java.util.Map)
	 */
	@Override
	public boolean validarExclusaoVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws NegocioException, ConcorrenciaException {

		String mensagemErro = "";
		VazaoCorretor vazaoCorretor = null;
		ServiceLocator.getInstancia().getControladorMedidor();
		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();

		if (integracaoBemVazaoCorretor.getBemDestino() == null) {
			Util.atualizaMapaInconsistencias(mapaInconsistencias,
							concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO, ""));
			mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO);

		} else {

			EntidadeNegocio entidade =
							controladorVazaoCorretor.obter(integracaoBemVazaoCorretor.getBemDestino(), VazaoCorretorImpl.class, true);
			if (entidade != null) {
				vazaoCorretor = (VazaoCorretor) entidade;
			}
			if (vazaoCorretor == null) {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO, ""));
				mensagemErro = MensagemUtil.obterMensagem(mensagens, Constantes.ERRO_NEGOCIO_INTEGRACAO_VAZAO_CORRETOR_NAO_CADASTRADO);

			} else {
				boolean existeRestricao = controladorVazaoCorretor.verificarRegistrosAssociados(vazaoCorretor.getChavePrimaria());
				if (existeRestricao) {
					vazaoCorretor.setHabilitado(false);
					super.atualizar(vazaoCorretor, VazaoCorretorImpl.class);
					atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemVazaoCorretor, vazaoCorretor.getChavePrimaria());
				} else {
					super.remover(vazaoCorretor, VazaoCorretorImpl.class);
					atualizarIntegracaoProcessado(IntegracaoBemImpl.class, integracaoBemVazaoCorretor, vazaoCorretor.getChavePrimaria());
				}
			}
		}
		if (mensagemErro != null && !mensagemErro.isEmpty()) {
			atualizarIntegracaoComErro(IntegracaoBemVazaoCorretorImpl.class, integracaoBemVazaoCorretor, mensagemErro);
			return false;
		}
		return true;

	}

	/**
	 * Validar atributos nao cadastrados vazao corretor.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @return the string
	 */
	private String validarAtributosNaoCadastradosVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					VazaoCorretor vazaoCorretor) {

		ServiceLocator service = ServiceLocator.getInstancia();
		String mensagemErro = "";

		if (!integracaoBemVazaoCorretor.getNumeroSerie().equals(vazaoCorretor.getNumeroSerie())) {
			vazaoCorretor.setNumeroSerie(integracaoBemVazaoCorretor.getNumeroSerie());
		}

		// Valida Protocolo de Comunicação
		if (integracaoBemVazaoCorretor.getProtocoloComunicacao() != null) {
			if ((vazaoCorretor.getProtocoloComunicacao() != null && (!integracaoBemVazaoCorretor.getProtocoloComunicacao().equals(
							vazaoCorretor.getProtocoloComunicacao().getChavePrimaria())))
							|| vazaoCorretor.getProtocoloComunicacao() == null) {
				vazaoCorretor.setProtocoloComunicacao((ProtocoloComunicacao) service.getBeanPorIDeSeteChavePrimaria(
								ProtocoloComunicacao.BEAN_ID_PROTOCOLO_COMUNICACAO, integracaoBemVazaoCorretor.getProtocoloComunicacao()));
			}

		} else {
			vazaoCorretor.setProtocoloComunicacao(null);
		}

		// Valida Tipo de Transdutor de Pressão
		if (integracaoBemVazaoCorretor.getTipoTransdutorPressao() != null) {
			if ((vazaoCorretor.getTipoTransdutorPressao() == null && (!integracaoBemVazaoCorretor.getTipoTransdutorPressao().equals(
							vazaoCorretor.getTipoTransdutorPressao().getChavePrimaria())))
							|| vazaoCorretor.getTipoTransdutorPressao() == null) {
				vazaoCorretor.setTipoTransdutorPressao((TipoTransdutorPressao) service.getBeanPorIDeSeteChavePrimaria(
								TipoTransdutorPressao.BEAN_ID_TIPO_TRANSDUTOR_PRESSAO,
								integracaoBemVazaoCorretor.getTipoTransdutorPressao()));
			}
		} else {
			vazaoCorretor.setTipoTransdutorPressao(null);
		}

		// Valida Tipo de Transdutor de Temperatura
		if (integracaoBemVazaoCorretor.getTipoTransdutorTemperatura() != null) {
			if ((vazaoCorretor.getTipoTransdutorTemperatura() != null && (!integracaoBemVazaoCorretor.getTipoTransdutorTemperatura()
							.equals(vazaoCorretor.getTipoTransdutorTemperatura().getChavePrimaria())))
							|| vazaoCorretor.getTipoTransdutorTemperatura() == null) {
				vazaoCorretor.setTipoTransdutorTemperatura((TipoTransdutorTemperatura) service.getBeanPorIDeSeteChavePrimaria(
								TipoTransdutorTemperatura.BEAN_ID_TIPO_TRANSDUTOR_TEMPERATURA,
								integracaoBemVazaoCorretor.getTipoTransdutorTemperatura()));
			}
		} else {
			vazaoCorretor.setTipoTransdutorTemperatura(null);
		}

		// Valida Temperatura Máxima do Transdutor
		if (integracaoBemVazaoCorretor.getTemperaturaMaximaTransdutor() != null) {
			if ((vazaoCorretor.getTemperaturaMaximaTransdutor() != null && (!integracaoBemVazaoCorretor.getTemperaturaMaximaTransdutor()
							.equals(vazaoCorretor.getTemperaturaMaximaTransdutor().getChavePrimaria())))
							|| vazaoCorretor.getTemperaturaMaximaTransdutor() == null) {

				vazaoCorretor.setTemperaturaMaximaTransdutor((TemperaturaMaximaTransdutor) service.getBeanPorIDeSeteChavePrimaria(
								TemperaturaMaximaTransdutor.BEAN_ID_TEMPERATURA_MAXIMA_TRANSDUTOR,
								integracaoBemVazaoCorretor.getTemperaturaMaximaTransdutor()));
			}
		} else {
			vazaoCorretor.setTemperaturaMaximaTransdutor(null);
		}

		// Valida Modelo Corretor
		if (integracaoBemVazaoCorretor.getModelo() != null) {
			if ((vazaoCorretor.getModelo() != null && (!integracaoBemVazaoCorretor.getModelo().equals(
							vazaoCorretor.getModelo().getChavePrimaria())))
							|| vazaoCorretor.getModelo() == null) {
				vazaoCorretor.setModelo((ModeloCorretor) service.getBeanPorIDeSeteChavePrimaria(ModeloCorretor.BEAN_ID_MODELO_CORRETOR,
								integracaoBemVazaoCorretor.getModelo()));
			}
		} else {
			vazaoCorretor.setModelo(null);
		}

		// Valida Marca Corretor
		if (!integracaoBemVazaoCorretor.getMarcaCorretor().equals(vazaoCorretor.getMarcaCorretor().getChavePrimaria())) {
			vazaoCorretor.setMarcaCorretor((MarcaCorretor) service.getBeanPorIDeSeteChavePrimaria(MarcaCorretor.BEAN_ID_MARCA_CORRETOR,
							integracaoBemVazaoCorretor.getMarcaCorretor()));
		}

		// Valida Local Armazenagem
		if (integracaoBemVazaoCorretor.getLocalArmazenagem() != null) {
			if ((vazaoCorretor.getLocalArmazenagem() != null && (!integracaoBemVazaoCorretor.getLocalArmazenagem().equals(
							vazaoCorretor.getLocalArmazenagem().getChavePrimaria())))
							|| vazaoCorretor.getLocalArmazenagem() == null) {

				vazaoCorretor.setLocalArmazenagem((MedidorLocalArmazenagem) service.getBeanPorIDeSeteChavePrimaria(
								MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, integracaoBemVazaoCorretor.getLocalArmazenagem()));
			}

		} else {
			vazaoCorretor.setLocalArmazenagem(null);
		}

		return mensagemErro;

	}

	/**
	 * Validar indicadores.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void validarIndicadores(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor, VazaoCorretor vazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws GGASException {

		ServiceLocator.getInstancia().getControladorMedidor();
		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		// Valida Ano Fabricacao
		if (integracaoBemVazaoCorretor.getAnoFabricacao() != null) {
			if ((vazaoCorretor.getNumeroAnoFabricacao() != null && (!integracaoBemVazaoCorretor.getAnoFabricacao().equals(
							Long.valueOf(vazaoCorretor.getNumeroAnoFabricacao()))))
							|| vazaoCorretor.getNumeroAnoFabricacao() == null) {
				if (integracaoBemVazaoCorretor.getAnoFabricacao() > Calendar.getInstance().get(Calendar.YEAR)) {
					Util.atualizaMapaInconsistencias(mapaInconsistencias,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.ANOFABRICACAO));

				} else {
					vazaoCorretor.setNumeroAnoFabricacao(Util.converteLongParaInteger(integracaoBemVazaoCorretor.getAnoFabricacao()));
				}
			}
		} else {
			vazaoCorretor.setNumeroAnoFabricacao(null);
		}

		if (integracaoBemVazaoCorretor.getIndicadorTipoMostrador() != null) {

			if ((vazaoCorretor.getTipoMostrador() != null && vazaoCorretor.getTipoMostrador().getCodigo() != integracaoBemVazaoCorretor
							.getIndicadorTipoMostrador().intValue()) || vazaoCorretor.getTipoMostrador() == null) {
				TipoMostrador tipoMostrador =
								controladorVazaoCorretor.obterTipoMostrador(integracaoBemVazaoCorretor.getIndicadorTipoMostrador()
												.intValue());
				vazaoCorretor.setTipoMostrador(tipoMostrador);
			}
		} else {
			vazaoCorretor.setTipoMostrador(null);
		}

		if (integracaoBemVazaoCorretor.getIndicadorCorrecaoPressao() != null) {
			if ((vazaoCorretor.getCorrecaoPressao() != null && !Util.converteLongParaBoolean(
							integracaoBemVazaoCorretor.getIndicadorCorrecaoPressao()).equals(vazaoCorretor.getCorrecaoPressao()))
							|| vazaoCorretor.getCorrecaoPressao() == null) {
				vazaoCorretor.setCorrecaoPressao(Util.converteLongParaBoolean(integracaoBemVazaoCorretor.getIndicadorCorrecaoPressao()));
			}
		} else {
			vazaoCorretor.setCorrecaoPressao(null);
		}

		if (integracaoBemVazaoCorretor.getIndicadorCorrecaoTemperatura() != null) {
			if ((vazaoCorretor.getCorrecaoTemperatura() != null && !Util.converteLongParaBoolean(
							integracaoBemVazaoCorretor.getIndicadorCorrecaoTemperatura()).equals(vazaoCorretor.getCorrecaoTemperatura()))
							|| vazaoCorretor.getCorrecaoTemperatura() == null) {
				vazaoCorretor.setCorrecaoTemperatura(Util.converteLongParaBoolean(integracaoBemVazaoCorretor
								.getIndicadorCorrecaoTemperatura()));
			}
		} else {
			vazaoCorretor.setCorrecaoTemperatura(null);
		}

		if (integracaoBemVazaoCorretor.getIndicadorControleVazao() != null) {
			if ((vazaoCorretor.getControleVazao() != null && !Util.converteLongParaBoolean(
							integracaoBemVazaoCorretor.getIndicadorControleVazao()).equals(vazaoCorretor.getControleVazao()))
							|| vazaoCorretor.getControleVazao() == null) {
				vazaoCorretor.setControleVazao(Util.converteLongParaBoolean(integracaoBemVazaoCorretor.getIndicadorControleVazao()));

			}
		} else {
			vazaoCorretor.setControleVazao(null);
		}

		if (integracaoBemVazaoCorretor.getIndicadorLinearizacaoFatorK() != null) {
			if ((vazaoCorretor.getLinearizacaoFatorK() != null && !Util.converteLongParaBoolean(
							integracaoBemVazaoCorretor.getIndicadorLinearizacaoFatorK()).equals(vazaoCorretor.getLinearizacaoFatorK()))
							|| vazaoCorretor.getLinearizacaoFatorK() == null) {
				vazaoCorretor.setLinearizacaoFatorK(Util.converteLongParaBoolean(integracaoBemVazaoCorretor
								.getIndicadorLinearizacaoFatorK()));
			}
		} else {
			vazaoCorretor.setLinearizacaoFatorK(null);
		}
		ParametroSistema parametroSistema = ctlParametroSistema.obterParametroPorCodigo(ParametroSistema.NUMERO_MAXIMO_DIGITOS_CORRETOR);
		if (!integracaoBemVazaoCorretor.getNumeroDigito().equals(Long.valueOf(vazaoCorretor.getNumeroDigitos()))) {
			if (integracaoBemVazaoCorretor.getNumeroDigito() != null
							&& ((integracaoBemVazaoCorretor.getNumeroDigito() > Long.parseLong(parametroSistema.getValor())) || integracaoBemVazaoCorretor
											.getNumeroDigito() == 0)) {
				Util.atualizaMapaInconsistencias(mapaInconsistencias,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_DADOS_INVALIDOS, Constantes.NUMERODIGITOS));
			} else {
				vazaoCorretor.setNumeroDigitos(Util.converteLongParaInteger(integracaoBemVazaoCorretor.getNumeroDigito()));
			}
		}

		// Valida Tombamento
		if (integracaoBemVazaoCorretor.getNumeroTombamento() != null) {
			if ((vazaoCorretor.getTombamento() != null && (!integracaoBemVazaoCorretor.getNumeroTombamento().equals(
							vazaoCorretor.getTombamento())))
							|| vazaoCorretor.getTombamento() == null) {
				vazaoCorretor.setTombamento(integracaoBemVazaoCorretor.getNumeroTombamento());
			}
		} else {
			vazaoCorretor.setTombamento(null);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#gerarMovimentacaoMedidor(java.lang.Long, java.lang.Long,
	 * br.com.ggas.cadastro.imovel.PontoConsumo, boolean)
	 */
	@Override
	public void gerarMovimentacaoMedidor(Long idMedidor, Long idMedidorOperacaoHistorico, PontoConsumo pontoConsumo, boolean instalacao)
					throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Medidor medidor = (Medidor) this.obter(idMedidor, MedidorImpl.class, true);

		HistoricoOperacaoMedidor historicoOperacaoMedidor =
						(HistoricoOperacaoMedidor) this.obter(idMedidorOperacaoHistorico, HistoricoOperacaoMedidorImpl.class, true);

		IntegracaoBemMovimentacaoImpl integracaoBemMovimentacao =
						converteDadosOperacaoMedidorParaIntegracaoBemMovimentacao(medidor, historicoOperacaoMedidor, pontoConsumo,
										instalacao);

		if (integracaoBemMovimentacao.getBem() == null) {
			throw new NegocioException(Constantes.ERRO_INTEGRACAO_INEXISTENTE, true);
		} else {

			this.inserir(integracaoBemMovimentacao);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#gerarMovimentacaoVazaoCorretor(java.lang.Long, java.lang.Long,
	 * br.com.ggas.cadastro.imovel.PontoConsumo, boolean)
	 */
	@Override
	public void gerarMovimentacaoVazaoCorretor(Long idVazaoCorretor, Long idVazaoCorretorOperacaoHistorico, PontoConsumo pontoConsumo,
					boolean instalacao) throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		VazaoCorretor vazaoCorretor = (VazaoCorretor) this.obter(idVazaoCorretor, VazaoCorretorImpl.class, true);

		VazaoCorretorHistoricoOperacao vazaoCorretorHistoricoOperacao =
						(VazaoCorretorHistoricoOperacao) this.obter(idVazaoCorretorOperacaoHistorico,
										VazaoCorretorHistoricoOperacaoImpl.class, true);

		IntegracaoBemMovimentacaoImpl integracaoBemMovimentacao =
						converteDadosOperacaoVazaoCorretorParaIntegracaoBemMovimentacao(vazaoCorretor, vazaoCorretorHistoricoOperacao,
										pontoConsumo, instalacao);

		if (integracaoBemMovimentacao.getBem() == null) {
			throw new NegocioException(Constantes.ERRO_INTEGRACAO_INEXISTENTE, true);
		} else {

			this.inserir(integracaoBemMovimentacao);
		}
	}

	/**
	 * Consultar integracao bem por codigo destino.
	 *
	 * @param codigoDestino
	 *            the codigo destino
	 * @param medidor
	 *            the medidor
	 * @return the integracao bem
	 */
	public IntegracaoBem consultarIntegracaoBemPorCodigoDestino(Long codigoDestino, boolean medidor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoBem().getSimpleName());
		hql.append(" integracaoBem");
		hql.append(" where integracaoBem.bemDestino = :codigoDestino");
		hql.append(" and integracaoBem.class = :tipoBem ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("codigoDestino", codigoDestino);
		String tipoBem;

		if (medidor) {

			tipoBem = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
							Constantes.C_INTEGRACAO_TIPO_BEM_MEDIDOR);
		} else {

			tipoBem = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
							Constantes.C_INTEGRACAO_TIPO_BEM_CORRETOR);
		}
		query.setString("tipoBem", tipoBem);

		return (IntegracaoBem) query.setMaxResults(1).uniqueResult();
	}

	/**
	 * Converte dados operacao medidor para integracao bem movimentacao.
	 *
	 * @param medidor
	 *            the medidor
	 * @param historicoOperacaoMedidor
	 *            the historico operacao medidor
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param instalacao
	 *            the instalacao
	 * @return the integracao bem movimentacao impl
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private IntegracaoBemMovimentacaoImpl converteDadosOperacaoMedidorParaIntegracaoBemMovimentacao(Medidor medidor,
					HistoricoOperacaoMedidor historicoOperacaoMedidor, PontoConsumo pontoConsumo, boolean instalacao)
					throws NegocioException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ParametroSistema codigoDestinoInstalacao = ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LOCAL_BEM_INSTALACAO);
		ConstanteSistema sistemaOrigem = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema situacaoNaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		IntegracaoBemMovimentacaoImpl integracaoBemMovimentacao =
						(IntegracaoBemMovimentacaoImpl) ServiceLocator.getInstancia().getBeanPorID(
										IntegracaoBemMovimentacao.BEAN_ID_INTEGRACAO_BEM_MOVIMENTACAO);
		ConstanteSistema operacao = null;
		String descricao = null;
		String constante = null;

		if (instalacao) {
			constante = Constantes.C_OPERACAO_INSTALACAO;
		} else {
			constante = Constantes.C_OPERACAO_RETIRADA;
		}
		operacao = controladorConstanteSistema.obterConstantePorCodigo(constante);
		descricao = controladorMedidor.consultarOperacaoMedidor(Long.valueOf(operacao.getValor()));

		IntegracaoBem bem = this.consultarIntegracaoBemPorCodigoDestino(medidor.getChavePrimaria(), true);
		integracaoBemMovimentacao.setBem(bem);

		if (bem == null) {
			return integracaoBemMovimentacao;
		}

		integracaoBemMovimentacao.setCodigoMovimentacao(buscarCodigoSistemaIntegranteNumerico(historicoOperacaoMedidor.getOperacaoMedidor()
						.getChavePrimaria(), historicoOperacaoMedidor.getOperacaoMedidor().getClass().getInterfaces()[0].getName(), bem
						.getSistemaOrigem()));
		integracaoBemMovimentacao.setDataOperacao(historicoOperacaoMedidor.getDataRealizada());
		integracaoBemMovimentacao.setDescricao(descricao);
		integracaoBemMovimentacao.setSituacaoBem(buscarCodigoSistemaIntegranteNumerico(medidor.getSituacaoMedidor().getChavePrimaria(),
						medidor.getSituacaoMedidor().getClass().getInterfaces()[0].getName(), bem.getSistemaOrigem()));
		if (instalacao) {
			integracaoBemMovimentacao.setLocalArmazenagemDestino(Long.valueOf(codigoDestinoInstalacao.getValor()));
			integracaoBemMovimentacao.setDescricaoDestino(pontoConsumo.getEnderecoFormatado());
		} else {
			integracaoBemMovimentacao.setLocalArmazenagemDestino(medidor.getLocalArmazenagem().getChavePrimaria());
		}
		integracaoBemMovimentacao.setFuncionario(historicoOperacaoMedidor.getFuncionario().getChavePrimaria());
		integracaoBemMovimentacao.setSituacao(situacaoNaoProcessado.getValor());
		integracaoBemMovimentacao.setSistemaOrigem(sistemaOrigem.getValor());
		integracaoBemMovimentacao.setVersao(medidor.getVersao());

		if (medidor.getDadosAuditoria() != null) {
			integracaoBemMovimentacao.setUsuario(String.valueOf(medidor.getDadosAuditoria().getUsuario().getChavePrimaria()));
		}

		return integracaoBemMovimentacao;
	}

	/**
	 * Converte dados operacao vazao corretor para integracao bem movimentacao.
	 *
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @param vazaoCorretorHistoricoOperacao
	 *            the vazao corretor historico operacao
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param instalacao
	 *            the instalacao
	 * @return the integracao bem movimentacao impl
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private IntegracaoBemMovimentacaoImpl converteDadosOperacaoVazaoCorretorParaIntegracaoBemMovimentacao(VazaoCorretor vazaoCorretor,
					VazaoCorretorHistoricoOperacao vazaoCorretorHistoricoOperacao, PontoConsumo pontoConsumo, boolean instalacao)
					throws NegocioException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ParametroSistema codigoDestinoInstalacao = ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LOCAL_BEM_INSTALACAO);
		ConstanteSistema sistemaOrigem = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema situacaoNaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		IntegracaoBemMovimentacaoImpl integracaoBemMovimentacao =
						(IntegracaoBemMovimentacaoImpl) ServiceLocator.getInstancia().getBeanPorID(
										IntegracaoBemMovimentacao.BEAN_ID_INTEGRACAO_BEM_MOVIMENTACAO);
		ConstanteSistema operacao = null;
		String descricao = null;
		String constante = null;
		if (instalacao) {
			constante = Constantes.C_OPERACAO_INSTALACAO;
		} else {
			constante = Constantes.C_OPERACAO_RETIRADA;
		}
		operacao = controladorConstanteSistema.obterConstantePorCodigo(constante);
		descricao = controladorMedidor.consultarOperacaoMedidor(Long.valueOf(operacao.getValor()));

		IntegracaoBem bem = this.consultarIntegracaoBemPorCodigoDestino(vazaoCorretor.getChavePrimaria(), false);
		integracaoBemMovimentacao.setBem(bem);

		if (bem == null) {
			return integracaoBemMovimentacao;
		}

		integracaoBemMovimentacao.setCodigoMovimentacao(buscarCodigoSistemaIntegranteNumerico(vazaoCorretorHistoricoOperacao
						.getOperacaoMedidor().getChavePrimaria(), vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getClass()
						.getInterfaces()[0].getName(), bem.getSistemaOrigem()));
		integracaoBemMovimentacao.setDataOperacao(vazaoCorretorHistoricoOperacao.getDataRealizada());
		integracaoBemMovimentacao.setDescricao(descricao);
		integracaoBemMovimentacao.setSituacaoBem(buscarCodigoSistemaIntegranteNumerico(vazaoCorretor.getSituacaoMedidor()
						.getChavePrimaria(), vazaoCorretor.getSituacaoMedidor().getClass().getInterfaces()[0].getName(), bem
						.getSistemaOrigem()));
		if (instalacao) {
			integracaoBemMovimentacao.setLocalArmazenagemDestino(Long.valueOf(codigoDestinoInstalacao.getValor()));
			if (pontoConsumo != null) {
				integracaoBemMovimentacao.setDescricaoDestino(pontoConsumo.getEnderecoFormatado());
			}
		} else {
			integracaoBemMovimentacao.setLocalArmazenagemDestino(vazaoCorretor.getLocalArmazenagem().getChavePrimaria());
		}

		integracaoBemMovimentacao.setFuncionario(vazaoCorretorHistoricoOperacao.getFuncionario().getChavePrimaria());
		integracaoBemMovimentacao.setSituacao(situacaoNaoProcessado.getValor());
		integracaoBemMovimentacao.setSistemaOrigem(sistemaOrigem.getValor());
		integracaoBemMovimentacao.setVersao(vazaoCorretor.getVersao());

		if (vazaoCorretor.getDadosAuditoria() != null) {
			integracaoBemMovimentacao.setUsuario(String.valueOf(vazaoCorretor.getDadosAuditoria().getUsuario().getChavePrimaria()));
		}

		return integracaoBemMovimentacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#inserirIntegracaoNotaFiscal(br.com.ggas.faturamento.DocumentoFiscal,
	 * java.lang.Boolean,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void inserirIntegracaoNotaFiscal(DocumentoFiscal documentoFiscal, Boolean ehEmitenteProprio, DadosAuditoria dadosAuditoria,
											StringBuilder logProcessamento) throws GGASException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);


		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorFatura ctlFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ConstanteSistema constanteSistemaTipoOperacaoSaida = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA);

		ConstanteSistema constanteSistemaTipoFaturamentoProduto =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_FATURAMENTO_PRODUTO);

		ConstanteSistema constanteSistemaSituacaoNaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		ConstanteSistema constanteSistemaIntegracaoSistemaGGAS =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);

		ConstanteSistema constanteSistemaOperacaoInclusao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalNormal =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_NORMAL);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalDevolucao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_DEVOLUCAO);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalEntrada =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_ENTRADA);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalSaida =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_SAIDA);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalProduto =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_PRODUTO);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalServico =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_SERVICO);

		ConstanteSistema constanteSistemaIntegracaoGerarNotaFiscal =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalTerceiros =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_TERCEIROS);

		ConstanteSistema constanteSistemaIntegracaoNotaFiscalProprio =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_NOTA_FISCAL_PROPRIO);

		ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Long idRubricaConsumoGas = Long.valueOf(ctlConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_CONSUMO_GAS));

		Long codigoRubricaCreditoDescontoIcmsProdesin =
						Long.parseLong(ctlConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_DESCONTO_ICMS_PRODESIN));

		Long tipoDebito = Long.valueOf(ctlConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO));

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idIntegracaoFuncao", Long.parseLong(constanteSistemaIntegracaoGerarNotaFiscal.getValor()));
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao = this.consultarIntegracaoSistemaFuncao(filtro);



		// ROTINA CRIADA PARA EVITAR DE CANCELAR UMA NOTA FISCAL QUE AINDA NÃO FOI INTEGRADA
		if (!String.valueOf(documentoFiscal.getTipoOperacao().getChavePrimaria()).equals(constanteSistemaTipoOperacaoSaida.getValor())) {

			for (IntegracaoNotaFiscal integracao : this.consultarIntegracaoNotaFiscal(documentoFiscal)) {
				ConstanteSistema constanteSistemaSituacaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
				if (!integracao.getOperacao().equals(constanteSistemaSituacaoProcessado.getValor())) {
					throw new NegocioException(Constantes.ERRO_INTEGRACAO_NOTA_FISCAL_NAO_INTEGRADA, true);
				}
			}
		}

		ParametroSistema referenciaIntegracaoTituloNota =
				controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

		ConstanteSistema referenciaIntegracaoContrato =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

		MultiKeyMap mapeamentosPorIntegrantes =
				this.consultarMapeamentosPorSistemasIntegrantes(this.agruparSiglasIntegracaoSistema(listaIntegracaoSistemaFuncao));

		for (IntegracaoSistemaFuncao integracaoSistemaFuncao : listaIntegracaoSistemaFuncao) {
			IntegracaoNotaFiscal integracaoNotaFiscal = (IntegracaoNotaFiscal) this.criarIntegracaoNotaFiscal();

			if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
				boolean isFaturamentoAgrupado = controladorFatura.isFaturaAgrupadora(documentoFiscal.getFatura());
				IntegracaoContrato contratoIntegrado = null;
				if (isFaturamentoAgrupado) {
					Collection<Fatura> listaFaturasFilhas = controladorFatura.consultarFaturaPorFaturaAgrupamento(
							documentoFiscal.getFatura().getChavePrimaria());
					if (listaFaturasFilhas != null && !listaFaturasFilhas.isEmpty()) {
						contratoIntegrado = this.obterIntegracaoContrato(documentoFiscal.getFatura().getContrato(),
								listaFaturasFilhas.iterator().next().getPontoConsumo());
					}
				} else {
					contratoIntegrado = this.obterIntegracaoContrato(documentoFiscal.getFatura().getContrato(),
							documentoFiscal.getFatura().getPontoConsumo());
				}
				integracaoNotaFiscal.setContrato(contratoIntegrado);
			} else {
				IntegracaoCliente clienteIntegrado = this.buscarIntegracaoCliente(documentoFiscal.getFatura().getCliente());
				integracaoNotaFiscal.setCliente(clienteIntegrado);
			}

			integracaoNotaFiscal.setFatura(documentoFiscal.getFatura().getChavePrimaria());
			if (documentoFiscal.getNaturezaOperacaoCFOP() != null) {
				integracaoNotaFiscal.setCfop(Long.valueOf(documentoFiscal.getNaturezaOperacaoCFOP().getCodigoCFOP()));
			}

			if (documentoFiscal.getSerie() != null) {

				integracaoNotaFiscal.setSerie(documentoFiscal.getSerie().getChavePrimaria());
			}
			integracaoNotaFiscal.setNumero(documentoFiscal.getNumero());
			integracaoNotaFiscal.setNumeroSerie(documentoFiscal.getDescricaoSerie());
			integracaoNotaFiscal.setInscricaoEstadualSubstituicao(documentoFiscal.getInscricaoEstadualSubs());
			integracaoNotaFiscal.setValorTotal(documentoFiscal.getValorTotal());
			integracaoNotaFiscal.setNomeCliente(documentoFiscal.getNomeCliente());
			integracaoNotaFiscal.setNumeroCpfCnpj(documentoFiscal.getCpfCpnj());
			integracaoNotaFiscal.setNumeroRG(documentoFiscal.getRg());
			integracaoNotaFiscal.setEndereco(documentoFiscal.getEndereco());
			integracaoNotaFiscal.setComplementoEndereco(documentoFiscal.getComplemento());
			integracaoNotaFiscal.setNomeBairro(documentoFiscal.getBairro());
			integracaoNotaFiscal.setNumeroCEP(documentoFiscal.getCep());
			integracaoNotaFiscal.setNomeMunicipio(documentoFiscal.getMunicipio());
			integracaoNotaFiscal.setUf(documentoFiscal.getUf());
			integracaoNotaFiscal.setDataApresentacao(documentoFiscal.getApresentacao());
			integracaoNotaFiscal.setDataEmissao(documentoFiscal.getDataEmissao());
			integracaoNotaFiscal.setMensagem(documentoFiscal.getMensagem());
			integracaoNotaFiscal.setDocumentoFiscalOrigem(documentoFiscal.getChavePrimaria());

			Contrato contratoDocumento = documentoFiscal.getFatura().getContratoAtual();
			if (contratoDocumento.getChavePrimariaPai() == null) {
				contratoDocumento = (Contrato) controladorContrato.obter(documentoFiscal.getFatura().getContrato().getChavePrimaria());
			} else {
				contratoDocumento = controladorContrato.
						consultarContratoAtivoPorContratoPai(documentoFiscal.getFatura().getContratoAtual().getChavePrimariaPai());
			}
			if (contratoDocumento.getArrecadadorContratoConvenio() != null) {
				integracaoNotaFiscal.setConvenioArrecadador(contratoDocumento.getArrecadadorContratoConvenio().getCodigoConvenio());
			}
			if (contratoDocumento.getBanco() != null) {
				integracaoNotaFiscal.setBancoCliente(contratoDocumento.getBanco().getCodigoBanco());
			}
			integracaoNotaFiscal.setAgenciaCliente(contratoDocumento.getAgencia());
			integracaoNotaFiscal.setContaCliente(contratoDocumento.getContaCorrente());

			if (String.valueOf(documentoFiscal.getTipoOperacao().getChavePrimaria()).equals(constanteSistemaTipoOperacaoSaida.getValor())) {
				integracaoNotaFiscal.setTipoNotaFiscal(constanteSistemaIntegracaoNotaFiscalNormal.getValor());
				integracaoNotaFiscal.setTipoOperacao(constanteSistemaIntegracaoNotaFiscalSaida.getValor());
			} else {
				integracaoNotaFiscal.setTipoNotaFiscal(constanteSistemaIntegracaoNotaFiscalDevolucao.getValor());
				integracaoNotaFiscal.setTipoOperacao(constanteSistemaIntegracaoNotaFiscalEntrada.getValor());
			}

			if (String.valueOf(documentoFiscal.getTipoFaturamento().getChavePrimaria()).equals(
					constanteSistemaTipoFaturamentoProduto.getValor())) {
				integracaoNotaFiscal.setTipoFaturamento(constanteSistemaIntegracaoNotaFiscalProduto.getValor());
			} else {
				integracaoNotaFiscal.setTipoFaturamento(constanteSistemaIntegracaoNotaFiscalServico.getValor());
			}

			if (documentoFiscal.getTipoEmissaoNfe() != null) {
				integracaoNotaFiscal.setTipoEmissaoNFE(documentoFiscal.getTipoEmissaoNfe().getCodigo());
			}
			if (ehEmitenteProprio) {
				integracaoNotaFiscal.setTipoEmitente(constanteSistemaIntegracaoNotaFiscalProprio.getValor());
			} else {
				integracaoNotaFiscal.setTipoEmitente(constanteSistemaIntegracaoNotaFiscalTerceiros.getValor());
			}

			integracaoNotaFiscal.setStatusNFE(documentoFiscal.getStatusNfe().getCodigo());

			integracaoNotaFiscal.setChaveAcessoNFE(documentoFiscal.getChaveAcesso());
			integracaoNotaFiscal.setProtocolo(documentoFiscal.getNumeroProtocolo());
			integracaoNotaFiscal.setOperacao(constanteSistemaOperacaoInclusao.getValor());
			integracaoNotaFiscal.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
			integracaoNotaFiscal.setSistemaOrigem(constanteSistemaIntegracaoSistemaGGAS.getValor());
			integracaoNotaFiscal.setSistemaDestino(integracaoSistemaFuncao.getIntegracaoSistema().getSigla());
			integracaoNotaFiscal.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
			integracaoNotaFiscal.setDadosAuditoria(dadosAuditoria);

			Collection<FaturaItem> listaFaturaItem = documentoFiscal.getFatura().getListaFaturaItem();
			
			try {
				listaFaturaItem.size();
			} catch (LazyInitializationException e) {
				LOG.error(e.getMessage(), e);
				listaFaturaItem = ctlFatura.listarFaturaItemPorChaveFatura(documentoFiscal.getFatura().getChavePrimaria());
			}

			List<FaturaItem> listaFaturaItemFinal = new ArrayList<FaturaItem>();
			FaturaItem faturaItemProdezin = null;
			for (FaturaItem faturaItem : listaFaturaItem) {
				if (faturaItem.getRubrica() != null) {

					if (faturaItem.getRubrica().getChavePrimaria() == codigoRubricaCreditoDescontoIcmsProdesin) {
						faturaItemProdezin = faturaItem;
					} else {
						listaFaturaItemFinal.add(faturaItem);
					}

				}

			}
			
			BigDecimal valorCredito = retornarValorCreditoFaturaItem(listaFaturaItemFinal);

			if (faturaItemProdezin != null && faturaItemProdezin.getValorTotal() != null) {
				integracaoNotaFiscal.setValorDesconto(faturaItemProdezin.getValorTotal());
			} else {
				integracaoNotaFiscal.setValorDesconto(BigDecimal.ZERO);
			}
			
			integracaoNotaFiscal.setValorDesconto(integracaoNotaFiscal.getValorDesconto().add(valorCredito));
			
			super.inserir(integracaoNotaFiscal);

			if(logProcessamento != null){
				logProcessamento.append(documentoFiscal.getNumero()).append(" - ").append(documentoFiscal.getNomeCliente()).
						append(" - ").append(documentoFiscal.getValorTotal());
				logProcessamento.append("\n");
			}

			Boolean descontoProdesinUtilizado = Boolean.FALSE;
			for (FaturaItem faturaItem : listaFaturaItemFinal) {
				if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getIndicadorComposicaoNotaFiscal() != null
						&& faturaItem.getRubrica().getIndicadorComposicaoNotaFiscal()) {
					ControladorRubrica controladorRubrica =
							(ControladorRubrica) ServiceLocator.getInstancia().getBeanPorID(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);
					Rubrica rubrica = (Rubrica) controladorRubrica.obter(faturaItem.getRubrica().getChavePrimaria());
					if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == tipoDebito) {
						IntegracaoNotaFiscalItem integracaoNotaFiscalItem =
								populaIntegracaoNotaFiscalItem(documentoFiscal, constanteSistemaSituacaoNaoProcessado,
										integracaoSistemaFuncao, integracaoNotaFiscal, faturaItem);

						descontoProdesinUtilizado =
								aplicaDescontoProdesin(idRubricaConsumoGas, faturaItemProdezin, faturaItem, integracaoNotaFiscalItem,
										descontoProdesinUtilizado);

						integracaoNotaFiscalItem.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
						integracaoNotaFiscalItem.setDadosAuditoria(dadosAuditoria);

						Collection<FaturaItemTributacao> listaFaturaItemTributacao = ctlFatura.listarFaturaItemTributacao(faturaItem);
						BigDecimal valorImpostoSubstituto = carregaValorImpostoSubstituto(listaFaturaItemTributacao);
						integracaoNotaFiscalItem.setValorTotal(integracaoNotaFiscalItem.getValorTotal()
								.add(valorImpostoSubstituto).subtract(valorCredito));
						
						integracaoNotaFiscalItem
								.setValorDesconto(integracaoNotaFiscalItem.getValorDesconto().add(valorCredito));

						super.inserir(integracaoNotaFiscalItem);

						// Usado para o prodesin
						BigDecimal baseCalculo = BigDecimal.ZERO;

						for (FaturaItemTributacao faturaItemTributacao : listaFaturaItemTributacao) {

							IntegracaoNotaFiscalItemTributo integracaoNotaFiscalItemTributo =
									(IntegracaoNotaFiscalItemTributo) criarIntegracaoNotaFiscalTributo();

							integracaoNotaFiscalItemTributo.setNotaFiscalItem(integracaoNotaFiscalItem);

							integracaoNotaFiscalItemTributo.setTributo(this.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes,
									faturaItemTributacao.getTributo(), integracaoSistemaFuncao.getIntegracaoSistema()));

							if (faturaItemTributacao.getPercentualAliquota() != null
									&& faturaItemTributacao.getPercentualAliquota().compareTo(BigDecimal.ZERO) > 0) {
								integracaoNotaFiscalItemTributo.setPercentualAliquota(faturaItemTributacao.getPercentualAliquota().multiply(
										new BigDecimal(CEM)));
							} else {
								integracaoNotaFiscalItemTributo.setPercentualAliquota(faturaItemTributacao.getPercentualAliquota());
							}

							integracaoNotaFiscalItemTributo.setBaseCalculo(faturaItemTributacao.getValorBaseCalculo());
							integracaoNotaFiscalItemTributo.setValorTributo(faturaItemTributacao.getValorImposto());
							integracaoNotaFiscalItemTributo.setBaseCalculoSubstituto(faturaItemTributacao.getValorBaseSubstituicao());
							integracaoNotaFiscalItemTributo.setValorSubstituto(faturaItemTributacao.getValorSubstituicao());
							integracaoNotaFiscalItemTributo.setOperacao(constanteSistemaOperacaoInclusao.getValor());
							integracaoNotaFiscalItemTributo.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
							integracaoNotaFiscalItemTributo.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
							integracaoNotaFiscalItemTributo.setDadosAuditoria(dadosAuditoria);
							integracaoNotaFiscalItemTributo.setIndicadorIsencao(false);
							integracaoNotaFiscalItemTributo.setIndicadorProdesin(false);

							baseCalculo = faturaItemTributacao.getValorBaseCalculo();

							super.inserir(integracaoNotaFiscalItemTributo);
						}
						// tributo Isento
						if (documentoFiscal.getFatura().getPontoConsumo() != null && integracaoNotaFiscalItem.getChavePrimaria() != 0
								&& faturaItem.getRubrica() != null && faturaItem.getRubrica().getChavePrimaria() == idRubricaConsumoGas) {

							PontoConsumo pontoConsumo = documentoFiscal.getFatura().getPontoConsumo();
							for (PontoConsumoTributoAliquota pontoConsumoTributoAliquota : pontoConsumo.getListaPontoConsumoTributoAliquota()) {

								IntegracaoNotaFiscalItemTributo integracaoNotaFiscalItemTributo =
										(IntegracaoNotaFiscalItemTributo) criarIntegracaoNotaFiscalTributo();

								integracaoNotaFiscalItemTributo.setNotaFiscalItem(integracaoNotaFiscalItem);

								integracaoNotaFiscalItemTributo.setTributo(this.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes,
										pontoConsumoTributoAliquota.getTributo(), integracaoSistemaFuncao.getIntegracaoSistema()));

								integracaoNotaFiscalItemTributo.setPercentualAliquota(pontoConsumoTributoAliquota.getPorcentagemAliquota());

								if (pontoConsumoTributoAliquota.getPorcentagemAliquota() != null
										&& pontoConsumoTributoAliquota.getPorcentagemAliquota().compareTo(BigDecimal.ZERO) > 0) {
									integracaoNotaFiscalItemTributo.setPercentualAliquota(pontoConsumoTributoAliquota.getPorcentagemAliquota()
											.multiply(new BigDecimal(CEM)));
								} else {
									integracaoNotaFiscalItemTributo.setPercentualAliquota(pontoConsumoTributoAliquota.getPorcentagemAliquota());
								}

								integracaoNotaFiscalItemTributo.setBaseCalculo(baseCalculo);
								if (pontoConsumoTributoAliquota.getIndicadorCreditoIcms() != null
										&& pontoConsumoTributoAliquota.getIndicadorCreditoIcms()) {
									// Usado para o prodesin
									integracaoNotaFiscalItemTributo.setValorOutros(baseCalculo);
								} else {
									integracaoNotaFiscalItemTributo.setValorOutros(BigDecimal.ZERO);
								}

								integracaoNotaFiscalItemTributo.setValorTributo(BigDecimal.ZERO);
								integracaoNotaFiscalItemTributo.setBaseCalculoSubstituto(BigDecimal.ZERO);
								integracaoNotaFiscalItemTributo.setValorSubstituto(BigDecimal.ZERO);
								integracaoNotaFiscalItemTributo.setOperacao(constanteSistemaOperacaoInclusao.getValor());
								integracaoNotaFiscalItemTributo.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
								integracaoNotaFiscalItemTributo.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
								integracaoNotaFiscalItemTributo.setDadosAuditoria(dadosAuditoria);
								integracaoNotaFiscalItemTributo.setIndicadorIsencao(pontoConsumoTributoAliquota.getIndicadorIsencao());
								integracaoNotaFiscalItemTributo.setIndicadorProdesin(pontoConsumoTributoAliquota.getIndicadorCreditoIcms());
								super.inserir(integracaoNotaFiscalItemTributo);
							}
						}

						// ICMS Substituido pela BR, não tem ICMS e so manda pra a integracao
						if (documentoFiscal.getFatura().getPontoConsumo() != null
								&& (documentoFiscal.getFatura().getPontoConsumo().getListaPontoConsumoTributoAliquota() == null || documentoFiscal
								.getFatura().getPontoConsumo().getListaPontoConsumoTributoAliquota().isEmpty())) {

							TarifaVigencia tarifaVigencia = null;
							Date dataTarifaVigente = null;
							if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getChavePrimaria() == idRubricaConsumoGas
									&& documentoFiscal.getFatura().getHistoricoConsumo() != null && faturaItem.getTarifa() != null) {

								dataTarifaVigente =
										documentoFiscal.getFatura().getHistoricoConsumo().getHistoricoAtual().getDataLeituraFaturada();
								ControladorTarifa controladorTarifa =
										(ControladorTarifa) ServiceLocator.getInstancia().getBeanPorID(
												ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);
								tarifaVigencia =
										controladorTarifa.consultarTarifaVigenciaAnteriorMaisRecente(faturaItem.getTarifa()
												.getChavePrimaria(), dataTarifaVigente);

								if (tarifaVigencia != null && tarifaVigencia.getInformarMsgIcmsSubstituido() != null
										&& tarifaVigencia.getInformarMsgIcmsSubstituido().equals(Boolean.TRUE)) {

									String paramICMS = ctlConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS);
									ControladorTributo controladorTributo =
											(ControladorTributo) ServiceLocator.getInstancia().getBeanPorID(
													ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);
									Long idIcms = Long.valueOf(paramICMS);
									Tributo tributoIcms = controladorTributo.obterTributo(idIcms);

									IntegracaoNotaFiscalItemTributo integracaoNotaFiscalItemTributo =
											(IntegracaoNotaFiscalItemTributo) criarIntegracaoNotaFiscalTributo();

									integracaoNotaFiscalItemTributo.setNotaFiscalItem(integracaoNotaFiscalItem);

									integracaoNotaFiscalItemTributo.setTributo(this.getCodigoEntidadeSistemaIntegrante(
											mapeamentosPorIntegrantes, tributoIcms, integracaoSistemaFuncao.getIntegracaoSistema()));

									integracaoNotaFiscalItemTributo.setPercentualAliquota(BigDecimal.ZERO);
									integracaoNotaFiscalItemTributo.setBaseCalculo(BigDecimal.ZERO);
									integracaoNotaFiscalItemTributo.setValorTributo(BigDecimal.ZERO);
									integracaoNotaFiscalItemTributo.setBaseCalculoSubstituto(BigDecimal.ZERO);
									integracaoNotaFiscalItemTributo.setValorSubstituto(BigDecimal.ZERO);
									integracaoNotaFiscalItemTributo.setValorOutros(faturaItem.getValorTotal());
									integracaoNotaFiscalItemTributo.setOperacao(constanteSistemaOperacaoInclusao.getValor());
									integracaoNotaFiscalItemTributo.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
									integracaoNotaFiscalItemTributo.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
									integracaoNotaFiscalItemTributo.setDadosAuditoria(dadosAuditoria);
									integracaoNotaFiscalItemTributo.setIndicadorIsencao(false);
									integracaoNotaFiscalItemTributo.setIndicadorProdesin(false);
									super.inserir(integracaoNotaFiscalItemTributo);
								}
							}
						}
					}
				}
			}
		}
	}

	private BigDecimal retornarValorCreditoFaturaItem(Collection<FaturaItem> listaFaturaItem) {
		BigDecimal retorno = BigDecimal.ZERO;

		for (FaturaItem faturaItem : listaFaturaItem) {
			LancamentoItemContabil lancamentoItemContabil = faturaItem.getRubrica().getLancamentoItemContabil();
			
			if("CRÉDITO".equals(lancamentoItemContabil.getTipoCreditoDebito().getDescricao())) {
				retorno = retorno.add(faturaItem.getValorTotal());
			}
		}

		return retorno;
	}

	private BigDecimal carregaValorImpostoSubstituto(Collection<FaturaItemTributacao> listaFaturaItemTributacao) {

		BigDecimal valorImpostoSubstituto = BigDecimal.ZERO;
		for (FaturaItemTributacao faturaItemTributacao : listaFaturaItemTributacao) {
			if (faturaItemTributacao.getValorSubstituicao() != null
							&& faturaItemTributacao.getValorSubstituicao().compareTo(BigDecimal.ZERO) > 0) {

				valorImpostoSubstituto = valorImpostoSubstituto.add(faturaItemTributacao.getValorSubstituicao());

			}
		}
		return valorImpostoSubstituto;
	}

	private IntegracaoNotaFiscalItem populaIntegracaoNotaFiscalItem(DocumentoFiscal documentoFiscal,
					ConstanteSistema constanteSistemaSituacaoNaoProcessado, IntegracaoSistemaFuncao integracaoSistemaFuncao,
					IntegracaoNotaFiscal integracaoNotaFiscal, FaturaItem faturaItem) throws NegocioException {

		IntegracaoNotaFiscalItem integracaoNotaFiscalItem = (IntegracaoNotaFiscalItem) criarIntegracaoNotaFiscalItem();
		if (faturaItem.getSegmento() != null) {
			integracaoNotaFiscalItem.setCodigoSegmento(buscarCodigoSistemaIntegranteNumerico(faturaItem.getSegmento().getChavePrimaria(),
							faturaItem.getSegmento().getClass().getInterfaces()[0].getName(), integracaoSistemaFuncao
											.getIntegracaoSistema().getSigla()));
			if (documentoFiscal.getFatura().getPontoConsumo() != null
							&& !documentoFiscal.getFatura().getPontoConsumo().getListaPontoConsumoTributoAliquota().isEmpty()) {
				// em caso da excecao tributaria o sistema passa o campo null e no ERP eh configurado a excecao tributaria
				integracaoNotaFiscalItem.setDescricaoClassificacaoFiscal(null);
			} else if (integracaoNotaFiscalItem.getCodigoSegmento() != null) {
				integracaoNotaFiscalItem.setDescricaoClassificacaoFiscal(integracaoNotaFiscalItem.getCodigoSegmento().toString());
			}

		}

		if(faturaItem.getFatura() != null && faturaItem.getFatura().getPontoConsumo() != null
						&& faturaItem.getFatura().getPontoConsumo().getRamoAtividade() != null) {

			integracaoNotaFiscalItem.setCodigoRamoAtividade(buscarCodigoSistemaIntegranteNumerico(faturaItem.getFatura().getPontoConsumo()
							.getRamoAtividade().getChavePrimaria(), faturaItem.getFatura().getPontoConsumo().getRamoAtividade().getClass()
							.getInterfaces()[0].getName(),
							integracaoSistemaFuncao.getIntegracaoSistema().getSigla()));

		}

		preencherCodigoSituacaoTributaria(integracaoNotaFiscalItem, faturaItem, documentoFiscal);

		integracaoNotaFiscalItem.setNotaFiscal(integracaoNotaFiscal);
		integracaoNotaFiscalItem.setValorTotal(faturaItem.getValorTotal());
		if (faturaItem.getQuantidade().compareTo(BigDecimal.ZERO) > 0) {
			integracaoNotaFiscalItem.setValorUnitario(faturaItem.getValorTotal()
							.divide(faturaItem.getQuantidade(), 8, RoundingMode.HALF_UP));
		} else {
			integracaoNotaFiscalItem.setValorUnitario(faturaItem.getValorTotal());
		}

		integracaoNotaFiscalItem.setCodigoItem(buscarCodigoSistemaIntegranteNumerico(faturaItem.getRubrica().getChavePrimaria(), faturaItem
						.getRubrica().getClass().getInterfaces()[0].getName(), integracaoSistemaFuncao.getIntegracaoSistema().getSigla()));
		integracaoNotaFiscalItem.setDescricao(faturaItem.getRubrica().getDescricao());

		integracaoNotaFiscalItem.setQuantidade(faturaItem.getQuantidade());
		if (faturaItem.getNaturezaOperacaoCFOP() != null) {
			integracaoNotaFiscalItem.setCfop(Long.valueOf(faturaItem.getNaturezaOperacaoCFOP().getCodigoCFOP()));
		}
		if (faturaItem.getRubrica().getCodigoCEST() != null) {
			integracaoNotaFiscalItem.setCodigoCEST(faturaItem.getRubrica().getCodigoCEST());
		}
		integracaoNotaFiscalItem.setSequencia(faturaItem.getNumeroSequencial());
		integracaoNotaFiscalItem.setOperacao(ServiceLocator.getInstancia().getControladorConstanteSistema()
						.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO).getValor());
		integracaoNotaFiscalItem.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
		return integracaoNotaFiscalItem;
	}

	private Boolean aplicaDescontoProdesin(Long idRubricaConsumoGas, FaturaItem faturaItemProdezin, FaturaItem faturaItem,
					IntegracaoNotaFiscalItem integracaoNotaFiscalItem, Boolean descontoProdesinUtilizado) {

		Boolean descontoProdesinUtilizadoAux = Boolean.FALSE;
		if (faturaItem.getRubrica().getChavePrimaria() == idRubricaConsumoGas && !descontoProdesinUtilizado) {
			descontoProdesinUtilizadoAux = Boolean.TRUE;
			// populando desconto
			if (faturaItemProdezin != null && faturaItemProdezin.getValorTotal() != null) {
				integracaoNotaFiscalItem.setValorDesconto(faturaItemProdezin.getValorTotal());
				integracaoNotaFiscalItem.setValorTotal(faturaItem.getValorTotal().subtract(faturaItemProdezin.getValorTotal()));
			} else {
				integracaoNotaFiscalItem.setValorDesconto(BigDecimal.ZERO);
			}
		} else {
			integracaoNotaFiscalItem.setValorDesconto(BigDecimal.ZERO);
		}
		return descontoProdesinUtilizadoAux;
	}

	@Override
	public void preencherCodigoSituacaoTributaria(IntegracaoNotaFiscalItem integracaoNotaFiscalItem, FaturaItem faturaItem,
					DocumentoFiscal documentoFiscal) throws NegocioException {

		ControladorParametroSistema ctlParametroSistema =
						(ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorFatura ctlFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorRubrica controladorRubrica =
						(ControladorRubrica) ServiceLocator.getInstancia().getBeanPorID(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);

		Long idTributoICMS = Long.valueOf((String) ctlParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TRIBUTO_ICMS));
		Long idTributoPIS = Long.valueOf((String) ctlParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TRIBUTO_PIS));
		Long idTributoCOFINS =
						Long.valueOf((String) ctlParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TRIBUTO_COFINS));

		// codigo situaçlão tributaria PIS e
		// COFINS (CST)
		Long codigoAliquotaNormalPisCofins =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_NORMAL_PIS_COFINS));
		Long codigoAliquotaDiferenciadaPisCofins =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_DIFERENCIADA_PIS_COFINS));
		Long codigoAliquotaIsentoPisCofins =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_ISENTO_PIS_COFINS));
		Long codigoAliquotaSemTributacaoPisCofins =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS));

		EntidadeConteudo aliquotaNormalPisCofins = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaNormalPisCofins);
		EntidadeConteudo aliquotaDiferenciadaPisCofins =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaDiferenciadaPisCofins);
		EntidadeConteudo aliquotaIsentoPisCofins = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaIsentoPisCofins);
		EntidadeConteudo aliquotaSemTributacaoPisCofins =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaSemTributacaoPisCofins);

		// codigo situaçlão tributaria ICMS (CST)
		Long codigoAliquotaTributadaIntegralmenteIcms =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS));
		Long codigoAliquotaTributadaCobrancaIcmsPorSubstTributaria =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_TRIBUTADA_COBRANCA_ICMS_POR_SUBST_TRIBUTARIA));
		Long codigoAliquotaComReducaoBaseCalculoIcms =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_COM_REDUCAO_BASE_CALCULO_ICMS));
		Long codigoAliquotaIsentaIcms =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_ISENTA_ICMS));
		Long codigoAliquotaNaoTributadaIcms =
						Long.parseLong((String) ctlParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_FATURAMENTO_ALIQUOTA_NAO_TRIBUTADA_ICMS));

		EntidadeConteudo aliquotaTributadaIntegralmenteIcms =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaTributadaIntegralmenteIcms);
		EntidadeConteudo aliquotaTributadaCobrancaIcmsPorSubstTributaria =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaTributadaCobrancaIcmsPorSubstTributaria);
		EntidadeConteudo aliquotaComReducaoBaseCalculoIcms =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaComReducaoBaseCalculoIcms);
		EntidadeConteudo aliquotaIsentaIcms = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaIsentaIcms);
		EntidadeConteudo aliquotaNaoTributadaIcms = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaNaoTributadaIcms);

		// obtendo o ponto de consumo e pegando o
		// ponto de consumo da fatura agrupada
		// caso precise

		Fatura fatura = null;
		if (documentoFiscal != null) {
			fatura = documentoFiscal.getFatura();
		} else {
			fatura = faturaItem.getFatura();
		}

		PontoConsumo pontoConsumo = fatura.getPontoConsumo();
		if (pontoConsumo == null) {
			Collection<Fatura> listaFaturasFilhas = ctlFatura.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
			pontoConsumo = listaFaturasFilhas.iterator().next().getPontoConsumo();
		}

		Boolean temICMS = false;
		Boolean temPIS = false;
		Boolean temCOFINS = false;

		Boolean icmsIsento = false;
		Boolean pisIsento = false;
		Boolean cofinsIsento = false;

		Boolean cofinsDiferenciado = false;
		Boolean pisDiferenciado = false;
		Boolean icmsDiferenciado = false;

		for (PontoConsumoTributoAliquota pcta : pontoConsumo.getListaPontoConsumoTributoAliquota()) {

			if (fatura.getDataEmissao().compareTo(pcta.getDataInicioVigencia()) >= 0
							&& (pcta.getDataFimVigencia() == null || fatura.getDataEmissao().compareTo(pcta.getDataFimVigencia()) <= 0)) {

				if (pcta.getTributo().getChavePrimaria() == idTributoICMS) {
					icmsIsento = pcta.getIndicadorIsencao();
					if (!icmsIsento) {
						icmsDiferenciado = true;
					}
				} else if (pcta.getTributo().getChavePrimaria() == idTributoPIS) {
					pisIsento = pcta.getIndicadorIsencao();
					if (!pisIsento) {
						pisDiferenciado = true;
					}
				} else if (pcta.getTributo().getChavePrimaria() == idTributoCOFINS) {
					cofinsIsento = pcta.getIndicadorIsencao();
					if (!cofinsIsento) {
						cofinsDiferenciado = true;
					}
				}

			}
		}
		ControladorTarifa controladorTarifa =
						(ControladorTarifa) ServiceLocator.getInstancia().getBeanPorID(ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);
		if (faturaItem.getTarifa() != null) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idTarifa", faturaItem.getTarifa().getChavePrimaria());
			Collection<TarifaVigenciaTributo> listaTarifaTributo = controladorTarifa.consultarTarifasTributo(filtro);
			for (TarifaVigenciaTributo tarifaTributo : listaTarifaTributo) {
				if (tarifaTributo.getTributo().getChavePrimaria() == idTributoICMS) {
					temICMS = true;
				} else if (tarifaTributo.getTributo().getChavePrimaria() == idTributoPIS) {
					temPIS = true;
				} else if (tarifaTributo.getTributo().getChavePrimaria() == idTributoCOFINS) {
					temCOFINS = true;
				}
			}
		} else {
			Rubrica rubrica = (Rubrica) controladorRubrica.obter(faturaItem.getRubrica().getChavePrimaria(), "tributos");
			for (RubricaTributo rubricaTributo : rubrica.getTributos()) {
				if (rubricaTributo.getTributo().getChavePrimaria() == idTributoICMS) {
					temICMS = true;
				} else if (rubricaTributo.getTributo().getChavePrimaria() == idTributoPIS) {
					temPIS = true;
				} else if (rubricaTributo.getTributo().getChavePrimaria() == idTributoCOFINS) {
					temCOFINS = true;
				}
			}
		}

		// setar codigo da situação tributaria
		// do PIS (CST)
		if (temPIS && !pisIsento && !pisDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaPis(aliquotaNormalPisCofins.getCodigo());
		} else if (temPIS && pisDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaPis(aliquotaDiferenciadaPisCofins.getCodigo());
		} else if (temPIS && pisIsento) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaPis(aliquotaIsentoPisCofins.getCodigo());
		} else {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaPis(aliquotaSemTributacaoPisCofins.getCodigo());
		}

		// setar codigo da situação tributaria
		// do COFINS (CST)
		if (temCOFINS && !cofinsIsento && !cofinsDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaCofins(aliquotaNormalPisCofins.getCodigo());
		} else if (temCOFINS && cofinsDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaCofins(aliquotaDiferenciadaPisCofins.getCodigo());
		} else if (temCOFINS && cofinsIsento) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaCofins(aliquotaIsentoPisCofins.getCodigo());
		} else {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaCofins(aliquotaSemTributacaoPisCofins.getCodigo());
		}

		Collection<FaturaTributacao> listaFaturaTributacao = ctlFatura.consultarFaturaTributacaoPorFatura(fatura.getChavePrimaria());

		boolean temIcmsSubst = false;
		for (FaturaTributacao faturaTributacao : listaFaturaTributacao) {
			if (faturaTributacao.getTributo().getChavePrimaria() == idTributoICMS && faturaTributacao.getValorBaseSubstituicao() != null
							&& faturaTributacao.getValorBaseSubstituicao().compareTo(BigDecimal.ZERO) > 0) {

				temIcmsSubst = true;
				break;
			}
		}

		// setar codigo da situação tributaria
		// do ICMS (CST)
		if (temICMS && !temIcmsSubst && !icmsIsento && !icmsDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaIcms(aliquotaTributadaIntegralmenteIcms.getCodigo());
		} else if (temICMS && temIcmsSubst && !icmsIsento && !icmsDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaIcms(aliquotaTributadaCobrancaIcmsPorSubstTributaria.getCodigo());
		} else if (temICMS && icmsDiferenciado) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaIcms(aliquotaComReducaoBaseCalculoIcms.getCodigo());
		} else if (temICMS && icmsIsento) {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaIcms(aliquotaIsentaIcms.getCodigo());
		} else {
			integracaoNotaFiscalItem.setCodigoSituacaoTributariaIcms(aliquotaNaoTributadaIcms.getCodigo());
		}

	}

	/**
	 *
	 * Método utilizado para atualizar uma integração Nota fiscal no momento do cancelamento
	 * @param documentoFiscal
	 * @param dadosAuditoria
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 *
	 */
	@Override
	public void atualizarIntegracaoNotaFiscal(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteSistemaSituacaoNaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		ConstanteSistema constanteSistemaOperacaoInclusao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

		ConstanteSistema constanteSistemaOperacaoAlteracao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);

		ConstanteSistema constanteSistemaOperacaoCancelamento =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_CANCELAMENTO);

		Collection<IntegracaoNotaFiscal> listaNotaFiscal = this.consultarIntegracaoNotaFiscal(documentoFiscal);
		
		for (IntegracaoNotaFiscal integracaoNotaFiscal : listaNotaFiscal) {
			integracaoNotaFiscal.setStatusNFE(documentoFiscal.getStatusNfe().getCodigo());
			if (integracaoNotaFiscal.getSituacao().equals(constanteSistemaSituacaoNaoProcessado.getValor())
							&& integracaoNotaFiscal.getSituacao().equals(constanteSistemaOperacaoInclusao.getValor())) {
				integracaoNotaFiscal.setOperacao(constanteSistemaOperacaoInclusao.getValor());
			} else {
				integracaoNotaFiscal.setOperacao(constanteSistemaOperacaoAlteracao.getValor());
			}
			integracaoNotaFiscal.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
			integracaoNotaFiscal.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
			integracaoNotaFiscal.setDadosAuditoria(dadosAuditoria);

			if (documentoFiscal.getFatura().getMotivoCancelamento() != null
							&& documentoFiscal.getFatura().getMotivoCancelamento().getChavePrimaria() > 0) {
				integracaoNotaFiscal.setDataCancelamento(new Date());
				integracaoNotaFiscal.setMotivoCancelamento(documentoFiscal.getFatura().getMotivoCancelamento().getChavePrimaria());
				integracaoNotaFiscal.setOperacao(constanteSistemaOperacaoCancelamento.getValor());
			}

			super.atualizar(integracaoNotaFiscal,
							ServiceLocator.getInstancia().getBeanPorID(IntegracaoNotaFiscal.BEAN_ID_INTEGRACAO_NOTA_FISCAL).getClass());
		}
		
	}

	/**
	 * Consultar integracao nota fiscal.
	 *
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<IntegracaoNotaFiscal> consultarIntegracaoNotaFiscal(DocumentoFiscal documentoFiscal) {

		ServiceLocator.getInstancia().getControladorMedidor();

		Criteria criteria =
						createCriteria(ServiceLocator.getInstancia().getClassPorID(IntegracaoNotaFiscal.BEAN_ID_INTEGRACAO_NOTA_FISCAL));

		criteria.add(Restrictions.eq("fatura", documentoFiscal.getFatura().getChavePrimaria()));
		criteria.add(Restrictions.eq("numeroSerie", documentoFiscal.getDescricaoSerie()));
		criteria.add(Restrictions.eq("numero", documentoFiscal.getNumero()));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#desabilitarRecursosSistema(boolean, long)
	 */
	@Override
	public void desabilitarRecursosSistema(boolean habilitado, long chavePrimariaIntegSistFuncao) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" UPDATE ");
		hql.append(getClasseEntidadeRecurso().getSimpleName());
		hql.append(" as recurso ");
		hql.append(" SET recurso.habilitado = ?");
		hql.append(" WHERE ");
		hql.append(" recurso.operacao.chavePrimaria IN ( ");
		hql.append(" SELECT ");
		hql.append(" integracaoOperacaoInativa.operacaoSistema.chavePrimaria ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoOperacaoInativa().getSimpleName());
		hql.append(" as integracaoOperacaoInativa ");
		hql.append(" WHERE ");
		hql.append(" integracaoOperacaoInativa.integracaoFuncao.chavePrimaria IN ( ");
		hql.append(" SELECT  ");
		hql.append(" integracaoSistemaFuncao.integracaoFuncao.chavePrimaria  ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoSistemaFuncao().getSimpleName());
		hql.append(" as integracaoSistemaFuncao ");
		hql.append(" WHERE ");
		hql.append(" integracaoSistemaFuncao.chavePrimaria = ? ) ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (habilitado) {
			query.setLong(0, 0);
		} else {
			query.setLong(0, 1);
		}
		query.setLong(1, chavePrimariaIntegSistFuncao);

		query.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#obterIntegracaoSistemaFuncaoSituacaoErro(br.com.ggas.integracao.geral.
	 * IntegracaoFuncaoTabela
	 * )
	 */
	@Override
	public boolean obterIntegracaoSistemaFuncaoSituacaoErro(IntegracaoFuncaoTabela intFunTabela) throws GGASException {

		ControladorConstanteSistema controladorConstantesSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		SQLQuery query = null;
		boolean erro = false;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT ");
		hql.append(" COUNT(*) ");
		hql.append(" FROM ");
		hql.append(intFunTabela.getTabela().getNome());
		hql.append(" int ");
		hql.append(" WHERE ");
		hql.append(" int.").append(intFunTabela.getTabela().getMnemonico()).append("_cd_situacao = '");
		hql.append(controladorConstantesSistema.
						obterValorConstanteSistemaPorCodigo(Constantes.C_INTEGRACAO_SITUACAO_ERRO));
		hql.append("' ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());

		BigDecimal operacaoErro = BigDecimal.class.cast(query.uniqueResult());
		if ("0".equals(operacaoErro.toString())) {
			erro = Boolean.FALSE;
		} else {
			erro = Boolean.TRUE;
		}

		return erro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#listarConteudoTabelaColunaSituacaoErro(br.com.ggas.cadastro.imovel.Tabela,
	 * java.util.List, java.lang.Long)
	 */
	@Override
	public List<Object> listarConteudoTabelaColunaSituacaoErro(Tabela tabela, List<AcompanhamentoIntegracaoVO> listaFiltros,
					Long situacaoErro) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		String[] array = tabela.getNomeClasse().split("\\.");
		hql.append(array[array.length - 1]).append("Impl");
		hql.append(" where 1=1 ");

		if (listaFiltros != null) {
			for (AcompanhamentoIntegracaoVO acompanhamentoIntegracaoVO : listaFiltros) {

				if (acompanhamentoIntegracaoVO.getValorColuna() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())
								&& acompanhamentoIntegracaoVO.getValorColuna2() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

					hql.append(" and ").append(acompanhamentoIntegracaoVO.getNomePropriedade()).append(" >= :")
							.append(acompanhamentoIntegracaoVO.getNomeColuna());

					hql.append(" and :").append(acompanhamentoIntegracaoVO.getNomeColuna()).append("2 >= ")
							.append(acompanhamentoIntegracaoVO.getNomePropriedade());

				} else if (acompanhamentoIntegracaoVO.getValorColuna() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())) {

					hql.append(" and ").append(acompanhamentoIntegracaoVO.getNomePropriedade()).append(" = :")
							.append(acompanhamentoIntegracaoVO.getNomeColuna());

				} else if (acompanhamentoIntegracaoVO.getValorColuna2() != null
								&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

					hql.append(" and ").append(acompanhamentoIntegracaoVO.getNomePropriedade()).append(" = :")
							.append(acompanhamentoIntegracaoVO.getNomeColuna()).append("2");

				}
			}
		}


		ControladorConstanteSistema controladorConstantesSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		if (situacaoErro == 1) {

			hql.append(" and situacao = '");
			hql.append(controladorConstantesSistema.
						obterValorConstanteSistemaPorCodigo(Constantes.C_INTEGRACAO_SITUACAO_ERRO));
			hql.append("' ");

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (listaFiltros != null) {
			try {
				for (AcompanhamentoIntegracaoVO acompanhamentoIntegracaoVO : listaFiltros) {

					if (acompanhamentoIntegracaoVO.getValorColuna() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())
									&& acompanhamentoIntegracaoVO.getValorColuna2() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna(), acompanhamentoIntegracaoVO
										.getValorColuna().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna() + "2", acompanhamentoIntegracaoVO
										.getValorColuna2().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

					} else if (acompanhamentoIntegracaoVO.getValorColuna() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna().toString().trim())) {

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna(), acompanhamentoIntegracaoVO
										.getValorColuna().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

					} else if (acompanhamentoIntegracaoVO.getValorColuna2() != null
									&& !"".equals(acompanhamentoIntegracaoVO.getValorColuna2().toString().trim())) {

						Util.setarTipoParametroQuery(query, acompanhamentoIntegracaoVO.getNomeColuna() + "2", acompanhamentoIntegracaoVO
										.getValorColuna2().toString(), acompanhamentoIntegracaoVO.getTipoColuna());

					}
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(ControladorIntegracao.ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO, true);
			}
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#obterEntidadePorClasse(java.lang.Long, java.lang.Class)
	 */
	@Override
	public Object obterEntidadePorClasse(Long chavePrimaria, Class<?> nomeClasse) throws NegocioException {

		return super.obter(chavePrimaria, nomeClasse);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#inserirIntegracaoTituloPagar(br.com.ggas.faturamento.Fatura,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void inserirIntegracaoTituloPagar(Fatura fatura, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorFatura ctlFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ConstanteSistema constanteSistemaSituacaoNaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		ConstanteSistema situacaoProcessado =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

		ConstanteSistema constanteSistemaIntegracaoSistemaGGAS =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);

		ConstanteSistema constanteSistemaOperacaoInclusao =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

		ConstanteSistema constanteSistemaIntegracaoTituloPagar =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);

		ParametroSistema referenciaIntegracaoTituloNota =
				controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

		ConstanteSistema referenciaIntegracaoContrato =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idIntegracaoFuncao", Long.parseLong(constanteSistemaIntegracaoTituloPagar.getValor()));
		List<String> listaIntegracaoSistemaFuncao =
						this.gerarListaSistemasIntegrantesPorFuncao(Long.parseLong(constanteSistemaIntegracaoTituloPagar.getValor()));

		for (String sistemaIntegrante : listaIntegracaoSistemaFuncao) {

			IntegracaoTituloPagar integracaoTituloPagar = (IntegracaoTituloPagar) this.criarIntegracaoTituloPagar();

			if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
				IntegracaoSistemaFuncao integracaoContratoAtiva = this.obterIntegracaoSistemaFuncao(Constantes.C_INTEGRACAO_CONTRATO);
				IntegracaoContrato contratoIntegrado = this.obterIntegracaoContrato(fatura.getContrato(), fatura.getPontoConsumo());
				if (integracaoContratoAtiva != null && (contratoIntegrado == null
						|| !situacaoProcessado.getValor().equals(contratoIntegrado.getSituacao()))) {
					throw new NegocioException(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE, true);
				}
				integracaoTituloPagar.setContrato(contratoIntegrado);
			} else {

				IntegracaoSistemaFuncao integracaoCadastroClienteAtiva = this.obterIntegracaoSistemaFuncao(Constantes.C_INTEGRACAO_CADASTRO_CLIENTE);
				IntegracaoCliente clienteIntegrado = this.buscarIntegracaoCliente(fatura.getCliente());

				if (integracaoCadastroClienteAtiva != null && clienteIntegrado == null) {
					throw new NegocioException(Constantes.ERRO_CLIENTE_INTEGRACAO_INEXISTENTE, true);
				}
				integracaoTituloPagar.setCliente(clienteIntegrado);
			}
			integracaoTituloPagar.setDataEmissao(fatura.getDataEmissao());
			integracaoTituloPagar.setDataVencimento(DataUtil.gerarDataHmsZerados(fatura.getDataVencimento()));
			integracaoTituloPagar.setFatura(fatura.getChavePrimaria());
			integracaoTituloPagar.setObservacaoNota(fatura.getObservacaoNota());
			integracaoTituloPagar.setAnoMesCicloReferencia(fatura.getCicloReferenciaFormatado());
			if (fatura.getSegmento() != null) {
				integracaoTituloPagar.setSegmento(buscarCodigoSistemaIntegranteNumerico(fatura.getSegmento().getChavePrimaria(), fatura
								.getSegmento().getClass().getInterfaces()[0].getName(), sistemaIntegrante));
			}
			integracaoTituloPagar.setValorTotal(fatura.getValorTotal());
			integracaoTituloPagar.setSistemaDestino(sistemaIntegrante);
			integracaoTituloPagar.setSistemaOrigem(constanteSistemaIntegracaoSistemaGGAS.getValor());
			integracaoTituloPagar.setOperacao(constanteSistemaOperacaoInclusao.getValor());
			integracaoTituloPagar.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
			integracaoTituloPagar.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
			integracaoTituloPagar.setDadosAuditoria(dadosAuditoria);

			super.inserir(integracaoTituloPagar);

			Collection<FaturaTributacao> listaFaturaTributacao = ctlFatura.consultarFaturaTributacaoPorFatura(fatura.getChavePrimaria());

			for (FaturaTributacao faturaTributacao : listaFaturaTributacao) {

				IntegracaoTituloPagarTributo integracaoTituloPagarTributo =
								(IntegracaoTituloPagarTributo) this.criarIntegracaoTituloPagarTributo();

				integracaoTituloPagarTributo.setTituloPagar(integracaoTituloPagar);
				integracaoTituloPagarTributo.setAliquota(faturaTributacao.getPercentualAliquota());
				integracaoTituloPagarTributo.setTributo(faturaTributacao.getTributo().getChavePrimaria());
				integracaoTituloPagarTributo.setBaseCalculo(faturaTributacao.getValorBaseCalculo());
				integracaoTituloPagarTributo.setValorImposto(faturaTributacao.getValorImposto());
				integracaoTituloPagarTributo.setBaseCalculoSubstituto(faturaTributacao.getValorBaseSubstituicao());
				integracaoTituloPagarTributo.setValorSubstituto(faturaTributacao.getValorSubstituicao());
				integracaoTituloPagarTributo.setOperacao(constanteSistemaOperacaoInclusao.getValor());
				integracaoTituloPagarTributo.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
				integracaoTituloPagarTributo.setUsuario(String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));
				integracaoTituloPagarTributo.setDadosAuditoria(dadosAuditoria);

				super.inserir(integracaoTituloPagarTributo);

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#cancelarTituloPagar(br.com.ggas.faturamento.Fatura,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void cancelarTituloPagar(Fatura fatura, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteSistemaSituacaoNaoProcessado =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		ConstanteSistema constanteSistemaOperacaoCancelamento =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_CANCELAMENTO);

		ConstanteSistema constanteSistemaIntegracaoTituloPagar =
						ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idIntegracaoFuncao", Long.parseLong(constanteSistemaIntegracaoTituloPagar.getValor()));
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao = this.consultarIntegracaoSistemaFuncao(filtro);

		for (IntegracaoSistemaFuncao integracaoSistemaFuncao : listaIntegracaoSistemaFuncao) {

			String sistemaIntegrante = integracaoSistemaFuncao.getIntegracaoSistema().getSigla();
			Long chavePrimaria = this.buscarTituloPorFatura(fatura.getChavePrimaria(), false, sistemaIntegrante);
			if (chavePrimaria == null) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_TITULO_PAGAR_INEXISTENTE, true);
			}

			Map<String, Object> filtroTituloPagar = new HashMap<String, Object>();
			filtroTituloPagar.put("fatura", fatura.getChavePrimaria());
			filtroTituloPagar.put("sistemaDestino", sistemaIntegrante);

			Collection<IntegracaoTituloPagar> listaIntegracaoTituloPagar = this.consultarIntegracaoTituloPagar(filtroTituloPagar);

			for (IntegracaoTituloPagar integracaoTituloPagar : listaIntegracaoTituloPagar) {

				integracaoTituloPagar.setMotivoCancelamento(buscarCodigoSistemaIntegrante(
								fatura.getMotivoCancelamento().getChavePrimaria(), fatura.getMotivoCancelamento().getClass()
												.getInterfaces()[0].getName(), sistemaIntegrante));
				integracaoTituloPagar.setDataCancelamento(fatura.getDataCancelamento());
				integracaoTituloPagar.setOperacao(constanteSistemaOperacaoCancelamento.getValor());
				integracaoTituloPagar.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
				integracaoTituloPagar.setUsuario(String.valueOf(fatura.getDadosAuditoria().getUsuario().getChavePrimaria()));
				integracaoTituloPagar.setDadosAuditoria(dadosAuditoria);

				super.atualizar(integracaoTituloPagar, IntegracaoTituloPagarImpl.class);

				ServiceLocator.getInstancia().getControladorParametroSistema();
				ServiceLocator.getInstancia().getControladorMedidor();

			}

		}

	}

	/**
	 * Consultar integracao titulo pagar.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	public Collection<IntegracaoTituloPagar> consultarIntegracaoTituloPagar(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoTituloPagar());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			ServiceLocator.getInstancia().getControladorParametroSistema();
			ServiceLocator.getInstancia().getControladorMedidor();

			Long fatura = (Long) filtro.get("fatura");
			if ((fatura != null) && (fatura > 0)) {
				criteria.add(Restrictions.eq("fatura", fatura));
			}

			String sistemaDestino = (String) filtro.get("sistemaDestino");
			if ((sistemaDestino != null) && (!"".equals(sistemaDestino.trim()))) {
				criteria.add(Restrictions.eq("sistemaDestino", sistemaDestino));
			}

		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#consultarIntegracaoTitulo(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<IntegracaoTitulo> consultarIntegracaoTitulo(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoTitulo());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String chavesSistemasIntegrantes = (String) filtro.get("chavesSistemasIntegrantes");
			if (chavesSistemasIntegrantes != null && !chavesSistemasIntegrantes.isEmpty()) {
				criteria.add(Restrictions.eq("sistemaDestino", chavesSistemasIntegrantes));
			}

			Object[] chavesSistemasIntegrantesOrigem = (Object[]) filtro.get("chavesSistemasIntegrantesOrigem");
			if (chavesSistemasIntegrantesOrigem != null && chavesSistemasIntegrantesOrigem.length > 0) {
				criteria.add(Restrictions.in("sistemaOrigem", chavesSistemasIntegrantesOrigem));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

			String naoProcessado = (String) filtro.get("naoProcessado");
			if (naoProcessado != null && !StringUtils.isEmpty(naoProcessado)) {
				criteria.add(Restrictions.eq("situacao", naoProcessado));
			}

			String processado = (String) filtro.get("processado");
			if (processado != null && !StringUtils.isEmpty(processado)) {
				criteria.add(Restrictions.eq("situacao", processado));
			}

		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarAtualizacaoTitulo(br.com.ggas.integracao.titulos.impl.IntegracaoTituloImpl,
	 * java.util.Map)
	 */
	@Override
	public boolean validarAtualizacaoTitulo(IntegracaoTituloImpl integracaoTitulo, Map<String, Object[]> mapaInconsistenciasTitulo)
					throws GGASException {

		ControladorFatura ctlFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		String mensagemErro = "";
		Boolean possuiErro = this.validarIntegracao(integracaoTitulo, IntegracaoTituloImpl.class, mapaInconsistenciasTitulo);

		if (!possuiErro) {

			Fatura fatura = null;

			if (integracaoTitulo.getTituloPagar() != null || integracaoTitulo.getTituloReceber() != null) {

				Long idFatura = null;

				if (integracaoTitulo.getTituloPagar() != null) {
					idFatura = integracaoTitulo.getTituloPagar().getFatura();
				}

				if (integracaoTitulo.getTituloReceber() != null) {
					idFatura = integracaoTitulo.getTituloReceber().getFatura();
				}

				fatura = (Fatura) ctlFatura.obter(idFatura);

				if (fatura == null) {
					Util.atualizaMapaInconsistencias(
									mapaInconsistenciasTitulo,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS,
													"Fatura não encontrada"));
				}

				mensagemErro = Util.gerarMensagemErroConsolidada(mapaInconsistenciasTitulo, mensagens);

			}

			if (!mensagemErro.isEmpty()) {
				atualizarIntegracaoComErro(IntegracaoTituloImpl.class, integracaoTitulo, mensagemErro);
				return false;
			} else {

				this.processarIntegracaoBaixaTitulo(integracaoTitulo, fatura);

				ControladorConstanteSistema ctlConstanteSistema =
								(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				ConstanteSistema constanteSistemaSituacaoProcessado =
								ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

				integracaoTitulo.setSituacao(constanteSistemaSituacaoProcessado.getValor());
				super.atualizar(integracaoTitulo, IntegracaoTituloImpl.class);

			}

		} else {
			return false;
		}
		return true;
	}

	/**
	 * Processar integracao baixa titulo.
	 *
	 * @param integracaoTitulo
	 *            the integracao titulo
	 * @param fatura
	 *            the fatura
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void processarIntegracaoBaixaTitulo(IntegracaoTitulo integracaoTitulo, Fatura fatura) throws GGASException {

		ControladorDevolucao controladorDevolucao =
						(ControladorDevolucao) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorDevolucao.BEAN_ID_CONTROLADOR_DEVOLUCAO);

		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Devolucao devolucao = (Devolucao) controladorDevolucao.criar();

		devolucao.setCliente(fatura.getCliente());
		devolucao.setDataDevolucao(integracaoTitulo.getData());
		devolucao.setValorDevolucao(integracaoTitulo.getValor());
		devolucao.setPontoConsumo(fatura.getPontoConsumo());
		devolucao.setDadosAuditoria(integracaoTitulo.getDadosAuditoria());
		devolucao.setHabilitado(true);

		Long idTipoDevolucao = 97L;
		EntidadeConteudo tipoDevolucao = controladorEntidadeConteudo.obterEntidadeConteudo(idTipoDevolucao);
		devolucao.setTipoDevolucao(tipoDevolucao);

		devolucao.getListaNotaCredito().add(fatura);

		Collection<Recebimento> listaRecebimento = new HashSet<Recebimento>();
		devolucao.setListaRecebimento(listaRecebimento);

		controladorDevolucao.inserir(devolucao);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#gerarBaixaTitulo(br.com.ggas.arrecadacao.recebimento.Recebimento, boolean,
	 * java.math.BigDecimal)
	 */
	@Override
	public void gerarBaixaTitulo(Recebimento recebimento, boolean estorno, BigDecimal somaPagosParcialTmp) throws GGASException {

		ConstanteSistema constanteTitulo = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);
		ConstanteSistema baixaParcial = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_PARCIAL);
		BigDecimal somaPagosParcial = somaPagosParcialTmp;
		List<String> listaSistemasIntegrantes = this.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteTitulo.getValor()));

		if (listaSistemasIntegrantes != null) {
			for (String sistemaDestino : listaSistemasIntegrantes) {
				IntegracaoTitulo titulo = null;
				if (estorno) {
					ConstanteSistema operacaoExclusao =
									controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
					ConstanteSistema situacaoNaoProcessado =
									controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

					// Monta filtro para a consulta
					Map<String, Object> filtro = new HashMap<String, Object>();

					filtro.put("idRecebimento", recebimento.getChavePrimaria());

					// Monta a ordenação da consulta
					String ordenacao = null;
					List<IntegracaoTitulo> listaIntegracaoTitulo =
									(List<IntegracaoTitulo>) this.consultarTituloBaixa(filtro, ordenacao, "");
					if (listaIntegracaoTitulo != null) {
						titulo = Util.primeiroElemento(listaIntegracaoTitulo);
						titulo.setOperacao(operacaoExclusao.getValor());
						titulo.setSituacao(situacaoNaoProcessado.getValor());
						if (recebimento.getDadosAuditoria() != null) {
							titulo.setUsuario(String.valueOf(recebimento.getDadosAuditoria().getUsuario().getChavePrimaria()));
						}

						atualizar(titulo, getClasseEntidadeIntegracaoTituloBaixa());
					}
				} else {
					titulo = this.converteRecebimentoParaIntegracaoTitulo(recebimento, sistemaDestino, estorno);
					if (somaPagosParcial != null && titulo.getTipoBaixa().equals(baixaParcial.getValor())) {
						somaPagosParcial = somaPagosParcial.add(BigDecimal.ONE);
					}
					this.inserir(titulo);
				}
			}
		}

	}

	/**
	 * Atualizar integracao fatura parcelada.
	 *
	 * @param fatura
	 *            the fatura
	 */
	public void atualizarIntegracaoFaturaParcelada(Fatura fatura) {

		ConstanteSistema constanteTitulo = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);
		List<String> listaSistemasIntegrantes = this.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteTitulo.getValor()));
		if (listaSistemasIntegrantes != null && !listaSistemasIntegrantes.isEmpty()) {
			// Possivel Implementação Futura.
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#gerarBaixaTituloPorFatura(br.com.ggas.faturamento.Fatura,
	 * java.math.BigDecimal,
	 * boolean, br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void gerarBaixaTituloPorFatura(Fatura fatura, BigDecimal valorConciliado, boolean notaDebito, Fatura faturaConciliacao)
					throws NegocioException, ConcorrenciaException {

		ConstanteSistema constanteTitulo = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);

		List<String> listaSistemasIntegrantes = this.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteTitulo.getValor()));

		if (listaSistemasIntegrantes != null) {
			for (String sistemaDestino : listaSistemasIntegrantes) {
				IntegracaoTitulo titulo = null;
				titulo = this.converteFaturaParaIntegracaoTitulo(fatura, valorConciliado, notaDebito, sistemaDestino, faturaConciliacao);
				// Na conciliação de notas de débito e crédito não é necessário inserir uma ti_baixa referente a baixa parcial
				if (titulo.getTituloPagar() != null && titulo.getTituloReceber() != null) {
					this.inserir(titulo);
				}
			}
		}
	}

	/**
	 * Converte recebimento para integracao titulo.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param sistemaDestino
	 *            the sistema destino
	 * @param estorno
	 *            the estorno
	 * @return the integracao titulo
	 * @throws GGASException
	 */
	private IntegracaoTitulo converteRecebimentoParaIntegracaoTitulo(Recebimento recebimento, String sistemaDestino, boolean estorno)
					throws GGASException {

		ControladorArrecadacao controladorArrecadacao =
						(ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ServiceLocator service = ServiceLocator.getInstancia();
		ConstanteSistema sistemaOrigem = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema situacaoNaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema operacaoInclusao = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
		ConstanteSistema operacaoExclusao = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
		ConstanteSistema baixaParcial = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_PARCIAL);
		ConstanteSistema baixaTotal = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_TOTAL);
		ConstanteSistema quitado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		IntegracaoTituloImpl tituloBaixa =
						(IntegracaoTituloImpl) ServiceLocator.getInstancia().getBeanPorID(IntegracaoTitulo.BEAN_ID_INTEGRACAO_TITULO);

		Long situacaoPagamento = recebimento.getFaturaGeral().getFaturaAtual().getSituacaoPagamento().getChavePrimaria();
		if (situacaoPagamento.compareTo(Long.valueOf(quitado.getValor())) == 0) {
			tituloBaixa.setTipoBaixa(baixaTotal.getValor());
		} else {
			tituloBaixa.setTipoBaixa(baixaParcial.getValor());
		}
		tituloBaixa.setRecebimento(Util.converteLongParaInteger(recebimento.getChavePrimaria()));
		Long idContaBancaria = recebimento.getAvisoBancario().getContaBancaria().getChavePrimaria();
		ContaBancaria contaBancaria = controladorArrecadacao.obterContaBancaria(idContaBancaria);

		tituloBaixa.setBanco(contaBancaria.getAgencia().getBanco().getCodigoBanco());

		tituloBaixa.setNumeroConta(contaBancaria.getNumeroConta());
		String agencia = contaBancaria.getAgencia().getCodigo();
		tituloBaixa.setNumeroAgencia(agencia.substring(0, agencia.length() - 1));
		tituloBaixa.setNumeroAgenciaDigito(agencia.substring(agencia.length() - 1, agencia.length()));
		Long chavePrimaria = buscarTituloPorFatura(recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria(), true, sistemaDestino);
		if (chavePrimaria == null) {
			throw new GGASException(Constantes.ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE, true);
		}
		tituloBaixa.setTituloReceber((IntegracaoTituloReceber) service.getBeanPorIDeSeteChavePrimaria(
						IntegracaoTituloReceber.BEAN_ID_INTEGRACAO_TITULO_RECEBER, chavePrimaria));
		tituloBaixa.setNumeroContaDigito(contaBancaria.getNumeroDigito());
		tituloBaixa.setData(recebimento.getDataRecebimento());
		tituloBaixa.setValorAbatimento(recebimento.getValorAbatimento());
		tituloBaixa.setValorCredito(recebimento.getValorCredito());
		tituloBaixa.setValor(recebimento.getValorRecebimento());
		tituloBaixa.setValorDescontos(recebimento.getValorDescontos());
		tituloBaixa.setValorJurosMulta(recebimento.getValorJurosMulta());
		tituloBaixa.setValorPrincipal(recebimento.getValorPrincipal());

		// consulta na baixa de titulo quantas baixas já foram realizadas para seguir com o sequencial correto
		Collection<IntegracaoTitulo> listaIntegracaoTitulo = consultarTituloBaixaPagarReceber(0L, chavePrimaria);
		tituloBaixa.setSequencial(listaIntegracaoTitulo.size() + 1);

		if (estorno) {
			tituloBaixa.setOperacao(operacaoExclusao.getValor());
		} else {
			tituloBaixa.setOperacao(operacaoInclusao.getValor());
		}
		tituloBaixa.setSistemaDestino(sistemaDestino);
		tituloBaixa.setSistemaOrigem(sistemaOrigem.getValor());
		tituloBaixa.setSituacao(situacaoNaoProcessado.getValor());
		if (recebimento.getDadosAuditoria() != null) {
			tituloBaixa.setUsuario(String.valueOf(recebimento.getDadosAuditoria().getUsuario().getChavePrimaria()));
		}

		return tituloBaixa;
	}

	/**
	 * Converte fatura para integracao titulo.
	 *
	 * @param fatura
	 *            the fatura
	 * @param valorConciliado
	 *            the valor conciliado
	 * @param notaDebito
	 *            the nota debito
	 * @param sistemaDestino
	 *            the sistema destino
	 * @param faturaConciliacao
	 *            the fatura conciliacao
	 * @return the integracao titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private IntegracaoTitulo converteFaturaParaIntegracaoTitulo(Fatura fatura, BigDecimal valorConciliado, boolean notaDebito,
					String sistemaDestino, Fatura faturaConciliacao) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ServiceLocator service = ServiceLocator.getInstancia();

		ConstanteSistema sistemaOrigem = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema situacaoNaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema operacaoInclusao = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
		controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);

		ConstanteSistema baixaParcial = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_PARCIAL);
		ConstanteSistema baixaTotal = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_TOTAL);

		ConstanteSistema quitado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		IntegracaoTituloImpl tituloBaixa =
						(IntegracaoTituloImpl) ServiceLocator.getInstancia().getBeanPorID(IntegracaoTitulo.BEAN_ID_INTEGRACAO_TITULO);

		Long idNotaDebito =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));

		Long situacaoPagamento = fatura.getSituacaoPagamento().getChavePrimaria();
		if (situacaoPagamento.compareTo(Long.valueOf(quitado.getValor())) == 0) {
			tituloBaixa.setTipoBaixa(baixaTotal.getValor());
		} else {
			tituloBaixa.setTipoBaixa(baixaParcial.getValor());
		}

		Long chavePrimariaFaturaConciliacao = null;
		if (notaDebito) {
			Long chavePrimaria = buscarTituloPorFatura(fatura.getChavePrimaria(), true, sistemaDestino);
			if (chavePrimaria == null) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE, true);
			}
			tituloBaixa.setTituloReceber((IntegracaoTituloReceber) service.getBeanPorIDeSeteChavePrimaria(
							IntegracaoTituloReceber.BEAN_ID_INTEGRACAO_TITULO_RECEBER, chavePrimaria));
			if (faturaConciliacao.getTipoDocumento().getChavePrimaria() != idNotaDebito) {
				chavePrimariaFaturaConciliacao = buscarTituloPorFatura(faturaConciliacao.getChavePrimaria(), false, sistemaDestino);
				tituloBaixa.setTituloPagar((IntegracaoTituloPagar) service.getBeanPorIDeSeteChavePrimaria(
								IntegracaoTituloPagar.BEAN_ID_INTEGRACAO_TITULO_PAGAR, chavePrimariaFaturaConciliacao));
			}
		} else {
			Long chavePrimaria = buscarTituloPorFatura(fatura.getChavePrimaria(), false, sistemaDestino);
			if (chavePrimaria == null) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_TITULO_PAGAR_INEXISTENTE, true);
			}
			tituloBaixa.setTituloPagar((IntegracaoTituloPagar) service.getBeanPorIDeSeteChavePrimaria(
							IntegracaoTituloPagar.BEAN_ID_INTEGRACAO_TITULO_PAGAR,
							buscarTituloPorFatura(fatura.getChavePrimaria(), false, sistemaDestino)));
			if (faturaConciliacao.getTipoDocumento().getChavePrimaria() == idNotaDebito) {
				chavePrimariaFaturaConciliacao = buscarTituloPorFatura(faturaConciliacao.getChavePrimaria(), true, sistemaDestino);
				tituloBaixa.setTituloReceber((IntegracaoTituloReceber) service.getBeanPorIDeSeteChavePrimaria(
								IntegracaoTituloReceber.BEAN_ID_INTEGRACAO_TITULO_RECEBER, chavePrimariaFaturaConciliacao));
			}
		}
		tituloBaixa.setRecebimento(Util.converteLongParaInteger(fatura.getChavePrimaria()));
		tituloBaixa.setData(fatura.getDataEmissao());
		// RECEBE O VALOR CONCILIADO
		tituloBaixa.setValor(valorConciliado);
		tituloBaixa.setValorPrincipal(fatura.getValorTotal());
		tituloBaixa.setOperacao(operacaoInclusao.getValor());
		tituloBaixa.setSistemaDestino(sistemaDestino);
		tituloBaixa.setSistemaOrigem(sistemaOrigem.getValor());
		tituloBaixa.setSituacao(situacaoNaoProcessado.getValor());

		BigDecimal totalRecebimentosFatura = BigDecimal.ZERO;

		// ROTINA CRIADA PARA ADCIONAR O SEQUENCIA NA BAIXA DE TITULOS
		Map<String, Object> filtroFat = new HashMap<String, Object>();
		filtroFat.put(CHAVE_PRIMARIA, fatura.getChavePrimaria());
		Collection<Fatura> listaFatura = controladorFatura.consultarFatura(filtroFat);
		if (!listaFatura.isEmpty()) {
			Fatura fat = controladorFatura.consultarFatura(filtroFat).iterator().next();
			ControladorRecebimento controladorRecebimento =
							(ControladorRecebimento) ServiceLocator.getInstancia().getControladorNegocio(
											ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);
			Collection<Recebimento> listaRecebimento = controladorRecebimento.consultarRecebimento(fat.getFaturaGeral());
			for (Iterator<Recebimento> iterator = listaRecebimento.iterator(); iterator.hasNext();) {
				Recebimento rec = iterator.next();
				totalRecebimentosFatura = totalRecebimentosFatura.add(rec.getValorRecebimento());
			}
			if (!listaRecebimento.isEmpty()) {
				tituloBaixa.setValor(fatura.getValorTotal().subtract(totalRecebimentosFatura));
			}
			tituloBaixa.setSequencial(listaRecebimento.size() + 1);
		} else {
			tituloBaixa.setSequencial(1);
			// ROTINA CRIADA PARA ADCIONAR O SEQUENCIA NA BAIXA DE TITULOS
		}

		// busca na integracao de baixa de tiluto quantos titulos já foram registrado com os critérios pagar/receber para ajustar o
		// sequencial
		if (tituloBaixa.getTituloPagar() != null && tituloBaixa.getTituloReceber() != null) {
			Collection<IntegracaoTitulo> listaIntegracaoTitulo =
							consultarTituloBaixaPagarReceber(tituloBaixa.getTituloPagar().getChavePrimaria(), tituloBaixa
											.getTituloReceber().getChavePrimaria());

			tituloBaixa.setSequencial(listaIntegracaoTitulo.size() + 1);

			IntegracaoTituloReceber tiBaixaRec = this.obterIntegracaoTituloReceber(tituloBaixa.getTituloReceber().getChavePrimaria());
			IntegracaoTituloPagar tiBaixaPag = this.obterIntegracaoTituloPagar(tituloBaixa.getTituloPagar().getChavePrimaria());

			BigDecimal totalRecebimentos = BigDecimal.ZERO;
			BigDecimal totalPagamentos = BigDecimal.ZERO;

			// Verifica os valores dos titulos para checar se é baixa integral ou parcial
			if (!listaIntegracaoTitulo.isEmpty()) {

				Map<Long, Object> valoresReceber = new HashMap<Long, Object>();
				Map<Long, Object> valoresPagar = new HashMap<Long, Object>();

				for (Iterator<IntegracaoTitulo> iterator = listaIntegracaoTitulo.iterator(); iterator.hasNext();) {

					IntegracaoTitulo integracaoTitulo = iterator.next();

					if (integracaoTitulo.getTituloReceber() != null) {
						IntegracaoTituloReceber tiRec =
										this.obterIntegracaoTituloReceber(integracaoTitulo.getTituloReceber().getChavePrimaria());
						if (!valoresReceber.containsKey(tiRec.getChavePrimaria())) {
							valoresReceber.put(tiRec.getChavePrimaria(), tiRec.getValorTotal());
						}
					}

					if (integracaoTitulo.getTituloPagar() != null) {
						IntegracaoTituloPagar tiPag = this.obterIntegracaoTituloPagar(integracaoTitulo.getTituloPagar().getChavePrimaria());

						if (!valoresPagar.containsKey(tiPag.getChavePrimaria())) {
							valoresPagar.put(tiPag.getChavePrimaria(), tiPag.getValorTotal());
						}
					}
				}

				if (!valoresReceber.containsKey(tiBaixaRec.getChavePrimaria())) {
					valoresReceber.put(tiBaixaRec.getChavePrimaria(), tiBaixaRec.getValorTotal());
				}
				if (!valoresPagar.containsKey(tiBaixaPag.getChavePrimaria())) {
					valoresPagar.put(tiBaixaPag.getChavePrimaria(), tiBaixaPag.getValorTotal());
				}

				for (Map.Entry<Long, Object> valor : valoresReceber.entrySet()) {
					totalRecebimentos = totalRecebimentos.add((BigDecimal) valor.getValue());
				}

				for (Map.Entry<Long, Object> valor : valoresPagar.entrySet()) {
					totalPagamentos = totalPagamentos.add((BigDecimal) valor.getValue());
				}

				totalPagamentos = totalPagamentos.add(totalRecebimentosFatura);

				if (totalRecebimentos.compareTo(totalPagamentos) <= 0) {
					tituloBaixa.setTipoBaixa(baixaTotal.getValor());
				} else {
					tituloBaixa.setTipoBaixa(baixaParcial.getValor());
				}

			} else if (tituloBaixa.getTituloReceber() != null && tituloBaixa.getTituloPagar() != null) {

				tiBaixaPag.setValorTotal(tiBaixaPag.getValorTotal().add(totalRecebimentosFatura));

				if (tiBaixaRec.getValorTotal().compareTo(tiBaixaPag.getValorTotal()) <= 0) {
					tituloBaixa.setTipoBaixa(baixaTotal.getValor());
				} else {
					tituloBaixa.setTipoBaixa(baixaParcial.getValor());
				}
			}

		}

		if (fatura.getDadosAuditoria() != null) {
			tituloBaixa.setUsuario(String.valueOf(fatura.getDadosAuditoria().getUsuario().getChavePrimaria()));
		}

		return tituloBaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#buscarTituloPorFatura(java.lang.Long, boolean, java.lang.String)
	 */
	@Override
	public Long buscarTituloPorFatura(Long chaveFatura, boolean tituloReceber, String sistemaIntegrante) throws NegocioException {

		ConstanteSistema situacaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select chavePrimaria ");
		hql.append(" from ");
		if (tituloReceber) {
			hql.append(getClasseEntidadeIntegracaoTituloReceber().getSimpleName());
		} else {
			hql.append(getClasseEntidadeIntegracaoTituloPagar().getSimpleName());
		}

		hql.append(" titulo");
		hql.append(" where titulo.fatura = :chaveFaturaGeral ");
		hql.append(" and titulo.sistemaDestino = :sistemaIntegrante");
		hql.append(" and titulo.situacao = :situacao");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveFaturaGeral", chaveFatura);
		query.setString("sistemaIntegrante", sistemaIntegrante);
		query.setString("situacao", situacaoProcessado.getValor());

		return (Long) query.setMaxResults(1).uniqueResult();
	}

	@Override
	public boolean integracaoPrecedenteContratoAtiva() {
		boolean retorno = false;
		try {
			ParametroSistema referenciaIntegracaoTituloNota
					= controladorParamento
					.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);
			ConstanteSistema referenciaIntegracaoContrato
					= controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

			if ((referenciaIntegracaoTituloNota != null && referenciaIntegracaoContrato != null)
					&& referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
				retorno = true;
			}
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}
		return retorno;
	}

	@Override
	public boolean existeIntegracaoContratoProcessada() {
		return existeIntegracaoProcessada(getClasseEntidadeIntegracaoContrato().getSimpleName());
	}

	@Override
	public boolean existeIntegracaoClienteProcessada() {
		return existeIntegracaoProcessada(getClasseEntidadeIntegracaoCliente().getSimpleName());
	}

	/**
	 * Verifica se existe registro processado para uma determinada classe de integração
	 * @param nomeEntidade O nome da entidade de integração
	 * @return True se houver registro integrado, false caso contrário
	 */
	@SuppressWarnings("squid:S1192")
	private boolean existeIntegracaoProcessada(String nomeEntidade) {
		ConstanteSistema situacaoProcessado =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(nomeEntidade);
		hql.append(" where situacao = :situacao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("situacao", situacaoProcessado.getValor());

		return !query.list().isEmpty();
	}

	/**
	 * Inserir integracao titulo receber.
	 *
	 * @param fatura - {@link Fatura}
	 * @param listaIntegracaoSistemaTituloReceber - {@link List}
	 * @param documentosPorFaturaGeral - {@link Map}
	 * @param mapeamentosPorIntegrantes - {@link MultiKeyMap}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public void inserirIntegracaoTituloReceber(Fatura fatura, List<String> listaIntegracaoSistemaTituloReceber,
					Map<Long, Collection<DocumentoCobrancaItem>> documentosPorFaturaGeral, MultiKeyMap mapeamentosPorIntegrantes)
					throws NegocioException {

		ControladorConstanteSistema ctlConstanteSistema =
				(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorFatura ctlFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ConstanteSistema constanteSistemaSituacaoNaoProcessado =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		ConstanteSistema situacaoProcessado =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

		ConstanteSistema constanteSistemaIntegracaoSistemaGGAS =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);

		ConstanteSistema constanteSistemaOperacaoInclusao =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

		ParametroSistema referenciaIntegracaoTituloNota =
				controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

		ConstanteSistema referenciaIntegracaoContrato =
				ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

		for (String sistemaIntegrante : listaIntegracaoSistemaTituloReceber) {

			IntegracaoSistema integracaoSistema = (IntegracaoSistema) criarIntegracaoSistema();
			integracaoSistema.setSigla(sistemaIntegrante);

			IntegracaoTituloReceber integracaoTituloReceber = (IntegracaoTituloReceber) ServiceLocator.getInstancia()
					.getBeanPorID(IntegracaoTituloReceber.BEAN_ID_INTEGRACAO_TITULO_RECEBER);

			if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {

				IntegracaoSistemaFuncao integracaoContratoAtivo = this.obterIntegracaoSistemaFuncao(Constantes.C_INTEGRACAO_CONTRATO);
				PontoConsumo pontoConsumo = fatura.getPontoConsumo();
				if (pontoConsumo == null) {
					Collection<Fatura> listaFaturasFilhas = ctlFatura.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
					pontoConsumo = listaFaturasFilhas.iterator().next().getPontoConsumo();
				}
				IntegracaoContrato contratoIntegrado = this.obterIntegracaoContrato(fatura.getContrato(), pontoConsumo);
				if (integracaoContratoAtivo != null && (contratoIntegrado == null ||
						!situacaoProcessado.getValor().equals(contratoIntegrado.getSituacao()))) {
					throw new NegocioException(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE, true);
				}
				integracaoTituloReceber.setContrato(contratoIntegrado);
			} else {
				IntegracaoSistemaFuncao integracaoCadastroClienteAtivo = this.obterIntegracaoSistemaFuncao(Constantes.C_INTEGRACAO_CADASTRO_CLIENTE);
				IntegracaoCliente clienteIntegrado = this.buscarIntegracaoCliente(fatura.getCliente());
				if (integracaoCadastroClienteAtivo != null && clienteIntegrado == null) {
					throw new NegocioException(Constantes.ERRO_CLIENTE_INTEGRACAO_INEXISTENTE, true);
				}
				integracaoTituloReceber.setCliente(clienteIntegrado);
			}

			integracaoTituloReceber.setFatura(fatura.getChavePrimaria());
			integracaoTituloReceber.setTipoDocumento(this.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes,
					fatura.getTipoDocumento(), integracaoSistema));
			integracaoTituloReceber.setSituacaoTitulo(this.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes,
					fatura.getCreditoDebitoSituacao(), integracaoSistema));

			if (fatura.getSegmento() != null) {
				integracaoTituloReceber.setSegmento(this.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes,
						fatura.getSegmento(), integracaoSistema));
			}
			integracaoTituloReceber.setDataEmissao(fatura.getDataEmissao());
			integracaoTituloReceber.setDataVencimento(fatura.getDataVencimento());
			integracaoTituloReceber.setAnoMesCicloReferencia(fatura.getCicloReferenciaFormatado());
			integracaoTituloReceber.setValorTotal(fatura.getValorTotal());
			integracaoTituloReceber.setObservacaoNota(fatura.getObservacaoNota());

			Contrato contratoFatura = fatura.getContratoAtual();
			if (contratoFatura.getChavePrimariaPai() == null) {
				contratoFatura = (Contrato) controladorContrato.obter(fatura.getContrato().getChavePrimaria());
			} else {
				contratoFatura = controladorContrato.consultarContratoAtivoPorContratoPai(fatura.getContratoAtual().getChavePrimariaPai());
			}
			if (contratoFatura.getArrecadadorContratoConvenio() != null) {
				integracaoTituloReceber.setConvenioArrecadador(contratoFatura.getArrecadadorContratoConvenio().getCodigoConvenio());
			}
			if (contratoFatura.getBanco() != null) {
				integracaoTituloReceber.setBancoCliente(contratoFatura.getBanco().getCodigoBanco());
			}
			integracaoTituloReceber.setAgenciaCliente(contratoFatura.getAgencia());
			integracaoTituloReceber.setContaCliente(contratoFatura.getContaCorrente());

			ControladorDocumentoCobranca controladorDocumentoCobranca = (ControladorDocumentoCobranca ) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);
			Collection<DocumentoCobrancaItem> listaDocumentoCobrancaItem
					= controladorDocumentoCobranca.consultarDocumentoCobrancaItemPelaFatura(fatura.getChavePrimaria());
			Long nossoNumero = null;
			if (!Util.isNullOrEmpty(listaDocumentoCobrancaItem)) {
				BigDecimal totalCredito = BigDecimal.ZERO;
				BigDecimal totalDebito = BigDecimal.ZERO;
				for (DocumentoCobrancaItem documentoCobrancaItem : listaDocumentoCobrancaItem) {
					if (documentoCobrancaItem.getValorDescontos() != null) {
						totalCredito = totalCredito.add(documentoCobrancaItem.getValorDescontos());
					}
					if (documentoCobrancaItem.getValorAcrecimos() != null) {
						totalDebito = totalDebito.add(documentoCobrancaItem.getValorAcrecimos());
					}
				}
				integracaoTituloReceber.setValorDescontos(totalCredito);
				integracaoTituloReceber.setValorAcrescimos(totalDebito);
				DocumentoCobrancaItem documentoCobrancaItem = listaDocumentoCobrancaItem.iterator().next();
				nossoNumero = documentoCobrancaItem.getDocumentoCobranca().getNossoNumero();
			}

			integracaoTituloReceber.setNossoNumero(nossoNumero);
			integracaoTituloReceber.setSistemaDestino(sistemaIntegrante);
			integracaoTituloReceber.setSistemaOrigem(constanteSistemaIntegracaoSistemaGGAS.getValor());
			integracaoTituloReceber.setOperacao(constanteSistemaOperacaoInclusao.getValor());
			integracaoTituloReceber.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
			integracaoTituloReceber.setUsuario(String.valueOf(fatura.getDadosAuditoria().getUsuario().getChavePrimaria()));
			integracaoTituloReceber.setDadosAuditoria(fatura.getDadosAuditoria());

			super.inserir(integracaoTituloReceber);

			Collection<FaturaTributacao> listaFaturaTributacao = ctlFatura.consultarFaturaTributacaoPorFatura(fatura.getChavePrimaria());

			for (FaturaTributacao faturaTributacao : listaFaturaTributacao) {

				IntegracaoTituloReceberTributo integracaoTituloReceberTributo =
						(IntegracaoTituloReceberTributo) ServiceLocator.getInstancia().getBeanPorID(
								IntegracaoTituloReceberTributo.BEAN_ID_INTEGRACAO_TITULO_RECEBER_TRIBUTO);

				integracaoTituloReceberTributo.setTituloReceber(integracaoTituloReceber);
				integracaoTituloReceberTributo.setAliquota(faturaTributacao.getPercentualAliquota());

				integracaoTituloReceberTributo.setTributo(this.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes,
						faturaTributacao.getTributo(), integracaoSistema));
				integracaoTituloReceberTributo.setBaseCalculo(faturaTributacao.getValorBaseCalculo());
				integracaoTituloReceberTributo.setValorImposto(faturaTributacao.getValorImposto());
				integracaoTituloReceberTributo.setBaseCalculoSubstituto(faturaTributacao.getValorBaseSubstituicao());
				integracaoTituloReceberTributo.setValorSubstituto(faturaTributacao.getValorSubstituicao());
				integracaoTituloReceberTributo.setOperacao(constanteSistemaOperacaoInclusao.getValor());
				integracaoTituloReceberTributo.setSituacao(constanteSistemaSituacaoNaoProcessado.getValor());
				integracaoTituloReceberTributo.setUsuario(String.valueOf(fatura.getDadosAuditoria().getUsuario().getChavePrimaria()));

				super.inserir(integracaoTituloReceberTributo);

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#buscarIntegracaoCliente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public IntegracaoCliente buscarIntegracaoCliente(Cliente cliente) {

		Query query = null;
		ConstanteSistema situacaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoCliente().getSimpleName());
		hql.append(" cliente ");
		hql.append(" where cliente.clienteOrigem = :idCliente ");
		hql.append(" and cliente.situacao = :situacao ");
		hql.append(" and cliente.sistemaOrigem = 'GAS' ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCliente", cliente.getChavePrimaria());
		query.setString("situacao", situacaoProcessado.getValor());

		return (IntegracaoCliente) query.uniqueResult();

	}

	/**
	 * Consultar titulo baixa.
	 *
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<IntegracaoTitulo> consultarTituloBaixa(Map<String, Object> filtro, String ordenacao, String... propriedadesLazy)
					throws NegocioException {

		Query query = null;

		Long idRecebimento = null;
		String sistemaOrigem = null;
		String sistemaDestino = null;
		String naoProcessado = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoTituloBaixa().getSimpleName());
		hql.append(" titb ");
		hql.append(" where 1=1 ");
		if (filtro != null && !filtro.isEmpty()) {
			idRecebimento = (Long) filtro.get("idRecebimento");

			if (idRecebimento != null) {
				hql.append(" and titb.recebimento = :idRecebimento ");
			}

			sistemaOrigem = (String) filtro.get("sistemaOrigem");
			if (sistemaOrigem != null && !StringUtils.isEmpty(sistemaOrigem)) {

				hql.append(" and upper(titb.sistemaOrigem) = upper(:sistemaOrigem) ");
			}

			sistemaDestino = (String) filtro.get("sistemaDestino");
			if (sistemaDestino != null && !StringUtils.isEmpty(sistemaDestino)) {

				hql.append(" and upper(titb.sistemaDestino) = upper(:sistemaDestino) ");
			}

			naoProcessado = (String) filtro.get("naoProcessado");
			if (naoProcessado != null && !StringUtils.isEmpty(naoProcessado)) {

				hql.append(" and upper(titb.situacao) = upper(:naoProcessado) ");
			}
		}

		if (ordenacao != null) {
			hql.append(" order by  ").append(ordenacao);
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null && idRecebimento != null) {
			query.setLong("idRecebimento", idRecebimento);
		}

		if (sistemaOrigem != null) {
			query.setString("sistemaOrigem", sistemaOrigem);
		}
		if (sistemaDestino != null) {
			query.setString("sistemaDestino", sistemaDestino);
		}
		if (naoProcessado != null) {
			query.setString("naoProcessado", naoProcessado);
		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > 0) {

			lista = super.inicializarLazyColecao(lista, propriedadesLazy);

		}

		return (Collection<IntegracaoTitulo>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao#cancelarTituloReceber(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void cancelarTituloReceber(Fatura fatura) throws NegocioException, ConcorrenciaException {

		ConstanteSistema situacaoNaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema situacaoAguardandoRetorno =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO);
		controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
		ConstanteSistema constanteSistemaIntegracaoTituloReceber =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);
		ConstanteSistema operacaoCancelamento =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_CANCELAMENTO);
		ConstanteSistema operacaoInclusao = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
		List<String> listaIntegracaoSistemaFuncao =
						this.gerarListaSistemasIntegrantesPorFuncao(Long.parseLong(constanteSistemaIntegracaoTituloReceber.getValor()));
		for (String sistemaIntegrante : listaIntegracaoSistemaFuncao) {
			Long chavePrimaria = this.buscarTituloPorFatura(fatura.getChavePrimaria(), true, sistemaIntegrante);
			if (chavePrimaria == null) {
				throw new NegocioException(Constantes.ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE, true);
			}
			IntegracaoTituloReceber integracaoTituloReceber =
							(IntegracaoTituloReceber) this.obter(chavePrimaria, IntegracaoTituloReceberImpl.class);
			if (!(integracaoTituloReceber.getOperacao().equals(operacaoInclusao.getValor()) && !integracaoTituloReceber.getSituacao()
							.equals(situacaoAguardandoRetorno.getValor()))) {
				throw new NegocioException(ERRO_PROCESSO_INTEGRACAO_TITULO_AGUARDANDO_RETORNO, true);
			} else {
				integracaoTituloReceber.setSituacaoTitulo(buscarCodigoSistemaIntegranteNumerico(fatura.getCreditoDebitoSituacao()
								.getChavePrimaria(), fatura.getCreditoDebitoSituacao().getClass().getInterfaces()[0].getName(),
								sistemaIntegrante));
				if (fatura.getCreditoDebitoSituacao().getDescricao().equalsIgnoreCase(PARCELADA)) {
					integracaoTituloReceber.setMotivoCancelamento(controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_FATURA_CANCELA_POR_PARCELAMENTO));
					integracaoTituloReceber.setDataCancelamento(new Date());
					integracaoTituloReceber.setObservacaoNota(fatura.getObservacaoNota());
					fatura.setObservacaoNota("");
				} else {
					integracaoTituloReceber.setMotivoCancelamento(buscarCodigoSistemaIntegrante(fatura.getMotivoCancelamento()
									.getChavePrimaria(), fatura.getMotivoCancelamento().getClass().getInterfaces()[0].getName(),
									sistemaIntegrante));
					integracaoTituloReceber.setDataCancelamento(fatura.getDataCancelamento());
				}
				integracaoTituloReceber.setOperacao(operacaoCancelamento.getValor());
				integracaoTituloReceber.setSituacao(situacaoNaoProcessado.getValor());
				integracaoTituloReceber.setUsuario(String.valueOf(fatura.getDadosAuditoria().getUsuario().getChavePrimaria()));
				this.atualizar(integracaoTituloReceber, IntegracaoTituloReceberImpl.class);

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.integracao.geral.ControladorIntegracao#validarProcessamentoLancamento(br.com.ggas.contabil.LancamentoContabilSintetico,
	 * java.util.Map)
	 */
	@Override
	public boolean validarProcessamentoLancamento(LancamentoContabilSintetico lancamentoContabilSintetico,
					Map<String, Object[]> mapaInconsistenciasLancamento) throws NegocioException, ConcorrenciaException {

		ControladorContabil controladorContabil = ServiceLocator.getInstancia().getControladorContabil();

		String mensagemErro = "";
		Boolean possuiErro =
						this.validarIntegracao(lancamentoContabilSintetico, IntegracaoLancamentoContabilImpl.class,
										mapaInconsistenciasLancamento);

		if (!possuiErro) {

			if (lancamentoContabilSintetico.getValor() == null) {
				Util.atualizaMapaInconsistencias(
								mapaInconsistenciasLancamento,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_VALOR,
												Constantes.LANCAMENTO_CONTABIL_VALOR));
			}

			if (lancamentoContabilSintetico.getContaContabilCredito() != null) {
				if (lancamentoContabilSintetico.getContaContabilCredito().getNomeConta() == null
								|| lancamentoContabilSintetico.getContaContabilCredito().getNumeroConta() == null) {
					Util.atualizaMapaInconsistencias(
									mapaInconsistenciasLancamento,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_CREDITO,
													Constantes.LANCAMENTO_CONTABIL_NOME_CONTA_CREDITO));
				}
			} else {
				Util.atualizaMapaInconsistencias(
								mapaInconsistenciasLancamento,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_CREDITO,
												Constantes.LANCAMENTO_CONTABIL_CONTA_CREDITO));
			}

			if (lancamentoContabilSintetico.getContaContabilDebito() != null) {
				if (lancamentoContabilSintetico.getContaContabilDebito().getNomeConta() == null
								|| lancamentoContabilSintetico.getContaContabilDebito().getNumeroConta() == null) {
					Util.atualizaMapaInconsistencias(
									mapaInconsistenciasLancamento,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_DEBITO,
													Constantes.LANCAMENTO_CONTABIL_NOME_CONTA_DEBITO));
				}
			} else {
				Util.atualizaMapaInconsistencias(
								mapaInconsistenciasLancamento,
								concatenaMensagemErro(Constantes.ERRO_NEGOCIO_INTEGRACAO_LANCAMENTO_CONTABIL_CONTA_DEBITO,
												Constantes.LANCAMENTO_CONTABIL_CONTA_DEBITO));
			}

			mensagemErro = Util.gerarMensagemErroConsolidada(mapaInconsistenciasLancamento, mensagens);

			if (!mensagemErro.isEmpty()) {
				return false;
			} else {

				ConstanteSistema constanteLancamentosContabeis =
								controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_LANCAMENTOS_CONTABEIS);

				List<String> listaSistemasIntegrantes =
								this.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteLancamentosContabeis.getValor()));

				for (String sistemaIntegrante : listaSistemasIntegrantes) {

					IntegracaoLancamentoContabil integracaoLancamentoContabilCredito =
									(IntegracaoLancamentoContabil) ServiceLocator.getInstancia().getBeanPorID(
													IntegracaoLancamentoContabil.BEAN_ID_INTEGRACAO_LANCAMENTO_CONTABIL);

					String tipoLancamento = "C";
					integracaoLancamentoContabilCredito =
									this.popularIntegracaoLancamento(integracaoLancamentoContabilCredito, lancamentoContabilSintetico,
													tipoLancamento, sistemaIntegrante);

					super.inserir(integracaoLancamentoContabilCredito);

					IntegracaoLancamentoContabil integracaoLancamentoContabilDebito =
									(IntegracaoLancamentoContabil) ServiceLocator.getInstancia().getBeanPorID(
													IntegracaoLancamentoContabil.BEAN_ID_INTEGRACAO_LANCAMENTO_CONTABIL);

					tipoLancamento = "D";
					integracaoLancamentoContabilDebito =
									this.popularIntegracaoLancamento(integracaoLancamentoContabilDebito, lancamentoContabilSintetico,
													tipoLancamento, sistemaIntegrante);
					super.inserir(integracaoLancamentoContabilDebito);

					lancamentoContabilSintetico.setIndicadorIntegrado(Boolean.TRUE);
					controladorContabil.atualizar(lancamentoContabilSintetico);

				}

			}

		}

		return true;

	}

	/**
	 * Popular integracao lancamento.
	 *
	 * @param integracaoLancamentoContabil
	 *            the integracao lancamento contabil
	 * @param lancamentoContabilSintetico
	 *            the lancamento contabil sintetico
	 * @param tipoLancamento
	 *            the tipo lancamento
	 * @param sistemaIntegrante
	 *            the sistema integrante
	 * @return the integracao lancamento contabil
	 */
	private IntegracaoLancamentoContabil popularIntegracaoLancamento(IntegracaoLancamentoContabil integracaoLancamentoContabil,
					LancamentoContabilSintetico lancamentoContabilSintetico, String tipoLancamento, String sistemaIntegrante) {

		ConstanteSistema constanteIntegracaoSistemaGGAS =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);

		ServiceLocator.getInstancia().getControladorMedidor();
		ServiceLocator.getInstancia().getControladorVazaoCorretor();
		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ConstanteSistema constanteInclusao = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

		ConstanteSistema constanteNaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

		String mascara = "";
		try {
			mascara =
							(String) ctlParametroSistema
											.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MODULO_CONTABILIDADE_MASCARA_NUMERO_CONTA);
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		if (lancamentoContabilSintetico.getEventoComercial() != null) {
			integracaoLancamentoContabil.setEventoComercial(buscarCodigoSistemaIntegranteNumerico(lancamentoContabilSintetico
							.getEventoComercial().getChavePrimaria(), lancamentoContabilSintetico.getEventoComercial().getClass()
							.getInterfaces()[0].getName(), sistemaIntegrante));
		}

		if (lancamentoContabilSintetico.getLancamentoItemContabil() != null) {
			integracaoLancamentoContabil.setLancamentoItemContabil(buscarCodigoSistemaIntegranteNumerico(lancamentoContabilSintetico
							.getLancamentoItemContabil().getChavePrimaria(), lancamentoContabilSintetico.getLancamentoItemContabil()
							.getClass().getInterfaces()[0].getName(), sistemaIntegrante));
		}

		if (lancamentoContabilSintetico.getSegmento() != null) {
			integracaoLancamentoContabil.setSegmento(buscarCodigoSistemaIntegranteNumerico(lancamentoContabilSintetico.getSegmento()
							.getChavePrimaria(), lancamentoContabilSintetico.getSegmento().getClass().getInterfaces()[0].getName(),
							sistemaIntegrante));
		}

		if (lancamentoContabilSintetico.getTributo() != null) {
			integracaoLancamentoContabil.setTributo(buscarCodigoSistemaIntegranteNumerico(lancamentoContabilSintetico.getTributo()
							.getChavePrimaria(), lancamentoContabilSintetico.getTributo().getClass().getInterfaces()[0].getName(),
							sistemaIntegrante));
		}

		integracaoLancamentoContabil.setOperacao(constanteInclusao.getValor());
		integracaoLancamentoContabil.setSistemaDestino(sistemaIntegrante);
		integracaoLancamentoContabil.setSistemaOrigem(constanteIntegracaoSistemaGGAS.getValor());
		integracaoLancamentoContabil.setSituacao(constanteNaoProcessado.getValor());
		if (lancamentoContabilSintetico.getDadosAuditoria() != null) {
			integracaoLancamentoContabil.setUsuario(String.valueOf(lancamentoContabilSintetico.getDadosAuditoria().getUsuario()
							.getChavePrimaria()));
		}

		integracaoLancamentoContabil.setHistorico(lancamentoContabilSintetico.getHistorico());
		integracaoLancamentoContabil.setValor(lancamentoContabilSintetico.getValor());
		integracaoLancamentoContabil.setLancamentoContabilSintetico(lancamentoContabilSintetico.getChavePrimaria());
		integracaoLancamentoContabil.setDataContabil(lancamentoContabilSintetico.getDataContabil());

		integracaoLancamentoContabil.setTipoLancamento(tipoLancamento);

		if ("C".equalsIgnoreCase(tipoLancamento)) {
			integracaoLancamentoContabil.setContaAuxiliar(lancamentoContabilSintetico.getContaAuxiliarCredito());
			if (lancamentoContabilSintetico.getContaContabilCredito() != null) {
				integracaoLancamentoContabil.setNumeroConta(Util.formatarMascara(mascara, lancamentoContabilSintetico
								.getContaContabilCredito().getNumeroConta()));
				integracaoLancamentoContabil.setDescricaoConta(lancamentoContabilSintetico.getContaContabilCredito().getNomeConta());
			}
		} else if ("D".equalsIgnoreCase(tipoLancamento)) {
			integracaoLancamentoContabil.setContaAuxiliar(lancamentoContabilSintetico.getContaAuxiliarDebito());
			if (lancamentoContabilSintetico.getContaContabilDebito() != null) {
				integracaoLancamentoContabil.setNumeroConta(Util.formatarMascara(mascara, lancamentoContabilSintetico
								.getContaContabilDebito().getNumeroConta()));
				integracaoLancamentoContabil.setDescricaoConta(lancamentoContabilSintetico.getContaContabilDebito().getNomeConta());
			}
		}

		return integracaoLancamentoContabil;
	}

	/**
	 *
	 *
	 * @throws ConcorrenciaException
	 */
	@Override
	public boolean validarAtualizacaoCliente(IntegracaoClienteImpl integracaoCliente, Map<String, Object[]> mapaInconsistenciasCliente)
					throws GGASException {

		ControladorCliente controladorCliente =
						(ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		String mensagemErro = "";
		Boolean possuiErro = this.validarIntegracao(integracaoCliente, IntegracaoClienteImpl.class, mapaInconsistenciasCliente);

		if (!possuiErro) {

			Cliente cliente = null;

			if (integracaoCliente != null) {

				try {
					cliente = (Cliente) controladorCliente.obter(integracaoCliente.getClienteDestino(), "contatos");
				} catch (NegocioException e) {
					LOG.error(e.getMessage(), e);
				}

				if (cliente == null) {
					Util.atualizaMapaInconsistencias(
									mapaInconsistenciasCliente,
									concatenaMensagemErro(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_NAO_PREENCHIDOS,
													"Cliente não encontrado"));
				}

				mensagemErro = Util.gerarMensagemErroConsolidada(mapaInconsistenciasCliente, mensagens);

			}

			if (integracaoCliente != null) {
				if (!mensagemErro.isEmpty()) {
					atualizarIntegracaoComErro(IntegracaoClienteImpl.class, integracaoCliente, mensagemErro);
					return false;
				} else {

					cliente.setContaAuxiliar(integracaoCliente.getContaAuxiliar());
					controladorCliente.atualizar(cliente, ClienteImpl.class);

					ControladorConstanteSistema ctlConstanteSistema =
									(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
													ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

					ConstanteSistema constanteSistemaSituacaoProcessado =
									ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

					integracaoCliente.setSituacao(constanteSistemaSituacaoProcessado.getValor());
					super.atualizar(integracaoCliente, IntegracaoClienteImpl.class);

				}
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Consultar titulo baixa pagar receber.
	 *
	 * @param idTituloPagar
	 *            the id titulo pagar
	 * @param idTituloReceber
	 *            the id titulo receber
	 * @return the collection
	 */
	private Collection<IntegracaoTitulo> consultarTituloBaixaPagarReceber(long idTituloPagar, long idTituloReceber) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoTituloBaixa().getSimpleName());
		hql.append(" titb ");
		hql.append(" where 1=1 ");

		if (idTituloPagar > 0 && idTituloReceber > 0) {
			hql.append(" and (titb.tituloPagar.chavePrimaria = :idTituloPagar ");
			hql.append(" or titb.tituloReceber.chavePrimaria = :idTituloReceber) ");
		} else {
			if (idTituloPagar > 0) {
				hql.append(" and titb.tituloPagar.chavePrimaria = :idTituloPagar ");
			}
			if (idTituloReceber > 0) {
				hql.append(" and titb.tituloReceber.chavePrimaria = :idTituloReceber ");
			}
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idTituloPagar > 0) {
			query.setLong("idTituloPagar", idTituloPagar);
		}

		if (idTituloReceber > 0) {
			query.setLong("idTituloReceber", idTituloReceber);
		}

		Collection<Object> lista = query.list();

		return (Collection<IntegracaoTitulo>) (Collection<?>) lista;
	}

	/**
	 * Obter integracao titulo pagar.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the integracao titulo pagar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public IntegracaoTituloPagar obterIntegracaoTituloPagar(Long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoTituloPagar().getSimpleName());

		hql.append(" titulo");
		hql.append(" where titulo.chavePrimaria = :chavePrimaria ");

		Query query = null;
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA, chavePrimaria);

		return (IntegracaoTituloPagar) query.uniqueResult();
	}

	@Override
	public IntegracaoTituloReceber obterIntegracaoTituloReceber(Long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoTituloReceber().getSimpleName());

		hql.append(" titulo");
		hql.append(" where titulo.chavePrimaria = :chavePrimaria ");

		Query query = null;
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA, chavePrimaria);

		return (IntegracaoTituloReceber) query.uniqueResult();

	}

	/**
	 * Método que faz a integracao de todos os clientes
	 * @throws Exception 
	 */
	@Override
	public void reintegrarClientes(StringBuilder logProcessamento) throws Exception {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		List<Cliente> clientes = (List<Cliente>) controladorCliente.listarClientesNaoIntegrados();

		AsyncExecutor asyncExecutor = ServiceLocator.getInstancia().getBean(AsyncExecutor.class);
		
		while(!clientes.isEmpty()) {
			List<CheckedSupplier<Boolean, GGASException>> functionsList = new ArrayList<>();

			for (Cliente cliente : clientes) {
				functionsList.add(() -> ServiceLocator.getInstancia().getControladorIntegracao().reintegrarCliente(cliente));
			}
			
			AsyncExecutor.execute(asyncExecutor, functionsList);
			
			clientes.clear();
			clientes = (List<Cliente>) controladorCliente.listarClientesNaoIntegrados();

		}
		
	}


	/**
	 * Método que faz a integracao de um cliente
	 *
	 * @param cliente Cliente
	 * @throws NegocioException
	 */
	@Override
	public boolean reintegrarCliente(Cliente cliente) throws NegocioException {
		ControladorParametroSistema ctlParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ParametroSistema parametroClienteEndereco =
				ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE);
		ParametroSistema parametroClienteTelefone =
				ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LIMITE_INTEGRACAO_TELEFONE_CLIENTE);
		ParametroSistema parametroClienteContato =
				ctlParametroSistema.obterParametroPorCodigo(Constantes.P_LIMITE_INTEGRACAO_CONTATO_CLIENTE);
		
		long quantidadeClienteEndereco = Long.parseLong(parametroClienteEndereco.getValor());
		long quantidadeClienteTelefone = Long.parseLong(parametroClienteTelefone.getValor());
		long quantidadeClienteContato = Long.parseLong(parametroClienteContato.getValor());
		
		Map<String, ConstanteSistema> mapConstantes = listarConstantesOperacaoIncluir();
		
		Collection<IntegracaoSistema> listaIntegracaoSistema =
				this.consultarIntegracaoSistemaFuncao(Long.parseLong(mapConstantes.get(Constantes.C_INTEGRACAO_CADASTRO_CLIENTES).getValor()));
		
		for (IntegracaoSistema sistemaIntegrante : listaIntegracaoSistema) {
			IntegracaoCliente integracaoCliente =
					preencherIntegracaoCliente(null, cliente, sistemaIntegrante.getSigla().toUpperCase(), mapConstantes);
			
			super.inserir(integracaoCliente);
			
			inserirIntegracaoClienteEndereco(cliente, quantidadeClienteEndereco, mapConstantes, sistemaIntegrante, integracaoCliente);
			
			inserirIntegracaoClienteTelefone(cliente, quantidadeClienteTelefone, mapConstantes, 
					sistemaIntegrante.getSigla().toUpperCase(), integracaoCliente);
			
			inserirIntegracaoClienteContato(cliente, quantidadeClienteContato, mapConstantes, 
					sistemaIntegrante.getSigla().toUpperCase(), integracaoCliente);
		}
		
		return true;
	}
	
	/**
	 * Gera um map com todas as constantes utilizadas no processo de reintegrar cliente
	 *
	 * @return Map<String, ConstanteSistema>
	 */
	private Map<String, ConstanteSistema> listarConstantesReintegrarClientes() {
		Map<String, ConstanteSistema> mapConstanstes = new HashMap<String, ConstanteSistema>();
		
		ControladorConstanteSistema controlador = getControladorConstanteSistema();
		
		ConstanteSistema constanteCadastroClientes = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_CADASTRO_CLIENTES);
		ConstanteSistema constanteIntegracaoSistemaGGAS = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);
		ConstanteSistema constanteSim = controlador.obterConstantePorCodigo(Constantes.C_SIM);
		ConstanteSistema constanteNao = controlador.obterConstantePorCodigo(Constantes.C_NAO);
		
		ConstanteSistema constanteNaoProcessado = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema constanteAguardandoRetorno =
				controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO);
		ConstanteSistema constanteErro = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_ERRO);
		
		mapConstanstes.put(Constantes.C_INTEGRACAO_SITUACAO_AGUARDANDO_RETORNO, constanteAguardandoRetorno);
		mapConstanstes.put(Constantes.C_INTEGRACAO_SITUACAO_ERRO, constanteErro);
		mapConstanstes.put(Constantes.C_INTEGRACAO_CADASTRO_CLIENTES, constanteCadastroClientes);
		mapConstanstes.put(Constantes.C_INTEGRACAO_SISTEMA_GGAS, constanteIntegracaoSistemaGGAS);
		mapConstanstes.put(Constantes.C_SIM, constanteSim);
		mapConstanstes.put(Constantes.C_NAO, constanteNao);
		mapConstanstes.put(ControladorIntegracaoImpl.OPERACAO, null);
		mapConstanstes.put(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO, constanteNaoProcessado);
		
		return mapConstanstes;
	}
	
	/**
	 * Gera um map com todas as constantes utilizadas na inclusao, 
	 * durante o processo de reintegrar cliente
	 *
	 * @return Map<String, ConstanteSistema>
	 */
	private Map<String, ConstanteSistema> listarConstantesOperacaoIncluir() {
		Map<String, ConstanteSistema> mapConstanstes = listarConstantesReintegrarClientes();
		
		ControladorConstanteSistema controlador = getControladorConstanteSistema();
		
		ConstanteSistema constanteInclusao = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);
		
		mapConstanstes.put(ControladorIntegracaoImpl.OPERACAO, constanteInclusao);
		return mapConstanstes;
	}
	
	/**
	 * Gera um map com todas as constantes utilizadas na remocao, 
	 * durante o processo de reintegrar cliente
	 *
	 * @return Map<String, ConstanteSistema>
	 */
	private Map<String, ConstanteSistema> listarConstantesOperacaoRemover() {
		Map<String, ConstanteSistema> mapConstanstes = listarConstantesReintegrarClientes();
		
		ControladorConstanteSistema controlador = getControladorConstanteSistema();
		
		ConstanteSistema constanteExclusao = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);
		
		mapConstanstes.put(ControladorIntegracaoImpl.OPERACAO, constanteExclusao);
		return mapConstanstes;
	}
	
	/**
	 * Gera um map com todas as constantes utilizadas na alteracao, 
	 * durante o processo de reintegrar cliente
	 *
	 * @return Map<String, ConstanteSistema>
	 */
	private Map<String, ConstanteSistema> listarConstantesOperacaoAlteracao() {
		Map<String, ConstanteSistema> mapConstanstes = listarConstantesReintegrarClientes();
		
		ControladorConstanteSistema controlador = getControladorConstanteSistema();
		

		ConstanteSistema constanteAlteracao = controlador.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);
		
		mapConstanstes.put(ControladorIntegracaoImpl.OPERACAO, constanteAlteracao);
		return mapConstanstes;
	}
	
	/**
	 * Netodo que atualiza a integracao da fatura
	 *
	 * @param Fatura fatura
	 * @param boolean tituloReceber
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	@Override
	public void atualizarIntegracaoFatura(Fatura fatura, boolean tituloReceber) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema ctlConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteTitulo = ctlConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);
		List<String> listaSistemasIntegrantes = this.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteTitulo.getValor()));
		if (listaSistemasIntegrantes != null && !listaSistemasIntegrantes.isEmpty()) {

			for (String sistemaIntegrante : listaSistemasIntegrantes) {
				Long chavePrimaria = this.buscarTituloPorFatura(fatura.getChavePrimaria(), tituloReceber, sistemaIntegrante);
				if (chavePrimaria == null) {
					throw new NegocioException(Constantes.ERRO_INTEGRACAO_TITULO_RECEBER_INEXISTENTE, true);
				}

				if (!tituloReceber) {
					Map<String, Object> filtroTituloPagar = new HashMap<String, Object>();
					filtroTituloPagar.put("fatura", fatura.getChavePrimaria());
					filtroTituloPagar.put("sistemaDestino", sistemaIntegrante);

					Collection<IntegracaoTituloPagar> listaIntegracaoTituloPagar = this.consultarIntegracaoTituloPagar(filtroTituloPagar);

					for (IntegracaoTituloPagar integracaoTituloPagar : listaIntegracaoTituloPagar) {
						integracaoTituloPagar.setDataVencimento(fatura.getDataVencimento());
						super.atualizar(integracaoTituloPagar, IntegracaoTituloPagarImpl.class);
					}

				} else {

					Map<String, Object> filtroTituloReceber = new HashMap<String, Object>();
					filtroTituloReceber.put("fatura", fatura.getChavePrimaria());
					filtroTituloReceber.put("sistemaDestino", sistemaIntegrante);

					Collection<IntegracaoTituloReceber> listaIntegracaoTituloReceber =
									this.consultarIntegracaoTituloReceber(filtroTituloReceber);

					for (IntegracaoTituloReceber integracaoTituloReceber : listaIntegracaoTituloReceber) {
						integracaoTituloReceber.setDataVencimento(fatura.getDataVencimento());
						super.atualizar(integracaoTituloReceber, IntegracaoTituloReceberImpl.class);
					}

				}

			}
		}

	}

	/**
	 * Consultar integracao titulo receber.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<IntegracaoTituloReceber> consultarIntegracaoTituloReceber(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoTituloReceber());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			ServiceLocator.getInstancia().getControladorParametroSistema();
			ServiceLocator.getInstancia().getControladorMedidor();

			Long fatura = (Long) filtro.get("fatura");
			if ((fatura != null) && (fatura > 0)) {
				criteria.add(Restrictions.eq("fatura", fatura));
			}

			String sistemaDestino = (String) filtro.get("sistemaDestino");
			if (!Strings.isNullOrEmpty(sistemaDestino)) {
				criteria.add(Restrictions.eq("sistemaDestino", sistemaDestino));
			}

		}

		return criteria.list();

	}

	/**
	 * Consultar integracao titulo baixa.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<IntegracaoTituloReceber> consultarIntegracaoTituloBaixa(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeIntegracaoTituloBaixa());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			ServiceLocator.getInstancia().getControladorParametroSistema();
			ServiceLocator.getInstancia().getControladorMedidor();

			Long fatura = (Long) filtro.get("fatura");
			if ((fatura != null) && (fatura > 0)) {
				criteria.add(Restrictions.eq("fatura", fatura));
			}

			String sistemaDestino = (String) filtro.get("sistemaDestino");
			if (!Strings.isNullOrEmpty(sistemaDestino)) {
				criteria.add(Restrictions.eq("sistemaDestino", sistemaDestino));
			}

		}

		return criteria.list();

	}

	/**
	 * Consultar integracao nota fiscal.
	 *
	 * @param fatura
	 *            the fatura
	 * @return the collection
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<IntegracaoNotaFiscal> consultarIntegracaoNotaFiscal(Fatura fatura) {

		Criteria criteria =
						createCriteria(ServiceLocator.getInstancia().getClassPorID(IntegracaoNotaFiscal.BEAN_ID_INTEGRACAO_NOTA_FISCAL));

		criteria.add(Restrictions.eq("fatura", fatura.getChavePrimaria()));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral.ControladorIntegracao
	 * #consultarIntegracaoTituloReceber(
	 * br.com.ggas.faturamento.Fatura)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<IntegracaoTituloReceber> consultarIntegracaoTituloReceber(Fatura fatura) {

		Criteria criteria =
						createCriteria(ServiceLocator.getInstancia().getClassPorID(
										IntegracaoTituloReceber.BEAN_ID_INTEGRACAO_TITULO_RECEBER));

		criteria.add(Restrictions.eq("fatura", fatura.getChavePrimaria()));

		return criteria.list();
	}

	/**
	 * Atualizar campos da tabela NFE com a tabela TI_NOTA_FISCAL
	 * @param dataEmissao String
	 * @throws GGASException
	 */
	public void atualizarCamposNFE(String dataEmissao) throws GGASException {

		List<IntegracaoNotaFiscalImpl> listaTINF = this.consultarIntegracaoNotaFiscalTipoOperacaoNfe("S", "P", dataEmissao);

		ControladorNfe controladorNfe =
				(ControladorNfe) ServiceLocator.getInstancia().getControladorNegocio(ControladorNfe.BEAN_ID_CONTROLADOR_NFE);

		if (listaTINF != null) {
			for (IntegracaoNotaFiscalImpl tiNotaFiscal : listaTINF) {
				if(tiNotaFiscal != null) {
					Nfe nfe = controladorNfe.consultarDadosNotaFiscalEletronica(tiNotaFiscal.getNumero(), tiNotaFiscal.getNumeroSerie(),
							tiNotaFiscal.getDataEmissao());
					
					if (nfe != null && !tiNotaFiscal.getStatusNFE().equals(nfe.getSisStatus())) {
						
						nfe.setSisStatus(tiNotaFiscal.getStatusNFE());
						nfe.setNumeroProtocoloEnvio(tiNotaFiscal.getProtocolo());
						nfe.setSisOperacao(tiNotaFiscal.getSituacao());
						nfe.setChaveAcesso(tiNotaFiscal.getChaveAcessoNFE());
						
						this.tratarMsgNFE(nfe, tiNotaFiscal.getStatusNFE());
						
						controladorNfe.atualizar(nfe);
					}
				}
			}
		}
	}

	/**
	 * Trata a mensagem NFe
	 * @param nfe O objeto NFe
	 * @param statusNFE O status NFe
	 */
	private void tratarMsgNFE(Nfe nfe, String statusNFE) {
		switch (statusNFE) {
			case "AUT":
				nfe.setNumeroMensagemE(NUMERO_MSG_ENV_AUT);
				break;
			case "CAN":
				nfe.setNumeroMensagemE(NUMERO_MSG_ENV_CAN);
				break;
			case "DEN":
				nfe.setNumeroMensagemE(NUMERO_MSG_ENV_DEN);
				break;
			default:
    			List<String> listStatusNFE = Arrays.asList("ER", "EMX", "EV", "ERD", "ERS", "EES", "REJ");
				if (listStatusNFE.contains(statusNFE)) {
			    	nfe.setNumeroMensagemE(NUMERO_MSG_ENV_ERRO);
				}
				break;
		}
	}

	/**
	 * Consultar nota fiscal em TI_NOTA_FISCAL que tiverem o Tipo Operacao diferente A
	 * e o Status for diferente de NP (processadas)
	 * @param codigoTipoOperacao O tipo da operação
	 * @param codigoSituacao A situação
	 * @param dataEmissao A data de emissão
	 * @return A lista de IntegracaoNotaFiscal
	 * @throws GGASException A GGASException
	 */
	@SuppressWarnings("unchecked")
	public List<IntegracaoNotaFiscalImpl> consultarIntegracaoNotaFiscalTipoOperacaoNfe(String codigoTipoOperacao, String codigoSituacao,
			String dataEmissao) throws GGASException {

		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoNotaFiscal().getSimpleName());
		hql.append(" integracaoNotaFiscal ");

		hql.append(" WHERE ");

		hql.append(" integracaoNotaFiscal.tipoOperacao = :codigoTipoOperacao ");
		hql.append(" and integracaoNotaFiscal.situacao = :codigoSituacao ");
		hql.append(" and integracaoNotaFiscal.dataEmissao > :dataEmissao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("codigoTipoOperacao", codigoTipoOperacao);
		query.setString("codigoSituacao", codigoSituacao);
		if (Strings.isNullOrEmpty(dataEmissao)) {
			DateTime dataEmissaoDefault = new DateTime(Calendar.getInstance().getTime());
			dataEmissaoDefault = dataEmissaoDefault.minusMonths(CONSTANTE_NUMERO_DOIS);
			query.setDate(DATA_EMISSAO, dataEmissaoDefault.toDate());
		} else {
			Date dataParametro = Util.converterCampoStringParaData(DATA_EMISSAO, dataEmissao, Constantes.FORMATO_DATA_BR);
			Util.adicionarRestricaoDataSemHora(query, dataParametro, DATA_EMISSAO, Boolean.TRUE);
		}

		return query.list();
	}

	/**
	 * Consultar nota fiscal em TI_NOTA_FISCAL que tiverem o Tipo Operacao diferente A
	 * e o Status for diferente de NP (processadas)
	 * @param codigoTipoOperacao
	 * @param codigoSituacao
	 * @param numero
	 * @throws NegocioException
	 */
	public IntegracaoNotaFiscalImpl consultarIntegracaoNotaFiscalTipoOperacaoNfe(String codigoTipoOperacao,
			String codigoSituacao, Integer numero) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidadeIntegracaoNotaFiscal().getSimpleName());
		hql.append(" integracaoNotaFiscal ");

		hql.append(" WHERE ");

		hql.append(" integracaoNotaFiscal.tipoOperacao = :codigoTipoOperacao ");
		hql.append(" and integracaoNotaFiscal.situacao = :codigoSituacao ");
		hql.append(" and integracaoNotaFiscal.numero = :numero ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("codigoTipoOperacao", codigoTipoOperacao);
		query.setString("codigoSituacao", codigoSituacao);
		query.setInteger("numero", numero);

		return (IntegracaoNotaFiscalImpl) query.uniqueResult();
	}
	
	@Override
	public IntegracaoCliente buscarSituacaoCliente(Cliente cliente) {

		Query query = null;
		ConstanteSistema situacaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
		StringBuilder hql = new StringBuilder();
		hql.append(" select cliente.clienteSituacao as clienteSituacao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoCliente().getSimpleName());
		hql.append(" cliente ");
		hql.append(" where cliente.clienteOrigem = :idCliente ");
		hql.append(" and cliente.situacao = :situacao ");
		hql.append(" and cliente.sistemaOrigem = 'GAS' ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeIntegracaoCliente()));
		query.setLong("idCliente", cliente.getChavePrimaria());
		query.setString("situacao", situacaoProcessado.getValor());

		return (IntegracaoCliente) query.uniqueResult();

	}
	
	@Override
	public IntegracaoFuncao buscarIntegracaoFuncaoNome(String nome) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIntegracaoFuncao().getSimpleName());
		hql.append(" integracaoFuncao ");
		hql.append(" where integracaoFuncao.nome = :nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("nome", nome);
		query.setMaxResults(1);

		return (IntegracaoFuncao) query.uniqueResult();

	}
}
