/*
 Copyright (C) <2011> GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este arquivo Ã© parte do GGAS, um sistema de gestÃ£o comercial de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este programa Ã© um software livre; vocÃª pode redistribuÃ­-lo e/ou
 modificÃ¡-lo sob os termos de LicenÃ§a PÃºblica Geral GNU, conforme
 publicada pela Free Software Foundation; versÃ£o 2 da LicenÃ§a.

 O GGAS Ã© distribuÃ­do na expectativa de ser Ãºtil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implÃ­cita de
 COMERCIALIZAÃ‡ÃƒO ou de ADEQUAÃ‡ÃƒO A QUALQUER PROPÃ“SITO EM PARTICULAR.
 Consulte a LicenÃ§a PÃºblica Geral GNU para obter mais detalhes.

 VocÃª deve ter recebido uma cÃ³pia da LicenÃ§a PÃºblica Geral GNU
 junto com este programa; se nÃ£o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place â€“ Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:10:20
 @author vtavares
 */

package br.com.ggas.integracao.geral.impl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.geral.IntegracaoParametro;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.IntegracaoSistemaFuncao;
import br.com.ggas.integracao.geral.IntegracaoSistemaParametro;
import br.com.ggas.util.Constantes;


/**
 * Classe responsável pela implementação dos métodos relacionados a parâmetros de integração do sistema
 *
 */
public class IntegracaoSistemaParametroImpl extends EntidadeNegocioImpl implements IntegracaoSistemaParametro {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -7983964813569591039L;

	private IntegracaoParametro integracaoParametro;

	private IntegracaoSistemaFuncao integracaoSistemaFuncao;

	private String valor;

	@Transient
	private IntegracaoSistema integracaoSistema;

	@Override
	public String getValor() {

		return valor;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.integracao.geral
	 * .IntegracaoSistemaParametro
	 * #getValorLong()
	 */
	@Override
	public Long getValorLong() throws NegocioException {
		String valorLong = this.getValor();
		if (StringUtils.isNotEmpty(valorLong)) {
			return Long.valueOf(valorLong);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, 
							getChavePrimaria());
		}
	}

	@Override
	public void setValor(String valor) {

		this.valor = valor;
	}

	@Override
	public IntegracaoParametro getIntegracaoParametro() {

		return integracaoParametro;
	}

	@Override
	public void setIntegracaoParametro(IntegracaoParametro integracaoParametro) {

		this.integracaoParametro = integracaoParametro;
	}

	@Override
	public IntegracaoSistemaFuncao getIntegracaoSistemaFuncao() {

		return integracaoSistemaFuncao;
	}

	@Override
	public void setIntegracaoSistemaFuncao(IntegracaoSistemaFuncao integracaoSistemaFuncao) {

		this.integracaoSistemaFuncao = integracaoSistemaFuncao;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	@Override
	public IntegracaoSistema getIntegracaoSistema() {

		return integracaoSistema;
	}

	@Override
	public void setIntegracaoSistema(IntegracaoSistema integracaoSistema) {

		this.integracaoSistema = integracaoSistema;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.valor == null || this.valor.length() == 0) {
			stringBuilder.append("valor");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
