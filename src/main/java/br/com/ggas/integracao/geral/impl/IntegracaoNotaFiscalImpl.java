/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:16:20
 @author vtavares
 */

package br.com.ggas.integracao.geral.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.IntegracaoNotaFiscal;

/**
 * Classe da Entidade de Integração das Notas fiscais 
 * Documentos fiscais para as tabelas de integração
 *
 */
public class IntegracaoNotaFiscalImpl extends IntegracaoImpl implements IntegracaoNotaFiscal {

	
	private static final long serialVersionUID = -7874459283226924071L;

	private Long cfop;

	private Long serie;

	private Long numero;

	private String numeroSerie;

	private String inscricaoEstadualSubstituicao;

	private BigDecimal valorTotal;

	private String nomeCliente;

	private String numeroCpfCnpj;

	private String inscricaoEstadual;

	private String numeroRG;

	private String endereco;

	private String complementoEndereco;

	private String nomeBairro;

	private String numeroCEP;

	private String nomeMunicipio;

	private String uf;

	private Date dataApresentacao;

	private Date dataEmissao;

	private String mensagem;

	private String tipoNotaFiscal;

	private String tipoEmitente;

	private String tipoOperacao;

	private String tipoFaturamento;

	private String tipoEmissaoNFE;

	private String statusNFE;

	private String chaveAcessoNFE;

	private String protocolo;

	private IntegracaoCliente cliente;

	private IntegracaoContrato contrato;

	private Long fatura;

	private Long documentoFiscalOrigem;

	private Long documentoFiscalDestino;

	private Date dataCancelamento;

	private Long motivoCancelamento;

	private BigDecimal valorDesconto;

	private String situacao;

	private String convenioArrecadador;

	private String bancoCliente;

	private String agenciaCliente;

	private String contaCliente;
	
	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	@Override
	public Long getCfop() {

		return cfop;
	}

	@Override
	public void setCfop(Long cfop) {

		this.cfop = cfop;
	}

	@Override
	public Long getSerie() {

		return serie;
	}

	@Override
	public void setSerie(Long serie) {

		this.serie = serie;
	}

	@Override
	public String getInscricaoEstadualSubstituicao() {

		return inscricaoEstadualSubstituicao;
	}

	@Override
	public void setInscricaoEstadualSubstituicao(String inscricaoEstadualSubstituicao) {

		this.inscricaoEstadualSubstituicao = inscricaoEstadualSubstituicao;
	}

	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	@Override
	public String getNomeCliente() {

		return nomeCliente;
	}

	@Override
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	@Override
	public String getNumeroCpfCnpj() {

		return numeroCpfCnpj;
	}

	@Override
	public void setNumeroCpfCnpj(String numeroCpfCnpj) {

		this.numeroCpfCnpj = numeroCpfCnpj;
	}

	@Override
	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	@Override
	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	@Override
	public String getNumeroRG() {

		return numeroRG;
	}

	@Override
	public void setNumeroRG(String numeroRG) {

		this.numeroRG = numeroRG;
	}

	@Override
	public String getEndereco() {

		return endereco;
	}

	@Override
	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	@Override
	public String getComplementoEndereco() {

		return complementoEndereco;
	}

	@Override
	public void setComplementoEndereco(String complementoEndereco) {

		this.complementoEndereco = complementoEndereco;
	}

	@Override
	public String getNomeBairro() {

		return nomeBairro;
	}

	@Override
	public void setNomeBairro(String nomeBairro) {

		this.nomeBairro = nomeBairro;
	}

	@Override
	public String getNumeroCEP() {

		return numeroCEP;
	}

	@Override
	public void setNumeroCEP(String numeroCEP) {

		this.numeroCEP = numeroCEP;
	}

	@Override
	public String getNomeMunicipio() {

		return nomeMunicipio;
	}

	@Override
	public void setNomeMunicipio(String nomeMunicipio) {

		this.nomeMunicipio = nomeMunicipio;
	}

	@Override
	public String getUf() {

		return uf;
	}

	@Override
	public void setUf(String uf) {

		this.uf = uf;
	}

	@Override
	public Date getDataApresentacao() {
		Date data = null;
		if (this.dataApresentacao != null) {
			data = (Date) dataApresentacao.clone();
		}
		return data;
	}

	@Override
	public void setDataApresentacao(Date dataApresentacao) {
		if (dataApresentacao != null) {
			this.dataApresentacao = (Date) dataApresentacao.clone();
		} else {
			this.dataApresentacao = null;
		}
	}

	@Override
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	@Override
	public void setDataEmissao(Date dataEmissao) {
		if(dataEmissao != null) {
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	@Override
	public String getMensagem() {

		return mensagem;
	}

	@Override
	public void setMensagem(String mensagem) {

		this.mensagem = mensagem;
	}

	@Override
	public String getTipoNotaFiscal() {

		return tipoNotaFiscal;
	}

	@Override
	public void setTipoNotaFiscal(String tipoNotaFiscal) {

		this.tipoNotaFiscal = tipoNotaFiscal;
	}

	@Override
	public String getTipoEmitente() {

		return tipoEmitente;
	}

	@Override
	public void setTipoEmitente(String tipoEmitente) {

		this.tipoEmitente = tipoEmitente;
	}

	@Override
	public String getTipoOperacao() {

		return tipoOperacao;
	}

	@Override
	public void setTipoOperacao(String tipoOperacao) {

		this.tipoOperacao = tipoOperacao;
	}

	@Override
	public String getTipoFaturamento() {

		return tipoFaturamento;
	}

	@Override
	public void setTipoFaturamento(String tipoFaturamento) {

		this.tipoFaturamento = tipoFaturamento;
	}

	@Override
	public String getTipoEmissaoNFE() {

		return tipoEmissaoNFE;
	}

	@Override
	public void setTipoEmissaoNFE(String tipoEmissaoNFE) {

		this.tipoEmissaoNFE = tipoEmissaoNFE;
	}

	@Override
	public String getStatusNFE() {

		return statusNFE;
	}

	@Override
	public void setStatusNFE(String statusNFE) {

		this.statusNFE = statusNFE;
	}

	@Override
	public String getChaveAcessoNFE() {

		return chaveAcessoNFE;
	}

	@Override
	public void setChaveAcessoNFE(String chaveAcessoNFE) {

		this.chaveAcessoNFE = chaveAcessoNFE;
	}

	@Override
	public String getProtocolo() {

		return protocolo;
	}

	@Override
	public void setProtocolo(String protocolo) {

		this.protocolo = protocolo;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	@Override
	public IntegracaoCliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(IntegracaoCliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public IntegracaoContrato getContrato() {
		return contrato;
	}

	@Override
	public void setContrato(IntegracaoContrato contrato) {
		this.contrato = contrato;
	}

	@Override
	public Long getFatura() {

		return fatura;
	}

	@Override
	public void setFatura(Long fatura) {

		this.fatura = fatura;
	}

	@Override
	public Long getNumero() {

		return numero;
	}

	@Override
	public void setNumero(Long numero) {

		this.numero = numero;
	}

	@Override
	public String getNumeroSerie() {

		return numeroSerie;
	}

	@Override
	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	@Override
	public Long getDocumentoFiscalOrigem() {

		return documentoFiscalOrigem;
	}

	@Override
	public void setDocumentoFiscalOrigem(Long documentoFiscalOrigem) {

		this.documentoFiscalOrigem = documentoFiscalOrigem;
	}

	@Override
	public Long getDocumentoFiscalDestino() {

		return documentoFiscalDestino;
	}

	@Override
	public void setDocumentoFiscalDestino(Long documentoFiscalDestino) {

		this.documentoFiscalDestino = documentoFiscalDestino;
	}

	@Override
	public Date getDataCancelamento() {
		Date data = null;
		if (this.dataCancelamento != null) {
			data = (Date) dataCancelamento.clone();
		}
		return data;
	}

	@Override
	public void setDataCancelamento(Date dataCancelamento) {
		if (dataCancelamento != null) {
			this.dataCancelamento = (Date) dataCancelamento.clone();
		} else {
			this.dataCancelamento = null;
		}
	}

	@Override
	public Long getMotivoCancelamento() {

		return motivoCancelamento;
	}

	@Override
	public void setMotivoCancelamento(Long motivoCancelamento) {

		this.motivoCancelamento = motivoCancelamento;
	}

	@Override
	public BigDecimal getValorDesconto() {

		return valorDesconto;
	}

	@Override
	public void setValorDesconto(BigDecimal valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	@Override
	public String getConvenioArrecadador() {
		return convenioArrecadador;
	}

	@Override
	public void setConvenioArrecadador(String convenioArrecadador) {
		this.convenioArrecadador = convenioArrecadador;
	}

	@Override
	public String getBancoCliente() {
		return bancoCliente;
	}

	@Override
	public void setBancoCliente(String bancoCliente) {
		this.bancoCliente = bancoCliente;
	}

	@Override
	public String getAgenciaCliente() {
		return agenciaCliente;
	}

	@Override
	public void setAgenciaCliente(String agenciaCliente) {
		this.agenciaCliente = agenciaCliente;
	}

	@Override
	public String getContaCliente() {
		return contaCliente;
	}

	@Override
	public void setContaCliente(String contaCliente) {
		this.contaCliente = contaCliente;
	}
}
