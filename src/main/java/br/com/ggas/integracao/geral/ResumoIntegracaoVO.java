/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 01/07/2013 16:17:49
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import java.util.Map;

/**
 * Classe Resumo Integracao.
 * 
 *
 */
public class ResumoIntegracaoVO {

	private Integer quantidadeRegistrosProcessados = 0;

	private Integer quantidadeRegistrosIncluidos = 0;

	private Integer quantidadeRegistrosAlterados = 0;

	private Integer quantidadeRegistrosExcluidos = 0;

	private Integer quantidadeRegistrosInconsistentes = 0;

	private Map<String, Object[]> mapaInconsistencias;

	/**
	 * Instantiates a new resumo integracao vo.
	 */
	public ResumoIntegracaoVO() {

		super();
	}

	/**
	 * Instantiates a new resumo integracao vo.
	 * 
	 * @param quantidadeRegistrosProcessados
	 *            the quantidade registros processados
	 * @param quantidadeRegistrosIncluidos
	 *            the quantidade registros incluidos
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @param quantidadeRegistrosInconsistentes
	 *            the quantidade registros inconsistentes
	 */
	public ResumoIntegracaoVO(Integer quantidadeRegistrosProcessados, Integer quantidadeRegistrosIncluidos,
								Map<String, Object[]> mapaInconsistencias, Integer quantidadeRegistrosInconsistentes) {

		super();
		this.quantidadeRegistrosProcessados = quantidadeRegistrosProcessados;
		this.quantidadeRegistrosIncluidos = quantidadeRegistrosIncluidos;
		this.mapaInconsistencias = mapaInconsistencias;
		this.quantidadeRegistrosInconsistentes = quantidadeRegistrosInconsistentes;
	}

	/**
	 * Instantiates a new resumo integracao vo.
	 * 
	 * @param quantidadeRegistrosProcessados
	 *            the quantidade registros processados
	 * @param quantidadeRegistrosIncluidos
	 *            the quantidade registros incluidos
	 * @param quantidadeRegistrosAlterados
	 *            the quantidade registros alterados
	 * @param quantidadeRegistrosExcluidos
	 *            the quantidade registros excluidos
	 * @param quantidadeRegistrosInconsistentes
	 *            the quantidade registros inconsistentes
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 */
	public ResumoIntegracaoVO(Integer quantidadeRegistrosProcessados, Integer quantidadeRegistrosIncluidos,
								Integer quantidadeRegistrosAlterados, Integer quantidadeRegistrosExcluidos,
								Integer quantidadeRegistrosInconsistentes, Map<String, Object[]> mapaInconsistencias) {

		super();
		this.quantidadeRegistrosProcessados = quantidadeRegistrosProcessados;
		this.quantidadeRegistrosIncluidos = quantidadeRegistrosIncluidos;
		this.quantidadeRegistrosAlterados = quantidadeRegistrosAlterados;
		this.quantidadeRegistrosExcluidos = quantidadeRegistrosExcluidos;
		this.quantidadeRegistrosInconsistentes = quantidadeRegistrosInconsistentes;
		this.mapaInconsistencias = mapaInconsistencias;
	}

	/**
	 * @return the quantidadeRegistrosProcessados
	 */
	public Integer getQuantidadeRegistrosProcessados() {

		return quantidadeRegistrosProcessados;
	}

	/**
	 * @param quantidadeRegistrosProcessados
	 *            the quantidadeRegistrosProcessados to set
	 */
	public void setQuantidadeRegistrosProcessados(Integer quantidadeRegistrosProcessados) {

		this.quantidadeRegistrosProcessados = quantidadeRegistrosProcessados;
	}

	/**
	 * Soma os registros processados.
	 */
	public void updateQuantidadeRegistrosProcessados() {
		
		this.quantidadeRegistrosProcessados = 
						quantidadeRegistrosAlterados 
						+ quantidadeRegistrosIncluidos 
						+ quantidadeRegistrosExcluidos
						+ quantidadeRegistrosInconsistentes;
	}

	/**
	 * @return the quantidadeRegistrosIncluidos
	 */
	public Integer getQuantidadeRegistrosIncluidos() {

		return quantidadeRegistrosIncluidos;
	}

	/**
	 * @param quantidadeRegistrosIncluidos
	 *            the quantidadeRegistrosIncluidos to set
	 */
	public void setQuantidadeRegistrosIncluidos(Integer quantidadeRegistrosIncluidos) {

		this.quantidadeRegistrosIncluidos = quantidadeRegistrosIncluidos;
	}

	/**
	 * Incrementa mais um a quantidade de registros incluidos.
	 */
	public void incrementarQuantidadeRegistrosIncluidos() {

		this.quantidadeRegistrosIncluidos = this.quantidadeRegistrosIncluidos + 1;
	}

	/**
	 * @return the mapaInconsistencias
	 */
	public Map<String, Object[]> getMapaInconsistencias() {

		return mapaInconsistencias;
	}

	/**
	 * @param mapaInconsistencias
	 *            the mapaInconsistencias to set
	 */
	public void setMapaInconsistencias(Map<String, Object[]> mapaInconsistencias) {

		this.mapaInconsistencias = mapaInconsistencias;
	}

	/**
	 * @return the quantidadeRegistrosInconsistentes
	 */
	public Integer getQuantidadeRegistrosInconsistentes() {

		return quantidadeRegistrosInconsistentes;
	}

	/**
	 * @param quantidadeRegistrosInconsistentes
	 *            the quantidadeRegistrosInconsistentes to set
	 */
	public void setQuantidadeRegistrosInconsistentes(Integer quantidadeRegistrosInconsistentes) {

		this.quantidadeRegistrosInconsistentes = quantidadeRegistrosInconsistentes;
	}

	/**
	 *  Incrementa mais um a quantidade de registro inconsistentes.
	 */
	public void incrementarQuantidadeRegistrosInconsistentes() {

		this.quantidadeRegistrosInconsistentes = this.quantidadeRegistrosInconsistentes + 1;
	}

	public Integer getQuantidadeRegistrosAlterados() {

		return quantidadeRegistrosAlterados;
	}

	public void setQuantidadeRegistrosAlterados(Integer quantidadeRegistrosAlterados) {

		this.quantidadeRegistrosAlterados = quantidadeRegistrosAlterados;
	}

	public Integer getQuantidadeRegistrosExcluidos() {

		return quantidadeRegistrosExcluidos;
	}

	public void setQuantidadeRegistrosExcluidos(Integer quantidadeRegistrosExcluidos) {

		this.quantidadeRegistrosExcluidos = quantidadeRegistrosExcluidos;
	}

}
