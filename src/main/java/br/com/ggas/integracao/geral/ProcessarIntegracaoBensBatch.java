/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/07/2013 16:53:42
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.bens.batch.ProcessarIntegracaoCadastroBensBatch;
import br.com.ggas.integracao.bens.batch.ProcessarIntegracaoMovimentacaoBensBatch;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Classe Processar Integracao Bens Batch.
 *
 *
 */
@Component
public class ProcessarIntegracaoBensBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		String logProcessamentoCadastroBens = null;
		String logProcessamentoMovimentacaoBens = null;

		try {

			ProcessarIntegracaoCadastroBensBatch processarIntegracaoCadastroBensBatch = new ProcessarIntegracaoCadastroBensBatch();
			logProcessamentoCadastroBens = processarIntegracaoCadastroBensBatch.processar(parametros);

			ProcessarIntegracaoMovimentacaoBensBatch processarIntegracaoMovimentacaoBensBatch = new ProcessarIntegracaoMovimentacaoBensBatch();
			logProcessamentoMovimentacaoBens = processarIntegracaoMovimentacaoBensBatch.processar(parametros);

		} catch(HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		} catch(NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new NegocioException(e);
		} catch(GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}
		return logProcessamentoCadastroBens + Constantes.PULA_LINHA + logProcessamentoMovimentacaoBens;
	}

}
