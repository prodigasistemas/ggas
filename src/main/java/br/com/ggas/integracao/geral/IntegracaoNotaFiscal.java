/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;

/**
 * The Interface IntegracaoNotaFiscal.
 */
public interface IntegracaoNotaFiscal extends Integracao {

	/** The bean id integracao nota fiscal. */
	String BEAN_ID_INTEGRACAO_NOTA_FISCAL = "integracaoNotaFiscal";

	/**
	 * Gets the cfop.
	 *
	 * @return the cfop
	 */
	Long getCfop();

	/**
	 * Sets the cfop.
	 *
	 * @param cfop the new cfop
	 */
	void setCfop(Long cfop);

	/**
	 * Gets the serie.
	 *
	 * @return the serie
	 */
	Long getSerie();

	/**
	 * Sets the serie.
	 *
	 * @param serie the new serie
	 */
	void setSerie(Long serie);

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	Long getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero the new numero
	 */
	void setNumero(Long numero);

	/**
	 * Gets the numero serie.
	 *
	 * @return the numero serie
	 */
	String getNumeroSerie();

	/**
	 * Sets the numero serie.
	 *
	 * @param numeroSerie the new numero serie
	 */
	void setNumeroSerie(String numeroSerie);

	/**
	 * Gets the inscricao estadual substituicao.
	 *
	 * @return the inscricao estadual substituicao
	 */
	String getInscricaoEstadualSubstituicao();

	/**
	 * Sets the inscricao estadual substituicao.
	 *
	 * @param inscricaoEstadualSubstituicao the new inscricao estadual substituicao
	 */
	void setInscricaoEstadualSubstituicao(String inscricaoEstadualSubstituicao);

	/**
	 * Gets the valor total.
	 *
	 * @return the valor total
	 */
	BigDecimal getValorTotal();

	/**
	 * Sets the valor total.
	 *
	 * @param valorTotal the new valor total
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * Gets the nome cliente.
	 *
	 * @return the nome cliente
	 */
	String getNomeCliente();

	/**
	 * Sets the nome cliente.
	 *
	 * @param nomeCliente the new nome cliente
	 */
	void setNomeCliente(String nomeCliente);

	/**
	 * Gets the numero cpf cnpj.
	 *
	 * @return the numero cpf cnpj
	 */
	String getNumeroCpfCnpj();

	/**
	 * Sets the numero cpf cnpj.
	 *
	 * @param numeroCpfCnpj the new numero cpf cnpj
	 */
	void setNumeroCpfCnpj(String numeroCpfCnpj);

	/**
	 * Gets the inscricao estadual.
	 *
	 * @return the inscricao estadual
	 */
	String getInscricaoEstadual();

	/**
	 * Sets the inscricao estadual.
	 *
	 * @param inscricaoEstadual the new inscricao estadual
	 */
	void setInscricaoEstadual(String inscricaoEstadual);

	/**
	 * Gets the numero rg.
	 *
	 * @return the numero rg
	 */
	String getNumeroRG();

	/**
	 * Sets the numero rg.
	 *
	 * @param numeroRG the new numero rg
	 */
	void setNumeroRG(String numeroRG);

	/**
	 * Gets the endereco.
	 *
	 * @return the endereco
	 */
	String getEndereco();

	/**
	 * Sets the endereco.
	 *
	 * @param endereco the new endereco
	 */
	void setEndereco(String endereco);

	/**
	 * Gets the complemento endereco.
	 *
	 * @return the complemento endereco
	 */
	String getComplementoEndereco();

	/**
	 * Sets the complemento endereco.
	 *
	 * @param complementoEndereco the new complemento endereco
	 */
	void setComplementoEndereco(String complementoEndereco);

	/**
	 * Gets the nome bairro.
	 *
	 * @return the nome bairro
	 */
	String getNomeBairro();

	/**
	 * Sets the nome bairro.
	 *
	 * @param nomeBairro the new nome bairro
	 */
	void setNomeBairro(String nomeBairro);

	/**
	 * Gets the numero cep.
	 *
	 * @return the numero cep
	 */
	String getNumeroCEP();

	/**
	 * Sets the numero cep.
	 *
	 * @param numeroCEP the new numero cep
	 */
	void setNumeroCEP(String numeroCEP);

	/**
	 * Gets the nome municipio.
	 *
	 * @return the nome municipio
	 */
	String getNomeMunicipio();

	/**
	 * Sets the nome municipio.
	 *
	 * @param nomeMunicipio the new nome municipio
	 */
	void setNomeMunicipio(String nomeMunicipio);

	/**
	 * Gets the uf.
	 *
	 * @return the uf
	 */
	String getUf();

	/**
	 * Sets the uf.
	 *
	 * @param uf the new uf
	 */
	void setUf(String uf);

	/**
	 * Gets the data apresentacao.
	 *
	 * @return the data apresentacao
	 */
	Date getDataApresentacao();

	/**
	 * Sets the data apresentacao.
	 *
	 * @param dataApresentacao the new data apresentacao
	 */
	void setDataApresentacao(Date dataApresentacao);

	/**
	 * Gets the data emissao.
	 *
	 * @return the data emissao
	 */
	Date getDataEmissao();

	/**
	 * Sets the data emissao.
	 *
	 * @param dataEmissao the new data emissao
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * Gets the mensagem.
	 *
	 * @return the mensagem
	 */
	String getMensagem();

	/**
	 * Sets the mensagem.
	 *
	 * @param mensagem the new mensagem
	 */
	void setMensagem(String mensagem);

	/**
	 * Gets the tipo nota fiscal.
	 *
	 * @return the tipo nota fiscal
	 */
	String getTipoNotaFiscal();

	/**
	 * Sets the tipo nota fiscal.
	 *
	 * @param tipoNotaFiscal the new tipo nota fiscal
	 */
	void setTipoNotaFiscal(String tipoNotaFiscal);

	/**
	 * Gets the tipo emitente.
	 *
	 * @return the tipo emitente
	 */
	String getTipoEmitente();

	/**
	 * Sets the tipo emitente.
	 *
	 * @param tipoEmitente the new tipo emitente
	 */
	void setTipoEmitente(String tipoEmitente);

	/**
	 * Gets the tipo operacao.
	 *
	 * @return the tipo operacao
	 */
	String getTipoOperacao();

	/**
	 * Sets the tipo operacao.
	 *
	 * @param tipoOperacao the new tipo operacao
	 */
	void setTipoOperacao(String tipoOperacao);

	/**
	 * Gets the tipo faturamento.
	 *
	 * @return the tipo faturamento
	 */
	String getTipoFaturamento();

	/**
	 * Sets the tipo faturamento.
	 *
	 * @param tipoFaturamento the new tipo faturamento
	 */
	void setTipoFaturamento(String tipoFaturamento);

	/**
	 * Gets the tipo emissao nfe.
	 *
	 * @return the tipo emissao nfe
	 */
	String getTipoEmissaoNFE();

	/**
	 * Sets the tipo emissao nfe.
	 *
	 * @param tipoEmissaoNFE the new tipo emissao nfe
	 */
	void setTipoEmissaoNFE(String tipoEmissaoNFE);

	/**
	 * Gets the status nfe.
	 *
	 * @return the status nfe
	 */
	String getStatusNFE();

	/**
	 * Sets the status nfe.
	 *
	 * @param statusNFE the new status nfe
	 */
	void setStatusNFE(String statusNFE);

	/**
	 * Gets the chave acesso nfe.
	 *
	 * @return the chave acesso nfe
	 */
	String getChaveAcessoNFE();

	/**
	 * Sets the chave acesso nfe.
	 *
	 * @param chaveAcessoNFE the new chave acesso nfe
	 */
	void setChaveAcessoNFE(String chaveAcessoNFE);

	/**
	 * Gets the protocolo.
	 *
	 * @return the protocolo
	 */
	String getProtocolo();

	/**
	 * Sets the protocolo.
	 *
	 * @param protocolo the new protocolo
	 */
	void setProtocolo(String protocolo);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	IntegracaoCliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(IntegracaoCliente cliente);

	/**
	 * Gets o integracao Contrato
	 * @return o integracao Contrato
	 */
	IntegracaoContrato getContrato();

	/**
	 * Sets o integracao Contrato
	 * @param contrato o integracao Contrato
	 */
	void setContrato(IntegracaoContrato contrato);

	/**
	 * Gets the fatura.
	 *
	 * @return the fatura
	 */
	Long getFatura();

	/**
	 * Sets the fatura.
	 *
	 * @param fatura the new fatura
	 */
	void setFatura(Long fatura);

	/**
	 * Gets the documento fiscal origem.
	 *
	 * @return the documento fiscal origem
	 */
	Long getDocumentoFiscalOrigem();

	/**
	 * Sets the documento fiscal origem.
	 *
	 * @param documentoFiscalOrigem the new documento fiscal origem
	 */
	void setDocumentoFiscalOrigem(Long documentoFiscalOrigem);

	/**
	 * Gets the documento fiscal destino.
	 *
	 * @return the documento fiscal destino
	 */
	Long getDocumentoFiscalDestino();

	/**
	 * Sets the documento fiscal destino.
	 *
	 * @param documentoFiscalDestino the new documento fiscal destino
	 */
	void setDocumentoFiscalDestino(Long documentoFiscalDestino);

	/**
	 * Gets the data cancelamento.
	 *
	 * @return the data cancelamento
	 */
	Date getDataCancelamento();

	/**
	 * Sets the data cancelamento.
	 *
	 * @param dataCancelamento the new data cancelamento
	 */
	void setDataCancelamento(Date dataCancelamento);

	/**
	 * Gets the motivo cancelamento.
	 *
	 * @return the motivo cancelamento
	 */
	Long getMotivoCancelamento();

	/**
	 * Sets the motivo cancelamento.
	 *
	 * @param motivoCancelamento the new motivo cancelamento
	 */
	void setMotivoCancelamento(Long motivoCancelamento);

	/**
	 * Gets the valor desconto.
	 *
	 * @return the valor desconto
	 */
	BigDecimal getValorDesconto();

	/**
	 * Sets the valor desconto.
	 *
	 * @param valorDesconto the new valor desconto
	 */
	void setValorDesconto(BigDecimal valorDesconto);

	/**
	 * Gets o convênio arrecadador
	 * @return o convênio arrecadador
	 */
	String getConvenioArrecadador();

	/**
	 * Sets o convênio arrecadador
	 * @param convenioArrecadador o convênio arrecadador
	 */
	void setConvenioArrecadador(String convenioArrecadador);

	/**
	 * Gets o código do banco do cliente
	 * @return o código do banco do cliente
	 */
	String getBancoCliente();

	/**
	 * Sets o código do banco do cliente
	 * @param bancoCliente o código do banco do cliente
	 */
	void setBancoCliente(String bancoCliente);

	/**
	 * Gets a agência do cliente
	 * @return a agência do cliente
	 */
	String getAgenciaCliente();

	/**
	 * Sets a agência do cliente
	 * @param agenciaCliente a agência do cliente
	 */
	void setAgenciaCliente(String agenciaCliente);

	/**
	 * Gets a conta corrente do cliente
	 * @return a conta corrente do cliente
	 */
	String getContaCliente();

	/**
	 * Sets a conta corrente do cliente
	 * @param contaCliente a conta corrente do cliente
	 */
	void setContaCliente(String contaCliente);
}
