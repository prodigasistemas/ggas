/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 12/09/2013 13:54:17
 @author wcosta
 */

package br.com.ggas.integracao.geral.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.geral.IntegracaoLancamentoContabil;

/**
 * Classe de Integração do Lançamento Contábil
 *
 */
public class IntegracaoLancamentoContabilImpl extends IntegracaoImpl implements IntegracaoLancamentoContabil {

	private static final long serialVersionUID = 1581169913844755373L;

	private Long eventoComercial;

	private Long lancamentoItemContabil;

	private Long segmento;

	private String tipoLancamento;

	private Long tributo;

	private String contaAuxiliar;

	private String historico;

	private String numeroConta;

	private String descricaoConta;

	private BigDecimal valor;

	private Long lancamentoContabilSintetico;

	private Date dataContabil;

	@Override
	public Long getEventoComercial() {

		return eventoComercial;
	}

	@Override
	public void setEventoComercial(Long eventoComercial) {

		this.eventoComercial = eventoComercial;
	}

	@Override
	public Long getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	@Override
	public void setLancamentoItemContabil(Long lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	@Override
	public Long getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Long segmento) {

		this.segmento = segmento;
	}

	@Override
	public String getTipoLancamento() {

		return tipoLancamento;
	}

	@Override
	public void setTipoLancamento(String tipoLancamento) {

		this.tipoLancamento = tipoLancamento;
	}

	@Override
	public Long getTributo() {

		return tributo;
	}

	@Override
	public void setTributo(Long tributo) {

		this.tributo = tributo;
	}

	@Override
	public String getContaAuxiliar() {

		return contaAuxiliar;
	}

	@Override
	public void setContaAuxiliar(String contaAuxiliar) {

		this.contaAuxiliar = contaAuxiliar;
	}

	@Override
	public String getHistorico() {

		return historico;
	}

	@Override
	public void setHistorico(String historico) {

		this.historico = historico;
	}

	@Override
	public String getNumeroConta() {

		return numeroConta;
	}

	@Override
	public void setNumeroConta(String numeroConta) {

		this.numeroConta = numeroConta;
	}

	@Override
	public String getDescricaoConta() {

		return descricaoConta;
	}

	@Override
	public void setDescricaoConta(String descricaoConta) {

		this.descricaoConta = descricaoConta;
	}

	@Override
	public BigDecimal getValor() {

		return valor;
	}

	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	@Override
	public Long getLancamentoContabilSintetico() {

		return lancamentoContabilSintetico;
	}

	@Override
	public void setLancamentoContabilSintetico(Long lancamentoContabilSintetico) {

		this.lancamentoContabilSintetico = lancamentoContabilSintetico;
	}

	@Override
	public Date getDataContabil() {
		Date data = null;
		if (this.dataContabil != null) {
			data = (Date) dataContabil.clone();
		}
		return data;
	}

	@Override
	public void setDataContabil(Date dataContabil) {
		if(dataContabil != null) {
			this.dataContabil = (Date) dataContabil.clone();
		} else {
			this.dataContabil = null;
		}
	}

}
