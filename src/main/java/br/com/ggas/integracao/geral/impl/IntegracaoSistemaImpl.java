/*
 Copyright (C) <2011> GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este arquivo Ã© parte do GGAS, um sistema de gestÃ£o comercial de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 Este programa Ã© um software livre; vocÃª pode redistribuÃ­-lo e/ou
 modificÃ¡-lo sob os termos de LicenÃ§a PÃºblica Geral GNU, conforme
 publicada pela Free Software Foundation; versÃ£o 2 da LicenÃ§a.

 O GGAS Ã© distribuÃ­do na expectativa de ser Ãºtil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implÃ­cita de
 COMERCIALIZAÃ‡ÃƒO ou de ADEQUAÃ‡ÃƒO A QUALQUER PROPÃ“SITO EM PARTICULAR.
 Consulte a LicenÃ§a PÃºblica Geral GNU para obter mais detalhes.

 VocÃª deve ter recebido uma cÃ³pia da LicenÃ§a PÃºblica Geral GNU
 junto com este programa; se nÃ£o, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS â€“ Sistema de GestÃ£o Comercial (Billing) de ServiÃ§os de DistribuiÃ§Ã£o de GÃ¡s

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place â€“ Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:10:20
 @author vtavares
 */

package br.com.ggas.integracao.geral.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.util.Constantes;

/**
 * Classe da Entidade de Integração Sistema 
 * 
 *
 */
public class IntegracaoSistemaImpl extends EntidadeNegocioImpl implements IntegracaoSistema {

	private static final int LIMITE_SIGLA = 3;

	/**
	 * 
	 */
	private static final long serialVersionUID = -7983964813569591039L;

	private String nome;

	private String sigla;

	private String descricao;

	@Override
	public String getNome() {

		return nome;
	}

	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getSigla() {

		return sigla;
	}

	@Override
	public void setSigla(String sigla) {

		this.sigla = sigla;
	}

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(nome)) {
			stringBuilder.append(NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(sigla) || sigla.length() > LIMITE_SIGLA) {
			stringBuilder.append(SIGLA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			if(StringUtils.isEmpty(nome) || StringUtils.isEmpty(sigla)) {
				erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
								camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
			} else if(sigla.length() > 3) {
				erros.put(Constantes.ERRO_NEGOCIO_CAMPO_SIGLA_MAXIMO_3,
								camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
			}

		}

		return erros;
	}

}
