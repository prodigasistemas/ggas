/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/01/2013 17:32:04
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.LancamentoContabilSintetico;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.integracao.bens.IntegracaoBemMovimentacao;
import br.com.ggas.integracao.bens.impl.IntegracaoBemMedidorImpl;
import br.com.ggas.integracao.bens.impl.IntegracaoBemVazaoCorretorImpl;
import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.cliente.impl.IntegracaoClienteImpl;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.impl.IntegracaoNotaFiscalImpl;
import br.com.ggas.integracao.titulos.IntegracaoTitulo;
import br.com.ggas.integracao.titulos.IntegracaoTituloReceber;
import br.com.ggas.integracao.titulos.impl.IntegracaoTituloImpl;
import br.com.ggas.parametrosistema.ParametroSistema;
import org.apache.commons.collections.map.MultiKeyMap;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Interface com os métodos 
 * para dados nas tabelas de integração 
 *
 */
public interface ControladorIntegracao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_INTEGRACAO = "controladorIntegracao";

	String ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO = "ERRO_NEGOCIO_VALOR_DIFERENTE_ESPECIFICADO";

	public static final String INTEGRACAO_SISTEMA = "INTEGRACAO_SISTEMA";

	String ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS = "ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS";

	String ERRO_NEGOCIO_EMAIL_INVALIDO = "ERRO_NEGOCIO_EMAIL_INVALIDO";

	String ERRO_NEGOCIO_INTEGRACAO_INCLUSAO = "ERRO_NEGOCIO_INTEGRACAO_INCLUSAO";

	String BEAN_ID_INTEGRACAO_SISTEMA_FUNCAO = "integracaoSistemaFuncao";

	/**
	 * Método responsável por criar uma
	 * Integracao Funcao Tabela.
	 * return IntegracaoFuncaoTabela
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarIntegracaoFuncaoTabela();

	/**
	 * Listar funcao.
	 *
	 * @return the collection
	 */
	Collection<IntegracaoFuncao> listarFuncao();

	/**
	 * Listar funcao por sistema.
	 *
	 * @param chavePrimariaSistema
	 *            the chave primaria sistema
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoSistemaFuncao> listarFuncaoPorSistema(Long chavePrimariaSistema) throws NegocioException;

	/**
	 * Listar tabelas.
	 *
	 * @param idFuncao
	 *            the id funcao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoFuncaoTabela> listarTabelas(Long idFuncao) throws NegocioException;

	/**
	 * Listar colunas tabela.
	 *
	 * @param chavePrimariaTabela
	 *            the chave primaria tabela
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Coluna> listarColunasTabela(Long chavePrimariaTabela) throws NegocioException;

	/**
	 * Listar classe.
	 *
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<?> listarClasse() throws NegocioException;

	/**
	 * Listar tabela colunas.
	 *
	 * @param chavePrimariaTabela
	 *            the chave primaria tabela
	 * @return the collection
	 * @throws NegocioException the Negocio Exception
	 */
	Collection<Tabela> listarTabelaColunas(Long chavePrimariaTabela) throws NegocioException;

	/**
	 * Listar conteudo tabela coluna.
	 *
	 * @param tabela {@link Tabela}
	 * @param listaFiltros {@link List}
	 * @return List retorna uma lista com o conteudo da tabela
	 * @throws ClassNotFoundException {@link ClassNotFoundException} 
	 * @throws NegocioException {@link NegocioException}
	 * @throws Exception {@link Exception}
	 */
	List<Object> listarConteudoTabelaColuna(Tabela tabela, List<AcompanhamentoIntegracaoVO> listaFiltros) throws NegocioException;

	/**
	 * Listar conteudo tabela coluna situacao erro.
	 *
	 * @param tabela
	 *            the tabela
	 * @param listaFiltros
	 *            the lista filtros
	 * @param situacaoErro
	 *            the situacao erro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public List<Object> listarConteudoTabelaColunaSituacaoErro(Tabela tabela, List<AcompanhamentoIntegracaoVO> listaFiltros,
					Long situacaoErro) throws NegocioException;

	/**
	 * Método responsável por listar os
	 * sitemas de integração.
	 *
	 * @return coleção de integração do
	 *         sistema.
	 */
	Collection<IntegracaoSistema> listarIntegracaoSistema();

	/**
	 * Método responsável por listar as
	 * funções de integração.
	 *
	 * @return coleção de funções de integração do
	 *         sistema.
	 */
	Collection<IntegracaoFuncao> listarIntegracaoFuncao();

	/**
	 * Método responsável por listar as
	 * funções de sitemas de integração.
	 *
	 * @param filtro
	 *            the filtro
	 * @return coleção de funções de integração do
	 *         sistema.
	 */
	Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao(Map<String, Object> filtro);

	/**
	 * Método responsável por listar as
	 * funções de sitemas de integração.
	 *
	 * @return coleção de funções de integração do
	 *         sistema.
	 */
	Collection<IntegracaoFuncaoTabela> listarIntegracaoFuncaoTabela();

	/**
	 * Método responsável por criar os
	 * sitemas de integração.
	 *
	 * @return coleção de integração do
	 *         sistema.
	 */
	EntidadeNegocio criarIntegracaoSistema();

	/**
	 * Método responsável por criar as
	 * funções de sitemas de integração.
	 *
	 * @return coleção de funções de integração do
	 *         sistema.
	 */
	EntidadeNegocio criarIntegracaoFuncao();

	/**
	 * Criar integracao sistema funcao.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarIntegracaoSistemaFuncao();

	/**
	 * Criar integracao sistema parametro.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarIntegracaoSistemaParametro();

	/**
	 * Método responsável por consultar os
	 * sitemas de integração.
	 *
	 * @param filtro
	 *            the filtro
	 * @return coleção de integração do
	 *         sistema.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoSistema> consultarIntegracaoSistema(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * funções de um sitema de integração.
	 *
	 * @param filtro
	 *            the filtro
	 * @return coleção de funções de integração do
	 *         sistema.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoSistemaFuncao> consultarIntegracaoSistemaFuncao(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * funções.
	 *
	 * @param filtro
	 *            the filtro
	 * @return coleção de funções de integração do
	 *         sistema.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoFuncao> consultarIntegracaoFuncao(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por remover os
	 * uma integração do sistema à funçao.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void removerSistemaFuncao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Obter integracao sistema.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the integracao sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntegracaoSistema obterIntegracaoSistema(long chavePrimaria) throws NegocioException;

	/**
	 * Obter integracao funcao tabela.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoFuncaoTabela> obterIntegracaoFuncaoTabela(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por atualizar um
	 * sistema integrante.
	 *
	 * @param integracaoSistema
	 *            the integracao sistema
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarIntegracaoSistema(IntegracaoSistema integracaoSistema) throws NegocioException, ConcorrenciaException;

	/**
	 * Metodo responsavel por excluir um sistema integrante.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void removerIntegracaoSistema(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * Verificar sistema integrante existente.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarSistemaIntegranteExistente(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Verificar existencia integracoes ativas.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarExistenciaIntegracoesAtivas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Verificar existencia integracoes relacionadas.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarExistenciaIntegracoesRelacionadas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Validar dados sistema integrante.
	 *
	 * @param integracaoSistema
	 *            the integracao sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosSistemaIntegrante(IntegracaoSistema integracaoSistema) throws NegocioException;

	/**
	 * Validar dados integracao sistema parametro.
	 *
	 * @param integracaoParametro
	 *            the integracao parametro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosIntegracaoSistemaParametro(IntegracaoParametro integracaoParametro) throws NegocioException;

	/**
	 * Inserir integracao sistema parametro.
	 *
	 * @param integracaoSistemaParametro
	 *            the integracao sistema parametro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirIntegracaoSistemaParametro(IntegracaoSistemaParametro integracaoSistemaParametro) throws NegocioException;

	/**
	 * Inserir integracao sistema mapeamento.
	 *
	 * @param integracaoSistemaMapeamento
	 *            the integracao sistema mapeamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirIntegracaoSistemaMapeamento(IntegracaoMapeamento integracaoSistemaMapeamento) throws NegocioException;

	/**
	 * Método responsável por listas os sistemas integrantes
	 * relacionados com alguma funcao.
	 *
	 * @param integracaoSistema
	 *            the integracao sistema
	 * @return coleção de Sistemas FUncoes.
	 */
	Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncao(IntegracaoSistema integracaoSistema);

	/**
	 * Listar parametros associados.
	 *
	 * @param integracaoFuncao
	 *            the integracao funcao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoParametro> listarParametrosAssociados(
					IntegracaoFuncao integracaoFuncao) throws NegocioException;

	/**
	 * Listar integracao sistema parametro.
	 *
	 * @param integracaoSistema the integracao sistema
	 * @param parametroSistema the parametro sistema
	 * @return the collection
	 * @throws NegocioException {@link NegocioException}
	 */
	IntegracaoSistemaParametro obterIntegracaoSistemaParametro(
					IntegracaoSistema integracaoSistema,
					ParametroSistema parametroSistema) throws NegocioException;

	/**
	 * Listar integracao sistema parametro.
	 *
	 * @param siglaSistema sigla do sistema integrante
	 * @param parametroSistema {@link ParametroSistema}
	 * @return IntegracaoSistemaParametro {@link IntegracaoSistemaParametro}
	 * @throws NegocioException  {@link NegocioException}
	 */
	IntegracaoSistemaParametro obterIntegracaoSistemaParametro(String siglaSistema,
					ParametroSistema parametroSistema) throws NegocioException;

	/**
	 * Listar mapeamentos associados.
	 *
	 * @param integracaoFuncao
	 *            the integracao funcao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoFuncaoTabela> listarMapeamentosAssociados(IntegracaoFuncao integracaoFuncao) throws NegocioException;

	/**
	 * Obter integracao funcao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the integracao funcao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntegracaoFuncao obterIntegracaoFuncao(long chavePrimaria) throws NegocioException;

	/**
	 * Obter integracao sistema funcao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the integracao sistema funcao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(long chavePrimaria) throws NegocioException;

	/**
	 * Obter integracao sistema funcao.
	 * 
	 * @param siglaIntegracao - {@link String}
	 * @return the integracao sistema funcao
	 * @throws NegocioException the negocio exception
	 */
	IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(String siglaIntegracao) throws NegocioException;

	/**
	 * Obter integracao sistema funcao.
	 *
	 * @param chaveSistema
	 *            the chave sistema
	 * @param chaveFuncao
	 *            the chave funcao
	 * @return the integracao sistema funcao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntegracaoSistemaFuncao obterIntegracaoSistemaFuncao(long chaveSistema, long chaveFuncao) throws NegocioException;

	/**
	 * Atualizar acompanhar integracao.
	 *
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @param classe
	 *            the classe
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarAcompanharIntegracao(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException, ConcorrenciaException;

	/**
	 * Obter funcao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<IntegracaoFuncao> obterFuncao(Long chavePrimaria);

	/**
	 * Consulta sistema funcao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the integracao sistema funcao
	 */
	IntegracaoSistemaFuncao consultaSistemaFuncao(Long chavePrimaria);

	/**
	 * Listar integracao funcao tabela.
	 *
	 * @param listaIntegracaoSistemaFuncao
	 *            the lista integracao sistema funcao
	 * @param idIntegracaoFuncao
	 *            the id integracao funcao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoFuncaoTabela> listarIntegracaoFuncaoTabela(Collection<IntegracaoSistemaFuncao> listaIntegracaoSistemaFuncao,
					Long idIntegracaoFuncao) throws NegocioException;

	/**
	 * Listar integracao mapeamento.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoMapeamento> listarIntegracaoMapeamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Exportar tabela integracao para csv.
	 *
	 * @param listaAcompanhamentoIntegracaoVO
	 *            the lista acompanhamento integracao vo
	 * @param separador
	 *            the separador
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public byte[] exportarTabelaIntegracaoParaCSV(List<AcompanhamentoIntegracaoVO[]> listaAcompanhamentoIntegracaoVO, String separador)
					throws NegocioException;

	/**
	 * Localizar integracao mapeamento.
	 *
	 * @param idIntegracaoSistemaFuncao
	 *            the id integracao sistema funcao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntegracaoMapeamento> localizarIntegracaoMapeamento(Long idIntegracaoSistemaFuncao) throws NegocioException;

	/**
	 * Atualizar integracao sistema parametro.
	 *
	 * @param integracaoSistemaParametro
	 *            the integracao sistema parametro
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void atualizarIntegracaoSistemaParametro(IntegracaoSistemaParametro integracaoSistemaParametro) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Validar destinatarios email.
	 *
	 * @param integracaoSistemaFuncao
	 *            the integracao sistema funcao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDestinatariosEmail(IntegracaoSistemaFuncao integracaoSistemaFuncao) throws NegocioException;

	/**
	 * Remover integracao mapeamento.
	 *
	 * @param listaIntegracaoMapeamento
	 *            the lista integracao mapeamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerIntegracaoMapeamento(Collection<IntegracaoMapeamento> listaIntegracaoMapeamento) throws NegocioException;

	/**
	 * Consultar integracao sistema funcao.
	 *
	 * @param integracaoFuncao
	 *            the integracao funcao
	 * @return the collection
	 */
	Collection<IntegracaoSistema> consultarIntegracaoSistemaFuncao(Long integracaoFuncao);

	/**
	 * Consultar integracao.
	 *
	 * @param filtro
	 *            the filtro
	 * @param classe
	 *            the classe
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 */
	Collection<Class<?>> consultarIntegracao(Map<String, Object> filtro, Class<?> classe, String ordenacao, String... propriedadesLazy);

	/**
	 * Gerar lista sistemas integrantes por funcao.
	 *
	 * @param chaveFuncao
	 *            the chave funcao
	 * @return the list
	 */
	List<String> gerarListaSistemasIntegrantesPorFuncao(Long chaveFuncao);

	/**
	 * Atualizar integracao com erro.
	 *
	 * @param classe
	 *            the classe
	 * @param integracao
	 *            the integracao
	 * @param descricaoErro
	 *            the descricao erro
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarIntegracaoComErro(Class<?> classe, Integracao integracao, String descricaoErro) throws NegocioException,
					ConcorrenciaException;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#validarExistenciaEntidades(br.com.ggas.geral.negocio.EntidadeNegocio,
	 * br.com.ggas.geral.negocio.EntidadeNegocio, java.util.Map)
	 */
	@Override
	String validarExistenciaEntidades(EntidadeNegocio classeOrigem, EntidadeNegocio objetoValor, Map<String, Object[]> mapaInconsistencias)
					throws NegocioException;

	/**
	 * Registrar movimentacao bens.
	 *
	 * @param listaIntegracaoBemMovimentacao
	 *            the lista integracao bem movimentacao
	 * @param controladorConstanteSistema
	 *            the controlador constante sistema
	 * @param processo
	 *            the processo
	 * @param logProcessamento
	 *            the log processamento
	 * @param listaSistemasIntegrantes
	 *            the lista sistemas integrantes
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 */
	void registrarMovimentacaoBens(Collection<IntegracaoBemMovimentacao> listaIntegracaoBemMovimentacao,
					ControladorConstanteSistema controladorConstanteSistema, Processo processo, StringBuilder logProcessamento,
					List<String> listaSistemasIntegrantes) throws NegocioException, ConcorrenciaException, IOException,
					IllegalAccessException, InvocationTargetException, NoSuchMethodException;

	/**
	 * Método responsável por realizar todas as validações para a inserção de um novo Medidor a partir da integração ERP x GGAS. Caso não
	 * ocorra
	 * violações, um novo Medidor é criado, caso contrário, as violações são inseridas no registro de integração (tabela TI_BEM)
	 *
	 * @param integracaoBemMedidor
	 *            - objeto com os valores para a criação de um novo Medidor.
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 */
	boolean validarInsercaoMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Map<String, Object[]> mapaInconsistencias)
					throws GGASException;

	/**
	 * Método responsável por realizar todas as validações para a alteração de um Medidor a partir da integração ERP x GGAS. Caso não ocorra
	 * violações, o Medidor é alterado com os novos valores, caso contrário, as violações são inseridas no campo de erro do registro de
	 * integração
	 * (tabela TI_BEM)
	 *
	 * @param integracaoBemMedidor
	 *            - objeto com os valores a serem alterados no Medidor.
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 */
	boolean validarAlteracaoMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Map<String, Object[]> mapaInconsistencias)
					throws GGASException;

	/**
	 * Método responsável por realizar todas as validações para a exclusão, física ou lógica, de um Medidor, a partir da integração ERP x
	 * GGAS. Caso
	 * não ocorra violações, o Medidor é excluído, caso contrário, as violações são inseridas no campo de erro do registro de integração
	 * (Tabela
	 * TI_BEM)
	 *
	 * @param integracaoBemMedidor
	 *            - objeto com o identificado do Medidor a ser excluído.
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	boolean validarExclusaoMedidor(IntegracaoBemMedidorImpl integracaoBemMedidor, Map<String, Object[]> mapaInconsistencias)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Validar insercao vazao corretor.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 */
	boolean validarInsercaoVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws GGASException,
					IllegalAccessException, InvocationTargetException, NoSuchMethodException;

	/**
	 * Validar alteracao vazao corretor.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 */
	boolean validarAlteracaoVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws GGASException,
					IllegalAccessException, InvocationTargetException, NoSuchMethodException;

	/**
	 * Validar exclusao vazao corretor.
	 *
	 * @param integracaoBemVazaoCorretor
	 *            the integracao bem vazao corretor
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	boolean validarExclusaoVazaoCorretor(IntegracaoBemVazaoCorretorImpl integracaoBemVazaoCorretor,
					Map<String, Object[]> mapaInconsistencias) throws NegocioException, ConcorrenciaException;

	/**
	 * Gerar movimentacao medidor.
	 *
	 * @param idMedidor
	 *            the id medidor
	 * @param idMedidorOperacaoHistorico
	 *            the id medidor operacao historico
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param instalacao
	 *            the instalacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void gerarMovimentacaoMedidor(Long idMedidor, Long idMedidorOperacaoHistorico, PontoConsumo pontoConsumo, boolean instalacao)
					throws NegocioException;

	/**
	 * Gerar movimentacao vazao corretor.
	 *
	 * @param idVazaoCorretor
	 *            the id vazao corretor
	 * @param idVazaoCorretorOperacaoHistorico
	 *            the id vazao corretor operacao historico
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param instalacao
	 *            the instalacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void gerarMovimentacaoVazaoCorretor(Long idVazaoCorretor, Long idVazaoCorretorOperacaoHistorico, PontoConsumo pontoConsumo,
					boolean instalacao) throws NegocioException;

	/**
	 * Inserir integracao nota fiscal.
	 *
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param ehEmitenteProprio
	 *            the eh emitente proprio
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param logProcessamento log
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void inserirIntegracaoNotaFiscal(DocumentoFiscal documentoFiscal, Boolean ehEmitenteProprio,
			DadosAuditoria dadosAuditoria, StringBuilder logProcessamento) throws GGASException;

	/**
	 * Atualizar integracao nota fiscal.
	 *
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarIntegracaoNotaFiscal(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Preencher log mapa inconsistencias.
	 *
	 * @param logProcessamento {@link StringBuilder}
	 * @param resumoIntegracaoVOCorretor {@link ResumoIntegracaoVO}
	 * @param logCadastroBens {@link boolean}
	 * @throws NegocioException  {@link NegocioException}
	 * @throws IOException  {@link IOException}
	 *             Signals that an I/O exception has occurred.
	 */
	void preencherLogMapaInconsistencias(StringBuilder logProcessamento,
					ResumoIntegracaoVO resumoIntegracaoVOCorretor, boolean logCadastroBens)
									throws NegocioException, IOException;

	/**
	 * Preencher log mapa inconsistencias.
	 *
	 * @param logProcessamento
	 *            the log processamento
	 * @param resumoIntegracaoVOCorretor
	 *            the resumo integracao vo corretor
	 * @param logCadastroBens
	 *            the logCadastroBens
	 * @param detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico
	 * 			  descricao das inconsistencias
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	void preencherLogMapaInconsistencias(StringBuilder logProcessamento,
					ResumoIntegracaoVO resumoIntegracaoVOCorretor, boolean logCadastroBens,
					String detalhesInconsistenciasContaCreditoDebitoLancamentoSintetico)
					throws NegocioException, IOException;

	/**
	 * Desabilitar recursos sistema.
	 *
	 * @param habilitado
	 *            the habilitado
	 * @param chavePrimariaIntegSistFuncao
	 *            the chave primaria integ sist funcao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void desabilitarRecursosSistema(boolean habilitado, long chavePrimariaIntegSistFuncao) throws NegocioException;

	/**
	 * Buscar email destinatario integracao.
	 *
	 * @param chaveFuncao
	 *            the chave funcao
	 * @return the string
	 */
	String buscarEmailDestinatarioIntegracao(Long chaveFuncao);

	/**
	 * Obter integracao sistema funcao situacao erro.
	 *
	 * @param intFunTabela
	 *            the int fun tabela
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public boolean obterIntegracaoSistemaFuncaoSituacaoErro(IntegracaoFuncaoTabela intFunTabela) throws GGASException;

	/**
	 * Obter entidade por classe.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param nomeClasse
	 *            the nome classe
	 * @return the object
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Object obterEntidadePorClasse(Long chavePrimaria, Class<?> nomeClasse) throws NegocioException;

	/**
	 * Inserir integracao titulo pagar.
	 *
	 * @param fatura
	 *            the fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void inserirIntegracaoTituloPagar(Fatura fatura, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Cancelar titulo pagar.
	 *
	 * @param fatura
	 *            the fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void cancelarTituloPagar(Fatura fatura, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Inserir integracao cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserirIntegracaoCliente(Cliente cliente) throws NegocioException;

	/**
	 * Inserir integracao contrato
	 * @param contrato O Contrato
	 * @throws GGASException the GGASException
	 */
	void inserirIntegracaoContrato(Contrato contrato) throws GGASException;

	/**
	 * Exclui registros de integração contrato baseado nos pontos de consumo removidos do contrato
	 * @param contrato O contrato alterado
	 * @param pontosRemovidos Os pontos de consumo removidos do contrato
	 * @throws GGASException the GGASException
	 */
	void excluirIntegracaoContratoPorPontoConsumo(Contrato contrato, List<ContratoPontoConsumo> pontosRemovidos) throws GGASException;

	/**
	 * Realiza a alteração do titular na IntegracaoContrato desativando o contrato antigo e criando um novo
	 * @param contrato O contrato alterado
	 * @param contratoAntigo O contrato a partir do qual foi feita a alteração
	 * @throws GGASException the GGASException
	 */
	void alterarTitularDeContratoAtivo(Contrato contrato, Contrato contratoAntigo) throws GGASException;

	/**
	 * Atualiza as informações do cliente para integração de contrato
	 * @param cliente O cliente alterado
	 * @throws NegocioException Quando existem integrações não processadas para esse cliente
	 */
	void atualizarClienteIntegracaoContrato(Cliente cliente) throws NegocioException;

	/**
	 * Atualiza as informações de ponto de consumo para a integração de contrato
	 * @param pontoConsumo O ponto de consumo alterado
	 * @throws NegocioException Quando existem integrações não processadas para esse ponto de consumo
	 */
	void atualizarPontoConsumoIntegracaoContrato(PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Atualizar integracao cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarIntegracaoCliente(Cliente cliente) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover integracao cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void removerIntegracaoCliente(Cliente cliente) throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar integracao titulo.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<?> consultarIntegracaoTitulo(Map<String, Object> filtro);

	/**
	 * Validar atualizacao titulo.
	 *
	 * @param integracao
	 *            the integracao
	 * @param mapaInconsistenciasTitulo
	 *            the mapa inconsistencias titulo
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 */
	boolean validarAtualizacaoTitulo(IntegracaoTituloImpl integracao, Map<String, Object[]> mapaInconsistenciasTitulo) throws GGASException;

	/**
	 * Gerar baixa titulo.
	 *
	 * @param recebimento {@link Recebimento}
	 * @param estorno {@link boolean}
	 * @param somaPagosParcial {@link BigDecimal}
	 * @throws GGASException {@link GGASException}
	 */
	void gerarBaixaTitulo(Recebimento recebimento, boolean estorno, BigDecimal somaPagosParcial) throws GGASException;

	/**
	 * Inserir integracao titulo receber.
	 *
	 * @param fatura - {@link Fatura}
	 * @param listaIntegracaoSistemaTituloReceber - {@link List}
	 * @param documentosPorFaturaGeral - {@link Map}
	 * @param mapeamentosPorIntegrantes - {@link MultiKeyMap}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public void inserirIntegracaoTituloReceber(Fatura fatura, List<String> listaIntegracaoSistemaTituloReceber,
			Map<Long, Collection<DocumentoCobrancaItem>> documentosPorFaturaGeral, MultiKeyMap mapeamentosPorIntegrantes)
			throws NegocioException;

	/**
	 * Buscar integracao cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @return the integracao cliente
	 */
	public IntegracaoCliente buscarIntegracaoCliente(Cliente cliente);

	/**
	 * Buscar titulo por fatura.
	 *
	 * @param chaveFaturaGeral
	 *            the chave fatura geral
	 * @param tituloReceber
	 *            the titulo receber
	 * @param sistemaIntegrante
	 *            the sistema integrante
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long buscarTituloPorFatura(Long chaveFaturaGeral, boolean tituloReceber, String sistemaIntegrante) throws NegocioException;

	/**
	 * Consultar titulo baixa
	 * 
	 * @param filtro - {@link Map}
	 * @param ordenacao - {@link String}
	 * @param propriedadesLazy - {@link String}
	 * @return colecao integracao titulo
	 * @throws NegocioException the negocio exception
	 */
	Collection<IntegracaoTitulo> consultarTituloBaixa(Map<String, Object> filtro, String ordenacao, String... propriedadesLazy)
					throws NegocioException;

	/**
	 * Cancelar titulo receber.
	 *
	 * @param fatura
	 *            the fatura
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void cancelarTituloReceber(Fatura fatura) throws NegocioException, ConcorrenciaException;

	/**
	 * Gerar baixa titulo por fatura.
	 *
	 * @param fatura
	 *            the fatura
	 * @param valorConciliado
	 *            the valor conciliado
	 * @param notaDebito
	 *            the nota debito
	 * @param faturaConciliacao
	 *            the fatura conciliacao
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void gerarBaixaTituloPorFatura(Fatura fatura, BigDecimal valorConciliado, boolean notaDebito, Fatura faturaConciliacao)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Validar processamento lancamento.
	 *
	 * @param lancamentoContabilSintetico
	 *            the lancamento contabil sintetico
	 * @param mapaInconsistenciasLancamento
	 *            the mapa inconsistencias lancamento
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	boolean validarProcessamentoLancamento(LancamentoContabilSintetico lancamentoContabilSintetico,
					Map<String, Object[]> mapaInconsistenciasLancamento) throws NegocioException, ConcorrenciaException;

	/**
	 * Validar atualizacao cliente.
	 *
	 * @param integracaoCliente
	 *            the integracao cliente
	 * @param mapaInconsistenciasCliente
	 *            the mapa inconsistencias cliente
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 */
	boolean validarAtualizacaoCliente(IntegracaoClienteImpl integracaoCliente, Map<String, Object[]> mapaInconsistenciasCliente)
					throws GGASException;

	/**
	 * Criar integracao titulo.
	 *
	 * @return the integracao titulo
	 */
	IntegracaoTitulo criarIntegracaoTitulo();

	/**
	 * Obter integracao titulo receber.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the integracao titulo receber
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntegracaoTituloReceber obterIntegracaoTituloReceber(Long chavePrimaria) throws NegocioException;

	/**
	 * Reintegrar clientes.
	 *@param StringBuilder
	 *            log de processamento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException 
	 * @throws Exception 
	 */
	void reintegrarClientes(StringBuilder logProcessamento) throws NegocioException, ConcorrenciaException, GGASException, Exception;

	/**
	 * Atualizar integracao fatura.
	 *
	 * @param fatura
	 *            the fatura
	 * @param tituloReceber
	 *            the titulo receber
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarIntegracaoFatura(Fatura fatura, boolean tituloReceber) throws NegocioException, ConcorrenciaException;

	/**
	 * Preencher codigo situacao tributaria.
	 *
	 * @param integracaoNotaFiscalItem
	 *            the integracao nota fiscal item
	 * @param faturaItem
	 *            the fatura item
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void preencherCodigoSituacaoTributaria(IntegracaoNotaFiscalItem integracaoNotaFiscalItem, FaturaItem faturaItem,
					DocumentoFiscal documentoFiscal) throws NegocioException;

	/**
	 * Consultar integracao nota fiscal.
	 *
	 * @param fatura
	 *            the fatura
	 * @return the collection
	 */
	Collection<IntegracaoNotaFiscal> consultarIntegracaoNotaFiscal(Fatura fatura);

	/**
	 * Consultar integracao titulo receber.
	 *
	 * @param fatura the fatura
	 * @return the collection
	 */
	Collection<IntegracaoTituloReceber> consultarIntegracaoTituloReceber(Fatura fatura);

	/**
	 * Listar integracao sistema funcao cadastro de bens.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<IntegracaoSistemaFuncao> listarIntegracaoSistemaFuncaoCadastroBens()
			throws NegocioException;

	/**
	 * Método responsável por criar e retornar uma nova IntegracaoMapeamento.
	 *
	 * @return integracao mapeamento
	 */
	IntegracaoMapeamento criarIntegracaoMapeamento();

	/**
	 * Método responsável por validar a partir da chave primaria do imovel a integracao do contrato a ele associado.
	 *
	 * @param idImovel
	 *            the imovel
	 * @return boolean
	 */
	Boolean validarIntegracaoContratoImovel(Long idImovel);

	/**
	 * Método responsável por validar a integração do contrato a partir do código do contrato.
	 *
	 * @param idContrato - {@link Long}
	 * @param idContratoPai - {@link Long}
	 *
	 * @return boolean - {@link Boolean}
	 * @exception NegocioException the negocio exception
	 */
	Boolean validarIntegracaoContrato(Long idContrato, Long idContratoPai) throws NegocioException;

	/**
	 * Método responsável por validar a partir da chave primaria do cliente a integracao do contrato a ele associado.
	 *
	 * @param idCliente - {@link Long}
	 *
	 * @return boolean - {@link Boolean}
	 * @exception NegocioException - the negocio exception
	 */
	Boolean validarIntegracaoContratoCliente(Long idCliente)throws NegocioException;

	/**
	 * Consulta os mapeamentos dos sistemas integrantes, a partir das siglas dos sistemas 
	 * passadas por parâmetro. Retorna um mapa com valor igual ao código da entidade, do 
	 * tipo {@link String}, no sistema integrante e com chave composta da sigla do sistema 
	 * integrante, do código da entidade no sistema GGAS e do nome da classe mapeada.
	 *
	 * @param siglasSistemasIntegrantes - {@link List}
	 * @return {@link MultiKeyMap}
	 */
	MultiKeyMap consultarMapeamentosPorSistemasIntegrantes(List<String> siglasSistemasIntegrantes);

	/**
	 * Obtém o código de uma entidade em um sistema integrante a partir dos mapeamentos dos sistemas integrantes,
	 * indexados por sigla, nome da entidade mapeada e código da entidade. Caso não exista mapeamento, retorna
	 * o código da entidade no sistema GGAS.
	 * @param mapeamentosPorIntegrantes - {@link MultiKeyMap}
	 * @param entidadeNegocio - {@link EntidadeNegocio}
	 * @param integracaoSistema - {@link IntegracaoSistema}
	 * @return codigoEntidadeSistemaIntegrante - {@link Long}
	 */
	Long getCodigoEntidadeSistemaIntegrante(MultiKeyMap mapeamentosPorIntegrantes, EntidadeNegocio entidadeNegocio,
					IntegracaoSistema integracaoSistema);

	/**
	 * Atualiza os campos NFE_SIS_STATUS; NFE_NR_PROT_ENV; NFE_NR_CHAVE_DADOS; NFE_SIS_OPERACAO
	 * da tabela NFE com os dados da tabela TI_NOTA_FISCAL dos campos: 
	 * TINF_NR_CHAVE; TINF_NR_PROTOCOLO; TINF_CD_OPERACAO; TINF_CD_SITUACAO
	 *
	 * @param dataEmissao {@link String}
	 * @throws GGASException {@link GGASException}
	 */	
	void atualizarCamposNFE(String dataEmissao) 
			throws GGASException;

	/**
	 * Consulta a integração da nota fiscal pelo tipo de operação da NFE
	 *
	 * @param codigoTipoOperacao {@link String}
	 * @param codigoSituacao {@link String}
	 * @param numero {@link Integer}
	 * @return IntegracaoNotaFiscalImpl {@link IntegracaoNotaFiscalImpl}
	 * @throws NegocioException {@link NegocioException}
	 */
	IntegracaoNotaFiscalImpl consultarIntegracaoNotaFiscalTipoOperacaoNfe(String codigoTipoOperacao, String codigoSituacao, Integer numero)
			throws NegocioException;

	/**
	 * Verifica se a referência para integração de nota e título está configurada como contrato
	 * @return boolean - {@link boolean}
	 */
	boolean integracaoPrecedenteContratoAtiva();

	/**
	 * Verifica se existe alguma integração de contrato que já foi processada pelo sistema destino
	 * @return True se houver registro integrado e false caso contrário
	 */
	boolean existeIntegracaoContratoProcessada();

	/**
	 * Verifica se existe alguma integração de cliente que já foi processada pelo sistema destino
	 * @return True se houver registro integrado e false caso contrário
	 */
	boolean existeIntegracaoClienteProcessada();

	/**
	 * Verifica se já existe uma integração Contrato para um determinado contrato e ponto de consumo
	 * @param contrato O contrato
	 * @param pontoConsumo O ponto de consumo
	 * @return O objeto IntegracaoContrato caso exista
	 */
	IntegracaoContrato obterIntegracaoContrato(Contrato contrato, PontoConsumo pontoConsumo);
	
	/**
	 * Verifica a situacao do cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @return the integracao cliente
	 */
	public IntegracaoCliente buscarSituacaoCliente(Cliente cliente);

	IntegracaoFuncao buscarIntegracaoFuncaoNome(String nome);

	Collection<IntegracaoSistemaFuncao> consultarIntegracaoSistemaFuncaoPorIntegracaoFuncao(Long integracaoFuncao);

	boolean reintegrarCliente(Cliente cliente) throws NegocioException;
}
