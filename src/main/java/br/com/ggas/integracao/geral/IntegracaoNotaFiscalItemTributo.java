/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.geral;

import java.math.BigDecimal;

/**
 * The Interface IntegracaoNotaFiscalItemTributo.
 */
public interface IntegracaoNotaFiscalItemTributo extends Integracao {

	/** The bean id integracao nota fiscal item tributo. */
	String BEAN_ID_INTEGRACAO_NOTA_FISCAL_ITEM_TRIBUTO = "integracaoNotaFiscalItemTributo";

	/**
	 * Gets the nota fiscal item.
	 *
	 * @return the nota fiscal item
	 */
	IntegracaoNotaFiscalItem getNotaFiscalItem();

	/**
	 * Sets the nota fiscal item.
	 *
	 * @param notaFiscalItem the new nota fiscal item
	 */
	void setNotaFiscalItem(IntegracaoNotaFiscalItem notaFiscalItem);

	/**
	 * Gets the tributo.
	 *
	 * @return the tributo
	 */
	Long getTributo();

	/**
	 * Sets the tributo.
	 *
	 * @param tributo the new tributo
	 */
	void setTributo(Long tributo);

	/**
	 * Gets the percentual aliquota.
	 *
	 * @return the percentual aliquota
	 */
	BigDecimal getPercentualAliquota();

	/**
	 * Sets the percentual aliquota.
	 *
	 * @param percentualAliquota the new percentual aliquota
	 */
	void setPercentualAliquota(BigDecimal percentualAliquota);

	/**
	 * Gets the base calculo.
	 *
	 * @return the base calculo
	 */
	BigDecimal getBaseCalculo();

	/**
	 * Sets the base calculo.
	 *
	 * @param baseCalculo the new base calculo
	 */
	void setBaseCalculo(BigDecimal baseCalculo);

	/**
	 * Gets the valor tributo.
	 *
	 * @return the valor tributo
	 */
	BigDecimal getValorTributo();

	/**
	 * Sets the valor tributo.
	 *
	 * @param valorTributo the new valor tributo
	 */
	void setValorTributo(BigDecimal valorTributo);

	/**
	 * Gets the base calculo substituto.
	 *
	 * @return the base calculo substituto
	 */
	BigDecimal getBaseCalculoSubstituto();

	/**
	 * Sets the base calculo substituto.
	 *
	 * @param baseCalculoSubstituto the new base calculo substituto
	 */
	void setBaseCalculoSubstituto(BigDecimal baseCalculoSubstituto);

	/**
	 * Gets the valor substituto.
	 *
	 * @return the valor substituto
	 */
	BigDecimal getValorSubstituto();

	/**
	 * Sets the valor substituto.
	 *
	 * @param valorSubstituto the new valor substituto
	 */
	void setValorSubstituto(BigDecimal valorSubstituto);

	/**
	 * Gets the indicador isencao.
	 *
	 * @return the indicador isencao
	 */
	Boolean getIndicadorIsencao();

	/**
	 * Sets the indicador isencao.
	 *
	 * @param indicadorIsencao the new indicador isencao
	 */
	void setIndicadorIsencao(Boolean indicadorIsencao);

	/**
	 * Gets the indicador prodesin.
	 *
	 * @return the indicador prodesin
	 */
	Boolean getIndicadorProdesin();

	/**
	 * Sets the indicador prodesin.
	 *
	 * @param indicadorProdesin the new indicador prodesin
	 */
	void setIndicadorProdesin(Boolean indicadorProdesin);

	/**
	 * Gets the valor outros.
	 *
	 * @return the valor outros
	 */
	BigDecimal getValorOutros();

	/**
	 * Sets the valor outros.
	 *
	 * @param valorOutros the new valor outros
	 */
	void setValorOutros(BigDecimal valorOutros);
}
