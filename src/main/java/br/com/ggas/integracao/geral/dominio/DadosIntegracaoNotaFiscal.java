package br.com.ggas.integracao.geral.dominio;

import org.apache.log4j.Logger;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * Classe dominio responsavel pelos dados integracao nota fiscal
 *
 */
public class DadosIntegracaoNotaFiscal {
	private static final Logger LOG = Logger.getLogger(DadosIntegracaoNotaFiscal.class);
	
	/**
	 * Obtem contrato do documento fiscal
	 * 
	 * @param documentoFiscal - {@link DocumentoFiscal}
	 * @return contrato - {@link Contrato}
	 * @throws NegocioException the Negocio Exception
	 */
	public Contrato obterContrato(DocumentoFiscal documentoFiscal) throws NegocioException {
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		
		Contrato contratoDocumento = null;
		
		try {
			contratoDocumento = documentoFiscal.getFatura().getContratoAtual();
			if (contratoDocumento.getChavePrimariaPai() == null) {
				contratoDocumento = (Contrato) controladorContrato.obter(documentoFiscal.getFatura().getContrato().getChavePrimaria());
			} else {
				contratoDocumento = controladorContrato.
						consultarContratoAtivoPorContratoPai(documentoFiscal.getFatura().getContratoAtual().getChavePrimariaPai());
			}
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException("Erro ao consultar contrato para integracao da nota fiscal");
		}
		
		return contratoDocumento;
	}
	
}
