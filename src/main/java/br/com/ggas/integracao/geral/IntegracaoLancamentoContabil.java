/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.integracao.geral;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Interface IntegracaoLancamentoContabil.
 */
public interface IntegracaoLancamentoContabil extends Integracao {

	/** The bean id integracao lancamento contabil. */
	String BEAN_ID_INTEGRACAO_LANCAMENTO_CONTABIL = "integracaoLancamentoContabil";

	/**
	 * Gets the evento comercial.
	 *
	 * @return the evento comercial
	 */
	public Long getEventoComercial();

	/**
	 * Sets the evento comercial.
	 *
	 * @param eventoComercial the new evento comercial
	 */
	public void setEventoComercial(Long eventoComercial);

	/**
	 * Gets the lancamento item contabil.
	 *
	 * @return the lancamento item contabil
	 */
	public Long getLancamentoItemContabil();

	/**
	 * Sets the lancamento item contabil.
	 *
	 * @param lancamentoItemContabil the new lancamento item contabil
	 */
	public void setLancamentoItemContabil(Long lancamentoItemContabil);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	public Long getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	public void setSegmento(Long segmento);

	/**
	 * Gets the tipo lancamento.
	 *
	 * @return the tipo lancamento
	 */
	public String getTipoLancamento();

	/**
	 * Sets the tipo lancamento.
	 *
	 * @param tipoLancamento the new tipo lancamento
	 */
	public void setTipoLancamento(String tipoLancamento);

	/**
	 * Gets the tributo.
	 *
	 * @return the tributo
	 */
	public Long getTributo();

	/**
	 * Sets the tributo.
	 *
	 * @param tributo the new tributo
	 */
	public void setTributo(Long tributo);

	/**
	 * Gets the conta auxiliar.
	 *
	 * @return the conta auxiliar
	 */
	public String getContaAuxiliar();

	/**
	 * Sets the conta auxiliar.
	 *
	 * @param contaAuxiliar the new conta auxiliar
	 */
	public void setContaAuxiliar(String contaAuxiliar);

	/**
	 * Gets the historico.
	 *
	 * @return the historico
	 */
	public String getHistorico();

	/**
	 * Sets the historico.
	 *
	 * @param historico the new historico
	 */
	public void setHistorico(String historico);

	/**
	 * Gets the numero conta.
	 *
	 * @return the numero conta
	 */
	public String getNumeroConta();

	/**
	 * Sets the numero conta.
	 *
	 * @param numeroConta the new numero conta
	 */
	public void setNumeroConta(String numeroConta);

	/**
	 * Gets the descricao conta.
	 *
	 * @return the descricao conta
	 */
	public String getDescricaoConta();

	/**
	 * Sets the descricao conta.
	 *
	 * @param descricaoConta the new descricao conta
	 */
	public void setDescricaoConta(String descricaoConta);

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public BigDecimal getValor();

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(BigDecimal valor);

	/**
	 * Gets the lancamento contabil sintetico.
	 *
	 * @return the lancamento contabil sintetico
	 */
	public Long getLancamentoContabilSintetico();

	/**
	 * Sets the lancamento contabil sintetico.
	 *
	 * @param lancamentoContabilSintetico the new lancamento contabil sintetico
	 */
	public void setLancamentoContabilSintetico(Long lancamentoContabilSintetico);

	/**
	 * Gets the data contabil.
	 *
	 * @return the data contabil
	 */
	public Date getDataContabil();

	/**
	 * Sets the data contabil.
	 *
	 * @param dataContabil the new data contabil
	 */
	public void setDataContabil(Date dataContabil);

}
