/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.integracao.portalnfe;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Classe de batch para o processo
 * de integração de notas fiscais
 *
 */
@Component
public class ProcessarIntegracaoPortalNfeBatch implements Batch {

	private static final String PROCESSO = "processo";
	private static final String STATUS_NFE = "idStatusNFE";
	private static final String DATA_EMISSAO = "dataEmissao";
	private static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";
	private static final String CICLO = "ciclo";

	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		/*Parâmetros da integração*/
		String statusNfe = (String) parametros.get(STATUS_NFE);
		String data = (String) parametros.get(DATA_EMISSAO);
		String anoMesFatu = (String) parametros.get(ANO_MES_FATURAMENTO);
		String ciclo = (String) parametros.get(CICLO);
		Date dataEmissao = null;
		Long idStatusNfe = null;
		Integer numeroCiclo = null;
		Integer anoMesFaturamento = null;
		

		ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(processo.getUsuario(), processo.getOperacao(),
				(String) controladorParametrosSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));

		//Formatando os parâmetros para a consulta das notas fiscais
		if (data != null) {
			dataEmissao = Util.converterCampoStringParaData(DATA_EMISSAO, data, Constantes.FORMATO_DATA_BR);
		}

		if (statusNfe != null) {
			idStatusNfe = Long.valueOf(statusNfe);
		}
		
		if (anoMesFatu != null) {
			anoMesFaturamento = Integer.parseInt(anoMesFatu.toString().
					replace("/", "").replaceAll("_", ""));
		}

		if (ciclo != null) {
			numeroCiclo = Integer.valueOf(ciclo);
		}

		Collection<DocumentoFiscal> documentosFiscais = 
				controladorFatura.consultarDocumentosFiscaisPorStatusNfeDataEmissao(
						idStatusNfe, dataEmissao, anoMesFaturamento, numeroCiclo);

		logProcessamento.append("\n\n Clientes e Valores das Notas Fiscais\n\n");

		//Integrando os documentos fiscais
		for(DocumentoFiscal documentoFiscal : documentosFiscais) {
			ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			ParametroSistema referenciaIntegracaoTituloNota =
					controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

			ConstanteSistema referenciaIntegracaoContrato =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

			ConstanteSistema situacaoProcessado =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

			if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
				IntegracaoContrato contratoIntegrado = controladorIntegracao.obterIntegracaoContrato(
						documentoFiscal.getFatura().getContrato(), documentoFiscal.getFatura().getPontoConsumo());
				if (contratoIntegrado == null || !situacaoProcessado.getValor().equals(contratoIntegrado.getSituacao())) {
					throw new NegocioException(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE, true);
				}
			} else {
				IntegracaoCliente clienteIntegrado = controladorIntegracao.buscarIntegracaoCliente(
						documentoFiscal.getFatura().getCliente());
				if (clienteIntegrado == null) {
					throw new NegocioException(Constantes.ERRO_CLIENTE_INTEGRACAO_INEXISTENTE, true);
				}
			}
			controladorIntegracao.inserirIntegracaoNotaFiscal(documentoFiscal, Boolean.FALSE, dadosAuditoria, logProcessamento);
		}


		return logProcessamento.toString();
	}

}
