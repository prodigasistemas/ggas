/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 25/07/2013 16:20:09
 @author vpessoa
 */

package br.com.ggas.integracao.titulos;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoTituloPagar.
 *
 * @author vpessoa
 */
public interface IntegracaoTituloPagar extends Integracao {

	/** The bean id integracao titulo pagar. */
	String BEAN_ID_INTEGRACAO_TITULO_PAGAR = "integracaoTituloPagar";

	/**
	 * Gets the fatura.
	 *
	 * @return the fatura
	 */
	Long getFatura();

	/**
	 * Sets the fatura.
	 *
	 * @param fatura the new fatura
	 */
	void setFatura(Long fatura);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	IntegracaoCliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(IntegracaoCliente cliente);

	/**
	 * Gets o integracao Contrato
	 * @return o integracao Contrato
	 */
	IntegracaoContrato getContrato();

	/**
	 * Sets o integracao Contrato
	 * @param contrato o integracao Contrato
	 */
	void setContrato(IntegracaoContrato contrato);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Long getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	void setSegmento(Long segmento);

	/**
	 * Gets the motivo cancelamento.
	 *
	 * @return the motivo cancelamento
	 */
	String getMotivoCancelamento();

	/**
	 * Sets the motivo cancelamento.
	 *
	 * @param motivoCancelamento the new motivo cancelamento
	 */
	void setMotivoCancelamento(String motivoCancelamento);

	/**
	 * Gets the data emissao.
	 *
	 * @return the data emissao
	 */
	Date getDataEmissao();

	/**
	 * Sets the data emissao.
	 *
	 * @param dataEmissao the new data emissao
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * Gets the data vencimento.
	 *
	 * @return the data vencimento
	 */
	Date getDataVencimento();

	/**
	 * Sets the data vencimento.
	 *
	 * @param dataVencimento the new data vencimento
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * Gets the data cancelamento.
	 *
	 * @return the data cancelamento
	 */
	Date getDataCancelamento();

	/**
	 * Sets the data cancelamento.
	 *
	 * @param dataCancelamento the new data cancelamento
	 */
	void setDataCancelamento(Date dataCancelamento);

	/**
	 * Gets the ano mes ciclo referencia.
	 *
	 * @return the ano mes ciclo referencia
	 */
	String getAnoMesCicloReferencia();

	/**
	 * Sets the ano mes ciclo referencia.
	 *
	 * @param anoMesCicloReferencia the new ano mes ciclo referencia
	 */
	void setAnoMesCicloReferencia(String anoMesCicloReferencia);

	/**
	 * Gets the valor total.
	 *
	 * @return the valor total
	 */
	BigDecimal getValorTotal();

	/**
	 * Sets the valor total.
	 *
	 * @param valorTotal the new valor total
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * Gets the observacao nota.
	 *
	 * @return the observacao nota
	 */
	String getObservacaoNota();

	/**
	 * Sets the observacao nota.
	 *
	 * @param observacaoNota the new observacao nota
	 */
	void setObservacaoNota(String observacaoNota);

	/**
	 * Gets the nosso numero.
	 *
	 * @return the nosso numero
	 */
	Integer getNossoNumero();

	/**
	 * Sets the nosso numero.
	 *
	 * @param nossoNumero the new nosso numero
	 */
	void setNossoNumero(Integer nossoNumero);
}
