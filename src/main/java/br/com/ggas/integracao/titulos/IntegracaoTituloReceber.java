/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.integracao.titulos;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoTituloReceber.
 * @author vpessoa
 */
public interface IntegracaoTituloReceber extends Integracao {

	/** The bean id integracao titulo receber. */
	String BEAN_ID_INTEGRACAO_TITULO_RECEBER = "integracaoTituloReceber";

	/**
	 * Gets the fatura.
	 *
	 * @return the fatura
	 */
	Long getFatura();

	/**
	 * Sets the fatura.
	 *
	 * @param fatura the new fatura
	 */
	void setFatura(Long fatura);

	/**
	 * Gets the tipo documento.
	 *
	 * @return the tipo documento
	 */
	Long getTipoDocumento();

	/**
	 * Sets the tipo documento.
	 *
	 * @param tipoDocumento the new tipo documento
	 */
	void setTipoDocumento(Long tipoDocumento);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	IntegracaoCliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(IntegracaoCliente cliente);

	/**
	 * Gets o integracao Contrato
	 * @return o integracao Contrato
	 */
	IntegracaoContrato getContrato();

	/**
	 * Sets o integracao Contrato
	 * @param contrato o integracao Contrato
	 */
	void setContrato(IntegracaoContrato contrato);

	/**
	 * Gets the situacao titulo.
	 *
	 * @return the situacao titulo
	 */
	Long getSituacaoTitulo();

	/**
	 * Sets the situacao titulo.
	 *
	 * @param situacaoTitulo the new situacao titulo
	 */
	void setSituacaoTitulo(Long situacaoTitulo);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Long getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	void setSegmento(Long segmento);

	/**
	 * Gets the motivo cancelamento.
	 *
	 * @return the motivo cancelamento
	 */
	String getMotivoCancelamento();

	/**
	 * Sets the motivo cancelamento.
	 *
	 * @param motivoCancelamento the new motivo cancelamento
	 */
	void setMotivoCancelamento(String motivoCancelamento);

	/**
	 * Gets the data emissao.
	 *
	 * @return the data emissao
	 */
	Date getDataEmissao();

	/**
	 * Sets the data emissao.
	 *
	 * @param dataEmissao the new data emissao
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * Gets the data cancelamento.
	 *
	 * @return the data cancelamento
	 */
	Date getDataCancelamento();

	/**
	 * Sets the data cancelamento.
	 *
	 * @param dataCancelamento the new data cancelamento
	 */
	void setDataCancelamento(Date dataCancelamento);

	/**
	 * Gets the data vencimento.
	 *
	 * @return the data vencimento
	 */
	Date getDataVencimento();

	/**
	 * Sets the data vencimento.
	 *
	 * @param dataVencimento the new data vencimento
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * Gets the ano mes ciclo referencia.
	 *
	 * @return the ano mes ciclo referencia
	 */
	String getAnoMesCicloReferencia();

	/**
	 * Sets the ano mes ciclo referencia.
	 *
	 * @param anoMesCicloReferencia the new ano mes ciclo referencia
	 */
	void setAnoMesCicloReferencia(String anoMesCicloReferencia);

	/**
	 * Gets the valor total.
	 *
	 * @return the valor total
	 */
	BigDecimal getValorTotal();

	/**
	 * Sets the valor total.
	 *
	 * @param valorTotal the new valor total
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * Gets the valor de acrescimos.
	 *
	 * @return the valor de acrescimos
	 */
	BigDecimal getValorAcrescimos();

	/**
	 * Sets the valor de acrescimos.
	 *
	 * @param valorAcrescimos the new valor de acrescimos
	 */
	void setValorAcrescimos(BigDecimal valorAcrescimos);

	/**
	 * Gets the valor de descontos.
	 *
	 * @return the valor de descontos
	 */
	BigDecimal getValorDescontos();

	/**
	 * Sets the valor de descontos.
	 *
	 * @param valorDescontos the new valor de descontos
	 */
	void setValorDescontos(BigDecimal valorDescontos);

	/**
	 * Gets the observacao nota.
	 *
	 * @return the observacao nota
	 */
	String getObservacaoNota();

	/**
	 * Sets the observacao nota.
	 *
	 * @param observacaoNota the new observacao nota
	 */
	void setObservacaoNota(String observacaoNota);

	/**
	 * Gets the nosso numero.
	 *
	 * @return the nosso numero
	 */
	Long getNossoNumero();

	/**
	 * Sets the nosso numero.
	 *
	 * @param nossoNumero the new nosso numero
	 */
	void setNossoNumero(Long nossoNumero);

	/**
	 * Gets o convênio arrecadador
	 * @return o convênio arrecadador
	 */
	String getConvenioArrecadador();

	/**
	 * Sets o convênio arrecadador
	 * @param convenioArrecadador o convênio arrecadador
	 */
	void setConvenioArrecadador(String convenioArrecadador);

	/**
	 * Gets o código do banco do cliente
	 * @return o código do banco do cliente
	 */
	String getBancoCliente();

	/**
	 * Sets o código do banco do cliente
	 * @param bancoCliente o código do banco do cliente
	 */
	void setBancoCliente(String bancoCliente);

	/**
	 * Gets a agência do cliente
	 * @return a agência do cliente
	 */
	String getAgenciaCliente();

	/**
	 * Sets a agência do cliente
	 * @param agenciaCliente a agência do cliente
	 */
	void setAgenciaCliente(String agenciaCliente);

	/**
	 * Gets a conta corrente do cliente
	 * @return a conta corrente do cliente
	 */
	String getContaCliente();

	/**
	 * Sets a conta corrente do cliente
	 * @param contaCliente a conta corrente do cliente
	 */
	void setContaCliente(String contaCliente);
}
