/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 25/07/2013 16:20:24
 @author vpessoa
 */

package br.com.ggas.integracao.titulos;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.geral.Integracao;

/**
 * Classe Integracao Titulo.
 * 
 * 
 *@author vpessoa
 *
 */
public interface IntegracaoTitulo extends Integracao {

	String BEAN_ID_INTEGRACAO_TITULO = "integracaoTitulo";

	/**
	 * Gets the titulo pagar.
	 *
	 * @return the titulo pagar
	 */
	IntegracaoTituloPagar getTituloPagar();

	/**
	 * Sets the titulo pagar.
	 *
	 * @param tituloPagar the new titulo pagar
	 */
	void setTituloPagar(IntegracaoTituloPagar tituloPagar);

	/**
	 * Gets the titulo receber.
	 *
	 * @return the titulo receber
	 */
	IntegracaoTituloReceber getTituloReceber();

	/**
	 * Sets the titulo receber.
	 *
	 * @param tituloReceber the new titulo receber
	 */
	void setTituloReceber(IntegracaoTituloReceber tituloReceber);

	/**
	 * Gets the recebimento.
	 *
	 * @return the recebimento
	 */
	Integer getRecebimento();

	/**
	 * Sets the recebimento.
	 *
	 * @param recebimento the new recebimento
	 */
	void setRecebimento(Integer recebimento);

	/**
	 * Gets the tipo baixa.
	 *
	 * @return the tipo baixa
	 */
	String getTipoBaixa();

	/**
	 * Sets the tipo baixa.
	 *
	 * @param tipoBaixa the new tipo baixa
	 */
	void setTipoBaixa(String tipoBaixa);

	/**
	 * Gets the meio devolucao.
	 *
	 * @return the meio devolucao
	 */
	Integer getMeioDevolucao();

	/**
	 * Sets the meio devolucao.
	 *
	 * @param meioDevolucao the new meio devolucao
	 */
	void setMeioDevolucao(Integer meioDevolucao);

	/**
	 * Gets the sequencial.
	 *
	 * @return the sequencial
	 */
	Integer getSequencial();

	/**
	 * Sets the sequencial.
	 *
	 * @param sequencial the new sequencial
	 */
	void setSequencial(Integer sequencial);

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	void setValor(BigDecimal valor);

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	Date getData();

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	void setData(Date data);

	/**
	 * Gets the valor principal.
	 *
	 * @return the valor principal
	 */
	BigDecimal getValorPrincipal();

	/**
	 * Sets the valor principal.
	 *
	 * @param valorPrincipal the new valor principal
	 */
	void setValorPrincipal(BigDecimal valorPrincipal);

	/**
	 * Gets the valor juros multa.
	 *
	 * @return the valor juros multa
	 */
	BigDecimal getValorJurosMulta();

	/**
	 * Sets the valor juros multa.
	 *
	 * @param valorJurosMulta the new valor juros multa
	 */
	void setValorJurosMulta(BigDecimal valorJurosMulta);

	/**
	 * Gets the valor descontos.
	 *
	 * @return the valor descontos
	 */
	BigDecimal getValorDescontos();

	/**
	 * Sets the valor descontos.
	 *
	 * @param valorDescontos the new valor descontos
	 */
	void setValorDescontos(BigDecimal valorDescontos);

	/**
	 * Gets the valor abatimento.
	 *
	 * @return the valor abatimento
	 */
	BigDecimal getValorAbatimento();

	/**
	 * Sets the valor abatimento.
	 *
	 * @param valorAbatimento the new valor abatimento
	 */
	void setValorAbatimento(BigDecimal valorAbatimento);

	/**
	 * Gets the valor credito.
	 *
	 * @return the valor credito
	 */
	BigDecimal getValorCredito();

	/**
	 * Sets the valor credito.
	 *
	 * @param valorCredito the new valor credito
	 */
	void setValorCredito(BigDecimal valorCredito);

	/**
	 * Gets the banco.
	 *
	 * @return the banco
	 */
	String getBanco();

	/**
	 * Sets the banco.
	 *
	 * @param banco the new banco
	 */
	void setBanco(String banco);

	/**
	 * Gets the numero agencia.
	 *
	 * @return the numero agencia
	 */
	String getNumeroAgencia();

	/**
	 * Sets the numero agencia.
	 *
	 * @param numeroAgencia the new numero agencia
	 */
	void setNumeroAgencia(String numeroAgencia);

	/**
	 * Gets the numero agencia digito.
	 *
	 * @return the numero agencia digito
	 */
	String getNumeroAgenciaDigito();

	/**
	 * Sets the numero agencia digito.
	 *
	 * @param numeroAgenciaDigito the new numero agencia digito
	 */
	void setNumeroAgenciaDigito(String numeroAgenciaDigito);

	/**
	 * Gets the numero conta.
	 *
	 * @return the numero conta
	 */
	String getNumeroConta();

	/**
	 * Sets the numero conta.
	 *
	 * @param numeroConta the new numero conta
	 */
	void setNumeroConta(String numeroConta);

	/**
	 * Gets the numero conta digito.
	 *
	 * @return the numero conta digito
	 */
	String getNumeroContaDigito();

	/**
	 * Sets the numero conta digito.
	 *
	 * @param numeroContaDigito the new numero conta digito
	 */
	void setNumeroContaDigito(String numeroContaDigito);

}
