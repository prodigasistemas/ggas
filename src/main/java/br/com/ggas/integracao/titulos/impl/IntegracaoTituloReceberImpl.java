/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 25/07/2013 16:19:20
 @author vpessoa
 */

package br.com.ggas.integracao.titulos.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;
import br.com.ggas.integracao.titulos.IntegracaoTituloReceber;

/**
 * Classe Integracao Titulo Receber.
 * 
 * 
 * @author vpessoa
 * 
 */
public class IntegracaoTituloReceberImpl extends IntegracaoImpl implements IntegracaoTituloReceber {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8246076829714881831L;

	private Long fatura;

	private Long tipoDocumento;

	private IntegracaoCliente cliente;

	private IntegracaoContrato contrato;

	private Long situacaoTitulo;

	private Long segmento;

	private String motivoCancelamento;

	private Date dataEmissao;

	private Date dataVencimento;

	private Date dataCancelamento;

	private String anoMesCicloReferencia;

	private BigDecimal valorTotal;

	private BigDecimal valorAcrescimos;

	private BigDecimal valorDescontos;

	private String observacaoNota;

	private Long nossoNumero;

	private String convenioArrecadador;

	private String bancoCliente;

	private String agenciaCliente;

	private String contaCliente;

	@Override
	public Long getFatura() {

		return fatura;
	}

	@Override
	public void setFatura(Long fatura) {

		this.fatura = fatura;
	}

	@Override
	public Long getTipoDocumento() {

		return tipoDocumento;
	}

	@Override
	public void setTipoDocumento(Long tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public IntegracaoCliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(IntegracaoCliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public IntegracaoContrato getContrato() {
		return contrato;
	}

	@Override
	public void setContrato(IntegracaoContrato contrato) {
		this.contrato = contrato;
	}

	@Override
	public Long getSituacaoTitulo() {

		return situacaoTitulo;
	}

	@Override
	public void setSituacaoTitulo(Long situacaoTitulo) {

		this.situacaoTitulo = situacaoTitulo;
	}

	@Override
	public Long getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Long segmento) {

		this.segmento = segmento;
	}

	@Override
	public String getMotivoCancelamento() {

		return motivoCancelamento;
	}

	@Override
	public void setMotivoCancelamento(String motivoCancelamento) {

		this.motivoCancelamento = motivoCancelamento;
	}

	@Override
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	@Override
	public void setDataEmissao(Date dataEmissao) {
		if(dataEmissao != null) {
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	@Override
	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	@Override
	public void setDataVencimento(Date dataVencimento) {
		if (dataVencimento != null) {
			this.dataVencimento = (Date) dataVencimento.clone();
		} else {
			this.dataVencimento = null;
		}
	}

	@Override
	public String getAnoMesCicloReferencia() {

		return anoMesCicloReferencia;
	}

	@Override
	public void setAnoMesCicloReferencia(String anoMesCicloReferencia) {

		this.anoMesCicloReferencia = anoMesCicloReferencia;
	}

	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	@Override
	public BigDecimal getValorAcrescimos() {
		return valorAcrescimos;
	}

	@Override
	public void setValorAcrescimos(BigDecimal valorAcrescimos) {
		this.valorAcrescimos = valorAcrescimos;
	}

	@Override
	public BigDecimal getValorDescontos() {
		return valorDescontos;
	}

	@Override
	public void setValorDescontos(BigDecimal valorDescontos) {
		this.valorDescontos = valorDescontos;
	}

	@Override
	public String getObservacaoNota() {

		return observacaoNota;
	}

	@Override
	public void setObservacaoNota(String observacaoNota) {

		this.observacaoNota = observacaoNota;
	}

	@Override
	public Long getNossoNumero() {

		return nossoNumero;
	}

	@Override
	public void setNossoNumero(Long nossoNumero) {

		this.nossoNumero = nossoNumero;
	}

	/**
	 * @return the dataCancelamento
	 */
	@Override
	public Date getDataCancelamento() {
		Date data = null;
		if (this.dataCancelamento != null) {
			data = (Date) dataCancelamento.clone();
		}
		return data;
	}

	/**
	 * @param dataCancelamento
	 *            the dataCancelamento to set
	 */
	@Override
	public void setDataCancelamento(Date dataCancelamento) {
		if (dataCancelamento != null) {
			this.dataCancelamento = (Date) dataCancelamento.clone();
		} else {
			this.dataCancelamento = null;
		}
	}

	@Override
	public String getConvenioArrecadador() {
		return convenioArrecadador;
	}

	@Override
	public void setConvenioArrecadador(String convenioArrecadador) {
		this.convenioArrecadador = convenioArrecadador;
	}

	@Override
	public String getBancoCliente() {
		return bancoCliente;
	}

	@Override
	public void setBancoCliente(String bancoCliente) {
		this.bancoCliente = bancoCliente;
	}

	@Override
	public String getAgenciaCliente() {
		return agenciaCliente;
	}

	@Override
	public void setAgenciaCliente(String agenciaCliente) {
		this.agenciaCliente = agenciaCliente;
	}

	@Override
	public String getContaCliente() {
		return contaCliente;
	}

	@Override
	public void setContaCliente(String contaCliente) {
		this.contaCliente = contaCliente;
	}
}
