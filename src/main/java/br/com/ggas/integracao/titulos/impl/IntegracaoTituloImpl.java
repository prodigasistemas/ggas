/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/07/2013 22:27:27
 @author vpessoa
 */

package br.com.ggas.integracao.titulos.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.geral.impl.IntegracaoImpl;
import br.com.ggas.integracao.titulos.IntegracaoTitulo;
import br.com.ggas.integracao.titulos.IntegracaoTituloPagar;
import br.com.ggas.integracao.titulos.IntegracaoTituloReceber;

/**
 * Classe responsável pelos atributos e métodos relacionados a IntegracaoTitulo.
 *
 */
public class IntegracaoTituloImpl extends IntegracaoImpl implements IntegracaoTitulo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7738015873505824417L;

	private IntegracaoTituloPagar tituloPagar;

	private IntegracaoTituloReceber tituloReceber;

	private Integer recebimento;

	private String tipoBaixa;

	private Integer meioDevolucao;

	private Integer sequencial;

	private BigDecimal valor;

	private Date data;

	private BigDecimal valorPrincipal;

	private BigDecimal valorJurosMulta;

	private BigDecimal valorDescontos;

	private BigDecimal valorAbatimento;

	private BigDecimal valorCredito;

	private String banco;

	private String numeroAgencia;

	private String numeroAgenciaDigito;

	private String numeroConta;

	private String numeroContaDigito;

	@Override
	public IntegracaoTituloPagar getTituloPagar() {

		return tituloPagar;
	}

	@Override
	public void setTituloPagar(IntegracaoTituloPagar tituloPagar) {

		this.tituloPagar = tituloPagar;
	}

	@Override
	public IntegracaoTituloReceber getTituloReceber() {

		return tituloReceber;
	}

	@Override
	public void setTituloReceber(IntegracaoTituloReceber tituloReceber) {

		this.tituloReceber = tituloReceber;
	}

	@Override
	public Integer getRecebimento() {

		return recebimento;
	}

	@Override
	public void setRecebimento(Integer recebimento) {

		this.recebimento = recebimento;
	}

	@Override
	public String getTipoBaixa() {

		return tipoBaixa;
	}

	@Override
	public void setTipoBaixa(String tipoBaixa) {

		this.tipoBaixa = tipoBaixa;
	}

	@Override
	public Integer getMeioDevolucao() {

		return meioDevolucao;
	}

	@Override
	public void setMeioDevolucao(Integer meioDevolucao) {

		this.meioDevolucao = meioDevolucao;
	}

	@Override
	public Integer getSequencial() {

		return sequencial;
	}

	@Override
	public void setSequencial(Integer sequencial) {

		this.sequencial = sequencial;
	}

	@Override
	public BigDecimal getValor() {

		return valor;
	}

	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	@Override
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	@Override
	public void setData(Date data) {
		if (data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	@Override
	public BigDecimal getValorPrincipal() {

		return valorPrincipal;
	}

	@Override
	public void setValorPrincipal(BigDecimal valorPrincipal) {

		this.valorPrincipal = valorPrincipal;
	}

	@Override
	public BigDecimal getValorJurosMulta() {

		return valorJurosMulta;
	}

	@Override
	public void setValorJurosMulta(BigDecimal valorJurosMulta) {

		this.valorJurosMulta = valorJurosMulta;
	}

	@Override
	public BigDecimal getValorDescontos() {

		return valorDescontos;
	}

	@Override
	public void setValorDescontos(BigDecimal valorDescontos) {

		this.valorDescontos = valorDescontos;
	}

	@Override
	public BigDecimal getValorAbatimento() {

		return valorAbatimento;
	}

	@Override
	public void setValorAbatimento(BigDecimal valorAbatimento) {

		this.valorAbatimento = valorAbatimento;
	}

	@Override
	public BigDecimal getValorCredito() {

		return valorCredito;
	}

	@Override
	public void setValorCredito(BigDecimal valorCredito) {

		this.valorCredito = valorCredito;
	}

	@Override
	public String getNumeroAgencia() {

		return numeroAgencia;
	}

	@Override
	public void setNumeroAgencia(String numeroAgencia) {

		this.numeroAgencia = numeroAgencia;
	}

	@Override
	public String getNumeroAgenciaDigito() {

		return numeroAgenciaDigito;
	}

	@Override
	public void setNumeroAgenciaDigito(String numeroAgenciaDigito) {

		this.numeroAgenciaDigito = numeroAgenciaDigito;
	}

	@Override
	public String getNumeroConta() {

		return numeroConta;
	}

	@Override
	public void setNumeroConta(String numeroConta) {

		this.numeroConta = numeroConta;
	}

	@Override
	public String getNumeroContaDigito() {

		return numeroContaDigito;
	}

	@Override
	public void setNumeroContaDigito(String numeroContaDigito) {

		this.numeroContaDigito = numeroContaDigito;
	}

	@Override
	public String getBanco() {

		return banco;
	}

	@Override
	public void setBanco(String banco) {

		this.banco = banco;
	}

}
