/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 13/08/2013 14:20:34
 @author wcosta
 */

package br.com.ggas.integracao.titulos.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.bens.impl.ClasseGenerica;
import br.com.ggas.integracao.bens.impl.IntegracaoBemVazaoCorretorImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.ResumoIntegracaoVO;
import br.com.ggas.integracao.titulos.IntegracaoTitulo;
import br.com.ggas.integracao.titulos.impl.IntegracaoTituloImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Classe Processar Integracao Baixa Titulo Batch.
 * 
 *
 */
@Component
public class ProcessarIntegracaoBaixaTituloBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();
		Map<String, Object[]> mapaInconsistenciasTitulo = new LinkedHashMap<String, Object[]>();
		ResumoIntegracaoVO resumoIntegracaoVOTitulo = new ResumoIntegracaoVO();

		try {

			ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			ConstanteSistema constanteTitulo = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);

			ConstanteSistema constanteSistemaSituacaoNaoProcessado = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

			ConstanteSistema constanteOperacaoInclusao = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

			ConstanteSistema constanteSistemaIntegracaoSistemaGGAS = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);

			controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);

			controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);

			processo.setEmailResponsavel(Util.concatenaEmails(processo.getEmailResponsavel(),
							controladorIntegracao.buscarEmailDestinatarioIntegracao(Long.valueOf(constanteTitulo.getValor()))));

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesSistemasIntegrantes", constanteSistemaIntegracaoSistemaGGAS.getValor());
			filtro.put("habilitado", Boolean.TRUE);
			filtro.put("naoProcessado", constanteSistemaSituacaoNaoProcessado.getValor());

			Collection<IntegracaoTitulo> listaIntegracaoTitulo = (Collection<IntegracaoTitulo>) controladorIntegracao
							.consultarIntegracaoTitulo(filtro);

			if(!listaIntegracaoTitulo.isEmpty()) {

				logProcessamento.append(Constantes.PULA_LINHA).append("[")
						.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
						.append("] ").append("Iniciando o tratamento dos registros da integração de baixa de títulos.");
				for (IntegracaoTitulo integracao : listaIntegracaoTitulo) {

					if(integracao instanceof IntegracaoTituloImpl) {

						// inclusao
						if(integracao.getOperacao().equals(constanteOperacaoInclusao.getValor())) {
							if(controladorIntegracao.validarAtualizacaoTitulo((IntegracaoTituloImpl) integracao, mapaInconsistenciasTitulo)) {
								resumoIntegracaoVOTitulo.setQuantidadeRegistrosIncluidos(resumoIntegracaoVOTitulo
												.getQuantidadeRegistrosIncluidos() + 1);
							} else {
								resumoIntegracaoVOTitulo.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOTitulo
												.getQuantidadeRegistrosInconsistentes() + 1);
							}
						} else {
							Util.atualizaMapaInconsistencias(mapaInconsistenciasTitulo, Util.concatenaMensagemErro(
											Constantes.ERRO_NEGOCIO_OPERACAO_INVALIDA, Constantes.OPERACAOINVALIDA));
							controladorIntegracao.atualizarIntegracaoComErro(IntegracaoBemVazaoCorretorImpl.class, integracao,
											Constantes.OPERACAOINVALIDA);
							resumoIntegracaoVOTitulo.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOTitulo
											.getQuantidadeRegistrosInconsistentes() + 1);
						}

					} else if(integracao instanceof ClasseGenerica) {
						controladorIntegracao.atualizarIntegracaoComErro(ClasseGenerica.class, integracao, "Título inválido");
					}

				}
				resumoIntegracaoVOTitulo.setQuantidadeRegistrosProcessados(resumoIntegracaoVOTitulo.getQuantidadeRegistrosAlterados()
								+ resumoIntegracaoVOTitulo.getQuantidadeRegistrosIncluidos()
								+ resumoIntegracaoVOTitulo.getQuantidadeRegistrosExcluidos()
								+ resumoIntegracaoVOTitulo.getQuantidadeRegistrosInconsistentes());
				resumoIntegracaoVOTitulo.setMapaInconsistencias(mapaInconsistenciasTitulo);

				if(resumoIntegracaoVOTitulo.getQuantidadeRegistrosProcessados() > 0 || !mapaInconsistenciasTitulo.isEmpty()) {

					logProcessamento.append(Constantes.PULA_LINHA).append("[")
							.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
							.append("] ").append("Iniciando o tratamento dos registros da integração de baixa de títulos.");
					controladorIntegracao.preencherLogMapaInconsistencias(logProcessamento, resumoIntegracaoVOTitulo, true);
					logProcessamento.append(Constantes.PULA_LINHA).append("[")
							.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
							.append("] ").append("Finalizando o tratamento dos registros da integração de baixa de títulos.");
				}
			} else {
				logProcessamento.append(Constantes.PULA_LINHA).append("[")
						.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()))
						.append("] ").append(Constantes.ERRO_NEGOCIO_SEM_DADOS + Constantes.PULA_LINHA);
			}

		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

}
