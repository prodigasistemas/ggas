/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 30/07/2013 09:55:06
 @author wcosta
 */

package br.com.ggas.integracao.titulos;

import java.math.BigDecimal;

import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoTituloPagarTributo.
 */
public interface IntegracaoTituloPagarTributo extends Integracao {

	/** The bean id integracao titulo pagar tributo. */
	String BEAN_ID_INTEGRACAO_TITULO_PAGAR_TRIBUTO = "integracaoTituloPagarTributo";

	/**
	 * Gets the titulo pagar.
	 *
	 * @return the titulo pagar
	 */
	IntegracaoTituloPagar getTituloPagar();

	/**
	 * Sets the titulo pagar.
	 *
	 * @param tituloPagar the new titulo pagar
	 */
	void setTituloPagar(IntegracaoTituloPagar tituloPagar);

	/**
	 * Gets the tributo.
	 *
	 * @return the tributo
	 */
	Long getTributo();

	/**
	 * Sets the tributo.
	 *
	 * @param tributo the new tributo
	 */
	void setTributo(Long tributo);

	/**
	 * Gets the aliquota.
	 *
	 * @return the aliquota
	 */
	BigDecimal getAliquota();

	/**
	 * Sets the aliquota.
	 *
	 * @param aliquota the new aliquota
	 */
	void setAliquota(BigDecimal aliquota);

	/**
	 * Gets the base calculo.
	 *
	 * @return the base calculo
	 */
	BigDecimal getBaseCalculo();

	/**
	 * Sets the base calculo.
	 *
	 * @param baseCalculo the new base calculo
	 */
	void setBaseCalculo(BigDecimal baseCalculo);

	/**
	 * Gets the valor imposto.
	 *
	 * @return the valor imposto
	 */
	BigDecimal getValorImposto();

	/**
	 * Sets the valor imposto.
	 *
	 * @param valorImposto the new valor imposto
	 */
	void setValorImposto(BigDecimal valorImposto);

	/**
	 * Gets the base calculo substituto.
	 *
	 * @return the base calculo substituto
	 */
	BigDecimal getBaseCalculoSubstituto();

	/**
	 * Sets the base calculo substituto.
	 *
	 * @param baseCalculoSubstituto the new base calculo substituto
	 */
	void setBaseCalculoSubstituto(BigDecimal baseCalculoSubstituto);

	/**
	 * Gets the valor substituto.
	 *
	 * @return the valor substituto
	 */
	BigDecimal getValorSubstituto();

	/**
	 * Sets the valor substituto.
	 *
	 * @param valorSubstituto the new valor substituto
	 */
	void setValorSubstituto(BigDecimal valorSubstituto);

}
