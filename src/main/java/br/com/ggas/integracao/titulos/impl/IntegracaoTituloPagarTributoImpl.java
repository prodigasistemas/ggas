/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 30/07/2013 09:58:24
 @author wcosta
 */

package br.com.ggas.integracao.titulos.impl;

import java.math.BigDecimal;

import br.com.ggas.integracao.geral.impl.IntegracaoImpl;
import br.com.ggas.integracao.titulos.IntegracaoTituloPagar;
import br.com.ggas.integracao.titulos.IntegracaoTituloPagarTributo;


/**
 * Classe Integracao Titulo Pagar Tributo.
 * 
 *
 *
 */
public class IntegracaoTituloPagarTributoImpl extends IntegracaoImpl implements IntegracaoTituloPagarTributo {

	private static final long serialVersionUID = -7874459283226924071L;

	private IntegracaoTituloPagar tituloPagar;

	private Long tributo;

	private BigDecimal aliquota;

	private BigDecimal baseCalculo;

	private BigDecimal valorImposto;

	private BigDecimal baseCalculoSubstituto;

	private BigDecimal valorSubstituto;

	@Override
	public IntegracaoTituloPagar getTituloPagar() {

		return tituloPagar;
	}

	@Override
	public void setTituloPagar(IntegracaoTituloPagar tituloPagar) {

		this.tituloPagar = tituloPagar;
	}

	@Override
	public Long getTributo() {

		return tributo;
	}

	@Override
	public void setTributo(Long tributo) {

		this.tributo = tributo;
	}

	@Override
	public BigDecimal getAliquota() {

		return aliquota;
	}

	@Override
	public void setAliquota(BigDecimal aliquota) {

		this.aliquota = aliquota;
	}

	@Override
	public BigDecimal getBaseCalculo() {

		return baseCalculo;
	}

	@Override
	public void setBaseCalculo(BigDecimal baseCalculo) {

		this.baseCalculo = baseCalculo;
	}

	@Override
	public BigDecimal getValorImposto() {

		return valorImposto;
	}

	@Override
	public void setValorImposto(BigDecimal valorImposto) {

		this.valorImposto = valorImposto;
	}

	@Override
	public BigDecimal getBaseCalculoSubstituto() {

		return baseCalculoSubstituto;
	}

	@Override
	public void setBaseCalculoSubstituto(BigDecimal baseCalculoSubstituto) {

		this.baseCalculoSubstituto = baseCalculoSubstituto;
	}

	@Override
	public BigDecimal getValorSubstituto() {

		return valorSubstituto;
	}

	@Override
	public void setValorSubstituto(BigDecimal valorSubstituto) {

		this.valorSubstituto = valorSubstituto;
	}

}
