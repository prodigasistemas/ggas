package br.com.ggas.integracao.titulos.batch;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.arrecadacao.recebimento.RecebimentoSituacao;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.titulos.IntegracaoTitulo;
import br.com.ggas.integracao.titulos.IntegracaoTituloReceber;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * Classe responsável por processar o retorno da baixa de títulos registrados pelo sistema de origem
 * com destino o GAS
 */
@Component public class ProcessarIntegracaoBaixaTituloRetornoBatch implements Batch {

	private static final String PROCESSO = "processo";
	private static final String INTEGRACAO_SISTEMA_GGAS = "GAS";
	private static final String INTEGRACAO_SISTEMA_PIR = "PIR";

	private static final ControladorConstanteSistema controladorConstanteSistema =
			(ControladorConstanteSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

	@Override public String processar(Map<String, Object> parametros) throws GGASException {

		ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		ConstanteSistema constanteSistemaSituacaoNaoProcessado =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);
		ConstanteSistema constanteSistemaSituacaoErro =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_ERRO);
		ConstanteSistema constanteSistemaSituacaoProcessado =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
		ConstanteSistema constanteOperacaoInclusao =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("sistemaOrigem", INTEGRACAO_SISTEMA_PIR);
		filtro.put("sistemaDestino", INTEGRACAO_SISTEMA_GGAS);
		filtro.put("naoProcessado", constanteSistemaSituacaoNaoProcessado.getValor());

		Collection<IntegracaoTitulo> listaIntegracaoTitulo = controladorIntegracao.consultarTituloBaixa(filtro, null, "tituloReceber");

		logProcessamento.append(Constantes.PULA_LINHA).append("[")
				.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime())).append("] ")
				.append("Iniciando o tratamento dos registros da integração de retorno da baixa de títulos.");

		//Quando existe uma fatura com ponto de consumo não associado a ROTA
		Collection<Recebimento> logRecebimentosFaturasIndividuais = new ArrayList<>();
		Map<String, Collection<Recebimento>> logRecebimentos = new HashMap<>();
		Map<Long, String> logErroRecebimentos = new HashMap<>();

		logProcessamento.append("\n\n").append("Inserindo Recebimentos").append("\n\n");
		for (IntegracaoTitulo integracaoTituloBaixa : listaIntegracaoTitulo) {
			if (integracaoTituloBaixa.getOperacao() != null && integracaoTituloBaixa.getOperacao()
					.equals(constanteOperacaoInclusao.getValor())) {

				Recebimento recebimento = this.gerarRecebimento(integracaoTituloBaixa);
				Rota rota = recebimento.getPontoConsumo().getRota();
				if (rota != null) {
					String grupoFaturamentoDescricao = rota.getGrupoFaturamento().getDescricao();
					if (logRecebimentos.keySet().contains(grupoFaturamentoDescricao)) {
						logRecebimentos.get(grupoFaturamentoDescricao).add(recebimento);
					} else {
						logRecebimentos.put(grupoFaturamentoDescricao, new ArrayList<>());
						logRecebimentos.get(grupoFaturamentoDescricao).add(recebimento);
					}
				} else {
					logRecebimentosFaturasIndividuais.add(recebimento);
				}

				try {

					controladorRecebimento.inserirRecebimento(recebimento);
					integracaoTituloBaixa.setSituacao(constanteSistemaSituacaoProcessado.getValor());
					controladorIntegracao.atualizar(integracaoTituloBaixa);
				} catch (ConstraintViolationException | GGASException e) {

					String mensagemErro = "";
					Long idFatura = recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria();
					if (e instanceof ConstraintViolationException) {
						if (e.getCause() != null) {
							mensagemErro = e.getCause().getMessage();
							logErroRecebimentos.put(idFatura, mensagemErro);
						} else {
							mensagemErro = e.getMessage();
							logErroRecebimentos.put(idFatura, mensagemErro);
						}
					}
					if (e instanceof GGASException) {
						mensagemErro = e.getMessage();
						logErroRecebimentos.put(idFatura, mensagemErro);
					}
					integracaoTituloBaixa.setSituacao(constanteSistemaSituacaoErro.getValor());
					integracaoTituloBaixa.setDescricaoErro(mensagemErro);
					controladorIntegracao.atualizar(integracaoTituloBaixa);
					processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
				}
			}
		}
		preencherLogProcessamento(logRecebimentos, logRecebimentosFaturasIndividuais, logErroRecebimentos, logProcessamento);
		return logProcessamento.toString();
	}

	/**
	 * Metódo responsável por preencher o log com as informações da execução do processo
	 *
	 * @param logRecebimentos                   recebimentos agrupados por grupo faturamento
	 * @param logRecebimentosFaturasIndividuais recebimentos onde a fatura associada possui pontos de consumo sem rota
	 * @param logErroRecebimentos               recebimentos com erros
	 * @param logProcessamento                  log de processo
	 */
	private void preencherLogProcessamento(Map<String, Collection<Recebimento>> logRecebimentos,
			Collection<Recebimento> logRecebimentosFaturasIndividuais, Map<Long, String> logErroRecebimentos,
			StringBuilder logProcessamento) {

		for (Map.Entry<String, Collection<Recebimento>> entry : logRecebimentos.entrySet()) {
			logProcessamento.append("--- GRUPO DE FATURAMENTO ").append(entry.getKey()).append(" ---").append("\n\n");
			preencherLog(entry.getValue(), logErroRecebimentos, logProcessamento);
		}

		if (logRecebimentosFaturasIndividuais != null && !logRecebimentosFaturasIndividuais.isEmpty()) {
			logProcessamento.append("--- FATURAS INDIVIDUAIS ---").append("\n\n");
			preencherLog(logRecebimentosFaturasIndividuais, logErroRecebimentos, logProcessamento);
		}
	}

	/**
	 * Metódo responsável por preencher o log com as informações da execução do processo
	 *
	 * @param recebimentos        recebimentos gerados
	 * @param logErroRecebimentos recebimentos com erros
	 * @param logProcessamento    log de processo
	 */
	private void preencherLog(Collection<Recebimento> recebimentos, Map<Long, String> logErroRecebimentos, StringBuilder logProcessamento) {

		for (Recebimento recebimento : recebimentos) {
			Long idFatura = recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria();
			if (logErroRecebimentos.keySet().contains(idFatura)) {
				logProcessamento.append("ATENÇÃO! Falha no processamento da integração").append("\n");
				logProcessamento.append("Fatura: ").append(idFatura).append("\n");
				logProcessamento.append("LOG: ").append(logErroRecebimentos.get(idFatura).trim());
			} else {
				logProcessamento.append("Fatura: ").append(recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria()).append("\n");
				logProcessamento.append("Cliente: ").append(recebimento.getCliente().getNome()).append("\n");
				logProcessamento.append("Contrato: ")
						.append(recebimento.getFaturaGeral().getFaturaAtual().getContrato().getNumeroFormatado()).append("\n");
				logProcessamento.append("Ponto de Consumo: ")
						.append(recebimento.getFaturaGeral().getFaturaAtual().getDescricaoPontoConsumo()).append("\n");
				logProcessamento.append("Data de Pagamento: ").append(Util.converterDataHoraParaString(recebimento.getDataRecebimento()));
			}
			logProcessamento.append("\n\n").append("---").append("\n\n");
		}
	}

	/**
	 * Metódo responsável por gerar um recebimento a partir do retorno de uma baixa de título.
	 *
	 * @param integracaoTituloBaixa integração referente a baixa de título.
	 * @return recebimento recebimento gerado
	 */
	private Recebimento gerarRecebimento(IntegracaoTitulo integracaoTituloBaixa) throws GGASException {

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorDocumentoCobranca controladorDocumentoCobranca = (ControladorDocumentoCobranca) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);
		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		Recebimento recebimento = (Recebimento) ServiceLocator.getInstancia().getBeanPorID(Recebimento.BEAN_ID_RECEBIMENTO);
		AvisoBancario avisoBancario = (AvisoBancario) ServiceLocator.getInstancia().getBeanPorID(AvisoBancario.BEAN_ID_AVISO_BANCARIO);
		IntegracaoTituloReceber integracaoTituloReceber = integracaoTituloBaixa.getTituloReceber();

		String recebimentoClassificado =
				this.controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);
		RecebimentoSituacao recebimentoSituacao = controladorRecebimento.obterRecebimentoSituacao(Long.parseLong(recebimentoClassificado));

		recebimento.setRecebimentoSituacao(recebimentoSituacao);

		Fatura fatura = (Fatura) controladorFatura.obter(integracaoTituloReceber.getFatura());
		this.atualizarSituacaoPagamentoFaturaPorTipoBaixa(fatura, integracaoTituloBaixa.getTipoBaixa());

		recebimento.setCliente(fatura.getCliente());
		recebimento.setPontoConsumo(fatura.getPontoConsumo());

		ArrecadadorContratoConvenio arrecadadorContratoConvenio = controladorArrecadacao
				.obterArrecadadorContratoConvenio(fatura.getContrato().getArrecadadorContratoConvenio().getChavePrimaria());
		Arrecadador arrecadador = arrecadadorContratoConvenio.getArrecadadorContrato().getArrecadador();
		avisoBancario.setArrecadador(arrecadador);
		avisoBancario.setIndicadorCreditoDebito(true);
		avisoBancario.setValorContabilizado(BigDecimal.ZERO);
		avisoBancario.setValorRealizado(integracaoTituloBaixa.getValor());
		avisoBancario.setDataPrevista(new Date());
		avisoBancario.setDataRealizada(new Date());
		avisoBancario.setValorArrecadacaoCalculado(integracaoTituloBaixa.getValor());
		avisoBancario.setValorDevolucaoCalculado(BigDecimal.ZERO);
		avisoBancario.setValorArrecadacaoInformado(integracaoTituloBaixa.getValor());
		avisoBancario.setValorDevolucaoInformado(BigDecimal.ZERO);
		avisoBancario.setAnoMesContabil(fatura.getAnoMesReferencia());
		avisoBancario.setHabilitado(true);

		EntidadeConteudo tipoConvenio = fatura.getContrato().getFormaCobranca();
		Collection<ArrecadadorContratoConvenio> colecaoArrecadadorContratoConvenio =
				controladorArrecadacao.consultarACConvenioPorArrecadador(arrecadador.getChavePrimaria(), tipoConvenio.getChavePrimaria());
		if (colecaoArrecadadorContratoConvenio != null && !colecaoArrecadadorContratoConvenio.isEmpty()) {
			avisoBancario.setContaBancaria(colecaoArrecadadorContratoConvenio.iterator().next().getContaCredito());
		}

		DocumentoCobrancaItem documentoCobrancaItem =
				controladorDocumentoCobranca.consultarDocumentoCobrancaItemPelaFatura(fatura.getChavePrimaria()).iterator().next();

		recebimento.setDocumentoCobrancaItem(documentoCobrancaItem);
		recebimento.setAvisoBancario(avisoBancario);

		recebimento.setFormaArrecadacao(tipoConvenio);
		recebimento.setFaturaGeral(fatura.getFaturaGeral());
		recebimento.setValorRecebimento(integracaoTituloBaixa.getValor());
		recebimento.setDataRecebimento(integracaoTituloBaixa.getData());
		recebimento.setVersao(integracaoTituloBaixa.getVersao());
		recebimento.setHabilitado(true);
		recebimento.setUltimaAlteracao(new Date());
		recebimento.setAnoMesContabil(fatura.getAnoMesReferencia());
		recebimento.setBaixado(true);
		recebimento.setValorPrincipal(integracaoTituloBaixa.getValorPrincipal());
		recebimento.setValorJurosMulta(integracaoTituloBaixa.getValorJurosMulta());
		recebimento.setValorDescontos(integracaoTituloBaixa.getValorDescontos());
		recebimento.setValorAbatimento(integracaoTituloBaixa.getValorAbatimento());
		recebimento.setValorCredito(integracaoTituloBaixa.getValorCredito());
		recebimento.setValorCobranca(integracaoTituloBaixa.getValor());
		recebimento.setSequencial(integracaoTituloBaixa.getSequencial());

		return recebimento;
	}

	/**
	 * Metódo responsável por atualizar fatura a partir do tipo da baixa
	 *
	 * @param fatura
	 * @param tipoBaixa tipo da baixa: parcial ou total
	 */
	private void atualizarSituacaoPagamentoFaturaPorTipoBaixa(Fatura fatura, String tipoBaixa) throws GGASException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ConstanteSistema constanteTipoBaixaTotal =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_TOTAL);
		ConstanteSistema constanteTipoBaixaParcial =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULO_BAIXA_PARCIAL);

		EntidadeConteudo situacaoPagamento = null;
		if (tipoBaixa.equals(constanteTipoBaixaTotal.getValor())) {
			situacaoPagamento =
					controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.SITUACAO_PAGAMENTO_FATURA_QUITADO);
		} else if (tipoBaixa.equals(constanteTipoBaixaParcial.getValor())) {
			situacaoPagamento = controladorEntidadeConteudo
					.obterEntidadeConteudoPorConstanteSistema(Constantes.SITUACAO_PAGAMENTO_FATURA_PARCIALMENTE_QUITADO);
		}

		if (situacaoPagamento != null) {
			fatura.setSituacaoPagamento(situacaoPagamento);
		}
	}

}
