/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 07/06/2013 14:13:32
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Interface IntegracaoBemMedidor.
 */
public interface IntegracaoBemMedidor extends IntegracaoBem {

	/** The bean id integracao bem medidor. */
	String BEAN_ID_INTEGRACAO_BEM_MEDIDOR = "integracaoBemMedidor";

	/**
	 * Gets the tipo medidor.
	 *
	 * @return the tipo medidor
	 */
	Long getTipoMedidor();

	/**
	 * Sets the tipo medidor.
	 *
	 * @param tipoMedidor the new tipo medidor
	 */
	void setTipoMedidor(Long tipoMedidor);

	/**
	 * Gets the marca medidor.
	 *
	 * @return the marca medidor
	 */
	Long getMarcaMedidor();

	/**
	 * Sets the marca medidor.
	 *
	 * @param marcaMedidor the new marca medidor
	 */
	void setMarcaMedidor(Long marcaMedidor);

	/**
	 * Gets the modelo.
	 *
	 * @return the modelo
	 */
	Long getModelo();

	/**
	 * Sets the modelo.
	 *
	 * @param modelo the new modelo
	 */
	void setModelo(Long modelo);

	/**
	 * Gets the fator k.
	 *
	 * @return the fator k
	 */
	Long getFatorK();

	/**
	 * Sets the fator k.
	 *
	 * @param fatorK the new fator k
	 */
	void setFatorK(Long fatorK);

	/**
	 * Gets the diametro medidor.
	 *
	 * @return the diametro medidor
	 */
	Long getDiametroMedidor();

	/**
	 * Sets the diametro medidor.
	 *
	 * @param diametroMedidor the new diametro medidor
	 */
	void setDiametroMedidor(Long diametroMedidor);

	/**
	 * Gets the capacidade minima.
	 *
	 * @return the capacidade minima
	 */
	Long getCapacidadeMinima();

	/**
	 * Sets the capacidade minima.
	 *
	 * @param capacidadeMinima the new capacidade minima
	 */
	void setCapacidadeMinima(Long capacidadeMinima);

	/**
	 * Gets the capacidade maxima.
	 *
	 * @return the capacidade maxima
	 */
	Long getCapacidadeMaxima();

	/**
	 * Sets the capacidade maxima.
	 *
	 * @param capacidadeMaxima the new capacidade maxima
	 */
	void setCapacidadeMaxima(Long capacidadeMaxima);

	/* (non-Javadoc)
	 * @see br.com.ggas.integracao.bens.IntegracaoBem#getAnoFabricacao()
	 */
	@Override
	Long getAnoFabricacao();

	/* (non-Javadoc)
	 * @see br.com.ggas.integracao.bens.IntegracaoBem#setAnoFabricacao(java.lang.Long)
	 */
	@Override
	void setAnoFabricacao(Long anoFabricacao);

	/**
	 * Gets the data aquisicao.
	 *
	 * @return the data aquisicao
	 */
	Date getDataAquisicao();

	/**
	 * Sets the data aquisicao.
	 *
	 * @param dataAquisicao the new data aquisicao
	 */
	void setDataAquisicao(Date dataAquisicao);

	/**
	 * Gets the data maxima instalacao.
	 *
	 * @return the data maxima instalacao
	 */
	Date getDataMaximaInstalacao();

	/**
	 * Sets the data maxima instalacao.
	 *
	 * @param dataMaximaInstalacao the new data maxima instalacao
	 */
	void setDataMaximaInstalacao(Date dataMaximaInstalacao);

	/**
	 * Gets the data ultima calibracao.
	 *
	 * @return the data ultima calibracao
	 */
	Date getDataUltimaCalibracao();

	/**
	 * Sets the data ultima calibracao.
	 *
	 * @param dataUltimaCalibracao the new data ultima calibracao
	 */
	void setDataUltimaCalibracao(Date dataUltimaCalibracao);

	/**
	 * Gets the numero anos calibracao.
	 *
	 * @return the numero anos calibracao
	 */
	Long getNumeroAnosCalibracao();

	/**
	 * Sets the numero anos calibracao.
	 *
	 * @param numeroAnosCalibracao the new numero anos calibracao
	 */
	void setNumeroAnosCalibracao(Long numeroAnosCalibracao);

	/**
	 * Gets the numero tombamento.
	 *
	 * @return the numero tombamento
	 */
	String getNumeroTombamento();

	/**
	 * Sets the numero tombamento.
	 *
	 * @param numeroTombamento the new numero tombamento
	 */
	void setNumeroTombamento(String numeroTombamento);

	/**
	 * Gets the local armazenagem.
	 *
	 * @return the local armazenagem
	 */
	Long getLocalArmazenagem();

	/**
	 * Sets the local armazenagem.
	 *
	 * @param localArmazenagem the new local armazenagem
	 */
	void setLocalArmazenagem(Long localArmazenagem);

	/**
	 * Gets the faixa temperatura trabalho.
	 *
	 * @return the faixa temperatura trabalho
	 */
	Long getFaixaTemperaturaTrabalho();

	/**
	 * Sets the faixa temperatura trabalho.
	 *
	 * @param temperaturaTrabalho the new faixa temperatura trabalho
	 */
	void setFaixaTemperaturaTrabalho(Long temperaturaTrabalho);

	/**
	 * Gets the pressao maxima.
	 *
	 * @return the pressao maxima
	 */
	BigDecimal getPressaoMaxima();

	/**
	 * Sets the pressao maxima.
	 *
	 * @param pressaoMaxima the new pressao maxima
	 */
	void setPressaoMaxima(BigDecimal pressaoMaxima);

	/**
	 * Gets the unidade pressao maxima trabalho.
	 *
	 * @return the unidade pressao maxima trabalho
	 */
	Long getUnidadePressaoMaximaTrabalho();

	/**
	 * Sets the unidade pressao maxima trabalho.
	 *
	 * @param unidadePressaoMaxima the new unidade pressao maxima trabalho
	 */
	void setUnidadePressaoMaximaTrabalho(Long unidadePressaoMaxima);

}
