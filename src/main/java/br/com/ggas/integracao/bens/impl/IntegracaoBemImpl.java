/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:16:20
 @author vtavares
 */

package br.com.ggas.integracao.bens.impl;

import br.com.ggas.integracao.bens.IntegracaoBem;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;

/**
 * 
 * Classe Integracao Bem Impl.
 * 
 *
 */
public class IntegracaoBemImpl extends IntegracaoImpl implements IntegracaoBem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7874459283226924071L;

	private Long situacaoBem;

	private String tipoBem;

	private String numeroSerie;

	private Long numeroDigito;

	private Long bemOrigem;

	private Long bemDestino;

	private Long anoFabricacao;

	/**
	 * @return the situacaoBem
	 */
	@Override
	public Long getSituacaoBem() {

		return situacaoBem;
	}

	/**
	 * @param situacaoBem
	 *            the situacaoBem to set
	 */
	@Override
	public void setSituacaoBem(Long situacaoBem) {

		this.situacaoBem = situacaoBem;
	}

	/**
	 * @return the tipoBem
	 */
	@Override
	public String getTipoBem() {

		return tipoBem;
	}

	/**
	 * @param tipoBem
	 *            the tipoBem to set
	 */
	@Override
	public void setTipoBem(String tipoBem) {

		this.tipoBem = tipoBem;
	}

	/**
	 * @return the numeroSerie
	 */
	@Override
	public String getNumeroSerie() {

		return numeroSerie;
	}

	/**
	 * @param numeroSerie
	 *            the numeroSerie to set
	 */
	@Override
	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	/**
	 * @return the numeroDigito
	 */
	@Override
	public Long getNumeroDigito() {

		return numeroDigito;
	}

	/**
	 * @param numeroDigito
	 *            the numeroDigito to set
	 */
	@Override
	public void setNumeroDigito(Long numeroDigito) {

		this.numeroDigito = numeroDigito;
	}

	/**
	 * @return the bemOrigem
	 */
	@Override
	public Long getBemOrigem() {

		return bemOrigem;
	}

	/**
	 * @param bemOrigem
	 *            the bemOrigem to set
	 */
	@Override
	public void setBemOrigem(Long bemOrigem) {

		this.bemOrigem = bemOrigem;
	}

	/**
	 * @return the bemDestino
	 */
	@Override
	public Long getBemDestino() {

		return bemDestino;
	}

	/**
	 * @param bemDestino
	 *            the bemDestino to set
	 */
	@Override
	public void setBemDestino(Long bemDestino) {

		this.bemDestino = bemDestino;
	}

	/**
	 * @return the anoFabricacao
	 */
	@Override
	public Long getAnoFabricacao() {

		return anoFabricacao;
	}

	/**
	 * @param anoFabricacao
	 *            the anoFabricacao to set
	 */
	@Override
	public void setAnoFabricacao(Long anoFabricacao) {

		this.anoFabricacao = anoFabricacao;
	}

}
