/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens;

import br.com.ggas.integracao.geral.Integracao;
/**
 * 
 * Iterface responsável pela assinatura dos métodos relacionados
 * a IntegracaoBemImpl
 *
 */
public interface IntegracaoBem extends Integracao {

	String BEAN_ID_INTEGRACAO_BEM = "integracaoBem";

	/**
	 * @return Long - Retorna Situação de bem.
	 */
	Long getSituacaoBem();

	/**
	 * @param situacaoBem - Set situação de bem.
	 */
	void setSituacaoBem(Long situacaoBem);

	/**
	 * @return String - Retorna Tipo Bem.
	 */
	String getTipoBem();

	/**
	 * @param tipoBem - Set Tipo bem.
	 */
	void setTipoBem(String tipoBem);

	/**
	 * @return Retorna Numero Serie
	 */
	String getNumeroSerie();

	/**
	 * @param numeroSerie - Set Número de série.
	 */
	void setNumeroSerie(String numeroSerie);

	/**
	 * @return Long - Set número de série.
	 */
	Long getNumeroDigito();

	/**
	 * @param numeroDigito - Set número de digito.
	 */
	void setNumeroDigito(Long numeroDigito);

	/**
	 * @return Long - Retorna Bem Origem.
	 */
	Long getBemOrigem();

	/**
	 * @param bemOrigem - Set bem origem.. 
	 */
	void setBemOrigem(Long bemOrigem);

	/**
	 * @return Long - Retorna Bem Destino.
	 */
	Long getBemDestino();

	/**
	 * @param bemDestino - Set bem destino.
	 */
	void setBemDestino(Long bemDestino);

	/**
	 * @return Long - Retorna Ano de Fabricação.
	 */
	Long getAnoFabricacao();

	/**
	 * @param anoFabricacao - Set Ano de fabricação
	 */
	void setAnoFabricacao(Long anoFabricacao);
}
