/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/01/2013 12:08:57
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens;

import java.util.Date;

import br.com.ggas.integracao.geral.Integracao;


/**
 * Interface Integracao Bem Movimentacao.
 *
 */
public interface IntegracaoBemMovimentacao extends Integracao {

	String BEAN_ID_INTEGRACAO_BEM_MOVIMENTACAO = "integracaoBemMovimentacao";

	/**
	 * @return Long - Retorna situação bem.
	 */
	Long getSituacaoBem();

	/**
	 * @param situacaoBem - Set situação bem.
	 */
	void setSituacaoBem(Long situacaoBem);

	/**
	 * @return Long - Retorna código da movimentação
	 */
	Long getCodigoMovimentacao();

	/**
	 * @param codigoMovimentacao - Set código da movimentação.
	 */
	void setCodigoMovimentacao(Long codigoMovimentacao);

	/**
	 * @return Date - Retorna data da operação.
	 */
	Date getDataOperacao();

	/**
	 * @param dataOperacao - Set data da operação.	
	 */
	void setDataOperacao(Date dataOperacao);

	/**
	 * @return IntegracaoBem - Retorna bem.
	 */
	IntegracaoBem getBem();

	/**
	 * @param bem  - Set bem.
	 */
	void setBem(IntegracaoBem bem);

	/**
	 * @return String - Retorna descrição.
	 */
	String getDescricao();

	/**
	 * @param descricao  - Set descrição.
	 */
	void setDescricao(String descricao);

	/**
	 * @return Long - Retorna motivo da movimentação.
	 */
	Long getMotivoMovimentacao();

	/**
	 * @param motivoMovimentacao - Set motivo da movimentação.
	 */
	void setMotivoMovimentacao(Long motivoMovimentacao);

	/**
	 * @return Long - Retorna local armazenagem destino.
	 */
	Long getLocalArmazenagemDestino();

	/**
	 * @param localArmazenagemDestino - Set local armazenagem Destino. 
	 */
	void setLocalArmazenagemDestino(Long localArmazenagemDestino);

	/**
	 * @return Long - Retorna Funcionário. 
	 */
	Long getFuncionario();

	/**
	 * @param funcionario - Set funcionário. 
	 */
	void setFuncionario(Long funcionario);

	/**
	 * @return String - Set descrição destino.
	 */
	String getDescricaoDestino();

	/**
	 * @param descricaoDestino Set descrição destino.
	 */
	void setDescricaoDestino(String descricaoDestino);

	/**
	 * @return Long - Retorna local armazenagem de origem.
	 */
	Long getLocalArmazenagemOrigem();

	/**
	 * @param localArmazenagemOrigem - Set Local armazenagem de origem.
	 */
	void setLocalArmazenagemOrigem(Long localArmazenagemOrigem);
}
