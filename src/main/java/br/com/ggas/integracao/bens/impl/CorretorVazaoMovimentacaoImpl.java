/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de CorretorVazaoMovimentacaoImpl
 @since 05/06/2013 10:22:57
 @author vpessoa
 */

package br.com.ggas.integracao.bens.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.integracao.bens.CorretorVazaoMovimentacao;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
/**
 * 
 * Classe responsável pela implementação dos métodos relacionados 
 * a movimentação do corretor de vazão
 *
 */
public class CorretorVazaoMovimentacaoImpl extends EntidadeNegocioImpl implements CorretorVazaoMovimentacao {

	private static final long serialVersionUID = 7496348328770560131L;

	private Date dataMovimento;

	private VazaoCorretor corretorVazao;

	private MotivoMovimentacaoMedidor motivoMovimentacao;

	private String descricaoParecer;

	private MedidorLocalArmazenagem localArmazenagemOrigem;

	private MedidorLocalArmazenagem localArmazenagemDestino;

	private Funcionario funcionario;

	@Override
	public Date getDataMovimento() {
		Date data = null;
		if(this.dataMovimento != null) {
			data = (Date) dataMovimento.clone();
		}
		return data;
	}

	@Override
	public void setDataMovimento(Date dataMovimento) {
		if (dataMovimento != null) {
			this.dataMovimento = (Date) dataMovimento.clone();
		} else {
			this.dataMovimento = null;
		}
	}

	@Override
	public VazaoCorretor getCorretorVazao() {

		return corretorVazao;
	}

	@Override
	public void setCorretorVazao(VazaoCorretor corretorVazao) {

		this.corretorVazao = corretorVazao;
	}

	@Override
	public MotivoMovimentacaoMedidor getMotivoMovimentacao() {

		return motivoMovimentacao;
	}

	@Override
	public void setMotivoMovimentacao(MotivoMovimentacaoMedidor motivoMovimentacao) {

		this.motivoMovimentacao = motivoMovimentacao;
	}

	@Override
	public String getDescricaoParecer() {

		return descricaoParecer;
	}

	@Override
	public void setDescricaoParecer(String descricaoParecer) {

		this.descricaoParecer = descricaoParecer;
	}

	@Override
	public MedidorLocalArmazenagem getLocalArmazenagemOrigem() {

		return localArmazenagemOrigem;
	}

	@Override
	public void setLocalArmazenagemOrigem(MedidorLocalArmazenagem localArmazenagemOrigem) {

		this.localArmazenagemOrigem = localArmazenagemOrigem;
	}

	@Override
	public MedidorLocalArmazenagem getLocalArmazenagemDestino() {

		return localArmazenagemDestino;
	}

	@Override
	public void setLocalArmazenagemDestino(MedidorLocalArmazenagem localArmazenagemDestino) {

		this.localArmazenagemDestino = localArmazenagemDestino;
	}

	@Override
	public Funcionario getFuncionario() {

		return funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
