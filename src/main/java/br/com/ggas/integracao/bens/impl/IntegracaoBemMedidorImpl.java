/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2013 09:48:05
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.integracao.bens.IntegracaoBemMedidor;

/**
 * 
 * Classe Integracao Bem Medidor.
 * 
 *
 */
public class IntegracaoBemMedidorImpl extends IntegracaoBemImpl implements IntegracaoBemMedidor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long tipoMedidor;

	private Long marcaMedidor;

	private Long modelo;

	private Long fatorK;

	private Long diametroMedidor;

	private Long capacidadeMinima;

	private Long capacidadeMaxima;

	private Date dataAquisicao;

	private Date dataMaximaInstalacao;

	private Date dataUltimaCalibracao;

	private Long numeroAnosCalibracao;

	private String numeroTombamento;

	private Long localArmazenagem;

	private Long faixaTemperaturaTrabalho;

	private BigDecimal pressaoMaxima;

	private Long unidadePressaoMaximaTrabalho;

	/**
	 * @return the tipoMedidor
	 */
	@Override
	public Long getTipoMedidor() {

		return tipoMedidor;
	}

	/**
	 * @param tipoMedidor
	 *            the tipoMedidor to set
	 */
	@Override
	public void setTipoMedidor(Long tipoMedidor) {

		this.tipoMedidor = tipoMedidor;
	}

	/**
	 * @return the marcaMedidor
	 */
	@Override
	public Long getMarcaMedidor() {

		return marcaMedidor;
	}

	/**
	 * @param marcaMedidor
	 *            the marcaMedidor to set
	 */
	@Override
	public void setMarcaMedidor(Long marcaMedidor) {

		this.marcaMedidor = marcaMedidor;
	}

	/**
	 * @return the modeloMedidor
	 */
	@Override
	public Long getModelo() {

		return modelo;
	}

	/**
	 * @param modeloMedidor
	 *            the modeloMedidor to set
	 */
	@Override
	public void setModelo(Long modelo) {

		this.modelo = modelo;
	}

	/**
	 * @return the fatorK
	 */
	@Override
	public Long getFatorK() {

		return fatorK;
	}

	/**
	 * @param fatorK
	 *            the fatorK to set
	 */
	@Override
	public void setFatorK(Long fatorK) {

		this.fatorK = fatorK;
	}

	/**
	 * @return the diametroMedidor
	 */
	@Override
	public Long getDiametroMedidor() {

		return diametroMedidor;
	}

	/**
	 * @param diametroMedidor
	 *            the diametroMedidor to set
	 */
	@Override
	public void setDiametroMedidor(Long diametroMedidor) {

		this.diametroMedidor = diametroMedidor;
	}

	/**
	 * @return the capacidadeMinima
	 */
	@Override
	public Long getCapacidadeMinima() {

		return capacidadeMinima;
	}

	/**
	 * @param capacidadeMinima
	 *            the capacidadeMinima to set
	 */
	@Override
	public void setCapacidadeMinima(Long capacidadeMinima) {

		this.capacidadeMinima = capacidadeMinima;
	}

	/**
	 * @return the capacidadeMaxima
	 */
	@Override
	public Long getCapacidadeMaxima() {

		return capacidadeMaxima;
	}

	/**
	 * @param capacidadeMaxima
	 *            the capacidadeMaxima to set
	 */
	@Override
	public void setCapacidadeMaxima(Long capacidadeMaxima) {

		this.capacidadeMaxima = capacidadeMaxima;
	}

	/**
	 * @return the dataAquisicao
	 */
	@Override
	public Date getDataAquisicao() {
		Date data = null;
		if (this.dataAquisicao != null) {
			data = (Date) dataAquisicao.clone();
		}
		return data;
	}

	/**
	 * @param dataAquisicao
	 *            the dataAquisicao to set
	 */
	@Override
	public void setDataAquisicao(Date dataAquisicao) {
		if (dataAquisicao != null) {
			this.dataAquisicao = (Date) dataAquisicao.clone();
		} else {
			this.dataAquisicao = null;
		}
	}

	/**
	 * @return the dataMaximaInstalacao
	 */
	@Override
	public Date getDataMaximaInstalacao() {
		Date data = null;
		if (dataMaximaInstalacao != null) {
			data = (Date) dataMaximaInstalacao.clone();
		}
		return data;
	}

	/**
	 * @param dataMaximaInstalacao
	 *            the dataMaximaInstalacao to set
	 */
	@Override
	public void setDataMaximaInstalacao(Date dataMaximaInstalacao) {
		if (dataMaximaInstalacao != null) {
			this.dataMaximaInstalacao = (Date) dataMaximaInstalacao.clone();
		} else {
			this.dataMaximaInstalacao = null;
		}
	}

	/**
	 * @return the dataUltimaCalibracao
	 */
	@Override
	public Date getDataUltimaCalibracao() {
		Date data = null;
		if(dataUltimaCalibracao != null) {
			data = (Date) dataUltimaCalibracao.clone();
		}
		return data;
	}

	/**
	 * @param dataUltimaCalibracao
	 *            the dataUltimaCalibracao to set
	 */
	@Override
	public void setDataUltimaCalibracao(Date dataUltimaCalibracao) {
		if(dataUltimaCalibracao != null) {
			this.dataUltimaCalibracao = (Date) dataUltimaCalibracao.clone();
		} else {
			this.dataUltimaCalibracao = null;
		}
	}

	/**
	 * @return the numeroAnosCalibracao
	 */
	@Override
	public Long getNumeroAnosCalibracao() {

		return numeroAnosCalibracao;
	}

	/**
	 * @param numeroAnosCalibracao
	 *            the numeroAnosCalibracao to set
	 */
	@Override
	public void setNumeroAnosCalibracao(Long numeroAnosCalibracao) {

		this.numeroAnosCalibracao = numeroAnosCalibracao;
	}

	/**
	 * @return the numeroTombamento
	 */
	@Override
	public String getNumeroTombamento() {

		return numeroTombamento;
	}

	/**
	 * @param numeroTombamento
	 *            the numeroTombamento to set
	 */
	@Override
	public void setNumeroTombamento(String numeroTombamento) {

		this.numeroTombamento = numeroTombamento;
	}

	/**
	 * @return the localArmazenagem
	 */
	@Override
	public Long getLocalArmazenagem() {

		return localArmazenagem;
	}

	/**
	 * @param localArmazenagem
	 *            the localArmazenagem to set
	 */
	@Override
	public void setLocalArmazenagem(Long localArmazenagem) {

		this.localArmazenagem = localArmazenagem;
	}

	/**
	 * @return the faixaTemperaturaTrabalho
	 */
	@Override
	public Long getFaixaTemperaturaTrabalho() {

		return faixaTemperaturaTrabalho;
	}

	/**
	 * @param faixaTemperaturaTrabalho
	 *            the faixaTemperaturaTrabalho to set
	 */
	@Override
	public void setFaixaTemperaturaTrabalho(Long faixaTemperaturaTrabalho) {

		this.faixaTemperaturaTrabalho = faixaTemperaturaTrabalho;
	}

	/**
	 * @return the pressaoMaxima
	 */
	@Override
	public BigDecimal getPressaoMaxima() {

		return pressaoMaxima;
	}

	/**
	 * @param pressaoMaxima
	 *            the pressaoMaxima to set
	 */
	@Override
	public void setPressaoMaxima(BigDecimal pressaoMaxima) {

		this.pressaoMaxima = pressaoMaxima;
	}

	/**
	 * @return the unidadePressaoMaximaTrabalho
	 */
	@Override
	public Long getUnidadePressaoMaximaTrabalho() {

		return unidadePressaoMaximaTrabalho;
	}

	/**
	 * @param unidadePressaoMaximaTrabalho
	 *            the unidadePressaoMaximaTrabalho to set
	 */
	@Override
	public void setUnidadePressaoMaximaTrabalho(Long unidadePressaoMaximaTrabalho) {

		this.unidadePressaoMaximaTrabalho = unidadePressaoMaximaTrabalho;
	}

}
