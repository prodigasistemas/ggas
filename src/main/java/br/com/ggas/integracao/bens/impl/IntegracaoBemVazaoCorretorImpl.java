/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2013 09:48:33
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens.impl;

import br.com.ggas.integracao.bens.IntegracaoBemVazaoCorretor;

/**
 * Classe Integracao Bem Vazao Corretor.
 * 
 *
 */
public class IntegracaoBemVazaoCorretorImpl extends IntegracaoBemImpl implements IntegracaoBemVazaoCorretor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long marcaCorretor;

	private Long indicadorTipoMostrador;

	private Long indicadorCorrecaoPressao;

	private Long indicadorCorrecaoTemperatura;

	private Long indicadorControleVazao;

	private Long indicadorLinearizacaoFatorK;

	private Long protocoloComunicacao;

	private Long tipoTransdutorPressao;

	private Long tipoTransdutorTemperatura;

	private Long temperaturaMaximaTransdutor;

	private Long pressaoMaximaTransdutor;

	private Long modelo;

	private String numeroTombamento;

	private Long localArmazenagem;

	/**
	 * @return the marcaCorretor
	 */
	@Override
	public Long getMarcaCorretor() {

		return marcaCorretor;
	}

	/**
	 * @param marcaCorretor
	 *            the marcaCorretor to set
	 */
	@Override
	public void setMarcaCorretor(Long marcaCorretor) {

		this.marcaCorretor = marcaCorretor;
	}

	/**
	 * @return the indicadorTipoMostrador
	 */
	@Override
	public Long getIndicadorTipoMostrador() {

		return indicadorTipoMostrador;
	}

	/**
	 * @param indicadorTipoMostrador
	 *            the indicadorTipoMostrador to set
	 */
	@Override
	public void setIndicadorTipoMostrador(Long indicadorTipoMostrador) {

		this.indicadorTipoMostrador = indicadorTipoMostrador;
	}

	/**
	 * @return the indicadorCorrecaoPressao
	 */
	@Override
	public Long getIndicadorCorrecaoPressao() {

		return indicadorCorrecaoPressao;
	}

	/**
	 * @param indicadorCorrecaoPressao
	 *            the indicadorCorrecaoPressao to set
	 */
	@Override
	public void setIndicadorCorrecaoPressao(Long indicadorCorrecaoPressao) {

		this.indicadorCorrecaoPressao = indicadorCorrecaoPressao;
	}

	/**
	 * @return the indicadorCorrecaoTemperatura
	 */
	@Override
	public Long getIndicadorCorrecaoTemperatura() {

		return indicadorCorrecaoTemperatura;
	}

	/**
	 * @param indicadorCorrecaoTemperatura
	 *            the indicadorCorrecaoTemperatura to set
	 */
	@Override
	public void setIndicadorCorrecaoTemperatura(Long indicadorCorrecaoTemperatura) {

		this.indicadorCorrecaoTemperatura = indicadorCorrecaoTemperatura;
	}

	/**
	 * @return the indicadorControleVazao
	 */
	@Override
	public Long getIndicadorControleVazao() {

		return indicadorControleVazao;
	}

	/**
	 * @param indicadorControleVazao
	 *            the indicadorControleVazao to set
	 */
	@Override
	public void setIndicadorControleVazao(Long indicadorControleVazao) {

		this.indicadorControleVazao = indicadorControleVazao;
	}

	/**
	 * @return the indicadorLinearizacaoFatorK
	 */
	@Override
	public Long getIndicadorLinearizacaoFatorK() {

		return indicadorLinearizacaoFatorK;
	}

	/**
	 * @param indicadorLinearizacaoFatorK
	 *            the indicadorLinearizacaoFatorK to set
	 */
	@Override
	public void setIndicadorLinearizacaoFatorK(Long indicadorLinearizacaoFatorK) {

		this.indicadorLinearizacaoFatorK = indicadorLinearizacaoFatorK;
	}

	/**
	 * @return the protocoloComunicacao
	 */
	@Override
	public Long getProtocoloComunicacao() {

		return protocoloComunicacao;
	}

	/**
	 * @param protocoloComunicacao
	 *            the protocoloComunicacao to set
	 */
	@Override
	public void setProtocoloComunicacao(Long protocoloComunicacao) {

		this.protocoloComunicacao = protocoloComunicacao;
	}

	/**
	 * @return the tipoTransdutorPressao
	 */
	@Override
	public Long getTipoTransdutorPressao() {

		return tipoTransdutorPressao;
	}

	/**
	 * @param tipoTransdutorPressao
	 *            the tipoTransdutorPressao to set
	 */
	@Override
	public void setTipoTransdutorPressao(Long tipoTransdutorPressao) {

		this.tipoTransdutorPressao = tipoTransdutorPressao;
	}

	/**
	 * @return the tipoTransdutorTemperatura
	 */
	@Override
	public Long getTipoTransdutorTemperatura() {

		return tipoTransdutorTemperatura;
	}

	/**
	 * @param tipoTransdutorTemperatura
	 *            the tipoTransdutorTemperatura to set
	 */
	@Override
	public void setTipoTransdutorTemperatura(Long tipoTransdutorTemperatura) {

		this.tipoTransdutorTemperatura = tipoTransdutorTemperatura;
	}

	/**
	 * @return the temperaturaMaximaTransdutor
	 */
	@Override
	public Long getTemperaturaMaximaTransdutor() {

		return temperaturaMaximaTransdutor;
	}

	/**
	 * @param temperaturaMaximaTransdutor
	 *            the temperaturaMaximaTransdutor to set
	 */
	@Override
	public void setTemperaturaMaximaTransdutor(Long temperaturaMaximaTransdutor) {

		this.temperaturaMaximaTransdutor = temperaturaMaximaTransdutor;
	}

	/**
	 * @return the pressaoMaximaTransdutor
	 */
	@Override
	public Long getPressaoMaximaTransdutor() {

		return pressaoMaximaTransdutor;
	}

	/**
	 * @param pressaoMaximaTransdutor
	 *            the pressaoMaximaTransdutor to set
	 */
	@Override
	public void setPressaoMaximaTransdutor(Long pressaoMaximaTransdutor) {

		this.pressaoMaximaTransdutor = pressaoMaximaTransdutor;
	}

	/**
	 * @return the modelo
	 */
	@Override
	public Long getModelo() {

		return modelo;
	}

	/**
	 * @param modelo
	 *            the modelo to set
	 */
	@Override
	public void setModelo(Long modelo) {

		this.modelo = modelo;
	}

	@Override
	public String getNumeroTombamento() {

		return numeroTombamento;
	}

	@Override
	public void setNumeroTombamento(String numeroTombamento) {

		this.numeroTombamento = numeroTombamento;
	}

	@Override
	public Long getLocalArmazenagem() {

		return localArmazenagem;
	}

	@Override
	public void setLocalArmazenagem(Long localArmazenagem) {

		this.localArmazenagem = localArmazenagem;
	}

}
