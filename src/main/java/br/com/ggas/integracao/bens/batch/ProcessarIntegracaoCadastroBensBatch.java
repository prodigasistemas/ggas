/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2013 10:37:43
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.bens.IntegracaoBem;
import br.com.ggas.integracao.bens.impl.ClasseGenerica;
import br.com.ggas.integracao.bens.impl.IntegracaoBemImpl;
import br.com.ggas.integracao.bens.impl.IntegracaoBemMedidorImpl;
import br.com.ggas.integracao.bens.impl.IntegracaoBemVazaoCorretorImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.geral.ResumoIntegracaoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe Processar Integracao Cadastro Bens Batch.
 * 
 *
 */
@Component
public class ProcessarIntegracaoCadastroBensBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();
		Map<String, Object[]> mapaInconsistenciasVazaoCorretor = new LinkedHashMap<String, Object[]>();
		Map<String, Object[]> mapaInconsistenciasMedidor = new LinkedHashMap<String, Object[]>();
		ResumoIntegracaoVO resumoIntegracaoVOMedidor = new ResumoIntegracaoVO();
		ResumoIntegracaoVO resumoIntegracaoVOCorretor = new ResumoIntegracaoVO();

		try {

			ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			ConstanteSistema constanteCadastroBens = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_CADASTRO_BENS);

			ConstanteSistema constanteSistemaSituacaoNaoProcessado = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

			ConstanteSistema constanteOperacaoInclusao = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

			ConstanteSistema constanteOperacaoAlteracao = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_ALTERACAO);

			ConstanteSistema constanteOperacaoExclusao = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_EXCLUSAO);

			List<String> listaSistemasIntegrantes = controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long
							.valueOf(constanteCadastroBens.getValor()));

			processo.setEmailResponsavel(Util.concatenaEmails(processo.getEmailResponsavel(),
							controladorIntegracao.buscarEmailDestinatarioIntegracao(Long.valueOf(constanteCadastroBens.getValor()))));

			if(listaSistemasIntegrantes != null && !listaSistemasIntegrantes.isEmpty()) {

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("chavesSistemasIntegrantes", listaSistemasIntegrantes.toArray());
				filtro.put("habilitado", Boolean.TRUE);
				filtro.put("naoProcessado", constanteSistemaSituacaoNaoProcessado.getValor());

				String ordenacao = "integracao.class asc, integracao.operacao asc";

				Collection<IntegracaoBem> listaIntegracaoBem = (Collection<IntegracaoBem>) (Collection<?>) controladorIntegracao
								.consultarIntegracao(filtro, IntegracaoBemImpl.class, ordenacao, "");

				if(!listaIntegracaoBem.isEmpty()) {
					logProcessamento.append(Constantes.PULA_LINHA).append("[")
							.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
							.append("] ").append("Iniciando o tratamento dos registros da integração no cadastro de bens.");
					for (int i = 0; i < listaIntegracaoBem.size(); i++) {
						IntegracaoBem integracao = (IntegracaoBem) ((List) listaIntegracaoBem).get(i);
						if(integracao instanceof IntegracaoBemVazaoCorretorImpl) {

							// inclusao
							if(integracao.getOperacao().equals(constanteOperacaoInclusao.getValor())) {
								if(controladorIntegracao.validarInsercaoVazaoCorretor((IntegracaoBemVazaoCorretorImpl) integracao,
												mapaInconsistenciasVazaoCorretor)) {
									resumoIntegracaoVOCorretor.setQuantidadeRegistrosIncluidos(resumoIntegracaoVOCorretor
													.getQuantidadeRegistrosIncluidos() + 1);
								} else {
									resumoIntegracaoVOCorretor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOCorretor
													.getQuantidadeRegistrosInconsistentes() + 1);
								}
								// alteracao
							} else if(integracao.getOperacao().equals(constanteOperacaoAlteracao.getValor())) {
								if(controladorIntegracao.validarAlteracaoVazaoCorretor((IntegracaoBemVazaoCorretorImpl) integracao,
												mapaInconsistenciasVazaoCorretor)) {
									resumoIntegracaoVOCorretor.setQuantidadeRegistrosAlterados(resumoIntegracaoVOCorretor
													.getQuantidadeRegistrosAlterados() + 1);
								} else {
									resumoIntegracaoVOCorretor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOCorretor
													.getQuantidadeRegistrosInconsistentes() + 1);
								}
								// exclusao
							} else if(integracao.getOperacao().equals(constanteOperacaoExclusao.getValor())) {
								if(controladorIntegracao.validarExclusaoVazaoCorretor((IntegracaoBemVazaoCorretorImpl) integracao,
												mapaInconsistenciasVazaoCorretor)) {
									resumoIntegracaoVOCorretor.setQuantidadeRegistrosExcluidos(resumoIntegracaoVOCorretor
													.getQuantidadeRegistrosExcluidos() + 1);
								} else {
									resumoIntegracaoVOCorretor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOCorretor
													.getQuantidadeRegistrosInconsistentes() + 1);
								}
							} else {
								Util.atualizaMapaInconsistencias(mapaInconsistenciasVazaoCorretor, Util.concatenaMensagemErro(
												Constantes.ERRO_NEGOCIO_OPERACAO_INVALIDA, Constantes.OPERACAOINVALIDA));
								controladorIntegracao.atualizarIntegracaoComErro(IntegracaoBemVazaoCorretorImpl.class, integracao,
												Constantes.OPERACAOINVALIDA);
								resumoIntegracaoVOCorretor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOCorretor
												.getQuantidadeRegistrosInconsistentes() + 1);
							}
						} else if(integracao instanceof IntegracaoBemMedidorImpl) {
							// insercao
							if(integracao.getOperacao().equals(constanteOperacaoInclusao.getValor())) {
								if(controladorIntegracao.validarInsercaoMedidor((IntegracaoBemMedidorImpl) integracao,
												mapaInconsistenciasMedidor)) {
									resumoIntegracaoVOMedidor.setQuantidadeRegistrosIncluidos(resumoIntegracaoVOMedidor
													.getQuantidadeRegistrosIncluidos() + 1);
								} else {
									resumoIntegracaoVOMedidor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOMedidor
													.getQuantidadeRegistrosInconsistentes() + 1);
								}// alteracao
							} else if(integracao.getOperacao().equals(constanteOperacaoAlteracao.getValor())) {
								if(controladorIntegracao.validarAlteracaoMedidor((IntegracaoBemMedidorImpl) integracao,
												mapaInconsistenciasMedidor)) {
									resumoIntegracaoVOMedidor.setQuantidadeRegistrosAlterados(resumoIntegracaoVOMedidor
													.getQuantidadeRegistrosAlterados() + 1);
								} else {
									resumoIntegracaoVOMedidor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOMedidor
													.getQuantidadeRegistrosInconsistentes() + 1);
								}
								// exclusao
							} else if(integracao.getOperacao().equals(constanteOperacaoExclusao.getValor())) {
								if(controladorIntegracao.validarExclusaoMedidor((IntegracaoBemMedidorImpl) integracao,
												mapaInconsistenciasMedidor)) {
									resumoIntegracaoVOMedidor.setQuantidadeRegistrosExcluidos(resumoIntegracaoVOMedidor
													.getQuantidadeRegistrosExcluidos() + 1);
								} else {
									resumoIntegracaoVOMedidor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOMedidor
													.getQuantidadeRegistrosInconsistentes() + 1);
								}
							} else {
								Util.atualizaMapaInconsistencias(mapaInconsistenciasMedidor, Util.concatenaMensagemErro(
												Constantes.ERRO_NEGOCIO_OPERACAO_INVALIDA, Constantes.OPERACAOINVALIDA));
								controladorIntegracao.atualizarIntegracaoComErro(IntegracaoBemMedidorImpl.class, integracao,
												Constantes.OPERACAOINVALIDA);
								resumoIntegracaoVOMedidor.setQuantidadeRegistrosInconsistentes(resumoIntegracaoVOMedidor
												.getQuantidadeRegistrosInconsistentes() + 1);
							}

						} else if(integracao instanceof ClasseGenerica) {
							controladorIntegracao.atualizarIntegracaoComErro(ClasseGenerica.class, integracao, "Tipo de Bem inválido");
						}

					}
					resumoIntegracaoVOCorretor.setQuantidadeRegistrosProcessados(resumoIntegracaoVOCorretor
									.getQuantidadeRegistrosAlterados()
									+ resumoIntegracaoVOCorretor.getQuantidadeRegistrosIncluidos()
									+ resumoIntegracaoVOCorretor.getQuantidadeRegistrosExcluidos()
									+ resumoIntegracaoVOCorretor.getQuantidadeRegistrosInconsistentes());
					resumoIntegracaoVOCorretor.setMapaInconsistencias(mapaInconsistenciasVazaoCorretor);
					resumoIntegracaoVOMedidor.setQuantidadeRegistrosProcessados(resumoIntegracaoVOMedidor.getQuantidadeRegistrosAlterados()
									+ resumoIntegracaoVOMedidor.getQuantidadeRegistrosIncluidos()
									+ resumoIntegracaoVOMedidor.getQuantidadeRegistrosExcluidos()
									+ resumoIntegracaoVOMedidor.getQuantidadeRegistrosInconsistentes());
					resumoIntegracaoVOMedidor.setMapaInconsistencias(mapaInconsistenciasMedidor);
					if (resumoIntegracaoVOCorretor.getQuantidadeRegistrosProcessados() > 0 || !mapaInconsistenciasVazaoCorretor.isEmpty()) {
						logProcessamento.append(Constantes.PULA_LINHA).append("[")
								.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
								.append("] ")
								.append("Iniciando o tratamento dos registros da integração no cadastro de corretores de vazão.");
						controladorIntegracao.preencherLogMapaInconsistencias(logProcessamento, resumoIntegracaoVOCorretor, true);
						logProcessamento.append(Constantes.PULA_LINHA).append("[")
								.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
								.append("] ")
								.append("Finalizando o tratamento dos registros da integração no cadastro de corretores de vazão.");
					}
					if (resumoIntegracaoVOMedidor.getQuantidadeRegistrosProcessados() > 0 || !mapaInconsistenciasMedidor.isEmpty()) {
						logProcessamento.append(Constantes.PULA_LINHA).append("[")
								.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
								.append("] ").append("Iniciando o tratamento dos registros da integração no cadastro de medidores.");
						controladorIntegracao.preencherLogMapaInconsistencias(logProcessamento, resumoIntegracaoVOMedidor, true);
						logProcessamento.append(Constantes.PULA_LINHA).append("[")
								.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
								.append("] ").append("Finalizando o tratamento dos registros da integração no cadastro de medidores.");
					}
					logProcessamento.append(Constantes.PULA_LINHA).append("[")
							.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(),  true))
							.append("] ").append("Finalizando o tratamento dos registros da integração no cadastro de bens.");
				} else {
					logProcessamento.append(Constantes.PULA_LINHA).append("[")
							.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
							.append("] ").append(Constantes.ERRO_NEGOCIO_SEM_DADOS + Constantes.PULA_LINHA);
				}

			} else {
				logProcessamento.append(Constantes.PULA_LINHA).append("[")
						.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
						.append("] ").append("Integração de cadastro de bens inativa.");
			}

		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

}
