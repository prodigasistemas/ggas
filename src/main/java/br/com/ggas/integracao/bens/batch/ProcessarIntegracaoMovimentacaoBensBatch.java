/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2013 10:38:41
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.bens.IntegracaoBemMovimentacao;
import br.com.ggas.integracao.bens.impl.IntegracaoBemMovimentacaoImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Classe Processar Integracao Movimentacao Bens Batch.
 * 
 *
 */
@Component
public class ProcessarIntegracaoMovimentacaoBensBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		try {

			ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			ConstanteSistema constanteMovimentacaoBens = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

			ConstanteSistema constanteSistemaSituacaoNaoProcessado = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

			List<String> listaSistemasIntegrantes = controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long
							.valueOf(constanteMovimentacaoBens.getValor()));

			if (listaSistemasIntegrantes != null && !listaSistemasIntegrantes.isEmpty()) {

				logProcessamento.append(Constantes.PULA_LINHA).append("[")
						.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
						.append("] ").append("Iniciando o tratamento dos registros da integração na movimentação de bens.")
						.append(Constantes.PULA_LINHA);

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("chavesSistemasIntegrantes", listaSistemasIntegrantes.toArray());
				filtro.put("habilitado", Boolean.TRUE);
				filtro.put("naoProcessado", constanteSistemaSituacaoNaoProcessado.getValor());

				String ordenacao = "bem.class asc, bem.bemDestino asc, integracao.dataOperacao asc";

				Collection<IntegracaoBemMovimentacao> listaIntegracaoBemMovimentacao = (Collection<IntegracaoBemMovimentacao>) 
								(Collection<?>) controladorIntegracao
								.consultarIntegracao(filtro, IntegracaoBemMovimentacaoImpl.class, ordenacao, "bem");

				if (listaIntegracaoBemMovimentacao != null && !listaIntegracaoBemMovimentacao.isEmpty()) {

					controladorIntegracao.registrarMovimentacaoBens(listaIntegracaoBemMovimentacao, controladorConstanteSistema, processo,
									logProcessamento, listaSistemasIntegrantes);

				} else {
					logProcessamento.append("[")
							.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
							.append("] ").append(Constantes.ERRO_NEGOCIO_SEM_DADOS + Constantes.PULA_LINHA);
				}

				logProcessamento.append(Constantes.PULA_LINHA).append("[")
						.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
						.append("] ").append("Finalizando o tratamento dos registros da integração na movimentação de bens.");

			} else {

				logProcessamento.append(Constantes.PULA_LINHA).append("[")
						.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true))
						.append("] ").append("Integração da movimentação de bens inativa.");
			}

		} catch (HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		} catch (NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new NegocioException(e);
		} catch (GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		} catch (Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

}
