/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 07/06/2013 14:13:51
 @author ccavalcanti
 */

package br.com.ggas.integracao.bens;

/**
 * The Interface IntegracaoBemVazaoCorretor.
 */
public interface IntegracaoBemVazaoCorretor extends IntegracaoBem {

	/** The bean id integracao bem vazao corretor. */
	String BEAN_ID_INTEGRACAO_BEM_VAZAO_CORRETOR = "integracaoBemVazaoCorretor";

	/**
	 * Gets the marca corretor.
	 *
	 * @return the marca corretor
	 */
	Long getMarcaCorretor();

	/**
	 * Sets the marca corretor.
	 *
	 * @param marcaCorretor the new marca corretor
	 */
	void setMarcaCorretor(Long marcaCorretor);

	/**
	 * Gets the indicador tipo mostrador.
	 *
	 * @return the indicador tipo mostrador
	 */
	Long getIndicadorTipoMostrador();

	/**
	 * Sets the indicador tipo mostrador.
	 *
	 * @param indicadorTipoMostrador the new indicador tipo mostrador
	 */
	void setIndicadorTipoMostrador(Long indicadorTipoMostrador);

	/**
	 * Gets the indicador correcao pressao.
	 *
	 * @return the indicador correcao pressao
	 */
	Long getIndicadorCorrecaoPressao();

	/**
	 * Sets the indicador correcao pressao.
	 *
	 * @param indicadorCorrecaoPressao the new indicador correcao pressao
	 */
	void setIndicadorCorrecaoPressao(Long indicadorCorrecaoPressao);

	/**
	 * Gets the indicador correcao temperatura.
	 *
	 * @return the indicador correcao temperatura
	 */
	Long getIndicadorCorrecaoTemperatura();

	/**
	 * Sets the indicador correcao temperatura.
	 *
	 * @param indicadorCorrecaoTemperatura the new indicador correcao temperatura
	 */
	void setIndicadorCorrecaoTemperatura(Long indicadorCorrecaoTemperatura);

	/**
	 * Gets the indicador controle vazao.
	 *
	 * @return the indicador controle vazao
	 */
	Long getIndicadorControleVazao();

	/**
	 * Sets the indicador controle vazao.
	 *
	 * @param indicadorControleVazao the new indicador controle vazao
	 */
	void setIndicadorControleVazao(Long indicadorControleVazao);

	/**
	 * Gets the indicador linearizacao fator k.
	 *
	 * @return the indicador linearizacao fator k
	 */
	Long getIndicadorLinearizacaoFatorK();

	/**
	 * Sets the indicador linearizacao fator k.
	 *
	 * @param indicadorLinearizacaoFatorK the new indicador linearizacao fator k
	 */
	void setIndicadorLinearizacaoFatorK(Long indicadorLinearizacaoFatorK);

	/**
	 * Gets the protocolo comunicacao.
	 *
	 * @return the protocolo comunicacao
	 */
	Long getProtocoloComunicacao();

	/**
	 * Sets the protocolo comunicacao.
	 *
	 * @param protocoloComunicacao the new protocolo comunicacao
	 */
	void setProtocoloComunicacao(Long protocoloComunicacao);

	/**
	 * Gets the tipo transdutor pressao.
	 *
	 * @return the tipo transdutor pressao
	 */
	Long getTipoTransdutorPressao();

	/**
	 * Sets the tipo transdutor pressao.
	 *
	 * @param tipoTransdutorPressao the new tipo transdutor pressao
	 */
	void setTipoTransdutorPressao(Long tipoTransdutorPressao);

	/**
	 * Gets the tipo transdutor temperatura.
	 *
	 * @return the tipo transdutor temperatura
	 */
	Long getTipoTransdutorTemperatura();

	/**
	 * Sets the tipo transdutor temperatura.
	 *
	 * @param tipoTransdutorTemperatura the new tipo transdutor temperatura
	 */
	void setTipoTransdutorTemperatura(Long tipoTransdutorTemperatura);

	/**
	 * Gets the temperatura maxima transdutor.
	 *
	 * @return the temperatura maxima transdutor
	 */
	Long getTemperaturaMaximaTransdutor();

	/**
	 * Sets the temperatura maxima transdutor.
	 *
	 * @param temperaturaMaximaTransdutor the new temperatura maxima transdutor
	 */
	void setTemperaturaMaximaTransdutor(Long temperaturaMaximaTransdutor);

	/**
	 * Gets the pressao maxima transdutor.
	 *
	 * @return the pressao maxima transdutor
	 */
	Long getPressaoMaximaTransdutor();

	/**
	 * Sets the pressao maxima transdutor.
	 *
	 * @param pressaoMaximaTransdutor the new pressao maxima transdutor
	 */
	void setPressaoMaximaTransdutor(Long pressaoMaximaTransdutor);

	/**
	 * Gets the modelo.
	 *
	 * @return the modelo
	 */
	Long getModelo();

	/**
	 * Sets the modelo.
	 *
	 * @param modelo the new modelo
	 */
	void setModelo(Long modelo);

	/**
	 * Gets the numero tombamento.
	 *
	 * @return the numero tombamento
	 */
	String getNumeroTombamento();

	/**
	 * Sets the numero tombamento.
	 *
	 * @param numeroTombamento the new numero tombamento
	 */
	void setNumeroTombamento(String numeroTombamento);

	/**
	 * Gets the local armazenagem.
	 *
	 * @return the local armazenagem
	 */
	Long getLocalArmazenagem();

	/**
	 * Sets the local armazenagem.
	 *
	 * @param localArmazenagem the new local armazenagem
	 */
	void setLocalArmazenagem(Long localArmazenagem);

}
