/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/04/2013 09:16:20
 @author vtavares
 */

package br.com.ggas.integracao.bens.impl;

import java.util.Date;

import br.com.ggas.integracao.bens.IntegracaoBem;
import br.com.ggas.integracao.bens.IntegracaoBemMovimentacao;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;

/**
 * Classe Integracao Bem Movimentacao
 *
 *
 */
public class IntegracaoBemMovimentacaoImpl extends IntegracaoImpl implements IntegracaoBemMovimentacao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5110281481364179162L;

	private Long situacaoBem;

	private Long codigoMovimentacao;

	private Date dataOperacao;

	private IntegracaoBem bem;

	private String descricao;

	private Long motivoMovimentacao;

	private Long localArmazenagemDestino;

	private Long localArmazenagemOrigem;

	private Long funcionario;

	private String descricaoDestino;

	/**
	 * @return the situacaoBem
	 */
	@Override
	public Long getSituacaoBem() {

		return situacaoBem;
	}

	/**
	 * @param situacaoBem
	 *            the situacaoBem to set
	 */
	@Override
	public void setSituacaoBem(Long situacaoBem) {

		this.situacaoBem = situacaoBem;
	}

	/**
	 * @return the codigoMovimentacao
	 */
	@Override
	public Long getCodigoMovimentacao() {

		return codigoMovimentacao;
	}

	/**
	 * @param codigoMovimentacao
	 *            the codigoMovimentacao to set
	 */
	@Override
	public void setCodigoMovimentacao(Long codigoMovimentacao) {

		this.codigoMovimentacao = codigoMovimentacao;
	}

	/**
	 * @return the dataOperacao
	 */
	@Override
	public Date getDataOperacao() {
		Date data = null;
		if (this.dataOperacao != null) {
			data = (Date) dataOperacao.clone();
		}
		return data;
	}

	/**
	 * @param dataOperacao
	 *            the dataOperacao to set
	 */
	@Override
	public void setDataOperacao(Date dataOperacao) {
		if (dataOperacao != null) {
			this.dataOperacao = (Date) dataOperacao.clone();
		} else {
			this.dataOperacao = null;
		}
	}

	/**
	 * @return the bem
	 */
	@Override
	public IntegracaoBem getBem() {

		return bem;
	}

	/**
	 * @param bem
	 *            the bem to set
	 */
	@Override
	public void setBem(IntegracaoBem bem) {

		this.bem = bem;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the motivoMovimentacao
	 */
	@Override
	public Long getMotivoMovimentacao() {

		return motivoMovimentacao;
	}

	/**
	 * @param motivoMovimentacao
	 *            the motivoMovimentacao to set
	 */
	@Override
	public void setMotivoMovimentacao(Long motivoMovimentacao) {

		this.motivoMovimentacao = motivoMovimentacao;
	}

	/**
	 * @return the localArmazenagemDestino
	 */
	@Override
	public Long getLocalArmazenagemDestino() {

		return localArmazenagemDestino;
	}

	/**
	 * @param localArmazenagemDestino
	 *            the localArmazenagemDestino to set
	 */
	@Override
	public void setLocalArmazenagemDestino(Long localArmazenagemDestino) {

		this.localArmazenagemDestino = localArmazenagemDestino;
	}

	/**
	 * @return the funcionario
	 */
	@Override
	public Long getFuncionario() {

		return funcionario;
	}

	/**
	 * @param funcionario
	 *            the funcionario to set
	 */
	@Override
	public void setFuncionario(Long funcionario) {

		this.funcionario = funcionario;
	}

	@Override
	public String getDescricaoDestino() {

		return descricaoDestino;
	}

	@Override
	public void setDescricaoDestino(String descricaoDestino) {

		this.descricaoDestino = descricaoDestino;
	}

	@Override
	public Long getLocalArmazenagemOrigem() {

		return localArmazenagemOrigem;
	}

	@Override
	public void setLocalArmazenagemOrigem(Long localArmazenagemOrigem) {

		this.localArmazenagemOrigem = localArmazenagemOrigem;
	}

}
