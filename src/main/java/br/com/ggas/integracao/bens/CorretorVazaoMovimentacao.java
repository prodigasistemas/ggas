/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 ada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General  License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General  License for more details.

 You should have received a copy of the GNU General  License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/06/2013 10:09:16
 @author vpessoa
 */

package br.com.ggas.integracao.bens;

import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a movimentação de corretor vazão
 *
 */
public interface CorretorVazaoMovimentacao extends EntidadeNegocio {

	String BEAN_ID_CORRETOR_VAZAO_MOVIMENTACAO = "corretorVazaoMovimentacao";

	/**
	 * @return Date - Retorna Data Movimento.
	 */
	Date getDataMovimento();

	/**
	 * @param dataMovimento - Set data do movimento.
	 */
	void setDataMovimento(Date dataMovimento);

	/**
	 * @return VazaoCorretor - Retorna objeto vazão corretor.
	 */
	VazaoCorretor getCorretorVazao();

	/**
	 * @param corretorVazao - Set corretor de vazão.
	 */
	void setCorretorVazao(VazaoCorretor corretorVazao);

	/**
	 * @return MotivoMovimentacaoMedidor - Retorna objeto motivo movimentação medidor.
	 */
	MotivoMovimentacaoMedidor getMotivoMovimentacao();

	/**
	 * @param motivoMovimentacao - Set motivo da movimentação.
	 */
	void setMotivoMovimentacao(MotivoMovimentacaoMedidor motivoMovimentacao);

	/**
	 * @return String - Descrição parecer.
	 */
	String getDescricaoParecer();

	/**
	 * @param descricaoParecer - Set descrição parecer.
	 */
	void setDescricaoParecer(String descricaoParecer);

	/**
	 * @return MedidorLocalArmazenagem - Retorna Local Armazenagem Origem.
	 */
	MedidorLocalArmazenagem getLocalArmazenagemOrigem();

	/**
	 * @param localArmazenagemOrigem - Set local armazenagem de origem.
	 */
	void setLocalArmazenagemOrigem(MedidorLocalArmazenagem localArmazenagemOrigem);

	/**
	 * @return MedidorLocalArmazenagem - Retorna Local Armazenagem Destino.
	 */
	MedidorLocalArmazenagem getLocalArmazenagemDestino();

	/**
	 * @param localArmazenagemDestino - Set Local de armazenagem destino.
	 */
	void setLocalArmazenagemDestino(MedidorLocalArmazenagem localArmazenagemDestino);

	/**
	 * @return Funcionario - Retorna um Objeto Funcionário.
	 */
	Funcionario getFuncionario();

	/**
	 * @param funcionario - Set Funcionário.
	 */
	void setFuncionario(Funcionario funcionario);
}
