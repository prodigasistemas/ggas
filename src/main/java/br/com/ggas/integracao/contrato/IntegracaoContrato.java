package br.com.ggas.integracao.contrato;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.integracao.geral.Integracao;

/**
 * The Interface IntegracaoContrato.
 */
public interface IntegracaoContrato extends Integracao {

	/** The bean id integracao cliente. */
	String BEAN_ID_INTEGRACAO_CONTRATO = "integracaoContrato";

	/**
	 * Gets the contrato origem
	 * @return o código do contrato de origem
	 */
	Contrato getContratoOrigem();

	/**
	 * Set the contrato origem
	 * @param contratoOrigem o contrato origem
	 */
	void setContratoOrigem(Contrato contratoOrigem);

	/**
	 * Gets the contrato pai origem
	 * @return the contrato pai origem
	 */
	Long getContratoPaiOrigem();

	/**
	 * Set the contrato pai origem
	 * @param contratoPaiOrigem the contrato pai origem
	 */
	void setContratoPaiOrigem(Long contratoPaiOrigem);

	/**
	 * Gets the contratoPontoConsumo
	 * @return the contratoPontoConsumo
	 */
	ContratoPontoConsumo getContratoPontoConsumo();

	/**
	 * Set the contratoPontoConsumo
	 * @param contratoPontoConsumo the contratoPontoConsumoItemFatura
	 */
	void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo);

	/**
	 * Gets the segmento
	 * @return the segmento
	 */
	Long getSegmento();

	/**
	 * Set the segmento
	 * @param segmento the segmento
	 */
	void setSegmento(Long segmento);

	/**
	 * Gets the contrato destino
	 * @return the contrato destino
	 */
	Long getContratoDestino();

	/**
	 * Set the contrato destino
	 * @param contratoDestino the contrato destino
	 */
	void setContratoDestino(Long contratoDestino);

	/**
	 * Gets a chave primária do cliente
	 * @return a chave primária do cliente
	 */
	Long getChaveCliente();

	/**
	 * Sets a chave primária do cliente
	 * @param chaveCliente a chave primária do cliente
	 */
	void setChaveCliente(Long chaveCliente);

	/**
	 * Gets the tipo cliente
	 * @return the tipo cliente
	 */
	Long getTipoCliente();

	/**
	 * Set o tipo cliente
	 * @param tipoCliente o tipo cliente
	 */
	void setTipoCliente(Long tipoCliente);

	/**
	 * Gets o nome cliente
	 * @return o nome cliente
	 */
	String getNomeCliente();

	/**
	 * Set o nome cliente
	 * @param nomeCliente o nome cliente
	 */
	void setNomeCliente(String nomeCliente);

	/**
	 * Gets o cpf cliente
	 * @return o cpf cliente
	 */
	String getCpfCliente();

	/**
	 * Set cpf cliente
	 * @param cpfCliente o cpf cliente
	 */
	void setCpfCliente(String cpfCliente);

	/**
	 * Gets o cnpj cliente
	 * @return o cnpj cliente
	 */
	String getCnpjCliente();

	/**
	 * Set o cnpj cliente
	 * @param cnpjCliente o cnpj cliente
	 */
	void setCnpjCliente(String cnpjCliente);

	/**
	 * Gets o ddd cliente
	 * @return o ddd cliente
	 */
	Integer getDddCliente();

	/**
	 * Set o ddd cliente
	 * @param dddCliente o ddd cliente
	 */
	void setDddCliente(Integer dddCliente);

	/**
	 * Gets o fone cliente
	 * @return o fone cliente
	 */
	Integer getFoneCliente();

	/**
	 * Set o fone cliente
	 * @param foneCliente o fone cliente
	 */
	void setFoneCliente(Integer foneCliente);

	/**
	 * Gets a inscrição estadual
	 * @return a inscrição estadual
	 */
	String getInscricaoEstadualCliente();

	/**
	 * Set a inscrição estadual
	 * @param inscricaoEstadualCliente a inscrição estadual
	 */
	void setInscricaoEstadualCliente(String inscricaoEstadualCliente);

	/**
	 * Gets o email do cliente
	 * @return o email do cliente
	 */
	String getEmailCliente();

	/**
	 * Set o email do cliente
	 * @param emailCliente o email do cliente
	 */
	void setEmailCliente(String emailCliente);

	/**
	 * Gets o numero do banco para débito
	 * @return o numero do banco para débito
	 */
	String getNumeroBancoCliente();

	/**
	 * Set o numero do banco para débito
	 * @param numeroBancoCliente o numero do banco para débito
	 */
	void setNumeroBancoCliente(String numeroBancoCliente);

	/**
	 * Gets a agência do cliente
	 * @return a agência do cliente
	 */
	String getAgenciaCliente();

	/**
	 * Set a agência do cliente
	 * @param agenciaCliente a agência do cliente
	 */
	void setAgenciaCliente(String agenciaCliente);

	/**
	 * Gets a conta corrente do cliente
	 * @return a conta corrente do cliente
	 */
	String getContaCorrenteCliente();

	/**
	 * Set a conta corrente do cliente
	 * @param contaCorrenteCliente a conta corrente do cliente
	 */
	void setContaCorrenteCliente(String contaCorrenteCliente);

	/**
	 * Gets o tipo do convênio de cobrança
	 * @return o tipo do convênio de cobrança
	 */
	String getTipoConvenioCobrancaCliente();

	/**
	 * Set o tipo do convênio de cobrança
	 * @param tipoConvenioCobrancaCliente o tipo do convênio de cobrança
	 */
	void setTipoConvenioCobrancaCliente(String tipoConvenioCobrancaCliente);

	/**
	 * Gets o ponto de consumo
	 * @return o ponto de consumo
	 */
	String getPontoConsumo();

	/**
	 * Set o ponto de consumo
	 * @param pontoConsumo o ponto de consumo
	 */
	void setPontoConsumo(String pontoConsumo);

	/**
	 * Gets o logradouro de faturamento
	 * @return o logradouro de faturamento
	 */
	String getLogradouroFaturamento();

	/**
	 * Set o logradouro de faturamento
	 * @param logradouroFaturamento o logradouro de faturamento
	 */
	void setLogradouroFaturamento(String logradouroFaturamento);

	/**
	 * Gets o número do endereço de faturamento
	 * @return o número do endereço de faturamento
	 */
	String getNumeroEnderecoFaturamento();

	/**
	 * Set o número do endereço de faturamento
	 * @param numeroEnderecoFaturamento o número do endereço de faturamento
	 */
	void setNumeroEnderecoFaturamento(String numeroEnderecoFaturamento);

	/**
	 * Gets o complemento do endereço de faturamento
	 * @return o complemento do endereço de faturamento
	 */
	String getComplementoEnderecoFaturamento();

	/**
	 * Set o complemento do endereço de faturamento
	 * @param complementoEnderecoFaturamento o complemento do endereço de faturamento
	 */
	void setComplementoEnderecoFaturamento(String complementoEnderecoFaturamento);

	/**
	 * Gets o bairro do faturamento
	 * @return o bairro do faturamento
	 */
	String getBairroFaturamento();

	/**
	 * Set o bairro do faturamento
	 * @param bairroFaturamento o bairro do faturamento
	 */
	void setBairroFaturamento(String bairroFaturamento);

	/**
	 * Gets o cep de faturamento
	 * @return o cep de faturamento
	 */
	Long getCepFaturamento();

	/**
	 * Set o cep de faturamento
	 * @param cepFaturamento o cep de faturamento
	 */
	void setCepFaturamento(Long cepFaturamento);

	/**
	 * Gets o código do município de faturamento
	 * @return o código do município de faturamento
	 */
	Long getCodMunicipioFaturamento();

	/**
	 * Set o código do município de faturamento
	 * @param codMunicipioFaturamento o código do município de faturamento
	 */
	void setCodMunicipioFaturamento(Long codMunicipioFaturamento);

	/**
	 * Gets o município de faturamento
	 * @return o município de faturamento
	 */
	String getMunicipioFaturamento();

	/**
	 * Set o município de faturamento
	 * @param municipioFaturamento o município de faturamento
	 */
	void setMunicipioFaturamento(String municipioFaturamento);

	/**
	 * Gets a sigla do estado de faturamento
	 * @return a sigla do estado de faturamento
	 */
	String getSiglaEstadoFaturamento();

	/**
	 * Set a sigla do estado de faturamento
	 * @param siglaEstadoFaturamento a sigla do estado de faturamento
	 */
	void setSiglaEstadoFaturamento(String siglaEstadoFaturamento);

	/**
	 * Gets o código do país de faturamento
	 * @return o código do país de faturamento
	 */
	Long getCodPaisFaturamento();

	/**
	 * Set o código do país de faturamento
	 * @param codPaisFaturamento o código do país de faturamento
	 */
	void setCodPaisFaturamento(Long codPaisFaturamento);

	/**
	 * Gets o logradouro de cobrança
	 * @return o logradouro de cobrança
	 */
	String getLogradouroCobranca();

	/**
	 * Set o logradouro de cobrança
	 * @param logradouroCobranca o logradouro de cobrança
	 */
	void setLogradouroCobranca(String logradouroCobranca);

	/**
	 * Gets o número do endereço de cobrança
	 * @return o número do endereço de cobrança
	 */
	String getNumeroEnderecoCobranca();

	/**
	 * Set o número do endereço de cobrança
	 * @param numeroEnderecoCobranca o número do endereço de cobrança
	 */
	void setNumeroEnderecoCobranca(String numeroEnderecoCobranca);

	/**
	 * Gets o complemento do endereço de cobrança
	 * @return o complemento do endereço de cobrança
	 */
	String getComplementoEnderecoCobranca();

	/**
	 * Set o complemento do endereço de cobrança
	 * @param complementoEnderecoCobranca o complemento do endereço de cobrança
	 */
	void setComplementoEnderecoCobranca(String complementoEnderecoCobranca);

	/**
	 * Gets o bairro de cobrança
	 * @return o bairro de cobrança
	 */
	String getBairroCobranca();

	/**
	 * Set o bairro de cobrança
	 * @param bairroCobranca o bairro de cobrança
	 */
	void setBairroCobranca(String bairroCobranca);

	/**
	 * Gets o código do município de cobrança
	 * @return o código do município de cobrança
	 */
	Long getCodMunicipioCobranca();

	/**
	 * Set o código do município de cobrança
	 * @param codMunicipioCobranca o código do município de cobrança
	 */
	void setCodMunicipioCobranca(Long codMunicipioCobranca);

	/**
	 * Gets o municípo de cobrança
	 * @return o municípo de cobrança
	 */
	String getMunicipioCobranca();

	/**
	 * Set o municípo de cobrança
	 * @param municipioCobranca o municípo de cobrança
	 */
	void setMunicipioCobranca(String municipioCobranca);

	/**
	 * Gets a sigla do estado de cobrança
	 * @return a sigla do estado de cobrança
	 */
	String getSiglaEstadoCobranca();

	/**
	 * Set a sigla do estado de cobrança
	 * @param siglaEstadoCobranca a sigla do estado de cobrança
	 */
	void setSiglaEstadoCobranca(String siglaEstadoCobranca);

	/**
	 * Gets o código do país de cobrança
	 * @return o código do país de cobrança
	 */
	Long getCodPaisCobranca();

	/**
	 * Set o código do país de cobrança
	 * @param codPaisCobranca o código do país de cobrança
	 */
	void setCodPaisCobranca(Long codPaisCobranca);

	/**
	 * Gets a constante representando a operação de integração.
	 * @return a constante representando a operação de integração.
	 */
	ConstanteSistema getConstanteOperacao();

	/**
	 * Sets a constante representando a operação de integração.
	 * @param constanteOperacao a constante representando a operação de integração.
	 */
	void setConstanteOperacao(ConstanteSistema constanteOperacao);

	/**
	 * Gets o objeto ponto de consumo
	 * @return o objeto ponto de consumo
	 */
	PontoConsumo getPontoConsumoObj();

	/**
	 * Sets o objeto ponto de consumo
	 * @param pontoConsumoObj o objeto ponto de consumo
	 */
	void setPontoConsumoObj(PontoConsumo pontoConsumoObj);
}
