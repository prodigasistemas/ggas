package br.com.ggas.integracao.contrato.impl;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.integracao.geral.impl.IntegracaoImpl;

/**
 * Classe responsável por implementar os métodos da interface IntegracaoContrato
 */
public class IntegracaoContratoImpl extends IntegracaoImpl implements IntegracaoContrato {

	private static final long serialVersionUID = -7874459283226924071L;

	private Contrato contratoOrigem;
	private Long contratoPaiOrigem;
	private ContratoPontoConsumo contratoPontoConsumo;
	private Long segmento;
	private Long contratoDestino;

	private Long chaveCliente;
	private Long tipoCliente;
	private String nomeCliente;
	private String cpfCliente;
	private String cnpjCliente;
	private Integer dddCliente;
	private Integer foneCliente;
	private String inscricaoEstadualCliente;
	private String emailCliente;
	private String numeroBancoCliente;
	private String agenciaCliente;
	private String contaCorrenteCliente;
	private String tipoConvenioCobrancaCliente;

	private String pontoConsumo;
	private String logradouroFaturamento;
	private String numeroEnderecoFaturamento;
	private String complementoEnderecoFaturamento;
	private String bairroFaturamento;
	private Long cepFaturamento;
	private Long codMunicipioFaturamento;
	private String municipioFaturamento;
	private String siglaEstadoFaturamento;
	private Long codPaisFaturamento;

	private String logradouroCobranca;
	private String numeroEnderecoCobranca;
	private String complementoEnderecoCobranca;
	private String bairroCobranca;
	private Long codMunicipioCobranca;
	private String municipioCobranca;
	private String siglaEstadoCobranca;
	private Long codPaisCobranca;

	private ConstanteSistema constanteOperacao;
	private PontoConsumo pontoConsumoObj;

	@Override public Contrato getContratoOrigem() {
		return contratoOrigem;
	}

	@Override public void setContratoOrigem(Contrato contratoOrigem) {
		this.contratoOrigem = contratoOrigem;
	}

	@Override public Long getContratoPaiOrigem() {
		return contratoPaiOrigem;
	}

	@Override public void setContratoPaiOrigem(Long contratoPaiOrigem) {
		this.contratoPaiOrigem = contratoPaiOrigem;
	}

	@Override public ContratoPontoConsumo getContratoPontoConsumo() {
		return contratoPontoConsumo;
	}

	@Override public void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) {
		this.contratoPontoConsumo = contratoPontoConsumo;
	}

	@Override public Long getSegmento() {
		return segmento;
	}

	@Override public void setSegmento(Long segmento) {
		this.segmento = segmento;
	}

	@Override public Long getContratoDestino() {
		return contratoDestino;
	}

	@Override public void setContratoDestino(Long contratoDestino) {
		this.contratoDestino = contratoDestino;
	}

	@Override public Long getChaveCliente() {
		return chaveCliente;
	}

	@Override public void setChaveCliente(Long chaveCliente) {
		this.chaveCliente = chaveCliente;
	}

	@Override public Long getTipoCliente() {
		return tipoCliente;
	}

	@Override public void setTipoCliente(Long tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	@Override public String getNomeCliente() {
		return nomeCliente;
	}

	@Override public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	@Override public String getCpfCliente() {
		return cpfCliente;
	}

	@Override public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	@Override public String getCnpjCliente() {
		return cnpjCliente;
	}

	@Override public void setCnpjCliente(String cnpjCliente) {
		this.cnpjCliente = cnpjCliente;
	}

	@Override public Integer getDddCliente() {
		return dddCliente;
	}

	@Override public void setDddCliente(Integer dddCliente) {
		this.dddCliente = dddCliente;
	}

	@Override public Integer getFoneCliente() {
		return foneCliente;
	}

	@Override public void setFoneCliente(Integer foneCliente) {
		this.foneCliente = foneCliente;
	}

	@Override public String getInscricaoEstadualCliente() {
		return inscricaoEstadualCliente;
	}

	@Override public void setInscricaoEstadualCliente(String inscricaoEstadualCliente) {
		this.inscricaoEstadualCliente = inscricaoEstadualCliente;
	}

	@Override public String getEmailCliente() {
		return emailCliente;
	}

	@Override public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	@Override public String getNumeroBancoCliente() {
		return numeroBancoCliente;
	}

	@Override public void setNumeroBancoCliente(String numeroBancoCliente) {
		this.numeroBancoCliente = numeroBancoCliente;
	}

	@Override public String getAgenciaCliente() {
		return agenciaCliente;
	}

	@Override public void setAgenciaCliente(String agenciaCliente) {
		this.agenciaCliente = agenciaCliente;
	}

	@Override public String getContaCorrenteCliente() {
		return contaCorrenteCliente;
	}

	@Override public void setContaCorrenteCliente(String contaCorrenteCliente) {
		this.contaCorrenteCliente = contaCorrenteCliente;
	}

	@Override public String getTipoConvenioCobrancaCliente() {
		return tipoConvenioCobrancaCliente;
	}

	@Override public void setTipoConvenioCobrancaCliente(String tipoConvenioCobrancaCliente) {
		this.tipoConvenioCobrancaCliente = tipoConvenioCobrancaCliente;
	}

	@Override public String getPontoConsumo() {
		return pontoConsumo;
	}

	@Override public void setPontoConsumo(String pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	@Override public String getLogradouroFaturamento() {
		return logradouroFaturamento;
	}

	@Override public void setLogradouroFaturamento(String logradouroFaturamento) {
		this.logradouroFaturamento = logradouroFaturamento;
	}

	@Override public String getNumeroEnderecoFaturamento() {
		return numeroEnderecoFaturamento;
	}

	@Override public void setNumeroEnderecoFaturamento(String numeroEnderecoFaturamento) {
		this.numeroEnderecoFaturamento = numeroEnderecoFaturamento;
	}

	@Override public String getComplementoEnderecoFaturamento() {
		return complementoEnderecoFaturamento;
	}

	@Override public void setComplementoEnderecoFaturamento(String complementoEnderecoFaturamento) {
		this.complementoEnderecoFaturamento = complementoEnderecoFaturamento;
	}

	@Override public String getBairroFaturamento() {
		return bairroFaturamento;
	}

	@Override public void setBairroFaturamento(String bairroFaturamento) {
		this.bairroFaturamento = bairroFaturamento;
	}

	@Override public Long getCepFaturamento() {
		return cepFaturamento;
	}

	@Override public void setCepFaturamento(Long cepFaturamento) {
		this.cepFaturamento = cepFaturamento;
	}

	@Override public Long getCodMunicipioFaturamento() {
		return codMunicipioFaturamento;
	}

	@Override public void setCodMunicipioFaturamento(Long codMunicipioFaturamento) {
		this.codMunicipioFaturamento = codMunicipioFaturamento;
	}

	@Override public String getMunicipioFaturamento() {
		return municipioFaturamento;
	}

	@Override public void setMunicipioFaturamento(String municipioFaturamento) {
		this.municipioFaturamento = municipioFaturamento;
	}

	@Override public String getSiglaEstadoFaturamento() {
		return siglaEstadoFaturamento;
	}

	@Override public void setSiglaEstadoFaturamento(String siglaEstadoFaturamento) {
		this.siglaEstadoFaturamento = siglaEstadoFaturamento;
	}

	@Override public Long getCodPaisFaturamento() {
		return codPaisFaturamento;
	}

	@Override public void setCodPaisFaturamento(Long codPaisFaturamento) {
		this.codPaisFaturamento = codPaisFaturamento;
	}

	@Override public String getLogradouroCobranca() {
		return logradouroCobranca;
	}

	@Override public void setLogradouroCobranca(String logradouroCobranca) {
		this.logradouroCobranca = logradouroCobranca;
	}

	@Override public String getNumeroEnderecoCobranca() {
		return numeroEnderecoCobranca;
	}

	@Override public void setNumeroEnderecoCobranca(String numeroEnderecoCobranca) {
		this.numeroEnderecoCobranca = numeroEnderecoCobranca;
	}

	@Override public String getComplementoEnderecoCobranca() {
		return complementoEnderecoCobranca;
	}

	@Override public void setComplementoEnderecoCobranca(String complementoEnderecoCobranca) {
		this.complementoEnderecoCobranca = complementoEnderecoCobranca;
	}

	@Override public String getBairroCobranca() {
		return bairroCobranca;
	}

	@Override public void setBairroCobranca(String bairroCobranca) {
		this.bairroCobranca = bairroCobranca;
	}

	@Override public Long getCodMunicipioCobranca() {
		return codMunicipioCobranca;
	}

	@Override public void setCodMunicipioCobranca(Long codMunicipioCobranca) {
		this.codMunicipioCobranca = codMunicipioCobranca;
	}

	@Override public String getMunicipioCobranca() {
		return municipioCobranca;
	}

	@Override public void setMunicipioCobranca(String municipioCobranca) {
		this.municipioCobranca = municipioCobranca;
	}

	@Override public String getSiglaEstadoCobranca() {
		return siglaEstadoCobranca;
	}

	@Override public void setSiglaEstadoCobranca(String siglaEstadoCobranca) {
		this.siglaEstadoCobranca = siglaEstadoCobranca;
	}

	@Override public Long getCodPaisCobranca() {
		return codPaisCobranca;
	}

	@Override public void setCodPaisCobranca(Long codPaisCobranca) {
		this.codPaisCobranca = codPaisCobranca;
	}

	@Override
	public ConstanteSistema getConstanteOperacao() {
		return constanteOperacao;
	}

	@Override
	public void setConstanteOperacao(ConstanteSistema constanteOperacao) {
		this.constanteOperacao = constanteOperacao;
	}

	@Override
	public PontoConsumo getPontoConsumoObj() {
		return pontoConsumoObj;
	}

	@Override
	public void setPontoConsumoObj(PontoConsumo pontoConsumoObj) {
		this.pontoConsumoObj = pontoConsumoObj;
	}
}
