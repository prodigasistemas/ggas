/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

import org.apache.commons.lang3.StringUtils;

import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipo;
import br.com.ggas.util.StringUtil;

/**
 * Classe DadosCadastraisLinha
 *
 * @author ProdigaSistemas
 * 
 */
public class DadosCadastraisLinha extends RegistroSefazTipo {

	private String logradouro;
	private String numeroLogradouro;
	private String complemento;
	private String cep;
	private String bairro;
	private String municipio;
	private String uf;
	private String telefoneContato;
	private String codigoConsumidor;
	private String numeroTelefonicoConsumidora;
	private String ufTelefonico;
	private String codigoMunicipio;
	private String cnpjEmitente;

	public String getLogradouro() {
		if (StringUtils.isBlank(this.logradouro)) {
			return StringUtil.preencherDireita("RUA", CAMPO_DE_TAMANHO_QUARENTA_E_CINCO, "");
		}
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumeroLogradouro() {
		if ("00S/N".equals(this.numeroLogradouro)) {
			return StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_CINCO, "0");
		}
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getTelefoneContato() {
		if (StringUtils.isBlank(this.telefoneContato)) {
			this.telefoneContato = "";
		}

		return StringUtil.preencherDireita(telefoneContato, CAMPO_DE_TAMANHO_DOZE, "");
	}

	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}

	public String getCodigoConsumidor() {
		return codigoConsumidor;
	}

	public void setCodigoConsumidor(String codigoConsumidor) {
		this.codigoConsumidor = codigoConsumidor;
	}

	public String getNumeroTelefonicoConsumidora() {
		if (StringUtils.isBlank(this.numeroTelefonicoConsumidora)) {
			this.numeroTelefonicoConsumidora = "";
		}

		return StringUtil.preencherDireita(this.numeroTelefonicoConsumidora, CAMPO_DE_TAMANHO_DOZE, "");
	}

	public void setNumeroTelefonicoConsumidora(String numeroTelefonicoConsumidora) {
		this.numeroTelefonicoConsumidora = numeroTelefonicoConsumidora;
	}

	public String getUfTelefonico() {
		this.ufTelefonico = StringUtil.preencherDireita("", CAMPO_DE_TAMANHO_DOIS, "");

		return ufTelefonico;
	}

	public void setUfTelefonico(String ufTelefonico) {
		this.ufTelefonico = ufTelefonico;
	}

	public String getCodigoMunicipio() {
		return codigoMunicipio;
	}

	public void setCodigoMunicipio(String codigoMunicipio) {
		this.codigoMunicipio = codigoMunicipio;
	}

	public String getCnpjEmitente() {
		return cnpjEmitente;
	}

	public void setCnpjEmitente(String cnpjEmitente) {
		this.cnpjEmitente = cnpjEmitente;
	}
}
