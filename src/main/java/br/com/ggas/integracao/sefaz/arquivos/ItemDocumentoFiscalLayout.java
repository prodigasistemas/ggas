/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

/**
 * Classe ItemDocumentoFiscalLayout
 *
 * @author ProdigaSistemas
 * 
 */
public class ItemDocumentoFiscalLayout extends AbstractArquivoLayout<ItemDocumentoFiscalLinha> {
	
	private static final String TIPO_LAYOUT = "I";
	private static final int TOTAL_ESPACOS_EM_BRANCO = 5;
	
	/**
	 * Construtor da classe
	 * @param dados da linha do documento
	 */
	public ItemDocumentoFiscalLayout(ItemDocumentoFiscalLinha dados) {
		super(dados);
	}

	@Override
	public void gerarLinha() {
		StringBuilder linha = new StringBuilder();
		
		linha.append(dados.getCnpjCpf());
		linha.append(dados.getUf());
		linha.append(dados.getClasseConsumo());
		linha.append(dados.getFaseTipoUtilizacao());
		linha.append(dados.getGrupoTensao());
		linha.append(dados.getDataEmissao());
		linha.append(dados.getModelo());
		linha.append(dados.getSerie());
		linha.append(dados.getNumero());
		linha.append(dados.getCfop());
		linha.append(dados.getNumeroOrdemItem());
		linha.append(dados.getCodigoItem());
		linha.append(dados.getDescricaoItem());
		linha.append(dados.getCodigoClassificacaoItem());
		linha.append(dados.getUnidade());
		linha.append(dados.getQuantidadeContratada());
		linha.append(dados.getQuantidadeMedida());
		linha.append(dados.getTotal());
		linha.append(dados.getDescontoRedutores());
		linha.append(dados.getAcrescimosDespesasAcessorias());
		linha.append(dados.getBcIcms());
		linha.append(dados.getIcms());
		linha.append(dados.getOperacoesIsentasNaoTributaveis());
		linha.append(dados.getOutrosValores());
		linha.append(dados.getAliquotaIcms());
		linha.append(dados.getSituacao());
		linha.append(dados.getAnoMesReferencia());
		linha.append(dados.getNumeroContrato());
		linha.append(dados.getQuantidadeFaturada());
		linha.append(dados.getTarifaAplicadaPrecoMedioEfetivo());
		linha.append(dados.getAliquotaPisPasep());
		linha.append(dados.getPisPasep());
		linha.append(dados.getAliquotaCofins());
		linha.append(dados.getCofins());
		linha.append(dados.getIndicadorDescontoJudicial());
		linha.append(dados.getTipoIsencaoReducaoBaseCalculo());
		linha.append(getEspacosBrancos(TOTAL_ESPACOS_EM_BRANCO));
		
		setLinha(linha.toString());
	}
	
	public static String getTipo() {
		return TIPO_LAYOUT;
	}
}
