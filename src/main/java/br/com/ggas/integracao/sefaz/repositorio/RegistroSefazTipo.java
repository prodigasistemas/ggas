/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.repositorio;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.StringUtil;

/**
 * Entidade abstrata para registro Sefa para formatacao de numerica de campo
 * 
 * @author ProdigaSistemas
 *
 */
public class RegistroSefazTipo {
	
	public static final int CAMPO_DE_TAMANHO_UM = 1;
	public static final int CAMPO_DE_TAMANHO_DOIS = 2;
	public static final int CAMPO_DE_TAMANHO_QUATRO = 4;
	public static final int CAMPO_DE_TAMANHO_CINCO = 5;
	public static final int CAMPO_DE_TAMANHO_SEIS = 6;
	public static final int CAMPO_DE_TAMANHO_OITO = 8;
	public static final int CAMPO_DE_TAMANHO_NOVE = 9;
	public static final int CAMPO_DE_TAMANHO_DEZ = 10;
	public static final int CAMPO_DE_TAMANHO_ONZE = 11;
	public static final int CAMPO_DE_TAMANHO_DOZE = 12;
	public static final int CAMPO_DE_TAMANHO_QUATORZE = 14;
	public static final int CAMPO_DE_TAMANHO_QUINZE = 15;
	public static final int CAMPO_DE_TAMANHO_TRINTA = 30;
	public static final int CAMPO_DE_TAMANHO_QUARENTA_E_CINCO = 45;
	
	private String cnpjCpf;
	private String inscricaoEstadual;
	private String razaoSocial;
	
	private String dataEmissao;
	private String modelo;
	private String serie;
	private String numero;
	
	protected RegistroSefazTipo() {
		
	}
	
	public String getCnpjCpf() {
		if(StringUtils.isBlank(cnpjCpf)) {
			cnpjCpf = "0";
		}
		
		return StringUtil.preencherEsquerda(cnpjCpf, CAMPO_DE_TAMANHO_QUATORZE, "0");
	}
	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		if("ISENTA".equals(inscricaoEstadual.trim()) || "ISENYO".equals(inscricaoEstadual.trim())) {
			inscricaoEstadual = StringUtil.preencherDireita("ISENTO", CAMPO_DE_TAMANHO_QUATORZE, "");
		}
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	/**
	 * Método com expressao regular para formatação de campo numérico 
	 * 
	 * @param valor de cadastro
	 * @param tamanho do valor resultante
	 * @return String valor formatado
	 */
	public String formataNumerico(String valor, int tamanho) {
		valor = valor.replaceAll(Constantes.SEPARADOR_DECIMAL_REGEX,  "");
		return StringUtil.preencherEsquerda(valor, tamanho, "0");
	}
	
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
}
