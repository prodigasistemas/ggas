/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.repositorio;

import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLinha;

/**
 * Classe MestreDocumentoPopulator
 *
 * @author ProdigaSistemas
 * 
 */
public class MestreDocumentoPopulator extends RegistroSefazPopulator {
	private static final String[] CAMPOS = new String[] {
			"cnpjCpf",
			"inscricaoEstadual",
			"razaoSocial",
			"uf",
			"classeConsumo",
			"faseOuTipoUtilizacao",
			"grupoTensao",
			"codigoIdentificacaoConsumidorAssinante",
			"dataEmissao",
			"modelo",
			"serie",
			"numero",
			"codigoAutenticacaoDigitalDocumentoFiscal",
			"valorTotal",
			"bcIcms",
			"icmsDestacado",
			"operacoesIsentasNaoTributadas",
			"outrosValores",
			"situacaoDocumento",
			"anoMesReferenciaApuracao",
			"referenciaItemNF",
			"numeroTerminalTelefonicoUnidadeConsumidora",  
			"indicacaoTipoInformacaoCampo1",
			"tipoCliente",
			"subclasseConsumo",
			"numeroTerminalTelefonicoPrincipal",
			"cnpjEmitente",
			"numeroCodigoFaturaComercial",
			"valorTotalFaturaComercial",
			"dataLeituraAnterior",
			"dataLeituraAtual",
			"campo32",
			"campo33",
			"informacoesAdicionais",
			"campo35",
			"ponto_consumo_tributo_aliquota",
			"indicadorIsencao",
			"dataCancelamento",
			"cfop",
			"codigoClassificacaoItem",
			"tipoPessoa"
	};

	@Override
	public RegistroSefazTipo getRegistroSefaz() {
		return new MestreDocumentoFiscalLinha();
	}

	@Override
	String[] getCampos() {
		return CAMPOS;
	}
	

}
