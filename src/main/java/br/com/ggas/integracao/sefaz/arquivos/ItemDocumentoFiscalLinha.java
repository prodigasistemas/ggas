/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA

 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipo;
import br.com.ggas.util.StringUtil;

/**
 * Classe ItemDocumentoFiscalLinha
 *
 * @author ProdigaSistemas
 *
 */
public class ItemDocumentoFiscalLinha extends RegistroSefazTipo {

	private String uf;
	private String classeConsumo;
	private String faseTipoUtilizacao;
	private String grupoTensao;
	private String dataEmissao;
	private String modelo;
	private String serie;
	private String numero;
	private String cfop;
	private String numeroOrdemItem;
	private String codigoItem;
	private String descricaoItem;
	private String codigoClassificacaoItem;
	private String unidade;
	private String quantidadeContratada;
	private String quantidadeMedida;
	private String total;
	private String descontoRedutores;
	private String acrescimosDespesasAcessorias;
	private String bcIcms;
	private String icms;
	private String operacoesIsentasNaoTributaveis;
	private String outrosValores;
	private String aliquotaIcms;
	private String situacao;
	private String anoMesReferencia;
	private String numeroContrato;
	private String quantidadeFaturada;
	private String tarifaAplicadaPrecoMedioEfetivo;
	private String aliquotaPisPasep;
	private String pisPasep;
	private String aliquotaCofins;
	private String cofins;
	private String indicadorDescontoJudicial;
	private String tipoIsencaoReducaoBaseCalculo;
	private String codigoPontoConsumo;
	private String codigoFatura;
	private String faturaDataCancelamento;
	private String cnpjEmitente;
	private String indicadorIsencao;
	private String dataCancelamento;
	private String pontoConsumoTributoICMSIsento;

	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getClasseConsumo() {
		if(StringUtils.isBlank(this.classeConsumo)) {
			this.classeConsumo = "0";
		}

		return classeConsumo;
	}
	public void setClasseConsumo(String classeConsumo) {
		this.classeConsumo = classeConsumo;
	}
	public String getFaseTipoUtilizacao() {
		if(StringUtils.isBlank(this.faseTipoUtilizacao)) {
			this.faseTipoUtilizacao = "1";
		}

		return faseTipoUtilizacao;
	}
	public void setFaseTipoUtilizacao(String faseTipoUtilizacao) {
		this.faseTipoUtilizacao = faseTipoUtilizacao;
	}
	public String getGrupoTensao() {
		if(StringUtils.isBlank(this.grupoTensao)) {
			this.grupoTensao = "00";
		}

		return grupoTensao;
	}
	public void setGrupoTensao(String grupoTensao) {
		this.grupoTensao = grupoTensao;
	}
	@Override
	public String getDataEmissao() {
		return dataEmissao;
	}
	@Override
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	@Override
	public String getModelo() {
		if(StringUtils.isBlank(this.modelo)) {
			this.modelo = "01";
		}

		return modelo;
	}
	@Override
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	@Override
	public String getSerie() {
		return serie;
	}
	@Override
	public void setSerie(String serie) {
		this.serie = serie;
	}
	@Override
	public String getNumero() {
		if(StringUtils.isBlank(this.numero)) {
			this.numero = StringUtil.preencherEsquerda("0",
					CAMPO_DE_TAMANHO_NOVE, "0");
		}

		return StringUtil.preencherEsquerda(numero,
				CAMPO_DE_TAMANHO_NOVE, "0");
	}
	@Override
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCfop() {
		if(getCodigoClassificacaoItem().equals(MestreDocumentoFiscalLinha.CODIGOS_CFOP_ZERADO)) {
			return "0000";
		}
		return cfop;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}
	public String getNumeroOrdemItem() {
		return numeroOrdemItem;
	}
	public void setNumeroOrdemItem(String numeroOrdemItem) {
		this.numeroOrdemItem = numeroOrdemItem;
	}
	public String getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}
	public String getDescricaoItem() {
		return descricaoItem;
	}
	public void setDescricaoItem(String descricaoItem) {
		this.descricaoItem = descricaoItem;
	}
	public String getCodigoClassificacaoItem() {
		return codigoClassificacaoItem;
	}
	public void setCodigoClassificacaoItem(String codigoClassificacaoItem) {
		this.codigoClassificacaoItem = codigoClassificacaoItem;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public String getQuantidadeContratada() {
		if(StringUtils.isBlank(this.quantidadeContratada)) {
			this.quantidadeContratada = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOZE, "0");
		}

		return quantidadeContratada;
	}
	public void setQuantidadeContratada(String quantidadeContratada) {
		this.quantidadeContratada = quantidadeContratada;
	}
	public String getQuantidadeMedida() {
		return formataNumerico(quantidadeMedida, CAMPO_DE_TAMANHO_DOZE);
	}
	public void setQuantidadeMedida(String quantidadeMedida) {
		this.quantidadeMedida = quantidadeMedida;
	}
	public String getTotal() {
		return formataNumerico(total, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setTotal(String total) {
		this.total = total;
		
		if (verificarCancelamento()) {
			this.total = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_ONZE, "0");
		}
	}
	public String getDescontoRedutores() {
		return formataNumerico(descontoRedutores, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setDescontoRedutores(String descontoRedutores) {
		this.descontoRedutores = descontoRedutores;
	}
	public String getAcrescimosDespesasAcessorias() {
		return formataNumerico(acrescimosDespesasAcessorias, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setAcrescimosDespesasAcessorias(String acrescimosDespesasAcessorias) {
		this.acrescimosDespesasAcessorias = acrescimosDespesasAcessorias;
	}
	public String getBcIcms() {
		return formataNumerico(bcIcms, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setBcIcms(String bcIcms) {
		this.bcIcms = bcIcms;
	}
	public String getIcms() {
		return formataNumerico(icms, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setIcms(String icms) {
		this.icms = icms;

		if (!StringUtils.isBlank(this.dataCancelamento) || this.pontoConsumoTributoICMSIsento != null) {
			this.icms = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_ONZE, "0");
		}
	}
	public String getOperacoesIsentasNaoTributaveis() {
		return formataNumerico(this.operacoesIsentasNaoTributaveis, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setOperacoesIsentasNaoTributaveis(String operacoesIsentasNaoTributaveis) {
		this.operacoesIsentasNaoTributaveis = operacoesIsentasNaoTributaveis;
		if (verificarCancelamento()) {
			this.operacoesIsentasNaoTributaveis = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_ONZE, "0");
		}
	}
	public String getOutrosValores() {
		return formataNumerico(this.outrosValores, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setOutrosValores(String outrosValores) {
		this.outrosValores = outrosValores;
		if (verificarCancelamento()) {
			this.outrosValores = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_ONZE, "0");
		}
		
	}
	public String getAliquotaIcms() {
		return formataNumerico(aliquotaIcms, CAMPO_DE_TAMANHO_QUATRO);
	}
	public void setAliquotaIcms(String aliquotaIcms) {
		this.aliquotaIcms = aliquotaIcms;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getAnoMesReferencia() {
		return anoMesReferencia;
	}
	public void setAnoMesReferencia(String anoMesReferencia) {
		this.anoMesReferencia = anoMesReferencia;
	}
	public String getNumeroContrato() {
		if(StringUtils.isBlank(this.numeroContrato)) {
			this.numeroContrato = StringUtil.preencherDireita("", CAMPO_DE_TAMANHO_QUINZE, "");
		}
		return numeroContrato;
	}
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getQuantidadeFaturada() {
		return formataNumerico(quantidadeFaturada, CAMPO_DE_TAMANHO_DOZE);
	}
	public void setQuantidadeFaturada(String quantidadeFaturada) {
		this.quantidadeFaturada = quantidadeFaturada;
	}
	public String getTarifaAplicadaPrecoMedioEfetivo() {
		if(StringUtils.isBlank(this.tarifaAplicadaPrecoMedioEfetivo)) {
			this.tarifaAplicadaPrecoMedioEfetivo = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_ONZE, "0");
		}

		return tarifaAplicadaPrecoMedioEfetivo;
	}
	public void setTarifaAplicadaPrecoMedioEfetivo(String tarifaAplicadaPrecoMedioEfetivo) {
		this.tarifaAplicadaPrecoMedioEfetivo = tarifaAplicadaPrecoMedioEfetivo;
	}
	public String getAliquotaPisPasep() {
		return formataNumerico(aliquotaPisPasep, CAMPO_DE_TAMANHO_SEIS);
	}
	public void setAliquotaPisPasep(String aliquotaPisPasep) {
		this.aliquotaPisPasep = aliquotaPisPasep;
	}
	public String getPisPasep() {
		return formataNumerico(pisPasep, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setPisPasep(String pisPasep) {
		this.pisPasep = pisPasep;
	}
	public String getAliquotaCofins() {
		return formataNumerico(aliquotaCofins, CAMPO_DE_TAMANHO_SEIS);
	}
	public void setAliquotaCofins(String aliquotaCofins) {
		this.aliquotaCofins = aliquotaCofins;
	}
	public String getCofins() {
		return formataNumerico(cofins, CAMPO_DE_TAMANHO_ONZE);
	}
	public void setCofins(String cofins) {
		this.cofins = cofins;
	}
	public String getIndicadorDescontoJudicial() {
		if(StringUtils.isBlank(this.indicadorDescontoJudicial)) {
			this.indicadorDescontoJudicial = StringUtil.preencherDireita("", CAMPO_DE_TAMANHO_UM, "");
		}

		return indicadorDescontoJudicial;
	}
	public void setIndicadorDescontoJudicial(String indicadorDescontoJudicial) {
		this.indicadorDescontoJudicial = indicadorDescontoJudicial;
	}
	public String getTipoIsencaoReducaoBaseCalculo() {
		if(StringUtils.isBlank(this.tipoIsencaoReducaoBaseCalculo)) {
			this.tipoIsencaoReducaoBaseCalculo = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOIS, "0");
		}

		return tipoIsencaoReducaoBaseCalculo;
	}
	public void setTipoIsencaoReducaoBaseCalculo(String tipoIsencaoReducaoBaseCalculo) {
		this.tipoIsencaoReducaoBaseCalculo = tipoIsencaoReducaoBaseCalculo;
	}
	public String getCodigoPontoConsumo() {
		return codigoPontoConsumo;
	}
	public void setCodigoPontoConsumo(String codigoPontoConsumo) {
		this.codigoPontoConsumo = codigoPontoConsumo;
	}
	public String getCodigoFatura() {
		return codigoFatura;
	}
	public void setCodigoFatura(String codigoFatura) {
		this.codigoFatura = codigoFatura;
	}
	public String getFaturaDataCancelamento() {
		return faturaDataCancelamento;
	}
	public void setFaturaDataCancelamento(String faturaDataCancelamento) {
		this.faturaDataCancelamento = faturaDataCancelamento;
	}
	public String getCnpjEmitente() {
		return cnpjEmitente;
	}
	public void setCnpjEmitente(String cnpjEmitente) {
		this.cnpjEmitente = cnpjEmitente;
	}
	public String getIndicadorIsencao() {
		return indicadorIsencao;
	}
	public void setIndicadorIsencao(String indicadorIsencao) {
		this.indicadorIsencao = indicadorIsencao;
	}
	public String getDataCancelamento() {
		return dataCancelamento;
	}
	public void setDataCancelamento(String dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}
	public String getPontoConsumoTributoICMSIsento() {
		return pontoConsumoTributoICMSIsento;
	}
	public void setPontoConsumoTributoICMSIsento(String pontoConsumoTributoICMSIsento) {
		this.pontoConsumoTributoICMSIsento = pontoConsumoTributoICMSIsento;
	}
	private boolean verificarCancelamento() {
		return this.dataCancelamento != null && !StringUtils.isBlank(this.dataCancelamento);
	}
}
