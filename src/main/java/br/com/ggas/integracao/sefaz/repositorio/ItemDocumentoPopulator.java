/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.repositorio;

import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLinha;

/**
 * Classe ItemDocumentoPopulator
 *
 * @author ProdigaSistemas
 * 
 */
public class ItemDocumentoPopulator extends RegistroSefazPopulator {
	
	private static final String[] CAMPOS = new String[] {
			"cnpjCpf",
			"uf",
			"classeConsumo",
			"faseTipoUtilizacao",
			"grupoTensao",
			"dataEmissao",
			"modelo",
			"serie",
			"numero",
			"cfop",
			"numeroOrdemItem",
			"codigoItem",
			"descricaoItem",
			"codigoClassificacaoItem",
			"unidade",
			"quantidadeContratada",
			"quantidadeMedida",
			"total",
			"descontoRedutores",
			"acrescimosDespesasAcessorias",
			"bcIcms",
			"icms",
			"operacoesIsentasNaoTributaveis",
			"outrosValores",
			"aliquotaIcms",
			"situacao",
			"anoMesReferencia",
			"numeroContrato",
			"quantidadeFaturada",
			"tarifaAplicadaPrecoMedioEfetivo",
			"aliquotaPisPasep",
			"pisPasep",
			"aliquotaCofins",
			"cofins",
			"indicadorDescontoJudicial",
			"tipoIsencaoReducaoBaseCalculo",
			"codigoPontoConsumo",
			"codigoFatura",
			"faturaDataCancelamento",
			"cnpjEmitente",
			"indicador_isencao",
			"data_cancelamento",
			"ponto_consumo_tributo_aliquota"
	};

	@Override
	public RegistroSefazTipo getRegistroSefaz() {
		return new ItemDocumentoFiscalLinha();
	}

	@Override
	String[] getCampos() {
		return CAMPOS;
	}
}
