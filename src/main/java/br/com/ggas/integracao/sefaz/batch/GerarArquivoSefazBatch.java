/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.batch;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ggas.batch.Batch;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.sefaz.ControladorSefaz;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipoEnum;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * Classe batch para o arquivo sefaz
 * 
 * @author ProdigaSistemas
 */
@Component
public class GerarArquivoSefazBatch implements Batch {

	@Autowired
	private ControladorSefaz controladorSefaz;

	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {
		StringBuilder logProcessamento = new StringBuilder();

		String anoMesReferencia = (String) parametros.get("anoMesReferencia");

		if (anoMesReferencia == null) {
			throw new GGASException("Parâmetro ano mês referência é obrigatório!");
		}

		String diretorio = getDiretorioConfigurado();

		logParametrosConfigurados(logProcessamento, diretorio, anoMesReferencia);

		String anoMes = anoMesReferencia.split("/")[0] + anoMesReferencia.split("/")[1];
		List<String> series = controladorSefaz.consultarSeries(anoMes);
		logProcessamento.append("\nSéries válidas do período: ");
		logProcessamento.append(String.join(", ", series));

		logProcessamento.append("\nGerando arquivo Item Documento Fiscal...");
		logProcessamento
				.append(controladorSefaz.gerarArquivo(diretorio, anoMes, series, RegistroSefazTipoEnum.ITEM).toString());

		logProcessamento.append("\nGerando arquivo Mestre Documento Fiscal...");
		logProcessamento.append(
				controladorSefaz.gerarArquivo(diretorio, anoMes, series, RegistroSefazTipoEnum.MESTRE).toString());

		logProcessamento.append("\nGerando arquivo Dados Cadastrais...");
		logProcessamento.append(controladorSefaz
				.gerarArquivo(diretorio, anoMes, series, RegistroSefazTipoEnum.DADOSCADASTRAIS).toString());

		return logProcessamento.toString();
	}

	private String getDiretorioConfigurado() throws NegocioException {
		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		return (String) controladorParametrosSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_SEFAZ);
	}

	private void logParametrosConfigurados(StringBuilder logProcessamento, String diretorio, String anoMesReferencia) {
		logProcessamento.append("DIRETORIO SELECIONADO: ");
		logProcessamento.append(diretorio);
		logProcessamento.append("\n");
		logProcessamento.append("ANO MES REFERENCIA: ");
		logProcessamento.append(anoMesReferencia);
	}
}
