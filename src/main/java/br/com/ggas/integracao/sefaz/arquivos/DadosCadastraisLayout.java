/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

/**
 * Classe DadosCadastraisLayout 
 *
 * @author ProdigaSistemas
 * 
 */
public class DadosCadastraisLayout extends AbstractArquivoLayout<DadosCadastraisLinha> {
	
	private static final String TIPO_LAYOUT = "D";
	
	/**
	 * Construtor da classe
	 * 
	 * @param dados da linha de cadastro para o arquivo sefaz 
	 */
	public DadosCadastraisLayout(DadosCadastraisLinha dados) {
		super(dados);
	}

	@Override
	public void gerarLinha() {		
		StringBuilder linha = new StringBuilder();
		
		linha.append(dados.getCnpjCpf());
		linha.append(dados.getInscricaoEstadual());
		linha.append(dados.getRazaoSocial());
		linha.append(dados.getLogradouro());
		linha.append(dados.getNumeroLogradouro());
		linha.append(dados.getComplemento());
		linha.append(dados.getCep());
		linha.append(dados.getBairro());
		linha.append(dados.getMunicipio());
		linha.append(dados.getUf());
		linha.append(dados.getTelefoneContato());
		linha.append(dados.getCodigoConsumidor());
		linha.append(dados.getNumeroTelefonicoConsumidora());
		linha.append(dados.getUfTelefonico());
		linha.append(dados.getDataEmissao());
		linha.append(dados.getModelo());
		linha.append(dados.getSerie());
		linha.append(dados.getNumero());
		linha.append(dados.getCodigoMunicipio());
		linha.append(getEspacosBrancos(5));
		
		setLinha(linha.toString());
	}

	public static String getTipo() {
		return TIPO_LAYOUT;
	}
}
