/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz;

import java.util.List;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipoEnum;

/**
 * Interface ControladorSefaz
 *
 * @author ProdigaSistemas
 */
public interface ControladorSefaz {

	final String BEAN_ID_CONTROLADOR_SEFAZ = "controladorSefaz";

	/**
	 * Método para gerar item de arquivo
	 * 
	 * @param diretorio endereco do arquivo
	 * @param anoMesReferencia  data de referencia
	 * @param serie numero de serie
	 * @return string de dados para o item de arquivo
	 * @throws GGASException GGAS exception
	 */
	String gerarArquivoItemDocumentoFiscal(String diretorio, String anoMesReferencia, String serie)
			throws GGASException;

	/**
	 * Método para gerar arquivo mestre
	 * 
	 * @param diretorio endereco do arquivo
	 * @param anoMesReferencia  data de referencia
	 * @param serie numero de serie
	 * @return string de dados para o arquivo mestre
	 * @throws GGASException GGAS exception
	 */
	String gerarArquivoMestreDocumentoFiscal(String diretorio, String anoMesReferencia, String serie)
			throws GGASException;

	/**
	 * Método para gerar dados cadastrais para o arquivo
	 * 
	 * @param diretorio endereco do arquivo
	 * @param anoMesReferencia  data de referencia
	 * @param serie numero de serie
	 * @return string de dados cadastrais para o arquivo
	 * @throws GGASException GGAS exception
	 */
	String gerarArquivoDadosCadastrais(String diretorio, String anoMesReferencia, String serie) throws GGASException;

	/**
	 * Método para buscar serie no repositorio sefaz
	 * 
	 * @param anoMesReferencia  data de referencia
	 * @return lista de strings encontradas no repositorio
	 */
	List<String> consultarSeries(String anoMesReferencia);

	/**
	 * Método para gerar mapa de dados para o arquivo
	 * 
	 * @param diretorio endereco do arquivo
	 * @param anoMesReferencia  data de referencia
	 * @param series numeros de serie
	 * @param tipoArquivo tipo de registro sefa
	 * @return stringbuilder de mestre, itens e dados cadastrais para o arquivo
	 * @throws GGASException GGAS exception
	 */
	StringBuilder gerarArquivo(String diretorio, String anoMesReferencia, List<String> series,
			RegistroSefazTipoEnum tipoArquivo) throws GGASException;

	/**
	 * Método para gerar mapa de dados para o arquivo
	 * 
	 * @param diretorio endereco do arquivo
	 * @param anoMesReferencia  data de referencia
	 * @param serie numero de serie
	 * @param tipoArquivo tipo de registro sefa
	 * @return string de mestre, itens e dados cadastrais para o arquivo
	 * @throws GGASException GGAS exception
	 */
	String gerarArquivo(String diretorio, String anoMesReferencia, String serie, RegistroSefazTipoEnum tipoArquivo)
			throws GGASException;
}
