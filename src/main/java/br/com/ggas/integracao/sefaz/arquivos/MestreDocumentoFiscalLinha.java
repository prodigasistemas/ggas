/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipo;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.StringUtil;

/**
 * Classe MestreDocumentoFiscalLinha
 *
 * @author ProdigaSistemas
 * 
 */
public class MestreDocumentoFiscalLinha extends RegistroSefazTipo {
	public static final String CODIGOS_CFOP_ZERADO = "0899";

	private String uf;
	private String classeConsumo;
	private String faseOuTipoUtilizacao;
	private String grupoTensao;
	private String codigoIdentificacaoConsumidorAssinante;
	private String codigoAutenticacaoDigital;
	private String valorTotal;
	private String bcIcms;
	private String icmsDestacado;
	private String operacoesIsentasNaoTributadas;
	private String outrosValores;
	private String situacaoDocumento;
	private String anoMesReferenciaApuracao;
	private String referenciaItemNF;
	private String numeroTerminalTelefonicoUnidadeConsumidora;
	private String indicacaoTipoInformacaoCampo1;
	private String tipoCliente;
	private String subclasseConsumo;
	private String numeroTerminalTelefonicoPrincipal;
	private String cnpjEmitente;
	private String numeroCodigoFaturaComercial;
	private String valorTotalFaturaComercial;
	private String dataLeituraAnterior;
	private String dataLeituraAtual;
	private String campo32;
	private String campo33;
	private String informacoesAdicionais;
	private String campo35;
	private String codigoPontoConsumo;
	private String indicadorIsencao;
	private String dataCancelamento;
	private String pontoConsumoTributoICMSIsento;
	private String cfop;
	private String codigoClassificacaoItem;
	private String tipoPessoa;

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getClasseConsumo() {
		if (StringUtils.isBlank(this.classeConsumo)) {
			this.classeConsumo = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_UM, "0");
		}

		return classeConsumo;
	}

	public void setClasseConsumo(String classeConsumo) {
		this.classeConsumo = classeConsumo;
	}

	public String getFaseOuTipoUtilizacao() {
		if (StringUtils.isBlank(this.faseOuTipoUtilizacao)) {
			this.faseOuTipoUtilizacao = StringUtil.preencherEsquerda("1", CAMPO_DE_TAMANHO_UM, "0");
		}

		return faseOuTipoUtilizacao;
	}

	public void setFaseOuTipoUtilizacao(String faseOuTipoUtilizacao) {
		this.faseOuTipoUtilizacao = faseOuTipoUtilizacao;
	}

	public String getGrupoTensao() {
		if (StringUtils.isBlank(this.grupoTensao)) {
			this.grupoTensao = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOIS, "0");
		}

		return grupoTensao;
	}

	public void setGrupoTensao(String grupoTensao) {
		this.grupoTensao = grupoTensao;
	}

	public String getCodigoIdentificacaoConsumidorAssinante() {
		return codigoIdentificacaoConsumidorAssinante;
	}

	public void setCodigoIdentificacaoConsumidorAssinante(String codigoIdentificacaoConsumidorAssinante) {
		this.codigoIdentificacaoConsumidorAssinante = codigoIdentificacaoConsumidorAssinante;
	}

	public String getCodigoAutenticacaoDigitalDocumentoFiscal() {
		StringBuilder info = new StringBuilder();

		info.append(getCnpjCpf());
		info.append(getNumero());
		info.append(getValorTotal());
		info.append(getBcIcms());
		info.append(getIcmsDestacado());
		info.append(getDataEmissao());
		info.append(getCnpjEmitente());

		return info.toString();
	}

	public String getValorTotal() {
		return formataNumerico(valorTotal, CAMPO_DE_TAMANHO_DOZE);
	}

	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;

		if (verificarCancelamento()) {
			this.valorTotal = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOZE, "0");
		}
	}

	public String getBcIcms() {
		return formataNumerico(bcIcms, CAMPO_DE_TAMANHO_DOZE);
	}

	public void setBcIcms(String bcIcms) {
		this.bcIcms = bcIcms;

		if (verificarCancelamento() || this.pontoConsumoTributoICMSIsento != null) {
			this.bcIcms = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOZE, "0");
		}
	}

	public String getIcmsDestacado() {
		return formataNumerico(icmsDestacado, CAMPO_DE_TAMANHO_DOZE);
	}

	public void setIcmsDestacado(String icmsDestacado) {
		this.icmsDestacado = icmsDestacado;

		if (verificarCancelamento() || this.pontoConsumoTributoICMSIsento != null) {
			this.icmsDestacado = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOZE, "0");
		}
	}

	public String getOperacoesIsentasNaoTributadas() {
		return formataNumerico(operacoesIsentasNaoTributadas, CAMPO_DE_TAMANHO_DOZE);
	}

	public void setOperacoesIsentasNaoTributadas(String operacoesIsentasNaoTributadas) {
		this.operacoesIsentasNaoTributadas = operacoesIsentasNaoTributadas;
		
		if (verificarCancelamento()) {
			this.operacoesIsentasNaoTributadas = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOZE, "0");
		}		
	}

	public String getOutrosValores() {
		return formataNumerico(this.outrosValores, CAMPO_DE_TAMANHO_DOZE);
	}

	public void setOutrosValores(String outrosValores) {
		this.outrosValores = outrosValores;
		
		if (verificarCancelamento()) {
			this.outrosValores = StringUtil.preencherEsquerda("0", CAMPO_DE_TAMANHO_DOZE, "0");
		}		
	}

	public String getSituacaoDocumento() {
		return situacaoDocumento;
	}

	public void setSituacaoDocumento(String situacaoDocumento) {
		this.situacaoDocumento = situacaoDocumento;

		if (this.dataCancelamento != null) {
			this.situacaoDocumento = "S";
		}
	}

	public String getAnoMesReferenciaApuracao() {
		return anoMesReferenciaApuracao;
	}

	public void setAnoMesReferenciaApuracao(String anoMesReferenciaApuracao) {
		this.anoMesReferenciaApuracao = anoMesReferenciaApuracao;
	}

	public String getReferenciaItemNF() {
		return referenciaItemNF;
	}

	public void setReferenciaItemNF(String referenciaItemNF) {
		this.referenciaItemNF = referenciaItemNF;
	}

	public String getNumeroTerminalTelefonicoUnidadeConsumidora() {
		if (StringUtils.isBlank(this.numeroTerminalTelefonicoUnidadeConsumidora)) {
			this.numeroTerminalTelefonicoUnidadeConsumidora = StringUtil.preencherDireita("", 
					CAMPO_DE_TAMANHO_DOZE, "");
		}

		return numeroTerminalTelefonicoUnidadeConsumidora;
	}

	public void setNumeroTerminalTelefonicoUnidadeConsumidora(String numeroTerminalTelefonicoUnidadeConsumidora) {
		this.numeroTerminalTelefonicoUnidadeConsumidora = numeroTerminalTelefonicoUnidadeConsumidora;
	}

	public String getIndicacaoTipoInformacaoCampo1() {
		if (StringUtils.isBlank(this.indicacaoTipoInformacaoCampo1) && 
				tipoPessoa.equals(String.valueOf(Constantes.PESSOA_JURIDICA))) {
			if(Long.parseLong(getCnpjCpf()) > 0L) {
				return "1";
			}
			return "3";
		}

		if(StringUtils.isBlank(this.indicacaoTipoInformacaoCampo1) &&
				tipoPessoa.equals(String.valueOf(Constantes.PESSOA_FISICA))) {
			if(Long.parseLong(getCnpjCpf()) > 0L) {
				return "2";
			}
			return "4";
		}

		return indicacaoTipoInformacaoCampo1;
	}

	public void setIndicacaoTipoInformacaoCampo1(String indicacaoTipoInformacaoCampo1) {
		this.indicacaoTipoInformacaoCampo1 = indicacaoTipoInformacaoCampo1;
	}

	public String getTipoCliente() {
		if (StringUtils.isBlank(this.tipoCliente)) {
			return "99";
		}
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getSubclasseConsumo() {
		if (StringUtils.isBlank(this.subclasseConsumo)) {
			this.subclasseConsumo = StringUtil.preencherEsquerda("00", 
					CAMPO_DE_TAMANHO_DOIS, "0");
		}

		return subclasseConsumo;
	}

	public void setSubclasseConsumo(String subclasseConsumo) {
		this.subclasseConsumo = subclasseConsumo;
	}

	public String getNumeroTerminalTelefonicoPrincipal() {
		if (StringUtils.isBlank(this.dataLeituraAnterior)) {
			this.numeroTerminalTelefonicoPrincipal = StringUtil.preencherDireita("", 
					CAMPO_DE_TAMANHO_DOZE, "");
		}

		return numeroTerminalTelefonicoPrincipal;
	}

	public void setNumeroTerminalTelefonicoPrincipal(String numeroTerminalTelefonicoPrincipal) {
		this.numeroTerminalTelefonicoPrincipal = numeroTerminalTelefonicoPrincipal;
	}

	public String getCnpjEmitente() {
		return cnpjEmitente;
	}

	public void setCnpjEmitente(String cnpjEmitente) {
		this.cnpjEmitente = cnpjEmitente;
	}

	public String getNumeroCodigoFaturaComercial() {
		return numeroCodigoFaturaComercial;
	}

	public void setNumeroCodigoFaturaComercial(String numeroCodigoFaturaComercial) {
		this.numeroCodigoFaturaComercial = numeroCodigoFaturaComercial;
	}

	public String getValorTotalFaturaComercial() {
		return formataNumerico(valorTotalFaturaComercial, CAMPO_DE_TAMANHO_DOZE);
	}

	public void setValorTotalFaturaComercial(String valorTotalFaturaComercial) {
		this.valorTotalFaturaComercial = valorTotalFaturaComercial;
	}

	public String getDataLeituraAnterior() {
		if (StringUtils.isBlank(this.dataLeituraAnterior)) {
			this.dataLeituraAnterior = StringUtil.preencherEsquerda("0", 
					CAMPO_DE_TAMANHO_OITO, "0");
		}
		return dataLeituraAnterior;
	}

	public void setDataLeituraAnterior(String dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}

	public String getDataLeituraAtual() {
		if (StringUtils.isBlank(this.dataLeituraAtual)) {
			this.dataLeituraAtual = StringUtil.preencherEsquerda("0", 
					CAMPO_DE_TAMANHO_OITO, "0");
		}
		return dataLeituraAtual;
	}

	public void setDataLeituraAtual(String dataLeituraAtual) {
		this.dataLeituraAtual = dataLeituraAtual;
	}

	public String getInformacoesAdicionais() {
		if (StringUtils.isBlank(this.informacoesAdicionais)) {
			this.informacoesAdicionais = StringUtil.preencherDireita("", 
					CAMPO_DE_TAMANHO_TRINTA, "");
		}
		return informacoesAdicionais;
	}

	public void setInformacoesAdicionais(String informacoesAdicionais) {
		this.informacoesAdicionais = informacoesAdicionais;
	}

	public String getCodigoAutenticacaoDigital() {
		return codigoAutenticacaoDigital;
	}

	public void setCodigoAutenticacaoDigital(String codigoAutenticacaoDigital) {
		this.codigoAutenticacaoDigital = codigoAutenticacaoDigital;
	}

	public String getCampo32() {
		return campo32;
	}

	public void setCampo32(String campo32) {
		this.campo32 = campo32;
	}

	public String getCampo33() {
		return campo33;
	}

	public void setCampo33(String campo33) {
		this.campo33 = campo33;
	}

	public String getCampo35() {
		return campo35;
	}

	public void setCampo35(String campo35) {
		this.campo35 = campo35;
	}

	public String getCodigoPontoConsumo() {
		return codigoPontoConsumo;
	}

	public void setCodigoPontoConsumo(String codigoPontoConsumo) {
		this.codigoPontoConsumo = codigoPontoConsumo;
	}

	public String getIndicadorIsencao() {
		return indicadorIsencao;
	}

	public void setIndicadorIsencao(String indicadorIsencao) {
		this.indicadorIsencao = indicadorIsencao;
	}

	public String getDataCancelamento() {
		return dataCancelamento;
	}

	public void setDataCancelamento(String dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public String getPontoConsumoTributoICMSIsento() {
		return pontoConsumoTributoICMSIsento;
	}

	public void setPontoConsumoTributoAliquotaICMSIsento(String pontoConsumoTributoICMSIsento) {
		this.pontoConsumoTributoICMSIsento = pontoConsumoTributoICMSIsento;
	}

	public String getCfop() {
		if (getCodigoClassificacaoItem().equals(CODIGOS_CFOP_ZERADO)) {
			return "0000";
		}
		return cfop;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	private boolean verificarCancelamento() {
		return this.dataCancelamento != null && !StringUtils.isBlank(this.dataCancelamento);
	}

	public String getCodigoClassificacaoItem() {
		return codigoClassificacaoItem;
	}

	public void setCodigoClassificacaoItem(String codigoClassificacaoItem) {
		this.codigoClassificacaoItem = codigoClassificacaoItem;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	public String getTipoPessoa() {
		return this.tipoPessoa;
	}
}
