/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA

 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.repositorio;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Responsável pelo preenchimento dos campos dos
 * especificados pelo Layout.
 **/
public abstract class RegistroSefazPopulator {
	public abstract RegistroSefazTipo getRegistroSefaz();
	abstract String[] getCampos();
	private static final Logger logger = Logger.getLogger(RegistroSefazPopulator.class);

	/**
	 * Método responsável por enriquecer o
	 * arquivo
	 *
	 * @param linha
	 *            the linha
	 * @return RegistroSefaTipo
	 */
	public RegistroSefazTipo popularObjeto(Object[] linha) {
		Map<Integer, String> mapper = getMapper();
		logger.info("Tamanho linha: " + mapper.size());
		RegistroSefazTipo objeto = getRegistroSefaz();
		mapper.entrySet().forEach(item -> {
			logger.info("Index: " + (item.getKey()-1) + " / Valor: " + (String) linha[item.getKey()-1] + " / Campo: " + item.getValue());
			String valorCampo = (String) linha[item.getKey()-1];
			String nomeCampo = item.getValue();
			setAtributo(objeto, nomeCampo, valorCampo);
		});
		return objeto;
	}

	public Map<Integer, String> getMapper() {
		/*
		 * Mapeamento de posição do  campo com o nome do atributo
		 * pra ser feito a conversão automática
		 */
		String[] campos = getCampos();
		HashMap<Integer, String> map = new HashMap<>();
		for (int i = 0; i < campos.length; i++) {
			map.put(i + 1, campos[i]);
		}

		return map;
	}

	static void setAtributo(RegistroSefazTipo obj, String atributo, String valor) {
	    Class<?> clazz = obj.getClass();
	    while (clazz != null) {
	        try {
	            Field field = clazz.getDeclaredField(atributo);
	            field.setAccessible(true);
	            field.set(obj, valor);
	            return;
	        } catch (NoSuchFieldException e) {
	            clazz = clazz.getSuperclass();
	        } catch (Exception e) {
	            throw new IllegalStateException(e);
	        }
	    }
	}
}
