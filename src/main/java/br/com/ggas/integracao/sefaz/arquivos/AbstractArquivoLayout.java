/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

import br.com.ggas.util.StringUtil;


/**
 * Implementação genérica do layout sefaz descrito em {@link Layout}
 *
 * @param <T> tipo da entidade
 *
 * @author ProdigaSistemas
 */
//classe com abstrações do arquivo e layout principal
public abstract class AbstractArquivoLayout<T> implements Layout {
	
	private String linha;
	//generalização de tipo de dados
	protected T dados;
	
	/**
	 * Construtor padrão
	 * 
	 * @param dados do layout
	 */
	//layouts será definido a partir dos dados de entrada
	public AbstractArquivoLayout(T dados) {
		//dados de entrada para a definição do layout
		this.dados = dados;
	}

	@Override
	public String getLinha() {
		//verifica a necessidade de gerar uma linha
		if(this.linha == null) {
			//gerar linha quando estiver nula
			gerarLinha();
		}
		
		//retorna linha com caracteres normalizados
		return normalizar(linha);
	}

	/**
	 * Método set para linha
	 * @param linha de arquivo
	 */
	public void setLinha(String linha) {
		this.linha = linha;
	}
	
	protected String getEspacosBrancos(int quantidade) {
		return StringUtil.preencherDireita("", quantidade, ""); 
	}
	
	protected String getZeros(int quantidade) {
		return StringUtil.preencherEsquerda("0", quantidade, "0");
	}
	
	protected String normalizar(String linha) {
		return linha
				.replaceAll("º", "o")
				.replaceAll("ª", "a")
				.replaceAll("²", "2")
				.replaceAll("³", "3");
	}
}
