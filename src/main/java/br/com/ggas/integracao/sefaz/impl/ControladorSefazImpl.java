/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.sefaz.ControladorSefaz;
import br.com.ggas.integracao.sefaz.arquivos.AbstractArquivoLayout;
import br.com.ggas.integracao.sefaz.arquivos.ArquivoSefaz;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLayout;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipoEnum;
import br.com.ggas.integracao.sefaz.repositorio.RepositorioSefaz;
import br.com.ggas.util.Constantes;

/**
 * Classe ControladorSefazImpl
 *
 * @author ProdigaSistemas
 */
@Service("controladorSefaz")
public class ControladorSefazImpl implements ControladorSefaz {
	
	@Autowired
	private RepositorioSefaz repositorioSefaz;

	@Override
	@Transactional
	public StringBuilder gerarArquivo(String diretorio, String anoMesReferencia, 
			List<String> series, RegistroSefazTipoEnum tipoArquivo)
			throws GGASException {
		StringBuilder logProcessamento = new StringBuilder();

		for (String serie : series) {
			String arquivoGerado = gerarArquivo(diretorio, anoMesReferencia, serie, tipoArquivo);
			logArquivoGerado(logProcessamento, arquivoGerado, serie);
		}
		return logProcessamento;
	}

	@Override
	@Transactional
	public String gerarArquivo(String diretorio, String anoMesReferencia, String serie,
			RegistroSefazTipoEnum tipoArquivo)
			throws GGASException {
		String arquivoGerado = "";
		switch (tipoArquivo) {
			case MESTRE:
				arquivoGerado = gerarArquivoMestreDocumentoFiscal(diretorio, anoMesReferencia, serie);
				break;
			case ITEM:
				arquivoGerado = gerarArquivoItemDocumentoFiscal(diretorio, anoMesReferencia, serie);
				break;
			case DADOSCADASTRAIS:
				arquivoGerado = gerarArquivoDadosCadastrais(diretorio, anoMesReferencia, serie);
				break;
		}
		return arquivoGerado;
	}

	@Override
	@Transactional
	public String gerarArquivoItemDocumentoFiscal(String diretorio, String anoMesReferencia, String serie)
			throws GGASException {
		List<ItemDocumentoFiscalLayout> linhas = repositorioSefaz.consultarItemDocumentoFiscal(anoMesReferencia, serie);

		ArquivoSefaz<ItemDocumentoFiscalLayout> arquivo = new ArquivoSefaz<ItemDocumentoFiscalLayout>(anoMesReferencia,
				serie, ItemDocumentoFiscalLayout.getTipo());

		return gerarArquivo(arquivo, diretorio, linhas);
	}

	@Override
	@Transactional
	public String gerarArquivoMestreDocumentoFiscal(String diretorio, String anoMesReferencia, String serie)
			throws GGASException {
		List<MestreDocumentoFiscalLayout> linhas = repositorioSefaz.consultarMestreDocumentoFiscal(anoMesReferencia, serie);

		ArquivoSefaz<MestreDocumentoFiscalLayout> arquivo = new ArquivoSefaz<MestreDocumentoFiscalLayout>(
				anoMesReferencia, serie, MestreDocumentoFiscalLayout.getTipo());

		return gerarArquivo(arquivo, diretorio, linhas);
	}

	@Override
	@Transactional
	public String gerarArquivoDadosCadastrais(String diretorio, String anoMesReferencia, String serie)
			throws GGASException {
		List<DadosCadastraisLayout> linhas = repositorioSefaz.consultarDadosCadastrais(anoMesReferencia, serie);

		ArquivoSefaz<DadosCadastraisLayout> arquivo = new ArquivoSefaz<DadosCadastraisLayout>(anoMesReferencia, serie,
				DadosCadastraisLayout.getTipo());

		return gerarArquivo(arquivo, diretorio, linhas);
	}

	/**
	 * Método para gerar mapa de dados para o arquivo
	 * 
	 * @param arquivo  o arquivo sefaz
	 * @param diretorio diretorio do arquivo
	 * @param linhas lista de linhas
	 * @return string de mestre, itens e dados cadastrais para o arquivo
	 */
	public <T extends AbstractArquivoLayout<?>> String gerarArquivo(ArquivoSefaz<T> arquivo, String diretorio, List<T> linhas) {
		if(linhas.isEmpty()) {
			return "SEM DADOS PARA GERAR ARQUIVO [ SERIE: " + arquivo.getSerie() + " ]";
		}
		
		Map<String, String> arquivoParams = repositorioSefaz.consultarCnpjUfEmpresaPrincipal();
		arquivoParams.put(ArquivoSefaz.DIRETORIO, diretorio);
		
		if (arquivo.gerarArquivo(arquivoParams, linhas)) {
			return arquivo.getNomeArquivo();
		}

		return null;
	}

	@Override
	public List<String> consultarSeries(String anoMesReferencia) {
		return repositorioSefaz.consultarSeries(anoMesReferencia);
	}

	private void logArquivoGerado(StringBuilder logProcessamento, String arquivoGerado, String serie) throws GGASException {
		if (arquivoGerado == null) {
			throw new GGASException(Constantes.ARQUIVO_GERADO_NULO, true);
		}

		logProcessamento.append("\nARQUIVO GERADO [ SERIE: ");
		logProcessamento.append(serie);
		logProcessamento.append(" ]: ");
		logProcessamento.append(arquivoGerado);
	}
}
