/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA

 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.repositorio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.integracao.sefaz.arquivos.ArquivoSefaz;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLayout;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLinha;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLinha;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLinha;

/**
 * Classe do Repositorio Sefaz.
 * 
 *
 */
@Repository
public class RepositorioSefaz extends HibernateDaoSupport {

	private static final String SERIE_NR = "serie_nr";
	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";
	private static final String QUERY_MESTRE_DOCUMENTO_FISCAL = "documentoFiscal.consultarMestreDocumentoFiscal";
	private static final String QUERY_ITEM_DOCUMENTO_FISCAL = "documentoFiscal.consultarItemDocumentoFiscal";
	private static final String QUERY_DADOS_CADASTRAIS = "documentoFiscal.consultarDadosCadastrais";

	private RegistroSefazFactory registroSefazFactory;

	/**
	 * Instantiates a new repositorio servico autorizacao.
	 *
	 * @param sessionFactory the session factory
	 */
	@Autowired
	public RepositorioSefaz(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);

		registroSefazFactory = RegistroSefazFactory.factory(builder -> {
			builder.add(RegistroSefazTipoEnum.DADOSCADASTRAIS, DadosCadastraisPopulator::new);
			builder.add(RegistroSefazTipoEnum.MESTRE, MestreDocumentoPopulator::new);
			builder.add(RegistroSefazTipoEnum.ITEM, ItemDocumentoPopulator::new);
		});
	}

	/**
	 * Consultar Dados Cadastrais do Destinatário do Documento Fiscal.
	 *
	 * @param anoMesReferencia {@link String}
	 * @param serie {@link String}
	 * @return the list
	 */
	public List<DadosCadastraisLayout> consultarDadosCadastrais(String anoMesReferencia, String serie) {
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(QUERY_DADOS_CADASTRAIS);
		query.setParameter(SERIE_NR, serie);
		query.setParameter(ANO_MES_REFERENCIA, anoMesReferencia);

		List<DadosCadastraisLayout> lista = new ArrayList<>();

		RegistroSefazPopulator populator = registroSefazFactory.create(RegistroSefazTipoEnum.DADOSCADASTRAIS);

		for (Object obj : query.list()) {
			Object[] resultadoArray = (Object[]) obj;

			DadosCadastraisLinha dadosCadastrais = (DadosCadastraisLinha) populator.popularObjeto(resultadoArray);
			DadosCadastraisLayout layout = new DadosCadastraisLayout(dadosCadastrais);
			lista.add(layout);
		}

		return lista;
	}

	/**
	 * Consultar Mestre do Documento Fiscal.
	 *
	 * @param anoMesReferencia {@link String}
	 * @param serie {@link String}
	 * @return the list
	 */
	public List<MestreDocumentoFiscalLayout> consultarMestreDocumentoFiscal(String anoMesReferencia, String serie) {
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(QUERY_MESTRE_DOCUMENTO_FISCAL);
		query.setParameter(SERIE_NR, serie);
		query.setParameter(ANO_MES_REFERENCIA, anoMesReferencia);

		List<MestreDocumentoFiscalLayout> lista = new ArrayList<>();
		RegistroSefazPopulator populator = registroSefazFactory.create(RegistroSefazTipoEnum.MESTRE);

		for (Object obj : query.list()) {
			Object[] resultadoArray = (Object[]) obj;

			MestreDocumentoFiscalLinha mestreDocumentoFiscal = (MestreDocumentoFiscalLinha) populator
					.popularObjeto(resultadoArray);

			MestreDocumentoFiscalLayout layout = new MestreDocumentoFiscalLayout(mestreDocumentoFiscal);
			lista.add(layout);
		}

		return lista;
	}

	/**
	 * Consultar Item do Documento Fiscal.
	 *
	 * @param anoMesReferencia {@link String}
	 * @param serie {@link String}
	 * @return the list
	 */
	public List<ItemDocumentoFiscalLayout> consultarItemDocumentoFiscal(String anoMesReferencia, String serie) {
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(QUERY_ITEM_DOCUMENTO_FISCAL);
		query.setParameter(SERIE_NR, serie);
		query.setParameter(ANO_MES_REFERENCIA, anoMesReferencia);

		List<ItemDocumentoFiscalLayout> lista = new ArrayList<>();
		RegistroSefazPopulator populator = registroSefazFactory.create(RegistroSefazTipoEnum.ITEM);

		for (Object obj : query.list()) {
			Object[] resultadoArray = (Object[]) obj;

			ItemDocumentoFiscalLinha itemDocumentoFiscal = (ItemDocumentoFiscalLinha) populator
					.popularObjeto(resultadoArray);

			ItemDocumentoFiscalLayout layout = new ItemDocumentoFiscalLayout(itemDocumentoFiscal);

			lista.add(layout);
		}

		return lista;
	}

	/**
	 * Consultar Series de Documentos Fiscais de um Periodo.
	 *
	 * @param anoMesReferencia {@link String}
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<String> consultarSeries(String anoMesReferencia) {
		StringBuilder hql = new StringBuilder();
		hql.append("select distinct df.serie.numero ");
		hql.append(" from");
		hql.append(" DocumentoFiscalImpl df ");
		hql.append(" where");
		hql.append(" df.fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and df.serie.indicadorSerieEletronica = :indicadorSerieEletronica ");
		hql.append(" and df.serie.indicadorContingenciaScan = :indicadorContingenciaScan ");
		hql.append(" and df.serie.habilitado = :habilitado ");
		hql.append(" and df.serie.tipoOperacao.descricao = 'SAIDA' ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(ANO_MES_REFERENCIA, Integer.valueOf(anoMesReferencia));
		query.setParameter("indicadorSerieEletronica", false);
		query.setParameter("indicadorContingenciaScan", false);
		query.setParameter("habilitado", true);

		return query.list();
	}

	/**
	 * Consultar Ponto Consumo Tributo Aliquota pelo codigo do ponto de consumo
	 *
	 * @param pontoConsumoId identificador do ponto de consumo
	 * @return PontoConsumoTributoAliquota ponto de consumo encontrado
	 */
	public PontoConsumoTributoAliquota consultarPontoConsumoTributoAliquotaICMSIsento(String pontoConsumoId) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pcta ");
		hql.append(" from");
		hql.append(" PontoConsumoTributoAliquotaImpl pcta ");
		hql.append(" where");
		hql.append(" pcta.tributo.descricao = :trib_ds ");
		hql.append(" and pcta.indicadorIsencao = :indicador_isencao ");
		hql.append(" and pcta.habilitado = :indicador_uso ");
		hql.append(" and pcta.pontoConsumo.chavePrimaria = :pocn_cd ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("pocn_cd", Long.parseLong(pontoConsumoId));
		query.setParameter("trib_ds", "ICMS");
		query.setParameter("indicador_isencao", true);
		query.setParameter("indicador_uso", true);

		return (PontoConsumoTributoAliquota) query.uniqueResult();
	}

	/**
	 * Verifica documento CNPJ da empresa principal
	 *
	 * @return mapa de empresas encontradas na consulta
	 */
	public Map<String, String> consultarCnpjUfEmpresaPrincipal() {
		StringBuilder hql = new StringBuilder();
		hql.append("select clie.clie_nr_cnpj, uf.unfe_sg ");
		hql.append(" from cliente clie ");
		hql.append(" join empresa emp on (emp.clie_cd = clie.clie_cd and emp.empr_in_principal = 1) ");
		hql.append(" join cliente_endereco rcend on (emp.clie_cd = rcend.clie_cd) ");
		hql.append(" join cep on (rcend.cep_cd = cep.cep_cd) ");
		hql.append(" join municipio mun on (cep.muni_cd = mun.muni_cd) ");
		hql.append(" join unidade_federacao uf on (mun.unfe_cd = uf.unfe_cd) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		Object[] result = (Object[]) query.uniqueResult();

		Map<String, String> cnpjUfEmpresa = new HashMap<String, String>();
		cnpjUfEmpresa.put(ArquivoSefaz.CNPJ, (String) result[0]);
		cnpjUfEmpresa.put(ArquivoSefaz.UF, (String) result[1]);

		return cnpjUfEmpresa;
	}
}
