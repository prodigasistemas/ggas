/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

/**
 * Classe MestreDocumentoFiscalLayout
 *
 * @author ProdigaSistemas
 * 
 */
public class MestreDocumentoFiscalLayout extends AbstractArquivoLayout<MestreDocumentoFiscalLinha> {
	
	private static final String TIPO_LAYOUT = "M";
	
	/**
	 * Construtor da classe
	 * 
	 * @param dados parametros do documento fiscal 
	 */
	public MestreDocumentoFiscalLayout(MestreDocumentoFiscalLinha dados) {
		super(dados);	
	}

	@Override
	public void gerarLinha() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(dados.getCnpjCpf());
		builder.append(dados.getInscricaoEstadual());
		builder.append(dados.getRazaoSocial());
		builder.append(dados.getUf());
		builder.append(dados.getClasseConsumo());
		builder.append(dados.getFaseOuTipoUtilizacao());
		builder.append(dados.getGrupoTensao());
		builder.append(dados.getCodigoIdentificacaoConsumidorAssinante());
		builder.append(dados.getDataEmissao());
		builder.append(dados.getModelo());
		builder.append(dados.getSerie());
		builder.append(dados.getNumero());
		builder.append(ArquivoSefaz.getMD5Hash(dados.getCodigoAutenticacaoDigitalDocumentoFiscal()));
		builder.append(dados.getValorTotal());
		builder.append(dados.getBcIcms());
		builder.append(dados.getIcmsDestacado());
		builder.append(dados.getOperacoesIsentasNaoTributadas());
		builder.append(dados.getOutrosValores());
		builder.append(dados.getSituacaoDocumento());
		builder.append(dados.getAnoMesReferenciaApuracao());
		builder.append(dados.getReferenciaItemNF());
		builder.append(dados.getNumeroTerminalTelefonicoUnidadeConsumidora());
		builder.append(dados.getIndicacaoTipoInformacaoCampo1());
		builder.append(dados.getTipoCliente());
		builder.append(dados.getSubclasseConsumo());
		builder.append(dados.getNumeroTerminalTelefonicoPrincipal());
		builder.append(dados.getCnpjEmitente());
		builder.append(dados.getNumeroCodigoFaturaComercial());
		builder.append(dados.getValorTotalFaturaComercial());
		builder.append(dados.getDataLeituraAnterior());
		builder.append(dados.getDataLeituraAtual());
		builder.append(getEspacosBrancos(50)); //NOSONAR desnecessário criação de constante.
		builder.append(getZeros(8)); //NOSONAR desnecessário criação de constante.
		builder.append(dados.getInformacoesAdicionais());
		builder.append(getEspacosBrancos(5)); //NOSONAR desnecessário criação de constante.
		
		setLinha(builder.toString());
	}
	
	public static String getTipo() {
		return TIPO_LAYOUT;
	}
}
