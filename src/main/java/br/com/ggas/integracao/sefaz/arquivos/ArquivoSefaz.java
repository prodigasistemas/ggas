/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.
 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.
 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 This file is part of GGAS, a commercial management system for Gas Distribution Services
 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 @author ProdigaSistemas
*/

package br.com.ggas.integracao.sefaz.arquivos;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import br.com.ggas.util.StringUtil;
import br.com.ggas.util.Util;

/**
 * Classe ArquivoSefaz
 *
 *Essa classe e responsavel pela geracao do arquivo e dos codigos de autenticacao
 *dos arquivos da sefaz
 *
 * @author ProdigaSistemas
 * 
 * @param <T> the Layout
 */
public class ArquivoSefaz<T extends AbstractArquivoLayout<?>> {
	private static final Logger LOG = Logger.getLogger(ArquivoSefaz.class);

	private static final String ALGORITMO_HASH = "MD5";
	private static final String CHARSET_ISO = "ISO-8859-1";
	private static final String CHARSET_UTF = "UTF-8";

	private String anoMesReferencia;
	private String serie;
	private String tipo;

	private String nomeArquivo;

	public static final String DIRETORIO = "diretorio";
	public static final String CNPJ = "cnpjEmitente";
	public static final String UF = "uf";

	private static final String MODELO_NF = "01";
	private static final String STATUS_SNN = "N01";
	private static final String VOLUME_VVV = "001";

	private static final int INICIO_POSICAO_ANOMES_CURTO = 2;
	private static final int FIM_POSICAO_ANOMES_CURTO = 6;

	/**
	 * Método construtor
	 * 
	 * @param anoMesReferencia data de referencia
	 * @param serie            serie do arquivo
	 * @param tipo             tipo do arquivo
	 * 
	 */
	public ArquivoSefaz(String anoMesReferencia, String serie, String tipo) {
		this.anoMesReferencia = anoMesReferencia.substring(INICIO_POSICAO_ANOMES_CURTO, FIM_POSICAO_ANOMES_CURTO);
		this.serie = StringUtil.preencherEsquerda(serie, 3, "0");
		this.tipo = tipo;
	}

	/**
	 * Método para gerar nome de arquivo
	 * 
	 * @param cnpjEmitente cnpj do emitente
	 * @param uf           unidade federativa
	 */
	public void gerarNomeArquivo(String cnpjEmitente, String uf) {
		this.nomeArquivo = new String(new StringBuilder(uf).append(cnpjEmitente).append(MODELO_NF).append(serie)
				.append(anoMesReferencia).append(STATUS_SNN).append(tipo).append(".").append(VOLUME_VVV));
	}

	/**
	 * Método que gera o arquivo sefaz
	 * 
	 * @param arquivoParams arquivo
	 * @param linhas        linhas do arquivo
	 * @return boolean verdadeiro se o arquivo foi gerado
	 */
	public boolean gerarArquivo(Map<String, String> arquivoParams, Collection<T> linhas) {
		//preparar nome de arquivo
		gerarNomeArquivo(arquivoParams.get(CNPJ), arquivoParams.get(UF));
		
		//tentar buscar arquivo caso exista em DIRETORIO
		try (PrintWriter gravarArquivo = new PrintWriter(getOutputStream(arquivoParams.get(DIRETORIO)))) {
			//preenchimento de linhas
			for (T layout : linhas) {
				byte[] linha = layout.getLinha().getBytes(Charset.forName(CHARSET_UTF));
				byte[] codigoAutenticacao = getMD5Hash(layout.getLinha()).getBytes(CHARSET_UTF);

				byte[] linhaMontada = ArrayUtils.addAll(linha, codigoAutenticacao);

				linhaMontada = ArrayUtils.addAll(linhaMontada, "\r\n".getBytes(CHARSET_UTF));

				gravarArquivo.print(new String(linhaMontada, Charset.forName(CHARSET_UTF)));
			}
			//grava linhas no arquivo
			gravarArquivo.flush();
			//retorna sucesso ao gravar arquivo
			return true;
		
		//gerar log de erro caso arquivo não seja gerado
		} catch (IOException e) {
			//gravar log erro
			LOG.error("Erro ao gerar arquivo", e);
			//retorna insucesso ao tentar gravar arquivo
			return false;
		}
	}

	/**
	 * metodo que gera o hash MD5 para o conteudo da linha
	 * 
	 * @param text com o conteudo para ser criptografado
	 * @return String hash md5
	 */
	public static String getMD5Hash(String text) {
		//verificação de MD5
		try {
			//retorna hash 
			return getMD5Hash(text.getBytes(CHARSET_ISO));

		} catch (NullPointerException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			//gravar log de erro
			LOG.error("erro na generacao do hash MD5", e);
			//retorna string padrão
			return "";
		}
	}

	private static String getMD5Hash(byte[] text) throws NoSuchAlgorithmException {
		MessageDigest md;
		md = MessageDigest.getInstance(ALGORITMO_HASH);
		md.update(text);

		return String.format("%032x", new BigInteger(1, md.digest()));
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public String getSerie() {
		return serie;
	}
	
	private Writer getOutputStream(String caminho) {
		FileOutputStream output = null;
		
		try {
			output = new FileOutputStream(Util.getFileChild(caminho, this.nomeArquivo));
		} catch (FileNotFoundException e) {
			LOG.error("Erro ao encontrar arquivo", e);
			return null;
		}

		return new OutputStreamWriter(output, Charset.forName(CHARSET_ISO));
	}
}
