/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/02/2014 10:26:08
 @author ifrancisco
 */

package br.com.ggas.atendimento.resposta.dominio;

import java.util.Map;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 * Classe responsável por representar a resposta do questionário
 * 
 */
public class QuestionarioPerguntaResposta extends EntidadeNegocioImpl {

	private static final long serialVersionUID = 5321106955789500956L;

	private QuestionarioPergunta pergunta;

	private Integer notaResposta;

	private String descricaoResposta;

	private ServicoAutorizacao servicoAutorizacao;

	private ServicoAutorizacaoHistorico servicoAutorizacaoHistorico;

	private QuestionarioPergunta questionarioPergunta;

	private Chamado chamado;

	/**
	 * @return pergunta
	 */
	public QuestionarioPergunta getPergunta() {

		return pergunta;
	}

	/**
	 * @param pergunta
	 */
	public void setPergunta(QuestionarioPergunta pergunta) {

		this.pergunta = pergunta;
	}

	/**
	 * @return descricaoResposta
	 */
	public String getDescricaoResposta() {

		return descricaoResposta;
	}

	/**
	 * @param descricaoResposta
	 */
	public void setDescricaoResposta(String descricaoResposta) {

		this.descricaoResposta = descricaoResposta;
	}

	/**
	 * @return servicoAutorizacao
	 */
	public ServicoAutorizacao getServicoAutorizacao() {

		return servicoAutorizacao;
	}

	/**
	 * @param servicoAutorizacao
	 */
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {

		this.servicoAutorizacao = servicoAutorizacao;
	}

	/**
	 * @return notaResposta
	 */
	public Integer getNotaResposta() {

		return notaResposta;
	}

	/**
	 * @param notaResposta
	 */
	public void setNotaResposta(Integer notaResposta) {

		this.notaResposta = notaResposta;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;

	}

	/**
	 * @return chamado
	 */
	public Chamado getChamado() {
		return chamado;
	}

	/**
	 * @param chamado
	 */
	public void setChamado(Chamado chamado) {
		this.chamado = chamado;
	}

	/**
	 * Retorna servicoAutorizacaoHistorico
	 * 
	 * @return servicoAutorizacaoHistorico
	 */
	public ServicoAutorizacaoHistorico getServicoAutorizacaoHistorico() {
		return servicoAutorizacaoHistorico;
	}

	/**
	 * Atribui servicoAutorizacaoHistorico
	 * 
	 * @param servicoAutorizacaoHistorico
	 */
	public void setServicoAutorizacaoHistorico(ServicoAutorizacaoHistorico servicoAutorizacaoHistorico) {
		this.servicoAutorizacaoHistorico = servicoAutorizacaoHistorico;
	}

	/**
	 * @return questionarioPergunta
	 */
	public QuestionarioPergunta getQuestionarioPergunta() {
		return questionarioPergunta;
	}

	/**
	 * @param questionarioPergunta
	 */
	public void setQuestionarioPergunta(QuestionarioPergunta questionarioPergunta) {
		this.questionarioPergunta = questionarioPergunta;
	}
}
