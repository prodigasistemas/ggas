/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/02/2014 10:52:26
 @author ifrancisco
 */

package br.com.ggas.atendimento.resposta.negocio;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * Interface ControladorResposta
 */
public interface ControladorResposta {


	/** incluirResposta
	 * @param chamado {@link Chamado}
	 * @param dados {@link Map}
	 * @param listaPerguntas {@link List}
	 * @throws ParseException {@link ParseException}
	 * @throws GGASException {@link GGASException}
	 */
	void incluirResposta(Chamado chamado, Map<String, Object> dados, List<QuestionarioPergunta> listaPerguntas)
			throws GGASException;

	/**
	 * Existe resposta.
	 * 
	 * @param chavePrimariaPergunta
	 *            the chave primaria pergunta
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean existeResposta(Long chavePrimariaPergunta) throws NegocioException;

	/**
	 * Questionario respondido.
	 * 
	 * @param chavePrimariaQuestionario {@link Long}
	 *            the chave primaria questionario
	 * @return true, if successful
	 */
	boolean questionarioRespondido(Long chavePrimariaQuestionario);

	/**
	 * Verifica se o questionário foi respondido
	 * 
	 * @param chavePrimariaQuestionario {@link Long}
	 * @param chamado {@link Chamado}
	 * @return true se já estiver sido respondido ou false
	 */
	boolean questionarioRespondido(Long chavePrimariaQuestionario, Chamado chamado);

	/**
	 * Inclui Resposta
	 * 
	 * @param servicoAutorizacaoHistorico {@link ServicoAutorizacaoHistorico}
	 * @param dados {@link Map}
	 * @param listaPerguntas {@link List}
	 * @throws GGASException {@link GGASException}
	 */
	void incluirResposta(ServicoAutorizacaoHistorico servicoAutorizacaoHistorico, Map<String, Object> dados,
			List<QuestionarioPergunta> listaPerguntas) throws GGASException;

	/**
	 * Obter respostas serviços autorização historico
	 * 
	 * @param chavePrimaria {@link Long}
	 * @return listaRespostas {@link Collection}
	 */
	Collection<QuestionarioPerguntaResposta> obterRespostasServicoAutorizacaoHistorico(long chavePrimaria);
}
