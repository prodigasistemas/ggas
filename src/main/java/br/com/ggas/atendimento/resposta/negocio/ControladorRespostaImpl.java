/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/02/2014 10:53:29
 @author ifrancisco
 */

package br.com.ggas.atendimento.resposta.negocio;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.resposta.repositorio.RepositorioResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Classe ControladorRespostaImpl
 */
@Service("controladorResposta")
@Transactional
public class ControladorRespostaImpl implements ControladorResposta {

	private static final int ULTIMA_VIRGULA = 2;
	@Autowired
	private RepositorioResposta repositorioResposta;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.resposta.negocio.ControladorResposta#incluirResposta(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * java.util.Map, java.util.List)
	 */
	@Transactional
	@Override
	public void incluirResposta(Chamado chamado, Map<String, Object> dados, List<QuestionarioPergunta> listaPerguntas)
			throws GGASException {

		for (QuestionarioPergunta pergunta : listaPerguntas) {
			QuestionarioPerguntaResposta resposta = popularQuestionarioPerguntaResposta(pergunta);
			resposta.setChamado(chamado);
			repositorioResposta.inserir(resposta);

		}
	}

	/**
	 * @param pergunta
	 * @return QuestionarioPerguntaResposta
	 */
	private QuestionarioPerguntaResposta popularQuestionarioPerguntaResposta(QuestionarioPergunta pergunta) {
		QuestionarioPerguntaResposta resposta = new QuestionarioPerguntaResposta();
		if (pergunta.getObjetiva()) {
			if (pergunta.getValoresPersonalizados()) {
				resposta.setDescricaoResposta(respostaAlternativa(pergunta));
			} else {
				resposta.setNotaResposta(pergunta.getNotaPergunta());
			}
		} else {
			resposta.setDescricaoResposta(pergunta.getDescricaoResposta());
		}
		resposta.setDadosAuditoria(pergunta.getDadosAuditoria());
		resposta.setHabilitado(true);
		resposta.setPergunta(pergunta);
		return resposta;
	}

	/**
	 * Retorna o valor de todas alternativas respondidas
	 * 
	 * @param pergunta
	 */
	private String respostaAlternativa(QuestionarioPergunta pergunta) {

		StringBuilder stringBuilder = new StringBuilder();

		if (pergunta.getListaAlternativasSelecionadas() != null && !pergunta.getListaAlternativasSelecionadas().isEmpty()) {

			for (QuestionarioAlternativa alternativa : pergunta.getListaAlternativasSelecionadas()) {
				stringBuilder.append(alternativa.getNomeAlternativa());
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			stringBuilder.setLength(stringBuilder.length() - ULTIMA_VIRGULA);
		}

		return stringBuilder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.resposta.negocio.ControladorResposta#incluirResposta(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * java.util.Map, java.util.List)
	 */
	@Transactional
	@Override
	public void incluirResposta(ServicoAutorizacaoHistorico servicoAutorizacaoHistorico, Map<String, Object> dados,
			List<QuestionarioPergunta> listaPerguntas) throws GGASException {

		for (QuestionarioPergunta pergunta : listaPerguntas) {
			QuestionarioPerguntaResposta resposta = popularQuestionarioPerguntaResposta(pergunta);
			resposta.setServicoAutorizacaoHistorico(servicoAutorizacaoHistorico);
			repositorioResposta.inserir(resposta);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.resposta.negocio.ControladorResposta#existeResposta(java.lang.Long)
	 */
	@Override
	public boolean existeResposta(Long chavePrimariaPergunta) throws NegocioException {

		Collection<QuestionarioPerguntaResposta> lista = repositorioResposta.consultarResposta(chavePrimariaPergunta);

		return lista != null && !lista.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.resposta.negocio.ControladorResposta#questionarioRespondido(java.lang.Long)
	 */
	@Override
	public boolean questionarioRespondido(Long chavePrimariaQuestionario) {

		Collection<QuestionarioPerguntaResposta> lista = repositorioResposta.consultarQuestionarioRespondido(chavePrimariaQuestionario);

		return lista != null && !lista.isEmpty();
	}

	/**
	 * Verifica se o questionário foi respondido
	 * 
	 * @param chavePrimariaQuestionario
	 * @param chamado
	 * @return true se já estiver sido respondido ou false
	 */
	@Override
	public boolean questionarioRespondido(Long chavePrimariaQuestionario, Chamado chamado) {

		Collection<QuestionarioPerguntaResposta> lista =
				repositorioResposta.consultarQuestionarioRespondido(chavePrimariaQuestionario, chamado.getChavePrimaria());
		return (lista != null && !lista.isEmpty());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.resposta.negocio.ControladorResposta#obterRespostasServicoAutorizacaoHistorico(long)
	 */
	@Override
	public Collection<QuestionarioPerguntaResposta> obterRespostasServicoAutorizacaoHistorico(long chavePrimaria) {
		return repositorioResposta.obterRespostasServicoAutorizacaoHistorico(chavePrimaria);
	}
}
