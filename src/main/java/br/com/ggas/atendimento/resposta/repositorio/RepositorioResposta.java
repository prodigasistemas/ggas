/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/02/2014 10:54:23
 @author ifrancisco
 */

package br.com.ggas.atendimento.resposta.repositorio;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe RepositorioResposta
 */
@Repository
public class RepositorioResposta extends RepositorioGenerico {

	private static final String PERGUNTA = "pergunta";

	/**
	 * Instantiates a new repositorio resposta.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioResposta(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new QuestionarioPerguntaResposta();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return QuestionarioPerguntaResposta.class;
	}

	/**
	 * Consultar resposta.
	 * 
	 * @param chavePrimariaPergunta
	 *            the chave primaria pergunta
	 * @return the list
	 */
	public List<QuestionarioPerguntaResposta> consultarResposta(Long chavePrimariaPergunta) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(PERGUNTA, PERGUNTA);
		criteria.add(Restrictions.eq("pergunta.chavePrimaria", chavePrimariaPergunta));
		return criteria.list();

	}

	/**
	 * Consultar questionario respondido.
	 * 
	 * @param chavePrimariaQuestionario
	 *            the chave primaria questionario
	 * @return the list
	 */
	public List<QuestionarioPerguntaResposta> consultarQuestionarioRespondido(Long chavePrimariaQuestionario) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(PERGUNTA, PERGUNTA);
		criteria.createAlias("pergunta.questionario", "questionario");
		criteria.add(Restrictions.eq("questionario.chavePrimaria", chavePrimariaQuestionario));
		return criteria.list();

	}

	/**
	 * Consultar questionario respondido.
	 * 
	 * @param chavePrimariaQuestionario the chave primaria questionario
	 * @param chaveChamado the chave primaria chamado
	 * @return the list
	 */
	public List<QuestionarioPerguntaResposta> consultarQuestionarioRespondido(Long chavePrimariaQuestionario, Long chaveChamado) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(PERGUNTA, PERGUNTA);
		criteria.createAlias("pergunta.questionario", "questionario");
		criteria.add(Restrictions.eq("questionario.chavePrimaria", chavePrimariaQuestionario));
		criteria.add(Restrictions.eq("chamado.chavePrimaria", chaveChamado));
		return criteria.list();

	}

	/**
	 * Obter respostas serviços autorização historico
	 * 
	 * @param chavePrimaria {@link Long}
	 * @return listaResposta {@link Collection}
	 */
	public Collection<QuestionarioPerguntaResposta> obterRespostasServicoAutorizacaoHistorico(long chavePrimaria) {
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.add(Restrictions.eq("servicoAutorizacaoHistorico.chavePrimaria", chavePrimaria));
		return criteria.list();
	}
}
