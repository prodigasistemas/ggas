/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 20/09/2013 09:21:26
 @author vtavares
 */

package br.com.ggas.atendimento.servicotipoagendamentoturno.dominio;

import java.util.Map;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável por representar a relação entre o tipo de serviço e o turno do agendamento do serviço
 */
public class ServicoTipoAgendamentoTurno extends EntidadeNegocioImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ServicoTipo servicoTipo;

	private EntidadeConteudo turno;

	private long quantidadeAgendamentosTurno;
	
	private Equipe equipe;

	/**
	 * Instantiates a new servico tipo agendamento turno.
	 */
	public ServicoTipoAgendamentoTurno() {

	}

	/**
	 * Instantiates a new servico tipo agendamento turno.
	 * 
	 * @param chaveTurno
	 *            the chave turno
	 * @param quantidade
	 *            the quantidade
	 */
	public ServicoTipoAgendamentoTurno(Long chaveTurno, Long quantidade) {

		EntidadeConteudo turnoLocal = (EntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
		turnoLocal.setChavePrimaria(chaveTurno);
		this.turno = turnoLocal;
		this.quantidadeAgendamentosTurno = quantidade;
	}

	public ServicoTipo getServicoTipo() {

		return servicoTipo;
	}

	public void setServicoTipo(ServicoTipo servicoTipo) {

		this.servicoTipo = servicoTipo;
	}

	public EntidadeConteudo getTurno() {

		return turno;
	}

	public void setTurno(EntidadeConteudo turno) {

		this.turno = turno;
	}

	public long getQuantidadeAgendamentosTurno() {

		return quantidadeAgendamentosTurno;
	}

	public void setQuantidadeAgendamentosTurno(long quantidadeAgendamentosTurno) {

		this.quantidadeAgendamentosTurno = quantidadeAgendamentosTurno;
	}

	/**
	 * @return the equipe
	 */
	public Equipe getEquipe() {
		return equipe;
	}

	/**
	 * @param equipe the equipe to set
	 */
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
