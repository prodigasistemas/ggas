
package br.com.ggas.atendimento.servicotipoagendamentoturno.repositorio;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Repositório responsável pelo turno e tipo de agendamento de serviço
 */
@Repository
public class RepositorioServicoTipoAgendamentoTurno extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio servico tipo agendamento turno.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioServicoTipoAgendamentoTurno(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ServicoTipoAgendamentoTurno();
	}

	@Override
	public Class<ServicoTipoAgendamentoTurno> getClasseEntidade() {

		return ServicoTipoAgendamentoTurno.class;
	}
}
