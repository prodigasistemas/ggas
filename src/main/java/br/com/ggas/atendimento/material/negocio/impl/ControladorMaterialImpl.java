/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2013 15:16:26
 @author vtavares
 */

package br.com.ggas.atendimento.material.negocio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.atendimento.material.negocio.ControladorMaterial;
import br.com.ggas.atendimento.material.repositorio.RepositorioMaterial;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.util.Constantes;

/**
 * Classe ControladorMaterialImpl
 * 
 * @author Procenge
 */
@Service
@Transactional
public class ControladorMaterialImpl implements ControladorMaterial {

	@Autowired
	private RepositorioMaterial repositorioMaterial;

	@Autowired
	@Qualifier("controladorUnidade")
	private ControladorUnidade controladorUnidade;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#inserir(br.com.ggas.atendimento.material.dominio.Material)
	 */
	@Override
	public Material inserir(Material material) throws NegocioException {

		repositorioMaterial.inserir(material);

		return material;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.material.negocio.ControladorMaterial#consultarMaterial(br.com.ggas.atendimento.material.dominio.Material,
	 * java.lang.Boolean)
	 */
	@Override
	@Transactional
	public Collection<Material> consultarMaterial(Material material, Boolean habilitado) throws NegocioException {

		return repositorioMaterial.consultarMaterial(material, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#obterMaterial(long)
	 */
	@Override
	@Transactional
	public Material obterMaterial(long chavePrimaria) throws NegocioException {

		return this.repositorioMaterial.obterMaterial(chavePrimaria);
	}

	@Transactional
	public ControladorUnidade getControladorUnidade() {

		return controladorUnidade;
	}

	@Transactional
	public void setControladorUnidade(ControladorUnidade controladorUnidade) {

		this.controladorUnidade = controladorUnidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#obterTodos()
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodos() throws NegocioException {

		return repositorioMaterial.obterTodas();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#removerMaterial(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerMaterial(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		for (Long chave : chavesPrimarias) {
			if(chave != null) {
				Material material = this.obterMaterial(chave);
				this.verificarMaterialRelacionado(material);
			}
		}
		this.repositorioMaterial.remover(chavesPrimarias, dadosAuditoria);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.material.negocio.ControladorMaterial#atualizarMaterial(br.com.ggas.atendimento.material.dominio.Material)
	 */
	@Override
	public void atualizarMaterial(Material material) throws NegocioException, ConcorrenciaException {

		this.repositorioMaterial.atualizar(material, Material.class);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.material.negocio.ControladorMaterial#verificarMaterialExistente(br.com.ggas.atendimento.material.dominio.
	 * Material)
	 */
	@Override
	public void verificarMaterialExistente(Material material) throws NegocioException {

		Material materialTemporario = (Material) repositorioMaterial.criar();
		materialTemporario.setChavePrimaria(material.getChavePrimaria());
		materialTemporario.setDescricao(material.getDescricao());
		material.isHabilitado();
		Long chavePrimaria = materialTemporario.getChavePrimaria();

		Material materialConsulta = repositorioMaterial.obterMaterialPorDescricao(material.getDescricao());

		if (materialConsulta != null && materialConsulta.getChavePrimaria() != chavePrimaria) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MATERIAL_EXISTENTE, materialTemporario.getDescricao());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#removerMaterial(br.com.ggas.atendimento.material.dominio.Material)
	 */
	@Override
	public void removerMaterial(Material material) throws NegocioException {

		repositorioMaterial.remover(material);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.material.negocio.ControladorMaterial#verificarMaterialRelacionado(br.com.ggas.atendimento.material.dominio.
	 * Material)
	 */
	@Override
	public void verificarMaterialRelacionado(Material material) throws NegocioException {

		Collection<ServicoTipoMaterial> materialConsulta = null;

		materialConsulta = repositorioMaterial.obterMaterialRelacionado(material.getChavePrimaria());

		if(materialConsulta != null && !materialConsulta.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, new String[] {});
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#verificarCodigoMaterialExistente(java.lang.String)
	 */
	@Override
	public void verificarCodigoMaterialExistente(String codigoMaterial) throws NegocioException {

		if(codigoMaterial != null && !codigoMaterial.isEmpty()) {
			Material material = new Material();
			material.setCodigoMaterial(codigoMaterial);

			Collection<Material> materialConsulta = repositorioMaterial.consultarMaterial(material, null);

			if(materialConsulta != null && !materialConsulta.isEmpty()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CODIGO_MATERIAL_EXISTENTE, material.getCodigoMaterial());
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.material.negocio.ControladorMaterial#listarMaterial()
	 */
	@Override
	public Collection<Material> listarMaterial() throws NegocioException {

		return repositorioMaterial.listarMaterial();
	}

}
