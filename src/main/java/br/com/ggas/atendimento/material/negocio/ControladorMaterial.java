/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 10/09/2013 15:16:11
 * @author vtavares
 */

package br.com.ggas.atendimento.material.negocio;

import java.util.Collection;

import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ControladorMaterial
 * 
 * @author Procenge
 */
public interface ControladorMaterial {

	/**
	 * Inserir.
	 * 
	 * @param material the material
	 * @return the material
	 * @throws NegocioException the negocio exception
	 */
	Material inserir(Material material) throws NegocioException;

	/**
	 * Consultar material.
	 * 
	 * @param material the material
	 * @param habilitado the habilitado
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Material> consultarMaterial(Material material, Boolean habilitado) throws NegocioException;

	/**
	 * Obter material.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the material
	 * @throws NegocioException the negocio exception
	 */
	Material obterMaterial(long chavePrimaria) throws NegocioException;

	/**
	 * Obter todos.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodos() throws NegocioException;

	/**
	 * Remover material.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	void removerMaterial(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar material.
	 * 
	 * @param material the material
	 * @throws NegocioException the negocio exception
	 * @throws GGASException the GGASException exception
	 */
	void atualizarMaterial(Material material) throws GGASException;

	/**
	 * Remover material.
	 * 
	 * @param material the material
	 * @throws NegocioException the negocio exception
	 */
	void removerMaterial(Material material) throws NegocioException;

	/**
	 * Verificar material existente.
	 * 
	 * @param material the material
	 * @throws NegocioException the negocio exception
	 */
	void verificarMaterialExistente(Material material) throws NegocioException;

	/**
	 * Verificar material relacionado.
	 * 
	 * @param material the material
	 * @throws NegocioException the negocio exception
	 */
	void verificarMaterialRelacionado(Material material) throws NegocioException;

	/**
	 * Verificar codigo material existente.
	 * 
	 * @param codigoMaterial the codigo material
	 * @throws NegocioException the negocio exception
	 */
	void verificarCodigoMaterialExistente(String codigoMaterial) throws NegocioException;

	/**
	 * Listar material.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Material> listarMaterial() throws NegocioException;

}
