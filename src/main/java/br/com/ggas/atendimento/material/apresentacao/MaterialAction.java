/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2013 15:35:42
 @author vtavares
 */

package br.com.ggas.atendimento.material.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.atendimento.material.negocio.ControladorMaterial;
import br.com.ggas.atendimento.material.negocio.impl.ControladorMaterialImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe MaterialAction
 * 
 * @author Procenge
 */
@Controller
public class MaterialAction extends GenericAction {

	private static final String MATERIAL_PARA_INCLUSAO = "material";

	private static final String LISTA_UNIDADE_MEDIDA = "listaUnidadeMedida";

	private static final String MATERIAL = "Material";

	@Autowired
	private ControladorMaterial controladorMaterial;

	@Autowired
	@Qualifier("controladorUnidade")
	private ControladorUnidade controladorUnidade;
	
	private static final Logger LOG = Logger.getLogger(MaterialAction.class);

	public ControladorMaterial getControladorMaterial() {

		return controladorMaterial;
	}

	public void setControladorMaterial(ControladorMaterialImpl controladorMaterial) {

		this.controladorMaterial = controladorMaterial;
	}

	/**
	 * Incluir material.
	 * 
	 * @param material
	 *            the material
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("incluirMaterial")
	public ModelAndView incluirMaterial(Material material, HttpServletRequest request) throws NegocioException {

		material.setHabilitado(true);
		material.setDescricao(Util.rtrim(Util.ltrim(material.getDescricao())));
		ModelAndView model = new ModelAndView("forward:/pesquisarMaterial");

		try {
			controladorMaterial.verificarCodigoMaterialExistente(material.getCodigoMaterial());
			controladorMaterial.verificarMaterialExistente(material);
			controladorMaterial.inserir(material);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, MATERIAL);
		} catch(GGASException e) {

			model = new ModelAndView("exibirInclusaoMaterial");
			model.addObject(LISTA_UNIDADE_MEDIDA, controladorUnidade.listaUnidadesOrdenadas());
			model.addObject(MATERIAL_PARA_INCLUSAO, material);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Popular material.
	 * 
	 * @param descricao
	 *            the descricao
	 * @param codigoMaterial
	 *            the codigo material
	 * @param unidade
	 *            the unidade
	 * @param habilitado
	 *            the habilitado
	 * @param material
	 *            the material
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularMaterial(String descricao, String codigoMaterial, String unidade, String habilitado, Material material)
					throws NegocioException {

		material.setDescricao(descricao);
		material.setCodigoMaterial(codigoMaterial);
		material.setHabilitado(Boolean.parseBoolean(habilitado));

		if(!"-1".equals(unidade)) {
			Unidade unidadeMedida = (Unidade) controladorUnidade.obter(Long.parseLong(unidade));
			material.setUnidadeMedida(unidadeMedida);
		} else {
			material.setUnidadeMedida(null);
		}

	}

	/**
	 * Alterar material.
	 * 
	 * @param chavesPrimaria
	 *            the chaves primaria
	 * @param descricao
	 *            the descricao
	 * @param codigoMaterial
	 *            the codigo material
	 * @param unidade
	 *            the unidade
	 * @param habilitado
	 *            the habilitado
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("alterarMaterial")
	public ModelAndView alterarMaterial(@RequestParam("chavesPrimaria") Long chavesPrimaria, @RequestParam("descricao") String descricao,
					@RequestParam("codigoMaterial") String codigoMaterial, @RequestParam("unidadeMedida") String unidade,
					@RequestParam(EntidadeNegocio.ATRIBUTO_HABILITADO) String habilitado, HttpServletRequest request) throws NegocioException {

		Long chavePrimaria = chavesPrimaria;
		Material materialAtualizado = controladorMaterial.obterMaterial(chavePrimaria);

		ModelAndView model = new ModelAndView("forward:/pesquisarMaterial");

		try {

			this.popularMaterial(descricao, codigoMaterial, unidade, habilitado, materialAtualizado);

			controladorMaterial.verificarCodigoMaterialExistente(materialAtualizado.getCodigoMaterial());
			controladorMaterial.verificarMaterialExistente(materialAtualizado);
			controladorMaterial.atualizarMaterial(materialAtualizado);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, MATERIAL);
		} catch(GGASException e) {
			model = new ModelAndView("forward:/exibirAlteracaoMaterial");
			model.addObject(LISTA_UNIDADE_MEDIDA, controladorUnidade.listaUnidadesOrdenadas());
			request.setAttribute("materialModificado", materialAtualizado);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Remover material.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerMaterial")
	public ModelAndView removerMaterial(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView("forward:/exibirPesquisaMaterial");
		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorMaterial.removerMaterial(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, MATERIAL);
		} catch(NegocioException e) {
			LOG.error(e.getMessage(), e);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_MATERIAL_RELACIONADO, true);
			} catch(NegocioException ex) {
				mensagemErroParametrizado(model, ex);
			}
		}
		return model;
	}

	/**
	 * Pesquisar material.
	 * 
	 * @param descricao
	 *            the descricao
	 * @param habilitado
	 *            the habilitado
	 * @param unidade
	 *            the unidade
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarMaterial")
	public ModelAndView pesquisarMaterial(@RequestParam(value = "descricao", required = false) String descricao,
					@RequestParam(value = EntidadeNegocio.ATRIBUTO_HABILITADO, required = false) String habilitado,
					@RequestParam(value = "unidadeMedida", required = false) String unidade) throws GGASException {

		Material material = new Material();
		if(descricao != null) {
			material.setDescricao(descricao.trim());
		}
		material.setHabilitado(Boolean.parseBoolean(habilitado));

		if (!StringUtils.isEmpty(unidade) && !"-1".equals(unidade)) {
			Unidade unidadeMedida = (Unidade) controladorUnidade.obter(Long.parseLong(unidade));
			material.setUnidadeMedida(unidadeMedida);
		}
		Boolean materialHabilitado = null;
		if(habilitado != null && !habilitado.isEmpty()) {
			materialHabilitado = Boolean.parseBoolean(habilitado);
		}

		Collection<Material> listaMaterial = controladorMaterial.consultarMaterial(material, materialHabilitado);

		ModelAndView model = new ModelAndView("exibirPesquisaMaterial");
		model.addObject("listaMaterial", listaMaterial);
		model.addObject(LISTA_UNIDADE_MEDIDA, controladorUnidade.listaUnidadesOrdenadas());
		model.addObject(MATERIAL_PARA_INCLUSAO, material);
		model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		if(habilitado == null) {
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");
		} else {
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		}

		return model;
	}

	/**
	 * Exibir pesquisa material.
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirPesquisaMaterial")
	public ModelAndView exibirPesquisaMaterial() throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaMaterial");
		model.addObject(LISTA_UNIDADE_MEDIDA, controladorUnidade.listaUnidadesOrdenadas());
		model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");

		return model;
	}

	/**
	 * Exibir detalhamento material.
	 * 
	 * @param chavesPrimaria
	 *            the chaves primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoMaterial")
	public ModelAndView exibirDetalhamentoMaterial(@RequestParam("chavesPrimaria") Long chavesPrimaria) throws GGASException {

		Long chavePrimaria = chavesPrimaria;

		ModelAndView model = new ModelAndView("exibirDetalhamentoMaterial");
		model.addObject(MATERIAL_PARA_INCLUSAO, controladorMaterial.obterMaterial(chavePrimaria));

		return model;
	}

	/**
	 * Exibir alteracao material.
	 * 
	 * @param chavesPrimaria
	 *            the chaves primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoMaterial")
	public ModelAndView exibirAlteracaoMaterial(@RequestParam("chavesPrimaria") Long chavesPrimaria, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoMaterial");

		Material materialModificado = (Material) request.getAttribute("materialModificado");
		model.addObject(LISTA_UNIDADE_MEDIDA, controladorUnidade.listaUnidadesOrdenadas());

		if(materialModificado == null) {
			Material material = controladorMaterial.obterMaterial(chavesPrimaria);
			material.setDescricao(material.getDescricao().trim());
			model.addObject(MATERIAL_PARA_INCLUSAO, material);
		} else {
			materialModificado.setDescricao(materialModificado.getDescricao().trim());
			model.addObject(MATERIAL_PARA_INCLUSAO, materialModificado);
		}

		return model;
	}

	/**
	 * Exibir inclusao material.
	 * 
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirInclusaoMaterial")
	public ModelAndView exibirInclusaoMaterial(HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirInclusaoMaterial");
		model.addObject(LISTA_UNIDADE_MEDIDA, controladorUnidade.listaUnidadesOrdenadas());

		return model;
	}

}

