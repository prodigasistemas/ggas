/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 10/09/2013 15:18:17
 * @author vtavares
 */

package br.com.ggas.atendimento.material.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Util;

/**
 * Classe RepositorioMaterial
 * 
 * @author Procenge
 */
@Repository
public class RepositorioMaterial extends RepositorioGenerico {

	private static final String ATRIBUTO_UNIDADE_MEDIDA = "unidadeMedida";
	
	private static final String FROM =" from ";
	private static final String WHERE =" where ";

	/**
	 * Instantiates a new repositorio material.
	 * 
	 * @param sessionFactory the session factory
	 * 
	 */
	@Autowired
	public RepositorioMaterial(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Material();
	}

	@Override
	public Class<Material> getClasseEntidade() {

		return Material.class;
	}

	/**
	 * Obter material relacionado.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * 
	 */
	public Collection<ServicoTipoMaterial> obterMaterialRelacionado(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(ServicoTipoMaterial.class.getSimpleName());
		hql.append(" servicoTipoMaterial ");
		hql.append(" inner join fetch servicoTipoMaterial.material material");
		hql.append(WHERE);
		hql.append(" material.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return query.list();

	}

	/**
	 * Obter material por descricao.
	 * 
	 * @param descricao the descricao
	 * @return the material
	 * 
	 */
	public Material obterMaterialPorDescricao(String descricao) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" material ");
		hql.append(WHERE);
		hql.append(" material.descricao = :descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao);

		return (Material) query.uniqueResult();
	}

	/**
	 * Obter material.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the material
	 * @throws NegocioException the negocio exception
	 * 
	 */
	public Material obterMaterial(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" material ");
		hql.append(" inner join fetch material.unidadeMedida unidade ");
		hql.append(WHERE);
		hql.append(" material.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return (Material) query.uniqueResult();

	}

	/**
	 * Consultar material.
	 * 
	 * @param material the material
	 * @param habilitado the habilitado
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * 
	 */
	public Collection<Material> consultarMaterial(Material material, Boolean habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(ATRIBUTO_UNIDADE_MEDIDA, FetchMode.JOIN);

		if (material.getDescricao() != null && !material.getDescricao().isEmpty()) {
			criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(material.getDescricao())));
		}
		if (material.getCodigoMaterial() != null && !material.getCodigoMaterial().isEmpty()) {
			criteria.add(Restrictions.eq("codigoMaterial", material.getCodigoMaterial()));
		}
		if (material.getUnidadeMedida() != null) {
			criteria.createAlias(ATRIBUTO_UNIDADE_MEDIDA, ATRIBUTO_UNIDADE_MEDIDA);
			criteria.add(Restrictions.eq("unidadeMedida.chavePrimaria", material.getUnidadeMedida().getChavePrimaria()));
		}
		if (habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		return criteria.list();
	}

	/**
	 * Listar material.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Collection<Material> listarMaterial() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

}
