
package br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.ControladorChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.repositorio.RepositorioChamadoAssuntoServicoTipo;
import br.com.ggas.geral.exception.NegocioException;
/**
 * Classe responsável pelas ações relacionadas ao tipo de serviço do assunto de um chamado
 */
@Service
@Transactional
public class ControladorChamadoAssuntoServicoTipoImpl implements ControladorChamadoAssuntoServicoTipo {

	@Autowired
	private RepositorioChamadoAssuntoServicoTipo repositorio;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.ControladorChamadoAssuntoServicoTipo#listarTodos(br.com.ggas.atendimento
	 * .chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo, java.lang.String)
	 */
	@Override
	public Collection<ChamadoAssuntoServicoTipo> listarTodos(ChamadoAssuntoServicoTipo chamado, String indicador) throws NegocioException {

		return repositorio.listarTodos(chamado, indicador);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.ControladorChamadoAssuntoServicoTipo#inserirChamadoAssuntoServicoTipo(br
	 * .com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo)
	 */
	@Override
	public Long inserirChamadoAssuntoServicoTipo(ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo) throws NegocioException {

		return repositorio.inserir(chamadoAssuntoServicoTipo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.ControladorChamadoAssuntoServicoTipo#consultarChamadoAssuntoServicoTipo
	 * (java.lang.Long)
	 */
	@Override
	public ChamadoAssuntoServicoTipo consultarChamadoAssuntoServicoTipo(Long chavePrimaria) throws NegocioException {

		return (ChamadoAssuntoServicoTipo) repositorio.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.ControladorChamadoAssuntoServicoTipo#ordernarLista(java.lang.Long)
	 */
	@Override
	public Collection<ChamadoAssuntoServicoTipo> ordernarLista(Long chavePrimaria) {

		return repositorio.ordernarLista(chavePrimaria);
	}

}
