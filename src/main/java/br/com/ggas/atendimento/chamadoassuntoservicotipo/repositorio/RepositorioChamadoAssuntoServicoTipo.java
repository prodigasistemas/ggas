
package br.com.ggas.atendimento.chamadoassuntoservicotipo.repositorio;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Repositório responsável pelo tipo de serviço do assunto de um chamado
 */
@Repository
public class RepositorioChamadoAssuntoServicoTipo extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio chamado assunto servico tipo.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioChamadoAssuntoServicoTipo(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ChamadoAssuntoServicoTipo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.RepositorioGenerico#getClasseEntidade()
	 */
	@Override
	public Class<ChamadoAssuntoServicoTipo> getClasseEntidade() {

		return ChamadoAssuntoServicoTipo.class;
	}

	/**
	 * Ordernar lista.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ChamadoAssuntoServicoTipo> ordernarLista(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamado ");
		hql.append(" where ");
		hql.append(" chamado.chamadoAssunto.chavePrimaria = :chavePrimaria ");
		hql.append(" order by numeroOrdem ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimaria);

		return query.list();
	}

	/**
	 * Listar todos.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param indicador
	 *            the indicador
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ChamadoAssuntoServicoTipo> listarTodos(ChamadoAssuntoServicoTipo chamado, String indicador) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamado ");
		hql.append(" order by numeroOrdem ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

}
