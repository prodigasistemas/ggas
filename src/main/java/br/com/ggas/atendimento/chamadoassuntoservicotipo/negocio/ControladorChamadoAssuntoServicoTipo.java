
package br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio;

import java.util.Collection;

import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.geral.exception.NegocioException;

/**
 * Interface responsável pelos métodos do controlador relacionado ao tipo de serviço do assunto de um chamado 
 */
public interface ControladorChamadoAssuntoServicoTipo {

	/**
	 * Listar todos.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param indicador
	 *            the indicador
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ChamadoAssuntoServicoTipo> listarTodos(ChamadoAssuntoServicoTipo chamado, String indicador) throws NegocioException;

	/**
	 * Inserir chamado assunto servico tipo.
	 * 
	 * @param chamadoAssuntoServicoTipo
	 *            the chamado assunto servico tipo
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long inserirChamadoAssuntoServicoTipo(ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo) throws NegocioException;

	/**
	 * Consultar chamado assunto servico tipo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the chamado assunto servico tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ChamadoAssuntoServicoTipo consultarChamadoAssuntoServicoTipo(Long chavePrimaria) throws NegocioException;

	/**
	 * Ordernar lista.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	public Collection<ChamadoAssuntoServicoTipo> ordernarLista(Long chavePrimaria);
}
