/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 16/10/2013 vpessoa 20:57:38
 */

package br.com.ggas.atendimento.chamado.negocio.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote.ResultadoEncerramentoEmLote;
import br.com.ggas.atendimento.chamado.dominio.AcaoChamado;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoAlteracaoVencimento;
import br.com.ggas.atendimento.chamado.dominio.ChamadoEmail;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistoricoAnexo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoTituloAnalise;
import br.com.ggas.atendimento.chamado.dominio.ChamadoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamado.repositorio.RepositorioChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendasVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.exception.AutorizacaoServicoRepetidaException;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.GGASRuntimeException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.relatorio.ChamadoResumoVO;
import br.com.ggas.util.ChamadoUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.fatura.TitulosGSAVO;

/**
 * ControladorChamadoImpl
 * classe utilizada para controlar os itens da tela de chamado.
 */
@Service("controladorChamado")
@Transactional(rollbackFor = {GGASException.class, NegocioException.class})
public class ControladorChamadoImpl implements ControladorChamado {

	private static final int INDICE_PROTOCOLO = 6;

	private static final int TAMANHO_MAXIMO_PROTOCOLO = 10;

	private static final int LIMITE_CAMPO = 2;

	private static final int HORAS_DIA = 24;

	private static final Logger LOG = Logger.getLogger(ControladorChamadoImpl.class);

	@Autowired
	private RepositorioChamado repositorioChamado;

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(value = "controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier(value = "controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresa;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	@Qualifier("controladorFeriado")
	private ControladorFeriado controladorFeriado;

	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	@Autowired
	private ControladorQuestionario controladorQuestionario;

	@Autowired
	private NotificadorEmailChamado notificadorEmailChamado;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#obterChamado(java.lang.Long)
	 */
	@Override
	@Transactional
	public Chamado obterChamado(Long chavePrimaria) throws NegocioException {

		return repositorioChamado.obterChamado(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#obterChamadoTipoPesquisaSatisfacao(java.lang.Long)
	 */
	@Override
	@Transactional
	public Chamado obterChamadoTipoPesquisaSatisfacao(Long chavePrimaria) throws NegocioException {

		Chamado chamado = repositorioChamado.obterChamadoTipoPesquisaSatisfacao(chavePrimaria);

		ConstanteSistema constante = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_PESQUISA);
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto
				.obter(Long.valueOf(constante.getValor()));
		if (chamado == null || chamado.getChamadoAssunto() == null
				|| !chamadoAssunto.equals(chamado.getChamadoAssunto())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_NAO_TIPO_PESQUISA, true);
		}
		return chamado;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#inserirChamado(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * java.lang.Boolean, java.lang.Long, java.util.Map, java.util.List)
	 */
	@Override
	public void inserirChamado(Chamado chamado, Boolean rascunho, Long numeroProtocolo, Map<String, Object> dados,
			List<ContatoCliente> listaChamadoContato) throws GGASException {
		incluirChamado(chamado, rascunho, numeroProtocolo, dados, listaChamadoContato);

		Collection<UnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora = controladorChamadoAssunto.
				obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(chamado.getChamadoAssunto().getChavePrimaria());

		notificadorEmailChamado.enviarEmailCasoNecessario(AcaoChamado.INSERIR, chamado);
		if(!listaUnidadeOrganizacionalVisualizadora.isEmpty()){
			notificadorEmailChamado.enviarEmailUnidadeOrganizacionalVisualizador(listaUnidadeOrganizacionalVisualizadora,
					chamado,AcaoChamado.INSERIR);
		}

	}

	/**
	 * Inclui chamado
	 *
	 * @param chamado
	 * @param rascunho
	 * @param numeroProtocolo
	 * @param dados
	 * @param listaChamadoContato
	 * @throws GGASException
	 */
	private void incluirChamado(Chamado chamado, Boolean rascunho, Long numeroProtocolo, Map<String, Object> dados,
			List<ContatoCliente> listaChamadoContato) throws GGASException {


		chamado.setQuantidadeReabertura(0);
		chamado.setQuantidadeReativacao(0);
		chamado.setQuantidadeReiteracao(0);
		chamado.setQuantidadeTentativaContato(0);
		chamado.setIndicadorAgenciaReguladora(Boolean.FALSE);
		ConstanteSistema constante = null;

		ChamadoProtocolo protocolo = null;
		if (listaChamadoContato != null && !listaChamadoContato.isEmpty()) {
			
			if(chamado.getCliente() != null && chamado.getCliente().getContatos() != null){
				chamado.getCliente().getContatos().clear();
				
				for (Iterator<ContatoCliente> iterator = listaChamadoContato.iterator(); iterator.hasNext();) {
					chamado.getCliente().getContatos().add(iterator.next());
					chamado.getCliente().setUltimaAlteracao(Calendar.getInstance().getTime());
			}
		}
			Cliente cliente = chamado.getCliente();

			controladorCliente.atualizar(cliente);
		}

		Long numProtocolo = null;
		if (chamado.getProtocolo() == null) {
			protocolo = new ChamadoProtocolo();
			chamado.setProtocolo(protocolo);
			numProtocolo = numeroProtocolo;
		} else {
			numProtocolo = chamado.getProtocolo().getNumeroProtocolo();
		}

		validarNumeroProtocolo(numProtocolo);

		if (numProtocolo != null && protocolo != null) {
			protocolo.setNumeroProtocolo(numProtocolo);
		}
		if (!rascunho) {
			repositorioChamado.validarDadosEntidade(chamado);
			if (chamado.getEmailSolicitante() != null && !chamado.getEmailSolicitante().isEmpty()) {
				Util.validarEmailComposto(chamado.getEmailSolicitante());
			}
		} else {
			validarCanalAtendimentoChamadoRascunho(chamado);
		}
		if (chamado.getProtocolo().getChavePrimaria() == 0) {
			protocolo = gerarProtocolo(numProtocolo);
			chamado.setProtocolo(protocolo);
		}
		chamado.getProtocolo().setHabilitado(Boolean.TRUE);

		configurarPrevisaoEncerramento(chamado);

		ChamadoAssunto chamadoAssunto = controladorChamadoAssunto
				.consultarChamadoAssunto(chamado.getChamadoAssunto().getChavePrimaria());
		ChamadoTipo chamadoTipo = controladorChamadoTipo
				.consultarChamadoTipo(chamadoAssunto.getChamadoTipo().getChavePrimaria());

		if (chamadoTipo.getSegmento() != null && chamado.getPontoConsumo() != null
				&& !chamadoTipo.getSegmento().equals(chamado.getPontoConsumo().getSegmento())) {

			throw new NegocioException(Constantes.ERRO_SEGMENTO_DIFERENTE_PONTO_CONSUMO_CHAMADO_TIPO, true);
		}
		
		validarMultiplosEnderecos(chamado);
		
		if (rascunho) {
			if (chamado.getEmailSolicitante() != null && !chamado.getEmailSolicitante().isEmpty()) {
				Util.validarEmail(chamado.getEmailSolicitante());
			}
			constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_RASCUNHO);
			EntidadeConteudo statusAberto = controladorEntidadeConteudo
					.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			chamado.setStatus(statusAberto);
			validarCanalAtendimentoChamadoRascunho(chamado);
			repositorioChamado.inserirSemValidacao(chamado);
		} else {

			if (Util.isTrue(chamado.getIndicadorPendencia())) {
				constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_PENDENTE);
			} else if (Util.isTrue(chamado.getChamadoAssunto().getIndicadorFechamentoAutomatico())) {
				constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_FINALIZADO);
			} else {
				constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
			}

			EntidadeConteudo statusAberto = controladorEntidadeConteudo
					.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			chamado.setStatus(statusAberto);
			repositorioChamado.inserir(chamado);
			gerarASAutomatica(chamado, dados);
		}
		incluirChamadoHistorico(chamado, Constantes.C_OPERACAO_CHAMADO_INCLUIDO, dados);
		
		
		if(dados.containsKey("listaTitulosAnalise")) {
			incluirChamadoTituloAnalise(chamado, dados, numeroProtocolo);
		}
	}

	@Override
	public void incluirChamadoTituloAnalise(Chamado chamado, Map<String, Object> dados, Long numeroProtocolo) throws NegocioException, ConcorrenciaException {
		Collection<TitulosGSAVO> listaTitulosAnalise = (Collection<TitulosGSAVO>) dados.get("listaTitulosAnalise");
		
		
		Usuario usuario = (Usuario) dados.get("usuario");
		for(TitulosGSAVO tituloAnalise : listaTitulosAnalise) {
			
			ChamadoTituloAnalise chamadoAnalise = new ChamadoTituloAnalise();
			chamadoAnalise.setChamado(chamado);
			chamadoAnalise.setUsuario(usuario);
			chamadoAnalise.setNumeroTitulo(tituloAnalise.getSequencialTitulo());
			chamadoAnalise.setEmAnalise(Boolean.TRUE);
			
			
			repositorioChamado.inserir(chamadoAnalise);
			repositorioChamado.incluirTituloAnaliseGSA(tituloAnalise.getSequencialTitulo(), usuario.getFuncionario().getNome(), "Protocolo Nº : " + chamado.getProtocolo().getNumeroProtocolo(), Boolean.FALSE);
			
		}
		
	}

	private void configurarPrevisaoEncerramento(Chamado chamado) throws NegocioException {
		if (chamado.getChamadoAssunto() != null) {

			if (chamado.getDataPrevisaoEncerramento() != null
					&& Util.isTrue(chamado.getChamadoAssunto().getIndicadorPrazoDiferenciado())) {

				Date dataHoraAgora = new Date();
				if (chamado.getDataPrevisaoEncerramento().compareTo(dataHoraAgora) < 0) {
					throw new NegocioException(Constantes.ERRO_PREVISAO_ENCERRAMENTO_CHAMADO_PASSADO);
				}
			} else {
				chamado.setDataPrevisaoEncerramento(this.obterDataPrevisao(chamado.getChamadoAssunto()));
			}
		}
	}
	
	private void validarMultiplosEnderecos(Chamado chamado) throws NegocioException {
		if(chamado.getImovel() != null && chamado.getEnderecoChamado() != null) {
			throw new NegocioException(Constantes.ERRO_MULTIPLOS_ENDERECOS);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
    @SuppressWarnings("squid:S1192")
	public List<Long> inserirChamadoEmLote(Chamado chamadoOriginal, List<Long> pontosConsumo, Map<String, Object> dados)
			throws GGASException {
		List<Chamado> chamados = new ArrayList<>();

		final List<ChamadoProtocolo> listaProtocolos = gerarListaProtolosEmLote(pontosConsumo);

		repositorioChamado.getHibernateTemplate().getSessionFactory().getCurrentSession().evict(chamadoOriginal);
		for (int i = 0; i < pontosConsumo.size(); i++) {
			Long chavePonto = pontosConsumo.get(i);
			Chamado novoChamado = ChamadoUtil.duplicarChamado(chamadoOriginal);
			ChamadoAssunto chamadoAssunto = controladorChamadoAssunto.consultarChamadoAssunto(novoChamado
					.getChamadoAssunto().getChavePrimaria());
			novoChamado.setChamadoAssunto(chamadoAssunto);
			ChamadoTipo chamadoTipo = controladorChamadoTipo.consultarChamadoTipo(chamadoAssunto.getChamadoTipo()
					.getChavePrimaria());
			novoChamado.setChamadoTipo(chamadoTipo);
			novoChamado.setImovel(chamadoOriginal.getImovel());

			ChamadoProtocolo protocolo = listaProtocolos.get(i);
			validarNumeroProtocolo(protocolo.getNumeroProtocolo());
			novoChamado.setProtocolo(protocolo);
			final Cliente cliente = this.controladorCliente.obterClientePorPontoConsumoDeContratoAtivo(chavePonto);
			novoChamado.setCliente(cliente);
			final PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(chavePonto);
			novoChamado.setPontoConsumo(pontoConsumo);
			novoChamado.setChamadoEmLote(true);

			repositorioChamado.validarDadosEntidade(novoChamado);

			protocolo.setHabilitado(Boolean.TRUE);

			configurarPrevisaoEncerramento(novoChamado);

			if (chamadoTipo.getSegmento() != null && novoChamado.getPontoConsumo() != null
					&& !chamadoTipo.getSegmento().equals(novoChamado.getPontoConsumo().getSegmento())) {

				throw new NegocioException(Constantes.ERRO_SEGMENTO_DIFERENTE_PONTO_CONSUMO_CHAMADO_TIPO, true);
			}

			ConstanteSistema constante = null;
			if (Util.isTrue(novoChamado.getChamadoAssunto().getIndicadorFechamentoAutomatico())) {

				constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_FINALIZADO);

			} else {
				constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
			}

			EntidadeConteudo statusAberto = controladorEntidadeConteudo
					.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			novoChamado.setStatus(statusAberto);
			chamados.add(novoChamado);


		}

		inserirChamadosEmLote(chamados, dados);
		this.notificadorEmailChamado.enviarEmailEmLoteCasoNecessario(AcaoChamado.INSERIR, chamadoOriginal, chamados);
		return listaProtocolos.stream().map(ChamadoProtocolo::getNumeroProtocolo).collect(Collectors.toList());

	}

	private void inserirChamadosEmLote(List<Chamado> chamados, Map<String, Object> dados) {
		chamados.forEach(c -> {
			try {
				repositorioChamado.inserir(c);
				gerarASAutomatica(c, dados);
				incluirChamadoHistorico(c, Constantes.C_OPERACAO_CHAMADO_INCLUIDO, dados);
			} catch(GGASException e) {
				LOG.error("Falha ao inserir chamado em lote", e);
				throw new GGASRuntimeException(e);

			}
		});
	}

	private List<ChamadoProtocolo> gerarListaProtolosEmLote(List<Long> pontosConsumo) {
		return pontosConsumo.stream().map(p -> {
			try {
				return gerarProtocolo(null);
			} catch (NegocioException e) {
				LOG.error("Falha ao gerar números de protocolo");
				throw new GGASRuntimeException(e);
			}
		}).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#gerarASAutomatica(
	 * br.com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */

	public void gerarASAutomatica(Chamado chamado, Map<String, Object> dados) throws GGASException {
		if (chamado.getChamadoAssunto().getIndicadorGeraServicoAutorizacao() && !chamado.isIndicadorPesquisa()) {
			Long[] chavePrimaria = new Long[] { chamado.getChavePrimaria() };
			try {
				this.gerarAutorizacaoServico(chavePrimaria, null, dados);
			} catch (AutorizacaoServicoRepetidaException ex) {
				notificadorEmailChamado.enviarEmailCasoNecessario(AcaoChamado.INSERIR, chamado);
				incluirChamadoHistorico(chamado, Constantes.C_OPERACAO_CHAMADO_INCLUIDO, dados);
				throw ex;
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * Valida se já existe chamado com numero de protocolo
	 *
	 * @param numeroProtocolo
	 * @throws NegocioException
	 */
	private void validarNumeroProtocolo(Long numeroProtocolo) throws NegocioException {
		Chamado chamado = repositorioChamado.obterChamadoPorProtocolo(numeroProtocolo);
		if (chamado != null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_PROTOCOLO_EXISTENTE, true);
		}
	}

	/**
	 * Método para obter uma data de previsão
	 *
	 * @param ChamadoAssunto chamadoAssunto
	 * @return the Date
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public Date obterDataPrevisao(ChamadoAssunto chamadoAssunto) throws NegocioException {

		Date dataPrevisao = null;
		ConstanteSistema util = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIA_UTIL);

		Calendar hoje = Calendar.getInstance();
		hoje.add(Calendar.HOUR_OF_DAY, chamadoAssunto.getQuantidadeHorasPrevistaAtendimento());
		Date data = hoje.getTime();

		double qtdHoras = chamadoAssunto.getQuantidadeHorasPrevistaAtendimento();
		Double qtdDias = qtdHoras / HORAS_DIA;
		Double modQtdHorasPorHorasDia = qtdHoras % Double.valueOf(HORAS_DIA);
		if (!(modQtdHorasPorHorasDia != null && modQtdHorasPorHorasDia == 0) && qtdHoras >= Double.valueOf(HORAS_DIA)) {
			qtdDias++;
			qtdDias = (double) qtdDias.intValue();
		}

		if (chamadoAssunto.getIndicadorDiaCorridoUtil().getChavePrimaria() == Long.parseLong(util.getValor())
				&& qtdHoras >= HORAS_DIA) {

			Empresa empresaPrincipal = controladorEmpresa.obterEmpresaPrincipal();

			Cliente municipioCliente = controladorCliente.obterCliente(empresaPrincipal.getCliente().getChavePrimaria(),
					"enderecos");

			Boolean diaUtil = null;

			Calendar diaHoje = Calendar.getInstance();
			Date dataHoje = diaHoje.getTime();

			int proximoDia = 1;

			int i = 0;
			do {
				diaUtil = controladorFeriado.isDiaUtil(dataHoje,
						municipioCliente.getEnderecoPrincipal().getMunicipio().getChavePrimaria(), Boolean.TRUE);
				if (!diaUtil) {
					Date proximoDiaUtil = controladorFeriado.obterProximoDiaUtil(dataHoje,
							municipioCliente.getEnderecoPrincipal().getMunicipio().getChavePrimaria(), Boolean.TRUE);
					dataHoje = proximoDiaUtil;
					qtdDias++;
				}

				if (!(qtdDias != null && qtdDias.compareTo(Double.valueOf(i)) == 0)) {
					diaHoje.add(Calendar.DAY_OF_WEEK, proximoDia);
					dataHoje = diaHoje.getTime();
				}

				i++;
			} while (i <= qtdDias);

			dataPrevisao = dataHoje;
		} else {
			dataPrevisao = data;
		}
		return dataPrevisao;
	}

	/**
	 * Inserir chamado pesquisa.
	 *
	 * @param chamado
	 *            the chamado
	 * @param numeroProtocolo
	 *            the numero protocolo
	 * @param dados
	 *            the dados
	 * @return the chamado
	 * @throws GGASException - {@link GGASException}
	 */
	@Transactional
	public Chamado inserirChamadoPesquisa(Chamado chamado, Long numeroProtocolo, Map<String, Object> dados)
			throws GGASException {

		chamado.setQuantidadeReabertura(0);
		chamado.setQuantidadeReativacao(0);
		chamado.setQuantidadeReiteracao(0);
		chamado.setQuantidadeTentativaContato(0);
		ConstanteSistema constante = null;
		ChamadoProtocolo protocolo = null;
		Long numProtocolo = null;

		if (chamado.getProtocolo() == null) {
			protocolo = new ChamadoProtocolo();
			chamado.setProtocolo(protocolo);
			numProtocolo = numeroProtocolo;
		} else {
			numProtocolo = chamado.getProtocolo().getNumeroProtocolo();
		}

		validarNumeroProtocolo(numProtocolo);

		repositorioChamado.validarDadosEntidade(chamado);
		if (chamado.getEmailSolicitante() != null && !chamado.getEmailSolicitante().isEmpty()) {
			Util.validarEmail(chamado.getEmailSolicitante());
		}
		if (chamado.getProtocolo().getChavePrimaria() == 0) {
			protocolo = gerarProtocolo(numProtocolo);
			chamado.setProtocolo(protocolo);
		}
		chamado.getProtocolo().setHabilitado(Boolean.TRUE);

		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		EntidadeConteudo statusAberto = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		chamado.setStatus(statusAberto);
		Long chavePrimaria = repositorioChamado.inserir(chamado);
		Chamado chamadoInserido = repositorioChamado.obterChamado(chavePrimaria);
		incluirChamadoHistorico(chamadoInserido, Constantes.C_OPERACAO_CHAMADO_INCLUIDO, dados);

		return chamadoInserido;

	}

	/**
	 * Validar canal atendimento chamado rascunho.
	 *
	 * @param chamado
	 *            the chamado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarCanalAtendimentoChamadoRascunho(Chamado chamado) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (chamado.getCanalAtendimento() == null) {
			stringBuilder.append(Constantes.CAMPO_CANAL_ATENDIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (chamado.getProtocolo() == null || chamado.getProtocolo().getNumeroProtocolo() == null
				|| chamado.getProtocolo().getNumeroProtocolo() == 0) {
			stringBuilder.append(Constantes.CAMPO_PROTOCOLO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
			throw new NegocioException(erros);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#consultarChamado(
	 * br.com.ggas.atendimento.chamado.dominio.ChamadoVO)
	 */
	@Override
	@Transactional
	public Collection<Chamado> consultarChamado(ChamadoVO chamadoVO, Boolean isRelatorio) throws GGASException {

		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");

		try {
			if (chamadoVO.getDataInicioResolucao() != null && !chamadoVO.getDataInicioResolucao().isEmpty()
					&& chamadoVO.getDataFimResolucao() != null && !chamadoVO.getDataFimResolucao().isEmpty()) {
				Date dataInicio = null;
				Date dataFim = null;
				dataInicio = formatoData.parse(chamadoVO.getDataInicioResolucao());
				dataFim = formatoData.parse(chamadoVO.getDataFimResolucao());
				if (dataInicio.after(dataFim)) {
					throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}
			}

			if (chamadoVO.getDataInicioEncerramento() != null && !chamadoVO.getDataInicioEncerramento().isEmpty()
					&& chamadoVO.getDataFimEncerramento() != null && !chamadoVO.getDataFimEncerramento().isEmpty()) {
				Date dataInicio = null;
				Date dataFim = null;
				dataInicio = formatoData.parse(chamadoVO.getDataInicioEncerramento());
				dataFim = formatoData.parse(chamadoVO.getDataFimEncerramento());
				if (dataInicio.after(dataFim)) {
					throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}
			}

			if (chamadoVO.getDataInicioAbertura() != null && !chamadoVO.getDataInicioAbertura().isEmpty()
					&& chamadoVO.getDataFimAbertura() != null && !chamadoVO.getDataFimAbertura().isEmpty()) {
				Date dataInicio = null;
				Date dataFim = null;
				dataInicio = formatoData.parse(chamadoVO.getDataInicioAbertura());
				dataFim = formatoData.parse(chamadoVO.getDataFimAbertura());
				if (dataInicio.after(dataFim)) {
					throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}
			}

			String cepImovel = chamadoVO.getCepImovel();
			String numeroImovel = chamadoVO.getNumeroImovel();
			String descricaoComplemento = chamadoVO.getDescricaoComplemento();
			String nomeImovel = chamadoVO.getNomeImovel();
			Long matriculaImovel = chamadoVO.getMatriculaImovel();
			Boolean condominioImovel = chamadoVO.getCondominioImovel();

			if ((cepImovel != null && !cepImovel.isEmpty()) && (numeroImovel != null && !numeroImovel.isEmpty())
					&& (descricaoComplemento != null && !descricaoComplemento.isEmpty()) && (nomeImovel != null && !nomeImovel.isEmpty())
					&& matriculaImovel != null && condominioImovel != null) {
				verificarImovelPai(chamadoVO);
			}

			if (chamadoVO.getIndicadorAcionamentoGasista() == null) {
				chamadoVO.setIndicadorAcionamentoGasista(false);
			}
			if (chamadoVO.getIndicadorAcionamentoPlantonista() == null) {
				chamadoVO.setIndicadorAcionamentoPlantonista(false);
			}

			incluirIdsUnidadesInferiores(chamadoVO);

		} catch (ParseException e) {
			LOG.error(e);
			throw new GGASException(e);
		}
		return repositorioChamado.consultarChamado(chamadoVO, isRelatorio);

	}

	private void incluirIdsUnidadesInferiores(ChamadoVO chamadoVO) {
		if (Objects.nonNull(chamadoVO.getIdUnidadeOrganizacionalUsuario())) {
			final List<Long> unidadesInferiores = controladorUnidadeOrganizacional
					.listarUnidadesOrganizacionaisInferiores(chamadoVO.getIdUnidadeOrganizacionalUsuario())
					.stream().map(UnidadeOrganizacional::getChavePrimaria).collect(Collectors.toList());

			chamadoVO.setIdsUnidadesInferiores(unidadesInferiores);
		}

	}

	/**
	 * Verificar imovel pai.
	 *
	 * @param chamadoVO
	 *            the chamado vo
	 */
	private void verificarImovelPai(ChamadoVO chamadoVO) {

		Collection<Imovel> imoveis = repositorioChamado.verificarImovelCondominio(chamadoVO);
		if (imoveis != null && !imoveis.isEmpty()) {
			List<Long> idsImovelPai = new ArrayList<>();
			for (Imovel imovel : imoveis) {
				if (imovel.getCondominio()) {
					idsImovelPai.add(imovel.getChavePrimaria());
				}
			}
			if (CollectionUtils.isNotEmpty(idsImovelPai)) {
				chamadoVO.setIdsCondominioImovel(idsImovelPai);
			}

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#atualizarChamado(
	 * br.com.ggas.atendimento.chamado.dominio.Chamado, java.util.List)
	 */
	@Transactional
	@Override
	public void atualizarChamado(Chamado chamado, List<ContatoCliente> listaChamadoContato) throws GGASException {

		if (chamado.getEmailSolicitante() != null && !chamado.getEmailSolicitante().isEmpty()) {
			Util.validarEmail(chamado.getEmailSolicitante());
		}
		if (listaChamadoContato != null && !listaChamadoContato.isEmpty()) {
			if (chamado.getCliente() != null && chamado.getCliente().getContatos() != null) {
				chamado.getCliente().getContatos().clear();
				for (Iterator<ContatoCliente> iterator = listaChamadoContato.iterator(); iterator.hasNext();) {
					chamado.getCliente().getContatos().add(iterator.next());
					chamado.getCliente().setUltimaAlteracao(Calendar.getInstance().getTime());
				}
			}
			Cliente cliente = chamado.getCliente();

			controladorCliente.atualizarCliente(cliente);
		}
		chamado.setChamadoTipo(chamado.getChamadoAssunto().getChamadoTipo());
		repositorioChamado.atualizar(chamado);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * atualizarChamadoSemValidar(br.com.ggas.atendimento.chamado.dominio.Chamado )
	 */
	@Transactional
	@Override
	public void atualizarChamadoSemValidar(Chamado chamado) throws ConcorrenciaException, NegocioException {

		if (chamado.getEmailSolicitante() != null && !chamado.getEmailSolicitante().isEmpty()) {
			Util.validarEmail(chamado.getEmailSolicitante());
		}
		validarCanalAtendimentoChamadoRascunho(chamado);
		repositorioChamado.atualizarSemValidar(chamado, Chamado.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#tramitarChamado(br
	 * .com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void tramitarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException {

		repositorioChamado.validarDadosEntidade(chamado);
		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());
		verificarAlteracaoTramitacaoChamado(chamadoConsulta, chamado);

		ConstanteSistema constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_EM_ANDAMENTO);
		EntidadeConteudo status = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
		chamadoConsulta.setStatus(status);
		chamadoConsulta.setListaAnexos(chamado.getListaAnexos());
		chamadoConsulta.substituirListaUnidadeOrganizacionalVisualizadora(
				chamado.getListaUnidadeOrganizacionalVisualizadora());

		boolean jaExiste = false;
		Collection<ChamadoUnidadeOrganizacional> listaUnidadeVisualizadora = chamado
				.getListaUnidadeOrganizacionalVisualizadora();
		for (ChamadoUnidadeOrganizacional unidadeVisualizadora : listaUnidadeVisualizadora) {
			if (unidadeVisualizadora.getUnidadeOrganizacional().getChavePrimaria() == chamadoConsulta
					.getUnidadeOrganizacional().getChavePrimaria()) {
				jaExiste = true;
				break;
			}
		}

		if (!jaExiste) {
			ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional = new ChamadoUnidadeOrganizacional();
			chamadoUnidadeOrganizacional.setChamado(chamadoConsulta);
			chamadoUnidadeOrganizacional.setUnidadeOrganizacional(chamadoConsulta.getUnidadeOrganizacional());
			chamadoConsulta.adicionarChamadoUnidadeOrganizacional(chamadoUnidadeOrganizacional);
			chamadoConsulta.setIndicadorUnidadesOrganizacionalVisualizadora(true);
		}

		chamadoConsulta.setUnidadeOrganizacional(chamado.getUnidadeOrganizacional());
		chamadoConsulta.setDescricao(chamado.getDescricao());
		chamadoConsulta.setUsuarioResponsavel(chamado.getUsuarioResponsavel());

		repositorioChamado.atualizar(chamadoConsulta);
		notificadorEmailChamado.enviarEmailCasoNecessario(AcaoChamado.TRAMITAR, chamadoConsulta);
		incluirChamadoHistorico(chamadoConsulta, Constantes.C_OPERACAO_CHAMADO_TRAMITADO, dados);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#reiterarChamado(br
	 * .com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void reiterarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException {

		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		Integer quantidadeReiteracao = chamadoConsulta.getQuantidadeReiteracao();
		chamadoConsulta.setQuantidadeReiteracao(quantidadeReiteracao + 1);
		chamadoConsulta.setDescricao(chamado.getDescricao());
		chamadoConsulta.setListaAnexos(chamado.getListaAnexos());
		chamadoConsulta.setUnidadeOrganizacional(chamado.getUnidadeOrganizacional());

		repositorioChamado.atualizar(chamadoConsulta);
		notificadorEmailChamado.enviarEmailCasoNecessario(AcaoChamado.REITERAR, chamadoConsulta);

		incluirChamadoHistorico(chamadoConsulta, Constantes.C_OPERACAO_CHAMADO_REITERADO, dados);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#reativarChamado(br
	 * .com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void reativarChamado(Chamado chamado, Map<String, Object> dados)
			throws ConcorrenciaException, NegocioException {

		this.validarMotivo(Constantes.CHAMADO_OPERACAO_REATIVACAO, dados);
		

		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		Integer quantidadeReativacao = chamadoConsulta.getQuantidadeReativacao();
		chamadoConsulta.setQuantidadeReativacao(quantidadeReativacao + 1);
		chamadoConsulta.setDescricao(chamado.getDescricao());

		ConstanteSistema constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		EntidadeConteudo status = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
		chamadoConsulta.setStatus(status);
		chamadoConsulta.setListaAnexos(chamado.getListaAnexos());

		repositorioChamado.atualizar(chamadoConsulta);

		incluirChamadoHistorico(chamadoConsulta, Constantes.C_OPERACAO_CHAMADO_REATIVADO, dados);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#reabrirChamado(br.
	 * com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void reabrirChamado(Chamado chamado, Map<String, Object> dados)
			throws ConcorrenciaException, NegocioException {

		this.validarMotivo(Constantes.CHAMADO_OPERACAO_REABERTURA, dados);

		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		Integer quantidadeReabertura = chamadoConsulta.getQuantidadeReabertura();
		chamadoConsulta.setQuantidadeReabertura(quantidadeReabertura + 1);
		chamadoConsulta.setDescricao(chamado.getDescricao());
		chamadoConsulta.setIndicadorAgenciaReguladora(chamado.getIndicadorAgenciaReguladora());

		ConstanteSistema constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		EntidadeConteudo status = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
		chamadoConsulta.setStatus(status);
		chamadoConsulta.setListaAnexos(chamado.getListaAnexos());

		repositorioChamado.atualizar(chamadoConsulta);

		incluirChamadoHistorico(chamadoConsulta, Constantes.C_OPERACAO_CHAMADO_REABERTO, dados);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#capturarChamado(br
	 * .com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void capturarChamado(Chamado chamado, Map<String, Object> dados)
			throws ConcorrenciaException, NegocioException {

		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		chamadoConsulta.setDescricao(chamado.getDescricao());
		ConstanteSistema constanteSistema = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_EM_ANDAMENTO);
		EntidadeConteudo status = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
		chamadoConsulta.setStatus(status);
		repositorioChamado.validarDadosEntidade(chamadoConsulta);
		chamadoConsulta.setUsuarioResponsavel(chamado.getUsuarioResponsavel());
		repositorioChamado.atualizarSemValidar(chamadoConsulta, Chamado.class);

		incluirChamadoHistorico(chamadoConsulta, Constantes.C_OPERACAO_CHAMADO_CAPTURADO, dados);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#encerrarChamado(br
	 * .com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void encerrarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException {

		this.validarMotivo(Constantes.CHAMADO_OPERACAO_ENCERRAMENTO, dados);
		
		String idMotivo = (String) dados.get("idMotivo");
		
		if(!(idMotivo.equals("482") || idMotivo.equals("483"))) {
			this.validarDataResolucao(Constantes.CHAMADO_OPERACAO_ENCERRAMENTO, dados, chamado);
		}
		
		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());

		ConstanteSistema constante = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_FINALIZADO);
		EntidadeConteudo statusEncerrado = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		validarAutorizacaoServicoAberta(chamado);

		verificarDataPrevisaoEncerramentoChamado(chamado, chamadoConsulta.getProtocolo().getUltimaAlteracao());
		chamadoConsulta.setDataResolucao(chamado.getDataResolucao());

		chamadoConsulta.setDescricao(chamado.getDescricao());
		chamadoConsulta.setIndicadorVazamentoConfirmado(chamado.getIndicadorVazamentoConfirmado());
		repositorioChamado.validarDadosEntidade(chamadoConsulta, AcaoChamado.ENCERRAR);

		chamadoConsulta.setStatus(statusEncerrado);
		chamadoConsulta.setListaAnexos(chamado.getListaAnexos());

		repositorioChamado.atualizarSemValidar(chamadoConsulta, Chamado.class);
		notificadorEmailChamado.enviarEmailCasoNecessario(AcaoChamado.ENCERRAR, chamadoConsulta);
		incluirChamadoHistorico(chamadoConsulta, Constantes.C_STATUS_CHAMADO_FINALIZADO, dados);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * incluirChamadoHistorico(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * java.lang.String, java.util.Map)
	 */
	@Transactional
	@Override
	public void incluirChamadoHistorico(Chamado chamado, String tipoOperacao, Map<String, Object> dados)
			throws ConcorrenciaException, NegocioException {

		ChamadoHistorico chamadoHistorioco = new ChamadoHistorico();
		chamadoHistorioco.setChamado(chamado);
		chamadoHistorioco.setDescricao(chamado.getDescricao());
		chamadoHistorioco.setUsuarioResponsavel(chamado.getUsuarioResponsavel());
		chamadoHistorioco.setStatus(chamado.getStatus());
		chamadoHistorioco.setUnidadeOrganizacional(chamado.getUnidadeOrganizacional());

		chamadoHistorioco.getUnidadeOrganizacional().setDadosAuditoria(chamado.getDadosAuditoria());

		Usuario usuario = null;
		if (dados != null && !dados.isEmpty()) {

			usuario = (Usuario) dados.get("usuario");
		} else {

			usuario = chamado.getUsuarioResponsavel();
		}

		if (chamado.getListaAnexos() != null) {
			chamadoHistorioco.setAnexos(new HashSet<ChamadoHistoricoAnexo>(chamado.getListaAnexos()));
			for (ChamadoHistoricoAnexo ch : chamadoHistorioco.getAnexos()) {
				ch.setChamado(chamado);
			}
		}

		chamadoHistorioco.setUsuario(usuario);

		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(tipoOperacao);

		EntidadeConteudo operacao = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
		chamadoHistorioco.setOperacao(operacao);
		
		chamadoHistorioco.setUltimaAlteracao(Calendar.getInstance().getTime());
		
		if (dados != null && !dados.isEmpty()) {

			String idMotivo = (String) dados.get("idMotivo");
			if (idMotivo != null && !idMotivo.isEmpty() && Long.parseLong(idMotivo) > 0) {
				EntidadeConteudo motivo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.parseLong(idMotivo));
				chamadoHistorioco.setMotivo(motivo);
			}
		}
		chamadoHistorioco.setDadosAuditoria(chamado.getDadosAuditoria());
		repositorioChamado.inserir(chamadoHistorioco);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * verificarStatusChamado(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * java.lang.String)
	 */
	@Transactional
	@Override
	public void verificarStatusChamado(Chamado chamado, String tipoOperacao) throws NegocioException {

		if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_TRAMITACAO) && chamado.getChamadoAssunto() != null
				&& !chamado.getChamadoAssunto().getIndicadorTramitacao()) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_TRAMITACAO);
		}

		if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_REABERTURA)) {
			if (!chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			}
		} else if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_REATIVACAO)) {
			if (!chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			}
		} else if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_TRAMITACAO)
				|| tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_REITERACAO)
				|| tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_CAPTURA)) {
			if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_RASCUNHO)
					|| chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			}
		} else if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_ENCERRAMENTO)) {
			if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			} else if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_RASCUNHO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			}
		} else if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_ALTERACAO)) {
			if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			}
		} else if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_GERACAO_SERVICO_AUTORIZACAO)) {
			if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_RASCUNHO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			} else if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO, chamado.getStatus().getDescricao());
			}
		} else if (tipoOperacao.equalsIgnoreCase(Constantes.CHAMADO_OPERACAO_RESPONDER_QUESTIONARIO)
				&& !chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_STATUS, Constantes.CHAMADO_STATUS_FINALIZADO);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#validarMotivo(java
	 * .lang.String, java.util.Map)
	 */
	@Transactional
	@Override
	public void validarMotivo(String tipoOperacao, Map<String, Object> dados) throws NegocioException {

		String idMotivo = (String) dados.get("idMotivo");
		
		if (idMotivo != null && !idMotivo.isEmpty() && Long.parseLong(idMotivo) < 0) {

			if (tipoOperacao.equals(Constantes.CHAMADO_OPERACAO_REABERTURA)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_MOTIVO,
						Constantes.CHAMADO_MOTIVO_REABERTURA);
			} else if (tipoOperacao.equals(Constantes.CHAMADO_OPERACAO_REATIVACAO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_MOTIVO,
						Constantes.CHAMADO_MOTIVO_REATIVACAO);
			} else if (tipoOperacao.equals(Constantes.CHAMADO_OPERACAO_ENCERRAMENTO)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_MOTIVO,
						Constantes.CHAMADO_MOTIVO_ENCERRAMENTO);
			} 
		}
	}
	
	@Transactional
	@Override
	public void validarDataResolucao(String tipoOperacao, Map<String, Object> dados, Chamado chamado) throws NegocioException{
		
		if(chamado.getDataResolucao() == null) {
			if(tipoOperacao.equals(Constantes.CHAMADO_OPERACAO_ENCERRAMENTO)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_MOTIVO,
					Constantes.CHAMADO_DATA_RESOLUCAO);
			}
		}
		
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#verificarCaptura(
	 * br.com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	@Transactional
	@Override
	public void verificarCaptura(Chamado chamado, Map<String, Object> dados) throws NegocioException {

		Usuario usuario = (Usuario) dados.get("usuario");
		if (chamado.getUsuarioResponsavel() != null
				&& usuario.getChavePrimaria() == chamado.getUsuarioResponsavel().getChavePrimaria()) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_CAPTURADO);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#gerarProtocolo(
	 * java.lang.Long)
	 */
	@Override
	@Transactional
	public ChamadoProtocolo gerarProtocolo(Long numeroProtocoloManual) throws NegocioException {

		ChamadoProtocolo chamadoProtocolo = new ChamadoProtocolo();

		if (numeroProtocoloManual == null) {
			Integer sequencialIncrementado = Integer.valueOf(0001);
			String sequencial;
			String numeroProtocolo = repositorioChamado.obterUltimoProtocoloDia();
			if (numeroProtocolo != null) {
				if (numeroProtocolo.length() < TAMANHO_MAXIMO_PROTOCOLO) {
					sequencial = numeroProtocolo.substring(INDICE_PROTOCOLO, numeroProtocolo.length());
				} else {
					sequencial = numeroProtocolo.substring(INDICE_PROTOCOLO, TAMANHO_MAXIMO_PROTOCOLO);
				}
				sequencialIncrementado = sequencialIncrementado + Integer.parseInt(sequencial);
			}
			SimpleDateFormat dt1 = new SimpleDateFormat("yyMMdd");
			String prefixo = dt1.format(Calendar.getInstance().getTime());

			chamadoProtocolo.setNumeroProtocolo(Long.valueOf(prefixo + String.format("%04d", sequencialIncrementado)));
		} else {
			chamadoProtocolo.setNumeroProtocolo(numeroProtocoloManual);
		}
		repositorioChamado.inserir(chamadoProtocolo);
		return chamadoProtocolo;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * consultarChamadoPorClienteOuImovel(java.lang.Long, java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	@Transactional
	public Collection<Chamado> consultarChamadoPorClienteOuImovel(Long chavePrimariaCliente, Long chavePrimariaImovel,
			Long chavePrimariaPontoConsumo) throws NegocioException {

		return repositorioChamado.consultarChamadoPorClienteImovelPontoConsumo(chavePrimariaCliente,
				chavePrimariaImovel, chavePrimariaPontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * listarChamadoHistorico(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	@Transactional
	public Collection<ChamadoHistorico> listarChamadoHistorico(Chamado chamado, EntidadeConteudo operacao)
			throws NegocioException {

		return repositorioChamado.listarChamadoHistorico(chamado, operacao);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#obterProtocolo(
	 * java.lang.Long)
	 */
	@Override
	@Transactional
	public ChamadoProtocolo obterProtocolo(Long chavePrimariaProtocolo) throws NegocioException {

		return (ChamadoProtocolo) repositorioChamado.obter(chavePrimariaProtocolo, ChamadoProtocolo.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * listarImoveisPorCliente(java.lang.Long)
	 */
	@Override
	@Transactional
	public List<Imovel> listarImoveisPorCliente(Long chaveCliente) throws NegocioException {

		List<Imovel> imoveis = new ArrayList<>();
		Collection<PontoConsumo> pontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(chaveCliente);
		for (Iterator<PontoConsumo> it = pontoConsumo.iterator(); it.hasNext();) {
			imoveis.add(it.next().getImovel());
		}
		return imoveis;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * validarUsuarioAberturaChamado(br.com.ggas.controleacesso.Usuario)
	 */
	@Transactional
	@Override
	public void validarUsuarioAberturaChamado(Usuario usuario) throws NegocioException {

		if (usuario == null || usuario.getFuncionario() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_USUARIO_NAO_AUTORIZADO);
		} else {
			if (usuario.getFuncionario().getUnidadeOrganizacional() != null) {
				if (!usuario.getFuncionario().getUnidadeOrganizacional().isIndicadorAbreChamado()) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_UNOR_NAO_AUTORIZADO);
				}
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_UNOR_NAO_POSSUI_UNIDADE_ORGANIZACIONAL);
			}
		}

	}

	/**
	 * Verificar alteracao tramitacao chamado.
	 *
	 * @param chamadoConsulta
	 *            the chamado consulta
	 * @param chamado
	 *            the chamado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Transactional
	public void verificarAlteracaoTramitacaoChamado(Chamado chamadoConsulta, Chamado chamado) throws NegocioException {

		if (chamadoConsulta.getUnidadeOrganizacional() != null && chamado.getUnidadeOrganizacional() != null
				&& chamadoConsulta.getUsuarioResponsavel() != null && chamado.getUsuarioResponsavel() != null) {
			if (chamadoConsulta.getUnidadeOrganizacional().getChavePrimaria() == chamado.getUnidadeOrganizacional()
					.getChavePrimaria()
					&& chamadoConsulta.getUsuarioResponsavel().getChavePrimaria() == chamado.getUsuarioResponsavel()
					.getChavePrimaria()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_ALTERACAO);
			}
		} else if (chamadoConsulta.getUnidadeOrganizacional() != null && chamado.getUnidadeOrganizacional() != null
				&& chamadoConsulta.getUsuarioResponsavel() == null && chamado.getUsuarioResponsavel() == null
				&& (chamadoConsulta.getUnidadeOrganizacional().getChavePrimaria() == chamado.getUnidadeOrganizacional()
				.getChavePrimaria())) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_ALTERACAO);
		}
		if(chamado.getUsuarioResponsavel() == null){
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_TRAMITACAO_SEM_RESPONSAVEL);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * gerarAutorizacaoServico(java.lang.Long[], java.util.Map)
	 */
	@Override
	@Transactional
	public Collection<ServicoAutorizacao> gerarAutorizacaoServico(Long[] chavesPrimarias,
			Long[] chavesPrimariasTipoServico, Map<String, Object> dados) throws GGASException {

		Collection<ServicoAutorizacao> listaServicoAutorizacao = new ArrayList<>();
		Collection<ServicoAutorizacao> listaTodos = new ArrayList<>();
		for (Long chave : chavesPrimarias) {
			if (chave != null && chave != 0L) {
				Chamado chamado = repositorioChamado.obterChamado(chave);

				if (!chamado.getChamadoAssunto().getIndicadorGeraServicoAutorizacao()) {
					this.verificarStatusChamado(chamado, Constantes.CHAMADO_GERACAO_SERVICO_AUTORIZACAO);
				}
				ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
				servicoAutorizacaoVO.setNumeroProtocoloChamado(chamado.getProtocolo().getNumeroProtocolo());
				Collection<ServicoAutorizacao> servicoAutorizacaoList = controladorServicoAutorizacao
						.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE);
				
				servicoAutorizacaoList = servicoAutorizacaoList.stream()
						.filter(p ->!p.getStatus().getDescricao().equals("CANCELADA"))
						.collect(Collectors.toList());

				if (servicoAutorizacaoList.isEmpty()
						|| servicoAutorizacaoList.size() != chamado.getChamadoAssunto().getListaServicos().size()) {
					popularListaServicoAutorizacao(listaServicoAutorizacao, chavesPrimariasTipoServico, chamado);
				} else {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_GERACAO_SERVICO_AUTORIZACAO);
				}

				for (ServicoAutorizacao servico : listaServicoAutorizacao) {
					
					if (servicoAutorizacaoList.stream().anyMatch(p -> p.getServicoTipo().getChavePrimaria() == servico
							.getServicoTipo().getChavePrimaria())) {
						continue;
					}
					
					servico.setEquipe((Equipe) dados.get("equipe"));
					servico.setTurnoPreferencia((EntidadeConteudo) dados.get("turnoPreferencia"));
					
					controladorServicoAutorizacao.inserirServicoAutorizacao(servico, dados, Boolean.FALSE);
					listaTodos.add(servico);
				}

			}

		}

		if (listaServicoAutorizacao.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_GERACAO_SEM_SERVICO);
		}

		return listaTodos;
	}

	/**
	 * @param listaServicoAutorizacao
	 * @param chavesPrimariasTipoServico
	 * @param chamado
	 */
	private void popularListaServicoAutorizacao(Collection<ServicoAutorizacao> listaServicoAutorizacao,
			Long[] chavesPrimariasTipoServico, Chamado chamado) throws NegocioException {

		long chavePrimaria = chamado.getChamadoAssunto().getChavePrimaria();
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(chavePrimaria);

		for (ChamadoAssuntoServicoTipo servicoTipo : chamadoAssunto.getListaServicos()) {
			if (chavesPrimariasTipoServico != null && chavesPrimariasTipoServico.length > 0) {
				montaServicoAutorizacaoSelecionado(listaServicoAutorizacao, chavesPrimariasTipoServico, chamado,
						servicoTipo);
			} else {
				montaServicoAutorizacao(listaServicoAutorizacao, chamado, servicoTipo);
			}
		}
	}

	/**
	 * @param listaServicoAutorizacao
	 * @param chavesPrimariasTipoServico
	 * @param chamado
	 * @param servicoTipo
	 */
	private void montaServicoAutorizacaoSelecionado(Collection<ServicoAutorizacao> listaServicoAutorizacao,
			Long[] chavesPrimariasTipoServico, Chamado chamado, ChamadoAssuntoServicoTipo servicoTipo) {

		for (Long chavePrimariaTipoServico : chavesPrimariasTipoServico) {
			if (chavePrimariaTipoServico != null
					&& chavePrimariaTipoServico.equals(servicoTipo.getServicoTipo().getChavePrimaria())) {
				montaServicoAutorizacao(listaServicoAutorizacao, chamado, servicoTipo);
			}
		}

	}

	/**
	 * Monta Autorizacao de Servico
	 *
	 * @param listaServicoAutorizacao
	 * @param chamado
	 * @param servicoAutorizacao
	 * @param chamadoAssuntoServicoTipo
	 */
	private void montaServicoAutorizacao(Collection<ServicoAutorizacao> listaServicoAutorizacao, Chamado chamado,
			ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo) {

		Boolean servicoRestrito = false;

		if (chamado.getImovel() != null && Util.isTrue(chamado.getChamadoAssunto().getIndicadorVerificaRestServ())) {
			Collection<ImovelServicoTipoRestricao> listaImovelServicoTipoRestricao = Fachada.getInstancia()
					.consultarImovelServicoTipoRestricaoPorImovel(chamado.getImovel().getChavePrimaria());

			for (ImovelServicoTipoRestricao servicoTipoRestricao : listaImovelServicoTipoRestricao) {
				if (servicoTipoRestricao.getServicoTipo().getChavePrimaria() == chamadoAssuntoServicoTipo
						.getServicoTipo().getChavePrimaria()) {
					servicoRestrito = true;
				}
			}

		}

		if (!servicoRestrito) {
			ServicoAutorizacao servicoAutorizacao = new ServicoAutorizacao();
			servicoAutorizacao.setServicoTipo(chamadoAssuntoServicoTipo.getServicoTipo());
			servicoAutorizacao
					.setServicoTipoPrioridade(chamadoAssuntoServicoTipo.getServicoTipo().getServicoTipoPrioridade());

			servicoAutorizacao.setChamado(chamado);

			if (chamado.getCliente() != null) {
				servicoAutorizacao.setCliente(chamado.getCliente());
			}

			if (chamado.getContrato() != null) {
				servicoAutorizacao.setContrato(chamado.getContrato());
			}

			if (chamado.getImovel() != null) {
				servicoAutorizacao.setImovel(chamado.getImovel());
			}

			if (chamado.getPontoConsumo() != null) {
				servicoAutorizacao.setPontoConsumo(chamado.getPontoConsumo());
			}

			servicoAutorizacao.setDataGeracao(chamado.getUltimaAlteracao());
			servicoAutorizacao.setDescricao(chamado.getDescricao());
			servicoAutorizacao.setHabilitado(Boolean.TRUE);
			servicoAutorizacao.setUsuarioSistema(chamado.getUsuarioResponsavel());
			servicoAutorizacao.setStatus(chamado.getStatus());

			listaServicoAutorizacao.add(servicoAutorizacao);
		}
	}

	/**
	 * Metodo que gera um chamado do tipo pesquisa de satisfação, o mesmo e gerado
	 * quando o serviço de altorização que esta sendo encerrado e o seu tipo de
	 * serviço esta com a opção "Gerar Pesquisa de Satisfação Automatica" marcada.
	 *
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param dados
	 *            the dados
	 * @return the chamado
	 * @throws GGASException
	 * @throws NumberFormatException
	 *             the number format exception
	 */
	@Override
	@Transactional
	public Chamado gerarChamadoPerquisaSatisfacao(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados)
			throws GGASException {

		Chamado chamado = new Chamado();
		ConstanteSistema constante = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_PESQUISA);
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(Long.valueOf(constante.getValor()));
		chamado.setChamadoTipo(chamadoAssunto.getChamadoTipo());
		chamado.setChamadoAssunto(chamadoAssunto);
		chamado.setCliente(servicoAutorizacao.getCliente());
		chamado.setImovel(servicoAutorizacao.getImovel());
		chamado.setDescricao("Pesquisa de satisfação");

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		chamado.setDataPrevisaoEncerramento(calendar.getTime());

		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CANAL_ATENDIMENTO_PESQUISA);
		chamado.setCanalAtendimento((CanalAtendimento) controladorUnidadeOrganizacional
				.obter(Long.parseLong(constante.getValor()), CanalAtendimentoImpl.class));
		chamado.setUnidadeOrganizacional(chamadoAssunto.getUnidadeOrganizacional());
		chamado.setPontoConsumo(servicoAutorizacao.getPontoConsumo());
		chamado.setProtocolo(gerarProtocolo(null));
		chamado.setIndicadorAgenciaReguladora(Boolean.FALSE);
		
		if(chamadoAssunto.getQuestionario() != null) {
			chamado.setQuestionario(chamadoAssunto.getQuestionario());
		}
		
		return this.inserirChamadoPesquisa(chamado, null, dados);
	}

	/**
	 * Metodo que verifica se existe algum chamado a tralado ao questionario passa
	 * como parametro.
	 *
	 * @param questionario
	 *            the questionario
	 * @return true, if successful
	 */
	@Override
	@Transactional
	public boolean existeChamadoQuestionario(Questionario questionario) {

		return repositorioChamado.existeChamadoQuestionario(questionario);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * obterChamadoAlteracaoVencimento(java.lang.Long, java.lang.Long)
	 */
	@Override
	@Transactional
	public ChamadoAlteracaoVencimento obterChamadoAlteracaoVencimento(Long idChamado, Long idPontoConsumo)
			throws NegocioException {

		return repositorioChamado.obterChamadoAlteracaoVencimento(idChamado, idPontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * obterChamadoHistorico(java.lang.Long)
	 */
	@Override
	@Transactional
	public ChamadoHistorico obterChamadoHistorico(Long chavePrimaria) throws NegocioException {

		return (ChamadoHistorico) repositorioChamado.obter(chavePrimaria, ChamadoHistorico.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * obterChamadoHistorico(java.lang.Long)
	 */
	@Override
	@Transactional
	public ChamadoHistoricoAnexo obterChamadoHistoricoAnexo(Long chavePrimaria) throws NegocioException {

		return (ChamadoHistoricoAnexo) repositorioChamado.obter(chavePrimaria, ChamadoHistoricoAnexo.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * inserirChamadoAlteracaoVencimento(br.com.ggas.atendimento.chamado.dominio.
	 * ChamadoAlteracaoVencimento)
	 */
	@Override
	public void inserirChamadoAlteracaoVencimento(ChamadoAlteracaoVencimento chamadoAlteracaoVencimento)
			throws NegocioException, ConcorrenciaException {

		repositorioChamado.inserir(chamadoAlteracaoVencimento);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * inserirChamadoAlteracaoCliente(br.com.ggas.cadastro.cliente.dominio.
	 * ChamadoAlteracaoCliente)
	 */
	@Override
	public void inserirChamadoAlteracaoCliente(ChamadoAlteracaoCliente chamadoAlteracaoCliente)
			throws NegocioException, ConcorrenciaException {

		repositorioChamado.inserir(chamadoAlteracaoCliente);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * registrarTentativaContato(br.com.ggas.atendimento.chamado.dominio.Chamado,
	 * java.util.Map)
	 */
	@Override
	public void registrarTentativaContato(Chamado chamado, Map<String, Object> dados) throws NegocioException {

		Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
		chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
		Integer quantidadeTentativaContato = chamadoConsulta.getQuantidadeTentativaContato();
		chamadoConsulta.setQuantidadeTentativaContato(quantidadeTentativaContato + 1);
		try {
			repositorioChamado.atualizar(chamadoConsulta);
			incluirChamadoHistorico(chamadoConsulta, Constantes.C_OPERACAO_CHAM_TENT_CONTATO, dados);
		} catch (ConcorrenciaException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REGISTRAR_TENTATIVA_CONTATO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * validarAutorizacaoServicoAberta(br.com.ggas.atendimento.chamado.dominio.
	 * Chamado)
	 */
	@Override
	public void validarAutorizacaoServicoAberta(Chamado chamado) throws GGASException {

		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ABERTO);
		EntidadeConteudo statusAbertoServicoAutorizacao = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
		servicoAutorizacaoVO.setNumeroProtocoloChamado(chamado.getProtocolo().getNumeroProtocolo());
		Collection<ServicoAutorizacao> listaServicoAutorizacao = controladorServicoAutorizacao
				.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE);
		for (ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacao) {
			if (servicoAutorizacao.getStatus().equals(statusAbertoServicoAutorizacao)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ENCERRAR_SEAU_ABERTA, true);
			}
		}
	}

	/**
	 * Validar versao entidade.
	 *
	 * @param versao
	 *            the versao
	 * @param versaoAtual
	 *            the versao atual
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void validarVersaoEntidade(int versao, int versaoAtual) throws ConcorrenciaException {

		if (versao != versaoAtual) {
			throw new ConcorrenciaException(Constantes.ERRO_ENTIDADE_VERSAO_DESATUALIZADA, "servicoAutorizacao");
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * verificarChamadoAbertoPorChamadoAssunto(java.lang.String, java.lang.Integer)
	 */
	@Override
	public Long verificarChamadoAbertoPorChamadoAssunto(String assunto, Integer numeroContrato)
			throws GGASException {

		ConstanteSistema constanteChamado = null;
		constanteChamado = controladorConstanteSistema.obterConstantePorCodigo(assunto);
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto
				.obter(Long.valueOf(constanteChamado.getValor()));

		constanteChamado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		EntidadeConteudo statusAberto = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteChamado.getValor()));

		ChamadoVO chamadoVO = new ChamadoVO();
		List<String> status = new ArrayList<>();
		status.add(statusAberto.getDescricao());

		chamadoVO.setListaStatus(status);
		chamadoVO.setChamadoAssunto(chamadoAssunto);
		chamadoVO.setNumeroContrato(numeroContrato);
		Collection<Chamado> chamado = repositorioChamado.consultarChamado(chamadoVO, Boolean.FALSE);

		if (chamado != null && !chamado.isEmpty()) {
			return chamado.iterator().next().getProtocolo().getNumeroProtocolo();
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * verificarChamadoAbertoPorChamadoAssuntoPontoConsumo(java.lang.String,
	 * java.lang.Integer, java.lang.Long)
	 */
	@Override
	public Long verificarChamadoAbertoPorChamadoAssuntoPontoConsumo(String assunto, Integer numeroContrato,
			Long pontoConsumo) throws NegocioException {

		Collection<Chamado> chamado = consultarChamadoAbertoPorChamadoAssuntoPontoConsumo(assunto, numeroContrato,
				pontoConsumo);

		if (chamado != null && !chamado.isEmpty()) {
			return chamado.iterator().next().getProtocolo().getNumeroProtocolo();
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * inserirChamadoEmail(java.util.List,
	 * br.com.ggas.atendimento.chamado.dominio.Chamado)
	 */
	@Override
	public void inserirChamadoEmail(List<ChamadoEmail> listaChamadoEmail, Chamado chamado) throws NegocioException {

		Collection<ChamadoEmail> listaChamadoEmailOld = listarChamadoEmail(chamado);
		for (ChamadoEmail chamadoEmail : listaChamadoEmailOld) {
			chamadoEmail.setChamado(chamado);
			repositorioChamado.remover(chamadoEmail);
		}

		for (ChamadoEmail chamadoEmail : listaChamadoEmail) {
			chamadoEmail.setChamado(chamado);
			repositorioChamado.inserir(chamadoEmail);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#listarChamadoEmail
	 * (br.com.ggas.atendimento.chamado.dominio.Chamado)
	 */
	@Override
	public Collection<ChamadoEmail> listarChamadoEmail(Chamado chamado) throws NegocioException {

		return repositorioChamado.listarChamadoEmail(chamado);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * obterUltimoChamadoAlteracaoVencimento(java.lang.Long)
	 */
	@Override
	@Transactional
	public ChamadoAlteracaoVencimento obterUltimoChamadoAlteracaoVencimento(Long idPontoConsumo)
			throws NegocioException {

		return repositorioChamado.obterUltimoChamadoAlteracaoVencimento(idPontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * consultarChamadoAbertoPorChamadoAssuntoPontoConsumo(java.lang.String,
	 * java.lang.Integer, java.lang.Long)
	 */
	@Override
	public Collection<Chamado> consultarChamadoAbertoPorChamadoAssuntoPontoConsumo(String assunto,
			Integer numeroContrato, Long chavePrimariaPontoConsumo) throws NegocioException {

		ConstanteSistema constanteChamado = null;
		constanteChamado = controladorConstanteSistema.obterConstantePorCodigo(assunto);
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto
				.obter(Long.valueOf(constanteChamado.getValor()));

		constanteChamado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		EntidadeConteudo statusAberto = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteChamado.getValor()));

		ChamadoVO chamadoVO = new ChamadoVO();
		chamadoVO.setStatus(statusAberto);
		chamadoVO.setChamadoAssunto(chamadoAssunto);
		chamadoVO.setNumeroContrato(numeroContrato);
		return repositorioChamado.consultarChamadoPontoConsumo(chamadoVO, chavePrimariaPontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#
	 * existeServicoAutorizacaoGerada(br.com.ggas.atendimento.chamado.dominio.
	 * Chamado)
	 */
	@Override
	public boolean existeServicoAutorizacaoGerada(Chamado chamado) {
		List<ServicoAutorizacao> listaServicoAutorizacao = controladorServicoAutorizacao
				.consultarServicoAutorizacaoPorChaveChamado(chamado.getChavePrimaria());
		return listaServicoAutorizacao != null && !listaServicoAutorizacao.isEmpty();
	}

	@Override
	public Collection<Chamado> listarChamadosAtrasadosPorUnidadeOrganizacional(
			UnidadeOrganizacional unidadeOrganizacional) throws GGASException {

		ChamadoVO chamadoVO = new ChamadoVO();
		chamadoVO.setIdUnidadeOrganizacionalUsuario(unidadeOrganizacional.getChavePrimaria());

		ArrayList<String> prazo = new ArrayList<>();
		prazo.add("ATRASADOS");

		chamadoVO.setListaPrazo(prazo);

		List<String> status = new ArrayList<>();
		status.add("EM ANDAMENTO");
		status.add("ABERTO");
		chamadoVO.setListaStatus(status);

		return repositorioChamado.consultarChamado(chamadoVO, Boolean.FALSE);
	}

	/**
	 * Valida dados do anexo
	 *
	 * @param chamadoHistoricoAnexo
	 * @throws NegocioException
	 */
	@Override
	public void validarDadosAnexo(ChamadoHistoricoAnexo chamadoHistoricoAnexo) throws NegocioException {

		Map<String, Object> errosChamadoHistoricoAnexo = chamadoHistoricoAnexo.validarDados();

		if (errosChamadoHistoricoAnexo != null && !errosChamadoHistoricoAnexo.isEmpty()) {
			throw new NegocioException(errosChamadoHistoricoAnexo);
		}

	}

	/**
	 * @param indexLista
	 * @param listaClienteAnexo
	 * @param clienteAnexo
	 * @throws NegocioException
	 */
	@Override
	public void verificarDescricaoAnexo(Integer indexLista, Collection<ChamadoHistoricoAnexo> listaAnexo,
			ChamadoHistoricoAnexo chamadoHistoricoAnexo) throws NegocioException {

		ArrayList<ChamadoHistoricoAnexo> listaAnexoArray = new ArrayList<>(listaAnexo);
		for (int i = 0; i < listaAnexo.size(); i++) {

			ChamadoHistoricoAnexo chamadoHistoricoAnexoExistente = listaAnexoArray.get(i);

			String descricao = chamadoHistoricoAnexoExistente.getDescricaoAnexo();
			if (descricao != null && descricao.equals(chamadoHistoricoAnexo.getDescricaoAnexo()) && indexLista != i) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE, true);
			}
		}
	}

	/**
	 * Verificar data previsao encerramento chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param dataInicio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarDataPrevisaoEncerramentoChamado(Chamado chamado, Date dataInicio) throws GGASException {

		if (chamado.getDataResolucao() != null
				&& (chamado.getDataResolucao().before(dataInicio) || chamado.getDataResolucao().after(new Date()))) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_DATAS, true);
		}
	}

	@Override
	public ResultadoEncerramentoEmLote encerrarChamadoEmLote(List<Chamado> chamados, Map<String, Object> dados) {
		ResultadoEncerramentoEmLote resultadoEncerramentoEmLote = ResultadoEncerramentoEmLote.build();
		List<String> erros = new ArrayList<>();
		List<Chamado> chamadosSucessos = new ArrayList<>();
		for (Chamado chamado : chamados) {

			try {
				String protocolo = "Chamado "+chamado.getProtocolo().getNumeroProtocolo().toString() + ":";
				encerrarEmLoteValidarMotivo(dados, erros, protocolo);

				if (chamado.isPossuiASAberta()) {
					erros.add(protocolo + " possui A.S. Aberta");
					continue;
				}

				encerrarEmLoteValidarASAberta(erros, chamado, protocolo);
				encerrarEmLoteValidarStatus(erros, chamado, protocolo);

				Chamado chamadoConsulta = repositorioChamado.obterChamado(chamado.getChavePrimaria());
				chamadoConsulta.setChamadoEmLote(true);
				encerrarEmLoteValidarVersaoEntidade(erros, chamado, protocolo, chamadoConsulta);

				ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_FINALIZADO);
				EntidadeConteudo statusEncerrado = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

				encerrarEmLoteValidarDataPrevisaoEncerramento(erros, chamado, protocolo, chamadoConsulta);
				encerrarEmLoteValidarEntidade(erros, protocolo, chamadoConsulta);

				chamadoConsulta.setDescricao(chamado.getDescricao());
				chamadoConsulta.setStatus(statusEncerrado);
				chamadoConsulta.setDataResolucao(chamado.getDataResolucao());

				encerrarEmLoteValidarAtualizacao(erros, protocolo, chamadoConsulta);

				encerrarEmLoteValidarInserirHistorico(dados, erros, protocolo, chamadoConsulta);
				chamado.setStatus(chamadoConsulta.getStatus());

				resultadoEncerramentoEmLote
						.adicionarChamadoSucesso(chamadoConsulta.getChavePrimaria(), chamadoConsulta.getStatus().getDescricao());
				chamadosSucessos.add(chamadoConsulta);
			} catch (Exception e) {
				LOG.error("O chamado do lote não foi encerrado", e);
			}
		}

		this.notificadorEmailChamado.enviarEmailEncerramentoLoteCasoNecessario(chamadosSucessos);
		resultadoEncerramentoEmLote.setErros(erros);
		return resultadoEncerramentoEmLote;
	}

	private void encerrarEmLoteValidarInserirHistorico(Map<String, Object> dados, List<String> erros, String protocolo,
			Chamado chamadoConsulta) throws GGASException {
		try {
			incluirChamadoHistorico(chamadoConsulta, Constantes.C_STATUS_CHAMADO_FINALIZADO, dados);
		} catch (ConcorrenciaException e) {
			erros.add(protocolo + " erro de concorrência ao inserir histórico do chamado");
			throw e;
		} catch (NegocioException e) {
			erros.add(protocolo + " erro na base de dados ao inserir histórico do chamado");
			throw e;
		}
	}

	private void encerrarEmLoteValidarAtualizacao(List<String> erros, String protocolo, Chamado chamadoConsulta) throws GGASException {
		try {
			repositorioChamado.atualizarSemValidar(chamadoConsulta, Chamado.class);
		} catch (ConcorrenciaException e) {
			erros.add(protocolo + " erro de concorrência ao atualizar chamado");
			throw e;
		} catch (NegocioException e) {
			erros.add(protocolo + " erro na base de dados ao atualizar chamado");
			throw e;
		}
	}

	private void encerrarEmLoteValidarEntidade(List<String> erros, String protocolo, Chamado chamadoConsulta) throws GGASException {
		try {
			repositorioChamado.validarDadosEntidade(chamadoConsulta, AcaoChamado.ENCERRAR);
		} catch (NegocioException e) {
			erros.add(protocolo + " dados de entidade inválida");
			throw e;
		}
	}

	private void encerrarEmLoteValidarDataPrevisaoEncerramento(List<String> erros, Chamado chamado, String protocolo,
			Chamado chamadoConsulta) throws GGASException {
		try {
			verificarDataPrevisaoEncerramentoChamado(chamado, chamadoConsulta.getProtocolo().getUltimaAlteracao());
		} catch (GGASException e) {
			erros.add(protocolo
					+ " Data de Resolução do chamado não pode ser inferior a Data de Abertura do chamado ou superior a data atual.");
			throw e;
		}
	}

	private void encerrarEmLoteValidarASAberta(List<String> erros, Chamado chamado, String protocolo) throws GGASException {
		try {
			validarAutorizacaoServicoAberta(chamado);
		} catch (GGASException e) {
			erros.add(protocolo + " possui autorização de serviço aberta");
			throw e;
		}
	}

	private void encerrarEmLoteValidarStatus(List<String> erros, Chamado chamado, String protocolo) throws GGASException {
		if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
			erros.add(protocolo + " já foi Encerrado");
			throw new GGASException("chamado já foi encerrado");
		}
		try {
			this.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_ENCERRAMENTO);
		} catch (GGASException e) {
			erros.add(protocolo + " status atual não é válido para o encerramento");
			throw e;
		}
	}

	private void encerrarEmLoteValidarVersaoEntidade(List<String> erros, Chamado chamado, String protocolo, Chamado chamadoConsulta)
			throws GGASException {
		try {
			chamadoConsulta.setChamadoTipo(chamadoConsulta.getChamadoAssunto().getChamadoTipo());
			validarVersaoEntidade(chamado.getVersao(), chamadoConsulta.getVersao());
		} catch (ConcorrenciaException e) {
			erros.add(protocolo + " desatualizado, recarregue para tentar encerrar");
			throw e;
		}
	}

	private void encerrarEmLoteValidarMotivo(Map<String, Object> dados, List<String> erros, String protocolo) throws GGASException {
		try {
			this.validarMotivo(Constantes.CHAMADO_OPERACAO_ENCERRAMENTO, dados);
		} catch (NegocioException e) {
			erros.add(protocolo + " não foi possível validar o motivo");
			throw e;
		}
	}

	public void verificarPermissaoTramitarUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) throws NegocioException {
		if (!unidadeOrganizacional.isIndicadorTramite()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_UNIDADE_ORGANIZACIONAL_TRAMITACAO,
					unidadeOrganizacional.getDescricao());
		}
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamado.negocio.ControladorChamado#enviarEmailHistoricoChamado(java.lang.Long)
	 */
	@Override
	public void enviarEmailHistoricoChamado(Long idChamado) throws GGASException {
		LOG.isTraceEnabled();
	}
	
	/**
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamado.negocio.ControladorChamado#gerarASAutomaticaInclusaoChamado(
	 * br.com.ggas.atendimento.chamado.dominio.Chamado, java.util.Map)
	 */
	public void gerarASAutomaticaInclusaoChamado(Chamado chamado, Map<String, Object> dados) throws GGASException {
		if (chamado.getChamadoAssunto().getIndicadorGeraServicoAutorizacao() && !chamado.isIndicadorPesquisa()) {
			Long[] chavePrimaria = new Long[] { chamado.getChavePrimaria() };
			try {
				this.gerarAutorizacaoServico(chavePrimaria, null, dados);
			} catch (AutorizacaoServicoRepetidaException ex) {
				LOG.warn(ex);
				chamado.setPossuiASDuplicada(true);
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		
	}

	/**
	 * Lista os agendamentos de um cliente
	 * @param cliente
	 * @return retorna lista de Agendamentos do Cliente
	 */
	@Override
	public List<ServicoAutorizacaoAgendasVO> listarAgandamentosPorCliente(Cliente cliente) {
		return controladorServicoAutorizacao.listarAgandamentosPorCliente(cliente);
	}

	@Override
	public void enviarEmailHistoricoChamado(String email, Long faturaID, Long docFiscalID) {
		LOG.isTraceEnabled();
		System.out.println(email + faturaID + docFiscalID);
		
	}

	@Override
	public void atualizarChamadoTituloAnalise(Collection<TitulosGSAVO> listaTitulosAnaliseSessao,
			Collection<ChamadoTituloAnalise> listaChamadoTituloAnalise, Map<String, Object> dados,
			Chamado chamadoExistente) throws ConcorrenciaException, NegocioException {
		
		Long numeroProtocolo = chamadoExistente.getProtocolo().getNumeroProtocolo();
		Usuario usuario = (Usuario) dados.get("usuario");
		if (listaTitulosAnaliseSessao.isEmpty() && !listaChamadoTituloAnalise.isEmpty()) {
			for (ChamadoTituloAnalise chamadoTituloAnalise : listaChamadoTituloAnalise) {
				chamadoTituloAnalise.setEmAnalise(Boolean.FALSE);

				repositorioChamado.atualizar(chamadoTituloAnalise, ChamadoTituloAnalise.class);
				repositorioChamado.incluirTituloAnaliseGSA(chamadoTituloAnalise.getNumeroTitulo(), usuario.getFuncionario().getNome(), "", Boolean.TRUE);
			
			}
		} else {
			Collection<ChamadoTituloAnalise> listaInserida = new ArrayList<ChamadoTituloAnalise>();
			for (TitulosGSAVO tituloAnalise : listaTitulosAnaliseSessao) {

				ChamadoTituloAnalise chamadoTituloAnalise = listaChamadoTituloAnalise.stream()
						.filter(tituloAberto -> tituloAberto.getNumeroTitulo().equals(tituloAnalise.getSequencialTitulo()) && tituloAberto.getEmAnalise())
						.findFirst().orElse(null);

				if (chamadoTituloAnalise == null) {
					chamadoTituloAnalise = new ChamadoTituloAnalise();
					chamadoTituloAnalise.setChamado(chamadoExistente);
					chamadoTituloAnalise.setUsuario(usuario);
					chamadoTituloAnalise.setNumeroTitulo(tituloAnalise.getSequencialTitulo());
					chamadoTituloAnalise.setEmAnalise(Boolean.TRUE);

					repositorioChamado.inserir(chamadoTituloAnalise);
					repositorioChamado.incluirTituloAnaliseGSA(chamadoTituloAnalise.getNumeroTitulo(), usuario.getFuncionario().getNome(), "Protocolo Nº " + numeroProtocolo, Boolean.FALSE);

				} 
				if(chamadoTituloAnalise != null) {
					listaInserida.add(chamadoTituloAnalise);
				}
			}
			
			Set<String> titulosEmAnalise = listaInserida.stream().map(ChamadoTituloAnalise::getNumeroTitulo)
					.collect(Collectors.toSet());

			listaChamadoTituloAnalise.removeIf(titulo -> titulosEmAnalise.contains(titulo.getNumeroTitulo()));
			
			for(ChamadoTituloAnalise chamadoTituloAnalise : listaChamadoTituloAnalise) {
				chamadoTituloAnalise.setEmAnalise(Boolean.FALSE);
				
				repositorioChamado.atualizar(chamadoTituloAnalise, ChamadoTituloAnalise.class);
				repositorioChamado.incluirTituloAnaliseGSA(chamadoTituloAnalise.getNumeroTitulo(), usuario.getFuncionario().getNome(), "", Boolean.TRUE);

			}
		}
	}

	@Override
	public Collection<ChamadoResumoVO> consultarChamadoResumo(ChamadoVO chamadoVO, Boolean isRelatorio, Boolean isGrafico) throws GGASException {
		return repositorioChamado.consultarChamadoResumo(chamadoVO, isRelatorio, isGrafico);
	}
}
