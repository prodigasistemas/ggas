/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.chamado.dominio;

import br.com.ggas.web.medicao.associacao.AssociacaoAction;

/**
 * Classe responsável pela representação de valores referentes a
 * funcionalidade Associar Ponto de Consumo ao Medidor / Corretor de
 * Vazão
 * 
 * @author bruno silva
 * 
 * @see AssociacaoAction
 *
 */
public class AssociacaoVO {

	private String lacre;
	private String status;
	private String medidor;
	private String modoUso;
	private String dataMedidor;
	private String numeroSerie;
	private String leituraAtual;
	private String corretorVazao;
	private String numeroMedidor;
	private String leituraAnterior;
	private String tipoMedidorAtual;
	private String marcaMedidorAtual;
	private String horaCorretorVazao;
	private String dataCorretorVazao;
	private String indicadorPesquisa;
	private String modeloMedidorAtual;
	private String numeroCorretorVazao;
	private String marcaCorretorVazaoAtual;
	private String modeloCorretorVazaoAtual;
	private String numeroDigitosMedidorAtual;
	private String leituraAtualCorretorVazao;
	private String leituraAnteriorCorretorVazao;
	private String numeroSerieCorretorVazaoAtual;
	private String numeroDigitosCorretorVazaoAtual;
	private String[] leituraLote;
	private String lacreDois;
	private String lacreTres;

	private Boolean exibirDadosFiltro;
	private Boolean contemCorretorVazao = Boolean.FALSE;
	private Boolean instalarCorretorVazao;
	private Boolean substituirCorretorVazao;
	private Boolean habilitado = Boolean.TRUE;
	private Boolean isAssociacaoMedidorIndependente;
	private Boolean indicadorAssociacaoEmLote;
	
	private Long idTipo;
	private Long idMarca;
	private Long idImovel;
	private Long idModelo;
	private Long idCliente;
	private Long funcionario;
	private Long idPontoConsumo;
	private Long medidorProtecao;
	private Long chaveMedidorAtual;
	private Long idOperacaoMedidor;
	private Long chaveMedidorAnterior;
	private Long medidorMotivoOperacao;
	private Long idMedidorIndependente;
	private Long localInstalacaoMedidor;
	private Long chaveCorretorVazaoAtual;
	private Long funcionarioCorretorVazao;
	private Long chaveCorretorVazaoAnterior;
	private Long localInstalacaoCorretorVazao;
	private Long idOperacaoMedidorIndependente;
	
	private Long[] listaChavePrimaria;
	private Long[] idPontoConsumoLote;
	private Long[] chaveMedidorLote;
	private Long[] chaveCorretorVazaoLote;
	
	private Integer tipoAssociacao;
	private Long pressaoFornecimentoCampo;
	private boolean isSalvarPressao;

	
	public String getLacre() {
		return lacre;
	}

	public void setLacre(String lacre) {
		this.lacre = lacre;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMedidor() {
		return medidor;
	}

	public void setMedidor(String medidor) {
		this.medidor = medidor;
	}

	public String getModoUso() {
		return modoUso;
	}

	public void setModoUso(String modoUso) {
		this.modoUso = modoUso;
	}

	public String getDataMedidor() {
		return dataMedidor;
	}

	public void setDataMedidor(String dataMedidor) {
		this.dataMedidor = dataMedidor;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getLeituraAtual() {
		return leituraAtual;
	}

	public void setLeituraAtual(String leituraAtual) {
		this.leituraAtual = leituraAtual;
	}
	
	public String getCorretorVazao() {
		return corretorVazao;
	}

	public void setCorretorVazao(String corretorVazao) {
		this.corretorVazao = corretorVazao;
	}

	public String getNumeroMedidor() {
		return numeroMedidor;
	}

	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}

	public String getLeituraAnterior() {
		return leituraAnterior;
	}

	public void setLeituraAnterior(String leituraAnterior) {
		this.leituraAnterior = leituraAnterior;
	}
	

	public String getTipoMedidorAtual() {
		return tipoMedidorAtual;
	}

	public void setTipoMedidorAtual(String tipoMedidorAtual) {
		this.tipoMedidorAtual = tipoMedidorAtual;
	}
	

	public String getMarcaMedidorAtual() {
		return marcaMedidorAtual;
	}

	public void setMarcaMedidorAtual(String marcaMedidorAtual) {
		this.marcaMedidorAtual = marcaMedidorAtual;
	}

	public String getHoraCorretorVazao() {
		return horaCorretorVazao;
	}

	public void setHoraCorretorVazao(String horaCorretorVazao) {
		this.horaCorretorVazao = horaCorretorVazao;
	}

	public String getDataCorretorVazao() {
		return dataCorretorVazao;
	}

	public void setDataCorretorVazao(String dataCorretorVazao) {
		this.dataCorretorVazao = dataCorretorVazao;
	}

	public String getIndicadorPesquisa() {
		return indicadorPesquisa;
	}

	public void setIndicadorPesquisa(String indicadorPesquisa) {
		this.indicadorPesquisa = indicadorPesquisa;
	}

	public String getModeloMedidorAtual() {
		return modeloMedidorAtual;
	}

	public void setModeloMedidorAtual(String modeloMedidorAtual) {
		this.modeloMedidorAtual = modeloMedidorAtual;
	}

	public String getNumeroCorretorVazao() {
		return numeroCorretorVazao;
	}

	public void setNumeroCorretorVazao(String numeroCorretorVazao) {
		this.numeroCorretorVazao = numeroCorretorVazao;
	}
	
	public String getMarcaCorretorVazaoAtual() {
		return marcaCorretorVazaoAtual;
	}

	public void setMarcaCorretorVazaoAtual(String marcaCorretorVazaoAtual) {
		this.marcaCorretorVazaoAtual = marcaCorretorVazaoAtual;
	}
	
	public String getModeloCorretorVazaoAtual() {
		return modeloCorretorVazaoAtual;
	}

	public void setModeloCorretorVazaoAtual(String modeloCorretorVazaoAtual) {
		this.modeloCorretorVazaoAtual = modeloCorretorVazaoAtual;
	}

	public String getNumeroDigitosMedidorAtual() {
		return numeroDigitosMedidorAtual;
	}

	public void setNumeroDigitosMedidorAtual(String numeroDigitosMedidorAtual) {
		this.numeroDigitosMedidorAtual = numeroDigitosMedidorAtual;
	}

	public String getLeituraAtualCorretorVazao() {
		return leituraAtualCorretorVazao;
	}

	public void setLeituraAtualCorretorVazao(String leituraAtualCorretorVazao) {
		this.leituraAtualCorretorVazao = leituraAtualCorretorVazao;
	}

	public String getLeituraAnteriorCorretorVazao() {
		return leituraAnteriorCorretorVazao;
	}

	public void setLeituraAnteriorCorretorVazao(String leituraAnteriorCorretorVazao) {
		this.leituraAnteriorCorretorVazao = leituraAnteriorCorretorVazao;
	}
	
	public String getNumeroSerieCorretorVazaoAtual() {
		return numeroSerieCorretorVazaoAtual;
	}

	public void setNumeroSerieCorretorVazaoAtual(String numeroSerieCorretorVazaoAtual) {
		this.numeroSerieCorretorVazaoAtual = numeroSerieCorretorVazaoAtual;
	}

	public String getNumeroDigitosCorretorVazaoAtual() {
		return numeroDigitosCorretorVazaoAtual;
	}

	public void setNumeroDigitosCorretorVazaoAtual(String numeroDigitosCorretorVazaoAtual) {
		this.numeroDigitosCorretorVazaoAtual = numeroDigitosCorretorVazaoAtual;
	}
	
	

	public String getLacreDois() {
		return lacreDois;
	}

	public void setLacreDois(String lacreDois) {
		this.lacreDois = lacreDois;
	}

	public String getLacreTres() {
		return lacreTres;
	}

	public void setLacreTres(String lacreTres) {
		this.lacreTres = lacreTres;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	public Boolean getExibirDadosFiltro() {
		return exibirDadosFiltro;
	}

	public void setExibirDadosFiltro(Boolean exibirDadosFiltro) {
		this.exibirDadosFiltro = exibirDadosFiltro;
	}

	public Boolean getContemCorretorVazao() {
		return contemCorretorVazao;
	}

	public void setContemCorretorVazao(Boolean contemCorretorVazao) {
		this.contemCorretorVazao = contemCorretorVazao;
	}

	public Boolean getInstalarCorretorVazao() {
		return instalarCorretorVazao;
	}

	public void setInstalarCorretorVazao(Boolean instalarCorretorVazao) {
		this.instalarCorretorVazao = instalarCorretorVazao;
	}

	public Boolean getSubstituirCorretorVazao() {
		return substituirCorretorVazao;
	}

	public void setSubstituirCorretorVazao(Boolean substituirCorretorVazao) {
		this.substituirCorretorVazao = substituirCorretorVazao;
	}

	public Boolean getIsAssociacaoMedidorIndependente() {
		return isAssociacaoMedidorIndependente;
	}

	public void setIsAssociacaoMedidorIndependente(Boolean isAssociacaoMedidorIndependente) {
		this.isAssociacaoMedidorIndependente = isAssociacaoMedidorIndependente;
	}

	public Boolean getIndicadorAssociacaoEmLote() {
		return indicadorAssociacaoEmLote;
	}

	public void setIndicadorAssociacaoEmLote(Boolean indicadorAssociacaoEmLote) {
		this.indicadorAssociacaoEmLote = indicadorAssociacaoEmLote;
	}

	public Long getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}

	public Long getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Long idMarca) {
		this.idMarca = idMarca;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public Long getIdModelo() {
		return idModelo;
	}

	public void setIdModelo(Long idModelo) {
		this.idModelo = idModelo;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Long funcionario) {
		this.funcionario = funcionario;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public Long getMedidorProtecao() {
		return medidorProtecao;
	}

	public void setMedidorProtecao(Long medidorProtecao) {
		this.medidorProtecao = medidorProtecao;
	}

	public Long getChaveMedidorAtual() {
		return chaveMedidorAtual;
	}

	public void setChaveMedidorAtual(Long chaveMedidorAtual) {
		this.chaveMedidorAtual = chaveMedidorAtual;
	}

	public Long getIdOperacaoMedidor() {
		return idOperacaoMedidor;
	}

	public void setIdOperacaoMedidor(Long idOperacaoMedidor) {
		this.idOperacaoMedidor = idOperacaoMedidor;
	}

	public Long getChaveMedidorAnterior() {
		return chaveMedidorAnterior;
	}

	public void setChaveMedidorAnterior(Long chaveMedidorAnterior) {
		this.chaveMedidorAnterior = chaveMedidorAnterior;
	}

	public Long getMedidorMotivoOperacao() {
		return medidorMotivoOperacao;
	}

	public void setMedidorMotivoOperacao(Long medidorMotivoOperacao) {
		this.medidorMotivoOperacao = medidorMotivoOperacao;
	}

	public Long getIdMedidorIndependente() {
		return idMedidorIndependente;
	}

	public void setIdMedidorIndependente(Long idMedidorIndependente) {
		this.idMedidorIndependente = idMedidorIndependente;
	}

	public Long getLocalInstalacaoMedidor() {
		return localInstalacaoMedidor;
	}

	public void setLocalInstalacaoMedidor(Long localInstalacaoMedidor) {
		this.localInstalacaoMedidor = localInstalacaoMedidor;
	}

	public Long getChaveCorretorVazaoAtual() {
		return chaveCorretorVazaoAtual;
	}

	public void setChaveCorretorVazaoAtual(Long chaveCorretorVazaoAtual) {
		this.chaveCorretorVazaoAtual = chaveCorretorVazaoAtual;
	}

	public Long getFuncionarioCorretorVazao() {
		return funcionarioCorretorVazao;
	}

	public void setFuncionarioCorretorVazao(Long funcionarioCorretorVazao) {
		this.funcionarioCorretorVazao = funcionarioCorretorVazao;
	}

	public Long getChaveCorretorVazaoAnterior() {
		return chaveCorretorVazaoAnterior;
	}

	public void setChaveCorretorVazaoAnterior(Long chaveCorretorVazaoAnterior) {
		this.chaveCorretorVazaoAnterior = chaveCorretorVazaoAnterior;
	}

	public Long getLocalInstalacaoCorretorVazao() {
		return localInstalacaoCorretorVazao;
	}

	public void setLocalInstalacaoCorretorVazao(Long localInstalacaoCorretorVazao) {
		this.localInstalacaoCorretorVazao = localInstalacaoCorretorVazao;
	}

	public Long getIdOperacaoMedidorIndependente() {
		return idOperacaoMedidorIndependente;
	}

	public void setIdOperacaoMedidorIndependente(Long idOperacaoMedidorIndependente) {
		this.idOperacaoMedidorIndependente = idOperacaoMedidorIndependente;
	}

	public Long[] getListaChavePrimaria() {
		return listaChavePrimaria.clone();
	}

	public void setListaChavePrimaria(Long[] listaChavePrimaria) {
		this.listaChavePrimaria = associacaoValidarClonar(listaChavePrimaria);
	}

	public Long[] getIdPontoConsumoLote() {
		return idPontoConsumoLote.clone();
	}

	public void setIdPontoConsumoLote(Long[] idPontoConsumoLote) {
		this.idPontoConsumoLote = associacaoValidarClonar(idPontoConsumoLote);
	}

	public String[] getLeituraLote() {
		return leituraLote.clone();
	}

	public void setLeituraLote(String[] leituraLote) {
		if(leituraLote != null) {
			this.leituraLote = leituraLote.clone();
		} else {
			this.leituraLote = null;
		}
	}

	public Long[] getChaveMedidorLote() {
		return chaveMedidorLote.clone();
	}

	public void setChaveMedidorLote(Long[] chaveMedidorLote) {
		this.chaveMedidorLote = associacaoValidarClonar(chaveMedidorLote);
	}

	public Long[] getChaveCorretorVazaoLote() {
		return chaveCorretorVazaoLote.clone();
	}

	public void setChaveCorretorVazaoLote(Long[] chaveCorretorVazaoLote) {
		this.chaveCorretorVazaoLote = associacaoValidarClonar(chaveCorretorVazaoLote);
	}

	public Integer getTipoAssociacao() {
		return tipoAssociacao;
	}

	public void setTipoAssociacao(Integer tipoAssociacao) {
		this.tipoAssociacao = tipoAssociacao;
	}

	public Long getPressaoFornecimentoCampo() {
		return pressaoFornecimentoCampo;
	}

	public void setPressaoFornecimentoCampo(Long pressaoFornecimentoCampo) {
		this.pressaoFornecimentoCampo = pressaoFornecimentoCampo;
	}

	public boolean isSalvarPressao() {
		return isSalvarPressao;
	}

	public void setSalvarPressao(boolean isSalvarPressao) {
		this.isSalvarPressao = isSalvarPressao;
	}
	
	/**
	 * Método responsável em verificar se uma variável é nula, retornando uma cópia da mesma.
	 * 
	 * @param tmp - {@link Long}
	 * @return tmp variável temporária. - {@link Long}
	 */
	private Long[] associacaoValidarClonar(Long[] tmp) {
		if (tmp != null) {
			return tmp.clone();
		} else {
			return null;
		}
	}

}
