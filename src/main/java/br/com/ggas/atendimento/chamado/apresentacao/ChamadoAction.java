/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *16/10/2013
 * vpessoa
 * 20:56:15
 */

package br.com.ggas.atendimento.chamado.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
import static br.com.ggas.util.Constantes.TRUE;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoAlteracaoVencimento;
import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamado.dominio.ChamadoEmail;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistoricoAnexo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoTituloAnalise;
import br.com.ggas.atendimento.chamado.dominio.ChamadoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamado.dominio.ClienteVOAutoComplete;
import br.com.ggas.atendimento.chamado.dominio.DocumentoFiscalVO;
import br.com.ggas.atendimento.chamado.dominio.PesquisarClientePopupVO;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.resposta.negocio.ControladorResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendasVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoCliente;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteContato;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteEndereco;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteTelefone;
import br.com.ggas.cadastro.cliente.dominio.ChamadoClienteAnexo;
import br.com.ggas.cadastro.cliente.impl.ClienteAnexoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteFoneImpl;
import br.com.ggas.cadastro.cliente.impl.ContatoClienteImpl;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoItemFaturamentoImpl;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.DocumentoCobrancaVO;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.impl.OrdenacaoDataEmissaoFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioChamado;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.StringUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.AjaxService;
import br.com.ggas.web.cobranca.extratodebito.ExtratoDebitoFaturaVO;
import br.com.ggas.web.cobranca.extratodebito.ExtratoDebitoVO;
import br.com.ggas.web.contrato.contrato.ContratoVO;
import br.com.ggas.web.faturamento.fatura.FaturaPesquisaVO;
import br.com.ggas.web.faturamento.fatura.FaturaVO;
import br.com.ggas.web.faturamento.fatura.TitulosGSAVO;
import br.com.ggas.web.medicao.consumo.HistoricoConsumoVO;
import br.com.ggas.web.medicao.leitura.HistoricoLeituraGSAVO;
import br.com.ggas.web.medicao.leitura.HistoricoMedicaoAgrupadoVO;
import br.com.ggas.webservice.PontoConsumoTO;
import br.com.ggas.webservice.Servico;

/**
 *
 * Classe Responsável pelas ações relacionadas à funcionalidade Chamado.
 
 * @author vpessoa
 *
 */
@Controller
@SessionAttributes(ChamadoAction.CHAMADO_VO)
public class ChamadoAction extends GenericAction {

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LISTA_CANAL_ATENDIMENTO = "listaCanalAtendimento";

	private static final String CHAMADOS_TIPO = "chamadosTipo";

	private static final String INDICADOR_UNIDADES_ORGANIZACIONAL_VISUALIZADORA = "indicadorUnidadesOrganizacionalVisualizadora";

	private static final String LISTA_ANEXOS = "listaAnexos";

	private static final String INDEX_LISTA = "indexLista";

	private static final String GRID_CHAMADO_PONTOS_CONSUMO = "gridChamadoPontosConsumo";

	private static final String QUESTIONARIO = "questionario";

	private static final int ANEXO = 3;

	private static final int TAMANHO_CPF = 11;

	private static final int TAMANHO_MAXIMO_ARQUIVO = 10485760;
	private static final String CONST_TAMANHO_MAXIMO_ARQUIVO = "TAMANHO_MAXIMO_ARQUIVO";

	private static final String TELA_GRID_ABA_CHAMADO_CONTATO = "gridAbaChamadoContato";

	private static final String TELA_POPUP_RESPOSTA_QUESTIONARIO = "popupRespostaQuestionario";

	private static final String GRID_ANEXOS = "gridAnexos";

	private static final String TELA_GRID_CHAMADO_EMAIL = "gridChamadoEmail";

	private static final String CHECK_BOX_MARCADOS_LISTA_MAPA = "checkBoxMarcadosListaMapa";

	private static final String MAPA_CAMPOS_LISTA_ALTERADOS_SESSAO = "mapaCamposListaAlteradosSessao";

	private static final String MAPA_CAMPOS_MAPA_LISTA_ANEXO_ALTERADOS_SESSAO = "mapaCamposMapaListaAnexoAlteradosSessao";

	private static final String MAPA_CAMPOS_ALTERADOS_SESSAO = "mapaCamposAlteradosSessao";

	private static final String LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ANEXO = "listaRegistrosExcluidosClienteAnexo";

	private static final String LISTA_REGISTROS_EXCLUIDOS_CONTATO_CLIENTE = "listaRegistrosExcluidosContatoCliente";

	private static final String LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ENDERECO = "listaRegistrosExcluidosClienteEndereco";

	private static final String LISTA_REGISTROS_EXCLUIDOS_CLIENTE_FONE = "listaRegistrosExcluidosClienteFone";

	private static final String TELA_EXIBIR_ALTERACAO_CADASTRO_CLIENTE_CHAMADO = "exibirAlteracaoCadastroClienteChamado";

	private static final String DATA_ENCERRAMENTO = "dataEncerramento";

	private static final String LISTA_PERGUNTAS = "listaPerguntas";

	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	private static final String LISTA_TIPO_CARGO = "listaTipoCargo";

	private static final String AJAX_ERRO = "ajaxErro";

	private static final String AJAX_ALERTA = "ajaxAlerta";

	private static final String DIV_CLIENTE_CHAMADO = "divClienteChamado";

	private static final String RASCUNHO = "rascunho";

	private static final String NUMERO_PROTOCOLO = "numeroProtocolo";

	private static final String CLIENTE = "cliente";

	private static final String TELEFONE = "telefone";

	private static final String CHAVE_ASSUNTO = "chaveAssunto";

	private static final String CHAMADO_TIPO = "chamadoTipo";

	private static final String FLUXO_ALTERACAO = "fluxoAlteracao";

	private static final String ERRO_ENCERRAMENTO = "erroEncerramento";

	private static final String LISTA_CHAMADO_ASSUNTO = "listaChamadoAssunto";

	private static final String LISTA_CHAMADO_TIPO = "listaChamadoTipo";

	private static final String LISTA_UNIDADE_ORGANIZACIONAL = "listaUnidadeOrganizacional";

	private static final String FLUXO_INCLUSAO = "fluxoInclusao";

	private static final String FLUXO_ALTERACAO_RASCUNHO = "fluxoAlteracaoRascunho";

	private static final String UNIDADE = "unidade";

	private static final String LISTA_USUARIO_RESPONSAVEL = "listaUsuarioResponsavel";

	private static final String ID_MOTIVO = "idMotivo";

	private static final String FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO = "forward:/exibirDetalhamentoChamado";

	private static final String LISTA_MOTIVOS = "listaMotivos";

	private static final String TELA_EXIBIR_INCLUSAO_CHAMADO = "exibirInclusaoChamado";

	private static final String CHAMADO = "chamado";

	private static final String LISTA_CHAMADO_CONTATO = "listaChamadoContato";

	private static final String LISTA_CHAMADO_EMAIL = "listaChamadoEmail";

	private static final String LISTA_IMOVEIS_CHAMADO = "listaImoveisChamado";
	
	private static final String LISTA_CEPS = "listaCeps";
	
	private static final String LISTA_CHAMADO_CEPS = "listaChamadoCeps";

	private static final String CONDOMINIO_IMOVEL = "condominioImovel";

	public static final String CHAMADO_VO = "chamadoVO";

	private static final String TELA_EXIBIR_PESQUISA_CHAMADO = "exibirPesquisaChamado";

	private static final Logger LOG = Logger.getLogger(ChamadoAction.class);

	private static final String CEP_IMOVEL = "cepImovel";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String NOME = "nome";

	private static final String ATIVIDADES_ECONOMICAS = "atividadesEconomicas";

	private static final String NACIONALIDADES = "listaNacionalidades";

	private static final String FAIXAS_RENDA = "faixasRenda";

	private static final String PROFISSOES = "profissoes";

	private static final String UNIDADES_FEDERACAO = "unidadesFederacao";

	private static final String ORGAOS_EMISSORES = "orgaosEmissores";

	private static final String SEXOS = "sexos";

	private static final String TIPO_PESSOA_FISICA = "tipoPessoaFisica";

	private static final String SITUACOES_CLIENTES = "situacoesClientes";

	private static final String TIPOS_CLIENTES = "tiposClientes";

	private static final String TIPO_ENDERECO = "listaTiposEndereco";

	private static final String LISTA_TIPO_CONTATO = "listaTipoContato";

	private static final String TIPO_PROFISSAO = "listaProfissao";

	private static final String MASCARA_INSCRICAO_ESTADUAL = "mascaraInscricaoEstadual";

	private static final String PERIODO_GERACAO = "Período de Geração";

	private static final String PERIODO_RESOLUCAO = "Período de Resolução";

	private static final String PERIODO_ENCERRAMENTO = "Período de Encerramento";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String LISTA_CATEGORIA = "listaCategoria";

	private static final String HABILITADO = "habilitado";

	private static final String[] PROPRIEDADES_LAZY = new String[] {"fones", "contatos", "enderecos", "anexos"};

	private static final String[] extensao =
			{ ".PDF", ".TXT", ".DOC", ".DOCX", ".XLS", ".XLSX", ".PNG", ".JPG", ".JPEG", ".BMP", ".PPT", ".PPTX" };

	private static final String PONTOS_CONSUMO = "pontosConsumo";

	private static final String GRID_IMOVEIS = "gridImoveis";
	private static final String GRID_IMOVEIS_BOOTSTRAP = "gridImoveisBootstrap";

	private static final String LISTA_TIPOS_CONTATO = "listaTiposContato";

	private static final String EQUIPE = "equipe";

	private static final String HABILITA_ACIONAMENTO = "habilitaAcionamento";

	private static final String INFORMACAO_ADICIONAL = "informacaoAdicional";

	private static final String CHAVE_PRIMARIA_CLIENTE = "chavePrimariaCliente";
	private static final String CHAVE_PRIMARIA_IMOVEL = "chavePrimariaImovel";
	public static final String LISTA_CHAMADO_STATUS = "listaChamadoStatus";
	public static final String LISTA_TIPO_SERVICO = "listaTipoServico";
	private static final String LISTA_CHAMADOS_ENCERRAR_LOTE = "chamados";

	
	private static final String POPUP_EXIBIR_INFORMACOES_CONSOLIDADAS = "popupExibirInformacoesConsolidadas";
	private static final String LISTA_DOC_FISCAL = "listaDocFiscal";
	private static final String VALOR_TOTAL_ATUALIZADO = "valorTotalAtualizadoFaturamento";
	private static final String LISTA_DOCUMENTO_COBRANCA = "listaDocCobranca";
	private static final String VALOR_TOTAL_ATUALIZADO_COBRANCA = "valorTotalAtualizadoCobranca";
	private static final String LISTA_SERV_AUTORIZ_AGENDAS = "servAutorizAgendas";

	public static final String CHAVE_PRIMARIA_DOC_FISCAL = "idDocumentoFiscal";
	public static final String CHAVE_PRIMARIA_DOC_COBR = "idDocumentoCobranca";
	public static final String CHAVE_PRIMARIA_FATURA = "idFatura";
	public static final String EMAIL = "email";

	private static final String SCALA_CONSUMO_APURADO = "escalaConsumoApurado";
	
	private static final Integer SCALA_NUMERO_DOIS = 2;
	private static final Integer QUANTIDADE_MES_COMERCIAL = 30;

	private static final String IS_ENVIADO = "isEmailEnviado";

	private static final String ERRO_NEGOCIO_DATAS_NAO_PREENCHIDAS = "ERRO_NEGOCIO_DATAS_NAO_PREENCHIDAS";

	private Long contadorAuxiliar = Long.valueOf(-1);
	
	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";
	
	public static final String ASSUNTO_EMAIL_FATURA = "Fatura Online";
	
	public static final String NOME_ANEXO_EMAIL_FATURA = "Fatura Nº ";
	
	public static final String TELA_GRID_ENDERECO = "gridEnderecoBootstrap";
	
	public static final String GRID_CHAMADO_ENDERECO = "gridChamadoEndereco";

	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();

	/**
	 * Controlador chamado
	 */
	@Autowired
	private ControladorChamado controladorChamado;

	/**
	 * Controlador de segmento
	 */
	@Autowired
	private ControladorSegmento controladorSegmento;

	/**
	 * Controlador Tipo de Chamado
	 */
	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	/**
	 * Controlador Assunto Chamado
	 */
	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	/**
	 * Controlador Servico tipo
	 */
	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	/**
	 * Controlador Questionario
	 */
	@Autowired
	private ControladorQuestionario controladorQuestionario;

	/**
	 * Controlador entidade conteudo
	 */
	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Controlador Unidade Organizacional
	 */
	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	/**
	 * Controlador Funcionario
	 */
	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	/**
	 * controlador Imovel
	 */
	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;

	/**
	 * Controlador parametros sistema
	 */
	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	/**
	 * Controlador cliente
	 */
	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	/**
	 * Controlador usuario
	 */
	@Autowired
	@Qualifier("controladorUsuario")
	private ControladorUsuario controladorUsuario;

	/**
	 * Controlador ponto consumo
	 */
	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Controlador contrato
	 */
	@Autowired
	@Qualifier("controladorContrato")
	private ControladorContrato controladorContrato;

	/**
	 * Controlador constante sistema
	 */
	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Controlador relatorio chamado
	 */
	@Autowired
	private ControladorRelatorioChamado controladorRelatorioChamado;

	/**
	 * Controlador fatura
	 */
	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;

	/**
	 * Controlador medidor
	 */
	@Autowired
	@Qualifier("controladorMedidor")
	private ControladorMedidor controladorMedidor;

	/**
	 * Controlador vazao corretor
	 */
	@Autowired
	@Qualifier("controladorVazaoCorretor")
	private ControladorVazaoCorretor controladorVazaoCorretor;

	/**
	 * Controlador historico consumo
	 */
	@Autowired
	@Qualifier("controladorHistoricoConsumo")
	private ControladorHistoricoConsumo controladorHistoricoConsumo;

	/**
	 * Controlador cobranca
	 */
	@Autowired
	@Qualifier("controladorCobranca")
	private ControladorCobranca controladorCobranca;

	/**
	 * Controlador autorizacao servico
	 */
	@Autowired
	@Qualifier("controladorServicoAutorizacao")
	private ControladorServicoAutorizacao controladorServicoAutorizacao;

	/**
	 * Controlador resposta
	 */
	@Autowired
	private ControladorResposta controladorResposta;

	/**
	 * Controlador endereco
	 */
	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	/**
	 * Controlador municipio
	 */
	@Autowired
	@Qualifier("controladorMunicipio")
	private ControladorMunicipio controladorMunicipio;

	/**
	 * Controlador arrecadador convenio
	 */
	@Autowired
	@Qualifier("controladorArrecadadorConvenio")
	private ControladorArrecadadorConvenio controladorArrecadadorConvenio;
	
	/**
	 * Controlador banco
	 */
	@Autowired
	@Qualifier("controladorBanco")
	private ControladorBanco controladorBanco;
	
	/**
	 * Controlador historico medicao
	 */
	@Autowired
	@Qualifier("controladorHistoricoMedicao")
	private ControladorHistoricoMedicao controladorHistoricoMedicao;
	
	
	/**
	 * Controlador equipe
	 */
	@Autowired
	@Qualifier("controladorEquipe")
	private ControladorEquipe controladorEquipe;	

	/**
	 * Exibir pesquisa chamado.
	 *
	 *
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException exceção lançada quando ocorre falha ao listar conteudo de entidade
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_CHAMADO)
	public ModelAndView exibirPesquisaChamado(HttpServletRequest request) throws GGASException {
		Log.info("Inicio exibirPesquisaChamado");
		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CHAMADO);

		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);

		ChamadoVO chamadoVO = new ChamadoVO();

		if (usuario.getFuncionario().getUnidadeOrganizacional() != null) {
			chamadoVO.setIdUnidadeOrganizacionalUsuario(usuario.getFuncionario().getUnidadeOrganizacional().getChavePrimaria());
		}

		chamadoVO.setCondominioImovel(null);
		chamadoVO.setListaStatus(new ArrayList<String>());
		chamadoVO.getListaStatus().add("Aberto");
		chamadoVO.getListaStatus().add("Em atendimento");
		model.addObject(CHAMADO_VO, chamadoVO);
		model.addObject(CONDOMINIO_IMOVEL, "");

		ConstanteSistema constanteSistema =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_ALT_PRAZO_ENCERRAMENTO);
		model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		model.addObject("isAdministradorAtendimento", usuario.isAdministradorAtendimento());

		carregarCombos(model);
		removerDadosSessao(request);

		Log.info("Fim exibirPesquisaChamado");
		return model;
	}

	/**
	 *
	 *
	 * Exibir detalhamento chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param popUp
	 *            the popUp
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException
	 *             the parse exception
	 */
	@RequestMapping("exibirDetalhamentoChamado")
	public ModelAndView exibirDetalhamentoChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			@RequestParam(required = false, value = "popUp") String... popUp) throws GGASException {
		
		Log.info("Inicio exibirDetalhamentoChamado");
		ModelAndView model = null;
		model = new ModelAndView("exibirDetalhamentoChamado");
		if (popUp != null && popUp.length != 0 && "yes".equals(popUp[0]) && popUp.length > 0) {
			model = new ModelAndView("popUpExibirDetalhamentoChamado");
		}
		exibirChamado(model, chavePrimaria, request);
		model.addObject("fluxoDetalhamento", Boolean.TRUE);
		Chamado chamado = (Chamado) model.getModel().get(CHAMADO);

		carregarAutorizacaoServicoHistorico(model, chamado);
		verificarTipoChamado(model, chamado);

		Boolean exibeAlerta = (Boolean) request.getAttribute(Constantes.ERRO_NEGOCIO_AUTORIZACAO_NAO_GERADA_AUTOMATICAMENTE);
		Boolean exibeAlertaASDuplicada = (Boolean) request.getAttribute(Constantes.ERRO_SERVICO_AUTORIZACAO_DUPLICADO_CHAMADO);
		if (exibeAlertaASDuplicada != null && exibeAlertaASDuplicada) {
			mensagemAlerta(model, Constantes.ERRO_SERVICO_AUTORIZACAO_DUPLICADO_CHAMADO,
					String.valueOf(chamado.getProtocolo().getNumeroProtocolo()));
		} else if (exibeAlerta != null && exibeAlerta) {
			mensagemAlerta(model, Constantes.ERRO_NEGOCIO_AUTORIZACAO_NAO_GERADA_AUTOMATICAMENTE,
					String.valueOf(chamado.getProtocolo().getNumeroProtocolo()));
		}
		
		if(chamado.getChamadoTituloAnalise() != null && !chamado.getChamadoTituloAnalise().isEmpty()) {

			String[] numerosTitulos = chamado.getChamadoTituloAnalise().stream()
					.map(ChamadoTituloAnalise::getNumeroTitulo).toArray(String[]::new);
			
			Collection<TitulosGSAVO> listaTitulosAnalise = controladorFatura.obterTitulosPorNumero(numerosTitulos);
			
			model.addObject("listaTitulosAnalise", listaTitulosAnalise);
		}
		
		request.getSession().removeAttribute("listaTitulosAbertosSessao");
		request.getSession().removeAttribute("listaTitulosAnalise");
		Log.info("Fim exibirDetalhamentoChamado");
		return model;
	}

	/**
	 *
	 *
	 * Verificar tipo chamado.
	 *
	 * @param model
	 *            the model
	 * @param chamado
	 *            the chamado
	 */
	private void verificarTipoChamado(ModelAndView model, Chamado chamado) {

		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_PESQUISA);

		if(chamado.getChamadoAssunto().getChavePrimaria() == Long.parseLong(constante.getValor())) {
			model.addObject("indicadorChamadoPesquisa", true);
		} else {
			model.addObject("indicadorChamadoPesquisa", false);
		}

		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO);
		if(chamado.getChamadoAssunto().getChavePrimaria() == Long.parseLong(constante.getValor())) {
			model.addObject("indicadorChamadoAlteracaoCadastral", true);
		}

		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_AGV_ALTERAR_VENCIMENTO);
		if(chamado.getChamadoAssunto().getChavePrimaria() == Long.parseLong(constante.getValor())) {
			model.addObject("indicadorChamadoAlteracaoDataVencimento", true);
		}
	}

	/**
	 *
	 *
	 * Carregar ponto consumo chamado.
	 *
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarPontoConsumoChamado(Long chavePontoConsumo, ModelAndView model) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(CHAVE_PRIMARIA, chavePontoConsumo);
		List<PontoConsumo> pontosConsumo = (List<PontoConsumo>) controladorPontoConsumo.consultarPontosConsumo(filtro);
		model.addObject(PONTOS_CONSUMO, pontosConsumo);

	}

	/**
	 *
	 *
	 * Exibir reativacao chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirReativacaoChamado")
	public ModelAndView exibirReativacaoChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		exibirChamado(model, chavePrimaria, request);
		Chamado chamado = controladorChamado.obterChamado(chavePrimaria);

		try {
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_REATIVACAO);

			chamado.setDescricao("");

			model.addObject(CHAMADO, chamado);
			model.addObject("fluxoReativacao", Boolean.TRUE);
			ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_REATIVACAO);
			model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		adicionarParametroCaixaAlta(request);
		return model;
	}

	/**
	 *
	 *
	 * Reativar chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("reativarChamado")
	public ModelAndView reativarChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			controladorChamado.reativarChamado(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_REATIVADO, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch (NegocioException e) {
			model = exibirReativacaoChamado(chamado.getChavePrimaria(), request);
			model.addObject(ID_MOTIVO, request.getParameter(ID_MOTIVO));
			model.addObject(CHAMADO, chamado);
			carregarListaAnexos(model, request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 *
	 *
	 * Exibir reabertura chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirReaberturaChamado")
	public ModelAndView exibirReaberturaChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		exibirChamado(model, chavePrimaria, request);
		Chamado chamado = controladorChamado.obterChamado(chavePrimaria);

		try {
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_REABERTURA);

			chamado.setDescricao("");

			model.addObject(CHAMADO, chamado);
			model.addObject("fluxoReabertura", Boolean.TRUE);

			ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_REABERTURA);
			model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}


		adicionarParametroCaixaAlta(request);
		return model;
	}

	/**
	 * Reabrir chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("reabrirChamado")
	public ModelAndView reabrirChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			controladorChamado.reabrirChamado(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_REABERTO, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(NegocioException e) {
			model = exibirReaberturaChamado(chamado.getChavePrimaria(), request);
			model.addObject(ID_MOTIVO, request.getParameter(ID_MOTIVO));
			model.addObject(CHAMADO, chamado);
			carregaCombos(model);
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir reiteracao chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirReiteracaoChamado")
	public ModelAndView exibirReiteracaoChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		exibirChamado(model, chavePrimaria, request);
		Chamado chamado = controladorChamado.obterChamado(chavePrimaria);

		try {
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_REITERACAO);

			chamado.setDescricao("");

			carregarAutorizacaoServicoHistorico(model, chamado);

			model.addObject(CHAMADO, chamado);

			model.addObject("fluxoReiteracao", Boolean.TRUE);
			ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_REITERACAO);
			model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		adicionarParametroCaixaAlta(request);
		return model;
	}

	/**
	 * Carrega o historico de autorização de serviço do chamado
	 *
	 * @param model
	 * @param chamado
	 * @throws GGASException
	 */
	private void carregarAutorizacaoServicoHistorico(ModelAndView model, Chamado chamado) throws GGASException {
		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
		servicoAutorizacaoVO.setNumeroProtocoloChamado(chamado.getProtocolo().getNumeroProtocolo());
		Collection<ServicoAutorizacao> listaServicoAutorizacao =
				controladorServicoAutorizacao.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE);
		model.addObject("listaServicoAutorizacao", listaServicoAutorizacao);
	}

	/**
	 * Reiterar chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("reiterarChamado")
	public ModelAndView reiterarChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			controladorChamado.reiterarChamado(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_REITERADO, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(GGASException e) {
			model = new ModelAndView("forward:/exibirReiteracaoChamado");
			model.addObject(ID_MOTIVO, request.getParameter(ID_MOTIVO));
			carregaCombos(model);
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir encerramento chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException
	 *             the parse exception
	 */
	@RequestMapping("exibirEncerramentoChamado")
	public ModelAndView exibirEncerramentoChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		Log.info("Inicio exibirEncerramentoChamado");
		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		EntidadeConteudo operacao = null;
		exibirChamado(model, chavePrimaria, request);
		Chamado chamado = (Chamado) model.getModel().get(CHAMADO);

		try {
			controladorChamado.validarAutorizacaoServicoAberta(chamado);
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_ENCERRAMENTO);

			String erroEncerramento = (String) request.getAttribute(ERRO_ENCERRAMENTO);
			if (StringUtils.isEmpty(erroEncerramento)) {
				chamado.setDescricao("");
			} else {
				chamado.setDescricao(erroEncerramento);
			}
			model.addObject(CHAMADO, chamado);

			carregarAutorizacaoServicoHistorico(model, chamado);

			model.addObject("fluxoEncerramento", Boolean.TRUE);
			ConstanteSistema constanteSistema = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_ENCERRAMENTO);
			model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
			Collection<ChamadoHistorico> listaChamadoHistorico = controladorChamado.listarChamadoHistorico(chamado, operacao);

			model.addObject("listaChamadoHistorico", listaChamadoHistorico);

		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);

		}

		adicionarParametroCaixaAlta(request);
		
		Log.info("Fim exibirEncerramentoChamado");
		return model;
	}

	/**
	 * Encerrar chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("encerrarChamado")
	public ModelAndView encerrarChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria) throws GGASException {

		Log.info("Inicio encerrarChamado");
		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			chamado.setDadosAuditoria(getDadosAuditoria(request));
			controladorChamado.encerrarChamado(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(NegocioException e) {
			model = new ModelAndView("forward:/exibirEncerramentoChamado");
			model.addObject(CHAMADO, chamado);
			model.addObject(ERRO_ENCERRAMENTO, chamado.getDescricao());
			model.addObject(ID_MOTIVO, request.getParameter(ID_MOTIVO));
			carregaCombos(model);
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim encerrarChamado");
		return model;
	}

	/**
	 * Exibir tramitacao chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirTramitacaoChamado")
	public ModelAndView exibirTramitacaoChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		try {
			Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
			exibirChamado(model, chavePrimaria, request);
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_TRAMITACAO);
			chamado.setDescricao("");
			carregarAutorizacaoServicoHistorico(model, chamado);
			model.addObject(CHAMADO, chamado);
			model.addObject("fluxoTramitacao", Boolean.TRUE);
			if (chamado.getUnidadeOrganizacional() != null) {
				model.addObject(LISTA_USUARIO_RESPONSAVEL, controladorFuncionario
						.listarFuncionarioPorUnidadeOrganizacional(chamado.getUnidadeOrganizacional().getChavePrimaria()));
			} else {
				model.addObject(LISTA_USUARIO_RESPONSAVEL, controladorFuncionario.listarFuncionarioPorUnidadeOrganizacional(null));
			}
		} catch (NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		adicionarParametroCaixaAlta(request);
		return model;
	}

	/**
	 * Carregar responsavel.
	 *
	 * @param chaveUnidadeOrganizacional
	 *            the chave unidade organizacional
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarResponsavel")
	public ModelAndView carregarResponsavel(@RequestParam("chaveUnidadeOrganizacional") Long chaveUnidadeOrganizacional)
			throws GGASException {

		ModelAndView model = new ModelAndView("divResponsavel");

		if(chaveUnidadeOrganizacional != null) {
			model.addObject(LISTA_USUARIO_RESPONSAVEL,
					controladorFuncionario.listarFuncionarioPorUnidadeOrganizacional(chaveUnidadeOrganizacional));
		} else {
			model.addObject(LISTA_USUARIO_RESPONSAVEL, controladorFuncionario.listarFuncionarioUnidadeOrganizacional());
		}

		return model;
	}

	/**
	 * Tramitar chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("tramitarChamado")
	public ModelAndView tramitarChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request)
			throws GGASException {

		Log.info("Inicio tramitarChamado");
		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		Map<String, Object> dados = new HashMap<>();
		try {
			controladorChamado.verificarPermissaoTramitarUnidadeOrganizacional(chamado.getUnidadeOrganizacional());
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			popularUnidadesOrganizacionaisVisiveis(chamado, request);
			chamado.setDadosAuditoria(getDadosAuditoria(request));
			controladorChamado.tramitarChamado(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_TRAMITADA, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(NegocioException e) {
			model = exibirTramitacaoChamado(chamado.getChavePrimaria(), request);

			verificarUnidadeOrganizacioalVisualizadora(chamado, model);

			model.addObject(LISTA_USUARIO_RESPONSAVEL, controladorFuncionario.listarFuncionarioPorUnidadeOrganizacional(chamado
					.getUnidadeOrganizacional().getChavePrimaria()));
			model.addObject(CHAMADO, chamado);

			carregaCombos(model);
			carregarListaAnexos(model, request);
			model.addObject(UNIDADE, chamado.getUnidadeOrganizacional().getChavePrimaria());
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim tramitarChamado");
		return model;
	}

	/**
	 * Carregar protocolo.
	 *
	 * @return the chamado protocolo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private ChamadoProtocolo carregarProtocolo() throws NegocioException {

		ParametroSistema indicadorProtocolo = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_NUMERO_PROTOCOLO_AUTOMATICO);
		ChamadoProtocolo protocolo = null;
		if("1".equals(indicadorProtocolo.getValor())) {
			protocolo = controladorChamado.gerarProtocolo(null);
		}

		return protocolo;
	}

	/**
	 * Exibir alteracao rascunho chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoRascunhoChamado")
	public ModelAndView exibirAlteracaoRascunhoChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);

		Chamado chamadoNovo = new Chamado();
		chamadoNovo.setChavePrimaria(chamado.getChavePrimaria());
		chamadoNovo.setProtocolo(chamado.getProtocolo());
		chamadoNovo.setVersao(chamado.getVersao());

		removerDadosSessao(request);
		carregaCombos(model);
		model.addObject(CHAMADO, chamadoNovo);
		model.addObject(FLUXO_ALTERACAO_RASCUNHO, Boolean.TRUE);

		return model;
	}

	/**
	 * Exibir inclusao chamado.
	 *
	 * @param acao
	 *            the acao
	 * @param numeroProtocolo
	 *            the numero protocolo
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_CHAMADO)
	public ModelAndView exibirInclusaoChamado(@RequestParam(value = "idPontoConsumo", required = false) String idPontoConsumo, @RequestParam(value = "acao", required = false) String acao,
			@RequestParam(value = "protocolo", required = false) Long numeroProtocolo, @RequestParam(value = "isInclusaoComErro", required = false) Boolean isInclusaoComErro, HttpServletRequest request)
			throws GGASException {

		Log.info("Inicio exibirInclusaoChamado");
		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		
		if(isInclusaoComErro == null || !isInclusaoComErro) {
			request.getSession().removeAttribute("listaTitulosAbertosSessao");
			request.getSession().removeAttribute("listaTitulosAnalise");
		}
		if(idPontoConsumo != null) {
			request.getSession().removeAttribute(LISTA_IMOVEIS_CHAMADO);
			request.getSession().removeAttribute(LISTA_CEPS);
			request.getSession().removeAttribute(LISTA_CHAMADO_CEPS);
			PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(Long.valueOf(idPontoConsumo));
			Collection<Imovel> listaImoveis = new ArrayList<Imovel>();
			listaImoveis.add(pontoConsumo.getImovel());
			request.getSession().setAttribute(LISTA_IMOVEIS_CHAMADO, listaImoveis);
			model.addObject("idImovel", pontoConsumo.getImovel().getChavePrimaria());
		}

		if(acao != null && "Limpar".equals(acao)) {
			removerDadosSessao(request);
		}

		Chamado chamado = new Chamado();
		ChamadoProtocolo protocolo = null;
		if(numeroProtocolo != null && numeroProtocolo != 0) {
			protocolo = controladorChamado.obterProtocolo(numeroProtocolo);
		}

		try {
			Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
			controladorChamado.validarUsuarioAberturaChamado(usuario);
			if(protocolo == null) {
				protocolo = carregarProtocolo();
			}
			chamado.setProtocolo(protocolo);
			model.addObject(CHAMADO, chamado);
			model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
			carregaCombos(model);
		} catch(NegocioException e) {
			model = new ModelAndView(TELA_EXIBIR_PESQUISA_CHAMADO);
			mensagemErroParametrizado(model, e);
		}

		adicionarParametroCaixaAlta(request);
		
		Log.info("Fim exibirInclusaoChamado");
		return model;
	}

	/**
	 * Exibe página de inclusão de chamado em lote
	 * @param request http servlet request
	 * @return retorna página
	 * @throws GGASException 
	 * 		the GGAS exception
	 */
	@RequestMapping("exibirInclusaoChamadoEmLote")
	public ModelAndView exibirInclusaoChamadoEmLote(HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirInclusaoChamadoEmLote");
		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);

		try {
			final Collection<Segmento> segmentos = controladorSegmento.listarSegmento();
			model.addObject("segmentos", segmentos);
			model.addObject(CONST_TAMANHO_MAXIMO_ARQUIVO, TAMANHO_MAXIMO_ARQUIVO);
			model.addObject(CHAMADOS_TIPO, controladorChamadoTipo.listarTodosHabilitado());
			model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
			model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
			controladorChamado.validarUsuarioAberturaChamado(usuario);
		} catch (NegocioException e) {
			LOG.error("Falha não identificada ao abrir inclusao de chamado em lote", e);
			model = new ModelAndView(TELA_EXIBIR_PESQUISA_CHAMADO);
			mensagemErroParametrizado(model, e);
		}
		adicionarParametroCaixaAlta(request);
		return model;
	}

	/**
	 * @param request
	 */
	private void removerDadosSessao(HttpServletRequest request) {
		request.getSession().removeAttribute(LISTA_IMOVEIS_CHAMADO);
		request.getSession().removeAttribute(LISTA_ANEXOS);
		request.getSession().removeAttribute(LISTA_CHAMADO_EMAIL);
		request.getSession().removeAttribute(LISTA_CHAMADO_CONTATO);
		request.getSession().removeAttribute(LISTA_CHAMADOS_ENCERRAR_LOTE);
		request.getSession().removeAttribute(LISTA_CEPS);
		request.getSession().removeAttribute(LISTA_CHAMADO_CEPS);
		request.getSession().removeAttribute("listaTitulosAbertosSessao");
		request.getSession().removeAttribute("listaTitulosAnalise");
	}

	/**
	 * Carrega combos.
	 *
	 * @param model
	 *            the model
	 * @throws GGASException
	 */
	private void carregaCombos(ModelAndView model) throws GGASException {

		Collection<ChamadoTipo> chamadosTipo = controladorChamadoTipo.listarTodosHabilitado();
		model.addObject(CHAMADOS_TIPO, chamadosTipo);
		model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		ConstanteSistema constanteManifestacao = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CLASSE_TIPO_MANIFESTACAO_CHAMADO);
		model.addObject("listaTipoManifestacao",
				controladorEntidadeConteudo.listarEntidadeConteudo(constanteManifestacao.getClasse()));

		model.addObject(LISTA_TIPOS_CONTATO,  fachada.listarTipoContato());
		model.addObject(HABILITA_ACIONAMENTO, false);

		Map<String, Object> filtro = new HashMap<>();

		filtro.put(HABILITADO, Boolean.TRUE);
		model.addObject(LISTA_SEGMENTO, fachada.consultarSegmento(filtro));

		ParametroSistema indicaManifestacao = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.P_CHAMADO_MANIFESTACAO_OBRIGATORIO);
		model.addObject("IndicadorObrigatorio", indicaManifestacao.getValor());
		ParametroSistema exibirPassaporte = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_PASSAPORTE_CLIENTE);
		model.addObject("exibirPassaporte", exibirPassaporte.getValor());
		model.addObject(CONST_TAMANHO_MAXIMO_ARQUIVO, TAMANHO_MAXIMO_ARQUIVO);
	}

	/**
	 * Pesquisar chamado.
	 *
	 * @param chamado the chamado vo
	 * @param result the result
	 * @param condominioImovel the condominio imovel
	 * @param codigoTipoPessoa the codigo tipo pessoa
	 * @param tamanhoPagina the tamanho da pagina
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 * @throws ParseException the parse exception
	 */
	@RequestMapping("pesquisarChamado")
	public ModelAndView pesquisarChamado(@ModelAttribute("ChamadoVO") ChamadoVO chamado, BindingResult result,
			@RequestParam(value = CONDOMINIO_IMOVEL, required = false) String condominioImovel,
			@RequestParam(value = "codigoTipoPessoa", required = false) Integer codigoTipoPessoa,
			@RequestParam(value = "tamanhoPagina", required = false) Integer tamanhoPagina, HttpServletRequest request)
			throws GGASException {
		
		Log.info("Inicio pesquisarChamado");
		ChamadoVO chamadoVO = chamado;
		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CHAMADO);
		
		String fluxoVoltar = chamadoVO.getFluxoVoltar();
		Boolean isFluxoVoltar = Boolean.FALSE;
		if (fluxoVoltar != null && "true".equals(fluxoVoltar)) {
			chamadoVO = (ChamadoVO) request.getSession().getAttribute(CHAMADO_VO);
			isFluxoVoltar = Boolean.TRUE;
		}
		
		String numeroProtocolo = (String) request.getSession().getAttribute(NUMERO_PROTOCOLO);

		if (validarDatasFiltroPreenchidas(chamado) || chamado.getNumeroProtocolo() != null || isFluxoVoltar
				|| (chamado.getCodigoLegado() != null && !chamado.getCodigoLegado().isEmpty())) {			
	
		int tamanhoPaginacao = Constantes.PAGINACAO_PADRAO;

		String[] unidadesVisualizadoras = request.getParameterValues("unidadesVisualizadora");
		if (unidadesVisualizadoras != null) {
			chamadoVO.setUnidadesVisualizadorasIds(Util.arrayStringParaArrayLong(unidadesVisualizadoras));
		}

		if(request.getSession().getAttribute(NUMERO_PROTOCOLO) != null){
			chamadoVO.setNumeroProtocolo((Long) request.getSession().getAttribute(NUMERO_PROTOCOLO));
		}

		if (tamanhoPagina != null) {
			tamanhoPaginacao = tamanhoPagina;
		}

		model.addObject("tamanhoPagina", tamanhoPaginacao);
		try {

			Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
			if (usuario.getFuncionario().getUnidadeOrganizacional() != null) {
				chamadoVO.setIdUnidadeOrganizacionalUsuario(usuario.getFuncionario().getUnidadeOrganizacional().getChavePrimaria());
			}
			chamadoVO.setUsuarioAdministrador(usuario.isAdministradorAtendimento());

			model.addObject("isAdministradorAtendimento", usuario.isAdministradorAtendimento());
			Collection<Chamado> listaChamados = controladorChamado.consultarChamado(chamadoVO, Boolean.FALSE);
			carregarSituacaoQuestionario(listaChamados);
			model.addObject("listaChamados", listaChamados);

			ConstanteSistema constanteSistema =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_ALT_PRAZO_ENCERRAMENTO);
			model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));

		} catch(NegocioException e) {
			mensagemErroParametrizado(model, e);
		}
		model.addObject(CHAMADO_VO, chamadoVO);
		carregarCombos(model);

		if(chamadoVO.getUnidadeOrganizacional() != null) {
			model.addObject(LISTA_USUARIO_RESPONSAVEL, controladorFuncionario.listarFuncionarioPorUnidadeOrganizacional(chamadoVO
					.getUnidadeOrganizacional().getChavePrimaria()));
		}
		if (chamadoVO.getIdChamadoTipo() != null && chamadoVO.getIdChamadoTipo() > 0) {
			model.addObject(LISTA_CHAMADO_ASSUNTO, controladorChamadoAssunto.consultarChamado(chamadoVO.getIdChamadoTipo()));
		}
		if(condominioImovel != null && !condominioImovel.isEmpty()) {
			chamadoVO.setCondominioImovel(Boolean.parseBoolean(condominioImovel));
		}
		if(codigoTipoPessoa != null) {
			chamadoVO.setCodigoTipoPessoa(codigoTipoPessoa);
		}

		model.addObject(CONDOMINIO_IMOVEL, condominioImovel);
		if(condominioImovel == null) {
			model.addObject(CONDOMINIO_IMOVEL, "");
		} else {
			model.addObject(CONDOMINIO_IMOVEL, condominioImovel);
		}

		removerDadosSessao(request);
		} else {
			try {
				throw new NegocioException(ERRO_NEGOCIO_DATAS_NAO_PREENCHIDAS, true);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		}
		Log.info("Fim pesquisarChamado");
		return model;
	}
	
	private Boolean validarDatasFiltroPreenchidas(ChamadoVO chamado) {
		if ((!"".equals(chamado.getDataInicioAbertura()) && chamado.getDataInicioAbertura() != null)
				&& (!"".equals(chamado.getDataFimAbertura()) && chamado.getDataFimAbertura() != null)) {
			return Boolean.TRUE;
		} else if ((!"".equals(chamado.getDataInicioEncerramento()) && chamado.getDataInicioEncerramento() != null)
				&& (!"".equals(chamado.getDataFimEncerramento()) && chamado.getDataFimEncerramento() != null)) {
			return Boolean.TRUE;
		} else if ((!"".equals(chamado.getDataInicioResolucao()) && chamado.getDataInicioResolucao() != null)
				&& (!"".equals(chamado.getDataFimResolucao()) && chamado.getDataFimResolucao() != null)) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	/**
	 * Carrega a situação do questionário dos chamados
	 *
	 * @param listaChamados
	 */
	private void carregarSituacaoQuestionario(Collection<Chamado> listaChamados) {
		for (Chamado chamado : listaChamados) {
			if (chamado.getChamadoAssunto().getQuestionario() != null) {

				boolean respondido = controladorResposta
						.questionarioRespondido(chamado.getChamadoAssunto().getQuestionario().getChavePrimaria(), chamado);
				if (respondido) {
					chamado.setSituacaoQuestionario(SituacaoQuestionario.RESPONDIDO);
				} else {
					chamado.setSituacaoQuestionario(SituacaoQuestionario.NAO_RESPONDIDO);
				}
			} else {
				chamado.setSituacaoQuestionario(SituacaoQuestionario.NAO_POSSUI);
			}
		}
	}

	/**
	 * Carregar chamado assunto.
	 *
	 * @param chaveChamadoTipo
	 *            the chave chamado tipo
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarChamadoAssunto")
	public ModelAndView carregarChamadoAssunto(@RequestParam("chaveChamadoTipo") Long chaveChamadoTipo) throws GGASException {
		return this.getChamadoAssunto(chaveChamadoTipo, "divAssuntos");
	}

	/**
	 * Carregar chamado assunto.
	 *
	 * @param chaveChamadoTipo
	 *            the chave chamado tipo
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarChamadoAssuntoPesquisa")
	public ModelAndView carregarChamadoAssuntoPesquisa(@RequestParam("chaveChamadoTipo") Long chaveChamadoTipo) throws GGASException {
		return this.getChamadoAssunto(chaveChamadoTipo, "divAssunto");
	}

	private ModelAndView getChamadoAssunto(Long chaveChamadoTipo, String div) throws GGASException  {
		ModelAndView model = new ModelAndView(div);

		if (chaveChamadoTipo != null && chaveChamadoTipo != 0) {
			model.addObject(LISTA_CHAMADO_ASSUNTO, controladorChamadoAssunto.obterChamadoAssuntoPorChaveChamadoTipo(chaveChamadoTipo));
		} else {
			model.addObject(LISTA_CHAMADO_ASSUNTO, Collections.emptyList());
		}

		return model;
	}

	/**
	 * Carregar chamado tipo.
	 *
	 * @param chaveChamadoSegmento the chave chamado segmento
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarChamadoTipo")
	public ModelAndView carregarChamadoTipo(@RequestParam("chaveChamadoSegmento") Long chaveChamadoSegmento) throws GGASException {

		ModelAndView model = new ModelAndView("divTipo");

		if (chaveChamadoSegmento != null && chaveChamadoSegmento > 0) {
			model.addObject(LISTA_CHAMADO_TIPO, controladorChamadoTipo.listarPorSegmento(chaveChamadoSegmento));
		} else {
			model.addObject(LISTA_CHAMADO_TIPO, controladorChamadoTipo.obterTodos());
		}

		return model;
	}

	/**
	 * Carregar responsavel por chamado assunto.
	 *
	 * @param chaveChamadoAssunto
	 *            the chave chamado assunto
	 * @return the model and view
	 */
	@RequestMapping("carregarResponsavelPorChamadoAssunto")
	public ModelAndView carregarResponsavelPorChamadoAssunto(@RequestParam("chaveChamadoAssunto") Long chaveChamadoAssunto) {

		ModelAndView model = new ModelAndView("divResponsavelPorChamadoAssunto");

		if(chaveChamadoAssunto > 0) {
			List<Funcionario> listaFuncionario = new ArrayList<>();
			Usuario usuario = controladorChamadoAssunto.carregarUsuarioResponsavelPorChamadoAssunto(chaveChamadoAssunto);
			if(usuario != null) {
				Funcionario funcionario = usuario.getFuncionario();
				listaFuncionario.add(funcionario);
				model.addObject("resposavelChamadoAssunto", funcionario.getChavePrimaria());
			}
			model.addObject(LISTA_USUARIO_RESPONSAVEL, listaFuncionario);
		}

		return model;
	}

	/**
	 * Carregar prazo diferenciado
	 *
	 * @param chaveChamadoAssunto
	 *            the chave chamado assunto
	 * @return the model and view
	 */
	@RequestMapping("carregarPrazoDiferenciado")
	public ModelAndView carregarPrazoDiferenciado(@RequestParam("chaveChamadoAssunto") Long chaveChamadoAssunto) {

		ModelAndView model = new ModelAndView("divPrazoDiferenciado");

		if(chaveChamadoAssunto > 0) {

			ChamadoAssunto chamadoAssunto = controladorChamadoAssunto.carregarChamadoAssuntoProjetado(chaveChamadoAssunto);

			model.addObject("indicadorPrazoDiferenciado", isPrazoDiferenciadoHabilitado(chamadoAssunto));
		}

		return model;
	}

	private boolean isPrazoDiferenciadoHabilitado(ChamadoAssunto chamadoAssunto) {
		return chamadoAssunto != null && chamadoAssunto.getIndicadorPrazoDiferenciado() != null;
	}

	/**
	 * Exibir alteracao chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoChamado")
	public ModelAndView exibirAlteracaoChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		Log.info("Inicio exibirAlteracaoChamado");
		Chamado chamadoNovo = (Chamado) request.getAttribute(CHAMADO);

		if(chamadoNovo == null) {
			chamadoNovo = new Chamado();
			chamadoNovo.setChavePrimaria(chavePrimaria);
		}

		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_RASCUNHO);
		EntidadeConteudo rascunho = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);

		try {
			if(chamadoNovo.getUltimaAlteracao() == null) {

				this.exibirChamado(model, chavePrimaria, request);
				Chamado chamado = (Chamado) model.getModel().get(CHAMADO);

				model.addObject(CHAMADO, chamado);

				if(chamado.getStatus().getChavePrimaria() == rascunho.getChavePrimaria()) {
					model.addObject(FLUXO_ALTERACAO_RASCUNHO, Boolean.TRUE);
				} else {
					controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_ALTERACAO);
					model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
				}
			} else {
				model.addObject(CHAMADO, chamadoNovo);
				if(chamadoNovo.getChamadoAssunto() != null) {
					ChamadoTipo chamadoTipo = new ChamadoTipo();
					chamadoTipo.setChavePrimaria(chamadoNovo.getChamadoAssunto().getChamadoTipo().getChavePrimaria());
					model.addObject(CHAMADO_TIPO, chamadoTipo);
					model.addObject("chamadoAssunto", chamadoNovo.getChamadoAssunto());
					model.addObject(CHAVE_ASSUNTO, chamadoNovo.getChamadoAssunto().getChavePrimaria());
				}
				model.addObject(CLIENTE, chamadoNovo.getCliente());
				model.addObject(FLUXO_ALTERACAO_RASCUNHO, Boolean.TRUE);

			}
			
			

		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim exibirAlteracaoChamado");
		return model;
	}

	/**
	 * Exibir alteracao chamado inclusao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoChamadoInclusao")
	public ModelAndView exibirAlteracaoChamadoInclusao(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			HttpServletRequest request) throws GGASException {

		Log.info("Inicio exibirAlteracaoChamadoInclusao");
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_RASCUNHO);
		EntidadeConteudo rascunho = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);

		try {

			this.exibirChamado(model, chavePrimaria, request);
			Chamado chamado = (Chamado) model.getModel().get(CHAMADO);
			if (chamado.getStatus().getChavePrimaria() == rascunho.getChavePrimaria()) {
				model.addObject(FLUXO_ALTERACAO_RASCUNHO, Boolean.TRUE);
			} else {
				controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_ALTERACAO);
				model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
				chamado.setDescricao("");
			}

			Collection<TitulosGSAVO> listaTitulosAnalise = new ArrayList<TitulosGSAVO>();

			if (chamado.getChamadoTituloAnalise() != null && !chamado.getChamadoTituloAnalise().isEmpty()) {

				String[] numerosTitulos = chamado.getChamadoTituloAnalise().stream()
						.map(ChamadoTituloAnalise::getNumeroTitulo).toArray(String[]::new);

				listaTitulosAnalise = controladorFatura.obterTitulosPorNumero(numerosTitulos);

			}
			
			model.addObject("listaTitulosAnalise", listaTitulosAnalise);
			request.getSession().setAttribute("listaTitulosAnalise", listaTitulosAnalise);
			String cnpfCPF = "";
			if (!listaTitulosAnalise.isEmpty()) {
				cnpfCPF = listaTitulosAnalise.iterator().next().getCnpjcpf();
			} else if (chamado.getCliente() != null) {
				Cliente cliente = chamado.getCliente();

				if (!StringUtils.isEmpty(cliente.getCpf())) {
					cnpfCPF = cliente.getCpf().replace(".", "").replace("-", "").replace("/", "");
				} else if (!StringUtils.isEmpty(cliente.getCnpj())) {
					cnpfCPF = cliente.getCnpj().replace(".", "").replace("-", "").replace("/", "");
				}
			}

			Collection<TitulosGSAVO> listaTitulosAbertosSessao = controladorFatura.obterTitulosAbertoCliente(cnpfCPF);

			for (TitulosGSAVO tituloAnalise : listaTitulosAnalise) {
				boolean existe = listaTitulosAbertosSessao.stream().anyMatch(
						tituloAberto -> tituloAberto.getSequencialTitulo().equals(tituloAnalise.getSequencialTitulo()));

				if (!existe) {
					listaTitulosAbertosSessao.add(tituloAnalise);
				}
			}

			request.getSession().setAttribute("listaTitulosAbertosSessao", listaTitulosAbertosSessao);

			Set<String> titulosEmAnalise = listaTitulosAnalise.stream().map(TitulosGSAVO::getSequencialTitulo)
					.collect(Collectors.toSet());

			Collection<TitulosGSAVO> listaTitulosAbertos = new ArrayList<TitulosGSAVO>(listaTitulosAbertosSessao);

			listaTitulosAbertos.removeIf(titulo -> titulosEmAnalise.contains(titulo.getSequencialTitulo()));

			model.addObject("listaTitulosAbertos", listaTitulosAbertos);

		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		adicionarParametroCaixaAlta(request);
		
		Log.info("Fim exibirAlteracaoChamadoInclusao");
		return model;
	}

	/**
	 * Carregar combos.
	 *
	 * @param model
	 *            the model
	 * @throws GGASException
	 */
	private void carregarCombos(ModelAndView model) throws GGASException {

		model.addObject(LISTA_CHAMADO_TIPO, controladorChamadoTipo.listarTodosHabilitado());
		model.addObject(LISTA_CHAMADO_ASSUNTO, controladorChamadoAssunto.obterOrdenado());
		ConstanteSistema constanateSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_STATUS_CHAMADO);
		model.addObject(LISTA_CHAMADO_STATUS,
				controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanateSistema.getValor())));
		model.addObject(LISTA_TIPO_SERVICO, controladorServicoTipo.listarTodos());
		model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject(LISTA_USUARIO_RESPONSAVEL, controladorFuncionario.listarFuncionarioUnidadeOrganizacional());
		model.addObject(HABILITA_ACIONAMENTO, false);
		model.addObject("listaUnidadeFederacao", Fachada.getInstancia().listarUnidadeFederacao());

		Map<String, Object> filtro = new HashMap<>();

		filtro.put(HABILITADO, Boolean.TRUE);
		model.addObject(LISTA_SEGMENTO, fachada.consultarSegmento(filtro));
		model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());

		ConstanteSistema constanteManifestacao = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_ENTIDADE_CLASSE_TIPO_MANIFESTACAO_CHAMADO);
		model.addObject("listaTipoManifestacao",
				controladorEntidadeConteudo.listarEntidadeConteudo(constanteManifestacao.getClasse()));
	}

	/**
	 * Carrega nomes clientes.
	 *
	 * @param nome
	 *            the nome
	 * @param nomeFantasia
	 *            the nome fantasia
	 * @return the cliente vo auto complete[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("carregaNomesClientes")
	@ResponseBody
	public ClienteVOAutoComplete[] carregaNomesClientes(@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "nomeFantasia", required = false) String nomeFantasia) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		if(nomeFantasia == null) {
			filtro.put("nome", nome);
		} else {
			filtro.put("nomeFantasia", nomeFantasia);
		}
		Collection<Cliente> clientes = controladorCliente.consultarClientes(filtro);
		List<Cliente> clientesAtuais = popularListaClientes(clientes);

		List<ClienteVOAutoComplete> listaClienteVoAutoComplete = new ArrayList<>();
		ClienteVOAutoComplete clienteVo = null;

		StringBuilder informacoesAltoComplete = new StringBuilder();
		for (int i = 0; i < clientesAtuais.size(); i++) {
			Cliente cliente = clientesAtuais.get(i);
			clienteVo = new ClienteVOAutoComplete();
			Long chaveCliente = cliente.getChavePrimaria();
			List<ContratoPontoConsumo> listaContratoPontoConsumo =
					(List<ContratoPontoConsumo>) controladorContrato.obterContratoPontoConsumoPorCliente(chaveCliente, "cep", "contrato");

			clienteVo.setChavePrimaria(chaveCliente);

			String nomeCliente = cliente.getNome();
			clienteVo.setNome("NOME : " + nomeCliente + "\r\n");
			String cnpj = cliente.getCnpj();
			String cpf = cliente.getCpf();
			if (cnpj != null || cpf != null) {
				if (cnpj == null || cnpj.isEmpty()) {
					clienteVo.setCpfCnpj("\r\n Cpf/Cnpj : " + cpf + "\r\n");
				} else {
					clienteVo.setCpfCnpj(" \r\n Cpf/Cnpj : " + cnpj + "\r\n");
				}
			} else {
				clienteVo.setCpfCnpj(" \r\n Cpf/Cnpj : " + "\r\n");
			}

			if (listaContratoPontoConsumo.isEmpty()) {
				clienteVo.setEndereco("Endereço Consumo: Não possui");
				clienteVo.setNumeroContrato("Contrato: Não possui");
				clienteVo.setInicioVigenciaContrato("Vigência Inicial: Não possui");
				listaClienteVoAutoComplete.add(clienteVo);
			}

			String nomeFantasiaCliente = cliente.getNomeFantasia();
			if (nomeFantasiaCliente != null) {
				clienteVo.setNomeFantasia(nomeFantasiaCliente);
			}

			for (int j = 0; j < listaContratoPontoConsumo.size(); j++) {
				ClienteVOAutoComplete clienteVOAutoComplete = new ClienteVOAutoComplete();
				ContratoPontoConsumo contratoPontoConsumo = listaContratoPontoConsumo.get(j);

				Contrato contrato = contratoPontoConsumo.getContrato();

				String enderecoFormatado = contratoPontoConsumo.getPontoConsumo().getEnderecoFormatado();

				this.adicionarEnderecoEmCliente(clienteVOAutoComplete, enderecoFormatado);

				Date dateVigencia = contrato.getDataAssinatura();
				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				Date dateRecisao = contrato.getDataRecisao();
				String numeroFormatado = contrato.getNumeroFormatado();
				String dataVigencia = formatarDataVigencia(dateVigencia, df);
				verificaVigenciaRecisaoNumeroFormatado(clienteVOAutoComplete, df, dateRecisao, numeroFormatado, dataVigencia);
				clienteVo.setIdContrato(contrato.getChavePrimaria());

				clienteVOAutoComplete.setChavePrimaria(clienteVo.getChavePrimaria());
				clienteVOAutoComplete.setNome(clienteVo.getNome());
				clienteVOAutoComplete.setCpfCnpj(clienteVo.getCpfCnpj());
				clienteVOAutoComplete.setNomeFantasia(clienteVo.getNomeFantasia());
				clienteVOAutoComplete.setIdContrato(contrato.getChavePrimaria());

				informacoesAltoComplete.append(enderecoFormatado);
				listaClienteVoAutoComplete.add(clienteVOAutoComplete);
			}
		}

		return listaClienteVoAutoComplete.toArray(new ClienteVOAutoComplete[listaClienteVoAutoComplete.size()]);
	}

	private String formatarDataVigencia(Date dateVigencia, SimpleDateFormat df) {
		String dataVigencia = null;
		if (dateVigencia != null) {
			dataVigencia = df.format(dateVigencia);
		}
		return dataVigencia;
	}

	/**
	 * Responsável por adicionar endereço do consumo no cliente.
	 *
	 * @param clienteVOAutoComplete - {@link ClienteVOAutoComplete}
	 * @param enderecoFormatado - {@link String}
	 */
	private void adicionarEnderecoEmCliente(ClienteVOAutoComplete clienteVOAutoComplete, String enderecoFormatado){

		if(enderecoFormatado != null && !enderecoFormatado.isEmpty()) {
			clienteVOAutoComplete.setEndereco("\r\n Endereço Consumo : " + enderecoFormatado + "\r\n");
		} else {
			clienteVOAutoComplete.setEndereco(" ");
		}
	}

	/**
	 *
	 * @param clienteVOAutoComplete
	 * @param df
	 * @param dateRecisao
	 * @param numeroFormatado
	 * @param dataVigencia
	 */
	private void verificaVigenciaRecisaoNumeroFormatado(ClienteVOAutoComplete clienteVOAutoComplete,
			SimpleDateFormat df, Date dateRecisao, String numeroFormatado, String dataVigencia) {
		if(dataVigencia != null && !dataVigencia.isEmpty()) {
			clienteVOAutoComplete.setInicioVigenciaContrato(" Vingecia Inicial : " + dataVigencia + "\r\n");
		} else {
			clienteVOAutoComplete.setInicioVigenciaContrato(" ");
		}
		if(dateRecisao != null) {
			String dataRevisao = df.format(dateRecisao);
			clienteVOAutoComplete.setFimVigenciaContrato("Vigencia Final: " + dataRevisao + "\r\n");
		} else {
			clienteVOAutoComplete.setFimVigenciaContrato(" ");
		}
		if(numeroFormatado != null && !numeroFormatado.isEmpty()) {
			clienteVOAutoComplete.setNumeroContrato(" Contrato : " + numeroFormatado);
		} else {
			clienteVOAutoComplete.setNumeroContrato(" ");
		}
	}

	/**
	 * Popular anexo chamado.
	 *
	 * @param chamado the chamado
	 * @param request the request
	 * @throws NegocioException the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private void popularAnexoChamado(Chamado chamado, HttpServletRequest request) {

		Collection<ChamadoHistoricoAnexo> listaAnexos = (Collection<ChamadoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);
		chamado.setListaAnexos(listaAnexos);
	}

	/**
	 * Popular unidades organizacionais chamado.
	 *
	 * @param chamado the chamado
	 * @param request the request
	 * @throws GGASException
	 */
	private void popularUnidadesOrganizacionaisVisiveis(Chamado chamado, HttpServletRequest request)
			throws GGASException {

		List<ChamadoUnidadeOrganizacional> unidadesVisiveis = new ArrayList<>();

		Boolean flagUnidadesVisualizadoras = Boolean.valueOf(request.getParameter("flagUnidadesVisualizadoras"))
				|| Boolean.valueOf(request.getParameter("fluxoTramitacao"));
		chamado.setIndicadorUnidadesOrganizacionalVisualizadora(flagUnidadesVisualizadoras);
		
		String[] unidadesIds;
		
		if(!Boolean.valueOf(request.getParameter("fluxoTramitacao"))) {
			unidadesIds = (chamado.getChamadoAssunto() != null && chamado.getChamadoAssunto().getChavePrimaria() > 0)
					? controladorChamadoAssunto
							.obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(
									chamado.getChamadoAssunto().getChavePrimaria())
							.stream()
							.filter(p -> p.getChavePrimaria() != chamado.getUnidadeOrganizacional().getChavePrimaria())
							.map(p -> String.valueOf(p.getChavePrimaria())).toArray(String[]::new)
					: new String[0];
		} else {
			unidadesIds = request.getParameterValues("unidadeOrganizacionalVisualizadoras");
		}

		if (flagUnidadesVisualizadoras && !ArrayUtils.isEmpty(unidadesIds) && chamado.getUnidadeOrganizacional() != null) {
			Collection<Long> chavesPrimarias = new ArrayList<>();
			for (int i = 0; i < unidadesIds.length; i++) {
				chavesPrimarias.add(Long.parseLong(unidadesIds[i]));
			}
			Collection<UnidadeOrganizacional> unidades =
					controladorUnidadeOrganizacional.obterUnidadesOrganizacionalPorChavesPrimarias(chavesPrimarias);
			for (UnidadeOrganizacional unidade : unidades) {
				if (unidade != null && chamado.getUnidadeOrganizacional().getChavePrimaria() != unidade.getChavePrimaria()) {
					ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional = new ChamadoUnidadeOrganizacional();
					chamadoUnidadeOrganizacional.setDadosAuditoria(getDadosAuditoria(request));
					chamadoUnidadeOrganizacional.setChamado(chamado);
					chamadoUnidadeOrganizacional.getChamado().getChamadoAssunto().setDadosAuditoria(getDadosAuditoria(request));
					chamadoUnidadeOrganizacional.setUnidadeOrganizacional(unidade);
					unidadesVisiveis.add(chamadoUnidadeOrganizacional);
				}
			}
		} else {
			chamado.setIndicadorUnidadesOrganizacionalVisualizadora(false);
		}

		chamado.setListaUnidadeOrganizacionalVisualizadora(new HashSet<>(unidadesVisiveis));
	}

	/**
	 * Incluir chamado.
	 *
	 * @param chamado the chamado
	 * @param result the result
	 * @param session the session
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("incluirChamado")
	public ModelAndView incluirChamado(@RequestParam(value = "idTurnoPreferencia", required = false) Long idTurnoPreferencia, @RequestParam(value = "isGerarAsAutomatica", required = false) String isGerarAsPrioritaria, @RequestParam(value="equipe", required = false) Long chaveEquipe, @ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpSession session,
			HttpServletRequest request) throws GGASException {

		Log.info("Inicio incluirChamado");
		ModelAndView model = null;
		Map<String, Object> dados = new HashMap<>();
		ParametroSistema indicaManifestacao =
				controladorParametroSistema.obterParametroPorCodigo(Constantes.P_CHAMADO_MANIFESTACAO_OBRIGATORIO);
		try {

			if (chamado.getChamadoAssunto() != null) {
				chamado.getChamadoAssunto().setDadosAuditoria(getDadosAuditoria(request));
			}
			
			validarEnderecoChamado(chamado);
		    verificarNecessidadeAtualiacaoCliente(request, chamado, model);
			popularAnexoChamado(chamado, request);
			popularUnidadesOrganizacionaisVisiveis(chamado, request);
			popularMapaDados(dados, request);
			
			if (chaveEquipe != null && chaveEquipe != -1L) {
				Equipe equipe = fachada.obterEquipe(chaveEquipe);
				dados.put(EQUIPE, equipe);
			}
			
			if(idTurnoPreferencia != null && idTurnoPreferencia > 0l) {
				dados.put("turnoPreferencia", Fachada.getInstancia().obterEntidadeConteudo(idTurnoPreferencia));
			}
			
			if(!StringUtils.isEmpty(isGerarAsPrioritaria)) {
				dados.put("isGerarAsPrioritaria", Boolean.valueOf(isGerarAsPrioritaria));
			}
			
			
			if ("1".equals(indicaManifestacao.getValor()) && chamado.getManifestacao() == null) {
				throw new NegocioException("O campo Manifestação é de preenchimento obrigatório ");
			}

			if (chamado.getIndicadorAcionamentoGasista() == null) {
				chamado.setIndicadorAcionamentoGasista(false);
			}

			if (chamado.getIndicadorAcionamentoPlantonista() == null) {
				chamado.setIndicadorAcionamentoPlantonista(false);
			}

			validarAcionamentoGasista(chamado, request);
			
			if(chamado.getUltimaAlteracao() != null) {
				chamado.setUltimaAlteracao(Calendar.getInstance().getTime());
			}	

			if (chamado.getIndicadorUnidadesOrganizacionalVisualizadora()
					&& Util.isNullOrEmpty(chamado.getListaUnidadeOrganizacionalVisualizadora())) {
				throw new NegocioException("Selecione as Unidades Organizacionais que podem visualizar esse chamado ");
			}

			chamado.setDadosAuditoria(getDadosAuditoria(request));
	
			tratarErroServicoDuplicado(chamado, request, dados);
			
			if (chamado.isPossuiASDuplicada()) {
				return exibirDetalhamentoChamado(chamado.getChavePrimaria(), request);
			}
			
			model = exibirDetalhamentoChamado(chamado.getChavePrimaria(), request);
			mensagemSucesso(model, Constantes.CHAMADO_INCLUIDO_SUCESSO, String.valueOf(chamado.getProtocolo().getNumeroProtocolo()));
			session.removeAttribute(LISTA_IMOVEIS_CHAMADO);
			session.removeAttribute(LISTA_CHAMADO_EMAIL);

		} catch (NegocioException e) {
			Collection<TitulosGSAVO> listaTitulosAbertos = atualizarLstaTitulosAberto(session);
			Collection<TitulosGSAVO> listaTitulosAnalise = (Collection<TitulosGSAVO>) session.getAttribute("listaTitulosAnalise");
			
			model = exibirInclusaoEMensagemErroChamado(chamado, request, e);
			model.addObject("listaTitulosAnalise", listaTitulosAnalise);
			
			model.addObject("listaTitulosAbertos", listaTitulosAbertos);
		}
		Log.info("Fim incluirChamado");
		return model;
	}
	
	private void validarEnderecoChamado(Chamado chamado) throws NegocioException {
		if(chamado.getEnderecoChamado() != null) {
			if(chamado.getEnderecoChamado().getCep() == null) {
				chamado.setEnderecoChamado(null);
			}else {
				if(chamado.getNumeroEndereco().isEmpty()) {
					throw new NegocioException(Constantes.ERRO_NUMERO_ENDERECO_CHAMADO);
				}
			}
		}
	}

	private void verificarNecessidadeAtualiacaoCliente(HttpServletRequest request, Chamado chamado, ModelAndView model)
			throws ConcorrenciaException, NegocioException {

		Cliente cliente = chamado.getCliente();

		if (cliente != null) {
			String email = request.getParameter("email");
			String telefone = request.getParameter("telefone").replace("-", "");

			boolean atualizado = false;


			if (!StringUtils.isEmpty(email) && !email.equals(cliente.getEmailPrincipal())) {
				cliente.setEmailPrincipal(email);
				atualizado = true;
			}
			
			if(!StringUtils.isEmpty(telefone) && !telefone.equals(cliente.retornarTelefonePrincipal())) {
				ClienteFone clienteFone = cliente.getFones().stream().filter(p -> p.getIndicadorPrincipal()).findAny().orElse(null);
				String telefones[] = telefone.split(" ");
				String ddd = telefones[0].replace("(", "").replace(")", "");
				String numero = telefones[1];
				
				clienteFone.setCodigoDDD(Integer.valueOf(ddd));
				clienteFone.setNumero(Integer.valueOf(numero));
				
				atualizado = true;
			}

			if (atualizado) {
				controladorCliente.atualizar(cliente);
				chamado.setCliente(cliente);
			}

		}

	}

	private void validarAcionamentoGasista(@ModelAttribute("Chamado") Chamado chamado, HttpServletRequest request) throws NegocioException {
		String indicadorAcionamento = request.getParameter("indicadorAcionamento");
		if (indicadorAcionamento != null && Boolean.parseBoolean(indicadorAcionamento)) {
			if (!chamado.getIndicadorAcionamentoGasista() && !chamado.getIndicadorAcionamentoPlantonista()) {
				throw new NegocioException("É obrigatório indicar quem foi acionado ");
			}
			if (StringUtils.isEmpty(chamado.getInformacaoAdicional())) {
				throw new NegocioException("O campo Informações Adicionais é de preenchimento obrigatório ");
			}
			
			validarCamposAcionamento(chamado, request);
			
			Date dataInicioChamado = null;
			Collection<ChamadoHistorico> chamadosHistorico = controladorChamado.listarChamadoHistorico(chamado, null);
	        ChamadoHistorico ultimoChamadoHistorico = chamadosHistorico.stream()
	        		.min(Comparator.comparing(ChamadoHistorico::getUltimaAlteracao))
				    .orElse(null);

	        dataInicioChamado = ultimoChamadoHistorico != null ? ultimoChamadoHistorico.getUltimaAlteracao() : null;

			if(dataInicioChamado != null) {
				String dataHoraAcionamento = request.getParameter("dataAcionamento") +" "+ chamado.getHoraAcionamento();
				try {
					Date dataHoraAcionamentoConvertida = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(dataHoraAcionamento);
					if(dataHoraAcionamentoConvertida.compareTo(dataInicioChamado) < 0) {
						throw new NegocioException(" A data e horário de acionamento do gasista não pode ser antes da data e horário de geração do chamado ");
					}
				} catch (ParseException e) {
					LOG.error(e);                 
					throw new NegocioException(e);
				}
		        
			}
			
		}
	}

	private void validarCamposAcionamento(Chamado chamado, HttpServletRequest request) throws NegocioException {
		if(!StringUtils.isEmpty(chamado.getHoraAcionamento()) || !StringUtils.isEmpty(request.getParameter("dataAcionamento"))) {
			
			if(StringUtils.isEmpty(chamado.getHoraAcionamento())) {
				throw new NegocioException("Os campos data e horário de acionamento precisam ser preenchidos ");
			}
			if(StringUtils.isEmpty(request.getParameter("dataAcionamento"))) {
				throw new NegocioException("Os campos de data e horário de acionamento precisam ser preenchidos ");
			}
		}
	}
	


	/**
     * Exibe a tela de pesquisa chamado sucesso com a lsita de protocolos
     * @param protocolos números de protocolos adicionados
     * @param request requisicao
     * @return retorna o model and view
     * @throws GGASException lançada caso haja falha na operação
     */
	@RequestMapping(value = "exibirPesquisaChamadoSucesso")
	public ModelAndView exibirPesquisaChamadoSucesso(@RequestParam List<Long> protocolos,
													 HttpServletRequest request) throws GGASException {

		final ModelAndView modelAndView = exibirPesquisaChamado(request);
		mensagemSucesso(modelAndView, Constantes.CHAMADOS_EM_LOTE_INCLUIDO_SUCESSO,
				protocolos.stream().map(Objects::toString).collect(Collectors.joining(", ")));
		return modelAndView;
	}

	/**
	 * Inclui o chamado indicado em lote
	 * 
	 * @param chamado chamado a ser gerado em lote
	 * @param result resultado
	 * @param request requisição
	 * @return retorna o json com a lista de chamados adicionados
	 * @throws GGASException lançada caso haja falha na operação
	 */
	@RequestMapping(value = "incluirChamadoEmLote", method = { RequestMethod.POST })
	public ResponseEntity<String> incluirChamadoEmLote(@ModelAttribute("Chamado") Chamado chamado, BindingResult result,
			HttpServletRequest request) throws GGASException {

		Log.info("Inicio incluirChamadoEmLote");
		ResponseEntity<String> resultado = null;
		Map<String, Object> dados = new HashMap<>();
		ParametroSistema indicaManifestacao = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.P_CHAMADO_MANIFESTACAO_OBRIGATORIO);
		try {

			popularAnexoChamado(chamado, request);
			popularUnidadesOrganizacionaisVisiveis(chamado, request);
			popularMapaDados(dados, request);

			if ("1".equals(indicaManifestacao.getValor()) && chamado.getManifestacao() == null) {
				throw new NegocioException("O campo Manifestação é de preenchimento obrigatório ");
			}

			if (chamado.getIndicadorUnidadesOrganizacionalVisualizadora()
					&& Util.isNullOrEmpty(chamado.getListaUnidadeOrganizacionalVisualizadora())) {
				throw new NegocioException("Selecione as Unidades Organizacionais que podem visualizar esse chamado ");
			}

			final List<Long> chavesPontoConsumo = Optional.ofNullable(request.getParameterValues("chavePontoConsumo[]"))
					.map(a -> Arrays.stream(a).map(Long::valueOf).collect(Collectors.toList()))
					.orElse(new ArrayList<>());

			if (CollectionUtils.isEmpty(chavesPontoConsumo)) {
				throw new NegocioException("Selecione um ou mais pontos de consumo para criar os chamados em lote");
			}

			final List<Long> protocolos = controladorChamado.inserirChamadoEmLote(chamado, chavesPontoConsumo, dados);

			resultado = new ResponseEntity<>(
					new Gson().toJson(ImmutableMap.<String, Object>builder().put("protocolos", protocolos).build()),
					HttpStatus.OK);

		} catch (NegocioException e) {
			LOG.error("Falha ao registrar chamado", e);
			resultado = new ResponseEntity<>(gerarJsonMensagemErro(e), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			LOG.error("Falha não identificada", e);
			resultado = new ResponseEntity<>(ExceptionUtils.getStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Log.info("Fim incluirChamadoEmLote");
		return resultado;
	}

	/**
	 * Exibir inclusao e mensagem erro chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param request
	 *            the request
	 * @param e
	 *            the e
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private ModelAndView exibirInclusaoEMensagemErroChamado(Chamado chamado, HttpServletRequest request, NegocioException e)
			throws GGASException {

		ModelAndView model;
		model = exibirInclusaoChamado(null, null, chamado.getProtocolo().getChavePrimaria(), Boolean.TRUE, request);
		model.addObject(CHAMADO, chamado);
		model.addObject("isInclusaoComErro", Boolean.TRUE);
		if(chamado.getChamadoAssunto() != null) {
			model.addObject(CHAVE_ASSUNTO, chamado.getChamadoAssunto().getChavePrimaria());
			if(chamado.getChamadoAssunto().getUnidadeOrganizacional() != null && chamado.getUnidadeOrganizacional() != null) {
				model.addObject(UNIDADE, chamado.getUnidadeOrganizacional().getChavePrimaria());
			}
		}
		if(chamado.getUnidadeOrganizacional() != null) {
			model.addObject(UNIDADE, chamado.getUnidadeOrganizacional().getChavePrimaria());
		}
		verificarUnidadeOrganizacioalVisualizadora(chamado, model);
		if(request.getParameter(CHAMADO_TIPO) != null) {
			ChamadoTipo chamadoTipo = new ChamadoTipo();

			if (StringUtils.isNotEmpty(request.getParameter(CHAMADO_TIPO))) {
				chamadoTipo.setChavePrimaria(Long.parseLong(request.getParameter(CHAMADO_TIPO)));
			}
			model.addObject(CHAMADO_TIPO, chamadoTipo);
		}
		if(chamado.getCliente() != null) {
			model.addObject(CLIENTE, chamado.getCliente());
		}

		carregarListaAnexos(model, request);
		carregaCombos(model);
		setVisibilidadeAcionamentoGasista(chamado, model);
		model.addObject(HABILITA_ACIONAMENTO, true);
		mensagemErroParametrizado(model, e);
		return model;
	}

	private void verificarUnidadeOrganizacioalVisualizadora(Chamado chamado, ModelAndView model) {
		if (chamado.getIndicadorUnidadesOrganizacionalVisualizadora() != null) {
			if (chamado.getIndicadorUnidadesOrganizacionalVisualizadora()) {
				model.addObject(INDICADOR_UNIDADES_ORGANIZACIONAL_VISUALIZADORA, true);
				Collection<ChamadoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora =
						chamado.getListaUnidadeOrganizacionalVisualizadora();
				if (!Util.isNullOrEmpty(listaUnidadeOrganizacionalVisualizadora)) {
					model.addObject("listaChamadoUnidadeOrganizacional", listaUnidadeOrganizacionalVisualizadora);
				}
			} else {
				model.addObject(INDICADOR_UNIDADES_ORGANIZACIONAL_VISUALIZADORA, false);
			}
		}
	}

	/**
	 * Verifica se o campo de acionamento gasista deve aparecer na tela ou não
	 *
	 * @param chamado
	 * @param model
	 */
	private void setVisibilidadeAcionamentoGasista(Chamado chamado, ModelAndView model) {
		if (chamado.getChamadoAssunto() != null && chamado.getChamadoAssunto().getIndicadorAcionamentoGasista() != null
				&& chamado.getChamadoAssunto().getIndicadorAcionamentoGasista()) {
			model.addObject("exibirAcionamentoGasista", true);
		} else {
			model.addObject("exibirAcionamentoGasista", false);
		}
	}

	/**
	 * Inserir chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param request
	 *            the request
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void inserirChamado(Chamado chamado, HttpServletRequest request, Map<String, Object> dados) throws GGASException {

		@SuppressWarnings("unchecked")
		List<ChamadoEmail> listaChamadoEmail = (List<ChamadoEmail>) request.getSession().getAttribute(LISTA_CHAMADO_EMAIL);
		@SuppressWarnings("unchecked")
		List<ContatoCliente> listaChamadoContato = (List<ContatoCliente>) request.getSession().getAttribute(LISTA_CHAMADO_CONTATO);
		
		Boolean isAmbienteProducao = controladorParametroSistema.obterIndicadorAmbienteProducao();
		if(chamado.getProtocolo() == null) {
			Long numeroProtocolo = null;
			if(request.getParameter(NUMERO_PROTOCOLO) != null && !request.getParameter(NUMERO_PROTOCOLO).replace(" ", "").isEmpty()
					&& StringUtils.isNumeric(request.getParameter(NUMERO_PROTOCOLO).replace(" ", ""))) {
				numeroProtocolo = Long.valueOf(request.getParameter(NUMERO_PROTOCOLO).replace(" ", ""));
			}
			controladorChamado.inserirChamado(chamado, Boolean.valueOf(request.getParameter(RASCUNHO)), numeroProtocolo, dados,
					listaChamadoContato);
		} else {
			controladorChamado.inserirChamado(chamado, Boolean.valueOf(request.getParameter(RASCUNHO)), null, dados, listaChamadoContato);
		}
		if(listaChamadoEmail != null && isAmbienteProducao) {
			controladorChamado.inserirChamadoEmail(listaChamadoEmail, chamado);
		}

	}

	/**
	 * Incluir chamado copiar.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param session
	 *            the session
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("incluirChamadoCopiar")
	public ModelAndView incluirChamadoCopiar(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpSession session,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = null;
		Map<String, Object> dados = new HashMap<>();
		@SuppressWarnings("unchecked")
		List<ContatoCliente> listaChamadoContato = (List<ContatoCliente>) session.getAttribute(LISTA_CHAMADO_CONTATO);
		try {
			if (chamado.getChamadoAssunto() != null) {
				chamado.setUnidadeOrganizacional(chamado.getChamadoAssunto().getUnidadeOrganizacional());
			}
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			controladorChamado.inserirChamado(chamado, Boolean.valueOf(request.getParameter(RASCUNHO)), null, dados, listaChamadoContato);
			model = exibirInclusaoChamado(null, null, chamado.getProtocolo().getChavePrimaria(), Boolean.FALSE, request);
			exibirChamado(model, chamado.getChavePrimaria(), request);
			mensagemSucesso(model, Constantes.CHAMADO_INCLUIDO_SUCESSO, chamado.getProtocolo().getNumeroProtocolo());
			session.removeAttribute(LISTA_IMOVEIS_CHAMADO);
		} catch(NegocioException e) {
			model = exibirInclusaoChamado(null, null, chamado.getProtocolo().getChavePrimaria(), Boolean.FALSE, request);
			model.addObject(CHAMADO, chamado);
			if(chamado.getChamadoAssunto() != null) {
				model.addObject(CHAVE_ASSUNTO, chamado.getChamadoAssunto().getChavePrimaria());
			}
			if(request.getParameter(CHAMADO_TIPO) != null) {
				ChamadoTipo chamadoTipo = new ChamadoTipo();
				chamadoTipo.setChavePrimaria(Long.parseLong(request.getParameter(CHAMADO_TIPO)));
				model.addObject(CHAMADO_TIPO, chamadoTipo);
			}
			if(chamado.getCliente() != null) {
				model.addObject(CLIENTE, chamado.getCliente());
			}
			carregarListaAnexos(model, request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Alterar chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param descricao
	 *            the descricao
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("alterarChamado")
	public ModelAndView alterarChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, @RequestParam("descricao") String descricao,
			@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request) throws GGASException {

		Log.info("Inicio alterarChamado");
		@SuppressWarnings("unchecked")
  		List<ContatoCliente> listaChamadoContato = (List<ContatoCliente>) request.getSession().getAttribute(LISTA_CHAMADO_CONTATO);
		@SuppressWarnings("unchecked")
		List<ChamadoEmail> listaChamadoEmail = (List<ChamadoEmail>) request.getSession().getAttribute(LISTA_CHAMADO_EMAIL);
		Chamado chamadoExistente = controladorChamado.obterChamado(chavePrimaria);

		ConstanteSistema constanteRascunho = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_RASCUNHO);
		EntidadeConteudo statusRascunho = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteRascunho.getValor()));
		
		ConstanteSistema constanteAberto = null;
		
		if (Util.isTrue(chamado.getIndicadorPendencia())) {
			constanteAberto = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_PENDENTE);
		} else {
			constanteAberto = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		}
		
		EntidadeConteudo statusEmAberto = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constanteAberto.getValor()));

		Boolean rascunho = Boolean.valueOf(request.getParameter(FLUXO_ALTERACAO_RASCUNHO));

		Date dataResolucao = chamado.getDataResolucao();

		chamado.setDataResolucao(chamadoExistente.getDataResolucao());
		chamado.setQuantidadeReiteracao(chamadoExistente.getQuantidadeReiteracao());
		chamado.setQuantidadeReabertura(chamadoExistente.getQuantidadeReabertura());
		chamado.setQuantidadeReativacao(chamadoExistente.getQuantidadeReativacao());
		chamado.setIndicadorAgenciaReguladora(chamadoExistente.getIndicadorAgenciaReguladora());
//		chamado.setIndicadorAcionamentoGasista(chamadoExistente.getIndicadorAcionamentoGasista());
//		chamado.setIndicadorAcionamentoPlantonista(chamadoExistente.getIndicadorAcionamentoPlantonista());
		chamado.setIndicadorUnidadesOrganizacionalVisualizadora(chamadoExistente.getIndicadorUnidadesOrganizacionalVisualizadora());
		chamado.setListaUnidadeOrganizacionalVisualizadora(chamadoExistente.getListaUnidadeOrganizacionalVisualizadora());
		chamado.setHabilitado(chamadoExistente.isHabilitado());
		chamado.setVersao(chamadoExistente.getVersao());
		chamado.setDadosAuditoria(getDadosAuditoria(request));
		

		chamadoExistente.setDataResolucao(dataResolucao);
		chamadoExistente.setDadosAuditoria(getDadosAuditoria(request));
		chamadoExistente.setIndicadorPendencia(chamado.getIndicadorPendencia());
		chamadoExistente.setHoraAcionamento(chamado.getHoraAcionamento());
		chamadoExistente.setInformacaoAdicional(chamado.getInformacaoAdicional());
		chamadoExistente.setIndicadorAcionamentoGasista(chamado.getIndicadorAcionamentoGasista());
		chamadoExistente.setIndicadorAcionamentoPlantonista(chamado.getIndicadorAcionamentoPlantonista());
		
		
		
		ModelAndView model = null;
		request.setAttribute("operacao", Constantes.C_OPERACAO_CHAMADO_ALTERADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			validarAcionamentoGasista(chamado,request);
			popularAnexoChamado(chamado, request);
			popularMapaDados(dados, request);
			if(rascunho) {
				chamado.setStatus(chamadoExistente.getStatus());
				controladorChamado.atualizarChamadoSemValidar(chamado);
			} else if(chamadoExistente.getStatus().getChavePrimaria() == statusRascunho.getChavePrimaria()) {
				chamado.setStatus(statusEmAberto);
				controladorChamado.atualizarChamado(chamado, listaChamadoContato);
				controladorChamado.gerarASAutomatica(chamado, dados);
				controladorChamado.incluirChamadoHistorico(chamado, Constantes.C_OPERACAO_CHAMADO_ALTERADO, dados);
				if(listaChamadoEmail != null && !listaChamadoEmail.isEmpty()) {
					controladorChamado.inserirChamadoEmail(listaChamadoEmail, chamado);
				}

			} else {
				chamadoExistente.setDescricao(descricao);
				chamadoExistente.setUnidadeOrganizacional(chamado.getUnidadeOrganizacional());
				if(chamado.getIndicadorPendencia() || chamadoExistente.getStatus().getDescricao().equals("PENDENTE")) {
					chamadoExistente.setStatus(statusEmAberto);
				}
				
				controladorChamado.atualizarChamado(chamadoExistente, listaChamadoContato);
				chamado.setUnidadeOrganizacional(chamadoExistente.getUnidadeOrganizacional());
				chamado.setStatus(chamadoExistente.getStatus());
				
				controladorChamado.incluirChamadoHistorico(chamado, Constantes.C_OPERACAO_CHAMADO_ALTERADO, dados);
				if(listaChamadoEmail != null && !listaChamadoEmail.isEmpty()) {
					controladorChamado.inserirChamadoEmail(listaChamadoEmail, chamado);
				}
			}
			
			Collection<TitulosGSAVO> listaTitulosAnaliseSessao = (Collection<TitulosGSAVO>) request.getSession().getAttribute("listaTitulosAnalise");
			Collection<ChamadoTituloAnalise> listaChamadoTituloAnalise = chamadoExistente.getChamadoTituloAnalise();
			
			
			controladorChamado.atualizarChamadoTituloAnalise(listaTitulosAnaliseSessao, listaChamadoTituloAnalise, dados, chamadoExistente);
			
			model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(NegocioException e) {
			chamado.setUltimaAlteracao(chamadoExistente.getUltimaAlteracao());
			request.setAttribute(CHAMADO, chamado);
			model = exibirAlteracaoChamadoInclusao(chavePrimaria, request);
			Chamado chamadoModel = (Chamado) model.getModel().get(CHAMADO);
			chamadoModel.setDescricao(chamado.getDescricao());
			carregarListaAnexos(model, request);
			carregaCombos(model);
			if (chamado.getChamadoAssunto() != null && chamado.getChamadoAssunto().getUnidadeOrganizacional() != null) {
				model.addObject(UNIDADE, chamado.getUnidadeOrganizacional().getChavePrimaria());
			}
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim alterarChamado");
		return model;
	}

	/**
	 * Copiar chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("copiarChamado")
	public ModelAndView copiarChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request) throws GGASException {

		ModelAndView model = exibirInclusaoChamado(null, null, null, Boolean.FALSE, request);

		exibirChamado(model, chavePrimaria, request);

		Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
		chamado.setProtocolo(carregarProtocolo());

		chamado.setDescricao("");

		model.addObject(CHAMADO, chamado);

		return model;
	}

	/**
	 * Exibir capturar chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirCapturarChamado")
	public ModelAndView exibirCapturarChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		exibirChamado(model, chavePrimaria, request);
		Chamado chamado = controladorChamado.obterChamado(chavePrimaria);

		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			popularMapaDados(dados, request);
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_CAPTURA);
			controladorChamado.verificarCaptura(chamado, dados);
			chamado.setUsuarioResponsavel(usuario);
			Usuario funcionario = controladorUsuario.obterUsuario(usuario.getChavePrimaria());
			chamado.getUsuarioResponsavel().setFuncionario(funcionario.getFuncionario());

			chamado.setDescricao("");

			model.addObject(CHAMADO, chamado);
			model.addObject("fluxoCapturar", Boolean.TRUE);

		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Capturar chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("capturarChamado")
	public ModelAndView capturarChamado(@ModelAttribute("Chamado") Chamado chamado, BindingResult result, HttpServletRequest request,
			@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria) throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		Map<String, Object> dados = new HashMap<>();

		try {
			popularMapaDados(dados, request);
			Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
			chamado.setUsuarioResponsavel(usuario);
			Usuario funcionario = controladorUsuario.obterUsuario(usuario.getChavePrimaria());
			chamado.getUsuarioResponsavel().setFuncionario(funcionario.getFuncionario());
			controladorChamado.capturarChamado(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_CAPTURADA, GenericAction.obterMensagem(Constantes.CHAMADO));

		} catch(NegocioException e) {
			model = new ModelAndView("forward:/exibirCapturarChamado");
			carregarListaAnexos(model, request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Carregar assuntos.
	 * @param chavePrimaria chave primaria
	 * @param chaveAssunto chave primaria do assunto
	 * @param chavePrimariaCliente chave primaria do cliente
	 * @param chavePrimariaImovel chave primaria do imovel
	 * @return retorna model and view do spring
	 * @throws GGASException lançado caso haja falha na operação
	 */
	@RequestMapping("carregarAssuntos")
	public ModelAndView carregarAssuntos(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			@RequestParam(value = CHAVE_ASSUNTO, required = false) Long chaveAssunto,
			@RequestParam(value = CHAVE_PRIMARIA_CLIENTE, required = false) Long chavePrimariaCliente,
			@RequestParam(value = CHAVE_PRIMARIA_IMOVEL, required = false) Long chavePrimariaImovel,
			@RequestParam(value = FLUXO_INCLUSAO, required = false) String fluxoInclusao) throws GGASException {
		
		ModelAndView model = new ModelAndView("divAssuntos");
		Collection<ChamadoAssunto> assuntos;
		if(fluxoInclusao != null && Boolean.valueOf(fluxoInclusao) == true) {
			assuntos = controladorChamadoAssunto.obterChamadoAssuntoAtivoPorChaveChamadoTipo(chavePrimaria);
		}else {
			assuntos = controladorChamadoAssunto.obterChamadoAssuntoPorChaveChamadoTipo(chavePrimaria);
		}
		if (chavePrimariaCliente != null && chavePrimariaImovel != null) {
			AjaxService ajaxService = new AjaxService();
			assuntos = assuntos.stream().filter(chamadoAssunto -> !chamadoAssunto.getIndicadorComGarantia() ||
					ajaxService.garantiaVigente(chavePrimariaCliente, chamadoAssunto.getChavePrimaria(), chavePrimariaImovel))
					.collect(Collectors.toList());
		}

		model.addObject("assuntos", assuntos);
		model.addObject(CHAVE_ASSUNTO, chaveAssunto);

		return model;
	}

	/**
	 * Carrega a unidade organizacional de acordo com o ChamadoAssunto selecionado e com a unidade que está definida no chamado.
	 *
	 * @param chavePrimariaAssunto a chave primária do ChamadoAssunto
	 * @param idUnidadeOrganizacionalChamado a chave primária da unidade que está vinculada ao chamado
	 * @return O fragmento correpondente com a unidade que deverá ser selecionada na listagem do combobox de unidades organizacionais
	 */
	@RequestMapping("carregarUnidadeOrganizacional")
	public ModelAndView carregarUnidadeOrganizacional(@RequestParam("chavePrimariaAssunto") Long chavePrimariaAssunto,
			@RequestParam("idUnidadeOrganizacionalChamado") Long idUnidadeOrganizacionalChamado) {
		ModelAndView model = new ModelAndView("divUnidadeOrganizacional");
		Long idUnidadeSelecionada = 0L;
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		if (idUnidadeOrganizacionalChamado == null) {
			UnidadeOrganizacional unidadeOrganizacional = controladorUnidadeOrganizacional.
					consultarUnidadeOrganizacionalPorChamadoAssunto(chavePrimariaAssunto);
			if (unidadeOrganizacional != null) {
				idUnidadeSelecionada = unidadeOrganizacional.getChavePrimaria();
			}
		} else {
			idUnidadeSelecionada = idUnidadeOrganizacionalChamado;
		}
		model.addObject(UNIDADE, idUnidadeSelecionada);
		return model;
	}

	/**
	 * Consultar imovel.
		* Utiliza a view de grid de imóveis antiga, sem uso do bootstrap, prefira o uso do consultarImovel
	 *
	 * @param nome
	 *            the nome
	 * @param complemento
	 *            the complemento
	 * @param matricula
	 *            the matricula
	 * @param numeroImovel
	 *            the numero imovel
	 * @param numeroCep
	 *            the numero cep
	 * @param pagina
	 *            the pagina
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("consultarImovelOld")
	public ModelAndView consultarImovelOld(@RequestParam(value = "nome", required = false) String nome,
					@RequestParam(value = "complemento", required = false) String complemento,
					@RequestParam(value = "matricula", required = false) String matricula,
					@RequestParam(value = "numeroImovel", required = false) String numeroImovel,
					@RequestParam(value = "numeroCep", required = false) String numeroCep,
					@RequestParam(value = "pagina", required = false) String pagina,
					HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_IMOVEIS);
		return consultarImovel(nome, complemento, matricula, numeroImovel, numeroCep, session, model);
	}

	/**
	 * Consultar imovel, retornando a view de grid imoveis com bootstrap
	 *
	 * @param nome
	 *            the nome
	 * @param complemento
	 *            the complemento
	 * @param matricula
	 *            the matricula
	 * @param numeroImovel
	 *            the numero imovel
	 * @param numeroCep
	 *            the numero cep
	 * @param pagina
	 *            the pagina
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("consultarImovel")
	public ModelAndView consultarImovel(@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "complemento", required = false) String complemento,
			@RequestParam(value = "matricula", required = false) String matricula,
			@RequestParam(value = "numeroImovel", required = false) String numeroImovel,
			@RequestParam(value = "numeroCep", required = false) String numeroCep,
			@RequestParam(value = "pagina", required = false) String pagina,
			HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_IMOVEIS_BOOTSTRAP);
		return consultarImovel(nome, complemento, matricula, numeroImovel, numeroCep, session, model);
	}

	private ModelAndView consultarImovel(String nome, String complemento, String matricula, String numeroImovel,
			String numeroCep, HttpSession session, ModelAndView model)
			throws FormatoInvalidoException, NegocioException {
		Map<String, Object> filtro = new HashMap<>();

		if(!StringUtils.isEmpty(matricula)) {
			filtro.put(CHAVE_PRIMARIA, Util.converterCampoStringParaValorLong(Imovel.MATRICULA, matricula));
		}

		if(!StringUtils.isEmpty(numeroCep)) {
			filtro.put(CEP_IMOVEL, numeroCep);
		}

		if(!StringUtils.isEmpty(nome)) {
			filtro.put(NOME, nome);
		}

		if(!StringUtils.isEmpty(complemento)) {
			filtro.put(COMPLEMENTO_IMOVEL, complemento);
		}

		if(!StringUtils.isEmpty(numeroImovel)) {
			filtro.put(NUMERO_IMOVEL, numeroImovel);
		}

		Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);
		session.setAttribute(LISTA_IMOVEIS_CHAMADO, listaImoveis);

		return model;
	}

	/**
	 * Popular lista clientes.
	 *
	 * @param clientes
	 *            the clientes
	 * @return the list
	 */
	private List<Cliente> popularListaClientes(Collection<Cliente> clientes) {

		List<Cliente> clientesAtuais = new ArrayList<>();
		Cliente clienteAtual = null;
		for (Cliente cliente : clientes) {
			clienteAtual = (Cliente) ServiceLocator.getInstancia().getBeanPorID(Cliente.BEAN_ID_CLIENTE);
			clienteAtual.setChavePrimaria(cliente.getChavePrimaria());
			clienteAtual.setNome(cliente.getNome());
			clienteAtual.setNomeFantasia(cliente.getNomeFantasia());

			clienteAtual.setCpf(cliente.getCpfFormatado());
			clienteAtual.setCnpj(cliente.getCnpjFormatado());
			clienteAtual.setRg(cliente.getRg());

			clientesAtuais.add(clienteAtual);
		}
		return clientesAtuais;
	}

	/**
	 * Carregar cliente.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param cpfCnpj
	 *            the cpf cnpj
	 * @param passaporte
	 *            the passaporte
	 * @param email
	 *            the email
	 * @param telefone
	 *            the telefone
	 * @param response
	 *            the response
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarCliente")
	public ModelAndView carregarCliente(@RequestParam(value = CHAVE_PRIMARIA, required = false) Long chavePrimaria,
			@RequestParam(value = "cpfCnpj", required = false) String cpfCnpj,
			@RequestParam(value = "passaporte", required = false) String passaporte,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "telefone", required = false) String telefone, HttpServletResponse response, HttpSession session)
			throws GGASException {

		ModelAndView model = new ModelAndView(DIV_CLIENTE_CHAMADO);
		session.removeAttribute(LISTA_IMOVEIS_CHAMADO);
		Cliente cliente = null;
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		try {
			if(chavePrimaria != null) {
				cliente = (Cliente) controladorCliente.obter(chavePrimaria);

			} else if(telefone != null && !telefone.isEmpty()) {
				cliente = controladorCliente.consultarClientePorTelefone(telefone);
			} else {
				Map<String, Object> filtro = new HashMap<>();
				if(Util.removerCaracteresEspeciais(cpfCnpj).length() <= TAMANHO_CPF) {
					filtro.put("cpf", cpfCnpj);
				} else if(Util.removerCaracteresEspeciais(cpfCnpj).length() > TAMANHO_CPF) {
					filtro.put("cnpj", cpfCnpj);
				}
				filtro.put("passaporte", passaporte);
				filtro.put(EMAIL, email);
				List<Cliente> clientes = (List<Cliente>) controladorCliente.consultarClientes(filtro);
				cliente = clientes.get(0);
			}

			if(cliente != null) {
				cliente.getContatos();

				model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
			}


		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
		}
		model.addObject(CLIENTE, cliente);

		ParametroSistema exibirPassaporte = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_PASSAPORTE_CLIENTE);
		model.addObject("exibirPassaporte", exibirPassaporte.getValor());
		return model;
	}

	/**
	 * Carregar cliente por contrato.
	 *
	 * @param numeroContrato
	 *            the numero contrato
	 * @param response
	 *            the response
	 * @param session
	 *            the session
	 * @return the model and view
	 */
	@RequestMapping("carregarClientePorContrato")
	public ModelAndView carregarClientePorContrato(@RequestParam("numeroContrato") String numeroContrato,
			HttpServletResponse response,
			HttpSession session) {

		ModelAndView model = new ModelAndView(DIV_CLIENTE_CHAMADO);

		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		try {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put("numero", Integer.valueOf(numeroContrato));
			Cliente cliente = ServiceLocator.getInstancia().getControladorCliente()
					.obterClienteAssinaturaPeloNumeroContrato(numeroContrato);


			Chamado chamado = new Chamado();
			chamado.setCliente(cliente);

			model.addObject(CHAMADO, chamado);
			model.addObject(CLIENTE, cliente);
			if(cliente == null) {
				model = new ModelAndView(AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
			} else {
				model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Carregar cliente por contrato.
	 *
	 * @param chavePrimariaContrato
	 *            the chave primaria contrato
	 * @param response
	 *            the response
	 * @param session
	 *            the session
	 * @return the model and view
	 */
	@RequestMapping("carregarClientePorIdContrato")
	public ModelAndView carregarClientePorIdContrato(
			@RequestParam("chavePrimariaContrato") Long chavePrimariaContrato,
			HttpServletResponse response,
			HttpSession session) {

		ModelAndView model = new ModelAndView(DIV_CLIENTE_CHAMADO);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);

		if(chavePrimariaContrato != null) {
			try {
				Cliente cliente = ServiceLocator.getInstancia().getControladorCliente()
						.obterClienteAssinaturaPeloIdContrato(chavePrimariaContrato);
				Chamado chamado = new Chamado();
				chamado.setCliente(cliente);
				model.addObject(CHAMADO, chamado);
				model.addObject(CLIENTE, cliente);
				if(cliente == null) {
					model = new ModelAndView(AJAX_ERRO);
					mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
				} else {
					model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				model = new ModelAndView(AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
			}
		}

		return model;
	}

	/**
	 * Carrega o telefone principal de um cliente
	 * @param cliente cliente a ser investigado
	 * @return retorna o telefone principal do cliente formatado
	 */
	private String carregarTelefonePrincipalCliente(Cliente cliente) {
		StringBuilder telefone = new StringBuilder();
		Collection<ClienteFone> fones = cliente.getFones();

		if (fones != null) {

			for (ClienteFone fone : fones) {
				if (Util.isTrue(fone.getIndicadorPrincipal()) || telefone.length() == 0) {

					formatarTelefone(telefone, fone);
				}
			}
		}
		return telefone.toString();
	}

	/**
	 * Formata a exibição do telefone
	 *
	 * @param telefone
	 * @param fone
	 */
	private void formatarTelefone(StringBuilder telefone, ClienteFone fone) {
		if (telefone.length() > 0) {
			telefone.setLength(0);
		}

		if (fone.getNumero() != null) {

			if (fone.getCodigoDDD() != null) {
				telefone.append("(").append(fone.getCodigoDDD()).append(") ");
			}

			telefone.append(fone.getNumero());

			if (fone.getRamal() != null) {
				telefone.append(" - ").append(fone.getRamal());
			}
		}
	}


	/**
	 * Carregar chamados cliente imovel.
	 *
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("carregarChamadosClienteImovel")
	public ModelAndView carregarChamadosClienteImovel(
			@RequestParam(value = "chavePrimariaCliente", required = false) Long chavePrimariaCliente,
			@RequestParam(value = "chavePrimariaImovel", required = false) Long chavePrimariaImovel,
			@RequestParam(value = "chavePrimariaPontoConsumo", required = false) Long chavePrimariaPontoConsumo,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView("gridChamadosCliente");

		if (chavePrimariaCliente != null || chavePrimariaImovel != null || chavePrimariaPontoConsumo != null) {
			try {
				List<Chamado> chamadosCliente = (List<Chamado>) controladorChamado.consultarChamadoPorClienteOuImovel(chavePrimariaCliente,
						chavePrimariaImovel, chavePrimariaPontoConsumo);
				model.addObject("chamadosCliente", chamadosCliente);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				try {
					response.sendError(1);
				} catch (IOException e1) {
					LOG.error(e1);
				}
			}
		}

		return model;
	}

	/**
	 * Método responsável pelo carregamento dos pontos de consumo.
	 *
	 * @param chavePrimariaPontoConsumo {@link Long}
	 * @param response {@link HttpServletResponse}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 * @throws IOException {@link IOException}
	 */
	@RequestMapping("carregarPontosConsumoClienteImovel")
	public ModelAndView carregarPontosConsumoClienteImovel(
			@RequestParam(value = "chavePrimariaPontoConsumo", required = false) Long chavePrimariaPontoConsumo,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView("gridChamadoTabelaPontoConsumo");

		try {
			List<Chamado> pontosConsumoCliente =
					(List<Chamado>) controladorChamado.consultarChamadoPorClienteOuImovel(null, null, chavePrimariaPontoConsumo);
			model.addObject("pontosConsumoCliente", pontosConsumoCliente);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			try {
				response.sendError(1);
			} catch (IOException e1) {
				LOG.error(e1);
			}
		}

		return model;
	}

	/**
	 * Carregar pontos consumo.
	 *
	 * @param chaveImovel the chave imovel
	 * @param fluxoExecucao the fluxo execucao
	 * @param chavePrimariaContrato the chave contrato
	 * @param response the response
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarPontosConsumo")
	public ModelAndView carregarPontosConsumo(@RequestParam("chavePrimariaImovel") Long chaveImovel,
			@RequestParam("fluxoExecucao") String fluxoExecucao, @RequestParam("chavePrimariaContrato") Long chavePrimariaContrato,
			HttpServletResponse response) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_CHAMADO_PONTOS_CONSUMO);

		List<PontoConsumo> pontosConsumoComContrato = new ArrayList<>();
		if(chavePrimariaContrato != null &&  chavePrimariaContrato > 0) {
			List<ContratoPontoConsumo> contratoPontosConsumo =
					(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(chavePrimariaContrato);

			for(ContratoPontoConsumo contratoPontoConsumo : contratoPontosConsumo) {
				pontosConsumoComContrato.add(contratoPontoConsumo.getPontoConsumo());
			}
		}

		try {
			Map<String, Object> filtro = new HashMap<>();
			Long[] chavesPrimarias = new Long[] {chaveImovel};
			filtro.put("chavesPrimariasImoveis", chavesPrimarias);

			List<PontoConsumo> pontosConsumoImovel = (List<PontoConsumo>) controladorPontoConsumo.consultarPontosConsumo(filtro, new String[]{});

			List<PontoConsumo> pontosConsumo = new ArrayList<>();

			for(PontoConsumo pontoConsumo1 : pontosConsumoImovel) {
				if(pontosConsumoComContrato.contains(pontoConsumo1) && !pontosConsumo.contains(pontoConsumo1)) {
					pontosConsumo.add(pontoConsumo1);
				}
			}

			if(pontosConsumoComContrato.isEmpty()) {
				pontosConsumo.addAll(0, pontosConsumoImovel);
			}

			model.addObject(PONTOS_CONSUMO, pontosConsumo);



			if(FLUXO_INCLUSAO.equals(fluxoExecucao)) {
				model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
			} else {
				model.addObject(FLUXO_INCLUSAO, null);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			try {
				response.sendError(1);
			} catch (IOException ex) {
				LOG.error(ex.getMessage(), ex);
			}
		}
		return model;
	}

	/**
	 * Carregar pontos consumo.
	 *
	 * @param fluxoExecucao the fluxo execucao
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping("limparPontosConsumo")
	public ModelAndView limparPontosConsumo(@RequestParam("fluxoExecucao") String fluxoExecucao, HttpServletResponse response) {

		ModelAndView model = new ModelAndView(GRID_CHAMADO_PONTOS_CONSUMO);

		model.addObject(PONTOS_CONSUMO, new ArrayList<PontoConsumo>());

		if (FLUXO_INCLUSAO.equals(fluxoExecucao)) {
			model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		} else {
			model.addObject(FLUXO_INCLUSAO, null);
		}
		return model;
	}

	/**
	 * Carregar pontos consumo por contrato
	 *
	 * @param chavePrimariaContrato the chave contrato
	 * @param numeroContrato the numero contrato
	 * @param fluxoExecucao the fluxo execucao
	 * @param response the response
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarPontosConsumoPorContrato")
	public ModelAndView carregarPontosConsumoPorContrato(@RequestParam("chavePrimariaContrato") Long chavePrimariaContrato,
			@RequestParam("numeroContrato") String numeroContrato, @RequestParam("fluxoExecucao") String fluxoExecucao,
			HttpServletResponse response) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_CHAMADO_PONTOS_CONSUMO);

		Long chaveContrato = chavePrimariaContrato;
		if((chavePrimariaContrato != null &&  chavePrimariaContrato > 0) || (numeroContrato != null && !numeroContrato.isEmpty())) {

			if(chavePrimariaContrato == null) {
				Contrato contrato = (Contrato)controladorContrato.consultarContratoPorNumero(numeroContrato);
				chaveContrato = contrato.getChavePrimaria();
			}

			List<ContratoPontoConsumo> contratoPontosConsumo =
					(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(chaveContrato);

			List<PontoConsumo> pontosConsumo= new ArrayList<>();
			for(ContratoPontoConsumo contratoPontoConsumo : contratoPontosConsumo) {
				pontosConsumo.add(contratoPontoConsumo.getPontoConsumo());
			}


			model.addObject(PONTOS_CONSUMO, pontosConsumo);


			if(FLUXO_INCLUSAO.equals(fluxoExecucao)) {
				model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
			} else {
				model.addObject(FLUXO_INCLUSAO, null);
			}
		}

		return model;
	}


	/**
	 * Carregar imoveis por cliente.
	 *
	 * @param chaveCliente
	 *            the chave cliente
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarImoveisPorCliente")
	public ModelAndView carregarImoveisPorCliente(@RequestParam("chavePrimariaCliente") Long chaveCliente, HttpSession session)
			throws GGASException {

		ModelAndView model = new ModelAndView(GRID_IMOVEIS);
		List<Imovel> listaImoveis = controladorChamado.listarImoveisPorCliente(chaveCliente);
		session.setAttribute(LISTA_IMOVEIS_CHAMADO, listaImoveis);

		return model;
	}

	/**
	 * Carregar cliente por ponto consumo.
	 *
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param response
	 *            the response
	 * @return the model and view
	 */
	@RequestMapping("carregarClientePorPontoConsumo")
	public ModelAndView carregarClientePorPontoConsumo(@RequestParam("chavePontoConsumo") Long chavePontoConsumo,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView(DIV_CLIENTE_CHAMADO);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		try {
			Cliente cliente = null;
			cliente = controladorCliente.obterClientePorPontoConsumo(chavePontoConsumo);
			Chamado chamado = new Chamado();
			chamado.setCliente(cliente);
			model.addObject(CHAMADO, chamado);
			model.addObject(CLIENTE, cliente);
			if(cliente == null) {
				model = new ModelAndView(AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
			} else {
				model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Exibir chamado.
	 *
	 * @param model
	 *            the model
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void exibirChamado(ModelAndView model, Long chavePrimaria, HttpServletRequest request) throws GGASException {
		
		Log.info("Inicio exibirChamado");
		Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
		model.addObject(CHAMADO, chamado);
		model.addObject(CLIENTE, chamado.getCliente());

		if (chamado.getCliente() != null) {
			model.addObject(TELEFONE, carregarTelefonePrincipalCliente(chamado.getCliente()));
		}

		if(chamado.getChamadoAssunto() != null) {
			model.addObject(CHAVE_ASSUNTO, chamado.getChamadoAssunto().getChavePrimaria());
			if(chamado.getChamadoAssunto().getUnidadeOrganizacional() != null) {
				model.addObject(UNIDADE, chamado.getChamadoAssunto().getUnidadeOrganizacional().getChavePrimaria());
			}
		}
		if(chamado.getUsuarioResponsavel() != null) {
			Usuario usuario = controladorUsuario.obterUsuario(chamado.getUsuarioResponsavel().getChavePrimaria());
			chamado.getUsuarioResponsavel().setFuncionario(usuario.getFuncionario());
		}

		if(chamado.getChamadoAssunto() != null) {
			Collection<ChamadoAssunto> assuntos = controladorChamadoAssunto.consultarChamado(chamado.getChamadoAssunto().getChamadoTipo()
					.getChavePrimaria());
			model.addObject("assuntos", assuntos);
		}

		Collection<ChamadoHistorico> listaChamadoHistorico = controladorChamado.listarChamadoHistorico(chamado, null);
		model.addObject("listaChamadoHistorico", listaChamadoHistorico);

		popularListaAnexos(model, listaChamadoHistorico);

		Collection<ChamadoEmail> listaChamadoEmail = (List<ChamadoEmail>) request.getSession().getAttribute(LISTA_CHAMADO_EMAIL);
		if(listaChamadoEmail == null || listaChamadoEmail.isEmpty()) {
			listaChamadoEmail = controladorChamado.listarChamadoEmail(chamado);
		}

		chamado.setChamadoEmail(listaChamadoEmail);

		model.addObject(LISTA_CHAMADO_EMAIL, listaChamadoEmail);
		request.getSession().setAttribute(LISTA_CHAMADO_EMAIL, listaChamadoEmail);
		if(chamado.getCliente() != null) {
			Cliente cliente = null;
			cliente = (Cliente) controladorCliente.obter(chamado.getCliente().getChavePrimaria(), PROPRIEDADES_LAZY);

			List<ContatoCliente> listaChamadoContato = new ArrayList<>(cliente.getContatos());
			model.addObject(LISTA_CHAMADO_CONTATO, listaChamadoContato);
			request.getSession().setAttribute(LISTA_CHAMADO_CONTATO, listaChamadoContato);
		}
		Collection<TipoContato> listaTipoContato = controladorCliente.listarTipoContato();
		model.addObject(LISTA_TIPO_CONTATO, listaTipoContato);
		request.getSession().setAttribute(LISTA_TIPO_CONTATO, listaTipoContato);

		Collection<Profissao> listaTipoCargo = controladorCliente.listarProfissao();
		model.addObject(LISTA_TIPO_CARGO, listaTipoCargo);
		request.getSession().setAttribute(LISTA_TIPO_CARGO, listaTipoCargo);

		if(chamado.getPontoConsumo() != null) {
			carregarPontoConsumoChamado(chamado.getPontoConsumo().getChavePrimaria(), model);
		}
		
		if(chamado.getEnderecoChamado() != null) {
			carregarEnderecoChamado(chamado,model);
		}

		model.addObject(HABILITA_ACIONAMENTO, chamado.getIndicadorAcionamentoGasista());
		model.addObject(INFORMACAO_ADICIONAL, chamado.getInformacaoAdicional());

		setVisibilidadeAcionamentoGasista(chamado, model);
		carregaCombos(model);
		Log.info("Fim exibirChamado");
	}

	/**
	 * @param model
	 * @param listaChamadoHistorico
	 */
	private void popularListaAnexos(ModelAndView model, Collection<ChamadoHistorico> listaChamadoHistorico) {
		Collection<HashMap<String, Object>> listaChamadoHistoricoAnexo = new ArrayList<>();

		for (ChamadoHistorico ch : listaChamadoHistorico) {
			for (ChamadoHistoricoAnexo cha : ch.getAnexos()) {

				HashMap<String, Object> hmp = new HashMap<>();
				hmp.put("descricao", ch.getDescricao());
				hmp.put("ultimaAlteracao", ch.getUltimaAlteracao());
				hmp.put("usuarioLogin", ch.getUsuario().getLogin());
				hmp.put("nomeDocumentoAnexo", cha.getDescricaoAnexo());
				hmp.put("idAnexo", cha.getChavePrimaria());
				hmp.put("operacao", ch.getOperacao().getDescricao());
				listaChamadoHistoricoAnexo.add(hmp);
			}
		}

		model.addObject("listaChamadoHistoricoAnexo", listaChamadoHistoricoAnexo);
	}

	/**
	 * Imprimir chamado
	 *
	 * @param chavePrimaria {@link Long}
	 * @param comAS {@link Boolean}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @throws NegocioException {@link NegocioException}
	 * @throws IOException {@link IOException}
	 */
	@RequestMapping("imprimirChamado")
	public void imprimirChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, @RequestParam("comAS") Boolean comAS,
			HttpServletRequest request, HttpServletResponse response) throws NegocioException {
		Log.info("Inicio imprimirChamado");
		byte[] relatorio = controladorRelatorioChamado.gerarRelatorio(chavePrimaria, comAS);

		salvarRelatoriosChamado(relatorio, response);
		Log.info("Fim imprimirChamado");
	}
	
	/**
	 * Imprimir chamados selecionados
	 *
	 * @param chavesPrimarias {@link Long}
	 * @param comAS {@link Boolean}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @throws NegocioException {@link NegocioException}
	 * @throws IOException {@link IOException}
	 */
	@RequestMapping("imprimirChamados")
	public void imprimirChamados(@RequestParam(CHAVES_PRIMARIAS) Long[] chavesPrimarias, @RequestParam("comAS") Boolean comAS,
			HttpServletRequest request, HttpServletResponse response) throws NegocioException {
		Log.info("Inicio imprimirChamados");
		List<byte[]> relatorios = new ArrayList<>();
		
		for(Long chavePrimaria : chavesPrimarias) {
			relatorios.add(controladorRelatorioChamado.gerarRelatorio(chavePrimaria, comAS));
		}
		
		byte[] relatorio = RelatorioUtil.unificarRelatoriosPdf(relatorios);

		salvarRelatoriosChamado(relatorio, response);
		Log.info("Fim imprimirChamados");
	}
	

	/**
	 * Imprimir arquivo.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("imprimirArquivoChamado")
	public void imprimirArquivo(@RequestParam("chaveChamadoHistorico") Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response) throws NegocioException {

		Log.info("Inicio imprimirArquivo - chamado");
		ChamadoHistoricoAnexo chamadoHistoricoAnexo = controladorChamado.obterChamadoHistoricoAnexo(chavePrimaria);

		byte[] arquivo = chamadoHistoricoAnexo.getDocumentoAnexo();

		if(arquivo != null) {
			try {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				String nomeArquivo = chamadoHistoricoAnexo.getNomeArquivo();
				response.setContentLength(arquivo.length);
				response.addHeader(CONTENT_DISPOSITION, "attachment; filename=" + nomeArquivo);
				servletOutputStream.write(arquivo, 0, arquivo.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error("Falha ao realizar operação de leitura / escrita de arqvuio", e);
				throw new NegocioException(e);
			}

		}
		Log.info("Fim imprimirArquivo - chamado");
	}

	/**
	 * Verificar tipo fluxo execucao.
	 *
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	private ModelAndView verificarTipoFluxoExecucao(HttpServletRequest request) {

		String fluxoPesquisa = request.getParameter("fluxoPesquisa");
		String fluxoDetalhamento = request.getParameter("fluxoDetalhamento");

		ModelAndView model = null;
		if(fluxoPesquisa != null) {
			model = new ModelAndView("forward:/pesquisarChamado");
		} else if(fluxoDetalhamento != null) {
			model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);
		}
		return model;
	}

	/**
	 * Carregar contrato por ponto consumo.
	 *
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param response
	 *            the response
	 * @return the model and view
	 */
	@RequestMapping("carregarContratoPorPontoConsumo")
	public ModelAndView carregarContratoPorPontoConsumo(@RequestParam("chavePontoConsumo") Long chavePontoConsumo,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView("divContrato");
		try {
			PontoConsumo pontoConsumo = (PontoConsumo) ServiceLocator.getInstancia().getBeanPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
			pontoConsumo.setChavePrimaria(chavePontoConsumo);
			ContratoPontoConsumo contratoPontoConsumo = null;
			contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
			Cliente cliente = controladorCliente.obterClientePorPontoConsumo(chavePontoConsumo);
			Chamado chamado = new Chamado();
			chamado.setCliente(cliente);
			chamado.setContrato(contratoPontoConsumo.getContrato());
			model.addObject(CHAMADO, chamado);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(AJAX_ALERTA);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CONTRATO_ATIVO_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Carregar pontos consumo.
	 *
	 * @param chavePrimariaContrato the chave contrato
	 * @param numeroContrato the numero contrato
	 * @param session the session
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarImovelPorContrato")
	public ModelAndView carregarImovelPorContrato(@RequestParam("chavePrimariaContrato") Long chavePrimariaContrato,
			@RequestParam("numeroContrato") String numeroContrato, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_IMOVEIS_BOOTSTRAP);
		Long chaveContrato = chavePrimariaContrato;

		if(chavePrimariaContrato == null && (numeroContrato != null && !numeroContrato.isEmpty())) {
			Contrato contrato = pesquisarContratoPorNumero(numeroContrato);
			if(contrato != null) {
				chaveContrato = contrato.getChavePrimaria();
			}
		}

		if (chaveContrato != null && chaveContrato > 0) {
			List<ContratoPontoConsumo> contratoPontosConsumo =
					(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(chaveContrato);
			List<Imovel> listaImoveis = new ArrayList<>();
			for(ContratoPontoConsumo contratoPontoConsumo : contratoPontosConsumo) {
				Map<String, Object> filtro = new HashMap<>();
				filtro.put(CHAVE_PRIMARIA, contratoPontoConsumo.getPontoConsumo().getImovel().getChavePrimaria());

				Collection<Imovel> imoveis = controladorImovel.consultarImoveis(filtro);

				if (!listaImoveis.containsAll(imoveis)) {
					listaImoveis.addAll(imoveis);
				}
			}
			session.setAttribute(LISTA_IMOVEIS_CHAMADO, listaImoveis);
		} else {
			session.removeAttribute(LISTA_IMOVEIS_CHAMADO);
		}

		return model;
	}

	/**
	 * Carregar imovel.
	 *
	 * @param chaveImovel
	 *            the chave imovel
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("carregarImovel")
	public ModelAndView carregarImovel(@RequestParam("chavePrimariaImovel") Long chaveImovel, HttpServletResponse response)
			throws IOException {

		ModelAndView model = new ModelAndView("divImovel");
		try {
			Imovel imovel = null;
			imovel = (Imovel) controladorImovel.obter(chaveImovel);
			Chamado chamado = new Chamado();
			chamado.setImovel(imovel);
			model.addObject(CHAMADO, chamado);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			response.sendError(1);
		}

		return model;
	}


	/**
	 * Gerar autorizacao servico.
	 *
	 * @param chavesPrimariasDialog {@link Long}
	 * @param chavesPrimarias {@link Long}
	 * @param chaveEquipe {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 * @throws IOException 
	 * @throws ParseException {@link ParseException}
	 */
	@RequestMapping("gerarAutorizacaoServico")
	public ModelAndView gerarAutorizacaoServico(@RequestParam("chavesPrimariasDialog") Long[] chavesPrimariasDialog,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, @RequestParam(value="equipe", required = false) Long chaveEquipe, @RequestParam(value = "idTurnoPreferencia", required = false) Long idTurnoPreferencia, @RequestParam(value = "isGerarAsAutomatica", required = false) String isGerarAsPrioritaria,  HttpServletRequest request)
			throws GGASException, IOException {

		ModelAndView model = new ModelAndView("forward:/pesquisarServicoAutorizacao");
		Map<String, Object> dados = new HashMap<>();
		try {
			popularMapaDados(dados, request);
			if (chaveEquipe != null && chaveEquipe != -1L) {
				Equipe equipe = fachada.obterEquipe(chaveEquipe);
				dados.put(EQUIPE, equipe);
			}
			
			if(idTurnoPreferencia != null && idTurnoPreferencia > 0l) {
				dados.put("turnoPreferencia", Fachada.getInstancia().obterEntidadeConteudo(idTurnoPreferencia));
			}
			
			if(!StringUtils.isEmpty(isGerarAsPrioritaria)) {
				dados.put("isGerarAsPrioritaria", Boolean.valueOf(isGerarAsPrioritaria));
			}
			
			Collection<ServicoAutorizacao> lista =
					controladorChamado.gerarAutorizacaoServico(chavesPrimarias, chavesPrimariasDialog, dados);
			model.addObject("lista", lista);
			request.setAttribute("lista", lista);
			
			ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
			model.addObject("servicoAutorizacaoVO", servicoAutorizacaoVO);

			mensagemSucesso(model, Constantes.SUCESSO_GERACAO_SERVICO_AUTORIZACAO);
		} catch (NegocioException e) {
			model = new ModelAndView("forward:/pesquisarChamado");
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		} catch (ParseException e) {
			LOG.error("Falha ao gerar autoriazação de serviço", e);
			throw new GGASException(e);
		}

		return model;
	}

	/**
	 * Popular informacoes medidor.
	 *
	 * @param model
	 *            the model
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private ModelAndView popularInformacoesMedidor(ModelAndView model, PontoConsumo pontoConsumo) throws NegocioException {

		if(pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getInstalacaoMedidor().getMedidor() != null) {
			model.addObject("medidor", pontoConsumo.getInstalacaoMedidor().getMedidor());
		}

		if(pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getInstalacaoMedidor().getMedidor() != null) {
			String valorConstanteOperacaoInstalacao = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_INSTALACAO);
			List<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = (List<HistoricoOperacaoMedidor>) controladorMedidor
					.consultarHistoricoOperacaoMedidor(pontoConsumo.getInstalacaoMedidor().getMedidor().getChavePrimaria(), null);
			model.addObject("listaHistoricoOperacaoMedidor", listaHistoricoOperacaoMedidor);

			if(listaHistoricoOperacaoMedidor != null && !listaHistoricoOperacaoMedidor.isEmpty()) {
				Date dataInstalacaoMedidor = null;
				BigDecimal leituraInstalacaoMedidor = null;
				int cont = 0;
				for (HistoricoOperacaoMedidor historicoOperacaoMedidor : listaHistoricoOperacaoMedidor) {

					if(historicoOperacaoMedidor.getOperacaoMedidor().getChavePrimaria() == Long.parseLong(valorConstanteOperacaoInstalacao)
							&& cont == 0) {
						dataInstalacaoMedidor = historicoOperacaoMedidor.getDataRealizada();
						leituraInstalacaoMedidor = historicoOperacaoMedidor.getNumeroLeitura();
						cont++;
					} else if(historicoOperacaoMedidor.getOperacaoMedidor().getChavePrimaria() == Long
							.parseLong(valorConstanteOperacaoInstalacao)
							&& historicoOperacaoMedidor.getDataRealizada().after(dataInstalacaoMedidor)) {
						dataInstalacaoMedidor = historicoOperacaoMedidor.getDataRealizada();
						leituraInstalacaoMedidor = historicoOperacaoMedidor.getNumeroLeitura();
					}
				}
				model.addObject("dataInstalacaoMedidor", dataInstalacaoMedidor);
				model.addObject("leituraInstalacaoMedidor", leituraInstalacaoMedidor);
			}

		}

		return model;
	}

	/**
	 * Popular informacoes corretor vazao.
	 *
	 * @param model
	 *            the model
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private ModelAndView popularInformacoesCorretorVazao(ModelAndView model, PontoConsumo pontoConsumo) throws NegocioException {

		if(pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null) {
			model.addObject("vazaoCorretor", pontoConsumo.getInstalacaoMedidor().getVazaoCorretor());
		}

		if(pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null) {
			List<VazaoCorretorHistoricoOperacao> listaVazaoCorretorHistoricoOperacao =
					(List<VazaoCorretorHistoricoOperacao>) controladorVazaoCorretor
							.listarHistoricoCorretorVazao(pontoConsumo.getInstalacaoMedidor().getVazaoCorretor().getChavePrimaria());
			model.addObject("listaVazaoCorretorHistorico", listaVazaoCorretorHistoricoOperacao);

			if(listaVazaoCorretorHistoricoOperacao != null && !listaVazaoCorretorHistoricoOperacao.isEmpty()) {
				Date dataInstalacaoCorretorVazao = null;
				BigDecimal leituraInstalacaoCorretorVazao = null;
				for (VazaoCorretorHistoricoOperacao vazaoCorretorHistoricoOperacao : listaVazaoCorretorHistoricoOperacao) {

					if("Instalação".equals(vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getDescricao())) {
						dataInstalacaoCorretorVazao = vazaoCorretorHistoricoOperacao.getDataRealizada();
						leituraInstalacaoCorretorVazao = vazaoCorretorHistoricoOperacao.getLeitura();
					} else if("Instalação".equals(vazaoCorretorHistoricoOperacao.getOperacaoMedidor().getDescricao())
							&& vazaoCorretorHistoricoOperacao.getDataRealizada().after(dataInstalacaoCorretorVazao)) {
						dataInstalacaoCorretorVazao = vazaoCorretorHistoricoOperacao.getDataRealizada();
						leituraInstalacaoCorretorVazao = vazaoCorretorHistoricoOperacao.getLeitura();
					}
					model.addObject("dataInstalacaoCorretorVazao", dataInstalacaoCorretorVazao);
					model.addObject("leituraInstalacaoCorretorVazao", leituraInstalacaoCorretorVazao);
				}
			}
		}

		return model;
	}

	/**
	 * Popular informacoes contrato.
	 *
	 * @param model
	 *            the model
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private ModelAndView popularInformacoesContrato(ModelAndView model, ContratoPontoConsumo contratoPontoConsumo) throws GGASException {

		Long chaveContrato = contratoPontoConsumo.getContrato().getChavePrimaria();
		Contrato contrato = (Contrato) controladorContrato.obter(chaveContrato, "arrecadadorContratoConvenio");
		ArrecadadorContratoConvenio arrecadadorContratoConvenio = contrato.getArrecadadorContratoConvenio();

		Map<String, Object> filtro = new HashMap<>();
		if(arrecadadorContratoConvenio != null) {
			filtro.put("idArrecadadorConvenio", arrecadadorContratoConvenio.getChavePrimaria());
			Collection<ArrecadadorContratoConvenio> listaArrecadadorContratoConvenio = controladorArrecadadorConvenio
					.consultarArrecadadorConvenio(filtro);
			arrecadadorContratoConvenio = listaArrecadadorContratoConvenio.iterator().next();

			model.addObject("arrecadadorContratoConvenio", arrecadadorContratoConvenio);
		}

		model.addObject("contrato", contratoPontoConsumo.getContrato());

		if(contratoPontoConsumo.getFaixaPressaoFornecimento() != null) {
			model.addObject("faixaPressaoFornecimento", contratoPontoConsumo.getFaixaPressaoFornecimento().getMedidaMaximo());
			model.addObject("unidadeFaixaPressaoFornecimento", contratoPontoConsumo.getFaixaPressaoFornecimento().getUnidadePressao().getDescricaoAbreviada());
		}

		List<ContratoQDC> listaContratoQDC = (List<ContratoQDC>) controladorContrato.listarContratoQDC(contratoPontoConsumo
				.getPontoConsumo().getChavePrimaria(), contratoPontoConsumo.getContrato().getChavePrimaria());
		List<String> listaContratoQDCFormatada = new ArrayList<>();
		if(listaContratoQDC != null && !listaContratoQDC.isEmpty()) {
			StringBuilder stringBuilder;
			for (ContratoQDC contratoQDC : listaContratoQDC) {
				stringBuilder = new StringBuilder();
				stringBuilder.append(Util.converterDataParaStringSemHora(contratoQDC.getData(), Constantes.FORMATO_DATA_BR));
				stringBuilder.append(" | ");
				stringBuilder.append(Util.converterCampoCurrencyParaString(contratoQDC.getMedidaVolume(), Constantes.LOCALE_PADRAO));
				listaContratoQDCFormatada.add(stringBuilder.toString());

			}
		}
		model.addObject("listaContratoQDC", listaContratoQDCFormatada);
		model.addObject("listaAlteracoesContrato",
				controladorContrato.listarContratoPorNumero(contratoPontoConsumo.getContrato().getNumero()));

		Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento = controladorContrato
				.listarContratoPontoConsumoItemFaturamentoPorContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
		model.addObject("listaContratoPontoConsumoItemFaturamento", listaContratoPontoConsumoItemFaturamento);
		
		model.addObject("listaBanco", controladorBanco.listarBanco());
		
		Map<String, Object> filtroArrecadadorDebito = new HashMap<String, Object>();
		filtroArrecadadorDebito.put(HABILITADO, TRUE);
		filtroArrecadadorDebito.put("tipoConvenio", Long.valueOf(ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO)));
		
		model.addObject("listaArrecadadorConvenioDebitoAutomatico", ServiceLocator.getInstancia()
				.getControladorArrecadadorConvenio().consultarArrecadadorConvenio(filtroArrecadadorDebito));

		return model;
	}

	/**
	 * Popular informacoes cobranca.
	 *
	 * @param model
	 *            the model
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private ModelAndView popularInformacoesCobranca(ModelAndView model, Long idCliente, Long idPontoConsumo) throws GGASException {

		Long idPagamentoPendente = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		Long idPagamentoParcial = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO));

		Long[] arrayPagamento = {idPagamentoPendente, idPagamentoParcial};
		Map<String, Object> filtroFatura = new HashMap<>();
		filtroFatura.put("situacaoPagamento", arrayPagamento);
		filtroFatura.put("situacaoValida", true);
		filtroFatura.put("idCliente", idCliente);
		filtroFatura.put("idPontoConsumo", idPontoConsumo);
		filtroFatura.put(OrdenacaoDataEmissaoFatura.KEY_ORDENACAO_DATA_EMISSAO_FATURA, OrdenacaoDataEmissaoFatura.DESCENDENTE);
		Collection<Fatura> listaFatura = controladorFatura.consultarFatura(filtroFatura);

		List<ExtratoDebitoFaturaVO> listaFaturaVO = new LinkedList<>();

		String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
		Long idFaturaParcialmentePaga = Util.converterCampoStringParaValorLong("Situação de Pagamento",
				codigoSituacaoPagamentoParcialmentePago);

		for (Fatura fatura : listaFatura) {
			ExtratoDebitoFaturaVO extratoDebito = new ExtratoDebitoFaturaVO();
			extratoDebito.setIdFatura(fatura.getChavePrimaria());
			if(fatura.getCicloReferenciaFormatado() != null) {
				extratoDebito.setCicloReferencia(fatura.getCicloReferenciaFormatado());
			}
			BigDecimal valorTotal = fatura.getValorTotal();
			BigDecimal valorSaldo = fatura.getValorSaldoConciliado();
			if((idFaturaParcialmentePaga != null) && (fatura.getSituacaoPagamento() != null)
					&& (idFaturaParcialmentePaga == fatura.getSituacaoPagamento().getChavePrimaria())) {
				BigDecimal valorRecebimento = controladorCobranca.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
				if(valorRecebimento != null) {
					valorSaldo = fatura.getValorSaldoConciliado().subtract(valorRecebimento);
				}
			}

			extratoDebito.setValor(Util.converterCampoCurrencyParaString(valorTotal, Constantes.LOCALE_PADRAO));
			extratoDebito.setSaldo(Util.converterCampoCurrencyParaString(valorSaldo, Constantes.LOCALE_PADRAO));

			if(fatura.getDataEmissao() != null) {
				extratoDebito.setDataEmissao(Util.converterDataParaStringSemHora(fatura.getDataEmissao(), Constantes.FORMATO_DATA_BR));
			}
			if(fatura.getDataVencimento() != null) {
				extratoDebito.setDataVencimento(Util.converterDataParaStringSemHora(fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
			}
			if(fatura.getTipoDocumento() != null) {
				extratoDebito.setTipoDocumento(fatura.getTipoDocumento().getDescricao());
				extratoDebito.setIdTipoDocumento(fatura.getTipoDocumento().getChavePrimaria());
			}
			if(fatura.getSituacaoPagamento() != null) {
				extratoDebito.setSituacaoPagamento(fatura.getSituacaoPagamento().getDescricao());
			}
			if(fatura.getPontoConsumo() != null) {
				extratoDebito.setIdPontoConsumo(fatura.getPontoConsumo().getChavePrimaria());
			}

			listaFaturaVO.add(extratoDebito);
		}

		model.addObject("listaFaturaCobranca", listaFaturaVO);

		return model;
	}

	/**
	 * Popular informacoes agendamento.
	 *
	 * @param model
	 *            the model
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the model and view
	 */
	private ModelAndView popularInformacoesAgendamento(ModelAndView model, Long idPontoConsumo) {

		List<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda = controladorServicoAutorizacao
				.consultarServicoAutorizacaoAgendaPorPontoConsumo(idPontoConsumo);

		model.addObject("listaServicoAutorizacaoAgenda", listaServicoAutorizacaoAgenda);

		return model;
	}

	/**
	 * Visualizar informacoes ponto consumo.
	 *
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("visualizarInformacoesPontoConsumo")
	public ModelAndView visualizarInformacoesPontoConsumo(@RequestParam("idPontoConsumo") Long idPontoConsumo, @RequestParam(value="situacaoTitulo", required=false) String situacaoTitulo) throws GGASException {

		ModelAndView model = new ModelAndView("visualizarInformacoesPontoConsumo");
		
		model.addObject("situacaoSelecionada", situacaoTitulo);

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo,"instalacaoMedidor");
		Cliente cliente = controladorCliente.obterClientePorPontoConsumo(idPontoConsumo);
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
		
		Boolean isColetorDados = "COLETOR DE DADOS".equals(pontoConsumo.getRota().getTipoLeitura().getDescricao());
		
		model.addObject("isColetorDados", isColetorDados);

		if(contratoPontoConsumo != null && contratoPontoConsumo.getCep() != null) {
			String enderecoFaturamento = contratoPontoConsumo.getEnderecoFormatadoRuaNumeroComplemento();
			enderecoFaturamento = enderecoFaturamento + " " + contratoPontoConsumo.getEnderecoFormatadoMunicipioUF();

			model.addObject("enderecoFaturamento", enderecoFaturamento);
			model.addObject("cepFaturamento", contratoPontoConsumo.getCep().getCep());
		}
		
		model.addObject("endereco", pontoConsumo.getImovel().getEnderecoFormatado());
		model.addObject("cep", pontoConsumo.getCep().getCep());
		
		
		if (pontoConsumo.getImovel() != null && pontoConsumo.getImovel().getChavePrimaria() > 0l) {
			Collection<ContratoPontoConsumo> listaContratoPontoConsumos = controladorContrato
					.consultarContratoClientePontoConsumoPorImovel(pontoConsumo.getImovel().getChavePrimaria());
			model.addObject("listaContratoPontoConsumos", listaContratoPontoConsumos);
			Collection<String> listaAnos = Util.listarAnos("20");
			((List<String>) listaAnos).remove(0);
			model.addObject("listaAnos", listaAnos);
		}

		
		model.addObject("contratoPontoConsumo", contratoPontoConsumo);
		model.addObject("pontoConsumo", pontoConsumo);


		if(cliente != null) {
			if(cliente.getCpf() != null && !cliente.getCpf().isEmpty()) {
				cliente.setCpf(cliente.getCpfFormatado());
			} else if(cliente.getCnpj() != null && !cliente.getCnpj().isEmpty()) {
				cliente.setCnpj(cliente.getCnpjFormatado());
			}
		}
		model.addObject(CLIENTE, cliente);

		/*
		 * List<FaturaVO> listaFaturaVO = (List<FaturaVO>)
		 * controladorFatura.listarFaturaPorPontoCosumo(idPontoConsumo);
		 * model.addObject("listaFaturaVO", listaFaturaVO);
		 */
		
		Collection<TitulosGSAVO> titulosGSA = controladorFatura.obterTitulosGSAPorPontoConsumo(pontoConsumo);
		
		if(situacaoTitulo != null && !situacaoTitulo.isEmpty()) {
			titulosGSA = titulosGSA.stream().filter(p -> p.getSituacaoTitulo().equals(situacaoTitulo)).collect(Collectors.toList());
		}
		
		model.addObject("listaTitulosGSA", titulosGSA);

		/*Collection<HistoricoConsumoVO> listaHistoricoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumoLeituraMovimento(pontoConsumo
				.getChavePrimaria());
		model.addObject("listaHistoricoConsumo", listaHistoricoConsumo);
		*/
		
		Collection<HistoricoLeituraGSAVO> listaHistoricoLeituraGSA = controladorHistoricoMedicao.obterHistoricoMedicaoGSAPorPontoConsumo(pontoConsumo);
		model.addObject("listaHistoricoLeituraGSA", listaHistoricoLeituraGSA);

		// mandar para o controlador
		if(contratoPontoConsumo != null) {
			popularInformacoesContrato(model, contratoPontoConsumo);
		}
		popularInformacoesMedidor(model, pontoConsumo);
		popularInformacoesCorretorVazao(model, pontoConsumo);
		if(cliente != null) {
			popularInformacoesCobranca(model, cliente.getChavePrimaria(), idPontoConsumo);
		}
		popularInformacoesAgendamento(model, idPontoConsumo);

		return model;
	}

	/**
	 * Popular mapa dados.
	 *
	 * @param dados the dados
	 * @param request the request
	 * @throws GGASException 
	 * @throws NumberFormatException 
	 */
	private void popularMapaDados(Map<String, Object> dados, HttpServletRequest request) throws NumberFormatException, GGASException {

		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		dados.put("usuario", usuario);

		String chaveEquipe = request.getParameter(EQUIPE);
		if (chaveEquipe != null && !chaveEquipe.isEmpty() && !"-1".equals(chaveEquipe)) {
			Equipe equipe = ServiceLocator.getInstancia().getControladorEquipe().obterEquipe(Long.valueOf(chaveEquipe));
			dados.put(EQUIPE, equipe);
		}
		
		String idTurnoPreferencia = request.getParameter("idTurnoPreferencia");

		if(idTurnoPreferencia != null && !idTurnoPreferencia.isEmpty() && !"-1".equals(idTurnoPreferencia)) {
			EntidadeConteudo turnoPreferencia = Fachada.getInstancia().obterEntidadeConteudo(Long.valueOf(idTurnoPreferencia));
			dados.put("turnoPreferencia", turnoPreferencia);
		}

		String idMotivo = request.getParameter(ID_MOTIVO);
		if(idMotivo != null) {
			dados.put(ID_MOTIVO, idMotivo);
		}
		
		if(request.getSession().getAttribute("listaTitulosAnalise") != null) {
			dados.put("listaTitulosAnalise", request.getSession().getAttribute("listaTitulosAnalise"));
		}
	}

	/**
	 * Exibir responder questionario.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param sessao the sessao
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirResponderQuestionario")
	public ModelAndView exibirResponderQuestionario(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpSession sessao,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirResponderQuestionarioChamado");
		Chamado chamado = null;
		try {
			chamado = controladorChamado.obterChamado(chavePrimaria);
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_RESPONDER_QUESTIONARIO);

			if (chamado.getChamadoAssunto().getQuestionario() == null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_SEM_QUESTIONARIO);
			}
			Questionario questionario = controladorQuestionario.consultarPorCodico(
					chamado.getChamadoAssunto().getQuestionario().getChavePrimaria(), LISTA_PERGUNTAS, "tipoServico", "segmento");

			boolean jaRespondeu = controladorResposta.questionarioRespondido(questionario.getChavePrimaria(), chamado);
			if (jaRespondeu) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_JA_RESPONDIDO);
			}

			chamado.setQuestionario(questionario);
			List<QuestionarioPergunta> listaPerguntas = new ArrayList<>(questionario.getListaPerguntas());
			carregarAlternativas(listaPerguntas);

			model.addObject(LISTA_PERGUNTAS, listaPerguntas);
			model.addObject(CHAMADO, chamado);
			model.addObject(QUESTIONARIO, questionario);

			ServicoAutorizacao servicoAltorizacao = controladorServicoAutorizacao.consultarServicoAutorizacaoPorChamado(chamado
					.getChavePrimaria());
			if(servicoAltorizacao != null) {
				model.addObject("chaveServicoAutorizacao", servicoAltorizacao.getChavePrimaria());
				if(servicoAltorizacao.getChamado() != null) {
					model.addObject("chamadoOrigem", servicoAltorizacao.getChamado());
				}
				if(servicoAltorizacao.getDataEncerramento() != null) {
					model.addObject(DATA_ENCERRAMENTO,
							Util.converterDataParaStringSemHora(servicoAltorizacao.getDataEncerramento(), Constantes.FORMATO_DATA_BR));
				}

			}
		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}
		return model;

	}

	/**
	 * Responder questionario.
	 *
	 * @param chamado
	 *            the chamado
	 * @param sessao
	 *            the sessao
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("responderQuestionario")
	public ModelAndView responderQuestionario(@ModelAttribute("Chamado") Chamado chamado, HttpSession sessao, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView("exibirResponderQuestionarioChamado");
		Map<String, Object> dados = new HashMap<>();
		List<QuestionarioPergunta> listaPerguntas = new ArrayList<>();
		Chamado chamadoNovo = null;

		try {
			int i = 0;
			chamadoNovo = controladorChamado.obterChamado(chamado.getChavePrimaria());
			Questionario questionario = null;
			if (chamadoNovo != null) {
				questionario = controladorQuestionario
						.consultarPorCodico(chamadoNovo.getChamadoAssunto().getQuestionario().getChavePrimaria(), LISTA_PERGUNTAS);
			}

			if (questionario != null) {
				boolean jaRespondeu = controladorResposta.questionarioRespondido(questionario.getChavePrimaria(), chamado);

				if (jaRespondeu) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_JA_RESPONDIDO);
				}

				listaPerguntas.addAll(questionario.getListaPerguntas());
			}

			carregarAlternativas(listaPerguntas);

			StringBuilder stringBuilder = new StringBuilder();

			for (QuestionarioPergunta pergunta : listaPerguntas) {
				if (pergunta.getObjetiva()) {
					validarPerguntaObjetiva(request, i, stringBuilder, pergunta);
				} else {
					validarPerguntaAberta(request, i, stringBuilder, pergunta);
				}
				i++;
			}
			String camposObrigatorios = stringBuilder.toString();

			if (camposObrigatorios.length() > 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_RESPOSTAS_OBRIGATORIOS, camposObrigatorios);
			}
			popularMapaDados(dados, request);
			controladorResposta.incluirResposta(chamadoNovo, dados, listaPerguntas);
			model = this.exibirPesquisaChamado(request);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Constantes.RESPOSTA);
		} catch (NegocioException e) {
			model = model.addObject(LISTA_PERGUNTAS, listaPerguntas);
			model.addObject(CHAMADO, chamadoNovo);
			ServicoAutorizacao servicoAltorizacao = null;
			if (chamadoNovo != null) {
				servicoAltorizacao = controladorServicoAutorizacao.consultarServicoAutorizacaoPorChamado(chamadoNovo.getChavePrimaria());
			}
			if (servicoAltorizacao != null) {
				model.addObject(DATA_ENCERRAMENTO,
						Util.converterDataParaStringSemHora(servicoAltorizacao.getDataEncerramento(), Constantes.FORMATO_DATA_BR));
			}
			mensagemErroParametrizado(model, e);
		} catch (ConcorrenciaException e) {

			LOG.error(e.getMessage(), e);
		} catch (GGASException e) {
			model.addObject(LISTA_PERGUNTAS, listaPerguntas);
			if (chamadoNovo != null) {
				model.addObject(CHAMADO, chamadoNovo);
				ServicoAutorizacao servicoAltorizacao =
						controladorServicoAutorizacao.consultarServicoAutorizacaoPorChamado(chamadoNovo.getChavePrimaria());
				model.addObject(DATA_ENCERRAMENTO,
						Util.converterDataParaStringSemHora(servicoAltorizacao.getDataEncerramento(), Constantes.FORMATO_DATA_BR));
			}
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Validar pergunta aberta
	 *
	 * @param request
	 * @param i
	 * @param stringBuilder
	 * @param pergunta
	 */
	private void validarPerguntaAberta(HttpServletRequest request, int i, StringBuilder stringBuilder, QuestionarioPergunta pergunta) {
		pergunta.setDescricaoResposta(request.getParameter("respostaDiscursiva" + i));
		if (pergunta.getRespostaObrigatoria() != null && pergunta.getRespostaObrigatoria()
				&& StringUtils.isEmpty(pergunta.getDescricaoResposta())) {
			stringBuilder.append(i + 1);
			stringBuilder.append(",");
		}
	}

	/**
	 * Validar pergunta Objetiva
	 *
	 * @param request
	 * @param i
	 * @param stringBuilder
	 * @param pergunta
	 */
	private void validarPerguntaObjetiva(HttpServletRequest request, int i, StringBuilder stringBuilder, QuestionarioPergunta pergunta) {
		if (pergunta.getValoresPersonalizados()) {
			String[] alternativaId = request.getParameterValues("alternativa" + i);

			if (alternativaId != null && alternativaId.length > 0) {

				Collection<QuestionarioAlternativa> alternativasSelecionadas =
						controladorQuestionario.obterAlternativas(Util.arrayStringParaArrayLong(alternativaId));
				pergunta.setListaAlternativasSelecionadas(alternativasSelecionadas);
			}else if (pergunta.getRespostaObrigatoria() != null && pergunta.getRespostaObrigatoria()){
				stringBuilder.append(i + 1);
				stringBuilder.append(",");
			}

		} else {
			String numero = request.getParameter("resposta" + i);
			if (numero != null) {
				pergunta.setNotaPergunta(Integer.parseInt(numero));
			} else if (pergunta.getRespostaObrigatoria() != null && pergunta.getRespostaObrigatoria()) {
				stringBuilder.append(i + 1);
				stringBuilder.append(",");
			}
		}
	}

	/**
	 * Ordena e carrega as alternativas da lista de pergunta
	 *
	 * @param listaPerguntas
	 */
	private void carregarAlternativas(List<QuestionarioPergunta> listaPerguntas) {
		Collections.sort(listaPerguntas);

		for (QuestionarioPergunta pergunta : listaPerguntas) {
			pergunta.setListaAlternativas(controladorQuestionario.obterAlternativas(pergunta.getChavePrimaria()));
		}
	}

	/**
	 * Exibir pesquisa relatorio chamado.
	 *
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRelatorioChamado")
	public ModelAndView exibirPesquisaRelatorioChamado(HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioChamado");

		ChamadoVO chamadoVO = new ChamadoVO();
		chamadoVO.setCondominioImovel(null);
		chamadoVO.setListaStatus(new ArrayList<String>());
		chamadoVO.getListaStatus().add("Aberto");
		chamadoVO.getListaStatus().add("Em atendimento");
		model.addObject(CHAMADO_VO, chamadoVO);

		carregarCombos(model);
		removerDadosSessao(request);
		return model;
	}

	/**
	 * Imprimir relatorio chamado cliente ponto consumo.
	 *
	 * @param chamadoVO the chamado vo
	 * @param tipoRelatorio the tipo relatorio
	 * @param exibirFiltros the exibir filtros
	 * @param modeloRelatorio the modelo relatorio
	 * @param request the request
	 * @param response the response
	 * @return the model and view
	 * @throws ParseException the parse exception
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("gerarRelatorioChamado")
	public ModelAndView imprimirRelatorioChamadoClientePontoConsumo(@ModelAttribute("ChamadoVO") ChamadoVO chamadoVO,
			@RequestParam("tipoRelatorio") String tipoRelatorio, @RequestParam("exibirFiltros") String exibirFiltros,
			@RequestParam("modeloRelatorio") String modeloRelatorio, HttpServletRequest request, HttpServletResponse response)
			throws GGASException {

		FormatoImpressao formatoImpressao = this.obterFornematoImpressao(tipoRelatorio);
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioChamado");
		String categoria = request.getParameter("categoriaChamado");

		if (StringUtils.isNotBlank(categoria) && !"-1".equals(categoria)) {
			chamadoVO.setCategoria(ChamadoCategoria.getByName(categoria));
		}
		
		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		if (usuario.getFuncionario().getUnidadeOrganizacional() != null) {
			chamadoVO.setIdUnidadeOrganizacionalUsuario(usuario.getFuncionario().getUnidadeOrganizacional().getChavePrimaria());
		}
		chamadoVO.setUsuarioAdministrador(usuario.isAdministradorAtendimento());

		model.addObject(CHAMADO_VO, chamadoVO);

		try {
			StringBuilder stringBuilder = new StringBuilder();
			validarDatasFormulario(chamadoVO, stringBuilder);
			String camposComErro = stringBuilder.toString();
			if(camposComErro.length() > 0) {
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, camposComErro);
			}
		} catch(NegocioException e) {
			model = new ModelAndView("exibirPesquisaRelatorioChamado");
			model.addObject(CHAMADO_VO, chamadoVO);
			carregarCombos(model);
			mensagemErroParametrizado(model, e);
			return model;
		}

		if (StringUtils.isNotEmpty(modeloRelatorio) && "analitico".equals(modeloRelatorio)) {
			return gerarRelatorioAnalitico(chamadoVO, exibirFiltros, response, formatoImpressao, model);
		} else if (StringUtils.isNotEmpty(modeloRelatorio) && "sintetico".equals(modeloRelatorio)) {
			return gerarRelatorioSintetico(chamadoVO, exibirFiltros, response, formatoImpressao, model);
		} else if (StringUtils.isNotEmpty(modeloRelatorio) && "quantitativo".equals(modeloRelatorio)){
			return gerarRelatorioQuantitativo(chamadoVO, exibirFiltros, response, formatoImpressao, model);
		}else {
			return gerarRelatorioGenerico(chamadoVO, exibirFiltros, response, formatoImpressao, model, modeloRelatorio);
		}
	}
	
	/**
	 * Funcao generica para refatorar e gerar os 4 relatorios de acordo com o modelo
	 *
	 * @param chamadoVO
	 * @param exibirFiltros
	 * @param response
	 * @param formatoImpressao
	 * @param model
	 * @param modeloRelatorio
	 * @return model and view
	 */
	private ModelAndView gerarRelatorioGenerico(ChamadoVO chamadoVO, String exibirFiltros, HttpServletResponse response,
			FormatoImpressao formatoImpressao, ModelAndView model, String modeloRelatorio) {
		byte[] relatorio;
		try {
			switch (modeloRelatorio) {
			case "sintetico":
				relatorio = controladorRelatorioChamado.gerarRelatorioChamadoClientePontoConsumo(chamadoVO, formatoImpressao, exibirFiltros);
				break;
			case "quantitativo":
				relatorio = controladorRelatorioChamado.gerarRelatorioChamadoQuantitativo(chamadoVO, formatoImpressao, exibirFiltros);
				break;
			case "resumo":
				relatorio = controladorRelatorioChamado.gerarRelatorioChamadoResumo(chamadoVO, formatoImpressao, exibirFiltros);
				break;
			default:
				relatorio = controladorRelatorioChamado.gerarRelatorioChamadoAnaltico(chamadoVO, formatoImpressao, exibirFiltros);
				break;
			}

			if(relatorio != null) {

				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				String diaMesAno = df.format(Calendar.getInstance().getTime());
				SimpleDateFormat formatoHora = new SimpleDateFormat("HH'h'mm");
				String hora = formatoHora.format(Calendar.getInstance().getTime());

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION,
						"attachment; filename=RelatóriodDosChamados_" + diaMesAno + "_" + hora + obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemAdvertencia(model, e.getChaveErro());

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		return model;
	}

	private ModelAndView gerarRelatorioAnalitico(ChamadoVO chamadoVO, String exibirFiltros,
			HttpServletResponse response, FormatoImpressao formatoImpressao, ModelAndView model) {
		byte[] relatorio;
		try {
			relatorio = controladorRelatorioChamado.gerarRelatorioChamadoAnaltico(chamadoVO, formatoImpressao, exibirFiltros);
			if(relatorio != null) {

				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				String diaMesAno = df.format(Calendar.getInstance().getTime());
				SimpleDateFormat formatoHora = new SimpleDateFormat("HH'h'mm");
				String hora = formatoHora.format(Calendar.getInstance().getTime());

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION,
						"attachment; filename=RelatóriodDosChamados_" + diaMesAno + "_" + hora + obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemAdvertencia(model, e.getChaveErro());

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		return model;
	}

	/**
	 * Gerar relatorio sintetico dos Chamados
	 *
	 * @param chamadoVO
	 * @param exibirFiltros
	 * @param response
	 * @param formatoImpressao
	 * @param model
	 * @return model and view
	 */
	private ModelAndView gerarRelatorioSintetico(ChamadoVO chamadoVO, String exibirFiltros, HttpServletResponse response,
			FormatoImpressao formatoImpressao, ModelAndView model) {
		byte[] relatorio;
		try {
			relatorio = controladorRelatorioChamado.gerarRelatorioChamadoClientePontoConsumo(chamadoVO, formatoImpressao, exibirFiltros);
			if(relatorio != null) {

				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				String diaMesAno = df.format(Calendar.getInstance().getTime());
				SimpleDateFormat formatoHora = new SimpleDateFormat("HH'h'mm");
				String hora = formatoHora.format(Calendar.getInstance().getTime());

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION,
						"attachment; filename=RelatóriodDosChamados_" + diaMesAno + "_" + hora + obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemAdvertencia(model, e.getChaveErro());

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		return model;
	}
	
	/**
	 * Gerar relatorio Quantitativo dos Chamados
	 *
	 * @param chamadoVO
	 * @param exibirFiltros
	 * @param response
	 * @param formatoImpressao
	 * @param model
	 * @return model and view
	 */
	private ModelAndView gerarRelatorioQuantitativo(ChamadoVO chamadoVO, String exibirFiltros, HttpServletResponse response,
			FormatoImpressao formatoImpressao, ModelAndView model) {
		byte[] relatorio;
		try {
			relatorio = controladorRelatorioChamado.gerarRelatorioChamadoQuantitativo(chamadoVO, formatoImpressao, exibirFiltros);
			if (relatorio != null) {

				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				String diaMesAno = df.format(Calendar.getInstance().getTime());
				SimpleDateFormat formatoHora = new SimpleDateFormat("HH'h'mm");
				String hora = formatoHora.format(Calendar.getInstance().getTime());

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION,
						"attachment; filename=RelatóriodDosChamados_" + diaMesAno + "_" + hora + obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemAdvertencia(model, e.getChaveErro());

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		return model;
	}

	private void validarDatasFormulario(ChamadoVO chamadoVO, StringBuilder stringBuilder) {
		boolean periodoGeracaoIncompleto =
				StringUtils.isEmpty(chamadoVO.getPeriodoCriacao());
		boolean periodoEncerramentoIncompleto =
				StringUtils.isEmpty(chamadoVO.getPeriodoPrevisaoEncerramento());
		boolean periodoResolucaoIncompleto =
				StringUtils.isEmpty(chamadoVO.getPeriodoResolucao());
		
		boolean dataGeracaoIncompleto =
				StringUtils.isEmpty(chamadoVO.getDataInicioCriacao()) || StringUtils.isEmpty(chamadoVO.getDataFimCriacao());
		boolean dataEncerramentoIncompleto =
				StringUtils.isEmpty(chamadoVO.getDataInicioEncerramento()) || StringUtils.isEmpty(chamadoVO.getDataFimEncerramento());
		boolean datalucaoIncompleto =
				StringUtils.isEmpty(chamadoVO.getDataInicioResolucao()) || StringUtils.isEmpty(chamadoVO.getDataFimResolucao());
		
		
		if (dataGeracaoIncompleto && dataEncerramentoIncompleto && datalucaoIncompleto && periodoGeracaoIncompleto && periodoEncerramentoIncompleto && periodoResolucaoIncompleto) {
			stringBuilder.append(PERIODO_GERACAO);
			acrescentarPeriodo(stringBuilder, PERIODO_RESOLUCAO);
			acrescentarPeriodo(stringBuilder, PERIODO_ENCERRAMENTO);
		} else {
			if (((chamadoVO.getDataInicioCriacao() != null && !chamadoVO.getDataInicioCriacao().isEmpty())
					|| (chamadoVO.getDataFimCriacao() != null && !chamadoVO.getDataFimCriacao().isEmpty()))
					&& (!Util.isDataValida(chamadoVO.getDataInicioCriacao(), Constantes.FORMATO_DATA_BR)
					|| !Util.isDataValida(chamadoVO.getDataFimCriacao(), Constantes.FORMATO_DATA_BR))) {
				stringBuilder.append(PERIODO_GERACAO);
			}

			if (((chamadoVO.getDataInicioEncerramento() != null && !chamadoVO.getDataInicioEncerramento().isEmpty())
					|| (chamadoVO.getDataFimEncerramento() != null && !chamadoVO.getDataFimEncerramento().isEmpty()))
					&& (!Util.isDataValida(chamadoVO.getDataInicioEncerramento(), Constantes.FORMATO_DATA_BR)
					|| !Util.isDataValida(chamadoVO.getDataFimEncerramento(), Constantes.FORMATO_DATA_BR))) {
				acrescentarPeriodo(stringBuilder, PERIODO_ENCERRAMENTO);
			}

			if (((chamadoVO.getDataInicioResolucao() != null && !chamadoVO.getDataInicioResolucao().isEmpty())
					|| (chamadoVO.getDataFimResolucao() != null && !chamadoVO.getDataFimResolucao().isEmpty()))
					&& (!Util.isDataValida(chamadoVO.getDataInicioResolucao(), Constantes.FORMATO_DATA_BR)
					|| !Util.isDataValida(chamadoVO.getDataFimResolucao(), Constantes.FORMATO_DATA_BR))) {
				acrescentarPeriodo(stringBuilder, PERIODO_RESOLUCAO);
			}
		}
	}

	private void acrescentarPeriodo(StringBuilder stringBuilder, String periodo) {
		Util.acrescentarConjuncao(stringBuilder);
		stringBuilder.append(periodo);
	}

	/**
	 * Obter fornemato impressao.
	 *
	 * @param formato
	 *            the formato
	 * @return the formato impressao
	 */
	private FormatoImpressao obterFornematoImpressao(String formato) {

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		if(formato.equalsIgnoreCase(FormatoImpressao.XLS.toString())) {
			formatoImpressao = FormatoImpressao.XLS;
		} else {
			if(formato.equalsIgnoreCase(FormatoImpressao.RTF.toString())) {
				formatoImpressao = FormatoImpressao.RTF;
			}
		}
		return formatoImpressao;
	}

	/**
	 * Exibir alteracao cadastro cliente chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping(TELA_EXIBIR_ALTERACAO_CADASTRO_CLIENTE_CHAMADO)
	public ModelAndView exibirAlteracaoCadastroClienteChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request, HttpServletResponse response, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_ALTERACAO_CADASTRO_CLIENTE_CHAMADO);
		sessao.removeAttribute("mapaCamposListaAlteradosSessaoContato");
		sessao.removeAttribute("checkBoxMarcadosListaMapaContato");

		try {
			Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
			ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO);
			ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(Long.valueOf(constante.getValor()));
			if (chamado == null || chamado.getChamadoAssunto() == null || !chamadoAssunto.equals(chamado.getChamadoAssunto())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_CADASTRO, true);
			}
			model.addObject(CHAMADO, chamado);
			ChamadoAlteracaoCliente chamadoAlteracaoCliente = controladorCliente.obterChamadoAlteracaoClientePorChamado(
					chamado.getChavePrimaria(), CLIENTE, "enderecosChamados", "enderecosChamados.tipoEndereco",
					"enderecosChamados.cep", "enderecosChamados.clienteEndereco", "enderecosChamados.clienteEndereco.cep",
					"enderecosChamados.tipoAcao", "fonesChamados", "fonesChamados.tipoFone", "contatosChamados", "anexosChamados",
					"anexosChamados.abaAnexo", "anexosChamados.tipoAcao", "anexosChamados.tipoAnexo",
					"anexosChamados.clienteAnexo", "cliente.anexos", "cliente.anexos.abaAnexo", "cliente.anexos.tipoAnexo",
					"tipoCliente", "cliente.enderecos", "cliente.fones", "cliente.contatos", "contatosChamados.tipoAcao",
					"contatosChamados.profissao", "contatosChamados.tipoContato", "contatosChamados.contatoCliente",
					"nacionalidade", "sexo", "cliente.nacionalidade", "cliente.sexo", "orgaoExpedidor", "cliente.orgaoExpedidor");

			if(chamadoAlteracaoCliente.getDataEmissaoRG() != null) {
				chamadoAlteracaoCliente.setDataEmissaoRGString(Util.converterDataParaStringSemHora(
						chamadoAlteracaoCliente.getDataEmissaoRG(), Constantes.FORMATO_DATA_BR));
			}

			if(chamadoAlteracaoCliente.getDataNascimento() != null) {
				chamadoAlteracaoCliente.setDataNascimentoString(Util.converterDataParaStringSemHora(
						chamadoAlteracaoCliente.getDataNascimento(), Constantes.FORMATO_DATA_BR));
			}
			if(chamadoAlteracaoCliente.getCliente().getDataEmissaoRG() != null) {
				chamadoAlteracaoCliente.setDataEmissaoRGClienteString(Util.converterDataParaStringSemHora(chamadoAlteracaoCliente
						.getCliente().getDataEmissaoRG(), Constantes.FORMATO_DATA_BR));
			}
			if(chamadoAlteracaoCliente.getCliente().getDataNascimento() != null) {
				chamadoAlteracaoCliente.setDataNascimentoClienteString(Util.converterDataParaStringSemHora(chamadoAlteracaoCliente
						.getCliente().getDataNascimento(), Constantes.FORMATO_DATA_BR));
			}

			this.exibirListasGerais(request, controladorCliente);
			this.exibirAbaEndereco(request, controladorCliente);
			this.exibirDadosAbaDocumentacao(request, controladorCliente);

			sessao.removeAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_FONE);

			ConstanteSistema constanteSistemaInclusao = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_TIPO_ACAO_INCLUSAO);
			ConstanteSistema constanteSistemaAlteracao = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_TIPO_ACAO_ALTERACAO);

			ConstanteSistema constanteSistemaExclusao = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_TIPO_ACAO_EXCLUSAO);

			sessao.setAttribute("codigoInclusao", constanteSistemaInclusao.getValor());
			sessao.setAttribute("codigoAlteracao", constanteSistemaAlteracao.getValor());

			ConstanteSistema constanteSistemaAbaAnexoIdentificacao = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_ABA_ANEXO_IDENTIFICACAO);
			ConstanteSistema constanteSistemaAbaAnexoEndereco = controladorConstanteSistema
					.obterConstantePorCodigo(Constantes.C_ABA_ANEXO_ENDERECO);

			model.addObject("chamadoAlteracaoCliente", chamadoAlteracaoCliente);

			// endereco
			model.addObject("listaChamadoAlteracaoClienteEndereco", chamadoAlteracaoCliente.getEnderecosChamados());

			model.addObject("listaChamadoAlteracaoClienteEnderecoAnexoAbaEndereco",
					this.preencherListaChamadoClienteAbaAnexo(chamadoAlteracaoCliente.getAnexosChamados(),
							Long.valueOf(constanteSistemaAbaAnexoEndereco.getValor())));

			model.addObject("listaClienteEnderecoAnexoAbaEndereco",
					this.preencherListaAbaAnexo(chamadoAlteracaoCliente.getCliente().getAnexos(),
							Long.valueOf(constanteSistemaAbaAnexoIdentificacao.getValor())));
			model.addObject("listaClienteEndereco", chamadoAlteracaoCliente.getCliente().getEnderecos());

			model.addObject("listaChamadoAlteracaoClienteAnexoAbaIdentificacao",
					this.preencherListaChamadoClienteAbaAnexo(chamadoAlteracaoCliente.getAnexosChamados(),
							Long.valueOf(constanteSistemaAbaAnexoIdentificacao.getValor())));
			model.addObject("listaClienteAnexoAbaIdentificacao",
					this.preencherListaAbaAnexo(chamadoAlteracaoCliente.getCliente().getAnexos(),
							Long.valueOf(constanteSistemaAbaAnexoIdentificacao.getValor())));

			// fone
			model.addObject("listaTipoFone", controladorCliente.listarTipoFone());
			model.addObject("listaChamadoAlteracaoClienteFone", chamadoAlteracaoCliente.getFonesChamados());
			model.addObject("listaClienteFones", chamadoAlteracaoCliente.getCliente().getFones());

			// contato
			model.addObject("listaChamadoAlteracaoClienteContato", chamadoAlteracaoCliente.getContatosChamados());
			model.addObject("listaClienteContato", chamadoAlteracaoCliente.getCliente().getContatos());

			this.preencherChavePrimariaClienteEndereco(chamadoAlteracaoCliente.getEnderecosChamados(),
					Long.valueOf(constanteSistemaInclusao.getValor()), Long.valueOf(constanteSistemaExclusao.getValor()), sessao);
			this.preencherChavePrimariaClienteFone(chamadoAlteracaoCliente.getFonesChamados(),
					Long.valueOf(constanteSistemaInclusao.getValor()), Long.valueOf(constanteSistemaExclusao.getValor()), sessao);
			this.preencherChavePrimariaClienteContato(chamadoAlteracaoCliente.getContatosChamados(),
					Long.valueOf(constanteSistemaInclusao.getValor()), Long.valueOf(constanteSistemaExclusao.getValor()), sessao);
			this.preencherChavePrimariaClienteAnexo(chamadoAlteracaoCliente.getAnexosChamados(),
					Long.valueOf(constanteSistemaInclusao.getValor()), Long.valueOf(constanteSistemaExclusao.getValor()), sessao);

			ParametroSistema parametroQuantidadeAnosVigenciaFeriado = controladorParametroSistema
					.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_VIGENCIA_FERIADO);

			request.setAttribute("intervaloAnosData", Util.intervaloAnosData(parametroQuantidadeAnosVigenciaFeriado.getValor()));

		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Preencher lista aba anexo.
	 *
	 * @param listaClienteAnexo
	 *            the lista cliente anexo
	 * @param codigoConstanteSistemaAbaAnexo
	 *            the codigo constante sistema aba anexo
	 * @return the collection
	 */
	@SuppressWarnings({"rawtypes"})
	public Collection<ClienteAnexo> preencherListaAbaAnexo(Collection<ClienteAnexo> listaClienteAnexo,
			Long codigoConstanteSistemaAbaAnexo) {

		List<ClienteAnexo> listaClienteAnexoAux = new ArrayList<>();

		for (Iterator iterator = listaClienteAnexo.iterator(); iterator.hasNext();) {

			ClienteAnexo clienteAnexo = (ClienteAnexo) iterator.next();
			if(codigoConstanteSistemaAbaAnexo == clienteAnexo.getAbaAnexo().getChavePrimaria()) {
				listaClienteAnexoAux.add(clienteAnexo);
			}
		}

		return listaClienteAnexoAux;
	}

	/**
	 * Preencher lista chamado cliente aba anexo.
	 *
	 * @param listaClienteAnexo
	 *            the lista cliente anexo
	 * @param codigoConstanteSistemaAbaAnexo
	 *            the codigo constante sistema aba anexo
	 * @return the collection
	 */
	@SuppressWarnings({"rawtypes"})
	public Collection<ChamadoClienteAnexo> preencherListaChamadoClienteAbaAnexo(Collection<ChamadoClienteAnexo> listaClienteAnexo,
			Long codigoConstanteSistemaAbaAnexo) {

		List<ChamadoClienteAnexo> listaChamadoClienteAnexo = new ArrayList<>();

		for (Iterator iterator = listaClienteAnexo.iterator(); iterator.hasNext();) {

			ChamadoClienteAnexo chamadoClienteAnexo = (ChamadoClienteAnexo) iterator.next();
			if(codigoConstanteSistemaAbaAnexo == chamadoClienteAnexo.getAbaAnexo().getChavePrimaria()) {
				listaChamadoClienteAnexo.add(chamadoClienteAnexo);
			}
		}

		return listaChamadoClienteAnexo;
	}

	/**
	 * Preencher chave primaria cliente endereco.
	 *
	 * @param listaObjetos
	 *            the lista objetos
	 * @param codigoInclusao
	 *            the codigo inclusao
	 * @param codigoExclusao
	 *            the codigo exclusao
	 * @param sessao
	 *            the sessao
	 */
	public void preencherChavePrimariaClienteEndereco(Collection<?> listaObjetos, Long codigoInclusao, Long codigoExclusao,
			HttpSession sessao) {

		List<Long> listaRegistrosExcluidos = new ArrayList<>();

		if (listaObjetos != null && !listaObjetos.isEmpty()) {
			for (Object objeto : listaObjetos) {

				ChamadoAlteracaoClienteEndereco chamadoAlteracaoClienteEndereco = (ChamadoAlteracaoClienteEndereco) objeto;

				if(chamadoAlteracaoClienteEndereco.getTipoAcao() != null
						&& chamadoAlteracaoClienteEndereco.getTipoAcao().getChavePrimaria() == codigoInclusao) {

					ClienteEndereco clienteEndereco = (ClienteEndereco) controladorCliente.criarClienteEndereco();
					clienteEndereco.setChavePrimaria(contadorAuxiliar);
					chamadoAlteracaoClienteEndereco.setClienteEndereco(clienteEndereco);

					contadorAuxiliar += -1;

				} else if (chamadoAlteracaoClienteEndereco.getTipoAcao() != null
						&& chamadoAlteracaoClienteEndereco.getTipoAcao().getChavePrimaria() == codigoExclusao
						&& chamadoAlteracaoClienteEndereco.getClienteEndereco() != null) {
					listaRegistrosExcluidos.add(chamadoAlteracaoClienteEndereco.getClienteEndereco().getChavePrimaria());
				}
			}

			if (!listaRegistrosExcluidos.isEmpty()) {
				sessao.setAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ENDERECO, listaRegistrosExcluidos);
			}
		}

	}

	/**
	 * Preencher chave primaria cliente contato.
	 *
	 * @param listaObjetos
	 *            the lista objetos
	 * @param codigoInclusao
	 *            the codigo inclusao
	 * @param codigoExclusao
	 *            the codigo exclusao
	 * @param sessao
	 *            the sessao
	 */
	public void preencherChavePrimariaClienteContato(Collection<?> listaObjetos, Long codigoInclusao, Long codigoExclusao,
			HttpSession sessao) {

		List<Long> listaRegistrosExcluidos = new ArrayList<>();

		if (listaObjetos != null && !listaObjetos.isEmpty()) {
			for (Object objeto : listaObjetos) {

				ChamadoAlteracaoClienteContato chamadoAlteracaoClienteContato = (ChamadoAlteracaoClienteContato) objeto;

				if(chamadoAlteracaoClienteContato.getTipoAcao() != null
						&& chamadoAlteracaoClienteContato.getTipoAcao().getChavePrimaria() == codigoInclusao) {

					ContatoCliente clienteContato = (ContatoCliente) controladorCliente.criarContatoCliente();
					clienteContato.setChavePrimaria(contadorAuxiliar);
					chamadoAlteracaoClienteContato.setContatoCliente(clienteContato);
					contadorAuxiliar += -1;

				} else if (chamadoAlteracaoClienteContato.getTipoAcao() != null
						&& chamadoAlteracaoClienteContato.getTipoAcao().getChavePrimaria() == codigoExclusao
						&& chamadoAlteracaoClienteContato.getContatoCliente() != null) {
					listaRegistrosExcluidos.add(chamadoAlteracaoClienteContato.getContatoCliente().getChavePrimaria());
				}
			}

			if (!listaRegistrosExcluidos.isEmpty()) {
				sessao.setAttribute(LISTA_REGISTROS_EXCLUIDOS_CONTATO_CLIENTE, listaRegistrosExcluidos);
			}
		}

	}

	/**
	 * Preencher chave primaria cliente fone.
	 *
	 * @param listaObjetos
	 *            the lista objetos
	 * @param codigoInclusao
	 *            the codigo inclusao
	 * @param codigoExclusao
	 *            the codigo exclusao
	 * @param sessao
	 *            the sessao
	 */
	public void preencherChavePrimariaClienteFone(Collection<?> listaObjetos, Long codigoInclusao, Long codigoExclusao, HttpSession sessao) {

		List<Long> listaRegistrosExcluidos = new ArrayList<>();

		if (listaObjetos != null && !listaObjetos.isEmpty()) {
			for (Object objeto : listaObjetos) {

				ChamadoAlteracaoClienteTelefone chamadoAlteracaoClienteTelefone = (ChamadoAlteracaoClienteTelefone) objeto;

				if(chamadoAlteracaoClienteTelefone.getTipoAcao() != null
						&& chamadoAlteracaoClienteTelefone.getTipoAcao().getChavePrimaria() == codigoInclusao) {

					ClienteFone clienteTelefone = (ClienteFone) controladorCliente.criarClienteFone();
					clienteTelefone.setChavePrimaria(contadorAuxiliar);
					contadorAuxiliar += -1;
					chamadoAlteracaoClienteTelefone.setClienteFone(clienteTelefone);

				} else if (chamadoAlteracaoClienteTelefone.getTipoAcao() != null
						&& chamadoAlteracaoClienteTelefone.getTipoAcao().getChavePrimaria() == codigoExclusao
						&& chamadoAlteracaoClienteTelefone.getClienteFone() != null) {
					listaRegistrosExcluidos.add(chamadoAlteracaoClienteTelefone.getClienteFone().getChavePrimaria());
				}
			}

			if (!listaRegistrosExcluidos.isEmpty()) {
				sessao.setAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_FONE, listaRegistrosExcluidos);
			}
		}

	}

	/**
	 * Preencher chave primaria cliente anexo.
	 *
	 * @param listaObjetos
	 *            the lista objetos
	 * @param codigoInclusao
	 *            the codigo inclusao
	 * @param codigoExclusao
	 *            the codigo exclusao
	 * @param sessao
	 *            the sessao
	 */
	public void preencherChavePrimariaClienteAnexo(Collection<?> listaObjetos, Long codigoInclusao, Long codigoExclusao, HttpSession sessao) {

		List<Long> listaRegistrosExcluidos = new ArrayList<>();

		if (listaObjetos != null && !listaObjetos.isEmpty()) {
			for (Object objeto : listaObjetos) {

				ChamadoClienteAnexo chamadoClienteAnexo = (ChamadoClienteAnexo) objeto;

				if(chamadoClienteAnexo.getTipoAcao() != null && chamadoClienteAnexo.getTipoAcao().getChavePrimaria() == codigoInclusao) {

					ClienteAnexo clienteAnexo = (ClienteAnexo) controladorCliente.criarClienteAnexo();
					clienteAnexo.setChavePrimaria(contadorAuxiliar);
					contadorAuxiliar += -1;
					chamadoClienteAnexo.setClienteAnexo(clienteAnexo);

				} else if (chamadoClienteAnexo.getTipoAcao() != null
						&& chamadoClienteAnexo.getTipoAcao().getChavePrimaria() == codigoExclusao
						&& chamadoClienteAnexo.getClienteAnexo() != null) {
					listaRegistrosExcluidos.add(chamadoClienteAnexo.getClienteAnexo().getChavePrimaria());
				}
			}

			if (!listaRegistrosExcluidos.isEmpty()) {
				sessao.setAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ANEXO, listaRegistrosExcluidos);
			}
		}

	}

	/**
	 * Visualizar anexo aba endereco.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param sessao
	 *            the sessao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("visualizarAnexoAbaEndereco")
	public void visualizarAnexoAbaEndereco(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response, HttpSession sessao) throws GGASException {

		ChamadoClienteAnexo chamadoClienteAnexo = (ChamadoClienteAnexo) controladorCliente.obter(chavePrimaria, ChamadoClienteAnexo.class);

		byte[] anexo = chamadoClienteAnexo.getDocumentoAnexo();

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
		Date dataAtual = Calendar.getInstance().getTime();

		ServletOutputStream servletOutputStream;
		try {
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(anexo.length);

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual) + "_";
			response.addHeader(CONTENT_DISPOSITION,
					"attachment; filename=anexo" + "_" + data + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(anexo, 0, anexo.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch(IOException e) {
			throw new GGASException(e);
		}

	}

	/**
	 * Visualizar anexo aba endereco cliente.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param sessao
	 *            the sessao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("visualizarAnexoAbaEnderecoCliente")
	public void visualizarAnexoAbaEnderecoCliente(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpServletRequest request,
			HttpServletResponse response, HttpSession sessao) throws GGASException {

		ClienteAnexo clienteAnexo = (ClienteAnexo) controladorCliente.obter(chavePrimaria, ClienteAnexoImpl.class);

		byte[] anexo = clienteAnexo.getDocumentoAnexo();

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
		Date dataAtual = Calendar.getInstance().getTime();

		ServletOutputStream servletOutputStream;
		try {
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(anexo.length);

			String data = Util.converterDataParaStringAnoMesDiaSemCaracteresEspeciais(dataAtual) + "_";
			response.addHeader(CONTENT_DISPOSITION,
					"attachment; filename=anexo" + "_" + data + this.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(anexo, 0, anexo.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch(IOException e) {
			throw new GGASException(e);
		}
	}

	/**
	 * Exibir listas gerais.
	 *
	 * @param request
	 *            the request
	 * @param controladorCliente
	 *            the controlador cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void exibirListasGerais(HttpServletRequest request, ControladorCliente controladorCliente) throws NegocioException {

		request.setAttribute(TIPOS_CLIENTES, controladorCliente.listarTipoCliente());
		request.setAttribute(SITUACOES_CLIENTES, controladorCliente.listarClienteSituacao());
		request.setAttribute(TIPO_PESSOA_FISICA, Constantes.PESSOA_FISICA);
		request.setAttribute(LISTA_TIPO_CONTATO, controladorCliente.listarTipoContato());
		request.setAttribute(TIPO_PROFISSAO, controladorCliente.listarProfissao());

	}

	/**
	 * Exibir aba endereco.
	 *
	 * @param request
	 *            the request
	 * @param controladorCliente
	 *            the controlador cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void exibirAbaEndereco(HttpServletRequest request, ControladorCliente controladorCliente) throws NegocioException {

		request.setAttribute(TIPO_ENDERECO, controladorCliente.listarTipoEndereco());

	}

	/**
	 * Preencher mapa alteracao cadastro cliente chamado.
	 *
	 * @param idCampo
	 *            the id campo
	 * @param valorChecked
	 *            the valor checked
	 * @param valor
	 *            the valor
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("preencherMapaAlteracaoCadastroClienteChamado")
	public ModelAndView preencherMapaAlteracaoCadastroClienteChamado(@RequestParam("idCampo") String idCampo,
			@RequestParam("valorChecked") String valorChecked, @RequestParam("valor") String valor, HttpServletRequest request,
			HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_ALTERACAO_CADASTRO_CLIENTE_CHAMADO);

		String[] nomeCampo = idCampo.split("Checkbox");
		Map<String, Object> mapaCamposAlteradosSessao = (Map<String, Object>) sessao.getAttribute(MAPA_CAMPOS_ALTERADOS_SESSAO);

		if (mapaCamposAlteradosSessao != null && !mapaCamposAlteradosSessao.isEmpty()) {

			if(mapaCamposAlteradosSessao.containsKey(nomeCampo[0])) {
				if("true".equalsIgnoreCase(valorChecked)) {

					mapaCamposAlteradosSessao.put(nomeCampo[0], valor);
				} else {

					mapaCamposAlteradosSessao.remove(nomeCampo[0]);
				}
			} else {
				mapaCamposAlteradosSessao.put(nomeCampo[0], valor);
			}
		} else {
			mapaCamposAlteradosSessao = new LinkedHashMap<>();
			mapaCamposAlteradosSessao.put(nomeCampo[0], valor);
		}

		sessao.setAttribute(MAPA_CAMPOS_ALTERADOS_SESSAO, mapaCamposAlteradosSessao);

		return model;
	}

	/**
	 * Preencher mapa lista anexo alteracao cadastro cliente chamado.
	 *
	 * @param chavePrimariaChamadoAnexo
	 *            the chave primaria chamado anexo
	 * @param chavePrimariaAnexo
	 *            the chave primaria anexo
	 * @param valorChecked
	 *            the valor checked
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("preencherMapaListaAnexoAlteracaoCadastroClienteChamado")
	public ModelAndView preencherMapaListaAnexoAlteracaoCadastroClienteChamado(
			@RequestParam("chavePrimariaChamadoAnexo") Long chavePrimariaChamadoAnexo,
			@RequestParam("chavePrimariaAnexo") Long chavePrimariaAnexo, @RequestParam("valorChecked") String valorChecked,
			HttpServletRequest request, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_ALTERACAO_CADASTRO_CLIENTE_CHAMADO);

		Map<Long, Object> mapaCamposMapaListaAnexoAlteradosSessao = (Map<Long, Object>) sessao
				.getAttribute(MAPA_CAMPOS_MAPA_LISTA_ANEXO_ALTERADOS_SESSAO);

		ChamadoClienteAnexo chamadoClienteAnexo = (ChamadoClienteAnexo) controladorCliente.obter(chavePrimariaChamadoAnexo,
				ChamadoClienteAnexo.class, Boolean.TRUE, PROPRIEDADES_LAZY[ANEXO]);

		if(mapaCamposMapaListaAnexoAlteradosSessao != null && !mapaCamposMapaListaAnexoAlteradosSessao.isEmpty()) {

			if(mapaCamposMapaListaAnexoAlteradosSessao.containsKey(chavePrimariaAnexo)) {
				if("true".equalsIgnoreCase(valorChecked)) {

					mapaCamposMapaListaAnexoAlteradosSessao.put(chavePrimariaAnexo, chamadoClienteAnexo);
				} else {

					mapaCamposMapaListaAnexoAlteradosSessao.remove(chavePrimariaAnexo);
				}
			} else {
				mapaCamposMapaListaAnexoAlteradosSessao.put(chavePrimariaAnexo, chamadoClienteAnexo);
			}
		} else {
			mapaCamposMapaListaAnexoAlteradosSessao = new LinkedHashMap<>();
			mapaCamposMapaListaAnexoAlteradosSessao.put(chavePrimariaAnexo, chamadoClienteAnexo);
		}

		sessao.setAttribute(MAPA_CAMPOS_MAPA_LISTA_ANEXO_ALTERADOS_SESSAO, mapaCamposMapaListaAnexoAlteradosSessao);

		return model;
	}

	/**
	 * Preencher mapa lista alteracao cadastro cliente chamado.
	 *
	 * @param idCampo
	 *            the id campo
	 * @param valorChecked
	 *            the valor checked
	 * @param valor
	 *            the valor
	 * @param chavePrimariaListaCliente
	 *            the chave primaria lista cliente
	 * @param tipoMapa
	 *            the tipo mapa
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @param posFixoTipoCheckbox
	 *            the pos fixo tipo checkbox
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("preencherMapaListaAlteracaoCadastroClienteChamado")
	public void preencherMapaListaAlteracaoCadastroClienteChamado(@RequestParam("idCampo") String idCampo,
			@RequestParam("valorChecked") String valorChecked, @RequestParam("valor") String valor,
			@RequestParam("chavePrimariaClienteEndereco") Long chavePrimariaListaCliente,
			@RequestParam("tipoMapa") String tipoMapa, HttpServletRequest request, HttpSession sessao,
			@RequestParam("posFixoTipoCheckbox") String posFixoTipoCheckbox) {

		String[] nomeCampo = idCampo.split(posFixoTipoCheckbox);
		Map<Long, Map<String, Object>> mapaCamposListaAlteradosSessao = (Map<Long, Map<String, Object>>) sessao
				.getAttribute(MAPA_CAMPOS_LISTA_ALTERADOS_SESSAO + tipoMapa);

		Map<String, Object> mapaCampos = null;
		List<String> listaNomeCampo = null;

		Map<Long, List<String>> checkBoxMarcadosListaMapa = (Map<Long, List<String>>) sessao.getAttribute(CHECK_BOX_MARCADOS_LISTA_MAPA
				+ tipoMapa);

		if (mapaCamposListaAlteradosSessao != null && !mapaCamposListaAlteradosSessao.isEmpty()) {

			mapaCampos = mapaCamposListaAlteradosSessao.get(chavePrimariaListaCliente);
			listaNomeCampo = checkBoxMarcadosListaMapa.get(chavePrimariaListaCliente);

			if(mapaCamposListaAlteradosSessao.containsKey(chavePrimariaListaCliente)){

				Collection<String> listaChaves = mapaCampos.keySet();

				if (listaChaves.contains(nomeCampo[0])) {

					this.verificaERemoveNomeCampoEChavePrimaria(valorChecked, mapaCampos, listaNomeCampo, nomeCampo,
							mapaCamposListaAlteradosSessao, chavePrimariaListaCliente);

				} else {
					this.verificaEAdicionaValoresCampos(valorChecked, mapaCampos, listaNomeCampo, nomeCampo, mapaCamposListaAlteradosSessao,
							chavePrimariaListaCliente, valor, checkBoxMarcadosListaMapa);
				}
			} else {
				if("true".equalsIgnoreCase(valorChecked)) {
					mapaCampos = new LinkedHashMap<>();
					mapaCampos.put(nomeCampo[0], valor);
					mapaCamposListaAlteradosSessao.put(chavePrimariaListaCliente, mapaCampos);

					listaNomeCampo = new ArrayList<>();
					listaNomeCampo.add(nomeCampo[0]);
					checkBoxMarcadosListaMapa.put(chavePrimariaListaCliente, listaNomeCampo);
				}
			}
		} else {
			if("true".equalsIgnoreCase(valorChecked)) {

				mapaCamposListaAlteradosSessao = new LinkedHashMap<>();
				mapaCampos = new LinkedHashMap<>();

				mapaCampos.put(nomeCampo[0], valor);
				mapaCamposListaAlteradosSessao.put(chavePrimariaListaCliente, mapaCampos);

				checkBoxMarcadosListaMapa = new LinkedHashMap<>();
				listaNomeCampo = new ArrayList<>();

				listaNomeCampo.add(nomeCampo[0]);
				checkBoxMarcadosListaMapa.put(chavePrimariaListaCliente, listaNomeCampo);
			}
		}

		sessao.setAttribute(MAPA_CAMPOS_LISTA_ALTERADOS_SESSAO + tipoMapa, mapaCamposListaAlteradosSessao);
		sessao.setAttribute(CHECK_BOX_MARCADOS_LISTA_MAPA + tipoMapa, checkBoxMarcadosListaMapa);

	}

	/**
	 * Retornar check mapa.
	 *
	 * @param tipoMapa
	 *            the tipo mapa
	 * @param sessao
	 *            the sessao
	 * @return the map
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("retornarCheckMapa")
	@ResponseBody
	public Map<Long, List<String>> retornarCheckMapa(@RequestParam("tipoMapa") String tipoMapa, HttpSession sessao) {

		return (Map<Long, List<String>>) sessao.getAttribute(CHECK_BOX_MARCADOS_LISTA_MAPA + tipoMapa);
	}

	private void verificaERemoveNomeCampoEChavePrimaria(String valorChecked, Map<String, Object> mapaCampos, List<String> listaNomeCampo,
			String[] nomeCampo, Map<Long, Map<String, Object>> mapaCamposListaAlteradosSessao, Long chavePrimariaListaCliente) {

		if ("false".equalsIgnoreCase(valorChecked)) {

			mapaCampos.remove(nomeCampo[0]);
			listaNomeCampo.remove(nomeCampo[0]);

			if (mapaCampos.isEmpty()) {
				mapaCamposListaAlteradosSessao.remove(chavePrimariaListaCliente);
			}
		}
	}

	private void verificaEAdicionaValoresCampos(String valorChecked, Map<String, Object> mapaCampos, List<String> listaNomeCampo,
			String[] nomeCampo, Map<Long, Map<String, Object>> mapaCamposListaAlteradosSessao, Long chavePrimariaListaCliente, String valor,
			Map<Long, List<String>> checkBoxMarcadosListaMapa) {

		if ("true".equalsIgnoreCase(valorChecked)) {
			mapaCampos.put(nomeCampo[0], valor);
			mapaCamposListaAlteradosSessao.put(chavePrimariaListaCliente, mapaCampos);

			listaNomeCampo.add(nomeCampo[0]);
			checkBoxMarcadosListaMapa.put(chavePrimariaListaCliente, listaNomeCampo);
		}
	}


	/**
	 * Preencher lista mapa cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @param sessao
	 *            the sessao
	 * @param tipoMapa
	 *            the tipo mapa
	 * @param listaObjetos
	 *            the lista objetos
	 * @param listaChavesPrimariasRemover
	 *            the lista chaves primarias remover
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NoSuchFieldException
	 *             the no such field exception
	 * @throws SecurityException
	 *             the security exception
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@RequestMapping("preencherListaMapaCliente")
	public void preencherListaMapaCliente(Cliente cliente, HttpSession sessao, String tipoMapa, Collection<?> listaObjetos,
			List<Long> listaChavesPrimariasRemover) throws GGASException {

		Map<Long, Map<String, Object>> mapaCamposListaAlteradosSessao = (Map<Long, Map<String, Object>>) sessao
				.getAttribute(MAPA_CAMPOS_LISTA_ALTERADOS_SESSAO + tipoMapa);

		List<ClienteFone> listaObjetosFoneInserir = new ArrayList<>();
		List<ContatoCliente> listaObjetosContatoInserir = new ArrayList<>();
		List<ClienteEndereco> listaObjetosEnderecoInserir = new ArrayList<>();

		String objetoReferencia = listaObjetos.iterator().next().getClass().getName();

		if (mapaCamposListaAlteradosSessao != null && !mapaCamposListaAlteradosSessao.isEmpty()) {

			for (Entry<Long, Map<String, Object>> entry : mapaCamposListaAlteradosSessao.entrySet()) {

				Long chavePrimaria = entry.getKey();

				for (Iterator iterator = listaObjetos.iterator(); iterator.hasNext();) {

					EntidadeNegocio obj = (EntidadeNegocio) iterator.next();

					this.popularCamposObjeto(chavePrimaria, obj, entry);

					this.removerItemDeListaDeChavesPrimarias(listaChavesPrimariasRemover, obj, iterator);

				}

				// FONE
				if(chavePrimaria == null || chavePrimaria < 0) {

					Map<String, Object> mapaCampos = entry.getValue();

					verificarObjetoRefernciaClienteFone(listaObjetosFoneInserir, objetoReferencia, mapaCampos);

					// CONTATO
					verificarOjetoReferenciaContatoCliente(listaObjetosContatoInserir, objetoReferencia, mapaCampos);

					// ENDERECO
					verificarObjetoReferenciaClienteEndereco(listaObjetosEnderecoInserir, objetoReferencia, mapaCampos);
				}
			}

			if(objetoReferencia.equals(ClienteFoneImpl.class.getName())) {
				cliente.getFones().addAll(listaObjetosFoneInserir);
			}
			if(objetoReferencia.equals(ContatoClienteImpl.class.getName())) {
				cliente.getContatos().addAll(listaObjetosContatoInserir);
			}
			if(objetoReferencia.equals(ClienteEnderecoImpl.class.getName())) {
				cliente.getEnderecos().addAll(listaObjetosEnderecoInserir);
			}
		} else {

			// verifica se existe algum registro para excluir
			for (Iterator iterator = listaObjetos.iterator(); iterator.hasNext();) {
				EntidadeNegocio obj = (EntidadeNegocio) iterator.next();
				if (listaChavesPrimariasRemover != null && !listaChavesPrimariasRemover.isEmpty()
						&& listaChavesPrimariasRemover.contains(obj.getChavePrimaria())) {
					iterator.remove();
				}
			}
		}

	}



	/**
	 *
	 * @param listaObjetosContatoInserir
	 * @param objetoReferencia
	 * @param mapaCampos
	 * @throws GGASException
	 * @throws ReflectiveOperationException
	 */
	private void verificarOjetoReferenciaContatoCliente(List<ContatoCliente> listaObjetosContatoInserir,
			String objetoReferencia, Map<String, Object> mapaCampos)
			throws GGASException {
		if(objetoReferencia.equals(ContatoClienteImpl.class.getName())) {
			ContatoCliente contatoCliente = new ContatoClienteImpl();
			this.popularCamposObjeto(mapaCampos, contatoCliente);
			contatoCliente.setUltimaAlteracao(Calendar.getInstance().getTime());
			listaObjetosContatoInserir.add(contatoCliente);

		}
	}


	/**
	 *
	 * @param listaObjetosFoneInserir
	 * @param objetoReferencia
	 * @param mapaCampos
	 * @throws GGASException
	 * @throws ReflectiveOperationException
	 */
	private void verificarObjetoRefernciaClienteFone(List<ClienteFone> listaObjetosFoneInserir, String objetoReferencia,
			Map<String, Object> mapaCampos) throws GGASException {
		if(objetoReferencia.equals(ClienteFoneImpl.class.getName())) {
			ClienteFone clienteFone = new ClienteFoneImpl();
			this.popularCamposObjeto(mapaCampos, clienteFone);
			clienteFone.setUltimaAlteracao(Calendar.getInstance().getTime());
			listaObjetosFoneInserir.add(clienteFone);

		}
	}

	/**
	 *
	 * @param listaObjetosEnderecoInserir
	 * @param objetoReferencia
	 * @param mapaCampos
	 * @throws GGASException
	 * @throws ReflectiveOperationException
	 * @throws NegocioException
	 */
	private void verificarObjetoReferenciaClienteEndereco(List<ClienteEndereco> listaObjetosEnderecoInserir, String objetoReferencia,
			Map<String, Object> mapaCampos) throws GGASException {
		if(objetoReferencia.equals(ClienteEnderecoImpl.class.getName())) {
			ClienteEndereco clienteEndereco = new ClienteEnderecoImpl();
			this.popularCamposObjeto(mapaCampos, clienteEndereco);

			Cep cep = controladorEndereco.obterCepPorChave(clienteEndereco.getCep().getChavePrimaria());

			clienteEndereco.setMunicipio(controladorMunicipio.obterMunicipioPorNomeUF(cep.getNomeMunicipio(), cep.getUf()));
			clienteEndereco.setCorrespondencia(Boolean.FALSE);
			clienteEndereco.setUltimaAlteracao(Calendar.getInstance().getTime());
			listaObjetosEnderecoInserir.add(clienteEndereco);

		}
	}

	/**
	 * Preencher mapa lista anexo cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @param sessao
	 *            the sessao
	 * @param listaObjetos
	 *            the lista objetos
	 * @param listaRegistrosExcluidosClienteAnexo
	 *            the lista registros excluidos cliente anexo
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@RequestMapping("preencherMapaListaAnexoCliente")
	public void preencherMapaListaAnexoCliente(Cliente cliente, HttpSession sessao, Collection<?> listaObjetos,
			List<Long> listaRegistrosExcluidosClienteAnexo) {

		List<ClienteAnexo> listaObjetosAnexoInserir = new ArrayList<>();

		Map<Long, Object> mapaCamposMapaListaAnexoAlteradosSessao = (Map<Long, Object>) sessao
				.getAttribute(MAPA_CAMPOS_MAPA_LISTA_ANEXO_ALTERADOS_SESSAO);

		if(mapaCamposMapaListaAnexoAlteradosSessao != null &&  !mapaCamposMapaListaAnexoAlteradosSessao.isEmpty()) {

			for (Map.Entry<Long, Object> entry : mapaCamposMapaListaAnexoAlteradosSessao.entrySet()) {

				ChamadoClienteAnexo chamadoClienteAnexo = (ChamadoClienteAnexo) entry.getValue();
				Long chavePrimaria = entry.getKey();

				for (Iterator iterator = listaObjetos.iterator(); iterator.hasNext();) {

					ClienteAnexo clienteAnexo = (ClienteAnexo) iterator.next();

					this.popularClienteAnexo(chavePrimaria, clienteAnexo, chamadoClienteAnexo);

					this.removerItemDeListaDeChavesPrimarias(listaRegistrosExcluidosClienteAnexo, clienteAnexo, iterator);


				}

				// ANEXO
				if(chavePrimaria == null || chavePrimaria < 0) {

					ClienteAnexo clienteAnexo = this.popularClienteAnexo(chamadoClienteAnexo);
					clienteAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());
					listaObjetosAnexoInserir.add(clienteAnexo);

				}

			}

			cliente.getAnexos().addAll(listaObjetosAnexoInserir);

		} else {

			// verifica se existe algum registro para excluir
			for (Iterator iterator = listaObjetos.iterator(); iterator.hasNext();) {
				EntidadeNegocio obj = (EntidadeNegocio) iterator.next();
				if (listaRegistrosExcluidosClienteAnexo != null && !listaRegistrosExcluidosClienteAnexo.isEmpty()
						&& listaRegistrosExcluidosClienteAnexo.contains(obj.getChavePrimaria())) {
					iterator.remove();
				}
			}
		}
	}

	private void removerItemDeListaDeChavesPrimarias(List<Long> listaChavesPrimariasRemover, EntidadeNegocio obj, Iterator iterator) {
		if (listaChavesPrimariasRemover != null && !listaChavesPrimariasRemover.isEmpty()
				&& listaChavesPrimariasRemover.contains(obj.getChavePrimaria())) {
			iterator.remove();
		}
	}

	/**
	 * Popular cliente anexo.
	 *
	 * @param chamadoClienteAnexo
	 *            the chamado cliente anexo
	 * @return the cliente anexo
	 */
	private ClienteAnexo popularClienteAnexo(ChamadoClienteAnexo chamadoClienteAnexo) {

		ClienteAnexo clienteAnexo = (ClienteAnexo) controladorCliente.criarClienteAnexo();

		this.popularClienteAnexo(clienteAnexo, chamadoClienteAnexo);

		return clienteAnexo;
	}

	private void popularClienteAnexo(Long chavePrimaria, ClienteAnexo clienteAnexo, ChamadoClienteAnexo chamadoClienteAnexo ) {

		if(chavePrimaria != null && clienteAnexo.getChavePrimaria() == chavePrimaria) {
			this.popularClienteAnexo(clienteAnexo, chamadoClienteAnexo);
		}
	}

	private void popularClienteAnexo(ClienteAnexo clienteAnexo, ChamadoClienteAnexo chamadoClienteAnexo ) {

		clienteAnexo.setAbaAnexo(chamadoClienteAnexo.getAbaAnexo());
		clienteAnexo.setDescricaoAnexo(chamadoClienteAnexo.getDescricaoAnexo());

		clienteAnexo.setDocumentoAnexo(chamadoClienteAnexo.getDocumentoAnexo());
		clienteAnexo.setTipoAnexo(chamadoClienteAnexo.getTipoAnexo());
	}

	/**
	 * Salvar alteracao cadastro cliente chamado.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("salvarAlteracaoCadastroClienteChamado")
	public ModelAndView salvarAlteracaoCadastroClienteChamado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			@RequestParam("chavePrimariaCliente") Long chavePrimariaCliente, HttpServletRequest request, HttpSession sessao)
			throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);

		try {
			Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
			model.addObject(CHAMADO, chamado);
			Map<String, Object> mapaCamposAlteradosSessao = (Map<String, Object>) sessao.getAttribute(MAPA_CAMPOS_ALTERADOS_SESSAO);

			Cliente cliente = (Cliente) controladorCliente.obter(chavePrimariaCliente, Boolean.TRUE, "enderecos", "fones", "contatos",
					PROPRIEDADES_LAZY[ANEXO], "tipoCliente");

			controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_TIPO_ACAO_INCLUSAO);

			if (cliente.getEnderecos() != null && !cliente.getEnderecos().isEmpty()) {
				this.preencherListaMapaCliente(cliente, sessao, "Endereco", cliente.getEnderecos(),
						(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ENDERECO));
			}

			if (cliente.getFones() != null && !cliente.getFones().isEmpty()) {
				this.preencherListaMapaCliente(cliente, sessao, "Fones", cliente.getFones(),
						(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_FONE));
			}

			if(cliente.getContatos() != null && !cliente.getContatos().isEmpty()) {
				this.preencherListaMapaCliente(cliente, sessao, "Contato", cliente.getContatos(),
						(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CONTATO_CLIENTE));
			}

			if (cliente.getAnexos() != null && !cliente.getAnexos().isEmpty()) {
				this.preencherMapaListaAnexoCliente(cliente, sessao, cliente.getAnexos(),
						(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ANEXO));
			}

			this.popularCamposObjeto(mapaCamposAlteradosSessao, cliente);
			controladorCliente.atualizarDependencias((List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_FONE),
					chamado.getChavePrimaria());
			controladorCliente.atualizarDependenciasChamadoClienteContato(
					(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CONTATO_CLIENTE), chamado.getChavePrimaria());
			controladorCliente.atualizarDependenciasChamadoClienteEndereco(
					(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ENDERECO), chamado.getChavePrimaria());

			controladorCliente.atualizarDependenciasChamadoClienteAnexo(
					(List<Long>) sessao.getAttribute(LISTA_REGISTROS_EXCLUIDOS_CLIENTE_ANEXO), chamado.getChavePrimaria());

			controladorCliente.atualizar(cliente);

			Map<String, Object> dados = new LinkedHashMap<>();
			Long motivo = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_ENCERRAMENTO_CHAMADO));
			request.setAttribute(ID_MOTIVO, motivo);
			this.popularMapaDados(dados, request);
			controladorChamado.encerrarChamado(chamado, dados);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(GGASException e) {
			model = new ModelAndView("forward:/exibirAlteracaoCadastroClienteChamado");
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	private void popularCamposObjeto(Long chavePrimaria, EntidadeNegocio entidadeNegocio,
			Entry<Long, Map<String, Object>> entry) throws GGASException {

		if(chavePrimaria != null && entidadeNegocio.getChavePrimaria() == chavePrimaria) {

			Map<String, Object> mapaCampos = entry.getValue();

			this.popularCamposObjeto(mapaCampos, entidadeNegocio);
		}

	}
	/**
	 * Popular campos objeto.
	 *
	 * @param mapaCamposAlteradosSessao
	 *            the mapa campos alterados sessao
	 * @param objeto
	 *            the objeto
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularCamposObjeto(Map<String, Object> mapaCamposAlteradosSessao, Object objeto)
			throws GGASException {
		try {
			if (mapaCamposAlteradosSessao != null && !mapaCamposAlteradosSessao.isEmpty()) {

				for (Map.Entry<String, Object> entry : mapaCamposAlteradosSessao.entrySet()) {

					Field field = objeto.getClass().getDeclaredField(entry.getKey());
					Object valor;

					if (!(field.getType().isPrimitive() || Util.isWrapperType(field.getType()))) {

						valor = this.adicionandoChavePrimariaEmCampoDeEntidade(field, entry);

					} else {
						valor = Util.converterValores(field.getType(), entry.getValue());
					}

					popularCampoReflection(objeto, field, valor);

				}
			}
		} catch (ReflectiveOperationException e) {
			LOG.error("Falha ao acessar recurso com reflexion", e);
			throw new GGASException(e);
		}

	}

	private void popularCampoReflection(Object objeto, Field field, Object valor) throws IllegalAccessException, GGASException {
		field.setAccessible(true);

		try {
			field.set(objeto, valor);
		} catch (IllegalArgumentException e) {
			LOG.error(e.getMessage(), e);
			field.set(objeto, Util.converterCampoStringParaData(Cliente.DATA_EMISSAO_RG, (String) valor, Constantes.FORMATO_DATA_BR));
		}

		field.setAccessible(false);
	}

	private Object adicionandoChavePrimariaEmCampoDeEntidade(Field field, Entry<String, Object> entry) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		Object objPai = null;

		if (field.getType().isInterface()) {
			String nomeClasse = field.getType().getPackage() + ".impl." + field.getType().getSimpleName() + "Impl";
			String[] nomeClasseFormatado = nomeClasse.split("package ");
			Class<?> classePai = Class.forName(nomeClasseFormatado[1]);

			objPai = classePai.newInstance();
			Method m = null;
			try {

				m = classePai.getSuperclass().getSuperclass().getMethod(Util.montarNomeSet(CHAVE_PRIMARIA), long.class);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				m = classePai.getSuperclass().getMethod(Util.montarNomeSet(CHAVE_PRIMARIA), long.class);
			}

			m.invoke(objPai, Long.valueOf((String) entry.getValue()));

		}

		return objPai;
	}

	/**
	 * Exibir dados aba documentacao.
	 *
	 * @param request
	 *            the request
	 * @param controladorCliente
	 *            the controlador cliente
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void exibirDadosAbaDocumentacao(HttpServletRequest request, ControladorCliente controladorCliente) throws GGASException {

		request.setAttribute(ORGAOS_EMISSORES, controladorCliente.listarOrgaoExpedidor());
		request.setAttribute(UNIDADES_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		request.setAttribute(PROFISSOES, controladorCliente.listarProfissao());
		request.setAttribute(FAIXAS_RENDA, controladorCliente.listarFaixaRendaFamiliar());
		request.setAttribute(NACIONALIDADES, controladorCliente.listarNacionalidade());
		request.setAttribute(ATIVIDADES_ECONOMICAS, controladorCliente.listarAtividadeEconomica());
		request.setAttribute(SEXOS, controladorCliente.listarPessoaSexo());
		request.setAttribute(MASCARA_INSCRICAO_ESTADUAL,
				controladorParametroSistema.obterValorDoParametroPorCodigo("MASCARA_INSCRICAO_ESTADUAL"));

	}

	/**
	 * Exibir solicitacao alteracao data vencimento.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirSolicitacaoAlteracaoDataVencimento")
	public ModelAndView exibirSolicitacaoAlteracaoDataVencimento(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirSolicitacaoAlteracaoDataVencimento");

		try {
			Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
			ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CHAMADO_ASSUNTO_AGV_ALTERAR_CADASTRO);
			ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(Long.valueOf(constante.getValor()));
			if (chamado == null || chamado.getChamadoAssunto() == null || !chamadoAssunto.equals(chamado.getChamadoAssunto())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_NAO_TIPO_ALTERAR_CADASTRO, true);
			}
			controladorChamado.verificarStatusChamado(chamado, Constantes.CHAMADO_OPERACAO_ENCERRAMENTO);

			Servico servico = new Servico();

			List<PontoConsumoTO> listaPontoConsumoTO = servico.consultarContratoPontoConsumo(chamado.getContrato().getChavePrimaria());

			for (PontoConsumoTO pontoConsumoTO : listaPontoConsumoTO) {
				ChamadoAlteracaoVencimento chamadoAlteracaoVencimento = controladorChamado.obterChamadoAlteracaoVencimento(
						chamado.getChavePrimaria(), pontoConsumoTO.getChavePrimaria());
				if(chamadoAlteracaoVencimento != null) {
					pontoConsumoTO.setVencimentoSolicitado(chamadoAlteracaoVencimento.getDiaSolicitado());
					pontoConsumoTO.setVencimentoVigente(chamadoAlteracaoVencimento.getDiaVigente());
				}
			}
			Long motivo = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_ENCERRAMENTO_CHAMADO));
			model.addObject(ID_MOTIVO, motivo);
			model.addObject(CHAMADO, chamado);
			model.addObject("listaPontoConsumoTO", listaPontoConsumoTO);
		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Salvar alteracao data vencimento.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("salvarAlteracaoDataVencimento")
	public ModelAndView salvarAlteracaoDataVencimento(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_TELA_EXIBIR_DETALHAMENTO_CHAMADO);

		try {
			Chamado chamado = controladorChamado.obterChamado(chavePrimaria);
			for (Long idPontoConsumo : chavesPrimarias) {

				ChamadoAlteracaoVencimento chamadoAlteracaoVencimento = controladorChamado.obterChamadoAlteracaoVencimento(
						chamado.getChavePrimaria(), idPontoConsumo);

				Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));
				ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoPontoConsumo(chamado.getContrato(),
						chamadoAlteracaoVencimento.getPontoConsumo());
				if(contratoPontoConsumo != null) {
					ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento = controladorContrato
							.consultarContratoPontoConsumoItemFaturamento(contratoPontoConsumo.getChavePrimaria(), itemFaturaGas);
					contratoPontoConsumoItemFaturamento.setNumeroDiaVencimento(chamadoAlteracaoVencimento.getDiaSolicitado());
					controladorContrato.atualizar(contratoPontoConsumoItemFaturamento, ContratoPontoConsumoItemFaturamentoImpl.class);
				}

			}
			Map<String, Object> dados = new LinkedHashMap<>();
			this.popularMapaDados(dados, request);
			controladorChamado.encerrarChamado(chamado, dados);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA, GenericAction.obterMensagem(Constantes.CHAMADO));
		} catch(NegocioException e) {
			model = new ModelAndView("forward:/exibirSolicitacaoAlteracaoDataVencimento");
			carregaCombos(model);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Registrar tentativa contato.
	 *
	 * @param chamado {@link Chamado}
	 * @param request {@link HttpServletRequest}
	 * @return the model {@link ModelAndView}
	 * @throws GGASException 
	 * @throws NumberFormatException 
	 */
	@RequestMapping("registrarTentativaContato")
	public ModelAndView registrarTentativaContato(@ModelAttribute("Chamado") Chamado chamado, HttpServletRequest request)
			throws NumberFormatException, GGASException {

		Map<String, Object> dados = new HashMap<>();
		popularMapaDados(dados, request);
		ModelAndView model = new ModelAndView("forward:/exibirResponderQuestionario");
		try {
			controladorChamado.registrarTentativaContato(chamado, dados);
			mensagemSucesso(model, Constantes.SUCESSO_REGISTRO_TENTATIVA_CONTATO);
		} catch(NegocioException e) {
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Salvar agendar.
	 *
	 * @param chamado objeto Chamado
	 * @param result bindingResult
	 * @param chaveEquipe chave do objeto equipe
	 * @param session sessão
	 * @param request request
	 * @param chavesPrimariasDialog the chavesPrimariasDialog
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("salvarAgendar")
	public ModelAndView salvarAgendar(@ModelAttribute("Chamado") Chamado chamado, BindingResult result,
			@RequestParam("equipe") Long chaveEquipe, @RequestParam(value = "idTurnoPreferencia", required = false) Long idTurnoPreferencia,
			@RequestParam("chavesPrimariasDialog") List<String> chavesPrimariasDialog,
			HttpSession session, HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirInclusaoAgendamento");
		
		Map<String, Object> dados = new HashMap<>();
		try {
			popularMapaDados(dados, request);
			Equipe equipe = null;
			if (chaveEquipe != null && chaveEquipe != -1L) {
				equipe = fachada.obterEquipe(chaveEquipe);
				dados.put(EQUIPE, equipe);
			}
			
			
			EntidadeConteudo turno = null;
			if(idTurnoPreferencia != null && idTurnoPreferencia > 0l) {
				turno = Fachada.getInstancia().obterEntidadeConteudo(idTurnoPreferencia);
				dados.put("turnoPreferencia", turno);
			}
			validarEnderecoChamado(chamado);
			popularUnidadesOrganizacionaisVisiveis(chamado, request);

			tratarErroServicoDuplicado(chamado, request, dados);
			if(!controladorChamado.existeServicoAutorizacaoGerada(chamado)){
				request.setAttribute(Constantes.ERRO_NEGOCIO_AUTORIZACAO_NAO_GERADA_AUTOMATICAMENTE, true);
				return exibirDetalhamentoChamado(chamado.getChavePrimaria(), request);
			}else{
				
				List<ServicoTipo> listaServicoTipo  = new ArrayList<>();
				if(chavesPrimariasDialog != null && !chavesPrimariasDialog.isEmpty()){
	
					for(String chavePrimaria : chavesPrimariasDialog){
						ServicoTipo tipo =  controladorServicoTipo.consultarServicoTipo(Long.parseLong(chavePrimaria));
						if(tipo != null){
							listaServicoTipo.add(tipo);
						}
					}
			
				}
		
				
				if (!listaServicoTipo.isEmpty()) {
					model.addObject(LISTA_TIPO_SERVICO, listaServicoTipo);
					model.addObject("protocolo", chamado.getProtocolo().getNumeroProtocolo());
					model.addObject("exibirBotaoAdicionar", Boolean.FALSE);
					model.addObject("dataSelecionada", Util.converterDataParaStringSemHora(new Date(), Constantes.FORMATO_DATA_BR));
					model.addObject("listaEquipe", controladorEquipe.listarEquipe());
					model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
					model.addObject("exibirBotaoAdicionar", Boolean.TRUE);
					model.addObject("equipe", equipe);
					model.addObject("turnoPreferencial", turno);
					
					ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoVO = new ServicoAutorizacaoAgendaPesquisaVO();
					servicoAutorizacaoVO.setDataSelecionada(Util.converterDataParaStringSemHora(new Date(), Constantes.FORMATO_DATA_BR));
					servicoAutorizacaoVO.setEquipe(equipe);
					servicoAutorizacaoVO.setTurno(turno);
					
					model.addObject("servicoAutorizacaoVO", servicoAutorizacaoVO);

					session.removeAttribute(LISTA_IMOVEIS_CHAMADO);
				} else {
					model = exibirInclusaoEMensagemErroChamado(chamado, request, new NegocioException(
							Constantes.ERRO_NEGOCIO_NAO_HA_SERVICO_TIPO_AGENDAMENTO));
				}
			}
		} catch(NegocioException e) {
			LOG.warn(e);
			model = exibirInclusaoEMensagemErroChamado(chamado, request, e);
		}

		return model;
	}

	/**
	 * Registra erro de serviço duplicado
	 *
	 * @param chamado
	 * @param request
	 * @param dados
	 * @throws GGASException
	 */
	private void tratarErroServicoDuplicado(Chamado chamado, HttpServletRequest request, Map<String, Object> dados) throws GGASException {
		inserirChamado(chamado, request, dados);
		if(chamado.isPossuiASDuplicada()){
			request.setAttribute(Constantes.ERRO_SERVICO_AUTORIZACAO_DUPLICADO_CHAMADO, true);
		}
	}

	/**
	 * Carregar servico tipo por chamado.
	 *
	 * @param chamado
	 *            the chamado
	 * @throws NegocioException
	 * 			the NegocioException
	 * @return the list
	 */
	public List<ServicoTipo> carregarServicoTipoPorChamado(Chamado chamado) throws NegocioException {
		Long chaveAssunto = chamado.getChamadoAssunto().getChavePrimaria();
		Collection<ChamadoAssuntoServicoTipo> listaServicos = new HashSet<>();

		if ( chaveAssunto != null && chaveAssunto != 0L) {
			ChamadoAssunto chamadoAssunto = ServiceLocator.getInstancia().getControladorChamadoAssunto()
					.consultarChamadoAssunto(chaveAssunto);
			listaServicos = chamadoAssunto.getListaServicos();
		}

		List<ServicoTipo> servicoTipoList = new ArrayList<>();

		for (ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo : listaServicos) {
			if (chamadoAssuntoServicoTipo.getServicoTipo().getIndicadorAgendamento()) {
				servicoTipoList.add(chamadoAssuntoServicoTipo.getServicoTipo());
			}

		}
		return servicoTipoList;
	}

	/**
	 * Exibir detalhamento chamado pesquisa satisfacao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoChamadoPesquisaSatisfacao")
	public ModelAndView exibirDetalhamentoChamadoPesquisaSatisfacao(@RequestParam("chaveChamadoOrigem") Long chavePrimaria,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = null;
		try {
			if(chavePrimaria == null) {
				Util.registroNaoEncontrato();
			}
			model = this.exibirDetalhamentoChamado(chavePrimaria, request);
			model.setViewName("exibirDetalhamentoChamadoPesquisaSatisfacao");
			model.addObject("fluxoPesquisaSatisfacao", true);
		} catch(NegocioException e) {
			model = new ModelAndView("forward:/exibirResponderQuestionario");
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Adicionar email.
	 *
	 * @param descricaoEmail
	 *            the descricao email
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("adicionarEmail")
	public ModelAndView adicionarEmail(@RequestParam("descricaoEmail") String descricaoEmail, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_CHAMADO_EMAIL);
		model.addObject("teste", 0);
		String descricaoEmailUTF8 = Util.utf8ToISO(descricaoEmail);
		@SuppressWarnings("unchecked")
		List<ChamadoEmail> listaChamadoEmail = (List<ChamadoEmail>) sessao.getAttribute(LISTA_CHAMADO_EMAIL);

		ChamadoEmail chamadoEmail = new ChamadoEmail();
		if (descricaoEmailUTF8 != null && !descricaoEmailUTF8.isEmpty() && !"".equals(descricaoEmailUTF8)) {

			if(listaChamadoEmail == null || listaChamadoEmail.isEmpty()) {
				listaChamadoEmail = new ArrayList<>();
				chamadoEmail.setDescricao(descricaoEmailUTF8);
				listaChamadoEmail.add(chamadoEmail);
			} else {
				chamadoEmail.setDescricao(descricaoEmailUTF8);
				listaChamadoEmail.add(chamadoEmail);

			}

		}
		model.addObject(LISTA_CHAMADO_EMAIL, listaChamadoEmail);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_EMAIL, listaChamadoEmail);
		return model;
	}

	/**
	 * Remover chamado email.
	 *
	 * @param indexListaChamadoEmail
	 *            the index lista chamado email
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerChamadoEmail")
	public ModelAndView removerChamadoEmail(@RequestParam("indexListaChamadoEmail") int indexListaChamadoEmail, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_CHAMADO_EMAIL);

		List<ChamadoEmail> chamadoEmailModel = (List<ChamadoEmail>) sessao.getAttribute(LISTA_CHAMADO_EMAIL);

		if(chamadoEmailModel != null) {
			chamadoEmailModel.remove(indexListaChamadoEmail);
		}

		model.addObject(LISTA_CHAMADO_EMAIL, chamadoEmailModel);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_EMAIL, chamadoEmailModel);

		return model;

	}

	/**
	 * Alterar chamado email.
	 *
	 * @param indexListaChamadoEmail
	 *            the index lista chamado email
	 * @param descricaoEmail
	 *            the descricao email
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("alterarChamadoEmail")
	public ModelAndView alterarChamadoEmail(@RequestParam("indexListaChamadoEmail") int indexListaChamadoEmail,
			@RequestParam("descricaoEmail") String descricaoEmail, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_CHAMADO_EMAIL);
		String descricaoEmailUTF8 = Util.utf8ToISO(descricaoEmail);
		List<ChamadoEmail> chamadoEmailModel = (List<ChamadoEmail>) sessao.getAttribute(LISTA_CHAMADO_EMAIL);
		if (descricaoEmailUTF8 != null && !descricaoEmailUTF8.isEmpty() && !"".equals(descricaoEmailUTF8) && chamadoEmailModel != null) {
			chamadoEmailModel.get(indexListaChamadoEmail).setDescricao(descricaoEmailUTF8);
		}
		model.addObject(LISTA_CHAMADO_EMAIL, chamadoEmailModel);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_EMAIL, chamadoEmailModel);

		return model;

	}

	/**
	 * Exibir contato cliente.
	 *
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirContatoCliente")
	public ModelAndView exibirContatoCliente(@RequestParam("chavePrimariaCliente") int chavePrimariaCliente, HttpSession sessao)
			throws GGASException {

		ModelAndView model = new ModelAndView("abaChamadoContato");

		Cliente cliente = null;
		cliente = (Cliente) controladorCliente.obter(chavePrimariaCliente, PROPRIEDADES_LAZY);

		Collection<TipoContato> listaTipoContato = controladorCliente.listarTipoContato();
		Collection<Profissao> listaTipoCargo = controladorCliente.listarProfissao();
		List<ContatoCliente> listaChamadoContato = new ArrayList<>(cliente.getContatos());
		model.addObject(LISTA_TIPO_CARGO, listaTipoCargo);
		model.addObject(LISTA_TIPO_CONTATO, listaTipoContato);
		model.addObject(LISTA_CHAMADO_CONTATO, listaChamadoContato);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_CONTATO, listaChamadoContato);

		return model;
	}

	/**
	 * Adicionar contato.
	 *
	 * @param nomeContato
	 *            the nome contato
	 * @param codigoDDD
	 *            the codigo ddd
	 * @param foneContato
	 *            the fone contato
	 * @param ramalContato
	 *            the ramal contato
	 * @param descricaoArea
	 *            the descricao area
	 * @param emailContato
	 *            the email contato
	 * @param idTipoContato
	 *            the id tipo contato
	 * @param idProfissaoContato
	 *            the id profissao contato
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("adicionarChamadoContato")
	public ModelAndView adicionarContato(@RequestParam("nomeContato") String nomeContato, @RequestParam("codigoDDD") Integer codigoDDD,
			@RequestParam("foneContato") Integer foneContato, @RequestParam("ramalContato") Integer ramalContato,
			@RequestParam("descricaoArea") String descricaoArea, @RequestParam("emailContato") String emailContato,
			@RequestParam("idTipoContato") Long idTipoContato, @RequestParam("idProfissaoContato") Long idProfissaoContato,
			HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_ABA_CHAMADO_CONTATO);

		ContatoCliente contato = new ContatoClienteImpl();
		@SuppressWarnings("unchecked")
		List<ContatoCliente> listaChamadoContato = (List<ContatoCliente>) sessao.getAttribute(LISTA_CHAMADO_CONTATO);
		if(idTipoContato != null && idTipoContato > 0) {
			TipoContato tipoContato = controladorCliente.buscarTipoContato(idTipoContato);
			contato.setTipoContato(tipoContato);

			if(idProfissaoContato != null && idProfissaoContato > 0) {
				Profissao profissaoContato = controladorCliente.obterProfissao(idProfissaoContato);
				contato.setProfissao(profissaoContato);
			}

			contato.setNome(nomeContato);
			contato.setCodigoDDD(codigoDDD);
			contato.setFone(foneContato);
			contato.setRamal(ramalContato);
			contato.setDescricaoArea(descricaoArea);
			contato.setEmail(emailContato);
			contato.setUltimaAlteracao(Calendar.getInstance().getTime());
			controladorCliente.validarDadosContatoCliente(contato);
			if(listaChamadoContato == null || listaChamadoContato.isEmpty()) {
				listaChamadoContato = new ArrayList<>();
				listaChamadoContato.add(contato);
			} else {
				listaChamadoContato.add(contato);

			}
		}
		model.addObject(LISTA_CHAMADO_CONTATO, listaChamadoContato);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_CONTATO, listaChamadoContato);
		return model;
	}

	/**
	 * Alterar contato.
	 *
	 * @param nomeContato
	 *            the nome contato
	 * @param codigoDDD
	 *            the codigo ddd
	 * @param foneContato
	 *            the fone contato
	 * @param ramalContato
	 *            the ramal contato
	 * @param descricaoArea
	 *            the descricao area
	 * @param emailContato
	 *            the email contato
	 * @param idTipoContato
	 *            the id tipo contato
	 * @param idProfissaoContato
	 *            the id profissao contato
	 * @param indexListaContatos
	 *            the index lista contatos
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("alterarChamadoContato")
	public ModelAndView alterarContato(@RequestParam("nomeContato") String nomeContato, @RequestParam("codigoDDD") Integer codigoDDD,
			@RequestParam("foneContato") Integer foneContato, @RequestParam("ramalContato") Integer ramalContato,
			@RequestParam("descricaoArea") String descricaoArea, @RequestParam("emailContato") String emailContato,
			@RequestParam("idTipoContato") Long idTipoContato, @RequestParam("idProfissaoContato") Long idProfissaoContato,
			@RequestParam("indexListaContatos") Integer indexListaContatos, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_ABA_CHAMADO_CONTATO);
		@SuppressWarnings("unchecked")
		List<ContatoCliente> listaContato = (List<ContatoCliente>) sessao.getAttribute(LISTA_CHAMADO_CONTATO);

		if(idTipoContato != null && idTipoContato != -1) {
			TipoContato tipoContato = controladorCliente.buscarTipoContato(idTipoContato);
			listaContato.get(indexListaContatos).setTipoContato(tipoContato);
		}
		if(idProfissaoContato != null && idProfissaoContato != -1) {
			Profissao profissaoContato = controladorCliente.obterProfissao(idProfissaoContato);
			listaContato.get(indexListaContatos).setProfissao(profissaoContato);
		}

		listaContato.get(indexListaContatos).setNome(nomeContato);
		listaContato.get(indexListaContatos).setCodigoDDD(codigoDDD);
		listaContato.get(indexListaContatos).setFone(foneContato);
		listaContato.get(indexListaContatos).setRamal(ramalContato);
		listaContato.get(indexListaContatos).setDescricaoArea(descricaoArea);
		listaContato.get(indexListaContatos).setEmail(emailContato);
		listaContato.get(indexListaContatos).setUltimaAlteracao(Calendar.getInstance().getTime());
		model.addObject(LISTA_CHAMADO_CONTATO, listaContato);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_CONTATO, listaContato);
		return model;
	}

	/**
	 * Remover chamado contato.
	 *
	 * @param indexListaContatos
	 *            the index lista contatos
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("removerChamadoContato")
	public ModelAndView removerChamadoContato(@RequestParam("indexListaContatos") int indexListaContatos, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_ABA_CHAMADO_CONTATO);
		@SuppressWarnings("unchecked")
		List<ContatoCliente> listaContato = (List<ContatoCliente>) sessao.getAttribute(LISTA_CHAMADO_CONTATO);

		if(listaContato != null) {
			listaContato.remove(indexListaContatos);
		}

		model.addObject(LISTA_CHAMADO_CONTATO, listaContato);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_CONTATO, listaContato);

		return model;

	}

	/**
	 * Atualizar chamado contato principal.
	 *
	 * @param indexListaContatos
	 *            the index lista contatos
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("atualizarChamadoContatoPrincipal")
	public ModelAndView atualizarChamadoContatoPrincipal(@RequestParam("indexListaContatos") int indexListaContatos, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_ABA_CHAMADO_CONTATO);
		@SuppressWarnings("unchecked")
		List<ContatoCliente> listaContato = (List<ContatoCliente>) sessao.getAttribute(LISTA_CHAMADO_CONTATO);

		if(listaContato != null) {
			ContatoCliente contatoCliente = null;
			for (Integer i = 0; i < listaContato.size(); i++) {
				contatoCliente = listaContato.get(i);
				if(i.equals(indexListaContatos)) {
					contatoCliente.setPrincipal(Boolean.TRUE);
				} else {
					contatoCliente.setPrincipal(Boolean.FALSE);
				}
			}

		}

		model.addObject(LISTA_CHAMADO_CONTATO, listaContato);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		sessao.setAttribute(LISTA_CHAMADO_CONTATO, listaContato);

		return model;
	}

	/**
	 * Carregar Tipos Chamados.
	 *
	 * @param idSegmento the chave primaria
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarTiposChamado")
	public ModelAndView carregarTiposChamado(@RequestParam("idSegmento") Long idSegmento) throws GGASException {

		ModelAndView model = new ModelAndView("divTipoChamado");
		Collection<ChamadoTipo> chamadosTipo = null;
		if (idSegmento != null && idSegmento != -1L) {
			chamadosTipo = controladorChamadoTipo.listarPorSegmento(idSegmento);
		} else {
			chamadosTipo = controladorChamadoTipo.listarTodosHabilitado();
		}

		model.addObject(CHAMADOS_TIPO, chamadosTipo);

		return model;
	}

	/**
	 * Carregar contrato por ponto consumo.
	 *
	 * @param numeroContrato the numero contrato
	 * @param response the response
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarContratoPorNumero")
	public ModelAndView carregarContratoPorNumero(@RequestParam("numeroContrato") String numeroContrato,
			HttpServletResponse response) {

		ModelAndView model = new ModelAndView("divContrato");
		try {
			Contrato contrato = pesquisarContratoPorNumero(numeroContrato);
			Chamado chamado = new Chamado();
			chamado.setContrato(contrato);
			model.addObject(CHAMADO, chamado);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(AJAX_ALERTA);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CONTRATO_ATIVO_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Pesquisa contrato por numero
	 *
	 * @param numeroContrato
	 * @return contrato
	 * @throws NegocioException
	 */
	private Contrato pesquisarContratoPorNumero (String numeroContrato) throws NegocioException {

		ArrayList<Contrato> contratos = (ArrayList<Contrato>) controladorContrato.consultarContratoPorNumero(numeroContrato);

		if (contratos != null && contratos.size() == 1) {
			return contratos.get(0);
		}
		return null;
	}

	/**
	 * Exibir página responder questionario.
	 *
	 * @param chavePrimariaChamado chave Primária
	 * @return the model and view
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirRespostaQuestionario")
	public ModelAndView exibirRespostaQuestionario(@RequestParam("chavePrimariaChamado") Long chavePrimariaChamado)
			throws NegocioException {
		ModelAndView model = new ModelAndView(TELA_POPUP_RESPOSTA_QUESTIONARIO);

		Chamado chamado = controladorChamado.obterChamado(chavePrimariaChamado);

		Questionario questionario = chamado.getChamadoAssunto().getQuestionario();

		Collection<QuestionarioPerguntaResposta> respostas =
				controladorQuestionario.obterRespostas(questionario.getChavePrimaria(), chamado.getChavePrimaria());

		model.addObject(QUESTIONARIO, questionario);
		model.addObject("respostas", respostas);

		return model;
	}

	/**
	 * limpa campo arquivo da tela
	 *
	 * @param request
	 * 			the request
	 * @return ModelAndView divCampoArquivo
	 */
	@RequestMapping("limparCampoArquivoChamado")
	public ModelAndView limparCampoArquivo(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("divCampoArquivo");

		model.addObject("arquivoAnexo", null);

		return model;
	}

	/**
	 * Adiciona anexo ao chamado historico
	 *
	 * @param request
	 * 			the request
	 * @return	ModelAndView gridAnexos
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarAnexoChamado")
	public ModelAndView adicionarAnexo(HttpServletRequest request) {

		ModelAndView model = new ModelAndView(GRID_ANEXOS);
		ChamadoHistoricoAnexo chamadoAnexo = new ChamadoHistoricoAnexo();
		Collection<ChamadoHistoricoAnexo> listaAnexos = (Collection<ChamadoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		try {
			popularAnexo(chamadoAnexo, request);

			controladorChamado.validarDadosAnexo(chamadoAnexo);

			if (indexLista == null || indexLista < 0) {

				chamadoAnexo.setDadosAuditoria(getDadosAuditoria(request));

				if (listaAnexos == null || listaAnexos.isEmpty()) {
					listaAnexos = new ArrayList<>();
				}

				controladorChamado.verificarDescricaoAnexo(indexLista, listaAnexos, chamadoAnexo);
				listaAnexos.add(chamadoAnexo);
			} else {

				this.alterarAnexo(chamadoAnexo, indexLista, listaAnexos, request);
			}

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		}

		model.addObject(LISTA_ANEXOS, listaAnexos);

		request.getSession().setAttribute(LISTA_ANEXOS, listaAnexos);

		return model;
	}

	/**
	 * Popula anexo do chamado histórico
	 *
	 * @param chamadoHistoricoAnexo
	 * 			the chamadoHistoricoAnexo
	 * @param request
	 * 			the request
	 * @throws NegocioException
	 *
	 */
	private void popularAnexo(ChamadoHistoricoAnexo chamadoHistoricoAnexo, HttpServletRequest request) throws NegocioException {

		MultipartHttpServletRequest multiPartRequest = (MultipartHttpServletRequest) request;
		MultipartFile arquivo = multiPartRequest.getFile("arquivoAnexo");

		String descricaoAnexo = Util.utf8ToISO(request.getParameter("descricaoAnexo"));
		String chavePrimariaAnexo = request.getParameter("chavePrimariaAnexo");

		if (!StringUtils.isEmpty(descricaoAnexo)) {
			chamadoHistoricoAnexo.setDescricaoAnexo(descricaoAnexo);
		}

		boolean condicao = false;
		if (arquivo != null) {
			condicao = Util.validarImagemDoArquivo(arquivo, extensao, TAMANHO_MAXIMO_ARQUIVO);
		}

		if (condicao && !arquivo.isEmpty()) {
			try {
				chamadoHistoricoAnexo.setDocumentoAnexo(arquivo.getBytes());
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(e);
			}
			chamadoHistoricoAnexo.setNomeArquivo(arquivo.getOriginalFilename());
		}


		if (StringUtils.isNotBlank(chavePrimariaAnexo)) {
			chamadoHistoricoAnexo.setChavePrimaria(Long.parseLong(chavePrimariaAnexo));
		}

		chamadoHistoricoAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());

		request.getSession().setAttribute("chamadoHistoricoAnexo", chamadoHistoricoAnexo);

	}

	/**
	 * Altera anexo da lista do chamado historico
	 *
	 * @param chamadoHistoricoAnexoAlterado
	 * 			the chamadoHistoricoAnexoAlterado
	 * @param indexListaAnexo
	 * 			the indexListaAnexo
	 * @param listaAnexo
	 * 			the listaAnexo
	 * @param request
	 * 			the request
	 * @throws GGASException
	 */
	private void alterarAnexo(ChamadoHistoricoAnexo chamadoHistoricoAnexoAlterado, Integer indexListaAnexo,
			Collection<ChamadoHistoricoAnexo> listaAnexo, HttpServletRequest request)
			throws GGASException {

		if (listaAnexo != null && indexListaAnexo != null) {

			controladorChamado.verificarDescricaoAnexo(indexListaAnexo, listaAnexo, chamadoHistoricoAnexoAlterado);

			ChamadoHistoricoAnexo chamadoHistoricoAnexoExistente = null;
			ArrayList<ChamadoHistoricoAnexo> listaAnexoArray = new ArrayList<>(listaAnexo);
			for (int i = 0; i < listaAnexo.size(); i++) {
				chamadoHistoricoAnexoExistente = listaAnexoArray.get(i);
				if (i == indexListaAnexo && chamadoHistoricoAnexoExistente != null) {

					popularAnexo(chamadoHistoricoAnexoExistente, request);
					chamadoHistoricoAnexoExistente.setDadosAuditoria(getDadosAuditoria(request));
				}
			}
		}
	}




	/**
	 * Visualizar anexo do chamado historico
	 *
	 * @param indexLista
	 * 			the indexLista
	 * @param request
	 * 			the request
	 * @param response
	 * 			the response
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("visualizarAnexoChamado")
	public void visualizarAnexo(@RequestParam("indexLista") Integer indexLista, HttpServletRequest request, HttpServletResponse response) {

		Collection<ChamadoHistoricoAnexo> listaAnexos = (Collection<ChamadoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		for (int i = 0; listaAnexos != null && i < listaAnexos.size(); i++) {

			ChamadoHistoricoAnexo chamadoHistoricoAnexo = ((ArrayList<ChamadoHistoricoAnexo>) listaAnexos).get(i);
			if (i == indexLista && chamadoHistoricoAnexo != null) {

				byte[] anexo = chamadoHistoricoAnexo.getDocumentoAnexo();

				ServletOutputStream servletOutputStream;
				try {
					servletOutputStream = response.getOutputStream();
					String nomeArquivo = chamadoHistoricoAnexo.getNomeArquivo();
					response.setContentLength(anexo.length);
					response.addHeader(CONTENT_DISPOSITION, "attachment; filename=" + nomeArquivo);
					servletOutputStream.write(anexo, 0, anexo.length);
					servletOutputStream.flush();
					servletOutputStream.close();

				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * Remover anexo aba identificacao.
	 *
	 * @param request the request
	 * @param response the resonse
	 * @return ModelAndView gridAnexos
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerAnexoChamado")
	public ModelAndView removerAnexo(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView(GRID_ANEXOS);

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		Collection<ChamadoHistoricoAnexo> listaAnexos = (Collection<ChamadoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		if (listaAnexos != null && indexLista != null) {
			ChamadoHistoricoAnexo chamadoHistoricoAnexo = ((ArrayList<ChamadoHistoricoAnexo>) listaAnexos).get(indexLista.intValue());
			listaAnexos.remove(chamadoHistoricoAnexo);
		}

		request.getSession().setAttribute(LISTA_ANEXOS, listaAnexos);

		model.addObject(LISTA_ANEXOS, listaAnexos);

		return model;
	}

	/**
	 * Carregar lista anexos.
	 *
	 * @param model
	 * 			the model
	 * @param request
	 * 			the request
	 */
	@SuppressWarnings("unchecked")
	private void carregarListaAnexos(ModelAndView model, HttpServletRequest request) {
		if (request.getSession().getAttribute(LISTA_ANEXOS) != null) {
			Collection<ChamadoHistoricoAnexo> chamadoHistoricoAnexos =
					(Collection<ChamadoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);
			model.addObject(LISTA_ANEXOS, chamadoHistoricoAnexos);
		}

	}

	/**
	 * exibir PopupPonto Consumo Contrato.
	 *
	 * @param chavePrimaria
	 * 			the chavePrimaria
	 * @param chamadoTela
	 * 			the chamadoTela
	 * @param request
	 * 			the request
	 * @return ação exibirPontoConsumoContrato
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPopupPontoConsumoContrato")
	public String exibirPopupPontoConsumoContrato(@RequestParam(CHAVE_PRIMARIA) String chavePrimaria,
			@RequestParam("chamadoTela") boolean chamadoTela,
			HttpServletRequest request) throws GGASException {

		return "forward:exibirPontoConsumoContrato";
	}
	
	/**
	 * Exibe a pagina de Informações Consolidadas.
	 *
	 * @param chavePrimaria - {@link Long}
	 * @param httpSession - {@link HttpSession}
	 * @param request - {@link HttpServletRequest}
	 * @param isEmailEnviado - {@link Boolean}}
	 * @return model - {@link ModelAndView}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("popupExibirInformacoesConsolidadas")
	public ModelAndView exibirInformacoesConsolidadas(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria,
			HttpSession httpSession, HttpServletRequest request, boolean isEmailEnviado) throws GGASException {
		String scala = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(
				Constantes.PARAMETRO_QUANTIDADE_CASAS_DECIMAIS_APRESENTACAO_CONSUMO_APURADO);
		ModelAndView model = new ModelAndView(POPUP_EXIBIR_INFORMACOES_CONSOLIDADAS);
		Cliente cliente = obterClienteRN(chavePrimaria);
		List<DocumentoFiscalVO> listaDocumentosFiscais = listarDocumentoFiscalPorClienteRN(cliente);
		List<DocumentoCobrancaVO> listaDocCobrancas = listarCobrancasPorClienteRN(cliente);
		List<ServicoAutorizacaoAgendasVO> listaAutorizacaoAgendas = listarAgandamentosPorClienteRN(cliente);

		model.addObject(LISTA_DOC_FISCAL, listaDocumentosFiscais);
		model.addObject(VALOR_TOTAL_ATUALIZADO, calcularValorTotalAtualizadoFaturamento(listaDocumentosFiscais));
		model.addObject(LISTA_DOCUMENTO_COBRANCA, listaDocCobrancas);
		model.addObject(VALOR_TOTAL_ATUALIZADO_COBRANCA, calcularValorTotalAtualizadoCobranca(listaDocCobrancas));
		model.addObject(LISTA_SERV_AUTORIZ_AGENDAS, listaAutorizacaoAgendas);

		model.addObject(CLIENTE, cliente);
		model.addObject(SCALA_CONSUMO_APURADO, scala);
		model.addObject(IS_ENVIADO, isEmailEnviado);

		return model;
	}
	
	private Cliente obterClienteRN(Long clienteID) {
		Cliente cliente = null;
		try {
			if (clienteID != null) {
				cliente = controladorCliente.obterCliente(clienteID);
			}
		} catch (NegocioException e) {
			LOG.error("Falha ao obter dados do cliente ", e);
		}
		return cliente;
	}

	private List<DocumentoFiscalVO> listarDocumentoFiscalPorClienteRN(Cliente cliente) throws NegocioException {
		List<DocumentoFiscalVO> listaDocFiscalAtualizada = new ArrayList<>();
		
		ConstanteSistema constanteSistemaSituacaoPagPendente =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.SITUACAO_PAGAMENTO_FATURA_PENDENTE);
		
		
		if (cliente != null) {
			List<DocumentoFiscalVO> docsFiscal = controladorFatura.listarDocumentoFiscalPorCliente(cliente);
			if (!docsFiscal.isEmpty()) {
				for (DocumentoFiscalVO dofi : docsFiscal) {
					if (dofi.getSituacaoDoPagamentoID().equals(constanteSistemaSituacaoPagPendente.getValorLong())) {
						if (dofi.getValorTotal() != null) {
							dofi.setValorAtualizado(calcularValorAtualizadoFaturamento(dofi));
						}
						if (dofi.getFaturaID() != null) {
							dofi.setDataPagamento(controladorFatura.obterDataPagamentoFatura(dofi.getFaturaID()));
						}
						listaDocFiscalAtualizada.add(dofi);
					}
				}
			}
		}
		return listaDocFiscalAtualizada;
	}

	private BigDecimal calcularValorAtualizadoFaturamento(DocumentoFiscalVO dofi) throws NegocioException {
		BigDecimal resultadoCalculo = BigDecimal.ZERO;

		ConstanteSistema constanteSistemaSituacaoPagQuitado =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.SITUACAO_PAGAMENTO_FATURA_QUITADO);

		if (dofi.getPercentualMulta() != null && dofi.getValorTotal() != null && dofi.getPercentualJurosMora() != null
				&& dofi.getDataVencimento() != null) {
			resultadoCalculo = dofi.getValorTotal();
			if (dofi.getDataVencimento().before(new Date())) {
				if (!dofi.getSituacaoDoPagamentoID().equals(constanteSistemaSituacaoPagQuitado.getValorLong())) {
					BigDecimal percentualMulta = dofi.getValorTotal().multiply(dofi.getPercentualMulta());
					BigDecimal valorJuros = (dofi.getPercentualJurosMora().divide(new BigDecimal(QUANTIDADE_MES_COMERCIAL), SCALA_NUMERO_DOIS,
							BigDecimal.ROUND_HALF_UP))
									.multiply(new BigDecimal(
											DataUtil.diferencaDiasEntreDatas(dofi.getDataVencimento(), new Date())));
					resultadoCalculo = dofi.getValorTotal().add(percentualMulta.add(valorJuros));
				}
			}
		}
		return resultadoCalculo.setScale(SCALA_NUMERO_DOIS, RoundingMode.HALF_UP);
	}

	private BigDecimal calcularValorTotalAtualizadoFaturamento(List<DocumentoFiscalVO> list) {
		BigDecimal valor = BigDecimal.ZERO;
		if (list != null) {
			for (DocumentoFiscalVO d : list) {
				valor = valor.add(d.getValorAtualizado());
			}
		}
		return valor.setScale(SCALA_NUMERO_DOIS, RoundingMode.HALF_UP);
	}

	private BigDecimal calcularValorTotalAtualizadoCobranca(List<DocumentoCobrancaVO> list) {
		BigDecimal valor = BigDecimal.ZERO;
		if (list != null) {
			for (DocumentoCobrancaVO d : list) {
				if (d.getValorAtualizado() != null) {
					valor = valor.add(d.getValorAtualizado());
				}
			}
		}
		return valor.setScale(SCALA_NUMERO_DOIS, RoundingMode.HALF_UP);
	}
	
	private List<DocumentoCobrancaVO> listarCobrancasPorClienteRN(Cliente cliente) {
		List<DocumentoCobrancaVO> listaDocumentoCobranca2 = new ArrayList<>();
		if (cliente != null) {
			List<DocumentoCobrancaVO> listaDocumentoCobranca = controladorCobranca.listarCobrancasPorCliente(cliente);
			if (!listaDocumentoCobranca.isEmpty()) {
				for (DocumentoCobrancaVO documentoCobrancaVO : listaDocumentoCobranca) {
					if (documentoCobrancaVO != null) {
						DocumentoCobrancaVO docCobr = controladorCobranca.obterFaturaPorDocCobrancaItem(documentoCobrancaVO);
						if (docCobr != null && docCobr.getFaturaID() != null) {
							documentoCobrancaVO.setFaturaID(docCobr.getFaturaID());;
						}
						List<DocumentoCobrancaVO> listaDocVO = controladorCobranca.listarRecebimentosPorCobranca(documentoCobrancaVO);
						if (!listaDocVO.isEmpty()) {
							for (DocumentoCobrancaVO doc : listaDocVO) {
								if (documentoCobrancaVO.getDataPagamento() != null) {
									documentoCobrancaVO.setDataPagamento(doc.getDataPagamento());
								}
								BigDecimal valor = calcularValorAtualizadoCobranca(documentoCobrancaVO, cliente);
								documentoCobrancaVO.setValorAtualizado(valor);

								listaDocumentoCobranca2.add(documentoCobrancaVO);
							}
						}
					}
				}
			}
		}
		return listaDocumentoCobranca2;
	}
	
	private BigDecimal calcularValorAtualizadoCobranca(DocumentoCobrancaVO dofi, Cliente cliente) {
	
		BigDecimal resultadoCalculo = BigDecimal.ZERO;
		
		if (cliente != null && dofi != null && dofi.getValorTotal() != null) {
			resultadoCalculo = dofi.getValorTotal();
			Contrato cont = controladorContrato.obterValorDosTributosDoContrato(cliente);
			if (cont != null && cont.getPercentualJurosMora() != null && cont.getPercentualMulta() != null) {
				dofi.setPercentualJurosMora(cont.getPercentualJurosMora());
				dofi.setPercentualMulta(cont.getPercentualMulta());
				if (dofi.getPercentualMulta() != null && dofi.getPercentualJurosMora() != null
						&& dofi.getValorTotal() != null && dofi.getDataVencimento() != null) {
					if (dofi.getDataVencimento().before(new Date())) {
						BigDecimal percentualmulta = dofi.getValorTotal().multiply(dofi.getPercentualMulta());
						BigDecimal valorjuros = (dofi.getPercentualJurosMora().divide(new BigDecimal(QUANTIDADE_MES_COMERCIAL), SCALA_NUMERO_DOIS,
								BigDecimal.ROUND_HALF_UP)).multiply(new BigDecimal(
										DataUtil.diferencaDiasEntreDatas(dofi.getDataVencimento(), new Date())));
						resultadoCalculo = dofi.getValorTotal().add(percentualmulta.add(valorjuros));
					}
				}
			}
		}
		return resultadoCalculo.setScale(SCALA_NUMERO_DOIS, RoundingMode.HALF_UP);
	}
	
	private List<ServicoAutorizacaoAgendasVO> listarAgandamentosPorClienteRN(Cliente cliente) throws NegocioException {
		
		ConstanteSistema constanteSistemaEncerrado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_OPERACAO_CHAMADO_ENCERRADO);
		
		List<ServicoAutorizacaoAgendasVO> listAgendamentoPorStatus = new ArrayList<>();
		List<ServicoAutorizacaoAgendasVO> listAgendamentosPorCliente = controladorChamado
				.listarAgandamentosPorCliente(cliente);
		if (listAgendamentosPorCliente !=null && !listAgendamentosPorCliente.isEmpty()) {
			for (ServicoAutorizacaoAgendasVO seau : listAgendamentosPorCliente) {
				if (seau.getStatusID() != null) {
					if (!seau.getStatusID().equals(constanteSistemaEncerrado.getValorLong())) {
						listAgendamentoPorStatus.add(seau);
					}
				}
			}
		}
		return listAgendamentoPorStatus;
	}
	
	/**
	 * imprimirSegundaViaFatura.
	 *
	 * @param idFatura the id fatura
	 * @param idDocumentoFiscal the idDocumentoFiscal
	 * @param request the request
	 * @param response the response
	 * @return the ModelAndView model and view
	 * @throws Exception the  exception
	 */
	
	@RequestMapping("imprimirSegundaViaFatura")
	public ModelAndView imprimirSegundaViaFatura(@RequestParam(CHAVE_PRIMARIA_FATURA) Long idFatura,
			@RequestParam(CHAVE_PRIMARIA_DOC_FISCAL) Long idDocumentoFiscal, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		if (idFatura != null && idDocumentoFiscal != null) {
			Fatura fatura = obterFaturaPorIdRN(idFatura);
			fatura.setDadosAuditoria(getDadosAuditoria(request));

			DocumentoFiscal documentoFiscal = null;
			documentoFiscal = obterDocFiscalRN(idDocumentoFiscal);

			imprimirPDF("relatorioFaturaNotaFiscal", gerarImpressaoFaturaSegundaViaRN(fatura, documentoFiscal), response);
		}
		return new ModelAndView(POPUP_EXIBIR_INFORMACOES_CONSOLIDADAS);
		
		
	}
	 
	private byte[] gerarImpressaoFaturaSegundaViaRN(Fatura fatura, DocumentoFiscal documentoFiscal) throws GGASException {
		byte[] impressao = null;
		impressao = fachada.gerarImpressaoFaturaSegundaVia(fatura, documentoFiscal);
		return impressao;
	}

	protected void imprimirPDF(String nomeArquivo, byte[] relatorio, HttpServletResponse response) {
		if (!nomeArquivo.isEmpty()) {
			if (relatorio != null) {
				try {
					response.setContentType("application/pdf");
					response.setContentLength(relatorio.length);
					response.addHeader(CONTENT_DISPOSITION, "attachment; filename=" + nomeArquivo + ".pdf");
					ServletOutputStream servletOutputStream = response.getOutputStream();
					servletOutputStream.write(relatorio, 0, relatorio.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				} catch (IOException e) {
					LOG.error(e);
					throw new InfraestruturaException();
				}
			}
		}
	}
	
	
	/**
	 * Enviar email chamado faturamento.
	 *
	 * @param email the email
	 * @param idFatura the id fatura
	 * @param idDocumentoFiscal the id documento fiscal
	 * @param codigoTipoPessoa the codigo tipo pessoa
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("enviarEmailChamadoFaturamento")
	public ModelAndView enviarEmailChamadoFaturamento(@RequestParam(EMAIL) String email,
			@RequestParam(CHAVE_PRIMARIA_FATURA) Long idFatura,
			@RequestParam(CHAVE_PRIMARIA_DOC_FISCAL) Long idDocumentoFiscal, HttpServletRequest request)
			throws GGASException {
		boolean isEmailEnviado = false;
		if(email != null && !email.isEmpty() && idFatura != null && idDocumentoFiscal != null){
			Fatura fatura = obterFaturaPorIdRN(idFatura); 
			fatura.setDadosAuditoria(getDadosAuditoria(request));

			DocumentoFiscal documentoFiscal = null;
			documentoFiscal = obterDocFiscalRN(idDocumentoFiscal);
			byte[] faturaRelatorio = gerarImpressaoFaturaSegundaViaRN(fatura, documentoFiscal);

			isEmailEnviado = montarEmailChamadoFaturamentoRN(email, faturaRelatorio, idFatura);
			
		}

		Cliente cliente = obterClientePorFaturaRN(idFatura);
		return exibirInformacoesConsolidadas(cliente.getChavePrimaria(), null, request, isEmailEnviado);
	}

	private boolean montarEmailChamadoFaturamentoRN(String email, byte[] faturaRelatorio, Long idFatura) throws NegocioException {
		String assunto = "Fatura Online";
		String attachmentFilename = "Fatura Nº " + idFatura;
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		String conteudoEmail = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEXTO_EMAIL_FATURA);
		return	enviarArquivoPorEmail(email, faturaRelatorio, conteudoEmail, assunto, attachmentFilename);
	}

	private boolean enviarArquivoPorEmail(String email, byte[] arquivo, String corpoMsg, String assunto,
			String nomeAnexo) throws NegocioException {
		ControladorConstanteSistema constanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		if (email != null) {
			ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(arquivo, "application/pdf");
			try {
				String valorConstanteSistemaEmailRemetentePadrao = constanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
				getJavaMailUtil().enviar(valorConstanteSistemaEmailRemetentePadrao, email, assunto, corpoMsg, nomeAnexo,
						byteArrayDataSource);
				return true;
			} catch (GGASException e) {
				LOG.error(e.getStackTrace(), e);
				throw new NegocioException(Constantes.ERRO_ENVIO_ARQUIVO_EMAIL, true);
			}
		}
		return false;
	}
	
	private JavaMailUtil getJavaMailUtil() {
		return (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);
	}
	
	/**
	 * obterClientePorFaturaRN.
	 *
	 * @param idFatura the id fatura
	 * @return the cliente
	 * @throws GGASException the GGAS exception
	 */
	public Cliente obterClientePorFaturaRN(Long idFatura) throws GGASException{
		ControladorCliente controladorClienteTmp = (ControladorCliente) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
		Cliente cliente = null;
		if (idFatura != null) {
			cliente = controladorClienteTmp.obterClientePorFatura(idFatura);
		}

		return cliente;
	}

	/**
	 * obterFaturaPorIdRN.
	 *
	 * @param idFatura the id fatura
	 * @return the fatura
	 * @throws GGASException the GGAS exception
	 */
	public Fatura obterFaturaPorIdRN(Long idFatura) throws GGASException {
		Fatura fatura = null;
		if (idFatura != null) {
			fatura = fachada.obterFatura(idFatura, "listaFaturaItem", "faturaGeral");
		}
		return fatura;
	}
	
	/**
	 * obterDocFiscalRN.
	 *
	 * @param idDocumentoFiscal the id documento fiscal
	 * @return the documentoFiscal
	 * @throws NegocioException the Negocio Exception
	 */
	public DocumentoFiscal obterDocFiscalRN(Long idDocumentoFiscal) throws NegocioException {
		DocumentoFiscal documentoFiscal = null;
		if (idDocumentoFiscal != null) {
			documentoFiscal = fachada.obterDocumentoFiscalPorChave(idDocumentoFiscal);
		}
		return documentoFiscal;
	}

	
	/**
	 * imprimirSegundaViaCobranca.
	 *
	 * @param idCliente - {@link Long}
	 * @param idFatura - {@link Long}
	 * @param idDocumentoFiscal - {@link Long}
	 * @param request - {@link HttpServletRequest}
	 * @param response - {@link HttpServletResponse}
	 * @return exibirInformacoesConsolidadas - {@link ModelAndView}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("imprimirSegundaViaCobranca")
	public ModelAndView imprimirSegundaViaCobranca(@RequestParam(CHAVE_PRIMARIA) Long idCliente,
			@RequestParam(CHAVE_PRIMARIA_FATURA) Long idFatura, @RequestParam(CHAVE_PRIMARIA_DOC_COBR) Long idDocumentoFiscal,
			HttpServletRequest request, HttpServletResponse response) throws GGASException {
		// Cliente cliente = obterClienteRN(idCliente);
		Fatura fatura = obterFaturaPorIdRN(idFatura);
		DocumentoFiscal documentoFiscal = obterDocFiscalRN(idDocumentoFiscal);

		imprimirPDF("relatorioCobrancaNotaFiscal", gerarImpressaoCobrancaSegundaViaRN(fatura, documentoFiscal), response);
		return exibirInformacoesConsolidadas(idCliente, null, request, false);
	}

	/**
	 * enviar Email Chamado Cobranca
	 * @param email the  email
	 * @param idFatura the idFatura
	 * @param idCliente the idCliente
	 * @param idDocumentoFiscal the id documento fiscal
	 * @param request the request
	 * @throws GGASException lançado caso haja falha na operação
	 * @return the ModelAndView the model and view
	 */
	@RequestMapping("enviarEmailChamadoCobranca")
	public ModelAndView enviarEmailChamadoCobranca(@RequestParam(EMAIL) String email, @RequestParam(CHAVE_PRIMARIA) Long idCliente,
			@RequestParam(CHAVE_PRIMARIA_FATURA) Long idFatura, @RequestParam(CHAVE_PRIMARIA_DOC_COBR) Long idDocumentoFiscal,
			HttpServletRequest request) throws GGASException {
		Fatura fatura = obterFaturaPorIdRN(idFatura);
		DocumentoFiscal documentoFiscal = obterDocFiscalRN(idDocumentoFiscal);

		byte[] faturaRelatorio =  gerarImpressaoCobrancaSegundaViaRN(fatura, documentoFiscal);
		boolean isEmailEnviado = montarEmailChamadoCobrancaRN(email, faturaRelatorio, idFatura);
		return exibirInformacoesConsolidadas(idCliente, null, request, isEmailEnviado);
	}
	
	private boolean montarEmailChamadoCobrancaRN(String email, byte[] faturaRelatorio, Long idFatura) throws NegocioException {
		String assunto = "Cobrança Online";
		String attachmentFilename = "Fatura Nº " + idFatura;
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		String conteudoEmail = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEXTO_EMAIL_FATURA);
		return enviarArquivoPorEmail(email, faturaRelatorio, conteudoEmail, assunto, attachmentFilename);
		
	}
	private byte[] gerarImpressaoCobrancaSegundaViaRN(Fatura fatura, DocumentoFiscal documentoFiscal) throws GGASException {
		byte[] impressao = null;
		impressao = fachada.gerarImpressaoFaturaSegundaVia(fatura, documentoFiscal);
		return impressao;
	}
	
	/**
	 * Carregar cliente por cpf ou cnpj.
	 *
	 * @param cpfCnpj - {@link String}
	 * @param response - {@link HttpServletResponse}
	 * @param session - {@link HttpSession}
	 * @return model - {@link ModelAndView}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@RequestMapping("carregarClientePorCpfCnpj")
	public ModelAndView carregarClientePorCpfCnpj(@RequestParam("cpfCnpj") String cpfCnpj,
			HttpServletResponse response,
			HttpSession session) throws NegocioException{

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO);
		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);

		if (cpfCnpj != null) {
			cpfCnpj = StringUtil.retirarMascara(cpfCnpj);
			try {
				Cliente cliente = pesquisarClientePorCpfCnpjRN(cpfCnpj);
				Chamado chamado = new Chamado();
				chamado.setCliente(cliente);
				model.addObject(CHAMADO, chamado);
				model.addObject(CLIENTE, cliente);
				if (cliente == null) {
					mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
				} else {
					model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return model;
	}

	/**
	 * pesquisar ClientePor Cpf CnpjRN.
	 * @param cpfCnpj
	 *            the cpfCnpj
	 * @return the Cliente and Cliente
	 * @throws NegocioException the Negocio Exception
	 */
	public Cliente pesquisarClientePorCpfCnpjRN(String cpfCnpj) throws NegocioException {
		return ServiceLocator.getInstancia().getControladorCliente()
				.obterClientePorCpfCnpj(cpfCnpj);
	}
	
	
	public void salvarRelatoriosChamado(byte[] relatorio, HttpServletResponse response) throws NegocioException {
		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		if(relatorio != null) {

			ServletOutputStream servletOutputStream;
			try {
				servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION, "attachment; filename=relatorioChamado.pdf");
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e.getMessage());
				throw new NegocioException(e);
			}

		}
	}
	
	
	/**
	 * Método responsável por imprimir a segunda via da nota de crédito / débito.
	 * 
	 * @param model
	 *            - {@link Model}
	 * @param request
	 *            - {@link HttpServletRequest}
	 * @param response
	 *            - {@link HttpServletResponse}
	 * @param idNotaDebitoCredito
	 *            - {@link Long}
	 * @return view - {@link String}
	 */
	@RequestMapping("imprimirSegundaViaNotaChamado")
	public ModelAndView imprimirSegundaViaNotaChamado(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "chavePrimaria", required = false) Long idNotaDebitoCredito) throws Exception {

		Long idPontoConsumo = 0l;
		try {
			if ((idNotaDebitoCredito != null) && (idNotaDebitoCredito > 0)) {

				Fatura notaDebitoCredito = (Fatura) controladorCobranca.obterFatura(idNotaDebitoCredito,
						"listaFaturaItem", "faturaGeral");
				notaDebitoCredito.setDadosAuditoria(getDadosAuditoria(request));
				idPontoConsumo = notaDebitoCredito.getPontoConsumo().getChavePrimaria();

				byte[] relatorioNotaDebitoCredito = gerarRelatorioFatura(idNotaDebitoCredito, notaDebitoCredito);

				if (relatorioNotaDebitoCredito != null) {
					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentType("application/pdf");
					response.setContentLength(relatorioNotaDebitoCredito.length);
					response.addHeader("Content-Disposition", "attachment; filename=relatorioNotaDebitoCredito.pdf");
					servletOutputStream.write(relatorioNotaDebitoCredito, 0, relatorioNotaDebitoCredito.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				}
			}
		} catch (GGASException e) {
			super.mensagemErroParametrizado(model, request, e);
		}
		return visualizarInformacoesPontoConsumo(idPontoConsumo, null);
	}

	private byte[] gerarRelatorioFatura(Long idNotaDebitoCredito, Fatura notaDebitoCredito)
			throws GGASException, NegocioException {
		byte[] relatorioNotaDebitoCredito = controladorFatura.gerarRelatorioNotaDebitoCredito(notaDebitoCredito,
				Boolean.TRUE);
		
		
		if (relatorioNotaDebitoCredito == null) {
			DocumentoFiscal documentoFiscal = null;

			documentoFiscal = controladorFatura.obterDocumentoFiscalPorCodigoFatura(idNotaDebitoCredito, Boolean.TRUE);
			
			relatorioNotaDebitoCredito = controladorFatura.gerarImpressaoFaturaSegundaVia(notaDebitoCredito, documentoFiscal);
		}
		return relatorioNotaDebitoCredito;
	}
	
	
	/**
	 * Metódo responsável por enviar fatura por email
	 * 
	 * @param faturaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("enviarEmailFaturaChamado")
	public ModelAndView enviarEmailFaturaChamado(Model model, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "chavePrimaria", required = false) Long idNotaDebitoCredito) throws GGASException {
		Long idPontoConsumo = 0l;
		try {
			if ((idNotaDebitoCredito != null) && (idNotaDebitoCredito > 0)) {
				Fatura notaDebitoCredito = (Fatura) controladorCobranca.obterFatura(idNotaDebitoCredito,
						"listaFaturaItem", "faturaGeral");
				notaDebitoCredito.setDadosAuditoria(getDadosAuditoria(request));
				idPontoConsumo = notaDebitoCredito.getPontoConsumo().getChavePrimaria();
				enviarFatura(idNotaDebitoCredito, null, request, model);
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return visualizarInformacoesPontoConsumo(idPontoConsumo, null);
	}
	
	/**
	 * Método privado de envio das faturas
	 * 
	 * @throws GGASException
	 */
	private void enviarFatura(Long idFatura, Long chavePrimariaDocFiscal, HttpServletRequest request, Model model) throws GGASException {

		if ((idFatura != null) && (idFatura > 0)) {
			Fatura fatura = (Fatura) controladorCobranca.obterFatura(idFatura, "listaFaturaItem", "faturaGeral");
			fatura.setDadosAuditoria(getDadosAuditoria(request));

			byte[] faturaRelatorio = gerarRelatorioFatura(idFatura, fatura);

			enviarFaturaPorEmail(faturaRelatorio, idFatura, model);

			mensagemSucesso(model, Fatura.SUCESSO_ENVIO_FATURA);
		}

	}
	
	/**
	 * Método para envio do relatório
	 * 
	 * @param faturaRelatorio {@link byte}
	 * @param idFatura {@link Long}
	 * @throws NegocioException {@link NegocioException}
	 */
	private void enviarFaturaPorEmail(byte[] faturaRelatorio, Long idFatura, Model model) throws NegocioException {
		String assunto = ASSUNTO_EMAIL_FATURA;
		String attachmentFilename = NOME_ANEXO_EMAIL_FATURA + idFatura;
		String conteudoEmail = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEXTO_EMAIL_FATURA);
		enviarArquivoPorEmail(faturaRelatorio, idFatura, conteudoEmail, assunto, attachmentFilename, model);
	}
	
	/**
	 * Método para envio de um arquivo por e-mail
	 * 
	 */
	private void enviarArquivoPorEmail(byte[] arquivo, Long idFatura, String corpoMsg, String assunto, String nomeAnexo, Model model)
			throws NegocioException {

		Cliente cliente = controladorCliente.obterClientePorFatura(idFatura);
		String email;
		if (cliente.getEmailPrincipal() != null && !cliente.getEmailPrincipal().isEmpty()) {
			email = cliente.getEmailPrincipal();
		} else {
			email = cliente.getEmailSecundario();
		}
		if (email == null) {
			throw new NegocioException(Constantes.CLIENTE_ERRO_EMAIL, true);
		}
		ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(arquivo, "application/pdf");
		try {
			String valorConstanteSistemaEmailRemetentePadrao =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
			getJavaMailUtil().enviar(valorConstanteSistemaEmailRemetentePadrao, email, assunto, corpoMsg, nomeAnexo, byteArrayDataSource);
		} catch (GGASException e) {
			LOG.error(e.getStackTrace(), e);
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_ENVIO_ARQUIVO_EMAIL, true));
		}
	}
	
	/**
	 * Método responsável por gerar o extrato de quitação anual.
	 * 
	 * @param extratoDebitoVO
	 *            {@link ExtratoDebitoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws IOException 
	 * @throws GGASException 
	 */
	@RequestMapping("gerarExtratoQuitacaoAnualChamado")
	public ModelAndView gerarExtratoQuitacaoAnualChamado(ExtratoDebitoVO extratoDebitoVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException, GGASException {

		Boolean indicadorPesquisaGeral = false;
		
		try {

			controladorCobranca.validarFiltroDeclaracaoQuitacaoAnual(extratoDebitoVO.getAnoGeracao(),
					extratoDebitoVO.getIdCliente(), extratoDebitoVO.getIdPontoConsumo(),
					extratoDebitoVO.getNumeroContrato(), indicadorPesquisaGeral);

			byte[] relatorioDeclaracaoQuitacaoAnual = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnual(
					extratoDebitoVO.getAnoGeracao(), extratoDebitoVO.getIdCliente(),
					extratoDebitoVO.getIdPontoConsumo(), null);

			if (relatorioDeclaracaoQuitacaoAnual != null) {
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType("application/pdf");
				response.setContentLength(relatorioDeclaracaoQuitacaoAnual.length);
				response.addHeader("Content-Disposition", "attachment; filename=relatorioExtratoDebito.pdf");
				servletOutputStream.write(relatorioDeclaracaoQuitacaoAnual, 0, relatorioDeclaracaoQuitacaoAnual.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} else {
				// exibir mensagem de processo
				// encaminhado para batch
				// ou mensagem de débitos em aberto
				// caso haja algum e não gere a
				// declaração de quitação anual.
			}
		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return visualizarInformacoesPontoConsumo(extratoDebitoVO.getIdPontoConsumo(), null);
	}
	
	
	/**
	 * Método responsável por salvar o débito automatico do contrato.
	 * 
	 * @param contratoVO
	 *            {@link ContratoVO}
	 * @param result
	 *            {@link BindingResult}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param response
	 *            {@link HttpServletResponse}
	 * @param model
	 *            {@link Model}
	 * @return String {@link String}
	 * @throws IOException 
	 * @throws GGASException 
	 */
	@RequestMapping("salvarDebitoAutomaticoContrato")
	public ModelAndView salvarDebitoAutomaticoContrato(ContratoVO contratoVO, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) throws IOException, GGASException {
		
		try {
			Long chavePrimaria = contratoVO.getChavePrimaria();

			Contrato contrato = (Contrato) controladorContrato.obter(chavePrimaria);

			validarDadosDebitoAutomatico(contratoVO);
			contrato.setIndicadorDebitoAutomatico(Boolean.valueOf(contratoVO.getDebitoAutomatico()));

			if (Boolean.valueOf(contratoVO.getDebitoAutomatico())) {
				Banco banco = (Banco) controladorBanco.obter(Long.valueOf(contratoVO.getBanco()));

				contrato.setAgencia(contratoVO.getAgencia());
				contrato.setContaCorrente(contratoVO.getContaCorrente());
				contrato.setBanco(banco);
			} else {
				contrato.setAgencia(null);
				contrato.setContaCorrente(null);
				contrato.setBanco(null);
			}

			controladorContrato.atualizar(contrato);
			mensagemSucesso(model, Constantes.CONTRATO_ALTERADO_SUCESSO, contrato.getNumeroFormatado());

		} catch (GGASException e) {
			mensagemErroParametrizado(model, request, e);
		}

		return visualizarInformacoesPontoConsumo(contratoVO.getIdPontoConsumo(), null);
	}

	private void validarDadosDebitoAutomatico(ContratoVO contratoVO) throws NegocioException {
		if(Boolean.valueOf(contratoVO.getDebitoAutomatico())) {
			StringBuilder erros = new StringBuilder();
			Long idBanco = Long.valueOf(contratoVO.getBanco());
			if(idBanco.compareTo(0l) <= 0) {
				erros.append("Banco,");
			}
			
			if(contratoVO.getAgencia() == null || contratoVO.getAgencia().isEmpty()) {
				erros.append("Agência,");
			}
			
			if(contratoVO.getContaCorrente() == null || contratoVO.getContaCorrente().isEmpty()) {
				erros.append("Conta Corrente,");
			}
			
			if(erros.length() > 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
						new String[] {
								erros.toString().substring(0,
										erros.toString().length() - 1)});
			}
	
		}
	}
	
	
	/**
	 * Método responsável por exibir a tela de pesquisa para visualizar ponto de consumo chamado.
	 *
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirPesquisaVisualizarPontoConsumo")
	public ModelAndView exibirPesquisaVisualizarPontoConsumo(ChamadoVO chamadoVO) throws NegocioException {
		ModelAndView model = new ModelAndView("exibirPesquisaVisualizarPontoConsumo");
		
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
		if(chamadoVO.getIdPontoConsumo() != null) {
			listaPontoConsumo.add(controladorPontoConsumo.obterPontoConsumo(chamadoVO.getIdPontoConsumo()));
			model.addObject("listaPontoConsumo", listaPontoConsumo);
		}
		return model;
	}
	
	/**
	 * Método responsável por pesquisar os pontos de consumo de um cliente ou um imóvel na tela de pesquisa de visualizar informações do ponto consumo.
	 *
	 * @param faturaPesquisaVO {@link FaturaPesquisaVO}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisaPontoConsumoChamado")
	public ModelAndView pesquisaPontoConsumoChamado(ChamadoVO chamadoVO, BindingResult result, HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaVisualizarPontoConsumo");
		
		try {
			salvarPesquisaFiltro(chamadoVO, model);
			Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
			if (verificaCampo(chamadoVO.getIdImovel())) {
				listaPontoConsumo = controladorImovel.carregarPontoConsumoPorChaveImovel(chamadoVO.getIdImovel(), Boolean.FALSE);
			} else if (verificaCampo(chamadoVO.getIdCliente())) {
				listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorChaveCliente(chamadoVO.getIdCliente());
			}
			
			if(listaPontoConsumo.size() == 1) {
					return visualizarInformacoesPontoConsumo(listaPontoConsumo.iterator().next().getChavePrimaria(), null);
			} else {
				model.addObject("listaPontoConsumo", listaPontoConsumo);
			}
			
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return model;
	}
	
	private void salvarPesquisaFiltro(ChamadoVO chamadoVO, ModelAndView model) throws NegocioException {
		model.addObject("faturaForm", chamadoVO);
		if (verificaCampo(chamadoVO.getIdCliente())) {
			Cliente cliente = controladorCliente.obterCliente(chamadoVO.getIdCliente(), "enderecos");
			model.addObject("cliente", cliente);
		} else {
			if (verificaCampo(chamadoVO.getIdImovel())) {
				ImovelImpl imovel = (ImovelImpl) controladorImovel.obter(chamadoVO.getIdImovel());
				model.addObject("imovel", imovel);
			}
		}
	}
	
	private boolean verificaCampo(Object campo) {
		boolean campoValido = false;

		if (campo != null) {
			if (campo instanceof String) {
				campoValido = true;
			} else if (campo instanceof Integer) {
				campoValido = (Integer) campo > 0;
			} else {
				campoValido = (Long) campo > 0;
			}
		}

		return campoValido;
	}
	
	
	/**
	 * Método responsável por exibir a tela pesquisa de Cliente
	 * 
	 * @param model {@link Model}
	 * @return exibirPesquisaClientePopup {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirGraficoVolumeGSA")
	public String exibirGraficoVolumeGSA(Model model, @RequestParam("chavePontoConsumo") String chavePontoConsumo) throws GGASException {
		
		PontoConsumo pontoConsumo = fachada.buscarPontoConsumoPorChave(Long.valueOf(chavePontoConsumo));
		
		DateTime dataAtual = new DateTime();
		String dataFim = Util.obterUltimoDiaMes(dataAtual.minusMonths(1).toDate(), Constantes.FORMATO_DATA_BR);
		String dataInicio = Util.obterPrimeiroDiaMes(dataAtual.minusYears(1).toDate(), Constantes.FORMATO_DATA_BR);
		
		Collection<HistoricoLeituraGSAVO> leiturasNoPeriodo = controladorHistoricoMedicao.obterVolumeFaturadoGSANoPeriodo(pontoConsumo, dataInicio, dataFim);
		
		model.addAttribute("leiturasNoPeriodo", leiturasNoPeriodo);
		
		
		return "exibirGraficoVolumeGSA";
	}
	
	@RequestMapping("consultarTitulosAbertos")
	public ModelAndView consultarTitulosAbertos(@RequestParam(value = "cnpjCpf", required = false) String cnpjCpf,
			HttpSession session) throws GGASException {
		ModelAndView model = new ModelAndView("gridTituloAbertoBootstrap");
		session.removeAttribute("listaTitulosAbertosSessao");
		session.removeAttribute("listaTitulosAnalise");

		
		cnpjCpf = cnpjCpf.replace(".", "").replace("-", "").replace("/", "");
		
		Collection<TitulosGSAVO> listaTitulosAbertos = controladorFatura.obterTitulosAbertoCliente(cnpjCpf);
		
		session.setAttribute("listaTitulosAbertosSessao", listaTitulosAbertos);
		model.addObject("listaTitulosAbertos", listaTitulosAbertos);
		
		return model;
	}
	
	@RequestMapping("adicionarTituloAnalise")
	public ModelAndView adicionarTituloAnalise(@RequestParam(value = "titulosAbertos", required = false) String[] titulosAbertos,
			HttpSession session) throws GGASException {
		ModelAndView model = new ModelAndView("gridTituloAnaliseBootstrap");
		
		Collection<TitulosGSAVO> listaTitulosAbertos = (Collection<TitulosGSAVO>) session.getAttribute("listaTitulosAbertosSessao");
		Collection<TitulosGSAVO> listaTitulosAnalise = new ArrayList<TitulosGSAVO>();
				
		if(session.getAttribute("listaTitulosAnalise") != null ) {
			listaTitulosAnalise = (Collection<TitulosGSAVO>) session.getAttribute("listaTitulosAnalise");
		}
				
				
		listaTitulosAnalise.addAll(listaTitulosAbertos.stream()
				.filter(titulo -> Arrays.asList(titulosAbertos).contains(titulo.getSequencialTitulo()))
				.collect(Collectors.toList()));
		
		model.addObject("listaTitulosAnalise", listaTitulosAnalise);
		session.setAttribute("listaTitulosAnalise", listaTitulosAnalise);
		
		return model;
	}
	
	
	@RequestMapping("removerTituloAnalise")
	public ModelAndView removerTituloAnalise(@RequestParam(value = "tituloRemover", required = false) String tituloRemover,
			HttpSession session) throws GGASException {
		ModelAndView model = new ModelAndView("gridTituloAnaliseBootstrap");
		
		Collection<TitulosGSAVO> listaTitulosAnalise = (Collection<TitulosGSAVO>) session.getAttribute("listaTitulosAnalise");
		
		listaTitulosAnalise.removeIf(p -> p.getSequencialTitulo().equals(tituloRemover));
		
		model.addObject("listaTitulosAnalise", listaTitulosAnalise);
		session.setAttribute("listaTitulosAnalise", listaTitulosAnalise);
		
		return model;
	}	
	
	
	
	@RequestMapping("atualizarTitulosAbertos")
	public ModelAndView atualizarTitulosAberto(HttpSession session) throws GGASException {
		ModelAndView model = new ModelAndView("gridTituloAbertoBootstrap");
		
		Collection<TitulosGSAVO> listaTitulosAbertos = atualizarLstaTitulosAberto(session);
		
		model.addObject("listaTitulosAbertos", listaTitulosAbertos);
		
		return model;
	}

	private Collection<TitulosGSAVO> atualizarLstaTitulosAberto(HttpSession session) {
		
		Collection<TitulosGSAVO> listaTitulosAbertosSessao = (Collection<TitulosGSAVO>) session
				.getAttribute("listaTitulosAbertosSessao");
		
		Collection<TitulosGSAVO> listaTitulosAbertos = null;
		
		if(listaTitulosAbertosSessao != null) {
			listaTitulosAbertos = new ArrayList<TitulosGSAVO>(listaTitulosAbertosSessao);
		} 
		
		Collection<TitulosGSAVO> listaTitulosAnalise = (Collection<TitulosGSAVO>) session.getAttribute("listaTitulosAnalise");
		
		if (listaTitulosAnalise != null && listaTitulosAbertos != null) {
			Set<String> titulosEmAnalise = listaTitulosAnalise.stream().map(TitulosGSAVO::getSequencialTitulo)
					.collect(Collectors.toSet());

			listaTitulosAbertos.removeIf(titulo -> titulosEmAnalise.contains(titulo.getSequencialTitulo()));
		}
		
		return listaTitulosAbertos;
	}

	
	@RequestMapping("consultarCepLogradouro")
	public ModelAndView consultarCepPorLogradouro(
			@RequestParam(value = "logradouro", required = false) String logradouro,
			@RequestParam(value = "tipoLogradouro", required = false) String tipoLogradouro,
			HttpSession session) throws GGASException{
		ModelAndView model = new ModelAndView(TELA_GRID_ENDERECO);
		List<Cep> listaCeps = controladorEndereco.consultarCepsPorLougradouro(logradouro,tipoLogradouro);
		session.setAttribute(LISTA_CEPS, listaCeps);
		return model;
	}

	@RequestMapping("carregarEndereco")
	public ModelAndView carregarEndereco(
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			HttpServletResponse response, HttpSession session) {

		ModelAndView model = new ModelAndView(GRID_CHAMADO_ENDERECO);

		try {
			Cep cep = (Cep) controladorEndereco.obterCepPorChave(chavePrimaria);
			List<Cep> listaChamadoCeps = new ArrayList<Cep>();
			listaChamadoCeps.add(cep);
//			model.addObject("listaChamadoCeps", listaChamadoCeps);
			session.setAttribute(LISTA_CHAMADO_CEPS, listaChamadoCeps);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			try {
				response.sendError(1);
			} catch (IOException e1) {
				LOG.error(e1);
			}
		}

		return model;
	}
	
	private void carregarEnderecoChamado(Chamado chamado, ModelAndView model) throws NegocioException {
		
		Cep cepChamado = (Cep) controladorEndereco.obterCepPorChave(chamado.getEnderecoChamado().getChavePrimaria());
		List<Cep> listaChamadoCeps = new ArrayList<Cep>();
		listaChamadoCeps.add(cepChamado);
		model.addObject("listaChamadoCeps",listaChamadoCeps);
		model.addObject("numeroEndereco",chamado.getNumeroEndereco());
	}
	
	/**
	 * Carregar pontos consumo.
	 *
	 * @param fluxoExecucao the fluxo execucao
	 * @param response the response
	 * @return the model and view
	 */
	@RequestMapping("limparEnderecos")
	public ModelAndView limparEnderecos(@RequestParam("fluxoExecucao") String fluxoExecucao, HttpServletResponse response) {

		ModelAndView model = new ModelAndView(GRID_CHAMADO_ENDERECO);

		model.addObject("listaEnderecos", new ArrayList<Cep>());

		if (FLUXO_INCLUSAO.equals(fluxoExecucao)) {
			model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		} else {
			model.addObject(FLUXO_INCLUSAO, null);
		}
		return model;
	}
}
