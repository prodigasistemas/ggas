package br.com.ggas.atendimento.chamado.dominio;

/**
 * Classe responsável por representar uma categoria do chamado
 *
 */
public enum ChamadoCategoria {
	RECLAMACAO("RECLAMAÇÃO"), SOLICITACAO("SOLICITAÇÃO"), INFORMACAO("INFORMAÇÃO"), INTERNO("INTERNO");

	private String descricao;

	/**
	 * Atribui descricao
	 * 
	 * @param descricao
	 * 
	 */
	private ChamadoCategoria(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Retorna descricao
	 * 
	 * @return descricao
	 * 
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Retorna name
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name();
	}

	/**
	 * Retorna categoria por nome
	 * 
	 * @param name {@link String}
	 * @return categoria
	 */
	public static ChamadoCategoria getByName(String name) {
		for (ChamadoCategoria categoria : values()) {
			if (categoria.getName().equals(name)) {
				return categoria;
			}
		}
		throw new IllegalArgumentException(name + " não é um valor valido.");
	}
}
