
package br.com.ggas.atendimento.chamado.dominio;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação de um chamado.
 */
public class ChamadoUnidadeOrganizacional extends EntidadeNegocioImpl {

	/** Constante serialVersionUID. */
	private static final long serialVersionUID = -8519624329576816063L;

	/** unidade organizacional. */
	private UnidadeOrganizacional unidadeOrganizacional;

	/** chamado. */
	private Chamado chamado;

	/**
	 * Nova instancia da classe com valores default para versão, ultima alteração e habilitado
	 */
	public ChamadoUnidadeOrganizacional() {
		this.setUltimaAlteracao(new Date());
		this.setHabilitado(true);
		this.setVersao(1);
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	/**
	 * Obter a unidade organizacional.
	 *
	 * @return a unidade organizacional
	 */
	public UnidadeOrganizacional getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	/**
	 * Seta a unidade organizacional.
	 *
	 * @param unidadeOrganizacional a nova unidade organizacional
	 */
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * Obter o chamado.
	 *
	 * @return o chamado
	 */
	public Chamado getChamado() {
		return chamado;
	}

	/**
	 * Seta o chamado.
	 *
	 * @param chamado o novo chamado
	 */
	public void setChamado(Chamado chamado) {
		this.chamado = chamado;
	}

}
