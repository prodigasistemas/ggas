/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/01/2014 16:43:25
 @author ifrancisco
 */

package br.com.ggas.atendimento.chamado.dominio;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;

/**
 * ClienteVO
 */
public class ChamadoClienteVO {

	private Cliente cliente;

	private PontoConsumo pontoConsumo;
	
	private String nomeCliente;
	
	private String nome;
	
	private String endereco;
	
	private Chamado chamado;
	
	private String dataAberturaFormatada;
	
	private String dataResolucaoFormatada;
	
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	/**
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
	
		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}
	
	/**
	 * @return endereco
	 */
	public String getEndereco() {
	
		return endereco;
	}
	
	/**
	 * @param endereco
	 */
	public void setEndereco(String endereco) {
	
		this.endereco = endereco;
	}
	
	/**
	 * Retorna cliente
	 * 
	 * @return the cliente
	 */
	public Cliente getCliente() {

		return cliente;
	}

	/**
	 * Atribui cliente
	 * 
	 * @param clente the clente to set
	 */
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}


	public PontoConsumo getPontoConsumo() {
		return pontoConsumo;
	}


	public void setPontoConsumo(PontoConsumo pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}


	public Chamado getChamado() {
		return chamado;
	}


	public void setChamado(Chamado chamado) {
		this.chamado = chamado;
	}


	public String getDataAberturaFormatada() {
		return dataAberturaFormatada;
	}


	public void setDataAberturaFormatada(String dataAberturaFormatada) {
		this.dataAberturaFormatada = dataAberturaFormatada;
	}


	public String getDataResolucaoFormatada() {
		return dataResolucaoFormatada;
	}


	public void setDataResolucaoFormatada(String dataResolucaoFormatada) {
		this.dataResolucaoFormatada = dataResolucaoFormatada;
	}


}
