package br.com.ggas.atendimento.chamado.dominio;

import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class ChamadoTituloAnalise extends EntidadeNegocioImpl{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8162372051597953205L;
	private Chamado chamado;
	private String numeroTitulo;
	private Boolean emAnalise;
	private Usuario usuario;
	
	
	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}


	public Chamado getChamado() {
		return chamado;
	}


	public void setChamado(Chamado chamado) {
		this.chamado = chamado;
	}


	public String getNumeroTitulo() {
		return numeroTitulo;
	}


	public void setNumeroTitulo(String numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}


	public Boolean getEmAnalise() {
		return emAnalise;
	}


	public void setEmAnalise(Boolean emAnalise) {
		this.emAnalise = emAnalise;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
