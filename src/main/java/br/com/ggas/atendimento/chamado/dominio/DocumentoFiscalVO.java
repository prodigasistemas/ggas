package br.com.ggas.atendimento.chamado.dominio;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.ggas.atendimento.chamado.apresentacao.ChamadoAction;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.util.DataUtil;


/**
 * Classe responsável pela representação de valores.
 * 
 * @see ChamadoAction
 */
public class DocumentoFiscalVO {
	private Long docFiscalID; 
	private Long numeroDocumentoFiscal; 
	private Date periodoConsumoInicial; 
	private Date periodoConsumoFinal; 
	private BigDecimal consumoApurado; 
	private BigDecimal valorTotal; 
	private BigDecimal valorAtualizado; 
	private Date dataVencimento; 
	private Date dataPagamento; 
	private String situacaoDoPagamento; 
	private Long situacaoDoPagamentoID; 
	private Integer numeroContrato; 
	private BigDecimal valorTotalAtualizado; 
	private Fatura fatura;
	private Long faturaID;
	private Integer qtdDiasConsumo;
	private Integer anoContrato;
	
	private BigDecimal percentualMulta;
	private BigDecimal percentualJurosMora;
	
	private HistoricoConsumo historicoConsumo;
	private EntidadeConteudo situacaoPagamento;
	private Contrato contrato;
	
	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public HistoricoConsumo getHistoricoConsumo() {
		return historicoConsumo;
	}

	public void setHistoricoConsumo(HistoricoConsumo historicoConsumo) {
		this.historicoConsumo = historicoConsumo;
	}

	public Fatura getFatura() {
		return fatura;
	}

	public void setFatura(Fatura fatura) {
		this.fatura = fatura;
	}

	public Date getPeriodoConsumoInicial() {
		return periodoConsumoInicial;
	}

	public void setPeriodoConsumoInicial(Date periodoConsumoInicial) {
		if(periodoConsumoInicial != null) {
			this.periodoConsumoInicial = periodoConsumoInicial;
		} else {
			this.periodoConsumoFinal = null;
		}
	}

	public Date getPeriodoConsumoFinal() {
		return periodoConsumoFinal;
	}

	public void setPeriodoConsumoFinal(Date periodoConsumoFinal) {
		if(periodoConsumoFinal != null) {
			this.periodoConsumoFinal = periodoConsumoFinal;
		} else {
			this.periodoConsumoFinal = null;
		}
	}

	public String getPeriodoConsumoFormatado() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
		if(periodoConsumoFinal != null && qtdDiasConsumo != null){
			periodoConsumoInicial = DataUtil.decrementarDia(periodoConsumoFinal, (qtdDiasConsumo - 1));
			return sdf.format(periodoConsumoInicial) + " à " + sdf.format(periodoConsumoFinal);
		}
		return "-";
	}
	
	public BigDecimal getConsumoApurado() {
		return consumoApurado;
	}
	
	public String getConsumoApuradoFormatado() {
		if(consumoApurado != null){
			return consumoApurado.toPlainString();
		}
		return "-";
	}

	public void setConsumoApurado(BigDecimal consumoApurado) {
		this.consumoApurado = consumoApurado;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorAtualizado() {
		return valorAtualizado;
	}

	public void setValorAtualizado(BigDecimal valorAtualizado) {
		this.valorAtualizado = valorAtualizado;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		if(dataVencimento != null) {
			this.dataVencimento = (Date) dataVencimento.clone();
		} else {
			this.dataPagamento = null;
		}
	}

	public String getDataVencimentoFormatada() {
		if(dataVencimento != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(dataVencimento);
		}
		return "-";
	}
	
	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		if(dataPagamento != null) {
			this.dataPagamento = (Date) dataPagamento.clone();
		} else {
			this.dataPagamento = null;
		}
	}

	public String getDataPagamentoFormatada() {
		if (dataPagamento != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(dataPagamento);
		}
		return "-";
	}
	
	public String getSituacaoDoPagamento() {
		return situacaoDoPagamento;
	}

	public void setSituacaoDoPagamento(String situacaoDoPagamento) {
		this.situacaoDoPagamento = situacaoDoPagamento;
	}

	public Integer getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(Integer numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getNumeroContratoFormatado() {
		StringBuilder sb = new StringBuilder();
		if(numeroContrato != null && anoContrato != null){
			return sb.append(anoContrato).append(String.format("%05d", numeroContrato)).toString();
		}
		return "-";
	}
	
	public BigDecimal getValorTotalAtualizado() {
		return valorTotalAtualizado;
	}

	public void setValorTotalAtualizado(BigDecimal valorTotalAtualizado) {
		this.valorTotalAtualizado = valorTotalAtualizado;
	}

	public Long getNumeroDocumentoFiscal() {
		return numeroDocumentoFiscal;
	}

	public void setNumeroDocumentoFiscal(Long numeroDocumentoFiscal) {
		this.numeroDocumentoFiscal = numeroDocumentoFiscal;
	}
	
	public String getNumeroDocumentoFiscalFormatado() {
		if(numeroDocumentoFiscal != null){
			return numeroDocumentoFiscal.toString();
		}
		return "-";
	}

	public EntidadeConteudo getSituacaoPagamento() {
		return situacaoPagamento;
	}

	public void setSituacaoPagamento(EntidadeConteudo situacaoPagamento) {
		this.situacaoPagamento = situacaoPagamento;
	}

	public Long getFaturaID() {
		return faturaID;
	}

	public void setFaturaID(Long faturaID) {
		this.faturaID = faturaID;
	}

	public Integer getQtdDiasConsumo() {
		return qtdDiasConsumo;
	}

	public void setQtdDiasConsumo(Integer qtdDiasConsumo) {
		this.qtdDiasConsumo = qtdDiasConsumo;
	}
	
	public BigDecimal getPercentualMulta() {
		return percentualMulta;
	}

	public void setPercentualMulta(BigDecimal percentualMulta) {
		this.percentualMulta = percentualMulta;
	}

	public BigDecimal getPercentualJurosMora() {
		return percentualJurosMora;
	}

	public void setPercentualJurosMora(BigDecimal percentualJurosMora) {
		this.percentualJurosMora = percentualJurosMora;
	}

	public Long getSituacaoDoPagamentoID() {
		return situacaoDoPagamentoID;
	}

	public void setSituacaoDoPagamentoID(Long situacaoDoPagamentoID) {
		this.situacaoDoPagamentoID = situacaoDoPagamentoID;
	}

	public Long getDocFiscalID() {
		return docFiscalID;
	}

	public void setDocFiscalID(Long docFiscalID) {
		this.docFiscalID = docFiscalID;
	}

	public Integer getAnoContrato() {
		return anoContrato;
	}

	public void setAnoContrato(Integer anoContrato) {
		this.anoContrato = anoContrato;
	}
	/**
	* Contem os Fields de posssiveis valores
	* 
	*/
	public enum Fields {
		NUMERO("numeroDocumentoFiscal"),
		fatura("fatura"),
		faturaID("faturaID"),
		valorTotal("valorTotal"),
		
		historicoConsumo("historicoConsumo"),
		consumoApurado("consumoApurado"),
		
		entidadeConteudo("entidadeConteudo"),
		situacaoDoPagamentoDescricao("situacaoDoPagamento"),
		situacaoDoPagamentoID("situacaoDoPagamentoID"),
		
		contrato("contrato"),
		numeroContrato("numeroContrato"),
		percentualMulta("percentualMulta"),
		percentualJurosMora("percentualJurosMora"),
		
		periodoConsumoInicial("periodoConsumoInicial"),
		periodoConsumoFinal("periodoConsumoFinal"),
		valorAtualizado("valorAtualizado"),
		dataVencimento("dataVencimento"),
		dataPagamento("dataPagamento"),
		valorTotalAtualizado("valorTotalAtualizado"),
		qtdDiasConsumo("qtdDiasConsumo"),
		docFiscalID("docFiscalID"),
		anoContrato("anoContrato"),
		;

		private String fields;

		private Fields(String fields) {
			this.fields = fields;
		}

		@Override
		public String toString() {
			return this.fields;
		}
	}

}
