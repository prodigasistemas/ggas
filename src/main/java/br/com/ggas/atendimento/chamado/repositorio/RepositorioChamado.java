/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *16/10/2013
 * vpessoa
 * 20:52:20
 */

package br.com.ggas.atendimento.chamado.repositorio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.ibm.icu.util.Calendar;

import br.com.ggas.atendimento.chamado.dominio.AcaoChamado;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoAlteracaoVencimento;
import br.com.ggas.atendimento.chamado.dominio.ChamadoEmail;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.relatorio.ChamadoResumoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * RepositorioChamado
 */
@Repository
public class RepositorioChamado extends RepositorioGenerico {

	private static final String ALIAS_CHAMADO_TIPO = "chamadoTipo";

	private static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";

	private static final String IMOVEL_CHAVE_PRIMARIA = "imovel.chavePrimaria";

	private static final String DATA_PREVISAO_ENCERRAMENTO = "dataPrevisaoEncerramento";

	private static final String PROTOCOLO = "protocolo";

	private static final String STATUS = "status";

	private static final String CHAMADO_ASSUNTO_CHAMADO_TIPO = "chamadoAssunto.chamadoTipo";

	private static final String CHAMADO_ASSUNTO = "chamadoAssunto";

	private static final String CANAL_ATENDIMENTO = "canalAtendimento";

	private static final String UNIDADE_ORGANIZACIONAL = "unidadeOrganizacional";

	private static final String USUARIO_RESPONSAVEL = "usuarioResponsavel";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String IMOVEL_QUADRA_FACE = "imovel.quadraFace";

	private static final String IMOVEL = "imovel";

	private static final String CONTRATO = "contrato";

	private static final String CLIENTE_FONES = "cliente.fones";

	private static final String CLIENTE = "cliente";

	private static final String WHERE = " where ";

	private static final String FROM = "  from ";

	private static final int LIMITE_MIN_SEG = 59;

	private static final int LIMITE_HORAS = 23;

	private static final int TAMANHO_CNPJ = 14;

	private static final String PROTOCOLO_NUMERO_PROTOCOLO = "protocolo.numeroProtocolo";

	private static final Logger LOG = Logger.getLogger(RepositorioGenerico.class);

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(value = "controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/**
	 * Instantiates a new repositorio chamado.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioChamado(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Chamado();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return Chamado.class;
	}

	public Class<?> getClasseEntidadeChamadoHistorico() {

		return ChamadoHistorico.class;
	}

	public Class<?> getClasseEntidadeChamadoProtocolo() {

		return ChamadoProtocolo.class;
	}

	public Class<?> getClasseEntidadeChamadoAlteracaoVencimento() {

		return ChamadoAlteracaoVencimento.class;
	}

	public Class<?> getClasseEntidadeChamadoEmail() {

		return ChamadoEmail.class;
	}

	/**
	 * Obter chamado.
	 * 
	 * @param chavePriamria
	 *            the chave priamria
	 * @return the chamado
	 */
	@SuppressWarnings("squid:S1192")
	public Chamado obterChamado(long chavePriamria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamado ");
		hql.append(" left join fetch chamado.listaUnidadeOrganizacionalVisualizadora listaUnidadesVisualizadoras ");
		hql.append(" left join fetch listaUnidadesVisualizadoras.unidadeOrganizacional ");
		hql.append(" left join fetch chamado.cliente cliente ");
		hql.append(" left join fetch chamado.cliente.contatos contatos");
		hql.append(" left join fetch chamado.contrato contrato ");
		hql.append(" left join fetch chamado.imovel imovel ");
		hql.append(" left join fetch chamado.pontoConsumo pontoConsumo ");
		hql.append(" left join fetch chamado.imovel.quadraFace quadraFace ");
		hql.append(" left join fetch chamado.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(" left join fetch chamado.usuarioResponsavel usuarioResponsavel ");
		hql.append(" left join fetch chamado.usuarioResponsavel.funcionario funcionario ");
		hql.append(" inner join fetch chamado.canalAtendimento canalAtendimento ");
		hql.append(" left join fetch chamado.chamadoAssunto chamadoAssunto ");
		hql.append(" left join fetch chamado.chamadoAssunto.enviarEmailPrioridadeProtocolo prioridadeProtocolo ");
		hql.append(" left join fetch chamado.chamadoAssunto.chamadoTipo chamadoTipo ");
		hql.append(" inner join fetch chamado.protocolo protocolo ");
		hql.append(" inner join fetch chamado.status status ");
		hql.append(" left join fetch chamadoAssunto.questionario questionario ");
		hql.append(" left join fetch questionario.listaPerguntas listaPerguntas ");
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePriamria);

		return (Chamado) query.uniqueResult();

	}

	/**
	 * Obter chamado tipo pesquisa satisfacao.
	 * 
	 * @param chavePriamria
	 *            the chave priamria
	 * @return the chamado
	 */
	public Chamado obterChamadoTipoPesquisaSatisfacao(long chavePriamria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamado ");
		hql.append(" left join fetch chamado.cliente cliente ");
		hql.append(" left join fetch chamado.cliente.fones fones ");
		hql.append(" left join fetch chamado.contrato contrato ");
		hql.append(" left join fetch chamado.imovel imovel ");
		hql.append(" left join fetch chamado.imovel.quadraFace quadraFace ");
		hql.append(" left join fetch chamado.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(" left join fetch chamado.usuarioResponsavel usuarioResponsavel ");
		hql.append(" left join fetch chamado.usuarioResponsavel.funcionario funcionario ");
		hql.append(" inner join fetch chamado.canalAtendimento canalAtendimento ");
		hql.append(" left join fetch chamado.chamadoAssunto chamadoAssunto ");
		hql.append(" left join fetch chamado.chamadoAssunto.chamadoTipo chamadoTipo ");
		hql.append(" inner join fetch chamado.protocolo protocolo");
		hql.append(" inner join fetch chamado.status status ");
		hql.append(" inner join fetch chamado.questionario questionario ");
		hql.append(" left join fetch chamado.questionario.listaPerguntas listaPerguntas ");
		hql.append(" left join fetch chamado.questionario.tipoServico tipoServico ");
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePriamria);

		return (Chamado) query.uniqueResult();

	}

	/**
	 * Consultar chamado.
	 * 
	 * @param chamadoVO
	 *            the chamado vo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException
	 *             the parse exception
	 */
	public Collection<Chamado> consultarChamado(ChamadoVO chamadoVO, Boolean isRelatorio) throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidade(), "root");
		Date dataFimPeriodo = null;

		criteria.setFetchMode(CLIENTE, FetchMode.JOIN);
		criteria.setFetchMode(CLIENTE_FONES, FetchMode.JOIN);
		criteria.setFetchMode("cliente.segmento", FetchMode.JOIN);
		criteria.setFetchMode(CONTRATO, FetchMode.JOIN);
		criteria.setFetchMode(IMOVEL, FetchMode.JOIN);
		criteria.setFetchMode(IMOVEL_QUADRA_FACE, FetchMode.JOIN);
		criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);
		criteria.setFetchMode("pontoConsumo.segmento", FetchMode.JOIN);
		criteria.setFetchMode(USUARIO_RESPONSAVEL, FetchMode.JOIN);
		criteria.setFetchMode(UNIDADE_ORGANIZACIONAL, FetchMode.JOIN);
		criteria.setFetchMode(CANAL_ATENDIMENTO, FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO_ASSUNTO, FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO_ASSUNTO_CHAMADO_TIPO, FetchMode.JOIN);

		criteria.setFetchMode(STATUS, FetchMode.JOIN);
		criteria.setFetchMode("chamadoHistoricos", FetchMode.JOIN);
		criteria.setFetchMode("listaUnidadeOrganizacionalVisualizadora", FetchMode.JOIN);
		
		criteria.createAlias(UNIDADE_ORGANIZACIONAL, UNIDADE_ORGANIZACIONAL, Criteria.LEFT_JOIN);
		criteria.createAlias(CHAMADO_ASSUNTO, CHAMADO_ASSUNTO, Criteria.LEFT_JOIN);
		criteria.createAlias(IMOVEL_QUADRA_FACE, IMOVEL_QUADRA_FACE, Criteria.LEFT_JOIN);
		criteria.createAlias(USUARIO_RESPONSAVEL, USUARIO_RESPONSAVEL, Criteria.LEFT_JOIN);
		criteria.createAlias(IMOVEL, IMOVEL, Criteria.LEFT_JOIN);
		criteria.createAlias(CONTRATO, CONTRATO, Criteria.LEFT_JOIN);
		criteria.createAlias(CLIENTE, CLIENTE, Criteria.LEFT_JOIN);
		criteria.createAlias("cliente.tipoCliente", "tipoCliente", Criteria.LEFT_JOIN);

		criteria.createAlias(CLIENTE_FONES, CLIENTE_FONES, Criteria.LEFT_JOIN);
		criteria.createAlias("chamadoHistoricos", "chamadoHistorico", Criteria.LEFT_JOIN);
		criteria.createAlias("chamadoHistorico.operacao", "operacao", Criteria.LEFT_JOIN);
		criteria.createAlias("chamadoHistorico.usuario", "usuario", Criteria.LEFT_JOIN);
		criteria.createAlias(STATUS, STATUS);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.segmento", "segmentoImovel", Criteria.LEFT_JOIN);
		criteria.createAlias("cliente.segmento", "segmentoCliente", Criteria.LEFT_JOIN);
		criteria.createAlias("listaUnidadeOrganizacionalVisualizadora", "unidadeVisualizadora", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.cep", "cepPontoConsumoChamado", Criteria.LEFT_JOIN);
		
		Criteria criteriaProtocolo = criteria.createCriteria(PROTOCOLO, PROTOCOLO, Criteria.LEFT_JOIN);

		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");

		criteria.add(Restrictions.eq("habilitado", true));
		Long protocolo = chamadoVO.getNumeroProtocolo();
		if(protocolo != null) {
			criteriaProtocolo.add(
					Restrictions.sqlRestriction(" {alias}.CHPR_NR_PROTOCOLO like ?", "%".concat(protocolo.toString()).concat("%"),
					StringType.INSTANCE));
		}

		adicionarRestricaoUnidadeOrganizacional(chamadoVO, criteria, isRelatorio);
		
		Long[] unidadesVisualizadorasIds = chamadoVO.getUnidadesVisualizadorasIds();

		if (unidadesVisualizadorasIds != null && unidadesVisualizadorasIds.length > 0) {
			criteria.add(Restrictions.in("unidadeVisualizadora.unidadeOrganizacional.chavePrimaria", unidadesVisualizadorasIds));
		}

		String cpfSolicitante = chamadoVO.getCpfSolicitante();
		String cnpjSolicitante = chamadoVO.getCnpjSolicitante();

		if(chamadoVO.getCpfCnpjSolicitante() != null && !chamadoVO.getCpfCnpjSolicitante().isEmpty()) {
			if(chamadoVO.getCpfCnpjSolicitante().length() == TAMANHO_CNPJ) {
				cpfSolicitante = chamadoVO.getCpfCnpjSolicitante();
			} else {
				cnpjSolicitante = chamadoVO.getCpfCnpjSolicitante();
			}
		}

		if(cpfSolicitante != null && !StringUtils.isEmpty(cpfSolicitante)) {
			criteria.add(Restrictions.eq("cpfCnpjSolicitante", cpfSolicitante));
		} else if(cnpjSolicitante != null && !StringUtils.isEmpty(cnpjSolicitante)) {
			criteria.add(Restrictions.eq("cpfCnpjSolicitante", cnpjSolicitante));
		}
		String nomeSolicitante = chamadoVO.getNomeSolicitante();
		if(nomeSolicitante != null && !StringUtils.isEmpty(nomeSolicitante)) {
			criteria.add(Restrictions.ilike("nomeSolicitante", Util.formatarTextoConsulta(nomeSolicitante)));
		}
		if(chamadoVO.getServicosTipo() != null && !chamadoVO.getServicosTipo().isEmpty()) {
			criteria.createAlias("chamadoAssunto.listaServicos", "listaServicos").add(
							Restrictions.in("listaServicos.servicoTipo", chamadoVO.getServicosTipo()));
		} else {
			Long idServicoTipo = chamadoVO.getIdServicoTipo();
			if(idServicoTipo != null && idServicoTipo > 0) {
				criteria.createAlias("chamadoAssunto.listaServicos", "listaServicos").createCriteria("listaServicos.servicoTipo")
								.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idServicoTipo));
			}
		}

		boolean associacaoChamadoTipo = false;

		if(chamadoVO.getChavesChamadoTipo() != null && !chamadoVO.getChavesChamadoTipo().isEmpty() && verificarExistenciaServicoTipo(chamadoVO.getChavesChamadoTipo())) {
			String[] ids = chamadoVO.getChavesChamadoTipo().split(",");
			List<Long> listaChaves = new ArrayList<>();
			for (int i = 0; i < ids.length; i++) {
				listaChaves.add(Long.valueOf(ids[i]));
			}
			criteria.add(Restrictions.in("chamadoAssunto.chamadoTipo.chavePrimaria", listaChaves));
		} else {
			Long idChamadoTipo = chamadoVO.getIdChamadoTipo();
			if (idChamadoTipo != null && idChamadoTipo > 0) {
				criteria.createAlias(CHAMADO_ASSUNTO_CHAMADO_TIPO, ALIAS_CHAMADO_TIPO);
				criteria.add(Restrictions.eq("chamadoTipo.chavePrimaria", idChamadoTipo));
				associacaoChamadoTipo = true;
			}
		}
		if(chamadoVO.getChamadoAssuntos() != null && !chamadoVO.getChamadoAssuntos().isEmpty()) {
			criteria.add(Restrictions.in(CHAMADO_ASSUNTO, chamadoVO.getChamadoAssuntos()));
		} else {
			ChamadoAssunto chamadoAssunto = chamadoVO.getChamadoAssunto();
			if(chamadoAssunto != null && chamadoAssunto.getChavePrimaria() > 0) {
				criteria.add(Restrictions.eq("chamadoAssunto.chavePrimaria", chamadoAssunto.getChavePrimaria()));
			}
		}

		if (chamadoVO.getCategoria() != null) {
			if (!associacaoChamadoTipo) {
				criteria.createAlias(CHAMADO_ASSUNTO_CHAMADO_TIPO, ALIAS_CHAMADO_TIPO);
			}
			criteria.add(Restrictions.eq("chamadoTipo.categoria", chamadoVO.getCategoria()));
		}

		if (chamadoVO.getIndicadorAcionamentoGasista() != null && chamadoVO.getIndicadorAcionamentoGasista()) {
			criteria.add(Restrictions.eq("indicadorAcionamentoGasista", true));
		}

		if (chamadoVO.getIndicadorAcionamentoPlantonista() != null && chamadoVO.getIndicadorAcionamentoPlantonista()) {
			criteria.add(Restrictions.eq("indicadorAcionamentoPlantonista", true));
		}

		if (chamadoVO.getIndicadorVazamentoConfirmado() != null && chamadoVO.getIndicadorVazamentoConfirmado()) {
			criteria.add(Restrictions.eq("indicadorVazamentoConfirmado", true));
		}

		List<CanalAtendimento> listaCanalAtendimento = chamadoVO.getListaCanalAtendimento();

		if (!Util.isNullOrEmpty(listaCanalAtendimento) && !Util.isAllNotNull(listaCanalAtendimento)) {
			List<Long> chavesPrimarias = Util.recuperarChavesPrimarias(listaCanalAtendimento);
			criteria.createAlias(CANAL_ATENDIMENTO, CANAL_ATENDIMENTO);
			criteria.add(Restrictions.in("canalAtendimento.chavePrimaria", chavesPrimarias));
		}
		UnidadeOrganizacional unidadeOrganizacional = chamadoVO.getUnidadeOrganizacional();
		if(unidadeOrganizacional != null) {
			criteria.add(Restrictions.eq("unidadeOrganizacional.chavePrimaria", unidadeOrganizacional.getChavePrimaria()));
		}
		Usuario usuario = chamadoVO.getUsuarioResponsavel();
		if(usuario != null) {
			criteria.add(Restrictions.eq("usuarioResponsavel.chavePrimaria", usuario.getChavePrimaria()));
		}
		DateTime dataFimNova;
		if(chamadoVO.getDataInicioCriacao() != null && chamadoVO.getDataFimCriacao() != null && 
						Util.isDataValida(chamadoVO.getDataInicioCriacao(), Constantes.FORMATO_DATA_BR)) {

			ConstanteSistema constanteSistema = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_OPERACAO_CHAMADO_INCLUIDO);
			EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
			criteria.add(Restrictions.eq("operacao.chavePrimaria", status.getChavePrimaria()));
			Date dataInicio = Util.lowDateTime(Util.converterCampoStringParaData("", chamadoVO.getDataInicioCriacao(),
							Constantes.FORMATO_DATA_BR));

			Date dataFim = Util.converterCampoStringParaData("", chamadoVO.getDataFimCriacao(), Constantes.FORMATO_DATA_BR);

			dataFimNova = new DateTime(dataFim);
			dataFimNova = dataFimNova.withHourOfDay(LIMITE_HORAS);
			dataFimNova = dataFimNova.withMinuteOfHour(LIMITE_MIN_SEG);
			dataFimNova = dataFimNova.withSecondOfMinute(LIMITE_MIN_SEG);
			criteria.add(Restrictions.between("chamadoHistorico.ultimaAlteracao", dataInicio, dataFimNova.toDate()));
		}

		try {
			if ((chamadoVO.getDataInicioAbertura() != null && !chamadoVO.getDataInicioAbertura().isEmpty())
					|| (chamadoVO.getDataFimAbertura() != null && !chamadoVO.getDataFimAbertura().isEmpty())) {

				DateTime dataInicio;
				DateTime dataFim;

				Date dataAberturaInicio = new Date(0);
				if (chamadoVO.getDataInicioAbertura() != null && !chamadoVO.getDataInicioAbertura().isEmpty()) {
					dataAberturaInicio = formatoData.parse(chamadoVO.getDataInicioAbertura());
				}

				Date dataAberturaFim = new Date();
				if (chamadoVO.getDataFimAbertura() != null && !chamadoVO.getDataFimAbertura().isEmpty()) {
					dataAberturaFim = formatoData.parse(chamadoVO.getDataFimAbertura());
				}

				dataInicio = Util.zerarHorario(new DateTime(dataAberturaInicio));
				dataFim = Util.ultimoHorario(new DateTime(dataAberturaFim));

				criteria.add(Restrictions.between("protocolo.ultimaAlteracao", dataInicio.toDate(), dataFim.toDate()));
			}

			if ((chamadoVO.getDataInicioResolucao() != null && !chamadoVO.getDataInicioResolucao().isEmpty())
					|| (chamadoVO.getDataFimResolucao() != null && !chamadoVO.getDataFimResolucao().isEmpty())) {

				DateTime dataInicio;
				DateTime dataFim;

				Date dataAberturaInicio = new Date(0);
				if (chamadoVO.getDataInicioResolucao() != null && !chamadoVO.getDataInicioResolucao().isEmpty()) {
					dataAberturaInicio = formatoData.parse(chamadoVO.getDataInicioResolucao());
				}

				Date dataAberturaFim = new Date();
				if (chamadoVO.getDataFimResolucao() != null && !chamadoVO.getDataFimResolucao().isEmpty()) {
					dataAberturaFim = formatoData.parse(chamadoVO.getDataFimResolucao());
				}

				dataInicio = Util.zerarHorario(new DateTime(dataAberturaInicio));
				dataFim = Util.ultimoHorario(new DateTime(dataAberturaFim));

				criteria.add(Restrictions.between("dataResolucao", dataInicio.toDate(), dataFim.toDate()));
			}

			if (chamadoVO.getDataInicioEncerramento() != null && !chamadoVO.getDataInicioEncerramento().isEmpty()
					|| chamadoVO.getDataFimEncerramento() != null && !chamadoVO.getDataFimEncerramento().isEmpty()) {

				DateTime dataInicio;
				DateTime dataFim;

				Date dataEncerramentoInicio = new Date(0);
				if (!StringUtils.isEmpty(chamadoVO.getDataInicioEncerramento())) {
					dataEncerramentoInicio = formatoData.parse(chamadoVO.getDataInicioEncerramento());
				}

				Date dataEncerramentoFim = new Date();
				if (!StringUtils.isEmpty(chamadoVO.getDataFimEncerramento())) {
					dataEncerramentoFim = formatoData.parse(chamadoVO.getDataFimEncerramento());
				}

				dataInicio = Util.zerarHorario(new DateTime(dataEncerramentoInicio));
				dataFim = Util.ultimoHorario(new DateTime(dataEncerramentoFim));

				criteria.add(Restrictions.between("ultimaAlteracao", dataInicio.toDate(), dataFim.toDate()));
				ConstanteSistema constanteSistema =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_FINALIZADO);
				EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));
				criteria.add(Restrictions.eq("status.chavePrimaria", status.getChavePrimaria()));
			} else {
				if (chamadoVO.getListaStatus() != null && !chamadoVO.getListaStatus().isEmpty()
						&& !chamadoVO.getListaStatus().contains("Todos")) {
					criteria.add(Restrictions.in("status.descricao", chamadoVO.getListaStatus()));
				}
			}
			
			Date dataInicioPeriodo = montarDataInicioPeriodo(chamadoVO);
			
			if(!StringUtils.isEmpty(chamadoVO.getPeriodoCriacao())) {
				dataFimPeriodo = montarDataFimPeriodoChamado(chamadoVO.getPeriodoCriacao());
				criteria.add(Restrictions.between("protocolo.ultimaAlteracao", dataInicioPeriodo, dataFimPeriodo));
			}
			
			if(!StringUtils.isEmpty(chamadoVO.getPeriodoPrevisaoEncerramento())) {
				dataFimPeriodo = montarDataFimPeriodoChamado(chamadoVO.getPeriodoPrevisaoEncerramento());
				criteria.add(Restrictions.between("dataPrevisaoEncerramento", dataInicioPeriodo, dataFimPeriodo));
			}
			
			if(!StringUtils.isEmpty(chamadoVO.getPeriodoResolucao())) {
				dataFimPeriodo = montarDataFimPeriodoChamado(chamadoVO.getPeriodoResolucao());
				criteria.add(Restrictions.between("dataResolucao", dataInicioPeriodo, dataFimPeriodo));

			}

			if (chamadoVO.getListaPrazo() != null && !chamadoVO.getListaPrazo().isEmpty()) {
				Junction restricaoPrazo = Restrictions.disjunction();

				Date hojeMin = formatoData.parse(formatoData.format(new Date()));
				Date hojeMax = new Date(hojeMin.getTime() + TimeUnit.DAYS.toMillis(1));
				Date hoje = new Date();

				if (chamadoVO.getListaPrazo().contains("ATRASADOS")) {
					restricaoPrazo.add(Restrictions.lt(DATA_PREVISAO_ENCERRAMENTO, hoje));
				}
				if (chamadoVO.getListaPrazo().contains("HOJE")) {
					restricaoPrazo.add(Restrictions.between(DATA_PREVISAO_ENCERRAMENTO, hoje, hojeMax));
				}
				if (chamadoVO.getListaPrazo().contains("FUTUROS")) {
					restricaoPrazo.add(Restrictions.gt(DATA_PREVISAO_ENCERRAMENTO, hojeMax));
				}
				criteria.add(restricaoPrazo);
			}
		} catch (ParseException e) {
			LOG.error(e);
			throw new GGASException(e);
		}

		if (chamadoVO.getSituacaoQuestionario() != null) {
			if (chamadoVO.getSituacaoQuestionario().equals(SituacaoQuestionario.NAO_POSSUI)) {
				criteria.add(Restrictions.isNull("chamadoAssunto.questionario"));
			} else {
				
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(QuestionarioPerguntaResposta.class, "respostas");
				detachedCriteria.createAlias("pergunta", "pergunta");
				detachedCriteria.createAlias("chamado.chamadoAssunto", "chamadoAss", Criteria.LEFT_JOIN);

				detachedCriteria.add(Property.forName("root.chavePrimaria").eqProperty("chamado.chavePrimaria")).add(Property
						.forName("chamadoAss.questionario").eqProperty("pergunta.questionario"))
						.setProjection(Projections.count("chavePrimaria"));
				
				criteria.add(Restrictions.isNotNull("chamadoAssunto.questionario"));

				if (chamadoVO.getSituacaoQuestionario().equals(SituacaoQuestionario.NAO_RESPONDIDO)) {
					criteria.add(Subqueries.eq(Long.valueOf(0), detachedCriteria));
				} else if (chamadoVO.getSituacaoQuestionario().equals(SituacaoQuestionario.RESPONDIDO)) {
					criteria.add(Subqueries.lt(Long.valueOf(0), detachedCriteria));
				}
			}
		}

		// IMOVEL

		if(!(chamadoVO.getIdsCondominioImovel() != null && !chamadoVO.getIdsCondominioImovel().isEmpty())) {
			String cepImovel = chamadoVO.getCepImovel();
			if(cepImovel != null && !StringUtils.isEmpty(cepImovel)) {
				criteria.createCriteria("imovel.quadraFace.endereco").createCriteria("cep").add(Restrictions.eq("cep", cepImovel));
			}
			String numeroImovel = chamadoVO.getNumeroImovel();
			if(numeroImovel != null && !StringUtils.isEmpty(numeroImovel)) {
				criteria.add(Restrictions.eq("imovel.numeroImovel", numeroImovel));
			}
			String descricaoComplemento = chamadoVO.getDescricaoComplemento();
			if(descricaoComplemento != null && !StringUtils.isEmpty(descricaoComplemento)) {
				criteria.add(Restrictions.eq("imovel.descricaoComplemento", Util.formatarTextoConsulta(descricaoComplemento)));
			}
			String nomeImovel = chamadoVO.getNomeImovel();
			if(nomeImovel != null && !StringUtils.isEmpty(nomeImovel)) {
				criteria.add(Restrictions.ilike("imovel.nome", Util.formatarTextoConsulta(nomeImovel)));
			}
			Long matriculaImovel = chamadoVO.getMatriculaImovel();
			if(matriculaImovel != null) {
				criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, matriculaImovel));
			}
			Boolean condominioImovel = chamadoVO.getCondominioImovel();
			if(condominioImovel != null) {
				criteria.add(Restrictions.eq("imovel.condominio", condominioImovel));
			}
		} else {
			Long matriculaImovel = chamadoVO.getMatriculaImovel();
			List<Long> idsImovelPai = chamadoVO.getIdsCondominioImovel();
			if(matriculaImovel != null) {
				Criterion rest1 = Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, matriculaImovel);
				Criterion rest2 = Restrictions.in("imovel.imovelCondominio.chavePrimaria", idsImovelPai);
				criteria.add(Restrictions.or(rest1, rest2));
			}
		} 
		
		// PONTO CONSUMO
		
		String cepPontoConsumoChamado = chamadoVO.getCepPontoConsumoChamado();
		if (StringUtils.isNotBlank(cepPontoConsumoChamado)) {
			criteria.add(Restrictions.eq("cepPontoConsumoChamado.cep", cepPontoConsumoChamado));
		}

		String siglaUnidadeFederacaoPontoConsumoChamado = chamadoVO.getSiglaUnidadeFederacaoPontoConsumoChamado();
		if (StringUtils.isNotBlank(siglaUnidadeFederacaoPontoConsumoChamado)) {
			criteria.add(Restrictions.ilike("cepPontoConsumoChamado.uf", siglaUnidadeFederacaoPontoConsumoChamado));
		}
				
		String nomeMunicipioPontoConsumoChamado = chamadoVO.getNomeMunicipioPontoConsumoChamado();
		if (StringUtils.isNotBlank(nomeMunicipioPontoConsumoChamado)) {
			criteria.add(Restrictions.ilike("cepPontoConsumoChamado.nomeMunicipio", nomeMunicipioPontoConsumoChamado));
		}

		String lougradoroPontoConsumoChamado = chamadoVO.getLougradoroPontoConsumoChamado();
		if (StringUtils.isNotBlank(lougradoroPontoConsumoChamado)) {
			criteria.add(
					Restrictions.ilike("cepPontoConsumoChamado.logradouro", Util.formatarTextoConsulta(lougradoroPontoConsumoChamado)));
		}

		String bairroPontoConsumoChamado = chamadoVO.getBairroPontoConsumoChamado();
		if (StringUtils.isNotBlank(bairroPontoConsumoChamado)) {
			criteria.add(Restrictions.ilike("cepPontoConsumoChamado.bairro", bairroPontoConsumoChamado));
		}

		Boolean indicadorAgenciaReguladora = chamadoVO.getIndicadorAgenciaReguladora();
		if(indicadorAgenciaReguladora != null) {
			criteria.add(Restrictions.eq("indicadorAgenciaReguladora", indicadorAgenciaReguladora));
		}

		Integer numeroContrato = chamadoVO.getNumeroContrato();
		if(numeroContrato != null && numeroContrato > 0) {
			criteria.add(Restrictions.like("contrato.numeroCompletoContrato", "%" + numeroContrato + "%"));
		}
		
		String codigoLegado = chamadoVO.getCodigoLegado();
		if(codigoLegado != null && !codigoLegado.isEmpty()) {
			criteria.add(Restrictions.like("pontoConsumo.codigoLegado", "%" + codigoLegado + "%"));
		}

		// CLIENTE
		Integer codigoTipoPessoa = chamadoVO.getCodigoTipoPessoa();
		if(codigoTipoPessoa != null) {
			criteria.add(Restrictions.eq("tipoCliente.tipoPessoa.codigo", codigoTipoPessoa));
		}
		String cpf = chamadoVO.getCpfCliente();
		if(cpf != null && !StringUtils.isEmpty(cpf)) {
			criteria.add(Restrictions.eq("cliente.cpf", Util.removerCaracteresEspeciais(cpf)));
		}
		String rg = chamadoVO.getRgCliente();
		if(rg != null && !StringUtils.isEmpty(rg)) {
			criteria.add(Restrictions.eq("cliente.rg", rg));
		}
		String nome = chamadoVO.getNomeCliente();
		if(nome != null && !StringUtils.isEmpty(nome)) {
			criteria.add(Restrictions.ilike("cliente.nome", Util.formatarTextoConsulta(nome)));
		}
		String passaporte = chamadoVO.getNumeroPassaporte();
		if(passaporte != null && !StringUtils.isEmpty(passaporte)) {
			criteria.add(Restrictions.eq("cliente.numeroPassaporte", passaporte));
		}
		String cnpj = chamadoVO.getCnpjCliente();
		if(cnpj != null && !StringUtils.isEmpty(cnpj)) {
			criteria.add(Restrictions.eq("cliente.cnpj", Util.removerCaracteresEspeciais(cnpj)));
		}
		String nomeFantasia = chamadoVO.getNomeFantasiaCliente();
		if(nomeFantasia != null && !StringUtils.isEmpty(nomeFantasia)) {
			criteria.add(Restrictions.ilike("cliente.nomeFantasia", Util.formatarTextoConsulta(nomeFantasia)));
		}
		
		Long idSegmentoCliente = chamadoVO.getIdSegmentoCliente();
		if (idSegmentoCliente != null && idSegmentoCliente != -1L) {
			criteria.add(Restrictions.eq("segmentoCliente.chavePrimaria", idSegmentoCliente));
		}
		
		Long idSegmentoImovel= chamadoVO.getIdSegmentoImovel();
		if (idSegmentoImovel != null && idSegmentoImovel != -1L) {
			criteria.add(Restrictions.eq("segmentoImovel.chavePrimaria", idSegmentoImovel));
		}

		Long idSegmentoChamado = chamadoVO.getIdSegmentoChamado();
		if (idSegmentoChamado != null && idSegmentoChamado != -1L) {
			if (!associacaoChamadoTipo) {
				criteria.createAlias(CHAMADO_ASSUNTO_CHAMADO_TIPO, ALIAS_CHAMADO_TIPO);
			}
			criteria.add(Restrictions.eq("chamadoTipo.segmento.chavePrimaria", idSegmentoChamado));
		}

		
		if(dataFimPeriodo != null) {
			criteria.addOrder(Order.asc("protocolo.ultimaAlteracao"));
		}
		criteria.addOrder(Order.desc(PROTOCOLO_NUMERO_PROTOCOLO));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return criteria.list();
	}

	private Boolean verificarExistenciaServicoTipo(String chavesChamadoTipo) {
		Boolean retorno = Boolean.FALSE;
		String[] ids = chavesChamadoTipo.split(",");
		
		for(String id : ids) {
			if(!id.equals("-1")) {
				retorno = Boolean.TRUE;
				break;
			}
		}
		
		return retorno;
	}

	private Date montarDataInicioPeriodo(ChamadoVO chamadoVO) {
		Integer anoReferencia = 0;
		
		if(!StringUtils.isEmpty(chamadoVO.getPeriodoCriacao())) {
			String[] anoMes = chamadoVO.getPeriodoCriacao().split("/");
			anoReferencia = Integer.valueOf(anoMes[1]);
		}
		
		if(!StringUtils.isEmpty(chamadoVO.getPeriodoPrevisaoEncerramento())) {
			String[] anoMes = chamadoVO.getPeriodoPrevisaoEncerramento().split("/");
			anoReferencia = Integer.valueOf(anoMes[1]);
		}
		
		if(!StringUtils.isEmpty(chamadoVO.getPeriodoResolucao())) {
			String[] anoMes = chamadoVO.getPeriodoResolucao().split("/");
			anoReferencia = Integer.valueOf(anoMes[1]);
		}

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.MONTH, 0);
		calendario.set(Calendar.DAY_OF_MONTH, 1);
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		
		if(anoReferencia != null) {
			calendario.set(Calendar.YEAR, anoReferencia);
		}
		
		Date dataInicioPeriodo = calendario.getTime();
		return dataInicioPeriodo;
	}

	private Date montarDataFimPeriodoChamado(String periodo) {
		Calendar calendario = Calendar.getInstance();
		String[] anoMes = periodo.split("/");
		
		
		calendario.set(Calendar.YEAR, Integer.valueOf(anoMes[1]));
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MONTH, Integer.valueOf(anoMes[0]) - 1);
		calendario.set(Calendar.DAY_OF_MONTH, Util.obterUltimoDiaDoMesParaData(new DateTime(calendario.getTime())).getDayOfMonth());
		
		
		calendario.add(Calendar.DAY_OF_YEAR, 1);
	
		return calendario.getTime();
	}

	private void adicionarRestricaoUnidadeOrganizacional(ChamadoVO chamadoVO, Criteria criteria, Boolean isRelatorio) {

		if ((chamadoVO.isUsuarioAdministrador() == null || !chamadoVO.isUsuarioAdministrador()) && !isRelatorio) {

			List<Long> idsUnidades = new ArrayList<>();
			Optional.ofNullable(chamadoVO.getIdUnidadeOrganizacionalUsuario()).ifPresent(idsUnidades::add);
			Optional.ofNullable(chamadoVO.getIdsUnidadesInferiores()).ifPresent(idsUnidades::addAll);

			final SimpleExpression ehUnidadeVisualizadora = Restrictions
					.eq("unidadeVisualizadora.unidadeOrganizacional.chavePrimaria", chamadoVO.getIdUnidadeOrganizacionalUsuario());
			if(CollectionUtils.isNotEmpty(idsUnidades)) {
				criteria.add(Restrictions.or(ehUnidadeVisualizadora, Restrictions.in("unidadeOrganizacional.chavePrimaria", idsUnidades)));
			} else {
				criteria.add(ehUnidadeVisualizadora);
			}
		}
	}

	/**
	 * Obter ultimo protocolo dia.
	 * 
	 * @return the chamado protocolo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public String obterUltimoProtocoloDia() {

		try {
			StringBuilder hql = new StringBuilder();

			hql.append("SELECT max(chamadoProtocolo.numeroProtocolo)");
			hql.append(FROM);
			hql.append(getClasseEntidadeChamadoProtocolo().getSimpleName());
			hql.append(" chamadoProtocolo ");
			hql.append(WHERE);
			hql.append(" to_char(chamadoProtocolo.ultimaAlteracao,'fmdd-mm-yyyy') = to_char(sysdate,'fmdd-mm-yyyy') ");
			hql.append(" and length(chamadoProtocolo.numeroProtocolo) >= 10 ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			return query.uniqueResult().toString();
		} catch (Exception e) {
			LOG.error("Falha ao obter ultimo protocolo do dia", e);
			return null;
		}

	}

	/**
	 * Consultar chamado por cliente imovel ponto consumo.
	 * 
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<Chamado> consultarChamadoPorClienteImovelPontoConsumo(Long chavePrimariaCliente, Long chavePrimariaImovel,
					Long chavePrimariaPontoConsumo) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(UNIDADE_ORGANIZACIONAL, FetchMode.JOIN);
		criteria.setFetchMode(CANAL_ATENDIMENTO, FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO_ASSUNTO, FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO_ASSUNTO_CHAMADO_TIPO, FetchMode.JOIN);
		criteria.setFetchMode(PROTOCOLO, FetchMode.JOIN);
		criteria.setFetchMode(STATUS, FetchMode.JOIN);
		criteria.createAlias(CLIENTE, CLIENTE, Criteria.LEFT_JOIN);

		criteria.createAlias(PROTOCOLO, PROTOCOLO);
		if(chavePrimariaCliente != null) {
			criteria.add(Restrictions.eq("cliente.chavePrimaria", chavePrimariaCliente));
		} else if(chavePrimariaImovel != null) {
			criteria.setFetchMode(IMOVEL, FetchMode.JOIN);
			criteria.createAlias(IMOVEL, IMOVEL);
			criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, chavePrimariaImovel));
		} else if(chavePrimariaPontoConsumo != null) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, chavePrimariaPontoConsumo));
			criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);
			criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
		}

		criteria.addOrder(Order.desc(PROTOCOLO_NUMERO_PROTOCOLO));
		return criteria.list();
	}

	/**
	 * Listar chamado historico.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param operacao
	 *            the operacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ChamadoHistorico> listarChamadoHistorico(Chamado chamado, EntidadeConteudo operacao) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeChamadoHistorico().getSimpleName());
		hql.append(" chamadoHistorico ");
		hql.append(" left join fetch chamadoHistorico.usuario usuario ");
		hql.append(" left join fetch chamadoHistorico.usuario.funcionario funcionario ");
		hql.append(" inner join fetch chamadoHistorico.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(" inner join fetch chamadoHistorico.chamado chamado ");
		hql.append(" inner join fetch chamadoHistorico.chamado.chamadoAssunto chamadoAssunto ");
		hql.append(" inner join fetch chamadoHistorico.status status ");
		hql.append(" left join fetch chamadoHistorico.motivo motivo ");
		hql.append(" inner join fetch chamadoHistorico.operacao operacao ");
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria = :chavePrimaria ");

		if(operacao == null) {
			hql.append(" and chamadoHistorico.operacao <> :operacao ");
		} else {
			hql.append(" and chamadoHistorico.operacao = :operacao ");
		}

		hql.append(" order by chamadoHistorico.ultimaAlteracao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chamado.getChavePrimaria());
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_OPERACAO_CHAMADO_ANEXO);
		EntidadeConteudo operacaoChamadoAnexo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		query.setParameter("operacao", operacaoChamadoAnexo);

		return query.list();
	}

	/**
	 * Obter protocolo por numero.
	 * 
	 * @param numeroProtocolo
	 *            the numero protocolo
	 * @return the chamado protocolo
	 */
	public ChamadoProtocolo obterProtocoloPorNumero(Long numeroProtocolo) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeChamadoProtocolo().getSimpleName());
		hql.append(" chamadoProtocolo ");
		hql.append(WHERE);
		hql.append(" chamadoProtocolo.numeroProtocolo = :numeroProtocolo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroProtocolo", numeroProtocolo);

		return (ChamadoProtocolo) query.uniqueResult();
	}

	/**
	 * Existe chamado questionario.
	 * 
	 * @param questionario
	 *            the questionario
	 * @return true, if successful
	 */
	public boolean existeChamadoQuestionario(Questionario questionario) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias("questionario", "questionario");
		criteria.add(Restrictions.eq("questionario.chavePrimaria", questionario.getChavePrimaria()));

		Collection<Questionario> lista = criteria.list();
		return lista != null && !lista.isEmpty();

	}

	/**
	 * Obter chamado alteracao vencimento.
	 * 
	 * @param idChamado
	 *            the id chamado
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the chamado alteracao vencimento
	 */
	public ChamadoAlteracaoVencimento obterChamadoAlteracaoVencimento(Long idChamado, Long idPontoConsumo) {

		Criteria criteria = createCriteria(getClasseEntidadeChamadoAlteracaoVencimento());

		criteria.setFetchMode("chamado", FetchMode.JOIN);
		criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);

		if(idChamado != null) {
			criteria.add(Restrictions.eq("chamado.chavePrimaria", idChamado));
		}
		if(idPontoConsumo != null) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
		}

		return (ChamadoAlteracaoVencimento) criteria.uniqueResult();
	}

	/**
	 * Verificar imovel condominio.
	 * 
	 * @param chamadoVO
	 *            the chamado vo
	 * @return the collection
	 */
	public Collection<Imovel> verificarImovelCondominio(ChamadoVO chamadoVO) {

		Criteria criteria = this.createCriteria(Imovel.class, IMOVEL);

		criteria.setFetchMode(IMOVEL, FetchMode.JOIN);
		criteria.setFetchMode(IMOVEL_QUADRA_FACE, FetchMode.JOIN);

		criteria.createAlias(IMOVEL_QUADRA_FACE, IMOVEL_QUADRA_FACE, Criteria.LEFT_JOIN);

		String cepImovel = chamadoVO.getCepImovel();
		if(cepImovel != null && !StringUtils.isEmpty(cepImovel)) {
			criteria.createCriteria("imovel.quadraFace.endereco").createCriteria("cep").add(Restrictions.eq("cep", cepImovel));
		}
		String numeroImovel = chamadoVO.getNumeroImovel();
		if(numeroImovel != null && !StringUtils.isEmpty(numeroImovel)) {
			criteria.add(Restrictions.eq("imovel.numeroImovel", numeroImovel));
		}
		String descricaoComplemento = chamadoVO.getDescricaoComplemento();
		if(descricaoComplemento != null && !StringUtils.isEmpty(descricaoComplemento)) {
			criteria.add(Restrictions.eq("imovel.descricaoComplemento", Util.formatarTextoConsulta(descricaoComplemento)));
		}
		String nomeImovel = chamadoVO.getNomeImovel();
		if(nomeImovel != null && !StringUtils.isEmpty(nomeImovel)) {
			criteria.add(Restrictions.ilike("imovel.nome", Util.formatarTextoConsulta(nomeImovel)));
		}
		Long matriculaImovel = chamadoVO.getMatriculaImovel();
		if(matriculaImovel != null) {
			criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, matriculaImovel));
		}
		Boolean condominioImovel = chamadoVO.getCondominioImovel();
		if(condominioImovel != null) {
			criteria.add(Restrictions.eq("imovel.condominio", condominioImovel));
		}

		return criteria.list();
	}

	/**
	 * Consultar chamado ponto consumo.
	 * 
	 * @param chamadoVO
	 *            the chamado vo
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException
	 *             the parse exception
	 */
	public Collection<Chamado> consultarChamadoPontoConsumo(ChamadoVO chamadoVO, Long pontoConsumo) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(CONTRATO, FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO_ASSUNTO, FetchMode.JOIN);
		criteria.setFetchMode(PROTOCOLO, FetchMode.JOIN);
		criteria.setFetchMode(STATUS, FetchMode.JOIN);
		criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);

		criteria.createAlias(CHAMADO_ASSUNTO, CHAMADO_ASSUNTO, Criteria.LEFT_JOIN);
		criteria.createAlias(CONTRATO, CONTRATO, Criteria.LEFT_JOIN);
		criteria.createAlias(PROTOCOLO, PROTOCOLO, Criteria.LEFT_JOIN);
		criteria.createAlias(STATUS, STATUS);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);

		Long protocolo = chamadoVO.getNumeroProtocolo();
		if(protocolo != null) {
			criteria.add(Restrictions.eq(PROTOCOLO_NUMERO_PROTOCOLO, protocolo));
		}
		if(chamadoVO.getChamadoAssuntos() != null && !chamadoVO.getChamadoAssuntos().isEmpty()) {
			criteria.add(Restrictions.in(CHAMADO_ASSUNTO, chamadoVO.getChamadoAssuntos()));
		} else {
			ChamadoAssunto chamadoAssunto = chamadoVO.getChamadoAssunto();
			if(chamadoAssunto != null && chamadoAssunto.getChavePrimaria() > 0) {
				criteria.add(Restrictions.eq("chamadoAssunto.chavePrimaria", chamadoAssunto.getChavePrimaria()));
			}
		}

		if(pontoConsumo != null && pontoConsumo > 0) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo));
		}

		EntidadeConteudo status = chamadoVO.getStatus();
		if(status != null) {
			criteria.add(Restrictions.eq("status.chavePrimaria", status.getChavePrimaria()));
		}

		Integer numeroContrato = chamadoVO.getNumeroContrato();
		if(numeroContrato != null && numeroContrato > 0) {
			criteria.add(Restrictions.like("contrato.numeroCompletoContrato", "%" + numeroContrato + "%"));
		}

		criteria.addOrder(Order.desc(PROTOCOLO_NUMERO_PROTOCOLO));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	/**
	 * Listar chamado email.
	 * 
	 * @param chamado
	 *            the chamado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ChamadoEmail> listarChamadoEmail(Chamado chamado) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeChamadoEmail().getSimpleName());
		hql.append(" chamadoEmail ");
		hql.append(" inner join fetch chamadoEmail.chamado chamado ");
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria = :chavePrimaria ");
		hql.append(" order by chamadoEmail.ultimaAlteracao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chamado.getChavePrimaria());

		return query.list();
	}

	/**
	 * Obter ultimo chamado alteracao vencimento.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the chamado alteracao vencimento
	 */
	public ChamadoAlteracaoVencimento obterUltimoChamadoAlteracaoVencimento(Long idPontoConsumo) {

		Criteria criteria = createCriteria(getClasseEntidadeChamadoAlteracaoVencimento());

		criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);

		if(idPontoConsumo != null) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
		}

		criteria.setMaxResults(1);
		criteria.addOrder(Order.desc("ultimaAlteracao"));

		return (ChamadoAlteracaoVencimento) criteria.uniqueResult();
	}
	
	/**
	 * Método responsável por validar dados de uma entidade.
	 * 
	 * @param entidadeNegocio - {@link - Chamado}
	 * @param acao - {@link - AcaoChamado}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	public void validarDadosEntidade(Chamado entidadeNegocio, AcaoChamado acao) throws NegocioException {

		Map<String, Object> erros = entidadeNegocio.validarDados(acao);
		if (erros != null && !erros.isEmpty()) {
			throw new NegocioException(erros);
		}

		posValidarDadosEntidade(entidadeNegocio);
	}

	/**
	 * Retorna chamado pelo numero protocolo
	 * 
	 * @param numeroProtocolo {@link Long}
	 * @return true
	 * @throws NegocioException {@link NegocioException}
	 */
	public Chamado obterChamadoPorProtocolo(Long numeroProtocolo) throws NegocioException {
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(PROTOCOLO, PROTOCOLO);
		criteria.add(Restrictions.eq(PROTOCOLO_NUMERO_PROTOCOLO, numeroProtocolo));
		try {
			Object result = criteria.uniqueResult();
			if (result != null) {
				return (Chamado) result;
			}
		} catch (HibernateException e) {
			LOG.error(e);
			throw new NegocioException("Mais de um chamado com o mesmo número de protocolo.");
		}

		return null;
	}
	
	
	public void incluirTituloAnaliseGSA(String titulo, String usuario, String motivo, Boolean isAtualizar) throws ConcorrenciaException, NegocioException{
		   
		StringBuilder hql = new StringBuilder();
		
		if(isAtualizar) {
			hql.append(" update jetdata.gsa_tituloemanalise@gsa set datasaida = trunc(SYSDATE) where seqtitulo = :titulo " );
		} else {
			hql.append(" insert into jetdata.gsa_tituloemanalise@gsa (seqtitulo,dataentrada,usuario,motivo) SELECT :titulo , sysdate,  :usuario , :motivo  ");     
			hql.append(" FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM jetdata.gsa_tituloemanalise@gsa WHERE seqtitulo = :titulo and datasaida is null) "); 
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		query.setParameter("titulo", titulo);
		
		if(!isAtualizar) {
			query.setParameter("usuario", usuario);
			query.setParameter("motivo", motivo);
		}
		
		query.executeUpdate();
	}
	
	/**
	 * Consultar chamado resumo.
	 * 
	 * @param chamadoVO
	 *            the chamado vo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException
	 *             the parse exception
	 */
	public Collection<ChamadoResumoVO> consultarChamadoResumo(ChamadoVO chamadoVO, Boolean isRelatorio, Boolean isGrafico) throws GGASException {
		
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT CHTI_DS AS tipoChamado, ");
		sql.append("CHAS_DS AS assuntoChamado, ");
		sql.append("ABERTO AS totalAberto, ");
		sql.append("EM_ANDAMENTO AS totalEmAndamento, ");
		sql.append("PENDENTE AS totalPendente, ");
		sql.append("FINALIZADO AS totalFinalizado, ");
		sql.append("(ABERTO + EM_ANDAMENTO + PENDENTE + FINALIZADO) AS total ");
		sql.append("FROM ( ");
		sql.append("  SELECT ct.CHTI_DS, ca.CHAS_DS, ec.ENCO_DS, c.CHAM_CD ");
		sql.append("  FROM CHAMADO c ");
		sql.append("  LEFT JOIN CHAMADO_ASSUNTO ca ON ca.CHAS_CD = c.CHAS_CD ");
		sql.append("  INNER JOIN CHAMADO_TIPO ct ON ca.CHTI_CD = ct.CHTI_CD "); // Corrigi a chave de junção
		sql.append("  INNER JOIN CHAMADO_PROTOCOLO cp ON cp.CHPR_CD = c.CHPR_CD ");
		sql.append("  INNER JOIN ENTIDADE_CONTEUDO ec ON ec.ENCO_CD = c.ENCO_CD_STATUS ");
		sql.append("  WHERE c.CHAM_IN_USO = 1 ");

		DateTime dataInicioCriacao = null;
		DateTime dataFimCriacao = null;
		DateTime dataInicioResolucao = null;
		DateTime dataFimResolucao = null;
		DateTime dataInicioEncerramento = null;
		DateTime dataFimEncerramento = null;

		try {

		    if ((chamadoVO.getDataInicioCriacao() != null && !chamadoVO.getDataInicioCriacao().isEmpty())
		            || (chamadoVO.getDataFimCriacao() != null && !chamadoVO.getDataFimCriacao().isEmpty())) {

		        Date dataAberturaInicio = new Date(0);
		        if (chamadoVO.getDataInicioCriacao() != null && !chamadoVO.getDataInicioCriacao().isEmpty()) {
		            dataAberturaInicio = formatoData.parse(chamadoVO.getDataInicioCriacao());
		        }

		        Date dataAberturaFim = new Date();
		        if (chamadoVO.getDataFimCriacao() != null && !chamadoVO.getDataFimCriacao().isEmpty()) {
		            dataAberturaFim = formatoData.parse(chamadoVO.getDataFimCriacao());
		        }

		        dataInicioCriacao = Util.zerarHorario(new DateTime(dataAberturaInicio));
		        dataFimCriacao = Util.ultimoHorario(new DateTime(dataAberturaFim));

		        sql.append(" AND cp.CHPR_TM_ULTIMA_ALTERACAO >= :dataInicioCriacao ");
		        sql.append(" AND cp.CHPR_TM_ULTIMA_ALTERACAO <= :dataFimCriacao ");
		    }

		    if ((chamadoVO.getDataInicioResolucao() != null && !chamadoVO.getDataInicioResolucao().isEmpty())
		            || (chamadoVO.getDataFimResolucao() != null && !chamadoVO.getDataFimResolucao().isEmpty())) {

		        Date dataAberturaInicio = new Date(0);
		        if (chamadoVO.getDataInicioResolucao() != null && !chamadoVO.getDataInicioResolucao().isEmpty()) {
		            dataAberturaInicio = formatoData.parse(chamadoVO.getDataInicioResolucao());
		        }

		        Date dataAberturaFim = new Date();
		        if (chamadoVO.getDataFimResolucao() != null && !chamadoVO.getDataFimResolucao().isEmpty()) {
		            dataAberturaFim = formatoData.parse(chamadoVO.getDataFimResolucao());
		        }

		        dataInicioResolucao = Util.zerarHorario(new DateTime(dataAberturaInicio));
		        dataFimResolucao = Util.ultimoHorario(new DateTime(dataAberturaFim));

		        sql.append(" AND cp.CHPR_TM_ULTIMA_ALTERACAO >= :dataInicioResolucao ");
		        sql.append(" AND cp.CHPR_TM_ULTIMA_ALTERACAO <= :dataFimResolucao ");
		    }

		    if (chamadoVO.getDataInicioEncerramento() != null && !chamadoVO.getDataInicioEncerramento().isEmpty()
		            || chamadoVO.getDataFimEncerramento() != null && !chamadoVO.getDataFimEncerramento().isEmpty()) {

		        Date dataEncerramentoInicio = new Date(0);
		        if (!StringUtils.isEmpty(chamadoVO.getDataInicioEncerramento())) {
		            dataEncerramentoInicio = formatoData.parse(chamadoVO.getDataInicioEncerramento());
		        }

		        Date dataEncerramentoFim = new Date();
		        if (!StringUtils.isEmpty(chamadoVO.getDataFimEncerramento())) {
		            dataEncerramentoFim = formatoData.parse(chamadoVO.getDataFimEncerramento());
		        }

		        dataInicioEncerramento = Util.zerarHorario(new DateTime(dataEncerramentoInicio));
		        dataFimEncerramento = Util.ultimoHorario(new DateTime(dataEncerramentoFim));

		        sql.append(" AND cp.CHPR_TM_ULTIMA_ALTERACAO >= :dataInicioEncerramento ");
		        sql.append(" AND cp.CHPR_TM_ULTIMA_ALTERACAO <= :dataFimEncerramento ");
		    }
		} catch (ParseException e) {
		    LOG.error(e);
		    throw new GGASException(e);
		}

		sql.append(") ");
		sql.append("PIVOT ( ");
		sql.append("  COUNT(CHAM_CD) ");
		sql.append("  FOR ENCO_DS IN ('ABERTO' AS ABERTO, 'EM ANDAMENTO' AS EM_ANDAMENTO, 'PENDENTE' AS PENDENTE, 'FINALIZADO' AS FINALIZADO) ");
		sql.append(") ");
		sql.append("ORDER BY total DESC ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
			.addScalar("tipoChamado",StringType.INSTANCE)
			.addScalar("assuntoChamado",StringType.INSTANCE)
			.addScalar("totalAberto",IntegerType.INSTANCE)
			.addScalar("totalEmAndamento",IntegerType.INSTANCE)
			.addScalar("totalPendente",IntegerType.INSTANCE)
			.addScalar("totalFinalizado",IntegerType.INSTANCE)
			.addScalar("total",IntegerType.INSTANCE)
			.setResultTransformer(Transformers.aliasToBean(ChamadoResumoVO.class));
			
			if(dataInicioCriacao != null && dataFimCriacao != null) {
				query.setParameter("dataInicioCriacao", dataInicioCriacao.toDate());
				query.setParameter("dataFimCriacao", dataFimCriacao.toDate());
			}
			if(dataInicioResolucao != null && dataFimResolucao != null) {
				query.setParameter("dataInicioResolucao", dataInicioResolucao.toDate());
				query.setParameter("dataFimResolucao", dataFimResolucao.toDate());
			}
			if(dataInicioEncerramento != null && dataFimEncerramento != null) {
				query.setParameter("dataInicioEncerramento", dataInicioEncerramento.toDate());
				query.setParameter("dataFimEncerramento", dataFimEncerramento.toDate());
			}

			if(isGrafico) {
				query.setMaxResults(5);
			}
			
			return query.list();


	}
}

