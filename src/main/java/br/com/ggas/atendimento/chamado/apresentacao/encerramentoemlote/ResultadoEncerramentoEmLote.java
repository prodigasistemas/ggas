/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa a resposta para o front end
 *
 *  @author italo.alan@logiquesistemas.com.br
 */
public class ResultadoEncerramentoEmLote implements Serializable {

	private List<String> erros = new ArrayList<>();
	private List<ChamadoResposta> chamadosSucesso = new ArrayList<>();

	private ResultadoEncerramentoEmLote() {
	}

	/**
	 * @return ResultadoEncerramentoEmLote
	 */
	public static ResultadoEncerramentoEmLote build() {
		return new ResultadoEncerramentoEmLote();
	}

	public List<String> getErros() {
		return erros;
	}

	public void setErros(List<String> erros) {
		this.erros = erros;
	}

	public List<ChamadoResposta> getChamadosSucesso() {
		return chamadosSucesso;
	}

	public void setChamadosSucesso(List<ChamadoResposta> chamadosSucesso) {
		this.chamadosSucesso = chamadosSucesso;
	}

	/**
	 * Adiciona um ChamadoResposta na lista de chamados que foram encerrados com sucesso
	 *
	 * @param chamado chave primária do Chamado
	 * @param status descrição do status do chamado
	 */
	public void adicionarChamadoSucesso(Long chamado, String status) {
		if (this.chamadosSucesso == null) {
			this.chamadosSucesso = new ArrayList<>();
		}
		this.chamadosSucesso.add(ChamadoResposta.build(chamado, status));
	}
}
