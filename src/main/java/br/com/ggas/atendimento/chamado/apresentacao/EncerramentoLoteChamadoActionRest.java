/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.chamado.apresentacao;

import br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote.DadosEncerramentoLoteDTO;
import br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote.DadosInvalidosEncerramentoLoteException;
import br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote.ResultadoEncerramentoEmLote;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericActionRest;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

/**
 * Action responsável por gerenciar o encerramento em lote de chamados
 *
 * @author italo.alan@logiquesistemas.com.br
 */
@SuppressWarnings("common-java:InsufficientCommentDensity")
@Controller
public class EncerramentoLoteChamadoActionRest extends GenericActionRest {

	private static final String FORMATO_DATA_HORA_PT_BR = "dd/MM/yyyy HH:mm";

	private static final Logger LOG = Logger.getLogger(EncerramentoLoteChamadoActionRest.class);

	private static final String TELA_EXIBIR_ENCERRAR_EM_LOTE_CHAMADO = "exibirEncerrarEmLoteChamado";
	private static final String CHAMADOS = "chamados";
	private static final String LISTA_MOTIVOS = "listaMotivos";

	@Autowired
	private ControladorChamado controladorChamado;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Exibe o formulário para preenchimento da descrição, motivo, data do encerramento e lista os chamados que serão encerrados
	 *
	 * @param chavesPrimariasLote as chaves primárias dos chamados que estão sendo encerrados
	 * @param request a requisição sendo realizada, utilizada para obter e inserir as chaves que serão encerradas na sessão.
	 * @return a classe do spring para exibição do usuário, exibe no jsp (exibirEncerrarEmLoteChamado.jsp)
	 */
	@RequestMapping("exibirEncerramentoEmLoteChamado")
	public ModelAndView exibirEncerramentoEmLoteChamado(@RequestParam("chavesPrimariasLote") List<Long> chavesPrimariasLote,
			HttpServletRequest request) {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_ENCERRAR_EM_LOTE_CHAMADO);

		try {
			List<Chamado> chamados = new ArrayList<>();

			List<Chamado> chamadosSessao = (List<Chamado>) request.getSession().getAttribute(CHAMADOS);
			if (chamadosSessao == null || chamadosSessao.isEmpty()) {
				for (Long chave : chavesPrimariasLote) {

					Chamado chamado = controladorChamado.obterChamado(chave);
					chamado.setPossuiASAberta(chamado.getListaServicoAutorizacao().stream()
							.anyMatch(servicoAutorizacao -> servicoAutorizacao.getDataEncerramento() == null));

					chamados.add(chamado);
				}
			} else {
				chamados = chamadosSessao;
			}

			ConstanteSistema constanteSistema =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_MOT_ENCERRAMENTO);
			model.addObject(LISTA_MOTIVOS, controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
			request.getSession().setAttribute(CHAMADOS, chamados);
		} catch (GGASException e) {
			LOG.error("Não foi possível obter chamado", e);
			model = new ModelAndView("forward:/pesquisarChamado");
			final String TELA_EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
			model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
			model.addObject("msg", obterMensagem(e.getChaveErro(), e.getParametrosErro()));
			return model;
		}

		return model;
	}

	private static String obterMensagem(String key, Object... params) {

		try {
			return MessageFormat.format(ResourceBundle.getBundle("mensagens").getString(key), params);
		} catch (MissingResourceException e) {
			LOG.error(e.getMessage(), e);
			return '!' + key + '!';
		}
	}

	/**
	 * Recebe uma requisição com as informações necessárias para encerrar os chamados em lote
	 *
	 * @param dadosEncerramentoLoteDTO classe com as informações genêricas para inserir nos chamados que serão encerrados
	 * @param request a requisição sendo realizada, utilizada para obter e inserir as chaves que serão encerradas na sessão.
	 * @return resultado com os chamados que foram encerrados e com os erros que ocorreram
	 */
	@RequestMapping(value = "encerramentoEmLoteChamado/encerrar",
			produces = "application/json; charset=utf-8", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ResultadoEncerramentoEmLote> encerramentoEmLoteChamado(
			@RequestBody DadosEncerramentoLoteDTO dadosEncerramentoLoteDTO, HttpServletRequest request) {

		List<String> erros = new ArrayList<>();
		ResultadoEncerramentoEmLote resultadoEncerramentoEmLote = ResultadoEncerramentoEmLote.build();
		List<Chamado> chamadosSessao = (List<Chamado>) request.getSession().getAttribute(CHAMADOS);

		try {
			this.validarDadosEncerramentoLote(erros, dadosEncerramentoLoteDTO);
			for (Chamado chamado : chamadosSessao) {
				chamado.setDataResolucao(this.getDataResolucao(dadosEncerramentoLoteDTO));
			}

			Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
			Map<String, Object> dados = new HashMap<>();
			dados.put("usuario", usuario);
			dados.put("idMotivo", dadosEncerramentoLoteDTO.getIdMotivo().toString());

			ResultadoEncerramentoEmLote resultado = controladorChamado.encerrarChamadoEmLote(chamadosSessao, dados);

			request.getSession().setAttribute(CHAMADOS, chamadosSessao);
			return new ResponseEntity<>(resultado, HttpStatus.OK);
		} catch (DadosInvalidosEncerramentoLoteException e) {
			LOG.info(e);
			resultadoEncerramentoEmLote.setErros(erros);
			return new ResponseEntity<>(resultadoEncerramentoEmLote, HttpStatus.BAD_REQUEST);
		}
	}

	private void validarDadosEncerramentoLote(List<String> resposta,
			DadosEncerramentoLoteDTO dadosEncerramentoLoteDTO) throws DadosInvalidosEncerramentoLoteException {
		if (dadosEncerramentoLoteDTO.getDescricao() == null || dadosEncerramentoLoteDTO.getDescricao().trim().isEmpty()) {
			resposta.add("Descrição é obrigatório");
		}
		if (dadosEncerramentoLoteDTO.getIdMotivo() == null || dadosEncerramentoLoteDTO.getIdMotivo() <= 0){
			resposta.add("Motivo é obrigatório");
		}

		try {
			Date formatado = new SimpleDateFormat(FORMATO_DATA_HORA_PT_BR).parse(dadosEncerramentoLoteDTO.getDataResolucao());

			if (!new SimpleDateFormat(FORMATO_DATA_HORA_PT_BR).format(formatado).equals(dadosEncerramentoLoteDTO.getDataResolucao())) {
				resposta.add("Data de Resolução é inválida");
			}
		} catch (Exception e) {
			LOG.info(e);
			resposta.add("Data de Resolução é obrigatória e deve ser informada no padrão dd/MM/yyyy HH:mm");
		}

		if (!resposta.isEmpty()) {
			throw new DadosInvalidosEncerramentoLoteException("Dados inválidos");
		}
	}

	private Date getDataResolucao(DadosEncerramentoLoteDTO dadosEncerramentoLoteDTO) {
		try {
			return new SimpleDateFormat(FORMATO_DATA_HORA_PT_BR).parse(dadosEncerramentoLoteDTO.getDataResolucao());
		} catch (ParseException e) {
			return null;
		}
	}
}
