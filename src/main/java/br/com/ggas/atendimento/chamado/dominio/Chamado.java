
package br.com.ggas.atendimento.chamado.dominio;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Transient;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
/**
 * Classe responsável pela representação de um chamado
 */
public class Chamado extends EntidadeNegocioImpl implements Cloneable {

	private static final int LIMITE_CAMPOS = 2;

	private static final int TAMANHO_CNPJ = 14;

	private static final int TAMANHO_CPF = 11;

	private static final long serialVersionUID = -8519624329576816063L;

	private ChamadoProtocolo protocolo;

	private String nomeSolicitante;

	private String cpfCnpjSolicitante;

	private String rgSolicitante;

	private String telefoneSolicitante;

	private String emailSolicitante;
	
	private TipoContato tipoContatoSolicitante;

	private ChamadoAssunto chamadoAssunto;

	private CanalAtendimento canalAtendimento;

	private String descricao;

	private String informacaoAdicional;

	private UnidadeOrganizacional unidadeOrganizacional;

	private Usuario usuarioResponsavel;

	private Imovel imovel;

	private Contrato contrato;

	private Cliente cliente;

	private PontoConsumo pontoConsumo;

	private Integer quantidadeReiteracao;

	private Integer quantidadeReabertura;

	private Integer quantidadeReativacao;

	private Integer quantidadeTentativaContato;

	private EntidadeConteudo status;

	private Collection<ChamadoHistorico> chamadoHistoricos = new HashSet<>();

	private Collection<QuestionarioPerguntaResposta> respostasQuestionario = new HashSet<>();

	private Questionario questionario;

	private Boolean indicadorAgenciaReguladora;

	private Boolean indicadorUnidadesOrganizacionalVisualizadora;

	private Boolean indicadorAcionamentoGasista;

	private Boolean indicadorAcionamentoPlantonista;

	private Boolean indicadorVazamentoConfirmado;

	private SituacaoQuestionario situacaoQuestionario;

	@Transient
	private ChamadoTipo chamadoTipo;

	@Transient
	private boolean indicadorPesquisa;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date dataPrevisaoEncerramento;

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date dataResolucao;

	@Transient
	private boolean chamadoEmLote;

	private Collection<ChamadoHistoricoAnexo> listaAnexos = new HashSet<>();

	private Collection<ChamadoEmail> chamadoEmail = new HashSet<>();

	private Collection<ChamadoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora = new HashSet<>();

	private EntidadeConteudo manifestacao;

	private Collection<ServicoAutorizacao> listaServicoAutorizacao = new HashSet<>();
	private boolean possuiASAberta = false;
	private boolean possuiASDuplicada = false;
	
	private Boolean indicadorPendencia;
	
	private String horaAcionamento;
	
	private Cep enderecoChamado;

	private Collection<ChamadoTituloAnalise> chamadoTituloAnalise = new HashSet<ChamadoTituloAnalise>();
	
	
	private String numeroEndereco;

	/**
	 * @return protocolo
	 */
	public ChamadoProtocolo getProtocolo() {

		return protocolo;
	}

	/**
	 * @param protocolo
	 */
	public void setProtocolo(ChamadoProtocolo protocolo) {

		this.protocolo = protocolo;
	}

	/**
	 * @return nomeSolicitante
	 */
	public String getNomeSolicitante() {

		return nomeSolicitante;
	}

	/**
	 * @param nomeSolicitante
	 */
	public void setNomeSolicitante(String nomeSolicitante) {

		this.nomeSolicitante = nomeSolicitante;
	}

	/**
	 * @return cpfCnpjSolicitante
	 */
	public String getCpfCnpjSolicitante() {

		return cpfCnpjSolicitante;
	}

	/**
	 * @param cpfCnpjSolicitante
	 */
	public void setCpfCnpjSolicitante(String cpfCnpjSolicitante) {

		this.cpfCnpjSolicitante = cpfCnpjSolicitante;
	}

	/**
	 * @return rgSolicitante
	 */
	public String getRgSolicitante() {

		return rgSolicitante;
	}

	/**
	 * @param rgSolicitante
	 */
	public void setRgSolicitante(String rgSolicitante) {

		this.rgSolicitante = rgSolicitante;
	}

	/**
	 * @return chamadoAssunto
	 */
	public ChamadoAssunto getChamadoAssunto() {

		return chamadoAssunto;
	}

	/**
	 * @param chamadoAssunto
	 */
	public void setChamadoAssunto(ChamadoAssunto chamadoAssunto) {

		this.chamadoAssunto = chamadoAssunto;
	}

	/**
	 * @return canalAtendimento
	 */
	public CanalAtendimento getCanalAtendimento() {

		return canalAtendimento;
	}

	/**
	 * @param canalAtendimento
	 */
	public void setCanalAtendimento(CanalAtendimento canalAtendimento) {

		this.canalAtendimento = canalAtendimento;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return unidadeOrganizacional
	 */
	public UnidadeOrganizacional getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	/**
	 * @param unidadeOrganizacional
	 */
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * @return usuarioResponsavel
	 */
	public Usuario getUsuarioResponsavel() {

		return usuarioResponsavel;
	}

	/**
	 * @param usuarioResponsavel
	 */
	public void setUsuarioResponsavel(Usuario usuarioResponsavel) {

		this.usuarioResponsavel = usuarioResponsavel;
	}

	/**
	 * @return imovel
	 */
	public Imovel getImovel() {

		return imovel;
	}

	/**
	 * @param imovel
	 */
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/**
	 * @return contrato
	 */
	public Contrato getContrato() {

		return contrato;
	}

	/**
	 * @param contrato
	 */
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/**
	 * @return cliente
	 */
	public Cliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 */
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return pontoConsumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return quantidadeReiteracao
	 */
	public Integer getQuantidadeReiteracao() {

		return quantidadeReiteracao;
	}

	/**
	 * @param quantidadeReiteracao
	 */
	public void setQuantidadeReiteracao(Integer quantidadeReiteracao) {

		this.quantidadeReiteracao = quantidadeReiteracao;
	}

	/**
	 * @return quantidadeReabertura
	 */
	public Integer getQuantidadeReabertura() {

		return quantidadeReabertura;
	}

	/**
	 * @param quantidadeReabertura
	 */
	public void setQuantidadeReabertura(Integer quantidadeReabertura) {

		this.quantidadeReabertura = quantidadeReabertura;
	}

	/**
	 * @return quantidadeReativacao
	 */
	public Integer getQuantidadeReativacao() {

		return quantidadeReativacao;
	}

	/**
	 * @param quantidadeReativacao
	 */
	public void setQuantidadeReativacao(Integer quantidadeReativacao) {

		this.quantidadeReativacao = quantidadeReativacao;
	}

	/**
	 * @return status
	 */
	public EntidadeConteudo getStatus() {

		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/**
	 * @return chamadoHistoricos
	 */
	public Collection<ChamadoHistorico> getChamadoHistoricos() {

		return chamadoHistoricos;
	}

	/**
	 * @param chamadoHistoricos
	 */
	public void setChamadoHistoricos(Collection<ChamadoHistorico> chamadoHistoricos) {

		this.chamadoHistoricos = chamadoHistoricos;
	}

	/**
	 * @return telefoneSolicitante
	 */
	public String getTelefoneSolicitante() {

		return telefoneSolicitante;
	}

	/**
	 * @param telefoneSolicitante
	 */
	public void setTelefoneSolicitante(String telefoneSolicitante) {

		this.telefoneSolicitante = telefoneSolicitante;
	}

	/**
	 * @return emailSolicitante
	 */
	public String getEmailSolicitante() {

		return emailSolicitante;
	}

	/**
	 * @param emailSolicitante
	 */
	public void setEmailSolicitante(String emailSolicitante) {

		this.emailSolicitante = emailSolicitante;
	}

	/**
	 * @return the tipoContatoSolicitante
	 */
	public TipoContato getTipoContatoSolicitante() {
		return tipoContatoSolicitante;
	}

	/**
	 * @param tipoContatoSolicitante the tipoContatoSolicitante to set
	 */
	public void setTipoContatoSolicitante(TipoContato tipoContatoSolicitante) {
		this.tipoContatoSolicitante = tipoContatoSolicitante;
	}

	/**
	 * @return cpfCnpjSolicitante
	 */
	public String getCpfCnpjSolicitanteFormatado() {

		if(this.cpfCnpjSolicitante != null && this.cpfCnpjSolicitante.length() == TAMANHO_CPF) {
			this.cpfCnpjSolicitante = Util.formatarValor(this.cpfCnpjSolicitante, Constantes.MASCARA_CPF);
		} else if(this.cpfCnpjSolicitante != null && this.cpfCnpjSolicitante.length() == TAMANHO_CNPJ) {
			this.cpfCnpjSolicitante = Util.formatarValor(this.cpfCnpjSolicitante, Constantes.MASCARA_CNPJ);
		}

		return cpfCnpjSolicitante;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.descricao == null || this.descricao.isEmpty() || descricao.trim().length() == 0) {
			stringBuilder.append(Constantes.CAMPO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.protocolo == null || this.protocolo.getNumeroProtocolo() == null) {
			stringBuilder.append(Constantes.CAMPO_PROTOCOLO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.chamadoAssunto == null) {
			stringBuilder.append(Constantes.CAMPO_CHAMADO_ASSUNTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.unidadeOrganizacional == null) {
			stringBuilder.append(Constantes.CAMPO_UNIDADE_ORGANIZACIONAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.canalAtendimento == null) {
			stringBuilder.append(Constantes.CAMPO_CANAL_ATENDIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.chamadoTipo == null) {
			stringBuilder.append(Constantes.CAMPO_CHAMADO_TIPO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(!this.chamadoEmLote && this.chamadoAssunto != null) {
			if (this.chamadoAssunto.getIndicadorNomeSolicitanteObrigatorio()
					&& (this.nomeSolicitante == null || this.nomeSolicitante.isEmpty())) {
				stringBuilder.append(Constantes.CHAMADO_NOME_SOLICITANTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.chamadoAssunto.getIndicadorCpfCnpjSolicitanteObrigatorio()
							&& (this.cpfCnpjSolicitante == null || this.cpfCnpjSolicitante.isEmpty())) {
				stringBuilder.append(Constantes.CHAMADO_CPFCNPJ_SOLICITANTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.chamadoAssunto.getIndicadorRgSolicitanteObrigatorio() && (this.rgSolicitante == null || this.rgSolicitante.isEmpty())) {
				stringBuilder.append(Constantes.CHAMADO_RG_SOLICITANTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if(this.telefoneSolicitante != null && this.chamadoAssunto.getIndicadorTelefoneSolicitanteObrigatorio() != null &&
							this.telefoneSolicitante.isEmpty() && this.chamadoAssunto.getIndicadorTelefoneSolicitanteObrigatorio()) {
				stringBuilder.append(Constantes.CHAMADO_TELEFONE_SOLICITANTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if(this.emailSolicitante != null && this.chamadoAssunto.getIndicadorEmailSolicitanteObrigatorio() != null &&
							this.emailSolicitante.isEmpty() && this.chamadoAssunto.getIndicadorEmailSolicitanteObrigatorio()) {
				stringBuilder.append(Constantes.CHAMADO_EMAIL_SOLICITANTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.chamadoAssunto.getIndicadorClienteObrigatorio() && this.cliente == null) {
				stringBuilder.append(Constantes.CHAMADO_CLIENTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.chamadoAssunto.getIndicadorImovelObrigatorio() && this.imovel == null) {
				stringBuilder.append(Constantes.CHAMADO_IMOVEL_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPOS));
		}

		return erros;
	}

	
	/**
	 * Validar dados 
	 * @param acao {@link - Acao}
	 * @return @Map {@link - Map}
	 */	
	public Map<String, Object> validarDados(AcaoChamado acao) {

		Map<String, Object> erros = validarDados();

		String camposObrigatorios = (String) erros.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS);

		StringBuilder stringBuilder = new StringBuilder();
		if (StringUtils.isNotBlank(camposObrigatorios)) {
			stringBuilder.append(camposObrigatorios)
			.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (AcaoChamado.ENCERRAR.equals(acao) && Util.isTrue(this.getChamadoAssunto().getIndicadorVerificarVazamento())
				&& this.getIndicadorVazamentoConfirmado() == null) {
			stringBuilder.append(Constantes.CHAMADO_VAZAMENTO_CONFIRMADO_OBRIGATORIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {

			erros.clear();
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPOS));
		}

		return erros;
	}

	/**
	 * @return questionario
	 */
	public Questionario getQuestionario() {

		return questionario;
	}

	/**
	 * @param questionario
	 */
	public void setQuestionario(Questionario questionario) {

		this.questionario = questionario;
	}

	/**
	 * @return indicadorPesquisa
	 */
	public boolean isIndicadorPesquisa() {

		return indicadorPesquisa;
	}

	/**
	 * @param indicadorPesquisa
	 */
	public void setIndicadorPesquisa(boolean indicadorPesquisa) {

		this.indicadorPesquisa = indicadorPesquisa;
	}

	/**
	 * @return indicadorAgenciaReguladora
	 */
	public Boolean getIndicadorAgenciaReguladora() {

		return indicadorAgenciaReguladora;
	}

	/**
	 * @param indicadorAgenciaReguladora
	 */
	public void setIndicadorAgenciaReguladora(Boolean indicadorAgenciaReguladora) {

		this.indicadorAgenciaReguladora = indicadorAgenciaReguladora;
	}

	/**
	 * @return quantidadeTentativaContato
	 */
	public Integer getQuantidadeTentativaContato() {

		return quantidadeTentativaContato;
	}

	/**
	 * @param quantidadeTentativaContato
	 */
	public void setQuantidadeTentativaContato(Integer quantidadeTentativaContato) {

		this.quantidadeTentativaContato = quantidadeTentativaContato;
	}

	/**
	 * @param chamadoTipo
	 */
	public void setChamadoTipo(ChamadoTipo chamadoTipo) {

		this.chamadoTipo = chamadoTipo;
	}

	/**
	 * @return chamadoTipo
	 */
	public ChamadoTipo getChamadoTipo() {

		return chamadoTipo;
	}

	/**
	 * @return dataPrevisaoEncerramento
	 */
	public Date getDataPrevisaoEncerramento() {

		return dataPrevisaoEncerramento;
	}

	/**
	 * @param dataPrevisaoEncerramento
	 */
	public void setDataPrevisaoEncerramento(Date dataPrevisaoEncerramento) {
		if(dataPrevisaoEncerramento != null){
			this.dataPrevisaoEncerramento = (Date) dataPrevisaoEncerramento.clone();
		} else {
			this.dataPrevisaoEncerramento = null;
		}
	}

	/**
	 * @return chamadoEmail
	 */
	public Collection<ChamadoEmail> getChamadoEmail() {

		return chamadoEmail;
	}

	/**
	 * @param chamadoEmail
	 */
	public void setChamadoEmail(Collection<ChamadoEmail> chamadoEmail) {

		this.chamadoEmail = chamadoEmail;
	}

	/**
	 * @return manifestacao
	 */
	public EntidadeConteudo getManifestacao() {

		return manifestacao;
	}

	/**
	 * @param manifestacao
	 */
	public void setManifestacao(EntidadeConteudo manifestacao) {

		this.manifestacao = manifestacao;
	}

	/**
	 * @return login ultimo usuario
	 */
	public String getLoginUltimoUsuario() {

		List<ChamadoHistorico> chamadosHistorico = new ArrayList<>(chamadoHistoricos);

		if (!Util.isNullOrEmpty(chamadosHistorico)) {
			Collections.sort(chamadosHistorico, new Comparator<ChamadoHistorico>() {

				@Override
				public int compare(ChamadoHistorico chamadoHistorico1, ChamadoHistorico chamadoHistorico2) {

					return chamadoHistorico2.getUltimaAlteracao().compareTo(chamadoHistorico1.getUltimaAlteracao());
				}
			});
			if (chamadosHistorico.get(0).getUsuario() != null) {
				return chamadosHistorico.get(0).getUsuario().getLogin();
			}
		}

		return "";
	}

	/**
	 * @return listaUnidadeOrganizacionalVisualizadora
	 */
	public Collection<ChamadoUnidadeOrganizacional> getListaUnidadeOrganizacionalVisualizadora() {
		return listaUnidadeOrganizacionalVisualizadora;
	}

	/**
	 * @param listaUnidadeOrganizacionalVisualizadora
	 */
	public void setListaUnidadeOrganizacionalVisualizadora(Collection<ChamadoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora) {
		this.listaUnidadeOrganizacionalVisualizadora = listaUnidadeOrganizacionalVisualizadora;
	}

	/**
	 * @return indicadorUnidadesOrganizacionalVisualizadora
	 */
	public Boolean getIndicadorUnidadesOrganizacionalVisualizadora() {
		return indicadorUnidadesOrganizacionalVisualizadora;
	}

	/**
	 * @param indicadorUnidadesOrganizacionalVisualizadora
	 */
	public void setIndicadorUnidadesOrganizacionalVisualizadora(Boolean indicadorUnidadesOrganizacionalVisualizadora) {
		this.indicadorUnidadesOrganizacionalVisualizadora = indicadorUnidadesOrganizacionalVisualizadora;
	}

	/**
	 * Adiciona um chamadoUnidadeOrganizacional em uma lista chamadoUnidadeOrganizacional
	 * @param chamadoUnidadeOrganizacional - {@link ChamadoUnidadeOrganizacional}
	 */
	public void adicionarChamadoUnidadeOrganizacional(ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional) {
		this.listaUnidadeOrganizacionalVisualizadora.add(chamadoUnidadeOrganizacional);
	}

	/**
	 * Remove um chamadoUnidadeOrganizacional de uma lista chamadoUnidadeOrganizacional
	 * @param chamadoUnidadeOrganizacional - {@link ChamadoUnidadeOrganizacional}
	 */
	public void removerChamadoUnidadeOrganizacional(ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional) {
		this.listaUnidadeOrganizacionalVisualizadora.remove(chamadoUnidadeOrganizacional);
	}

	/**
	 * @return dataResolucao
	 */
	public Date getDataResolucao() {
		if (dataResolucao != null) {
			return new Date(dataResolucao.getTime());
		}
		return null;
	}

	/**
	 * @param dataResolucao
	 */
	public void setDataResolucao(Date dataResolucao) {
		if (dataResolucao != null) {
			this.dataResolucao = new Date(dataResolucao.getTime());
		} else {
			this.dataResolucao = null;
		}
	}

	/**
	 * @return imagem prazo
	 * @throws ParseException
	 */
	public String getImagemPrazo() throws ParseException {
		String caminhoImagem = "/imagens/circle_green.png";

		if (dataPrevisaoEncerramento != null) {

			DateTime hoje = new DateTime();

			SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
			Date hojeMin = formatoData.parse(formatoData.format(new Date()));
			DateTime hojeMax = new DateTime(hojeMin.getTime() + TimeUnit.DAYS.toMillis(1));

			DateTime previsaoEncerramento = new DateTime(dataPrevisaoEncerramento);

			if (previsaoEncerramento.isBefore(hoje)) {
				caminhoImagem = "/imagens/circle_red.png";
			} else if (previsaoEncerramento.isBefore(hojeMax)) {
				caminhoImagem = "/imagens/circle_yellow.png";
			}
		}

		if (("FINALIZADO").equals(this.getStatus().getDescricao())) {
			caminhoImagem = "/imagens/circle_dark_gray.png";
		}

		return caminhoImagem;
	}

	/**
	 * @return conteudo email
	 */
	public String getCondeudoEmail() {
		
		DateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		StringBuilder email = new StringBuilder();

		email.append("<html>");
		email.append("<p><strong>Número protocolo:</strong> ").append(this.getProtocolo().getNumeroProtocolo()).append("</p>");
		email.append("<p><strong>Data de Abertura:</strong> ").append(formato.format(this.getProtocolo().getUltimaAlteracao()))
				.append("</p>");
		email.append("<p><strong>Data previsão encerramento:</strong> ").append(formato.format(this.getDataPrevisaoEncerramento()))
				.append("</p>");
		email.append("<p><strong>Unidade Organizacional responsável:</strong> ").append(this.getUnidadeOrganizacional().getDescricao())
				.append("</p>");
		if (this.getUsuarioResponsavel() != null) {
			email.append("<p><strong>Responsável pelo atendimento:</strong> ")
					.append(this.getUsuarioResponsavel().getFuncionario().getNome()).append("</p>");
		}
		email.append("<p><strong>Status:</strong> ").append(this.getStatus().getDescricao()).append("</p>");
		email.append("<p><strong>Tipo - Assunto:</strong> ").append(this.getChamadoTipo().getDescricao()).append(" - ")
				.append(this.getChamadoAssunto().getDescricao()).append("</p>");
		email.append("<p><strong>Canal de Atendimento:</strong> ").append(this.getCanalAtendimento().getDescricao()).append("</p>");
		
		if (this.getCliente() != null) {
			email.append("<p><strong>Nome cliente:</strong> ").append(this.getCliente().getNome()).append("</p>");
		}
		if (this.getPontoConsumo() != null) {
			email.append("<p><strong>Descrição Ponto de Consumo:</strong> ").append(this.getPontoConsumo().getDescricao()).append("</p>");
		}

		email.append("<p><strong>Descrição:</strong> ").append(this.getDescricao()).append("</p>");
		email.append("<hr><h4>Dados do Solicitante</h4>");
		email.append("<p><strong>Nome:</strong> ");
		if (this.getNomeSolicitante() != null) {
			email.append(this.getNomeSolicitante());
		}
		email.append("</p>");
		email.append("<p><strong>Telefone:</strong> ");
		if (this.getTelefoneSolicitante() != null) {
			email.append(this.getTelefoneSolicitante());
		}
		email.append("</p>");
		email.append("<p><strong>Email:</strong> ");
		if (this.getEmailSolicitante() != null) {
			email.append(this.getEmailSolicitante());
		}
		email.append("</p>");
		email.append("</html>");
		return email.toString();
	}

	/**
	 * @return indicadorAcionamentoGasista
	 */
	public Boolean getIndicadorAcionamentoGasista() {
		return indicadorAcionamentoGasista;
	}

	/**
	 * @param indicadorAcionamentoGasista
	 */
	public void setIndicadorAcionamentoGasista(Boolean indicadorAcionamentoGasista) {
		this.indicadorAcionamentoGasista = indicadorAcionamentoGasista;
	}

	/**
	 * @return indicadorAcionamentoPlantonista
	 */
	public Boolean getIndicadorAcionamentoPlantonista() {
		return indicadorAcionamentoPlantonista;
	}

	/**
	 * @param indicadorAcionamentoPlantonista
	 */
	public void setIndicadorAcionamentoPlantonista(Boolean indicadorAcionamentoPlantonista) {
		this.indicadorAcionamentoPlantonista = indicadorAcionamentoPlantonista;
	}

	/**
	 * @return informacaoAdicional
	 */
	public String getInformacaoAdicional() {
		return informacaoAdicional;
	}

	/**
	 * @param informacaoAdicional
	 */
	public void setInformacaoAdicional(String informacaoAdicional) {
		this.informacaoAdicional = informacaoAdicional;
	}

	@Override
	public Chamado clone() throws CloneNotSupportedException {

		return (Chamado) super.clone();
	}

	/**
	 * @return the indicadorVazamentoConfirmado
	 */
	public Boolean getIndicadorVazamentoConfirmado() {
		return indicadorVazamentoConfirmado;
	}

	/**
	 * @param indicadorVazamentoConfirmado the indicadorVazamentoConfirmado to set
	 */
	public void setIndicadorVazamentoConfirmado(Boolean indicadorVazamentoConfirmado) {
		this.indicadorVazamentoConfirmado = indicadorVazamentoConfirmado;
	}

	/**
	 * @return situacaoQuestionario
	 */
	public SituacaoQuestionario getSituacaoQuestionario() {
		return situacaoQuestionario;
	}

	/**
	 * @param situacaoQuestionario
	 */
	public void setSituacaoQuestionario(SituacaoQuestionario situacaoQuestionario) {
		this.situacaoQuestionario = situacaoQuestionario;
	}

	/**
	 * @return respostasQuestionario
	 */
	public Collection<QuestionarioPerguntaResposta> getRespostasQuestionario() {
		return respostasQuestionario;
	}

	/**
	 * @param respostasQuestionario
	 */
	public void setRespostasQuestionario(Collection<QuestionarioPerguntaResposta> respostasQuestionario) {
		this.respostasQuestionario = respostasQuestionario;
	}

	/**
	 * @return listaServicoAutorizacao
	 */
	public Collection<ServicoAutorizacao> getListaServicoAutorizacao() {
		return listaServicoAutorizacao;
	}

	/**
	 * @param listaServicoAutorizacao
	 */
	public void setListaServicoAutorizacao(Collection<ServicoAutorizacao> listaServicoAutorizacao) {
		this.listaServicoAutorizacao = listaServicoAutorizacao;
	}

	/**
	 * @return the listaAnexos
	 */
	public Collection<ChamadoHistoricoAnexo> getListaAnexos() {
		return listaAnexos;
	}

	/**
	 * @param listaAnexos the listaAnexos to set
	 */
	public void setListaAnexos(Collection<ChamadoHistoricoAnexo> listaAnexos) {
		this.listaAnexos = listaAnexos;
	}

	/**
	 * Substitui a lista de unidadeOrganizacionalVisualizadora por outra recebida por parâmetro
	 * @param listaUnidadeOrganizacionalVisualizadora - Collection de Chamado Unidade Organizacional
	 */
	public void substituirListaUnidadeOrganizacionalVisualizadora(
			Collection<ChamadoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora) {

		this.listaUnidadeOrganizacionalVisualizadora.clear();
		for (ChamadoUnidadeOrganizacional chamadoUnidadeVisualizadora : listaUnidadeOrganizacionalVisualizadora) {
			ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional = new ChamadoUnidadeOrganizacional();
			chamadoUnidadeOrganizacional.setChamado(this);
			chamadoUnidadeOrganizacional.setUnidadeOrganizacional(chamadoUnidadeVisualizadora.getUnidadeOrganizacional());
			this.adicionarChamadoUnidadeOrganizacional(chamadoUnidadeOrganizacional);
		}
	}

	public boolean isChamadoEmLote() {
		return chamadoEmLote;
	}

	public void setChamadoEmLote(Boolean chamadoEmLote) {
		this.chamadoEmLote = chamadoEmLote;
	}
	public boolean isPossuiASAberta() {
		return possuiASAberta;
	}

	public void setPossuiASAberta(boolean possuiASAberta) {
		this.possuiASAberta = possuiASAberta;
	}

	/**
	 * @return the possuiASDuplicada
	 */
	public boolean isPossuiASDuplicada() {
		return possuiASDuplicada;
	}

	/**
	 * @param possuiASDuplicada the possuiASDuplicada to set
	 */
	public void setPossuiASDuplicada(boolean possuiASDuplicada) {
		this.possuiASDuplicada = possuiASDuplicada;
	}

	public Boolean getIndicadorPendencia() {
		return indicadorPendencia;
	}

	public void setIndicadorPendencia(Boolean indicadorPendencia) {
		this.indicadorPendencia = indicadorPendencia;
	}
	
	
	/**
	 * @return dataAcionamento
	 */
	public String getHoraAcionamento() {
		return horaAcionamento;
	}

	/**
	 * @param dataAcionamento
	 */
	public void setHoraAcionamento(String horaAcionamento) {
		this.horaAcionamento = horaAcionamento;
	}

	public Cep getEnderecoChamado() {
		return enderecoChamado;
	}

	public void setEnderecoChamado(Cep enderecoChamado) {
		this.enderecoChamado = enderecoChamado;
	}

	public String getNumeroEndereco() {
		return numeroEndereco;
	}

	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}
	
	public Collection<ChamadoTituloAnalise> getChamadoTituloAnalise() {
		return chamadoTituloAnalise;
	}

	public void setChamadoTituloAnalise(Collection<ChamadoTituloAnalise> chamadoTituloAnalise) {
		this.chamadoTituloAnalise = chamadoTituloAnalise;
	}
}
