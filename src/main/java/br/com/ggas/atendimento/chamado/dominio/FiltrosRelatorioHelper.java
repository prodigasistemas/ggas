/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.chamado.dominio;

/**
 * Classe auxiliar para gerar relatorio de chamado
 *
 */
public class FiltrosRelatorioHelper {

	private String numeroProtocolo;
	private String assuntoChamado;
	private String tipoServico;
	private String cliente;
	private String segmento;
	private String categoria;
	private String questionario;
	private String categoriasSegmentos;
	private String periodoResolucao;
	private String unidadeOrganizacional;
	private String responsavel;
	private String status;
	private String prazo;
	private String canalAtendimento;

	/**
	 * Retorna numeroProtocolo
	 * 
	 * @return the numeroProtocolo
	 */
	public String getNumeroProtocolo() {
		return numeroProtocolo;
	}

	/**
	 * Atribui numeroProtocolo
	 * 
	 * @param numeroProtocolo the numeroProtocolo to set
	 */
	public void setNumeroProtocolo(String numeroProtocolo) {
		this.numeroProtocolo = numeroProtocolo;
	}

	/**
	 * Retorna assuntoChamado
	 * 
	 * @return the assuntoChamado
	 */
	public String getAssuntoChamado() {
		return assuntoChamado;
	}

	/**
	 * Atribui assuntoChamado
	 * 
	 * @param assuntoChamado the assuntoChamado to set
	 */
	public void setAssuntoChamado(String assuntoChamado) {
		this.assuntoChamado = assuntoChamado;
	}

	/**
	 * Retorna tipoServico
	 * 
	 * @return the tipoServico
	 */
	public String getTipoServico() {
		return tipoServico;
	}

	/**
	 * Atribui tipoServico
	 * 
	 * @param tipoServico the tipoServico to set
	 */
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Retorna cliente
	 * 
	 * @return the cliente
	 */
	public String getCliente() {
		return cliente;
	}

	/**
	 * Atribui cliente
	 * 
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	/**
	 * Retorna segmento
	 * 
	 * @return the segmento
	 */
	public String getSegmento() {
		return segmento;
	}

	/**
	 * Atribui segmento
	 * 
	 * @param segmento the segmento to set
	 */
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	/**
	 * Retorna periodoResolucao
	 * 
	 * @return the periodoResolucao
	 */
	public String getPeriodoResolucao() {
		return periodoResolucao;
	}

	/**
	 * Atribui periodoResolucao
	 * 
	 * @param periodoResolucao the periodoResolucao to set
	 */
	public void setPeriodoResolucao(String periodoResolucao) {
		this.periodoResolucao = periodoResolucao;
	}

	/**
	 * Retorna unidadeOrganizacional
	 * 
	 * @return the unidadeOrganizacional
	 */
	public String getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	/**
	 * Atribui unidadeOrganizacional
	 * 
	 * @param unidadeOrganizacional the unidadeOrganizacional to set
	 */
	public void setUnidadeOrganizacional(String unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * Retorna responsavel
	 * 
	 * @return the responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}

	/**
	 * Atribui responsavel
	 * 
	 * @param responsavel the responsavel to set
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	/**
	 * Retorna status
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Atribui status
	 * 
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Retorna prazo
	 * 
	 * @return the prazo
	 */
	public String getPrazo() {
		return prazo;
	}

	/**
	 * Atribui prazo
	 * 
	 * @param prazo the prazo to set
	 */
	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}

	/**
	 * Retorna canalAtendimento
	 * 
	 * @return the canalAtendimento
	 */
	public String getCanalAtendimento() {
		return canalAtendimento;
	}

	/**
	 * Atribui canalAtendimento
	 * 
	 * @param canalAtendimento the canalAtendimento to set
	 */
	public void setCanalAtendimento(String canalAtendimento) {
		this.canalAtendimento = canalAtendimento;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getQuestionario() {
		return questionario;
	}

	public void setQuestionario(String questionario) {
		this.questionario = questionario;
	}

	public String getCategoriasSegmentos() {
		return categoriasSegmentos;
	}

	public void setCategoriasSegmentos(String categoriasSegmentos) {
		this.categoriasSegmentos = categoriasSegmentos;
	}
}
