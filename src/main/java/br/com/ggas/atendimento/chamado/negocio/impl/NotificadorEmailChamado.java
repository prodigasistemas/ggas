/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.chamado.negocio.impl;

import br.com.ggas.atendimento.chamado.dominio.AcaoChamado;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.dominio.OperacaoEnvioEmail;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FreemarkerUtils;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Sets.newHashSet;

/**
 * Armazena a lógica de notificação via email ao manipular um {@link Chamado}
 * Envia um email dependendo da execução realizada no chamado informado
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Component
public class NotificadorEmailChamado {

	/**
	 * LOG do notificador de chamado
	 */
	private static final Logger LOG = Logger.getLogger(NotificadorEmailChamado.class);
	private static final String FORMATO_DATA_HORA = "dd/MM/yyyy HH:mm";

	/**
	 * Injeta o controlador de acesso aos
	 * parâmetros do sistema
	 */
	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema parametroSistema;

	/**
	 * Envia um email de alteração ou criação de chamado.
	 * As operações e para quem é enviado o email é descrito no chamado assunto
	 * Os email são enviados de acordo com a sua configuração em chamado assunto
	 *
	 * @param acao ação que foi executada no chamado
	 * @param chamado chamado que foi manipulado

	 * @throws GGASException exceção lançada quando ocorre alguma falha durante o envio de email
	 */
	public void enviarEmailCasoNecessario(AcaoChamado acao, Chamado chamado) throws GGASException {

		if (validarEnvioEmail(acao, chamado.getChamadoAssunto())) {
			JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

			ParametroSistema emailRemetentePadrao = parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
			String assunto = construirAssuntoEmail(acao, chamado);

			final Set<String> destinatarios = gerarDestinatarios(acao, chamado);
			if (CollectionUtils.isNotEmpty(destinatarios)) {
				for (String destinatario : destinatarios) {
					mailUtil.enviar(emailRemetentePadrao.getValor(), destinatario, assunto, chamado.getCondeudoEmail(), true, true);
				}
			}

		}
	}

	/**
	 * Envia email para a unidade organizacional
	 * @param listaUnidadeOrganizacional A lista de unidades
	 * @param chamado O cchamado
	 * @param acao A acao
	 * @throws GGASException O GGASException
	 */
	public void enviarEmailUnidadeOrganizacionalVisualizador(Collection<UnidadeOrganizacional> listaUnidadeOrganizacional,Chamado chamado,
			AcaoChamado acao ) throws GGASException {

		if (validarEnvioEmail(acao, chamado.getChamadoAssunto())) {
			JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

			ParametroSistema emailRemetentePadrao = parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
			String assunto = construirAssuntoEmail(acao, chamado);

			final Set<String> destinatarios = gerarDestinatariosunidadeVisualizadora(listaUnidadeOrganizacional);
			if (CollectionUtils.isNotEmpty(destinatarios)) {
				for (String destinatario : destinatarios) {
					mailUtil.enviar(emailRemetentePadrao.getValor(), destinatario, assunto, chamado.getCondeudoEmail(), true, true);
				}
			}

		}
	}

	/**
	 * Gera a lista de destinatarios para onde o email irá ser enviado
	 * Avalia as 3 condições para envio de email:
	 * - Unidade Responsável
	 * - Unidade Superior
	 * - Unidade Visualziadora
	 *
	 * @param listaUnidadeOrganizacional chamado manipulado
	 * @return retorna o conjunto de emails destinatarios
	 */
	private Set<String> gerarDestinatariosunidadeVisualizadora(Collection<UnidadeOrganizacional> listaUnidadeOrganizacional) {
		Set<String> destinatarios = new HashSet<>();
		int contador = 0;
		while (listaUnidadeOrganizacional.iterator().hasNext()){
			UnidadeOrganizacional unidadeOrganizacional = listaUnidadeOrganizacional.iterator().next();

			if((!destinatarios.contains(unidadeOrganizacional.getEmailContato()) && unidadeOrganizacional.getEmailContato() != null)
					&& unidadeOrganizacional.isIndicadorEmail()){
				destinatarios.add(unidadeOrganizacional.getEmailContato());
			}

			if(unidadeOrganizacional.getUnidadeSuperior() != null &&
					(!destinatarios.contains(unidadeOrganizacional.getUnidadeSuperior().getEmailContato()) &&
					unidadeOrganizacional.getUnidadeSuperior().getEmailContato() != null)
					&& unidadeOrganizacional.getUnidadeSuperior().isIndicadorEmail()){
				destinatarios.add(unidadeOrganizacional.getUnidadeSuperior().getEmailContato());
			}

			contador += 1;
			if(contador == listaUnidadeOrganizacional.size()){
				break;
			}
		}

		return destinatarios;
	}

	/**
	 * Envia email de encarramento de chamados em lote
	 * @param chamados lista de chamados que foram encerrados
	 */
	public void enviarEmailEncerramentoLoteCasoNecessario(List<Chamado> chamados) {
		AcaoChamado acao = AcaoChamado.ENCERRAR;

		HashMap<ChamadoAssunto, List<Chamado>> chamadosAgrupados = new HashMap<>();
		chamados.forEach(chamado -> {
			if (!chamadosAgrupados.containsKey(chamado.getChamadoAssunto())) {
				List<Chamado> listaChamado = new ArrayList<>();
				listaChamado.add(chamado);
				chamadosAgrupados.put(chamado.getChamadoAssunto(), listaChamado);
			} else {
				chamadosAgrupados.get(chamado.getChamadoAssunto()).add(chamado);
			}
		});

		chamadosAgrupados.forEach((key, value) -> {
			try {
				if (validarEnvioEmail(acao, key)) {
					JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

					ParametroSistema emailRemetentePadrao =
							parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);

					String assunto = acao.getDescricao().concat(" - Chamados em lote - ").concat(key.getDescricao());

					final Set<String> destinatarios = new HashSet<>();
					value.forEach(chamado -> {
						destinatarios.addAll(gerarDestinatarios(acao, chamado));
					});

					if (CollectionUtils.isNotEmpty(destinatarios)) {

						final ImmutableMap<String, Object> parametros = ImmutableMap.<String, Object>builder()
								.put("chamados", chamados)
								.build();

						final String conteudoEmail = Util.isoToUtf8(FreemarkerUtils.processarTemplate("emailEncerramentoChamadosEmLote.ftl",
								parametros));

						enviarEmailAosDestinatarios(mailUtil, emailRemetentePadrao, assunto, destinatarios, conteudoEmail);
					}
				}
			} catch (GGASException e) {
				LOG.error("Erro ao percorrer chamados agrupados para envio de notificação em lote", e);
			}
		});
	}

	/**
	 * Método responsável por enviar Email para o conjunto de destinatários
	 * @param mailUtil O mailUtil
	 * @param emailRemetentePadrao O remetente padrão
	 * @param assunto O assunto
	 * @param destinatarios O destinatário
	 * @param conteudoEmail O conteúdo
	 * @throws GGASException O GGASExcption
	 */
	public void enviarEmailAosDestinatarios(JavaMailUtil mailUtil, ParametroSistema emailRemetentePadrao, String assunto,
			Set<String> destinatarios, String conteudoEmail) throws GGASException {
		for (String destinatario : destinatarios) {
			mailUtil.enviar(emailRemetentePadrao.getValor(), destinatario, assunto, conteudoEmail, true, true);
		}
	}

	/**
	 *
	 * Envia email de geração de chamados em lote, caso o chamado assunto informado permita para a ação informada
	 * @param acao operação que está sendo executada no chamado
	 * @param chamadoOriginal chamado original
	 * @param chamados lista de chamados que foram criados em lote
	 * @throws GGASException é lançado se algum erro ocorrer no envio de e-mail
	 */
	public void enviarEmailEmLoteCasoNecessario(AcaoChamado acao, Chamado chamadoOriginal, List<Chamado> chamados) throws GGASException {

		if (validarEnvioEmail(acao, chamadoOriginal.getChamadoAssunto())) {
			JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

			ParametroSistema emailRemetentePadrao = parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);

			final Date dataEncerramento = chamados.stream().map(Chamado::getDataPrevisaoEncerramento).findFirst().orElse(null);
			final Date dataAbertura = chamados.stream().map(Chamado::getUltimaAlteracao).findFirst().orElse(null);
			final String status = chamados.stream().map(Chamado::getStatus).map(EntidadeConteudo::getDescricao).findFirst().orElse(null);

			String assunto = construirAssuntoEmailEmLote(acao, chamadoOriginal,
					dataEncerramento);

			final ImmutableMap<String, Object> parametros = ImmutableMap.<String, Object>builder()
					.put("nomeImovel", chamadoOriginal.getImovel().getNome())
					.put("dataAbertura", DataUtil.converterDataParaString(dataAbertura, true))
					.put("dataPrevisaoEncerramento", DataUtil.converterDataParaString(dataEncerramento))
					.put("unidadeOrganizacional", chamadoOriginal.getUnidadeOrganizacional().getDescricao())
					.put("status", status)
					.put("tipoChamado", chamadoOriginal.getChamadoTipo().getDescricao())
					.put("assuntoChamado", chamadoOriginal.getChamadoAssunto().getDescricao())
					.put("canalAtendimento", chamadoOriginal.getCanalAtendimento().getDescricao())
					.put("chamados", chamados)
					.build();

			final String conteudoEmail = Util.isoToUtf8(FreemarkerUtils.processarTemplate("emailChamadosEmLote.ftl", parametros));

			final Set<String> destinatarios = gerarDestinatarios(acao, chamadoOriginal);
			if (CollectionUtils.isNotEmpty(destinatarios)) {
				for (String destinatario : destinatarios) {
					mailUtil.enviar(emailRemetentePadrao.getValor(), destinatario, assunto, conteudoEmail, true, true);
				}
			}

		}


	}

	/**
	 * Gera a lista de destinatarios para onde o email irá ser enviado
	 * Avalia as 3 condições para envio de email:
	 * - Unidade Responsável
	 * - Unidade Superior
	 * - Unidade Visualziadora
	 *
	 * @param acao ação realizada no email
	 * @param chamado chamado manipulado
	 * @return retorna o conjunto de emails destinatarios
	 */
	private Set<String> gerarDestinatarios(AcaoChamado acao, Chamado chamado) {
		Set<String> destinatarios = new HashSet<>();

		final Set<OperacaoEnvioEmail> operacoes = acao.getFuncaoOperacaoEnvio().apply(chamado.getChamadoAssunto());
		final String emailContatoResponsavel = chamado.getUnidadeOrganizacional().getEmailContato();
		final String emailContatoUnidadeSuperior = Optional.ofNullable(chamado.getChamadoAssunto().getUnidadeOrganizacional()
				.getUnidadeSuperior()).map(UnidadeOrganizacional::getEmailContato).orElse(null);

		if (CollectionUtils.containsAny(operacoes, newHashSet(OperacaoEnvioEmail.RESPONSAVEL))
				&& StringUtils.isNotBlank(emailContatoResponsavel)) {
			destinatarios.add(emailContatoResponsavel);
		}

		if (CollectionUtils.containsAny(operacoes, newHashSet(OperacaoEnvioEmail.UNIDADE_SUPERIOR))
				&& StringUtils.isNotBlank(emailContatoUnidadeSuperior)) {
			destinatarios.add(emailContatoUnidadeSuperior);
		}

		if (CollectionUtils.containsAny(operacoes, newHashSet(OperacaoEnvioEmail.UNIDADES_VISUALIZADORAS))) {
			destinatarios.addAll(chamado.getListaUnidadeOrganizacionalVisualizadora().stream()
					.map(u -> u.getUnidadeOrganizacional().getEmailContato())
					.filter(email -> StringUtils.isNotBlank(email)
							&& ObjectUtils.notEqual(email, emailContatoResponsavel)
							&& ObjectUtils.notEqual(email, emailContatoUnidadeSuperior))
					.collect(Collectors.toSet()));

			if(!destinatarios.contains(chamado.getUnidadeOrganizacional().getEmailContato())
					&& chamado.getUnidadeOrganizacional().getEmailContato() != null){
				destinatarios.add(chamado.getUnidadeOrganizacional().getEmailContato());
			}

		}

		return destinatarios;
	}

	/**
	 * Constroi o assunto do email para o chamado informado
	 *
	 * @param acao ação executada no chamado
	 * @param chamado chamado manipulado
	 * @return retorna o assunto do email
	 */
	private String construirAssuntoEmail(AcaoChamado acao, Chamado chamado) {
		String assunto = acao.getDescricao().concat(" - Chamado ")
				.concat(chamado.getProtocolo().getNumeroProtocolo().toString());

		if (chamado.getPontoConsumo() != null) {
			assunto = assunto.concat(" - ").concat(chamado.getPontoConsumo().getDescricao());
		}

		DateFormat formato = new SimpleDateFormat(FORMATO_DATA_HORA);
		assunto = assunto.concat(" - ").concat(chamado.getChamadoAssunto().getDescricao()).concat(" - Prazo: ")
				.concat(formato.format(chamado.getDataPrevisaoEncerramento()));
		return assunto;
	}

	private String construirAssuntoEmailEmLote(AcaoChamado acao, Chamado chamadoOriginal, Date previsaoEncerramento) {
		String assunto = acao.getDescricao().concat(" - Chamados em Lote");
		DateFormat formato = new SimpleDateFormat(FORMATO_DATA_HORA);

		assunto = assunto.concat(" - ").concat(chamadoOriginal.getImovel().getNome()).concat(" - Prazo: ")
				.concat(formato.format(previsaoEncerramento));

		return assunto;
	}

	/**
	 * Informa se há a necessidade de envio de email,
	 * devido a uma determinada ação
	 * A validação de email analisa se o servidor de email está configurado
	 * nos parâmetros do sistema e se o o chamado assunto está configurado
	 * para enviar algum email para a ação informada
	 *
	 * @param acao ação operada no chamado
	 * @param chamadoAssunto chamado assunto referente
	 * @return retorna se há necessidade envio de email sim ou não
	 */
	private boolean validarEnvioEmail(AcaoChamado acao, ChamadoAssunto chamadoAssunto) throws NegocioException {

		ParametroSistema servidorEmailConfigurado = parametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO);
		final Boolean haNecessidadeDeEnvioEmail =
				Optional.ofNullable(acao).map(a -> !acao.getFuncaoOperacaoEnvio().apply(chamadoAssunto).isEmpty()).orElse(false);
		final Boolean isServidorEmailConfigurado = Objects.nonNull(servidorEmailConfigurado)
				&& BooleanUtil.converterStringCharParaBooleano(servidorEmailConfigurado.getValor());

		if (!isServidorEmailConfigurado) {
			LOG.info("Email de operação de chamado não será enviado, pois o servidor de email não está configurado");
		}

		return haNecessidadeDeEnvioEmail && isServidorEmailConfigurado;
	}

}
