/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/10/2013 10:41:38
 @author vtavares
 */

package br.com.ggas.atendimento.chamado.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;

/**
 * Chamado VO
 *
 */
public class ChamadoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// atributos de chamado

	private String descricao;

	private String nomeSolicitante;

	private String cpfSolicitante;

	private String cnpjSolicitante;

	private String rgSolicitante;
	
	private TipoContato tipoContatoSolicitante;

	private Long numeroProtocolo;

	private Long idChamadoTipo;

	private Long idServicoTipo;

	private ChamadoAssunto chamadoAssunto;

	private List<CanalAtendimento> listaCanalAtendimento = new ArrayList<>();

	private SituacaoQuestionario situacaoQuestionario;

	private UnidadeOrganizacional unidadeOrganizacional;

	private Usuario usuarioResponsavel;

	private EntidadeConteudo status;

	private Integer numeroContrato;

	private String dataInicioCriacao;

	private String dataFimCriacao;

	private String dataInicioEncerramento;

	private String dataFimEncerramento;

	private String dataInicioResolucao;

	private String dataFimResolucao;

	private String dataInicioAbertura;

	private String dataFimAbertura;

	private String cpfCnpjSolicitante;

	private List<ChamadoAssunto> chamadoAssuntos = new ArrayList<>();
	
	//atributos do ponto consumo

	private String siglaUnidadeFederacaoPontoConsumoChamado;
	
	private String nomeMunicipioPontoConsumoChamado;
	
	private String bairroPontoConsumoChamado;
	
	private String lougradoroPontoConsumoChamado;
	
	private String cepPontoConsumoChamado;
	
	// atributos de imovel
	private String cepImovel;

	private String numeroImovel;

	private String descricaoComplemento;

	private String nomeImovel;

	private Long matriculaImovel;

	private Boolean condominioImovel;

	private List<Long> idsCondominioImovel;

	// atributos de cliente
	private String cpfCliente;

	private String rgCliente;

	private String nomeCliente;

	private String numeroPassaporte;

	private String cnpjCliente;

	private String nomeFantasiaCliente;

	private Integer codigoTipoPessoa;

	private List<String> listaStatus;

	private List<String> listaPrazo;

	private Boolean indicadorAgenciaReguladora;

	private Boolean indicadorAcionamentoGasista;

	private Boolean indicadorAcionamentoPlantonista;

	private Boolean indicadorVazamentoConfirmado;

	private List<ServicoTipo> servicosTipo = new ArrayList<>();

	private List<ChamadoTipo> listaChamadoTipos = new ArrayList<>();

	private Long[] unidadesVisualizadorasIds;

	private String chavesChamadoTipo;
	
	private Long idSegmentoCliente;

	private Long idSegmentoImovel;

	private Long idSegmentoChamado;

	private Long idCategoriaChamado;

	private Long idUnidadeOrganizacionalUsuario;

	private Boolean usuarioAdministrador = false;

	private ChamadoCategoria categoria;

	private List<Long> idsUnidadesInferiores;
	
	private Long idImovel;
	
	private Long idCliente;
	
	private Long idPontoConsumo;
	
	private String fluxoVoltar;
	
	private String codigoLegado;
	
	private String periodoCriacao;
	
	private String periodoPrevisaoEncerramento;
	
	private String periodoResolucao;

	/**
	 * Retorna chavesChamadoTipo
	 * 
	 * @return chavesChamadoTipo
	 */
	public String getChavesChamadoTipo() {

		new Chamado();
		return chavesChamadoTipo;
	}

	/**
	 * Atribui chavesChamadoTipo
	 * 
	 * @param chavesChamadoTipo
	 */
	public void setChavesChamadoTipo(String chavesChamadoTipo) {

		this.chavesChamadoTipo = chavesChamadoTipo;
	}

	/**
	 * Retorna descricao
	 * 
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * Atribui descricao
	 * 
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * Retorna nomeSolicitante
	 * 
	 * @return nomeSolicitante
	 */
	public String getNomeSolicitante() {

		return nomeSolicitante;
	}

	/**
	 * Atribui nomeSolicitante
	 * 
	 * @param nomeSolicitante
	 */
	public void setNomeSolicitante(String nomeSolicitante) {

		this.nomeSolicitante = nomeSolicitante;
	}

	/**
	 * Retorna cpf
	 * 
	 * @return cpfSolicitante
	 */
	public String getCpfSolicitante() {

		return cpfSolicitante;
	}

	/**
	 * Atribui cpf
	 * 
	 * @param cpfSolicitante
	 */
	public void setCpfSolicitante(String cpfSolicitante) {

		this.cpfSolicitante = cpfSolicitante;
	}

	/**
	 * Retorna cnpj
	 * 
	 * @return cnpjSolicitante
	 */
	public String getCnpjSolicitante() {

		return cnpjSolicitante;
	}

	/**
	 * Atribui cnpj
	 * 
	 * @param cnpjSolicitante
	 */
	public void setCnpjSolicitante(String cnpjSolicitante) {

		this.cnpjSolicitante = cnpjSolicitante;
	}

	/**
	 * Retorna rg
	 * 
	 * @return rgSolicitante
	 */
	public String getRgSolicitante() {

		return rgSolicitante;
	}

	/**
	 * Atribui rg
	 * 
	 * @param rgSolicitante
	 */
	public void setRgSolicitante(String rgSolicitante) {

		this.rgSolicitante = rgSolicitante;
	}

	/**
	 * Retorna tipoContato
	 * 
	 * @return the tipoContatoSolicitante
	 */
	public TipoContato getTipoContatoSolicitante() {
		return tipoContatoSolicitante;
	}

	/**
	 * Atribui tipoContato
	 * 
	 * @param tipoContatoSolicitante the tipoContatoSolicitante to set
	 */
	public void setTipoContatoSolicitante(TipoContato tipoContatoSolicitante) {
		this.tipoContatoSolicitante = tipoContatoSolicitante;
	}

	/**
	 * Retorna numeroProtocolo
	 * 
	 * @return numeroProtocolo
	 */
	public Long getNumeroProtocolo() {

		return numeroProtocolo;
	}

	/**
	 * Atribui numeroProtocolo
	 * 
	 * @param numeroProtocolo
	 */
	public void setNumeroProtocolo(Long numeroProtocolo) {

		this.numeroProtocolo = numeroProtocolo;
	}

	/**
	 * Retorna idChamadoTipo
	 * 
	 * @return idChamadoTipo
	 */
	public Long getIdChamadoTipo() {

		return idChamadoTipo;
	}

	/**
	 * Atribui idChamadoTipo
	 * 
	 * @param idChamadoTipo
	 */
	public void setIdChamadoTipo(Long idChamadoTipo) {

		this.idChamadoTipo = idChamadoTipo;
	}

	/**
	 * Retorna idServicoTipo
	 * 
	 * @return idServicoTipo
	 */
	public Long getIdServicoTipo() {

		return idServicoTipo;
	}

	/**
	 * Atribui idServicoTipo
	 * 
	 * @param idServicoTipo
	 */
	public void setIdServicoTipo(Long idServicoTipo) {

		this.idServicoTipo = idServicoTipo;
	}

	/**
	 * Retorna chamadoAssunto
	 * 
	 * @return chamadoAssunto
	 */
	public ChamadoAssunto getChamadoAssunto() {

		return chamadoAssunto;
	}

	/**
	 * Atribui chamadoAssunto
	 * 
	 * @param chamadoAssunto
	 */
	public void setChamadoAssunto(ChamadoAssunto chamadoAssunto) {

		this.chamadoAssunto = chamadoAssunto;
	}

	/**
	 * Retorna canalAtendimento
	 * 
	 * @return canalAtendimento
	 */
	public List<CanalAtendimento> getListaCanalAtendimento() {

		return listaCanalAtendimento;
	}

	/**
	 * Atribui canalAtendimento
	 * 
	 * @param canalAtendimento
	 */
	public void setListaCanalAtendimento(List<CanalAtendimento> listaCanalAtendimento) {

		this.listaCanalAtendimento = listaCanalAtendimento;
	}

	/**
	 * Retorna unidadeOrganizacional
	 * 
	 * @return unidadeOrganizacional
	 */
	public UnidadeOrganizacional getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	/**
	 * Atribui unidadeOrganizacional
	 * 
	 * @param unidadeOrganizacional
	 */
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * Retorna usuarioResponsavel
	 * 
	 * @return usuarioResponsavel
	 */
	public Usuario getUsuarioResponsavel() {

		return usuarioResponsavel;
	}

	/**
	 * Atribui usuarioResponsavel
	 * 
	 * @param usuarioResponsavel
	 */
	public void setUsuarioResponsavel(Usuario usuarioResponsavel) {

		this.usuarioResponsavel = usuarioResponsavel;
	}

	/**
	 * Retorna cepImovel
	 * 
	 * @return cepImovel
	 */
	public String getCepImovel() {

		return cepImovel;
	}

	/**
	 * Atribui cepImovel
	 * 
	 * @param cepImovel
	 */
	public void setCepImovel(String cepImovel) {

		this.cepImovel = cepImovel;
	}

	/**
	 * @return numeroImovel
	 */
	public String getNumeroImovel() {

		return numeroImovel;
	}

	/**
	 * @param numeroImovel
	 */
	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	/**
	 * @return descricaoComplemento
	 */
	public String getDescricaoComplemento() {

		return descricaoComplemento;
	}

	/**
	 * @param descricaoComplemento
	 */
	public void setDescricaoComplemento(String descricaoComplemento) {

		this.descricaoComplemento = descricaoComplemento;
	}

	/**
	 * @return nomeImovel
	 */
	public String getNomeImovel() {

		return nomeImovel;
	}

	/**
	 * @param nomeImovel
	 */
	public void setNomeImovel(String nomeImovel) {

		this.nomeImovel = nomeImovel;
	}

	/**
	 * @return matriculaImovel
	 */
	public Long getMatriculaImovel() {

		return matriculaImovel;
	}

	/**
	 * @param matriculaImovel
	 */
	public void setMatriculaImovel(Long matriculaImovel) {

		this.matriculaImovel = matriculaImovel;
	}

	/**
	 * @return condominioImovel
	 */
	public Boolean getCondominioImovel() {

		return condominioImovel;
	}

	/**
	 * @param condominioImovel
	 */
	public void setCondominioImovel(Boolean condominioImovel) {

		this.condominioImovel = condominioImovel;
	}

	/**
	 * @return numeroContrato
	 */
	public Integer getNumeroContrato() {

		return numeroContrato;
	}

	/**
	 * @param numeroContrato
	 */
	public void setNumeroContrato(Integer numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return cpfCliente
	 */
	public String getCpfCliente() {

		return cpfCliente;
	}

	/**
	 * @param cpfCliente
	 */
	public void setCpfCliente(String cpfCliente) {

		this.cpfCliente = cpfCliente;
	}

	/**
	 * @return rgCliente
	 */
	public String getRgCliente() {

		return rgCliente;
	}

	/**
	 * @param rgCliente
	 */
	public void setRgCliente(String rgCliente) {

		this.rgCliente = rgCliente;
	}

	/**
	 * @return nomeCliente
	 */
	public String getNomeCliente() {

		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/**
	 * @return numeroPassaporte
	 */
	public String getNumeroPassaporte() {

		return numeroPassaporte;
	}

	/**
	 * @param numeroPassaporte
	 */
	public void setNumeroPassaporte(String numeroPassaporte) {

		this.numeroPassaporte = numeroPassaporte;
	}

	/**
	 * @return cnpjCliente
	 */
	public String getCnpjCliente() {

		return cnpjCliente;
	}

	/**
	 * @param cnpjCliente
	 */
	public void setCnpjCliente(String cnpjCliente) {

		this.cnpjCliente = cnpjCliente;
	}

	/**
	 * @return nomeFantasiaCliente
	 */
	public String getNomeFantasiaCliente() {

		return nomeFantasiaCliente;
	}

	/**
	 * @param nomeFantasiaCliente
	 */
	public void setNomeFantasiaCliente(String nomeFantasiaCliente) {

		this.nomeFantasiaCliente = nomeFantasiaCliente;
	}

	/**
	 * @return status
	 */
	public EntidadeConteudo getStatus() {

		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/**
	 * @return serialVersionUID
	 */
	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/**
	 * @return codigoTipoPessoa
	 */
	public Integer getCodigoTipoPessoa() {

		return codigoTipoPessoa;
	}

	/**
	 * @param codigoTipoPessoa
	 */
	public void setCodigoTipoPessoa(Integer codigoTipoPessoa) {

		this.codigoTipoPessoa = codigoTipoPessoa;
	}

	/**
	 * @return listaStatus
	 */
	public List<String> getListaStatus() {

		return listaStatus;
	}

	/**
	 * @param listaStatus
	 */
	public void setListaStatus(List<String> listaStatus) {

		this.listaStatus = listaStatus;
	}

	/**
	 * @return dataInicioCriacao
	 */
	public String getDataInicioCriacao() {

		return dataInicioCriacao;
	}

	/**
	 * @param dataInicioCriacao
	 */
	public void setDataInicioCriacao(String dataInicioCriacao) {

		this.dataInicioCriacao = dataInicioCriacao;
	}

	/**
	 * @return dataFimCriacao
	 */
	public String getDataFimCriacao() {

		return dataFimCriacao;
	}

	/**
	 * @param dataFimCriacao
	 */
	public void setDataFimCriacao(String dataFimCriacao) {

		this.dataFimCriacao = dataFimCriacao;
	}

	/**
	 * @return dataInicioEncerramento
	 */
	public String getDataInicioEncerramento() {

		return dataInicioEncerramento;
	}

	/**
	 * @param dataInicioEncerramento
	 */
	public void setDataInicioEncerramento(String dataInicioEncerramento) {

		this.dataInicioEncerramento = dataInicioEncerramento;
	}

	/**
	 * @return dataFimEncerramento
	 */
	public String getDataFimEncerramento() {

		return dataFimEncerramento;
	}

	/**
	 * @param dataFimEncerramento
	 */
	public void setDataFimEncerramento(String dataFimEncerramento) {

		this.dataFimEncerramento = dataFimEncerramento;
	}

	/**
	 * @return cpfCnpjSolicitante
	 */
	public String getCpfCnpjSolicitante() {

		return cpfCnpjSolicitante;
	}

	/**
	 * @param cpfCnpjSolicitante
	 */
	public void setCpfCnpjSolicitante(String cpfCnpjSolicitante) {

		this.cpfCnpjSolicitante = cpfCnpjSolicitante;
	}

	/**
	 * @return indicadorAgenciaReguladora
	 */
	public Boolean getIndicadorAgenciaReguladora() {

		return indicadorAgenciaReguladora;
	}

	/**
	 * @param indicadorAgenciaReguladora
	 */
	public void setIndicadorAgenciaReguladora(Boolean indicadorAgenciaReguladora) {

		this.indicadorAgenciaReguladora = indicadorAgenciaReguladora;
	}

	/**
	 * @return servicosTipo
	 */
	public List<ServicoTipo> getServicosTipo() {

		return servicosTipo;
	}

	/**
	 * @param servicosTipo
	 */
	public void setServicosTipo(List<ServicoTipo> servicosTipo) {

		this.servicosTipo = servicosTipo;
	}

	/**
	 * @return chamadoAssuntos
	 */
	public List<ChamadoAssunto> getChamadoAssuntos() {

		return chamadoAssuntos;
	}

	/**
	 * @param chamadoAssuntos
	 */
	public void setChamadoAssuntos(List<ChamadoAssunto> chamadoAssuntos) {

		this.chamadoAssuntos = chamadoAssuntos;
	}

	/**
	 * @return listaChamadoTipos
	 */
	public List<ChamadoTipo> getListaChamadoTipos() {

		return listaChamadoTipos;
	}

	/**
	 * @param listaChamadoTipos
	 */
	public void setListaChamadoTipos(List<ChamadoTipo> listaChamadoTipos) {

		this.listaChamadoTipos = listaChamadoTipos;
	}

	/**
	 * @return idsCondominioImovel
	 */
	public List<Long> getIdsCondominioImovel() {

		return idsCondominioImovel;
	}

	/**
	 * @param idsCondominioImovel
	 */
	public void setIdsCondominioImovel(List<Long> idsCondominioImovel) {

		this.idsCondominioImovel = idsCondominioImovel;
	}

	/**
	 * @return the idSegmentoImovel
	 */
	public Long getIdSegmentoImovel() {
		return idSegmentoImovel;
	}

	/**
	 * @param idSegmentoImovel the idSegmentoImovel to set
	 */
	public void setIdSegmentoImovel(Long idSegmentoImovel) {
		this.idSegmentoImovel = idSegmentoImovel;
	}

	/**
	 * @return the idSegmentoCliente
	 */
	public Long getIdSegmentoCliente() {
		return idSegmentoCliente;
	}

	/**
	 * @param idSegmentoCliente the idSegmentoCliente to set
	 */
	public void setIdSegmentoCliente(Long idSegmentoCliente) {
		this.idSegmentoCliente = idSegmentoCliente;
	}

	/**
	 * @return idUnidadeOrganizacionalUsuario
	 */
	public Long getIdUnidadeOrganizacionalUsuario() {
		return idUnidadeOrganizacionalUsuario;
	}

	/**
	 * @param idUnidadeOrganizacionalUsuario
	 */
	public void setIdUnidadeOrganizacionalUsuario(Long idUnidadeOrganizacionalUsuario) {
		this.idUnidadeOrganizacionalUsuario = idUnidadeOrganizacionalUsuario;
	}

	/**
	 * @return dataInicioAbertura
	 */
	public String getDataInicioAbertura() {
		return dataInicioAbertura;
	}

	/**
	 * @param dataInicioAbertura
	 */
	public void setDataInicioAbertura(String dataInicioAbertura) {
		this.dataInicioAbertura = dataInicioAbertura;
	}

	/**
	 * @return dataFimAbertura
	 */
	public String getDataFimAbertura() {
		return dataFimAbertura;
	}

	/**
	 * @param dataFimAbertura
	 */
	public void setDataFimAbertura(String dataFimAbertura) {
		this.dataFimAbertura = dataFimAbertura;
	}

	/**
	 * @return dataInicioResolucao
	 */
	public String getDataInicioResolucao() {
		return dataInicioResolucao;
	}

	/**
	 * @param dataInicioResolucao
	 */
	public void setDataInicioResolucao(String dataInicioResolucao) {
		this.dataInicioResolucao = dataInicioResolucao;
	}

	/**
	 * @return dataFimResolucao
	 */
	public String getDataFimResolucao() {
		return dataFimResolucao;
	}

	/**
	 * @param dataFimResolucao
	 */
	public void setDataFimResolucao(String dataFimResolucao) {
		this.dataFimResolucao = dataFimResolucao;
	}

	/**
	 * @return listaPrazo
	 */
	public List<String> getListaPrazo() {
		return listaPrazo;
	}

	/**
	 * @param listaPrazo
	 */
	public void setListaPrazo(List<String> listaPrazo) {
		this.listaPrazo = listaPrazo;
	}

	/**
	 * @return indicadorAcionamentoGasista
	 */
	public Boolean getIndicadorAcionamentoGasista() {
		return indicadorAcionamentoGasista;
	}

	/**
	 * @param indicadorAcionamentoGasista
	 */
	public void setIndicadorAcionamentoGasista(Boolean indicadorAcionamentoGasista) {
		this.indicadorAcionamentoGasista = indicadorAcionamentoGasista;
	}

	/**
	 * @return indicadorAcionamentoPlantonista
	 */
	public Boolean getIndicadorAcionamentoPlantonista() {
		return indicadorAcionamentoPlantonista;
	}

	/**
	 * @param indicadorAcionamentoPlantonista
	 */
	public void setIndicadorAcionamentoPlantonista(Boolean indicadorAcionamentoPlantonista) {
		this.indicadorAcionamentoPlantonista = indicadorAcionamentoPlantonista;
	}

	public Boolean isUsuarioAdministrador() {
		return usuarioAdministrador;
	}

	public void setUsuarioAdministrador(Boolean usuarioAdministrador) {
		this.usuarioAdministrador = usuarioAdministrador;
	}

	/**
	 * @return the indicadorVazamentoConfirmado
	 */
	public Boolean getIndicadorVazamentoConfirmado() {
		return indicadorVazamentoConfirmado;
	}

	/**
	 * @param indicadorVazamentoConfirmado the indicadorVazamentoConfirmado to set
	 */
	public void setIndicadorVazamentoConfirmado(Boolean indicadorVazamentoConfirmado) {
		this.indicadorVazamentoConfirmado = indicadorVazamentoConfirmado;
	}

	/**
	 * @return situacaoQuestionario
	 */
	public SituacaoQuestionario getSituacaoQuestionario() {
		return situacaoQuestionario;
	}

	/**
	 * @param situacaoQuestionario
	 */
	public void setSituacaoQuestionario(SituacaoQuestionario situacaoQuestionario) {
		this.situacaoQuestionario = situacaoQuestionario;
	}

	/**
	 * @return idSegmentoChamado
	 */
	public Long getIdSegmentoChamado() {
		return idSegmentoChamado;
	}

	/**
	 * @param idSegmentoChamado
	 */
	public void setIdSegmentoChamado(Long idSegmentoChamado) {
		this.idSegmentoChamado = idSegmentoChamado;
	}

	/**
	 * @return siglaUnidadeFederacaoPontoConsumoChamado
	 */
	public String getSiglaUnidadeFederacaoPontoConsumoChamado() {
		return siglaUnidadeFederacaoPontoConsumoChamado;
	}

	/**
	 * @param siglaUnidadeFederacaoPontoConsumoChamado
	 */
	public void setSiglaUnidadeFederacaoPontoConsumoChamado(String siglaUnidadeFederacaoPontoConsumoChamado) {
		this.siglaUnidadeFederacaoPontoConsumoChamado = siglaUnidadeFederacaoPontoConsumoChamado;
	}

	/**
	 * @return nomeMunicipioPontoConsumoChamado
	 */
	public String getNomeMunicipioPontoConsumoChamado() {
		return nomeMunicipioPontoConsumoChamado;
	}

	/**
	 * @param nomeMunicipioPontoConsumoChamado
	 */
	public void setNomeMunicipioPontoConsumoChamado(String nomeMunicipioPontoConsumoChamado) {
		this.nomeMunicipioPontoConsumoChamado = nomeMunicipioPontoConsumoChamado;
	}

	/**
	 * @return bairroPontoConsumoChamado
	 */
	public String getBairroPontoConsumoChamado() {
		return bairroPontoConsumoChamado;
	}

	/**
	 * @param bairroPontoConsumoChamado
	 */
	public void setBairroPontoConsumoChamado(String bairroPontoConsumoChamado) {
		this.bairroPontoConsumoChamado = bairroPontoConsumoChamado;
	}

	/**
	 * @return lougradoroPontoConsumoChamado
	 */
	public String getLougradoroPontoConsumoChamado() {
		return lougradoroPontoConsumoChamado;
	}

	/**
	 * @param lougradoroPontoConsumoChamado
	 */
	public void setLougradoroPontoConsumoChamado(String lougradoroPontoConsumoChamado) {
		this.lougradoroPontoConsumoChamado = lougradoroPontoConsumoChamado;
	}

	/**
	 * @return cepPontoConsumoChamado
	 */
	public String getCepPontoConsumoChamado() {
		return cepPontoConsumoChamado;
	}

	/**
	 * @param cepPontoConsumoChamado
	 */
	public void setCepPontoConsumoChamado(String cepPontoConsumoChamado) {
		this.cepPontoConsumoChamado = cepPontoConsumoChamado;
	}

	/**
	 * @return unidadesVisualizadorasIds
	 */
	public Long[] getUnidadesVisualizadorasIds() {
		return unidadesVisualizadorasIds;
	}

	/**
	 * @param unidadesVisualizadorasIds
	 */
	public void setUnidadesVisualizadorasIds(Long[] unidadesVisualizadorasIds) {
		if(unidadesVisualizadorasIds != null) {
			this.unidadesVisualizadorasIds = unidadesVisualizadorasIds.clone();
		}
	}

	/**
	 * @return categoria
	 */
	public ChamadoCategoria getCategoria() {
		return categoria;
	}

	/**
	 * @param categoria
	 */
	public void setCategoria(ChamadoCategoria categoria) {
		this.categoria = categoria;
	}

	public Long getIdCategoriaChamado() {
		return idCategoriaChamado;
	}

	public void setIdCategoriaChamado(Long idCategoriaChamado) {
		this.idCategoriaChamado = idCategoriaChamado;
	}

	/**
	 * Obtém a lista de ids de unidades inferiores
	 * @return retorna a lista de ids de unidades
	 */
	public List<Long> getIdsUnidadesInferiores() {
		return idsUnidadesInferiores;
	}

	/**
	 * Configura os ids de unidades inferiores
	 * @param idsUnidadesInferiores lista de ids de unidades inferiores
	 */
	public void setIdsUnidadesInferiores(List<Long> idsUnidadesInferiores) {
		this.idsUnidadesInferiores = idsUnidadesInferiores;
	}

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public String getFluxoVoltar() {
		return fluxoVoltar;
	}

	public void setFluxoVoltar(String fluxoVoltar) {
		this.fluxoVoltar = fluxoVoltar;
	}

	public String getCodigoLegado() {
		return codigoLegado;
	}

	public void setCodigoLegado(String codigoLegado) {
		this.codigoLegado = codigoLegado;
	}

	public String getPeriodoCriacao() {
		return periodoCriacao;
	}

	public void setPeriodoCriacao(String periodoCriacao) {
		this.periodoCriacao = periodoCriacao;
	}

	public String getPeriodoPrevisaoEncerramento() {
		return periodoPrevisaoEncerramento;
	}

	public void setPeriodoPrevisaoEncerramento(String periodoPrevisaoEncerramento) {
		this.periodoPrevisaoEncerramento = periodoPrevisaoEncerramento;
	}

	public String getPeriodoResolucao() {
		return periodoResolucao;
	}

	public void setPeriodoResolucao(String periodoResolucao) {
		this.periodoResolucao = periodoResolucao;
	}
}
