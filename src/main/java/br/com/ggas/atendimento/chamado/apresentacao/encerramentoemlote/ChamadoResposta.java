/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote;

import java.io.Serializable;

/**
 * Classe com as informações necessárias para o frontend processar e exibir os chamados que foram encerrados com sucesso
 *
 * Não contém lógica, contém somente a composição dos dados (chave primária do chamado e descrição do seu status)
 *
 * @author italo.alan@logiquesistemas.com.br
 */
public class ChamadoResposta implements Serializable {
	private Long chavePrimaria;
	private String statusDescricao;

	private ChamadoResposta() {
	}

	/**
	 * constroi uma instancia de {@link ChamadoResposta}
	 * @param chavePrimaria {@link Long}
	 * @param statusDescricao {@link String}
	 * @return chamadoResposta {@link ChamadoResposta}
	 */
	public static ChamadoResposta build(Long chavePrimaria, String statusDescricao) {
		ChamadoResposta chamadoResposta = new ChamadoResposta();
		chamadoResposta.setChavePrimaria(chavePrimaria);
		chamadoResposta.setStatusDescricao(statusDescricao);
		return chamadoResposta;
	}

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public String getStatusDescricao() {
		return statusDescricao;
	}

	public void setStatusDescricao(String statusDescricao) {
		this.statusDescricao = statusDescricao;
	}
}
