/*
 * Copyright (C) <2011> GGAS – Sistema de Gestao Comercial (Billing) de Serviços de Distribuiçao de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestao comercial de Serviços de Distribuiçao de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versao 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇaO ou de
 * ADEQUAÇaO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se nao, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestao Comercial (Billing) de Serviços de Distribuiçao de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.chamado.dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.resposta.negocio.ControladorResposta;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * ChamadoVORelatorioQuantitativo
 *
 */
public class ChamadoVORelatorioQuantitativo {

	private static final int DUAS_HORAS = 2;

	private static final int SEIS_HORAS = 6;

	private static final int QUARENTA_E_OITO_HORAS = 48;

	private static final int TRINTA_MINUTOS = 30;

	private static final int TREZE_HORAS = 13;

	private static final int DEZESSETE_HORAS = 17;

	private static final int DOZE_HORAS = 12;

	private static final int OITO_HORAS = 8;

	private static final int DIA_VINTE = 20;

	private static final int DIA_DEZ = 10;

	private static final int SITUACAO_QUESTIONARIO = 3;

	private static final int CHAMADO_STATUS = 425;

	private static final int TIPO_RESTRICAO_UM = 300;

	private static final int TIPO_RESTRICAO_DOIS = 301;

	private static final int TIPO_RESTRICAO_TRES = 302;

	private static final int TIPO_RESTRICAO_QUATRO = 303;

	private Integer finalizadosNoPrazo = 0;

	private Integer finalizadosComAtraso = 0;

	private Integer emAndamento = 0;

	private Integer emAberto = 0;

	private Integer emAtraso = 0;

	private List<SubReportHelper> segmentos = new ArrayList<>();

	private List<SubReportHelper> categorias = new ArrayList<>();

	private List<SubReportHelper> questionario = new ArrayList<>();

	private List<SubReportHelper> categoriasSegmentos = new ArrayList<>();

	private List<SubReportHelper> listaCanalAtendimento = new ArrayList<>();

	private Integer totalVazamentos = 0;

	private Integer vazamentosConfirmados = 0;

	private Integer dia1a10 = 0;

	private Integer dia11a20 = 0;

	private Integer dia21a31 = 0;

	private Integer manha = 0;

	private Integer tarde = 0;

	private Integer intervalo = 0;

	private Integer foraHora = 0;

	private Integer totalChamados = 0;

	private Integer emergencia = 0;

	private Integer urgencia = 0;

	private Integer comum = 0;

	private Integer convencional = 0;

	/**
	 * Controlador resposta
	 */
	@Autowired
	private ControladorResposta controladorResposta;

	/**
	 * Construtor
	 * 
	 * @param chamados {@link List}
	 * @throws NegocioException {@link NegocioException}
	 */
	public ChamadoVORelatorioQuantitativo(List<Chamado> chamados) throws NegocioException {

		if (chamados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO, true);
		}
		setTotalChamados(chamados.size());
		for (Chamado chamado : chamados) {

			contabilizaGradacao(chamado.getChamadoAssunto().getQuantidadeHorasPrevistaAtendimento());
			contabilizaStatus(chamado);

			if (chamado.getIndicadorVazamentoConfirmado() != null) {
				totalVazamentos += 1;
				if (chamado.getIndicadorVazamentoConfirmado()) {
					vazamentosConfirmados += 1;
				}
			}

			Date aberturaChamado = chamado.getProtocolo().getUltimaAlteracao();
			preencherPeriodo(aberturaChamado);

			Segmento segmento = chamado.getChamadoAssunto().getChamadoTipo().getSegmento();
			if (segmento != null) {
				SubReportHelper subReportHelper = adicionaItemLista(segmento.getDescricao(), segmentos);
				adicionaItemLista(chamado.getChamadoAssunto().getChamadoTipo().getDescricao(), subReportHelper.getListaSubReport());
			} else {
				SubReportHelper subReportHelper = adicionaItemLista("NÃO DEFINIDO", segmentos);
				adicionaItemLista(chamado.getChamadoAssunto().getChamadoTipo().getDescricao(), subReportHelper.getListaSubReport());
			}

			String canalAtendimento = chamado.getCanalAtendimento().getDescricao();
			adicionaItemLista(canalAtendimento, listaCanalAtendimento);

			ChamadoCategoria categoria = chamado.getChamadoAssunto().getChamadoTipo().getCategoria();
			if (categoria != null && "RECLAMAÇÃO".equals(categoria.getDescricao())) {
				SubReportHelper subReportHelper = adicionaItemLista(categoria.getDescricao(), getCategorias());
				adicionaItemLista(chamado.getChamadoAssunto().getChamadoTipo().getDescricao(), subReportHelper.getListaSubReport());
			}

			ChamadoCategoria categoriaSegmento = chamado.getChamadoAssunto().getChamadoTipo().getCategoria();
			if (categoria != null && chamado.getChamadoAssunto().getChamadoTipo().getSegmento() != null) {
				SubReportHelper subReportHelper1 = adicionaItemLista(categoriaSegmento.getDescricao(), getCategoriasSegmentos());
				adicionaItemLista(chamado.getChamadoAssunto().getChamadoTipo().getSegmento().getDescricao(),
						subReportHelper1.getListaSubReport());
			}
			ChamadoTipo restrincao = chamado.getChamadoAssunto().getChamadoTipo();

			Questionario questionarios = chamado.getChamadoAssunto().getQuestionario();
			if (chamado.getSituacaoQuestionario() != null && chamado.getSituacaoQuestionario().getCodigo() == SITUACAO_QUESTIONARIO && questionarios != null
					&& chamado.getStatus().getChavePrimaria() == CHAMADO_STATUS && restrincao.getChavePrimaria() != TIPO_RESTRICAO_UM
					&& restrincao.getChavePrimaria() != TIPO_RESTRICAO_DOIS && restrincao.getChavePrimaria() != TIPO_RESTRICAO_TRES
					&& restrincao.getChavePrimaria() != TIPO_RESTRICAO_QUATRO) {
				SubReportHelper subReportHelper = adicionaItemLista(questionarios.getNomeQuestionario(), getQuestionario());
				adicionaItemLista(chamado.getChamadoAssunto().getQuestionario().getNomeQuestionario(), subReportHelper.getListaSubReport());
			}

		}
	}

	/**
	 * Contabiliza os status
	 * 
	 * @param chamado
	 */
	private void contabilizaStatus(Chamado chamado) {
		if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_FINALIZADO)) {
			if (chamado.getDataPrevisaoEncerramento().after(chamado.getUltimaAlteracao())) {
				finalizadosNoPrazo += 1;
			} else {
				finalizadosComAtraso += 1;
			}
		} else {
			if (chamado.getDataPrevisaoEncerramento() != null && chamado.getDataPrevisaoEncerramento().before(new Date())) {
				emAtraso += 1;
			} else if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_EM_ANDAMENTO)) {
				emAndamento += 1;
			} else if (chamado.getStatus().getDescricao().equalsIgnoreCase(Constantes.CHAMADO_STATUS_ABERTO)) {
				emAberto += 1;
			}
		}
	}

	/**
	 * Contabiliza a gradação
	 * 
	 * @param quantidadeHorasPrevistaAtendimento
	 */
	private void contabilizaGradacao(Integer quantidadeHorasPrevistaAtendimento) {

		if (quantidadeHorasPrevistaAtendimento <= DUAS_HORAS) {
			setEmergencia(getEmergencia() + 1);
		} else if (quantidadeHorasPrevistaAtendimento <= SEIS_HORAS) {
			setUrgencia(getUrgencia() + 1);
		} else if (quantidadeHorasPrevistaAtendimento <= QUARENTA_E_OITO_HORAS) {
			setComum(getComum() + 1);
		} else {
			setConvencional(getConvencional() + 1);
		}

	}

	/**
	 * Preenche o periodo baseado na data de abertura
	 * 
	 * @param aberturaChamado
	 */
	private void preencherPeriodo(Date aberturaChamado) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(aberturaChamado);

		int diaAbertura = calendar.get(Calendar.DAY_OF_MONTH);

		if (diaAbertura <= DIA_DEZ) {
			dia1a10 += 1;
		} else if (diaAbertura <= DIA_VINTE) {
			dia11a20 += 1;
		} else {
			dia21a31 += 1;
		}

		int horaAbertura = calendar.get(Calendar.HOUR_OF_DAY);
		int minAbertura = calendar.get(Calendar.MINUTE);

		if (horaAbertura >= OITO_HORAS && horaAbertura < DOZE_HORAS) {
			manha += 1;
		} else if (horaAbertura == DOZE_HORAS) {
			intervalo += 1;
		} else if (horaAbertura > DOZE_HORAS && horaAbertura <= DEZESSETE_HORAS) {
			if (horaAbertura == TREZE_HORAS && minAbertura < TRINTA_MINUTOS) {
				intervalo += 1;
			} else if (horaAbertura == DEZESSETE_HORAS && minAbertura > TRINTA_MINUTOS) {
				foraHora += 1;
			} else {
				tarde += 1;
			}
		} else {
			foraHora += 1;
		}
	}

	/**
	 * Adiciona 1 ao segmento ou cria se náo existir
	 * 
	 * @param segmento
	 */
	private SubReportHelper adicionaItemLista(String descricao, List<SubReportHelper> lista) {

		SubReportHelper subReport = new SubReportHelper(descricao);

		for (SubReportHelper item : lista) {
			if (subReport.getDescricao().equals(item.getDescricao())) {
				item.plusOne();
				return item;
			}
		}

		lista.add(subReport);

		return subReport;
	}

	/**
	 * Obtem quantidade de chamados finalizados no prazo
	 * 
	 * @return the finalizadosNoPrazo
	 */
	public Integer getFinalizadosNoPrazo() {
		return finalizadosNoPrazo;
	}

	/**
	 * Atribui quantidade de chamados finalizados no prazo
	 * 
	 * @param finalizadosNoPrazo the finalizadosNoPrazo to set
	 */
	public void setFinalizadosNoPrazo(Integer finalizadosNoPrazo) {
		this.finalizadosNoPrazo = finalizadosNoPrazo;
	}

	/**
	 * Obtem quantidade de chamados finalizados com atraso
	 * 
	 * @return the finalizadosComAtraso
	 */
	public Integer getFinalizadosComAtraso() {
		return finalizadosComAtraso;
	}

	/**
	 * Atribui quantidade de chamados finalizados com atraso
	 * 
	 * @param finalizadosComAtraso the finalizadosComAtraso to set
	 */
	public void setFinalizadosComAtraso(Integer finalizadosComAtraso) {
		this.finalizadosComAtraso = finalizadosComAtraso;
	}

	/**
	 * Obtem quantidade de chamados em andamento
	 * 
	 * @return the emAndamento
	 */
	public Integer getEmAndamento() {
		return emAndamento;
	}

	/**
	 * Atribui quantidade de chamados em andamento
	 * 
	 * @param emAndamento the emAndamento to set
	 */
	public void setEmAndamento(Integer emAndamento) {
		this.emAndamento = emAndamento;
	}

	/**
	 * Obtem quantidade de chamados em aberto
	 * 
	 * @return the emAberto
	 */
	public Integer getEmAberto() {
		return emAberto;
	}

	/**
	 * Atribui quantidade de chamados em aberto
	 * 
	 * @param emAberto the emAberto to set
	 */
	public void setEmAberto(Integer emAberto) {
		this.emAberto = emAberto;
	}

	/**
	 * Obtem quantidade de chamados em atraso
	 * 
	 * @return the emAtraso
	 */
	public Integer getEmAtraso() {
		return emAtraso;
	}

	/**
	 * Atribui quantidade de chamados em atraso
	 * 
	 * @param emAtraso the emAtraso to set
	 */
	public void setEmAtraso(Integer emAtraso) {
		this.emAtraso = emAtraso;
	}

	/**
	 * Obtem quantidade de chamados agrupado por segmentos
	 * 
	 * @return the segmentos
	 */
	public List<SubReportHelper> getSegmentos() {
		return segmentos;
	}

	/**
	 * Atribui quantidade de chamados agrupado por segmentos
	 * 
	 * @param segmentos the segmentos to set
	 */
	public void setSegmentos(List<SubReportHelper> segmentos) {
		this.segmentos = segmentos;
	}

	/**
	 * Obtem quantidade de chamados agrupado por canal de contato
	 * 
	 * @return the canalContatos
	 */
	public List<SubReportHelper> getListaCanalAtendimento() {
		return listaCanalAtendimento;
	}

	/**
	 * Atribui quantidade de chamados agrupado por canal de contato
	 * 
	 * @param canalContatos the canalContatos to set
	 */
	public void setCanalContatos(List<SubReportHelper> listaCanalAtendimento) {
		this.listaCanalAtendimento = listaCanalAtendimento;
	}

	/**
	 * Obtem quantidade de chamado com vazamentos
	 * 
	 * @return the totalVazamentos
	 */
	public Integer getTotalVazamentos() {
		return totalVazamentos;
	}

	/**
	 * Atribui quantidade de chamado com vazamentos
	 * 
	 * @param totalVazamentos the totalVazamentos to set
	 */
	public void setTotalVazamentos(Integer totalVazamentos) {
		this.totalVazamentos = totalVazamentos;
	}

	/**
	 * Obtem quantidade de chamado com vazamentos confirmados
	 * 
	 * @return the vazamentosConfirmados
	 */
	public Integer getVazamentosConfirmados() {
		return vazamentosConfirmados;
	}

	/**
	 * Atribui quantidade de chamado com vazamentos confirmados
	 * 
	 * @param vazamentosConfirmados the vazamentosConfirmados to set
	 */
	public void setVazamentosConfirmados(Integer vazamentosConfirmados) {
		this.vazamentosConfirmados = vazamentosConfirmados;
	}

	/**
	 * Obtem quantidade de chamado do dia 1 ao 10
	 * 
	 * @return the dia1a10
	 */
	public Integer getDia1a10() {
		return dia1a10;
	}

	/**
	 * Atribui quantidade de chamado do dia 1 ao 10
	 * 
	 * @param dia1a10 the dia1a10 to set
	 */
	public void setDia1a10(Integer dia1a10) {
		this.dia1a10 = dia1a10;
	}

	/**
	 * Obtem quantidade de chamado do dia 11 ao 20
	 * 
	 * @return the dia11a20
	 */
	public Integer getDia11a20() {
		return dia11a20;
	}

	/**
	 * Atribui quantidade de chamado do dia 11 ao 20
	 * 
	 * @param dia11a20 the dia11a20 to set
	 */
	public void setDia11a20(Integer dia11a20) {
		this.dia11a20 = dia11a20;
	}

	/**
	 * Obtem quantidade de chamado do dia 21 ao 31
	 * 
	 * @return the dia21a31
	 */
	public Integer getDia21a31() {
		return dia21a31;
	}

	/**
	 * Atribui quantidade de chamado do dia 21 ao 31
	 * 
	 * @param dia21a31 the dia21a31 to set
	 */
	public void setDia21a31(Integer dia21a31) {
		this.dia21a31 = dia21a31;
	}

	/**
	 * Obtem quantidade de chamado pela manha
	 * 
	 * @return the manha
	 */
	public Integer getManha() {
		return manha;
	}

	/**
	 * Atribui quantidade de chamado pela manha
	 * 
	 * @param manha the manha to set
	 */
	public void setManha(Integer manha) {
		this.manha = manha;
	}

	/**
	 * Obtem quantidade de chamado a tarde
	 * 
	 * @return the tarde
	 */
	public Integer getTarde() {
		return tarde;
	}

	/**
	 * Atribui quantidade de chamado a tarde
	 * 
	 * @param tarde the tarde to set
	 */
	public void setTarde(Integer tarde) {
		this.tarde = tarde;
	}

	/**
	 * Obtem quantidade de chamado no intervalo
	 * 
	 * @return the intervalo
	 */
	public Integer getIntervalo() {
		return intervalo;
	}

	/**
	 * Atribui quantidade de chamado no intervalo
	 * 
	 * @param intervalo the intervalo to set
	 */
	public void setIntervalo(Integer intervalo) {
		this.intervalo = intervalo;
	}

	/**
	 * Obtem quantidade de chamado fora de horario comercial
	 * 
	 * @return the foraHora
	 */
	public Integer getForaHora() {
		return foraHora;
	}

	/**
	 * Atribui quantidade de chamado fora de horario comercial
	 * 
	 * @param foraHora the foraHora to set
	 */
	public void setForaHora(Integer foraHora) {
		this.foraHora = foraHora;
	}

	/**
	 * Retorna o total de chamados
	 * 
	 * @return the totalChamados
	 */
	public Integer getTotalChamados() {
		return totalChamados;
	}

	/**
	 * Atribui o total de chamados
	 * 
	 * @param totalChamados the totalChamados to set
	 */
	public final void setTotalChamados(Integer totalChamados) {
		this.totalChamados = totalChamados;
	}

	/**
	 * Retorna emergencia
	 * 
	 * @return emergencia
	 */
	public Integer getEmergencia() {
		return emergencia;
	}

	/**
	 * Atribui emergencia
	 * 
	 * @param emergencia
	 */
	public void setEmergencia(Integer emergencia) {
		this.emergencia = emergencia;
	}

	/**
	 * Retorna urgencia
	 * 
	 * @return urgencia
	 */
	public Integer getUrgencia() {
		return urgencia;
	}

	/**
	 * Atribui urgencia
	 * 
	 * @param urgencia
	 */
	public void setUrgencia(Integer urgencia) {
		this.urgencia = urgencia;
	}

	/**
	 * Retorna comum
	 * 
	 * @return comum
	 */
	public Integer getComum() {
		return comum;
	}

	/**
	 * Atribui comum
	 * 
	 * @param comum
	 */
	public void setComum(Integer comum) {
		this.comum = comum;
	}

	/**
	 * Retorna convencional
	 * 
	 * @return convencional
	 */
	public Integer getConvencional() {
		return convencional;
	}

	/**
	 * Atribui convencional
	 * 
	 * @param convencional
	 */
	public void setConvencional(Integer convencional) {
		this.convencional = convencional;
	}

	/**
	 * Retorna categorias
	 * 
	 * @return categorias
	 */
	public List<SubReportHelper> getCategorias() {
		return categorias;
	}

	/**
	 * Atribui categorias
	 * 
	 * @param categorias
	 */
	public void setCategorias(List<SubReportHelper> categorias) {
		this.categorias = categorias;
	}

	/**
	 * Retorna questionario
	 * 
	 * @return questionario
	 */
	public List<SubReportHelper> getQuestionario() {
		return questionario;
	}

	/**
	 * Atribui questionario
	 * 
	 * @param questionario
	 */
	public void setQuestionario(List<SubReportHelper> questionario) {
		this.questionario = questionario;
	}

	/**
	 * Retorna categoriasSegmentos
	 * 
	 * @return categoriasSegmentos
	 */
	public List<SubReportHelper> getCategoriasSegmentos() {
		return categoriasSegmentos;
	}

	/**
	 * Atribui categoriasSegmentos
	 * 
	 * @param categoriasSegmentos
	 */
	public void setCategoriasSegmentos(List<SubReportHelper> categoriasSegmentos) {
		this.categoriasSegmentos = categoriasSegmentos;
	}

}
