/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 09/01/2014 15:10:12
 @author ifrancisco
 */

package br.com.ggas.atendimento.chamado.dominio;

import java.util.List;

/**
 * ChamadoVORelatorioClientePontoConsumo
 *
 */
public class ChamadoVORelatorioClientePontoConsumo {

	//private List<ClienteVO> listaClienteVO;
	
	private List<ChamadoClienteVO> listaClienteVO;

	private Integer totalGeralCliente;

	private Integer totalGeralPontoConsumo;

	private Integer totalChamado;

	/**
	 * @return the listaClienteVO
	 */
	public List<ChamadoClienteVO> getListaClienteVO() {

		return listaClienteVO;
	}

	/**
	 * @param listaClienteVO
	 *            the listaClienteVO to set
	 */
	public void setListaClienteVO(List<ChamadoClienteVO> listaClienteVO) {

		this.listaClienteVO = listaClienteVO;
	}

	/**
	 * @return o total geral do cliente
	 */
	public Integer getTotalGeralCliente() {

		return totalGeralCliente;
	}

	/**
	 * @param totalGeralCliente
	 */
	public void setTotalGeralCliente(Integer totalGeralCliente) {

		this.totalGeralCliente = totalGeralCliente;
	}

	/**
	 * @return o total geral do ponto de consumo
	 */
	public Integer getTotalGeralPontoConsumo() {

		return totalGeralPontoConsumo;
	}

	/**
	 * @param totalGeralPontoConsumo
	 */
	public void setTotalGeralPontoConsumo(Integer totalGeralPontoConsumo) {

		this.totalGeralPontoConsumo = totalGeralPontoConsumo;
	}

	/**
	 * @return total de chamados
	 */
	public Integer getTotalChamado() {

		return totalChamado;
	}

	/**
	 * @param totalChamado
	 */
	public void setTotalChamado(Integer totalChamado) {

		this.totalChamado = totalChamado;
	}
	
	public int calcularTotalGeralPontoConsumo() {
		
		int total = 0;
		for (ChamadoClienteVO vo : listaClienteVO) {
			if (vo.getPontoConsumo() != null) {
				total++;
			}
		}
		return total;
	}
	
	public int calcularTotalChamado() {

		if (listaClienteVO != null) {
			return listaClienteVO.size();
		} else {
			return 0;
		}
	}

}
