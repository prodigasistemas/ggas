package br.com.ggas.atendimento.chamado.dominio;

/**
 * PesquisarClientePopupVO
 * Classe responsável pela representação de uma entidade 
 * para pesquisa de cliente.
 * 
 */
public class PesquisarClientePopupVO {
	
	private boolean pessoaFisica;
	
	private boolean pessoaJuridica;
	
	private String numeroCep;
	
	private String funcionalidade;
	
	private String habilitado;
	
	private boolean multSelect;
	
	private Long[] chavesPrimarias;
	
	private Long[] listaIdCliente;
	
	private boolean comGrupoEconomico;
	
	/**
	 * @return booleano PessoaFisica
	 */
	public boolean isPessoaFisica() {
		return pessoaFisica;
	}

	/**
	 * @param pessoaFisica
	 */
	public void setPessoaFisica(boolean pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	/**
	 * @return boolean
	 */
	public boolean isPessoaJuridica() {
		return pessoaJuridica;
	}

	/**
	 * @param pessoaJuridica
	 */
	public void setPessoaJuridica(boolean pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	/**
	 * @return numeroCep
	 */
	public String getNumeroCep() {
		return numeroCep;
	}

	/**
	 * @param numeroCep
	 */
	public void setNumeroCep(String numeroCep) {
		this.numeroCep = numeroCep;
	}

	/**
	 * @return String
	 */
	public String getFuncionalidade() {
		return funcionalidade;
	}

	/**
	 * @param funcionalidade
	 */
	public void setFuncionalidade(String funcionalidade) {
		this.funcionalidade = funcionalidade;
	}

	/**
	 * @return String
	 */
	public String getHabilitado() {
		return habilitado;
	}

	/**
	 * @param habilitado
	 */
	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	/**
	 * @return boolean
	 */
	public boolean isMultSelect() {
		return multSelect;
	}

	/**
	 * @param multSelect
	 */
	public void setMultSelect(boolean multSelect) {
		this.multSelect = multSelect;
	}

	/**
	 * @return Long[]
	 */
	public Long[] getChavesPrimarias() {
		return chavesPrimarias.clone();
	}

	/**
	 * @param chavesPrimarias
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		this.chavesPrimarias = chavesPrimarias.clone();
	}

	/**
	 * @return Long[]
	 */
	public Long[] getListaIdCliente() {
		return listaIdCliente.clone();
	}

	/**
	 * @param listaIdCliente
	 */
	public void setListaIdCliente(Long[] listaIdCliente) {
		this.listaIdCliente = listaIdCliente.clone();
	}

	/**
	 * @return boolean
	 */
	public boolean isComGrupoEconomico() {
		return comGrupoEconomico;
	}

	/**
	 * @param comGrupoEconomico
	 */
	public void setComGrupoEconomico(boolean comGrupoEconomico) {
		this.comGrupoEconomico = comGrupoEconomico;
	}
	
	/**
	 * 
	 * @return {@link boolean}
	 */
	public boolean verificaChaves() {
		boolean result=false;
		if(chavesPrimarias != null) {
			result = true;
		}
		return result;
	}
	
	/**
	 * 
	 * @return {@link boolean}
	 */
	public boolean verificarIds() {
		boolean result=false;
		if(listaIdCliente != null) {
			result = true;
		}
		return result;
	}
}
