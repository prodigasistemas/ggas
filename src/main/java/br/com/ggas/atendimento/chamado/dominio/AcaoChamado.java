package br.com.ggas.atendimento.chamado.dominio;

import java.util.Set;
import java.util.function.Function;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.dominio.OperacaoEnvioEmail;

/**
 * Classe responsável por representar uma ação do chamado
 *
 */
 
public enum AcaoChamado {

	INSERIR("ABERTO", ChamadoAssunto::getEnviarEmailCadastro),
	TRAMITAR("TRAMITADO", ChamadoAssunto::getEnviarEmailTramitar),
	REITERAR("REITERADO", ChamadoAssunto::getEnviarEmailReiterar),
	ENCERRAR("ENCERRADO", ChamadoAssunto::getEnviarEmailEncerrar);
	
	private String descricao;

	/**
	 * Função get correspondente da classe {@link ChamadoAssunto} para se obter o conjunto de operacoes de envio de email
	 */
	private Function<ChamadoAssunto, Set<OperacaoEnvioEmail>> funcaoOperacaoEnvio;

	/**
	 * Construtor padrao
	 * @param descricao
	 * @param funcaoOperacaoEnvio
	 */
	AcaoChamado(String descricao, Function<ChamadoAssunto, Set<OperacaoEnvioEmail>> funcaoOperacaoEnvio) {
		this.descricao = descricao;
		this.funcaoOperacaoEnvio = funcaoOperacaoEnvio;
	}

	/**
	 * 
	 * @return descricao
	 * 
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Obtem a funcao operacao de envio
	 * @return funcao get
	 */
	public Function<ChamadoAssunto, Set<OperacaoEnvioEmail>> getFuncaoOperacaoEnvio() {
		return funcaoOperacaoEnvio;
	}
	
}
