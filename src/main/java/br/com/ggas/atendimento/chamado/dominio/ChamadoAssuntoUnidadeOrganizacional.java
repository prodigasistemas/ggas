
package br.com.ggas.atendimento.chamado.dominio;

import java.util.Date;
import java.util.Map;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 * Classe responsável pela representação de um chamadoAssunto
 * 
 * 
 */
public class ChamadoAssuntoUnidadeOrganizacional extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -8519624329576816063L;

	private UnidadeOrganizacional unidadeOrganizacional;

	private ChamadoAssunto chamadoAssunto;

	/**
	 * Nova instancia da classe com valores default para versão, ultima alteração e habilitado
	 */
	public ChamadoAssuntoUnidadeOrganizacional() {
		this.setUltimaAlteracao(new Date());
		this.setHabilitado(true);
		this.setVersao(1);
	}

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	/**
	 * @return unidadeOrganizacional
	 */
	public UnidadeOrganizacional getUnidadeOrganizacional() {
		return unidadeOrganizacional;
	}

	/**
	 * @param unidadeOrganizacional
	 */
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {
		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * @return chamadoAssunto
	 */
	public ChamadoAssunto getChamadoAssunto() {
		return chamadoAssunto;
	}

	/**
	 * @param chamadoAssunto
	 */
	public void setChamadoAssunto(ChamadoAssunto chamadoAssunto) {
		this.chamadoAssunto = chamadoAssunto;
	}
}
