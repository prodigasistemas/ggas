/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/02/2014 12:29:59
 @author vtavares
 */

package br.com.ggas.atendimento.chamado.dominio;

import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação de uma alteração do vencimento do chamado.
 */
public class ChamadoAlteracaoVencimento extends EntidadeNegocioImpl {

	private static final long serialVersionUID = 6218198831102796245L;

	private Chamado chamado;

	private PontoConsumo pontoConsumo;

	private Integer diaVigente;

	private Integer diaSolicitado;

	/**
	 * @return chamado
	 */
	public Chamado getChamado() {

		return chamado;
	}

	/**
	 * @param chamado
	 */
	public void setChamado(Chamado chamado) {

		this.chamado = chamado;
	}

	/**
	 * @return pontoConsumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return diaVigente
	 */
	public Integer getDiaVigente() {

		return diaVigente;
	}

	/**
	 * @param diaVigente
	 */
	public void setDiaVigente(Integer diaVigente) {

		this.diaVigente = diaVigente;
	}

	/**
	 * @return diaSolicitado
	 */
	public Integer getDiaSolicitado() {

		return diaSolicitado;
	}

	/**
	 * @param diaSolicitado
	 */
	public void setDiaSolicitado(Integer diaSolicitado) {

		this.diaSolicitado = diaSolicitado;
	}

	/**
	 * @return serialVersionUID
	 */
	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
