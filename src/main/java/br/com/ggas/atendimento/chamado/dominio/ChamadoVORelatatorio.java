/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *13/11/2013
 * vpessoa
 * 08:29:42
 */

package br.com.ggas.atendimento.chamado.dominio;

import java.util.List;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVORelatorio;

/**
 * ChamadoVORelatatorio tem como objetivo carregar informações que visualizadas no relatório.
 */
public class ChamadoVORelatatorio {

	private String numeroProtocolo;

	private String nomeSolicitante;

	private String cpfCnpjSolicitante;

	private String tipoChamado;

	private String assuntoChamado;

	private String canalAtendimento;

	private String unidadeOrganizacional;

	private String responsavel;

	private String descricao;

	private String nomeCliente;

	private String nomeFantasiaCliente;

	private String cpfCnpjCliente;

	private String passaporteCliente;

	private String numeroContrato;

	private String matriculaImovel;

	private String nomeFantasiaImovel;

	private String cepImovel;

	private String enderecoImovel;

	private String descricaoPontoConsumo;

	private boolean comAutorizacaoServico;

	private List<ServicoAutorizacaoVORelatorio> servicoAutorizacaoList;

	private List<ChamadoHistoricoVO> chamadoHistoricoList;

	private String data;

	private String usuario;

	private String motivo;

	private String operacao;

	private String situacao;
	
	private String horaAcionamento;

	public String getNumeroProtocolo() {

		return numeroProtocolo;
	}

	public void setNumeroProtocolo(String numeroProtocolo) {

		this.numeroProtocolo = numeroProtocolo;
	}

	public String getNomeSolicitante() {

		return nomeSolicitante;
	}

	public void setNomeSolicitante(String nomeSolicitante) {

		this.nomeSolicitante = nomeSolicitante;
	}

	public String getCpfCnpjSolicitante() {

		return cpfCnpjSolicitante;
	}

	public void setCpfCnpjSolicitante(String cpfCnpjSolicitante) {

		this.cpfCnpjSolicitante = cpfCnpjSolicitante;
	}

	public String getTipoChamado() {

		return tipoChamado;
	}

	public void setTipoChamado(String tipoChamado) {

		this.tipoChamado = tipoChamado;
	}

	public String getAssuntoChamado() {

		return assuntoChamado;
	}

	public void setAssuntoChamado(String assuntoChamado) {

		this.assuntoChamado = assuntoChamado;
	}

	public String getCanalAtendimento() {

		return canalAtendimento;
	}

	public void setCanalAtendimento(String canalAtendimento) {

		this.canalAtendimento = canalAtendimento;
	}

	public String getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	public void setUnidadeOrganizacional(String unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	public String getResponsavel() {

		return responsavel;
	}

	public void setResponsavel(String responsavel) {

		this.responsavel = responsavel;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getNomeFantasiaCliente() {

		return nomeFantasiaCliente;
	}

	public void setNomeFantasiaCliente(String nomeFantasiaCliente) {

		this.nomeFantasiaCliente = nomeFantasiaCliente;
	}

	public String getCpfCnpjCliente() {

		return cpfCnpjCliente;
	}

	public void setCpfCnpjCliente(String cpfCnpjCliente) {

		this.cpfCnpjCliente = cpfCnpjCliente;
	}

	public String getPassaporteCliente() {

		return passaporteCliente;
	}

	public void setPassaporteCliente(String passaporteCliente) {

		this.passaporteCliente = passaporteCliente;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getMatriculaImovel() {

		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {

		this.matriculaImovel = matriculaImovel;
	}

	public String getNomeFantasiaImovel() {

		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {

		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getCepImovel() {

		return cepImovel;
	}

	public void setCepImovel(String cepImovel) {

		this.cepImovel = cepImovel;
	}

	public String getEnderecoImovel() {

		return enderecoImovel;
	}

	public void setEnderecoImovel(String enderecoImovel) {

		this.enderecoImovel = enderecoImovel;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public List<ChamadoHistoricoVO> getChamadoHistoricoList() {

		return chamadoHistoricoList;
	}

	public void setChamadoHistoricoList(List<ChamadoHistoricoVO> chamadoHistoricoList) {

		this.chamadoHistoricoList = chamadoHistoricoList;
	}

	public boolean getComAutorizacaoServico() {
		return comAutorizacaoServico;
	}

	public void setComAutorizacaoServico(boolean comAutorizacaoServico) {
		this.comAutorizacaoServico = comAutorizacaoServico;
	}

	public List<ServicoAutorizacaoVORelatorio> getServicoAutorizacaoList() {
		return servicoAutorizacaoList;
	}

	public void setServicoAutorizacaoList(List<ServicoAutorizacaoVORelatorio> servicoAutorizacaoList) {
		this.servicoAutorizacaoList = servicoAutorizacaoList;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getHoraAcionamento() {
		return horaAcionamento;
	}

	public void setHoraAcionamento(String horaAcionamento) {
		this.horaAcionamento = horaAcionamento;
	}
	
}
