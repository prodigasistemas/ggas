/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/01/2014 16:43:25
 @author ifrancisco
 */

package br.com.ggas.atendimento.chamado.dominio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.DadosExtratoDebito;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;

/**
 * ClienteVO
 */
public class ClienteVO {

	private Cliente cliente;

	private Equipe equipe;

	private List<PontoConsumoVO> listaPontoConsumoVO;
	
	private String nomeCliente;
	
	private String nome;
	
	private String docIdentificador;
	
	private String endereco;
	
	private String codigoCliente;
	
	private String inscricaoEstadual;
	
	private String tipoCliente;
	
	private Collection<DadosExtratoDebito> listaDadosExtratoDebito = new ArrayList<>();
	
	private Integer quantidadeFaturasVencidas;
	
	private BigDecimal valorTotalFaturasVencidas;
	
	
	
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	/**
	 * @return listaDadosExtratoDebito
	 */
	public Collection<DadosExtratoDebito> getListaDadosExtratoDebito() {
	
		return listaDadosExtratoDebito;
	}

	
	/**
	 * @param listaDadosExtratoDebito
	 */
	public void setListaDadosExtratoDebito(Collection<DadosExtratoDebito> listaDadosExtratoDebito) {
	
		this.listaDadosExtratoDebito = listaDadosExtratoDebito;
	}

	/**
	 * @return nomeCliente
	 */
	public String getNomeCliente() {
	
		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}
	
	/**
	 * @return docIdentificador
	 */
	public String getDocIdentificador() {
	
		return docIdentificador;
	}
	
	/**
	 * @param docIdentificador
	 */
	public void setDocIdentificador(String docIdentificador) {
	
		this.docIdentificador = docIdentificador;
	}
	
	/**
	 * @return endereco
	 */
	public String getEndereco() {
	
		return endereco;
	}
	
	/**
	 * @param endereco
	 */
	public void setEndereco(String endereco) {
	
		this.endereco = endereco;
	}
	
	/**
	 * @return codigoCliente
	 */
	public String getCodigoCliente() {
	
		return codigoCliente;
	}
	
	/**
	 * @param codigoCliente
	 */
	public void setCodigoCliente(String codigoCliente) {
	
		this.codigoCliente = codigoCliente;
	}

	/**
	 * @return inscricaoEstadual
	 */
	public String getInscricaoEstadual() {
	
		return inscricaoEstadual;
	}

	/**
	 * @param inscricaoEstadual
	 */
	public void setInscricaoEstadual(String inscricaoEstadual) {
	
		this.inscricaoEstadual = inscricaoEstadual;
	}

	/**
	 * Retorna tipoCliente
	 * 
	 * @return tipoCliente
	 */
	public String getTipoCliente() {
	
		return tipoCliente;
	}
	
	/**
	 * Atribui tipoCliente
	 * 
	 * @param tipoCliente
	 */
	public void setTipoCliente(String tipoCliente) {
	
		this.tipoCliente = tipoCliente;
	}

	/**
	 * Retorna cliente
	 * 
	 * @return the cliente
	 */
	public Cliente getCliente() {

		return cliente;
	}

	/**
	 * Atribui cliente
	 * 
	 * @param clente the clente to set
	 */
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return the listaPontoConsumoVO
	 */
	public List<PontoConsumoVO> getListaPontoConsumoVO() {

		return listaPontoConsumoVO;
	}

	/**
	 * @param listaPontoConsumoVO
	 *            the listaPontoConsumoVO to set
	 */
	public void setListaPontoConsumoVO(List<PontoConsumoVO> listaPontoConsumoVO) {

		this.listaPontoConsumoVO = listaPontoConsumoVO;
	}

	/**
	 * Retorna equipe
	 * 
	 * @return equipe
	 */
	public Equipe getEquipe() {

		return equipe;
	}

	/**
	 * Atribui equipe
	 * 
	 * @param equipe
	 */
	public void setEquipe(Equipe equipe) {

		this.equipe = equipe;
	}


	public Integer getQuantidadeFaturasVencidas() {
		return quantidadeFaturasVencidas;
	}


	public void setQuantidadeFaturasVencidas(Integer quantidadeFaturasVencidas) {
		this.quantidadeFaturasVencidas = quantidadeFaturasVencidas;
	}


	public BigDecimal getValorTotalFaturasVencidas() {
		return valorTotalFaturasVencidas;
	}


	public void setValorTotalFaturasVencidas(BigDecimal valorTotalFaturasVencidas) {
		this.valorTotalFaturasVencidas = valorTotalFaturasVencidas;
	}

}
