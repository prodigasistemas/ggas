/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *16/10/2013
 * vpessoa
 * 20:57:04
 */

package br.com.ggas.atendimento.chamado.negocio;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.atendimento.chamado.apresentacao.encerramentoemlote.ResultadoEncerramentoEmLote;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoAlteracaoVencimento;
import br.com.ggas.atendimento.chamado.dominio.ChamadoEmail;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistoricoAnexo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoTituloAnalise;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendasVO;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoCliente;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ChamadoResumoVO;
import br.com.ggas.web.faturamento.fatura.TitulosGSAVO;

/**
 * ControladorChamado
 */
public interface ControladorChamado {

	/**
	 * Obter chamado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the chamado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Chamado obterChamado(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param rascunho
	 *            the rascunho
	 * @param numeroProtocolo
	 *            the numero protocolo
	 * @param dados
	 *            the dados
	 * @param listaChamadoContato
	 *            the lista chamado contato
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void inserirChamado(Chamado chamado, Boolean rascunho, Long numeroProtocolo, Map<String, Object> dados,
					List<ContatoCliente> listaChamadoContato) throws GGASException;

	/**
	 * Gerar protocolo.
	 * 
	 * @param numeroProtocoloManual
	 *            the numero protocolo manual
	 * @return the chamado protocolo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ChamadoProtocolo gerarProtocolo(Long numeroProtocoloManual) throws NegocioException;

	/**
	 * Consultar chamado.
	 * 
	 * @param chamadoVO
	 *            the chamado vo
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<Chamado> consultarChamado(ChamadoVO chamadoVO, Boolean isRelatorio) throws GGASException;

	/**
	 * Atualizar chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param listaChamadoContato
	 *            the lista chamado contato
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarChamado(Chamado chamado, List<ContatoCliente> listaChamadoContato) throws GGASException;

	/**
	 * Atualizar chamado sem validar.
	 * 
	 * @param chamado
	 *            the chamado
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarChamadoSemValidar(Chamado chamado) throws GGASException;

	/**
	 * Consultar chamado por cliente ou imovel.
	 * 
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Chamado> consultarChamadoPorClienteOuImovel(Long chavePrimariaCliente, Long chavePrimariaImovel,
					Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Listar chamado historico.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param operacao
	 *            the operacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ChamadoHistorico> listarChamadoHistorico(Chamado chamado, EntidadeConteudo operacao) throws NegocioException;

	/**
	 * Obter protocolo.
	 * 
	 * @param chavePrimariaProtocolo
	 *            the chave primaria protocolo
	 * @return the chamado protocolo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ChamadoProtocolo obterProtocolo(Long chavePrimariaProtocolo) throws NegocioException;

	/**
	 * Listar imoveis por cliente.
	 * 
	 * @param chaveCliente
	 *            the chave cliente
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Imovel> listarImoveisPorCliente(Long chaveCliente) throws NegocioException;

	/**
	 * Tramitar chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws GGASException {@link GGASException}
	 */
	void tramitarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Reiterar chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws GGASException {@link GGASException}
	 */
	void reiterarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Reativar chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException {@link GGASException}
	 */
	void reativarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Reabrir chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException {@link GGASException}
	 */
	void reabrirChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Capturar chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException {@link GGASException}
	 */
	void capturarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Encerrar chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void encerrarChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Verificar status chamado.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param tipoOperacao
	 *            the tipo operacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarStatusChamado(Chamado chamado, String tipoOperacao) throws NegocioException;

	/**
	 * Verificar captura.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarCaptura(Chamado chamado, Map<String, Object> dados) throws NegocioException;

	/**
	 * Incluir chamado historico.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param tipoOperacao
	 *            the tipo operacao
	 * @param dados
	 *            the dados
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException {@link GGASException}
	 */
	void incluirChamadoHistorico(Chamado chamado, String tipoOperacao, Map<String, Object> dados) throws GGASException;

	/**
	 * Validar motivo.
	 * 
	 * @param tipoOperacao
	 *            the tipo operacao
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarMotivo(String tipoOperacao, Map<String, Object> dados) throws NegocioException;
	
	/**
	 * Validar dataResolucao.
	 * 
	 * @param tipoOperacao
	 *            the tipo operacao
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	//
	void validarDataResolucao(String tipoOperacao, Map<String, Object> dados, Chamado chamado) throws NegocioException;

	/**
	 * Validar usuario abertura chamado.
	 * 
	 * @param usuario
	 *            the usuario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarUsuarioAberturaChamado(Usuario usuario) throws NegocioException;

	/**
	 * Gerar autorizacao servico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param chavesPrimariasTipoServico
	 * 			  Para verificar se o serviço foi autorizado
	 * @param dados
	 *            the dados
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException 
	 */
	Collection<ServicoAutorizacao> gerarAutorizacaoServico(Long[] chavePrimaria, Long[] chavesPrimariasTipoServico,
			Map<String, Object> dados) throws ParseException, GGASException, IOException;

	/**
	 * Gerar chamado perquisa satisfacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param dados
	 *            the dados
	 * @return the chamado
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException {@link GGASException}
	 */
	Chamado gerarChamadoPerquisaSatisfacao(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados) throws GGASException;

	/**
	 * Existe chamado questionario.
	 * 
	 * @param questionario
	 *            the questionario
	 * @return true, if successful
	 */
	boolean existeChamadoQuestionario(Questionario questionario);

	/**
	 * Obter chamado tipo pesquisa satisfacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the chamado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Chamado obterChamadoTipoPesquisaSatisfacao(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter chamado alteracao vencimento.
	 * 
	 * @param idChamado
	 *            the id chamado
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the chamado alteracao vencimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ChamadoAlteracaoVencimento obterChamadoAlteracaoVencimento(Long idChamado, Long idPontoConsumo) throws NegocioException;

	/**
	 * Inserir chamado alteracao vencimento.
	 * 
	 * @param chamadoAlteracaoVencimento {@link ChamadoAlteracaoVencimento}
	 *            the chamado alteracao vencimento
	 * @throws GGASException {@link GGASException}
	 */
	void inserirChamadoAlteracaoVencimento(ChamadoAlteracaoVencimento chamadoAlteracaoVencimento) throws GGASException;

	/**
	 * Registrar tentativa contato.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void registrarTentativaContato(Chamado chamado, Map<String, Object> dados) throws NegocioException;

	/**
	 * Validar autorizacao servico aberta.
	 * 
	 * @param chamado {@link Chamado}
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException {@link GGASException}
	 */
	void validarAutorizacaoServicoAberta(Chamado chamado) throws GGASException;

	/**
	 * Inserir chamado alteracao cliente.
	 * 
	 * @param chamadoAlteracaoCliente {@link ChamadoAlteracaoCliente}
	 * @throws GGASException {@link GGASException}
	 */
	void inserirChamadoAlteracaoCliente(ChamadoAlteracaoCliente chamadoAlteracaoCliente) throws GGASException;

	/**
	 * Verificar chamado aberto por chamado assunto.
	 * 
	 * @param assunto
	 *            the assunto
	 * @param numeroContrato
	 *            the numero contrato
	 * @return the long
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Long verificarChamadoAbertoPorChamadoAssunto(String assunto, Integer numeroContrato) throws GGASException;

	/**
	 * Verificar chamado aberto por chamado assunto ponto consumo.
	 * 
	 * @param assunto - {@link String}
	 * @param numeroContrato - {@link Integer}
	 * @param pontoConsumo - {@link Long}
	 * @return the long - {@link Long}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Long verificarChamadoAbertoPorChamadoAssuntoPontoConsumo(String assunto, Integer numeroContrato, Long pontoConsumo)
			throws NegocioException;

	/**
	 * Obter data previsao.
	 * 
	 * @param chamadoAssunto
	 *            the chamado assunto
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Date obterDataPrevisao(ChamadoAssunto chamadoAssunto) throws NegocioException;

	/**
	 * Obter chamado historico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the chamado historico
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ChamadoHistorico obterChamadoHistorico(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir chamado email.
	 * 
	 * @param listaChamadoEmail
	 *            the lista chamado email
	 * @param chamado
	 *            the chamado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirChamadoEmail(List<ChamadoEmail> listaChamadoEmail, Chamado chamado) throws NegocioException;

	/**
	 * Listar chamado email.
	 * 
	 * @param chamado
	 *            the chamado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ChamadoEmail> listarChamadoEmail(Chamado chamado) throws NegocioException;

	/**
	 * Obter ultimo chamado alteracao vencimento.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the chamado alteracao vencimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ChamadoAlteracaoVencimento obterUltimoChamadoAlteracaoVencimento(Long idPontoConsumo) throws NegocioException;

	/**
	 * Recupera o chamado por assunto, pelo número do contrato e pela chave do ponto de consumo.
	 * 
	 * @param assunto
	 *            the assunto
	 * @param numeroContrato
	 *            the numero contrato
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<Chamado> consultarChamadoAbertoPorChamadoAssuntoPontoConsumo(String assunto, Integer numeroContrato, Long pontoConsumo)
					throws GGASException;

	/**
	 * Verifica se existe Autorização gerada para o chamado.
	 * 
	 * @param chamado {@link Chamado}
	 * @return boolean {@link boolean}
	 */
	public boolean existeServicoAutorizacaoGerada(Chamado chamado);

	/**
	 * Lista chamados atrasados por unidade organizacional
	 * 
	 * @param unidadeOrganizacional {@link UnidadeOrganizacional}
	 * @return lista de chamados {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	Collection<Chamado> listarChamadosAtrasadosPorUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) throws GGASException;

	/**
	 * Valida dados do anexo
	 * 
	 * @param chamadoHistoricoAnexo {@link ChamadoHistoricoAnexo}
	 * @throws NegocioException {@link NegocioException}
	 */
	void validarDadosAnexo(ChamadoHistoricoAnexo chamadoHistoricoAnexo) throws NegocioException;

	/**
	 * Verifica a descricao do anexo
	 * 
	 * @param indexLista {@link Integer}
	 * @param listaAnexo {@link Collection}
	 * @param chamadoHistoricoAnexo {@link ChamadoHistoricoAnexo}
	 * @throws NegocioException {@link NegocioException}
	 */
	public void verificarDescricaoAnexo(Integer indexLista, Collection<ChamadoHistoricoAnexo> listaAnexo,
			ChamadoHistoricoAnexo chamadoHistoricoAnexo)
			throws NegocioException;

	/**
	 * Obtem o chamado historico anexo pela chave primaria
	 * 
	 * @param chavePrimaria {@link Long}
	 * @return ChamadoHistoricoAnexo {@link ChamadoHistoricoAnexo}
	 * @throws NegocioException {@link NegocioException}
	 */
	public ChamadoHistoricoAnexo obterChamadoHistoricoAnexo(Long chavePrimaria) throws NegocioException;

	/**
	 * Gera AS Automatica na criação do Chamado
	 * 
	 * @param chamado {@link Chamado}
	 * @param dados {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	void gerarASAutomatica(Chamado chamado, Map<String, Object> dados) throws GGASException ;

	/**
	 * Realiza a criação de email para chamado em lote
	 * @param chamado chamado com as informações a serem criadas
	 * @param pontosConsumo pontos de consumo a serem criados em lote
	 * @param dados lista de dados para serem criados os chamados
	 * @return retorna os números dos protocolos dos chamados criados
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	List<Long> inserirChamadoEmLote(Chamado chamado, List<Long> pontosConsumo, Map<String, Object> dados) throws GGASException;

	/**
	 * Encerrar vários chamados em lote e notifica por e-mail agrupando pelo chamado assunto
	 *
	 * Os chamados que não forem possível de serem encerrados, serão inseridos em uma lista e retornado pelo método
	 * @param chamados para serem encerrados em lote
	 * @param dados com as informações do usuário e do motivo
	 * @return o resultado com o encerramento em lote (sucessos e falhas)
	 */
	ResultadoEncerramentoEmLote encerrarChamadoEmLote(List<Chamado> chamados, Map<String, Object> dados);
	
	/**
	 * Realiza a montagem e envio do email com o histórico do chamado
	 * @param idChamado  chave primária do chamado
	 * @throws GGASException {@link GGASException}
	 */
	void enviarEmailHistoricoChamado(Long idChamado) throws GGASException;

	/**
	 * Verifica se a Unidade Organizacional tem a permissão para tramitação de chamado
	 *
	 * @param unidadeOrganizacional a unidade a ser verificada
	 * @throws NegocioException caso a unidade não tenha a permissão de tramitação de chamado
	 */
	void verificarPermissaoTramitarUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) throws NegocioException;
	
	/**
	 * Gera AS Automatica na inclusão do Chamado
	 *
	 * @param chamado {@link Chamado}
	 * @param dados {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	void gerarASAutomaticaInclusaoChamado(Chamado chamado, Map<String, Object> dados) throws GGASException;

	/**
	 * Lista os agendamentos de um cliente
	 * @param cliente
	 * 			the cliente
	 * @return retorna lista de Agendamentos do Cliente
	 */
	List<ServicoAutorizacaoAgendasVO> listarAgandamentosPorCliente(Cliente cliente);

	/**
	 * enviarEmai lHistorico Chamado
	 * @param email the email
	 * @param faturaID the faturaID
	 * @param docFiscalID the docFiscalID
	 */
	void enviarEmailHistoricoChamado(String email, Long faturaID, Long docFiscalID);

	void incluirChamadoTituloAnalise(Chamado chamado, Map<String, Object> dados, Long numeroProtocolo) throws NegocioException, ConcorrenciaException;

	void atualizarChamadoTituloAnalise(Collection<TitulosGSAVO> listaTitulosAnaliseSessao,
			Collection<ChamadoTituloAnalise> listaChamadoTituloAnalise, Map<String, Object> dados, Chamado chamadoExistente) throws ConcorrenciaException, NegocioException;
	
	Collection<ChamadoResumoVO> consultarChamadoResumo(ChamadoVO chamadoVO, Boolean isRelatorio, Boolean isGrafico) throws GGASException;

}
