/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 11:13:51
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMotivoEncerramento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.util.DataUtil;

/**
 * Classe responsável pela representação de uma Autorização de Serviço Temp
 *
 */
public class ServicoAutorizacaoTempImpl extends EntidadeNegocioImpl implements ServicoAutorizacaoTemp {

	private static final long serialVersionUID = 8487220708946391134L;

	private String observacao;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataLeituraOriginal;
	private Integer numeroLeituraOriginal;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataLeituraNovo;
	private Integer numeroLeituraNovo;
	private String numeroLacreMedidorNovo1;
	private String numeroLacreMedidorNovo2;
	private String numeroLacreMedidorNovo3;
	private String localizacaoGeografica;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date inicioExecucao;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fimExecucao;
	
	private String medidorNumeroSerieOriginal;
	private String medidorNumeroSerieNovo;
	private String descricaoGrauRisco;
	
	private ServicoAutorizacao servicoAutorizacao;
	private EntidadeConteudo status;
	private Funcionario funcionario;

	private OperacaoMedidor medidorOperacao;
	private MotivoOperacaoMedidor motivoOperacaoMedidor;
	private Medidor medidorOriginal;
	private Medidor medidorNovo;
	private MedidorLocalInstalacao medidorLocalInstalacao;
	private MedidorProtecao medidorProtecao;
	private FaixaPressaoFornecimento faixaPressaoFornecimento;
	private ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento;
	private EntidadeConteudo ocorrencia;
	private EntidadeConteudo localRede;
	private EntidadeConteudo causaAtendimento;
	private EntidadeConteudo solucaoAtendimento;
	private String horaAtendimento;
	private String identificacao;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date coletaDataHora;
	private Double coletaPressao;
	private String coletaLacre;
	private EntidadeConteudo cilindro1;
	private EntidadeConteudo cilindro2;
	private EntidadeConteudo cilindro3;
	private EntidadeConteudo cilindro4;

	private EntidadeConteudo anormalidade;

	private EntidadeConteudo localInstalacao;

	private BigDecimal vazaoFornecimentoNova;

	private BigDecimal pressaoFornecimentoNova;
	
	@Override
	public String getObservacao() {
		return observacao;
	}
	
	@Override
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Override
	public Date getDataLeituraOriginal() {
		return dataLeituraOriginal;
	}
	
	@Override
	public void setDataLeituraOriginal(Date dataLeituraOriginal) {
		this.dataLeituraOriginal = dataLeituraOriginal;
	}
	
	@Override
	public Integer getNumeroLeituraOriginal() {
		return numeroLeituraOriginal;
	}
	
	@Override
	public void setNumeroLeituraOriginal(Integer numeroLeituraOriginal) {
		this.numeroLeituraOriginal = numeroLeituraOriginal;
	}
	
	@Override
	public Date getDataLeituraNovo() {
		return dataLeituraNovo;
	}
	
	@Override
	public void setDataLeituraNovo(Date dataLeituraNovo) {
		this.dataLeituraNovo = dataLeituraNovo;
	}
	
	@Override
	public Integer getNumeroLeituraNovo() {
		return numeroLeituraNovo;
	}
	
	@Override
	public void setNumeroLeituraNovo(Integer numeroLeituraNovo) {
		this.numeroLeituraNovo = numeroLeituraNovo;
	}
	
	@Override
	public String getNumeroLacreMedidorNovo1() {
		return numeroLacreMedidorNovo1;
	}
	
	@Override
	public void setNumeroLacreMedidorNovo1(String numeroLacreMedidorNovo1) {
		this.numeroLacreMedidorNovo1 = numeroLacreMedidorNovo1;
	}
	
	@Override
	public String getNumeroLacreMedidorNovo2() {
		return numeroLacreMedidorNovo2;
	}
	
	@Override
	public void setNumeroLacreMedidorNovo2(String numeroLacreMedidorNovo2) {
		this.numeroLacreMedidorNovo2 = numeroLacreMedidorNovo2;
	}
	
	@Override
	public String getNumeroLacreMedidorNovo3() {
		return numeroLacreMedidorNovo3;
	}
	
	@Override
	public void setNumeroLacreMedidorNovo3(String numeroLacreMedidorNovo3) {
		this.numeroLacreMedidorNovo3 = numeroLacreMedidorNovo3;
	}
	
	@Override
	public String getLocalizacaoGeografica() {
		return localizacaoGeografica;
	}
	
	@Override
	public void setLocalizacaoGeografica(String localizacaoGeografica) {
		this.localizacaoGeografica = localizacaoGeografica;
	}
	
	@Override
	public Date getInicioExecucao() {
		return inicioExecucao;
	}
	
	@Override
	public void setInicioExecucao(Date inicioExecucao) {
		this.inicioExecucao = inicioExecucao;
	}
	
	@Override
	public Date getFimExecucao() {
		return fimExecucao;
	}
	
	@Override
	public void setFimExecucao(Date fimExecucao) {
		this.fimExecucao = fimExecucao;
	}
	
	@Override
	public String getMedidorNumeroSerieOriginal() {
		return medidorNumeroSerieOriginal;
	}
	
	@Override
	public void setMedidorNumeroSerieOriginal(String medidorNumeroSerieOriginal) {
		this.medidorNumeroSerieOriginal = medidorNumeroSerieOriginal;
	}
	
	@Override
	public String getMedidorNumeroSerieNovo() {
		return medidorNumeroSerieNovo;
	}
	
	@Override
	public void setMedidorNumeroSerieNovo(String medidorNumeroSerieNovo) {
		this.medidorNumeroSerieNovo = medidorNumeroSerieNovo;
	}
	
	@Override
	public String getDescricaoGrauRisco() {
		return descricaoGrauRisco;
	}
	
	@Override
	public void setDescricaoGrauRisco(String descricaoGrauRisco) {
		this.descricaoGrauRisco = descricaoGrauRisco;
	}
	
	@Override
	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}
	
	@Override
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
	}
	
	@Override
	public EntidadeConteudo getStatus() {
		return status;
	}
	
	@Override
	public void setStatus(EntidadeConteudo status) {
		this.status = status;
	}
	
	@Override
	public Funcionario getFuncionario() {
		return funcionario;
	}
	
	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	@Override
	public OperacaoMedidor getMedidorOperacao() {
		return medidorOperacao;
	}
	
	@Override
	public void setMedidorOperacao(OperacaoMedidor medidorOperacao) {
		this.medidorOperacao = medidorOperacao;
	}
	
	@Override
	public MotivoOperacaoMedidor getMotivoOperacaoMedidor() {
		return motivoOperacaoMedidor;
	}
	
	@Override
	public void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor) {
		this.motivoOperacaoMedidor = motivoOperacaoMedidor;
	}
	
	@Override
	public Medidor getMedidorOriginal() {
		return medidorOriginal;
	}
	
	@Override
	public void setMedidorOriginal(Medidor medidorOriginal) {
		this.medidorOriginal = medidorOriginal;
	}
	
	@Override
	public Medidor getMedidorNovo() {
		return medidorNovo;
	}
	
	@Override
	public void setMedidorNovo(Medidor medidorNovo) {
		this.medidorNovo = medidorNovo;
	}
	
	@Override
	public MedidorLocalInstalacao getMedidorLocalInstalacao() {
		return medidorLocalInstalacao;
	}
	
	@Override
	public void setMedidorLocalInstalacao(MedidorLocalInstalacao medidorLocalInstalacao) {
		this.medidorLocalInstalacao = medidorLocalInstalacao;
	}
	
	@Override
	public MedidorProtecao getMedidorProtecao() {
		return medidorProtecao;
	}
	
	@Override
	public void setMedidorProtecao(MedidorProtecao medidorProtecao) {
		this.medidorProtecao = medidorProtecao;
	}
	
	@Override
	public FaixaPressaoFornecimento getFaixaPressaoFornecimento() {
		return faixaPressaoFornecimento;
	}
	
	@Override
	public void setFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) {
		this.faixaPressaoFornecimento = faixaPressaoFornecimento;
	}
	
	@Override
	public ServicoAutorizacaoMotivoEncerramento getServicoAutorizacaoMotivoEncerramento() {
		return servicoAutorizacaoMotivoEncerramento;
	}
	
	@Override
	public void setServicoAutorizacaoMotivoEncerramento(
			ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento) {
		this.servicoAutorizacaoMotivoEncerramento = servicoAutorizacaoMotivoEncerramento;
	}
	
	@Override
	public EntidadeConteudo getOcorrencia() {
		return ocorrencia;
	}
	
	@Override
	public void setOcorrencia(EntidadeConteudo ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	
	@Override
	public EntidadeConteudo getLocalRede() {
		return localRede;
	}
	
	@Override
	public void setLocalRede(EntidadeConteudo localRede) {
		this.localRede = localRede;
	}
	
	@Override
	public EntidadeConteudo getCausaAtendimento() {
		return causaAtendimento;
	}
	
	@Override
	public void setCausaAtendimento(EntidadeConteudo causaAtendimento) {
		this.causaAtendimento = causaAtendimento;
	}
	
	@Override
	public EntidadeConteudo getSolucaoAtendimento() {
		return solucaoAtendimento;
	}
	
	@Override
	public void setSolucaoAtendimento(EntidadeConteudo solucaoAtendimento) {
		this.solucaoAtendimento = solucaoAtendimento;
	}


	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getDataExecucaoFormatada() {

		return DataUtil.converterDataParaString(this.inicioExecucao, Boolean.TRUE);
	}

	@Override
	public String getHoraAtendimento() {
		return horaAtendimento;
	}

	@Override
	public void setHoraAtendimento(String horaAtendimento) {
		this.horaAtendimento = horaAtendimento;
	}

	@Override
	public String getIdentificacao() {
		return this.identificacao;
	}

	@Override
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	@Override
	public Date getColetaDataHora() {
		return coletaDataHora;
	}

	@Override
	public void setColetaDataHora(Date coletaDataHora) {
		this.coletaDataHora = coletaDataHora;
	}

	@Override
	public Double getColetaPressao() {
		return coletaPressao;
	}

	@Override
	public void setColetaPressao(Double coletaPressao) {
		this.coletaPressao = coletaPressao;
	}

	@Override
	public String getColetaLacre() {
		return coletaLacre;
	}

	@Override
	public void setColetaLacre(String coletaLacre) {
		this.coletaLacre = coletaLacre;
		
	}

	@Override
	public EntidadeConteudo getCilindro1() {
		return cilindro1;
	}

	@Override
	public void setCilindro1(EntidadeConteudo cilindro1) {
		this.cilindro1 = cilindro1;
	}

	@Override
	public EntidadeConteudo getCilindro2() {
		return cilindro2;
	}

	@Override
	public void setCilindro2(EntidadeConteudo cilindro2) {
		this.cilindro2 = cilindro2;
	}

	@Override
	public EntidadeConteudo getCilindro3() {
		return cilindro3;
	}

	@Override
	public void setCilindro3(EntidadeConteudo cilindro3) {
		this.cilindro3 = cilindro3;
	}

	@Override
	public EntidadeConteudo getCilindro4() {
		return cilindro4;
	}

	@Override
	public void setCilindro4(EntidadeConteudo cilindro4) {
		this.cilindro4 = cilindro4;
	}

	@Override
	public void setLocalInstalacao(EntidadeConteudo localInstalacao) {
		this.localInstalacao = localInstalacao;
		
	}

	@Override
	public EntidadeConteudo getLocalInstalacao() {
		return this.localInstalacao;
	}

	@Override
	public EntidadeConteudo getAnormalidade() {
		return this.anormalidade;
	}

	@Override
	public void setAnormalidade(EntidadeConteudo anormalidade) {
		this.anormalidade = anormalidade;
		
	}

	public boolean isValidoParaAtualizacao() {
		return possuiNovoMedidor() || possuiAtualizacaoContratual();
	}
	
	public boolean possuiNovoMedidor() {
		return this.medidorNovo != null;
	}
	
	public boolean possuiAtualizacaoContratual() {
		return this.pressaoFornecimentoNova != null || this.vazaoFornecimentoNova != null;
	}

	@Override
	public void setPressaoFornecimentoNova(BigDecimal pressaoFornecimentoNova) {
		this.pressaoFornecimentoNova = pressaoFornecimentoNova;
		
	}

	@Override
	public BigDecimal getPressaoFornecimentoNova() {
		return this.pressaoFornecimentoNova;
	}

	@Override
	public void setVazaoFornecimentoNova(BigDecimal vazaoFornecimentoNova) {
		this.vazaoFornecimentoNova = vazaoFornecimentoNova;
		
	}

	@Override
	public BigDecimal getVazaoFornecimentoNova() {
		return this.vazaoFornecimentoNova;
	}
}
