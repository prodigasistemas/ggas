/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 14:37:42
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Map;

import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável por representar a relação entre autorização de serviço e material
 */
public class ServicoAutorizacaoMaterial extends EntidadeNegocioImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8814549850828379124L;

	private ServicoAutorizacao servicoAutorizacao;

	private Material material;

	private Integer quantidadeMaterial;

	/**
	 * @return servicoAutorizacao
	 */
	public ServicoAutorizacao getServicoAutorizacao() {

		return servicoAutorizacao;
	}

	/**
	 * @param servicoAutorizacao
	 */
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {

		this.servicoAutorizacao = servicoAutorizacao;
	}

	/**
	 * @return material
	 */
	public Material getMaterial() {

		return material;
	}

	/**
	 * @param material
	 */
	public void setMaterial(Material material) {

		this.material = material;
	}

	/**
	 * @return quantidadeMaterial
	 */
	public Integer getQuantidadeMaterial() {

		return quantidadeMaterial;
	}

	/**
	 * @param quantidadeMaterial
	 */
	public void setQuantidadeMaterial(Integer quantidadeMaterial) {

		this.quantidadeMaterial = quantidadeMaterial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
