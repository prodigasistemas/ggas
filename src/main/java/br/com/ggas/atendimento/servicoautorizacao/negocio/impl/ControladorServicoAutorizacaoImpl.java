/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 19/11/2013 11:11:34
 * @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.negocio.impl;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.mail.util.ByteArrayDataSource;
import javax.swing.ImageIcon;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.impl.TipoDocumentoImpl;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVORelatatorio;
import br.com.ggas.atendimento.chamado.dominio.FiltrosRelatorioHelper;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.repositorio.RepositorioEquipe;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.atendimento.exception.AutorizacaoServicoImovelCicloLeituraException;
import br.com.ggas.atendimento.exception.AutorizacaoServicoImovelServicoTipoRestritoException;
import br.com.ggas.atendimento.material.dominio.MaterialVO;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.resposta.negocio.ControladorResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AcaoServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AutorizacaoServicoLoteVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.EquipeAlgasVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.FiltroServicoAutorizacaoLoteDTO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndicadorEmergenciaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndiceVazamentoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendasVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoEquipamento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistoricoAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistoricoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMaterial;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMotivoEncerramento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTempAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVORelatorio;
import br.com.ggas.atendimento.servicoautorizacao.dominio.impl.ServicoAutorizacaoRegistroLocalImpl;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.repositorio.RepositorioServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.apresentacao.RelatorioServicoTipoHelper;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipo.repositorio.RepositorioServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.batch.acao.negocio.ControladorAcao;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoEstendida;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoPontoConsumo;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.batch.acaocomando.repositorio.RepositorioAcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.exception.AutorizacaoServicoRepetidaException;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.faturamento.impl.FaturaItemImpl;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.impl.EntidadeConteudoVO;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorio;
import br.com.ggas.relatorio.RedeComprimentoVO;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.relatorio.ServicoAutorizacaoResumoVO;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.AjaxService;
import br.com.ggas.web.faturamento.fatura.FaturaVO;
import br.com.ggas.web.relatorio.atendimento.ControleServicoAutorizacaoVO;
import br.com.ggas.web.relatorio.atendimento.PesquisaRelatorioControleServicoAutorizacaoVO;
import br.com.ggas.web.relatorio.atendimento.RelatorioControleServicoAutorizacaoWrapper;
import br.com.ggas.webservice.ClienteFoneTO;

/**
 * Classe responsável pelas ações relacionadas a uma autorização de serviço
 */
/**
 * @author leonardo.consenso
 *
 */
@Service("controladorServicoAutorizacao")
@Transactional
public class ControladorServicoAutorizacaoImpl implements ControladorServicoAutorizacao {

	private static final String IMAGEM = "imagem";

	private static final String SERVICO_AUTORIZACAO = "servicoAutorizacao";

	private static final String DATA_SOLICITACAO_RAMAL = "dataSolicitacaoRamal";

	private static final String ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE = "ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE";

	private static final int HORAS_DIA = 24;

	private static final int LIMITE_CAMPO = 2;

	private static final int DIAS_ADICIONAR = 10;

	private static final int OPERACAO_CHAVE = 443;

	private static final String PADRAO_FORMATO_DATA = "dd/MM/yyyy";
	
	private static final String PADRAO_FORMATO_DATA_COM_HORA = "dd/MM/yyyy HH:mm";

	private static final Logger LOG = Logger.getLogger(ControladorServicoAutorizacaoImpl.class);

	private static String RELATORIO_SERVICO_AUTORIZACAO = "relatorioAutorizacaoServico.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_S_QUESTIONARIO = "relatorioAutorizacaoServicoSQuestionario.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_BAHIAGAS = "relatorioAutorizacaoServicoBahiaGas.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_BAHIAGAS_GEOPE = "relatorioAutorizacaoServicoBahiaGasOperacoes.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_OPERACOES = "relatorioAutorizacaoServicoOperacoes.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_AGENDAMENTO = "relatorioAutorizacaoServicoAgendamento.jasper";

	private static String RELATORIO_CONTROLE_AUTORIZACAO_SERVICO = "relatorioControleServicoAutorizacao.jasper";
	
	private static String RELATORIO_SERVICO_AUTORIZACAO_ALGAS = "relatorioAutorizacaoServicoAlgas.jasper";
	
	private static String RELATORIO_SERVICO_AUTORIZACAO_INICIO_FORNECIMENTO_ALGAS = "relatorioASInicioFornecimentoAlgas.jasper";
	
	private static String RELATORIO_SERVICO_AUTORIZACAO_ATENDIMENTO_ALGAS = "relatorioAutorizacaoServicoAlgasAtendimento.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_ADEQUACAO_ALGAS = "relatorioAutorizacaoServicoAlgasAdequacao.jasper";
	
	private static String RELATORIO_SERVICO_AUTORIZACAO_INSPECAO_LOCAL_ALGAS = "relatorioAutorizacaoInspecaoLocal.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_DESLIGAMENTO_RIF_ALGAS = "relatorioAutorizacaoDesligamentoRIFAlgas.jasper";
	
	private static String RELATORIO_SERVICO_AUTORIZACAO_INSTALACAO_EQUIPAMENTO_ALGAS = "relatorioAutorizacaoServicoInstalacaoEquipamentoAlgas.jasper";

	private static String RELATORIO_SERVICO_AUTORIZACAO_COLETA_GAS = "relatorioAutorizacaoServicoColetaGas.jasper";
	
	private static String RELATORIO_TEMPO_ATENDIMENTO_EMERGENCIA = "relatorioTempoAtendimentoEmergencia.jasper";
	
	private static String RELATORIO_NUMERO_VAZAMENTOS = "relatorioNumeroVazamentos.jasper";

	private static final String LISTA_PERGUNTAS = "listaPerguntas";
	
	private static final String USUARIO = "usuario";

	private static final String DATA = "data";

	private ControladorParametroSistema parametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
			.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

	@Autowired
	private RepositorioServicoAutorizacao repositorioServicoAutorizacao;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	private ControladorResposta controladorResposta;

	@Autowired
	private RepositorioEquipe repositorioEquipe;
	
	@Autowired
	private RepositorioServicoTipo repositorioServicoTipo;

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(value = "controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier(value = "controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	private ControladorQuestionario controladorQuestionario;

	@Autowired
	@Qualifier(value = "controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier("controladorFeriado")
	private ControladorFeriado controladorFeriado;

	@Autowired
	@Qualifier("controladorEmpresa")
	private ControladorEmpresa controladorEmpresas;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	@Qualifier("controladorContrato")
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorChamado controladorChamado;

	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	@Autowired
	@Qualifier("controladorCreditoDebito")
	private ControladorCreditoDebito controladorCreditoDebito;

	@Autowired
	@Qualifier("controladorCobranca")
	private ControladorCobranca controladorCobranca;

	@Autowired
	@Qualifier("controladorTipoDocumento")
	private ControladorTipoDocumento controladorTipoDocumento;

	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;

	@Autowired
	@Qualifier("controladorLeituraMovimento")
	private ControladorLeituraMovimento controladorLeituraMovimento;
	
	@Autowired
	@Qualifier("controladorEntregaDocumento")
	private ControladorEntregaDocumento controladorEntregaDocumento;
	
	@Autowired
	@Qualifier("controladorAvisoCorte")
	private ControladorAvisoCorte controladorAvisoCorte;

	@Autowired
	@Qualifier(value = "controladorProcesso")
	private ControladorProcesso controladorProcesso;
	
	@Autowired
	@Qualifier(value = "controladorAcesso")
	private ControladorAcesso controladorAcesso;
	
	@Autowired
	@Qualifier(value = "controladorAcao")
	private ControladorAcao controladorAcao;
	
	@Autowired
	@Qualifier(value = "controladorAcaoComando")
	private ControladorAcaoComando controladorAcaoComando;
	
	@Autowired
	private RepositorioAcaoComando repositorioAcaoComando;
	
	@Autowired
	@Qualifier(value = "controladorHistoricoMedicao")
	private ControladorHistoricoMedicao controladorHistoricoMedicao;
	
	@Autowired
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	private ControladorRelatorio controladorRelatorio;

	public Class<?> getClasseEntidadeServicoAutorizacaoTemp() {
		return ServiceLocator.getInstancia().getClassPorID(ServicoAutorizacaoTemp.BEAN_ID_SERVICO_AUTORIZACAO_TEMP);

	}
	
	public Class<?> getClasseEntidadeServicoAutorizacaoTempAnexo() {
		return ServiceLocator.getInstancia().getClassPorID(ServicoAutorizacaoTempAnexo.BEAN_ID_SERVICO_AUTORIZACAO_TEMP_ANEXO);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#listarServicoAutorizacao(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacaoVO, boolean)
	 */
	@Override
	@Transactional
	@ExceptionHandler
	public Collection<ServicoAutorizacao> listarServicoAutorizacao(ServicoAutorizacaoVO servicoAutorizacaoVO,
			boolean pesquisaPopupAgendamento) throws GGASException {

		if (pesquisaPopupAgendamento) {
			ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ABERTO);
			Long chavePrimariaStatus = Long.valueOf(constanteSistema.getValor());
			EntidadeConteudo statusAberto = controladorEntidadeConteudo.obterEntidadeConteudo(chavePrimariaStatus);
			servicoAutorizacaoVO.setListaStatus(new ArrayList<String>());
			servicoAutorizacaoVO.getListaStatus().add(statusAberto.getDescricao());
			servicoAutorizacaoVO.setServicoAgendado(Boolean.FALSE);
			servicoAutorizacaoVO.setIsChamadoPendente(Boolean.TRUE);
			servicoAutorizacaoVO.setPodeSerAgendado(Boolean.TRUE);

		}

		String cepImovel = servicoAutorizacaoVO.getCepImovel();
		String numeroImovel = servicoAutorizacaoVO.getNumeroImovel();
		String descricaoComplemento = servicoAutorizacaoVO.getDescricaoComplemento();
		String nomeImovel = servicoAutorizacaoVO.getNomeImovel();
		Long matriculaImovel = servicoAutorizacaoVO.getMatriculaImovel();
		Boolean condominioImovel = servicoAutorizacaoVO.getCondominioImovel();

		if (StringUtils.isNotEmpty(cepImovel) && StringUtils.isNotEmpty(numeroImovel)
				&& StringUtils.isNotEmpty(descricaoComplemento) && StringUtils.isNotEmpty(nomeImovel)
				&& matriculaImovel != null && condominioImovel != null) {
			verificarImovelPai(servicoAutorizacaoVO);
		}

		return repositorioServicoAutorizacao.consultarServicoAutorizacao(servicoAutorizacaoVO);
	}

	/**
	 * Verificar imovel pai.
	 *
	 * @param servicoAutorizacaoVO the servico autorizacao vo
	 */
	private void verificarImovelPai(ServicoAutorizacaoVO servicoAutorizacaoVO) {

		Collection<Imovel> imoveis = repositorioServicoAutorizacao.verificarImovelCondominio(servicoAutorizacaoVO);
		if (CollectionUtils.isNotEmpty(imoveis)) {
			List<Long> idsImovelPai = new ArrayList<>();
			for (Imovel imovel : imoveis) {
				if (imovel.getCondominio()) {
					idsImovelPai.add(imovel.getChavePrimaria());
				}
			}
			if (!idsImovelPai.isEmpty()) {
				servicoAutorizacaoVO.setIdsCondominioImovel(idsImovelPai);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#inserirServicoAutorizacao(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacao, java.util.Map)
	 */
	@Override
	public ServicoAutorizacao inserirServicoAutorizacao(ServicoAutorizacao servicoAutorizacao,
			Map<String, Object> dados, Boolean isComunicacao) throws GGASException {

		ServicoTipo servicoTipo = servicoAutorizacao.getServicoTipo();
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		Boolean isGerarAsAutomatica = (Boolean) dados.get("isGerarAsPrioritaria");

		ServicoAutorizacaoVO vo = new ServicoAutorizacaoVO();

		if (servicoTipo != null) {
			vo.setServicoTipo(servicoTipo);

			if (pontoConsumo != null && servicoAutorizacao.getImovel() != null) {
				vo.setPontoConsumo(pontoConsumo);

				Imovel imovel = Fachada.getInstancia().buscarImovelPorChave(servicoAutorizacao.getImovel().getChavePrimaria());

				verificarServicoAutorizacaoRepetida(vo);
				
				Boolean isPermiteGerarImovelCiclo = Boolean.FALSE;
				if(dados.get("isPontoCicloLeitura") != null) {
					isPermiteGerarImovelCiclo = Boolean.valueOf((String) dados.get("isPontoCicloLeitura"));
				}
				
				if(!isComunicacao && !isPermiteGerarImovelCiclo) {
					verificarImovelCicloLeitura(servicoTipo, pontoConsumo, imovel);
				}
				verificarServicoTipoRestrito(servicoTipo, imovel);
				verificarNumeroExecucoes(servicoAutorizacao, imovel);
			}

			if(dados.get("dataPrevisaoComunicacao") != null) {
				servicoAutorizacao.setDataPrevisaoEncerramento((Date) dados.get("dataPrevisaoComunicacao"));
			} else {
				String[] dataEncerramento = this.obterPrevisaoEncerramento(servicoTipo.getChavePrimaria());
				servicoAutorizacao.setDataPrevisaoEncerramento(DataUtil.converterParaDataComHora(dataEncerramento[1]));
			}
			if(dados.get(DATA_SOLICITACAO_RAMAL)!= null){
				servicoAutorizacao.setDataSolicitacaoRamal(DataUtil.converterParaData((String) dados.get(DATA_SOLICITACAO_RAMAL)));
			}


			montarServicoAutorizacaoMaterial(servicoAutorizacao, servicoTipo);
			montarServicoAutorizacaoEquipamento(servicoAutorizacao, servicoTipo);
		}

		servicoAutorizacao.setStatus(obterStatus(Constantes.C_STATUS_SERV_AUT_ABERTO));
		servicoAutorizacao.setHabilitado(Boolean.TRUE);
		servicoAutorizacao.setDataGeracao(Calendar.getInstance().getTime());
		servicoAutorizacao.setUsuarioSistema((Usuario) dados.get(USUARIO));
		
		servicoAutorizacao.setDescricao(montarDescricaoClienteTelefoneTipoServico(servicoAutorizacao) + servicoAutorizacao.getDescricao());
		
		if (servicoAutorizacao.getChamado() == null) {
			repositorioServicoAutorizacao.inserir(servicoAutorizacao);
		} else {
			repositorioServicoAutorizacao.inserirSemValidacao(servicoAutorizacao);
		}
		
		Double diferencaTempoGeracaoPrevisao = (DataUtil.obterDiferencaMinutosEntreDatas(
				servicoAutorizacao.getDataGeracao(), servicoAutorizacao.getDataPrevisaoEncerramento())) / 60.0;
		
		if(diferencaTempoGeracaoPrevisao.compareTo(12.0) <= 0 && (isGerarAsAutomatica != null && isGerarAsAutomatica)) {
			try {
				this.agendarServicoPrioritario(servicoAutorizacao);
			} catch(IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}

		enviarEmail(AcaoServicoAutorizacao.INSERIR, servicoAutorizacao);

		this.inserirServicoAutorizacaoHistorico(servicoAutorizacao, Constantes.C_STATUS_SERV_AUT_ABERTO, dados);
		return servicoAutorizacao;
	}

	private void agendarServicoPrioritario(ServicoAutorizacao servicoAutorizacao) throws GGASException, IOException {
		ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaVO = new ServicoAutorizacaoAgendaPesquisaVO();
		servicoAutorizacaoAgendaVO.setServicoTipo(servicoAutorizacao.getServicoTipo());
		servicoAutorizacaoAgendaVO.setEquipe(servicoAutorizacao.getEquipe());
		
		Long[] chaves = new Long[] { Long.valueOf(servicoAutorizacao.getChavePrimaria()) };
		
		Collection<ServicoAutorizacao> listaServicoAutorizacao = repositorioServicoAutorizacao
				.consultarServicoAutorizacaoPorChavesPrimarias(chaves);
		
		Collection<EntidadeConteudo> listaTurnos = controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno");
		
		EntidadeConteudo turno = listaTurnos.iterator().next();
		
		for(ServicoAutorizacao servicoAutorizacaoLista : listaServicoAutorizacao) {
			
			inserirServicoAutorizacaoAgenda(turno, Calendar.getInstance(), servicoAutorizacaoLista, Boolean.TRUE);
			
		}

	}

	private String montarDescricaoClienteTelefoneTipoServico(ServicoAutorizacao servicoAutorizacao) {
		StringBuilder montarDescricaoClienteTelefoneTipoServico = new StringBuilder();
		
		if(servicoAutorizacao.getServicoTipo() != null ) {
			montarDescricaoClienteTelefoneTipoServico.append(servicoAutorizacao.getServicoTipo().getDescricao());
			montarDescricaoClienteTelefoneTipoServico.append("\n");
		}
		
		if(servicoAutorizacao.getCliente() != null) {
			montarDescricaoClienteTelefoneTipoServico.append("Cliente: ");
			montarDescricaoClienteTelefoneTipoServico.append(servicoAutorizacao.getCliente().getNome());
			
			ClienteFone telefone = servicoAutorizacao.getCliente().getFones().stream().filter(p -> p.getIndicadorPrincipal()).findFirst().orElse(null);
			
			if(telefone != null) {
				montarDescricaoClienteTelefoneTipoServico.append(" - ");
				montarDescricaoClienteTelefoneTipoServico.append("(" + telefone.getCodigoDDD() + ") " + telefone.getNumero());
				montarDescricaoClienteTelefoneTipoServico.append("\n");
			}
		}
		
		
		return montarDescricaoClienteTelefoneTipoServico.toString();
	}

	private void montarServicoAutorizacaoEquipamento(ServicoAutorizacao servicoAutorizacao, ServicoTipo servicoTipo) {
		if (CollectionUtils.isNotEmpty(servicoTipo.getListaEquipamentoEspecial())) {
			List<Object> lista = Arrays.asList(servicoTipo.getListaEquipamentoEspecial());

			List<ServicoAutorizacaoEquipamento> equipamentos = new ArrayList<ServicoAutorizacaoEquipamento>();

			for (int i = 0; i < lista.size(); i++) {
				equipamentos.add(new ServicoAutorizacaoEquipamento());
				equipamentos.get(i).setEquipamento(((ServicoTipoEquipamento) lista.get(i)).getEquipamento());
				equipamentos.get(i).setServicoAutorizacao(servicoAutorizacao);
				equipamentos.get(i).setHabilitado(Boolean.TRUE);
				equipamentos.get(i).setVersao(0);
				equipamentos.get(i).setUltimaAlteracao(Calendar.getInstance().getTime());
			}

			servicoAutorizacao.getServicoAutorizacaoEquipamento().clear();
			servicoAutorizacao.getServicoAutorizacaoEquipamento().addAll(equipamentos);
		}
	}

	private void montarServicoAutorizacaoMaterial(ServicoAutorizacao servicoAutorizacao, ServicoTipo servicoTipo) {
		if (CollectionUtils.isNotEmpty(servicoTipo.getListaMateriais())) {
			List<Object> lista = Arrays.asList(servicoTipo.getListaMateriais());

			List<ServicoAutorizacaoMaterial> materiais = new ArrayList<ServicoAutorizacaoMaterial>();

			for (int i = 0; i < lista.size(); i++) {
				materiais.add(new ServicoAutorizacaoMaterial());
				materiais.get(i).setMaterial(((ServicoTipoMaterial) lista.get(i)).getMaterial());
				materiais.get(i).setQuantidadeMaterial((int) ((ServicoTipoMaterial) lista.get(i)).getQuantidadeMaterial());
				materiais.get(i).setServicoAutorizacao(servicoAutorizacao);
				materiais.get(i).setHabilitado(Boolean.TRUE);
				materiais.get(i).setUltimaAlteracao(Calendar.getInstance().getTime());
			}

			servicoAutorizacao.getServicoAutorizacaoMaterial().clear();
			servicoAutorizacao.getServicoAutorizacaoMaterial().addAll(materiais);
		}
	}

	private void verificarServicoTipoRestrito(ServicoTipo servicoTipo, Imovel imovel) throws NegocioException {
		for (ImovelServicoTipoRestricao restricao : imovel.getListaImovelServicoTipoRestricao()) {
			if (servicoTipo.getChavePrimaria() == restricao.getServicoTipo().getChavePrimaria()) {
				throw new AutorizacaoServicoImovelServicoTipoRestritoException(Constantes.ERRO_SERVICO_TIPO_RESTRITO,
						new String[] { imovel.getNome(), servicoTipo.getDescricao() });
			}
		}
	}

	private void verificarNumeroExecucoes(ServicoAutorizacao servicoAutorizacao, Imovel imovel) throws NegocioException {
		Long numeroMaximoExecucoes = servicoAutorizacao.getServicoTipo().getNumeroMaximoExecucoes();
		Long prazoGarantia = servicoAutorizacao.getServicoTipo().getPrazoGarantia();

		Long chavePrimariaCliente = 0L;
		if (servicoAutorizacao.getCliente() != null) {
			chavePrimariaCliente = servicoAutorizacao.getCliente().getChavePrimaria();
		}
		if (numeroMaximoExecucoes != null) {
			Optional<ServicoAutorizacao> servicoAutorizacaoGarantia = repositorioServicoAutorizacao.consultarServicoAutorizacaoReferenciaGarantia(
					chavePrimariaCliente, servicoAutorizacao.getServicoTipo().getChavePrimaria(), imovel.getChavePrimaria());
			if (servicoAutorizacaoGarantia.isPresent()) {
				LocalDate agora = LocalDate.now();

				LocalDate dataLimiteGarantia = AjaxService.asLocalDate(servicoAutorizacaoGarantia.get().getDataExecucao());

				if (prazoGarantia != null) {
					dataLimiteGarantia = dataLimiteGarantia.plus(prazoGarantia, ChronoUnit.DAYS);
				}
				validarNumeroDeExecucoes(servicoAutorizacao, imovel, numeroMaximoExecucoes, prazoGarantia, chavePrimariaCliente,
						servicoAutorizacaoGarantia, agora, dataLimiteGarantia);
			}
		}
	}

	private void validarNumeroDeExecucoes(ServicoAutorizacao servicoAutorizacao, Imovel imovel, Long numeroMaximoExecucoes,
			Long prazoGarantia, Long chavePrimariaCliente, Optional<ServicoAutorizacao> servicoAutorizacaoGarantia, LocalDate agora,
			LocalDate dataLimiteGarantia) throws NegocioException {
		if (dataLimiteGarantia.isBefore(agora) || prazoGarantia == null) {
			Long numeroExecucoes = repositorioServicoAutorizacao.consultarNumeroExecucoesPorClienteEServicoTipo(chavePrimariaCliente,
					servicoAutorizacaoGarantia.get().getServicoTipo().getChavePrimaria(), imovel.getChavePrimaria(),
					AjaxService.asDateFinalDia(dataLimiteGarantia));

			if (numeroExecucoes >= numeroMaximoExecucoes) {
				throw new NegocioException(Constantes.ERRO_SERVICO_NUMERO_EXECUCOES_ESTOURADO,
						new String[] { imovel.getNome(), servicoAutorizacao.getServicoTipo().getDescricao() });
			}
		}
	}

	private void verificarImovelCicloLeitura(ServicoTipo servicoTipo, PontoConsumo pontoConsumo, Imovel imovel) throws NegocioException {
		if (isServicoTipoSubstituicaoMedidor(servicoTipo) && pontoConsumo.temGrupoFaturamento()) {
			GrupoFaturamento grupo = pontoConsumo.getRota().getGrupoFaturamento();

			LeituraMovimento leituraMovimento = pesquisarLeituraMovimentoCicloMedicao(pontoConsumo, grupo);
			if (leituraMovimento != null) {
				throw new AutorizacaoServicoImovelCicloLeituraException(Constantes.ERRO_IMOVEL_EM_CICLO_DE_LEITURA,
						new String[] { servicoTipo.getDescricao(), imovel.getNome() });
			}
		}
	}

	@Override
	public LeituraMovimento pesquisarLeituraMovimentoCicloMedicao(PontoConsumo pontoConsumo, GrupoFaturamento grupo) {
		LeituraMovimento leituraMovimento = pesquisarLeituraMovimento(pontoConsumo, grupo, SituacaoLeituraMovimento.GERADO);
		if (leituraMovimento == null) {
			leituraMovimento = pesquisarLeituraMovimento(pontoConsumo, grupo, SituacaoLeituraMovimento.EM_LEITURA);
		}
		return leituraMovimento;
	}

	private LeituraMovimento pesquisarLeituraMovimento(PontoConsumo pontoConsumo, GrupoFaturamento grupo, long situacao) {
		LeituraMovimento filtro = obterFiltroLeituraMovimento(pontoConsumo, grupo, situacao);
		return controladorLeituraMovimento.consultarPorSituacaoAnoMesCicloPontoConsumo(filtro);
	}

	private void verificarServicoAutorizacaoRepetida(ServicoAutorizacaoVO vo)
			throws FormatoInvalidoException, NegocioException {

		long statusCancelado = obterStatus(Constantes.C_STATUS_SERV_AUT_CANCELADO).getChavePrimaria();
		long statusEncerrado = obterStatus(Constantes.C_STATUS_SERV_AUT_ENCERRADO).getChavePrimaria();

		List<ServicoAutorizacao> lista = (List<ServicoAutorizacao>) repositorioServicoAutorizacao.consultarServicoAutorizacao(vo);

		for (ServicoAutorizacao sa : lista) {

			if (sa.getStatus().getChavePrimaria() != statusCancelado &&
				sa.getStatus().getChavePrimaria() != statusEncerrado) {

				throw new AutorizacaoServicoRepetidaException(Constantes.ERRO_SERVICO_AUTORIZACAO_DUPLICADO,
						new String[] { sa.getServicoTipo().getDescricao(), String.valueOf(sa.getChavePrimaria()) });
			}
		}
	}

	private EntidadeConteudo obterStatus(String status) {
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(status);
		return controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
	}

	@Override
	public boolean isServicoTipoSubstituicaoMedidor(ServicoTipo servicoTipo) {
		return servicoTipo != null &&
			   "1".equals(servicoTipo.getIndicadorTipoAssociacaoPontoConsumo()) &&
			   Long.parseLong(servicoTipo.getIndicadorOperacaoMedidor()) == OperacaoMedidor.CODIGO_SUBSTITUICAO;
	}

	private LeituraMovimento obterFiltroLeituraMovimento(PontoConsumo pontoConsumo, GrupoFaturamento grupo, long situacaoLeitura) {
		LeituraMovimento filtro = (LeituraMovimento) controladorLeituraMovimento.criar();
		filtro.setSituacaoLeitura(obterSituacaoLeituraMovimento(situacaoLeitura));
		filtro.setPontoConsumo(pontoConsumo);
		filtro.setAnoMesFaturamento(grupo.getAnoMesReferencia());
		filtro.setCiclo(grupo.getNumeroCiclo());
		return filtro;
	}

	private SituacaoLeituraMovimento obterSituacaoLeituraMovimento(long situacao) {
		SituacaoLeituraMovimento situacaoLeitura = (SituacaoLeituraMovimento) ServiceLocator.getInstancia().getBeanPorID(
				SituacaoLeituraMovimento.BEAN_ID_SITUACAO_LEITURA_MOVIMENTO);

		situacaoLeitura.setChavePrimaria(situacao);

		return situacaoLeitura;
	}

	/**
	 * Inserir servico autorizacao historico.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @param tipoOperacao the tipo operacao
	 * @param dados the dados
	 * @throws GGASException {@link GGASException}
	 */
	public void inserirServicoAutorizacaoHistorico(ServicoAutorizacao servicoAutorizacao, String tipoOperacao, Map<String, Object> dados)
			throws GGASException {
		this.inserirServicoAutorizacaoHistorico(servicoAutorizacao, tipoOperacao, dados, null);
	}

	/**
	 * Inserir servico autorizacao historico.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @param tipoOperacao the tipo operacao
	 * @param dados the dados
	 * @param listaPergunta {@link List}
	 * @throws GGASException {@link GGASException}
	 */
	public void inserirServicoAutorizacaoHistorico(ServicoAutorizacao servicoAutorizacao, String tipoOperacao, Map<String, Object> dados,
			List<QuestionarioPergunta> listaPergunta) throws GGASException {

		ServicoAutorizacaoHistorico historico = new ServicoAutorizacaoHistorico();

		Usuario usuario = (Usuario) dados.get(USUARIO);
		historico.setUsuarioSistema(usuario);

		historico.setDataPrevisaoEncerramento(servicoAutorizacao.getDataPrevisaoEncerramento());
		historico.setEquipe(servicoAutorizacao.getEquipe());
		historico.setDescricao(servicoAutorizacao.getDescricao());
		historico.setHabilitado(Boolean.TRUE);
		historico.setServicoAutorizacao(servicoAutorizacao);
		historico.setStatus(servicoAutorizacao.getStatus());
		historico.setServicoAutorizacaoMotivoEncerramento(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento());

		if (servicoAutorizacao.getUsuarioSistema() == null) {
			historico.setUsuarioSistema(usuario);
		} else {
			historico.setUsuarioSistema(servicoAutorizacao.getUsuarioSistema());
		}

		if (servicoAutorizacao.getAnexos() != null) {
			historico.setAnexos(new HashSet<ServicoAutorizacaoHistoricoAnexo>(servicoAutorizacao.getAnexos()));
			for (ServicoAutorizacaoHistoricoAnexo saha : historico.getAnexos()) {
				saha.setServicoAutorizacao(servicoAutorizacao);
			}
		}

		historico.setVersao(0);

		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(tipoOperacao);
		EntidadeConteudo operacao = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteSistema.getValor()));

		historico.setOperacao(operacao);
		long chavePrimaria = repositorioServicoAutorizacao.inserirSemValidacao(historico);
		historico.setChavePrimaria(chavePrimaria);
		if (listaPergunta != null) {
			controladorResposta.incluirResposta(historico, dados, listaPergunta);
		} else if (Constantes.C_STATUS_SERV_AUT_EXECUTADO.equals(tipoOperacao)
				&& servicoAutorizacao.getServicoTipo().getIndicadorFormularioObrigatorio()) {
			throw new GGASException(Constantes.ERRO_FORMULARIO_OBRIGATORIO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterServicoAutorizacao(java.lang.Long)
	 */
	@Override
	public ServicoAutorizacao obterServicoAutorizacao(Long chavePrimaria) {
		return repositorioServicoAutorizacao.obterServicoAutorizacao(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterServicoAutorizacaoMotivoEncerramento(java.lang
	 * .Long)
	 */
	@Override
	public ServicoAutorizacaoMotivoEncerramento obterServicoAutorizacaoMotivoEncerramento(Long chavePrimaria) throws NegocioException {
		return (ServicoAutorizacaoMotivoEncerramento) repositorioServicoAutorizacao.obter(chavePrimaria,
				ServicoAutorizacaoMotivoEncerramento.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#listarServicoAutorizacaoHistorico(java.lang.Long)
	 */
	@Override
	public Collection<ServicoAutorizacaoHistorico> listarServicoAutorizacaoHistorico(Long chavePrimaria) throws NegocioException {
		return repositorioServicoAutorizacao.listarServicoAutorizacaoHistorico(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarServicoAutorizacaoPorClienteOuImovel(java.
	 * lang .Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorClienteOuImovel(Long chavePrimariaCliente,
			Long chavePrimariaImovel, Long chavePrimariaPontoConsumo) throws NegocioException {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoPorClienteImovelPontoConsumo(chavePrimariaCliente,
				chavePrimariaImovel, chavePrimariaPontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#listarImoveisPorCliente(java.lang.Long)
	 */
	@Override
	public List<Imovel> listarImoveisPorCliente(Long chaveCliente) throws NegocioException {

		List<Imovel> imoveis = new ArrayList<>();

		if (chaveCliente != null) {

			Collection<PontoConsumo> pontoConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(chaveCliente);
			for (Iterator<PontoConsumo> it = pontoConsumo.iterator(); it.hasNext();) {
				Imovel imovel = it.next().getImovel();
				imovel.getModalidadeMedicaoImovel();
				if (imovel.getImovelCondominio() != null) {
					imovel.getImovelCondominio().getModalidadeMedicaoImovel();
				}
				imoveis.add(imovel);
			}
		}
		return imoveis;
	}

	/**
	 * Verificar equipe.
	 *
	 * @param servicoAutorizacaoNovo the servico autorizacao novo
	 * @param servicoAutorizacaoChave the servico Autorizacao Chave
	 * @throws NegocioException the negocio exception
	 */
	public void verificarEquipe(ServicoAutorizacao servicoAutorizacaoNovo, ServicoAutorizacao servicoAutorizacaoChave)
			throws NegocioException {

		if (servicoAutorizacaoNovo.getEquipe() == null
				&& servicoAutorizacaoNovo.getEquipe().getChavePrimaria() == servicoAutorizacaoChave.getEquipe().getChavePrimaria()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_REMANEJAR_SEM_ALTERACAO);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#remanejar(br.com.ggas.atendimento.
	 * servicoautorizacao. dominio .ServicoAutorizacao, java.util.Map)
	 * 
	 * @param servicoAutorizacaoNovo the servico autorizacao novo
	 * @param dados the servico Autorizacao Chave
	 * @param servicoAutorizacaoChave the servico Autorizacao Chave
	 * @throws NegocioException the negocio exception
	 */
	@Override
	public void remanejar(ServicoAutorizacao servicoAutorizacaoNovo, Map<String, Object> dados, ServicoAutorizacao servicoAutorizacaoChave)
			throws GGASException {

		ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoNovo;

		if (servicoAutorizacao.getEquipe() != null) {
			validarDadosRemanejamento(servicoAutorizacao);
			verificarEquipe(servicoAutorizacao, servicoAutorizacaoChave);
			ServicoAutorizacao servicoAutorizacaoConsulta =
					repositorioServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacaoChave.getChavePrimaria());
			validarVersaoEntidade(servicoAutorizacao.getVersao(), servicoAutorizacaoConsulta.getVersao());

			servicoAutorizacaoConsulta.setDescricao(servicoAutorizacaoNovo.getDescricao());
			servicoAutorizacaoConsulta.setEquipe(servicoAutorizacaoNovo.getEquipe());
			servicoAutorizacaoConsulta.setAnexos(servicoAutorizacaoNovo.getAnexos());

			repositorioServicoAutorizacao.getHibernateTemplate().evict(servicoAutorizacao);
			repositorioServicoAutorizacao.atualizarSemValidar(servicoAutorizacaoConsulta, ServicoAutorizacao.class);
			inserirServicoAutorizacaoHistorico(servicoAutorizacaoConsulta, Constantes.C_OPERACAO_SERV_AUT_REMANEJADO, dados);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_EQUIPE_INEXISTENTE);
		}
	}

	/**
	 * Verificar data.
	 *
	 * @param data the data
	 * @throws NegocioException the negocio exception
	 */
	private void verificarData(String data) throws NegocioException {
		if (!Util.isDataValida(data, Constantes.FORMATO_DATA_BR)) {
			throw new NegocioException(Constantes.ERRO_DATA_INVALIDADE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#atualizarServicoAutorizacao(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacao, java.util.Map, java.util.Collection, java.util.Collection)
	 */
	@Override
	public ServicoAutorizacao atualizarServicoAutorizacao(ServicoAutorizacao servicoAutorizacaoTmp, Map<String, Object> dados,
			Collection<ServicoAutorizacaoMaterial> listaMateriais, Collection<ServicoAutorizacaoEquipamento> listaEquipamentos)
			throws ParseException, GGASException {

		ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoTmp;
		ServicoAutorizacao servicoConsulta = repositorioServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
		validarVersaoEntidade(servicoAutorizacao.getVersao(), servicoConsulta.getVersao());
		repositorioServicoAutorizacao.getHibernateTemplate().evict(servicoConsulta);

		servicoConsulta.setDescricao(servicoAutorizacao.getDescricao());
		servicoConsulta.setEquipe(servicoAutorizacao.getEquipe());
		servicoConsulta.setAnexos(servicoAutorizacao.getAnexos());
		servicoConsulta.setDataExecucao(servicoAutorizacao.getDataExecucao());
		servicoConsulta.setNumeroProtocolo(servicoAutorizacao.getNumeroProtocolo());
		servicoConsulta.setMedidor(servicoAutorizacao.getMedidor());
		servicoConsulta.setNumeroOS(servicoAutorizacao.getNumeroOS());
		servicoConsulta.setTurnoPreferencia(servicoAutorizacao.getTurnoPreferencia());

		String dataPrevisaoEncerramento = (String) dados.get("dataPrevisaoEncerramento");
		if (dataPrevisaoEncerramento != null && !dataPrevisaoEncerramento.isEmpty()) {
			verificarData(dataPrevisaoEncerramento);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date data = df.parse(dataPrevisaoEncerramento);
			servicoConsulta.setDataPrevisaoEncerramento(data);
			verificarDataPrevisaoEncerramento(servicoConsulta);

		} else {
			servicoConsulta.setDataPrevisaoEncerramento(null);
		}

		String dataSolicitacaoRamal = (String) dados.get(DATA_SOLICITACAO_RAMAL);
		if (dataSolicitacaoRamal != null && !dataSolicitacaoRamal.isEmpty()) {
			verificarData(dataSolicitacaoRamal);
			SimpleDateFormat df = new SimpleDateFormat(PADRAO_FORMATO_DATA);
			Date data = df.parse(dataSolicitacaoRamal);
			servicoConsulta.setDataSolicitacaoRamal(data);
		} else {
			servicoConsulta.setDataSolicitacaoRamal(null);
		}

		servicoAutorizacao = servicoConsulta;
		manipularListaMateriaisEquipamentos(servicoAutorizacao, listaMateriais, listaEquipamentos);
		servicoAutorizacao.setUltimaAlteracao(Calendar.getInstance().getTime());
		this.validarDadosAtualizar(servicoAutorizacao);
		repositorioServicoAutorizacao.atualizarSemValidar(servicoAutorizacao, ServicoAutorizacao.class);
		enviarEmail(AcaoServicoAutorizacao.ALTERAR, servicoAutorizacao);
		this.inserirServicoAutorizacaoHistorico(servicoAutorizacao, Constantes.C_OPERACAO_CHAMADO_ALTERADO, dados);

		
		return servicoAutorizacao;
	}

	@Override
	public void atualizarServicoAutorizacaoTemp(ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws GGASException {
		if(!StringUtils.isEmpty(servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal())) {
			List<Medidor> medidor = (List<Medidor>) Fachada.getInstancia().obterMedidorPorNumeroSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal());
			if(medidor != null && !medidor.isEmpty()) {
				servicoAutorizacaoTemp.setMedidorOriginal(medidor.get(medidor.size() - 1));
			} else {
				servicoAutorizacaoTemp.setMedidorOriginal(null);
				servicoAutorizacaoTemp.setMedidorNumeroSerieOriginal("");
			}
		} else {
			servicoAutorizacaoTemp.setMedidorOriginal(null);
		}
		
		if (!StringUtils.isEmpty(servicoAutorizacaoTemp.getMedidorNumeroSerieNovo())) {
			List<Medidor> medidor = (List<Medidor>) Fachada.getInstancia()
					.obterMedidorPorNumeroSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieNovo());
			if (medidor != null && !medidor.isEmpty()) {
				servicoAutorizacaoTemp.setMedidorNovo(medidor.get(medidor.size() - 1));
			} else {
				servicoAutorizacaoTemp.setMedidorNovo(null);
				servicoAutorizacaoTemp.setMedidorNumeroSerieNovo("");
			}
		} else {
			servicoAutorizacaoTemp.setMedidorNovo(null);
		}
		
		
		this.atualizarServicoAutorizacaoTempSemValidar(servicoAutorizacaoTemp);
		
	}

	/**
	 * Verificar data previsao encerramento.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @throws NegocioException the negocio exception
	 */
	private void verificarDataPrevisaoEncerramento(ServicoAutorizacao servicoAutorizacao) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(PADRAO_FORMATO_DATA);
		String geracao = df.format(servicoAutorizacao.getDataGeracao());
		String previsao = df.format(servicoAutorizacao.getDataPrevisaoEncerramento());

		if (servicoAutorizacao.getDataPrevisaoEncerramento().before(servicoAutorizacao.getDataGeracao()) && !geracao.equals(previsao)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATAS);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#executarServicoAutorizacao(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacao, java.util.Collection, java.util.Collection, java.lang.String, java.util.Map)
	 */
	@Override
	public void executarServicoAutorizacao(ServicoAutorizacao autorizacaoServico, Collection<ServicoAutorizacaoMaterial> listaMateriais,
			Collection<ServicoAutorizacaoEquipamento> listaEquipamentos, String tipoStatus, Map<String, Object> dados,
			List<QuestionarioPergunta> listaPergunta) throws GGASException {

		ServicoAutorizacao servicoAutorizacao = autorizacaoServico;
		ServicoAutorizacao servicoConsulta = repositorioServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
		validarVersaoEntidade(servicoAutorizacao.getVersao(), servicoConsulta.getVersao());

		verificarFormularioObrigatorio(listaPergunta, servicoAutorizacao);

		servicoConsulta.setDataExecucao(servicoAutorizacao.getDataExecucao());
		verificarDataPrevisaoEncerramentoServicoAutorizacao(servicoAutorizacao, servicoConsulta.getDataGeracao());

		servicoConsulta.setExecutante(servicoAutorizacao.getExecutante());
		servicoConsulta.setDescricao(servicoAutorizacao.getDescricao());
		servicoAutorizacao.setUltimaAlteracao(Calendar.getInstance().getTime());
		servicoConsulta.setAnexos(servicoAutorizacao.getAnexos());
		servicoConsulta.setIndicadorSucessoExecucao(servicoAutorizacao.getIndicadorSucessoExecucao());
		servicoConsulta.setServicoAutorizacaoMotivoEncerramento(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento());

		if (tipoStatus != null) {
			servicoConsulta.setStatus(obterStatus(tipoStatus));

			if (tipoStatus.equals(Constantes.C_STATUS_SERV_AUT_ENCERRADO)) {
				servicoConsulta.setDataEncerramento(Calendar.getInstance().getTime());

				validarEncerramentoServicoAutorizacao(servicoConsulta);
				verificaEncerramentoChamado(servicoConsulta, dados);
			}
		}

		Boolean deveVerificarReferenciaGarantia = isDeveVerificarReferenciaGarantia(servicoAutorizacao);

		if (deveVerificarReferenciaGarantia != null) {
			servicoConsulta.setIndicadorReferenciaGarantia(this.isVerificarServicoDeveSerReferenciaNaGarantia(servicoAutorizacao));
		} else {
			servicoConsulta.setIndicadorReferenciaGarantia(false);
		}

		repositorioServicoAutorizacao.getHibernateTemplate().evict(servicoConsulta);
		servicoAutorizacao = servicoConsulta;
		manipularListaMateriaisEquipamentos(servicoAutorizacao, listaMateriais, listaEquipamentos);
		repositorioServicoAutorizacao.atualizar(servicoAutorizacao);
		enviarEmail(AcaoServicoAutorizacao.EXECUTAR, servicoAutorizacao);
		this.inserirServicoAutorizacaoHistorico(servicoAutorizacao, Constantes.C_OPERACAO_SERV_AUT_EXECUTADO, dados, listaPergunta);
	}

	private Boolean isDeveVerificarReferenciaGarantia(ServicoAutorizacao servicoAutorizacao) {
		Boolean deveVerificarReferenciaGarantia = servicoAutorizacao.getIndicadorSucessoExecucao();
		if (servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() != null &&
				servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getDescricao().equals(Constantes.MOTIVO_SUCESSO_ENCERRAMENTO_AS)
				&& deveVerificarReferenciaGarantia == null) {
			deveVerificarReferenciaGarantia = true;
		}
		return deveVerificarReferenciaGarantia;
	}

	private boolean isVerificarServicoDeveSerReferenciaNaGarantia(ServicoAutorizacao servicoAutorizacao) {

		boolean isReferencia = false;
		if (servicoAutorizacao.getDataExecucao() != null && servicoAutorizacao.getCliente() != null
				&& servicoAutorizacao.getServicoTipo() != null && servicoAutorizacao.getPontoConsumo() != null
				&& servicoAutorizacao.getPontoConsumo().getImovel() != null) {
			Optional<ServicoAutorizacao> servicoAutorizacaoReferencia =
					this.consultarServicoAutorizacaoReferenciaGarantia(servicoAutorizacao.getCliente().getChavePrimaria(),
					servicoAutorizacao.getServicoTipo().getChavePrimaria(),
							servicoAutorizacao.getPontoConsumo().getImovel().getChavePrimaria());
			if (servicoAutorizacaoReferencia.isPresent()) {
				Optional<LocalDate> dataLimiteGarantia =
						AjaxService.buildDataGarantia(servicoAutorizacaoReferencia.get().getServicoTipo().getPrazoGarantia(),
								servicoAutorizacaoReferencia);
				if (dataLimiteGarantia.isPresent()) {
					isReferencia = servicoAutorizacao.getDataExecucao().after(AjaxService.asDateFinalDia(dataLimiteGarantia.get()));
				}
			} else {
				isReferencia = true;
			}
		}

		return isReferencia;
	}

	private void verificarFormularioObrigatorio(List<QuestionarioPergunta> listaPergunta,
			ServicoAutorizacao servicoAutorizacao) throws GGASException {
		if (listaPergunta == null && servicoAutorizacao.getServicoTipo().getIndicadorFormularioObrigatorio()) {
			throw new GGASException(Constantes.ERRO_FORMULARIO_OBRIGATORIO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#encerrarServicoAutorizacao(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacao, java.util.Collection, java.util.Collection, java.lang.String, java.util.Map)
	 */
	@Override
	public void encerrarServicoAutorizacao(ServicoAutorizacao autorizacaoServico, Collection<ServicoAutorizacaoMaterial> listaMateriais,
			Collection<ServicoAutorizacaoEquipamento> listaEquipamentos, String tipoStatus, Map<String, Object> dados)
			throws GGASException, java.net.UnknownHostException {

		ServicoAutorizacao servicoAutorizacao = autorizacaoServico;
		ServicoAutorizacao servicoConsulta = repositorioServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
		validarVersaoEntidade(servicoAutorizacao.getVersao(), servicoConsulta.getVersao());

		servicoConsulta.setExecutante(servicoAutorizacao.getExecutante());
		servicoConsulta.setDescricao(servicoAutorizacao.getDescricao());
		servicoConsulta.setServicoAutorizacaoMotivoEncerramento(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento());
		servicoConsulta.setDataEncerramento(Calendar.getInstance().getTime());
		servicoConsulta.setDataExecucao(servicoAutorizacao.getDataExecucao());
		servicoConsulta.setUltimaAlteracao(Calendar.getInstance().getTime());
		servicoConsulta.setAnexos(servicoAutorizacao.getAnexos());

		validarEncerramentoServicoAutorizacao(servicoConsulta);

		setarStatusServicoAutorizacao(tipoStatus, servicoConsulta);

		repositorioServicoAutorizacao.getHibernateTemplate().evict(servicoConsulta);
		servicoAutorizacao = servicoConsulta;

		manipularListaMateriaisEquipamentos(servicoAutorizacao, listaMateriais, listaEquipamentos);

		setarChamado(tipoStatus, dados, servicoAutorizacao);

		if (servicoAutorizacao.getChamado() != null && servicoAutorizacao.getChamado().getChamadoAssunto().getIndicadorDebito()
				&& servicoAutorizacao.getServicoTipo().getIndicadorCobranca() != null
				&& servicoAutorizacao.getServicoTipo().getIndicadorCobranca()) {

			if (servicoAutorizacao.getServicoTipo().getIndicadorProximaFatura() != null) {

				if (servicoAutorizacao.getServicoTipo().getIndicadorProximaFatura()) {
					gerarCreditoDebitoProximaFatura(servicoAutorizacao);
				} else {
					validarInserirNotaDebitoCredito(servicoAutorizacao);
				}
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_INDICADOR_COBRANCA);
			}
		}

		repositorioServicoAutorizacao.atualizarSemValidar(servicoAutorizacao, ServicoAutorizacao.class);
		enviarEmail(AcaoServicoAutorizacao.ENCERRAR, servicoAutorizacao);
		this.inserirServicoAutorizacaoHistorico(servicoAutorizacao, Constantes.C_OPERACAO_CHAMADO_ENCERRADO, dados);

		this.verificaEncerramentoChamado(servicoAutorizacao, dados);
	}

	private void setarChamado(String tipoStatus, Map<String, Object> dados, ServicoAutorizacao servicoAutorizacao)
			throws GGASException {
		if (servicoAutorizacao.getServicoTipo().getIndicadorPesquisaSatisfacao() != null
				&& servicoAutorizacao.getServicoTipo().getIndicadorPesquisaSatisfacao() && tipoStatus != null
				&& tipoStatus.equalsIgnoreCase(Constantes.C_STATUS_SERV_AUT_ENCERRADO) && servicoAutorizacao.getCliente() != null) {
			Chamado chamado = controladorChamado.gerarChamadoPerquisaSatisfacao(servicoAutorizacao, dados);
			servicoAutorizacao.setChamadoTipoPesquisa(chamado);
		}
	}

	private void validarInserirNotaDebitoCredito(ServicoAutorizacao servicoAutorizacao)
			throws UnknownHostException, GGASException {
		validaPontoDeConsumoSelecionado(servicoAutorizacao);
		FaturaImpl notaDebitoCredito = obterNotaDebitoCredito(servicoAutorizacao);
		adcionandoSegmentoNotaDebito(servicoAutorizacao, notaDebitoCredito);

		notaDebitoCredito.setValorConciliado(BigDecimal.ZERO);

		BigDecimal valorReferencia = servicoAutorizacao.getChamado().getChamadoAssunto()
				.getRubricaDebito().getValorReferencia();
		BigDecimal valorMaximo = servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getValorMaximo();

		atribuiValoresANotaDeDebito(valorReferencia, valorMaximo, notaDebitoCredito);

		notaDebitoCredito.setSituacaoPagamento(obterSituacaoPagamento());
		notaDebitoCredito.setTipoDocumento(obterTipoDocumento());
		notaDebitoCredito.setProvisaoDevedoresDuvidosos(Boolean.FALSE);
		notaDebitoCredito.setCreditoDebitoSituacao(obterCreditoDebitoSituacao());
		notaDebitoCredito.setCobrada(Boolean.TRUE);

		FaturaItemImpl faturaItem = new FaturaItemImpl();

		faturaItem.setQuantidade(BigDecimal.ONE);
		faturaItem.setValorTotal(notaDebitoCredito.getValorTotal());
		faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
		faturaItem.setRubrica(servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito());
		faturaItem.setValorUnitario(notaDebitoCredito.getValorTotal());
		faturaItem.setNumeroSequencial(1);

		notaDebitoCredito.getListaFaturaItem().add(faturaItem);
		notaDebitoCredito.setProvisaoDevedoresDuvidosos(Boolean.FALSE);

		InetAddress[] addresses = InetAddress.getAllByName(InetAddress.getLocalHost().getHostAddress());

		notaDebitoCredito.setDadosAuditoria(Util.getDadosAuditoria(servicoAutorizacao.getChamado().getUsuarioResponsavel(),
				null, Arrays.toString(addresses)));

		Cliente cliente = servicoAutorizacao.getCliente();
		if (cliente != null) {
			controladorFatura.validarDadosNotaDebito(notaDebitoCredito);
			controladorFatura.inserirNota(notaDebitoCredito, new Long[] {}, null);
		}
	}

	private FaturaImpl obterNotaDebitoCredito(ServicoAutorizacao servicoAutorizacao) {
		FaturaImpl notaDebitoCredito = new FaturaImpl();
		notaDebitoCredito.setCliente(servicoAutorizacao.getChamado().getCliente());
		notaDebitoCredito.setContrato(servicoAutorizacao.getChamado().getContrato());
		notaDebitoCredito.setContratoAtual(servicoAutorizacao.getChamado().getContrato());
		notaDebitoCredito.setDataEmissao(Calendar.getInstance().getTime());

		Calendar hoje = Calendar.getInstance();
		hoje.add(Calendar.DAY_OF_MONTH, DIAS_ADICIONAR);
		Date dataVencimento = hoje.getTime();

		notaDebitoCredito.setDataVencimento(dataVencimento);
		notaDebitoCredito.setHabilitado(Boolean.TRUE);
		notaDebitoCredito.setPontoConsumo(servicoAutorizacao.getChamado().getPontoConsumo());
		return notaDebitoCredito;
	}

	private CreditoDebitoSituacao obterCreditoDebitoSituacao() throws NegocioException {
		Long idSituacaoNormal = Long.valueOf((String) controladorParametroSistema.obterValorDoParametroPorCodigo(
				Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL));
		return controladorCobranca.obterCreditoDebitoSituacao(idSituacaoNormal);
	}

	private EntidadeConteudo obterSituacaoPagamento() {
		Long situacaoPendente = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		return controladorEntidadeConteudo.obterEntidadeConteudo(situacaoPendente);
	}

	private TipoDocumentoImpl obterTipoDocumento() throws NegocioException {
		Long tipoDocumento = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		return (TipoDocumentoImpl) controladorTipoDocumento.obter(tipoDocumento);
	}

	private void setarStatusServicoAutorizacao(String tipoStatus, ServicoAutorizacao servicoConsulta) {
		if (tipoStatus != null && tipoStatus.equals(Constantes.C_STATUS_SERV_AUT_ENCERRADO)) {
			ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ENCERRADO);
			EntidadeConteudo statusEncerrado = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			servicoConsulta.setStatus(statusEncerrado);
		} else if (tipoStatus != null && tipoStatus.equals(Constantes.C_STATUS_SERV_AUT_PENDENTE)) {
			ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_PENDENTE);
			EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			servicoConsulta.setStatus(statusPendente);
		} else if (tipoStatus != null && tipoStatus.equals(Constantes.C_STATUS_SERV_AUT_CANCELADO)) {
			ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_CANCELADO);
			EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			servicoConsulta.setStatus(statusPendente);
		}
	}

	private void gerarCreditoDebitoProximaFatura(ServicoAutorizacao servicoAutorizacao)
			throws GGASException {
		Map<String, Object> dadosChamado = new HashMap<>();

		this.adicionarClienteEmDadosChamado(servicoAutorizacao, dadosChamado);
		this.adicionarPontoConsumoEmDadosChamado(servicoAutorizacao, dadosChamado);
		this.popularDadosChamado(servicoAutorizacao, dadosChamado);

		dadosChamado.put("parcelas", 1);
		dadosChamado.put("valorUnitario", "valor unitário");
		dadosChamado.put("descricao", "valor cobrança");
		dadosChamado.put("tipoDocumento", "documento");
		dadosChamado.put("quantidade", BigDecimal.ONE);
		dadosChamado.put("dataInicioCredito", Calendar.getInstance().getTime());

		DadosGeraisParcelas dadosGerais = controladorCreditoDebito.gerarParcelasCreditoDebito(dadosChamado, true);

		controladorCreditoDebito.salvarCreditoDebitoARealizar(dadosChamado, dadosGerais);
	}

	private void popularDadosChamado(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dadosChamado) throws GGASException {
		if (servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito() != null) {

			if (servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getValorReferencia() != null
					&& servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getValorReferencia()
							.compareTo(BigDecimal.ZERO) > 0) {

				servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getValorReferencia();

				String valor =
						Util.converterCampoValorDecimalParaString("Valor", servicoAutorizacao.getChamado().getChamadoAssunto()
								.getRubricaDebito().getValorReferencia(), Constantes.LOCALE_PADRAO);

				BigDecimal valorDecimal =
						Util.converterCampoStringParaValorBigDecimal("Valor", valor, Constantes.FORMATO_VALOR_NUMERO,
								Constantes.LOCALE_PADRAO);

				dadosChamado.put("valorHidden", valorDecimal);

				dadosChamado.put("idRubrica", servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getChavePrimaria());

				dadosChamado.put("valorUnitarioHidden", valorDecimal);

				dadosChamado.put("valor", valorDecimal);

			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_RUBRICA_DEBITO);
			}
		}
	}

	private void adicionarPontoConsumoEmDadosChamado(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dadosChamado) {
		if ((servicoAutorizacao.getChamado().getPontoConsumo() != null)
				&& (servicoAutorizacao.getChamado().getPontoConsumo().getChavePrimaria() > 0)) {
			dadosChamado.put("idPontoConsumo", servicoAutorizacao.getChamado().getPontoConsumo().getChavePrimaria());
		}
	}

	private void adicionarClienteEmDadosChamado(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dadosChamado) {
		if ((servicoAutorizacao.getChamado().getCliente() != null) && (servicoAutorizacao.getChamado().getCliente().getChavePrimaria() > 0)) {
			dadosChamado.put("idCliente", servicoAutorizacao.getChamado().getCliente().getChavePrimaria());
		}
	}

	private void atribuiValoresANotaDeDebito(BigDecimal valorReferencia, BigDecimal valorMaximo, FaturaImpl notaDebitoCredito)
			throws NegocioException {

		if (valorReferencia != null) {
			notaDebitoCredito.setValorTotal(valorReferencia);
		} else if (valorMaximo != null) {
			notaDebitoCredito.setValorTotal(valorMaximo);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_RUBRICA_DEBITO);
		}
	}

	private void validaPontoDeConsumoSelecionado(ServicoAutorizacao servicoAutorizacao) throws NegocioException {

		if (servicoAutorizacao.getChamado().getPontoConsumo() != null) {
			controladorFatura.validarPontoConsumoSelecionado(servicoAutorizacao.getChamado().getPontoConsumo().getChavePrimaria());
		}

	}

	private void adcionandoSegmentoNotaDebito(ServicoAutorizacao servicoAutorizacao, FaturaImpl notaDebitoCredito) {
		if (servicoAutorizacao.getChamado().getPontoConsumo() != null) {
			notaDebitoCredito.setSegmento(servicoAutorizacao.getChamado().getPontoConsumo().getSegmento());
		}
	}

	/**
	 * Verifica se o chamado da autorização de serviço deve ser encerrado.
	 *
	 * @param servicoAutorizacao the servicoAutorizacao
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	@Override
	public void verificaEncerramentoChamado(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados) throws GGASException {

		if (servicoAutorizacao.getChamado() != null) {

			Chamado chamado = controladorChamado.obterChamado(servicoAutorizacao.getChamado().getChavePrimaria());
			ChamadoAssunto chamadoAssunto = controladorChamadoAssunto.consultarChamadoAssunto(
					chamado.getChamadoAssunto().getChavePrimaria());

			if (Util.isTrue(chamadoAssunto.getIndicadorEncerraAutoASRel())) {

				Collection<ServicoAutorizacao> listaServicoAutorizacao = consultarServicoAutorizacaoPorChaveChamado(
						chamado.getChavePrimaria());

				Boolean encerraChamado = this.verificarServicoautorizacaoEncerraChamado(
						listaServicoAutorizacao, obterStatus(Constantes.C_STATUS_SERV_AUT_ENCERRADO));

				if (encerraChamado) {

					ConstanteSistema constASEncerradas =
							controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CONTEUDO_MOTIVO_AS_EXECUTADAS);
					EntidadeConteudo motivoASEncerradas =
							controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constASEncerradas.getValor()));

					dados.put("idMotivo", String.valueOf(motivoASEncerradas.getChavePrimaria()));

					chamado.setIndicadorVazamentoConfirmado((Boolean) dados.get("indicadorVazamentoConfirmado"));
					chamado.setDescricao(servicoAutorizacao.getDescricao());
					chamado.setDataResolucao(servicoAutorizacao.getDataExecucao());
					chamado.setDadosAuditoria(servicoAutorizacao.getDadosAuditoria());

					controladorChamado.encerrarChamado(chamado, dados);
				}
			}
		}
	}

	private Boolean verificarServicoautorizacaoEncerraChamado(Collection<ServicoAutorizacao> listaServicoAutorizacao,
			EntidadeConteudo statusEncerrado) {
		Boolean encerraChamado = true;
		for (ServicoAutorizacao sa : listaServicoAutorizacao) {
			if (!statusEncerrado.equals(sa.getStatus())) {
				encerraChamado = false;
			}
		}
		return encerraChamado;
	}

	/**
	 * Validar versao entidade.
	 *
	 * @param versao the versao
	 * @param versaoAtual the versao atual
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void validarVersaoEntidade(int versao, int versaoAtual) throws ConcorrenciaException {
		if (versao != versaoAtual) {
			throw new ConcorrenciaException(Constantes.ERRO_ENTIDADE_VERSAO_DESATUALIZADA, SERVICO_AUTORIZACAO);
		}
	}

	/**
	 * Manipular lista materiais equipamentos.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @param listaMateriais the lista materiais
	 * @param listaEquipamentos the lista equipamentos
	 */
	private void manipularListaMateriaisEquipamentos(ServicoAutorizacao servicoAutorizacao,
			Collection<ServicoAutorizacaoMaterial> listaMateriais, Collection<ServicoAutorizacaoEquipamento> listaEquipamentos) {

		if (listaEquipamentos != null) {
			servicoAutorizacao.getServicoAutorizacaoEquipamento().clear();
			servicoAutorizacao.getServicoAutorizacaoEquipamento().addAll(listaEquipamentos);
			for (ServicoAutorizacaoEquipamento servicoAutorizacaoEquipamento : listaEquipamentos) {
				servicoAutorizacaoEquipamento.setServicoAutorizacao(servicoAutorizacao);
				servicoAutorizacaoEquipamento.setHabilitado(Boolean.TRUE);
				servicoAutorizacaoEquipamento.setUltimaAlteracao(Calendar.getInstance().getTime());
			}
		}
		if (listaMateriais != null) {
			servicoAutorizacao.getServicoAutorizacaoMaterial().clear();
			servicoAutorizacao.getServicoAutorizacaoMaterial().addAll(listaMateriais);
			for (ServicoAutorizacaoMaterial servicoAutorizacaoMaterial : servicoAutorizacao.getServicoAutorizacaoMaterial()) {
				servicoAutorizacaoMaterial.setServicoAutorizacao(servicoAutorizacao);
				servicoAutorizacaoMaterial.setHabilitado(Boolean.TRUE);
				servicoAutorizacaoMaterial.setUltimaAlteracao(Calendar.getInstance().getTime());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#listarEquipeServicoAutorizacao(long)
	 */
	@Override
	public Collection<ServicoAutorizacao> listarEquipeServicoAutorizacao(long chavePrimaria) {
		return repositorioServicoAutorizacao.listarEquipeServicoAutorizacao(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#verificarStatusServicoAutorizacao(br.com.ggas.
	 * atendimento. servicoautorizacao.dominio.ServicoAutorizacao, java.lang.String)
	 */
	@Override
	public void verificarStatusServicoAutorizacao(ServicoAutorizacao servico, String tipoOperacao) throws NegocioException {
		String encerrado = obterStatus(Constantes.C_STATUS_SERV_AUT_ENCERRADO).getDescricao();
		String cancelado = obterStatus(Constantes.C_STATUS_SERV_AUT_CANCELADO).getDescricao();
		String aberto = obterStatus(Constantes.C_STATUS_SERV_AUT_ABERTO).getDescricao();
		String pendente = obterStatus(Constantes.C_STATUS_SERV_AUT_PENDENTE).getDescricao();
		String emExecucao = obterStatus(Constantes.C_STATUS_SERV_AUT_EXECUCAO).getDescricao();
		String executado = obterStatus(Constantes.C_STATUS_SERV_AUT_EXECUTADO).getDescricao();

		String status = servico.getStatus().getDescricao();

		verificarStatusServicoAutorizacaoRemanejado(status, tipoOperacao, encerrado, cancelado, pendente, emExecucao, executado);
		verificarStatusServicoAutorizacaoAlterado(status, tipoOperacao, aberto);
		verificarStatusServicoAutorizacaoExecutado(status, tipoOperacao, aberto, emExecucao);
		verificarStatusServicoAutorizacaoEncerrado(status, tipoOperacao, encerrado, cancelado);
	}

	private void verificarStatusServicoAutorizacaoRemanejado(String status, String tipoOperacao,
			String encerrado, String cancelado, String pendente, String emExecucao, String executado) throws NegocioException {

		if (tipoOperacao.equals(Constantes.C_OPERACAO_SERV_AUT_REMANEJADO)
				&& (status.equals(encerrado) ||
					status.equals(cancelado) ||
					status.equals(pendente) ||
					status.equals(emExecucao) ||
					status.equals(executado))) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO, status);
		}
	}

	private void verificarStatusServicoAutorizacaoAlterado(String status, String tipoOperacao, String aberto) throws NegocioException {
		if (tipoOperacao.equals(Constantes.C_OPERACAO_SERV_AUT_ALTERADO) && !status.equals(aberto)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO, status);
		}
	}

	private void verificarStatusServicoAutorizacaoExecutado(String status, String tipoOperacao,
			String aberto, String emExecucao) throws NegocioException {
		if (tipoOperacao.equals(Constantes.C_OPERACAO_SERV_AUT_EXECUTADO)
				&& !(status.equals(aberto) || status.equals(emExecucao))) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO, status);
		}
	}

	private void verificarStatusServicoAutorizacaoEncerrado(String status, String tipoOperacao,
			String encerrado, String cancelado) throws NegocioException {
		if (tipoOperacao.equals(Constantes.C_OPERACAO_SERV_AUT_ENCERRAMENTO)
				&& (status.equals(encerrado) || status.equals(cancelado))) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO, status);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#validarEncerramentoServicoAutorizacao(br.com.ggas.
	 * atendimento .servicoautorizacao.dominio.ServicoAutorizacao)
	 */
	@Override
	public void validarEncerramentoServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) throws NegocioException {
		// Correção: Não irá permitir encerrar por cancelamento, no cenário em que a Autorização de Serviço estiver pendente de atua.
		EntidadeConteudo statusPendente = obterStatus(Constantes.C_STATUS_SERV_AUT_PENDENTE);

		if (servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() != null
				&& "Cancelamento".equals(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getDescricao())
				&& servicoAutorizacao.getStatus().getDescricao().equals(statusPendente.getDescricao())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_MOTIVO_CANCELAMENTO, true);

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#validarMotivoEncerramento(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacao)
	 */
	@Override
	public void validarMotivoEncerramento(ServicoAutorizacao servicoAutorizacao) throws NegocioException {
		if (servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_SEM_MOTIVO);
		}
	}

	/**
	 * Validar dados atualizar.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosAtualizar(ServicoAutorizacao servicoAutorizacao) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (servicoAutorizacao.getDescricao() == null || StringUtils.isEmpty(servicoAutorizacao.getDescricao())
				|| servicoAutorizacao.getDescricao().trim().length() == 0) {
			stringBuilder.append(Constantes.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (servicoAutorizacao.getDataPrevisaoEncerramento() == null) {
			stringBuilder.append(Constantes.DATA_PREVISAO_ENCERRAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
			throw new NegocioException(erros);
		}
	}

	/**
	 * Validar dados remanejamento.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosRemanejamento(ServicoAutorizacao servicoAutorizacao) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (servicoAutorizacao.getDescricao() == null || StringUtils.isEmpty(servicoAutorizacao.getDescricao())
				|| servicoAutorizacao.getDescricao().trim().length() == 0) {
			stringBuilder.append(Constantes.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
			throw new NegocioException(erros);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#verificarEntidadeRelacionadaTelaAtualizacao(br.com
	 * .ggas. atendimento.servicoautorizacao.dominio.ServicoAutorizacao, java.lang.String)
	 */
	@Override
	public void verificarEntidadeRelacionadaTelaAtualizacao(ServicoAutorizacao servicoAutorizacao, String telaAtualizacao)
			throws NegocioException {

		if (telaAtualizacao.equals(Constantes.C_ATUALIZACAO_TELA_CONTRATO) && servicoAutorizacao.getContrato() == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_SEM_VINCULO_COM_SERVICO_TIPO,
					GenericAction.obterMensagem(Contrato.CONTRATO_ROTULO));
		} else if (telaAtualizacao.equals(Constantes.C_ATUALIZACAO_TELA_IMOVEL) && servicoAutorizacao.getImovel() == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_SEM_VINCULO_COM_SERVICO_TIPO,
					GenericAction.obterMensagem(Imovel.IMOVEL_ROTULO));
		} else if (telaAtualizacao.equals(Constantes.C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO)
				&& servicoAutorizacao.getPontoConsumo() == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_SEM_VINCULO_COM_SERVICO_TIPO,
					GenericAction.obterMensagem(PontoConsumo.PONTO_CONSUMO));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#listarServicoAutorizacaoMotivoEncerramento()
	 */
	@Override
	public Collection<EntidadeNegocio> listarServicoAutorizacaoMotivoEncerramento() throws NegocioException {
		return repositorioServicoAutorizacao.obterTodas(ServicoAutorizacaoMotivoEncerramento.class, true);
	}
	
	@Override
	public ServicoAutorizacaoMotivoEncerramento obterServicoAutorizacaoMotivoEncerramentoPelaDescricao(String descricao)
			throws NegocioException {
		return repositorioServicoAutorizacao.obterServicoAutorizacaoMotivoEncerramentoPelaDescricao(
				ServicoAutorizacaoMotivoEncerramento.class, descricao);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterPrevisaoEncerramento(java.lang.Long)
	 */
	@Override
	public String[] obterPrevisaoEncerramento(Long chavePrimaria) throws NegocioException {

		ServicoTipo servicoTipo = controladorServicoTipo.consultarServicoTipoComPrioridade(chavePrimaria);
		String previsao = obterDataPrevisao(servicoTipo);
		return new String[] { servicoTipo.getServicoTipoPrioridade().getDescricao(), previsao,
				String.valueOf(servicoTipo.getServicoTipoPrioridade().getChavePrimaria()) };
	}

	/**
	 * Obter data previsao.
	 *
	 * @param servicoTipo the servico tipo
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String obterDataPrevisao(ServicoTipo servicoTipo) throws NegocioException {

		String previsao = null;
		SimpleDateFormat df = new SimpleDateFormat(PADRAO_FORMATO_DATA_COM_HORA);
		ConstanteSistema util = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIA_UTIL);

		Calendar hoje = Calendar.getInstance();
		hoje.add(Calendar.HOUR_OF_DAY, servicoTipo.getServicoTipoPrioridade().getQuantidadeHorasMaxima());
		Date data = hoje.getTime();

		double qtdHoras = servicoTipo.getServicoTipoPrioridade().getQuantidadeHorasMaxima();
		Double qtdDias = qtdHoras / HORAS_DIA;
		Double modQtdHorasPorHorasDia = qtdHoras % Double.valueOf(HORAS_DIA);
		if (!(modQtdHorasPorHorasDia != null && modQtdHorasPorHorasDia.compareTo(Double.valueOf(0)) == 0)
				&& qtdHoras >= Double.valueOf(HORAS_DIA)) {
			qtdDias++;
			qtdDias = (double) qtdDias.intValue();
		}

		if (servicoTipo.getServicoTipoPrioridade().getIndicadorDiaCorridoUtil().getChavePrimaria() == Long.parseLong(util.getValor())
				&& qtdHoras >= HORAS_DIA) {

			Empresa empresaPrincipal = controladorEmpresas.obterEmpresaPrincipal();

			Cliente municipioCliente =
					controladorCliente.obterCliente(empresaPrincipal.getCliente().getChavePrimaria(), "enderecos");

			Boolean diaUtil = null;

			Calendar diaHoje = Calendar.getInstance();
			Date dataHoje = diaHoje.getTime();

			int proximoDia = 1;
			int i = 0;
			do {
				diaUtil = controladorFeriado.isDiaUtil(dataHoje, municipioCliente.getEnderecoPrincipal().getMunicipio().getChavePrimaria(), Boolean.TRUE);
				if (!diaUtil) {
					Date proximoDiaUtil =
							controladorFeriado.obterProximoDiaUtil(dataHoje, municipioCliente.getEnderecoPrincipal().getMunicipio()
									.getChavePrimaria(), Boolean.TRUE);
					dataHoje = proximoDiaUtil;
					qtdDias++;
				}

				if (!(qtdDias != null && qtdDias.compareTo(Double.valueOf(0)) == 0)) {
					diaHoje.add(Calendar.DAY_OF_WEEK, proximoDia);
					dataHoje = diaHoje.getTime();
				}

				i++;
				// FIXME ROBERTO ALENCAR:
			} while (i <= qtdDias);

			previsao = df.format(dataHoje);
		} else {
			previsao = df.format(data);
		}
		return previsao;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#gerarAutorizacaoServicoLote(br.com.ggas.batch.
	 * acaocomando. dominio.AcaoComando, java.lang.StringBuilder, java.util.Map)
	 */
	@Override
	public void gerarAutorizacaoServicoLote(long idComando, StringBuilder logProcessamento, Map<String, Object> dados) throws GGASException, IOException {
		AcaoComandoEstendida acaoComandoEstendida = controladorAcaoComando.obterAcaoComando(idComando, 
				"acao", "acao.servicoTipo", "grupoFaturamento", "localidade", "setorComercial", "municipio", 
				"marcaMedidor", "modeloMedidor", "marcaCorretor", "modeloCorretor", "situacaoConsumo", "segmento");

		Collection<PontoConsumo> pontosConsumo = obterPontosConsumo(acaoComandoEstendida);
		Collection<ServicoAutorizacaoVO> listagemAS = gerarAutorizacaoServicoLote(pontosConsumo, dados,
				logProcessamento, acaoComandoEstendida.getAcao(), acaoComandoEstendida.getIndicadorSimulacao());
		
		this.gerarRelatorioListagemAS(listagemAS, dados);
	}

	private Collection<PontoConsumo> obterPontosConsumo(AcaoComandoEstendida acaoComandoEstendida) throws NegocioException {
		Collection<AcaoComandoPontoConsumo> listaAcaoComandoPontoConsumo = controladorAcaoComando
				.pesquisarPontosConsumoPorAcaoComando(acaoComandoEstendida.getChavePrimaria());

		Collection<PontoConsumo> pontosConsumo = null;
		if (listaAcaoComandoPontoConsumo != null && !listaAcaoComandoPontoConsumo.isEmpty()) {
			pontosConsumo = new ArrayList<PontoConsumo>();

			for (AcaoComandoPontoConsumo acaoComandoPontoConsumo : listaAcaoComandoPontoConsumo) {
				pontosConsumo.add(acaoComandoPontoConsumo.getPontoConsumo());
			}
		} else {
			pontosConsumo = controladorPontoConsumo.consultarPontoConsumoPorComandoAcao(acaoComandoEstendida);
		}

		return pontosConsumo;
	}

	private Collection<ServicoAutorizacaoVO> gerarAutorizacaoServicoLote(Collection<PontoConsumo> pontosConsumo, Map<String, Object> dados, 
			StringBuilder logProcessamento, Acao acao, boolean indicadorSimulacao) throws NegocioException {
		
		int contadorGeradas = 0;
		int contadorNaoGeradas = 0;
		Collection<ServicoAutorizacaoVO> listaServicoAutorizacaoVO = new ArrayList<ServicoAutorizacaoVO>();
		if (acao.getIndicadorGerarAutorizacaoServico()) {

			if (acao.getServicoTipo().getIndicadorGeraLote()) {

				for (PontoConsumo pontoConsumo : pontosConsumo) {
					ServicoAutorizacao servicoAutorizacao = criarServicoAutorizacao(acao, pontoConsumo);

					try {

						this.repositorioServicoAutorizacao.validarDadosEntidade(servicoAutorizacao);

						if (!indicadorSimulacao) {
							this.inserirServicoAutorizacao(servicoAutorizacao, dados, Boolean.FALSE);
						}

						logProcessamento.append(Constantes.PULA_LINHA);
						logProcessamento.append("Autorização de Serviço gerada para o ponto de consumo: ");
						logProcessamento.append(pontoConsumo.getDescricaoFormatada());
						
						ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
						servicoAutorizacaoVO.setNomeCliente(servicoAutorizacao.getCliente().getNome());
						servicoAutorizacaoVO.setDescricaoServicoTipo(servicoAutorizacao.getServicoTipo().getDescricao());
						servicoAutorizacaoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricaoFormatada());
						
						listaServicoAutorizacaoVO.add(servicoAutorizacaoVO);
						
						contadorGeradas++;

					} catch (Exception e) {

						LOG.error(e.getMessage(), e);

						logProcessamento.append(Constantes.PULA_LINHA);
						logProcessamento.append("Autorização de Serviço não gerada para o ponto de consumo: ");
						logProcessamento.append(pontoConsumo.getDescricaoFormatada());

						contadorNaoGeradas++;
					}
				}

				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Total de pontos de consumo processados: ");
				logProcessamento.append(pontosConsumo.size());

				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Total de autorizações de serviço geradas: ");
				logProcessamento.append(contadorGeradas);

				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Total de autorizações de serviço não geradas: ");
				logProcessamento.append(contadorNaoGeradas);
			} else {
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Tipo de Serviço não permite gerar autorizações de serviço em lote.");
			}
		} else {
			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("Ação não não permite gerar autorizações de serviço em lote.");
		}
		
		return listaServicoAutorizacaoVO;
	}

	private ServicoAutorizacao criarServicoAutorizacao(ItemLoteComunicacao item) throws NegocioException {
		return this.criarServicoAutorizacao(item.getLoteComunicacao().getServicoTipo(), 
				item.getServicoTipoAgendamentoTurno().getEquipe(), 
				item.getPontoConsumo());
	}
	
	private ServicoAutorizacao criarServicoAutorizacao(Acao acao, PontoConsumo pontoConsumo) throws NegocioException {
		return this.criarServicoAutorizacao(acao.getServicoTipo(), acao.getEquipe(), pontoConsumo);
	}
	
	private ServicoAutorizacao criarServicoAutorizacao(ServicoTipo servicoTipo, Equipe equipe, PontoConsumo pontoConsumo) throws NegocioException {
		ServicoAutorizacao servicoAutorizacao = new ServicoAutorizacao();
		servicoAutorizacao.setDataGeracao(Calendar.getInstance().getTime());
		servicoAutorizacao.setServicoTipo(servicoTipo);
		servicoAutorizacao.setDataPrevisaoEncerramento(DataUtil.converterParaData(obterDataPrevisao(servicoTipo)));
		servicoAutorizacao.setEquipe(equipe);
		servicoAutorizacao.setServicoTipoPrioridade(servicoTipo.getServicoTipoPrioridade());

		Cliente cliente = controladorCliente.obterClientePorPontoConsumo(pontoConsumo.getChavePrimaria());
		this.adicionarClienteAoServicoAutorizacao(cliente, servicoAutorizacao);

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

		this.adicionarContratoPontoDeConsumoEmServicoAutorizacao(servicoAutorizacao, contratoPontoConsumo);

		servicoAutorizacao.getServicoAutorizacaoMaterial()
				.addAll(converterListaMaterial(servicoTipo.getListaMateriais(), servicoAutorizacao));


		servicoAutorizacao.getServicoAutorizacaoEquipamento()
				.addAll(converterListaEquipamento(servicoTipo.getListaEquipamentoEspecial(), servicoAutorizacao));
		servicoAutorizacao.setImovel(pontoConsumo.getImovel());
		servicoAutorizacao.setPontoConsumo(pontoConsumo);
		servicoAutorizacao.setDescricao("Autorização de Serviço Gerada em Lote");
		servicoAutorizacao.setStatus(obterStatus(Constantes.C_STATUS_SERV_AUT_ABERTO));
		return servicoAutorizacao;
	}
	
	private void adicionarContratoPontoDeConsumoEmServicoAutorizacao(ServicoAutorizacao servicoAutorizacao,
			ContratoPontoConsumo contratoPontoConsumo) {
		Contrato contrato = null;

		if (contratoPontoConsumo != null) {
			contrato = contratoPontoConsumo.getContrato();
		}
		if (contrato != null) {
			servicoAutorizacao.setContrato(contrato);
		}
	}

	private void adicionarClienteAoServicoAutorizacao(Cliente cliente, ServicoAutorizacao servicoAutorizacao) {
		if (cliente != null) {
			servicoAutorizacao.setCliente(cliente);
		}
	}

	private List<ServicoAutorizacaoMaterial> converterListaMaterial(Collection<ServicoTipoMaterial> materialList,
			ServicoAutorizacao servicoAutorizacao) {

		List<ServicoAutorizacaoMaterial> servicoAutorizacaoMaterialList = new ArrayList<>();
		ServicoAutorizacaoMaterial servicoAutorizacaoMaterial = null;
		for (ServicoTipoMaterial material : materialList) {
			servicoAutorizacaoMaterial = new ServicoAutorizacaoMaterial();
			servicoAutorizacaoMaterial.setMaterial(material.getMaterial());
			servicoAutorizacaoMaterial.setQuantidadeMaterial(Integer.valueOf(String.valueOf(material.getQuantidadeMaterial())));
			servicoAutorizacaoMaterial.setServicoAutorizacao(servicoAutorizacao);
			servicoAutorizacaoMaterial.setHabilitado(Boolean.TRUE);
			servicoAutorizacaoMaterial.setVersao(0);
			servicoAutorizacaoMaterial.setUltimaAlteracao(Calendar.getInstance().getTime());
			servicoAutorizacaoMaterialList.add(servicoAutorizacaoMaterial);

		}
		return servicoAutorizacaoMaterialList;
	}

	/**
	 * Converter lista equipamento.
	 *
	 * @param equipamentoList the equipamento list
	 * @param servicoAutorizacao the servico autorizacao
	 * @return the list
	 */
	public List<ServicoAutorizacaoEquipamento> converterListaEquipamento(Collection<ServicoTipoEquipamento> equipamentoList,
			ServicoAutorizacao servicoAutorizacao) {

		List<ServicoAutorizacaoEquipamento> servicoAutorizacaoEquipamentoList = new ArrayList<>();
		ServicoAutorizacaoEquipamento servicoAutorizacaoEquipamento = null;
		for (ServicoTipoEquipamento equipamento : equipamentoList) {
			servicoAutorizacaoEquipamento = new ServicoAutorizacaoEquipamento();
			servicoAutorizacaoEquipamento.setEquipamento(equipamento.getEquipamento());
			servicoAutorizacaoEquipamento.setServicoAutorizacao(servicoAutorizacao);
			servicoAutorizacaoEquipamento.setUltimaAlteracao(Calendar.getInstance().getTime());
			servicoAutorizacaoEquipamento.setVersao(0);
			servicoAutorizacaoEquipamento.setHabilitado(Boolean.TRUE);
			servicoAutorizacaoEquipamentoList.add(servicoAutorizacaoEquipamento);
		}
		return servicoAutorizacaoEquipamentoList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#gerarRelatorio(java.lang.Long)
	 */
	@Override
	public byte[] gerarRelatorio(Long chavePrimariaServicoAutorizacao, Boolean comChamado, Collection<ServicoAutorizacaoHistorico> historico)
			throws GGASException {

		ServicoAutorizacao servicoAutorizacao = repositorioServicoAutorizacao.obterServicoAutorizacao(chavePrimariaServicoAutorizacao);
		
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> parametros = new HashMap<>();
		
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();

		parametros.put(IMAGEM,
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		parametros.put("enderecoEmpresa", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatado());

		
		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
		Collection<Object> collRelatorio = new ArrayList<>();

		byte[] retorno = null;

		if (servicoAutorizacao.getServicoTipo().getFormulario() != null) {
			Questionario questionario =
					controladorQuestionario.consultarPorCodico(servicoAutorizacao.getServicoTipo().getFormulario().getChavePrimaria(),
							LISTA_PERGUNTAS, "tipoServico", "segmento");

			controladorQuestionario.obterAlternativas(questionario.getChavePrimaria());

			List<QuestionarioPergunta> listaPerguntas = new ArrayList<>(questionario.getListaPerguntas());
			Collections.sort(listaPerguntas);

			if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_OPERACOES)) {
				ServicoAutorizacaoVORelatorio relatorio =
						this.converterServicoAutorizacaoParaQuestionarioVO(servicoAutorizacao, null, questionario, comChamado, historico);

				collRelatorio.add(relatorio);
				retorno =
						RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO_OPERACOES, formatoImpressao);
			} else {
				ServicoAutorizacaoVORelatorio relatorio =
						this.converterServicoAutorizacaoParaQuestionarioVO(servicoAutorizacao, null, questionario, comChamado, historico);
				collRelatorio.add(relatorio);
				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO, formatoImpressao);
			}

		} else {
			
			if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_BAHIAGAS)) {

				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVOBahiagas(servicoAutorizacao);
				collRelatorio.add(relatorio);
				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO_BAHIAGAS, formatoImpressao);

			} else if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_BAHIAGAS_GEOPE)) {
				popularDadosChamado(parametros, servicoAutorizacao);
				retorno =
						RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO_BAHIAGAS_GEOPE,
								formatoImpressao);
			} else if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_OPERACOES)) {
				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVO(servicoAutorizacao, null, comChamado);

				collRelatorio.add(relatorio);
				retorno =
						RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO_OPERACOES, formatoImpressao);
			} else if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_ALGAS)) {
				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVOAlgas(servicoAutorizacao);
				
				ServicoAutorizacaoTemp servicoAutorizacaoTemp = repositorioServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);

				ServicoAutorizacaoTempAnexo servicoAutorizacaoTempAnexo = this.consultarAssinaturaAnexoAutorizacaoServicoTemp(servicoAutorizacao);

				ContratoPontoConsumo contratoPontoConsumo = null;
				
				if (servicoAutorizacao.getPontoConsumo() != null) {
					contratoPontoConsumo = controladorContrato.obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(
							servicoAutorizacao.getPontoConsumo().getChavePrimaria());
				}
				
				StringBuilder pressaoContrato = new StringBuilder();
				
				if(contratoPontoConsumo != null) {
					pressaoContrato.append(contratoPontoConsumo.getMedidaPressao());
					pressaoContrato.append(" ");
					pressaoContrato.append(contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());
				}
				
				relatorio.setPressaoContrato(pressaoContrato.toString());

				if(servicoAutorizacaoTempAnexo != null && Util.checarSeArquivoExiste(servicoAutorizacaoTempAnexo.getCaminho())) 
					parametros.put("assinaturaAlgas",servicoAutorizacaoTempAnexo.getCaminho());
				
				if(servicoAutorizacaoTemp != null) {
					parametros.put("nomeCliente",servicoAutorizacaoTemp.getIdentificacao());
				}else {
					parametros.put("nomeCliente","");
				}
				
				relatorio = this.montarRelatorioTemp(relatorio, servicoAutorizacaoTemp);

				
				collRelatorio.add(relatorio);
				
				preencherDadosRemocaoMedidor(servicoAutorizacaoTemp, servicoAutorizacao, relatorio);
				
				StringBuilder descricaoServico = new StringBuilder();
				descricaoServico.append("REGISTRO DE ");
				descricaoServico.append(servicoAutorizacao.getServicoTipo().getDescricao().split(" ")[0]);
				descricaoServico.append(" NO FORNECIMENTO DE GÁS");
				
				parametros.put("descricaoServico", descricaoServico.toString());

				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO_ALGAS, formatoImpressao);
				
			} else if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_INICIO_FORNECIMENTO_ALGAS)
					|| servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
							.equals(RELATORIO_SERVICO_AUTORIZACAO_ADEQUACAO_ALGAS)
					|| servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
							.equals(RELATORIO_SERVICO_AUTORIZACAO_INSTALACAO_EQUIPAMENTO_ALGAS)) {
				
				ServicoAutorizacaoTemp servicoAutorizacaoTemp = repositorioServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);
				
				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoInicioFornecimentoParaVOAlgas(servicoAutorizacao, servicoAutorizacaoTemp);
				

				ServicoAutorizacaoTempAnexo servicoAutorizacaoTempAnexo = this.consultarAssinaturaAnexoAutorizacaoServicoTemp(servicoAutorizacao);
				
				ContratoPontoConsumo contratoPontoConsumo = null;
				
				if (servicoAutorizacao.getPontoConsumo() != null) {
					contratoPontoConsumo = controladorContrato.obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(
							servicoAutorizacao.getPontoConsumo().getChavePrimaria());
				}
				
				Empresa codigoClienteEmpresa = repositorioServicoAutorizacao.consultarCodigoClienteEmpresa();
				
				String nomeMunicipio = "";
				if(codigoClienteEmpresa != null) {
					if(codigoClienteEmpresa.getCliente() != null) {
							nomeMunicipio = repositorioServicoAutorizacao.consultarMunicipioEmpresa(codigoClienteEmpresa.getCliente().getChavePrimaria());
					}
				}
				parametros.put("nomeMunicipio", nomeMunicipio);
				
				
				StringBuilder pressaoContrato = new StringBuilder();
				
				if(contratoPontoConsumo != null) {
					pressaoContrato.append(contratoPontoConsumo.getMedidaPressao());
					pressaoContrato.append(" ");
					pressaoContrato.append(contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());
				}
				
				relatorio.setPressaoContrato(pressaoContrato.toString());

				if(servicoAutorizacaoTempAnexo != null && Util.checarSeArquivoExiste(servicoAutorizacaoTempAnexo.getCaminho()))
					parametros.put("assinaturaAlgas",servicoAutorizacaoTempAnexo.getCaminho());
				
				if(servicoAutorizacaoTemp != null) {
					parametros.put("nomeCliente",servicoAutorizacaoTemp.getIdentificacao());
				}else {
					parametros.put("nomeCliente","");
				}
				
				relatorio = this.montarRelatorioTemp(relatorio, servicoAutorizacaoTemp);
				
				if(servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_INICIO_FORNECIMENTO_ALGAS) 
					&& servicoAutorizacaoTemp == null) {
					//Sobrescrever campos de numero de serie e leitura inicial em caso de inicio de fornecimento
					relatorio = this.montarRelatorioInicioFornecimento(relatorio, servicoAutorizacao);
				}
				
				collRelatorio.add(relatorio);

				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros,
						servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo(),
						formatoImpressao);
				
			} else if (servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_ATENDIMENTO_ALGAS) || servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_INSPECAO_LOCAL_ALGAS)) {
				
				ServicoAutorizacaoVORelatorio relatorio = new ServicoAutorizacaoVORelatorio();
				montarInformacaoComumRelatorioAlgas(relatorio, servicoAutorizacao);
				
				ServicoAutorizacaoTemp servicoAutorizacaoTemp = repositorioServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);
				ServicoAutorizacaoTempAnexo servicoAutorizacaoTempAnexo = this.consultarAssinaturaAnexoAutorizacaoServicoTemp(servicoAutorizacao);
				ContratoPontoConsumo contratoPontoConsumo = null;
				
				if (servicoAutorizacao.getPontoConsumo() != null) {
					contratoPontoConsumo = controladorContrato.obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(
							servicoAutorizacao.getPontoConsumo().getChavePrimaria());
				}
				ServicoAutorizacaoTempAnexo servicoAutorizacaoTempAnexoFotoCRM = this.consultarFotoCRM(servicoAutorizacao);
				
				StringBuilder pressaoContrato = new StringBuilder();
				
				if(contratoPontoConsumo != null) {
					pressaoContrato.append(contratoPontoConsumo.getMedidaPressao());
					pressaoContrato.append(" ");
					pressaoContrato.append(contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());
				}
				
				relatorio.setPressaoContrato(pressaoContrato.toString());
				
				if(servicoAutorizacaoTempAnexo != null && Util.checarSeArquivoExiste(servicoAutorizacaoTempAnexo.getCaminho()))
					parametros.put("assinaturaAlgas",servicoAutorizacaoTempAnexo.getCaminho());
				
				if(servicoAutorizacaoTemp != null) {
					parametros.put("nomeCliente",servicoAutorizacaoTemp.getIdentificacao());
				}else {
					parametros.put("nomeCliente","");
				}
				
				relatorio = this.montarRelatorioTemp(relatorio, servicoAutorizacaoTemp);
				
				collRelatorio.add(relatorio);
				
				
				if(servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_INSPECAO_LOCAL_ALGAS)) {
					parametros.put("imagemInspecao", Constantes.URL_IMAGENS_GGAS + "inspecaoLocal.png");
				} else {
					if(servicoAutorizacaoTempAnexoFotoCRM != null && Util.checarSeArquivoExiste(servicoAutorizacaoTempAnexoFotoCRM.getCaminho()))
						parametros.put("imagemDetalheVazamento",servicoAutorizacaoTempAnexoFotoCRM.getCaminho());
					else
						parametros.put("imagemDetalheVazamento", Constantes.URL_IMAGENS_GGAS + "detalhesVazamento.png");
				}

				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo(), formatoImpressao);
				
			} else if(servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_DESLIGAMENTO_RIF_ALGAS)) {
				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVOAlgas(servicoAutorizacao);
				StringBuilder pressaoContrato = new StringBuilder();
				
				ContratoPontoConsumo contratoPontoConsumo = null;
				
				if (servicoAutorizacao.getPontoConsumo() != null) {
					contratoPontoConsumo = controladorContrato.obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(
							servicoAutorizacao.getPontoConsumo().getChavePrimaria());
				}
				
				if(contratoPontoConsumo != null) {
					pressaoContrato.append(contratoPontoConsumo.getMedidaPressao());
					pressaoContrato.append(" ");
					pressaoContrato.append(contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());
				}
				
				relatorio.setPressaoContrato(pressaoContrato.toString());
				// Dados adicionais relatório temp
				ServicoAutorizacaoTemp servicoAutorizacaoTemp = repositorioServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);

				ServicoAutorizacaoTempAnexo servicoAutorizacaoTempAnexo = this.consultarAssinaturaAnexoAutorizacaoServicoTemp(servicoAutorizacao);
				
				if(servicoAutorizacaoTempAnexo != null && Util.checarSeArquivoExiste(servicoAutorizacaoTempAnexo.getCaminho()))
					parametros.put("assinaturaAlgas",servicoAutorizacaoTempAnexo.getCaminho());
				
				if(servicoAutorizacaoTemp != null) {
					parametros.put("nomeCliente",servicoAutorizacaoTemp.getIdentificacao());
				}else {
					parametros.put("nomeCliente","");
				}
				
				relatorio = this.montarRelatorioTemp(relatorio, servicoAutorizacaoTemp);
				// Fim dos dados adicionais
				
				collRelatorio.add(relatorio);
				
				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo(), formatoImpressao);

			}
			else if(servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
					.equals(RELATORIO_SERVICO_AUTORIZACAO_COLETA_GAS)) {
				ServicoAutorizacaoAgenda servicoAutorizacaoAgenda = this.repositorioServicoAutorizacao.consultarServicoAutorizacaoAgendaPorChave(servicoAutorizacao.getChavePrimaria());
				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVO(servicoAutorizacao, servicoAutorizacaoAgenda, comChamado);
				
				// Dados adicionais relatório temp e coleta gas
				relatorio = this.montarRelatorioColetaGas(servicoAutorizacao, relatorio);
				
				ServicoAutorizacaoTemp servicoAutorizacaoTemp = repositorioServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);
				
				relatorio = this.montarRelatorioTemp(relatorio, servicoAutorizacaoTemp);
				
				// Fim dos dados adicionais
				
				collRelatorio.add(relatorio);
				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo(), formatoImpressao);
				
			}
			
			else {
				ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVO(servicoAutorizacao, null, comChamado);
				collRelatorio.add(relatorio);
				retorno =
						RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_SERVICO_AUTORIZACAO_S_QUESTIONARIO,
								formatoImpressao);
			}

		}
		return retorno;
	}

	private void popularDadosChamado(Map<String, Object> parametros, ServicoAutorizacao servicoAutorizacao) {

		if (servicoAutorizacao.getChamado() != null) {
			if (servicoAutorizacao.getChamado().getProtocolo() != null) {
				parametros.put("numeroChamado", servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo().toString());

				parametros.put("dataAbertura", DataUtil.converterDataParaString(servicoAutorizacao.getChamado().getProtocolo()
						.getUltimaAlteracao()));

				parametros.put("horaAbertura",
						Util.obterHoraData(servicoAutorizacao.getChamado().getProtocolo().getUltimaAlteracao(), Boolean.FALSE));
			}

			if (servicoAutorizacao.getChamado().getChamadoAssunto() != null
					&& servicoAutorizacao.getChamado().getChamadoAssunto().getChamadoTipo() != null) {
				parametros.put("tipoChamado", servicoAutorizacao.getChamado().getChamadoAssunto().getChamadoTipo().getDescricao());
			}
		}

		if (servicoAutorizacao.getCliente() != null && servicoAutorizacao.getCliente().getEnderecoPrincipal() != null) {
			parametros.put("enderecoCliente", servicoAutorizacao.getCliente().getEnderecoPrincipal()
					.getEnderecoFormatadoRuaNumeroBairroMunicipioUF());
		}

		popularSegmento(servicoAutorizacao, parametros);
	}
	
	private void preencherDadosRemocaoMedidor(ServicoAutorizacaoTemp servicoAutorizacaoTemp, ServicoAutorizacao servicoAutorizacao, ServicoAutorizacaoVORelatorio relatorio) throws NegocioException {
		
		String servicoTipo = servicoAutorizacao.getServicoTipo().getDescricao();
		String statusAS = servicoAutorizacao.getStatus().getDescricao();
		
		if(servicoAutorizacaoTemp == null && servicoAutorizacao.getPontoConsumo() != null && servicoTipo.equals("REMOCAO CRM") 
				&& (!statusAS.equals("CANCELADA") && !statusAS.equals("ABERTA")) ) {
			
			
			PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
			Collection<InstalacaoMedidor> instalacaoMedidor = controladorMedidor.consultarInstalacaoMedidor(null, pontoConsumo.getChavePrimaria());
			
			Medidor medidor = instalacaoMedidor != null && instalacaoMedidor.size() > 0 ? 
					instalacaoMedidor.stream().max(Comparator.comparing(InstalacaoMedidor::getData)).get().getMedidor() : null;
					if(medidor != null) {
					relatorio.setMedidorSerie(medidor.getNumeroSerie());
					relatorio.setMedidorTipo(medidor.getTipoMedidor() != null ? medidor.getTipoMedidor().getDescricao() : "");
					relatorio.setMedidorModelo(medidor.getModelo() != null ? medidor.getModelo().getDescricao() : "");
					relatorio.setMedidorFabricante(medidor.getMarcaMedidor() != null ? medidor.getMarcaMedidor().getDescricao() : "");
					relatorio.setServicoTipo(servicoTipo);

					FiltroHistoricoOperacaoMedidor filtroHistorico = new FiltroHistoricoOperacaoMedidor();
					filtroHistorico.setIdPontoConsumo(servicoAutorizacao.getPontoConsumo().getChavePrimaria());
					filtroHistorico.setOrdenacaoDataAlteracao(Ordenacao.DESCENDENTE);
					filtroHistorico.setLimiteResultados(1);
					HistoricoOperacaoMedidor historicoMedidorRemovido = controladorMedidor.obterUnicoHistoricoOperacaoMedidor(filtroHistorico);
					relatorio.setNumeroLeituraOriginal(historicoMedidorRemovido.getNumeroLeitura() != null ? historicoMedidorRemovido.getNumeroLeitura().toString() : "");
				
					}		
		
		}
	}
	
	
	private ServicoAutorizacaoVORelatorio converterServicoAutorizacaoParaVOBahiagas(ServicoAutorizacao servicoAutorizacao) {

		ServicoAutorizacaoVORelatorio relatorio = new ServicoAutorizacaoVORelatorio();

		relatorio.setCodigo(String.valueOf(servicoAutorizacao.getChavePrimaria()));

		if (servicoAutorizacao.getDataGeracao() != null) {
			relatorio.setDataGeracao(Util.converterDataParaStringSemHora(servicoAutorizacao.getDataGeracao(), Constantes.FORMATO_DATA_BR));
		}

		Imovel imovel = servicoAutorizacao.getImovel();

		if (imovel != null) {
			relatorio.setNomeFantasiaImovel(imovel.getNome());
			relatorio.setComplementoImovel(imovel.getDescricaoComplemento());
		}

		Contrato contrato = servicoAutorizacao.getContrato();

		if (contrato != null) {

			Cliente cliente = contrato.getClienteAssinatura();

			if (cliente != null) {

				relatorio.setNomeCliente(cliente.getNome());
				Collection<ClienteFone> fones = cliente.getFones();

				for (ClienteFone fone : fones) {

					if (fone.getIndicadorPrincipal()) {

						StringBuilder foneCompleto = new StringBuilder();
						foneCompleto.append("(");
						foneCompleto.append(fone.getCodigoDDD());
						foneCompleto.append(")");
						foneCompleto.append(fone.getNumero());

						if (fone.getRamal() != null) {
							foneCompleto.append("Ramal: ");
							foneCompleto.append(fone.getRamal());
						}

						relatorio.setFoneCliente(foneCompleto.toString());
					}
				}
			}
		}

		Chamado chamado = servicoAutorizacao.getChamado();

		if (chamado != null && chamado.getProtocolo() != null) {
			relatorio.setProtocoloChamado(chamado.getProtocolo().getNumeroProtocolo().toString());
		}

		return relatorio;
	}

	/**
	 * Converter servico autorizacao para vo.
	 *
	 * @param servicoAutorizacao the servico autorizacao
	 * @param agenda the agenda
	 * @param comChamado
	 * @return the servico autorizacao vo relatorio
	 */
	private ServicoAutorizacaoVORelatorio converterServicoAutorizacaoParaVO(ServicoAutorizacao servicoAutorizacao,
			ServicoAutorizacaoAgenda agenda, Boolean comChamado) {

		ServicoAutorizacaoVORelatorio relatorio = new ServicoAutorizacaoVORelatorio();

		relatorio.setCodigo(String.valueOf(servicoAutorizacao.getChavePrimaria()));

		relatorio.setComChamado(comChamado);

		relatorio.setDescricao(servicoAutorizacao.getDescricao());
		
		if (servicoAutorizacao.getDataGeracao() != null) {
			relatorio.setDataGeracao(servicoAutorizacao.getDataGeracaoFormatada());
		}
		if (servicoAutorizacao.getDataEncerramento() != null) {
			relatorio.setDataEncerramento(servicoAutorizacao.getDataEncerramentoFormatada());
		}
		if (servicoAutorizacao.getDataPrevisaoEncerramento() != null) {
			relatorio.setDataPrevisaoEncerramento(servicoAutorizacao.getDataPrevisaoEncerramentoFormatada());
		}
		if (servicoAutorizacao.getServicoTipo() != null) {
			relatorio.setServicoTipo(servicoAutorizacao.getServicoTipo().getDescricao());
		}
		if (servicoAutorizacao.getServicoTipo() != null && servicoAutorizacao.getServicoTipo().getServicoTipoPrioridade() != null) {
			relatorio.setPrioridadeServicoTipo(servicoAutorizacao.getServicoTipo().getServicoTipoPrioridade().getDescricao());
		}
		if (servicoAutorizacao.getEquipe() != null) {
			relatorio.setEquipe(servicoAutorizacao.getEquipe().getNome());
		}
		if (servicoAutorizacao.getChamado() != null && servicoAutorizacao.getChamado().getProtocolo() != null) {
			relatorio.setProtocoloChamado(String.valueOf(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo()));
		}
		if (servicoAutorizacao.getStatus() != null) {
			relatorio.setStatus(servicoAutorizacao.getStatus().getDescricao());
		}
		if (servicoAutorizacao.getCliente() != null) {
			relatorio.setNomeCliente(servicoAutorizacao.getCliente().getNome());
			relatorio.setNomeFantasiaCliente(servicoAutorizacao.getCliente().getNomeFantasia());
			relatorio.setPassaporte(servicoAutorizacao.getCliente().getNumeroPassaporte());

			if (servicoAutorizacao.getCliente().getCpf() != null && !servicoAutorizacao.getCliente().getCpf().isEmpty()) {
				relatorio.setCpfCnpj(servicoAutorizacao.getCliente().getCpfFormatado());
			} else if (servicoAutorizacao.getCliente().getCnpj() != null && !servicoAutorizacao.getCliente().getCnpj().isEmpty()) {
				relatorio.setCpfCnpj(servicoAutorizacao.getCliente().getCnpjFormatado());
			}

			if (servicoAutorizacao.getCliente().getFones() != null && !servicoAutorizacao.getCliente().getFones().isEmpty()) {
				relatorio.setListaClienteFoneTO(this.converterTelefoneParaTelefoneVO(servicoAutorizacao.getCliente().getFones()));
			} else {
				relatorio.setListaClienteFoneTO(null);
			}
		}

		if (servicoAutorizacao.getContrato() != null) {
			relatorio.setNumeroContrato(servicoAutorizacao.getContrato().getNumeroFormatado());
		}
		if (servicoAutorizacao.getImovel() != null) {
			relatorio.setMatriculaImovel(servicoAutorizacao.getImovel().getNumeroImovel());
			relatorio.setNomeFantasiaImovel(servicoAutorizacao.getImovel().getNome());
			relatorio.setCepImovel(servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getCep());
			relatorio.setEnderecoImovel(servicoAutorizacao.getImovel().getEnderecoFormatado());
		}
		if (servicoAutorizacao.getPontoConsumo() != null) {
			relatorio.setDescricaoPontoConsumo(servicoAutorizacao.getPontoConsumo().getDescricao());
		}
		if (servicoAutorizacao.getServicoAutorizacaoHistoricos() != null && !servicoAutorizacao.getServicoAutorizacaoHistoricos().isEmpty()) {
			relatorio
					.setListaServicoAutorizacaoHistorico(converterServicoAutorizacaoHistoricoParaServicoAutorizacaoHistoricoVO(servicoAutorizacao
							.getServicoAutorizacaoHistoricos()));
		}
		if (servicoAutorizacao.getServicoAutorizacaoMaterial() != null && !servicoAutorizacao.getServicoAutorizacaoMaterial().isEmpty()) {
			relatorio.setListaMaterial(converterMaterialParaMaterialVO(servicoAutorizacao.getServicoAutorizacaoMaterial()));
		}
		if (servicoAutorizacao.getServicoAutorizacaoEquipamento() != null
				&& !servicoAutorizacao.getServicoAutorizacaoEquipamento().isEmpty()) {
			relatorio.setListaEquipamento(converterEquipamentoParaEquipamentoVO(servicoAutorizacao.getServicoAutorizacaoEquipamento()));
		}

		if (agenda != null) {
			relatorio.setTurno(agenda.getTurno().getDescricao());
			relatorio.setDataAgendamento(Util.converterDataParaStringSemHora(agenda.getDataAgenda(), Constantes.FORMATO_DATA_BR));
			relatorio.setConfirmado(agenda.getIndicadorConfirmado().toString());
			relatorio.setHorario(DataUtil.converterDataParaString(agenda.getDataAgenda(), Boolean.TRUE));
		}

		if (comChamado != null && comChamado) {
			relatorio.setProtocoloChamado(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo().toString());
			relatorio.setListaChamadoHistorico(popularChamdoHistorico(servicoAutorizacao));
		}
		
		String motivoEncerramento = "";
		String situacao = "";
		
		if(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() != null) {
			motivoEncerramento = servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getDescricao();
		}
		
		Collection<ServicoAutorizacaoHistorico> servicoAutorizacaoHistoricos = servicoAutorizacao.getServicoAutorizacaoHistoricos();
		List<ServicoAutorizacaoHistorico> listaAutorizacaoHistoricos = new ArrayList<>(servicoAutorizacaoHistoricos);
		if (!listaAutorizacaoHistoricos.isEmpty()) {
			listaAutorizacaoHistoricos.sort(Comparator.comparing(ServicoAutorizacaoHistorico::getUltimaAlteracao));
			ServicoAutorizacaoHistorico ultimoItemHistorico = listaAutorizacaoHistoricos.get(listaAutorizacaoHistoricos.size() - 1);
			if(ultimoItemHistorico.getStatus()!= null) {
				situacao = ultimoItemHistorico.getStatus().getDescricao();
			}
		}
		
		relatorio.setSituacao(situacao);
		relatorio.setMotivoEncerramento(motivoEncerramento);

		return relatorio;
	}
	/**
	 * montar relatorio coleta gas
	 * @param servicoAutorizacao
	 * @param relatorio
	 * @return relatorio
	 */
	private ServicoAutorizacaoVORelatorio montarRelatorioColetaGas(ServicoAutorizacao servicoAutorizacao, ServicoAutorizacaoVORelatorio relatorio) {
		
		if(servicoAutorizacao != null) {
			String localColeta = "";
			String localEntrega = "";
			if(servicoAutorizacao.getImovel() != null) {
				Erp erp = consultarErpPorChaveImovel(servicoAutorizacao.getImovel().getChavePrimaria());
				if(erp != null) {
					localColeta = erp.getNome();
				}
				if(servicoAutorizacao.getImovel().getQuadraFace() != null) {
					if(servicoAutorizacao.getImovel().getQuadraFace().getEndereco() != null) {
						if(servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep() != null) {
								localEntrega = 
										(servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getTipoLogradouro() == null ? 
										"" : (servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getTipoLogradouro()	+ " ")) +
										(servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getLogradouro() == null ? 
										"" : (servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getLogradouro() + ", ")) +
										(servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getBairro() == null ? 
										"" : (servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getBairro()));	
						}
					}
				}
			}
			relatorio.setLocalColeta(localColeta);
			relatorio.setLocalEntrega(localEntrega);
			
			if(servicoAutorizacao.getServicoTipo() != null) {
				relatorio.setServicoOrientacao(servicoAutorizacao.getServicoTipo().getOrientacao());
			}
			if(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() != null) {
				if(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getDescricao() != null) {
					relatorio.setAtividadeExecutada(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getDescricao());
				}else {
					relatorio.setAtividadeExecutada("");
				}
			}else {
				relatorio.setAtividadeExecutada("");
			}
		}
		
		return relatorio;
	}
	
	/**
	 * @param servicoAutorizacao
	 * @param agenda
	 * @param questionario
	 * @param historico
	 * @param comChamado
	 * @return
	 */
	private ServicoAutorizacaoVORelatorio converterServicoAutorizacaoParaQuestionarioVO(ServicoAutorizacao servicoAutorizacao,
			ServicoAutorizacaoAgenda agenda, Questionario questionario, Boolean comChamado,
			Collection<ServicoAutorizacaoHistorico> historico) {

		ServicoAutorizacaoVORelatorio relatorio = new ServicoAutorizacaoVORelatorio();

		relatorio.setComChamado(comChamado);

		relatorio.setCodigo(String.valueOf(servicoAutorizacao.getChavePrimaria()));

		relatorio.setNomeQuestionario(questionario.getNomeQuestionario());

		relatorio.setListaPerguntasVO(this.converterRespostasQuestionarioParaVO(historico));

		if (relatorio.getListaPerguntasVO() == null) {
			relatorio.setListaPerguntasVO(this.converterPerguntasQuestionarioParaVO(questionario.getListaPerguntas()));
		}

		relatorio.setDescricao(servicoAutorizacao.getDescricao());
		if (servicoAutorizacao.getDataGeracao() != null) {
			relatorio.setDataGeracao(servicoAutorizacao.getDataGeracaoFormatada());
		}
		if (servicoAutorizacao.getDataEncerramento() != null) {
			relatorio.setDataEncerramento(servicoAutorizacao.getDataEncerramentoFormatada());
		}

		if (servicoAutorizacao.getExecutante() != null) {
			relatorio.setNomeExecutante(servicoAutorizacao.getExecutante().getNome());
		}
		if (servicoAutorizacao.getDataPrevisaoEncerramento() != null) {
			relatorio.setDataPrevisaoEncerramento(servicoAutorizacao.getDataPrevisaoEncerramentoFormatada());
		}
		if (servicoAutorizacao.getServicoTipo() != null) {
			relatorio.setServicoTipo(servicoAutorizacao.getServicoTipo().getDescricao());
		}
		if (servicoAutorizacao.getServicoTipo() != null && servicoAutorizacao.getServicoTipo().getServicoTipoPrioridade() != null) {
			relatorio.setPrioridadeServicoTipo(servicoAutorizacao.getServicoTipo().getServicoTipoPrioridade().getDescricao());
		}
		if (servicoAutorizacao.getEquipe() != null) {
			relatorio.setEquipe(servicoAutorizacao.getEquipe().getNome());
		}
		if (servicoAutorizacao.getChamado() != null && servicoAutorizacao.getChamado().getProtocolo() != null) {
			relatorio.setProtocoloChamado(String.valueOf(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo()));
		}
		if (servicoAutorizacao.getStatus() != null) {
			relatorio.setStatus(servicoAutorizacao.getStatus().getDescricao());
		}
		if (servicoAutorizacao.getCliente() != null) {
			relatorio.setNomeCliente(servicoAutorizacao.getCliente().getNome());
			relatorio.setNomeFantasiaCliente(servicoAutorizacao.getCliente().getNomeFantasia());
			relatorio.setPassaporte(servicoAutorizacao.getCliente().getNumeroPassaporte());

			if (servicoAutorizacao.getCliente().getCpf() != null && !servicoAutorizacao.getCliente().getCpf().isEmpty()) {
				relatorio.setCpfCnpj(servicoAutorizacao.getCliente().getCpfFormatado());
			} else if (servicoAutorizacao.getCliente().getCnpj() != null && !servicoAutorizacao.getCliente().getCnpj().isEmpty()) {
				relatorio.setCpfCnpj(servicoAutorizacao.getCliente().getCnpjFormatado());
			}

			if (servicoAutorizacao.getCliente().getFones() != null && !servicoAutorizacao.getCliente().getFones().isEmpty()) {
				relatorio.setListaClienteFoneTO(this.converterTelefoneParaTelefoneVO(servicoAutorizacao.getCliente().getFones()));
			} else {
				relatorio.setListaClienteFoneTO(null);
			}
		}

		if (servicoAutorizacao.getContrato() != null) {
			relatorio.setNumeroContrato(servicoAutorizacao.getContrato().getNumeroFormatado());
		}
		if (servicoAutorizacao.getImovel() != null) {
			relatorio.setMatriculaImovel(servicoAutorizacao.getImovel().getNumeroImovel());
			relatorio.setNomeFantasiaImovel(servicoAutorizacao.getImovel().getNome());
			relatorio.setCepImovel(servicoAutorizacao.getImovel().getQuadraFace().getEndereco().getCep().getCep());
			relatorio.setEnderecoImovel(servicoAutorizacao.getImovel().getEnderecoFormatado());
		}
		if (servicoAutorizacao.getPontoConsumo() != null) {
			relatorio.setDescricaoPontoConsumo(servicoAutorizacao.getPontoConsumo().getDescricao());
		}
		if (servicoAutorizacao.getServicoAutorizacaoHistoricos() != null && !servicoAutorizacao.getServicoAutorizacaoHistoricos().isEmpty()) {
			relatorio
					.setListaServicoAutorizacaoHistorico(converterServicoAutorizacaoHistoricoParaServicoAutorizacaoHistoricoVO(servicoAutorizacao
							.getServicoAutorizacaoHistoricos()));
		}
		if (servicoAutorizacao.getServicoAutorizacaoMaterial() != null && !servicoAutorizacao.getServicoAutorizacaoMaterial().isEmpty()) {
			relatorio.setListaMaterial(converterMaterialParaMaterialVO(servicoAutorizacao.getServicoAutorizacaoMaterial()));
		}
		if (servicoAutorizacao.getServicoAutorizacaoEquipamento() != null
				&& !servicoAutorizacao.getServicoAutorizacaoEquipamento().isEmpty()) {
			relatorio.setListaEquipamento(converterEquipamentoParaEquipamentoVO(servicoAutorizacao.getServicoAutorizacaoEquipamento()));
		}

		if (agenda != null) {
			relatorio.setTurno(agenda.getTurno().getDescricao());
			relatorio.setDataAgendamento(Util.converterDataParaStringSemHora(agenda.getDataAgenda(), Constantes.FORMATO_DATA_BR));
			relatorio.setConfirmado(agenda.getIndicadorConfirmado().toString());
			relatorio.setHorario(DataUtil.converterDataParaString(agenda.getDataAgenda(), Boolean.TRUE));
		}

		if (comChamado) {
			relatorio.setProtocoloChamado(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo().toString());
			relatorio.setListaChamadoHistorico(popularChamdoHistorico(servicoAutorizacao));
		}

		return relatorio;
	}

	/**
	 * Preenche os dados do histórico do chamado
	 *
	 * @param servicoAutorizacao
	 */
	private List<ChamadoVORelatatorio> popularChamdoHistorico(ServicoAutorizacao servicoAutorizacao) {

		List<ChamadoVORelatatorio> listaChamadoHistorico = new ArrayList<>();

		if (servicoAutorizacao.getChamado() != null) {
			Collection<ChamadoHistorico> historico = servicoAutorizacao.getChamado().getChamadoHistoricos();
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			for (ChamadoHistorico chamadoHistorico : historico) {
				ChamadoVORelatatorio chamadoVORelatatorio = new ChamadoVORelatatorio();
				chamadoVORelatatorio.setData(formato.format(chamadoHistorico.getUltimaAlteracao()));
				chamadoVORelatatorio.setUsuario(chamadoHistorico.getUsuario().getLogin());
				if (chamadoHistorico.getUsuarioResponsavel() != null) {
					chamadoVORelatatorio.setResponsavel(chamadoHistorico.getUsuarioResponsavel().getFuncionario().getNome());
				}
				chamadoVORelatatorio.setUnidadeOrganizacional(chamadoHistorico.getUnidadeOrganizacional().getDescricao());
				chamadoVORelatatorio.setDescricao(chamadoHistorico.getDescricao());
				if (chamadoHistorico.getMotivo() != null) {
					chamadoVORelatatorio.setMotivo(chamadoHistorico.getMotivo().getDescricao());
				}
				chamadoVORelatatorio.setOperacao(chamadoHistorico.getOperacao().getDescricao());
				chamadoVORelatatorio.setSituacao(chamadoHistorico.getStatus().getDescricao());

				listaChamadoHistorico.add(chamadoVORelatatorio);
			}
		}

		return listaChamadoHistorico;
	}

	/**
	 * Converter servico autorizacao historico para servico autorizacao historico vo.
	 *
	 * @param listaServicoAutorizacaoHistorico the lista servico autorizacao historico
	 * @return the list
	 */
	private List<ServicoAutorizacaoHistoricoVO> converterServicoAutorizacaoHistoricoParaServicoAutorizacaoHistoricoVO(
			Collection<ServicoAutorizacaoHistorico> listaServicoAutorizacaoHistorico) {

		List<ServicoAutorizacaoHistorico> servicoAutorizacaoHistoricos = new ArrayList<>();
		servicoAutorizacaoHistoricos.addAll(listaServicoAutorizacaoHistorico);
		List<ServicoAutorizacaoHistoricoVO> listaServicoAutorizacaoHistoricoVO = new ArrayList<>();
		ServicoAutorizacaoHistoricoVO servicoAutorizacaoHistoricoVO;

		for (ServicoAutorizacaoHistorico historico : servicoAutorizacaoHistoricos) {
			servicoAutorizacaoHistoricoVO = new ServicoAutorizacaoHistoricoVO();
			servicoAutorizacaoHistoricoVO.setDescricao(historico.getDescricao());
			if (historico.getUltimaAlteracao() != null) {
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy: HH:mm");
				servicoAutorizacaoHistoricoVO.setDataHora(df.format(historico.getUltimaAlteracao()));
			}
			if (historico.getOperacao() != null) {
				servicoAutorizacaoHistoricoVO.setOperacao(historico.getOperacao().getDescricao());
			}
			if (historico.getEquipe() != null) {
				servicoAutorizacaoHistoricoVO.setEquipe(historico.getEquipe().getNome());
			}
			if (historico.getUsuarioSistema() != null) {
				servicoAutorizacaoHistoricoVO.setUsuario(historico.getUsuarioSistema().getLogin());
			}
			if (historico.getStatus() != null) {
				servicoAutorizacaoHistoricoVO.setStatus(historico.getStatus().getDescricao());
			}

			if (historico.getDataPrevisaoEncerramento() != null) {
				SimpleDateFormat df = new SimpleDateFormat(PADRAO_FORMATO_DATA);
				servicoAutorizacaoHistoricoVO.setDataPrevisaoEncerramento(df.format(historico.getDataPrevisaoEncerramento()));
			}

			listaServicoAutorizacaoHistoricoVO.add(servicoAutorizacaoHistoricoVO);

		}

		return listaServicoAutorizacaoHistoricoVO;
	}

	/**
	 * Converter material para material vo.
	 *
	 * @param listaMaterial the lista material
	 * @return the list
	 */
	private List<MaterialVO> converterMaterialParaMaterialVO(Collection<ServicoAutorizacaoMaterial> listaMaterial) {

		List<ServicoAutorizacaoMaterial> servicoAutorizacaoMaterias = new ArrayList<>();
		servicoAutorizacaoMaterias.addAll(listaMaterial);
		List<MaterialVO> listaMaterialVO = new ArrayList<>();
		MaterialVO materialVO;

		for (ServicoAutorizacaoMaterial material : servicoAutorizacaoMaterias) {
			materialVO = new MaterialVO();
			if (material.getMaterial() != null) {
				materialVO.setDescricao(material.getMaterial().getDescricao());
				if (material.getMaterial().getUnidadeMedida() != null) {
					materialVO.setUnidade(material.getMaterial().getUnidadeMedida().getDescricao());
				}
			}
			if (material.getQuantidadeMaterial() != null) {
				materialVO.setQuantidade(String.valueOf(material.getQuantidadeMaterial()));
			}
			listaMaterialVO.add(materialVO);
		}

		return listaMaterialVO;
	}

	/**
	 * converter Telefone Para Telefone VO
	 *
	 * @param listaClienteTelefone the lista fone
	 * @return the list
	 */
	private Collection<ClienteFoneTO> converterTelefoneParaTelefoneVO(Collection<ClienteFone> listaClienteTelefone) {

		Collection<ClienteFoneTO> listaClienteFoneTO = new ArrayList<>();

		for (ClienteFone fone : listaClienteTelefone) {
			ClienteFoneTO clienteFoneTO = new ClienteFoneTO();
			clienteFoneTO.setCodigoDDD(fone.getCodigoDDD());
			clienteFoneTO.setNumeroFone(fone.getNumero());

			listaClienteFoneTO.add(clienteFoneTO);
		}

		return listaClienteFoneTO;
	}

	/**
	 * @param listaPerguntasResposta
	 * @return
	 */
	private Collection<QuestionarioVO> converterPerguntasQuestionarioParaVO(Collection<QuestionarioPergunta> listaPerguntasResposta) {

		Collection<QuestionarioVO> listaPerguntasVO = new ArrayList<>();

		int index = 0;

		for (QuestionarioPergunta perg : listaPerguntasResposta) {
			QuestionarioVO questionarioVO = new QuestionarioVO();

			questionarioVO.setNomePerguntaQuestionario(perg.getNomePergunta());

			if (perg.getObjetiva()) {
				if (perg.getValoresPersonalizados() && perg.getMultiplaEscolha()) {
					questionarioVO.setListaAlternativaQuestionario(montarAlternativasMultiplaEscolha(controladorQuestionario
							.obterAlternativas(perg.getChavePrimaria())));

				} else {
					questionarioVO.setAlternativaQuestionario(montarAlternativas(controladorQuestionario.obterAlternativas(perg
							.getChavePrimaria())));
				}

				Integer notaMinima = perg.getNotaMinima();
				Integer notaMaxima = perg.getNotaMaxima();

				if (notaMinima != null && !notaMinima.equals(notaMaxima)) {
					questionarioVO.setNotaPerguntaQuestionario(montarAlternativasNotas(notaMinima, notaMaxima));
				}
			}

			index = index + 1;
			questionarioVO.setIndex(index);
			listaPerguntasVO.add(questionarioVO);

		}

		return listaPerguntasVO;
	}

	private Collection<QuestionarioVO> converterRespostasQuestionarioParaVO(Collection<ServicoAutorizacaoHistorico> historico) {

		Collection<QuestionarioVO> listaPerguntasVO = new ArrayList<>();

		for (ServicoAutorizacaoHistorico servicoAutorizacaoHistorico : historico) {

			int index = 0;

			if (servicoAutorizacaoHistorico.getOperacao().getChavePrimaria() == OPERACAO_CHAVE) {

				Collection<QuestionarioPerguntaResposta> resposta =
						controladorResposta.obterRespostasServicoAutorizacaoHistorico(servicoAutorizacaoHistorico.getChavePrimaria());

				for (QuestionarioPerguntaResposta perguntaResposta : resposta) {
					QuestionarioVO questionarioVO = new QuestionarioVO();

					questionarioVO.setNomePerguntaQuestionario(perguntaResposta.getPergunta().getNomePergunta());

					if (perguntaResposta.getPergunta().getObjetiva()) {
						if (perguntaResposta.getPergunta().getValoresPersonalizados()
								&& perguntaResposta.getPergunta().getMultiplaEscolha()) {
							questionarioVO.setListaAlternativaQuestionario(montarRespostaLista(perguntaResposta.getDescricaoResposta()));
						} else {
							questionarioVO.setAlternativaQuestionario(montarRespostaLista(perguntaResposta.getDescricaoResposta()));
						}

						Integer notaMinima = perguntaResposta.getPergunta().getNotaMinima();
						Integer notaMaxima = perguntaResposta.getPergunta().getNotaMaxima();

						if (notaMinima != null && !notaMinima.equals(notaMaxima)) {
							questionarioVO.setNotaPerguntaQuestionario(montarRespostaNota(perguntaResposta.getNotaResposta().toString(),
									notaMinima, notaMaxima));
						}
					} else {
						questionarioVO.setNomeRespostaExecutada(montarRespostaDescricao(perguntaResposta.getDescricaoResposta()));
					}

					index = index + 1;
					questionarioVO.setIndex(index);
					listaPerguntasVO.add(questionarioVO);

				}
			}

		}

		if (listaPerguntasVO.isEmpty()) {
			return null;
		}
		return listaPerguntasVO;
	}

	private String montarRespostaNota(String notaResposta, Integer notaMinima, Integer notaMaxima) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("Valores de ").append(notaMinima).append(" a ").append(notaMaxima).append(" ");
		stringBuilder.append(" - Nota: ").append(notaResposta);

		return stringBuilder.toString();
	}

	private String montarRespostaDescricao(String descricaoResposta) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("Resposta: ").append(descricaoResposta);

		return stringBuilder.toString();
	}

	private String montarRespostaLista(String descricaoResposta) {
		StringBuilder stringBuilder = new StringBuilder();

		if (descricaoResposta != null) {
			stringBuilder.append("Alternativa: ").append(descricaoResposta);
		}

		return stringBuilder.toString();
	}

	/**
	 * @param notaMinima
	 * @param notaMaxima
	 * @return
	 */
	private String montarAlternativasNotas(Integer notaMinima, Integer notaMaxima) {
		StringBuilder stringBuilder = new StringBuilder();

		for (int nota = notaMinima; nota <= notaMaxima; nota++) {

			stringBuilder.append("(   ) ").append(nota).append("   ");
		}

		return stringBuilder.toString();
	}

	/**
	 * Monta lista de alternativas
	 *
	 * @param obterAlternativas
	 * @return alternativas
	 */
	private String montarAlternativasMultiplaEscolha(Collection<QuestionarioAlternativa> obterAlternativas) {
		StringBuilder stringBuilder = new StringBuilder();

		for (QuestionarioAlternativa alternativaMultiplaEscolha : obterAlternativas) {
			stringBuilder.append("(  ) ").append(alternativaMultiplaEscolha.getNomeAlternativa()).append("   ");
		}
		return stringBuilder.toString();
	}

	/**
	 * @param obterAlternativas
	 * @return
	 */
	private String montarAlternativas(Collection<QuestionarioAlternativa> obterAlternativas) {
		StringBuilder stringBuilder = new StringBuilder();

		for (QuestionarioAlternativa alternativa : obterAlternativas) {
			stringBuilder.append(" (   ) ").append(alternativa.getNomeAlternativa()).append("   ");
		}
		return stringBuilder.toString();
	}

	/**
	 * Converter equipamento para equipamento vo.
	 *
	 * @param listaEquipamento the lista equipamento
	 * @return the list
	 */
	private List<EntidadeConteudoVO> converterEquipamentoParaEquipamentoVO(Collection<ServicoAutorizacaoEquipamento> listaEquipamento) {

		List<ServicoAutorizacaoEquipamento> servicoAutorizacaoEquipamentos = new ArrayList<>();
		servicoAutorizacaoEquipamentos.addAll(listaEquipamento);
		List<EntidadeConteudoVO> listaEquipamentoVO = new ArrayList<>();
		EntidadeConteudoVO entidadeConteudoVO;

		for (ServicoAutorizacaoEquipamento equipamento : servicoAutorizacaoEquipamentos) {
			entidadeConteudoVO = new EntidadeConteudoVO();
			if (equipamento.getEquipamento() != null) {
				entidadeConteudoVO.setDescricao(equipamento.getEquipamento().getDescricao());
			}
			listaEquipamentoVO.add(entidadeConteudoVO);
		}

		return listaEquipamentoVO;
	}

	/**
	 * Consulta serviço autorização que tem chamado tipo pesquisa satisfação.
	 *
	 * @param chavePrimariaChamado the chave primaria chamado
	 * @return the servico autorizacao
	 */
	@Override
	public ServicoAutorizacao consultarServicoAutorizacaoPorChamado(Long chavePrimariaChamado) {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoPorChamado(chavePrimariaChamado);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarAgendamentos(br.com.ggas.atendimento.
	 * servicoautorizacao .dominio.ServicoAutorizacaoAgendaPesquisaVO)
	 */
	@Override
	public List<ServicoAutorizacaoAgendaVO> consultarAgendamentos(ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
			Long chaveServicoTipo) throws NumberFormatException, GGASException {

		List<ServicoAutorizacaoAgendaVO> agendamentosPreenchidosSemana = new ArrayList<>();

		Calendar dataAgenda = Calendar.getInstance();
		dataAgenda.set(servicoAutorizacaoAgendaPesquisaVO.getAno(), servicoAutorizacaoAgendaPesquisaVO.getMes() - 1, 1);
		List<ServicoAutorizacaoAgendaVO> agendamentosBanco =
				repositorioServicoAutorizacao.consultarAgendamentos(chaveServicoTipo, dataAgenda.getTime());
		Long totalAgendamento = repositorioServicoAutorizacao.consultarTotalAgendamentos(chaveServicoTipo);
		if (agendamentosBanco != null) {
			preencherDiasFaltantes(agendamentosBanco, servicoAutorizacaoAgendaPesquisaVO.getMes() - 1,
					servicoAutorizacaoAgendaPesquisaVO.getAno(), totalAgendamento);
		}
		if (agendamentosBanco != null && !agendamentosBanco.isEmpty()) {
			ServicoAutorizacaoAgendaVO autorizacaoAgenda = agendamentosBanco.get(0);
			Calendar cal = Calendar.getInstance();
			cal.setTime(autorizacaoAgenda.getData());
			agendamentosPreenchidosSemana.addAll(agendamentosBanco);
		}
		return agendamentosPreenchidosSemana;
	}

	/**
	 * Preencher dias faltantes.
	 *
	 * @param agendamentos the agendamentos
	 * @param mes the mes
	 * @param ano the ano
	 * @param total the total
	 * @return the list
	 */
	public List<ServicoAutorizacaoAgendaVO> preencherDiasFaltantes(List<ServicoAutorizacaoAgendaVO> agendamentos, int mes, int ano,
			Long total) {

		Calendar cal = Calendar.getInstance();
		cal.set(ano, mes, 1);
		int qtdDias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		if (agendamentos.isEmpty()) {
			for (int i = 0; i < qtdDias; i++) {
				agendamentos.add(new ServicoAutorizacaoAgendaVO(ano, mes, i + 1, 0l, 0l, 0l));
			}
			return agendamentos;
		}
		for (int i = 0; i < qtdDias; i++) {
			try {
				if (agendamentos.get(i).getDataFormatada() != i + 1) {
					agendamentos.add(i, new ServicoAutorizacaoAgendaVO(ano, mes, i + 1, 0l, 0l, 0l));
				}
			} catch (IndexOutOfBoundsException e) {
				LOG.error(e.getMessage(), e);
				agendamentos.add(i, new ServicoAutorizacaoAgendaVO(ano, mes, i + 1, 0l, 0l, 0l));
			}
		}
		return agendamentos;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarServicoAutorizacaoAgenda(br.com.ggas.
	 * atendimento. servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO)
	 */
	@Override
	public List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgenda(
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws GGASException {

		List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgenda = null;
		Date data = null;

		if (!"".equals(servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada())) {
			data = Util.converterCampoStringParaData(DATA, servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(),
					Constantes.FORMATO_DATA_BR);
		}
		servicoAutorizacaoAgenda = repositorioServicoAutorizacao.consultarServicoAutorizacaoAgenda(
				servicoAutorizacaoAgendaPesquisaVO.getServicoTipo() != null
						? servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getChavePrimaria()
						: null,
				data, null,
				servicoAutorizacaoAgendaPesquisaVO.getEquipe() != null
						? servicoAutorizacaoAgendaPesquisaVO.getEquipe().getChavePrimaria()
						: null, Boolean.FALSE);
		return servicoAutorizacaoAgenda;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#removerAgendamento(java.lang.Long)
	 */
	@Override
	public void removerAgendamento(Long chaveAgendamento) throws NegocioException {
		ServicoAutorizacaoAgenda agenda = new ServicoAutorizacaoAgenda();
		agenda.setChavePrimaria(chaveAgendamento);
		repositorioServicoAutorizacao.remover(agenda);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#adicionarServicoAutorizacaoAgendamento(java.lang
	 * .Long[], br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO)
	 */
	@Override
	public void adicionarServicoAutorizacaoAgendamento(Long[] chaves,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, Boolean isServicoPrioritario) throws GGASException, IOException {


		Collection<ServicoAutorizacao> listaServicoAutorizacao = repositorioServicoAutorizacao
				.consultarServicoAutorizacaoPorChavesPrimarias(chaves);

		Calendar dataAgendamento = Calendar.getInstance();
		dataAgendamento.setTime(DataUtil.converterParaData(servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada()));
		
		if(isServicoPrioritario) {
			dataAgendamento.setTime(new Date());
		}
		
		
		EntidadeConteudo turnoPadrao = controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno").stream()
				.filter(p -> "MANHÃ".equals(p.getDescricao())).findFirst().orElse(null);
		
		for(ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacao) {
			
			EntidadeConteudo turno = null;
			
			if(servicoAutorizacaoAgendaPesquisaVO.getTurno() != null) {
				turno = servicoAutorizacaoAgendaPesquisaVO.getTurno();
				servicoAutorizacao.setTurnoPreferencia(turno);
				this.atualizarSemValidar(servicoAutorizacao);
				
			} else if (servicoAutorizacao.getTurnoPreferencia() != null) {
				turno = servicoAutorizacao.getTurnoPreferencia();
			} else {
				turno = turnoPadrao;
			}
			
			inserirServicoAutorizacaoAgenda(turno, dataAgendamento, servicoAutorizacao, Boolean.FALSE);
			
		}

	}

	private void inserirServicoAutorizacaoAgenda(EntidadeConteudo turno,
			Calendar dataAgendada, ServicoAutorizacao servicoAutorizacao, Boolean isServicoPrioritario) throws NegocioException {
		ServicoAutorizacaoAgenda agenda = new ServicoAutorizacaoAgenda();
		agenda.setServicoAutorizacao(servicoAutorizacao);
		agenda.setTurno(turno);
		agenda.setDataAgenda(dataAgendada.getTime());
		agenda.setIndicadorConfirmado(isServicoPrioritario);
		this.repositorioServicoAutorizacao.inserir(agenda);
	}
	


	private void calcularDataParaPrimeiroPontoDaRoteirizacao(Calendar dataAgendada,
			ServicoAutorizacao servicoAutorizacao, PontoConsumo pontoConsumoRoteizar) throws NegocioException, IOException {
		Collection<ServicoAutorizacao> listaServicoAutorizacaoAux = new ArrayList<>();
		listaServicoAutorizacaoAux.add(servicoAutorizacao);
		
		if(pontoConsumoRoteizar != null && servicoAutorizacao.getPontoConsumoRoteirizacao().getLatitudeGrau().compareTo(pontoConsumoRoteizar.getLatitudeGrau()) == 0 && servicoAutorizacao.getPontoConsumoRoteirizacao().getLongitudeGrau().compareTo(pontoConsumoRoteizar.getLongitudeGrau()) == 0) {
			dataAgendada.add(Calendar.MINUTE, 1);
		} else {
			Collection<ServicoAutorizacao> colecaoAux = this.roteirizarServicoAutorizacao(listaServicoAutorizacaoAux, pontoConsumoRoteizar);
			
			ServicoAutorizacao servicoAux = colecaoAux.iterator().next();
			
			dataAgendada.add(Calendar.MINUTE, servicoAux.getTempoDistancia().intValue() / 60);
			dataAgendada.add(Calendar.MINUTE, 5);
		}

	}

	private Long calcularTempoExecucao(ServicoAutorizacao servicoAutorizacao) {
		ServicoTipo servicoTipo = servicoAutorizacao.getServicoTipo();
		Boolean isMetadadeTempoExecResid = servicoTipo.getIndicadorMetadeTempoExecucao();
		PontoConsumo ponto = servicoAutorizacao.getPontoConsumoRoteirizacao();
		Long tempoExecucao = servicoTipo.getQuantidadeTempoMedio();
		
		if (isMetadadeTempoExecResid && ponto.getSegmento() != null && "RESIDENCIAL".equals(ponto.getSegmento().getDescricao())
				&& ( ponto.getImovel() != null && ponto.getImovel().isIndividual() != null && ponto.getImovel().isIndividual())) {
			tempoExecucao = tempoExecucao / 2;
		}
		
		
		return tempoExecucao;
	}

	private Collection<ServicoAutorizacao> roteirizarServicoAutorizacao(Collection<ServicoAutorizacao> listaServicoAutorizacao, PontoConsumo pontoConsumoRoteirizar) throws NegocioException, IOException {
		
		Collection<ServicoAutorizacao> listaRoteirizada = new ArrayList<ServicoAutorizacao>();
		Double latitudeComparar = null;
		Double longitudeComparar = null;
		
		if(pontoConsumoRoteirizar != null) {
			latitudeComparar = pontoConsumoRoteirizar.getLatitudeGrau().doubleValue();
			longitudeComparar = pontoConsumoRoteirizar.getLongitudeGrau().doubleValue();
		} else {
			latitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
			longitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());
		}
		
		if(pontoConsumoRoteirizar != null) {
			latitudeComparar = pontoConsumoRoteirizar.getLatitudeGrau().doubleValue();
			longitudeComparar = pontoConsumoRoteirizar.getLongitudeGrau().doubleValue();
		} else {
			latitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
			longitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());
		}
		
		while (!listaServicoAutorizacao.isEmpty()) {

			Double menorDistancia = -1.0;
			ServicoAutorizacao servicoAutorizacao = listaServicoAutorizacao.iterator().next();
			PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumoRoteirizacao();
			
			Long tempoDistancia = 0l;
			List<PontoConsumo> listaPontoConsumo = listaServicoAutorizacao.stream()
					.filter(p -> p.getPontoConsumo() != null).map(ServicoAutorizacao::getPontoConsumoRoteirizacao)
					.collect(Collectors.toList());

			Map<String, Object> mapaRetornado = Util.obterMenorDistanciaTempoEntrePontosConsumo(listaPontoConsumo,
					latitudeComparar, longitudeComparar, menorDistancia, pontoConsumo, tempoDistancia);

			PontoConsumo pontoConsumoRoteirizado = (PontoConsumo) mapaRetornado.get("pontoConsumo");
			tempoDistancia = (Long) mapaRetornado.get("tempoDistancia");
			menorDistancia = (Double) mapaRetornado.get("menorDistancia");

			latitudeComparar = pontoConsumoRoteirizado.getLatitudeGrau().doubleValue();
			longitudeComparar = pontoConsumoRoteirizado.getLongitudeGrau().doubleValue();

			Collection<ServicoAutorizacao> listaServicoRoteirizado = listaServicoAutorizacao.stream().filter(
					p -> p.getPontoConsumoRoteirizacao().getLatitudeGrau().compareTo(pontoConsumoRoteirizado.getLatitudeGrau()) == 0
							&& p.getPontoConsumoRoteirizacao().getLongitudeGrau()
									.compareTo(pontoConsumoRoteirizado.getLongitudeGrau()) == 0)
					.collect(Collectors.toCollection(ArrayList::new));

			Long tempoDistanciaFinal = tempoDistancia;

			listaServicoRoteirizado.stream().forEach(p -> {
				p.setTempoDistancia(tempoDistanciaFinal);
			});

			listaRoteirizada.addAll(listaServicoRoteirizado);
			listaServicoAutorizacao.removeIf(
					p -> p.getPontoConsumoRoteirizacao().getLatitudeGrau().compareTo(pontoConsumoRoteirizado.getLatitudeGrau()) == 0
							&& p.getPontoConsumoRoteirizacao().getLongitudeGrau()
									.compareTo(pontoConsumoRoteirizado.getLongitudeGrau()) == 0);

		}
		
		return listaRoteirizada;
	}

	private Map<String, EquipeAlgasVO> montarMapaEquipeAlgas(
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, Funcionario funcionario)
			throws GGASException, NegocioException {
		Map<String, EquipeAlgasVO> mapaEquipeALGAS = new LinkedHashMap<String, EquipeAlgasVO>();

		List<EquipeTurnoItem> listaEquipeTurno = obterListaEscala(servicoAutorizacaoAgendaPesquisaVO,
				funcionario);
		
		
		for (EquipeTurnoItem equipeTurnoItem : listaEquipeTurno) {
			EquipeAlgasVO equipeAlgasVO = new EquipeAlgasVO();

			equipeAlgasVO.setNome(equipeTurnoItem.getFuncionario().getNome());
			equipeAlgasVO.setDataInicial(equipeTurnoItem.getDataInicio());
			equipeAlgasVO.setDataFinal(equipeTurnoItem.getDataFim());
			equipeAlgasVO.setDuracao(DataUtil.obterDiferencaMinutosEntreDatas(equipeAlgasVO.getDataInicial(),
					equipeAlgasVO.getDataFinal()));

			equipeAlgasVO.setTurno(equipeTurnoItem.getTurno());
			equipeAlgasVO.setEquipe(equipeTurnoItem.getEquipe());

			equipeAlgasVO.setFuncionario(equipeTurnoItem.getFuncionario());

			mapaEquipeALGAS.put(equipeAlgasVO.getNome() + equipeAlgasVO.getEquipe().getChavePrimaria()
					+ equipeAlgasVO.getTurno().getDescricao(), equipeAlgasVO);
		}
		return mapaEquipeALGAS;
	}

	private List<EquipeTurnoItem> obterListaEscala(
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, Funcionario funcionario)
			throws GGASException {
		List<EquipeTurnoItem> listaEquipeTurnoItem = repositorioServicoAutorizacao.consultarEscalaDisponivel(
				servicoAutorizacaoAgendaPesquisaVO.getEquipe(), servicoAutorizacaoAgendaPesquisaVO.getTurno(),
				servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada());
		
		
		List<EquipeTurnoItem> listaEquipeTurnoItemAux = new ArrayList<EquipeTurnoItem>();
		Integer indiceFuncionario = 0;

		if(funcionario != null) {
			indiceFuncionario = listaEquipeTurnoItem.stream().map(p -> p.getFuncionario())
					.collect(Collectors.toList()).indexOf(funcionario) + 1;		
		}
		
		for (int i = 0; i < listaEquipeTurnoItem.size(); i++) {
			
			if(indiceFuncionario > listaEquipeTurnoItem.size() - 1) {
				indiceFuncionario = 0;
			} 
			
			listaEquipeTurnoItemAux.add(listaEquipeTurnoItem.get(indiceFuncionario));
			indiceFuncionario++;
			
		}
		return listaEquipeTurnoItemAux;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterServicoAutorizacaoAgenda(java.lang.Long)
	 */
	@Override
	public ServicoAutorizacaoAgenda obterServicoAutorizacaoAgenda(Long chavePrimaria) throws NegocioException {
		return (ServicoAutorizacaoAgenda) repositorioServicoAutorizacao.obter(chavePrimaria, ServicoAutorizacaoAgenda.class,
				SERVICO_AUTORIZACAO);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarAgendamentosDisponiveisTurno(br.com.ggas.
	 * atendimento .servicotipo.dominio.ServicoTipo, java.util.Date)
	 */
	@Override
	public List<ServicoTipoAgendamentoTurno> consultarAgendamentosDisponiveisTurno(ServicoTipo tipo, Date dataAgenda) {
		return repositorioServicoAutorizacao.consultarAgendamentosDisponiveisTurno(tipo, dataAgenda);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#gerarRelatorioAgendamento(br.com.ggas.atendimento.
	 * servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO)
	 */
	@Override
	public byte[] gerarRelatorioAgendamento(ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws GGASException {

		Date data =
				Util.converterCampoStringParaData(DATA, servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(), Constantes.FORMATO_DATA_BR);
		
		ServicoTipo servicoTipo = servicoAutorizacaoAgendaPesquisaVO.getServicoTipo();

		
		List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgendaList =
				repositorioServicoAutorizacao.consultarServicoAutorizacaoAgenda(servicoTipo != null ? servicoTipo.getChavePrimaria() : null, data, null, null, Boolean.TRUE, servicoAutorizacaoAgendaPesquisaVO.getChavesPrimarias());
		List<ServicoAutorizacaoVORelatorio> relatorioList = new ArrayList<>();
		for (ServicoAutorizacaoAgenda agenda : servicoAutorizacaoAgendaList) {
			ServicoAutorizacaoVORelatorio relatorio = this.converterServicoAutorizacaoParaVO(agenda.getServicoAutorizacao(), agenda, false);
			relatorioList.add(relatorio);
		}
		Map<String, Object> parametros = new HashMap<>();
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		parametros.put(IMAGEM,
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		return RelatorioUtil.gerarRelatorio(relatorioList, parametros, RELATORIO_SERVICO_AUTORIZACAO_AGENDAMENTO, formatoImpressao);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarServicoAutorizacaoAgendaPorPontoConsumo(
	 * java .lang .Long)
	 */
	@Override
	public List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgendaPorPontoConsumo(Long chavePontoConsumo) {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoAgendaPorPontoConsumo(chavePontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#atualizarServicoAutorizacaoAgenda(br.com.ggas.
	 * atendimento. servicoautorizacao.dominio.ServicoAutorizacaoAgenda)
	 */
	@Override
	public void atualizarServicoAutorizacaoAgenda(ServicoAutorizacaoAgenda agenda) throws ConcorrenciaException, NegocioException {
		repositorioServicoAutorizacao.atualizar(agenda, ServicoAutorizacaoAgenda.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#alterarServicoAutorizacaoEmExecucao(java.lang.Long
	 * [], java.util.Map)
	 */
	@Override
	public void alterarServicoAutorizacaoEmExecucao(Long[] chavesPrimarias, Map<String, Object> dados) throws GGASException {

		List<ServicoAutorizacao> servicoAutorizacaoList = repositorioServicoAutorizacao.obterServicoAutorizacao(chavesPrimarias);
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_EXECUCAO);
		EntidadeConteudo statusEmExecucao = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ABERTO);
		EntidadeConteudo statusAberto = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		for (ServicoAutorizacao servicoAutorizacao : servicoAutorizacaoList) {
			if (servicoAutorizacao.getStatus().getChavePrimaria() != statusAberto.getChavePrimaria()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO, servicoAutorizacao.getStatus().getDescricao());
			} else {
				servicoAutorizacao.setStatus(statusEmExecucao);
				repositorioServicoAutorizacao.atualizar(servicoAutorizacao);
				enviarEmail(AcaoServicoAutorizacao.EXECUTANDO, servicoAutorizacao);
				this.inserirServicoAutorizacaoHistorico(servicoAutorizacao, Constantes.C_STATUS_SERV_AUT_EXECUCAO, dados);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarServicoAutorizacaoPorComandoAcao(br.com.
	 * ggas .batch .acaocomando.dominio.AcaoComando)
	 */
	@Override
	public Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorComandoAcao(AcaoComando acaoComando) throws NegocioException {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoPorComandoAcao(acaoComando);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterMateriasOrdenado(java.lang.Long[])
	 */
	@Override
	public Collection<ServicoAutorizacaoMaterial> obterMateriasOrdenado(Long[] chavesServicoAutorizacaoMaterial) throws NegocioException {
		return repositorioServicoAutorizacao.obterMateriasOrdenado(chavesServicoAutorizacaoMaterial);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#atualizarSemValidar(br.com.ggas.atendimento.
	 * servicoautorizacao .dominio.ServicoAutorizacao)
	 */
	@Override
	public void atualizarSemValidar(ServicoAutorizacao servicoAutorizacao) throws ConcorrenciaException, NegocioException {
		repositorioServicoAutorizacao.atualizarSemValidar(servicoAutorizacao, ServicoAutorizacao.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterServicoAutorizacaoHistorico(java.lang.Long)
	 */
	@Override
	public ServicoAutorizacaoHistorico obterServicoAutorizacaoHistorico(Long chavePrimaria) throws NegocioException {
		return (ServicoAutorizacaoHistorico) repositorioServicoAutorizacao.obter(chavePrimaria, ServicoAutorizacaoHistorico.class,
				SERVICO_AUTORIZACAO, "servicoAutorizacao.servicoTipo");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterServicoAutorizacaoHistoricoAnexo(java.lang.
	 * Long)
	 */
	@Override
	@Transactional
	public ServicoAutorizacaoHistoricoAnexo obterServicoAutorizacaoHistoricoAnexo(Long chavePrimaria) throws NegocioException {

		return (ServicoAutorizacaoHistoricoAnexo) repositorioServicoAutorizacao
				.obter(chavePrimaria, ServicoAutorizacaoHistoricoAnexo.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#gerarRelatorioControleServicoAutorizacao(br.com.
	 * ggas. web.relatorio .atendimento.PesquisaRelatorioControleServicoAutorizacaoVO, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorioControleServicoAutorizacao(
			PesquisaRelatorioControleServicoAutorizacaoVO pesquisaRelatorioControleServicoAutorizacaoVO, FormatoImpressao formatoImpressao)
			throws GGASException {

		List<ServicoAutorizacao> servicoAutorizacaoList =
				repositorioServicoAutorizacao.consultarControleServicoAutorizacao(pesquisaRelatorioControleServicoAutorizacaoVO);
		ServiceLocator.getInstancia().getControladorSegmento();
		List<ControleServicoAutorizacaoVO> controleServicoAutorizacaoVOList = new ArrayList<>();
		RelatorioControleServicoAutorizacaoWrapper wrapper = new RelatorioControleServicoAutorizacaoWrapper();
		ControleServicoAutorizacaoVO vo;
		for (ServicoAutorizacao servicoAutorizacao : servicoAutorizacaoList) {
			vo = new ControleServicoAutorizacaoVO();
			vo.setDataEmissao(servicoAutorizacao.getDataGeracao());
			vo.setEmpresaContratada(servicoAutorizacao.getEquipe().getUnidadeOrganizacional().getDescricao());
			vo.setNomeCliente(servicoAutorizacao.getCliente().getNome());
			vo.setNumeroContrato(servicoAutorizacao.getContrato().getNumeroCompletoContrato());
			vo.setNumeroServicoAutorizacao(String.valueOf(servicoAutorizacao.getChavePrimaria()));
			if (servicoAutorizacao.getPontoConsumo() != null) {
				vo.setSegmento(servicoAutorizacao.getPontoConsumo().getSegmento().getDescricao());
			}
			if (servicoAutorizacao.getChamado() != null && servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito() != null
					&& servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getValorReferencia() != null) {
				vo.setValor(servicoAutorizacao.getChamado().getChamadoAssunto().getRubricaDebito().getValorReferencia());
			}
			controleServicoAutorizacaoVOList.add(vo);
		}
		wrapper.setListaServicoAutorizacao(controleServicoAutorizacaoVOList);
		Map<String, Object> parametros = new HashMap<>();
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		parametros.put(IMAGEM,
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		Collection<Object> collRelatorio = new ArrayList<>();
		collRelatorio.add(wrapper);

		if (controleServicoAutorizacaoVOList != null && !controleServicoAutorizacaoVOList.isEmpty()) {
			return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_CONTROLE_AUTORIZACAO_SERVICO, formatoImpressao);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#obterServicoAutorizacao(java.lang.Long,
	 * java.util.Date, java.lang.Long)
	 */
	@Override
	public Collection<ServicoAutorizacao> obterServicoAutorizacao(Long pontoConsumo, Date dtavisoCorte, Long idServicoTipo,
			List<Long> listaChavePontoConsumo, EntidadeConteudo statusCancelado) {
		return repositorioServicoAutorizacao.obterServicoAutorizacao(pontoConsumo, dtavisoCorte, idServicoTipo, listaChavePontoConsumo,
				statusCancelado);
	}

	private void popularSegmento(ServicoAutorizacao servicoAutorizacao, Map<String, Object> parametros) {

		if (servicoAutorizacao.getPontoConsumo() != null && servicoAutorizacao.getPontoConsumo().getSegmento() != null
				&& parametros != null) {
			Segmento segmento = servicoAutorizacao.getPontoConsumo().getSegmento();
			if ("residencial".equalsIgnoreCase(segmento.getDescricao())) {
				parametros.put("segmentoResidencial", "X");
			} else if ("comercial".equalsIgnoreCase(segmento.getDescricao())) {
				parametros.put("segmentoComercial", "X");
			} else if ("automotivo".equalsIgnoreCase(segmento.getDescricao()) || "veicular".equalsIgnoreCase(segmento.getDescricao())) {
				parametros.put("segmentoAutomotivo", "X");
			} else if (segmento.getDescricao().startsWith("INDUSTR") || segmento.getDescricao().endsWith("GERAÇÃO")) {
				parametros.put("segmentoIndustrialCogeracao", "X");
			} else if ("infraestrutura".equalsIgnoreCase(segmento.getDescricao())) {
				parametros.put("segmentoInfraestrutura", "X");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#
	 * consultarServicoAutorizacaoAgendaEmAndamento(java.lang.Long)
	 */
	@Override
	public List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgendaEmAndamento(Long chaveServicoTipo) {

		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ABERTO);
		EntidadeConteudo statusAbertoServicoAutorizacao =
				controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_EXECUCAO);
		EntidadeConteudo statusExecucaoServicoAutorizacao =
				controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		List<Long> listaStatus = new ArrayList<>();
		listaStatus.add(statusExecucaoServicoAutorizacao.getChavePrimaria());
		listaStatus.add(statusAbertoServicoAutorizacao.getChavePrimaria());

		return repositorioServicoAutorizacao.consultarServicoAutorizacaoAgenda(chaveServicoTipo, null, listaStatus, null, Boolean.TRUE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#consultarServicoAutorizacaoPorChaveChamado(java.lang
	 * .Long)
	 */
	@Override
	public List<ServicoAutorizacao> consultarServicoAutorizacaoPorChaveChamado(Long chavePrimariaChamado) {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoPorChaveChamado(chavePrimariaChamado);
	}

	/**
	 * @param acao
	 * @param servico
	 * @throws GGASException
	 */
	private void enviarEmail(AcaoServicoAutorizacao acao, ServicoAutorizacao servico) throws GGASException {

		Chamado chamado = servico.getChamado();

		ParametroSistema servidorEmailConfigurado =
				parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO);

		ParametroSistema emailRemetentePadrao = parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
		JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		if (validarEnvioEmail(acao, servico.getServicoTipo()) && servidorEmailConfigurado != null
				&& BooleanUtil.converterStringCharParaBooleano(servidorEmailConfigurado.getValor())) {

			StringBuilder assuntoEmail = new StringBuilder();
			if (chamado != null) {
				assuntoEmail.append(" [Chamado: ");
				assuntoEmail.append(chamado.getProtocolo().getNumeroProtocolo());
				assuntoEmail.append("] ");
			}
			assuntoEmail.append("Serviço com código ");
			assuntoEmail.append(servico.getChavePrimaria());
			assuntoEmail.append(" ");
			assuntoEmail.append(acao.getDescricao());

			if (StringUtils.isNotBlank(servico.getEquipe().getUnidadeOrganizacional().getEmailContato())) {
				mailUtil.enviar(emailRemetentePadrao.getValor(), servico.getEquipe().getUnidadeOrganizacional().getEmailContato(),
						assuntoEmail.toString(), servico.getCondeudoEmail(), true, true);
			}

			if (servico.getEquipe() != null) {
				Collection<EquipeComponente> equipeComponentes = repositorioEquipe
						.listarEquipeComponente(servico.getEquipe().getChavePrimaria());

				for (EquipeComponente componente : equipeComponentes) {
					if (StringUtils.isNotBlank(componente.getFuncionario().getEmail())) {
						mailUtil.enviar(emailRemetentePadrao.getValor(), componente.getFuncionario().getEmail(),
								assuntoEmail.toString(), servico.getCondeudoEmail(), true, true);
					}
				}
			}
			if (chamado != null) {
				Collection<ChamadoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora =
						chamado.getListaUnidadeOrganizacionalVisualizadora();

				if (!isUnidadeNaLista(listaUnidadeOrganizacionalVisualizadora, chamado.getUnidadeOrganizacional())
						&& StringUtils.isNotBlank(chamado.getUnidadeOrganizacional().getEmailContato())) {

					mailUtil.enviar(emailRemetentePadrao.getValor(), chamado.getUnidadeOrganizacional().getEmailContato(),
							assuntoEmail.toString(), servico.getCondeudoEmail(), true, true);
				}

				for (ChamadoUnidadeOrganizacional chamadoUnidadeOrganizacional : listaUnidadeOrganizacionalVisualizadora) {
					if (StringUtils.isNotBlank(chamadoUnidadeOrganizacional.getUnidadeOrganizacional().getEmailContato())) {
						mailUtil.enviar(emailRemetentePadrao.getValor(), chamadoUnidadeOrganizacional.getUnidadeOrganizacional()
								.getEmailContato(), assuntoEmail.toString(), servico.getCondeudoEmail(), true, true);

					}
				}
			}
			if(servico.getServicoTipo().getIndicadorEmailCliente() != null) {
				if(servico.getServicoTipo().getIndicadorEmailCliente()) {
					if(servico.getCliente() != null) {
						if(servico.getCliente().getEmailPrincipal() != null) {
								if(servico.getServicoAutorizacaoMotivoEncerramento() != null) {
									if(servico.getServicoAutorizacaoMotivoEncerramento().getFluxoEncerramento() != null && servico.getServicoAutorizacaoMotivoEncerramento().getFluxoEncerramento()) {
										
										
										Collection<ServicoAutorizacaoHistorico> historico = listarServicoAutorizacaoHistorico(servico.getChavePrimaria());

										byte[] relatorio = gerarRelatorio(servico.getChavePrimaria(), Boolean.FALSE , historico);
										
										enviarArquivoPorEmail(servico.getCliente().getEmailPrincipal(), relatorio, servico.getConteudoEncerramentoAutorizacaoServicoCliente(), "Algás - Serviço Realizado", "Autorização de Serviço");

									}
								}
							}

						
					}
				}
			}
			
		}
		
		if (acao == AcaoServicoAutorizacao.ENCERRAR && servidorEmailConfigurado != null
				&& BooleanUtil.converterStringCharParaBooleano(servidorEmailConfigurado.getValor())) {
			enviarEmailPersonalizado(servico, emailRemetentePadrao, mailUtil);
		}

	}

	@Override
	public void enviarEmailPersonalizado(ServicoAutorizacao servico, ParametroSistema emailRemetentePadrao,
			JavaMailUtil mailUtil) throws NegocioException, GGASException {
		if (servico.getServicoTipo().getIndicadorEmailPersonalizado() != null
				&& servico.getServicoTipo().getIndicadorEmailPersonalizado()
				&& StringUtils.isNotEmpty(servico.getServicoTipo().getListaEmailPersonalizado())
				&& StringUtils.isNotEmpty(servico.getServicoTipo().getLayoutEmail())) {
			String layoutEmail = servico.getServicoTipo().getLayoutEmail();

			ConstanteSistema constanteTipoRelatorio = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_EMAIL_AS);
			Long tipoRelatorio = constanteTipoRelatorio != null ? constanteTipoRelatorio.getValorLong() : 0l;
			

			Collection<RelatorioToken> listaTokens = controladorRelatorio.obterListaRelatorioTokens(tipoRelatorio);
			
			ServicoAutorizacaoTemp servicoAutorizacaoTemp = this.consultarAutorizacaoServicoTemp(servico);
			
			RelatorioServicoTipoHelper relatorioServicoTipoHelper = new RelatorioServicoTipoHelper();
			relatorioServicoTipoHelper.setServicoAutorizacao(servico);
			relatorioServicoTipoHelper.setServicoAutorizacaoTemp(servicoAutorizacaoTemp);
			relatorioServicoTipoHelper.setLayout(layoutEmail);
			
			relatorioServicoTipoHelper.processarLayout(listaTokens);
			

			for (String emailDestinatario : servico.getServicoTipo().getListaEmailPersonalizado().split(";")) {
				mailUtil.enviar(emailRemetentePadrao.getValor(), emailDestinatario, servico.getServicoTipo().getAssuntoEmail(),
						relatorioServicoTipoHelper.getLayout(), true, false);
			}

		}
	}

	private boolean enviarArquivoPorEmail(String email, byte[] arquivo, String corpoMsg, String assunto,
			String nomeAnexo) throws NegocioException {
		ControladorConstanteSistema constanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		
		JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);
		if (email != null) {
			ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(arquivo, "application/pdf");
			try {
				String valorConstanteSistemaEmailRemetentePadrao = constanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
//				mailUtil.enviar(valorConstanteSistemaEmailRemetentePadrao, email, assunto.toString(), corpoMsg, nomeAnexo, byteArrayDataSource, true);
				mailUtil.enviar(valorConstanteSistemaEmailRemetentePadrao, email, assunto.toString(), corpoMsg, nomeAnexo,
						byteArrayDataSource, true, true);
				return true;
			} catch (GGASException e) {
				LOG.error(e.getStackTrace(), e);
				throw new NegocioException(Constantes.ERRO_ENVIO_ARQUIVO_EMAIL, true);
			}
		}
		return false;
	}
	
	/**
	 * @param listaUnidadeOrganizacionalVisualizadora
	 * @param unidadeOrganizacional
	 * @return true se unidadeOrganizacional estiver na lista ou false
	 */
	private boolean isUnidadeNaLista(Collection<ChamadoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora,
			UnidadeOrganizacional unidadeOrganizacional) {

		for (ChamadoUnidadeOrganizacional chamadoUnidade : listaUnidadeOrganizacionalVisualizadora) {
			if (chamadoUnidade.getUnidadeOrganizacional().getChavePrimaria() == unidadeOrganizacional.getChavePrimaria()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @param acao acao
	 * @param servicoTipo tipo do serviço
	 * @return true se for para enviar email ou false caso nao
	 */
	private Boolean validarEnvioEmail(AcaoServicoAutorizacao acao, ServicoTipo servicoTipo) {
		Collection<AcaoServicoAutorizacao> listaAcoes = servicoTipo.getListaAcaoComEmail();
		return listaAcoes.contains(acao);
	}

	/**
	 * @param servicoAutorizacaoHistoricoAnexo
	 * @throws NegocioException
	 */
	@Override
	public void validarDadosAnexo(ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo) throws NegocioException {

		Map<String, Object> errosServicoAutorizacaoHistoricoAnexo = servicoAutorizacaoHistoricoAnexo.validarDados();

		if (errosServicoAutorizacaoHistoricoAnexo != null && !errosServicoAutorizacaoHistoricoAnexo.isEmpty()) {
			throw new NegocioException(errosServicoAutorizacaoHistoricoAnexo);
		}

	}

	/**
	 * @param indexLista
	 * @param listaAnexo
	 * @param servicoAutorizacaoHistoricoAnexo
	 * @throws NegocioException
	 */
	@Override
	public void verificarDescricaoAnexo(Integer indexLista, Collection<ServicoAutorizacaoHistoricoAnexo> listaAnexo,
			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo) throws NegocioException {

		ArrayList<ServicoAutorizacaoHistoricoAnexo> listaAnexoArray = new ArrayList<>(listaAnexo);
		for (int i = 0; i < listaAnexo.size(); i++) {

			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexoExistente = listaAnexoArray.get(i);

			String descricao = servicoAutorizacaoHistoricoAnexoExistente.getDescricaoAnexo();
			if (descricao != null && descricao.equals(servicoAutorizacaoHistoricoAnexo.getDescricaoAnexo()) && indexLista != i) {
				throw new NegocioException(ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao#existeASQuestionario(br.com.ggas.atendimento.
	 * questionario.dominio.Questionario)
	 */
	@Override
	public boolean existeASQuestionario(Questionario questionario) {
		return repositorioServicoAutorizacao.existeASQuestionario(questionario);
	}

	private void verificarDataPrevisaoEncerramentoServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, Date dataInicio)
			throws GGASException {
		if (servicoAutorizacao.getDataExecucao() != null
				&& (servicoAutorizacao.getDataExecucao().before(dataInicio) || servicoAutorizacao.getDataExecucao().after(new Date()))) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_EXECUTAR_DATAS, true);
		}
	}

	@Override
	public Optional<ServicoAutorizacao> consultarServicoAutorizacaoReferenciaGarantia(Long chavePrimariaCliente,
			Long chavePrimariaServicoTipo, Long chaveImovel) {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoReferenciaGarantia(chavePrimariaCliente, chavePrimariaServicoTipo,
				chaveImovel);
	}

	@Override
	public Long consultarNumeroExecucoesPorClienteEServicoTipo(Long chavePrimariaCliente, Long chavePrimariaServicoTipo, Long chaveImovel,
			Date dataGarantia) {
		return repositorioServicoAutorizacao.consultarNumeroExecucoesPorClienteEServicoTipo(chavePrimariaCliente, chavePrimariaServicoTipo,
				chaveImovel, dataGarantia);
	}

	/**
	 * Lista os agendamentos de um cliente
	 * @param cliente
	 * @return retorna lista de Agendamentos do Cliente
	 */
	@Override
	public List<ServicoAutorizacaoAgendasVO> listarAgandamentosPorCliente(Cliente cliente) {
		return repositorioServicoAutorizacao.listarAgandamentosPorCliente(cliente);
	}
	
	/**
	 * Método de envio de email de agendamento de serviço
	 * @param acao
	 * @param servico
	 * @param servicoAutorizacaoAgendaPesquisaVO
	 * @return valor true ou false
	 * @throws GGASException
	 */
	public Boolean enviarEmailAgendamentoServico(AcaoServicoAutorizacao acao, ServicoAutorizacao servico,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws GGASException {
		
		String descricaoturno = "";
		String orientacaoServico = "";
		String numeroProtocolo = "";
		String destinatario = "";
		Boolean isVeioDoChamado = false;
		
		ServicoAutorizacao servicoAutorizacao = repositorioServicoAutorizacao
				.obterUnidadeOrganizacionalPorServicoAutorizacao(servico.getChavePrimaria());
		
		if(servicoAutorizacao.getEquipe().getUnidadeOrganizacional().getEmailContato() != null){
			destinatario = servico.getCliente().getEmailPrincipal() + servicoAutorizacao.getEquipe().getUnidadeOrganizacional().getEmailContato();
		}else{
			destinatario = servico.getCliente().getEmailPrincipal();
		}
		
		if(servicoAutorizacaoAgendaPesquisaVO.getTurno() != null){
			descricaoturno = servicoAutorizacaoAgendaPesquisaVO.getTurno().getDescricao();
		}
		
		if(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getOrientacao() != null){
			orientacaoServico = servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getOrientacao();
		}
		
		if(servico.getChamado() != null){
			numeroProtocolo = servico.getChamado().getProtocolo().toString();
			isVeioDoChamado = true;
		}
		
		String[] listaInformacoes = new String[]{servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(),
			numeroProtocolo, servico.getCliente().getNome(), descricaoturno,orientacaoServico,
			servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getDescricao()};
	
		String conteudoEmail = StringUtils.join(listaInformacoes, ";");
		String assuntoEmail = ("[GGAS] - Confirmação de Agendamento");

		JavaMailUtil javaMailUtil = (JavaMailUtil) ServiceLocator.getInstancia()
				.getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		try {
			javaMailUtil.enviarAgendamentoServico(destinatario, assuntoEmail, conteudoEmail, isVeioDoChamado,  true);
			return true;
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			return false;
		}
	}
	
	@Override
	public Collection<AvisoCorte> priorizacaoCorte(Collection<AvisoCorte> listaAvisoCorte) throws NegocioException{
				
		Map<PontoConsumo, BigDecimal> listaSomaDebitos = obterSomarTotalDebitos(listaAvisoCorte);
		
		for(AvisoCorte avisoCorte : listaAvisoCorte) {
			
			BigDecimal somaDebitos = listaSomaDebitos.get(avisoCorte.getPontoConsumo());
						
			Integer vencimentoTituloAvisoCorte = DataUtil.diferencaDiasEntreDatas(avisoCorte.getFatura().getDataVencimento(), new Date());
			BigDecimal criticidade = obterValorCriticidade(vencimentoTituloAvisoCorte);
				
			avisoCorte.setProdutoCorte(
					somaDebitos.multiply(criticidade.multiply(new BigDecimal(vencimentoTituloAvisoCorte))).intValue());
			avisoCorte.setCriticidade(criticidade.intValue());
		}
		return Util.ordenarColecaoPorAtributo(listaAvisoCorte, "produtoCorte", Boolean.FALSE);
	}
	
	
	/**
	 * Obtem a soma dos debitos de todas as faturas do ponto de consumo
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @return Map com a chave do ponto de consumo e a soma dos debitos vencidos
	 * @throws NegocioException - {@link NegocioException}
	 */
	private Map<PontoConsumo, BigDecimal> obterSomarTotalDebitos(Collection<AvisoCorte> listaAvisoCorte) throws NegocioException{
		Map<PontoConsumo, AvisoCorte> pontoConsumoAvisoCorte = new HashMap<>();
		Map<PontoConsumo, BigDecimal> listaSomaDebito = new HashMap<>();
		
		for(AvisoCorte avisoCorte : listaAvisoCorte ) {
			pontoConsumoAvisoCorte.put(avisoCorte.getPontoConsumo(), avisoCorte);
		}
				
		for(Map.Entry<PontoConsumo, AvisoCorte> entry : pontoConsumoAvisoCorte.entrySet()) {
			Collection<Fatura> listaFatura = controladorFatura.consultarFaturasVencidasPontoConsumo(entry.getKey().getChavePrimaria());
			
			BigDecimal somaDebitos = BigDecimal.ZERO;
			
			for(Fatura fatura : listaFatura) {				
				somaDebitos = somaDebitos.add(fatura.getValorTotal());
			}
			
			listaSomaDebito.put(entry.getKey(), somaDebitos);
		}
		
		return listaSomaDebito;
	}
	@Override
	public Collection<AvisoCorte> checagemExecucaoServicoCorte(Collection<AvisoCorte> listaAvisoCorte)
			throws GGASException {
		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
		List<String> listaStatus = new ArrayList<>();

		ServicoTipo servicoTipo = repositorioServicoTipo.obterServicoTipoPorDescricao(ServicoTipo.CORTE);

		servicoAutorizacaoVO.setServicoTipo(servicoTipo);
		listaStatus.add(ServicoAutorizacao.EM_EXECUCAO);
		listaStatus.add(ServicoAutorizacao.ABERTA);
		listaStatus.add(ServicoAutorizacao.PENDENTE_DE_ATUALIZACAO);
		servicoAutorizacaoVO.setListaStatus(listaStatus);

		Collection<ServicoAutorizacao> listaServicoAutorizacaoCorte = listarServicoAutorizacao(servicoAutorizacaoVO,
				false);

		Map<PontoConsumo, ServicoAutorizacao> mapServicoAutorizacao = new HashMap<>();
		for (ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacaoCorte) {
			mapServicoAutorizacao.put(servicoAutorizacao.getPontoConsumo(), servicoAutorizacao);
		}

		for (AvisoCorte avisoCorte : listaAvisoCorte) {

			if (checagemAutorizacaoServicoSobreAvisoDeCorte(mapServicoAutorizacao, avisoCorte)) {


				// SERVICO_AUTORIZACAO
				Map<String, Object> dados = new HashMap<>();

				ServicoAutorizacao servicoAutorizacao = construirObjetoServicoAutorizacaoCorte(mapServicoAutorizacao, avisoCorte);

				try {
					String data = DataUtil.converterDataParaString(servicoAutorizacao.getDataPrevisaoEncerramento(),
							Boolean.FALSE);
					dados.put("dataPrevisaoEncerramento", data);

					atualizarServicoAutorizacao(servicoAutorizacao, dados, null, null);
				} catch (ParseException e) {
					LOG.error(e);
				}

				// AVISOCORTE
				avisoCorte.setNumeroChecagens(avisoCorte.getNumeroChecagens() + 1);
				controladorAvisoCorte.atualizar(avisoCorte);
			}
		}

		return listaAvisoCorte;

	}
		
	/**
	 * Checagem da quantidade de dias parametrizado com a data de geracao da autorizacao de servico de corte
	 * @param mapServicoAutorizacao - map de servicos de autorizacao de corte
	 * @param avisoCorte - {@link AvisoCorte}
	 * @return Boolean - {@link Boolean}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private Boolean checagemAutorizacaoServicoSobreAvisoDeCorte(
			Map<PontoConsumo, ServicoAutorizacao> mapServicoAutorizacao, AvisoCorte avisoCorte)
			throws NegocioException {
		ParametroSistema parametroQuantidadeDias = controladorParametroSistema
				.obterParametroPorCodigo(ParametroSistema.NUMERO_DIAS_CHECAGEM_CORTE);

		if (mapServicoAutorizacao.containsKey(avisoCorte.getPontoConsumo()) && DataUtil.diferencaDiasEntreDatas(
				mapServicoAutorizacao.get(avisoCorte.getPontoConsumo()).getDataGeracao(),
				new Date()) >= parametroQuantidadeDias.getValorInteger()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Controi objeto para cancelar servico autorizacao 
	 * @param mapServicoAutorizacao - map de servicos de autorizacao de corte
	 * @param avisoCorte - {@link AvisoCorte}
	 * @return ServicoAutorizacao - {@link ServicoAutorizacao}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private ServicoAutorizacao construirObjetoServicoAutorizacaoCorte(
			Map<PontoConsumo, ServicoAutorizacao> mapServicoAutorizacao, AvisoCorte avisoCorte)
			throws NegocioException {

		EntidadeConteudo statusCancelado = controladorEntidadeConteudo
				.listarEntidadeConteudoPorEntidadeClasseEDescricao("Status Autorizacao de Servico",
						"CANCELADA");

		ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivo = obterServicoAutorizacaoMotivoEncerramentoPelaDescricao(
				ServicoAutorizacaoMotivoEncerramento.CANCELAMENTO);
		
		ServicoAutorizacao servicoAutorizacao = mapServicoAutorizacao.get(avisoCorte.getPontoConsumo());
		servicoAutorizacao.setStatus(statusCancelado);
		servicoAutorizacao.setDataEncerramento(new Date());
		servicoAutorizacao.setServicoAutorizacaoMotivoEncerramento(servicoAutorizacaoMotivo);
		
		return servicoAutorizacao;
	}
	
	private BigDecimal obterValorCriticidade(Integer vencimentoTituloAvisoCorte) throws NegocioException {
		BigDecimal criticidade;
		Integer prazoMinimo = controladorParametroSistema.obterParametroPorCodigo(Constantes.PRAZO_MINIMO_CRITICIDADE)
				.getValorInteger();
		Integer prazoMaximo = controladorParametroSistema.obterParametroPorCodigo(Constantes.PRAZO_MAXIMO_CRITICIDADE)
				.getValorInteger();
		Integer valorMinimoCriticidade = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.VALOR_MINIMO_CRITICIDADE).getValorInteger();
		Integer valorMedioCriticidade = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.VALOR_MEDIO_CRITICIDADE).getValorInteger();
		Integer valorMaximoCriticidade = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.VALOR_MAXIMO_CRITICIDADE).getValorInteger();

		if (vencimentoTituloAvisoCorte <= prazoMinimo) {
			criticidade = new BigDecimal(valorMinimoCriticidade);
		} else if (vencimentoTituloAvisoCorte <= prazoMaximo) {
			criticidade = new BigDecimal(valorMedioCriticidade);
		} else {
			criticidade = new BigDecimal(valorMaximoCriticidade);
		}		
		return criticidade;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AvisoCorte> roteirizacaoCorte(Collection<AvisoCorte> listaAvisoCorte) throws NegocioException{
		Collection<AvisoCorte> listaAvisoCorteAux = (Collection<AvisoCorte>) SerializationUtils.clone((Serializable) listaAvisoCorte);
		Double latitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
		Double longitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());		
		AvisoCorte avisoCorte = listaAvisoCorteAux.iterator().next();
		Collection<AvisoCorte> listaRoteirizada = new HashSet<>();
		Integer ordem = 1;
		
		while (!listaAvisoCorteAux.isEmpty()) {
			avisoCorte = calculaPontoConsumoMenorDistancia(listaAvisoCorteAux, latitudeComparar, longitudeComparar);
			
			latitudeComparar = avisoCorte.getPontoConsumo().getLatitudeGrau().doubleValue();
			longitudeComparar = avisoCorte.getPontoConsumo().getLongitudeGrau().doubleValue();
			
			avisoCorte.setOrdemRoteirizar(ordem);
			
			listaRoteirizada.add(avisoCorte);
			listaAvisoCorteAux.remove(avisoCorte);
			
			ordem++;
		}
		
		return Util.ordenarColecaoPorAtributo(listaRoteirizada, "ordemRoteirizar", Boolean.TRUE);
	}
	
	private AvisoCorte calculaPontoConsumoMenorDistancia(Collection<AvisoCorte> listaAvisoCorte,
			Double latitudeComparar, Double longitudeComparar) {
		Double menorDistancia = -1.0;
		AvisoCorte avisoCorte = listaAvisoCorte.iterator().next();
		for (AvisoCorte avisoCorteAux : listaAvisoCorte) {
			Double distanciaCalculada = Util.calculoDistancia(latitudeComparar, longitudeComparar,
					avisoCorteAux.getPontoConsumo().getLatitudeGrau().doubleValue(),
					avisoCorteAux.getPontoConsumo().getLongitudeGrau().doubleValue());

			if (distanciaCalculada < menorDistancia || menorDistancia.compareTo(Double.valueOf(-1.0)) == 0) {
				menorDistancia = distanciaCalculada;
				avisoCorte = avisoCorteAux;
			}
		}
		
		avisoCorte.setDistanciaEntrePontos(menorDistancia);
		return avisoCorte;
	}
	
	/**
	 * Método responsável para emitir as Autorizacoes de Servico em Lote via processo batch 
	 * 
	 * @param avisos {@link Collection<AvisoCorte>}
	 * @param usuario {@link Usuario}
	 * @param dadosAuditoria {@link DadosAuditoria}
	 */
	public Processo inserirProcessoEmLoteParaCorte(Collection<AvisoCorte> avisos, Usuario usuario,
			DadosAuditoria dadosAuditoria) throws GGASException {
		AcaoComando acaoComando = inserirAcaoComando();
		inserirAcaoComandoPontoConsumo(avisos, acaoComando);
		return inserirProcesso(usuario, dadosAuditoria, acaoComando.getChavePrimaria());
	}
	
	private AcaoComando inserirAcaoComando() throws GGASException {
		Acao acaoAux = new Acao();
		acaoAux.setDescricao("AUTORIZACAO DE SERVICO (CORTE)");
		Acao acao = (Acao) controladorAcao.consultarAcao(acaoAux, Boolean.TRUE).iterator().next();
		
		AcaoComandoEstendida acaoComando = new AcaoComandoEstendida();
		acaoComando.setAcao(acao);
		acaoComando.setNome(getNomeAcaoComando());
		acaoComando.setDescricao(Constantes.DESCRICAO_ACAO_COMANDO_AS_LOTE);
		acaoComando.setIndicadorSimulacao(Boolean.FALSE);
		acaoComando.setHabilitado(Boolean.TRUE);
		acaoComando.setVersao(0);
		
		long id = repositorioAcaoComando.inserir(acaoComando);
		
		acaoComando.setChavePrimaria(id);
		
		return acaoComando;
	}
	
	private String getNomeAcaoComando() throws FormatoInvalidoException {
		String dataHora = DataUtil.converterDataParaString(new Date(), Boolean.TRUE);
		
		String dataHoraFormatada = DataUtil.formatarDataString(dataHora, Constantes.FORMATO_DATA_HORA_BR, Constantes.FORMATO_DATA_HORA_US);
		dataHoraFormatada = dataHoraFormatada.replace(":",	"").replace("-", "").replace(" ", "");
		
		return "AS_CORTE_LOTE_" + dataHoraFormatada;
	}
	
	private void inserirAcaoComandoPontoConsumo(Collection<AvisoCorte> avisos, AcaoComando acaoComando) throws NegocioException {
		for (AvisoCorte aviso : avisos) {
			
			Collection<Fatura> colecaoFaturasFilhas = controladorFatura
					.consultarFaturaPorFaturaAgrupamento(aviso.getFatura().getChavePrimaria());
			if (colecaoFaturasFilhas != null && !colecaoFaturasFilhas.isEmpty()) {
				for (Fatura faturaAgrupada : colecaoFaturasFilhas) {
					AcaoComandoPontoConsumo acaoComandoPontoConsumo = new AcaoComandoPontoConsumo();
					acaoComandoPontoConsumo.setAcaoComando(acaoComando);
					acaoComandoPontoConsumo.setPontoConsumo(faturaAgrupada.getPontoConsumo());
					acaoComandoPontoConsumo.setHabilitado(Boolean.TRUE);
					acaoComandoPontoConsumo.setVersao(0);

					repositorioAcaoComando.inserir(acaoComandoPontoConsumo);
				}
			} else {
				AcaoComandoPontoConsumo acaoComandoPontoConsumo = new AcaoComandoPontoConsumo();
				acaoComandoPontoConsumo.setAcaoComando(acaoComando);
				acaoComandoPontoConsumo.setPontoConsumo(aviso.getPontoConsumo());
				acaoComandoPontoConsumo.setHabilitado(Boolean.TRUE);
				acaoComandoPontoConsumo.setVersao(0);

				repositorioAcaoComando.inserir(acaoComandoPontoConsumo);
			}

		}
	}
	
	private Processo inserirProcesso(Usuario usuario, DadosAuditoria dadosAuditoria, long idAcaoComando)
			throws GGASException {
		Processo processo = (Processo) controladorProcesso.criar();
		processo.setOperacao(getOperacao());
		processo.setAgendado(Boolean.FALSE);
		processo.setVersao(0);
		processo.setDataInicioAgendamento(Calendar.getInstance().getTime());
		processo.setDataFinalAgendamento(null);
		processo.setHabilitado(Boolean.TRUE);
		processo.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
		processo.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
		processo.setUsuario(usuario);
		processo.setDiaNaoUtil(Boolean.TRUE);
		processo.setDadosAuditoria(dadosAuditoria);
		processo.setDescricao("Emitir Autorizações de Serviço em Lote: CORTE");

		Map<String, String> parametros = new HashMap<String, String>();
		parametros.put("idComando", String.valueOf(idAcaoComando));
		processo.setParametros(parametros);

		processo.setChavePrimaria(controladorProcesso.inserir(processo));
		
		return processo;
	}
	
	private Operacao getOperacao() {
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_OPERACAO_GERAR_SERV_AUT_LOTE);
		return controladorAcesso.obterOperacaoSistema(Long.parseLong(constante.getValor()));
	}
	
	
	private ServicoAutorizacaoVORelatorio converterServicoAutorizacaoParaVOAlgas(ServicoAutorizacao servicoAutorizacao) throws NegocioException {
		ServicoAutorizacaoVORelatorio relatorio = new ServicoAutorizacaoVORelatorio();
		
		montarInformacaoComumRelatorioAlgas(relatorio, servicoAutorizacao);
		
		ServicoAutorizacaoAgenda servicoAutorizacaoAgenda = repositorioServicoAutorizacao
				.consultarServicoAutorizacaoAgendaPorServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
		
		if(servicoAutorizacaoAgenda != null) {
			
			StringBuilder mensagemAgendamento = new StringBuilder();
			mensagemAgendamento.append(DataUtil.converterDataParaString(servicoAutorizacaoAgenda.getDataAgenda(), false));
			
			relatorio.setAgendamento(mensagemAgendamento.toString());
		}
		
		Collection<FaturaVO> colecaoFaturasVencidas = montarFaturasVencidas(servicoAutorizacao.getPontoConsumo().getCodigoLegado(), relatorio);
		
		relatorio.setFaturasVencidas(colecaoFaturasVencidas);
		
		
		
		return relatorio;
	}
	
	private void montarInformacoesNovoMedidor(ServicoAutorizacaoVORelatorio relatorio,
			ServicoAutorizacao servicoAutorizacao, ServicoAutorizacaoTemp servicoAutorizacaoTemp) {
		Medidor medidor = null;

		if (servicoAutorizacao.getMedidor() != null) {
			medidor = servicoAutorizacao.getMedidor();
			
			FiltroHistoricoOperacaoMedidor filtroHistorico = new FiltroHistoricoOperacaoMedidor();
			filtroHistorico.setIdPontoConsumo(servicoAutorizacao.getPontoConsumo().getChavePrimaria());
			filtroHistorico.setOrdenacaoDataAlteracao(Ordenacao.DESCENDENTE);
			filtroHistorico.setLimiteResultados(1);
			HistoricoOperacaoMedidor historico = controladorMedidor.obterUnicoHistoricoOperacaoMedidor(filtroHistorico);
			
			if(historico != null && historico.getMedidor().getChavePrimaria() == medidor.getChavePrimaria()) {
				relatorio.setNumeroLeituraNovo(historico.getNumeroLeitura().toString());
			}
		
		} else if (servicoAutorizacaoTemp == null && "ENCERRADA".equals(servicoAutorizacao.getStatus().getDescricao())
				&& servicoAutorizacao.getPontoConsumo() != null
				&& servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor() != null) {
			medidor = servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor();
		}

		relatorio.setNovoMedidorModelo(medidor != null ? medidor.getModelo().getDescricaoAbreviada() : "");
		relatorio.setNovoMedidorSerie(medidor != null ? medidor.getNumeroSerie() : "");
		relatorio.setNovoMedidorTipo(medidor != null ? medidor.getTipoMedidor().getDescricao() : "");
		relatorio.setNovoMedidorFabricante(medidor != null ? medidor.getMarcaMedidor().getDescricao() : "");
		relatorio.setNovoPlacaBP(medidor != null && StringUtils.isNotEmpty(medidor.getTombamento()) ? medidor.getTombamento() : "" );
	}

	private Collection<FaturaVO> montarFaturasVencidas(String codigoUnico, ServicoAutorizacaoVORelatorio relatorio)
			throws NegocioException {
		Collection<FaturaVO> faturasVencidas = new HashSet<FaturaVO>();
		int i = 0;
		String mensagemDebito = "";

		for (Object object : controladorFatura.listarFaturasAbertoGSA(codigoUnico)) {
			FaturaVO faturaVencida = new FaturaVO();
			Object[] resultado = (Object[]) object;

			faturaVencida.setNumero(resultado[0] != null ? resultado[0].toString() : "");
			faturaVencida.setDataEmissao(DataUtil.converterParaData(resultado[1].toString()));
			faturaVencida.setDataVencimento(DataUtil.converterParaData(resultado[2].toString()));
			faturaVencida.setValor(resultado[3] != null ? resultado[3].toString() : "");

			faturasVencidas.add(faturaVencida);

			i++;

			if (i == 6) {
				mensagemDebito = "Existem outros débitos, solicite o extrato total dessas informações.";
				break;
			}
		}

		relatorio.setMensagemDebito(mensagemDebito);
		return faturasVencidas;
	}
	
	private void montarInformacaoComumRelatorioAlgas(ServicoAutorizacaoVORelatorio relatorio, ServicoAutorizacao servicoAutorizacao) throws NegocioException {
		
		if(servicoAutorizacao.getCliente() != null) {
			relatorio.setNomeCliente(servicoAutorizacao.getCliente().getNome());
		} else {
			relatorio.setNomeCliente("");
		}
		
		if(servicoAutorizacao.getImovel() != null) {
			relatorio.setEnderecoImovel(servicoAutorizacao.getImovel().getEnderecoFormatado());
		} else {
			relatorio.setEnderecoImovel("");
		}
		
		if(servicoAutorizacao.getPontoConsumo() != null) {
			relatorio.setCodigoUnico(servicoAutorizacao.getPontoConsumo().getCodigoLegado());
			relatorio.setNomePontoConsumo(servicoAutorizacao.getPontoConsumo().getDescricao());
		} else {
			relatorio.setCodigoUnico("");
			relatorio.setNomePontoConsumo("");
		}
		
		relatorio.setCodigo(String.valueOf(servicoAutorizacao.getChavePrimaria()));
		if(servicoAutorizacao.getChamado() != null) {
			relatorio.setProtocoloChamado(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo().toString());
		} else if (servicoAutorizacao.getNumeroProtocolo() != null){
			relatorio.setProtocoloChamado(servicoAutorizacao.getNumeroProtocolo().toString());
		} else {
			relatorio.setProtocoloChamado("");
		}
		
		if(servicoAutorizacao.getNumeroOS() != null) {
			relatorio.setNumeroOS(String.valueOf(servicoAutorizacao.getNumeroOS()));
		} else {
			relatorio.setNumeroOS("");
		}
		
		if(servicoAutorizacao.getDataEncerramento() != null) {
			relatorio.setDataEncerramento(String.valueOf(servicoAutorizacao.getDataEncerramentoFormatada()).split(" ")[0]);
		}else {
			relatorio.setDataEncerramento("");
		}
		
		if(servicoAutorizacao.getDataGeracao() != null && servicoAutorizacao.getChamado() == null) {
			relatorio.setDataGeracao(servicoAutorizacao.getDataGeracaoFormatada());
		}else {
			if(servicoAutorizacao.getChamado() != null) {
					// Data do histórico chamado para AS que utiliza formulário de atendimento
				
						if(servicoAutorizacao.getChamado().getChamadoHistoricos() != null) {
							Date dataInclusaoChamado = null;
								
							ChamadoHistorico chamadoHistorico = servicoAutorizacao.getChamado()
								    .getChamadoHistoricos().stream()
								    .min(Comparator.comparing(ChamadoHistorico::getUltimaAlteracao))
								    .orElse(null);
							
							dataInclusaoChamado = chamadoHistorico != null ? chamadoHistorico.getUltimaAlteracao() : null;
							 
							if(dataInclusaoChamado == null) {
								relatorio.setDataGeracao("");
							} else {
								relatorio.setDataGeracao(DataUtil.converterDataParaString(dataInclusaoChamado,Boolean.TRUE));
							}
						}else {
							relatorio.setDataGeracao("");
						}
						//Fim da inserção para formulário de atendimento
			}else {
				relatorio.setDataGeracao(""); //
			}
		}
		if(servicoAutorizacao.getDescricao() != null) {
			String descricao = "";
			
			if(servicoAutorizacao.getServicoAutorizacaoHistoricos() != null && !servicoAutorizacao.getServicoAutorizacaoHistoricos().isEmpty()) {
				ServicoAutorizacaoHistorico servicoAutorizacaoHistorico = servicoAutorizacao
						.getServicoAutorizacaoHistoricos().stream()
						.sorted(Comparator.comparing(ServicoAutorizacaoHistorico::getChavePrimaria).reversed())
						.findFirst()
						.orElse(null);
				
				descricao = servicoAutorizacaoHistorico.getDescricao();
				
			}
			
			relatorio.setDescricao(descricao);
		}else {
			relatorio.setDescricao("");
		}
		if(servicoAutorizacao.getChamado() != null && !StringUtils.isEmpty(servicoAutorizacao.getChamado().getHoraAcionamento()) ) {
			String dataChamado = servicoAutorizacao.getChamado().getHoraAcionamento();
			
			relatorio.setDataChamado(DataUtil.setarDataCompleta(relatorio.getDataGeracao(), dataChamado,
					DataUtil.compararHoras(relatorio.getDataGeracao(), dataChamado)));
		}else {
			relatorio.setDataChamado("");
		}
		
		relatorio.setServicoOrientacao(servicoAutorizacao.getServicoTipo().getOrientacao());
		
		Double diferencaTempoGeracaoPrevisao = (DataUtil.obterDiferencaMinutosEntreDatas(
				servicoAutorizacao.getDataGeracao(), servicoAutorizacao.getDataPrevisaoEncerramento())) / 60.0;
		
		String mensagemUrgencia = "";
		
		if(diferencaTempoGeracaoPrevisao.compareTo(12.0) <= 0) {
			mensagemUrgencia = "EXECUTAR NO DIA "
					+ DataUtil.converterDataParaString(servicoAutorizacao.getDataPrevisaoEncerramento(), false)
					+ " ATÉ AS " + Util.obterHoraData(servicoAutorizacao.getDataPrevisaoEncerramento(), Boolean.FALSE);
		}
		
		relatorio.setMensagemUrgencia(mensagemUrgencia);
		
		String medidorModelo = "";
		String medidorSerie = "";
		String medidorTipo = "";
		String medidorFabricante = "";
		String placaBP = "";
		String ultimaLeitura = "";
		String situacao = "";
		String motivoEncerramento = "";

		
		String operacao = StringUtils.isNotEmpty(servicoAutorizacao.getServicoTipo().getIndicadorOperacaoMedidor()) ? servicoAutorizacao.getServicoTipo().getIndicadorOperacaoMedidor() : "0";
		
		if ((OperacaoMedidor.CODIGO_SUBSTITUICAO != Long.valueOf(operacao) || !"ENCERRADA".equals(servicoAutorizacao.getStatus().getDescricao())) && (servicoAutorizacao.getPontoConsumo() != null && servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor() != null)
				&& (servicoAutorizacao.getPontoConsumo() != null && servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor() != null)) {
			Medidor medidor = servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor();
			HistoricoMedicao ultimoHistorico = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(medidor, null, null, Boolean.TRUE);

			medidorModelo = medidor.getModelo().getDescricaoAbreviada();
			medidorSerie = medidor.getNumeroSerie();
			medidorTipo = medidor.getTipoMedidor().getDescricao();
			medidorFabricante = medidor.getMarcaMedidor().getDescricao();
			placaBP = medidor.getTombamento() != null ? medidor.getTombamento() : "";
			ultimaLeitura = ultimoHistorico != null && ultimoHistorico.getNumeroLeituraInformada() != null ? ultimoHistorico.getNumeroLeituraInformada().toString() : "";
			
		} else if(servicoAutorizacao.getPontoConsumo() != null && servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor() != null
				&& servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor() != null) {
			FiltroHistoricoOperacaoMedidor filtroHistorico = new FiltroHistoricoOperacaoMedidor();
			filtroHistorico.setIdPontoConsumo(servicoAutorizacao.getPontoConsumo().getChavePrimaria());
			filtroHistorico.setOrdenacaoDataAlteracao(Ordenacao.DESCENDENTE);
			filtroHistorico.setLimiteResultados(1);
			filtroHistorico.setIdMedidorDiferente(servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor().getChavePrimaria());
			HistoricoOperacaoMedidor historico = controladorMedidor.obterUnicoHistoricoOperacaoMedidor(filtroHistorico);
			
			if(historico != null) {
				Medidor medidor = historico.getMedidor();
				
				HistoricoMedicao ultimoHistorico = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(medidor, null, null, Boolean.TRUE);
				
				medidorModelo = medidor.getModelo().getDescricaoAbreviada();
				medidorSerie = medidor.getNumeroSerie();
				medidorTipo = medidor.getTipoMedidor().getDescricao();
				medidorFabricante = medidor.getMarcaMedidor().getDescricao();
				placaBP = medidor.getTombamento() != null ? medidor.getTombamento() : "";
				ultimaLeitura = ultimoHistorico != null && ultimoHistorico.getNumeroLeituraInformada() != null  ? ultimoHistorico.getNumeroLeituraInformada().toString() : "";
				
				relatorio.setNumeroLeituraOriginal(historico.getNumeroLeitura() != null ? historico.getNumeroLeitura().toString() : "");
			} else {
				medidorModelo = "";
	            medidorSerie = "";
	            medidorTipo = "";
	            medidorFabricante = "";
	            placaBP = "";
	            ultimaLeitura = "";
			}

		} else {
			medidorModelo = "";
            medidorSerie = "";
            medidorTipo = "";
            medidorFabricante = "";
            placaBP = "";
            ultimaLeitura = "";
		}
		
		if(servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() != null) {
			motivoEncerramento = servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getDescricao();
		}
		
		Collection<ServicoAutorizacaoHistorico> servicoAutorizacaoHistoricos = servicoAutorizacao.getServicoAutorizacaoHistoricos();
		List<ServicoAutorizacaoHistorico> listaAutorizacaoHistoricos = new ArrayList<>(servicoAutorizacaoHistoricos);
		if (!listaAutorizacaoHistoricos.isEmpty()) {
			listaAutorizacaoHistoricos.sort(Comparator.comparing(ServicoAutorizacaoHistorico::getUltimaAlteracao));
			ServicoAutorizacaoHistorico ultimoItemHistorico = listaAutorizacaoHistoricos.get(listaAutorizacaoHistoricos.size() - 1);
			if(ultimoItemHistorico.getStatus()!= null) {
				situacao = ultimoItemHistorico.getStatus().getDescricao();
			}
		}
		
		relatorio.setMedidorModelo(medidorModelo);
		relatorio.setMedidorSerie(medidorSerie);
		relatorio.setMedidorTipo(medidorTipo);
		relatorio.setMedidorFabricante(medidorFabricante);
		relatorio.setPlacaBP(placaBP);
		relatorio.setUltimaLeitura(ultimaLeitura);
		relatorio.setSituacao(situacao);
		relatorio.setMotivoEncerramento(motivoEncerramento);
		
		ServicoAutorizacaoAgenda servicoAutorizacaoAgenda = repositorioServicoAutorizacao
				.consultarServicoAutorizacaoAgendaPorServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
		
		if(servicoAutorizacao.getServicoTipo().getDocumentoImpressaoLayout().getNomeArquivo()
							.equals(RELATORIO_SERVICO_AUTORIZACAO_ADEQUACAO_ALGAS)) {
			if(servicoAutorizacao.getDataExecucao() != null) {
				relatorio.setAgendamento(DataUtil.converterDataParaString(servicoAutorizacao.getDataExecucao(),false));
			}else {
				relatorio.setAgendamento("");
			}
			
		}else {
			if(servicoAutorizacaoAgenda != null) {
				relatorio.setAgendamento(DataUtil.converterDataParaString(servicoAutorizacaoAgenda.getDataAgenda(), false));
			} else {
				relatorio.setAgendamento("");
			}
		}
			
	}
	
	
	private ServicoAutorizacaoVORelatorio converterServicoAutorizacaoInicioFornecimentoParaVOAlgas(
			ServicoAutorizacao servicoAutorizacao, ServicoAutorizacaoTemp servicoAutorizacaoTemp)
			throws NegocioException {
		ServicoAutorizacaoVORelatorio relatorio = new ServicoAutorizacaoVORelatorio();

		montarInformacaoComumRelatorioAlgas(relatorio, servicoAutorizacao);
		montarInformacoesNovoMedidor(relatorio, servicoAutorizacao, servicoAutorizacaoTemp);

		StringBuilder pressaoContrato = new StringBuilder();
		StringBuilder vazaoContrato = new StringBuilder();

		ContratoPontoConsumo contratoPontoConsumo = null;

		if (servicoAutorizacao.getPontoConsumo() != null) {
			contratoPontoConsumo = controladorContrato.obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(
					servicoAutorizacao.getPontoConsumo().getChavePrimaria());
		}

		if (contratoPontoConsumo != null) {
			pressaoContrato.append(contratoPontoConsumo.getMedidaPressao());
			pressaoContrato.append(" ");
			pressaoContrato.append(contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());

			if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {
				vazaoContrato.append(contratoPontoConsumo.getMedidaVazaoMaximaInstantanea());
				vazaoContrato.append(" ");
				vazaoContrato.append(contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricaoAbreviada());
			}
		}

		relatorio.setVazaoContrato(vazaoContrato.toString());
		relatorio.setPressaoContrato(pressaoContrato.toString());

		return relatorio;
	}
	
	private ServicoAutorizacaoVORelatorio montarRelatorioTemp(ServicoAutorizacaoVORelatorio relatorio, ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws NegocioException{
		if(servicoAutorizacaoTemp != null) {
			
			Long tipoOperacao = servicoAutorizacaoTemp.getMedidorOperacao() != null ? servicoAutorizacaoTemp.getMedidorOperacao().getChavePrimaria() : 0l;
			if (servicoAutorizacaoTemp.getServicoAutorizacao().getServicoTipo().getDocumentoImpressaoLayout()
					.getNomeArquivo().equals(RELATORIO_SERVICO_AUTORIZACAO_INICIO_FORNECIMENTO_ALGAS)
					&& (OperacaoMedidor.CODIGO_INSTALACAO == tipoOperacao
							|| OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO == tipoOperacao)) {
				if (servicoAutorizacaoTemp.getNumeroLeituraNovo() != null) {
					relatorio.setNumeroLeituraOriginal(servicoAutorizacaoTemp.getNumeroLeituraNovo().toString());
				} else if (servicoAutorizacaoTemp.getNumeroLeituraOriginal() != null) {
					relatorio.setNumeroLeituraOriginal(servicoAutorizacaoTemp.getNumeroLeituraOriginal().toString());
				} else {
					relatorio.setNumeroLeituraOriginal("");
				}

				if (servicoAutorizacaoTemp.getMedidorNovo() != null) {
					if (servicoAutorizacaoTemp.getMedidorNumeroSerieNovo() != null) {
						relatorio.setMedidorSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieNovo());
					} else {
						relatorio.setMedidorSerie("");
					}

					if (servicoAutorizacaoTemp.getMedidorNovo().getModelo() != null) {
						relatorio.setMedidorModelo(
								servicoAutorizacaoTemp.getMedidorNovo().getModelo().getDescricaoAbreviada());
					} else {
						relatorio.setMedidorModelo("");
					}

					if (servicoAutorizacaoTemp.getMedidorNovo().getTipoMedidor() != null) {
						relatorio.setMedidorTipo(
								servicoAutorizacaoTemp.getMedidorNovo().getTipoMedidor().getDescricao());
					} else {
						relatorio.setMedidorTipo("");
					}

					if (servicoAutorizacaoTemp.getMedidorNovo().getMarcaMedidor() != null) {
						relatorio.setMedidorFabricante(
								servicoAutorizacaoTemp.getMedidorNovo().getMarcaMedidor().getDescricao());
					} else {
						relatorio.setMedidorFabricante("");
					}

					relatorio.setPlacaBP(servicoAutorizacaoTemp.getMedidorNovo().getTombamento() != null
							? servicoAutorizacaoTemp.getMedidorNovo().getTombamento()
							: "");
				} else if (servicoAutorizacaoTemp.getMedidorOriginal() != null) {
					if (servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal() != null) {
						relatorio.setMedidorSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal());
					} else {
						relatorio.setMedidorSerie("");
					}

					if (servicoAutorizacaoTemp.getMedidorOriginal().getModelo() != null) {
						relatorio.setMedidorModelo(
								servicoAutorizacaoTemp.getMedidorOriginal().getModelo().getDescricaoAbreviada());
					} else {
						relatorio.setMedidorModelo("");
					}

					if (servicoAutorizacaoTemp.getMedidorOriginal().getTipoMedidor() != null) {
						relatorio.setMedidorTipo(
								servicoAutorizacaoTemp.getMedidorOriginal().getTipoMedidor().getDescricao());
					} else {
						relatorio.setMedidorTipo("");
					}

					if (servicoAutorizacaoTemp.getMedidorOriginal().getMarcaMedidor() != null) {
						relatorio.setMedidorFabricante(
								servicoAutorizacaoTemp.getMedidorOriginal().getMarcaMedidor().getDescricao());
					} else {
						relatorio.setMedidorFabricante("");
					}

					relatorio.setPlacaBP(servicoAutorizacaoTemp.getMedidorOriginal().getTombamento() != null
							? servicoAutorizacaoTemp.getMedidorOriginal().getTombamento()
							: "");
				} else {
					relatorio.setMedidorSerie("");

					relatorio.setMedidorModelo("");

					relatorio.setMedidorTipo("");

					relatorio.setMedidorFabricante("");

					relatorio.setPlacaBP("");
				}

			} else {
				if (servicoAutorizacaoTemp.getNumeroLeituraOriginal() != null) {
					relatorio.setNumeroLeituraOriginal(servicoAutorizacaoTemp.getNumeroLeituraOriginal().toString());
				} else {
					relatorio.setNumeroLeituraOriginal("");
				}
				if (servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal() != null) {
					relatorio.setMedidorSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal());
				} else {
					relatorio.setMedidorSerie("");
				}

				if (servicoAutorizacaoTemp.getMedidorOriginal() != null) {

					if (servicoAutorizacaoTemp.getMedidorOriginal().getModelo() != null) {
						relatorio.setMedidorModelo(
								servicoAutorizacaoTemp.getMedidorOriginal().getModelo().getDescricaoAbreviada());
					} else {
						relatorio.setMedidorModelo("");
					}

					if (servicoAutorizacaoTemp.getMedidorOriginal().getTipoMedidor() != null) {
						relatorio.setMedidorTipo(
								servicoAutorizacaoTemp.getMedidorOriginal().getTipoMedidor().getDescricao());
					} else {
						relatorio.setMedidorTipo("");
					}

					if (servicoAutorizacaoTemp.getMedidorOriginal().getMarcaMedidor() != null) {
						relatorio.setMedidorFabricante(
								servicoAutorizacaoTemp.getMedidorOriginal().getMarcaMedidor().getDescricao());
					} else {
						relatorio.setMedidorFabricante("");
					}

					relatorio.setPlacaBP(servicoAutorizacaoTemp.getMedidorOriginal().getTombamento() != null
							? servicoAutorizacaoTemp.getMedidorOriginal().getTombamento()
							: "");

				}

			}
			
			if(servicoAutorizacaoTemp.getNumeroLacreMedidorNovo1() != null) {
				relatorio.setNumeroLacreMedidorNovo1(servicoAutorizacaoTemp.getNumeroLacreMedidorNovo1().toString());
			}else {
				relatorio.setNumeroLacreMedidorNovo1("");
			}
			if(servicoAutorizacaoTemp.getNumeroLacreMedidorNovo2() != null) {
				relatorio.setNumeroLacreMedidorNovo2(servicoAutorizacaoTemp.getNumeroLacreMedidorNovo2().toString());
			}else {
				relatorio.setNumeroLacreMedidorNovo2("");
			}
			if(servicoAutorizacaoTemp.getNumeroLacreMedidorNovo3() != null) {
				relatorio.setNumeroLacreMedidorNovo3(servicoAutorizacaoTemp.getNumeroLacreMedidorNovo3().toString());
			}else {
				relatorio.setNumeroLacreMedidorNovo3("");
			}
			if(servicoAutorizacaoTemp.getFuncionario() != null) {
				if(servicoAutorizacaoTemp.getFuncionario().getNome() != null) {
					relatorio.setNome(servicoAutorizacaoTemp.getFuncionario().getNome().toString());
				}else {
					relatorio.setNome("");
				}
				if(servicoAutorizacaoTemp.getFuncionario().getDescricaoCargo() != null) {
					relatorio.setCargo(servicoAutorizacaoTemp.getFuncionario().getDescricaoCargo());
				}else {
					relatorio.setCargo("");
				}
				
			}
			if(servicoAutorizacaoTemp.getLocalizacaoGeografica() != null) {
				relatorio.setLocalizacaoGeografica(servicoAutorizacaoTemp.getLocalizacaoGeografica().toString());
			}else {
				relatorio.setLocalizacaoGeografica("");
			}
			if(servicoAutorizacaoTemp.getNumeroLeituraNovo() != null) {
				relatorio.setNumeroLeituraNovo(servicoAutorizacaoTemp.getNumeroLeituraNovo().toString());
			}else {
				relatorio.setNumeroLeituraNovo("");
			}
			if(servicoAutorizacaoTemp.getMedidorNovo() != null) {
				relatorio.setMedidorNovo(String.valueOf(servicoAutorizacaoTemp.getMedidorNovo().getChavePrimaria()));
			}else {
				relatorio.setMedidorNovo("");
			}
			if(servicoAutorizacaoTemp.getFimExecucao() != null) {
				relatorio.setFimExecucao(DataUtil.converterDataParaString(servicoAutorizacaoTemp.getFimExecucao(),Boolean.TRUE));
			}else {
				relatorio.setFimExecucao("");
			}
			if(servicoAutorizacaoTemp.getObservacao() != null) {
				relatorio.setDescricao(servicoAutorizacaoTemp.getObservacao());
			}else {
				relatorio.setDescricao("");
			}
			if(servicoAutorizacaoTemp.getInicioExecucao() != null) {
				relatorio.setInicioExecucao(servicoAutorizacaoTemp.getDataExecucaoFormatada());
			}else {
				relatorio.setInicioExecucao("");
			}
			if(servicoAutorizacaoTemp.getOcorrencia() != null) {
				relatorio.setOcorrencia(String.valueOf(servicoAutorizacaoTemp.getOcorrencia().getDescricao()));
			}else {
				relatorio.setOcorrencia("");
			}
			if(servicoAutorizacaoTemp.getDescricaoGrauRisco() != null) {
				relatorio.setDescricaoGrauRisco(servicoAutorizacaoTemp.getDescricaoGrauRisco());
			}else {
				relatorio.setDescricaoGrauRisco("");
			}
			if(servicoAutorizacaoTemp.getCausaAtendimento() != null) {
				relatorio.setCausaAtendimento(String.valueOf(servicoAutorizacaoTemp.getCausaAtendimento().getDescricao()));
			}else {
				relatorio.setCausaAtendimento("");
			}
			if(servicoAutorizacaoTemp.getLocalRede() != null) {
				relatorio.setLocalRede(String.valueOf(servicoAutorizacaoTemp.getLocalRede().getDescricao()));
			}else {
				relatorio.setLocalRede("");
			}
			if(servicoAutorizacaoTemp.getSolucaoAtendimento() != null) {
				relatorio.setSolucaoAtendimento(String.valueOf(servicoAutorizacaoTemp.getSolucaoAtendimento().getDescricao()));
			}else {
				relatorio.setSolucaoAtendimento("");
			}
			if(servicoAutorizacaoTemp.getMedidorNumeroSerieNovo() != null) {
				relatorio.setNovoMedidorSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieNovo());
			}else {
				relatorio.setNovoMedidorSerie("");
			}
			
			if(servicoAutorizacaoTemp.getMedidorNovo() != null) {
				if(servicoAutorizacaoTemp.getMedidorNovo().getTipoMedidor() != null) {
					if(servicoAutorizacaoTemp.getMedidorNovo().getTipoMedidor().getDescricao() != null) {
						relatorio.setNovoMedidorFabricante(servicoAutorizacaoTemp.getMedidorNovo().getMarcaMedidor().getDescricao());
					}else {
						relatorio.setNovoMedidorFabricante("");
					}
				}else {
					relatorio.setNovoMedidorFabricante("");
				}
				if(servicoAutorizacaoTemp.getMedidorNovo().getModelo() != null) {
					relatorio.setNovoMedidorModelo(servicoAutorizacaoTemp.getMedidorNovo().getModelo().getDescricaoAbreviada());
				}else {
					relatorio.setNovoMedidorModelo("");
				}
				if(servicoAutorizacaoTemp.getMedidorNovo().getNumeroSerie() != null) {
					relatorio.setNovoMedidorSerie(servicoAutorizacaoTemp.getMedidorNovo().getNumeroSerie());
				}else {
					relatorio.setNovoMedidorSerie("");
				}
				if(servicoAutorizacaoTemp.getMedidorNovo().getTipoMedidor() != null) {
					relatorio.setNovoMedidorTipo(servicoAutorizacaoTemp.getMedidorNovo().getTipoMedidor().getDescricao());
				}else {
					relatorio.setNovoMedidorTipo("");
				}
				if(servicoAutorizacaoTemp.getMedidorNovo().getTombamento() != null) {
					relatorio.setNovoPlacaBP(servicoAutorizacaoTemp.getMedidorNovo().getTombamento());
				}else {
					relatorio.setNovoPlacaBP("");
				}
			}else {
				relatorio.setNovoMedidorFabricante("");
				relatorio.setNovoMedidorModelo("");
				relatorio.setNovoMedidorSerie("");
				relatorio.setNovoMedidorTipo("");
				relatorio.setNovoPlacaBP("");
			}
			if (servicoAutorizacaoTemp.getHoraAtendimento() != null) {
				relatorio.setHoraAtendimento(DataUtil.setarDataCompleta(
						DataUtil.converterDataParaString(servicoAutorizacaoTemp.getInicioExecucao(), true),
						servicoAutorizacaoTemp.getHoraAtendimento(),
						DataUtil.compararHoras(
								DataUtil.converterDataParaString(servicoAutorizacaoTemp.getInicioExecucao(), true),
								servicoAutorizacaoTemp.getHoraAtendimento())));

			} else {
				relatorio.setHoraAtendimento("");
			}

			if(servicoAutorizacaoTemp.getColetaDataHora() != null) {
				String dataFormatada = DataUtil.converterDataParaString(servicoAutorizacaoTemp.getColetaDataHora());
				relatorio.setColetaData(dataFormatada);
			}
			if(servicoAutorizacaoTemp.getColetaDataHora() != null) {
				relatorio.setColetaDataHora(DataUtil.converterDataParaString(servicoAutorizacaoTemp.getColetaDataHora(),Boolean.TRUE).split(" ")[1]);
			}
			if(servicoAutorizacaoTemp.getColetaPressao() != null) {
				relatorio.setColetaPressao(String.valueOf(servicoAutorizacaoTemp.getColetaPressao()));
			}
			if(servicoAutorizacaoTemp.getCilindro1() != null) {
				relatorio.setCilindro1(servicoAutorizacaoTemp.getCilindro1().getDescricao());
			}
			if(servicoAutorizacaoTemp.getCilindro2() != null) {
				relatorio.setCilindro2(servicoAutorizacaoTemp.getCilindro2().getDescricao());
			}
			if(servicoAutorizacaoTemp.getCilindro3() != null) {
				relatorio.setCilindro3(servicoAutorizacaoTemp.getCilindro3().getDescricao());
			}
			if(servicoAutorizacaoTemp.getCilindro4() != null) {
				relatorio.setCilindro4(servicoAutorizacaoTemp.getCilindro4().getDescricao());
			}
			if(servicoAutorizacaoTemp.getColetaLacre() != null) {
				relatorio.setColetaLacre(servicoAutorizacaoTemp.getColetaLacre());
			}
			if(servicoAutorizacaoTemp.getFuncionario() != null) {
				
			}
			
			if (servicoAutorizacaoTemp.getMedidorOperacao() != null && servicoAutorizacaoTemp.getMedidorOperacao()
					.getChavePrimaria() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
				relatorio.setIsOperacaoSubstituicao(Boolean.TRUE);
			} else {
				relatorio.setIsOperacaoSubstituicao(Boolean.FALSE);
			}
			
			
		}else {
			relatorio.setNumeroLeituraOriginal(StringUtils.isEmpty(relatorio.getNumeroLeituraOriginal()) ? "" : relatorio.getNumeroLeituraOriginal());
			relatorio.setNumeroLacreMedidorNovo1(StringUtils.isEmpty(relatorio.getNumeroLacreMedidorNovo1()) ? "" : relatorio.getNumeroLacreMedidorNovo1());
			relatorio.setNumeroLacreMedidorNovo2(StringUtils.isEmpty(relatorio.getNumeroLacreMedidorNovo2()) ? "" : relatorio.getNumeroLacreMedidorNovo2());
			relatorio.setNumeroLacreMedidorNovo3(StringUtils.isEmpty(relatorio.getNumeroLacreMedidorNovo3()) ? "" : relatorio.getNumeroLacreMedidorNovo3());
			relatorio.setNome(StringUtils.isEmpty(relatorio.getNome()) ? "" : relatorio.getNome());
			relatorio.setLocalizacaoGeografica(StringUtils.isEmpty(relatorio.getLocalizacaoGeografica()) ? "" : relatorio.getLocalizacaoGeografica());
			relatorio.setNumeroLeituraNovo(StringUtils.isEmpty(relatorio.getNumeroLeituraNovo()) ? "" : relatorio.getNumeroLeituraNovo());
			relatorio.setMedidorNovo(StringUtils.isEmpty(relatorio.getMedidorNovo()) ? "" : relatorio.getMedidorNovo());
			relatorio.setFimExecucao(StringUtils.isEmpty(relatorio.getFimExecucao()) ? "" : relatorio.getFimExecucao());
			relatorio.setInicioExecucao(StringUtils.isEmpty(relatorio.getInicioExecucao()) ? "" : relatorio.getInicioExecucao());
			relatorio.setOcorrencia(StringUtils.isEmpty(relatorio.getOcorrencia()) ? "" : relatorio.getOcorrencia());
			relatorio.setDescricaoGrauRisco(StringUtils.isEmpty(relatorio.getDescricaoGrauRisco()) ? "" : relatorio.getDescricaoGrauRisco());
			relatorio.setCausaAtendimento(StringUtils.isEmpty(relatorio.getCausaAtendimento()) ? "" : relatorio.getCausaAtendimento());
			relatorio.setLocalRede(StringUtils.isEmpty(relatorio.getLocalRede()) ? "" : relatorio.getLocalRede());
			relatorio.setSolucaoAtendimento(StringUtils.isEmpty(relatorio.getSolucaoAtendimento()) ? "" : relatorio.getSolucaoAtendimento());
			relatorio.setHoraAtendimento(StringUtils.isEmpty(relatorio.getHoraAtendimento()) ? "" : relatorio.getHoraAtendimento());
		}
		return relatorio;
	}
	
	public ServicoAutorizacaoVORelatorio montarRelatorioInicioFornecimento(ServicoAutorizacaoVORelatorio relatorio, ServicoAutorizacao servicoAutorizacao) {
		
		String medidorSerie = "";
		String numeroLeituraInicial = "";
		if(servicoAutorizacao.getPontoConsumo() != null) {
			if(servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor() != null) {
				if(servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor() != null) {
					medidorSerie = servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getMedidor().getNumeroSerie();
				}
				numeroLeituraInicial = servicoAutorizacao.getPontoConsumo().getInstalacaoMedidor().getLeituraAtivacao() + "";
			}
		}
		relatorio.setMedidorSerie(medidorSerie);
		relatorio.setNumeroLeituraOriginal(numeroLeituraInicial);
		return relatorio;
	}
	

	public Collection<ServicoAutorizacaoVO> gerarAutorizacaoServicoItemComunicacao(ItemLoteComunicacao item, Map<String, Object> dados, 
			Map<String, Collection<String>> logSucessoErro) throws NegocioException {
		
		Collection<ServicoAutorizacaoVO> listaServicoAutorizacaoVO = new ArrayList<ServicoAutorizacaoVO>();

		ServicoAutorizacao servicoAutorizacao = criarServicoAutorizacao(item);
		
		StringBuilder logProcessamento = new StringBuilder();
		logProcessamento.append(Constantes.PULA_LINHA);

		try {

			this.repositorioServicoAutorizacao.validarDadosEntidade(servicoAutorizacao);

			this.inserirServicoAutorizacao(servicoAutorizacao, dados, Boolean.TRUE);
			
			logProcessamento.append("Autorizacao de Servico gerada para o ponto de consumo: ");
			logProcessamento.append(item.getPontoConsumo().getDescricaoFormatada());
			
			Collection<String> logSucesso = logSucessoErro.get("SUCESSO");
			logSucesso.add(logProcessamento.toString());
			logSucessoErro.put("SUCESSO", logSucesso);
			
			ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
			servicoAutorizacaoVO.setDescricaoServicoTipo(servicoAutorizacao.getServicoTipo().getDescricao());
			servicoAutorizacaoVO.setDescricaoPontoConsumo(item.getPontoConsumo().getDescricao());
			servicoAutorizacaoVO.setChavePrimaria(servicoAutorizacao.getChavePrimaria());
			servicoAutorizacaoVO.setEnderecoImovel(servicoAutorizacao.getImovel().getEnderecoFormatado());
			
			item.setServicoAutorizacao(servicoAutorizacao);
			
			repositorioServicoAutorizacao.atualizar(item, item.getClass());
			
			this.agendarAutorizacaoServicoItemComunicacao(item);
			
			listaServicoAutorizacaoVO.add(servicoAutorizacaoVO);
			
			

		} catch (Exception e) {
			
			LOG.error(e.getMessage(), e);

			logProcessamento.append("Autorizaco de Servico não gerada para o ponto de consumo: ");
			logProcessamento.append(item.getPontoConsumo().getDescricaoFormatada());
			if (e instanceof GGASException) {
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append(" MOTIVO: ");
				String chave = null;
				Object[] erros = ((GGASException) e).getParametrosErro();
				if (e.getMessage() != null) {
					chave = e.getMessage();
				} else {
					chave = ((GGASException) e).getChaveErro();
				}
				
				if (chave != null && MensagemUtil.existeChave(chave)) {
					if (erros != null && erros.length > 0) {
						logProcessamento.append(MensagemUtil.obterMensagem(chave, erros));
					} else {
						logProcessamento.append(MensagemUtil.obterMensagem(chave));
					}
				}
			}
			
			Collection<String> logErro = logSucessoErro.get("ERRO");
			logErro.add(logProcessamento.toString());
			logSucessoErro.put("ERRO", logErro);

		}

		return listaServicoAutorizacaoVO;
	}
	
	@Override
	public void agendarAutorizacaoServicoItemComunicacao(ItemLoteComunicacao item)
			throws GGASException {

		ServicoAutorizacaoAgenda agenda = new ServicoAutorizacaoAgenda();

		agenda.setServicoAutorizacao(item.getServicoAutorizacao());
		agenda.setTurno(item.getServicoTipoAgendamentoTurno().getTurno());
		agenda.setDataAgenda(item.getDataHoraExecucao());
		agenda.setIndicadorConfirmado(Boolean.FALSE);
		agenda.setIndicadorConfirmado(Boolean.TRUE);
		
		this.repositorioServicoAutorizacao.inserir(agenda);
	}
	
	@Override
	public void gerarRelatorioListagemAS(Collection<ServicoAutorizacaoVO> listagemAS, Map<String, Object> dados)
			throws IOException, GGASException {

		if (dados.get("processo") != null) {

			Processo processo = (Processo) dados.get("processo");

			byte[] relatorioListagemAS = gerarRelatorioListagemAutorizacaoServico(listagemAS);
			
			List<byte[]> lista = new ArrayList<>();
			
			lista.add(relatorioListagemAS);
			
			for(ServicoAutorizacaoVO servicoAutorizacaoVO : listagemAS) {
				Collection<ServicoAutorizacaoHistorico> historico =
						this.listarServicoAutorizacaoHistorico(servicoAutorizacaoVO.getChavePrimaria());
				
				lista.add(this.gerarRelatorio(servicoAutorizacaoVO.getChavePrimaria(), Boolean.FALSE, historico));
			}

			if (!lista.isEmpty()) {
				ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
						.getControladorProcessoDocumento();

				ByteArrayInputStream byteArrayInputStream = null;
				File pdf = null;
				
				byte[] relatorioUnificado = RelatorioUtil.unificarRelatoriosPdf(lista);

				byteArrayInputStream = new ByteArrayInputStream(relatorioUnificado);

				ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
						.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

				String diretorioRelatorioGerados = (String) controladorParametrosSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);
				pdf = Util.getFile(diretorioRelatorioGerados);
				if (!pdf.exists()) {
					pdf.mkdirs();
				}

				final String nomeDocumento = "ListagemAS" + processo.getChavePrimaria()
						+ Calendar.getInstance().getTime().getTime();

				final String diretorioDocumento = diretorioRelatorioGerados + nomeDocumento
						+ Constantes.EXTENSAO_ARQUIVO_PDF;
				controladorProcesso.salvarRelatorio(processo, controladorProcessoDocumento, byteArrayInputStream,
						nomeDocumento, diretorioDocumento);
			}
		}
	}

	private byte[] gerarRelatorioListagemAutorizacaoServico(Collection<ServicoAutorizacaoVO> listagemAS) throws GGASException, NegocioException {
		byte[] relatorioListagemAS = null;
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		String arquivoRelatorio = "relatorioListagemAutorizacaoServico.jasper";
		if (arquivoRelatorio == null || arquivoRelatorio.isEmpty()) {

			throw new GGASException(Constantes.ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO);

		}

		parametros.put("listagemAS", listagemAS);
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));

		if (!listagemAS.isEmpty()) {
			Collection<ServicoAutorizacaoVO> listaAS = new ArrayList<ServicoAutorizacaoVO>();
			listaAS.add(listagemAS.iterator().next());

			relatorioListagemAS = RelatorioUtil.gerarRelatorioPDF(listaAS, parametros, arquivoRelatorio);
		}
		return relatorioListagemAS;
	}
	
	public Collection<Usuario> listarUsuariosSolcitantesAutorizacaoServico() throws NegocioException {
		return repositorioServicoAutorizacao.listarUsuariosSolcitantesAutorizacaoServico();
	}
	
	@Override
	public Collection<ServicoAutorizacao> listarServicoAutorizacaoPendente() throws NegocioException {
		return repositorioServicoAutorizacao.listarServicoAutorizacaoPendente();
	}

	@Override
	public Collection<ServicoTipoAgendamentoTurno> listaTurnoPorEquipeServicoTipo(Long chaveEquipe) {
		return this.repositorioServicoAutorizacao.listaTurnoPorEquipeServicoTipo(chaveEquipe);
	}
	
	@Override
	public Collection<EquipeTurnoItem> consultarEscalaDisponivel(Equipe equipe, EntidadeConteudo turno, String dataSelecionada) throws GGASException{
		return this.repositorioServicoAutorizacao.consultarEscalaDisponivel(equipe, turno, dataSelecionada);
	}
	
	@Override
	public Collection<ServicoAutorizacaoAgenda> obterListaAgendamentosPorDiaturnoServicoTipoEquipe(Date dataAgendamento,
			EntidadeConteudo turno, ServicoTipo servicoTipo, Equipe equipe) {
		return this.repositorioServicoAutorizacao.obterListaAgendamentosPorDiaTurnoServicoTipoEquipe(dataAgendamento,
				turno.getChavePrimaria(), servicoTipo, equipe, null);
	}
	
	@Override
	public Collection<ServicoAutorizacaoTemp> listarAutorizacoesServicoEncerramentoAPP() {
		return this.repositorioServicoAutorizacao.listarAutorizacoesServicoEncerramentoAPP();
	}
	
	@Override
	public ServicoAutorizacaoTemp consultarAutorizacaoServicoTemp(ServicoAutorizacao autorizacaoServico) {
		return this.repositorioServicoAutorizacao.consultarAutorizacaoServicoTemp(autorizacaoServico);
	}
	

	@Override
	public Collection<ServicoAutorizacaoTempAnexo> listarAnexosAutorizacaoServicoEncerramentoAPP(ServicoAutorizacao autorizacaoServico) {
		return this.repositorioServicoAutorizacao.listarAnexosAutorizacaoServicoEncerramentoAPP(autorizacaoServico);
	}
	
	@Override
	public void atualizarServicoAutorizacaoTempSemValidar(ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws ConcorrenciaException, NegocioException {
		repositorioServicoAutorizacao.atualizarSemValidar(servicoAutorizacaoTemp, getClasseEntidadeServicoAutorizacaoTemp());
	}
	

	@Override
	public Boolean roteirizarServicoAutorizacaoAgendamento(Long[] chaves,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, Boolean isSimulacao,  Collection<ServicoAutorizacaoAgenda> servicoAgendaSimulado) throws IOException, GGASException {
		Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda = new ArrayList<ServicoAutorizacaoAgenda>();

		
		if (chaves.length > 0) {
			listaServicoAutorizacaoAgenda.addAll(repositorioServicoAutorizacao.listaServicoAutorizacaoAgenda(chaves));
		}

		if (servicoAgendaSimulado != null) {
			listaServicoAutorizacaoAgenda.addAll(servicoAgendaSimulado);
			servicoAutorizacaoAgendaPesquisaVO.setTurno(null);
		} else {
			this.limparRoteirizacao(listaServicoAutorizacaoAgenda);
		}
		
		this.verificarCoordenadasPontoConsumo(listaServicoAutorizacaoAgenda);

		Integer contadorTotalAgendamentos = listaServicoAutorizacaoAgenda.size();

		Collection<EntidadeConteudo> listaTurno = this
				.consultaTurnosEquipeEscala(servicoAutorizacaoAgendaPesquisaVO.getEquipe(),
						servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada())
				.stream().distinct().collect(Collectors.toCollection(ArrayList::new));
		Collection<ServicoAutorizacaoAgenda> listaAgendaProximoTurno = new ArrayList<ServicoAutorizacaoAgenda>();

		Integer contadorAgendamentos = 0;

		Funcionario funcionario = null;
		Collection<ServicoAutorizacaoAgenda> listaUltimosAgendamento = repositorioServicoAutorizacao
				.consultarUltimosAgendamentosPorEquipe(servicoAutorizacaoAgendaPesquisaVO.getEquipe(),
						servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(), null);

		if (!listaUltimosAgendamento.isEmpty()) {
			funcionario = listaUltimosAgendamento.iterator().next().getFuncionario();
		}

		Map<String, EquipeAlgasVO> mapaEquipeALGAS = montarMapaEquipeAlgas(servicoAutorizacaoAgendaPesquisaVO,
				funcionario);

		for (Entry<String, EquipeAlgasVO> entry : mapaEquipeALGAS.entrySet()) {
			Collection<ServicoAutorizacaoAgenda> listaAgendamentoRemover = new ArrayList<ServicoAutorizacaoAgenda>();

			EquipeAlgasVO equipeAlgasVO = entry.getValue();

			for (EntidadeConteudo turno : listaTurno) {
				servicoAutorizacaoAgendaPesquisaVO.setTurno(turno);

				Collection<ServicoAutorizacaoAgenda> listaAgendamentoTurno = montarListaAgendamentoTurno(
						listaServicoAutorizacaoAgenda, listaAgendaProximoTurno, turno);

				Collection<ServicoAutorizacao> listaRoteirizada = listaAgendamentoTurno.stream()
						.map(p -> p.getServicoAutorizacao()).collect(Collectors.toCollection(ArrayList::new));

				Calendar dataAgendamento = Calendar.getInstance();
				dataAgendamento
						.setTime(DataUtil.converterParaData(servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada()));

				Map<ServicoTipoAgendamentoTurno, Long> mapaServicoAgendamentoTurno = montarMapaServicoTipoAgendamentoTurno(
						servicoAutorizacaoAgendaPesquisaVO, turno, listaRoteirizada);

				PontoConsumo pontoConsumoRoteirizar = null;


				Date dataMinima = null;
				Date dataMaxima = null;
				Long diferencaMinutos = 0l;
				Boolean isPrimeiroPonto = Boolean.TRUE;
				Date dataFinalEquipe = equipeAlgasVO.getDataFinal();
				Long tempoExecucao = 0l;

				if (equipeAlgasVO.getTurno().getChavePrimaria() != turno.getChavePrimaria()) {
					continue;
				}

				Collection<ServicoAutorizacaoAgenda> lista = repositorioServicoAutorizacao
						.obterListaAgendamentosPorDiaTurnoServicoTipoEquipe(dataAgendamento.getTime(),
								equipeAlgasVO.getTurno().getChavePrimaria(), null, equipeAlgasVO.getEquipe(),
								equipeAlgasVO.getFuncionario());

				listaRoteirizada = roteirizarServicoAutorizacao(listaRoteirizada, pontoConsumoRoteirizar);
				
				if (!lista.isEmpty()) {
					dataMinima = lista.stream().map(ServicoAutorizacaoAgenda::getDataAgenda).min(Date::compareTo).get();
					dataMaxima = lista.stream().map(ServicoAutorizacaoAgenda::getDataAgenda).max(Date::compareTo).get();

					if (DataUtil.menorOuIgualQue(dataMaxima, equipeAlgasVO.getDataInicial(), false)) {
						dataMaxima = null;
					} else {
						ServicoAutorizacao ultimoServicoAutorizacao = lista.stream()
								.max(Comparator.comparing(ServicoAutorizacaoAgenda::getDataAgenda)).get()
								.getServicoAutorizacao();
						tempoExecucao = ultimoServicoAutorizacao.getServicoTipo().getQuantidadeTempoMedio();
						pontoConsumoRoteirizar = ultimoServicoAutorizacao.getPontoConsumoRoteirizacao();

						diferencaMinutos = DataUtil.obterDiferencaMinutosEntreDatas(dataMinima, dataMaxima);
					}

				}

				Long duracao = equipeAlgasVO.getDuracao();

				Long duracaoRestante = duracao - diferencaMinutos;

				Calendar dataAgendada = Calendar.getInstance();

				if (dataMaxima == null) {
					dataAgendada.setTime(equipeAlgasVO.getDataInicial());
				} else {
					dataAgendada.setTime(dataMaxima);
					isPrimeiroPonto = Boolean.FALSE;
				}

				if (DataUtil.menorOuIgualQue(dataAgendada.getTime(), Calendar.getInstance().getTime(), false)) {
					dataAgendada.setTime(Calendar.getInstance().getTime());
				}

				if (DataUtil.menorOuIgualQue(dataFinalEquipe, dataAgendada.getTime(), false)) {
					continue;
				}

				Collection<ServicoAutorizacao> listaRemover = new ArrayList<ServicoAutorizacao>();
				Collection<ServicoAutorizacao> listaAutorizacaoServicoMesmaLoc = new ArrayList<ServicoAutorizacao>();

				for (ServicoAutorizacao servicoAutorizacao : listaRoteirizada) {

					Boolean isCalculadoTempoExecuado = Boolean.FALSE;

					ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno = servicoAutorizacao.getServicoTipo()
							.getListaAgendamentosTurno().stream()
							.filter(p -> p.getEquipe().getChavePrimaria() == servicoAutorizacaoAgendaPesquisaVO
									.getEquipe().getChavePrimaria()
									&& p.getTurno().getChavePrimaria() == turno.getChavePrimaria())
							.findFirst().orElse(null);

					Long quantidadeTurno = mapaServicoAgendamentoTurno.get(servicoTipoAgendamentoTurno);

					if (listaAutorizacaoServicoMesmaLoc.contains(servicoAutorizacao)) {
						continue;
					}

					if (quantidadeTurno == null || quantidadeTurno <= 0l) {
						continue;
					}

					if (duracaoRestante < tempoExecucao) {
						break;
					}

					calcularDataParaPrimeiroPontoDaRoteirizacao(dataAgendada, servicoAutorizacao,
							pontoConsumoRoteirizar);
					if (isPrimeiroPonto) {
						isPrimeiroPonto = Boolean.FALSE;
					} else {
						isCalculadoTempoExecuado = Boolean.TRUE;
					}
					dataAgendada.add(Calendar.MINUTE, tempoExecucao.intValue());
					tempoExecucao = calcularTempoExecucao(servicoAutorizacao);

					if (DataUtil.menorOuIgualQue(dataFinalEquipe, dataAgendada.getTime(), false)) {
						continue;
					}

					Collection<ServicoAutorizacao> listaServicoAutorizacaoMesmaLoc = listaRoteirizada.stream()
							.filter(p -> p.getPontoConsumoRoteirizacao().getLatitudeGrau()
									.compareTo(servicoAutorizacao.getPontoConsumoRoteirizacao().getLatitudeGrau()) == 0
									&& p.getPontoConsumoRoteirizacao().getLongitudeGrau()
											.compareTo(servicoAutorizacao.getPontoConsumoRoteirizacao().getLongitudeGrau()) == 0)
							.collect(Collectors.toList());

					if (listaServicoAutorizacaoMesmaLoc.size() > 1 && verificarPontosConsumoMesmaLocalizacao(
							listaServicoAutorizacaoMesmaLoc, servicoAutorizacao, listaAgendamentoTurno, dataAgendada,
							turno, isCalculadoTempoExecuado, duracaoRestante, mapaServicoAgendamentoTurno, dataFinalEquipe)) {

						listaAutorizacaoServicoMesmaLoc.addAll(listaServicoAutorizacaoMesmaLoc);
						continue;
					} else if (verificarTempoRetornoOrigem(dataAgendada, servicoAutorizacao, dataFinalEquipe,
							Boolean.TRUE)) {
						break;
					}

					duracaoRestante -= servicoAutorizacao.getServicoTipo().getQuantidadeTempoMedio();
					quantidadeTurno--;

					mapaServicoAgendamentoTurno.put(servicoTipoAgendamentoTurno, quantidadeTurno);
					
					ServicoAutorizacaoAgenda servicoAutorizacaoAgenda = atualizarServicoAutorizacaoAgenda(
							listaServicoAutorizacaoAgenda, equipeAlgasVO, dataAgendada, servicoAutorizacao,
							isSimulacao);

					contadorAgendamentos++;

					pontoConsumoRoteirizar = servicoAutorizacao.getPontoConsumoRoteirizacao();

					listaAgendamentoRemover.add(servicoAutorizacaoAgenda);
					listaRemover.add(servicoAutorizacao);

					if (duracaoRestante <= 0) {
						break;
					}

				}

				listaRoteirizada.removeAll(listaRemover);

				listaAgendamentoTurno.removeAll(listaAgendamentoRemover);
				listaAgendamentoTurno.removeIf(p -> p.getServicoAutorizacao().getTurnoPreferencia() != null);

				listaAgendaProximoTurno = new ArrayList<ServicoAutorizacaoAgenda>();
				listaAgendaProximoTurno.addAll(listaAgendamentoTurno);
				listaServicoAutorizacaoAgenda.removeAll(listaAgendamentoRemover);

				if (listaRoteirizada.isEmpty()) {
					break;
				}
			}
		}

		if (isSimulacao) {
			return contadorAgendamentos == contadorTotalAgendamentos;
		}

		return Boolean.TRUE;
	}

	
	

	private void verificarCoordenadasPontoConsumo(
			Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda) throws IOException, NegocioException {
		
		Collection<ServicoAutorizacaoAgenda> listaRemover = new ArrayList<ServicoAutorizacaoAgenda>();

		for(ServicoAutorizacaoAgenda servicoAutorizacaoAgenda : listaServicoAutorizacaoAgenda) {
			ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoAgenda.getServicoAutorizacao();
			PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumoRoteirizacao();
			Chamado chamado = servicoAutorizacao.getChamado();
			
			if (pontoConsumo != null && pontoConsumo.getLatitudeGrau() != null
					&& pontoConsumo.getLongitudeGrau() != null) {
				continue;
			} else if ((pontoConsumo == null
					|| (pontoConsumo.getLatitudeGrau() == null || pontoConsumo.getLongitudeGrau() == null))
					&& (chamado == null || chamado.getEnderecoChamado() == null || StringUtils.isEmpty(chamado.getNumeroEndereco()))) {
				listaRemover.add(servicoAutorizacaoAgenda);
			}  else if (chamado.getEnderecoChamado() != null && !StringUtils.isEmpty(chamado.getNumeroEndereco())) {
				Cep cepChamadoEndereco = chamado.getEnderecoChamado();
				String numeroEndereco = chamado.getNumeroEndereco();
				
				Map<String, BigDecimal> coordenadas = Util.obterCoordenadasEndereco(cepChamadoEndereco, numeroEndereco);
				
				if(coordenadas.isEmpty()) {
					listaRemover.add(servicoAutorizacaoAgenda);
					continue;
				}
				
				BigDecimal latitude = coordenadas.get("latitude");
				BigDecimal longitude = coordenadas.get("longitude");
				
				if(pontoConsumo == null) {
					pontoConsumo = Fachada.getInstancia().criarPontoConsumo();
					servicoAutorizacao.setPontoConsumoRoteirizacao(pontoConsumo);
				} 
				
				pontoConsumo.setLatitudeGrau(latitude);
				pontoConsumo.setLongitudeGrau(longitude);
			}
		}
		listaServicoAutorizacaoAgenda.removeAll(listaRemover);
		
	}

	private void limparRoteirizacao(Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda) throws ConcorrenciaException, NegocioException {
		for(ServicoAutorizacaoAgenda servicoAutorizacaoAgenda : listaServicoAutorizacaoAgenda) {
			servicoAutorizacaoAgenda.setFuncionario(null);
			servicoAutorizacaoAgenda.setDataAgenda(DataUtil.zerarHoraParaData(servicoAutorizacaoAgenda.getDataAgenda()));
			
			this.atualizarServicoAutorizacaoAgenda(servicoAutorizacaoAgenda);
		}
		
	}

	@SuppressWarnings("unchecked")
	private Boolean verificarPontosConsumoMesmaLocalizacao(Collection<ServicoAutorizacao> listaServicoAutorizacaoMesmaLoc,
			ServicoAutorizacao servicoAutorizacao, Collection<ServicoAutorizacaoAgenda> listaAgendamentoTurno, Calendar dataAgendada, EntidadeConteudo turno, Boolean isCalculadoTempoExecuado, Long duracaoRestante, Map<ServicoTipoAgendamentoTurno, Long> mapaServicoAgendamentoTurno, Date dataFinalEquipe) throws NegocioException, IOException {
		
		Calendar agendaRetorno = (Calendar) SerializationUtils.clone((Serializable) dataAgendada);
		Map<ServicoTipoAgendamentoTurno, Long> mapaServicoAgendamentoTurnoCopia = (Map<ServicoTipoAgendamentoTurno, Long>) SerializationUtils.clone((Serializable)mapaServicoAgendamentoTurno);

		Long duracaoTotalServico = 0l;
		Boolean quantidadeTurnoEsgotado = Boolean.FALSE;
		
		for (ServicoAutorizacao servicoAutorizacaoAux : listaServicoAutorizacaoMesmaLoc) {

			ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno = servicoAutorizacaoAux.getServicoTipo()
					.getListaAgendamentosTurno().stream()
					.filter(p -> p.getEquipe().getChavePrimaria() == servicoAutorizacaoAux.getEquipe().getChavePrimaria()
							&& p.getTurno().getChavePrimaria() == turno.getChavePrimaria())
					.findFirst().orElse(null);
			
			Long quantidadeTurno = mapaServicoAgendamentoTurnoCopia.get(servicoTipoAgendamentoTurno);

			Long duracaoServico = calcularTempoExecucao(servicoAutorizacaoAux);
			if (isCalculadoTempoExecuado
					&& servicoAutorizacao.getChavePrimaria() != servicoAutorizacaoAux.getChavePrimaria()) {
				agendaRetorno.add(Calendar.MINUTE, duracaoServico.intValue());
			}
			duracaoTotalServico += duracaoServico;
			
			quantidadeTurno--;
			
			if(quantidadeTurno < 0) {
				quantidadeTurnoEsgotado = Boolean.TRUE;
				break;
			}
			
			mapaServicoAgendamentoTurnoCopia.put(servicoTipoAgendamentoTurno, quantidadeTurno);
		}
		
		if(DataUtil.menorOuIgualQue(dataFinalEquipe, agendaRetorno.getTime(), false) || quantidadeTurnoEsgotado || duracaoRestante < duracaoTotalServico || verificarTempoRetornoOrigem(agendaRetorno, servicoAutorizacao, dataFinalEquipe, Boolean.FALSE)) {
			
			if(listaAgendamentoTurno.stream().anyMatch(p -> p.getServicoAutorizacao().getTurnoPreferencia() != null)) {
				listaAgendamentoTurno.removeIf(p -> listaServicoAutorizacaoMesmaLoc.contains(p.getServicoAutorizacao()));
			}
			
			return Boolean.TRUE;
		}
		
		
		return Boolean.FALSE;
	}

	private boolean verificarTempoRetornoOrigem(Calendar dataAgendada, ServicoAutorizacao servicoAutorizacao,
			Date dataFinalEquipe, Boolean isConsiderarTempoExecucao) throws NegocioException, IOException {

		Calendar agendaRetorno = (Calendar) SerializationUtils.clone((Serializable) dataAgendada);
		LocalTime fimDataEquipe = DataUtil.obterHoraMinutoData(dataFinalEquipe);

		Collection<ServicoAutorizacao> listaServicoAutorizacaoAux = new ArrayList<>();
		listaServicoAutorizacaoAux.add(servicoAutorizacao);

		Collection<ServicoAutorizacao> colecaoAux = this.roteirizarServicoAutorizacao(listaServicoAutorizacaoAux, null);

		ServicoAutorizacao servicoAux = colecaoAux.iterator().next();

		agendaRetorno.add(Calendar.MINUTE, servicoAux.getTempoDistancia().intValue() / 60);
		
		if(isConsiderarTempoExecucao) {
			agendaRetorno.add(Calendar.MINUTE, servicoAux.getServicoTipo().getQuantidadeTempoMedio().intValue());
		}

		LocalTime horarioFinal = LocalTime.of(agendaRetorno.get(Calendar.HOUR_OF_DAY),
				agendaRetorno.get(Calendar.MINUTE));

		if (horarioFinal.isAfter(fimDataEquipe)) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	@Override
	@Transactional
	public ServicoAutorizacaoTempAnexo obterServicoAutorizacaoTempAnexo(Long chavePrimaria) throws NegocioException {

		return (ServicoAutorizacaoTempAnexo) repositorioServicoAutorizacao
				.obter(chavePrimaria, getClasseEntidadeServicoAutorizacaoTempAnexo());
	}
	
	@Override
	public ServicoAutorizacaoTempAnexo consultarAssinaturaAnexoAutorizacaoServicoTemp(ServicoAutorizacao servicoAutorizacao) throws NegocioException {
		return this.repositorioServicoAutorizacao.consultarAssinaturaAnexoAutorizacaoServicoTemp(servicoAutorizacao);
	}
	
	@Override
	public ServicoAutorizacaoTempAnexo consultarFotoCRM(ServicoAutorizacao servicoAutorizacao) throws NegocioException{
		return this.repositorioServicoAutorizacao.consultarFotoCRM(servicoAutorizacao);
	}
	
	private Collection<ServicoAutorizacaoAgenda> montarListaAgendamentoTurno(
			Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda,
			Collection<ServicoAutorizacaoAgenda> listaAgendaProximoTurno, EntidadeConteudo turno) {
		Collection<ServicoAutorizacaoAgenda> listaAgendamentoTurno = new ArrayList<ServicoAutorizacaoAgenda>();
		listaAgendamentoTurno.addAll(listaAgendaProximoTurno);
		listaServicoAutorizacaoAgenda.stream()
	    .filter(p -> p.getTurno().getChavePrimaria() == turno.getChavePrimaria())
	    .forEach(item -> {
	        boolean alreadyExists = listaAgendamentoTurno.stream()
	            .anyMatch(existingItem -> existingItem.getChavePrimaria() == item.getChavePrimaria());
	        if (!alreadyExists) {
	            listaAgendamentoTurno.add(item);
	        }
	    });
		
		return listaAgendamentoTurno;
	}

	private ServicoAutorizacaoAgenda atualizarServicoAutorizacaoAgenda(
			Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda, EquipeAlgasVO equipeAlgasVO,
			Calendar dataAgendada, ServicoAutorizacao servicoAutorizacao, Boolean isSimulacao)
			throws ConcorrenciaException, NegocioException {
		ServicoAutorizacaoAgenda servicoAutorizacaoAgenda = listaServicoAutorizacaoAgenda.stream().filter(
				p -> p.getServicoAutorizacao().getChavePrimaria() == servicoAutorizacao.getChavePrimaria())
				.findFirst().orElse(null);
		
		if(servicoAutorizacaoAgenda != null && !isSimulacao) {
			servicoAutorizacaoAgenda.setFuncionario(equipeAlgasVO.getFuncionario());
			servicoAutorizacaoAgenda.setTurno(equipeAlgasVO.getTurno());
			servicoAutorizacaoAgenda.setDataAgenda(dataAgendada.getTime());
			
			this.atualizarServicoAutorizacaoAgenda(servicoAutorizacaoAgenda);
		}
		return servicoAutorizacaoAgenda;
	}

	private Map<ServicoTipoAgendamentoTurno, Long> montarMapaServicoTipoAgendamentoTurno(
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, EntidadeConteudo turno,
			Collection<ServicoAutorizacao> listaRoteirizada) {
		Collection<ServicoTipoAgendamentoTurno> listaServicoAgendamentoTurno = listaRoteirizada.stream().flatMap(p -> p.getServicoTipo().getListaAgendamentosTurno().stream()).distinct().collect(Collectors.toList());
		
		
		Map<ServicoTipoAgendamentoTurno, Long> mapaServicoAgendamentoTurno = listaServicoAgendamentoTurno.stream()
				.filter(p -> p.getEquipe().getChavePrimaria() == servicoAutorizacaoAgendaPesquisaVO.getEquipe()
						.getChavePrimaria() && p.getTurno().getChavePrimaria() == turno.getChavePrimaria())
				.collect(Collectors.toMap(Function.identity(),
						ServicoTipoAgendamentoTurno::getQuantidadeAgendamentosTurno));
		
		return mapaServicoAgendamentoTurno;
	}
	
	
	
	
	@Override
	public Collection<EntidadeConteudo> consultaTurnosEquipeEscala(Equipe equipe, String dataAgenda) throws GGASException {
		return repositorioServicoAutorizacao.consultaTurnosEquipeEscala(equipe, dataAgenda);
	}

	@Override
	public void roteirizarServicoAutorizacaoAgendamentoCorte(Long[] chaves,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws IOException, GGASException {
		Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda = repositorioServicoAutorizacao
				.listaServicoAutorizacaoAgenda(chaves);
		
		Collection<ServicoAutorizacao> listaRoteirizada = listaServicoAutorizacaoAgenda.stream()
				.map(p -> p.getServicoAutorizacao()).collect(Collectors.toCollection(ArrayList::new));
		
		
		listaRoteirizada = this.roteirizarServicoAutorizacao(listaRoteirizada, null);
		
		
		Funcionario funcionario = null;
		Collection<ServicoAutorizacaoAgenda> listaUltimosAgendamento = repositorioServicoAutorizacao
				.consultarUltimosAgendamentosPorEquipe(servicoAutorizacaoAgendaPesquisaVO.getEquipe(),
						servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(), null);	
		
		if(!listaUltimosAgendamento.isEmpty()) {
			funcionario = listaUltimosAgendamento.iterator().next().getFuncionario();
		}

		Map<String, EquipeAlgasVO> mapaEquipeALGAS = montarMapaEquipeAlgas(servicoAutorizacaoAgendaPesquisaVO, funcionario);
		
		EquipeAlgasVO equipeAlgasVO = mapaEquipeALGAS.entrySet().iterator().next().getValue(); 
		
		Calendar dataAgendada = Calendar.getInstance();
		dataAgendada.setTime(DataUtil.zerarHoraParaData(new Date()));
		
		for(ServicoAutorizacao servicoAutorizacao : listaRoteirizada) {
			
			atualizarServicoAutorizacaoAgenda(
					listaServicoAutorizacaoAgenda, equipeAlgasVO, dataAgendada, servicoAutorizacao, Boolean.FALSE);
			
			dataAgendada.add(Calendar.MINUTE, 30);
		}
		
	}
	
	@Override
	public Collection<PontoConsumo> consultarPontosLoteAutorizacaoServico(
			FiltroServicoAutorizacaoLoteDTO filtroServicoAutorizacaoLoteDTO) throws HibernateException, GGASException {
		return this.repositorioServicoAutorizacao.consultarPontosLoteAutorizacaoServico(filtroServicoAutorizacaoLoteDTO);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AutorizacaoServicoLoteVO> roterizacaoAutorizacaoServico(Collection<PontoConsumo> listaPontoConsumo) throws NegocioException, IOException{
		List<PontoConsumo> listaPontoConsumoAux = (List<PontoConsumo>) SerializationUtils.clone((Serializable) listaPontoConsumo.stream().collect(Collectors.toList()));
		Double latitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
		Double longitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());		
		Collection<AutorizacaoServicoLoteVO> listaRoteirizada = new ArrayList<>();
		
		while (!listaPontoConsumoAux.isEmpty()) {
			AutorizacaoServicoLoteVO autorizacaoServicoLoteVO = calculaPontoConsumoMenorDistancia(listaPontoConsumoAux, latitudeComparar, longitudeComparar);
			
			latitudeComparar = autorizacaoServicoLoteVO.getPontoConsumo().getLatitudeGrau().doubleValue();
			longitudeComparar = autorizacaoServicoLoteVO.getPontoConsumo().getLongitudeGrau().doubleValue();
			
			listaRoteirizada.add(autorizacaoServicoLoteVO);
			listaPontoConsumoAux.remove(autorizacaoServicoLoteVO.getPontoConsumo());
			
		}
		
		return listaRoteirizada;
	}
	
	private AutorizacaoServicoLoteVO calculaPontoConsumoMenorDistancia(List<PontoConsumo> listaPontoConsumo,
			Double latitudeComparar, Double longitudeComparar) throws IOException {
		AutorizacaoServicoLoteVO autorizacaoServicoLoteVO = new AutorizacaoServicoLoteVO();
		Double menorDistancia = -1.0;
		PontoConsumo pontoConsumo = listaPontoConsumo.iterator().next();
		Long tempoDistancia = 0l;
		
		LOG.info("Iniciando chamada da API do Google (AS em LOTE) ");
		Map<String, Object> mapaRetornado = Util.obterMenorDistanciaTempoEntrePontosConsumo(listaPontoConsumo, latitudeComparar, longitudeComparar, menorDistancia, pontoConsumo, tempoDistancia);
		LOG.info("Finalizando chamada da API do Google (AS em LOTE) ");
		pontoConsumo = (PontoConsumo) mapaRetornado.get("pontoConsumo");
		tempoDistancia = (Long) mapaRetornado.get("tempoDistancia");
		menorDistancia = (Double) mapaRetornado.get("menorDistancia");
		
		BigDecimal distancia = new BigDecimal(menorDistancia).divide(new BigDecimal(1000)).setScale(2, RoundingMode.HALF_UP);
		
		autorizacaoServicoLoteVO.setPontoConsumo(pontoConsumo);
		autorizacaoServicoLoteVO.setDistanciaEntrePontos(distancia.doubleValue());
		
		
		tempoDistancia = tempoDistancia / 60;
		
		if(tempoDistancia.compareTo(1l) < 0) {
			tempoDistancia = 1l;
		}

		autorizacaoServicoLoteVO.setTempoDistancia(String.valueOf(tempoDistancia));
		
		autorizacaoServicoLoteVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
		autorizacaoServicoLoteVO.setEnderecoFormatado(pontoConsumo.getEnderecoFormatado());
		
		return autorizacaoServicoLoteVO;

	}
	
	
	@Override
	public void calcularDistanciaPontoConsumo(AutorizacaoServicoLoteVO autorizacaoServicoLote) throws NegocioException, IOException {
		Double latitudeComparar = Double
				.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
		Double longitudeComparar = Double
				.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());
		List<PontoConsumo> listaPontoConsumo = new ArrayList<>();
		listaPontoConsumo.add(autorizacaoServicoLote.getPontoConsumo());
		
		AutorizacaoServicoLoteVO retorno = this.calculaPontoConsumoMenorDistancia(listaPontoConsumo, latitudeComparar, longitudeComparar);
		autorizacaoServicoLote.setDistanciaEntrePontos(retorno.getDistanciaEntrePontos());
		autorizacaoServicoLote.setTempoDistancia(retorno.getTempoDistancia());

	}

	@Override
	public List<byte[]> gerarLote(Collection<AutorizacaoServicoLoteVO> listaAutorizacaoServicoLote, ServicoTipo servicoTipo, Usuario usuario, LoteComunicacao lote) throws GGASException {

		List<byte[]> relatorio = new ArrayList<byte[]>();
		Collection<ServicoAutorizacaoVO> listaServicoAutorizacaoVO = new ArrayList<ServicoAutorizacaoVO>();
		
		Map<String, Object> dados = new HashMap<>();
		dados.put("usuario", usuario);
		
		Collection<ItemLoteComunicacao> colecaoItemLote = new ArrayList<ItemLoteComunicacao>();
		if(lote != null) {
			colecaoItemLote.addAll(lote.getItens());
		}
		
		
		for(AutorizacaoServicoLoteVO autorizacaoServicoLoteVO : listaAutorizacaoServicoLote) {
			Date dataExec = Util.converterCampoStringParaData("Data Execução", autorizacaoServicoLoteVO.getDataExecucao(), Constantes.FORMATO_DATA_BR);
			String[] horaExecucao = autorizacaoServicoLoteVO.getHoraInicialExecucao().split(":");
			Calendar c = Calendar.getInstance();
			c.setTime(dataExec);
			c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaExecucao[0]));
			c.set(Calendar.MINUTE, Integer.valueOf(horaExecucao[1]));
			
			ServicoAutorizacao servicoAutorizacao = this.criarServicoAutorizacaoLote(servicoTipo, autorizacaoServicoLoteVO, c.getTime());
			
			servicoAutorizacao = this.inserirServicoAutorizacao(servicoAutorizacao, dados, Boolean.TRUE);
			
			
			this.inserirServicoAutorizacaoAgenda(autorizacaoServicoLoteVO.getServicoTipoAgendamentoTurno().getTurno(), c, servicoAutorizacao, Boolean.TRUE);
			
			relatorio.add(this.gerarRelatorio(servicoAutorizacao.getChavePrimaria(), Boolean.FALSE, null));
			
			
			if(!colecaoItemLote.isEmpty()) {
				PontoConsumo pontoConsumoAS = servicoAutorizacao.getPontoConsumo();
				ItemLoteComunicacao item = colecaoItemLote.stream().filter(p -> p.getPontoConsumo().getChavePrimaria() == pontoConsumoAS.getChavePrimaria()).findFirst().orElse(null);
				
				if(item != null) {
					item.setServicoAutorizacaoProtocolo(servicoAutorizacao);
				}
			}
			
			ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
			servicoAutorizacaoVO.setNomeCliente(servicoAutorizacao.getCliente().getNome());
			servicoAutorizacaoVO.setDescricaoServicoTipo(servicoAutorizacao.getServicoTipo().getDescricao());
			servicoAutorizacaoVO.setDescricaoPontoConsumo(servicoAutorizacao.getPontoConsumo().getDescricao());
			servicoAutorizacaoVO.setChavePrimaria(servicoAutorizacao.getChavePrimaria());
			servicoAutorizacaoVO.setEnderecoImovel(servicoAutorizacao.getPontoConsumo().getEnderecoFormatado());
			
			listaServicoAutorizacaoVO.add(servicoAutorizacaoVO);

		}
		
		for(ItemLoteComunicacao item : colecaoItemLote) {
			repositorioServicoAutorizacao.atualizar(item, item.getClass());
		}
		
		relatorio.add(this.gerarRelatorioListagemAutorizacaoServico(listaServicoAutorizacaoVO));
		
		return relatorio;
		
	}

	private ServicoAutorizacao criarServicoAutorizacaoLote(ServicoTipo servicoTipo, AutorizacaoServicoLoteVO autorizacaoServicoLoteVO, Date dataHoraExecucao) throws NegocioException {
		
		Equipe equipe = Fachada.getInstancia().consultarEquipePorDescricao(autorizacaoServicoLoteVO.getEquipe());
		
		ServicoAutorizacao servicoAutorizacao = new ServicoAutorizacao();
		servicoAutorizacao.setDataGeracao(Calendar.getInstance().getTime());
		servicoAutorizacao.setServicoTipo(servicoTipo);
		servicoAutorizacao.setDataPrevisaoEncerramento(dataHoraExecucao);
		servicoAutorizacao.setEquipe(equipe);
		servicoAutorizacao.setServicoTipoPrioridade(servicoTipo.getServicoTipoPrioridade());
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterUltimoContratoPontoConsumoPorPontoConsumo(autorizacaoServicoLoteVO.getPontoConsumo().getChavePrimaria());

		Cliente cliente = contratoPontoConsumo != null ? contratoPontoConsumo.getContrato().getClienteAssinatura() : null;
		this.adicionarClienteAoServicoAutorizacao(cliente, servicoAutorizacao);


		this.adicionarContratoPontoDeConsumoEmServicoAutorizacao(servicoAutorizacao, contratoPontoConsumo);

		servicoAutorizacao.getServicoAutorizacaoMaterial()
				.addAll(converterListaMaterial(servicoTipo.getListaMateriais(), servicoAutorizacao));


		servicoAutorizacao.getServicoAutorizacaoEquipamento()
				.addAll(converterListaEquipamento(servicoTipo.getListaEquipamentoEspecial(), servicoAutorizacao));
		servicoAutorizacao.setImovel(autorizacaoServicoLoteVO.getPontoConsumo().getImovel());
		servicoAutorizacao.setPontoConsumo(autorizacaoServicoLoteVO.getPontoConsumo());
		servicoAutorizacao.setDescricao("Autorização de Serviço Gerada em Lote");
		servicoAutorizacao.setStatus(obterStatus(Constantes.C_STATUS_SERV_AUT_ABERTO));
		
		return servicoAutorizacao;
	}
	
	private List<IndicadorEmergenciaVO> montarListaItensRelatorioIndicadorEmergencia(
			Collection<IndicadorEmergenciaVO> itensIndicador, IndicadorEmergenciaVO indicadorEmergenciaVO, String periodoCriacao
			, String descricaoTipoServico) {
		List<IndicadorEmergenciaVO> listaRelatorio = new ArrayList<IndicadorEmergenciaVO>();
		int mesFiltroOriginal = Integer.valueOf(periodoCriacao.split("/")[0]);
		int mesFiltro = 12;
		IndicadorEmergenciaVO relatorio;
		for (int i = 1; i <= mesFiltro; i++) {
			relatorio = new IndicadorEmergenciaVO();
			relatorio.setMesAno(DataUtil.obterNomeMesPeloNumero(i));
			listaRelatorio.add(relatorio);
			relatorio.setMesAnoAuxiliar(i);
			relatorio.setMesMaximo(mesFiltroOriginal);
		}
		
		if(descricaoTipoServico.equals("FALTA DE GAS")) {
		for (IndicadorEmergenciaVO item : itensIndicador) {
			String hora = item.getHorasMaxima();
			String itemMes = DataUtil.obterNomeMesPeloNumero(Integer.valueOf(item.getMesAno().split("-")[1]));
			if (hora.equals("1h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHora1(item.getTotalRegistros());
						break;
					}
				}
				
			} else if (hora.equals("2h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHora2(item.getTotalRegistros());
						break;
					}
				}
			} else if (hora.equals("3h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHora3(item.getTotalRegistros());
						break;
					}
				}
			}  else if (hora.equals(">3h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHoraMaior3(item.getTotalRegistros());
						break;
					}
				}
			}
		}
	}else {
		for (IndicadorEmergenciaVO item : itensIndicador) {
			String hora = item.getHorasMaxima();
			String itemMes = DataUtil.obterNomeMesPeloNumero(Integer.valueOf(item.getMesAno().split("-")[1]));
			if (hora.equals("até 0,5h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHora1(item.getTotalRegistros());
						break;
					}
				}
				
			} else if (hora.equals("até 1h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHora2(item.getTotalRegistros());
						break;
					}
				}
			} else if (hora.equals("1h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHora3(item.getTotalRegistros());
						break;
					}
				}
			}  else if (hora.equals(">1h")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setHoraMaior3(item.getTotalRegistros());
						break;
					}
				}
			}
		}
	}
		return listaRelatorio;
	}
	
	private List<IndiceVazamentoVO> montarListaNumeroVazamentos(
			Collection<IndiceVazamentoVO> itensIndicador, IndiceVazamentoVO indicadorEmergenciaVO, String periodoCriacao
			, String descricaoTipoServico) {
		
		List<IndiceVazamentoVO> listaRelatorio = new ArrayList<IndiceVazamentoVO>();
		int mesFiltroOriginal = Integer.valueOf(periodoCriacao.split("/")[0]);
		int anoFiltro = Integer.valueOf(periodoCriacao.split("/")[1]);
		int mesFiltro = 12;
		IndiceVazamentoVO relatorio;
		List<RedeComprimentoVO> listComprimentosRede = consultarRedeComprimento(anoFiltro);
		int comprimentoRedeSize = listComprimentosRede.size();
		
		for (int i = 1; i <= mesFiltro; i++) {
			relatorio = new IndiceVazamentoVO();
			relatorio.setMesAno(DataUtil.obterNomeMesPeloNumero(i));
			if(i <= comprimentoRedeSize) {
				relatorio.setComprimentoRede(String.valueOf(listComprimentosRede.get(i-1).getRedeComprimento()));
			}else {
				relatorio.setComprimentoRede("");
			}
			relatorio.setMesAnoAuxiliar(i);
			relatorio.setMesMaximo(mesFiltroOriginal);
			listaRelatorio.add(relatorio);
		}
		
		for (IndiceVazamentoVO item : itensIndicador) {
			String tipoInstalacao = item.getTipoInstalacao();
			String itemMes = DataUtil.obterNomeMesPeloNumero(Integer.valueOf(item.getMesAno().split("-")[1]));
			if (tipoInstalacao.equals("CRM/CM")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setLocalCRM(item.getTotalRegistros());
						break;
					}
				}
				
			} else if (tipoInstalacao.equals("ERPM")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setLocalERMP(item.getTotalRegistros());
						break;
					}
				}
			} else if (tipoInstalacao.equals("ERP")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setLocalERP(item.getTotalRegistros());
						break;
					}
				}
			}  else if (tipoInstalacao.equals("ETC")) {
				for (int i=0; i < listaRelatorio.size(); i++) {
					if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
						listaRelatorio.get(i).setLocalETC(item.getTotalRegistros());
						break;
					}
				}
			}
			  else if (tipoInstalacao.equals("Rede PEAD")) {
					for (int i=0; i < listaRelatorio.size(); i++) {
						if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
							listaRelatorio.get(i).setLocalRedePEAD(item.getTotalRegistros());
							break;
						}
					}
			}
			  else if (tipoInstalacao.equals("Rede Aço")) {
					for (int i=0; i < listaRelatorio.size(); i++) {
						if(itemMes.equals(listaRelatorio.get(i).getMesAno())) {
							listaRelatorio.get(i).setLocalRedeAco(item.getTotalRegistros());
							break;
						}
					}
			}
		}
	
		return listaRelatorio;
	}
	
	private List<IndiceVazamentoVO> montarListaNumeroVazamentosAcumulados(
			Collection<IndiceVazamentoVO> itensIndicador, IndiceVazamentoVO indicadorEmergenciaVO, String periodoCriacao
			, String descricaoTipoServico) {
		
		List<IndiceVazamentoVO> listaRelatorio = new ArrayList<IndiceVazamentoVO>();
		int mesFiltroOriginal = Integer.valueOf(periodoCriacao.split("/")[0]);
		int mesFiltro = 12;
		IndiceVazamentoVO relatorio;

		int acumulosCRMTotais = 0;
		int acumulosERMPTotais = 0;
		int acumulosERPTotais = 0;
		int acumulosETCTotais = 0;
		int acumuloPEADTotais = 0;
		int acumuloAcoTotais = 0;
		
		for (int i = 1; i <= mesFiltro; i++) {
			relatorio = new IndiceVazamentoVO();
			relatorio.setMesAno(DataUtil.obterNomeMesPeloNumero(i));
			relatorio.setMesAnoAuxiliar(i);
			relatorio.setMesMaximo(mesFiltroOriginal);
			listaRelatorio.add(relatorio);
		}
		
		for (IndiceVazamentoVO item : itensIndicador) {
			
			String tipoInstalacao = item.getTipoInstalacao();
			String itemMes = DataUtil.obterNomeMesPeloNumero(Integer.valueOf(item.getMesAno().split("-")[1]));
			
				if(tipoInstalacao.equals("CRM/CM")) {
					acumulosCRMTotais += item.getTotalRegistros();
					for (int i = 0; i < listaRelatorio.size(); i++) {
						if(listaRelatorio.get(i).getMesAno().equals(itemMes)) {
							listaRelatorio.get(i).setLocalCRM(acumulosCRMTotais);
						}
					}
				}
				if(tipoInstalacao.equals("ERPM")) {
					acumulosERMPTotais += item.getTotalRegistros();
					for (int i = 0; i < listaRelatorio.size(); i++) {
						if(listaRelatorio.get(i).getMesAno().equals(itemMes)) {
							listaRelatorio.get(i).setLocalERMP(acumulosERMPTotais);
						}
					}
				}
				if(tipoInstalacao.equals("ERP")) {
					acumulosERPTotais += item.getTotalRegistros();
					for (int i = 0; i < listaRelatorio.size(); i++) {
						if(listaRelatorio.get(i).getMesAno().equals(itemMes)) {
							listaRelatorio.get(i).setLocalERP(acumulosERPTotais);
						}
					}
				}
				if(tipoInstalacao.equals("ETC")) {
					acumulosETCTotais += item.getTotalRegistros();
					for (int i = 0; i < listaRelatorio.size(); i++) {
						if(listaRelatorio.get(i).getMesAno().equals(itemMes)) {
							listaRelatorio.get(i).setLocalETC(acumulosETCTotais);
						}
					}
				}
				if(tipoInstalacao.equals("Rede PEAD")) {
					acumuloPEADTotais += item.getTotalRegistros();
					for (int i = 0; i < listaRelatorio.size(); i++) {
						if(listaRelatorio.get(i).getMesAno().equals(itemMes)) {
							listaRelatorio.get(i).setLocalRedePEAD(acumuloPEADTotais);
						}
					}
				}
				if(tipoInstalacao.equals("Rede Aço")) {
					acumuloAcoTotais += item.getTotalRegistros();
					for (int i = 0; i < listaRelatorio.size(); i++) {
						if(listaRelatorio.get(i).getMesAno().equals(itemMes)) {
							listaRelatorio.get(i).setLocalRedeAco(acumuloAcoTotais);
						}
					}
				}
				
		}
		acumulosCRMTotais = 0;
		acumulosERMPTotais = 0;
		acumulosERPTotais = 0; 
		acumulosETCTotais = 0; 
		acumuloPEADTotais = 0; 
		acumuloAcoTotais = 0; 
		for (int i = 0; i < listaRelatorio.size(); i++) {
			if(listaRelatorio.get(i).getLocalCRM() != null) {
				acumulosCRMTotais = listaRelatorio.get(i).getLocalCRM();
			}
			if(listaRelatorio.get(i).getLocalERMP() != null) {
				acumulosERMPTotais = listaRelatorio.get(i).getLocalERMP();
			}
			if(listaRelatorio.get(i).getLocalERP() != null) {
				acumulosERPTotais = listaRelatorio.get(i).getLocalERP();
			}
			if(listaRelatorio.get(i).getLocalETC() != null) {
				acumulosETCTotais = listaRelatorio.get(i).getLocalETC();
			}
			if(listaRelatorio.get(i).getLocalETC() != null) {
				acumulosETCTotais = listaRelatorio.get(i).getLocalETC();
			}
			if(listaRelatorio.get(i).getLocalRedePEAD() != null) {
				acumuloPEADTotais = listaRelatorio.get(i).getLocalRedePEAD();
			}
			if(listaRelatorio.get(i).getLocalRedeAco() != null) {
				acumuloAcoTotais = listaRelatorio.get(i).getLocalRedeAco();
			}
			listaRelatorio.get(i).setLocalCRM(acumulosCRMTotais);
			listaRelatorio.get(i).setLocalERMP(acumulosERMPTotais);
			listaRelatorio.get(i).setLocalERP(acumulosERPTotais);
			listaRelatorio.get(i).setLocalETC(acumulosETCTotais);
			listaRelatorio.get(i).setLocalRedePEAD(acumuloPEADTotais);
			listaRelatorio.get(i).setLocalRedeAco(acumuloAcoTotais);
		}
	
		return listaRelatorio;
	}
	
	@Override
	public Collection<PontoConsumo> filtrarPontosParaLoteDeAutorizacaoServico(String descricaoPonto, String codigoLegado, String servicoTipo, Integer offset) {
		return this.repositorioServicoAutorizacao.filtrarPontosParaLoteDeAutorizacaoServico(descricaoPonto, codigoLegado, servicoTipo, offset);
	}
	
	@Override
	public List<Object[]> consultarLocalizacaoOperadorMobile(Long idFuncionario) {
		return this.repositorioServicoAutorizacao.consultarLocalizacaoOperadorMobile(idFuncionario);
	}

	@Override
	public Boolean verificarAsAbertaMesmaOperacao(Long idPontoConsumo, Long idServicoTipo) {
		return this.repositorioServicoAutorizacao.verificarAsAbertaMesmaOperacao(idPontoConsumo, idServicoTipo);
	}

	@Override
	public Erp consultarErpPorChaveImovel(Long chavePrimariaImovel) {
		return this.repositorioServicoAutorizacao.consultarErpPorChaveImovel(chavePrimariaImovel);
	}
	
	@Override
	public void enviarEmailEncerramentoApp(AcaoServicoAutorizacao acao, ServicoAutorizacao servicoAutorizacao) throws GGASException {
		enviarEmail(acao, servicoAutorizacao);
	}

	@Override
	public Collection<IndicadorEmergenciaVO> consultarIndicadorServicoTipo(String periodoCriacao, String descricaoTipoServico, Long localRede) {
		return this.repositorioServicoAutorizacao.consultarIndicadorServicoTipo(periodoCriacao, descricaoTipoServico, localRede);
	}

	@Override
	public byte[] gerarRelatorioIndicadorEmergencia(FormatoImpressao formatoImpressao, String exibirFiltros, String periodoCriacao, String descricaoTipoServico, Long localRede) throws GGASException {

		IndicadorEmergenciaVO indicadorEmergenciaVO = new IndicadorEmergenciaVO();
		
		List<IndicadorEmergenciaVO> indicadorEmergenciaVOs = (List<IndicadorEmergenciaVO>) consultarIndicadorServicoTipo(periodoCriacao, descricaoTipoServico, localRede);
		
		
		List<IndicadorEmergenciaVO> dados = montarListaItensRelatorioIndicadorEmergencia(indicadorEmergenciaVOs, indicadorEmergenciaVO, periodoCriacao, descricaoTipoServico);
		
		List<IndicadorEmergenciaVO> indicadoresEmergenciaPorPontoDeConsumo = (List<IndicadorEmergenciaVO>) consultarIndicadorServicoTipoPontosConsumo(periodoCriacao,descricaoTipoServico, localRede);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		
		parametros.put("dataAtual", formato.format(new Date()));
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
		parametros.put("logo", logo);

		Collection<FiltrosRelatorioHelper> filtros = new HashSet<FiltrosRelatorioHelper>();
		FiltrosRelatorioHelper filtro = new FiltrosRelatorioHelper();

		if(!periodoCriacao.isEmpty()) {
			filtro.setPeriodoResolucao(periodoCriacao);
		}
		if(!descricaoTipoServico.isEmpty()) {
			filtro.setTipoServico(descricaoTipoServico);
		}
		
		String anoReferencia = periodoCriacao != null && !periodoCriacao.isEmpty() ? periodoCriacao.split("/")[1] : "";
		
		filtros.add(filtro);
		parametros.put("filtros", filtros);
		parametros.put("listaIndicadorEmergencia", dados);
		parametros.put("logoArsal", Constantes.URL_IMAGENS_GGAS + "logoArsal.png");
		parametros.put("newLogoAlgas", Constantes.URL_IMAGENS_GGAS + "newLogoAlgas.png");
		parametros.put("referencia", anoReferencia);
		parametros.put("descricaoTipoServico",descricaoTipoServico);
		parametros.put("listaIndicadoresEmergenciaPorPontoDeConsumo",indicadoresEmergenciaPorPontoDeConsumo);
		
		byte[] relatorioIndicador = null;

		if(descricaoTipoServico.equals("FALTA DE GAS")) {
			relatorioIndicador = RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_TEMPO_ATENDIMENTO_EMERGENCIA, formatoImpressao);
		}else if(descricaoTipoServico.equals("VAZAMENTO DE GAS")){
			relatorioIndicador = RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_TEMPO_ATENDIMENTO_EMERGENCIA, formatoImpressao);
		}else {
			
		}
		
		return relatorioIndicador;
	}

	@Override
	public Collection<IndicadorEmergenciaVO> consultarIndicadorServicoTipoPontosConsumo(String periodoCriacao,
			String descricaoTipoServico, Long localRede) {
		return this.repositorioServicoAutorizacao.consultarIndicadorServicoTipoPontosConsumo(periodoCriacao, descricaoTipoServico, localRede);
	}

	@Override
	public Collection<IndiceVazamentoVO> consultarIndiceVazamentos(String periodoCriacao, String descricaoTipoServico, Long localRede) {
		return this.repositorioServicoAutorizacao.consultarIndiceVazamentos(periodoCriacao, descricaoTipoServico, localRede);
	}

	@Override
	public byte[] gerarRelatorioIndiceVazamento(FormatoImpressao formatoImpressao, String exibirFiltros,
			String periodoCriacao, String descricaoTipoServico, Long localRede) throws GGASException {
		IndiceVazamentoVO indiceVazamentoVO = new IndiceVazamentoVO();
		
		List<IndiceVazamentoVO> indiceVazamentoVOs = (List<IndiceVazamentoVO>) consultarIndiceVazamentos(periodoCriacao, descricaoTipoServico, localRede);
		
		
		List<IndiceVazamentoVO> dados = montarListaNumeroVazamentos(indiceVazamentoVOs, indiceVazamentoVO, periodoCriacao, descricaoTipoServico);
		
		List<IndiceVazamentoVO> listaVazamentosAcumulados = montarListaNumeroVazamentosAcumulados(indiceVazamentoVOs, indiceVazamentoVO, periodoCriacao, descricaoTipoServico);
		
		List<IndiceVazamentoVO> listaVazamentosPorPontoConsumo = (List<IndiceVazamentoVO>) consultarIndiceVazamentosPorPontoConsumo (periodoCriacao,descricaoTipoServico, localRede);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		
		parametros.put("dataAtual", formato.format(new Date()));
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
		parametros.put("logo", logo);

		Collection<FiltrosRelatorioHelper> filtros = new HashSet<FiltrosRelatorioHelper>();
		FiltrosRelatorioHelper filtro = new FiltrosRelatorioHelper();

		if(!periodoCriacao.isEmpty()) {
			filtro.setPeriodoResolucao(periodoCriacao);
		}
		if(!descricaoTipoServico.isEmpty()) {
			filtro.setTipoServico(descricaoTipoServico);
		}

		String anoReferencia = periodoCriacao != null && !periodoCriacao.isEmpty() ? periodoCriacao.split("/")[1] : "";

		filtros.add(filtro);
		parametros.put("filtros", filtros);
		parametros.put("listaVazamentos", dados);
		parametros.put("listaVazamentosAcumulados",listaVazamentosAcumulados);
		parametros.put("logoArsal", Constantes.URL_IMAGENS_GGAS + "logoArsal.png");
		parametros.put("newLogoAlgas", Constantes.URL_IMAGENS_GGAS + "newLogoAlgas.png");
		parametros.put("referencia", anoReferencia);
		parametros.put("mesReferencia", DataUtil.obterNomeMesPeloNumero(Integer.valueOf(periodoCriacao.split("/")[0])));
		parametros.put("descricaoTipoServico",descricaoTipoServico);
		parametros.put("listaVazamentosPorPontoConsumo",listaVazamentosPorPontoConsumo);
		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_NUMERO_VAZAMENTOS, formatoImpressao);
	}

	@Override
	public Collection<IndiceVazamentoVO> consultarIndiceVazamentosPorPontoConsumo(String periodoCriacao,
			String descricaoTipoServico, Long localRede) {
		return this.repositorioServicoAutorizacao.consultarIndiceVazamentosPorPontoConsumo(periodoCriacao, descricaoTipoServico, localRede);
	}
	
	@Override
	public Collection<ServicoAutorizacaoAgenda> obterListaAgendamentosPorDiaEquipe(Date data, Equipe equipe, Long statusASCancelada) {
		return this.repositorioServicoAutorizacao.obterListaAgendamentosPorDiaEquipe(data, equipe, statusASCancelada);
	}
	
	@Override
	public Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorChavesPrimarias(Long[] chaves) {
		return this.repositorioServicoAutorizacao.consultarServicoAutorizacaoPorChavesPrimarias(chaves);
	}

	@Transactional
	@Override
	public void atualizar(ServicoAutorizacaoRegistroLocalImpl servicoAutorizacaoRegistroLocal)
			throws ConcorrenciaException, NegocioException {
		this.repositorioServicoAutorizacao.atualizar(servicoAutorizacaoRegistroLocal, ServicoAutorizacaoRegistroLocalImpl.class);
	}

	@Override
	public ServicoAutorizacaoRegistroLocalImpl consultarRegistroServicoAutorizacaoRegLocal(Long chavePrimaria)
			throws NegocioException {
		return this.repositorioServicoAutorizacao.consultarRegistroServicoAutorizacaoRegLocal(chavePrimaria);
	}
	
	
	@Override
	public ServicoAutorizacaoAgenda obterServicoAutorizacaoAgendaPorServicoAutorizacao(Long chavePrimaria) throws NegocioException {
		return repositorioServicoAutorizacao.obterServicoAutorizacaoAgendaPorServicoAutorizacao(chavePrimaria);
	}
	
	@Override
	public ServicoAutorizacaoTemp obterServicoAutorizacaoTemp(Long chavePrimaria) {
		return repositorioServicoAutorizacao.obterServicoAutorizacaoTemp(chavePrimaria);
	}
	
	@Override
	public List<RedeComprimentoVO> consultarRedeComprimento(Integer anoFiltro) {
		return this.repositorioServicoAutorizacao.consultarRedeComprimento(anoFiltro);
	}

	@Override
	public List<ServicoAutorizacaoResumoVO> consultarServicoAutorizacaoResumo(
			ServicoAutorizacaoVO servicoAutorizacaoVO, FormatoImpressao formatoImpressao, String agrupamento,
			String exibirFiltros, Boolean isGrafico) throws GGASException {
		return repositorioServicoAutorizacao.consultarServicoAutorizacaoResumo(servicoAutorizacaoVO, formatoImpressao,  agrupamento,
				 exibirFiltros, isGrafico);
	}
	
}