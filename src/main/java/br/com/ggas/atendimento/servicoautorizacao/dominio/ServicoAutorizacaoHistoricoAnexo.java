package br.com.ggas.atendimento.servicoautorizacao.dominio;

import br.com.ggas.geral.impl.AnexoImpl;

/**
 * ServicoAutorizacaoHistoricoAnexo
 * 
 * Classe para salvar os anexos das autorizações de serviço
 */
public class ServicoAutorizacaoHistoricoAnexo extends AnexoImpl {

	/**
	 * serial UID
	 */
	private static final long serialVersionUID = -6799871480599480315L;

	private ServicoAutorizacao servicoAutorizacao;

	/**
	 * 
	 * @return servicoAutorizacao
	 * 				a autorização de serviço
	 */
	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}

	/**
	 * 
	 * @param servicoAutorizacao 
	 * 				a autorização de serviço
	 */
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
	}

}
