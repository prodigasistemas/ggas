package br.com.ggas.atendimento.servicoautorizacao.dominio;

import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.cadastro.imovel.PontoConsumo;

public class AutorizacaoServicoLoteVO {
	private PontoConsumo pontoConsumo;
	private String dataExecucao;
	private String periodoExecucao;
	private Double distanciaEntrePontos;
	private String tempoExecucao;
	private String horaInicialExecucao;
	private String descricaoPontoConsumo;
	private ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno;
	private String tempoDistancia;
	private String enderecoFormatado;
	private String codigoLegado;
	private String segmento;
	private String equipe;
	private String tempoExecucaoMinutos;

	public PontoConsumo getPontoConsumo() {
		return pontoConsumo;
	}

	public void setPontoConsumo(PontoConsumo pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	public String getDataExecucao() {
		return dataExecucao;
	}

	public void setDataExecucao(String dataExecucao) {
		this.dataExecucao = dataExecucao;
	}

	public String getPeriodoExecucao() {
		return periodoExecucao;
	}

	public void setPeriodoExecucao(String periodoExecucao) {
		this.periodoExecucao = periodoExecucao;
	}

	public Double getDistanciaEntrePontos() {
		return distanciaEntrePontos;
	}

	public void setDistanciaEntrePontos(Double distanciaEntrePontos) {
		this.distanciaEntrePontos = distanciaEntrePontos;
	}

	public String getTempoExecucao() {
		return tempoExecucao;
	}

	public void setTempoExecucao(String tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}

	public String getHoraInicialExecucao() {
		return horaInicialExecucao;
	}

	public void setHoraInicialExecucao(String horaInicialExecucao) {
		this.horaInicialExecucao = horaInicialExecucao;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public ServicoTipoAgendamentoTurno getServicoTipoAgendamentoTurno() {
		return servicoTipoAgendamentoTurno;
	}

	public void setServicoTipoAgendamentoTurno(ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno) {
		this.servicoTipoAgendamentoTurno = servicoTipoAgendamentoTurno;
	}

	public String getTempoDistancia() {
		return tempoDistancia;
	}

	public void setTempoDistancia(String tempoDistancia) {
		this.tempoDistancia = tempoDistancia;
	}

	public String getEnderecoFormatado() {
		return enderecoFormatado;
	}

	public void setEnderecoFormatado(String enderecoFormatado) {
		this.enderecoFormatado = enderecoFormatado;
	}

	public String getCodigoLegado() {
		return codigoLegado;
	}

	public void setCodigoLegado(String codigoLegado) {
		this.codigoLegado = codigoLegado;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public String getEquipe() {
		return equipe;
	}

	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}

	public String getTempoExecucaoMinutos() {
		return tempoExecucaoMinutos;
	}

	public void setTempoExecucaoMinutos(String tempoExecucaoMinutos) {
		this.tempoExecucaoMinutos = tempoExecucaoMinutos;
	}
}
