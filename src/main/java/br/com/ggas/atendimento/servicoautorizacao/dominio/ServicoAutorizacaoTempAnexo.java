package br.com.ggas.atendimento.servicoautorizacao.dominio;

import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ServicoAutorizacaoTempAnexo extends EntidadeNegocio {
	
	public static String BEAN_ID_SERVICO_AUTORIZACAO_TEMP_ANEXO = "servicoAutorizacaoTempAnexo";


	public String getDescricao();

	public void setDescricao(String descricao);
	
	public String getCaminho();

	public void setCaminho(String caminho);

	public ServicoAutorizacao getServicoAutorizacao();

	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao);
	
	public String getNome();
	
	public void setNome(String nome);
}
