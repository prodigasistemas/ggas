package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;

public interface ServicoAutorizacaoTemp extends EntidadeNegocio {
	
	public static String BEAN_ID_SERVICO_AUTORIZACAO_TEMP = "servicoAutorizacaoTemp";

	public String getObservacao();
	
	
	public void setObservacao(String observacao);
	
	
	public Date getDataLeituraOriginal();
	
	
	public void setDataLeituraOriginal(Date dataLeituraOriginal);
	
	
	public Integer getNumeroLeituraOriginal();
	
	
	public void setNumeroLeituraOriginal(Integer numeroLeituraOriginal);
	
	
	public Date getDataLeituraNovo();
	
	
	public void setDataLeituraNovo(Date dataLeituraNovo);
	
	
	public Integer getNumeroLeituraNovo();
	
	
	public void setNumeroLeituraNovo(Integer numeroLeituraNovo);
	
	
	public String getNumeroLacreMedidorNovo1();
	
	
	public void setNumeroLacreMedidorNovo1(String numeroLacreMedidorNovo1);
	
	
	public String getNumeroLacreMedidorNovo2();
	
	
	public void setNumeroLacreMedidorNovo2(String numeroLacreMedidorNovo2);
	
	
	public String getNumeroLacreMedidorNovo3();
	
	
	public void setNumeroLacreMedidorNovo3(String numeroLacreMedidorNovo3);
	
	
	public String getLocalizacaoGeografica();
	
	
	public void setLocalizacaoGeografica(String localizacaoGeografica);
	
	
	public Date getInicioExecucao();
	
	
	public void setInicioExecucao(Date inicioExecucao);
	
	
	public Date getFimExecucao();
	
	
	public void setFimExecucao(Date fimExecucao);
	
	
	public String getMedidorNumeroSerieOriginal();
	
	
	public void setMedidorNumeroSerieOriginal(String medidorNumeroSerieOriginal);
	
	
	public String getMedidorNumeroSerieNovo();
	
	
	public void setMedidorNumeroSerieNovo(String medidorNumeroSerieNovo);
	
	
	public String getDescricaoGrauRisco();
	
	
	public void setDescricaoGrauRisco(String descricaoGrauRisco);
	
	
	public ServicoAutorizacao getServicoAutorizacao();
	
	
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao);
	
	
	public EntidadeConteudo getStatus();
	
	
	public void setStatus(EntidadeConteudo status);
	
	
	public Funcionario getFuncionario();
	
	
	public void setFuncionario(Funcionario funcionario);
	
	
	public OperacaoMedidor getMedidorOperacao();
	
	
	public void setMedidorOperacao(OperacaoMedidor medidorOperacao);
	
	
	public MotivoOperacaoMedidor getMotivoOperacaoMedidor();
	
	
	public void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor);
	
	
	public Medidor getMedidorOriginal();
	
	
	public void setMedidorOriginal(Medidor medidorOriginal);
	
	
	public Medidor getMedidorNovo();
	
	
	public void setMedidorNovo(Medidor medidorNovo);
	
	
	public MedidorLocalInstalacao getMedidorLocalInstalacao();
	
	
	public void setMedidorLocalInstalacao(MedidorLocalInstalacao medidorLocalInstalacao);
	
	
	public MedidorProtecao getMedidorProtecao();
	
	
	public void setMedidorProtecao(MedidorProtecao medidorProtecao);
	
	
	public FaixaPressaoFornecimento getFaixaPressaoFornecimento();
	
	
	public void setFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento);
	
	
	public ServicoAutorizacaoMotivoEncerramento getServicoAutorizacaoMotivoEncerramento();
	
	
	public void setServicoAutorizacaoMotivoEncerramento(
			ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento);
	
	
	public EntidadeConteudo getOcorrencia();
	
	
	public void setOcorrencia(EntidadeConteudo ocorrencia);
	
	
	public EntidadeConteudo getLocalRede();
	
	
	public void setLocalRede(EntidadeConteudo localRede);
	
	
	public EntidadeConteudo getCausaAtendimento();
	
	
	public void setCausaAtendimento(EntidadeConteudo causaAtendimento);
	
	
	public EntidadeConteudo getSolucaoAtendimento();
	
	
	public void setSolucaoAtendimento(EntidadeConteudo solucaoAtendimento);
	

	public String getDataExecucaoFormatada();
	
	
	public String getHoraAtendimento();
	
	
	public void setHoraAtendimento(String horaAtendimento);
	
	
	public String getIdentificacao();
	
	
	public void setIdentificacao(String identificacao);
	
	
	public Date getColetaDataHora();
	
	
	public void setColetaDataHora(Date coletaDataHora);
	
	
	public Double getColetaPressao();
	
	
	public void setColetaPressao(Double coletaPressao);
	
	
	public String getColetaLacre();
	
	
	public void setColetaLacre(String coletaLacre);
	
	
	public EntidadeConteudo getCilindro1();
	
	
	public void setCilindro1(EntidadeConteudo cilindro1);
	
	
	public EntidadeConteudo getCilindro2();
	
	
	public void setCilindro2(EntidadeConteudo cilindro2);
	
	
	public EntidadeConteudo getCilindro3();
	
	
	public void setCilindro3(EntidadeConteudo cilindro3);
	
	
	public EntidadeConteudo getCilindro4();
	
	
	public void setLocalInstalacao(EntidadeConteudo localInstalacao);
	
	public EntidadeConteudo getLocalInstalacao();
	
	
	public void setCilindro4(EntidadeConteudo anormaia);
	
	public EntidadeConteudo getAnormalidade();
	
	
	public void setAnormalidade(EntidadeConteudo anormalidade);
	
	public boolean isValidoParaAtualizacao();
	
	public boolean possuiAtualizacaoContratual();
	
	public void setPressaoFornecimentoNova(BigDecimal pressaoFornecimentoNova);
	
	public BigDecimal getPressaoFornecimentoNova();
	
	public void setVazaoFornecimentoNova(BigDecimal vazaoFornecimentoNova);
	
	public BigDecimal getVazaoFornecimentoNova();
	
	public boolean possuiNovoMedidor();
	
}
