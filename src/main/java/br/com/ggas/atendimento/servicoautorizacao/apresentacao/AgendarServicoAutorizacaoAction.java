/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 20/11/2013 11:46:09
 * @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.apresentacao;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AcaoServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas telas relacionadas a classe AgendarServicoAutorizacao
 */
@Controller
public class AgendarServicoAutorizacaoAction extends GenericAction {

	private static final String LISTA_TIPO_SERVICO = "listaTipoServico";

	private static final String LISTA_EQUIPE = "listaEquipe";

	private static final int INDICE_FINAL_HORA = 2;

	private static final int INDICE_FINAL_MINUTO = 5;

	private static final int INDICE_INICIAL_MINUTO = 3;

	private static final String LISTA_TURNOS = "listaTurnos";

	private static final String SERVICO_AUTORIZACAO_AGENDA_LIST = "servicoAutorizacaoAgendaList";

	private static final String TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS = "gridServicoAutorizacaoAgendamentos";
	
	private static final String DESCRICAO_ENTIDADE = "Turno";

	private static final Logger LOG = Logger.getLogger(AgendarServicoAutorizacaoAction.class);

	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	private ControladorEquipe controladorEquipe;

	@Autowired
	private ControladorServicoTipoPrioridade controladorServicoTipoPrioridade;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	private static final String DATA = "data";

	/**
	 * Exibir inclusao agendamento.
	 * 
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("exibirInclusaoAgendamento")
	public ModelAndView exibirInclusaoAgendamento() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirInclusaoAgendamento");
		model.addObject(LISTA_TIPO_SERVICO, controladorServicoTipo.listarServicoTipoAgendamento(Boolean.TRUE, Boolean.TRUE));
		model.addObject(LISTA_EQUIPE, controladorEquipe.listarEquipe());
		model.addObject("listaEquipeComponente", controladorEquipe.listarTodasEquipeComponente());
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(DESCRICAO_ENTIDADE));
		model.addObject("exibirBotaoAdicionar", Boolean.TRUE);
		return model;
	}

	/**
	 * Consultar servico autorizacao agenda.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda pesquisa vo
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("consultarServicoAutorizacaoAgenda")
	public ModelAndView consultarServicoAutorizacaoAgenda(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS);

		
		if(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo() == null) {
			ServicoTipo servicoTipo = new ServicoTipo();
			servicoTipo.setChavePrimaria(-1l);
			
			servicoAutorizacaoAgendaPesquisaVO.setServicoTipo(servicoTipo);
		}
		
		List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgendaList =
				controladorServicoAutorizacao.consultarServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO);

		servicoAutorizacaoAgendaList.stream().forEach(p -> p.setDataAgenda(DataUtil.arredondarMinutosHora(p.getDataAgenda())));

		model.addObject(SERVICO_AUTORIZACAO_AGENDA_LIST, servicoAutorizacaoAgendaList);
		model.addObject("listaEquipeComponente", controladorEquipe.listarTodasEquipeComponente());

		if (servicoAutorizacaoAgendaList.isEmpty()) {
			model.addObject("listaServicoAutorizacaoPreenchida", false);
		} else {
			model.addObject("listaServicoAutorizacaoPreenchida", true);
		}

		return model;
	}

	/**
	 * Alterar horario.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda pesquisa vo
	 * @return the model and view
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("alterarHorario")
	public ModelAndView alterarHorario(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS);
		try {
			ServicoAutorizacaoAgenda agenda =
					controladorServicoAutorizacao.obterServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO.getChavePrimaria());
			Calendar cal = Calendar.getInstance();
			cal.setTime(agenda.getDataAgenda());
			cal.set(Calendar.HOUR_OF_DAY,
					Integer.parseInt(servicoAutorizacaoAgendaPesquisaVO.getHorario().substring(0, INDICE_FINAL_HORA)));
			cal.set(Calendar.MINUTE, Integer
					.parseInt(servicoAutorizacaoAgendaPesquisaVO.getHorario().substring(INDICE_INICIAL_MINUTO, INDICE_FINAL_MINUTO)));

			agenda.setDataAgenda(cal.getTime());
			controladorServicoAutorizacao.atualizarServicoAutorizacaoAgenda(agenda);
			List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgendaList =
					controladorServicoAutorizacao.consultarServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO);

			model.addObject(SERVICO_AUTORIZACAO_AGENDA_LIST, servicoAutorizacaoAgendaList);
		} catch (Exception e) {
			model = new ModelAndView("ajaxErro");
			mensagemAdvertencia(model, Constantes.FORMATO_HORA_INVALIDO);
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	/**
	 * Confirmar.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda
	 *                                           pesquisa vo
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("confirmar")
	public ModelAndView confirmar(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS);
		ServicoAutorizacaoAgenda agenda = controladorServicoAutorizacao
				.obterServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO.getChavePrimaria());

		agenda.setIndicadorConfirmado(Boolean.TRUE);
		controladorServicoAutorizacao.atualizarServicoAutorizacaoAgenda(agenda);
		List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgendaList = controladorServicoAutorizacao
				.consultarServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO);

		model.addObject(SERVICO_AUTORIZACAO_AGENDA_LIST, servicoAutorizacaoAgendaList);

		if (servicoAutorizacaoAgendaPesquisaVO.getServicoTipo() != null && servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getIndicadorEmailAgendar() != null
				&& !servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getIndicadorEmailAgendar()) {
			controladorServicoAutorizacao.enviarEmailAgendamentoServico(AcaoServicoAutorizacao.AGENDAR,
					agenda.getServicoAutorizacao(), servicoAutorizacaoAgendaPesquisaVO);
		}
		return model;
	}

	/**
	 * Desconfirmar.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda pesquisa vo
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("desconfirmar")
	public ModelAndView desconfirmar(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS);
		ServicoAutorizacaoAgenda agenda =
				controladorServicoAutorizacao.obterServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO.getChavePrimaria());

		agenda.setIndicadorConfirmado(Boolean.FALSE);
		controladorServicoAutorizacao.atualizarServicoAutorizacaoAgenda(agenda);
		List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgendaList =
				controladorServicoAutorizacao.consultarServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO);

		model.addObject(SERVICO_AUTORIZACAO_AGENDA_LIST, servicoAutorizacaoAgendaList);

		return model;
	}

	/**
	 * Remover agendamento.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda pesquisa vo
	 * @param chaveAgendamento the chave agendamento
	 * @param result the result
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerAgendamento")
	public ModelAndView removerAgendamento(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
			@RequestParam("chaveAgendamento") Long chaveAgendamento, BindingResult result) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS);
		ServicoAutorizacaoAgenda agenda = controladorServicoAutorizacao.obterServicoAutorizacaoAgenda(chaveAgendamento);

		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_PENDENTE);
		EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ENCERRADO);
		EntidadeConteudo statusEncerrado = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		if (agenda.getServicoAutorizacao().getStatus().getChavePrimaria() != statusPendente.getChavePrimaria()
				&& agenda.getServicoAutorizacao().getStatus().getChavePrimaria() != statusEncerrado.getChavePrimaria()) {
			controladorServicoAutorizacao.removerAgendamento(chaveAgendamento);
			List<ServicoAutorizacaoAgenda> servicoAutorizacaoAgendaList =
					controladorServicoAutorizacao.consultarServicoAutorizacaoAgenda(servicoAutorizacaoAgendaPesquisaVO);

			model.addObject(SERVICO_AUTORIZACAO_AGENDA_LIST, servicoAutorizacaoAgendaList);
		} else {
			model = new ModelAndView("ajaxErro");
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_EXCLUIR_AGENDAMENTO_PENDENTE);
		}

		return model;
	}

	/**
	 * Exibir popup pesquisar autorizacao servico.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda pesquisa vo
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirPopupPesquisarAutorizacaoServico")
	public ModelAndView exibirPopupPesquisarAutorizacaoServico(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {
		
		
		if(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo() == null) {
			ServicoTipo servicoTipo = new ServicoTipo();
			servicoTipo.setChavePrimaria(-1l);
			servicoAutorizacaoAgendaPesquisaVO.setServicoTipo(servicoTipo);
		}
		
		ModelAndView model = new ModelAndView("popupPesquisarServicoAutorizacao");
		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
		model.addObject("servicoAutorizacaoVO", servicoAutorizacaoVO);
		model.addObject("servicoAutorizacaoAgenda", servicoAutorizacaoAgendaPesquisaVO);
		model.addObject("listaServicoTipoPrioridade", controladorServicoTipoPrioridade.listar());
		model.addObject(LISTA_EQUIPE, controladorEquipe.listar());
		if(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo() != null) {
			Date data = Util.converterCampoStringParaData(DATA, servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(),
					Constantes.FORMATO_DATA_BR);
			
			model.addObject("listaTurnosDisponiveis", controladorServicoAutorizacao
					.consultarAgendamentosDisponiveisTurno(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo(), data));
			
			model.addObject("equipePrioritaria", controladorServicoTipo.obterEquipePrioritaria(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo().getChavePrimaria()));
		}
		
		model.addObject(LISTA_TURNOS, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(DESCRICAO_ENTIDADE));

		return model;
	}

	/**
	 * Adicionar servico autorizacao agendamento.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO the servico autorizacao agenda pesquisa vo
	 * @param chaves the chaves
	 * @param result the result
	 * @return the model and view
	 * @throws ParseException the parse exception
	 * @throws GGASException {@link GGASException}
	 * @throws IOException 
	 */
	@RequestMapping("adicionarServicoAutorizacaoAgendamento")
	public ModelAndView adicionarServicoAutorizacaoAgendamento(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
			@RequestParam("chavesPrimarias") Long[] chaves, BindingResult result) throws GGASException, IOException {

		controladorServicoAutorizacao.adicionarServicoAutorizacaoAgendamento(chaves, servicoAutorizacaoAgendaPesquisaVO, Boolean.FALSE);

		return exibirPopupPesquisarAutorizacaoServico(servicoAutorizacaoAgendaPesquisaVO);
	}

	/**
	 * Imprimir servico autorizacao.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO {@link ServicoAutorizacaoAgendaPesquisaVO}
	 * @param response {@link HttpServletResponse}
	 * @throws IOException {@link IOException}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("imprimirServicoAutorizacaoAgenda")
	public void imprimirServicoAutorizacao(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
			HttpServletResponse response) throws GGASException {

		byte[] relatorio = controladorServicoAutorizacao.gerarRelatorioAgendamento(servicoAutorizacaoAgendaPesquisaVO);

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();

		fileName.append(" attachment; filename=relatorioAutorizacaoServicoAgendamento_");
		fileName.append(diaMesAno);
		fileName.append("_");
		fileName.append(hora);
		fileName.append(".pdf");

		if (relatorio != null) {

			ServletOutputStream servletOutputStream;
			try {
				servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition", fileName.toString());
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e.getMessage());
				throw new GGASException(e);
			}
		}
	}

	/**
	 * Consultar servico autorizacao por protocolo. lista as autorizações de serviço, caso não exista é retornado nulo
	 * 
	 * @param protocolo the protocolo
	 * @param servicoTipo the servico tipo
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("consultarServicoAutorizacaoPorProtocolo")
	public ModelAndView consultarServicoAutorizacaoPorProtocolo(@RequestParam("protocolo") Long protocolo,
			@RequestParam("servicoTipo") Long servicoTipo, @RequestParam(value="dataSelecionada", required=false) String dataSelecionada) throws GGASException {

		ModelAndView model = new ModelAndView("gridServicoAutorizacaoAgendamentoPorChamado");
		if (protocolo != null) {
			ServicoAutorizacaoVO vo = new ServicoAutorizacaoVO();
			vo.setNumeroProtocoloChamado(protocolo);
			ServicoTipo servicotipo = new ServicoTipo();
			servicotipo.setChavePrimaria(servicoTipo);
			vo.setServicoTipo(servicotipo);
			
			if(!StringUtils.isEmpty(dataSelecionada)) {
				vo.setDataSelecionada(dataSelecionada);
			}
			

			List<ServicoAutorizacao> listaServicoAutorizacao =
					(List<ServicoAutorizacao>) controladorServicoAutorizacao.listarServicoAutorizacao(vo, Boolean.TRUE);
			if (!listaServicoAutorizacao.isEmpty()) {
				model.addObject("listaServicoAutorizacao", listaServicoAutorizacao);
			} else {
				model.addObject("listaServicoAutorizacao", null);
			}
		}
		return model;
	}

	/**
	 * Carregar Turnos
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO {@link ServicoAutorizacaoAgendaPesquisaVO}
	 * @return Model {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("carregarTurnos")
	public ModelAndView carregarTurnos(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {

		ModelAndView model = new ModelAndView(LISTA_TURNOS);
		ServicoTipo servicoTipo = servicoAutorizacaoAgendaPesquisaVO.getServicoTipo();
		model.addObject(LISTA_TURNOS, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse(DESCRICAO_ENTIDADE));
		Date data = Util.converterCampoStringParaData(DATA, servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada(),
				Constantes.FORMATO_DATA_BR);
		//model.addObject("listaTurnosDisponiveis", controladorServicoAutorizacao.consultarAgendamentosDisponiveisTurno(servicoTipo, data));

		return model;
	}

	/**
	 * Lanca Alerta após tentativa de abertura de popup para adicionar uma autorização de serviço na agenda informando que não é possivel
	 * realizar esta operação
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO {@link ServicoAutorizacaoAgendaPesquisaVO}
	 * @return Model {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibeAlertNaoHaServicoAutorizacao")
	public ModelAndView exibeAlertNaoHaServicoAutorizacao(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException {

		ModelAndView model = new ModelAndView("exibirInclusaoAgendamento");
		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
		servicoAutorizacaoVO.setServicoTipo(servicoAutorizacaoAgendaPesquisaVO.getServicoTipo());
		if (controladorServicoAutorizacao.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.TRUE).isEmpty()) {
			mensagemAdvertencia(model, Constantes.ALERTA_NAO_HA_AUTORIZACOES_DE_SERVICO_PARA_SEREM_AGENDADAS);
		}
		model.addObject(LISTA_TIPO_SERVICO, controladorServicoTipo.listarServicoTipoAgendamento(Boolean.TRUE, Boolean.TRUE));
		model.addObject(LISTA_EQUIPE, controladorEquipe.listarEquipe());
		model.addObject("exibirBotaoAdicionar", Boolean.TRUE);
		model.addObject("servicoAutorizacaoAgenda", servicoAutorizacaoAgendaPesquisaVO);
		model.addObject("servicoAutorizacaoVO", servicoAutorizacaoVO);
		model.addObject("exibeAlerta", Boolean.TRUE);
		return model;
	}
	
	
	
	
	/**
	 * Roteirizar servico autorizacao agendamento.
	 * 
	 */
	@RequestMapping("roteirizarServicoAutorizacaoAgendamento")
	public ModelAndView roteirizarServicoAutorizacaoAgendamento(
			@ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
			@RequestParam("chavesPrimarias") Long[] chaves, BindingResult result) throws GGASException, IOException {

		ModelAndView model = new ModelAndView(TELA_GRID_SERVICO_AUTORIZACAO_AGENDAMENTOS); 
		
		controladorServicoAutorizacao.roteirizarServicoAutorizacaoAgendamento(chaves, servicoAutorizacaoAgendaPesquisaVO, Boolean.FALSE, null);
		

		return model;
	}
	
	
	@RequestMapping("conferirPossibilidadeInclusaoAgendamentoChamado")
	@ResponseBody
	public Map<String, Object> conferirPossibilidadeInclusaoAgendamentoChamado(
	        @ModelAttribute("ServicoAutorizacaoAgendaPesquisaVO") ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
	        @RequestParam("chavesPrimarias") Long[] chaves, 
	        BindingResult result) throws GGASException, IOException {

	    Map<String, Object> response = new HashMap<>();
	    
		ConstanteSistema statusCancelado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_CANCELADO);

	    
	    Collection<ServicoAutorizacaoAgenda> listaAgendamentos = controladorServicoAutorizacao.obterListaAgendamentosPorDiaEquipe(DataUtil.converterParaData(servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada()), servicoAutorizacaoAgendaPesquisaVO.getEquipe(), statusCancelado.getValorLong());
	    
	    Long[] chaveAgendamento = Util.collectionParaArrayChavesPrimarias(listaAgendamentos);
	    
	    Collection<ServicoAutorizacao> listaServicoAutorizacao = controladorServicoAutorizacao.consultarServicoAutorizacaoPorChavesPrimarias(chaves);
	    
	    Collection<ServicoAutorizacaoAgenda> servicoAgendaSimulado = this.montarObjetoSimuladoAgenda(listaServicoAutorizacao, servicoAutorizacaoAgendaPesquisaVO.getTurno(), servicoAutorizacaoAgendaPesquisaVO.getDataSelecionada());
	    
	    Boolean isPossivelInclusao = controladorServicoAutorizacao.roteirizarServicoAutorizacaoAgendamento(chaveAgendamento, servicoAutorizacaoAgendaPesquisaVO, Boolean.TRUE,servicoAgendaSimulado);
	    
	    response.put("isPossivelInclusao", isPossivelInclusao);
	    
	    return response;
	}

	private Collection<ServicoAutorizacaoAgenda> montarObjetoSimuladoAgenda(
			Collection<ServicoAutorizacao> listaServicoAutorizacao, EntidadeConteudo turno, String dataSelecionada) throws NegocioException {
		Collection<ServicoAutorizacaoAgenda> retorno = new ArrayList<ServicoAutorizacaoAgenda>();
		Date dataSelecionadaConvertida = DataUtil.converterParaData(dataSelecionada);
		
		for(ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacao) {
			
			servicoAutorizacao.setTurnoPreferencia(turno);
			
			ServicoAutorizacaoAgenda agenda = new ServicoAutorizacaoAgenda();
			agenda.setServicoAutorizacao(servicoAutorizacao);
			agenda.setTurno(turno);
			agenda.setDataAgenda(dataSelecionadaConvertida);
			agenda.setIndicadorConfirmado(Boolean.FALSE);
			
			retorno.add(agenda);
		}
		
		return retorno;
	}
}
