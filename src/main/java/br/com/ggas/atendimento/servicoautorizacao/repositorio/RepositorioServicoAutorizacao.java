/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 11:12:53
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.repositorio;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.h2.expression.Alias;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.StringType;
import org.hibernate.type.TimeType;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.servicoautorizacao.dominio.FiltroServicoAutorizacaoLoteDTO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndicadorEmergenciaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndiceVazamentoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendasVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMaterial;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMotivoEncerramento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoRegistroLocal;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoRelatorioVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTempAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.impl.ServicoAutorizacaoRegistroLocalImpl;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.relatorio.RedeComprimentoVO;
import br.com.ggas.relatorio.ServicoAutorizacaoResumoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.atendimento.PesquisaRelatorioControleServicoAutorizacaoVO;

/**
 * Classe responsável pelo repositório de autorização de serviço
 */
@Repository
public class RepositorioServicoAutorizacao extends RepositorioGenerico {

	private static final String ALIAS_SERVICO_AUTORIZACAO = " servicoAutorizacao ";
	private static final String CONTRATO = "contrato";
	private static final String DATA_AGENDA = "dataAgenda";
	private static final String DATA_GERACAO = "dataGeracao";
	private static final String PONTO_CONSUMO = "pontoConsumo";
	private static final String CHAMADO = "chamado";
	private static final String EQUIPE = "equipe";
	private static final String STATUS = "status";
	private static final String SERVICO_AUTORIZACAO_MOTIVO_ENCERRAMENTO = "servicoAutorizacaoMotivoEncerramento";
	private static final int LIMITE_MIN_SEG = 59;
	private static final int LIMITE_HORAS = 23;
	private static final String CLIENTE = "cliente";
	private static final String IMOVEL = "imovel";

	private static final String IMOVEL_QUADRA_FACE = "imovel.quadraFace";
	private static final String SERVICO_TIPO ="servicoTipo";
	private static final String CHAMADO_ASSUNTO ="chamado.chamadoAssunto";
	private static final String SERVICO_TIPO_PRIORIDADE ="servicoTipoPrioridade";
	private static final String IMOVEL_CHAVE_PRIMERIA = "imovel.chavePrimaria";
	private static final String IMOVEL_CONDOMINIO = "imovel.condominio";
	private static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";
	private static final String FROM = " from ";
	private static final String WHERE = " where ";
	private static final String AUTORIZACAO =" autorizacao";
	private static final String SERVICO_TIPO_AGENDAMENTO_TURNO = " servicoTipoAgendamentoTurno ";
	private static final String USUARIO_SOLICITANTE_CHAVE_PRIMARIA = "usuarioSistema.chavePrimaria";
	/**
	 * Instantiates a new repositorio servico autorizacao.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioServicoAutorizacao(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ServicoAutorizacao();
	}

	@Override
	public Class<ServicoAutorizacao> getClasseEntidade() {

		return ServicoAutorizacao.class;
	}

	public Class<?> getClasseEntidadeServicoAutorizacaoHistorico() {

		return ServicoAutorizacaoHistorico.class;
	}

	public Class<?> getClasseEntidadeServicoAutorizacaoMotivoEncerramento() {

		return ServicoAutorizacaoMotivoEncerramento.class;
	}

	public Class<?> getClasseEntidadeServicoTipoAgendamentoTurno() {

		return ServicoTipoAgendamentoTurno.class;
	}

	public Class<?> getClasseEntidadeServicoAutorizacaoAgenda() {

		return ServicoAutorizacaoAgenda.class;
	}
	
	public Class<?> getClasseEntidadeEquipeTurnoItem() {

		return ServiceLocator.getInstancia().getClassPorID(EquipeTurnoItem.BEAN_ID_EQUIPE_TURNO_ITEM);
	}	

	public Class<?> getClasseEntidadeServicoAutorizacaoTemp() {

		return ServiceLocator.getInstancia().getClassPorID(ServicoAutorizacaoTemp.BEAN_ID_SERVICO_AUTORIZACAO_TEMP);
	}
	
	public Class<?> getClasseEntidadeServicoAutorizacaoTempAnexo() {
		return ServiceLocator.getInstancia().getClassPorID(ServicoAutorizacaoTempAnexo.BEAN_ID_SERVICO_AUTORIZACAO_TEMP_ANEXO);

	}
	
	public Class<?> getClasseEntidadeEmpresa(){
		return ServiceLocator.getInstancia().getClassPorID(Empresa.BEAN_ID_EMPRESA);
	}

	public Class<?> getClasseEntidadePontoConsumoCityGate() {
		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoCityGate.BEAN_ID_PONTO_CONSUMO_CITY_GATE);
	}
	
	public Class<?> getClasseEntidadeTronco() {
		return ServiceLocator.getInstancia().getClassPorID(Tronco.BEAN_ID_TRONCO);
	}
	
	public Class<?> getClasseEntidadeContratoPontoConsumo() {
		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}
	
	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {
		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}
	
	public Class<?> getClasseEntidadePontoConsumo() {
		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}
	
	public Class<?> getClasseEntidadeServicoAutorizacaoRegistroLocal() {
		return ServiceLocator.getInstancia().getClassPorID(ServicoAutorizacaoRegistroLocal.BEAN_ID_SERVICO_AUTORIZACAO_REGISTRO_LOCAL);
	}	
	public Class<?> getClasseEntidadeErp() {
		return ServiceLocator.getInstancia().getClassPorID(Erp.BEAN_ID_ERP);
	}	
	
	public Class<?> getClasseEntidadeServicoTipo() {
		return ServicoTipo.class;
	}	
	
	/**
	 * Consultar servico autorizacao.
	 * 
	 * @param servicoAutorizacaoVO {@link ServicoAutorizacaoVO}
	 *            the servico autorizacao vo
	 * @return the collection
	 * @throws FormatoInvalidoException {@link FormatoInvalidoException}
	 * @throws NegocioException 
	 */
	public Collection<ServicoAutorizacao> consultarServicoAutorizacao(ServicoAutorizacaoVO servicoAutorizacaoVO)
					throws FormatoInvalidoException, NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(CLIENTE, FetchMode.JOIN);
		Integer numeroContrato = servicoAutorizacaoVO.getNumeroContrato();
		if (numeroContrato != null && numeroContrato > 0) {
			criteria.setFetchMode(CONTRATO, FetchMode.JOIN);
			criteria.setFetchMode("contrato.listaContratoPontoConsumo", FetchMode.SELECT);
		}
		criteria.setFetchMode(IMOVEL, FetchMode.JOIN);
		criteria.setFetchMode(IMOVEL_QUADRA_FACE, FetchMode.JOIN);
		criteria.setFetchMode(STATUS, FetchMode.JOIN);
		criteria.setFetchMode(EQUIPE, FetchMode.JOIN);
		criteria.setFetchMode(SERVICO_TIPO, FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO, FetchMode.JOIN);
		criteria.setFetchMode("chamado.protocolo", FetchMode.JOIN);
		criteria.setFetchMode(CHAMADO_ASSUNTO, FetchMode.JOIN);
		criteria.setFetchMode("chamado.canalAtendimento", FetchMode.JOIN);
		criteria.setFetchMode("chamado.status", FetchMode.JOIN);
		criteria.setFetchMode("chamado.usuarioResponsavel", FetchMode.JOIN);
		criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);

		criteria.createAlias(CHAMADO, CHAMADO, Criteria.LEFT_JOIN);
		criteria.createAlias("chamado.protocolo", "protocolo", Criteria.LEFT_JOIN);
		criteria.createAlias(CHAMADO_ASSUNTO, "chamadoAssunto", Criteria.LEFT_JOIN);
		criteria.createAlias("chamado.canalAtendimento", "canalAtendimento", Criteria.LEFT_JOIN);
		criteria.createAlias("chamadoAssunto.chamadoTipo", "chamadoTipo", Criteria.LEFT_JOIN);
		criteria.createAlias("chamadoAssunto.unidadeOrganizacional", "unidadeOrganizacional", Criteria.LEFT_JOIN);
		criteria.createAlias("chamado.status", "statusChamado", Criteria.LEFT_JOIN);
		criteria.createAlias("chamado.usuarioResponsavel", "usuarioResponsavel", Criteria.LEFT_JOIN);
		criteria.createAlias("usuarioResponsavel.funcionario", "funcionario", Criteria.LEFT_JOIN);
		criteria.createAlias(IMOVEL_QUADRA_FACE, IMOVEL_QUADRA_FACE, Criteria.LEFT_JOIN);
		criteria.createAlias(SERVICO_AUTORIZACAO_MOTIVO_ENCERRAMENTO, SERVICO_AUTORIZACAO_MOTIVO_ENCERRAMENTO, Criteria.LEFT_JOIN);
		criteria.createAlias(IMOVEL, IMOVEL, Criteria.LEFT_JOIN);
		criteria.createAlias(SERVICO_TIPO, SERVICO_TIPO, Criteria.LEFT_JOIN);


		numeroContrato = servicoAutorizacaoVO.getNumeroContrato();
		if (numeroContrato != null && numeroContrato > 0) {
			criteria.createAlias(CONTRATO, CONTRATO, Criteria.LEFT_JOIN);
		}
		criteria.createAlias(CLIENTE, CLIENTE, Criteria.LEFT_JOIN);
		criteria.createAlias(EQUIPE, EQUIPE, Criteria.LEFT_JOIN);
		criteria.createAlias("servicoTipo.servicoTipoPrioridade", SERVICO_TIPO_PRIORIDADE, Criteria.LEFT_JOIN);
		criteria.createAlias(STATUS, STATUS, Criteria.LEFT_JOIN);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.cep", "cepPontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("cepPontoConsumo.municipio", "municipioPontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("servicoAutorizacaoTemp", "servicoAutorizacaoTemp",Criteria.LEFT_JOIN);
		criteria.createAlias("servicoAutorizacaoAgenda", "servicoAutorizacaoAgenda", Criteria.LEFT_JOIN);
		

		DateTime dataFim;

		if ((servicoAutorizacaoVO.getDataGeracaoInicio() != null && !servicoAutorizacaoVO.getDataGeracaoInicio().isEmpty())
						&& (servicoAutorizacaoVO.getDataGeracaoFim() != null && !servicoAutorizacaoVO.getDataGeracaoFim().isEmpty())) {

			Date dataGeracaoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataGeracaoInicio());
			Date dataGeracaoFim = DataUtil.parse(servicoAutorizacaoVO.getDataGeracaoFim());

			dataFim = new DateTime(dataGeracaoFim);
			dataFim = dataFim.withHourOfDay(LIMITE_HORAS);
			dataFim = dataFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataFim = dataFim.withSecondOfMinute(LIMITE_MIN_SEG);

			criteria.add(Restrictions.between(DATA_GERACAO, dataGeracaoInicio, dataFim.toDate()));
		}
		

		if ((servicoAutorizacaoVO.getDataPrevisaoInicio() != null && !servicoAutorizacaoVO.getDataPrevisaoInicio().isEmpty())
						&& (servicoAutorizacaoVO.getDataPrevisaoFim() != null && !servicoAutorizacaoVO.getDataPrevisaoFim().isEmpty())) {
			Date dataPrevisaoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataPrevisaoInicio());
			Date dataPrevisaoFim = DataUtil.parse(servicoAutorizacaoVO.getDataPrevisaoFim());

			dataFim = new DateTime(dataPrevisaoFim);
			dataFim = dataFim.withHourOfDay(LIMITE_HORAS);
			dataFim = dataFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataFim = dataFim.withSecondOfMinute(LIMITE_MIN_SEG);

			criteria.add(Restrictions.between("dataPrevisaoEncerramento", dataPrevisaoInicio, dataFim.toDate()));
		}

		if ((servicoAutorizacaoVO.getDataEncerramentoInicio() != null && !servicoAutorizacaoVO.getDataEncerramentoInicio().isEmpty())
						&& (servicoAutorizacaoVO.getDataEncerramentoFim() != null && !servicoAutorizacaoVO.getDataEncerramentoFim()
										.isEmpty())) {
			Date dataEncerramentoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataEncerramentoInicio());
			Date dataEncerramentoFim = DataUtil.parse(servicoAutorizacaoVO.getDataEncerramentoFim());

			dataFim = new DateTime(dataEncerramentoFim);
			dataFim = dataFim.withHourOfDay(LIMITE_HORAS);
			dataFim = dataFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataFim = dataFim.withSecondOfMinute(LIMITE_MIN_SEG);

			criteria.add(Restrictions.between("dataEncerramento", dataEncerramentoInicio, dataFim.toDate()));
		}

		if ((servicoAutorizacaoVO.getDataExecucaoInicio() != null && !servicoAutorizacaoVO.getDataExecucaoInicio().isEmpty())
				|| (servicoAutorizacaoVO.getDataExecucaoFim() != null && !servicoAutorizacaoVO.getDataExecucaoFim().isEmpty())) {

			Date dataExecucaoInicio = new Date(0);
			Date dataExecucaoFim = new Date();

			if (servicoAutorizacaoVO.getDataExecucaoInicio() != null && !servicoAutorizacaoVO.getDataExecucaoInicio().isEmpty()) {
				dataExecucaoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataExecucaoInicio());
			}
			if (servicoAutorizacaoVO.getDataExecucaoFim() != null && !servicoAutorizacaoVO.getDataExecucaoFim().isEmpty()) {
				dataExecucaoFim = DataUtil.parse(servicoAutorizacaoVO.getDataExecucaoFim());
			}

			dataFim = new DateTime(dataExecucaoFim);
			dataFim = dataFim.withHourOfDay(LIMITE_HORAS);
			dataFim = dataFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataFim = dataFim.withSecondOfMinute(LIMITE_MIN_SEG);

			criteria.add(Restrictions.between("dataExecucao", dataExecucaoInicio, dataFim.toDate()));
		}

		Long protocolo = servicoAutorizacaoVO.getNumeroProtocoloChamado();
		if (protocolo != null && protocolo > 0) {
			criteria.add(Restrictions.eq("protocolo.numeroProtocolo", protocolo));
		}
		
		Long numeroOS = servicoAutorizacaoVO.getNumeroOS();
		if (numeroOS != null && numeroOS > 0) {
			criteria.add(Restrictions.eq("numeroOS", numeroOS));
		}

		Equipe equipe = servicoAutorizacaoVO.getEquipe();
		if (equipe != null) {
			criteria.add(Restrictions.eq("equipe.chavePrimaria", equipe.getChavePrimaria()));
		}

		if (servicoAutorizacaoVO.getUsuarioResponsavelChamado() != null
						&& servicoAutorizacaoVO.getUsuarioResponsavelChamado().getFuncionario() != null) {
			criteria.add(Restrictions.eq("funcionario.chavePrimaria", servicoAutorizacaoVO.getUsuarioResponsavelChamado().getFuncionario()
							.getChavePrimaria()));
		}

		if (servicoAutorizacaoVO.getServicosTipos() != null && !servicoAutorizacaoVO.getServicosTipos().isEmpty()) {
			criteria.add(Restrictions.in(SERVICO_TIPO, servicoAutorizacaoVO.getServicosTipos()));
		} else {
			ServicoTipo servicoTipo = servicoAutorizacaoVO.getServicoTipo();
			if (servicoTipo != null && servicoTipo.getChavePrimaria() > 0) {
				criteria.add(Restrictions.eq("servicoTipo.chavePrimaria", servicoTipo.getChavePrimaria()));
			}
		}
		
		if(servicoAutorizacaoVO.getPodeSerAgendado() != null && servicoAutorizacaoVO.getPodeSerAgendado()) {
			criteria.add(Restrictions.eq("servicoTipo.indicadorAgendamento", servicoAutorizacaoVO.getPodeSerAgendado()));
		}

		ServicoTipoPrioridade servicoTipoPrioridade = servicoAutorizacaoVO.getServicoTipoPrioridade();
		if (servicoTipoPrioridade != null) {
			criteria.add(Restrictions.eq("servicoTipoPrioridade.chavePrimaria", servicoTipoPrioridade.getChavePrimaria()));
		}

		List<String> listaStatus = servicoAutorizacaoVO.getListaStatus();
		if (listaStatus != null && !listaStatus.isEmpty() && !listaStatus.contains("Todos")) {
			Junction juncao = Restrictions.disjunction();
			for (String status : listaStatus) {
				juncao.add(Restrictions.ilike("status.descricao", Util.removerAcentuacao(status), MatchMode.EXACT));
			}
			criteria.add(juncao);
		}

		Long chavePrimaria = servicoAutorizacaoVO.getChavePrimaria();
		if (chavePrimaria != null && chavePrimaria > 0) {
			criteria.add(Restrictions.eq(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		}

		if (servicoAutorizacaoVO.getIndicadorAssociado() != null) {
			if (servicoAutorizacaoVO.getIndicadorAssociado()) {
				criteria.add(Restrictions.isNotNull(CLIENTE));
			} else {
				criteria.add(Restrictions.isNull(CLIENTE));
			}
		}
		if (servicoAutorizacaoVO.getEncerradoAposPrazo() != null) {
			if (servicoAutorizacaoVO.getEncerradoAposPrazo()) {
				criteria.add(Restrictions.sqlRestriction(" Trunc(seau_dt_encerramento) >  Trunc(seau_dt_previsao_encerramento) "));
			} else {
				criteria.add(Restrictions.sqlRestriction(" Trunc(seau_dt_encerramento) <=  Trunc(seau_dt_previsao_encerramento) "));
			}
		}

		// IMOVEL

		if (!(servicoAutorizacaoVO.getIdsCondominioImovel() != null && !servicoAutorizacaoVO.getIdsCondominioImovel().isEmpty())) {
			String cepImovel = servicoAutorizacaoVO.getCepImovel();
			if (cepImovel != null && !StringUtils.isEmpty(cepImovel)) {
				criteria.createCriteria("imovel.quadraFace.endereco").createCriteria("cep").add(Restrictions.eq("cep", cepImovel));
			}
			String numeroImovel = servicoAutorizacaoVO.getNumeroImovel();
			if (numeroImovel != null && !StringUtils.isEmpty(numeroImovel)) {
				criteria.add(Restrictions.eq("imovel.numeroImovel", numeroImovel));
			}
			String descricaoComplemento = servicoAutorizacaoVO.getDescricaoComplemento();
			if (descricaoComplemento != null && !StringUtils.isEmpty(descricaoComplemento)) {
				criteria.add(Restrictions.ilike("imovel.descricaoComplemento", Util.formatarTextoConsulta(descricaoComplemento)));
			}
			String nomeImovel = servicoAutorizacaoVO.getNomeImovel();
			if (nomeImovel != null && !StringUtils.isEmpty(nomeImovel)) {
				criteria.add(Restrictions.ilike("imovel.nome", Util.formatarTextoConsulta(nomeImovel)));
			}
			Long matriculaImovel = servicoAutorizacaoVO.getMatriculaImovel();
			if (matriculaImovel != null) {
				criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMERIA, matriculaImovel));
			}
			Boolean condominioImovel = servicoAutorizacaoVO.getCondominioImovel();
			if (condominioImovel != null) {
				criteria.add(Restrictions.eq(IMOVEL_CONDOMINIO, condominioImovel));
			}
		} else {
			Long matriculaImovel = servicoAutorizacaoVO.getMatriculaImovel();
			List<Long> idsImovelPai = servicoAutorizacaoVO.getIdsCondominioImovel();
			if (matriculaImovel != null) {
				Criterion rest1 = Restrictions.eq(IMOVEL_CHAVE_PRIMERIA, matriculaImovel);
				Criterion rest2 = Restrictions.in("imovel.imovelCondominio.chavePrimaria", idsImovelPai);
				criteria.add(Restrictions.or(rest1, rest2));
			}
		}
		
		// PONTO CONSUMO
		String cepPontoConsumo = servicoAutorizacaoVO.getCepPontoConsumo();
		if(StringUtils.isNotBlank(cepPontoConsumo)) {
			criteria.add(Restrictions.eq("cepPontoConsumo.cep", cepPontoConsumo));
		}

		String siglaUnidadeFederacaoPontoConsumo = servicoAutorizacaoVO.getSiglaUnidadeFederacaoPontoConsumo();
		if(StringUtils.isNotBlank(siglaUnidadeFederacaoPontoConsumo)) {
			criteria.add(Restrictions.ilike("cepPontoConsumo.uf", siglaUnidadeFederacaoPontoConsumo));
		}
				
		String nomeMunicipioPontoConsumo = servicoAutorizacaoVO.getNomeMunicipioPontoConsumo();
		if(StringUtils.isNotBlank(nomeMunicipioPontoConsumo)) {
			criteria.add(Restrictions.ilike("municipioPontoConsumo.descricao", nomeMunicipioPontoConsumo));
		}
		
		String lougradoroPontoConsumo = servicoAutorizacaoVO.getLougradoroPontoConsumo();
		if(StringUtils.isNotBlank(lougradoroPontoConsumo)) {
			criteria.add(Restrictions.ilike("cepPontoConsumo.logradouro", Util.formatarTextoConsulta(lougradoroPontoConsumo)));
		}
		
		String bairroPontoConsumo = servicoAutorizacaoVO.getBairroPontoConsumo();
		if(StringUtils.isNotBlank(bairroPontoConsumo)) {
			criteria.add(Restrictions.ilike("cepPontoConsumo.bairro", bairroPontoConsumo));
		}

		numeroContrato = servicoAutorizacaoVO.getNumeroContrato();
		if (numeroContrato != null && numeroContrato > 0) {
			criteria.add(Restrictions.eq("contrato.numero", numeroContrato));
		}

		String cpf = servicoAutorizacaoVO.getCpfCliente();
		if (cpf != null && !StringUtils.isEmpty(cpf)) {
			criteria.add(Restrictions.eq("cliente.cpf", Util.removerCaracteresEspeciais(cpf)));
		}
		String rg = servicoAutorizacaoVO.getRgCliente();
		if (rg != null && !StringUtils.isEmpty(rg)) {
			criteria.add(Restrictions.eq("cliente.rg", rg));
		}
		String nome = servicoAutorizacaoVO.getNomeCliente();
		if (nome != null && !StringUtils.isEmpty(nome)) {
			criteria.add(Restrictions.ilike("cliente.nome", Util.formatarTextoConsulta(nome)));
		}
		String passaporte = servicoAutorizacaoVO.getNumeroPassaporte();
		if (passaporte != null && !StringUtils.isEmpty(passaporte)) {
			criteria.add(Restrictions.eq("cliente.numeroPassaporte", passaporte));
		}
		String cnpj = servicoAutorizacaoVO.getCnpjCliente();
		if (cnpj != null && !StringUtils.isEmpty(cnpj)) {
			criteria.add(Restrictions.eq("cliente.cnpj", Util.removerCaracteresEspeciais(cnpj)));
		}
		String nomeFantasia = servicoAutorizacaoVO.getNomeFantasiaCliente();
		if (nomeFantasia != null && !StringUtils.isEmpty(nomeFantasia)) {
			criteria.add(Restrictions.ilike("cliente.nomeFantasia", Util.formatarTextoConsulta(nomeFantasia)));
		}

		if (servicoAutorizacaoVO.getServicoAtraso() != null) {
			if (servicoAutorizacaoVO.getServicoAtraso()) {
				criteria.add(Restrictions.sqlRestriction(" Trunc(seau_dt_previsao_encerramento) < '"
								+ DataUtil.converterDataParaString(Calendar.getInstance().getTime()) + "'"));
			} else {
				criteria.add(Restrictions.or(Restrictions.ge("dataPrevisaoEncerramento", Calendar.getInstance().getTime()),
								Restrictions.sqlRestriction(" Trunc(seau_dt_encerramento) <=  Trunc(seau_dt_previsao_encerramento)")));
			}
		}

		if (servicoAutorizacaoVO.getPontoConsumo() != null) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, servicoAutorizacaoVO.getPontoConsumo().getChavePrimaria()));
		}

		if (servicoAutorizacaoVO.getServicoAgendado() != null && !servicoAutorizacaoVO.getServicoAgendado()) {
			
			Date dataSelecionada = StringUtils.isEmpty(servicoAutorizacaoVO.getDataSelecionada()) ? null
					: DataUtil.converterParaData(servicoAutorizacaoVO.getDataSelecionada());
			try {
				dataSelecionada = StringUtils.isEmpty(servicoAutorizacaoVO.getDataSelecionada()) ? null
						: DataUtil.converterParaData(servicoAutorizacaoVO.getDataSelecionada());
			} catch (NegocioException e) {
				e.printStackTrace();
			}
			
			Long[] array = Util.collectionParaArrayChavesPrimarias(obterServicoAutorizacaoDoAgendamento(consultarServicoAutorizacaoAgenda(
							servicoAutorizacaoVO.getServicoTipo().getChavePrimaria(),  dataSelecionada ,null, null, Boolean.TRUE)));
			if (array.length > 0) {
				Criterion in = Restrictions.in(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, array);
				criteria.add(Restrictions.not(in));
			}
		}

		// chamado
		if (servicoAutorizacaoVO.getNomeSolicitanteChamado() != null && !servicoAutorizacaoVO.getNomeSolicitanteChamado().isEmpty()) {
			criteria.add(Restrictions.eq("chamado.nomeSolicitante", servicoAutorizacaoVO.getNomeSolicitanteChamado()));
		}
		if (servicoAutorizacaoVO.getCpfCnpjSolicitanteChamado() != null && !servicoAutorizacaoVO.getCpfCnpjSolicitanteChamado().isEmpty()) {
			criteria.add(Restrictions.eq("chamado.cpfCnpjSolicitante", servicoAutorizacaoVO.getCpfCnpjSolicitanteChamado()));
		}
		if (servicoAutorizacaoVO.getChamadoAssuntos() != null && !servicoAutorizacaoVO.getChamadoAssuntos().isEmpty()) {
			criteria.add(Restrictions.in(CHAMADO_ASSUNTO, servicoAutorizacaoVO.getChamadoAssuntos()));
		}
		if (servicoAutorizacaoVO.getIdChamadoTipo() != null && servicoAutorizacaoVO.getIdChamadoTipo() > 0) {
			criteria.add(Restrictions.eq("chamadoTipo.chavePrimaria", servicoAutorizacaoVO.getIdChamadoTipo()));
		}
		if (servicoAutorizacaoVO.getStatusChamado() != null) {
			criteria.add(Restrictions.eq("statusChamado.chavePrimaria", servicoAutorizacaoVO.getStatusChamado().getChavePrimaria()));
		}
		if (servicoAutorizacaoVO.getCanalAtendimento() != null) {
			criteria.add(Restrictions.eq("canalAtendimento.chavePrimaria", servicoAutorizacaoVO.getCanalAtendimento().getChavePrimaria()));
		}
		if (servicoAutorizacaoVO.getUnidadeOrganizacionalChamado() != null) {
			criteria.add(Restrictions.eq("unidadeOrganizacional.chavePrimaria", servicoAutorizacaoVO.getUnidadeOrganizacionalChamado()
							.getChavePrimaria()));
		}
		if (servicoAutorizacaoVO.getIndicadorSucessoExecucao() != null) {
			criteria.add(Restrictions.eq("indicadorSucessoExecucao", servicoAutorizacaoVO.getIndicadorSucessoExecucao()));
		}
		if (servicoAutorizacaoVO.getServicoAutorizacaoMotivoEncerramento() != null) {
			criteria.add(Restrictions.eq(SERVICO_AUTORIZACAO_MOTIVO_ENCERRAMENTO,
					servicoAutorizacaoVO.getServicoAutorizacaoMotivoEncerramento()));
		}
		if (servicoAutorizacaoVO.getSolicitante() != null) {
			criteria.add(Restrictions.eq(USUARIO_SOLICITANTE_CHAVE_PRIMARIA,
					servicoAutorizacaoVO.getSolicitante().getChavePrimaria()));
		}
		if (servicoAutorizacaoVO.getChamadoAssunto() != null && servicoAutorizacaoVO.getChamadoAssunto() > 0) {
			criteria.add(Restrictions.eq("chamadoAssunto.chavePrimaria", servicoAutorizacaoVO.getChamadoAssunto()));
		}
		if (servicoAutorizacaoVO.getIdTipoChamado() != null && servicoAutorizacaoVO.getIdTipoChamado() > 0) {
			criteria.add(Restrictions.eq("chamadoTipo.chavePrimaria", servicoAutorizacaoVO.getIdTipoChamado()));
		}
		if(servicoAutorizacaoVO.getIsAgendamento() != null) {
			if(servicoAutorizacaoVO.getIsAgendamento()) {
				criteria.add(Restrictions.isNull("servicoAutorizacaoAgenda.servicoAutorizacao.chavePrimaria"));
			}
		}
		
		criteria.addOrder(Order.desc(DATA_GERACAO));

		return criteria.list();
	}

	/**
	 * Listar servico autorizacao.
	 * 
	 * @return the collection
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<ServicoAutorizacao> listarServicoAutorizacao() {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(AUTORIZACAO);
		hql.append(" left join fetch autorizacao.servicoTipo");
		hql.append(" left join fetch autorizacao.equipe");
		hql.append(" left join fetch autorizacao.chamado");
		hql.append(" left join fetch autorizacao.cliente");
		hql.append(" left join fetch autorizacao.imovel");
		hql.append(" left join fetch autorizacao.contrato");
		hql.append(" left join fetch autorizacao.pontoConsumo");
		hql.append(" left join fetch autorizacao.servicoAutorizacaoMotivoEncerramento");
		hql.append(" left join fetch autorizacao.usuarioSistema");
		hql.append(" left join fetch autorizacao.status");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Obter servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico autorizacao
	 */
	@SuppressWarnings("squid:S1192")
	public ServicoAutorizacao obterServicoAutorizacao(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(AUTORIZACAO);
		hql.append(" left join fetch autorizacao.servicoTipo servicoTipo");
		hql.append(" left join fetch autorizacao.turnoPreferencia turnoPreferencia");
		hql.append(" left join fetch autorizacao.servicoTipoPrioridade");
		hql.append(" left join fetch autorizacao.equipe");
		hql.append(" left join fetch autorizacao.chamado");
		hql.append(" left join fetch autorizacao.chamado.chamadoAssunto");
		hql.append(" left join fetch autorizacao.chamado.chamadoAssunto.chamadoTipo");
		hql.append(" left join fetch autorizacao.cliente");
		hql.append(" left join fetch autorizacao.executante");
		hql.append(" left join fetch autorizacao.imovel");
		hql.append(" left join fetch autorizacao.contrato");
		hql.append(" left join fetch autorizacao.pontoConsumo");
		hql.append(" left join fetch autorizacao.servicoAutorizacaoMotivoEncerramento");
		hql.append(" left join fetch autorizacao.usuarioSistema");
		hql.append(" left join fetch autorizacao.status");
		hql.append(" left join fetch autorizacao.medidor");
		
		hql.append(WHERE);
		hql.append(" autorizacao.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return (ServicoAutorizacao) query.uniqueResult();
	}

	/**
	 * Obter servico autorizacao.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return the list
	 */
	@SuppressWarnings("squid:S1192")
	public List<ServicoAutorizacao> obterServicoAutorizacao(Long[] chavesPrimarias) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(AUTORIZACAO);
		hql.append(" left join fetch autorizacao.status");

		hql.append(WHERE);
		hql.append(" autorizacao.chavePrimaria in (:chavePrimaria) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, Arrays.asList(chavesPrimarias));

		return query.list();
	}

	/**
	 * Listar servico autorizacao historico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<ServicoAutorizacaoHistorico> listarServicoAutorizacaoHistorico(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeServicoAutorizacaoHistorico().getSimpleName());
		hql.append(" historico ");
		hql.append(" left join fetch historico.usuarioSistema ");
		hql.append(" left join fetch historico.equipe ");
		hql.append(" left join fetch historico.status ");
		hql.append(" left join fetch historico.operacao ");
		hql.append(" left join fetch historico.servicoAutorizacao servicoAutorizacao");
		hql.append(" left join fetch servicoAutorizacao.usuarioSistema usuarioSistema");
		hql.append(" left join fetch usuarioSistema.funcionario funcionario");
		hql.append(" left join fetch historico.servicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento");
		hql.append(WHERE);
		hql.append(" servicoAutorizacao.chavePrimaria = :chavePrimaria ");
		hql.append(" order by historico.ultimaAlteracao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return query.list();
	}

	/**
	 * Consultar servico autorizacao por cliente imovel ponto consumo.
	 * 
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @return the collection
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorClienteImovelPontoConsumo(Long chavePrimariaCliente,
					Long chavePrimariaImovel, Long chavePrimariaPontoConsumo) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("unidadeOrganizacional", FetchMode.JOIN);
		criteria.setFetchMode("canalAtendimento", FetchMode.JOIN);
		criteria.setFetchMode("chamadoAssunto", FetchMode.JOIN);
		criteria.setFetchMode("chamadoAssunto.chamadoTipo", FetchMode.JOIN);
		criteria.setFetchMode("protocolo", FetchMode.JOIN);
		criteria.setFetchMode(STATUS, FetchMode.JOIN);

		if (chavePrimariaCliente != null) {
			criteria.add(Restrictions.eq("cliente.chavePrimaria", chavePrimariaCliente));
			criteria.setFetchMode(CLIENTE, FetchMode.JOIN);
			criteria.createAlias(CLIENTE, CLIENTE);
		} else if (chavePrimariaImovel != null) {
			criteria.setFetchMode(IMOVEL, FetchMode.JOIN);
			criteria.createAlias(IMOVEL, IMOVEL);
			criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMERIA, chavePrimariaImovel));
		} else if (chavePrimariaPontoConsumo != null) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, chavePrimariaPontoConsumo));
			criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);
			criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
		}

		return criteria.list();
	}

	/**
	 * Listar equipe servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<ServicoAutorizacao> listarEquipeServicoAutorizacao(long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(AUTORIZACAO);
		hql.append(" left join fetch autorizacao.equipe equipe");
		hql.append(WHERE);
		hql.append(" equipe.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return query.list();
	}

	/**
	 * Obter servico autorizacao motivo encerramento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	public Collection<ServicoAutorizacaoMotivoEncerramento> obterServicoAutorizacaoMotivoEncerramento(long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeServicoAutorizacaoMotivoEncerramento().getSimpleName());
		hql.append(" motivo");
		hql.append(WHERE);
		hql.append(" movivo.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return query.list();
	}

	/**
	 * Obter equipe servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the boolean
	 */
	public Boolean obterEquipeServicoAutorizacao(Long chavePrimaria) {

		Boolean existe = false;

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(AUTORIZACAO);
		hql.append(" inner join fetch autorizacao.equipe equipe");

		hql.append(WHERE);
		hql.append(" equipe.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		if (!query.list().isEmpty()) {
			existe = true;
		}

		return existe;
	}

	/**
	 * Consultar agendamentos.
	 * 
	 * @param codigoServicoTipo
	 *            the codigo servico tipo
	 * @param dataAgenda
	 *            the data agenda
	 * @return the list
	 */
	public List<ServicoAutorizacaoAgendaVO> consultarAgendamentos(Long codigoServicoTipo, Date dataAgenda) throws NumberFormatException, GGASException {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT to_char(SEAA_DT_AGENDA, 'dd/MM/yyyy'), ENCO_CD_TURNO, count(*) ");
		hql.append(" FROM SERVICO_AUTORIZACAO_AGENDA SEAA");
		hql.append(" INNER JOIN SERVICO_AUTORIZACAO SEAU ON SEAU.SEAU_CD = SEAA.SEAU_CD ");
		hql.append(" WHERE ");
		hql.append(" 1 = 1 ");
		if(codigoServicoTipo != null && codigoServicoTipo > 0) {
			hql.append(" AND SEAU.SRTI_CD = :servicoTipo ");
		}
		
		hql.append(" AND TO_CHAR(SEAA.SEAA_DT_AGENDA, 'MM/yyyy') = TO_CHAR(:dataAgenda, 'MM/yyyy') ");
		hql.append(" GROUP BY to_char(SEAA_DT_AGENDA, 'dd/MM/yyyy'), ENCO_CD_TURNO ");
		hql.append(" ORDER BY to_char(SEAA_DT_AGENDA, 'dd/MM/yyyy') asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		
		if(codigoServicoTipo != null && codigoServicoTipo > 0) {
			query.setParameter(SERVICO_TIPO, codigoServicoTipo);
		}
		
		query.setParameter(DATA_AGENDA, dataAgenda);
		
		List<Object> resultado = query.list();
		
		
		List<ServicoAutorizacaoAgendaVO> listaServicoAutorizacaoAgenda = new ArrayList<ServicoAutorizacaoAgendaVO>();
		
		for(Object retorno : resultado) {
			Object[] linha = (Object[]) retorno;
			
			Date dataAgendada = DataUtil.converterParaData(linha[0].toString());
			EntidadeConteudo turno = Fachada.getInstancia().obterEntidadeConteudo(Long.valueOf(linha[1].toString()));
			Long quantidadeTurnoData = Long.valueOf(linha[2].toString());
			
			
			ServicoAutorizacaoAgendaVO servicoAutorizacaoAgendaVO = !listaServicoAutorizacaoAgenda.isEmpty()
					? listaServicoAutorizacaoAgenda.stream().filter(p -> Util.compararDatas(p.getData(), dataAgendada) == 0).findAny().orElse(null)
					: null;
			
			if(servicoAutorizacaoAgendaVO == null ){
				servicoAutorizacaoAgendaVO = new ServicoAutorizacaoAgendaVO();
				servicoAutorizacaoAgendaVO.setData(dataAgendada);
				listaServicoAutorizacaoAgenda.add(servicoAutorizacaoAgendaVO);
			}
			
			if(turno.getDescricao().equals("MANHÃ")) {
				servicoAutorizacaoAgendaVO.setTurnoManha(quantidadeTurnoData);
			} else if (turno.getDescricao().equals("TARDE")) {
				servicoAutorizacaoAgendaVO.setTurnoTarde(quantidadeTurnoData);
			} else if (turno.getDescricao().equals("NOITE")) {
				servicoAutorizacaoAgendaVO.setTurnoNoite(quantidadeTurnoData);
			}
			
		}
		
		return listaServicoAutorizacaoAgenda;

	}

	/**
	 * Consultar total agendamentos.
	 * 
	 * @param codigoServicoTipo
	 *            the codigo servico tipo
	 * @return the long
	 */
	public Long consultarTotalAgendamentos(Long codigoServicoTipo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT SUM(servicoTipoAgendamentoTurno.quantidadeAgendamentosTurno) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeServicoTipoAgendamentoTurno().getSimpleName());
		hql.append(SERVICO_TIPO_AGENDAMENTO_TURNO);
		
		hql.append(" where 1 = 1 " );
		if(codigoServicoTipo != null && codigoServicoTipo > 0) {
			hql.append(" and servicoTipoAgendamentoTurno.servicoTipo.chavePrimaria = :servicoTipo ");
			hql.append(" group by servicoTipoAgendamentoTurno.servicoTipo  ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(codigoServicoTipo != null && codigoServicoTipo > 0) {
			query.setParameter(SERVICO_TIPO, codigoServicoTipo);
		}
		
		return (Long) query.uniqueResult();

	}

	/**
	 * Consultar servico autorizacao agenda.
	 * 
	 * @param codigoServicoTipo {@link Long}
	 * @param dataAgenda {@link Date}
	 * @param status {@link List}
	 * @return Resultado da consulta - List
	 */
	public List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgenda(Long codigoServicoTipo, Date dataAgenda,
			List<Long> status, Long chaveEquipe, Boolean isConsideraAberta) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAutorizacaoAgenda ");
		hql.append(" left join fetch servicoAutorizacaoAgenda.servicoAutorizacao.cliente cliente ");
		hql.append(" left join fetch  servicoAutorizacaoAgenda.servicoAutorizacao.imovel imovel");
		hql.append(" left join fetch  imovel.quadraFace quadraFace");
		hql.append(" left join fetch  quadraFace.endereco");
		hql.append(" left join fetch  servicoAutorizacaoAgenda.servicoAutorizacao.servicoTipo servicoTipo");
		hql.append(" where 1 = 1 ");
		
		if (codigoServicoTipo!= null && codigoServicoTipo.compareTo(-1l) != 0) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.servicoTipo.chavePrimaria = :servicoTipo ");
		}
		
		if (dataAgenda != null) {
			hql.append(" and to_char(servicoAutorizacaoAgenda.dataAgenda, 'dd/MM/yyyy') = to_char(:dataAgenda,'dd/MM/yyyy') ");
		} else {
			hql.append(" and servicoAutorizacaoAgenda.dataAgenda >= :dataAgenda ");
		}
		if (status!=null && !status.isEmpty()) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria in(:status) ");
		}
		
		
		if(chaveEquipe != null && chaveEquipe.compareTo(-1l) != 0) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.equipe.chavePrimaria = :chaveEquipe ");
		}
		
		if(isConsideraAberta) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.status.descricao = 'ABERTA' ");
		}
		
		hql.append(" order by servicoAutorizacaoAgenda.dataAgenda asc, servicoAutorizacaoAgenda.turno asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoServicoTipo!= null && codigoServicoTipo.compareTo(-1l) != 0) {
			query.setParameter(SERVICO_TIPO, codigoServicoTipo);
		}
		if (dataAgenda != null) {
			query.setParameter(DATA_AGENDA, dataAgenda);
		} else {
			query.setParameter(DATA_AGENDA, Calendar.getInstance().getTime());
		}
		if (status!=null && !status.isEmpty()) {
			query.setParameterList(STATUS, status);
		}
		
		if(chaveEquipe != null && chaveEquipe.compareTo(-1l) != 0) {
			query.setParameter("chaveEquipe", chaveEquipe);
		}
		
		return query.list();
	}

	public List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgenda(Long codigoServicoTipo, Date dataAgenda,
			List<Long> status, Long chaveEquipe, Boolean isConsideraAberta, List<Long> chavesPrimarias) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAutorizacaoAgenda ");
		hql.append(" left join fetch servicoAutorizacaoAgenda.servicoAutorizacao.cliente cliente ");
		hql.append(" left join fetch  servicoAutorizacaoAgenda.servicoAutorizacao.imovel imovel");
		hql.append(" left join fetch  imovel.quadraFace quadraFace");
		hql.append(" left join fetch  quadraFace.endereco");
		hql.append(" left join fetch  servicoAutorizacaoAgenda.servicoAutorizacao.servicoTipo servicoTipo");
		hql.append(" where 1 = 1 ");
		
		if (codigoServicoTipo!= null && codigoServicoTipo.compareTo(-1l) != 0) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.servicoTipo.chavePrimaria = :servicoTipo ");
		}
		
		if (dataAgenda != null) {
			hql.append(" and to_char(servicoAutorizacaoAgenda.dataAgenda, 'dd/MM/yyyy') = to_char(:dataAgenda,'dd/MM/yyyy') ");
		} else {
			hql.append(" and servicoAutorizacaoAgenda.dataAgenda >= :dataAgenda ");
		}
		if (status!=null && !status.isEmpty()) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.status.chavePrimaria in(:status) ");
		}
		
		
		if(chaveEquipe != null && chaveEquipe.compareTo(-1l) != 0) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.equipe.chavePrimaria = :chaveEquipe ");
		}
		
		if(isConsideraAberta) {
			hql.append(" and servicoAutorizacaoAgenda.servicoAutorizacao.status.descricao = 'ABERTA' ");
		}
		
		if(chavesPrimarias != null) {
			if(chavesPrimarias.size() > 0) {
				hql.append( " and servicoAutorizacaoAgenda.chavePrimaria IN (:chavesPrimarias)");
			}
		}
		
		hql.append(" order by servicoAutorizacaoAgenda.dataAgenda asc, servicoAutorizacaoAgenda.turno asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoServicoTipo!= null && codigoServicoTipo.compareTo(-1l) != 0) {
			query.setParameter(SERVICO_TIPO, codigoServicoTipo);
		}
		if (dataAgenda != null) {
			query.setParameter(DATA_AGENDA, dataAgenda);
		} else {
			query.setParameter(DATA_AGENDA, Calendar.getInstance().getTime());
		}
		if (status!=null && !status.isEmpty()) {
			query.setParameterList(STATUS, status);
		}
		
		if(chaveEquipe != null && chaveEquipe.compareTo(-1l) != 0) {
			query.setParameter("chaveEquipe", chaveEquipe);
		}
		
		if(chavesPrimarias != null) {
			if(chavesPrimarias.size() > 0) {
				query.setParameterList("chavesPrimarias", chavesPrimarias);
			}
		}
		
		return query.list();
	}
	/**
	 * Obter servico autorizacao do agendamento.
	 * 
	 * @param agendamentos
	 *            the agendamentos
	 * @return the list
	 */
	private List<ServicoAutorizacao> obterServicoAutorizacaoDoAgendamento(List<ServicoAutorizacaoAgenda> agendamentos) {

		List<ServicoAutorizacao> servicoAutorizacaoList = new ArrayList<>();
		for (ServicoAutorizacaoAgenda agenda : agendamentos) {
			servicoAutorizacaoList.add(agenda.getServicoAutorizacao());
		}
		return servicoAutorizacaoList;
	}

	/**
	 * Listar servico tipo autorizacao servico.
	 * 
	 * @return the list
	 */
	public List<Long> listarServicoTipoAutorizacaoServico() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct servicoAutorizacao.servicoTipo.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servicoAutorizacao");
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Consultar agendamentos disponiveis turno.
	 * 
	 * @param tipo
	 *            the tipo
	 * @param dataAgenda
	 *            the data agenda
	 * @return the list
	 */
	public List<ServicoTipoAgendamentoTurno> consultarAgendamentosDisponiveisTurno(ServicoTipo tipo, Date dataAgenda) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select servico_autorizacao_agenda.enco_cd_turno, servico_tipo_agendamento_turno.seat_qn - COUNT(*) ");
		hql.append(" FROM SERVICO_AUTORIZACAO_AGENDA ");
		hql.append(" INNER JOIN SERVICO_AUTORIZACAO ON SERVICO_AUTORIZACAO_AGENDA.SEAU_CD = SERVICO_AUTORIZACAO.SEAU_CD ");
		hql.append(" inner join servico_tipo_agendamento_turno on servico_tipo_Agendamento_turno.enco_cd_turno = "
						+ "servico_autorizacao_agenda.enco_cd_turno ");
		hql.append(" WHERE SERVICO_AUTORIZACAO.SRTI_CD = :servicoTipo and servico_tipo_agendamento_turno.srti_cd = :servicoTipo");
		hql.append(" and to_char(servico_autorizacao_agenda.seaa_dt_agenda,'dd/MM/yyyy') = to_char(:dataAgenda,'dd/MM/yyyy')  ");
		hql.append(" group by servico_autorizacao_agenda.enco_cd_turno, servico_tipo_agendamento_turno.seat_qn ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());

		query.setParameter(SERVICO_TIPO, tipo.getChavePrimaria());
		query.setParameter(DATA_AGENDA, dataAgenda);

		Map<Long, ServicoTipoAgendamentoTurno> turnosDisponiveisMap = new HashMap<>();
		List<ServicoTipoAgendamentoTurno> turnosDisponiveis = new ArrayList<>();

		for (Object obj : query.list()) {
			Object[] resultadoArray = (Object[]) obj;
			ServicoTipoAgendamentoTurno agendamentoTurno = new ServicoTipoAgendamentoTurno();
			EntidadeConteudo turno = (EntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
							EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
			Long chavePrimaria = Long.valueOf(((BigDecimal) resultadoArray[0]).toString());
			turno.setChavePrimaria(Long.parseLong(((BigDecimal) resultadoArray[0]).toString()));
			agendamentoTurno.setTurno(turno);
			agendamentoTurno.setQuantidadeAgendamentosTurno(Long.parseLong(((BigDecimal) resultadoArray[1]).toString()));
			turnosDisponiveis.add(agendamentoTurno);
			turnosDisponiveisMap.put(chavePrimaria, null);
		}
		hql = new StringBuilder();
		hql.append(" select servico_tipo_agendamento_turno.enco_cd_turno, servico_tipo_agendamento_turno.seat_qn  ");
		hql.append(" FROM servico_tipo_agendamento_turno ");
		hql.append(" WHERE servico_tipo_agendamento_turno.SRTI_CD = :servicoTipo ");
		hql.append(" group by servico_tipo_agendamento_turno.enco_cd_turno, servico_tipo_agendamento_turno.seat_qn ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());

		query.setParameter(SERVICO_TIPO, tipo.getChavePrimaria());

		for (Object obj : query.list()) {
			Object[] resultadoArray = (Object[]) obj;
			ServicoTipoAgendamentoTurno agendamentoTurno = new ServicoTipoAgendamentoTurno();
			EntidadeConteudo turno = (EntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
							EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
			Long chavePrimaria = Long.valueOf(((BigDecimal) resultadoArray[0]).toString());
			if (!turnosDisponiveisMap.containsKey(chavePrimaria)) {
				turno.setChavePrimaria(Long.parseLong(((BigDecimal) resultadoArray[0]).toString()));
				agendamentoTurno.setTurno(turno);
				agendamentoTurno.setQuantidadeAgendamentosTurno(Long.parseLong(((BigDecimal) resultadoArray[1]).toString()));
				turnosDisponiveis.add(agendamentoTurno);
			}
		}

		return turnosDisponiveis;
	}

	/**
	 * Consultar servico autorizacao agenda por ponto consumo.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @return the list
	 */
	public List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgendaPorPontoConsumo(Long chavePontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAutorizacaoAgenda ");
		hql.append(" inner join fetch servicoAutorizacaoAgenda.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join fetch servicoAutorizacaoAgenda.turno turno ");
		hql.append(" inner join fetch servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" left join fetch servicoAutorizacao.chamado chamado ");
		hql.append(" left join fetch chamado.protocolo protocolo ");
		hql.append(" where servicoAutorizacao.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" order by servicoAutorizacaoAgenda.dataAgenda desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePontoConsumo", chavePontoConsumo);

		return query.list();
	}

	/**
	 * Consultar servico autorizacao por chamado.
	 * 
	 * @param chavePrimariaChamado
	 *            the chave primaria chamado
	 * @return the servico autorizacao
	 */
	public ServicoAutorizacao consultarServicoAutorizacaoPorChamado(Long chavePrimariaChamado) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias("chamadoTipoPesquisa", "chamadoTipoPesquisa");
		criteria.add(Restrictions.eq("chamadoTipoPesquisa.chavePrimaria", chavePrimariaChamado));
		return (ServicoAutorizacao) criteria.uniqueResult();
	}

	/**
	 * Consultar servico autorizacao por comando acao.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorComandoAcao(AcaoComando acaoComando) throws NegocioException {

		Criteria criteria = this.createCriteria(ServicoAutorizacao.class, "servicoAutorizacao");

		criteria.createAlias("servicoAutorizacao.pontoConsumo", PONTO_CONSUMO, Criteria.INNER_JOIN);

		if (acaoComando.getGrupoFaturamento() != null || acaoComando.getSetorComercial() != null || acaoComando.getLocalidade() != null
						|| acaoComando.getMunicipio() != null) {
			criteria.createAlias("pontoConsumo.rota", "rota", Criteria.INNER_JOIN);
			if (acaoComando.getGrupoFaturamento() != null) {
				criteria.createAlias("rota.grupoFaturamento", "grupoFaturamento", Criteria.INNER_JOIN);
				criteria.add(Restrictions.eq("grupoFaturamento.chavePrimaria", acaoComando.getGrupoFaturamento().getChavePrimaria()));
			}
			if (acaoComando.getSetorComercial() != null || acaoComando.getLocalidade() != null || acaoComando.getMunicipio() != null) {
				criteria.createAlias("rota.setorComercial", "setorComercial", Criteria.INNER_JOIN);
				if (acaoComando.getSetorComercial() != null) {
					criteria.add(Restrictions.eq("setorComercial.chavePrimaria", acaoComando.getSetorComercial().getChavePrimaria()));
				}
				if (acaoComando.getLocalidade() != null) {
					criteria.createAlias("setorComercial.localidade", "localidade", Criteria.INNER_JOIN);
					criteria.add(Restrictions.eq("localidade.chavePrimaria", acaoComando.getLocalidade().getChavePrimaria()));
				}
				if (acaoComando.getMunicipio() != null) {
					criteria.createAlias("setorComercial.municipio", "municipio", Criteria.INNER_JOIN);
					criteria.add(Restrictions.eq("municipio.chavePrimaria", acaoComando.getMunicipio().getChavePrimaria()));
				}

			}
		}

		if (acaoComando.getMarcaCorretor() != null || acaoComando.getModeloCorretor() != null || acaoComando.getModeloMedidor() != null
						|| acaoComando.getMarcaMedidor() != null) {
			criteria.createAlias("pontoConsumo.instalacaoMedidor", "instalacaoMedidor", Criteria.INNER_JOIN);
			if (acaoComando.getMarcaCorretor() != null || acaoComando.getModeloCorretor() != null) {
				criteria.createAlias("instalacaoMedidor.vazaoCorretor", "vazaoCorretor", Criteria.INNER_JOIN);
				if (acaoComando.getMarcaCorretor() != null) {
					criteria.createAlias("vazaoCorretor.marcaCorretor", "marcaCorretor", Criteria.INNER_JOIN);
					criteria.add(Restrictions.eq("marcaCorretor.chavePrimaria", acaoComando.getMarcaCorretor().getChavePrimaria()));
				}
				if (acaoComando.getModeloCorretor() != null) {
					criteria.createAlias("vazaoCorretor.modelo", "modeloCorretor", Criteria.INNER_JOIN);
					criteria.add(Restrictions.eq("modeloCorretor.chavePrimaria", acaoComando.getModeloCorretor().getChavePrimaria()));
				}
			}
			if (acaoComando.getModeloMedidor() != null || acaoComando.getMarcaMedidor() != null) {
				criteria.createAlias("instalacaoMedidor.medidor", "medidor", Criteria.INNER_JOIN);
				if (acaoComando.getMarcaMedidor() != null) {
					criteria.createAlias("medidor.marcaMedidor", "marcaMedidor", Criteria.INNER_JOIN);
					criteria.add(Restrictions.eq("marcaMedidor.chavePrimaria", acaoComando.getMarcaMedidor().getChavePrimaria()));
				}
				if (acaoComando.getModeloMedidor() != null) {
					criteria.createAlias("medidor.modelo", "modeloMedidor", Criteria.INNER_JOIN);
					criteria.add(Restrictions.eq("modeloMedidor.chavePrimaria", acaoComando.getModeloMedidor().getChavePrimaria()));
				}

			}

		}

		if (acaoComando.getSituacaoConsumo() != null) {
			criteria.createAlias("pontoConsumo.situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("situacaoConsumo.chavePrimaria", acaoComando.getSituacaoConsumo().getChavePrimaria()));
		}

		if (acaoComando.getSegmento() != null) {
			criteria.createAlias("pontoConsumo.segmento", "segmento", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("segmento.chavePrimaria", acaoComando.getSegmento().getChavePrimaria()));
		}

		if (acaoComando.getIndicadorCondominio() != null) {
			criteria.createAlias("pontoConsumo.imovel", IMOVEL, Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq(IMOVEL_CONDOMINIO, acaoComando.getIndicadorCondominio()));
		}
		return criteria.list();
	}

	/**
	 * Obter materias ordenado.
	 * 
	 * @param chavesServicoAutorizacaoMaterial
	 *            the chaves servico autorizacao material
	 * @return the collection
	 */
	public Collection<ServicoAutorizacaoMaterial> obterMateriasOrdenado(Long[] chavesServicoAutorizacaoMaterial) {

		Criteria criteria = this.createCriteria(ServicoAutorizacaoMaterial.class, "servicoAutorizacaoMaterial");
		criteria.createAlias("servicoAutorizacaoMaterial.material", "material", Criteria.INNER_JOIN);
		criteria.add(Restrictions.in("servicoAutorizacaoMaterial.chavePrimaria", chavesServicoAutorizacaoMaterial));

		criteria.addOrder(Order.desc("material.descricao"));

		return criteria.list();
	}

	/**
	 * Verificar imovel condominio.
	 * 
	 * @param servicoAutorizacaoVO
	 *            the servico autorizacao vo
	 * @return the collection
	 */
	public Collection<Imovel> verificarImovelCondominio(ServicoAutorizacaoVO servicoAutorizacaoVO) {

		Criteria criteria = this.createCriteria(Imovel.class, IMOVEL);

		criteria.setFetchMode(IMOVEL, FetchMode.JOIN);
		criteria.setFetchMode(IMOVEL_QUADRA_FACE, FetchMode.JOIN);

		criteria.createAlias(IMOVEL_QUADRA_FACE, IMOVEL_QUADRA_FACE, Criteria.LEFT_JOIN);

		String cepImovel = servicoAutorizacaoVO.getCepImovel();
		if (cepImovel != null && !StringUtils.isEmpty(cepImovel)) {
			criteria.createCriteria("imovel.quadraFace.endereco").createCriteria("cep").add(Restrictions.eq("cep", cepImovel));
		}
		String numeroImovel = servicoAutorizacaoVO.getNumeroImovel();
		if (numeroImovel != null && !StringUtils.isEmpty(numeroImovel)) {
			criteria.add(Restrictions.eq("imovel.numeroImovel", numeroImovel));
		}
		String descricaoComplemento = servicoAutorizacaoVO.getDescricaoComplemento();
		if (descricaoComplemento != null && !StringUtils.isEmpty(descricaoComplemento)) {
			criteria.add(Restrictions.ilike("imovel.descricaoComplemento", Util.formatarTextoConsulta(descricaoComplemento)));
		}
		String nomeImovel = servicoAutorizacaoVO.getNomeImovel();
		if (nomeImovel != null && !StringUtils.isEmpty(nomeImovel)) {
			criteria.add(Restrictions.ilike("imovel.nome", Util.formatarTextoConsulta(nomeImovel)));
		}
		Long matriculaImovel = servicoAutorizacaoVO.getMatriculaImovel();
		if (matriculaImovel != null) {
			criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMERIA, matriculaImovel));
		}
		Boolean condominioImovel = servicoAutorizacaoVO.getCondominioImovel();
		if (condominioImovel != null) {
			criteria.add(Restrictions.eq(IMOVEL_CONDOMINIO, condominioImovel));
		}

		return criteria.list();
	}

	/**
	 * Consultar controle servico autorizacao.
	 * 
	 * @param pesquisaRelatorioControleServicoAutorizacaoVO
	 *            the pesquisa relatorio controle servico autorizacao vo
	 * @return the list
	 * @throws HibernateException
	 *             the hibernate exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public List<ServicoAutorizacao> consultarControleServicoAutorizacao(
					PesquisaRelatorioControleServicoAutorizacaoVO pesquisaRelatorioControleServicoAutorizacaoVO) throws HibernateException,
					GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(ALIAS_SERVICO_AUTORIZACAO);
		hql.append(" left join fetch servicoAutorizacao.pontoConsumo pontoConsumo");
		hql.append(" left join fetch pontoConsumo.segmento segmento ");
		hql.append(" inner join fetch servicoAutorizacao.contrato contrato ");
		hql.append(" inner join fetch servicoAutorizacao.equipe equipe ");
		hql.append(" inner join fetch equipe.unidadeOrganizacional unidadeOrganizacional");
		hql.append(" inner join fetch servicoAutorizacao.cliente cliente");
		hql.append(" left join fetch servicoAutorizacao.chamado chamado ");
		hql.append(" left join fetch chamado.chamadoAssunto.rubricaDebito debito");
		hql.append(" where 1=1 ");
		if (pesquisaRelatorioControleServicoAutorizacaoVO.getIdCliente() != null) {
			hql.append(" and servicoAutorizacao.cliente.chavePrimaria = :idCliente ");
		}
		if (pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESInicial() != null
						&& !pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESInicial().isEmpty()) {
			hql.append(" and servicoAutorizacao.dataGeracao >= :dataEmissaoInicial");
		}

		if (pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESFinal() != null
						&& !pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESFinal().isEmpty()) {
			hql.append(" and servicoAutorizacao.dataGeracao <= :dataEmissaoFinal");
		}

		if (pesquisaRelatorioControleServicoAutorizacaoVO.getSegmento() != null
						&& pesquisaRelatorioControleServicoAutorizacaoVO.getSegmento() != -1) {
			hql.append(" and pontoConsumo.segmento.chavePrimaria = :segmento");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (pesquisaRelatorioControleServicoAutorizacaoVO.getIdCliente() != null) {
			query.setParameter("idCliente", pesquisaRelatorioControleServicoAutorizacaoVO.getIdCliente());
		}
		if (pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESInicial() != null
						&& !pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESInicial().isEmpty()) {
			query.setParameter("dataEmissaoInicial", Util.converterCampoStringParaData("Data Inicio",
							pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESInicial(), Constantes.FORMATO_DATA_BR));

		}
		if (pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESFinal() != null
						&& !pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESFinal().isEmpty()) {
			query.setParameter("dataEmissaoFinal", Util.converterCampoStringParaData("Data Inicio",
							pesquisaRelatorioControleServicoAutorizacaoVO.getDataEmissaoAESFinal(), Constantes.FORMATO_DATA_BR));
		}

		if (pesquisaRelatorioControleServicoAutorizacaoVO.getSegmento() != null
						&& pesquisaRelatorioControleServicoAutorizacaoVO.getSegmento() != -1) {
			query.setParameter("segmento", pesquisaRelatorioControleServicoAutorizacaoVO.getSegmento());
		}

		return query.list();
	}

	/**
	 * Obter servico autorizacao.
	 * 
	 * @param pontoConsumo {@link Long}
	 * @param dtavisoCorte {@link Date}
	 * @param idServicoTipo {@link Long}
	 * @param listaChavePontoConsumo {@link List}
	 * @param statusCancelado {@link EntidadeConteudo}
	 * @return Lista de Autorização de Serviço {@link List}
	 */
	public Collection<ServicoAutorizacao> obterServicoAutorizacao(Long pontoConsumo, Date dtavisoCorte, Long idServicoTipo,
			List<Long> listaChavePontoConsumo, EntidadeConteudo statusCancelado) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.createAlias(EQUIPE, EQUIPE);
		criteria.createAlias(STATUS, STATUS);
		criteria.createAlias(SERVICO_TIPO, SERVICO_TIPO);
		criteria.createAlias(SERVICO_TIPO_PRIORIDADE, SERVICO_TIPO_PRIORIDADE);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
		criteria.createAlias("usuarioSistema", "usuarioSistema");

		if (pontoConsumo != null && pontoConsumo > 0) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo));
		}

		if (dtavisoCorte != null) {
			criteria.add(Restrictions.ge(DATA_GERACAO, dtavisoCorte));

		}

		if ((idServicoTipo != null) && (idServicoTipo > 0)) {
			criteria.add(Restrictions.eq("servicoTipo.chavePrimaria", idServicoTipo));

		}

		if (listaChavePontoConsumo != null && !listaChavePontoConsumo.isEmpty()) {
			criteria.add(Restrictions.in(PONTO_CONSUMO_CHAVE_PRIMARIA, listaChavePontoConsumo));
		}

		if (statusCancelado != null) {
			criteria.add(Restrictions.ne("status.chavePrimaria", statusCancelado.getChavePrimaria()));
		}

		criteria.addOrder(Order.asc(DATA_GERACAO));

		return criteria.list();
	}
	
	/**
	 * Consultar servico autorizacao por chamado.
	 * 
	 * @param chavePrimariaChamado A chave primaria chamado
	 * @return lista de servico autorizacao
	 */
	public List<ServicoAutorizacao> consultarServicoAutorizacaoPorChaveChamado(Long chavePrimariaChamado) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(CHAMADO, CHAMADO);
		criteria.add(Restrictions.eq("chamado.chavePrimaria", chavePrimariaChamado));
		return criteria.list();
	}

	/**
	 * Verifica se existe questionario
	 * 
	 * @param questionario {@link Questionario}
	 * @return true se existir, false caso contrario
	 */
	public boolean existeASQuestionario(Questionario questionario) {
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(SERVICO_TIPO, SERVICO_TIPO);
		criteria.add(Restrictions.eq("servicoTipo.formulario.chavePrimaria", questionario.getChavePrimaria()));

		Collection<Questionario> lista = criteria.list();

		return lista != null && !lista.isEmpty();
	}
 
	/**
	 * Consulta autorização de serviço
	 * @param chavePrimariaCliente id do cliente
	 * @param chavePrimariaServicoTipo id do tipo de serviço
	 * @param chaveImovel id do imóvel
	 * @return retorna a autorização de serviço
	 * @throws HibernateException lançada caso ocorra falha na execução da query
	 */
	@SuppressWarnings("squid:S1192")
	public Optional<ServicoAutorizacao> consultarServicoAutorizacaoReferenciaGarantia(Long chavePrimariaCliente,
			Long chavePrimariaServicoTipo, Long chaveImovel)
			throws HibernateException{

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT servicoAutorizacao ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(ALIAS_SERVICO_AUTORIZACAO);
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" inner join servicoAutorizacao.contrato contrato ");
		hql.append(" inner join servicoAutorizacao.cliente cliente");
		hql.append(" left join servicoAutorizacao.servicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento");
		hql.append(" left join fetch servicoAutorizacao.chamado chamado ");

		hql.append(" where cliente.chavePrimaria = :chavePrimariaCliente");
		hql.append(" and servicoTipo.chavePrimaria = :chavePrimariaServicoTipo");
		hql.append(" and servicoAutorizacao.dataExecucao IS NOT NULL");
		hql.append(" and servicoAutorizacao.imovel.chavePrimaria = :chaveImovel");
		hql.append(" and ((servicoAutorizacaoMotivoEncerramento.descricao = :SUCESSO and servicoAutorizacao.indicadorSucessoExecucao IS NULL) ");
		hql.append(" or servicoAutorizacao.indicadorSucessoExecucao = :SUCESSO_BOOLEAN) ");
		hql.append(" and servicoAutorizacao.indicadorReferenciaGarantia = :SUCESSO_BOOLEAN ");

		hql.append(" order by servicoAutorizacao.dataExecucao DESC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimariaCliente", chavePrimariaCliente);
		query.setParameter("chavePrimariaServicoTipo", chavePrimariaServicoTipo);
		query.setParameter("chaveImovel", chaveImovel);
		query.setParameter("SUCESSO", Constantes.MOTIVO_SUCESSO_ENCERRAMENTO_AS);
		query.setParameter("SUCESSO_BOOLEAN", true);
		query.setMaxResults(1);

		List<ServicoAutorizacao> servicos = query.list();
		if (servicos.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(servicos.get(0));
		}
	}

	/**
	 * Obtém número de execuções por cliente
	 * @param chavePrimariaCliente id do cliente
	 * @param chavePrimariaServicoTipo id do tipo de serviço
	 * @param chaveImovel id do imóvel
	 * @param dataGarantia data da garantia
	 * @return retorna o numero de execuções
	 * @throws HibernateException lançada caso haja erro de execução da query
	 */
	public Long consultarNumeroExecucoesPorClienteEServicoTipo(Long chavePrimariaCliente,
			Long chavePrimariaServicoTipo, Long chaveImovel, Date dataGarantia)
			throws HibernateException{

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT count(*) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(ALIAS_SERVICO_AUTORIZACAO);
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" inner join servicoAutorizacao.contrato contrato ");
		hql.append(" inner join servicoAutorizacao.cliente cliente");
		hql.append(" left join servicoAutorizacao.servicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento");
		hql.append(" where cliente.chavePrimaria = :chavePrimariaCliente");
		hql.append(" and servicoTipo.chavePrimaria = :chavePrimariaServicoTipo");
		hql.append(" and ((servicoAutorizacao.dataExecucao IS NOT NULL and servicoAutorizacao.dataExecucao > :dataGarantia) ");
		hql.append(" or servicoAutorizacao.indicadorReferenciaGarantia = :SUCESSO_BOOLEAN) ");
		hql.append(" and servicoAutorizacao.imovel.chavePrimaria = :chaveImovel ");
		hql.append(" and ((servicoAutorizacaoMotivoEncerramento.descricao = :SUCESSO and servicoAutorizacao.indicadorSucessoExecucao IS NULL) ");
		hql.append(" or servicoAutorizacao.indicadorSucessoExecucao = :SUCESSO_BOOLEAN) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimariaCliente", chavePrimariaCliente);
		query.setParameter("chavePrimariaServicoTipo", chavePrimariaServicoTipo);
		query.setParameter("chaveImovel", chaveImovel);
		query.setParameter("SUCESSO", Constantes.MOTIVO_SUCESSO_ENCERRAMENTO_AS);
		query.setParameter("SUCESSO_BOOLEAN", true);
		query.setDate("dataGarantia", dataGarantia);

		return (Long) query.uniqueResult();
	}

	/**
	 * Lista os agendamentos de um cliente
	 * @param cliente
	 * 			the cliente
	 * @return retorna lista de Agendamentos do Cliente
	 */
	@SuppressWarnings("unchecked")
	public List<ServicoAutorizacaoAgendasVO> listarAgandamentosPorCliente(Cliente cliente) {
		Criteria criteria = createCriteria(getClasseEntidadeServicoAutorizacaoAgenda(), "seaa");
		criteria.createAlias("seaa.turno", "turn");
		criteria.createAlias("seaa.servicoAutorizacao", "seau");
		criteria.createAlias("seau.cliente", "clie");
		criteria.createAlias("seau.equipe", "equi");
		criteria.createAlias("seau.servicoTipo", "seti");
		criteria.createAlias("seau.status", "stat");
		criteria.createAlias("seau.chamado", "cham");
		criteria.createAlias("cham.protocolo", "prot");
		
		ProjectionList list = Projections.projectionList();
		list.add(Projections.property("seaa.chavePrimaria"), ServicoAutorizacaoAgendasVO.Fields.chavePrimaria.toString());
		list.add(Projections.property("seti.descricao"), ServicoAutorizacaoAgendasVO.Fields.tipoServicoDescricao.toString());
		list.add(Projections.property("seau.dataGeracao"), ServicoAutorizacaoAgendasVO.Fields.dataAbertura.toString());
		list.add(Projections.property("seaa.dataAgenda"), ServicoAutorizacaoAgendasVO.Fields.dataAgendamento.toString());
		list.add(Projections.property("turn.descricao"), ServicoAutorizacaoAgendasVO.Fields.turnoDescricao.toString());
		list.add(Projections.property("equi.nome"), ServicoAutorizacaoAgendasVO.Fields.equipeDescricao.toString());
		list.add(Projections.property("prot.numeroProtocolo"), ServicoAutorizacaoAgendasVO.Fields.numeroProtocolo.toString());
		list.add(Projections.property("stat.descricao"), ServicoAutorizacaoAgendasVO.Fields.status.toString());
		list.add(Projections.property("stat.chavePrimaria"), ServicoAutorizacaoAgendasVO.Fields.statusID.toString());
		criteria.setProjection(list);
		
		if(cliente.getChavePrimaria() != 0L){
			criteria.add(Restrictions.eq("clie.chavePrimaria", cliente.getChavePrimaria()));
		}
		criteria.setResultTransformer(Transformers.aliasToBean(ServicoAutorizacaoAgendasVO.class));
		criteria.addOrder(Order.asc("prot.numeroProtocolo"));
		return (List<ServicoAutorizacaoAgendasVO>) criteria.list();
	}

	/**
	 * Obtém uma autorização de servico
	 * @param chavePrimaria
	 * 			the chave primaria
	 * @return retorna uma autorização de servico
	 */
	public ServicoAutorizacao obterUnidadeOrganizacionalPorServicoAutorizacao(long chavePrimaria) {
		Criteria criteria = createCriteria(getClasseEntidade(), "seau");
		criteria.createAlias("seau.equipe", "equi");
		criteria.createAlias("equi.unidadeOrganizacional", "unor");
		
		if(chavePrimaria != 0L){
			criteria.add(Restrictions.eq("seau.chavePrimaria", chavePrimaria));
		}
		
		return (ServicoAutorizacao) criteria.uniqueResult();
	}
	
	/**
	 * Obtem AS pelo Motivo Encerramento e Pela Desricao
	 * 
	 * @param classe - {@link Class}	
	 * @param descricao - {@link String}
	 * @return ServicoAutorizacaoMotivoEncerramento - {@link ServicoAutorizacaoMotivoEncerramento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public ServicoAutorizacaoMotivoEncerramento obterServicoAutorizacaoMotivoEncerramentoPelaDescricao(Class<?> classe,
			String descricao) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe.getSimpleName());
		hql.append( " servicoAutorizacaoMotivoEncerramento ");
		hql.append( "where  servicoAutorizacaoMotivoEncerramento.descricao like :descricao");


		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("descricao", descricao);

		return (ServicoAutorizacaoMotivoEncerramento) query.uniqueResult();
		
	}
	
	public ServicoAutorizacaoAgenda consultarServicoAutorizacaoAgendaPorServicoAutorizacao(Long chaveServicoAutorizacao) {
		StringBuilder hql = new StringBuilder();
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAgenda ");
		hql.append(" where servicoAgenda.habilitado = true ");
		hql.append(" and servicoAgenda.servicoAutorizacao.chavePrimaria = :chaveServicoAutorizacao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setLong("chaveServicoAutorizacao", chaveServicoAutorizacao);	

		return (ServicoAutorizacaoAgenda) query.setMaxResults(1).uniqueResult();
		
	}
	
	public List<Object> listarFaturasAbertoGSA(String codigoUnico) {
		String nomeQuery = "fatura.titulosAbertoGSA";
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(nomeQuery);
		query.setParameter("codigoUnico", codigoUnico);
		
		return query.list();
	}

	public Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorChavesPrimarias(Long[] chaves) {

		StringBuilder hql = new StringBuilder();
		hql.append("FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(AUTORIZACAO);
		hql.append(" WHERE ");
		Map<String, List<Long>> mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql,
				"autorizacao.chavePrimaria", "SEAU_CONS", Arrays.asList(chaves));

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return query.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EquipeTurnoItem> consultarEscalaDisponivel(Equipe equipe, EntidadeConteudo turno, String dataAgenda) throws GGASException {
		StringBuilder hql = new StringBuilder();
		hql.append(" select  equipeTurnoItem ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" equipeTurnoItem ");
		hql.append(" WHERE  1 = 1 ");
		if(turno != null && turno.getChavePrimaria() > 0) {
			hql.append(" AND equipeTurnoItem.turno.chavePrimaria = :turno ");
		}
		
		hql.append(" AND equipeTurnoItem.equipe.chavePrimaria = :equipe ");
		hql.append(" AND equipeTurnoItem.dataInicio BETWEEN :dataInicial AND :dataFinal ");
		hql.append(" ORDER BY equipeTurnoItem.dataInicio ASC, equipeTurnoItem.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("equipe", equipe.getChavePrimaria());
		
		if(turno != null && turno.getChavePrimaria() > 0) {
			query.setParameter("turno", turno.getChavePrimaria());
		}
		
		Date dataInicial = Util.converterCampoStringParaData("Data Inicial", dataAgenda, Constantes.FORMATO_DATA_BR);
		Date dataFinal = Util.adicionarDiasData(dataInicial, 1);
		
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		
		return query.list();
	}
	
	public Collection<ServicoAutorizacaoAgenda> obterListaAgendamentosPorDiaTurnoServicoTipoEquipe(Date dataAgenda, Long turno, ServicoTipo servicoTipo, Equipe equipe, Funcionario funcionario) {
		
		StringBuilder hql = new StringBuilder();
		hql.append("select servicoAgenda ");
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAgenda ");
		hql.append(" inner join servicoAgenda.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" inner join servicoAutorizacao.equipe equipe ");
		hql.append(" WHERE ");
		hql.append(" servicoAgenda.dataAgenda >= :dataAgendaInicial");
		hql.append(" and servicoAgenda.dataAgenda < :dataAgendaFinal");
		hql.append(" and servicoAgenda.indicadorConfirmado = true");		
		
		if (servicoTipo != null && servicoTipo.getChavePrimaria() > 0) {
			hql.append(" and servicoTipo.chavePrimaria = :servicoTipo");
		}
		
		if(funcionario != null && funcionario.getChavePrimaria() > 0) {
			hql.append(" and servicoAgenda.funcionario.chavePrimaria = :funcionario");
		}
		
		hql.append(" and equipe.chavePrimaria = :equipe");
		hql.append(" and servicoAgenda.turno.chavePrimaria = :turno");
		hql.append(" order by servicoAgenda.dataAgenda ");
		
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("dataAgendaInicial", dataAgenda);
		query.setParameter("dataAgendaFinal", DataUtil.incrementarDia(dataAgenda, 1));
		
		query.setParameter("turno", turno);
		
		if (servicoTipo != null && servicoTipo.getChavePrimaria() > 0) {
			query.setParameter("servicoTipo", servicoTipo.getChavePrimaria());
		}
		
		if (funcionario != null && funcionario.getChavePrimaria() > 0) {
			query.setParameter("funcionario", funcionario.getChavePrimaria());
		}
		
		query.setParameter("equipe", equipe.getChavePrimaria());
		
		
		return query.list();
	}

	/**
	 * Listar servico tipo autorizacao servico.
	 * 
	 * @return the list
	 */
	public List<Usuario> listarUsuariosSolcitantesAutorizacaoServico() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct servicoAutorizacao.usuarioSistema ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servicoAutorizacao");
		hql.append(" inner join servicoAutorizacao.usuarioSistema.funcionario fucionario");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	
	public Collection<ServicoTipoAgendamentoTurno> listaTurnoPorEquipeServicoTipo(Long chaveEquipe) {
		StringBuilder hql = new StringBuilder();
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoTipoAgendamentoTurno().getSimpleName());
		hql.append(" servicoTipoAgendamentoTurno ");
		hql.append(" where servicoTipoAgendamentoTurno.equipe.chavePrimaria = :chaveEquipe ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setLong("chaveEquipe", chaveEquipe);	


		return query.list();
		
	}

	public Collection<ServicoAutorizacao> listarServicoAutorizacaoPendente() {
		
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(ALIAS_SERVICO_AUTORIZACAO);
		hql.append(WHERE);
		hql.append(" (status.descricao = 'ABERTA' ");
		hql.append(" or status.descricao = 'PENDENTE DE ATUALIZACAO')" );
		hql.append(" and dataPrevisaoEncerramento < :dataAtual ");
		hql.append(" order by equipe.nome, dataPrevisaoEncerramento ");
		
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setDate("dataAtual", Calendar.getInstance().getTime());
		
		
		return query.list();

		
	}
	
	public Collection<ServicoAutorizacaoTemp> listarAutorizacoesServicoEncerramentoAPP() {
		StringBuilder hql = new StringBuilder();
		
		hql.append("SELECT servicoTemp ");
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoTemp().getSimpleName());
		hql.append(" servicoTemp ");
		hql.append(" inner join servicoTemp.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" where servicoTemp.status.descricao like 'EXECUTADA' ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		return query.list();
	}
	
	public ServicoAutorizacaoTemp consultarAutorizacaoServicoTemp(ServicoAutorizacao autorizacaoServico) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoTemp().getSimpleName());
		hql.append(" servicoTemp ");
		hql.append(" where servicoTemp.servicoAutorizacao.chavePrimaria = :numeroAs ");
		hql.append(" order by servicoTemp.ultimaAlteracao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("numeroAs", autorizacaoServico.getChavePrimaria());
		
		return (ServicoAutorizacaoTemp) query.setMaxResults(1).uniqueResult();
	}
	
	public Collection<ServicoAutorizacaoTempAnexo> listarAnexosAutorizacaoServicoEncerramentoAPP(ServicoAutorizacao autorizacaoServico) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("SELECT anexo ");
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoTempAnexo().getSimpleName());
		hql.append(" anexo ");
		hql.append(" where anexo.servicoAutorizacao.chavePrimaria = :numeroAs ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("numeroAs", autorizacaoServico.getChavePrimaria());
		
		return query.list();
	}
	
	public ServicoAutorizacaoTempAnexo consultarAssinaturaAnexoAutorizacaoServicoTemp(ServicoAutorizacao autorizacaoServico) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoTempAnexo().getSimpleName());
		hql.append(" anexo ");
		hql.append(" where anexo.servicoAutorizacao.chavePrimaria = :numeroAs ");
		hql.append(" and anexo.nome like '%ASSINATURA%' ");
		hql.append(" order by anexo.ultimaAlteracao desc ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("numeroAs", autorizacaoServico.getChavePrimaria());
		
		return (ServicoAutorizacaoTempAnexo) query.setMaxResults(1).uniqueResult();
	}

	public Collection<ServicoAutorizacaoAgenda> listaServicoAutorizacaoAgenda(Long[] chavesServicoAutorizacaoAgenda) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" agenda ");

		hql.append(WHERE);
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "agenda.chavePrimaria", "SA_CONS", Arrays.asList(chavesServicoAutorizacaoAgenda));
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		
		return query.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<EntidadeConteudo> consultaTurnosEquipeEscala(Equipe equipe, String dataAgenda) throws GGASException {
		StringBuilder hql = new StringBuilder();
		hql.append(" select equipeTurnoItem.turno ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" equipeTurnoItem ");
		hql.append(" WHERE  1 = 1 ");
		
		hql.append(" AND equipeTurnoItem.equipe.chavePrimaria = :equipe ");
		hql.append(" AND equipeTurnoItem.dataInicio BETWEEN :dataInicial AND :dataFinal ");
		hql.append(" ORDER BY equipeTurnoItem.dataInicio ASC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("equipe", equipe.getChavePrimaria());
		
		Date dataInicial = Util.converterCampoStringParaData("Data Inicial", dataAgenda, Constantes.FORMATO_DATA_BR);
		Date dataFinal = Util.adicionarDiasData(dataInicial, 1);
		
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		
		return query.list();
	}

	public ServicoAutorizacaoTempAnexo consultarFotoCRM(ServicoAutorizacao servicoAutorizacao) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoTempAnexo().getSimpleName());
		hql.append(" anexo ");
		hql.append(" where anexo.servicoAutorizacao.chavePrimaria = :numeroAs ");
		hql.append(" and anexo.nome like '%MEDIDOR_CRM%' ");
		hql.append(" order by anexo.ultimaAlteracao desc ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("numeroAs", servicoAutorizacao.getChavePrimaria());
		
		return (ServicoAutorizacaoTempAnexo) query.setMaxResults(1).uniqueResult();
	}
	
	public Collection<ServicoAutorizacaoAgenda> consultarUltimosAgendamentosPorEquipe(Equipe equipe, String dataSelecioanda, EntidadeConteudo turno) throws HibernateException, NegocioException {
		StringBuilder hql = new StringBuilder();
		hql.append(" select servicoAutorizacaoAgenda ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAutorizacaoAgenda " );
		hql.append(" inner join fetch servicoAutorizacaoAgenda.servicoAutorizacao servicoAutorizacao" );
		
		hql.append(" WHERE  1 = 1 ");
		hql.append(" AND servicoAutorizacao.equipe.chavePrimaria = :chaveEquipe ");
		hql.append(" AND servicoAutorizacaoAgenda.indicadorConfirmado = true ");
		hql.append(" AND TO_CHAR(servicoAutorizacaoAgenda.dataAgenda, 'dd-mm-yyyy') =  TO_DATE ( ");
		hql.append(" (select  to_char(max(servicoAutorizacaoAgendaSub.dataAgenda), 'dd-mm-yyyy') ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAutorizacaoAgendaSub " );
		hql.append(" inner join servicoAutorizacaoAgendaSub.servicoAutorizacao servicoAutorizacaoSub" );
		
		hql.append(" WHERE  1 = 1 ");
		
		if(turno != null) {
			hql.append(" AND servicoAutorizacaoAgendaSub.turno.chavePrimaria = :turno ");
		}
		hql.append(" AND servicoAutorizacaoSub.equipe.chavePrimaria = :chaveEquipe ");
		hql.append(" AND servicoAutorizacaoAgendaSub.indicadorConfirmado = true ");
		hql.append(" AND servicoAutorizacaoAgendaSub.dataAgenda <= :dataSelecionada )");
		hql.append(" , 'dd-mm-yyyy') ");
		hql.append(" ORDER BY servicoAutorizacaoAgenda.dataAgenda desc, servicoAutorizacaoAgenda.ultimaAlteracao desc ");

		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveEquipe", equipe.getChavePrimaria());
		query.setParameter("dataSelecionada", DataUtil.converterParaData(dataSelecioanda));
		
		if(turno != null) {
			query.setParameter("turno", turno.getChavePrimaria());
		}
		
		return query.list();
	}

	public ServicoAutorizacaoAgenda consultarServicoAutorizacaoAgendaPorChave(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT servicoAutorizacaoAgenda ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAutorizacaoAgenda ");
		hql.append(" INNER JOIN servicoAutorizacaoAgenda.servicoAutorizacao servicoAutorizacao ");
		hql.append(" WHERE ");
		hql.append(" servicoAutorizacao.chavePrimaria = :chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (ServicoAutorizacaoAgenda) query.setMaxResults(1).uniqueResult();
	}
	public Empresa consultarCodigoClienteEmpresa() {
		StringBuilder hql = new StringBuilder();
		
		hql.append(" SELECT empresa ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEmpresa().getSimpleName());
		hql.append(" empresa ");
		hql.append(" WHERE empresa.principal = 1 ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		return (Empresa) query.uniqueResult();
	}
	
	public String consultarMunicipioEmpresa(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT MUNI_DS ");
		hql.append(" FROM ");
		hql.append(" CLIENTE_ENDERECO ");
		hql.append(" clienteEndereco ");
		hql.append(" INNER JOIN MUNICIPIO municipio ON municipio.MUNI_CD = clienteEndereco.MUNI_CD ");
		hql.append(" INNER JOIN CLIENTE cliente ON cliente.CLIE_CD = clienteEndereco.CLIE_CD ");
		hql.append(" WHERE cliente.CLIE_CD = :chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (String) query.uniqueResult();
	}
	
	public Collection<PontoConsumo> consultarPontosLoteAutorizacaoServico(
			FiltroServicoAutorizacaoLoteDTO filtroServicoAutorizacaoLoteDTO) throws HibernateException, GGASException {
		StringBuilder hql = new StringBuilder("SELECT pc ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" cpc ");
		hql.append(" inner join cpc.contrato c ");
		hql.append(" inner join cpc.pontoConsumo pc ");
		hql.append(" inner join pc.quadraFace.quadra quadra ");
		hql.append(" inner join pc.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join pc.instalacaoMedidor.medidor medidor ");
		hql.append(" inner join pc.situacaoConsumo situacaoConsumo ");
		hql.append(" inner join pc.imovel imovel ");
		hql.append(" WHERE 1=1 ");
		
		if(filtroServicoAutorizacaoLoteDTO.getSituacaoContrato() != null && filtroServicoAutorizacaoLoteDTO.getSituacaoContrato() > 0) {
			hql.append(" and c.situacao.chavePrimaria = :situacao ");
		}
		
		hql.append(" and cpc.chavePrimaria = ");
		hql.append(" ( select max(cpc2.chavePrimaria) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" cpc2 ");
		hql.append(" WHERE ");
		hql.append(" cpc.pontoConsumo.chavePrimaria = cpc2.pontoConsumo.chavePrimaria )");
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroCityGatePreenchido()) {
			hql.append(" and pc.chavePrimaria in (select pcg.pontoConsumo.chavePrimaria from " ); 
			hql.append(getClasseEntidadePontoConsumoCityGate().getSimpleName());
			hql.append(" pcg where pcg.cityGate.chavePrimaria = :idCityGate) ");
		}
			
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroCepComunicacaoPreenchido())
			hql.append(" and pc.cep.cep = :numeroCep ");
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroQuadraPreenchido())
			hql.append(" and quadra.chavePrimaria = :idQuadra ");
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroTroncoPreenchido()) {
			hql.append(" and EXISTS (select tronco from ");
			hql.append(getClasseEntidadeTronco().getSimpleName());
			hql.append(" tronco inner join tronco.cityGate cityGate where tronco.chavePrimaria = :idTronco and cityGate.chavePrimaria = :idCityGate) ");
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroSegmentoPreenchido())
			hql.append(" and pc.segmento.chavePrimaria in (:chavesSegmentos) ");
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroNumeroMedidorPreenchido())
			hql.append(" and medidor.numeroSerie = :numMedidor ");
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroDataAquisicaoPreenchido()) {
			hql.append(" and medidor.dataAquisicao >= :dataAquisicaoInicial ");
			hql.append(" and medidor.dataAquisicao <= :dataAquisicaoFinal ");
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroDataInstalacaoPreenchido()) {
			hql.append(" and instalacaoMedidor.data >= :dataInstalacaoInicial ");
			hql.append(" and instalacaoMedidor.data <= :dataInstalacaoFinal ");
		}
		
		if(filtroServicoAutorizacaoLoteDTO.isFiltroDataAquisicaoPreenchido() || filtroServicoAutorizacaoLoteDTO.isFiltroDataInstalacaoPreenchido()) {
			hql.append(" and medidor.chavePrimaria not in ( ");
			hql.append(" select medidor2.chavePrimaria ");
			hql.append(" FROM ");
			hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
			hql.append(" historicoOperacaoMedidor ");
			hql.append(" INNER JOIN historicoOperacaoMedidor.medidor medidor2");
			hql.append(" WHERE ");
			hql.append(" historicoOperacaoMedidor.operacaoMedidor.chavePrimaria in (2,5))");
		}

		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroSituacaoMedidorPreenchido()) {
			hql.append(" and medidor.situacaoMedidor.chavePrimaria = :situacaoMedidor ");
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroSituacaoConsumo()) {
			hql.append(" and situacaoConsumo.chavePrimaria = :situacaoConsumo ");
		}
		
		if(filtroServicoAutorizacaoLoteDTO.isFiltroTipoMedicao()) {
			hql.append(" and imovel.modalidadeMedicaoImovel = :tipoMedicao ");
			
			if(!filtroServicoAutorizacaoLoteDTO.getTipoMedicao()) {
				hql.append(" and imovel.condominio = true ");
			}
		}
		
		if(filtroServicoAutorizacaoLoteDTO.isFiltroRamoAtividadePreenchido()) {
			hql.append(" and pc.ramoAtividade.chavePrimaria = :idRamoAtividade ");
		}
		
		if(filtroServicoAutorizacaoLoteDTO.isFiltroModeloMedidorPreenchido()) {
			hql.append(" and medidor.modelo.chavePrimaria = :idModeloMedidor ");

		}
		
		hql.append(" and pc.chavePrimaria not in ( ");
		hql.append(" select seau.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" seau ");
		hql.append(" where 1=1 ");
		hql.append(" and seau.servicoTipo.chavePrimaria in (  ");
		hql.append(" SELECT st.chavePrimaria from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st  ");
		hql.append(" WHERE ");
		hql.append(" st.indicadorOperacaoMedidor in ( ");
		hql.append(" SELECT st1.indicadorOperacaoMedidor from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st1  ");
		hql.append(" WHERE ");
		hql.append(" st1.chavePrimaria = :servicoTipo ");
		hql.append(") ) ");
		hql.append(" and seau.dataEncerramento is null) ");
		
		hql.append(" ORDER BY pc.chavePrimaria ");
		
		Query query = getQuery(hql.toString());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroCityGatePreenchido() )
			query.setParameter("idCityGate", filtroServicoAutorizacaoLoteDTO.getCityGate().getChavePrimaria());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroCepComunicacaoPreenchido())
			query.setParameter("numeroCep", filtroServicoAutorizacaoLoteDTO.getCepComunicacao());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroQuadraPreenchido())
			query.setParameter("idQuadra", filtroServicoAutorizacaoLoteDTO.getQuadra());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroTroncoPreenchido())
			query.setParameter("idTronco", filtroServicoAutorizacaoLoteDTO.getTronco().getChavePrimaria());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroSegmentoPreenchido())
			query.setParameterList("chavesSegmentos", filtroServicoAutorizacaoLoteDTO.getSegmentosSelecionados());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroNumeroMedidorPreenchido())
			query.setParameter("numMedidor", filtroServicoAutorizacaoLoteDTO.getNumeroMedidor());
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroDataAquisicaoPreenchido()) {
			query.setParameter("dataAquisicaoInicial", Util.converterCampoStringParaData("Data Aquisição Início", filtroServicoAutorizacaoLoteDTO.getDataAquisicaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataAquisicaoFinal", Util.converterCampoStringParaData("Data Aquisição Final", filtroServicoAutorizacaoLoteDTO.getDataAquisicaoFinal(), Constantes.FORMATO_DATA_BR));
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroDataInstalacaoPreenchido()) {
			query.setParameter("dataInstalacaoInicial", Util.converterCampoStringParaData("Data Instalação Início", filtroServicoAutorizacaoLoteDTO.getDataInstalacaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataInstalacaoFinal", Util.converterCampoStringParaData("Data Instalação Final", filtroServicoAutorizacaoLoteDTO.getDataInstalacaoFinal(), Constantes.FORMATO_DATA_BR));
		}
		
		if (filtroServicoAutorizacaoLoteDTO.getServicoTipo() != null) {
			query.setLong("servicoTipo", filtroServicoAutorizacaoLoteDTO.getServicoTipo().getChavePrimaria());
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroSituacaoMedidorPreenchido()) {
			query.setLong("situacaoMedidor", filtroServicoAutorizacaoLoteDTO.getSituacaoMedidor().getChavePrimaria());
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroSituacaoConsumo()) {
			query.setLong("situacaoConsumo", filtroServicoAutorizacaoLoteDTO.getSituacaoConsumo());
		}
		
		if (filtroServicoAutorizacaoLoteDTO.isFiltroTipoMedicao()) {
			query.setLong("tipoMedicao", filtroServicoAutorizacaoLoteDTO.getTipoMedicao() ? 1l : 2l);
		}
		
		if(filtroServicoAutorizacaoLoteDTO.getSituacaoContrato() != null && filtroServicoAutorizacaoLoteDTO.getSituacaoContrato() > 0) {
			query.setLong("situacao", filtroServicoAutorizacaoLoteDTO.getSituacaoContrato());
		}
		
		if(filtroServicoAutorizacaoLoteDTO.isFiltroRamoAtividadePreenchido()) {
			query.setLong("idRamoAtividade", filtroServicoAutorizacaoLoteDTO.getRamoAtividade().getChavePrimaria());

		}
		
		if(filtroServicoAutorizacaoLoteDTO.isFiltroModeloMedidorPreenchido()) {
			query.setLong("idModeloMedidor", filtroServicoAutorizacaoLoteDTO.getModeloMedidor().getChavePrimaria());

		}
		
		return (Collection<PontoConsumo>) query.list().stream().distinct().collect(Collectors.toList());
	}
	
	@SuppressWarnings("unchecked")
	public Collection<PontoConsumo> filtrarPontosParaLoteDeAutorizacaoServico(String descricaoPonto, String codigoLegado, String servicoTipo, Integer offset) {
		
		StringBuilder hql = new StringBuilder("SELECT pc ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pc ");
		hql.append(" inner join pc.quadraFace.quadra quadra ");
		hql.append(" inner join pc.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join pc.instalacaoMedidor.medidor medidor ");
		hql.append(" WHERE 1=1 ");
		
		hql.append(" and pc.chavePrimaria not in ( ");
		hql.append(" select seau.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" seau ");
		hql.append(" where 1=1 ");
		hql.append(" and seau.servicoTipo.chavePrimaria in (  ");
		hql.append(" SELECT st.chavePrimaria from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st  ");
		hql.append(" WHERE ");
		hql.append(" st.indicadorOperacaoMedidor in ( ");
		hql.append(" SELECT st1.indicadorOperacaoMedidor from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st1  ");
		hql.append(" WHERE ");
		hql.append(" st1.chavePrimaria = :servicoTipo ");
		hql.append(") ) ");
		hql.append(" and seau.dataEncerramento is null) ");
		
		if(descricaoPonto != null && !descricaoPonto.isEmpty()) {
			hql.append(" and upper(pc.descricao) like :descricaoPontoConsumo " );
		}
		
		if(codigoLegado != null && !codigoLegado.isEmpty()) {
			hql.append(" and pc.codigoLegado like :codigoLegado " );
		}
		
		hql.append(" ORDER BY pc.descricao ");
		
		Query query = getQuery(hql.toString());
		
		if(descricaoPonto != null && !descricaoPonto.isEmpty()) {
			query.setString("descricaoPontoConsumo",
					Util.removerAcentuacao(Util.formatarTextoConsulta(descricaoPonto).toUpperCase()));
		}
		
		if(codigoLegado != null && !codigoLegado.isEmpty()) {
			query.setString("codigoLegado", "%" + codigoLegado + "%");
		}
		
		query.setLong("servicoTipo", Long.valueOf(servicoTipo));
		
		query.setMaxResults(50);
		
		if(offset != 0) {
			offset = (50 * offset) + 1;
		}
		
		query.setFirstResult(offset);

		
		return (Collection<PontoConsumo>) query.list().stream().distinct().collect(Collectors.toList());
	}
	
	public List<Object[]> consultarLocalizacaoOperadorMobile(Long idFuncionario) {
		
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT t.func_nm, t.sarl_nm_localizacao, sat.SEAU_CD, PC.POCN_DS, TO_CHAR(t.SARL_TM_DATA, 'HH24:MI') AS HORARIO, t.ENCO_DS, t.SEAU_CD as SERVICOAUT, t.POCN_DS as NOMEPONTO, t.SARL_TM_DATA as HORARIOCOMPLETO, t.FUNC_CD, t.SARL_IN_LOCALIZACAO, t.SARL_PREVISAO_CHEGADA, t.SARL_CD ");
		sql.append("FROM (SELECT func.func_nm, ");
		sql.append("			 sarl.SARL_TM_DATA, ");
		sql.append("			 e.ENCO_DS, ");
		sql.append("             sarl.sarl_nm_localizacao, ");
		sql.append("             sarl.func_cd, ");
		sql.append("             sarl.seau_cd, ");
		sql.append("             p.pocn_ds, ");
		sql.append("             sarl.sarl_in_localizacao, ");
		sql.append("             sarl.sarl_previsao_chegada, ");
		sql.append("             sarl.sarl_cd, ");
		sql.append("             ROW_NUMBER() OVER (PARTITION BY sarl.func_cd ORDER BY sarl.sarl_tm_data DESC) AS rn ");
		sql.append("      FROM SERVICO_AUTORIZACAO_REG_LOCAL sarl ");
		sql.append("      INNER JOIN FUNCIONARIO func ON sarl.func_cd = func.func_cd ");
		sql.append("      INNER JOIN EQUIPE_COMPONENTE ec ON func.func_cd = ec.func_cd ");
		sql.append("      INNER JOIN ENTIDADE_CONTEUDO e ON e.ENCO_CD = sarl.SARL_IN_LOCALIZACAO ");
		sql.append("      LEFT JOIN SERVICO_AUTORIZACAO S on S.SEAU_CD = SARL.SEAU_CD ");
		sql.append("      LEFT JOIN PONTO_CONSUMO P on P.POCN_CD = S.POCN_CD ");	
		sql.append("      WHERE TO_CHAR(sarl.sarl_tm_data, 'dd/MM/yyyy') = TO_CHAR(CURRENT_TIMESTAMP, 'dd/MM/yyyy') ");
				
		if (idFuncionario != null && idFuncionario > 0) {
		    sql.append("      AND func.func_cd = :idFuncionario ");
		}
		sql.append(") t ");
		sql.append(" LEFT JOIN SERVICO_AUTORIZACAO_TEMP sat ON t.func_cd = sat.func_cd ");
		sql.append(" AND (sat.satp_tm_inicio IS NOT NULL AND sat.satp_tm_fim IS NULL) ");
		sql.append(" LEFT JOIN SERVICO_AUTORIZACAO SA ON SA.SEAU_CD = SAT.SEAU_CD ");
		sql.append(" LEFT JOIN PONTO_CONSUMO PC ON PC.POCN_CD = SA.POCN_CD ");
		sql.append("WHERE t.rn = 1");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
		    .addScalar("func_nm", StringType.INSTANCE)
		    .addScalar("sarl_nm_localizacao", StringType.INSTANCE)
		    .addScalar("seau_cd", LongType.INSTANCE)
		    .addScalar("POCN_DS", StringType.INSTANCE)
		    .addScalar("HORARIO", StringType.INSTANCE)
		    .addScalar("ENCO_DS", StringType.INSTANCE)
		    .addScalar("SERVICOAUT", LongType.INSTANCE)
		    .addScalar("NOMEPONTO", StringType.INSTANCE)
		    .addScalar("HORARIOCOMPLETO", StandardBasicTypes.TIMESTAMP)
			.addScalar("FUNC_CD", LongType.INSTANCE)
			.addScalar("SARL_IN_LOCALIZACAO", LongType.INSTANCE)
			.addScalar("SARL_PREVISAO_CHEGADA", LongType.INSTANCE)
			.addScalar("SARL_CD", LongType.INSTANCE);
			
		

		if (idFuncionario != null && idFuncionario > 0) {
		    query.setParameter("idFuncionario", idFuncionario);
		}

		return query.list();
		
	}

	public Boolean verificarAsAbertaMesmaOperacao(Long idPontoConsumo, Long idServicoTipo) {
		StringBuilder hql = new StringBuilder("SELECT pc ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pc ");
		hql.append(" WHERE 1=1 ");
		hql.append(" and pc.chavePrimaria = :idPontoConsumo ");
		hql.append(" and pc.chavePrimaria not in ( ");
		hql.append(" select seau.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" seau ");
		hql.append(" where 1=1 ");
		hql.append(" and seau.servicoTipo.chavePrimaria in (  ");
		hql.append(" SELECT st.chavePrimaria from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st  ");
		hql.append(" WHERE ");
		hql.append(" st.indicadorOperacaoMedidor in ( ");
		hql.append(" SELECT st1.indicadorOperacaoMedidor from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st1  ");
		hql.append(" WHERE ");
		hql.append(" st1.chavePrimaria = :idServicoTipo ");
		hql.append(") ) ");
		hql.append(" and seau.dataEncerramento is null) ");

		Query query = getQuery(hql.toString());

		query.setLong("idServicoTipo", idServicoTipo);
		query.setLong("idPontoConsumo", idPontoConsumo);

		return query.list().isEmpty();
	}
	
	public Erp consultarErpPorChaveImovel(Long chavePrimariaImovel) {
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeErp().getSimpleName());
		hql.append(" erp ");
		hql.append(" WHERE erp.imovel.chavePrimaria = :chavePrimariaImovel ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("chavePrimariaImovel", chavePrimariaImovel);

		return (Erp) query.setMaxResults(1).uniqueResult();
	} 
	
	public Collection<IndicadorEmergenciaVO> consultarIndicadorServicoTipo(String periodoCriacao, String descricaoTipoServico, Long localRede) {
		
		StringBuilder sql = new StringBuilder();

		sql.append("WITH DuracaoCalculada AS (");
		sql.append(" SELECT");
		sql.append("     sa.SEAU_DT_EXECUCAO AS dataOriginal,");
		sql.append("     TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM') AS mesAno,");
		sql.append("     COALESCE(pc.POCN_DS, 'Sem Local Definido') AS descricaoPontoConsumo,");
		if(descricaoTipoServico.equals("VAZAMENTO DE GAS")) {
		sql.append("         CASE ");
		sql.append("         WHEN sat.SATP_HORA_ATENDIMENTO IS NOT NULL ");
		sql.append("         THEN EXTRACT(DAY FROM ((TO_TIMESTAMP(TO_CHAR(CHI.CHHI_TM_ULTIMA_ALTERACAO, 'YYYY-MM-DD') || ' ' || sat.SATP_HORA_ATENDIMENTO, 'YYYY-MM-DD HH24:MI'))) - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI') )* 24 * 60 ");
		sql.append("			+ EXTRACT(HOUR FROM ((TO_TIMESTAMP(TO_CHAR(CHI.CHHI_TM_ULTIMA_ALTERACAO, 'YYYY-MM-DD') || ' ' || sat.SATP_HORA_ATENDIMENTO, 'YYYY-MM-DD HH24:MI') ) ) - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI') ) * 60 ");
		sql.append("         	+ EXTRACT(MINUTE FROM ((TO_TIMESTAMP(TO_CHAR(CHI.CHHI_TM_ULTIMA_ALTERACAO, 'YYYY-MM-DD') || ' ' || sat.SATP_HORA_ATENDIMENTO, 'YYYY-MM-DD HH24:MI') ) ) - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI') ) ");
		sql.append("         ELSE EXTRACT(DAY FROM (sat.SATP_TM_FIM - CHHI_TM_ULTIMA_ALTERACAO)) * 24 * 60 ");
		sql.append("			+ EXTRACT(HOUR FROM (sat.SATP_TM_FIM - CHHI_TM_ULTIMA_ALTERACAO)) * 60 ");
		sql.append("            + EXTRACT(MINUTE FROM (sat.SATP_TM_FIM - CHHI_TM_ULTIMA_ALTERACAO))  ");
		sql.append(" END ");
		}else {
			sql.append("          EXTRACT(DAY FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI'))) * 24 * 60 ");
			sql.append("		+ EXTRACT(HOUR FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI'))) * 60 ");
			sql.append("        + EXTRACT(MINUTE FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI')))  ");
		}		

		sql.append(" AS minutosDiferenca");
		sql.append(" FROM");
		sql.append("     SERVICO_AUTORIZACAO sa");
		sql.append(" LEFT JOIN PONTO_CONSUMO pc ON");
		sql.append("     sa.POCN_CD = pc.POCN_CD");
		sql.append(" INNER JOIN SERVICO_TIPO st ON");
		sql.append("     sa.SRTI_CD = st.SRTI_CD");
		sql.append(" INNER JOIN SERVICO_TIPO_PRIORIDADE stp ON");
		sql.append("     st.SEPI_CD = stp.SEPI_CD");
		sql.append(" INNER JOIN SERVICO_AUTORIZACAO_TEMP sat ON");
		sql.append("     sa.SEAU_CD = sat.SEAU_CD");
		sql.append(" LEFT JOIN CHAMADO CH ON");
		sql.append("     CH.CHAM_CD = SA.CHAM_CD");
		sql.append(" LEFT JOIN CHAMADO_PROTOCOLO CP ON");
		sql.append("     CP.CHPR_CD = CH.CHPR_CD");
		sql.append(" LEFT JOIN CHAMADO_HISTORICO CHI ON ");
		sql.append(" CHI.CHAM_CD = CH.CHAM_CD ");
		sql.append(" WHERE");
		sql.append("    sat.ENCO_CD_STATUS = 440 ");
		sql.append("    AND CHI.ENCO_CD_OPERACAO = 413 ");
		sql.append("    AND stp.SEPI_QN_HORAS_MAXIMA_EXECUCAO < 10");
		sql.append(" 	AND sa.SEAU_DT_EXECUCAO >= :periodoCriacaoInicio ");
		sql.append(" 	AND sa.SEAU_DT_EXECUCAO <= :periodoCriacaoFim ");
		sql.append(" 	AND st.SRTI_DS LIKE :descricaoTipoServico ");
		if(localRede != null) {
			sql.append(" AND sat.ENCO_CD_LOCAL = :localRede ");
		}else {
			sql.append(" AND sat.ENCO_CD_LOCAL IS NOT NULL ");
		}
		sql.append(")");
		sql.append(" SELECT");
		sql.append("     mesAno,");
		if(descricaoTipoServico.equals("FALTA DE GAS")) {
			sql.append("     CASE");
			sql.append("         WHEN minutosDiferenca <= 60 THEN '1h'");
			sql.append("         WHEN minutosDiferenca <= 120 THEN '2h'");
			sql.append("         WHEN minutosDiferenca <= 180 THEN '3h'");
			sql.append("         ELSE '>3h'");
		}else {
			sql.append("     CASE");
			sql.append("         WHEN minutosDiferenca <= 30 THEN 'até 0,5h'");
			sql.append("         WHEN minutosDiferenca < 60 THEN 'até 1h'");
			sql.append("         WHEN minutosDiferenca = 60 THEN '1h'");
			sql.append("         ELSE '>1h'");
		}
		sql.append("     END AS horasMaxima,");
		sql.append("     COUNT(*) AS totalRegistros");
		sql.append(" FROM");
		sql.append("     DuracaoCalculada");
		sql.append(" GROUP BY");
		sql.append("     mesAno,");
		if(descricaoTipoServico.equals("FALTA DE GAS")) {
			sql.append("     CASE");
			sql.append("         WHEN minutosDiferenca <= 60 THEN '1h'");
			sql.append("         WHEN minutosDiferenca <= 120 THEN '2h'");
			sql.append("         WHEN minutosDiferenca <= 180 THEN '3h'");
			sql.append("         ELSE '>3h'");
		}else {
			sql.append("     CASE");
			sql.append("         WHEN minutosDiferenca <= 30 THEN 'até 0,5h'");
			sql.append("         WHEN minutosDiferenca < 60 THEN 'até 1h'");
			sql.append("         WHEN minutosDiferenca = 60 THEN '1h'");
			sql.append("         ELSE '>1h'");
		}
		sql.append("     END");
		sql.append(" ORDER BY");
		sql.append("     mesAno DESC ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
		.addScalar("mesAno",StringType.INSTANCE)
		.addScalar("horasMaxima",StringType.INSTANCE)
		.addScalar("totalRegistros",IntegerType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(IndicadorEmergenciaVO.class));
		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoInicio = montarDataInicioPeriodo(periodoCriacao);
			query.setParameter("periodoCriacaoInicio", periodoCriacaoInicio);
		}		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoFim = montarDataFimPeriodo(periodoCriacao);
			query.setParameter("periodoCriacaoFim", periodoCriacaoFim);
		}		
		if(!descricaoTipoServico.isEmpty()) {
			query.setParameter("descricaoTipoServico", descricaoTipoServico);
		}		
		if(localRede != null) {
			query.setLong("localRede", localRede);
		}
		return query.list();
	}
	
	public Collection<IndicadorEmergenciaVO> consultarIndicadorServicoTipoPontosConsumo(String periodoCriacao, String descricaoTipoServico, Long localRede){
		StringBuilder sql = new StringBuilder();
		
		sql.append(" WITH TempoClassificado AS ( ");
		sql.append("     SELECT ");
		sql.append("		 sa.SEAU_DT_EXECUCAO AS dataOriginal, ");
		sql.append("         TO_CHAR(sa.SEAU_DT_EXECUCAO, 'DD-MM-YYYY') AS mesAno, ");
		sql.append("         COALESCE(pc.POCN_DS, 'Sem Local Definido') AS descricaoPontoConsumo, ");
		sql.append("         ec.ENCO_DS AS tipoInstalacao, ");
		sql.append("		 CP.CHPR_NR_PROTOCOLO AS chamadoProtocolo, ");
		if(descricaoTipoServico.equals("VAZAMENTO DE GAS")) {
		sql.append("         CASE ");
		sql.append("         WHEN sat.SATP_HORA_ATENDIMENTO IS NOT NULL ");
		sql.append("         THEN EXTRACT(DAY FROM ((TO_TIMESTAMP(TO_CHAR(CHI.CHHI_TM_ULTIMA_ALTERACAO, 'YYYY-MM-DD') || ' ' || sat.SATP_HORA_ATENDIMENTO, 'YYYY-MM-DD HH24:MI'))) - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI') )* 24 * 60 ");
		sql.append("			+ EXTRACT(HOUR FROM ((TO_TIMESTAMP(TO_CHAR(CHI.CHHI_TM_ULTIMA_ALTERACAO, 'YYYY-MM-DD') || ' ' || sat.SATP_HORA_ATENDIMENTO, 'YYYY-MM-DD HH24:MI') ) ) - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI') ) * 60 ");
		sql.append("         	+ EXTRACT(MINUTE FROM ((TO_TIMESTAMP(TO_CHAR(CHI.CHHI_TM_ULTIMA_ALTERACAO, 'YYYY-MM-DD') || ' ' || sat.SATP_HORA_ATENDIMENTO, 'YYYY-MM-DD HH24:MI') ) ) - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI') ) ");
		sql.append("         ELSE EXTRACT(DAY FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI'))) * 24 * 60 ");
		sql.append("			+ EXTRACT(HOUR FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI'))) * 60 ");
		sql.append("            + EXTRACT(MINUTE FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI')))  ");
		sql.append(" END ");
		}else {
			sql.append("         	  EXTRACT(DAY FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI'))) * 24 * 60 ");
			sql.append("			+ EXTRACT(HOUR FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI'))) * 60 ");
			sql.append("            + EXTRACT(MINUTE FROM (sat.SATP_TM_FIM - TRUNC(CHI.CHHI_TM_ULTIMA_ALTERACAO,'MI')))  ");
		}
		sql.append("     AS minutos ");
		sql.append("     FROM SERVICO_AUTORIZACAO sa ");
		sql.append("     LEFT JOIN PONTO_CONSUMO pc ON sa.POCN_CD = pc.POCN_CD  ");
		sql.append("     INNER JOIN SERVICO_TIPO st ON sa.SRTI_CD = st.SRTI_CD  ");
		sql.append("     INNER JOIN SERVICO_TIPO_PRIORIDADE stp ON st.SEPI_CD = stp.SEPI_CD  ");
		sql.append("     INNER JOIN SERVICO_AUTORIZACAO_TEMP sat ON sa.SEAU_CD = sat.SEAU_CD  ");
		sql.append(" 	 LEFT JOIN CHAMADO CH ON CH.CHAM_CD = SA.CHAM_CD ");
		sql.append(" 	 LEFT JOIN CHAMADO_PROTOCOLO CP ON CP.CHPR_CD = CH.CHPR_CD ");
		sql.append("     LEFT JOIN CHAMADO_HISTORICO CHI ON CHI.CHAM_CD = CH.CHAM_CD ");
		sql.append("     LEFT JOIN ENTIDADE_CONTEUDO ec ON sat.ENCO_CD_TIPO_INSTALACAO = ec.ENCO_CD ");
		sql.append("     WHERE  ");
		sql.append("       sat.ENCO_CD_STATUS = 440 ");
		sql.append("       AND CHI.ENCO_CD_OPERACAO = 413 ");
		sql.append("       AND stp.SEPI_QN_HORAS_MAXIMA_EXECUCAO < 10 ");
		sql.append("       AND sa.SEAU_DT_EXECUCAO >= :periodoCriacaoInicio ");
		sql.append("       AND sa.SEAU_DT_EXECUCAO <= :periodoCriacaoFim ");
		sql.append("       AND st.SRTI_DS LIKE :descricaoTipoServico ");
		if(localRede != null) {
			sql.append(" AND sat.ENCO_CD_LOCAL = :localRede ");
		}else {
			sql.append(" AND sat.ENCO_CD_LOCAL IS NOT NULL ");
		}
		sql.append(" ) " );
		sql.append(" SELECT  ");
		sql.append("     mesAno, ");
		sql.append("     minutos, ");
		sql.append("     descricaoPontoConsumo, ");
		sql.append("     tipoInstalacao, ");
		sql.append("     chamadoProtocolo ");
		sql.append(" FROM TempoClassificado ");
		sql.append(" GROUP BY mesAno, descricaoPontoConsumo,minutos, dataOriginal, tipoInstalacao, chamadoProtocolo ");
		sql.append(" ORDER BY dataOriginal ASC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
		.addScalar("mesAno", StringType.INSTANCE)
		.addScalar("minutos", IntegerType.INSTANCE)
		.addScalar("descricaoPontoConsumo", StringType.INSTANCE)
		.addScalar("tipoInstalacao",StringType.INSTANCE)
		.addScalar("chamadoProtocolo", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(IndicadorEmergenciaVO.class));
		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoInicio = montarDataInicioPeriodoMesAtual(periodoCriacao);
			query.setParameter("periodoCriacaoInicio", periodoCriacaoInicio);
		}		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoFim = montarDataFimPeriodo(periodoCriacao);
			query.setParameter("periodoCriacaoFim", periodoCriacaoFim);
		}		
		if(!descricaoTipoServico.isEmpty()) {
			query.setParameter("descricaoTipoServico", descricaoTipoServico);
		}
		if(localRede != null) {
			query.setLong("localRede", localRede);
		}
		return query.list();
	}
	
	private Date montarDataInicioPeriodo(String periodo) {
		Integer anoReferencia = 0;
		
		if(!StringUtils.isEmpty(periodo)) {
			String[] anoMes = periodo.split("/");
			anoReferencia = Integer.valueOf(anoMes[1]);
		}

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.MONTH, 0);
		calendario.set(Calendar.DAY_OF_MONTH, 1);
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		
		if(anoReferencia != null) {
			calendario.set(Calendar.YEAR, anoReferencia);
		}
		
		Date dataInicioPeriodo = calendario.getTime();
		return dataInicioPeriodo;
	}
	
	private Date montarDataFimPeriodo(String periodo) {
		Calendar calendario = Calendar.getInstance();
		String[] anoMes = periodo.split("/");
		
		
		calendario.set(Calendar.YEAR, Integer.valueOf(anoMes[1]));
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MONTH, Integer.valueOf(anoMes[0]) - 1);
		calendario.set(Calendar.DAY_OF_MONTH, Util.obterUltimoDiaDoMesParaData(new DateTime(calendario.getTime())).getDayOfMonth());
		
		
		calendario.add(Calendar.DAY_OF_YEAR, 1);
	
		return calendario.getTime();
	}
	
	private Date montarDataInicioPeriodoMesAtual(String periodo) {
		Calendar calendario = Calendar.getInstance();
		String[] anoMes = periodo.split("/");
		
		
		calendario.set(Calendar.YEAR, Integer.valueOf(anoMes[1]));
		calendario.set(Calendar.HOUR_OF_DAY, 0);
		calendario.set(Calendar.MINUTE, 0);
		calendario.set(Calendar.SECOND, 0);
		calendario.set(Calendar.MONTH, Integer.valueOf(anoMes[0]) - 1);
		
		calendario.set(Calendar.DAY_OF_MONTH, Util.obterPrimeiroDiaDoMesParaData(new DateTime(calendario.getTime())).getDayOfMonth());
			
		return calendario.getTime();
	}
	
	
	
	public Collection<IndiceVazamentoVO> consultarIndiceVazamentos(String periodoCriacao, String descricaoTipoServico, Long localRede){
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT ");
		sql.append(" TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM') AS mesAno, ");
		sql.append(" ec.ENCO_DS AS tipoInstalacao, ");
		sql.append(" COUNT(*) AS totalRegistros ");
		sql.append(" FROM SERVICO_AUTORIZACAO_TEMP sat ");
		sql.append(" INNER JOIN SERVICO_AUTORIZACAO sa ON sat.SEAU_CD = sa.SEAU_CD ");
		sql.append(" INNER JOIN SERVICO_TIPO st ON sa.SRTI_CD = st.SRTI_CD ");
		sql.append(" LEFT JOIN PONTO_CONSUMO pc ON sa.POCN_CD = pc.POCN_CD ");
		sql.append(" INNER JOIN ENTIDADE_CONTEUDO ec ON sat.ENCO_CD_TIPO_INSTALACAO = ec.ENCO_CD ");
		sql.append(" WHERE ");
		sql.append(" sat.ENCO_CD_STATUS = 440 ");
		sql.append(" AND sa.SEAU_DT_EXECUCAO >= :periodoCriacaoInicio ");
		sql.append(" AND sa.SEAU_DT_EXECUCAO <= :periodoCriacaoFim ");
		sql.append(" AND st.SRTI_DS LIKE :descricaoTipoServico ");
		if(localRede != null) {
			sql.append(" AND sat.ENCO_CD_LOCAL = :localRede ");
		}else {
			sql.append(" AND sat.ENCO_CD_LOCAL IS NOT NULL ");
		}
		sql.append(" GROUP BY TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM'),ec.ENCO_DS ");
		sql.append(" ORDER BY TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM') ASC ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
				.addScalar("mesAno", StringType.INSTANCE)
				.addScalar("tipoInstalacao", StringType.INSTANCE)
				.addScalar("totalRegistros",IntegerType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(IndiceVazamentoVO.class));
		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoInicio = montarDataInicioPeriodo(periodoCriacao);
			query.setParameter("periodoCriacaoInicio", periodoCriacaoInicio);
		}		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoFim = montarDataFimPeriodo(periodoCriacao);
			query.setParameter("periodoCriacaoFim", periodoCriacaoFim);
		}		
		if(!descricaoTipoServico.isEmpty()) {
			query.setParameter("descricaoTipoServico", descricaoTipoServico);
		}
		if(localRede != null) {
			query.setParameter("localRede", localRede);
		}
		
		return query.list();
	}
	
	public Collection<IndiceVazamentoVO> consultarIndiceVazamentosPorPontoConsumo(String periodoCriacao, String descricaoTipoServico, Long localRede){
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT ");
		sql.append(" TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM') AS mesAno, ");
		sql.append(" ec.ENCO_DS AS tipoInstalacao, ");
		sql.append(" COALESCE(pc.POCN_DS,'Sem Local Definido') AS descricaoPontoConsumo, ");
		sql.append(" CP.CHPR_NR_PROTOCOLO as chamadoProtocolo ");
		//sql.append(" COUNT(*) AS totalRegistros ");
		sql.append(" FROM SERVICO_AUTORIZACAO_TEMP sat ");
		sql.append(" INNER JOIN SERVICO_AUTORIZACAO sa ON sat.SEAU_CD = sa.SEAU_CD ");
		sql.append(" INNER JOIN SERVICO_TIPO st ON sa.SRTI_CD = st.SRTI_CD ");
		sql.append(" LEFT JOIN PONTO_CONSUMO pc ON sa.POCN_CD = pc.POCN_CD ");
		sql.append(" INNER JOIN ENTIDADE_CONTEUDO ec ON sat.ENCO_CD_TIPO_INSTALACAO = ec.ENCO_CD ");
		sql.append(" LEFT JOIN CHAMADO CH ON CH.CHAM_CD = sa.CHAM_CD ");
		sql.append(" LEFT JOIN CHAMADO_PROTOCOLO CP ON CP.CHPR_CD = CH.CHPR_CD ");
		sql.append(" WHERE ");
		sql.append(" sat.ENCO_CD_STATUS = 440 ");
		sql.append(" AND sa.SEAU_DT_EXECUCAO >= :periodoCriacaoInicio ");
		sql.append(" AND sa.SEAU_DT_EXECUCAO <= :periodoCriacaoFim ");
		sql.append(" AND st.SRTI_DS LIKE :descricaoTipoServico ");
		if(localRede != null) {
			sql.append(" AND sat.ENCO_CD_LOCAL = :localRede ");
		}else {
			sql.append(" AND sat.ENCO_CD_LOCAL IS NOT NULL ");
		}
		//sql.append(" GROUP BY TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM'),ec.ENCO_DS, pc.POCN_DS ");
		sql.append(" ORDER BY TO_CHAR(sa.SEAU_DT_EXECUCAO, 'YYYY-MM') ASC ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
				.addScalar("mesAno", StringType.INSTANCE)
				.addScalar("tipoInstalacao", StringType.INSTANCE)
				//.addScalar("totalRegistros",IntegerType.INSTANCE)
				.addScalar("descricaoPontoConsumo", StringType.INSTANCE)
				.addScalar("chamadoProtocolo", StringType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(IndiceVazamentoVO.class));
		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoInicio = montarDataInicioPeriodoMesAtual(periodoCriacao);
			query.setParameter("periodoCriacaoInicio", periodoCriacaoInicio);
		}		
		if(!periodoCriacao.isEmpty()) {
			Date periodoCriacaoFim = montarDataFimPeriodo(periodoCriacao);
			query.setParameter("periodoCriacaoFim", periodoCriacaoFim);
		}		
		if(!descricaoTipoServico.isEmpty()) {
			query.setParameter("descricaoTipoServico", descricaoTipoServico);
		}
		if(localRede != null) {
			query.setParameter("localRede", localRede);
		}
		
		return query.list();
	}

	public Collection<ServicoAutorizacaoAgenda> obterListaAgendamentosPorDiaEquipe(Date dataAgenda, Equipe equipe, Long statusASCancelada) {
		
		StringBuilder hql = new StringBuilder();
		hql.append("select servicoAgenda ");
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAgenda ");
		hql.append(" inner join servicoAgenda.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" inner join servicoAutorizacao.equipe equipe ");
		hql.append(" WHERE ");
		hql.append(" servicoAgenda.dataAgenda >= :dataAgendaInicial");
		hql.append(" and servicoAgenda.dataAgenda < :dataAgendaFinal");
		hql.append(" and servicoAgenda.indicadorConfirmado = false");		
		if (equipe != null && equipe.getChavePrimaria() > 0) {
			hql.append(" and equipe.chavePrimaria = :equipe");
		}
		hql.append(" and servicoAutorizacao.status.chavePrimaria <> :statusASCancelada");
		hql.append(" order by servicoAgenda.dataAgenda ");
		
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("dataAgendaInicial", dataAgenda);
		query.setParameter("dataAgendaFinal", DataUtil.incrementarDia(dataAgenda, 1));
		query.setParameter("statusASCancelada", statusASCancelada);
		
		if (equipe != null && equipe.getChavePrimaria() > 0) {
			query.setParameter("equipe", equipe.getChavePrimaria());
		}
		
		return query.list();
	}

	public ServicoAutorizacaoRegistroLocalImpl consultarRegistroServicoAutorizacaoRegLocal(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoRegistroLocal().getSimpleName());
		hql.append(" sarl ");
		hql.append(" WHERE sarl.chavePrimaria = :chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (ServicoAutorizacaoRegistroLocalImpl) query.setMaxResults(1).uniqueResult();
	}

	public ServicoAutorizacaoAgenda obterServicoAutorizacaoAgendaPorServicoAutorizacao(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		hql.append("select servicoAgenda ");
		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoAgenda().getSimpleName());
		hql.append(" servicoAgenda ");
		hql.append(" WHERE ");
		hql.append(" servicoAgenda.servicoAutorizacao.chavePrimaria = :chavePrimaria");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (ServicoAutorizacaoAgenda) query.uniqueResult();
	}

	public ServicoAutorizacaoTemp obterServicoAutorizacaoTemp(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();

		hql.append("FROM ");
		hql.append(getClasseEntidadeServicoAutorizacaoTemp().getSimpleName());
		hql.append(" servicoTemp ");
		hql.append(" where servicoTemp.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chavePrimaria);

		return (ServicoAutorizacaoTemp) query.uniqueResult();
	}
	
	public List<RedeComprimentoVO> consultarRedeComprimento(Integer anoFiltro){
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT ");
		sql.append(" RECO_CD AS chavePrimaria, ");
		sql.append(" RECO_MD_COMPRIMENTO AS redeComprimento, ");
		sql.append(" RECO_DT_AM_REFERENCIA AS redeReferencia ");
		sql.append(" FROM REDE_COMPRIMENTO rc ");
		sql.append(" WHERE rc.RECO_DT_AM_REFERENCIA like :anoFiltro ");
		sql.append(" ORDER BY RECO_CD ASC ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
				.addScalar("chavePrimaria", LongType.INSTANCE)
				.addScalar("redeComprimento", BigDecimalType.INSTANCE)
				.addScalar("redeReferencia", BigDecimalType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(RedeComprimentoVO.class));
		
		query.setParameter("anoFiltro", "%"+anoFiltro+"%");
		
		return query.list();
	}

	public List<ServicoAutorizacaoResumoVO> consultarServicoAutorizacaoResumo(
			ServicoAutorizacaoVO servicoAutorizacaoVO, FormatoImpressao formatoImpressao, String agrupamento,
			String exibirFiltros, Boolean isGrafico) throws FormatoInvalidoException, NegocioException{
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT SRTI_DS AS servicoTipo, "
				+ "ABERTA AS totalAberto, "
				+ "EXECUTADA AS totalExecutada, "
				+ "PENDENTE_DE_ATUALIZACAO AS totalPendenteAtualizacao, "
				+ "CANCELADA AS totalCancelada, "
				+ "ENCERRADA AS totalEncerrada, ");
		sql.append("(ABERTA + EXECUTADA + PENDENTE_DE_ATUALIZACAO + CANCELADA + ENCERRADA) AS TOTAL ");
		sql.append("FROM ( ");
		sql.append("    SELECT st.SRTI_DS, ec.ENCO_DS, sa.SEAU_CD ");
		sql.append("    FROM SERVICO_AUTORIZACAO sa ");
		sql.append("    INNER JOIN SERVICO_TIPO st ON st.SRTI_CD = sa.SRTI_CD ");
		sql.append("    INNER JOIN ENTIDADE_CONTEUDO ec ON ec.ENCO_CD = sa.ENCO_CD_STATUS ");
		sql.append("    WHERE sa.SEAU_IN_USO = 1 ");

		Date dataGeracaoInicio = null;
		DateTime dataGeracaoFim = null;

		if ((servicoAutorizacaoVO.getDataGeracaoInicio() != null && !servicoAutorizacaoVO.getDataGeracaoInicio().isEmpty())
						&& (servicoAutorizacaoVO.getDataGeracaoFim() != null && !servicoAutorizacaoVO.getDataGeracaoFim().isEmpty())) {

			dataGeracaoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataGeracaoInicio());
			Date dataGeracaoFimTemp = DataUtil.parse(servicoAutorizacaoVO.getDataGeracaoFim());

			dataGeracaoFim = new DateTime(dataGeracaoFimTemp);
			dataGeracaoFim = dataGeracaoFim.withHourOfDay(LIMITE_HORAS);
			dataGeracaoFim = dataGeracaoFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataGeracaoFim = dataGeracaoFim.withSecondOfMinute(LIMITE_MIN_SEG);
			sql.append(" AND sa.SEAU_DT_GERACAO >= :dataGeracaoInicio ");
			sql.append(" AND sa.SEAU_DT_GERACAO <= :dataGeracaoFim ");
		}
		
		Date dataPrevisaoInicio = null;
		DateTime dataPrevisaoFim = null;
		if ((servicoAutorizacaoVO.getDataPrevisaoInicio() != null && !servicoAutorizacaoVO.getDataPrevisaoInicio().isEmpty())
						&& (servicoAutorizacaoVO.getDataPrevisaoFim() != null && !servicoAutorizacaoVO.getDataPrevisaoFim().isEmpty())) {
			dataPrevisaoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataPrevisaoInicio());
			Date dataPrevisaoFimTemp = DataUtil.parse(servicoAutorizacaoVO.getDataPrevisaoFim());

			dataPrevisaoFim = new DateTime(dataPrevisaoFimTemp);
			dataPrevisaoFim = dataPrevisaoFim.withHourOfDay(LIMITE_HORAS);
			dataPrevisaoFim = dataPrevisaoFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataPrevisaoFim = dataPrevisaoFim.withSecondOfMinute(LIMITE_MIN_SEG);
			
			sql.append(" AND sa.SEAU_DT_PREVISAO_ENCERRAMENTO >= :dataPrevisaoInicio ");
			sql.append(" AND sa.SEAU_DT_PREVISAO_ENCERRAMENTO <= :dataPrevisaoFim ");
		}
		
		Date dataEncerramentoInicio = null;
		DateTime dataEncerramentoFim = null;
		if ((servicoAutorizacaoVO.getDataEncerramentoInicio() != null && !servicoAutorizacaoVO.getDataEncerramentoInicio().isEmpty())
						&& (servicoAutorizacaoVO.getDataEncerramentoFim() != null && !servicoAutorizacaoVO.getDataEncerramentoFim()
										.isEmpty())) {
			dataEncerramentoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataEncerramentoInicio());
			Date dataEncerramentoFimTemp = DataUtil.parse(servicoAutorizacaoVO.getDataEncerramentoFim());

			dataEncerramentoFim = new DateTime(dataEncerramentoFimTemp);
			dataEncerramentoFim = dataEncerramentoFim.withHourOfDay(LIMITE_HORAS);
			dataEncerramentoFim = dataEncerramentoFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataEncerramentoFim = dataEncerramentoFim.withSecondOfMinute(LIMITE_MIN_SEG);

			sql.append(" AND sa.SEAU_DT_ENCERRAMENTO >= :dataEncerramentoInicio ");
			sql.append(" AND sa.SEAU_DT_ENCERRAMENTO <= :dataEncerramentoFim ");
		}
		
		Date dataExecucaoInicio = null;
		DateTime dataExecucaoFim = null;
		if ((servicoAutorizacaoVO.getDataExecucaoInicio() != null && !servicoAutorizacaoVO.getDataExecucaoInicio().isEmpty())
				|| (servicoAutorizacaoVO.getDataExecucaoFim() != null && !servicoAutorizacaoVO.getDataExecucaoFim().isEmpty())) {

			dataExecucaoInicio = new Date(0);
			Date dataExecucaoFimTemp = new Date();

			if (servicoAutorizacaoVO.getDataExecucaoInicio() != null && !servicoAutorizacaoVO.getDataExecucaoInicio().isEmpty()) {
				dataExecucaoInicio = DataUtil.parse(servicoAutorizacaoVO.getDataExecucaoInicio());
			}
			if (servicoAutorizacaoVO.getDataExecucaoFim() != null && !servicoAutorizacaoVO.getDataExecucaoFim().isEmpty()) {
				dataExecucaoFimTemp = DataUtil.parse(servicoAutorizacaoVO.getDataExecucaoFim());
			}

			dataExecucaoFim = new DateTime(dataExecucaoFimTemp);
			dataExecucaoFim = dataExecucaoFim.withHourOfDay(LIMITE_HORAS);
			dataExecucaoFim = dataExecucaoFim.withMinuteOfHour(LIMITE_MIN_SEG);
			dataExecucaoFim = dataExecucaoFim.withSecondOfMinute(LIMITE_MIN_SEG);

			sql.append(" AND sa.SEAU_DT_EXECUCAO >= :dataExecucaoInicio ");
			sql.append(" AND sa.SEAU_DT_EXECUCAO <= :dataExecucaoFim ");
		}
		
		sql.append(") ");
		sql.append("PIVOT ( ");
		sql.append("    COUNT(SEAU_CD) ");
		sql.append("    FOR ENCO_DS IN ('ABERTA' AS ABERTA, 'EXECUTADA' AS EXECUTADA, ");
		sql.append("    'PENDENTE DE ATUALIZACAO' AS PENDENTE_DE_ATUALIZACAO, ");
		sql.append("    'CANCELADA' AS CANCELADA, 'ENCERRADA' AS ENCERRADA) ");
		sql.append(") ");
		sql.append("ORDER BY TOTAL DESC");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
		.addScalar("servicoTipo",StringType.INSTANCE)
		.addScalar("totalAberto",IntegerType.INSTANCE)
		.addScalar("totalExecutada",IntegerType.INSTANCE)
		.addScalar("totalPendenteAtualizacao",IntegerType.INSTANCE)
		.addScalar("totalCancelada",IntegerType.INSTANCE)
		.addScalar("totalEncerrada",IntegerType.INSTANCE)
		.addScalar("total",IntegerType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(ServicoAutorizacaoResumoVO.class));
		
		if(dataGeracaoInicio != null && dataGeracaoFim != null) {
			query.setParameter("dataGeracaoInicio",dataGeracaoInicio);
			query.setParameter("dataGeracaoFim",dataGeracaoFim.toDate());
		}
		if(dataPrevisaoInicio != null && dataPrevisaoFim != null) {
			query.setParameter("dataPrevisaoInicio",dataPrevisaoInicio);
			query.setParameter("dataPrevisaoFim",dataPrevisaoFim.toDate());
		}
		if(dataEncerramentoInicio != null && dataEncerramentoFim != null) {
			query.setParameter("dataEncerramentoInicio",dataEncerramentoInicio);
			query.setParameter("dataEncerramentoFim",dataEncerramentoFim.toDate());
		}
		if(dataExecucaoInicio != null && dataExecucaoFim != null) {
			query.setParameter("dataExecucaoInicio",dataExecucaoInicio);
			query.setParameter("dataExecucaoFim",dataExecucaoFim.toDate());
		}
		
		if(isGrafico) {
			query.setMaxResults(5);
		}
		
		return query.list();
	}

}
