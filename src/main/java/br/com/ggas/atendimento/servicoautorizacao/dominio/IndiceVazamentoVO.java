package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.io.Serializable;

public class IndiceVazamentoVO implements Serializable {

	private String mesAno;
	private String comprimentoRede;
	private String tipoInstalacao;
	private Integer localCRM;
	private Integer localERMP;
	private Integer localERP;
	private Integer localETC;
	private Integer localRedePEAD;
	private Integer localRedeAco;
	private Integer totalRegistros;
	private Integer mesAnoAuxiliar;
	private Integer mesMaximo;
	private String descricaoPontoConsumo;
	private String chamadoProtocolo;
	
	public String getMesAno() {
		return mesAno;
	}
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}
	public String getComprimentoRede() {
		return comprimentoRede;
	}
	public void setComprimentoRede(String comprimentoRede) {
		this.comprimentoRede = comprimentoRede;
	}
	public String getTipoInstalacao() {
		return tipoInstalacao;
	}
	public void setTipoInstalacao(String tipoInstalacao) {
		this.tipoInstalacao = tipoInstalacao;
	}
	public Integer getLocalCRM() {
		return localCRM;
	}
	public void setLocalCRM(Integer localCRM) {
		this.localCRM = localCRM;
	}
	public Integer getLocalERMP() {
		return localERMP;
	}
	public void setLocalERMP(Integer localERMP) {
		this.localERMP = localERMP;
	}
	public Integer getLocalERP() {
		return localERP;
	}
	public void setLocalERP(Integer localERP) {
		this.localERP = localERP;
	}
	public Integer getLocalETC() {
		return localETC;
	}
	public void setLocalETC(Integer localETC) {
		this.localETC = localETC;
	}
	public Integer getLocalRedePEAD() {
		return localRedePEAD;
	}
	public void setLocalRedePEAD(Integer localRedePEAD) {
		this.localRedePEAD = localRedePEAD;
	}
	public Integer getLocalRedeAco() {
		return localRedeAco;
	}
	public void setLocalRedeAco(Integer localRedeAco) {
		this.localRedeAco = localRedeAco;
	}
	public Integer getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public Integer getMesAnoAuxiliar() {
		return mesAnoAuxiliar;
	}
	public void setMesAnoAuxiliar(Integer mesAnoAuxiliar) {
		this.mesAnoAuxiliar = mesAnoAuxiliar;
	}
	public Integer getMesMaximo() {
		return mesMaximo;
	}
	public void setMesMaximo(Integer mesMaximo) {
		this.mesMaximo = mesMaximo;
	}
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public String getChamadoProtocolo() {
		return chamadoProtocolo;
	}
	public void setChamadoProtocolo(String chamadoProtocolo) {
		this.chamadoProtocolo = chamadoProtocolo;
	}
}
