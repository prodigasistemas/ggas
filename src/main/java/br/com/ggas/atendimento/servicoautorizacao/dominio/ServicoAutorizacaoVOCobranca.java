/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 08/04/2015 12:49:21
 @author crsilva
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Date;

import br.com.ggas.cobranca.avisocorte.AvisoCorte;
/**
 * Classe responsável pela representação de um objeto de valor relacionado a uma cobrança de um serviço autorizado
 */
public class ServicoAutorizacaoVOCobranca {

	private String nomeEquipeReligacao;

	private String prioridadeCorte;

	private String prioridadeReligacao;

	private String statusCorte;

	private String statusReligacao;

	private Date dataAutorizacao;

	private Date dataCorte;

	private Date dataReativacao;

	private AvisoCorte avisoCorte;

	private String nomeEquipeCorte;

	private String nomeOperadorCorte;

	private String nomeOperadorReligacao;

	private String servicoTipoCorte;

	private String servicoTipoReativacao;

	private String idPontoConsumoSelecionado;

	public String getStatusCorte() {

		return statusCorte;
	}

	public String getStatusReligacao() {

		return statusReligacao;
	}

	public void setStatusCorte(String statusCorte) {

		this.statusCorte = statusCorte;
	}

	public void setStatusReligacao(String statusReligacao) {

		this.statusReligacao = statusReligacao;
	}

	public AvisoCorte getAvisoCorte() {

		return avisoCorte;
	}

	public void setAvisoCorte(AvisoCorte avisoCorte) {

		this.avisoCorte = avisoCorte;
	}

	public Date getDataAutorizacao() {
		Date data = null;
		if (this.dataAutorizacao != null) {
			data = (Date) dataAutorizacao.clone();
		}
		return data;
	}

	public void setDataAutorizacao(Date dataAutorizacao) {
		if(dataAutorizacao != null) {
			this.dataAutorizacao = (Date) dataAutorizacao.clone();
		} else {
			this.dataAutorizacao = null;
		}
	}

	public Date getDataCorte() {
		Date data = null;
		if(this.dataCorte != null) {
			data = (Date) dataCorte.clone();
		}
		return data;
	}

	public void setDataCorte(Date dataCorte) {
		if(dataCorte != null) {
			this.dataCorte = (Date) dataCorte.clone();
		} else {
			this.dataCorte = null;
		}
	}

	public Date getDataReativacao() {
		Date data = null;
		if (this.dataReativacao != null) {
			data = (Date) dataReativacao.clone();
		}
		return data;
	}

	public void setDataReativacao(Date dataReativacao) {
		if(dataReativacao != null) {
			this.dataReativacao = (Date) dataReativacao.clone();
		} else {
			this.dataReativacao = null;
		}
	}

	public String getNomeEquipeReligacao() {

		return nomeEquipeReligacao;
	}

	public void setNomeEquipeReligacao(String nomeEquipeReligacao) {

		this.nomeEquipeReligacao = nomeEquipeReligacao;
	}

	public String getNomeEquipeCorte() {

		return nomeEquipeCorte;
	}

	public void setNomeEquipeCorte(String nomeEquipeCorte) {

		this.nomeEquipeCorte = nomeEquipeCorte;
	}

	public String getNomeOperadorCorte() {

		return nomeOperadorCorte;
	}

	public String getNomeOperadorReligacao() {

		return nomeOperadorReligacao;
	}

	public void setNomeOperadorCorte(String nomeOperadorCorte) {

		this.nomeOperadorCorte = nomeOperadorCorte;
	}

	public void setNomeOperadorReligacao(String nomeOperadorReligacao) {

		this.nomeOperadorReligacao = nomeOperadorReligacao;
	}

	public String getPrioridadeCorte() {

		return prioridadeCorte;
	}

	public String getPrioridadeReligacao() {

		return prioridadeReligacao;
	}

	public void setPrioridadeCorte(String prioridadeCorte) {

		this.prioridadeCorte = prioridadeCorte;
	}

	public void setPrioridadeReligacao(String prioridadeReligacao) {

		this.prioridadeReligacao = prioridadeReligacao;
	}

	public String getServicoTipoCorte() {

		return servicoTipoCorte;
	}

	public String getServicoTipoReativacao() {

		return servicoTipoReativacao;
	}

	public void setServicoTipoCorte(String servicoTipoCorte) {

		this.servicoTipoCorte = servicoTipoCorte;
	}

	public void setServicoTipoReativacao(String servicoTipoReativacao) {

		this.servicoTipoReativacao = servicoTipoReativacao;
	}

	public String getIdPontoConsumoSelecionado() {

		return idPontoConsumoSelecionado;
	}

	public void setIdPontoConsumoSelecionado(String idPontoConsumoSelecionado) {

		this.idPontoConsumoSelecionado = idPontoConsumoSelecionado;
	}

}
