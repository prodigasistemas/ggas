/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 11:10:45
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.negocio;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hibernate.HibernateException;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.ComunicacaoLoteVO;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AcaoServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AutorizacaoServicoLoteVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.FiltroServicoAutorizacaoLoteDTO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndicadorEmergenciaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndiceVazamentoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendasVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoEquipamento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistoricoAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMaterial;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMotivoEncerramento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoRelatorioVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTempAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.impl.ServicoAutorizacaoRegistroLocalImpl;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.batch.Processo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.relatorio.RedeComprimentoVO;
import br.com.ggas.relatorio.ServicoAutorizacaoResumoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.web.relatorio.atendimento.PesquisaRelatorioControleServicoAutorizacaoVO;

/**
 * Interface responsável pelos métodos do Controlador relacionado a uma autorização de serviço
 */
public interface ControladorServicoAutorizacao {

	String BEAN_ID_CONTROLADOR_SERVICO_AUTORIZACAO = "controladorServicoAutorizacao";

	/**
	 * Obter servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico autorizacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ServicoAutorizacao obterServicoAutorizacao(Long chavePrimaria) throws NegocioException;

	/**
	 * Listar servico autorizacao.
	 * 
	 * @param servicoAutorizacaoVO
	 *            the servico autorizacao vo
	 * @param pesquisaPopupAgendamento
	 *            the pesquisa popup agendamento
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<ServicoAutorizacao> listarServicoAutorizacao(ServicoAutorizacaoVO servicoAutorizacaoVO, boolean pesquisaPopupAgendamento)
					throws GGASException;

	/**
	 * Listar servico autorizacao historico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoAutorizacaoHistorico> listarServicoAutorizacaoHistorico(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir servico autorizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao {@link ServicoAutorizacao}
	 * @param dados
	 *            the dados {@link Map}
	 * @return the servico autorizacao
	 * @throws GGASException {@link GGASException}
	 */
	ServicoAutorizacao inserirServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados, Boolean isComunicacao) throws GGASException;

	/**
	 * Consultar servico autorizacao por cliente ou imovel.
	 * 
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorClienteOuImovel(Long chavePrimariaCliente, Long chavePrimariaImovel,
					Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Listar imoveis por cliente.
	 * 
	 * @param chaveCliente
	 *            the chave cliente
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Imovel> listarImoveisPorCliente(Long chaveCliente) throws NegocioException;

	/**
	 * Metodo para cahamadas do Remanejar.
	 * 
	 * @param servicoAutorizacao the servico autorizacao {@link ServicoAutorizacao}
	 * @param dados the dados {@link Map}
	 * @param servicoAutorizacaoChave the servicoAutorizacaoChave {@link ServicoAutorizacao}
	 * 
	 * @throws GGASException 
	 * the ggas expection {@link GGASException}
	 */
	void remanejar(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados, ServicoAutorizacao servicoAutorizacaoChave)
			throws GGASException;

	/**
	 * Atualizar servico autorizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao {@link ServicoAutorizacao}
	 * @param dados
	 *            the dados {@link Map}
	 * @param listaMateriais
	 *            the lista materiais {@link Collection}
	 * @param listaEquipamentos
	 *            the lista equipamentos {@link Collection}
	 * @return the servico autorizacao
	 * @throws GGASException 
	 *             the ggas exception {@link GGASException}
	 * @throws ParseException
	 *             the parse exception {@link ParseException}
	 */
	ServicoAutorizacao atualizarServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados,
					Collection<ServicoAutorizacaoMaterial> listaMateriais, Collection<ServicoAutorizacaoEquipamento> listaEquipamentos)
					throws GGASException, ParseException;

	/**
	 * Executar servico autorizacao.
	 * 
	 * @param servicoAutorizacao the servico autorizacao {@link ServicoAutorizacao}
	 * @param listaMateriais the lista materiais {@link Collection}
	 * @param listaEquipamentos the lista equipamentos {@link Collection}
	 * @param tipoStatus the tipo status {@link String}
	 * @param dados the dados {@link Map}
	 * @param listaPergunta the lista pergunta {@link List}
	 * @throws GGASException {@link GGASException}
	 */
	void executarServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, Collection<ServicoAutorizacaoMaterial> listaMateriais,
			Collection<ServicoAutorizacaoEquipamento> listaEquipamentos, String tipoStatus, Map<String, Object> dados,
			List<QuestionarioPergunta> listaPergunta) throws GGASException;

	/**
	 * Listar equipe servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<ServicoAutorizacao> listarEquipeServicoAutorizacao(long chavePrimaria);

	/**
	 * Verificar status servico autorizacao.
	 * 
	 * @param servico
	 *            the servico
	 * @param tipoOperacao
	 *            the tipo operacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarStatusServicoAutorizacao(ServicoAutorizacao servico, String tipoOperacao) throws NegocioException;

	/**
	 * Verificar entidade relacionada tela atualizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param telaAtualizacao
	 *            the tela atualizacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarEntidadeRelacionadaTelaAtualizacao(ServicoAutorizacao servicoAutorizacao, String telaAtualizacao) throws NegocioException;

	/**
	 * Obter servico autorizacao motivo encerramento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico autorizacao motivo encerramento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ServicoAutorizacaoMotivoEncerramento obterServicoAutorizacaoMotivoEncerramento(Long chavePrimaria) throws NegocioException;

	/**
	 * Listar servico autorizacao motivo encerramento.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> listarServicoAutorizacaoMotivoEncerramento() throws NegocioException;

	/**
	 * Validar motivo encerramento.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarMotivoEncerramento(ServicoAutorizacao servicoAutorizacao) throws NegocioException;

	/**
	 * Encerrar servico autorizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param listaMateriais
	 *            the lista materiais
	 * @param listaEquipamentos
	 *            the lista equipamentos
	 * @param tipoStatus
	 *            the tipo status
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws UnknownHostException
	 *             the unknown host exception
	 */
	void encerrarServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, Collection<ServicoAutorizacaoMaterial> listaMateriais,
					Collection<ServicoAutorizacaoEquipamento> listaEquipamentos, String tipoStatus, Map<String, Object> dados)
					throws GGASException, UnknownHostException;

	/**
	 * Obter previsao encerramento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the string[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ParseException
	 *             the parse exception
	 */
	String[] obterPrevisaoEncerramento(Long chavePrimaria) throws NegocioException, ParseException;

	/**
	 * Gerar autorizacao servico lote.
	 * 
	 * @param idComando A acao comando
	 * @param logProcessamento the log processamento
	 * @param dados the dados
	 * @throws GGASException the ggas exception
	 * @throws IOException 
	 */
	void gerarAutorizacaoServicoLote(long idComando, StringBuilder logProcessamento, Map<String, Object> dados) throws GGASException, IOException;
	
	/**
	 * Gerar relatorio.
	 * 
	 * @param chavePrimariaServicoAutorizacao the chave primaria servico autorizacao {@link Long}
	 * @param comChamado {@link Boolean}
	 * @param historico {@link Collection}
	 * @return the byte[] {@link Byte}
	 * @throws GGASException the GGAS exception {@link GGASException}
	 */
	byte[] gerarRelatorio(Long chavePrimariaServicoAutorizacao, Boolean comChamado, Collection<ServicoAutorizacaoHistorico> historico)
			throws GGASException;

	/**
	 * Validar encerramento servico autorizacao.
	 * 
	 * @param servico
	 *            the servico
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarEncerramentoServicoAutorizacao(ServicoAutorizacao servico) throws NegocioException;

	/**
	 * Consultar Agendamentos
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO {@link ServicoAutorizacaoAgendaPesquisaVO}
	 * @param chaveServicoTipo {@link Long}
	 * @return Lista de Agendamentos. {@link List}
	 * @throws GGASException 
	 * @throws NumberFormatException 
	 */
	List<ServicoAutorizacaoAgendaVO> consultarAgendamentos(ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO,
			Long chaveServicoTipo) throws NumberFormatException, GGASException;

	/**
	 * Consultar servico autorizacao agenda.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO
	 *            the servico autorizacao agenda pesquisa vo {@link ServicoAutorizacaoAgendaPesquisaVO}
	 * @return the list {@link List}
	 * @throws GGASException {@link GGASException}
	 */
	List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgenda(ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO)
			throws GGASException;

	/**
	 * Remover agendamento.
	 * 
	 * @param chaveAgendamento
	 *            the chave agendamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAgendamento(Long chaveAgendamento) throws NegocioException;

	/**
	 * Adicionar servico autorizacao agendamento.
	 * 
	 * @param chaves
	 *            the chaves {@link Long}
	 * @param servicoAutorizacaoAgendaPesquisaVO {@link ServicoAutorizacaoAgendaPesquisaVO}
	 *            the servico autorizacao agenda pesquisa vo
	 * @throws GGASException {@link GGASException}
	 * @throws IOException 
	 */
	void adicionarServicoAutorizacaoAgendamento(Long[] chaves, ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, Boolean isServicoPrioritario)
					throws GGASException, IOException;

	/**
	 * Obter servico autorizacao agenda.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico autorizacao agenda
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ServicoAutorizacaoAgenda obterServicoAutorizacaoAgenda(Long chavePrimaria) throws NegocioException;

	/**
	 * Consultar agendamentos disponiveis turno.
	 * 
	 * @param tipo
	 *            the tipo
	 * @param dataAgenda
	 *            the data agenda
	 * @return the list
	 */
	public List<ServicoTipoAgendamentoTurno> consultarAgendamentosDisponiveisTurno(ServicoTipo tipo, Date dataAgenda);

	/**
	 * Gerar relatorio agendamento.
	 * 
	 * @param servicoAutorizacaoAgendaPesquisaVO
	 *            the servico autorizacao agenda pesquisa vo {@link ServicoAutorizacaoAgendaPesquisaVO}
	 * @return the byte[] {@link Byte}
	 * @throws GGASException {@link GGASException}
	 */
	byte[] gerarRelatorioAgendamento(ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws GGASException;

	/**
	 * Atualizar servico autorizacao agenda.
	 * 
	 * @param agenda
	 *            the agenda {@link ServicoAutorizacaoAgenda}
	 * @throws GGASException
	 *             the ggas exception {@link GGASException}
	 */
	void atualizarServicoAutorizacaoAgenda(ServicoAutorizacaoAgenda agenda) throws GGASException;

	/**
	 * Consultar servico autorizacao agenda por ponto consumo.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @return the list
	 */
	List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgendaPorPontoConsumo(Long chavePontoConsumo);

	/**
	 * Alterar servico autorizacao em execucao.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias {@link Long}
	 * @param dados
	 *            the dados {@link Map}
	 * @throws GGASException
	 *             the ggas exception {@link GGASException}
	 */
	void alterarServicoAutorizacaoEmExecucao(Long[] chavesPrimarias, Map<String, Object> dados) throws GGASException;

	/**
	 * Consultar servico autorizacao por comando acao.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorComandoAcao(AcaoComando acaoComando) throws NegocioException;

	/**
	 * Consultar servico autorizacao por chamado.
	 * 
	 * @param chavePrimariaChamado
	 *            the chave primaria chamado
	 * @return the servico autorizacao
	 */
	ServicoAutorizacao consultarServicoAutorizacaoPorChamado(Long chavePrimariaChamado);

	/**
	 * Obter materias ordenado.
	 * 
	 * @param chavesServicoAutorizacaoMaterial
	 *            the chaves servico autorizacao material
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoAutorizacaoMaterial> obterMateriasOrdenado(Long[] chavesServicoAutorizacaoMaterial) throws NegocioException;

	/**
	 * Atualizar sem validar.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao {@link ServicoAutorizacao}
	 * @throws GGASException
	 *             the ggas exception {@link GGASException}
	 */
	void atualizarSemValidar(ServicoAutorizacao servicoAutorizacao) throws GGASException;

	/**
	 * Obter servico autorizacao historico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico autorizacao historico
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ServicoAutorizacaoHistorico obterServicoAutorizacaoHistorico(Long chavePrimaria) throws NegocioException;

	/**
	 * Gerar relatorio controle servico autorizacao.
	 * 
	 * @param pesquisaRelatorioControleServicoAutorizacaoVO
	 *            the pesquisa relatorio controle servico autorizacao vo
	 * @param formatoImpressao
	 *            the formato impressao
	 * @return the byte[]
	 * @throws HibernateException
	 *             the hibernate exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public byte[] gerarRelatorioControleServicoAutorizacao(
					PesquisaRelatorioControleServicoAutorizacaoVO pesquisaRelatorioControleServicoAutorizacaoVO,
					FormatoImpressao formatoImpressao) throws HibernateException, GGASException;

	/**
	 * Obter servico autorizacao.
	 * 
	 * @param pontoConsumo {@link Long}
	 * @param dtavisoCorte {@link Date}
	 * @param idServicoTipo {@link Long}
	 * @param listaChavePontoConsumo {@link List}
	 * @param statusCancelado {@link EntidadeConteudo}
	 * @return Lista de Autorização de Serviço {@link Collection}
	 */
	Collection<ServicoAutorizacao> obterServicoAutorizacao(Long pontoConsumo, Date dtavisoCorte, Long idServicoTipo,
			List<Long> listaChavePontoConsumo, EntidadeConteudo statusCancelado);

	/**
	 * Consulta Autorizaão de serviço agendada por tipo de serviço.
	 * 
	 * @param chaveServicoTipo - {@link Long}
	 * @return lista de ServicoAutorizacaoAgenda - {@link List}
	 */
	List<ServicoAutorizacaoAgenda> consultarServicoAutorizacaoAgendaEmAndamento(Long chaveServicoTipo);

	/**
	 * Consultar servico autorizacao por chamado.
	 * 
	 * @param chavePrimariaChamado A chave primaria chamado
	 * @return lista de servico autorizacao
	 */
	public List<ServicoAutorizacao> consultarServicoAutorizacaoPorChaveChamado(Long chavePrimariaChamado);

	/**
	 * Obtem Servico Autorizacao Historico Anexo pela chave primaria 
	 *
	 * @param chavePrimaria - {@link Long}
	 * @return ServicoAutorizacaoHistoricoAnexo - {@link ServicoAutorizacaoHistoricoAnexo}
	 * @throws NegocioException the negocio exception
	 */
	ServicoAutorizacaoHistoricoAnexo obterServicoAutorizacaoHistoricoAnexo(Long chavePrimaria) throws NegocioException;

	/**
	 * Valida dados do Servico Autorizacao Historico Anexo
	 * 
	 * @param servicoAutorizacaoHistoricoAnexo - {@link ServicoAutorizacaoHistoricoAnexo}
	 * @throws NegocioException the Negocio Exception
	 */
	void validarDadosAnexo(ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo) throws NegocioException;

	/**
	 * Verifica a descricao do Servico Autorizacao Historico Anexo
	 * 
	 * @param indexLista - {@link Integer}
	 * @param listaAnexo - {@link ServicoAutorizacaoHistoricoAnexo}
	 * @param servicoAutorizacaoHistoricoAnexo - {@link ServicoAutorizacaoHistoricoAnexo}
	 * @throws NegocioException the negocio Exception
	 */
	void verificarDescricaoAnexo(Integer indexLista, Collection<ServicoAutorizacaoHistoricoAnexo> listaAnexo,
			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo) throws NegocioException;

	/**
	 * Verifica se existe questionario para a Autorização Serviço
	 * 
	 * @param questionario - {@link Questionario}
	 * @return true se existir, false caso contrario
	 */
	boolean existeASQuestionario(Questionario questionario);

	/**
	 * Verificar se existe Serviço autorização
	 * @param chavePrimariaCliente id do cliente
	 * @param chavePrimariaServicoTipo id do tipo de serviço
	 * @param chaveImovel id do imovel
	 * @return retorna se possui autorizacao de serviço ou não
	 */
	Optional<ServicoAutorizacao> consultarServicoAutorizacaoReferenciaGarantia(Long chavePrimariaCliente, Long chavePrimariaServicoTipo,
			Long chaveImovel);

	/**
	 * Verificar o número de execulões por cliente, serviço tipo e ponto de consumo
	 * @param chavePrimariaCliente id do cliente
	 * @param chavePrimariaServicoTipo id do tipo de serviço
	 * @param chaveImovel id do imovel
	 * @param dataGarantia data da garantia
	 * @return retorna o numero de execucoes
	 */
	Long consultarNumeroExecucoesPorClienteEServicoTipo(Long chavePrimariaCliente, Long chavePrimariaServicoTipo, Long chaveImovel,
			Date dataGarantia);

	/**
	 * Lista os agendamentos de um cliente
	 * @param cliente
	 * 			the cliente
	 * @return retorna lista de Agendamentos do Cliente
	 */
	List<ServicoAutorizacaoAgendasVO> listarAgandamentosPorCliente(Cliente cliente);

	/**
	 * Obter o conteudo para a elaboração do email
	 * @param acao the acao
	 * @param servico the servico
	 * @param servicoAutorizacaoAgendaPesquisaVO the servicoAutorizacaoAgendaPesquisaVO
	 * @return retorna verdadeiro ou falso
	 * @throws GGASException the GGASException
	 */
	Boolean enviarEmailAgendamentoServico(AcaoServicoAutorizacao acao, ServicoAutorizacao servico,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws GGASException;

	/**
	 * Metodo para priorizar corte
	 * 
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @return colecao aviso corte priorizado - {@link AvisoCorte}
	 * @throws NegocioException the negocio exception
	 */
	Collection<AvisoCorte> priorizacaoCorte(Collection<AvisoCorte> listaAvisoCorte) throws NegocioException;
	
	/**
	 * Metodo de checagem de AS de CORTE
	 * 
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @return colecao aviso corte - {@link AvisoCorte}
	 * @throws GGASException - {@link GGASException}
	 */
	Collection<AvisoCorte> checagemExecucaoServicoCorte(Collection<AvisoCorte> listaAvisoCorte) throws GGASException;

	/**
	 * Obtem AS pelo Motivo de Encerramento e Pela descriao
	 * 
	 * @param descricao - {@link String}
	 * @return ServicoAutorizacaoMotivoEncerramento - {@link ServicoAutorizacaoMotivoEncerramento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	ServicoAutorizacaoMotivoEncerramento obterServicoAutorizacaoMotivoEncerramentoPelaDescricao(String descricao)
			throws NegocioException;

	/**
	 * Metodo para roteirizar corte
	 * @param listaAvisoCorte - {@link AvisoCorte}
	 * @return Colecao aviso corte roteirizada - {@link AvisoCorte}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<AvisoCorte> roteirizacaoCorte(Collection<AvisoCorte> listaAvisoCorte) throws NegocioException;

	/**
	 * Método responsável para emitir as Autorizacoes de Servico em Lote via processo batch 
	 * 
	 * @param avisos {@link Collection<AvisoCorte>}
	 * @param usuario {@link Usuario}
	 * @param dadosAuditoria {@link DadosAuditoria}
	 * @throws GGASException - {@link GGASException}
	 * @return Processo - {@link Processo}
	 */
	Processo inserirProcessoEmLoteParaCorte(Collection<AvisoCorte> avisos, Usuario usuario,
			DadosAuditoria dadosAuditoria) throws GGASException;
	
	/**
	 * Metodo responsavel por emitir as Autorizacoes de Servico em Lote via processo batch, 
	 * de Comunicacao de Servico 
	 * 
	 * @param item {@link  }
	 * @param dados {@link Map<String, Object>}
	 * @throws NegocioException - {@link NegocioException}
	 * @return autorizacoes - {@link Collection<ServicoAutorizacaoVO>}
	 */
	Collection<ServicoAutorizacaoVO> gerarAutorizacaoServicoItemComunicacao(ItemLoteComunicacao item, Map<String, Object> dados, 
			Map<String, Collection<String>> logSucessoErro) throws NegocioException;
	
	/**
	 * Metodo responsavel por agendar uma autorizacao de servico de um item de comunicacao 
	 * 
	 * @param item {@link ItemLoteComunicacao }
	 * @throws GGASException - {@link GGASException}
	 */
	void agendarAutorizacaoServicoItemComunicacao(ItemLoteComunicacao item)
			throws GGASException;

	/**
	 * Gerar Relatorio Listagem AS Batch
	 * 
	 * @param listagemAS - {@link ServicoAutorizacaoVO}
	 * @param dados - {@link Map<String, Object>}
	 * @throws IOException - {@link IOException}
	 * @throws GGASException - {@link GGASException}
	 */
	void gerarRelatorioListagemAS(Collection<ServicoAutorizacaoVO> listagemAS, Map<String, Object> dados)
			throws IOException, GGASException;

	/**
	 * Listar usuarios que ja solicitaram autorizacoes de servico
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Usuario> listarUsuariosSolcitantesAutorizacaoServico() throws NegocioException;

	Collection<ServicoTipoAgendamentoTurno> listaTurnoPorEquipeServicoTipo(Long chaveEquipe);
	/**
	 * Lista AS Abertas
	 * @return the collection
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	Collection<ServicoAutorizacao> listarServicoAutorizacaoPendente() throws NegocioException;

	Collection<EquipeTurnoItem> consultarEscalaDisponivel(Equipe equipe, EntidadeConteudo turno,
			String dataSelecionada) throws GGASException;

	Collection<ServicoAutorizacaoAgenda> obterListaAgendamentosPorDiaturnoServicoTipoEquipe(Date dataAgendamento,
			EntidadeConteudo turno, ServicoTipo servicoTipo, Equipe equipe);
	
	/**
	 * Listar as AS's que serao encerradas automaticamente pelo APP
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoAutorizacaoTemp> listarAutorizacoesServicoEncerramentoAPP();
	
	/**
	 * Método responsável os retornar as informacoes mais atuais da AS executada no app 
	 * 
	 * @param autorizacaoServico {@link ServicoAutorizacao}
	 * @throws GGASException - {@link GGASException}
	 * @return ServicoAutorizacaoTemp - {@link ServicoAutorizacaoTemp}
	 */
	ServicoAutorizacaoTemp consultarAutorizacaoServicoTemp(ServicoAutorizacao autorizacaoServico);
	
	/**
	 * Listar as os anexos das AS's que serao encerradas automaticamente pelo APP
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoAutorizacaoTempAnexo> listarAnexosAutorizacaoServicoEncerramentoAPP(ServicoAutorizacao autorizacaoServico);

	/**
	 * 
	 * @param servicoAutorizacaoTemp
	 * @throws ConcorrenciaException
	 * @throws NegocioException
	 */
	void atualizarServicoAutorizacaoTempSemValidar(ServicoAutorizacaoTemp servicoAutorizacaoTemp)
			throws ConcorrenciaException, NegocioException;

	ServicoAutorizacaoTempAnexo obterServicoAutorizacaoTempAnexo(Long chavePrimaria) throws NegocioException;

	ServicoAutorizacaoTempAnexo consultarAssinaturaAnexoAutorizacaoServicoTemp(ServicoAutorizacao servicoAutorizacao)
			throws NegocioException;
	
	Boolean roteirizarServicoAutorizacaoAgendamento(Long[] chaves,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO, Boolean isSimulacao, Collection<ServicoAutorizacaoAgenda> servicoAgendaSimulado) throws NegocioException, IOException, GGASException;

	Collection<EntidadeConteudo> consultaTurnosEquipeEscala(Equipe equipe, String dataAgenda) throws GGASException;

	ServicoAutorizacaoTempAnexo consultarFotoCRM(ServicoAutorizacao servicoAutorizacao) throws NegocioException;

	void roteirizarServicoAutorizacaoAgendamentoCorte(Long[] chaves,
			ServicoAutorizacaoAgendaPesquisaVO servicoAutorizacaoAgendaPesquisaVO) throws NegocioException, IOException, GGASException;

	Collection<PontoConsumo> consultarPontosLoteAutorizacaoServico(
			FiltroServicoAutorizacaoLoteDTO filtroServicoAutorizacaoLoteDTO) throws HibernateException, GGASException;

	Collection<AutorizacaoServicoLoteVO> roterizacaoAutorizacaoServico(Collection<PontoConsumo> listaPontoConsumo)
			throws NegocioException, IOException;
	
	void calcularDistanciaPontoConsumo(AutorizacaoServicoLoteVO autorizacaoServicoLote) throws NegocioException, IOException;

	List<byte[]> gerarLote(Collection<AutorizacaoServicoLoteVO> listaAutorizacaoServicoLote, ServicoTipo servicoTipo, Usuario usuario, LoteComunicacao lote) throws NegocioException, GGASException;

	Collection<PontoConsumo> filtrarPontosParaLoteDeAutorizacaoServico(String descricaoPonto, String codigoLegado,
			String servicoTipo, Integer offset);

	List<Object[]> consultarLocalizacaoOperadorMobile(Long idFuncionario);

	Boolean verificarAsAbertaMesmaOperacao(Long idPontoConsumo, Long idServicoTipo);
	
	void enviarEmailEncerramentoApp(AcaoServicoAutorizacao acao, ServicoAutorizacao servicoAutorizacao) throws GGASException;

	LeituraMovimento pesquisarLeituraMovimentoCicloMedicao(PontoConsumo pontoConsumo, GrupoFaturamento grupo);

	boolean isServicoTipoSubstituicaoMedidor(ServicoTipo servicoTipo);
	
	Erp consultarErpPorChaveImovel(Long chavePrimariaImovel);

	/**
	 * Verifica se o chamado da autorização de serviço deve ser encerrado.
	 *
	 * @param ServicoAutorizacao the servicoAutorizacao
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	void verificaEncerramentoChamado(ServicoAutorizacao servicoAutorizacao, Map<String, Object> dados)
			throws GGASException;
	
	Collection<IndicadorEmergenciaVO> consultarIndicadorServicoTipo(String periodoCriacao, String descricaoTipoServico, Long localRede);
	
	Collection<IndicadorEmergenciaVO> consultarIndicadorServicoTipoPontosConsumo(String periodoCriacao, String descricaoTipoServico, Long localRede);
	
	byte[] gerarRelatorioIndicadorEmergencia(FormatoImpressao formatoImpressao, String exibirFiltros, String periodoCriacao, String descricaoTipoServico, Long redeLocal) throws GGASException;

	Collection<IndiceVazamentoVO> consultarIndiceVazamentos(String periodoCriacao, String descricaoTipoServico, Long localRede);
	
	Collection<IndiceVazamentoVO> consultarIndiceVazamentosPorPontoConsumo(String periodoCriacao, String descricaoTipoServico, Long localRede);
	
	byte[] gerarRelatorioIndiceVazamento(FormatoImpressao formatoImpressao, String exibirFiltros, String periodoCriacao, String descricaoTipoServico, Long localRede) throws GGASException;

	Collection<ServicoAutorizacaoAgenda> obterListaAgendamentosPorDiaEquipe(Date data, Equipe equipe, Long statusASCancelada);

	Collection<ServicoAutorizacao> consultarServicoAutorizacaoPorChavesPrimarias(Long[] chaves);

	/**
	 * 
	 * @param servicoAutorizacaoRegistroLocal
	 * @throws ConcorrenciaException
	 * @throws NegocioException
	 */
	void atualizar(ServicoAutorizacaoRegistroLocalImpl servicoAutorizacaoRegistroLocal) throws ConcorrenciaException, NegocioException;
		
	/**
	 * 
	 * @param chavePrimaria
	 * @throws NegocioException
	 * @return ServicoAutorizacaoRegistroLocal
	 */
	ServicoAutorizacaoRegistroLocalImpl consultarRegistroServicoAutorizacaoRegLocal(Long chavePrimaria) throws NegocioException;

	ServicoAutorizacaoAgenda obterServicoAutorizacaoAgendaPorServicoAutorizacao(Long chavePrimaria)
			throws NegocioException;

	ServicoAutorizacaoTemp obterServicoAutorizacaoTemp(Long chavePrimaria);

	void atualizarServicoAutorizacaoTemp(ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws GGASException;
	
	List<RedeComprimentoVO> consultarRedeComprimento(Integer anoFiltro);

	List<ServicoAutorizacaoResumoVO> consultarServicoAutorizacaoResumo(ServicoAutorizacaoVO servicoAutorizacaoVO,
			FormatoImpressao formatoImpressao, String agrupamento, String exibirFiltros, Boolean isGrafico) throws GGASException;

	void enviarEmailPersonalizado(ServicoAutorizacao servico, ParametroSistema emailRemetentePadrao,
			JavaMailUtil mailUtil) throws NegocioException, GGASException;
}
