package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ServicoAutorizacaoRegistroLocal extends EntidadeNegocio {
	
	public static String BEAN_ID_SERVICO_AUTORIZACAO_REGISTRO_LOCAL = "servicoAutorizacaoRegistroLocal";

	String getAndroidId();

	void setAndroidId(String androidId);

	Date getData();

	void setData(Date data);

	String getLocalizacao();

	void setLocalizacao(String localizacao);

	ServicoAutorizacao getServicoAutorizacao();

	void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao);

	EntidadeConteudo getTipoLocalizacao();

	void setTipoLocalizacao(EntidadeConteudo tipoLocalizacao);

	Funcionario getFuncionario();

	void setFuncionario(Funcionario funcionario);
	
	Long getPrevisaoChegada();
	
	void setPrevisaoChegada(Long previsaoChegada);

}