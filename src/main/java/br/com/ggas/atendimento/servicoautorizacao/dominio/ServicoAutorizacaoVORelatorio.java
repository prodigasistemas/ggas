/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/01/2014 14:57:02
 @author vtavares
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Collection;
import java.util.List;

import br.com.ggas.atendimento.chamado.dominio.ChamadoVORelatatorio;
import br.com.ggas.atendimento.material.dominio.MaterialVO;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoVO;
import br.com.ggas.web.faturamento.fatura.FaturaVO;
import br.com.ggas.webservice.ClienteFoneTO;

/**
 * Classe responsável pela representação de um objeto de valor relacionado a um relatório da autorização de um serviço
 */
public class ServicoAutorizacaoVORelatorio {

	private String codigo;

	private String dataGeracao;

	private String dataEncerramento;

	private String servicoTipo;

	private String dataPrevisaoEncerramento;

	private String prioridadeServicoTipo;

	private String equipe;

	private String protocoloChamado;

	private String status;

	private String descricao;

	private String nomeCliente;

	private String nomeFantasiaCliente;

	private String foneCliente;

	private String cpfCnpj;

	private String passaporte;

	private String numeroContrato;

	private String matriculaImovel;

	private String nomeFantasiaImovel;

	private String cepImovel;

	private String enderecoImovel;

	private String descricaoPontoConsumo;
	
	private String nomePontoConsumo;

	private List<MaterialVO> listaMaterial;

	private Collection<ClienteFoneTO> listaClienteFoneTO;

	private List<EntidadeConteudoVO> listaEquipamento;

	private List<ServicoAutorizacaoHistoricoVO> listaServicoAutorizacaoHistorico;

	private String turno;

	private String dataAgendamento;

	private String horario;

	private String confirmado;

	private String complementoImovel;

	private Boolean comChamado;

	private List<ChamadoVORelatatorio> listaChamadoHistorico;

	private String nomeQuestionario;

	private Collection<QuestionarioVO> listaPerguntasVO;

	private Collection<QuestionarioVO> listaRespostasVO;

	private String nomeExecutante;
	
	private String codigoUnico;
	
	private String medidorSerie;
	
	private String medidorModelo;
	
	private String medidorTipo;
	
	private String medidorFabricante;

	private String ultimaLeitura;
	
	private String servicoOrientacao;
	
	private String agendamento;
	
	private String pressaoContrato;
	
	private Collection<FaturaVO> faturasVencidas;
	
	private String vazaoContrato;
	
	private String novoMedidorSerie;
	
	private String novoMedidorModelo;
	
	private String novoMedidorTipo;
	
	private String novoMedidorFabricante;
	
	private String mensagemUrgencia;
	
	private String placaBP;
	
	private String numeroOS;
	
	private String mensagemDebito;
	
	private String numeroLeituraNovo;
	
	private String numeroLeituraOriginal;
	
	private String numeroLacreMedidorNovo1;
	
	private String numeroLacreMedidorNovo2;
	
	private String numeroLacreMedidorNovo3;

	private String nome;
	
	private String localizacaoGeografica;
	
	private String medidorNovo;
	
	private String fimExecucao;
	
	private String inicioExecucao;
	
	private String ocorrencia;
	
	private String causaAtendimento;
	
	private String descricaoGrauRisco;
	
	private String localRede;
	
	private String solucaoAtendimento;
	
	private String novoPlacaBP;
	
	private String dataChamado;
	
	private String horaAtendimento;
	
	private String localColeta;
	
	private String localEntrega;
	
	private String dataProgramada;
	
	private String coletaDataHora;
	
	private String coletaData;
	
	private String coletaPressao;
	
	private String coletaLacre;
	
	private String cilindro1;
	
	private String cilindro2;
	
	private String cilindro3;
	
	private String cilindro4;
	
	private String atividadeExecutada;
	
	private String cargo;
	
	private String motivoEncerramento;
	
	private String situacao;
	
	private Boolean isOperacaoSubstituicao;

	public String getDataGeracao() {

		return dataGeracao;
	}

	public void setDataGeracao(String dataGeracao) {

		this.dataGeracao = dataGeracao;
	}

	public String getDataEncerramento() {

		return dataEncerramento;
	}

	public void setDataEncerramento(String dataEncerramento) {

		this.dataEncerramento = dataEncerramento;
	}

	public String getServicoTipo() {

		return servicoTipo;
	}

	public void setServicoTipo(String servicoTipo) {

		this.servicoTipo = servicoTipo;
	}

	public String getDataPrevisaoEncerramento() {

		return dataPrevisaoEncerramento;
	}

	public void setDataPrevisaoEncerramento(String dataPrevisaoEncerramento) {

		this.dataPrevisaoEncerramento = dataPrevisaoEncerramento;
	}

	public String getPrioridadeServicoTipo() {

		return prioridadeServicoTipo;
	}

	public void setPrioridadeServicoTipo(String prioridadeServicoTipo) {

		this.prioridadeServicoTipo = prioridadeServicoTipo;
	}

	public String getEquipe() {

		return equipe;
	}

	public void setEquipe(String equipe) {

		this.equipe = equipe;
	}

	public String getProtocoloChamado() {

		return protocoloChamado;
	}

	public void setProtocoloChamado(String protocoloChamado) {

		this.protocoloChamado = protocoloChamado;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getNomeFantasiaCliente() {

		return nomeFantasiaCliente;
	}

	public void setNomeFantasiaCliente(String nomeFantasiaCliente) {

		this.nomeFantasiaCliente = nomeFantasiaCliente;
	}

	public String getFoneCliente() {

		return foneCliente;
	}

	public void setFoneCliente(String foneCliente) {

		this.foneCliente = foneCliente;
	}

	public String getCpfCnpj() {

		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {

		this.cpfCnpj = cpfCnpj;
	}

	public String getPassaporte() {

		return passaporte;
	}

	public void setPassaporte(String passaporte) {

		this.passaporte = passaporte;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getMatriculaImovel() {

		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {

		this.matriculaImovel = matriculaImovel;
	}

	public String getNomeFantasiaImovel() {

		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {

		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getCepImovel() {

		return cepImovel;
	}

	public void setCepImovel(String cepImovel) {

		this.cepImovel = cepImovel;
	}

	public String getEnderecoImovel() {

		return enderecoImovel;
	}

	public void setEnderecoImovel(String enderecoImovel) {

		this.enderecoImovel = enderecoImovel;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public List<MaterialVO> getListaMaterial() {

		return listaMaterial;
	}

	public void setListaMaterial(List<MaterialVO> listaMaterial) {

		this.listaMaterial = listaMaterial;
	}

	public List<EntidadeConteudoVO> getListaEquipamento() {

		return listaEquipamento;
	}

	public void setListaEquipamento(List<EntidadeConteudoVO> listaEquipamento) {

		this.listaEquipamento = listaEquipamento;
	}

	public List<ServicoAutorizacaoHistoricoVO> getListaServicoAutorizacaoHistorico() {

		return listaServicoAutorizacaoHistorico;
	}

	public void setListaServicoAutorizacaoHistorico(List<ServicoAutorizacaoHistoricoVO> listaServicoAutorizacaoHistorico) {

		this.listaServicoAutorizacaoHistorico = listaServicoAutorizacaoHistorico;
	}

	public String getTurno() {

		return turno;
	}

	public void setTurno(String turno) {

		this.turno = turno;
	}

	public String getDataAgendamento() {

		return dataAgendamento;
	}

	public void setDataAgendamento(String dataAgendamento) {

		this.dataAgendamento = dataAgendamento;
	}

	public String getHorario() {

		return horario;
	}

	public void setHorario(String horario) {

		this.horario = horario;
	}

	public String getConfirmado() {

		return confirmado;
	}

	public void setConfirmado(String confirmado) {

		this.confirmado = confirmado;
	}

	public String getComplementoImovel() {

		return complementoImovel;
	}

	public void setComplementoImovel(String complementoImovel) {

		this.complementoImovel = complementoImovel;
	}

	public Boolean getComChamado() {
		return comChamado;
	}

	public void setComChamado(Boolean comChamado) {
		this.comChamado = comChamado;
	}

	public List<ChamadoVORelatatorio> getListaChamadoHistorico() {
		return listaChamadoHistorico;
	}

	public void setListaChamadoHistorico(List<ChamadoVORelatatorio> listaChamadoHistorico) {
		this.listaChamadoHistorico = listaChamadoHistorico;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Collection<ClienteFoneTO> getListaClienteFoneTO() {
		return listaClienteFoneTO;
	}

	public void setListaClienteFoneTO(Collection<ClienteFoneTO> listaClienteFoneTO) {
		this.listaClienteFoneTO = listaClienteFoneTO;
	}

	public String getNomeQuestionario() {
		return nomeQuestionario;
	}

	public void setNomeQuestionario(String nomeQuestionario) {
		this.nomeQuestionario = nomeQuestionario;
	}

	public Collection<QuestionarioVO> getListaPerguntasVO() {
		return listaPerguntasVO;
	}

	public void setListaPerguntasVO(Collection<QuestionarioVO> listaPerguntasVO) {
		this.listaPerguntasVO = listaPerguntasVO;
	}

	public Collection<QuestionarioVO> getListaRespostasVO() {
		return listaRespostasVO;
	}

	public void setListaRespostasVO(Collection<QuestionarioVO> listaRespostasVO) {
		this.listaRespostasVO = listaRespostasVO;
	}

	public String getNomeExecutante() {
		return nomeExecutante;
	}

	public void setNomeExecutante(String nomeExecutante) {
		this.nomeExecutante = nomeExecutante;
	}

	public String getCodigoUnico() {
		return codigoUnico;
	}

	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}

	public String getMedidorSerie() {
		return medidorSerie;
	}

	public void setMedidorSerie(String medidorSerie) {
		this.medidorSerie = medidorSerie;
	}

	public String getMedidorModelo() {
		return medidorModelo;
	}

	public void setMedidorModelo(String medidorModelo) {
		this.medidorModelo = medidorModelo;
	}

	public String getMedidorTipo() {
		return medidorTipo;
	}

	public void setMedidorTipo(String medidorTipo) {
		this.medidorTipo = medidorTipo;
	}

	public String getServicoOrientacao() {
		return servicoOrientacao;
	}

	public void setServicoOrientacao(String servicoOrientacao) {
		this.servicoOrientacao = servicoOrientacao;
	}

	public String getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(String agendamento) {
		this.agendamento = agendamento;
	}

	public Collection<FaturaVO> getFaturasVencidas() {
		return faturasVencidas;
	}

	public void setFaturasVencidas(Collection<FaturaVO> faturasVencidas) {
		this.faturasVencidas = faturasVencidas;
	}

	public String getPressaoContrato() {
		return pressaoContrato;
	}

	public void setPressaoContrato(String pressaoContrato) {
		this.pressaoContrato = pressaoContrato;
	}

	public String getNomePontoConsumo() {
		return nomePontoConsumo;
	}

	public void setNomePontoConsumo(String nomePontoConsumo) {
		this.nomePontoConsumo = nomePontoConsumo;
	}

	public String getMedidorFabricante() {
		return medidorFabricante;
	}

	public void setMedidorFabricante(String medidorFabricante) {
		this.medidorFabricante = medidorFabricante;
	}

	public String getUltimaLeitura() {
		return ultimaLeitura;
	}

	public void setUltimaLeitura(String ultimaLeitura) {
		this.ultimaLeitura = ultimaLeitura;
	}

	public String getVazaoContrato() {
		return vazaoContrato;
	}

	public void setVazaoContrato(String vazaoContrato) {
		this.vazaoContrato = vazaoContrato;
	}

	public String getNovoMedidorSerie() {
		return novoMedidorSerie;
	}

	public void setNovoMedidorSerie(String novoMedidorSerie) {
		this.novoMedidorSerie = novoMedidorSerie;
	}

	public String getNovoMedidorModelo() {
		return novoMedidorModelo;
	}

	public void setNovoMedidorModelo(String novoMedidorModelo) {
		this.novoMedidorModelo = novoMedidorModelo;
	}

	public String getNovoMedidorTipo() {
		return novoMedidorTipo;
	}

	public void setNovoMedidorTipo(String novoMedidorTipo) {
		this.novoMedidorTipo = novoMedidorTipo;
	}

	public String getNovoMedidorFabricante() {
		return novoMedidorFabricante;
	}

	public void setNovoMedidorFabricante(String novoMedidorFabricante) {
		this.novoMedidorFabricante = novoMedidorFabricante;
	}

	public String getMensagemUrgencia() {
		return mensagemUrgencia;
	}

	public void setMensagemUrgencia(String mensagemUrgencia) {
		this.mensagemUrgencia = mensagemUrgencia;
	}

	public String getPlacaBP() {
		return placaBP;
	}

	public void setPlacaBP(String placaBP) {
		this.placaBP = placaBP;
	}
	
	public String getNumeroOS() {
		return numeroOS;
	}

	public void setNumeroOS(String numeroOS) {
		this.numeroOS = numeroOS;
	}

	public String getMensagemDebito() {
		return mensagemDebito;
	}

	public void setMensagemDebito(String mensagemDebito) {
		this.mensagemDebito = mensagemDebito;
	}

	public String getNumeroLeituraNovo() {
		return numeroLeituraNovo;
	}

	public void setNumeroLeituraNovo(String numeroLeituraNovo) {
		this.numeroLeituraNovo = numeroLeituraNovo;
	}

	public String getNumeroLeituraOriginal() {
		return numeroLeituraOriginal;
	}

	public void setNumeroLeituraOriginal(String numeroLeituraOriginal) {
		this.numeroLeituraOriginal = numeroLeituraOriginal;
	}

	public String getNumeroLacreMedidorNovo1() {
		return numeroLacreMedidorNovo1;
	}

	public void setNumeroLacreMedidorNovo1(String numeroLacreMedidorNovo1) {
		this.numeroLacreMedidorNovo1 = numeroLacreMedidorNovo1;
	}

	public String getNumeroLacreMedidorNovo2() {
		return numeroLacreMedidorNovo2;
	}

	public void setNumeroLacreMedidorNovo2(String numeroLacreMedidorNovo2) {
		this.numeroLacreMedidorNovo2 = numeroLacreMedidorNovo2;
	}

	public String getNumeroLacreMedidorNovo3() {
		return numeroLacreMedidorNovo3;
	}

	public void setNumeroLacreMedidorNovo3(String numeroLacreMedidorNovo3) {
		this.numeroLacreMedidorNovo3 = numeroLacreMedidorNovo3;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocalizacaoGeografica() {
		return localizacaoGeografica;
	}

	public void setLocalizacaoGeografica(String localizacaoGeografica) {
		this.localizacaoGeografica = localizacaoGeografica;
	}

	public String getMedidorNovo() {
		return medidorNovo;
	}

	public void setMedidorNovo(String medidorNovo) {
		this.medidorNovo = medidorNovo;
	}

	public String getFimExecucao() {
		return fimExecucao;
	}

	public void setFimExecucao(String fimExecucao) {
		this.fimExecucao = fimExecucao;
	}

	public String getInicioExecucao() {
		return inicioExecucao;
	}

	public void setInicioExecucao(String inicioExecucao) {
		this.inicioExecucao = inicioExecucao;
	}
	
	public String getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public String getCausaAtendimento() {
		return causaAtendimento;
	}

	public void setCausaAtendimento(String causaAtendimento) {
		this.causaAtendimento = causaAtendimento;
	}

	public String getDescricaoGrauRisco() {
		return descricaoGrauRisco;
	}

	public void setDescricaoGrauRisco(String descricaoGrauRisco) {
		this.descricaoGrauRisco = descricaoGrauRisco;
	}

	public String getLocalRede() {
		return localRede;
	}

	public void setLocalRede(String localRede) {
		this.localRede = localRede;
	}

	public String getSolucaoAtendimento() {
		return solucaoAtendimento;
	}

	public void setSolucaoAtendimento(String solucaoAtendimento) {
		this.solucaoAtendimento = solucaoAtendimento;
	}

	public String getNovoPlacaBP() {
		return novoPlacaBP;
	}

	public void setNovoPlacaBP(String novoPlacaBP) {
		this.novoPlacaBP = novoPlacaBP;
	}

	public String getDataChamado() {
		return dataChamado;
	}

	public void setDataChamado(String dataChamado) {
		this.dataChamado = dataChamado;
	}

	public String getHoraAtendimento() {
		return horaAtendimento;
	}

	public void setHoraAtendimento(String horaAtendimento) {
		this.horaAtendimento = horaAtendimento;
	}

	public String getLocalColeta() {
		return localColeta;
	}

	public void setLocalColeta(String localColeta) {
		this.localColeta = localColeta;
	}

	public String getLocalEntrega() {
		return localEntrega;
	}

	public void setLocalEntrega(String localEntrega) {
		this.localEntrega = localEntrega;
	}

	public String getDataProgramada() {
		return dataProgramada;
	}

	public void setDataProgramada(String dataProgramada) {
		this.dataProgramada = dataProgramada;
	}

	public String getColetaDataHora() {
		return coletaDataHora;
	}

	public void setColetaDataHora(String coletaDataHora) {
		this.coletaDataHora = coletaDataHora;
	}

	public String getColetaPressao() {
		return coletaPressao;
	}

	public void setColetaPressao(String coletaPressao) {
		this.coletaPressao = coletaPressao;
	}

	public String getColetaLacre() {
		return coletaLacre;
	}

	public void setColetaLacre(String coletaLacre) {
		this.coletaLacre = coletaLacre;
	}

	public String getCilindro1() {
		return cilindro1;
	}

	public void setCilindro1(String cilindro1) {
		this.cilindro1 = cilindro1;
	}

	public String getCilindro2() {
		return cilindro2;
	}

	public void setCilindro2(String cilindro2) {
		this.cilindro2 = cilindro2;
	}

	public String getCilindro3() {
		return cilindro3;
	}

	public void setCilindro3(String cilindro3) {
		this.cilindro3 = cilindro3;
	}

	public String getCilindro4() {
		return cilindro4;
	}

	public void setCilindro4(String cilindro4) {
		this.cilindro4 = cilindro4;
	}

	public String getAtividadeExecutada() {
		return atividadeExecutada;
	}

	public void setAtividadeExecutada(String atividadeExecutada) {
		this.atividadeExecutada = atividadeExecutada;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getMotivoEncerramento() {
		return motivoEncerramento;
	}

	public void setMotivoEncerramento(String motivoEncerramento) {
		this.motivoEncerramento = motivoEncerramento;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getColetaData() {
		return coletaData;
	}

	public void setColetaData(String coletaData) {
		this.coletaData = coletaData;
	}

	public Boolean getIsOperacaoSubstituicao() {
		return isOperacaoSubstituicao;
	}

	public void setIsOperacaoSubstituicao(Boolean isOperacaoSubstituicao) {
		this.isOperacaoSubstituicao = isOperacaoSubstituicao;
	}
}
