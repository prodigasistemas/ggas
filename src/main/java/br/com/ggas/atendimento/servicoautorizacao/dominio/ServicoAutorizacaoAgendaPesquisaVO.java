/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *05/02/2014
 * vpessoa
 * 10:26:43
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.io.Serializable;
import java.util.List;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.EntidadeConteudo;

/**
 * Classe ServicoAutorizacaoAgendaPesquisaVO
 */
public class ServicoAutorizacaoAgendaPesquisaVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5252318403472802322L;

	private ServicoTipo servicoTipo;

	private int mes;

	private int ano;

	private EntidadeConteudo turno;

	private Integer pagina;

	private String horario;

	private Long chavePrimaria;
	
	private String dataSelecionada;
	
	private Equipe equipe;
	
    private List<Long> chavesPrimarias;

	public ServicoTipo getServicoTipo() {

		return servicoTipo;
	}

	public void setServicoTipo(ServicoTipo servicoTipo) {

		this.servicoTipo = servicoTipo;
	}

	public int getMes() {

		return mes;
	}

	public void setMes(int mes) {

		this.mes = mes;
	}

	public int getAno() {

		return ano;
	}

	public void setAno(int ano) {

		this.ano = ano;
	}

	public EntidadeConteudo getTurno() {

		return turno;
	}

	public void setTurno(EntidadeConteudo turno) {

		this.turno = turno;
	}

	public Integer getPagina() {

		return pagina;
	}

	public void setPagina(Integer pagina) {

		this.pagina = pagina;
	}

	public String getHorario() {

		return horario;
	}

	public void setHorario(String horario) {

		this.horario = horario;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getDataSelecionada() {
		return dataSelecionada;
	}

	public void setDataSelecionada(String dataSelecionada) {
		this.dataSelecionada = dataSelecionada;
	}

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

    public List<Long> getChavesPrimarias() {
        return chavesPrimarias;
    }

    public void setChavesPrimarias(List<Long> chavesPrimarias) {
        this.chavesPrimarias = chavesPrimarias;
    }
}
