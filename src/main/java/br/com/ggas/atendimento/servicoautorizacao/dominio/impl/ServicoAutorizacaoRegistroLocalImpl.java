/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoRegistroLocal;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação de uma Autorização de Serviço Temp
 *
 */
public class ServicoAutorizacaoRegistroLocalImpl extends EntidadeNegocioImpl implements ServicoAutorizacaoRegistroLocal{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3349541089364919221L;
	
	private String androidId;
	private Date data;
	private String localizacao;
	private ServicoAutorizacao servicoAutorizacao;
	private EntidadeConteudo tipoLocalizacao;
	private Funcionario funcionario;
	private Long previsaoChegada;

	@Override
	public String getAndroidId() {
		return androidId;
	}
	
	@Override
	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}

	@Override
	public Date getData() {
		return data;
	}

	@Override
	public void setData(Date data) {
		this.data = data;
	}

	@Override
	public String getLocalizacao() {
		return localizacao;
	}

	@Override
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	@Override
	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}

	@Override
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
	}

	@Override
	public EntidadeConteudo getTipoLocalizacao() {
		return tipoLocalizacao;
	}

	@Override
	public void setTipoLocalizacao(EntidadeConteudo tipoLocalizacao) {
		this.tipoLocalizacao = tipoLocalizacao;
	}

	@Override
	public Funcionario getFuncionario() {
		return funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getPrevisaoChegada() {
		return previsaoChegada;
	}

	@Override
	public void setPrevisaoChegada(Long previsaoChegada) {
		this.previsaoChegada = previsaoChegada;
	}
	
}



