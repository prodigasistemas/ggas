package br.com.ggas.atendimento.servicoautorizacao.dominio.impl;

import java.util.Map;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTempAnexo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class ServicoAutorizacaoTempAnexoImpl  extends EntidadeNegocioImpl implements ServicoAutorizacaoTempAnexo{

	
	private static final long serialVersionUID = 3183910110842535928L;

	private String descricao;
	private String caminho;
	private ServicoAutorizacao servicoAutorizacao;
	private String nome;

	@Override
	public String getDescricao() {
		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String getCaminho() {
		return caminho;
	}

	@Override
	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	@Override
	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}

	@Override
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNome() {
		return nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
		
	}
	
	

}
