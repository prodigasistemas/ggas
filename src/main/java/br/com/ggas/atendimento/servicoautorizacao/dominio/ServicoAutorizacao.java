/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 11:13:51
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;



/**
 * Classe responsável pela representação de uma Autorização de Serviço
 *
 */
public class ServicoAutorizacao extends EntidadeNegocioImpl {

	public static String BEAN_ID_SERVICO_AUTORIZACAO = "servicoAutorizacao";
	
	private static final int LIMITE_CAMPO = 2;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1301569523238471519L;

	private Date dataGeracao;

	private ServicoTipo servicoTipo;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date dataPrevisaoEncerramento;

	private Equipe equipe;

	private String descricao;

	private Chamado chamado;

	private Chamado chamadoTipoPesquisa;

	private Contrato contrato;

	private Cliente cliente;

	private Questionario questionarioPergunta;

	private Imovel imovel;

	private PontoConsumo pontoConsumo;

	private ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento;

	private Date dataEncerramento;

	private Usuario usuarioSistema;

	private EntidadeConteudo status;

	private ServicoTipoPrioridade servicoTipoPrioridade;

	private QuestionarioPergunta questionarioPerguntaServico;

	private Collection<ServicoAutorizacaoHistoricoAnexo> anexos = new HashSet<>();

	private Collection<ServicoAutorizacaoHistorico> servicoAutorizacaoHistoricos = new HashSet<>();

	private Collection<ServicoAutorizacaoMaterial> servicoAutorizacaoMaterial = new HashSet<>();

	private Collection<ServicoAutorizacaoEquipamento> servicoAutorizacaoEquipamento = new HashSet<>();

	private Collection<ClienteFone> servicoAutorizacaoClienteTelefone = new HashSet<>();

	private Collection<QuestionarioPergunta> listaPerguntasQuestionario = new HashSet<>();

	private Collection<QuestionarioAlternativa> listaAlternativasQuestionario = new HashSet<>();

	private Date dataSolicitacaoRamal;

	private Boolean indicadorSucessoExecucao;

	private Boolean indicadorReferenciaGarantia;

	private Funcionario executante;
	
	private Medidor medidor;
	
	private Long numeroProtocolo;
	
	private Long tempoDistancia;

	private Long numeroOS;
	
	private PontoConsumo pontoConsumoRoteirizacao;
	
	
	/** The Constant EM_EXECUCAO. */
	public static final String EM_EXECUCAO = "EM EXECUCAO";
	
	/** The Constant ABERTA. */
	public static final String ABERTA = "ABERTA";
	
	/** The Constant PENDENTE_DE_ATUALIZACAO. */
	public static final String PENDENTE_DE_ATUALIZACAO = "PENDENTE DE ATUALIZACAO";

	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date dataExecucao;
	
	private String dataExecucaoFormatadaTexto;
	
	private EntidadeConteudo turnoPreferencia;
	
	private ServicoAutorizacaoTemp servicoAutorizacaoTemp;
	
	private ServicoAutorizacaoAgenda servicoAutorizacaoAgenda;
	
	public String getDataExecucaoFormatadaTexto() {
		if(dataExecucao != null) {
			return DataUtil.converterDataParaString(dataExecucao,Boolean.TRUE);
		}else {
			return dataExecucaoFormatadaTexto;
		}
	}

	public void setDataExecucaoFormatadaTexto(String dataExecucaoFormatadaTexto) {
		this.dataExecucaoFormatadaTexto = dataExecucaoFormatadaTexto;
	}

	/**
	 * @return servicoAutorizacaoMaterial
	 */
	public Collection<ServicoAutorizacaoMaterial> getServicoAutorizacaoMaterial() {

		return servicoAutorizacaoMaterial;
	}

	/**
	 * @param servicoAutorizacaoMaterial
	 */
	public void setServicoAutorizacaoMaterial(Collection<ServicoAutorizacaoMaterial> servicoAutorizacaoMaterial) {

		this.servicoAutorizacaoMaterial = servicoAutorizacaoMaterial;
	}

	/**
	 * @return servicoAutorizacaoEquipamento
	 */
	public Collection<ServicoAutorizacaoEquipamento> getServicoAutorizacaoEquipamento() {

		return servicoAutorizacaoEquipamento;
	}


	/**
	 * @param servicoAutorizacaoEquipamento
	 */
	public void setServicoAutorizacaoEquipamento(Collection<ServicoAutorizacaoEquipamento> servicoAutorizacaoEquipamento) {

		this.servicoAutorizacaoEquipamento = servicoAutorizacaoEquipamento;
	}

	/**
	 * @return servicoAutorizacaoClienteTelefone
	 */
	public Collection<ClienteFone> getServicoAutorizacaoClienteTelefone() {

		return servicoAutorizacaoClienteTelefone;
	}

	/**
	 * @param servicoAutorizacaoClienteTelefone
	 */
	public void setServicoAutorizacaoClienteTelefone(Collection<ClienteFone> servicoAutorizacaoClienteTelefone) {

		this.servicoAutorizacaoClienteTelefone = servicoAutorizacaoClienteTelefone;
	}

	/**
	 * @return servicoAutorizacaoHistoricos
	 */
	public Collection<ServicoAutorizacaoHistorico> getServicoAutorizacaoHistoricos() {

		return servicoAutorizacaoHistoricos;
	}

	/**
	 * @param servicoAutorizacaoHistoricos
	 */
	public void setServicoAutorizacaoHistoricos(Collection<ServicoAutorizacaoHistorico> servicoAutorizacaoHistoricos) {

		this.servicoAutorizacaoHistoricos = servicoAutorizacaoHistoricos;
	}

	/**
	 * @return dataGeracao
	 */
	public Date getDataGeracao() {
		Date dataGeracaoTmp = null;
		if(dataGeracao != null){
			dataGeracaoTmp = (Date) dataGeracao.clone();
		}
		return dataGeracaoTmp;
	}

	/**
	 * @param dataGeracao
	 */
	public void setDataGeracao(Date dataGeracao) {
		if(dataGeracao != null){
			this.dataGeracao = (Date) dataGeracao.clone();
		} else {
			this.dataGeracao = null;
		}
	}

	/**
	 * @return servicoTipo
	 */
	public ServicoTipo getServicoTipo() {

		return servicoTipo;
	}

	/**
	 * @param servicoTipo
	 */
	public void setServicoTipo(ServicoTipo servicoTipo) {

		this.servicoTipo = servicoTipo;
	}

	/**
	 * @return dataPrevisaoEncerramento
	 */
	public Date getDataPrevisaoEncerramento() {
		Date dataPrevisaoEncerramentoTmp = null;
		if(dataPrevisaoEncerramento != null){
			dataPrevisaoEncerramentoTmp = (Date) dataPrevisaoEncerramento.clone();
		}
		return dataPrevisaoEncerramentoTmp;
	}

	/**
	 * @param dataPrevisaoEncerramento
	 */
	public void setDataPrevisaoEncerramento(Date dataPrevisaoEncerramento) {
		if(dataPrevisaoEncerramento != null){
			this.dataPrevisaoEncerramento = (Date) dataPrevisaoEncerramento.clone();
		} else {
			this.dataPrevisaoEncerramento = null;
		}
	}

	/**
	 * @return equipe
	 */
	public Equipe getEquipe() {

		return equipe;
	}

	/**
	 * @param equipe
	 */
	public void setEquipe(Equipe equipe) {

		this.equipe = equipe;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @return servicoAutorizacaoMotivoEncerramento
	 */
	public ServicoAutorizacaoMotivoEncerramento getServicoAutorizacaoMotivoEncerramento() {

		return servicoAutorizacaoMotivoEncerramento;
	}

	/**
	 * @param servicoAutorizacaoMotivoEncerramento
	 */
	public void setServicoAutorizacaoMotivoEncerramento(ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento) {

		this.servicoAutorizacaoMotivoEncerramento = servicoAutorizacaoMotivoEncerramento;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return chamado
	 */
	public Chamado getChamado() {

		return chamado;
	}

	/**
	 * @param chamado
	 */
	public void setChamado(Chamado chamado) {

		this.chamado = chamado;
	}

	/**
	 * @return contrato
	 */
	public Contrato getContrato() {

		return contrato;
	}

	/**
	 * @param contrato
	 */
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/**
	 * @return cliente
	 */
	public Cliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 */
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return imovel
	 */
	public Imovel getImovel() {

		return imovel;
	}

	/**
	 * @param imovel
	 */
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/**
	 * @return pontoConsumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return dataEncerramento
	 */
	public Date getDataEncerramento() {
		Date dataEncerrar = null;
		if(this.dataEncerramento != null) {
			dataEncerrar = (Date) this.dataEncerramento.clone();
		}
		return dataEncerrar;
	}
	
	/**
	 * @param dataEncerramento
	 */
	public void setDataEncerramento(Date dataEncerramento) {
		if (dataEncerramento != null) {
			this.dataEncerramento = (Date) dataEncerramento.clone();
		} else {
			this.dataEncerramento = null;
		}
	}

	/**
	 * @return usuarioSistema
	 */
	public Usuario getUsuarioSistema() {

		return usuarioSistema;
	}

	/**
	 * @param usuarioSistema
	 */
	public void setUsuarioSistema(Usuario usuarioSistema) {

		this.usuarioSistema = usuarioSistema;
	}

	/**
	 * @return status
	 */
	public EntidadeConteudo getStatus() {

		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/**
	 * @return data geração formatada
	 */
	public String getDataGeracaoFormatada() {

		return DataUtil.converterDataParaString(this.dataGeracao, Boolean.TRUE);
	}

	/**
	 * @return data previsão encerramento formatada
	 */
	public String getDataPrevisaoEncerramentoFormatada() {

		return DataUtil.converterDataParaString(this.dataPrevisaoEncerramento, Boolean.TRUE);
	}

	/**
	 * @return data previsão encerramento formatada, utilizada na seguinte página exibirPesquisaServicoAutorizacao.jsp
	 */
	public String getApenasDataPrevisaoEncerramentoFormatada() {

		return Util.converterDataParaString(this.dataPrevisaoEncerramento);
	}

	/**
	 * @return encerramento
	 */
	public String getDataEncerramentoFormatada() {

		if(dataEncerramento != null) {
			return DataUtil.converterDataParaString(this.dataEncerramento, Boolean.TRUE);
		}
		return null;
	}

	/**
	 * @return data solicitação formatada
	 */
	public String getDataSolicitacaoRamalFormatada() {

		if(this.dataSolicitacaoRamal != null) {
			return DataUtil.converterDataParaString(this.dataSolicitacaoRamal, Boolean.TRUE);
		} else {
			return "";
		}
	}
	
	/**
	 * @return data execução formatada
	 */
	public String getDataExecucaoFormatada() {

		if(this.dataExecucao != null) {
			return DataUtil.converterDataParaStringSemSegundos(this.dataExecucao);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(descricao == null || StringUtils.isEmpty(descricao) || descricao.trim().length() == 0) {
			stringBuilder.append(Constantes.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(dataPrevisaoEncerramento == null) {
			stringBuilder.append(Constantes.DATA_PREVISAO_ENCERRAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(equipe == null) {
			stringBuilder.append(Constantes.EQUIPE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(servicoTipo == null) {
			stringBuilder.append(Constantes.TIPO_SERVICO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			stringBuilder.append(Constantes.PRIORIDADE_TIPO_SERVICO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(servicoTipo != null) {
			if(this.servicoTipo.getIndicadorClienteObrigatorio() && this.cliente == null) {
				stringBuilder.append(Constantes.CHAMADO_CLIENTE_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.servicoTipo.getIndicadorImovelObrigatorio() && this.imovel == null) {
				stringBuilder.append(Constantes.CHAMADO_IMOVEL_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.servicoTipo.getIndicadorContratoObrigatorio() && this.contrato == null) {
				stringBuilder.append(Constantes.SERVICO_AUTORIZACAO_CONTRATO_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.servicoTipo.getIndicadorPontoConsumoObrigatorio() && this.pontoConsumo == null) {
				stringBuilder.append(Constantes.SERVICO_AUTORIZACAO_PONTO_CONSUMO_OBRIGATORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * @return chamadoTipoPesquisa
	 */
	public Chamado getChamadoTipoPesquisa() {

		return chamadoTipoPesquisa;
	}

	/**
	 * @param chamadoTipoPesquisa
	 */
	public void setChamadoTipoPesquisa(Chamado chamadoTipoPesquisa) {

		this.chamadoTipoPesquisa = chamadoTipoPesquisa;
	}

	/**
	 * @return servicoTipoPrioridade
	 */
	public ServicoTipoPrioridade getServicoTipoPrioridade() {

		return servicoTipoPrioridade;
	}

	/**
	 * @param servicoTipoPrioridade
	 */
	public void setServicoTipoPrioridade(ServicoTipoPrioridade servicoTipoPrioridade) {

		this.servicoTipoPrioridade = servicoTipoPrioridade;
	}

	/**
	 * @return the anexos
	 */
	public Collection<ServicoAutorizacaoHistoricoAnexo> getAnexos() {
		return anexos;
	}

	/**
	 * @param anexos the anexos to set
	 */
	public void setAnexos(Collection<ServicoAutorizacaoHistoricoAnexo> anexos) {
		this.anexos = anexos;
	}

	/**
	 * @return dataSolicitacaoRamal
	 */
	public Date getDataSolicitacaoRamal() {

		return dataSolicitacaoRamal;
	}

	/**
	 * @param dataSolicitacaoRamal
	 */
	public void setDataSolicitacaoRamal(Date dataSolicitacaoRamal) {
		if(dataSolicitacaoRamal != null){
			this.dataSolicitacaoRamal = (Date) dataSolicitacaoRamal.clone();
		} else {
			this.dataSolicitacaoRamal = null;
		}
	}

	/**
	 * @return dataExecucao
	 */
	public Date getDataExecucao() {
		if (this.dataExecucao != null) {
			 return new Date(this.dataExecucao.getTime());
		}
		return null;
	}

	/**
	 * @param dataExecucao
	 */
	public void setDataExecucao(Date dataExecucao) {
		if (dataExecucao != null) {
			this.dataExecucao = new Date(dataExecucao.getTime());
		} else {
			this.dataExecucao = null;
		}
	}
	
	public String getCondeudoEmail() {
		
		DateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		StringBuilder email = new StringBuilder();

		email.append("<html>");
		email.append("<p><strong>Código da AS:</strong> ").append(this.getChavePrimaria()).append("</p>");
		if (this.getChamado() != null) {
			email.append("<p><strong>Número protocolo do chamado:</strong> ").append(this.getChamado().getProtocolo().getNumeroProtocolo())
					.append("</p>");
		}
		email.append("<p><strong>Data de Geração:</strong> ").append(formato.format(this.getDataGeracao()))
				.append("</p>");
		email.append("<p><strong>Data previsão encerramento:</strong> ").append(formato.format(this.getDataPrevisaoEncerramento()))
				.append("</p>");

		if (this.getEquipe() != null) {
			email.append("<p><strong>Equipe responsável:</strong> ").append(this.getEquipe().getNome()).append("</p>");
		}

		email.append("<p><strong>Status:</strong> ").append(this.getStatus().getDescricao()).append("</p>");
		email.append("<p><strong>Tipo de Serviço:</strong> ").append(this.getServicoTipo().getDescricao()).append("</p>");
			
		if (this.getCliente() != null) {
			email.append("<p><strong>Nome do Cliente:</strong> ").append(this.getCliente().getNome()).append("</p>");
		}
		if (this.getPontoConsumo() != null) {
			email.append("<p><strong>Descrição Ponto de Consumo:</strong> ").append(this.getPontoConsumo().getDescricao()).append("</p>");
		}
		email.append("<p><strong>Descrição:</strong> ").append(this.getDescricao()).append("</p>");
		email.append("</html>");
		return email.toString();
	}
	
	
	public String getConteudoEncerramentoAutorizacaoServicoCliente() {
		
		ControladorConstanteSistema controladorConstanteSistema =
				(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder email = new StringBuilder();
		String logoAlgas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_URL_IMAGEM_LOGO_ALGAS);//"https://i.postimg.cc/KzHYVZzL/logo-Algas.jpg";
		String imgInfoPublica = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_URL_IMAGEM_INFORMACAO_PUBLICA);//"https://i.postimg.cc/PNnHPpWx/informacao-publica.jpg";
		String pontoConsumoNomeMunicipio = "";
		String uf = "";
		String pontoConsumoDescricao = "";
		String servicoTipoDescricao = "";
		String bairro = "";
		String numeroImovel = "";
		String logradouro = "";
		String tipoLogradouro = "";
		
        Date data = new Date();

        SimpleDateFormat formato = new SimpleDateFormat("d 'de' MMMM 'de' yyyy", new Locale("pt", "BR"));
        String dataFormatada = formato.format(data);
		
		if(this.getPontoConsumo()!= null) {
			numeroImovel = this.getPontoConsumo().getNumeroImovel();
			pontoConsumoDescricao = this.getPontoConsumo().getDescricao();
			if(this.getPontoConsumo().getCep() != null) {
				pontoConsumoNomeMunicipio = this.getPontoConsumo().getCep().getNomeMunicipio();
				uf = this.getPontoConsumo().getCep().getUf();
				bairro = this.getPontoConsumo().getCep().getBairro();
				logradouro = this.getPontoConsumo().getCep().getLogradouro();
				tipoLogradouro = this.getPontoConsumo().getCep().getTipoLogradouro();
			}
		}
		
		if(this.getServicoTipo()!= null) {
			servicoTipoDescricao = this.getServicoTipo().getDescricao();
		}
		
		email.append("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"  <meta charset=\"utf-8\">\r\n" + 
				"  <title>Email com Imagens</title>\r\n" + 
				"</head>\r\n" + 
				"<body>\r\n" + 
				"  <table border=\"1\" cellspacing=\"3\" cellpadding=\"0\" width=\"100%\" style=\"width:100%; mso-cellspacing:2.2pt; mso-yfti-tbllook:1184; mso-padding-alt:0cm 0cm 0cm 0cm;\">\r\n" + 
				"    <tr style=\"mso-yfti-irow:0; mso-yfti-firstrow:yes; mso-yfti-lastrow:yes;\">\r\n" + 
				"      <td style=\"padding:.75pt .75pt .75pt .75pt;\">\r\n" + 
				"        <div style=\"margin-left:5px; padding:10px; text-align: center;\">\r\n" + 
				"          <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n" + 
				"            <tr>\r\n" + 
				"              <td style=\"text-align: left;\">\r\n" + 
				"				 <figure>" +
				"                	<img src=\""+logoAlgas+"\" alt=\"Imagem 1\" height=\"80\" width=\"214\" style=\"border:0;\">\r\n" + 
				"                   <figcaption></figcaption>" +
				"				 </figure>" +
				"              </td>\r\n" + 
//				"              <td style=\"text-align: right;\">\r\n" + 
//				"				 <figure>" +
//				"                	<img src=\""+imgInfoPublica+"\" alt=\"Imagem 2\" height=\"80\" width=\"214\" style=\"border:0;\">\r\n" + 
//				"                   <figcaption style='text-align:right; margin-right:10px;' size=\"2\">" + pontoConsumoNomeMunicipio + " - " + uf + ", "+ dataFormatada +"</figcaption>" +					
//				"				 </figure>" +
//				"              </td>\r\n" + 
				"            </tr>\r\n" + 
				"          </table>\r\n" + 
				"        </div>\r\n" + 
				"        <table style=\"width:100%;\">\r\n" + 
//				"          <tr>\r\n" + 
//				"            <th style=\"background:#0066CC; text-align:left; padding:7.5pt 3.75pt 7.5pt 22.5pt;\">\r\n" + 
//				"              <font color=\"white\" size=\"4\">Algas/Comunicação</font>\r\n" + 
//				"            </th>\r\n" + 
//				"          </tr>\r\n" + 
				"        </table>\r\n" + 
				"        <br>\r\n" + 
				"        <div style=\"width:100%; margin-left:25px; border-color:gray; padding:2pt 3.75pt 2pt 3.75pt;\">\r\n" + 
				"          <font color=\"black\" size=\"4\">" +	
							pontoConsumoDescricao +
							"<br>" +
							tipoLogradouro + ", " +
							logradouro + ", " +
							numeroImovel + ", " +
							bairro + ", " +
							pontoConsumoNomeMunicipio + ", " +
							uf + "<br><br>" +
				"Prezado(a) Senhor(a),<br>"+ 
				"Comunicamos que o serviço <strong>" + servicoTipoDescricao + "</strong>"+ 
				" foi realizado em <strong>"+this.getDataEncerramentoFormatada() +"</strong> e que seu formulário" +
				" com o detalhamento do serviço encontra-se em anexo. <br><br>" +
				" Atenciosamente, <br>" +
				"RUA ARTUR VITAL DA SILVA, 04, GRUTA DE LOURDES, MACEIO, AL <br>" + 
				"SAC e Plantão: 117 <br>" + 
				"atendimento@algas.com.br" +
				"		</font>\r\n" + 
				"        </div>\r\n" + 
				"        <br>\r\n" + 
				"      </td>\r\n" + 
				"    </tr>\r\n" + 
				"  </table>\r\n" + 
				"</body>\r\n" + 
				"</html>");
		
		return email.toString();
	}
	
	/**
	 * @return the indicadorSucessoExecucao
	 */
	public Boolean getIndicadorSucessoExecucao() {
		return indicadorSucessoExecucao;
	}

	/**
	 * @param indicadorSucessoExecucao the indicadorSucessoExecucao to set
	 */
	public void setIndicadorSucessoExecucao(Boolean indicadorSucessoExecucao) {
		this.indicadorSucessoExecucao = indicadorSucessoExecucao;
	}

	public Boolean getIndicadorReferenciaGarantia() {
		return indicadorReferenciaGarantia;
	}

	public void setIndicadorReferenciaGarantia(Boolean indicadorReferenciaGarantia) {
		this.indicadorReferenciaGarantia = indicadorReferenciaGarantia;
	}

	/**
	 * @return executante
	 */
	public Funcionario getExecutante() {
		return executante;
	}

	/**
	 * @param executante
	 */
	public void setExecutante(Funcionario executante) {
		this.executante = executante;
	}

	public Collection<QuestionarioPergunta> getListaPerguntasQuestionario() {
		return listaPerguntasQuestionario;
	}

	public void setListaPerguntasQuestionario(Collection<QuestionarioPergunta> listaPerguntasQuestionario) {
		this.listaPerguntasQuestionario = listaPerguntasQuestionario;
	}

	public Questionario getQuestionarioPergunta() {
		return questionarioPergunta;
	}

	public void setQuestionarioPergunta(Questionario questionarioPergunta) {
		this.questionarioPergunta = questionarioPergunta;
	}

	public Collection<QuestionarioAlternativa> getListaAlternativasQuestionario() {
		return listaAlternativasQuestionario;
	}

	public void setListaAlternativasQuestionario(Collection<QuestionarioAlternativa> listaAlternativasQuestionario) {
		this.listaAlternativasQuestionario = listaAlternativasQuestionario;
	}

	public QuestionarioPergunta getQuestionarioPerguntaServico() {
		return questionarioPerguntaServico;
	}

	public void setQuestionarioPerguntaServico(QuestionarioPergunta questionarioPerguntaServico) {
		this.questionarioPerguntaServico = questionarioPerguntaServico;
	}

	/**
	 * Método que verifica um Segmento associado ao Ponto de Cosumo
	 * 
	 * @return boolean {@link boolean}
	 */
	public boolean temSegmento() {
		return this.pontoConsumo != null && this.pontoConsumo.getSegmento() != null;
	}

	public Medidor getMedidor() {
		return medidor;
	}

	public void setMedidor(Medidor medidor) {
		this.medidor = medidor;
	}

	public Long getNumeroProtocolo() {
		return numeroProtocolo;
	}

	public void setNumeroProtocolo(Long numeroProtocolo) {
		this.numeroProtocolo = numeroProtocolo;
	}

	public Long getTempoDistancia() {
		return tempoDistancia;
	}

	public void setTempoDistancia(Long tempoDistancia) {
		this.tempoDistancia = tempoDistancia;
	}
	
	public Long getNumeroOS() {
		return numeroOS;
	}

	public void setNumeroOS(Long numeroOS) {
		this.numeroOS = numeroOS;
	}
	
	public EntidadeConteudo getTurnoPreferencia() {
		return turnoPreferencia;
	}

	public void setTurnoPreferencia(EntidadeConteudo turnoPreferencia) {
		this.turnoPreferencia = turnoPreferencia;
	}

	public ServicoAutorizacaoTemp getServicoAutorizacaoTemp() {
		return servicoAutorizacaoTemp;
	}

	public void setServicoAutorizacaoTemp(ServicoAutorizacaoTemp servicoAutorizacaoTemp) {
		this.servicoAutorizacaoTemp = servicoAutorizacaoTemp;
	}

	public ServicoAutorizacaoAgenda getServicoAutorizacaoAgenda() {
		return servicoAutorizacaoAgenda;
	}

	public void setServicoAutorizacaoAgenda(ServicoAutorizacaoAgenda servicoAutorizacaoAgenda) {
		this.servicoAutorizacaoAgenda = servicoAutorizacaoAgenda;
	}

	public PontoConsumo getPontoConsumoRoteirizacao() {
		if(pontoConsumo != null) {
			return pontoConsumo;
		} else {
			return pontoConsumoRoteirizacao;
		}
	}

	public void setPontoConsumoRoteirizacao(PontoConsumo pontoConsumoRoteirizacao) {
		
		this.pontoConsumoRoteirizacao = pontoConsumoRoteirizacao;
	}
	
}
