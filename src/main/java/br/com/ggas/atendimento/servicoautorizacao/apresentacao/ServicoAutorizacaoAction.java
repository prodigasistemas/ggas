/*
t Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 20/11/2013 11:46:09
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamado.dominio.ClienteVOAutoComplete;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.ComunicacaoLoteVO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroAcompanhamentoLoteDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroGeracaoComunicaoLoteDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroLoteComunicacaoDTO;
import br.com.ggas.atendimento.comunicacao.negocio.ControladorComunicacao;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.atendimento.material.negocio.ControladorMaterial;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.resposta.negocio.ControladorResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AutorizacaoServicoLoteVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.FiltroGeracaoServicoAutorizacaoLoteDTO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.FiltroServicoAutorizacaoLoteDTO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.IndicadorEmergenciaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgendaPesquisaVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoEquipamento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistoricoAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMaterial;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTempAnexo;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioServicoAutorizacao;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelas ações relacionadas as telas que manipulam a entidade {@link ServicoAutorizacao}
 */
@Controller
@SessionAttributes({"servicoAutorizacao", "servicoAutorizacaoVO"})
public class ServicoAutorizacaoAction extends GenericAction {

	private static final String QUESTIONARIO = "questionario";

	private static final String LISTA_PERGUNTAS = "listaPerguntas";

	private static final String INDEX_LISTA = "indexLista";

	private static final String GRID_ANEXOS = "gridAnexos";

	private static final String LISTA_ANEXOS = "listaAnexos";

	private static final int TAMANHO_CPF = 11;

	private static final int PRIORIDADE_SERVICO = 2;

	private static final int INDICE_CONTRATO = 4;

	private static final int TAMANHO_MAXIMO_ARQUIVO = 1048576;
	private static final String CONST_TAMANHO_MAXIMO_ARQUIVO = "TAMANHO_MAXIMO_ARQUIVO";


	private static final String DATA_SOLICITACAO_RAMAL = "dataSolicitacaoRamal";

	private static final String CONTENT_DISPOSITION = "Content-Disposition";

	private static final String TELA_AJAX_ERRO = "ajaxErro";

	private static final String TELA_DIV_CLIENTE = "divCliente";

	private static final String DATA_PREVISAO_ENCERRAMENTO = "dataPrevisaoEncerramento";

	private static final String URL_RETORNO_ATUALIZACAO_CASTRAL = "urlRetornoAtualizacaoCastral";

	private static final String INDICADOR_ATUALIZACAO_CADASTRAL = "indicadorAtualizacaoCadastral";

	private static final String LISTA_AUTORIZACAO_EQUIPAMENTOS = "listaAutorizacaoEquipamentos";

	private static final String LISTA_AUTORIZACAO_MATERIAIS = "listaAutorizacaoMateriais";

	private static final String FLUXO_ALTERACAO = "fluxoAlteracao";

	private static final String PONTOS_CONSUMO = "pontosConsumo";

	private static final String CLIENTE = "cliente";

	private static final String TELEFONE = "telefone";

	private static final String IDCONTRATO = "idContrato";

	private static final String PREVISAO_ENCERRAMENTO = "previsaoEncerramento";

	private static final String DD_MM_YYYY = "dd/MM/yyyy";

	private static final String SERVICO_TIPO_PRIORIDADE = "servicoTipoPrioridade";

	private static final String FORWARD_PESQUISAR_SERVICO_AUTORIZACAO = "forward:/pesquisarServicoAutorizacao";

	private static final String FLUXO_INCLUSAO = "fluxoInclusao";

	private static final String FLUXO_DETALHAMENTO = "fluxoDetalhamento";

	private static final String TELA_EXIBIR_INCLUSAO_SERVICO_AUTORIZACAO = "exibirInclusaoServicoAutorizacao";

	private static final String SERVICO_AUTORIZACAO = "servicoAutorizacao";

	private static final String SERVICO_AUTORIZACAO_VO = "servicoAutorizacaoVO";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String GRID_IMOVEIS = "gridImoveis";

	private static final String LISTA_IMOVEIS_CHAMADO = "listaImoveisChamado";
	
	private static final String EQUIPE_PRIORITARIA = "equipePrioritaria";

	private static final Logger LOG = Logger.getLogger(ServicoAutorizacaoAction.class);
	
	private static final String[] extensao =
			{ ".PDF", ".TXT", ".DOC", ".DOCX", ".XLS", ".XLSX", ".PNG", ".JPG", ".JPEG", ".BMP", ".PPT", ".PPTX" };
	
	private static final String LISTA_MARCA_MEDIDOR = "listaMarcaMedidor";

	private static final String LISTA_MODELO_MEDIDOR = "listaModeloMedidor";

	private static final String LISTA_TIPO_MEDIDOR = "listaTipoMedidor";
	
	private static final String LISTA_LOCAL_ARMAZENAGEM = "listaLocalArmazenagem";

	private static final String MODO_USO_VIRTUAL = "modoUsoVirtual";

	private static final String MODO_USO_NORMAL = "modoUsoNormal";

	private static final String MODO_USO_INDEPENDENTE = "modoUsoIndependente";
	
	private static final String LISTA_CITY_GATE = "listaCityGate";
	
	private static final String LISTA_SEGMENTO = "listaSegmento";
	
	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";
	
	private static final String LISTA_PONTO_CONSUMO_SESSAO = "listaPontoConsumoSessao";
	
	private static final String LISTA_SERVICO_TIPO = "listaServicoTipo";
	
	private static final String LISTA_SITUACAO_MEDIDOR = "listaSituacaoMedidor";
	
	private static final String LISTA_SITUACAO_PONTO_CONSUMO = "listaSituacaoPontoConsumo";

	private static final String LISTA_SITUACAO_CONTRATO = "listaSituacaoContrato";
	
	private static final String LISTA_TURNO = "listaTurno";
	
	private static final String LISTA_RAMO_ATIVIDADE = "listaRamoAtividade";
	
	private static final String AUTORIZACAO_SERVICO_LOTE = "autorizacaoServicolote";
	
	private static final String LISTA_CHAMADO_TIPO = "listaChamadoTipo";

	private static final String RELATORIO_SERVICO_AUTORIZACAO_SINTETICO = "relatorioServicoAutorizacaoSintetico";
	
	private static final String RELATORIO_SERVICO_AUTORIZACAO_ANALITICO = "relatorioServicoAutorizacaoAnalitico";
	
	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;
	
	@Autowired
	private ControladorChamado controladorChamado;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	private ControladorEquipe controladorEquipe;

	/**
	 * controlador Imovel
	 */
	@Autowired
	@Qualifier("controladorImovel")
	private ControladorImovel controladorImovel;

	@Autowired
	private ControladorServicoTipoPrioridade controladorServicoTipoPrioridade;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorMaterial controladorMaterial;

	@Autowired
	@Qualifier("controladorDevolucao")
	private ControladorDevolucao controladorDevolucao;

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	/**
	 * Controlador contrato
	 */
	@Autowired
	@Qualifier("controladorContrato")
	private ControladorContrato controladorContrato;

	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	@Autowired
	private ControladorRelatorioServicoAutorizacao controladorRelatorioServicoAutorizacao;

	@Autowired
	private ControladorQuestionario controladorQuestionario;

	@Autowired
	private ControladorResposta controladorResposta;
	
	@Autowired 
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	private ControladorComunicacao controladorComunicacao;

	/**
	 * Controlador parametros sistema
	 */
	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	ControladorModeloContrato controladorModeloContrato;
	
	@Autowired
	private ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento;
	
	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();

	private static final String DATA_GERACAO = "Data de Geração";

	private static final String DATA_PREVISAO = "Data Previsão de Encerramento";

	private static final String DATA_ENCERRAMENTO = "Data de Encerramento";

	private static final String DATA = "data";

	private static final String MSG_SUCESSO_LOTE_INCLUIDO = "Lote de Autorização de Serviço gerado com Sucesso";
	
	/**
	 * Pesquisar servico autorizacao.
	 * 
	 * @param servicoAutorizacaoVO {@link ServicoAutorizacaoVO}
	 * @param tamanhoPagina {@link Integer}
	 * @param result {@link BindingResult}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link ModelAndView}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarServicoAutorizacao")
	public ModelAndView pesquisarServicoAutorizacao(@ModelAttribute("ServicoAutorizacaoVO") ServicoAutorizacaoVO servicoAutorizacaoVO,
			@RequestParam(value = "tamanhoPagina", required = false) Integer tamanhoPagina, BindingResult result,
			HttpServletRequest request, ModelAndView model) throws GGASException {

		String fluxoAlteracao = (String) request.getAttribute("exibirAlteracao");
		
		ModelAndView modelAndView = new ModelAndView("exibirPesquisaServicoAutorizacao");

		int tamanhoPaginacao = Constantes.PAGINACAO_PADRAO;
		if (tamanhoPagina != null) {
			tamanhoPaginacao = tamanhoPagina;
		}
		
		if(request.getParameter("fluxoVoltar") != null) {
			if(request.getParameter("fluxoVoltar").equals("true")) {
				modelAndView.addObject("servicoAutorizacaoVO",servicoAutorizacaoVO);
			}else {
				modelAndView = new ModelAndView("exibirPesquisaServicoAutorizacao");
			}
		}
		
		modelAndView.addObject("tamanhoPagina", tamanhoPaginacao);
		return carregarServicoAutorizacao(servicoAutorizacaoVO, request, modelAndView, false, fluxoAlteracao);

	}

	/**
	 * Pesquisar servico autorizacao popup.
	 * 
	 * @param servicoAutorizacaoVO
	 *            the servico autorizacao vo
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarServicoAutorizacaoPopup")
	public ModelAndView pesquisarServicoAutorizacaoPopup(@ModelAttribute("ServicoAutorizacaoVO") ServicoAutorizacaoVO servicoAutorizacaoVO,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("popupPesquisarServicoAutorizacao");
		ServicoAutorizacaoAgendaPesquisaVO agenda = new ServicoAutorizacaoAgendaPesquisaVO();
		agenda.setDataSelecionada(servicoAutorizacaoVO.getDataSelecionada());
		agenda.setServicoTipo(servicoAutorizacaoVO.getServicoTipo());
		model.addObject("servicoAutorizacaoAgenda", agenda);
		servicoAutorizacaoVO.setIsAgendamento(true);
		
		
		if (servicoAutorizacaoVO.getServicoTipo() != null
				&& servicoAutorizacaoVO.getServicoTipo().getChavePrimaria() > 0) {
			Date data = Util.converterCampoStringParaData(DATA, servicoAutorizacaoVO.getDataSelecionada(),
					Constantes.FORMATO_DATA_BR);
			model.addObject("listaTurnosDisponiveis", controladorServicoAutorizacao
					.consultarAgendamentosDisponiveisTurno(servicoAutorizacaoVO.getServicoTipo(), data));
		} else {
			ServicoTipo servicoTipo = new ServicoTipo();
			servicoTipo.setChavePrimaria(-1l);
			
			servicoAutorizacaoVO.setServicoTipo(servicoTipo);
		}
		
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));

		return carregarServicoAutorizacao(servicoAutorizacaoVO, request, model, true, null);
	}

	/**
	 * Carregar servico autorizacao.
	 * 
	 * @param servicoAutorizacaoTmp
	 *            the servico autorizacao vo
	 * @param request
	 *            the request
	 * @param model
	 *            the model
	 * @param pesquisaPopupAgendamento
	 *            the pesquisa popup agendamento
	 * @param fluxoAlteracao
	 *            the fluxo alteracao
	 * @return the model and view
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private ModelAndView carregarServicoAutorizacao(ServicoAutorizacaoVO servicoAutorizacaoTmp, HttpServletRequest request,
			ModelAndView model, boolean pesquisaPopupAgendamento, String fluxoAlteracao) throws GGASException {
		ServicoAutorizacaoVO servicoAutorizacaoVO = servicoAutorizacaoTmp;
		String fluxoVoltar = request.getParameter("fluxoVoltar");
		if(fluxoVoltar != null && "true".equals(fluxoVoltar)) {
			servicoAutorizacaoVO = (ServicoAutorizacaoVO) request.getSession().getAttribute(SERVICO_AUTORIZACAO_VO);
		}
		
		Integer numeroContratoPesquisa = null;

		if(servicoAutorizacaoVO.getNumeroContrato() != null) {

			numeroContratoPesquisa = servicoAutorizacaoVO.getNumeroContrato();

			String numeroContrato = servicoAutorizacaoVO.getNumeroContrato().toString();

			if(numeroContrato.length() > INDICE_CONTRATO) {
				String numeroContratoNaoFormatado = numeroContrato.substring(INDICE_CONTRATO);
				Integer contrato = Integer.parseInt(Util.ltrim(numeroContratoNaoFormatado));
				servicoAutorizacaoVO.setNumeroContrato(contrato);
			}

		}

		ServicoAutorizacao servico = (ServicoAutorizacao) request.getAttribute(SERVICO_AUTORIZACAO);

		try {
			
			if(servico != null && servico.getChavePrimaria() != 0 && fluxoAlteracao == null) {
				servico = controladorServicoAutorizacao.obterServicoAutorizacao(servico.getChavePrimaria());
				servicoAutorizacaoVO.setChavePrimaria(servico.getChavePrimaria());
				servicoAutorizacaoVO.setEquipe(servico.getEquipe());
				servicoAutorizacaoVO.setDataGeracaoInicio(servico.getDataGeracaoFormatada());
				servicoAutorizacaoVO.setDataGeracaoFim(servico.getDataGeracaoFormatada());
				servicoAutorizacaoVO.setDataPrevisaoInicio(servico.getDataPrevisaoEncerramentoFormatada());
				servicoAutorizacaoVO.setDataPrevisaoFim(servico.getDataPrevisaoEncerramentoFormatada());
				servicoAutorizacaoVO.setListaStatus(new ArrayList<String>());
				servicoAutorizacaoVO.getListaStatus().add(servico.getStatus().getDescricao());
				servicoAutorizacaoVO.setServicoTipo(servico.getServicoTipo());
				servicoAutorizacaoVO.setServicoTipoPrioridade(servico.getServicoTipoPrioridade());
				servicoAutorizacaoVO.setNumeroProtocoloChamado(null);
				
				
				if(servico.getChamado() != null) {
					servicoAutorizacaoVO.setNumeroProtocoloChamado(servico.getChamado().getProtocolo().getNumeroProtocolo());
				}
				if (servico.getServicoAutorizacaoMotivoEncerramento() != null) {
					servicoAutorizacaoVO.setServicoAutorizacaoMotivoEncerramento(servico.getServicoAutorizacaoMotivoEncerramento());
				}
			}

			if(servicoAutorizacaoVO.getDataGeracaoInicio() != null && servicoAutorizacaoVO.getDataGeracaoFim() != null
							&& !"".equals(servicoAutorizacaoVO.getDataGeracaoInicio())) {
				controladorDevolucao.validarDataDevolucaoInicialFinal(servicoAutorizacaoVO.getDataGeracaoInicio(),
								servicoAutorizacaoVO.getDataGeracaoFim());
			}

			if(servicoAutorizacaoVO.getDataPrevisaoInicio() != null && servicoAutorizacaoVO.getDataPrevisaoFim() != null
							&& !"".equals(servicoAutorizacaoVO.getDataPrevisaoFim())) {
				controladorDevolucao.validarDataDevolucaoInicialFinal(servicoAutorizacaoVO.getDataPrevisaoInicio(),
								servicoAutorizacaoVO.getDataPrevisaoFim());
			}
			if(servicoAutorizacaoVO.getDataEncerramentoInicio() != null && servicoAutorizacaoVO.getDataEncerramentoFim() != null) {
				controladorDevolucao.validarDataDevolucaoInicialFinal(servicoAutorizacaoVO.getDataEncerramentoInicio(),
								servicoAutorizacaoVO.getDataEncerramentoFim());
			}
			SimpleDateFormat formatoData = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			if (servicoAutorizacaoVO.getDataExecucaoInicio() != null && !servicoAutorizacaoVO.getDataExecucaoInicio().isEmpty()
					&& servicoAutorizacaoVO.getDataExecucaoFim() != null && !servicoAutorizacaoVO.getDataExecucaoFim().isEmpty()) {
				conversaoDatas(servicoAutorizacaoVO, formatoData);
			}

			Collection<ServicoAutorizacao> listaServicoAutorizacao = controladorServicoAutorizacao.listarServicoAutorizacao(
							servicoAutorizacaoVO, pesquisaPopupAgendamento);

			Collection<ServicoAutorizacao> lista = (Collection<ServicoAutorizacao>) request.getAttribute("lista");

			if(lista == null) {
				model.addObject("listaServicoAutorizacao", listaServicoAutorizacao);
			} else {
				model.addObject("listaServicoAutorizacao", lista);
			}
			servicoAutorizacaoVO.setNumeroContrato(numeroContratoPesquisa);
			model.addObject(SERVICO_AUTORIZACAO_VO, servicoAutorizacaoVO);
			model.addObject("servicoAtraso", servicoAutorizacaoVO.getServicoAtraso());
		} catch(GGASException e) {
			model.addObject(SERVICO_AUTORIZACAO_VO, servicoAutorizacaoVO);
			mensagemErroParametrizado(model, e);
		}

		this.carregarCombos(model);
		return model;
	}

	/**
	 * @param servicoAutorizacaoVO
	 * @param formatoData
	 * @throws GGASException
	 */
	private void conversaoDatas(ServicoAutorizacaoVO servicoAutorizacaoVO, SimpleDateFormat formatoData)
			throws GGASException {
		Date dataInicio = null;
		Date dataFim = null;
		try {
			dataInicio = formatoData.parse(servicoAutorizacaoVO.getDataExecucaoInicio());
			dataFim = formatoData.parse(servicoAutorizacaoVO.getDataExecucaoFim());
		} catch (ParseException e) {
			LOG.error(e.getMessage());
			throw new GGASException(e);
		}
		if (dataInicio.after(dataFim)) {
			throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
		}
	}

	/**
	 * Exibir pesquisa servico autorizacao.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirPesquisaServicoAutorizacao")
	public ModelAndView exibirPesquisaServicoAutorizacao(HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaServicoAutorizacao");
		this.carregarCombos(model);
		ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();

		servicoAutorizacaoVO.setListaStatus(new ArrayList<String>());
		servicoAutorizacaoVO.getListaStatus().add("Aberta");
		servicoAutorizacaoVO.getListaStatus().add("Em Execução");
		servicoAutorizacaoVO.getListaStatus().add("Pendente de Atualização");
		model.addObject(SERVICO_AUTORIZACAO_VO, servicoAutorizacaoVO);
		removerDadosSessao(sessao);
		return model;
	}

	/**
	 * Exibir inclusao servico autorizacao.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_SERVICO_AUTORIZACAO)
	public ModelAndView exibirInclusaoServicoAutorizacao(HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_SERVICO_AUTORIZACAO);
		model.addObject(FLUXO_INCLUSAO, true);
		model.addObject(SERVICO_AUTORIZACAO, new ServicoAutorizacao());
		this.carregarCombos(model);
		removerDadosSessao(sessao);
		adicionarParametroCaixaAlta(sessao);
		return model;
	}

	/**
	 * Incluir servico autorizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("incluirServicoAutorizacao")
	public ModelAndView incluirServicoAutorizacao(@RequestParam(value = "isGerarAsAutomatica", required = false) String isGerarAsPrioritaria, @ModelAttribute("ServicoAutorizacao") ServicoAutorizacao servicoAutorizacao,
					BindingResult result, HttpServletRequest request) throws GGASException {

		Log.info("Iniciando incluirServicoAutorizacao");
		ModelAndView model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
		Map<String, Object> dados = new HashMap<>();
		String descricao = servicoAutorizacao.getDescricao();
		try {
			popularAnexoServicoAutorizacao(servicoAutorizacao, request);
			popularMapaDados(dados, request);
			
			if(!StringUtils.isEmpty(isGerarAsPrioritaria)) {
				dados.put("isGerarAsPrioritaria", Boolean.valueOf(isGerarAsPrioritaria));
			}
			
			servicoAutorizacao.setDadosAuditoria(getDadosAuditoria(request));
			servicoAutorizacao.setUsuarioSistema((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO));
			ServicoAutorizacao autorizacaoServico = controladorServicoAutorizacao.inserirServicoAutorizacao(servicoAutorizacao, dados, Boolean.FALSE);
			model.addObject(SERVICO_AUTORIZACAO, autorizacaoServico);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
		} catch(GGASException e) {
			
			servicoAutorizacao.setDescricao(descricao);
			
			model = new ModelAndView(TELA_EXIBIR_INCLUSAO_SERVICO_AUTORIZACAO);
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
			model.addObject(FLUXO_INCLUSAO, true);

			if(servicoAutorizacao.getServicoTipoPrioridade() != null) {
				model.addObject(SERVICO_TIPO_PRIORIDADE, servicoAutorizacao.getServicoTipoPrioridade());
			}

			if(servicoAutorizacao.getDataPrevisaoEncerramento() != null) {
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				model.addObject(PREVISAO_ENCERRAMENTO, df.format(servicoAutorizacao.getDataPrevisaoEncerramento()));

			}

			if(servicoAutorizacao.getCliente() != null) {
				model.addObject(CLIENTE, servicoAutorizacao.getCliente());
			}

			if(servicoAutorizacao.getImovel() != null) {
				model.addObject("imovel", servicoAutorizacao.getImovel());
			}
			if(servicoAutorizacao.getPontoConsumo() != null) {
				model.addObject(PONTOS_CONSUMO, Arrays.asList(servicoAutorizacao.getPontoConsumo()));
			}

			this.carregarCombos(model);
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim incluirServicoAutorizacao");
		return model;

	}

	/**
	 * Exibir detalhamento servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoServicoAutorizacao")
	public ModelAndView exibirDetalhamentoServicoAutorizacao(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws GGASException {
		Log.info("Iniciando exibirDetalhamentoServicoAutorizacao");
		ModelAndView model = new ModelAndView("exibirDetalhamentoServicoAutorizacao");
		model.addObject(FLUXO_DETALHAMENTO, Boolean.TRUE);
		this.exibirServicoAutorizacao(model, chavePrimaria, sessao);
		Log.info("Fim exibirDetalhamentoServicoAutorizacao");
		return model;
	}

	/**
	 * Exibir alteracao servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @param reexibicao  {@link Boolean}
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoServicoAutorizacao")
	public ModelAndView exibirAlteracaoServicoAutorizacao(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request,
			HttpSession sessao, Boolean reexibicao)
			throws GGASException {

		Log.info("Inicio exibirAlteracaoServicoAutorizacao");
		ModelAndView model = new ModelAndView("exibirAlteracaoServicoAutorizacao");
		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(chavePrimaria);
		
		if (!Util.isTrue(reexibicao)) {
			removerDadosSessao(sessao);
		}

		try {
			controladorServicoAutorizacao.verificarStatusServicoAutorizacao(servicoAutorizacao, Constantes.C_OPERACAO_SERV_AUT_ALTERADO);
			model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
			this.exibirServicoAutorizacao(model, chavePrimaria, sessao);

			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);

		} catch(NegocioException e) {
			request.setAttribute("exibirAlteracao", "true");
			model = verificarTipoFluxoExecucao(request);
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim exibirAlteracaoServicoAutorizacao");
		return model;
	}

	/**
	 * Exibir encerrar servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @param reexibicao {@link Boolean}
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirEncerrarServicoAutorizacao")
	public ModelAndView exibirEncerrarServicoAutorizacao(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request,
			HttpSession sessao, Boolean reexibicao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_SERVICO_AUTORIZACAO);
		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(chavePrimaria);
		ServicoAutorizacaoTemp servicoAutorizacaoTemp = controladorServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);

		if (!Util.isTrue(reexibicao)) {
			removerDadosSessao(sessao);
		}

		try {
			controladorServicoAutorizacao
							.verificarStatusServicoAutorizacao(servicoAutorizacao, Constantes.C_OPERACAO_SERV_AUT_ENCERRAMENTO);
			model.addObject("fluxoEncerramento", Boolean.TRUE);
			this.exibirServicoAutorizacao(model, chavePrimaria, sessao);
			servicoAutorizacao.setDescricao("");
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
			model.addObject("servicoAutorizacaoTemp", servicoAutorizacaoTemp);


		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Encerrar servico autorizacao.
	 * 
	 * @param servicoAutorizacaoTmp
	 *            the servico autorizacao
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("encerrarServicoAutorizacao")
	public ModelAndView encerrarServicoAutorizacao(
			@RequestParam(value = "tipoAssociacao", required = false) String tipoAssociacao,
			@ModelAttribute("ServicoAutorizacao") ServicoAutorizacao servicoAutorizacaoTmp, BindingResult result,
			HttpServletRequest request, HttpSession sessao) throws GGASException {

		Log.info("Inicio encerrarServicoAutorizacao");
		ModelAndView model = null;
		ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoTmp;
		@SuppressWarnings("unchecked")
		Collection<ServicoAutorizacaoMaterial> listaMateriais = (Collection<ServicoAutorizacaoMaterial>) sessao
				.getAttribute(LISTA_AUTORIZACAO_MATERIAIS);
		@SuppressWarnings("unchecked")
		Collection<ServicoAutorizacaoEquipamento> listaEquipamentos = (Collection<ServicoAutorizacaoEquipamento>) sessao
				.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS);

		Boolean indicadorAtualizacaoCadastralImediata = (Boolean) sessao.getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);

		Map<String, Object> dados = new HashMap<>();
		try {

			if (!validaDataExecucaoChamado(servicoAutorizacaoTmp)) {
				throw new NegocioException(
						"Data e Hora de Execução tem que ser maior que a data de criação e menor que a data atual!");
			}

			if (indicadorAtualizacaoCadastralImediata != null) {
				servicoAutorizacao = (ServicoAutorizacao) sessao.getAttribute(SERVICO_AUTORIZACAO);
				servicoAutorizacao = controladorServicoAutorizacao
						.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
			} else {
				popularAnexoServicoAutorizacao(servicoAutorizacao, request);
			}
			popularMapaDados(dados, request);
			controladorServicoAutorizacao.validarMotivoEncerramento(servicoAutorizacao);

			if (indicadorAtualizacaoCadastralImediata != null) {
				model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
				controladorServicoAutorizacao.encerrarServicoAutorizacao(servicoAutorizacao, listaMateriais,
						listaEquipamentos, Constantes.C_STATUS_SERV_AUT_ENCERRADO, dados);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA,
						GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
				removerDadosSessao(sessao);

			} else if (servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getFluxoEncerramento() != null
					&& !servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getFluxoEncerramento()) {
				model = new ModelAndView("forward:/exibirPesquisaServicoAutorizacao");
				controladorServicoAutorizacao.encerrarServicoAutorizacao(servicoAutorizacao, listaMateriais,
						listaEquipamentos, Constantes.C_STATUS_SERV_AUT_CANCELADO, dados);
				mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA,
						GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
				removerDadosSessao(sessao);

			} else if (servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getFluxoEncerramento() != null
					&& servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento().getFluxoEncerramento()) {
				if ((servicoAutorizacao.getServicoTipo().getIndicadorAtualizacaoCadastral()
						.equals(Constantes.ATUALIZACAO_CADASTRAL_POSTERIOR)
						|| servicoAutorizacao.getServicoTipo().getIndicadorAtualizacaoCadastral()
								.equals(Constantes.ATUALIZACAO_CADASTRAL_IMEDIATA))) {
					controladorServicoAutorizacao.encerrarServicoAutorizacao(servicoAutorizacao, listaMateriais,
							listaEquipamentos, Constantes.C_STATUS_SERV_AUT_PENDENTE, dados);
					sessao.setAttribute(INDICADOR_ATUALIZACAO_CADASTRAL, Boolean.TRUE);
					model = escolherTelaAtualizacaoCadastral(servicoAutorizacao);

					model = this.encerrarServicoDeAutorizacao(model, servicoAutorizacao, listaMateriais, sessao,
							listaEquipamentos, dados);
				} else {
					model = new ModelAndView("forward:/exibirPesquisaServicoAutorizacao");
					controladorServicoAutorizacao.encerrarServicoAutorizacao(servicoAutorizacao, listaMateriais,
							listaEquipamentos, Constantes.C_STATUS_SERV_AUT_ENCERRADO, dados);
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA,
							GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
					removerDadosSessao(sessao);
				}

			}
		} catch (GGASException e) {
			model = exibirEncerrarServicoAutorizacao(servicoAutorizacao.getChavePrimaria(), request, sessao, true);
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
			ServicoAutorizacao servicoAutorizacaoBase = controladorServicoAutorizacao
					.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
			servicoAutorizacao.setChamado(servicoAutorizacaoBase.getChamado());
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		} catch (IOException e) {
			LOG.error("Falha ao realizar operação de leitura e escrita", e);
			throw new GGASException(e);
		}
		Log.info("Fim encerrarServicoAutorizacao");
		return model;
	}

	private boolean validaDataExecucaoChamado(ServicoAutorizacao servicoAutorizacaoTmp) throws GGASException {
		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacaoTmp.getChavePrimaria());
		
		
		if(servicoAutorizacao != null && servicoAutorizacao.getChamado() != null && servicoAutorizacaoTmp.getDataExecucao() != null) {
			Chamado chamado = servicoAutorizacao.getChamado();
			
			if(chamado != null) {
				Date dataCriacao = chamado.getProtocolo().getUltimaAlteracao();
				Date dataExecucaoConvertida = Util.converterCampoStringParaData("dataExecucao", servicoAutorizacaoTmp.getDataExecucaoFormatada(),
						Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);
				
				if (DataUtil.menorQue(dataExecucaoConvertida, dataCriacao)
						|| DataUtil.menorQue(new Date(), dataExecucaoConvertida)) {
					return Boolean.FALSE;
				}
				
			}
			
		}
		
		
		return Boolean.TRUE;
	}

	private ModelAndView encerrarServicoDeAutorizacao(ModelAndView modelParam, ServicoAutorizacao servicoAutorizacao,
			Collection<ServicoAutorizacaoMaterial> listaMateriais, HttpSession sessao,
			Collection<ServicoAutorizacaoEquipamento> listaEquipamentos, Map<String, Object> dados)
			throws UnknownHostException, GGASException {

		Log.info("Inicio encerrarServicoDeAutorizacao");
		ModelAndView model = modelParam;
		
		if (model == null) {
			model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
			controladorServicoAutorizacao.encerrarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos,
					Constantes.C_STATUS_SERV_AUT_ENCERRADO, dados);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ENCERRADA, GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
			removerDadosSessao(sessao);
		} else {
			sessao.setAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL, "encerrarServicoAutorizacao");
		}
		Log.info("Fim encerrarServicoDeAutorizacao");
		return model;
	}
	
	/**
	 * Exibir executar servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @param reexibicao {@link Boolean}
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirExecutarServicoAutorizacao")
	public ModelAndView exibirExecutarServicoAutorizacao(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request,
			HttpSession sessao, Boolean reexibicao) throws GGASException {

		Log.info("Inicio exibirExecutarServicoAutorizacao");
		ModelAndView model = new ModelAndView("exibirExecutarServicoAutorizacao");
		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(chavePrimaria);

		validarQuestionarioSessao(request, servicoAutorizacao.getChavePrimaria());

		if (!Util.isTrue(reexibicao)) {
			removerDadosSessao(sessao);
		}

		try {
			controladorServicoAutorizacao.verificarStatusServicoAutorizacao(servicoAutorizacao, Constantes.C_OPERACAO_SERV_AUT_EXECUTADO);
			model.addObject("fluxoExecucao", Boolean.TRUE);
			this.exibirServicoAutorizacao(model, chavePrimaria, sessao);
			servicoAutorizacao.setDescricao("");

			if (servicoAutorizacao.getServicoTipo().getFormulario() != null) {
				Questionario questionario = controladorQuestionario.consultarPorCodico(servicoAutorizacao.getServicoTipo()
						.getFormulario().getChavePrimaria(), LISTA_PERGUNTAS, "tipoServico", "segmento");
				List<QuestionarioPergunta> listaPerguntas = new ArrayList<QuestionarioPergunta>();
				if (sessao.getAttribute(LISTA_PERGUNTAS) != null) {
					listaPerguntas.addAll((Collection<? extends QuestionarioPergunta>) sessao.getAttribute(LISTA_PERGUNTAS));
				} else {
					listaPerguntas.addAll(questionario.getListaPerguntas());
				}
				Collections.sort(listaPerguntas);

				carregarAlternativas(listaPerguntas);
				model.addObject(LISTA_PERGUNTAS, listaPerguntas);
				model.addObject(QUESTIONARIO, questionario);
			}

			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);

		} catch(NegocioException e) {
			model = verificarTipoFluxoExecucao(request);
			mensagemErroParametrizado(model, e);
		}

		Log.info("Fim exibirExecutarServicoAutorizacao");
		return model;
	}

	/**
	 * Executar servico autorizacao.
	 * 
	 * @param servicoAutorizacaoTmp
	 *            the servico autorizacao
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("executarServicoAutorizacao")
	public ModelAndView executarServicoAutorizacao(@ModelAttribute("ServicoAutorizacao") ServicoAutorizacao servicoAutorizacaoTmp,
					BindingResult result, HttpServletRequest request, HttpSession sessao) throws GGASException {

		Log.info("Inicio executarServicoAutorizacao");
		ModelAndView model = null;
		ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoTmp;
		@SuppressWarnings("unchecked")
		Collection<ServicoAutorizacaoMaterial> listaMateriais = (Collection<ServicoAutorizacaoMaterial>) sessao
						.getAttribute(LISTA_AUTORIZACAO_MATERIAIS);
		@SuppressWarnings("unchecked")
		Collection<ServicoAutorizacaoEquipamento> listaEquipamentos = (Collection<ServicoAutorizacaoEquipamento>) sessao
						.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS);

		Boolean indicadorAtualizacaoCadastralImediata = (Boolean) sessao.getAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);

		Map<String, Object> dados = new HashMap<>();
		try {
			if(indicadorAtualizacaoCadastralImediata != null) {
				servicoAutorizacao = (ServicoAutorizacao) sessao.getAttribute(SERVICO_AUTORIZACAO);
				servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
			} else {
				popularAnexoServicoAutorizacao(servicoAutorizacao, request);
			}
			Object perguntaSessao = request.getSession().getAttribute(LISTA_PERGUNTAS);
			List<QuestionarioPergunta> listaPerguntas = null;
			if (perguntaSessao != null) {
				listaPerguntas = (List<QuestionarioPergunta>) perguntaSessao;
			}
			popularMapaDados(dados, request);
			if(indicadorAtualizacaoCadastralImediata != null
							|| !servicoAutorizacao.getServicoTipo().getIndicadorAtualizacaoCadastral()
											.equals(Constantes.ATUALIZACAO_CADASTRAL_IMEDIATA)) {
				model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);

				if(servicoAutorizacao.getServicoTipo().getIndicadorAtualizacaoCadastral()
								.equals(Constantes.ATUALIZACAO_CADASTRAL_POSTERIOR)) {
					controladorServicoAutorizacao.executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos,
							Constantes.C_STATUS_SERV_AUT_PENDENTE, dados, listaPerguntas);
					mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_PENDENTE_ATUALIZACAO,
									GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
				} else {
					executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos, dados, model, listaPerguntas);
				}

				removerDadosSessao(sessao);
			} else if(servicoAutorizacao.getServicoTipo().getIndicadorAtualizacaoCadastral()
							.equals(Constantes.ATUALIZACAO_CADASTRAL_IMEDIATA)) {

				controladorServicoAutorizacao.executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos,
						Constantes.C_STATUS_SERV_AUT_PENDENTE, dados, listaPerguntas);
				sessao.setAttribute(INDICADOR_ATUALIZACAO_CADASTRAL, Boolean.TRUE);
				model = escolherTelaAtualizacaoCadastral(servicoAutorizacao);

				if(model == null) {
					model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
					executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos, dados, model, listaPerguntas);
				} else {
					sessao.setAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL, "executarServicoAutorizacao");
				}
			}
			sessao.removeAttribute(LISTA_PERGUNTAS);
		} catch(GGASException e) {
			model = exibirExecutarServicoAutorizacao(servicoAutorizacao.getChavePrimaria(), request, sessao, true);
			ServicoAutorizacao servicoAutorizacaoBase =
					controladorServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
			servicoAutorizacao.setChamado(servicoAutorizacaoBase.getChamado());
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim executarServicoAutorizacao");
		return model;
	}

	/**
	 * Executa Servico Autorização
	 * 
	 * @param servicoAutorizacao
	 * @param listaMateriais
	 * @param listaEquipamentos
	 * @param dados
	 * @param model
	 * @param listaPergunta
	 * @throws GGASException
	 */
	private void executarServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, Collection<ServicoAutorizacaoMaterial> listaMateriais,
			Collection<ServicoAutorizacaoEquipamento> listaEquipamentos, Map<String, Object> dados, ModelAndView model,
			List<QuestionarioPergunta> listaPergunta) throws GGASException {

		Log.info("Inicio executarServicoAutorizacao");
		if (servicoAutorizacao.getServicoTipo().getIndicadorEncerramentoAuto() == null
				|| servicoAutorizacao.getServicoTipo().getIndicadorEncerramentoAuto()) {

			if (servicoAutorizacao.getServicoAutorizacaoMotivoEncerramento() == null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_ENCERRAMENTO_SEM_MOTIVO);
			}

			controladorServicoAutorizacao.executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos,
					Constantes.C_STATUS_SERV_AUT_ENCERRADO, dados, listaPergunta);
		} else if (!servicoAutorizacao.getServicoTipo().getIndicadorEncerramentoAuto()) {

			if (Util.isTrue(servicoAutorizacao.getIndicadorSucessoExecucao())) {
				controladorServicoAutorizacao.executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos,
						Constantes.C_STATUS_SERV_AUT_EXECUTADO, dados, listaPergunta);
			} else {
				controladorServicoAutorizacao.executarServicoAutorizacao(servicoAutorizacao, listaMateriais, listaEquipamentos,
						Constantes.C_STATUS_SERV_AUT_ABERTO, dados, listaPergunta);
			}
		}
		Log.info("Fim executarServicoAutorizacao");
		mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXECUTADA, GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
	}

	/**
	 * Escolher tela atualizacao cadastral.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 *
	 * @return the model and view
	 */
	private ModelAndView escolherTelaAtualizacaoCadastral(ServicoAutorizacao servicoAutorizacao) {

		ModelAndView model = null;

		Long codigoEntidadeConteudo = servicoAutorizacao.getServicoTipo().getTelaAtualizacao().getChavePrimaria();
		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorValor(String.valueOf(codigoEntidadeConteudo));

		if(constanteSistema.getNome().equals(Constantes.C_ATUALIZACAO_TELA_CLIENTE) && servicoAutorizacao.getCliente() != null) {
			model = new ModelAndView("forward:/exibirAlteracaoCliente?status=false");
			model.addObject(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, servicoAutorizacao.getCliente().getChavePrimaria());
		} else if (constanteSistema.getNome().equals(Constantes.C_ATUALIZACAO_TELA_IMOVEL)
				&& servicoAutorizacao.getPontoConsumo() != null
				&& servicoAutorizacao.getPontoConsumo().getImovel() != null) {
			model = new ModelAndView("forward:/exibirAlteracaoImovel");
			model.addObject(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA,
					servicoAutorizacao.getPontoConsumo().getImovel().getChavePrimaria());

		} else if(constanteSistema.getNome().equals(Constantes.C_ATUALIZACAO_TELA_CONTRATO)) {
			model = new ModelAndView("forward:/pesquisarContrato");
			
			if(servicoAutorizacao.getContrato() != null) {
				model.addObject(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, servicoAutorizacao.getContrato().getChavePrimaria());
			}
		
			model.addObject(FLUXO_ALTERACAO, Constantes.TRUE);
			
		} else if(constanteSistema.getNome().equals(Constantes.C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO)
						&& servicoAutorizacao.getPontoConsumo() != null) {

			String idPontoConsumo = String.valueOf(servicoAutorizacao.getPontoConsumo().getChavePrimaria());
			String idOperacaoMedidor = servicoAutorizacao.getServicoTipo().getIndicadorOperacaoMedidor();
			String tipoAssociacao = servicoAutorizacao.getServicoTipo().getIndicadorTipoAssociacaoPontoConsumo();

			model = new ModelAndView("forward:/exibirAssociacao");
			model.addObject("idPontoConsumo", idPontoConsumo);
			model.addObject("idOperacaoMedidor", idOperacaoMedidor);
			model.addObject("tipoAssociacao", tipoAssociacao);
		}

		return model;
	}

	/**
	 * Exibir remanejar servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao {@link HttpSession}
	 * @param reexibicao {@link Boolean}
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirRemanejarServicoAutorizacao")
	public ModelAndView exibirRemanejarServicoAutorizacao(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao,
			Boolean reexibicao)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirRemanejarServicoAutorizacao");
		model.addObject("fluxoRemanejar", Boolean.TRUE);
		this.exibirServicoAutorizacao(model, chavePrimaria, sessao);

		if (!Util.isTrue(reexibicao)) {
			removerDadosSessao(sessao);
		}

		try {
			ServicoAutorizacao servicoAutorizacao = (ServicoAutorizacao) model.getModel().get(SERVICO_AUTORIZACAO);
			controladorServicoAutorizacao.verificarStatusServicoAutorizacao(servicoAutorizacao, Constantes.C_OPERACAO_SERV_AUT_REMANEJADO);
			servicoAutorizacao.setDescricao("");
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);

		} catch(GGASException e) {
			model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Remanejar servico autorizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("remanejarServicoAutorizacao")
	public ModelAndView remanejarServicoAutorizacao(@ModelAttribute("ServicoAutorizacao") ServicoAutorizacao servicoAutorizacao,
			BindingResult result, HttpServletRequest request, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
		Map<String, Object> dados = new HashMap<>();
		try {
			popularAnexoServicoAutorizacao(servicoAutorizacao, request);
			popularMapaDados(dados, request);
			ServicoAutorizacao servicoAutorizacaoChave = (ServicoAutorizacao) sessao.getAttribute(SERVICO_AUTORIZACAO);
			controladorServicoAutorizacao.remanejar(servicoAutorizacao, dados, servicoAutorizacaoChave);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_REMANEJADA, GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
		} catch(GGASException e) {
			model = exibirRemanejarServicoAutorizacao(servicoAutorizacao.getChavePrimaria(), sessao, true);
			ServicoAutorizacao servicoAutorizacaoConsulta = controladorServicoAutorizacao.obterServicoAutorizacao(servicoAutorizacao
							.getChavePrimaria());
			servicoAutorizacaoConsulta.setEquipe(servicoAutorizacao.getEquipe());
			servicoAutorizacaoConsulta.setDescricao(servicoAutorizacao.getDescricao());
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacaoConsulta);
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Alterar servico autorizacao.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ParseException
	 *             the parse exception
	 */
	@RequestMapping("alterarServicoAutorizacao")
	public ModelAndView alterarServicoAutorizacao(@ModelAttribute("ServicoAutorizacao") ServicoAutorizacao servicoAutorizacao,
					BindingResult result, HttpServletRequest request, HttpSession sessao) throws GGASException, ParseException {
		Log.info("Inicio alterarServicoAutorizacao");
		ModelAndView model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);

		@SuppressWarnings("unchecked")
		Collection<ServicoAutorizacaoMaterial> listaMateriais = (Collection<ServicoAutorizacaoMaterial>) sessao
						.getAttribute(LISTA_AUTORIZACAO_MATERIAIS);
		@SuppressWarnings("unchecked")
		Collection<ServicoAutorizacaoEquipamento> listaEquipamentos = (Collection<ServicoAutorizacaoEquipamento>) sessao
						.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS);
		Map<String, Object> dados = new HashMap<>();
		try {
			
			
			popularAnexoServicoAutorizacao(servicoAutorizacao, request);
			popularMapaDados(dados, request);
			ServicoAutorizacao autorizacao = controladorServicoAutorizacao.atualizarServicoAutorizacao(servicoAutorizacao, dados,
							listaMateriais, listaEquipamentos);
			
			/*if(servicoAutorizacao.getServicoAutorizacaoTemp() != null) {
				ServicoAutorizacaoTemp temp = servicoAutorizacao.getServicoAutorizacaoTemp();
				temp.setServicoAutorizacao(autorizacao);
				
				controladorServicoAutorizacao.atualizarServicoAutorizacaoTemp(temp);
			}*/
			
//			model.addObject(SERVICO_AUTORIZACAO, autorizacao);
			removerDadosSessao(sessao);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GenericAction.obterMensagem(Constantes.SERVICO_AUTORIZACAO));
		} catch(GGASException e) {
			model = exibirAlteracaoServicoAutorizacao(servicoAutorizacao.getChavePrimaria(), request, sessao, true);

			servicoAutorizacao.setServicoAutorizacaoMaterial(listaMateriais);
			servicoAutorizacao.setServicoAutorizacaoEquipamento(listaEquipamentos);
			
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
			String dataPrevisaoEncerramento = request.getParameter(DATA_PREVISAO_ENCERRAMENTO);
			if(servicoAutorizacao.getDataPrevisaoEncerramento() != null) {
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				model.addObject(PREVISAO_ENCERRAMENTO, df.format(servicoAutorizacao.getDataPrevisaoEncerramento()));
			} else if(dataPrevisaoEncerramento != null && !dataPrevisaoEncerramento.isEmpty()) {
				model.addObject(PREVISAO_ENCERRAMENTO, dataPrevisaoEncerramento);
			} else {
				model.addObject(PREVISAO_ENCERRAMENTO, null);
			}
			
			String dataSolicitacaoRamal = request.getParameter(DATA_SOLICITACAO_RAMAL);
			if(servicoAutorizacao.getDataSolicitacaoRamal() != null) {
				SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
				model.addObject(DATA_SOLICITACAO_RAMAL, df.format(servicoAutorizacao.getDataSolicitacaoRamal()));
			} else if(dataSolicitacaoRamal != null && !dataSolicitacaoRamal.isEmpty()) {
				model.addObject(DATA_SOLICITACAO_RAMAL, dataSolicitacaoRamal);
			} else {
				model.addObject(DATA_SOLICITACAO_RAMAL, null);
			}
			
			sessao.setAttribute(LISTA_AUTORIZACAO_MATERIAIS, servicoAutorizacao.getServicoAutorizacaoMaterial());
			sessao.setAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS, servicoAutorizacao.getServicoAutorizacaoEquipamento());
			carregarListaAnexos(model, request);
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim alterarServicoAutorizacao");
		return model;
	}

	/**
	 * Exibir servico autorizacao.
	 * 
	 * @param model
	 *            the model
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void exibirServicoAutorizacao(ModelAndView model, Long chavePrimaria, HttpSession sessao) throws GGASException {

		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(chavePrimaria);
		Collection<ServicoAutorizacaoHistorico> historico = controladorServicoAutorizacao
						.listarServicoAutorizacaoHistorico(servicoAutorizacao.getChavePrimaria());

		for (ServicoAutorizacaoHistorico servicoAutorizacaoHistorico : historico) {
			servicoAutorizacaoHistorico.setRespostasQuestionario(
					controladorResposta.obterRespostasServicoAutorizacaoHistorico(servicoAutorizacaoHistorico.getChavePrimaria()));
		}
		
		if(servicoAutorizacao != null && servicoAutorizacao.getMedidor() != null) {
			Collection<Medidor> listaMedidor = new ArrayList<Medidor>();
			listaMedidor.add(servicoAutorizacao.getMedidor());
			
			sessao.setAttribute("listaMedidor", listaMedidor);
		}

		model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
		model.addObject("servicoAutorizacaoHistorico", historico);
		model.addObject(CLIENTE, servicoAutorizacao.getCliente());
		model.addObject("imovel", servicoAutorizacao.getImovel());
		model.addObject(PONTOS_CONSUMO, Arrays.asList(servicoAutorizacao.getPontoConsumo()));
		model.addObject(SERVICO_TIPO_PRIORIDADE, servicoAutorizacao.getServicoTipoPrioridade());
		model.addObject("contrato", servicoAutorizacao.getContrato());
		
		popularListaAnexos(model, historico,servicoAutorizacao);

		if(servicoAutorizacao.getChamado() != null ) {
			
			Collection<ChamadoHistorico> listaChamadoHistorico = controladorChamado.
					listarChamadoHistorico(servicoAutorizacao.getChamado(), null);
			
			model.addObject("listaChamadoHistorico", listaChamadoHistorico);
			
			ConstanteSistema constante = controladorConstanteSistema.
					obterConstantePorCodigo(Constantes.C_OPERACAO_CHAMADO_ANEXO);
			EntidadeConteudo operacao = controladorEntidadeConteudo.
					obterEntidadeConteudo(Long.valueOf(constante.getValor()));
			Collection<ChamadoHistorico> listaChamadoHistoricoAnexo = controladorChamado.
					listarChamadoHistorico(servicoAutorizacao.getChamado(), operacao);
			
			model.addObject("listaChamadoHistoricoAnexo", listaChamadoHistoricoAnexo);
		} else {
			model.addObject("listaChamadoHistorico", null);
						
			model.addObject("listaChamadoHistoricoAnexo", null);
		}
		
		Collection<ServicoAutorizacaoMaterial> listaServicoAutorizacaoMaterial = servicoAutorizacao.getServicoAutorizacaoMaterial();
		if(listaServicoAutorizacaoMaterial != null && !listaServicoAutorizacaoMaterial.isEmpty()) {

			int i = 0;
			Long[] chaves = new Long[listaServicoAutorizacaoMaterial.size()];
			for (ServicoAutorizacaoMaterial servicoAutorizacaoMaterial : listaServicoAutorizacaoMaterial) {
				chaves[i] = servicoAutorizacaoMaterial.getChavePrimaria();
				i = i + 1;
			}
			Collection<ServicoAutorizacaoMaterial> listaAutorizacaoMateriais = controladorServicoAutorizacao.obterMateriasOrdenado(chaves);
			sessao.setAttribute(LISTA_AUTORIZACAO_MATERIAIS, listaAutorizacaoMateriais);
		}

		sessao.setAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS, servicoAutorizacao.getServicoAutorizacaoEquipamento());

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		model.addObject(PREVISAO_ENCERRAMENTO, df.format(servicoAutorizacao.getDataPrevisaoEncerramento()));
		if(servicoAutorizacao.getDataSolicitacaoRamal() != null){
			model.addObject(DATA_SOLICITACAO_RAMAL, df.format(servicoAutorizacao.getDataSolicitacaoRamal()));
		}
		model.addObject("dataGeracao", df.format(servicoAutorizacao.getDataGeracao()));

		if (servicoAutorizacao.getCliente() != null) {
			model.addObject(TELEFONE, carregarTelefonePrincipalCliente(servicoAutorizacao.getCliente()));
			if (servicoAutorizacao.getContrato() != null) {
				model.addObject(IDCONTRATO, servicoAutorizacao.getContrato().getNumeroFormatadoSemAditivo());
			}
		}
		
		Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = new ArrayList<FaixaPressaoFornecimento>();
		
		if(servicoAutorizacao.getPontoConsumo() != null) {
			listaFaixaPressaoFornecimento =
					controladorFaixaPressaoFornecimento.listarFaixaPressaoFornecimentoPorSegmento(servicoAutorizacao.getPontoConsumo().getSegmento());
			
			Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimentoSemSegmentoAssociado =
					controladorFaixaPressaoFornecimento.listarFaixaPressaoFornecimentoSemSegmentoAssociado();
			
			listaFaixaPressaoFornecimento.addAll(listaFaixaPressaoFornecimentoSemSegmentoAssociado);
		} else {
			listaFaixaPressaoFornecimento = fachada.listarTodasFaixasPressaoFornecimento();
		}

		model.addObject("listaFaixaPressaoFornecimento", listaFaixaPressaoFornecimento);
		
		ServicoAutorizacaoTemp servicoAutorizacaoTemp = controladorServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);
		
		if(servicoAutorizacaoTemp != null) {
			model.addObject("servicoAutorizacaoTemp", servicoAutorizacaoTemp);
		}
		

		this.carregarCombos(model);

	}

	/**
	 * @param model
	 * @param historico
	 */
	private void popularListaAnexos(ModelAndView model, Collection<ServicoAutorizacaoHistorico> historico, ServicoAutorizacao servicoAutorizacao) {
		Collection<HashMap<String, Object>> listaServicoAutorizacaoHistoricoAnexo = new ArrayList<>();

		for (ServicoAutorizacaoHistorico sah : historico) {
			for (ServicoAutorizacaoHistoricoAnexo saha : sah.getAnexos()) {

				HashMap<String, Object> hmp = new HashMap<>();
				hmp.put("descricao", sah.getDescricao());
				hmp.put("ultimaAlteracao", saha.getUltimaAlteracao());
				hmp.put("usuarioLogin", sah.getUsuarioSistema().getLogin());
				hmp.put("nomeDocumentoAnexo", saha.getDescricaoAnexo());
				hmp.put("idAnexo", saha.getChavePrimaria());
				hmp.put("operacao", sah.getOperacao().getDescricao());
				listaServicoAutorizacaoHistoricoAnexo.add(hmp);
				hmp.put("mobile", false);
			}
		}
		
		Collection<ServicoAutorizacaoTempAnexo> anexosApp = 
				controladorServicoAutorizacao.listarAnexosAutorizacaoServicoEncerramentoAPP(servicoAutorizacao);

		
		String nomeFuncionario = obterNomeFuncionarioMobile(anexosApp);
		
		for (ServicoAutorizacaoTempAnexo anexoApp : anexosApp) {
			String[] nomeAnexo = anexoApp.getCaminho().split("/");
			
			HashMap<String, Object> hmp = new HashMap<>();
			hmp.put("descricao", "Documento vindo do app");
			hmp.put("ultimaAlteracao", anexoApp.getUltimaAlteracao());
			hmp.put("usuarioLogin", nomeFuncionario.equals("") ? anexoApp.getServicoAutorizacao().getUsuarioSistema().getLogin() : nomeFuncionario);
			hmp.put("nomeDocumentoAnexo", nomeAnexo[nomeAnexo.length - 1]);
			hmp.put("idAnexo", anexoApp.getChavePrimaria());
			hmp.put("operacao", null);
			hmp.put("mobile", true);
			listaServicoAutorizacaoHistoricoAnexo.add(hmp);
		}
		model.addObject("listaServicoAutorizacaoHistoricoAnexo", listaServicoAutorizacaoHistoricoAnexo);
	}

	private String obterNomeFuncionarioMobile(Collection<ServicoAutorizacaoTempAnexo> anexosApp) {
		
		String nomeFuncionario = "";
		
		if(anexosApp != null && !anexosApp.isEmpty()) {
			ServicoAutorizacao servicoAutorizacao = anexosApp.iterator().next().getServicoAutorizacao();
			ServicoAutorizacaoTemp servicoAutorizacaoTemp = controladorServicoAutorizacao.consultarAutorizacaoServicoTemp(servicoAutorizacao);
			nomeFuncionario = servicoAutorizacaoTemp.getFuncionario().getNome();
		}
		return nomeFuncionario;
	}

	/**
	 * Carregar prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ParseException
	 *             the parse exception
	 */
	@RequestMapping("carregarPrioridade")
	public ModelAndView carregarPrioridade(@RequestParam(value = "servicoTipo", required = false) Long chavePrimaria)
					throws NegocioException, ParseException {

		ModelAndView model = new ModelAndView("inputServicoTipoPrioridade");

		String[] tipoServico = controladorServicoAutorizacao.obterPrevisaoEncerramento(chavePrimaria);
		ServicoTipoPrioridade servicoTipoPrioridade = new ServicoTipoPrioridade();
		servicoTipoPrioridade.setChavePrimaria(Long.parseLong(tipoServico[PRIORIDADE_SERVICO]));
		servicoTipoPrioridade.setDescricao(tipoServico[0]);
		model.addObject(SERVICO_TIPO_PRIORIDADE, servicoTipoPrioridade);
		model.addObject(PREVISAO_ENCERRAMENTO, tipoServico[1]);
		model.addObject(FLUXO_INCLUSAO, true);

		return model;
	}

	/**
	 * Carregar prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ParseException
	 *             the parse exception
	 */
	@RequestMapping("carregarEquipePrioritaria")
	public ModelAndView carregarEquipePrioritaria(@RequestParam(value = "servicoTipo", required = false) Long chavePrimaria)
					throws NegocioException, ParseException {

		ModelAndView model = new ModelAndView("inputServicoTipoEquipePrioritaria");

		Equipe equipe = controladorServicoTipo.obterEquipePrioritaria(chavePrimaria);
		model.addObject(EQUIPE_PRIORITARIA, equipe);
		return model;
	}
	
	/**
	 * Carregar cliente.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param cpfCnpj
	 *            the cpf cnpj
	 * @param passaporte
	 *            the passaporte
	 * @param email
	 *            the email
	 * @param telefone
	 *            the telefone
	 * @param response
	 *            the response
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarClienteServicoAutorizacao")
	public ModelAndView carregarCliente(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = "cpfCnpj", required = false) String cpfCnpj,
					@RequestParam(value = "passaporte", required = false) String passaporte,
					@RequestParam(value = "email", required = false) String email,
					@RequestParam(value = "telefone", required = false) String telefone, HttpServletResponse response, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_DIV_CLIENTE);
		Cliente cliente = null;
		try {
			if(chavePrimaria != null) {
				cliente = (Cliente) controladorCliente.obter(chavePrimaria);
			} else if(telefone != null && !telefone.isEmpty()) {
				cliente = controladorCliente.consultarClientePorTelefone(telefone);
			} else {
				Map<String, Object> filtro = new HashMap<>();
				if(Util.removerCaracteresEspeciais(cpfCnpj).length() <= TAMANHO_CPF) {
					filtro.put("cpf", cpfCnpj);
				} else if(Util.removerCaracteresEspeciais(cpfCnpj).length() > TAMANHO_CPF) {
					filtro.put("cnpj", cpfCnpj);
				}
				filtro.put("passaporte", passaporte);
				filtro.put("email", email);
				List<Cliente> clientes = (List<Cliente>) controladorCliente.consultarClientes(filtro);
				cliente = clientes.get(0);
			}

			if (cliente != null) {
				cliente.getContatos();
				model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_GENERICO_DE_SISTEMAS);
		}
		model.addObject(CLIENTE, cliente);
		ParametroSistema exibirPassaporte = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_PASSAPORTE_CLIENTE);
		model.addObject("exibirPassaporte", exibirPassaporte.getValor());
		model.addObject(TELEFONE, "");
		return model;
	}

	/**
	 * @param cliente
	 * @return
	 */
	private String carregarTelefonePrincipalCliente(Cliente cliente) {
		StringBuilder telefone = new StringBuilder();
		Collection<ClienteFone> fones = cliente.getFones();

		if (fones != null) {

			for (ClienteFone fone : fones) {
				if (Util.isTrue(fone.getIndicadorPrincipal()) || telefone.length() == 0) {

					formatarTelefone(telefone, fone);
				}
			}
		}
		return telefone.toString();
	}

	/**
	 * Formata a exibição do telefone
	 * 
	 * @param telefone
	 * @param fone
	 */
	private void formatarTelefone(StringBuilder telefone, ClienteFone fone) {
		if (telefone.length() > 0) {
			telefone.setLength(0);
		}

		if (fone.getNumero() != null) {

			if (fone.getCodigoDDD() != null) {
				telefone.append("(").append(fone.getCodigoDDD()).append(") ");
			}

			telefone.append(fone.getNumero());

			if (fone.getRamal() != null) {
				telefone.append(" - ").append(fone.getRamal());
			}
		}
	}

	/**
	 * Carregar cliente por contrato.
	 * 
	 * @param numeroContrato
	 *            the numero contrato
	 * @param response
	 *            the response
	 * @param session
	 *            the session
	 * @return the model and view
	 */

	@RequestMapping("carregarClientePorContratoServicoAutorizacao")
	public ModelAndView carregarClientePorContratoServicoAutorizacao(@RequestParam("numeroContrato") String numeroContrato,
			HttpServletResponse response, HttpSession session) {

		ModelAndView model = new ModelAndView(TELA_DIV_CLIENTE);

		model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
		model.addObject(FLUXO_DETALHAMENTO, Boolean.TRUE);
		try {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put("numero", Integer.valueOf(numeroContrato));
			Cliente cliente =
					ServiceLocator.getInstancia().getControladorCliente().obterClienteAssinaturaPeloNumeroContrato(numeroContrato);

			ServicoAutorizacao servicoAutorizacao = new ServicoAutorizacao();
			servicoAutorizacao.setCliente(cliente);

			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
			model.addObject(CLIENTE, cliente);
			if (cliente == null) {
				model = new ModelAndView(TELA_AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
			} else {
				model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
				model.addObject(IDCONTRATO, servicoAutorizacao.getContrato().getNumeroFormatadoSemAditivo());
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Carregar contrato por ponto consumo.
	 * 
	 * @param numeroContrato the numero contrato
	 * @param response the response
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarContratoPorNumeroSA")
	public ModelAndView carregarContratoPorNumeroSA(@RequestParam("numeroContrato") String numeroContrato, HttpServletResponse response)
			throws GGASException {

		ModelAndView model = new ModelAndView("divContrato");
		try {
			Contrato contrato = pesquisarContratoPorNumero(numeroContrato);
			ServicoAutorizacao servicoAutorizacao = new ServicoAutorizacao();
			servicoAutorizacao.setContrato(contrato);
			model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CONTRATO_ATIVO_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Pesquisa contrato por numero
	 * 
	 * @param numeroContrato
	 * @return contrato
	 * @throws NegocioException
	 */
	private Contrato pesquisarContratoPorNumero(String numeroContrato) throws NegocioException {

		ArrayList<Contrato> contratos = (ArrayList<Contrato>) controladorContrato.consultarContratoPorNumero(numeroContrato);

		if (contratos != null && contratos.size() == 1) {
			return contratos.get(0);
		}
		return null;
	}

	/**
	 * Carregar servico autorizacao cliente imovel.
	 * 
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("carregarClienteImovelServicoAutorizacao")
	public ModelAndView carregarServicoAutorizacaoClienteImovel(
					@RequestParam(value = "chavePrimariaCliente", required = false) Long chavePrimariaCliente,
					@RequestParam(value = "chavePrimariaImovel", required = false) Long chavePrimariaImovel,
					@RequestParam(value = "chavePrimariaPontoConsumo", required = false) Long chavePrimariaPontoConsumo,
					HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("gridServicoAutorizacaoCliente");
		try {
			List<ServicoAutorizacao> servicoAutorizacaoCliente = (List<ServicoAutorizacao>) controladorServicoAutorizacao
							.consultarServicoAutorizacaoPorClienteOuImovel(chavePrimariaCliente, chavePrimariaImovel,
											chavePrimariaPontoConsumo);
			model.addObject("servicoAutorizacaoCliente", servicoAutorizacaoCliente);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			response.sendError(1);
		}

		return model;
	}

	/**
	 * Carregar pontos consumo.
	 * 
	 * @param chaveImovel
	 *            the chave imovel
	 * @param fluxoExecucao
	 *            the fluxo execucao
	 * @param response
	 *            the response
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("carregarPontosConsumoServicoAutorizacao")
	public ModelAndView carregarPontosConsumo(@RequestParam("chavePrimariaImovel") Long chaveImovel,
					@RequestParam("fluxoExecucao") String fluxoExecucao, HttpServletResponse response) throws GGASException, IOException {

		ModelAndView model = new ModelAndView("gridPontosConsumo");
		try {
			Map<String, Object> filtro = new HashMap<>();
			Long[] chavesPrimarias = new Long[] {chaveImovel};
			filtro.put("chavesPrimariasImoveis", chavesPrimarias);
			List<PontoConsumo> pontosConsumo = (List<PontoConsumo>) controladorPontoConsumo.consultarPontosConsumo(filtro);
			model.addObject(PONTOS_CONSUMO, pontosConsumo);

			if(FLUXO_INCLUSAO.equals(fluxoExecucao)) {
				model.addObject(FLUXO_INCLUSAO, Boolean.TRUE);
			} else {
				model.addObject(FLUXO_INCLUSAO, null);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			response.sendError(1);
		}
		// TESTE
		return model;
	}

	/**
	 * Carregar imoveis por cliente.
	 * 
	 * @param chaveCliente
	 *            the chave cliente
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarImoveisPorClienteServicoAutorizacao")
	public ModelAndView carregarImoveisPorCliente(@RequestParam("chavePrimariaCliente") Long chaveCliente, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView(GRID_IMOVEIS);
		List<Imovel> listaImoveis = controladorServicoAutorizacao.listarImoveisPorCliente(chaveCliente);
		session.setAttribute(LISTA_IMOVEIS_CHAMADO, listaImoveis);

		return model;
	}

	/**
	 * Carregar pontos consumo.
	 * 
	 * @param chavePrimariaContrato the chave contrato
	 * @param numeroContrato the numero contrato
	 * @param session the session
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("carregarImovelPorContratoAS")
	public ModelAndView carregarImovelPorContratoAS(@RequestParam("chavePrimariaContrato") Long chavePrimariaContrato,
			@RequestParam("numeroContrato") String numeroContrato, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_IMOVEIS);
		Long chaveContrato = chavePrimariaContrato;

		if (chavePrimariaContrato == null && (numeroContrato != null && !numeroContrato.isEmpty())) {
			Contrato contrato = pesquisarContratoPorNumero(numeroContrato);
			if (contrato != null) {
				chaveContrato = contrato.getChavePrimaria();
			}
		}

		if (chaveContrato != null && chaveContrato > 0) {
			List<ContratoPontoConsumo> contratoPontosConsumo =
					(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(chaveContrato);
			List<Imovel> listaImoveis = new ArrayList<>();
			for (ContratoPontoConsumo contratoPontoConsumo : contratoPontosConsumo) {
				Map<String, Object> filtro = new HashMap<>();
				filtro.put(CHAVE_PRIMARIA, contratoPontoConsumo.getPontoConsumo().getImovel().getChavePrimaria());

				Collection<Imovel> imoveis = controladorImovel.consultarImoveis(filtro);

				if (!listaImoveis.containsAll(imoveis)) {
					listaImoveis.addAll(imoveis);
				}
			}
			session.setAttribute(LISTA_IMOVEIS_CHAMADO, listaImoveis);
		} else {
			session.removeAttribute(LISTA_IMOVEIS_CHAMADO);
		}

		return model;
	}

	/**
	 * Carregar cliente por ponto consumo.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param response
	 *            the response
	 * @return the model and view
	 */
	@RequestMapping("carregarClientePorPontoConsumoServicoAutorizacao")
	public ModelAndView carregarClientePorPontoConsumo(@RequestParam("chavePontoConsumo") Long chavePontoConsumo,
					HttpServletResponse response) {

		ModelAndView model = new ModelAndView(TELA_DIV_CLIENTE);
		try {
			Cliente cliente = controladorCliente.obterClientePorPontoConsumo(chavePontoConsumo);
			
			if(cliente != null) {
				ServicoAutorizacao servicoAutorizacao = new ServicoAutorizacao();
				servicoAutorizacao.setCliente(cliente);
				model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
				model.addObject(CLIENTE, cliente);
				model.addObject(TELEFONE, carregarTelefonePrincipalCliente(cliente));
			}else {
				model = new ModelAndView(TELA_AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_CLIENTE_NAO_LOCALIZADO);
		}

		return model;
	}

	/**
	 * Carrega nomes clientes.
	 * 
	 * @param nome
	 *            the nome
	 * @param nomeFantasia
	 *            the nome fantasia
	 * @param response
	 *            the response
	 * @return the cliente[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("carregaNomesClientesServicoAutorizacao")
	@ResponseBody
	public ClienteVOAutoComplete[] carregaNomesClientes(@RequestParam(value = "nome", required = false) String nome,
			@RequestParam(value = "nomeFantasia", required = false) String nomeFantasia, HttpServletResponse response)
			throws NegocioException {

		ControladorCliente controladorClientes = ServiceLocator.getInstancia().getControladorCliente();
		Map<String, Object> filtro = new HashMap<>();
		if(nomeFantasia == null) {
			filtro.put("nome", nome);
		} else {
			filtro.put("nomeFantasia", nomeFantasia);
		}
		Collection<Cliente> clientes = controladorClientes.consultarClientes(filtro);
		List<Cliente> clientesAtuais = popularListaClientes(clientes);

		List<ClienteVOAutoComplete> listaClienteVoAutoComplete = new ArrayList<>();
		ClienteVOAutoComplete clienteVo = null;

		StringBuilder informacoesAltoComplete = new StringBuilder();
		int index = 0;
		for (int i = 0; i < clientesAtuais.size(); i++) {
			clienteVo = new ClienteVOAutoComplete();
			Cliente cliente = clientesAtuais.get(i);
			Long chaveCliente = cliente.getChavePrimaria();

			clienteVo.setChavePrimaria(chaveCliente);

			String nomeCliente = cliente.getNome();
			clienteVo.setNome("NOME : " + nomeCliente + "\r\n");
			String cnpj = cliente.getCnpj();
			String cpf = cliente.getCpf();
			if (cnpj == null || cnpj.isEmpty()) {
				clienteVo.setCpfCnpj("\r\n Cpf/Cnpj : " + cpf + "\r\n");
			} else {
				clienteVo.setCpfCnpj(" \r\n Cpf/Cnpj : " + cnpj + "\r\n");
			}

			String nomeFantasiaCliente = cliente.getNomeFantasia();
			if (nomeFantasiaCliente != null) {
				clienteVo.setNomeFantasia(nomeFantasiaCliente);
			}

			List<ContratoPontoConsumo> listaContratoPontoConsumo =
					(List<ContratoPontoConsumo>) controladorContrato.obterContratoPontoConsumoPorCliente(chaveCliente, "cep", "contrato");
			if (!listaContratoPontoConsumo.isEmpty()) {
				for (int j = 0; j < listaContratoPontoConsumo.size(); j++) {
					ClienteVOAutoComplete clienteVOAutoComplete = new ClienteVOAutoComplete();
					ContratoPontoConsumo contratoPontoConsumo = listaContratoPontoConsumo.get(j);

					Contrato contrato = contratoPontoConsumo.getContrato();

					String enderecoFormatado = contratoPontoConsumo.getPontoConsumo().getEnderecoFormatado();
					atribuiEnderecoFormatado(clienteVOAutoComplete, enderecoFormatado);

					Date dateVigencia = contrato.getDataAssinatura();
					SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
					Date dateRecisao = contrato.getDataRecisao();
					String numeroFormatado = contrato.getNumeroFormatado();
					String dataVigencia = df.format(dateVigencia);

					verificaVigenciaRecisaoNumeroFormatado(clienteVOAutoComplete, df, dateRecisao, numeroFormatado, dataVigencia);

					clienteVOAutoComplete.setChavePrimaria(clienteVo.getChavePrimaria());
					clienteVOAutoComplete.setNome(clienteVo.getNome());
					clienteVOAutoComplete.setCpfCnpj(clienteVo.getCpfCnpj());
					clienteVOAutoComplete.setNomeFantasia(clienteVo.getNomeFantasia());
					clienteVOAutoComplete.setIdContrato(contrato.getChavePrimaria());

					listaClienteVoAutoComplete.add(index, clienteVOAutoComplete);
					index++;
					informacoesAltoComplete.append(enderecoFormatado);
				}

			} else {
				clienteVo.setNumeroContrato(" ");
				clienteVo.setInicioVigenciaContrato(" ");
				clienteVo.setFimVigenciaContrato(" ");
				clienteVo.setEndereco(" ");
				clienteVo.setIdContrato(null);
				listaClienteVoAutoComplete.add(index, clienteVo);
				index++;

			}

		}

		return listaClienteVoAutoComplete.toArray(new ClienteVOAutoComplete[listaClienteVoAutoComplete.size()]);

	}

	/**
	 * Atribui endereço formatado
	 * 
	 * @param clienteVOAutoComplete
	 * @param enderecoFormatado
	 */
	private void atribuiEnderecoFormatado(ClienteVOAutoComplete clienteVOAutoComplete, String enderecoFormatado) {
		if (enderecoFormatado != null && !enderecoFormatado.isEmpty()) {
			clienteVOAutoComplete.setEndereco("\r\n Endereço Consumo : " + enderecoFormatado + "\r\n");
		} else {
			clienteVOAutoComplete.setEndereco(" ");
		}
	}

	/**
	 * 
	 * @param clienteVOAutoComplete
	 * @param df
	 * @param dateRecisao
	 * @param numeroFormatado
	 * @param dataVigencia
	 */
	private void verificaVigenciaRecisaoNumeroFormatado(ClienteVOAutoComplete clienteVOAutoComplete, SimpleDateFormat df, Date dateRecisao,
			String numeroFormatado, String dataVigencia) {
		if (!dataVigencia.isEmpty()) {
			clienteVOAutoComplete.setInicioVigenciaContrato(" Vingecia Inicial : " + dataVigencia + "\r\n");
		} else {
			clienteVOAutoComplete.setInicioVigenciaContrato(" ");
		}
		if (dateRecisao != null) {
			String dataRevisao = df.format(dateRecisao);
			clienteVOAutoComplete.setFimVigenciaContrato("Vigencia Final: " + dataRevisao + "\r\n");
		} else {
			clienteVOAutoComplete.setFimVigenciaContrato(" ");
		}
		if (numeroFormatado != null && !numeroFormatado.isEmpty()) {
			clienteVOAutoComplete.setNumeroContrato(" Contrato : " + numeroFormatado);
		} else {
			clienteVOAutoComplete.setNumeroContrato(" ");
		}
	}

	/**
	 * Popular lista clientes.
	 * 
	 * @param clientes
	 *            the clientes
	 * @return the list
	 */
	private List<Cliente> popularListaClientes(Collection<Cliente> clientes) {

		List<Cliente> clientesAtuais = new ArrayList<>();
		Cliente clienteAtual = null;
		for (Cliente cliente : clientes) {
			clienteAtual = (Cliente) ServiceLocator.getInstancia().getBeanPorID(Cliente.BEAN_ID_CLIENTE);
			clienteAtual.setChavePrimaria(cliente.getChavePrimaria());
			clienteAtual.setNome(cliente.getNome());
			clienteAtual.setNomeFantasia(cliente.getNomeFantasia());
			clientesAtuais.add(clienteAtual);
		}
		return clientesAtuais;
	}

	/**
	 * Validar equipamento existente.
	 * 
	 * @param equipamentos
	 *            the equipamentos
	 * @param servicoAutorizacaoEquipamento
	 *            the servico autorizacao equipamento
	 * @return true, if successful
	 */
	private boolean validarEquipamentoExistente(Collection<ServicoAutorizacaoEquipamento> equipamentos,
					ServicoAutorizacaoEquipamento servicoAutorizacaoEquipamento) {

		for (ServicoAutorizacaoEquipamento equipamento : equipamentos) {
			if(equipamento.getEquipamento().getChavePrimaria() == servicoAutorizacaoEquipamento.getEquipamento().getChavePrimaria()) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Validar material existente.
	 * 
	 * @param materiais
	 *            the materiais
	 * @param servicoAutorizacaoMaterial
	 *            the servico autorizacao material
	 * @return true, if successful
	 */
	private boolean validarMaterialExistente(Collection<ServicoAutorizacaoMaterial> materiais,
					ServicoAutorizacaoMaterial servicoAutorizacaoMaterial) {

		for (ServicoAutorizacaoMaterial material : materiais) {
			if(material.getMaterial().getChavePrimaria() == servicoAutorizacaoMaterial.getMaterial().getChavePrimaria()) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Adicionar equipamento.
	 * 
	 * @param chaveEquipamento
	 *            the chave equipamento
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarEquipamento")
	public ModelAndView adicionarEquipamento(@RequestParam("equipamento") Long chaveEquipamento, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridEquipamentosServicoAutorizacao");
		ServicoAutorizacaoEquipamento servicoAutorizacaoEquipamento = new ServicoAutorizacaoEquipamento();

		EntidadeConteudo equipamento = controladorEntidadeConteudo.obter(chaveEquipamento);
		servicoAutorizacaoEquipamento.setEquipamento(equipamento);

		Collection<ServicoAutorizacaoEquipamento> equipamentos = new ArrayList<>();
		if(session.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS) != null) {
			equipamentos.addAll((Collection<ServicoAutorizacaoEquipamento>) session.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS));
			session.setAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS, equipamentos);
		} else {
			if(session.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS) == null) {
				session.setAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS, equipamentos);
			}
		}

		if(!validarEquipamentoExistente(equipamentos, servicoAutorizacaoEquipamento)) {
			((Collection<ServicoAutorizacaoEquipamento>) session.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS))
							.add(servicoAutorizacaoEquipamento);
		} else {
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_SERVICO_EQUIPAMENTO_EXISTENTE);
		}
		carregarCombos(model);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		return model;
	}

	/**
	 * Remover equipamento servico autorizacao.
	 * 
	 * @param chavePrimariaEquipamento
	 *            the chave primaria equipamento
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerEquipamentoServicoAutorizacao")
	public ModelAndView removerEquipamentoServicoAutorizacao(@RequestParam("chavePrimariaEquipamento") Long chavePrimariaEquipamento,
					HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridEquipamentosServicoAutorizacao");

		Collection<ServicoAutorizacaoEquipamento> listaEquipamentos = (Collection<ServicoAutorizacaoEquipamento>) session
						.getAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS);
		for (ServicoAutorizacaoEquipamento equipamento : listaEquipamentos) {
			if(equipamento.getEquipamento().getChavePrimaria() == chavePrimariaEquipamento) {
				listaEquipamentos.remove(equipamento);
				break;
			}
		}
		carregarCombos(model);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		return model;
	}

	/**
	 * Incluir material servico autorizacao.
	 * 
	 * @param chaveMaterial
	 *            the chave material
	 * @param quantidadeMaterial
	 *            the quantidade material
	 * @param indexList
	 *            the index list
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirMaterialServicoAutorizacao")
	public ModelAndView incluirMaterialServicoAutorizacao(@RequestParam("material") Long chaveMaterial,
					@RequestParam("quantidadeMaterial") Integer quantidadeMaterial, @RequestParam("indexList") Integer indexList,
					HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridMateriaisServicoAutorizacao");

		ServicoAutorizacaoMaterial servicoAutorizacaoMaterial = new ServicoAutorizacaoMaterial();

		Material material = (Material) controladorServicoTipo.obterMaterial(chaveMaterial);
		servicoAutorizacaoMaterial.setMaterial(material);
		servicoAutorizacaoMaterial.setQuantidadeMaterial(quantidadeMaterial);

		Collection<ServicoAutorizacaoMaterial> materiais = new ArrayList<>();
		if(session.getAttribute(LISTA_AUTORIZACAO_MATERIAIS) != null) {
			materiais.addAll((Collection<ServicoAutorizacaoMaterial>) session.getAttribute(LISTA_AUTORIZACAO_MATERIAIS));
			session.setAttribute(LISTA_AUTORIZACAO_MATERIAIS, materiais);
		} else {
			if(session.getAttribute(LISTA_AUTORIZACAO_MATERIAIS) == null) {
				session.setAttribute(LISTA_AUTORIZACAO_MATERIAIS, materiais);
			}
		}

		// caso seja inclusão de um novo material, o indexList não é informado
		if(indexList == null || indexList < 0) {
			if(!validarMaterialExistente(materiais, servicoAutorizacaoMaterial)) {
				((Collection<ServicoAutorizacaoMaterial>) session.getAttribute(LISTA_AUTORIZACAO_MATERIAIS))
								.add(servicoAutorizacaoMaterial);
			} else {
				model = new ModelAndView(TELA_AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_SERVICO_MATERIAL_EXISTENTE);
			}
		} else {
			this.alterarMaterial(indexList, servicoAutorizacaoMaterial, session);
		}

		carregarCombos(model);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		return model;
	}

	/**
	 * Alterar material.
	 * 
	 * @param indexLista
	 *            the index lista
	 * @param material
	 *            the material
	 * @param session
	 *            the session
	 */
	@SuppressWarnings("unchecked")
	private void alterarMaterial(Integer indexLista, ServicoAutorizacaoMaterial material, HttpSession session) {

		Collection<ServicoAutorizacaoMaterial> listaMateriais = new ArrayList<>();
		listaMateriais.addAll((Collection<ServicoAutorizacaoMaterial>) session.getAttribute(LISTA_AUTORIZACAO_MATERIAIS));
		
		if(CollectionUtils.isNotEmpty(listaMateriais) && indexLista != null) {

			for (int i = 0; i < listaMateriais.size(); i++) {
				ServicoAutorizacaoMaterial materialExistente = ((ArrayList<ServicoAutorizacaoMaterial>) listaMateriais).get(i);
				if(i == indexLista && materialExistente != null) {
					materialExistente.setQuantidadeMaterial(material.getQuantidadeMaterial());
					materialExistente.setMaterial(material.getMaterial());
					break;
				}
			}

		}
		session.setAttribute(LISTA_AUTORIZACAO_MATERIAIS, listaMateriais);

	}

	/**
	 * Remover material servico autorizacao.
	 * 
	 * @param chavePrimariaMaterial
	 *            the chave primaria material
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerMaterialServicoAutorizacao")
	public ModelAndView removerMaterialServicoAutorizacao(@RequestParam("chavePrimariaMaterial") Long chavePrimariaMaterial,
					HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridMateriaisServicoAutorizacao");

		Collection<ServicoAutorizacaoMaterial> listaMateriais = (Collection<ServicoAutorizacaoMaterial>) session
						.getAttribute(LISTA_AUTORIZACAO_MATERIAIS);
		for (ServicoAutorizacaoMaterial material : listaMateriais) {
			if(material.getMaterial().getChavePrimaria() == chavePrimariaMaterial) {
				listaMateriais.remove(material);
				break;
			}
		}
		carregarCombos(model);
		model.addObject(FLUXO_ALTERACAO, Boolean.TRUE);
		return model;
	}

	/**
	 * Imprimir servico autorizacao.
	 * 
	 * @param idServicoAutorizacao  {@link Long}
	 * @param comChamado  {@link Boolean}
	 * @param request  {@link HttpServletRequest}
	 * @param response  {@link HttpServletResponse}
	 * @throws IOException
	 * @throws GGASException  {@link GGASException}
	 */
	@RequestMapping("imprimirServicoAutorizacao")
	public void imprimirServicoAutorizacao(@RequestParam("idServicoAutorizacao") Long idServicoAutorizacao,
			@RequestParam Boolean comChamado, HttpServletRequest request, HttpServletResponse response)
			throws GGASException {

		Log.info("Inicio imprimirServicoAutorizacao");
		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(idServicoAutorizacao);
		Collection<ServicoAutorizacaoHistorico> historico =
				controladorServicoAutorizacao.listarServicoAutorizacaoHistorico(servicoAutorizacao.getChavePrimaria());

		for (ServicoAutorizacaoHistorico servicoAutorizacaoHistorico : historico) {
			servicoAutorizacaoHistorico.setRespostasQuestionario(
					controladorResposta.obterRespostasServicoAutorizacaoHistorico(servicoAutorizacaoHistorico.getChavePrimaria()));
		}

		byte[] relatorio =
				controladorServicoAutorizacao.gerarRelatorio(idServicoAutorizacao, comChamado, historico);

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();
		fileName.append(" attachment; filename=");
		fileName.append(gerarNomeRelatorio(servicoAutorizacao));
		
		if (relatorio != null) {

			ServletOutputStream servletOutputStream;
			try {
				servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION, fileName.toString());
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch (IOException e) {
				LOG.error(e.getMessage());
				throw new GGASException(e);
			}
		}
		
		Log.info("Fim imprimirServicoAutorizacao");
	}
	
	/**
	 * Imprimir servicos autorizacao selecionados
	 *
	 * @param chavesPrimarias {@link Long}
	 * @param comAS {@link Boolean}
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @throws GGASException 
	 * @throws IOException {@link IOException}
	 */
	@RequestMapping("imprimirServicosAutorizacao")
	public void imprimirServicosAutorizacao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam("comChamado") Boolean comChamado, HttpServletRequest request, HttpServletResponse response)
			throws GGASException, IOException {

		Log.info("Inicio imprimirServicosAutorizacao");

		if (chavesPrimarias.length == 1) {
		    // Apenas uma chave primária, enviar PDF diretamente
		    Long chavePrimaria = chavesPrimarias[0];  // Obtém o único valor

		    ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao
		            .obterServicoAutorizacao(chavePrimaria);
		    
		    Collection<ServicoAutorizacaoHistorico> historico = controladorServicoAutorizacao
		            .listarServicoAutorizacaoHistorico(servicoAutorizacao.getChavePrimaria());

		    for (ServicoAutorizacaoHistorico servicoAutorizacaoHistorico : historico) {
		        servicoAutorizacaoHistorico.setRespostasQuestionario(controladorResposta
		                .obterRespostasServicoAutorizacaoHistorico(servicoAutorizacaoHistorico.getChavePrimaria()));
		    }

		    byte[] relatorio = controladorServicoAutorizacao.gerarRelatorio(chavePrimaria, comChamado, historico);

		    if (relatorio != null) {
		        // Definir o nome do arquivo PDF
		        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		        String diaMesAno = df.format(Calendar.getInstance().getTime());
		        df = new SimpleDateFormat("HH'h'mm");
		        String hora = df.format(Calendar.getInstance().getTime());

		        String fileName = gerarNomeRelatorio(servicoAutorizacao);

		        // Enviar o PDF diretamente na resposta HTTP
		        response.setContentType("application/pdf");
		        response.setContentLength(relatorio.length);
		        response.addHeader("Content-Disposition", "attachment; filename=" + fileName);

		        ServletOutputStream servletOutputStream = response.getOutputStream();
		        servletOutputStream.write(relatorio, 0, relatorio.length);
		        servletOutputStream.flush();
		        servletOutputStream.close();
		    }

		} else {
		    // Mais de uma chave, compactar PDFs em um arquivo ZIP
		    ByteArrayOutputStream zipOutputStream = new ByteArrayOutputStream();
		    ZipOutputStream zipStream = new ZipOutputStream(zipOutputStream);

		    for (Long chavePrimaria : chavesPrimarias) {
		        ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao
		                .obterServicoAutorizacao(chavePrimaria);

		        Collection<ServicoAutorizacaoHistorico> historico = controladorServicoAutorizacao
		                .listarServicoAutorizacaoHistorico(servicoAutorizacao.getChavePrimaria());

		        for (ServicoAutorizacaoHistorico servicoAutorizacaoHistorico : historico) {
		            servicoAutorizacaoHistorico.setRespostasQuestionario(controladorResposta
		                    .obterRespostasServicoAutorizacaoHistorico(servicoAutorizacaoHistorico.getChavePrimaria()));
		        }

		        byte[] relatorio = controladorServicoAutorizacao.gerarRelatorio(chavePrimaria, comChamado, historico);

		        if (relatorio != null) {
		            // Adicionar cada PDF ao arquivo ZIP
		            ZipEntry zipEntry = new ZipEntry(gerarNomeRelatorio(servicoAutorizacao));
		            zipStream.putNextEntry(zipEntry);
		            zipStream.write(relatorio);
		            zipStream.closeEntry();
		        }
		    }

		    // Finalizar o arquivo ZIP
		    zipStream.close();

		    // Configurar a resposta HTTP para download do arquivo ZIP
		    response.setContentType("application/zip");
		    response.setContentLength(zipOutputStream.size());
		    response.setHeader("Content-Disposition", "attachment; filename=relatorios.zip");

		    // Enviar o arquivo ZIP
		    ServletOutputStream servletOutputStream = response.getOutputStream();
		    zipOutputStream.writeTo(servletOutputStream);
		    servletOutputStream.flush();
		    servletOutputStream.close();
		}



		Log.info("Fim imprimirServicosAutorizacao");
	}

	private String gerarNomeRelatorio(ServicoAutorizacao servicoAutorizacao) {
		StringBuilder nomeRelatorio = new StringBuilder();
		
		if(servicoAutorizacao.getChamado() != null && servicoAutorizacao.getChamado().getProtocolo() != null) {
			nomeRelatorio.append(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo());
		}
		
		if(servicoAutorizacao.getPontoConsumo() != null) {
			nomeRelatorio.append(" ");
			String nomePontoConsumo = servicoAutorizacao.getPontoConsumo().getDescricao();
			nomePontoConsumo = nomePontoConsumo.replaceAll("\\p{Punct}", "");
			nomeRelatorio.append(nomePontoConsumo);
		}
		
		if(nomeRelatorio.toString().isEmpty() || servicoAutorizacao.getChamado() == null) {
			if(!nomeRelatorio.toString().isEmpty()) {
				nomeRelatorio.append(" ");
			}
		}
		
		if(servicoAutorizacao.getChamado() == null) {
			nomeRelatorio.append("AS - " + servicoAutorizacao.getChavePrimaria());
		}
		
		nomeRelatorio.append(".pdf");
		
		return nomeRelatorio.toString();
	}

	/**
	 * Exibir pesquisa relatorio autorizacao servico.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirPesquisaRelatorioAutorizacaoServico")
	public ModelAndView exibirPesquisaRelatorioAutorizacaoServico(HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioAutorizacaoServico");
		try {
			ServicoAutorizacaoVO servicoAutorizacaoVO = new ServicoAutorizacaoVO();
			servicoAutorizacaoVO.setChamadoVO(new ChamadoVO());
			this.carregarCombos(model);
			this.carregarCombosChamado(model);
			model.addObject(SERVICO_AUTORIZACAO_VO, servicoAutorizacaoVO);
			return model;
		} catch(NegocioException e) {
			mensagemErroParametrizado(model, e);
		} catch(GGASException e) {
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Exibir pesquisa relatorio indicador emergencia.
	 *
	 * @param request the request
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaRelatorioIndicadorEmergencia")
	public ModelAndView exibirPesquisaRelatorioIndicadorEmergencia(HttpServletRequest request) throws GGASException {
		
		ModelAndView model = new ModelAndView("exibirPesquisaRelatorioIndicadorEmergencia");
		
		carregarCombos(model);
		removerDadosSessao(request);
		return model;
	}
	
	@RequestMapping("gerarRelatorioIndicadorEmergencia")
	public ModelAndView gerarRelatorioIndicadorEmergencia(
			@RequestParam("periodoCriacao") String periodoCriacao,
			@RequestParam("descricaoTipoServico")String descricaoTipoServico,
			@RequestParam("exibirFiltros") String exibirFiltros,
			@RequestParam("tipoRelatorio") String tipoRelatorio,
			@RequestParam("redeLocal") Long redeLocal,
			HttpServletRequest request,
			HttpServletResponse response) throws GGASException {
		byte[] relatorio;
		FormatoImpressao formatoImpressao = this.obterFormatoImpressao(tipoRelatorio);
		
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioIndicadorEmergencia");
		try {
			StringBuilder stringBuilder = new StringBuilder();
			validarDatasFormulario(periodoCriacao, stringBuilder);
			String camposComErro = stringBuilder.toString();
			if(camposComErro.length() > 0) {
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, camposComErro);
			}
			relatorio = controladorServicoAutorizacao.gerarRelatorioIndicadorEmergencia(formatoImpressao, exibirFiltros, periodoCriacao, descricaoTipoServico, redeLocal);
			if(relatorio != null) {

				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				String diaMesAno = df.format(Calendar.getInstance().getTime());
				SimpleDateFormat formatoHora = new SimpleDateFormat("HH'h'mm");
				String hora = formatoHora.format(Calendar.getInstance().getTime());

				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION,
						"attachment; filename=RelatórioIndicadorDeEmergência_" + diaMesAno + "_" + hora + obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();

			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			model.addObject("periodoCriacao",periodoCriacao);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, e);

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		return model;
	}
	
	@RequestMapping("gerarRelatorioIndiceVazamento")
	public ModelAndView gerarRelatorioIndiceVazamento(
			@RequestParam("periodoCriacao") String periodoCriacao,
			@RequestParam("descricaoTipoServico")String descricaoTipoServico,
			@RequestParam("exibirFiltros") String exibirFiltros,
			@RequestParam("tipoRelatorio") String tipoRelatorio,
			@RequestParam("redeLocal") Long redeLocal,
			HttpServletRequest request,
			HttpServletResponse response) throws GGASException {
		byte[] relatorio;
		FormatoImpressao formatoImpressao = this.obterFormatoImpressao(tipoRelatorio);
		
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioIndicadorEmergencia");
		try {
			StringBuilder stringBuilder = new StringBuilder();
			validarDatasFormulario(periodoCriacao, stringBuilder);
			String camposComErro = stringBuilder.toString();
			if(camposComErro.length() > 0) {
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, camposComErro);
			}
			relatorio = controladorServicoAutorizacao.gerarRelatorioIndiceVazamento(formatoImpressao, exibirFiltros, periodoCriacao, descricaoTipoServico, redeLocal);
			if(relatorio != null) {
				
				SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
				String diaMesAno = df.format(Calendar.getInstance().getTime());
				SimpleDateFormat formatoHora = new SimpleDateFormat("HH'h'mm");
				String hora = formatoHora.format(Calendar.getInstance().getTime());
				
				ServletOutputStream servletOutputStream = response.getOutputStream();
				response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
				response.setContentLength(relatorio.length);
				response.addHeader(CONTENT_DISPOSITION,
						"attachment; filename=RelatórioIndiceVazamento_" + diaMesAno + "_" + hora + obterFormatoRelatorio(formatoImpressao));
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
				
			}
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			model.addObject("periodoCriacao",periodoCriacao);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, e);
			
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			try {
				carregarCombos(model);
			} catch (GGASException e1) {
				LOG.error(e1.getMessage(), e1);
			}
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		return model;
	}
	
	private void validarDatasFormulario(String periodoCriacao, StringBuilder stringBuilder) {
		boolean periodoGeracaoIncompleto =
				StringUtils.isEmpty(periodoCriacao);
		
		if ( periodoGeracaoIncompleto) {
			stringBuilder.append("Período de Geração");

		}  else {
			if (((periodoCriacao != null && !periodoCriacao.isEmpty())) && 
					(!Util.isDataValida(periodoCriacao, Constantes.FORMATO_DATA_BR_MES_ANO))) {
				stringBuilder.append("Período de Geração");
			}
		}
	}
	
	
	/**
	 * Obter fornemato impressao.
	 *
	 * @param formato
	 *            the formato
	 * @return the formato impressao
	 */
	private FormatoImpressao obterFormatoImpressao(String formato) {

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		if(formato.equalsIgnoreCase(FormatoImpressao.XLS.toString())) {
			formatoImpressao = FormatoImpressao.XLS;
		} else {
			if(formato.equalsIgnoreCase(FormatoImpressao.RTF.toString())) {
				formatoImpressao = FormatoImpressao.RTF;
			}
		}
		return formatoImpressao;
	}
	
	
	private void removerDadosSessao(HttpServletRequest request) {
		request.getSession().removeAttribute(LISTA_IMOVEIS_CHAMADO);
		request.getSession().removeAttribute(LISTA_ANEXOS);
	}
	/**
	 * Gerar relatorio autorizacao servico.
	 * 
	 * @param servicoAutorizacaoVO
	 *            the servico autorizacao vo
	 * @param tipoRelatorio
	 *            the tipo relatorio
	 * @param response
	 *            the response
	 * @param agrupamento
	 *            the agrupamento
	 * @param exibirFiltros
	 *            the exibir filtros
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("gerarRelatorioAutorizacaoServico")
	public ModelAndView gerarRelatorioAutorizacaoServico(@ModelAttribute(SERVICO_AUTORIZACAO_VO) ServicoAutorizacaoVO servicoAutorizacaoVO,
					@RequestParam("tipoRelatorio") String tipoRelatorio, HttpServletResponse response,
					@RequestParam("agrupamento") String agrupamento,
					@RequestParam("exibirFiltros") String exibirFiltros)
					throws GGASException {

		Log.info("Inicio gerarRelatorioAutorizacaoServico");
		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioAutorizacaoServico");

		try {

			StringBuilder stringBuilder = new StringBuilder();

			if(((servicoAutorizacaoVO.getDataGeracaoInicio() != null && !servicoAutorizacaoVO.getDataGeracaoInicio().isEmpty())
							|| (servicoAutorizacaoVO.getDataGeracaoFim() != null && !servicoAutorizacaoVO.getDataGeracaoFim().isEmpty())) && 
							(!Util.isDataValida(servicoAutorizacaoVO.getDataGeracaoInicio(), Constantes.FORMATO_DATA_BR)|| 
							!Util.isDataValida(servicoAutorizacaoVO.getDataGeracaoFim(), Constantes.FORMATO_DATA_BR))) {
	
				stringBuilder.append(DATA_GERACAO);
			}

			if(((servicoAutorizacaoVO.getDataPrevisaoInicio() != null && !servicoAutorizacaoVO.getDataPrevisaoInicio().isEmpty())
							|| (servicoAutorizacaoVO.getDataPrevisaoFim() != null && !servicoAutorizacaoVO.getDataPrevisaoFim().isEmpty())) && 
							(!Util.isDataValida(servicoAutorizacaoVO.getDataPrevisaoInicio(), Constantes.FORMATO_DATA_BR)
											|| !Util.isDataValida(servicoAutorizacaoVO.getDataPrevisaoFim(), Constantes.FORMATO_DATA_BR))) {

				if(stringBuilder.length() > 0) {
					stringBuilder.append(", " + DATA_PREVISAO);
				} else {
					stringBuilder.append(DATA_PREVISAO);
				}
			}

			if(((servicoAutorizacaoVO.getDataEncerramentoInicio() != null && !servicoAutorizacaoVO.getDataEncerramentoInicio().isEmpty())
							|| (servicoAutorizacaoVO.getDataEncerramentoFim() != null && !servicoAutorizacaoVO.getDataEncerramentoFim().isEmpty())) && 
							(!Util.isDataValida(servicoAutorizacaoVO.getDataEncerramentoInicio(), Constantes.FORMATO_DATA_BR) || 
							!Util.isDataValida(servicoAutorizacaoVO.getDataEncerramentoFim(), Constantes.FORMATO_DATA_BR))) {
		
				if(stringBuilder.length() > 0) {
					stringBuilder.append(", " + DATA_ENCERRAMENTO);
				} else {
					stringBuilder.append(DATA_ENCERRAMENTO);
				}
			}

			String camposComErro = stringBuilder.toString();
			if(camposComErro.length() > 0) {
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, camposComErro);
			}

		} catch(NegocioException e) {
			model = new ModelAndView("exibirPesquisaRelatorioAutorizacaoServico");
			model.addObject(SERVICO_AUTORIZACAO_VO, servicoAutorizacaoVO);
			mensagemErroParametrizado(model, e);
			return model;
		}

		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());
			
			if(servicoAutorizacaoVO.getTipagemRelatorio().equals(RELATORIO_SERVICO_AUTORIZACAO_SINTETICO)) {
				relatorio = controladorRelatorioServicoAutorizacao.gerarRelatorioServicoAutorizacaoSintetico(servicoAutorizacaoVO, 
						formatoImpressao, agrupamento, exibirFiltros);
			}else if (servicoAutorizacaoVO.getTipagemRelatorio().equals(RELATORIO_SERVICO_AUTORIZACAO_ANALITICO)){
				relatorio = controladorRelatorioServicoAutorizacao.gerarRelatorioServicoAltorizacao(servicoAutorizacaoVO, 
						formatoImpressao, agrupamento, exibirFiltros);
			}else {
				relatorio = controladorRelatorioServicoAutorizacao.gerarRelatorioServicoAutorizacaoResumo(servicoAutorizacaoVO
						,formatoImpressao, agrupamento, exibirFiltros);
			}
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(CONTENT_DISPOSITION, "attachment; filename=RelatórioDasAutorizaçõesDeServiço_" + diaMesAno + "_" + hora
							+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage(), e));
		} catch (NegocioException e) {
			LOG.debug(e.getStackTrace(), e);
			try {
				this.carregarCombos(model);
				this.carregarCombosChamado(model);
			} catch (GGASException e1) {
				LOG.debug(e1.getStackTrace(), e1);
			}

			mensagemErroParametrizado(model, e);
		} catch(ParseException e) {
			mensagemErroParametrizado(model, new GGASException(e.getMessage()));
		}
		Log.info("Fim gerarRelatorioAutorizacaoServico");
		return model;

	}

	/**
	 * Remover dados sessao.
	 * 
	 * @param sessao
	 *            the sessao
	 */
	private void removerDadosSessao(HttpSession sessao) {

		sessao.removeAttribute(LISTA_AUTORIZACAO_EQUIPAMENTOS);
		sessao.removeAttribute(LISTA_AUTORIZACAO_MATERIAIS);
		sessao.removeAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);
		sessao.removeAttribute(LISTA_IMOVEIS_CHAMADO);
		sessao.removeAttribute(INDICADOR_ATUALIZACAO_CADASTRAL);
		sessao.removeAttribute(URL_RETORNO_ATUALIZACAO_CASTRAL);
		sessao.removeAttribute(LISTA_ANEXOS);
		sessao.removeAttribute("listaMedidor");

	}

	/**
	 * Alterar servico autorizacao em execucao.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param servicoAutorizacaoVO
	 *            the servico autorizacao vo
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param model
	 *            the model
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("alterarServicoAutorizacaoEmExecucao")
	public ModelAndView alterarServicoAutorizacaoEmExecucao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
					@ModelAttribute("ServicoAutorizacaoVO") ServicoAutorizacaoVO servicoAutorizacaoVO, BindingResult result,
					HttpServletRequest request, ModelAndView model) throws GGASException {

		Log.info("Inicio alterarServicoAutorizacaoEmExecucao");
		model.setViewName(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
		Map<String, Object> dados = new HashMap<>();
		popularMapaDados(dados, request);
		try {
			controladorServicoAutorizacao.alterarServicoAutorizacaoEmExecucao(chavesPrimarias, dados);
			mensagemSucesso(model, "SUCESSO_STATUS_EM_EXECUCAO", chavesPrimarias[0]);
		} catch(NegocioException e) {
			mensagemErroParametrizado(model, e);
		}
		Log.info("Fim alterarServicoAutorizacaoEmExecucao");
		return model;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model
	 *            the model
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void carregarCombos(ModelAndView model) throws GGASException {

		model.addObject("listaServicoTipoPrioridade", controladorServicoTipoPrioridade.listarServicoTipoPrioridade());
		model.addObject("listaTipoServico", controladorServicoTipo.listarServicosTipoAtivos());
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		model.addObject("listaMateriais", controladorMaterial.listarMaterial());
		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_EQUIPAMENTO);
		model.addObject("listaEquipamentos", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		model.addObject("listaMotivoEncerramento", controladorServicoAutorizacao.listarServicoAutorizacaoMotivoEncerramento());
		model.addObject("listaUnidadeFederacao", Fachada.getInstancia().listarUnidadeFederacao());
		model.addObject("listaExecutante", controladorFuncionario.listarFuncionarioUnidadeOrganizacional());
		model.addObject("listaUnidadeOrganizacional", controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject("listaSolicitantes", controladorServicoAutorizacao.listarUsuariosSolcitantesAutorizacaoServico());		
		model.addObject(LISTA_CHAMADO_TIPO, controladorChamadoTipo.listarTodosHabilitado());
		model.addObject(CONST_TAMANHO_MAXIMO_ARQUIVO, TAMANHO_MAXIMO_ARQUIVO);
		
		model.addObject(LISTA_TIPO_MEDIDOR, controladorMedidor.listarTipoMedidor());
		model.addObject(LISTA_MARCA_MEDIDOR, controladorMedidor.listarMarcaMedidor());
		model.addObject(LISTA_MODELO_MEDIDOR, controladorMedidor.listarModeloMedidor());
		model.addObject(LISTA_LOCAL_ARMAZENAGEM, controladorMedidor.listarLocalArmazenagem());
		model.addObject(MODO_USO_INDEPENDENTE, controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE));
		model.addObject(MODO_USO_NORMAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL));
		model.addObject(MODO_USO_VIRTUAL,
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_CILINDRO_AS);
		model.addObject("listaCilindroAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_SOLUCAO_AS);
		model.addObject("listaSolucaoAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_CAUSA_AS);
		model.addObject("listaCausaAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_LOCAL_AS);
		model.addObject("listaLocalAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_OCORRENCIA_AS);
		model.addObject("listaOcorrenciaAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_STATUS_AS);
		model.addObject("listaStatusAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_ANORM_AS);
		model.addObject("listaAnormalidadeAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TIPO_INST_AS);
		model.addObject("listaLocalInstAS", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("habilitado", true);
		model.addObject("listaFuncionario", controladorFuncionario.consultarFuncionarios(filtro));
		
		model.addObject("listaOperacaoMedidor", fachada.listarOperacaoMedidor());
		
		model.addObject("listaMotivoOperacaoMedidor", fachada.listarMotivoOperacaoMedidor());
		
		model.addObject("listaLocalInstalacao", fachada.listarMedidorLocalInstalacao());

		model.addObject("listaMedidorProtecao", fachada.listarMedidorProtecao());
		
	}

	/**
	 * Verificar tipo fluxo execucao.
	 * 
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	private ModelAndView verificarTipoFluxoExecucao(HttpServletRequest request) {

		String fluxoPesquisa = request.getParameter("fluxoPesquisa");
		String fluxoDetalhamento = request.getParameter(FLUXO_DETALHAMENTO);

		ModelAndView model = null;
		if(fluxoPesquisa != null) {
			model = new ModelAndView(FORWARD_PESQUISAR_SERVICO_AUTORIZACAO);
		} else if(fluxoDetalhamento != null) {
			model = new ModelAndView("forward:/exibirDetalhamentoServicoAutorizacao");
		}
		return model;
	}

	/**
	 * Popular mapa dados.
	 * 
	 * @param dados
	 *            the dados
	 * @param request
	 *            the request
	 */
	private void popularMapaDados(Map<String, Object> dados, HttpServletRequest request) {

		Usuario usuario = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		dados.put("usuario", usuario);

		String indicadorVazamentoConfirmado = request.getParameter("indicadorVazamentoConfirmado");
		dados.put("indicadorVazamentoConfirmado", Boolean.parseBoolean(indicadorVazamentoConfirmado));

		if (request.getParameter(DATA_PREVISAO_ENCERRAMENTO) != null) {
			String dataPrevisaoEncerramento = request.getParameter(DATA_PREVISAO_ENCERRAMENTO);
			dados.put(DATA_PREVISAO_ENCERRAMENTO, dataPrevisaoEncerramento);
		}
		if(request.getParameter(DATA_SOLICITACAO_RAMAL) != null && !"".equals(request.getParameter(DATA_SOLICITACAO_RAMAL))) {
			String dataInformada = request.getParameter(DATA_SOLICITACAO_RAMAL);
			dados.put(DATA_SOLICITACAO_RAMAL, dataInformada);
		}
		
		if(request.getParameter("isPontoCicloLeitura") != null) {
			dados.put("isPontoCicloLeitura", request.getParameter("isPontoCicloLeitura"));
		}
	}

	/**
	 * Carregar combos chamado.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarCombosChamado(ModelAndView model) throws NegocioException {

		model.addObject("listaChamadoTipo", controladorChamadoTipo.obterTodos());
		model.addObject("listaChamadoAssunto", controladorChamadoAssunto.obterTodos());
		ConstanteSistema constanateSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_STATUS_CHAMADO);
		model.addObject("listaStatusChamado",
						controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanateSistema.getValor())));
		model.addObject("listaTipoServico", controladorServicoTipo.listarServicoTipo());
		model.addObject("listaCanalAtendimento", controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addObject("listaUnidadeOrganizacional", controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject("listaUsuarioResponsavel", controladorFuncionario.listarFuncionarioUnidadeOrganizacional());
	}

	/**
	 * Carregar assuntos.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param chaveAssunto
	 *            the chave assunto
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("carregarAssuntosServico")
	public ModelAndView carregarAssuntos(@RequestParam("chavePrimaria") Long chavePrimaria,
					@RequestParam(value = "chaveAssunto", required = false) Long chaveAssunto) throws GGASException {

		ModelAndView model = new ModelAndView("divAssuntosServico");
		Collection<ChamadoAssunto> assuntos = controladorChamadoAssunto.consultarChamado(chavePrimaria);
		model.addObject("assuntos", assuntos);
		model.addObject("chaveAssunto", chaveAssunto);

		return model;
	}
	
	@RequestMapping("carregarChamadoAssuntoServico")
	public ModelAndView carregarChamadoAssuntoServico(@RequestParam("chaveChamadoTipo") Long chaveChamadoTipo) throws GGASException {
		
		ModelAndView model = new ModelAndView("divAssunto");
		if (chaveChamadoTipo != null && chaveChamadoTipo != 0) {
			model.addObject("listaChamadoAssunto", controladorChamadoAssunto.obterChamadoAssuntoAtivoPorChaveChamadoTipo(chaveChamadoTipo));
		}
		return model;
		
	}

	/**
	 * Exibir detalhamento servico autorizacao pesquisa satisfacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoServicoAutorizacaoPesquisaSatisfacao")
	public ModelAndView exibirDetalhamentoServicoAutorizacaoPesquisaSatisfacao(@RequestParam("chaveServicoAutorizacao") Long chavePrimaria,
					HttpSession sessao) throws GGASException {

		ModelAndView model = null;
		try {
			if(chavePrimaria == null) {
				Util.registroNaoEncontrato();
			}
			model = this.exibirDetalhamentoServicoAutorizacao(chavePrimaria, sessao);
			model.setViewName("exibirDetalhamentoServicoAutorizacaoPesquisaSatisfacao");
			model.addObject("fluxoPesquisaSatisfacao", Boolean.TRUE);
		} catch(NegocioException e) {
			model = new ModelAndView("forward:/exibirResponderQuestionario");
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Popular anexo chamado.
	 * 
	 * @param servicoAutorizacao the servico autorizacao
	 * @param request the request
	 */
	@SuppressWarnings("unchecked")
	private void popularAnexoServicoAutorizacao(ServicoAutorizacao servicoAutorizacao, HttpServletRequest request) {

		Collection<ServicoAutorizacaoHistoricoAnexo> listaAnexos =
				(Collection<ServicoAutorizacaoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);
		servicoAutorizacao.setAnexos(listaAnexos);
	}

	/**
	 * Imprimir arquivo servico autorizacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping("imprimirArquivoServicoAutorizacao")
	public void imprimirArquivoServicoAutorizacao(@RequestParam("chaveServicoAutorizacaoHistorico") Long chavePrimaria, @RequestParam("mobile") Boolean mobile,
			HttpServletRequest request, HttpServletResponse response) throws NegocioException, IOException {

		Log.info("Inicio imprimirArquivoServicoAutorizacao");
		
		byte[] arquivo = null;
		String nomeArquivo = "";
		
		if(mobile) {
			ServicoAutorizacaoTempAnexo servicoAutorizacaoHistoricoAnexoTemp =
					controladorServicoAutorizacao.obterServicoAutorizacaoTempAnexo(chavePrimaria);
			
			String caminho = servicoAutorizacaoHistoricoAnexoTemp.getCaminho();
			String[] nomeAnexo = caminho.split("/");
			
			arquivo = obterArquivoAnexoMobile(caminho);
			
			if(arquivo != null) {
				nomeArquivo = nomeAnexo[nomeAnexo.length - 1];
			} else {
				
				String caminhoImagemFixa = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_FALHA_REGISTRO_FOTO);
				File file = new File(caminhoImagemFixa);
				
				arquivo = FileUtils.readFileToByteArray(file);
			}
		} else {
			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo =
					controladorServicoAutorizacao.obterServicoAutorizacaoHistoricoAnexo(chavePrimaria);
			
			arquivo = servicoAutorizacaoHistoricoAnexo.getDocumentoAnexo();
			nomeArquivo = servicoAutorizacaoHistoricoAnexo.getDescricaoAnexo();
		}


		if (arquivo != null) {

			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentLength(arquivo.length);
			response.addHeader(CONTENT_DISPOSITION, "attachment; filename=" + nomeArquivo);
			servletOutputStream.write(arquivo, 0, arquivo.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		}
		
		Log.info("Fim imprimirArquivoServicoAutorizacao");
		return;
	}
	
	@RequestMapping("visualizarImagem")
    public ResponseEntity<byte[]> exibirBlob(@RequestParam("chaveServicoAutorizacaoHistorico") Long chavePrimaria, @RequestParam("mobile") Boolean mobile,
			HttpServletRequest request, HttpServletResponse response) throws NegocioException, IOException {
    	
		Log.info("Inicio visualizarImagem");
		
		byte[] arquivo = null;
		String nomeArquivo = "";
		if(mobile) {
			ServicoAutorizacaoTempAnexo servicoAutorizacaoHistoricoAnexoTemp = controladorServicoAutorizacao.obterServicoAutorizacaoTempAnexo(chavePrimaria);
			
			String caminho = servicoAutorizacaoHistoricoAnexoTemp.getCaminho();
			String[] nomeAnexo = caminho.split("/");
			
			arquivo = obterArquivoAnexoMobile(caminho);
			nomeArquivo = nomeAnexo[nomeAnexo.length - 1];
			

		} else {
			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo =
					controladorServicoAutorizacao.obterServicoAutorizacaoHistoricoAnexo(chavePrimaria);
			
			arquivo = servicoAutorizacaoHistoricoAnexo.getDocumentoAnexo();
			nomeArquivo = servicoAutorizacaoHistoricoAnexo.getNomeArquivo();
		}
		
        if (arquivo != null) {
            HttpHeaders headers = new HttpHeaders();
            String extensao = obterExtensaoArquivo(nomeArquivo);
            headers.set("Content-disposition","inline; filename="+nomeArquivo);
            if (extensao != null) {
                switch (extensao) {
                    case "jpg":
                    case "jpeg":
                        headers.setContentType(MediaType.IMAGE_JPEG);
                        break;
                    case "png":
                        headers.setContentType(MediaType.IMAGE_PNG);
                        break;
                    case "gif":
                        headers.setContentType(MediaType.IMAGE_GIF);
                        break;
                    case "pdf":
                        headers.setContentType(MediaType.parseMediaType("application/pdf"));
                        break;
                    default:
                        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
                        break;
                }
            }
            Log.info("Fim visualizarImagem");
            return new ResponseEntity<>(arquivo, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
        }
        
        
    }

    private String obterExtensaoArquivo(String nomeArquivo) {
        int index = nomeArquivo.lastIndexOf(".");
        if (index > 0) {
            return nomeArquivo.substring(index + 1);
        }
        return null; 
    }
    
	private byte[] obterArquivoAnexoMobile(String caminho) throws IOException {
		
		byte[] retorno = null;
		File arquivo = Util.getFile(caminho);
		
		if(arquivo != null && arquivo.exists()) {
			retorno = Util.converterFileParaByte(arquivo);
			
		}
		return retorno;
	}

	/**
	 * Método responsável por limpar campo relacionado ao Arquivo anexo.
	 * 
	 * @param request  {@link HttpServletRequest}
	 * @return model divCampoArquivo  {@link Model}
	 */
	@RequestMapping("limparCampoArquivoServicoAutorizacao")
	public ModelAndView limparCampoArquivo(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("divCampoArquivo");

		model.addObject("arquivoAnexo", null);

		return model;
	}

	/**
	 * Adiciona anexo ao chamado historico
	 * 
	 * @param request {@link HttpServletRequest}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarAnexoServicoAutorizacao")
	public ModelAndView adicionarAnexo(HttpServletRequest request) {

		ModelAndView model = new ModelAndView(GRID_ANEXOS);
		ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoAnexo = new ServicoAutorizacaoHistoricoAnexo();
		List<ServicoAutorizacaoHistoricoAnexo> listaAnexos =
				(List<ServicoAutorizacaoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		try {
			popularAnexo(servicoAutorizacaoAnexo, request);

			controladorServicoAutorizacao.validarDadosAnexo(servicoAutorizacaoAnexo);

			if (indexLista == null || indexLista < 0) {

				servicoAutorizacaoAnexo.setDadosAuditoria(getDadosAuditoria(request));

				if (listaAnexos == null || listaAnexos.isEmpty()) {
					listaAnexos = new ArrayList<>();
				}

				controladorServicoAutorizacao.verificarDescricaoAnexo(indexLista, listaAnexos, servicoAutorizacaoAnexo);
				listaAnexos.add(servicoAutorizacaoAnexo);
			} else {

				this.alterarAnexo(servicoAutorizacaoAnexo, indexLista, listaAnexos, request);
			}

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		}

		model.addObject(LISTA_ANEXOS, listaAnexos);

		request.getSession().setAttribute(LISTA_ANEXOS, listaAnexos);

		return model;
	}

	/**
	 * Popula anexo do servicoAutorizacao histórico
	 * 
	 * @param ServicoAutorizacaoHistoricoAnexo
	 * @param request
	 * @throws NegocioException
	 */
	private void popularAnexo(ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo, HttpServletRequest request)
			throws NegocioException {

		MultipartHttpServletRequest multiPartRequest = (MultipartHttpServletRequest) request;
		MultipartFile arquivo = multiPartRequest.getFile("arquivoAnexo");

		String descricaoAnexo = Util.utf8ToISO(request.getParameter("descricaoAnexo"));
		String chavePrimariaAnexo = request.getParameter("chavePrimariaAnexo");

		if (!StringUtils.isEmpty(descricaoAnexo)) {
			servicoAutorizacaoHistoricoAnexo.setDescricaoAnexo(descricaoAnexo);
		}

		boolean condicao = false;
		if (arquivo != null) {
			condicao = Util.validarImagemDoArquivo(arquivo, extensao, TAMANHO_MAXIMO_ARQUIVO);
		}

		if (condicao && Util.isAllNotNull(arquivo) && !arquivo.isEmpty()) {
			try {
				servicoAutorizacaoHistoricoAnexo.setDocumentoAnexo(arquivo.getBytes());
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(e);
			}
			servicoAutorizacaoHistoricoAnexo.setNomeArquivo(arquivo.getOriginalFilename());
		}
		
		if (StringUtils.isNotBlank(chavePrimariaAnexo)) {
			servicoAutorizacaoHistoricoAnexo.setChavePrimaria(Long.parseLong(chavePrimariaAnexo));
		}

		servicoAutorizacaoHistoricoAnexo.setUltimaAlteracao(Calendar.getInstance().getTime());

		request.getSession().setAttribute("servicoAutorizacaoHistoricoAnexo", servicoAutorizacaoHistoricoAnexo);

	}

	/**
	 * Altera anexo da lista do servicoAutorizacao historico
	 * 
	 * @param indexListaAnexo
	 * @param listaAnexo
	 * @param request
	 * @throws GGASException
	 */
	private void alterarAnexo(ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexoAlterado, Integer indexListaAnexo,
			List<ServicoAutorizacaoHistoricoAnexo> listaAnexo, HttpServletRequest request) throws GGASException {

		if (listaAnexo != null && indexListaAnexo != null && listaAnexo.size() > indexListaAnexo) {

			controladorServicoAutorizacao.verificarDescricaoAnexo(indexListaAnexo, listaAnexo, servicoAutorizacaoHistoricoAnexoAlterado);
			ServicoAutorizacaoHistoricoAnexo servico = listaAnexo.get(indexListaAnexo);
			popularAnexo(servico, request);
			servico.setDadosAuditoria(getDadosAuditoria(request));

		}
	}


	/**
	 * Visualizar anexo do servicoAutorizacao historico
	 *
	 * @param indexLista  {@link Integer}
	 * @param request {@link HttpServletRequest}
	 * @param response  {@link HttpServletResponse}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("visualizarAnexoServicoAutorizacao")
	public void visualizarAnexo(@RequestParam(INDEX_LISTA) Integer indexLista, HttpServletRequest request, HttpServletResponse response) {

		Collection<ServicoAutorizacaoHistoricoAnexo> listaAnexos =
				(Collection<ServicoAutorizacaoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);



		for (int i = 0; listaAnexos != null && i < listaAnexos.size(); i++) {

			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo =
					((ArrayList<ServicoAutorizacaoHistoricoAnexo>) listaAnexos).get(i);

			if (i == indexLista && servicoAutorizacaoHistoricoAnexo != null) {

				byte[] anexo = servicoAutorizacaoHistoricoAnexo.getDocumentoAnexo();

				ServletOutputStream servletOutputStream;
				try {
					servletOutputStream = response.getOutputStream();
					String nomeArquivo = servicoAutorizacaoHistoricoAnexo.getNomeArquivo();
					response.setContentLength(anexo.length);
					response.addHeader(CONTENT_DISPOSITION, "attachment; filename=" + nomeArquivo);
					servletOutputStream.write(anexo, 0, anexo.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}

		}
	}

	/**
	 * Remover anexo aba identificacao.
	 * 
	 * @param request {@link HttpServletRequest}
	 * @param response {@link HttpServletResponse}
	 * @return gridAnexos carregada 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerAnexoServicoAutorizacao")
	public ModelAndView removerAnexo(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView model = new ModelAndView(GRID_ANEXOS);

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		Collection<ServicoAutorizacaoHistoricoAnexo> listaAnexos =
				(Collection<ServicoAutorizacaoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);

		if (listaAnexos != null && indexLista != null) {
			ServicoAutorizacaoHistoricoAnexo servicoAutorizacaoHistoricoAnexo =
					((ArrayList<ServicoAutorizacaoHistoricoAnexo>) listaAnexos).get(indexLista.intValue());
			listaAnexos.remove(servicoAutorizacaoHistoricoAnexo);
		}

		request.getSession().setAttribute(LISTA_ANEXOS, listaAnexos);

		model.addObject(LISTA_ANEXOS, listaAnexos);

		return model;
	}

	/**
	 * Exibe tela para resposta do formulário
	 *
	 * @param chavePrimariaAS {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @throws GGASException {@link GGASException}
	 * @return modelAndView - {@link ModelAndView}
	 */
	@RequestMapping("exibirResponderFormulario")
	public ModelAndView exibirResponderFormulario(@RequestParam("chavePrimariaAS") Long chavePrimariaAS, HttpServletRequest request)
			throws GGASException {

		ModelAndView model = new ModelAndView("popupResponderFormulario");

		ServicoAutorizacao servicoAutorizacao = controladorServicoAutorizacao.obterServicoAutorizacao(chavePrimariaAS);

		Questionario questionario = controladorQuestionario.consultarPorCodico(
				servicoAutorizacao.getServicoTipo().getFormulario().getChavePrimaria(), LISTA_PERGUNTAS, "tipoServico", "segmento");

		validarQuestionarioSessao(request, servicoAutorizacao.getChavePrimaria());

		List<QuestionarioPergunta> listaPerguntas = new ArrayList<>(questionario.getListaPerguntas());
		Collections.sort(listaPerguntas);

		carregarAlternativas(listaPerguntas);

		model.addObject(LISTA_PERGUNTAS, listaPerguntas);
		model.addObject(SERVICO_AUTORIZACAO, servicoAutorizacao);
		model.addObject(QUESTIONARIO, questionario);

		return model;
	}

	/**
	 * Remove a lista de perguntas da sessao caso seja de um serviço diferente
	 * 
	 * @param request
	 * @param servicoAutorizacaoId
	 */
	@SuppressWarnings("unchecked")
	private void validarQuestionarioSessao(HttpServletRequest request, long servicoAutorizacaoId) {
		Object perguntasSessao = request.getSession().getAttribute(LISTA_PERGUNTAS);
		
		if (perguntasSessao != null) {
			List<QuestionarioPergunta> perguntas = (List<QuestionarioPergunta>) perguntasSessao;
			if (perguntas.get(0).getServicoAutorizacao().getChavePrimaria() != servicoAutorizacaoId) {
				request.getSession().removeAttribute(LISTA_PERGUNTAS);
			}
		}
	}

	/**
	 * Responder formulario.
	 * 
	 * @param servicoAutorizacao the servicoAutorizacao
	 * @param sessao the sessao
	 * @param request the request
	 * @return the model and view
	 */
	@RequestMapping(value = "responderFormulario", produces = "application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String> responderQuestionario(@ModelAttribute(SERVICO_AUTORIZACAO) ServicoAutorizacao servicoAutorizacao,
			HttpSession sessao, HttpServletRequest request) {

		Map<String, Object> dados = new HashMap<>();
		List<QuestionarioPergunta> listaPerguntas = new ArrayList<>();
		ResponseEntity<String> resposta = new ResponseEntity<>(HttpStatus.OK);
		try {
			int i = 0;

			Questionario questionario = controladorQuestionario
					.consultarPorCodico(servicoAutorizacao.getServicoTipo().getFormulario().getChavePrimaria(), LISTA_PERGUNTAS);

			listaPerguntas.addAll(questionario.getListaPerguntas());
			Collections.sort(listaPerguntas);
			StringBuilder stringBuilder = new StringBuilder();
			for (QuestionarioPergunta pergunta : listaPerguntas) {
				pergunta.setServicoAutorizacao(servicoAutorizacao);
				if (pergunta.getObjetiva()) {
					validarPerguntaObjetiva(request, i, stringBuilder, pergunta);
				} else {
					validarPerguntaAberta(request, i, stringBuilder, pergunta);
				}
				i++;
			}
			String camposObrigatorios = stringBuilder.toString();

			if (camposObrigatorios.length() > 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_RESPOSTAS_OBRIGATORIOS, camposObrigatorios);
			}
			popularMapaDados(dados, request);
			carregarAlternativas(listaPerguntas);
			request.getSession().setAttribute(LISTA_PERGUNTAS, listaPerguntas);

		} catch (NegocioException e) {
			LOG.debug("Falha ao responder o formulario", e);
			resposta = new ResponseEntity<>(obterMensagem(e.getChaveErro(), e.getParametrosErro()), HttpStatus.BAD_REQUEST);
		}

		return resposta;
	}

	/**
	 * Exibir página responder questionario.
	 * 
	 * @param chavePrimariaASHistorico  {@link Long}
	 * @return model  {@link ModelAndView}
	 * @throws NegocioException {@link NegocioException}
	 */
	@RequestMapping("exibirRespostaFormulario")
	public ModelAndView exibirRespostaQuestionario(@RequestParam("chavePrimariaASHistorico") Long chavePrimariaASHistorico)
			throws NegocioException {
		ModelAndView model = new ModelAndView("popupRespostaFormulario");

		ServicoAutorizacaoHistorico servicoAutorizacaoHistorico =
				controladorServicoAutorizacao.obterServicoAutorizacaoHistorico(chavePrimariaASHistorico);

		Questionario questionario = servicoAutorizacaoHistorico.getServicoAutorizacao().getServicoTipo().getFormulario();

		Collection<QuestionarioPerguntaResposta> respostas = controladorQuestionario
				.obterRespostasFormulario(questionario.getChavePrimaria(), servicoAutorizacaoHistorico.getChavePrimaria());

		model.addObject(QUESTIONARIO, questionario);
		model.addObject("respostas", respostas);

		return model;
	}

	/**
	 * Ordena e carrega as alternativas da lista de pergunta
	 * 
	 * @param listaPerguntas
	 */
	private void carregarAlternativas(List<QuestionarioPergunta> listaPerguntas) {
		Collections.sort(listaPerguntas);

		for (QuestionarioPergunta pergunta : listaPerguntas) {
			pergunta.setListaAlternativas(controladorQuestionario.obterAlternativas(pergunta.getChavePrimaria()));
		}
	}

	/**
	 * Validar pergunta Objetiva
	 * 
	 * @param request
	 * @param i
	 * @param stringBuilder
	 * @param pergunta
	 */
	private void validarPerguntaObjetiva(HttpServletRequest request, int i, StringBuilder stringBuilder, QuestionarioPergunta pergunta) {
		if (pergunta.getValoresPersonalizados()) {
			String[] alternativaId = request.getParameterValues("alternativa" + i);

			if (alternativaId != null && alternativaId.length > 0) {

				Collection<QuestionarioAlternativa> alternativasSelecionadas =
						controladorQuestionario.obterAlternativas(Util.arrayStringParaArrayLong(alternativaId));
				pergunta.setListaAlternativasSelecionadas(alternativasSelecionadas);
			} else if (pergunta.getRespostaObrigatoria() != null && pergunta.getRespostaObrigatoria()) {
				stringBuilder.append(i + 1);
				stringBuilder.append(",");
			}

		} else {
			String numero = request.getParameter("resposta" + i);
			if (numero != null) {
				pergunta.setNotaPergunta(Integer.parseInt(numero));
			} else if (pergunta.getRespostaObrigatoria() != null && pergunta.getRespostaObrigatoria()) {
				stringBuilder.append(i + 1);
				stringBuilder.append(",");
			}
		}
	}
	
	/**
	 * Validar pergunta aberta
	 * 
	 * @param request
	 * @param i
	 * @param stringBuilder
	 * @param pergunta
	 */
	private void validarPerguntaAberta(HttpServletRequest request, int i, StringBuilder stringBuilder, QuestionarioPergunta pergunta) {
		pergunta.setDescricaoResposta(request.getParameter("respostaDiscursiva" + i));
		if (pergunta.getRespostaObrigatoria() != null && pergunta.getRespostaObrigatoria()
				&& StringUtils.isEmpty(pergunta.getDescricaoResposta())) {
			stringBuilder.append(i + 1);
			stringBuilder.append(",");
		}
	}

	/**
	 * Carregar ListaAnexos.
	 * 
	 * @param model the model
	 * @throws GGASException
	 */
	@SuppressWarnings("unchecked")
	private void carregarListaAnexos(ModelAndView model, HttpServletRequest request) {
		if (request.getSession().getAttribute(LISTA_ANEXOS) != null) {
			Collection<ServicoAutorizacaoHistoricoAnexo> servicoAutorizacaoHistoricoAnexo =
					(Collection<ServicoAutorizacaoHistoricoAnexo>) request.getSession().getAttribute(LISTA_ANEXOS);
			model.addObject(LISTA_ANEXOS, servicoAutorizacaoHistoricoAnexo);
		}

	}
	
	
	/**
	 * 
	 * @param numeroSerie
	 * @param marcaMedidor
	 * @param tipoMedidor
	 * @param modelo
	 * @param modoUso
	 * @param session
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("consultarMedidorDisponivel")
	public ModelAndView consultarMedidorDisponivel(@RequestParam(value = "numeroSerie", required = false) String numeroSerie,
			@RequestParam(value = "marcaMedidor", required = false) Long marcaMedidor,
			@RequestParam(value = "tipoMedidor", required = false) Long tipoMedidor,
			@RequestParam(value = "modelo", required = false) Long modelo,
			@RequestParam(value = "modoUso", required = false) Long modoUso,
			HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridMedidorBootstrap");
		
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltroMedidor(filtro, numeroSerie, marcaMedidor, tipoMedidor, modelo, modoUso);
		Collection<Medidor> listaMedidor = controladorMedidor.consultarMedidorDisponivel(filtro);
		
		
		session.setAttribute("listaMedidor", listaMedidor);
		return model;
	}

	private void prepararFiltroMedidor(Map<String, Object> filtro, String numeroSerie, Long marcaMedidor,
			Long tipoMedidor, Long modelo, Long modoUso) {
		if (numeroSerie != null && !"".equals(numeroSerie)) {
			filtro.put("numeroSerieParcial", numeroSerie);
		}
		if (marcaMedidor != null && marcaMedidor > 0) {
			filtro.put("idMarca", marcaMedidor);
		}
		
		if (tipoMedidor != null && tipoMedidor > 0) {
			filtro.put("idTipo", tipoMedidor);
		}
		
		if (modelo != null && modelo > 0) {
			filtro.put("idModelo", modelo);
		}
		
		if (modoUso != null) {
			filtro.put("modoUso", modoUso);
		}
		
		filtro.put("habilitado", Boolean.TRUE);
		
	}
	
	
	
	@RequestMapping("exibirPesquisaAutorizacaoServicoLote")
	public ModelAndView exibirPesquisaAutorizacaoServicoLote(HttpSession session, HttpServletResponse response) throws GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaAutorizacaoServicoLote");
		
		model.addObject(LISTA_CITY_GATE, fachada.listarCityGate());
		model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicosTipoAtivos());
		model.addObject(LISTA_SITUACAO_MEDIDOR, fachada.listarSituacaoMedidor());
		model.addObject(LISTA_SITUACAO_PONTO_CONSUMO, fachada.listarSituacoesPontoConsumo());
		model.addObject(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addObject(LISTA_MODELO_MEDIDOR, fachada.listarModeloMedidor());
		model.addObject(LISTA_RAMO_ATIVIDADE, fachada.listarRamoAtividade());

		
		session.removeAttribute("listaPontoConsumoInserido");

		
		return model;
	}
	
	@RequestMapping("pesquisarAutorizacaoServicoLote")
	public ModelAndView pesquisarAutorizacaoServicoLote(@ModelAttribute("FiltroServicoAutorizacaoLoteDTO") FiltroServicoAutorizacaoLoteDTO filtroServicoAutorizacaoLoteDTO, BindingResult result,
			HttpServletRequest request) throws HibernateException, GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaAutorizacaoServicoLote");
		Collection<PontoConsumo> colecaoPontoConsumo = controladorServicoAutorizacao.consultarPontosLoteAutorizacaoServico(filtroServicoAutorizacaoLoteDTO);
		request.getSession().removeAttribute("listaPontoConsumoInserido");
		
		model.addObject(LISTA_CITY_GATE, fachada.listarCityGate());
		model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
		model.addObject(LISTA_PONTO_CONSUMO, colecaoPontoConsumo);
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicosTipoAtivos());
		model.addObject(LISTA_SITUACAO_MEDIDOR, fachada.listarSituacaoMedidor());
		model.addObject(LISTA_SITUACAO_PONTO_CONSUMO, fachada.listarSituacoesPontoConsumo());
		model.addObject(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addObject(LISTA_MODELO_MEDIDOR, fachada.listarModeloMedidor());
		model.addObject(LISTA_RAMO_ATIVIDADE, fachada.listarRamoAtividade());

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SESSAO, colecaoPontoConsumo);
		model.addObject(AUTORIZACAO_SERVICO_LOTE, filtroServicoAutorizacaoLoteDTO);
		
		return model;
	}
	
	@RequestMapping("exibirGerarAutorizacaoServicoLote")
	public ModelAndView exibirGerarAutorizacaoServicoLote(@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias, @RequestParam("servicoTipo") ServicoTipo servicoTipo, HttpServletRequest request) throws NegocioException {
		ModelAndView model = new ModelAndView("exibirGerarAutorizacaoServicoLote");
		String tipoGeracaoLote = request.getParameter("tipoGeracaoLote");
		
		ConstanteSistema servicoTipoProtocolo = controladorConstanteSistema.obterConstantePorCodigo(Constantes.SERVICO_TIPO_PROTOCOLO);
		FiltroGeracaoServicoAutorizacaoLoteDTO filtroGeracaoServicoAutorizacaoLoteDTO = new FiltroGeracaoServicoAutorizacaoLoteDTO();
		filtroGeracaoServicoAutorizacaoLoteDTO.setServicoTipo(servicoTipo);
		
		Collection<PontoConsumo> colecaoPontoConsumo = obterColecaoPontoConsumo(request, tipoGeracaoLote);
		List<Long> chavesPontoConsumo = obterChavesPontoConsumo(chavesPrimarias, request, tipoGeracaoLote, colecaoPontoConsumo);
		Collection<PontoConsumo> colecaoPontoConsumoSelecionado = colecaoPontoConsumo.stream().filter(p -> chavesPontoConsumo.contains(p.getChavePrimaria())).collect(Collectors.toList());
		
		request.getSession().setAttribute("listaPontosSelecionados", colecaoPontoConsumoSelecionado);
		
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicosTipoAtivos());
		model.addObject("filtroGeracaoServicoAutorizacaoLoteDTO", filtroGeracaoServicoAutorizacaoLoteDTO);
		model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		
		if(servicoTipo != null && servicoTipo.getChavePrimaria() != servicoTipoProtocolo.getValorLong()) {
			request.getSession().removeAttribute("loteComunicacaoASProtocolo");
		}

		return model;
	}
	
	private Collection<PontoConsumo> obterColecaoPontoConsumo(HttpServletRequest request, String tipoGeracaoLote) {
		if("tab-automatica".equals(tipoGeracaoLote)) {
			return (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SESSAO);
		} else {
			return (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontoConsumoInserido");
		}
	}
	
	private List<Long> obterChavesPontoConsumo(Long[] chavesPrimarias, HttpServletRequest request,
			String tipoGeracaoLote, Collection<PontoConsumo> colecaoPontoConsumo) {
		List<Long> chavesPontoConsumoTipoGeracaoLote;
		if("tab-automatica".equals(tipoGeracaoLote)) {
			chavesPontoConsumoTipoGeracaoLote = Arrays.asList(chavesPrimarias);
		} else {
			chavesPontoConsumoTipoGeracaoLote = Arrays.asList(Util.collectionParaArrayChavesPrimarias(colecaoPontoConsumo));
		}
		return chavesPontoConsumoTipoGeracaoLote;
	}
	
	@RequestMapping("gerarAutorizacaoServicoLote")
	public ModelAndView gerarAutorizacaoServicoLote(@ModelAttribute("FiltroGeracaoServicoAutorizacaoLoteDTO") FiltroGeracaoServicoAutorizacaoLoteDTO filtroGeracaoServicoAutorizacaoLoteDTO, HttpServletRequest request, HttpServletResponse response) throws GGASException {
		
		ModelAndView model = new ModelAndView("exibirPesquisaAutorizacaoServicoLote");
		DadosAuditoria dados = this.getDadosAuditoria(request);
		
		Collection<AutorizacaoServicoLoteVO> listaAutorizacaoServicoLote = (Collection<AutorizacaoServicoLoteVO>) request.getSession().getAttribute("listaPontoConsumoRoteirizado");
		
		
		try {
			
			LoteComunicacao lote = null;
			if (request.getSession().getAttribute("loteComunicacaoASProtocolo") != null) {
				lote = (LoteComunicacao) request.getSession().getAttribute("loteComunicacaoASProtocolo");
			}
			
			List<byte[]> relatorios = controladorServicoAutorizacao.gerarLote(listaAutorizacaoServicoLote, filtroGeracaoServicoAutorizacaoLoteDTO.getServicoTipo(), dados.getUsuario(), lote);
			mensagemSucesso(model, MSG_SUCESSO_LOTE_INCLUIDO);
			model.addObject(LISTA_CITY_GATE, fachada.listarCityGate());
			model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
			model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicosTipoAtivos());
			model.addObject(LISTA_SITUACAO_MEDIDOR, fachada.listarSituacaoMedidor());
			
			request.getSession().removeAttribute("listaPontoConsumoInserido");
			request.getSession().removeAttribute("listaPontoConsumoRoteirizado");
			request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_SESSAO);
			request.getSession().removeAttribute("listaPontosSelecionados");
			request.getSession().setAttribute("relatorioAutorizacaoServicoLote", relatorios);
			
			if (lote != null) {
				
				model = new ModelAndView("exibirPesquisaAcompanhamentoComunicacao");
				
				Collection<LoteComunicacao> colecaoLoteComunicacao = new ArrayList<>();
				colecaoLoteComunicacao.add(lote);
				
				model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
				model.addObject("listaLoteAcompanhamento", colecaoLoteComunicacao);
				mensagemSucesso(model, MSG_SUCESSO_LOTE_INCLUIDO);
				
				request.getSession().removeAttribute("loteComunicacaoASProtocolo");
				
			}

			
		} catch(NegocioException e) {
			model = new ModelAndView("exibirGerarAutorizacaoServicoLote");
			
			model.addObject("listaPontoConsumo", listaAutorizacaoServicoLote);
			model.addObject("filtroGeracaoServicoAutorizacaoLoteDTO", filtroGeracaoServicoAutorizacaoLoteDTO);
			model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicosTipoAtivos());
			model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
			
			String mensagem = "Não foi possível gerar as Autorizações de Serviço";

			mensagemErro(model, mensagem);
		}
		
		
		return model;
	}
	
	@RequestMapping("roteirizarServicoAutorizacaoLoteLote")
	public ModelAndView roteirizarServicoAutorizacaoLoteLote(@ModelAttribute("FiltroGeracaoServicoAutorizacaoLoteDTO") FiltroGeracaoServicoAutorizacaoLoteDTO filtroGeracaoServicoAutorizacaoLoteDTO, @RequestParam(value="diasSemana", required = false) Integer[] diasSemana ,HttpServletRequest request, HttpServletResponse response) throws GGASException, IOException {
		ModelAndView model = new ModelAndView("exibirGerarAutorizacaoServicoLote");
		
		model.addObject("filtroGeracaoServicoAutorizacaoLoteDTO", filtroGeracaoServicoAutorizacaoLoteDTO);
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicosTipoAtivos());
		model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		
		EntidadeConteudo turnos[] = filtroGeracaoServicoAutorizacaoLoteDTO.getTurno();
		
        List<Long> turnosSelecionados = new ArrayList<Long>();

		if (turnos != null && turnos.length > 0) {
			for (EntidadeConteudo turno : turnos) {
				turnosSelecionados.add(turno.getChavePrimaria());
			}
		}
        
        model.addObject("turnosSelecionados", turnosSelecionados);

		
		Map<String, Object> erros = filtroGeracaoServicoAutorizacaoLoteDTO.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			ServicoTipo servicoTipo = filtroGeracaoServicoAutorizacaoLoteDTO.getServicoTipo();
			
			
			if(servicoTipo.getListaAgendamentosTurno() == null || servicoTipo.getListaAgendamentosTurno().isEmpty() || !consultarExistenciaTurno(filtroGeracaoServicoAutorizacaoLoteDTO.getTurno(), servicoTipo.getListaAgendamentosTurno())) {
				mensagemErro(model, new ArrayList<>(Arrays.asList("Não existe equipe/turno Cadastrada para esse Serviço!")));
			} else {
				Collection<PontoConsumo> colecaoPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontosSelecionados");
				
				Collection<AutorizacaoServicoLoteVO> listaPontoConsumoRoteirizado = controladorServicoAutorizacao.roterizacaoAutorizacaoServico(colecaoPontoConsumo);

				Collection<AutorizacaoServicoLoteVO> listaPontoConsumoRoteirizadoPopulado = new ArrayList<AutorizacaoServicoLoteVO>();
				
				model.addObject("diasSemana", Arrays.asList(diasSemana));

				try {
					listaPontoConsumoRoteirizadoPopulado = popularAutorizacaoServicoLoteVO(listaPontoConsumoRoteirizado,
							filtroGeracaoServicoAutorizacaoLoteDTO, diasSemana);
				} catch (NegocioException e) {
					mensagemErro(model, e.getMessage());
				}
				
				
				model.addObject("listaPontoConsumo", listaPontoConsumoRoteirizadoPopulado);
				
				request.getSession().setAttribute("listaPontoConsumoRoteirizado", listaPontoConsumoRoteirizadoPopulado);
				
			}
			
		}
		
		return model;
	}
	
	private Collection<AutorizacaoServicoLoteVO> popularAutorizacaoServicoLoteVO(
			Collection<AutorizacaoServicoLoteVO> listaPontoConsumoRoteirizado,
			FiltroGeracaoServicoAutorizacaoLoteDTO filtroGeracaoServicoAutorizacaoLoteDTO, Integer[] diasSemana) throws GGASException, IOException {
		Boolean isMudouTurno = Boolean.TRUE;
		Integer sequencial = 1;
		LocalTime fimTurnoManha = LocalTime.of(12, 0);
		LocalTime fimTurnoTarde = LocalTime.of(17, 0);
		LocalTime fimTurnoNoite = LocalTime.of(23, 59);
		
		Integer quantidadePontosRoteirizados = listaPontoConsumoRoteirizado.size();
		Boolean isMetadadeTempoExecResid = filtroGeracaoServicoAutorizacaoLoteDTO.getServicoTipo()
				.getIndicadorMetadeTempoExecucao();
		List<Integer> listaDiasSemana = Arrays.asList(diasSemana);
		Boolean isMudouDia = Boolean.TRUE;
		Integer tentativas = 0;

		Calendar c = Calendar.getInstance();
		Calendar dataAtual = Calendar.getInstance();
		c.setTime(Util.converterCampoStringParaData("Data Execução Inicial",
				filtroGeracaoServicoAutorizacaoLoteDTO.getDataServico(), Constantes.FORMATO_DATA_BR));
		String anoCorrente = "." + c.get(Calendar.YEAR);

		Collection<AutorizacaoServicoLoteVO> listaRetorno = new ArrayList<AutorizacaoServicoLoteVO>();

		do {

			if (!listaDiasSemana.contains(c.get(Calendar.DAY_OF_WEEK) - 1)) {
				do {
					c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) + 1);
				} while (!listaDiasSemana.contains(c.get(Calendar.DAY_OF_WEEK) - 1));
			}

			dataAtual.setTime(c.getTime());
			Map<String, List<ServicoTipoAgendamentoTurno>> mapa = controladorComunicacao
					.montarMapaServicoTipoAgendamento(filtroGeracaoServicoAutorizacaoLoteDTO.getServicoTipo(), filtroGeracaoServicoAutorizacaoLoteDTO.getTurno());

			for (Entry<String, List<ServicoTipoAgendamentoTurno>> entry : mapa.entrySet()) {

				List<ServicoTipoAgendamentoTurno> listaRemover = new ArrayList<ServicoTipoAgendamentoTurno>();

				if (entry.getKey().equals("TARDE") && !isMudouDia) {
					c.set(Calendar.HOUR_OF_DAY, 13);
					c.set(Calendar.MINUTE, 0);
				} else if (entry.getKey().equals("NOITE") && !isMudouDia) {
					c.set(Calendar.HOUR_OF_DAY, 18);
					c.set(Calendar.MINUTE, 0);
				} else {
					String[] hora = filtroGeracaoServicoAutorizacaoLoteDTO.getHoraServico().split(":");
					c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hora[0]));
					c.set(Calendar.MINUTE, Integer.valueOf(hora[1]));
					c.set(Calendar.DAY_OF_YEAR, dataAtual.get(Calendar.DAY_OF_YEAR));
				}

				do {
					List<ServicoTipoAgendamentoTurno> listaTurno = entry.getValue();

					for (ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno : listaTurno) {

						if (quantidadePontosRoteirizados == 0) {
							break;
						}

						AutorizacaoServicoLoteVO autorizcaoServicoLoteVO = listaPontoConsumoRoteirizado.iterator().next();

						if (servicoTipoAgendamentoTurno.getQuantidadeAgendamentosTurno() == 0) {
							listaRemover.add(servicoTipoAgendamentoTurno);
							break;
						}
					
						servicoTipoAgendamentoTurno.setQuantidadeAgendamentosTurno(
								servicoTipoAgendamentoTurno.getQuantidadeAgendamentosTurno() - 1);


						Long tempoExecucao = filtroGeracaoServicoAutorizacaoLoteDTO.getTempoExecucao();
						PontoConsumo ponto = autorizcaoServicoLoteVO.getPontoConsumo();

						if (isMetadadeTempoExecResid && "RESIDENCIAL".equals(ponto.getSegmento().getDescricao())
								&& (ponto.getImovel().isIndividual() != null && ponto.getImovel().isIndividual())) {
							tempoExecucao = tempoExecucao / 2;
						}

						if (isMudouTurno) {
							controladorServicoAutorizacao.calcularDistanciaPontoConsumo(autorizcaoServicoLoteVO);

							isMudouTurno = Boolean.FALSE;
						}

						c.add(Calendar.MINUTE, Integer.valueOf(autorizcaoServicoLoteVO.getTempoDistancia()));

						String periodoExecucao = "";

						Calendar calendarioHoraFinal = Calendar.getInstance();
						calendarioHoraFinal.setTime(c.getTime());

						calendarioHoraFinal.add(Calendar.MINUTE, 60);

						Date dataIncrementada = calendarioHoraFinal.getTime();
						String horaInicialExecucao = Util
								.converterDataParaStringHoraMinutoComCaracteresEspeciais(c.getTime());
						Date dataExecucao = c.getTime();
						String horaFinalExecucao = Util
								.converterDataParaStringHoraMinutoComCaracteresEspeciais(dataIncrementada);
						LocalTime horarioFinal = LocalTime.of(calendarioHoraFinal.get(Calendar.HOUR_OF_DAY),
								calendarioHoraFinal.get(Calendar.MINUTE));

						c.add(Calendar.MINUTE, tempoExecucao.intValue());

						if ((servicoTipoAgendamentoTurno.getTurno().getDescricao().equals("TARDE")
								&& horarioFinal.isAfter(fimTurnoTarde))
								|| (servicoTipoAgendamentoTurno.getTurno().getDescricao().equals("MANHÃ")
										&& horarioFinal.isAfter(fimTurnoManha))
								|| (servicoTipoAgendamentoTurno.getTurno().getDescricao().equals("NOITE")
										&& horarioFinal.isAfter(fimTurnoNoite))) {
							isMudouTurno = Boolean.TRUE;
							break;
						}

						if (isMudouDia) {
							isMudouDia = Boolean.FALSE;
						}

						periodoExecucao = horaInicialExecucao + " horas e " + horaFinalExecucao + " horas";

						autorizcaoServicoLoteVO.setTempoExecucao(String.valueOf((tempoExecucao)));
						autorizcaoServicoLoteVO.setDataExecucao(DataUtil.converterDataParaString(dataExecucao));
						autorizcaoServicoLoteVO.setPeriodoExecucao(periodoExecucao);
						autorizcaoServicoLoteVO.setHoraInicialExecucao(horaInicialExecucao);
						autorizcaoServicoLoteVO.setServicoTipoAgendamentoTurno(servicoTipoAgendamentoTurno);
						autorizcaoServicoLoteVO.setCodigoLegado(autorizcaoServicoLoteVO.getPontoConsumo().getCodigoLegado());
						autorizcaoServicoLoteVO.setEquipe(servicoTipoAgendamentoTurno.getEquipe().getNome());
						autorizcaoServicoLoteVO.setSegmento(autorizcaoServicoLoteVO.getPontoConsumo().getSegmento().getDescricao());

						quantidadePontosRoteirizados--;
						listaRetorno.add(autorizcaoServicoLoteVO);
						listaPontoConsumoRoteirizado.remove(autorizcaoServicoLoteVO);

						sequencial++;
						tentativas = 0;

					}

					listaTurno.removeAll(listaRemover);

				} while (!entry.getValue().isEmpty() && quantidadePontosRoteirizados != 0);
				isMudouTurno = Boolean.TRUE;
			}

			Date proximoDiaUtil = Util.adicionarDiasData(c.getTime(), 1);
			c.setTime(proximoDiaUtil);
			dataAtual.setTime(c.getTime());
			isMudouDia = Boolean.TRUE;
			tentativas++;
			
			if(tentativas >= 3) {
				throw new NegocioException("Não foi possível roteirizar com esses parâmetros, tente novamente com outros!");
			}

		} while (quantidadePontosRoteirizados != 0);

		return listaRetorno;
	}

	private boolean consultarExistenciaTurno(EntidadeConteudo turnos[],
			Collection<ServicoTipoAgendamentoTurno> listaAgendamentosTurno) {
		
		if(turnos != null && turnos.length > 0) {
			return listaAgendamentosTurno.stream()
		            .anyMatch(p -> Arrays.stream(turnos)
                            .anyMatch(turno -> p.getTurno().getChavePrimaria() == turno.getChavePrimaria()));
		}
		
		return Boolean.TRUE;
	}
	
	
	@RequestMapping(value = "buscarPontosConsumoAS", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Map<String, Object> buscarPontosConsumoAS(@RequestParam String q, @RequestParam String servicoTipo, @RequestParam(required = false, value="page") Integer page) throws HibernateException, GGASException {
		
		Integer _page = page != null ? page : 0;
		Map<String, Object> results = new HashMap<String, Object>();
		Collection<PontoConsumo> colecaoPontoConsumo = new ArrayList<>();
		
			if(q.matches("[0-9]+")) {
				colecaoPontoConsumo = controladorServicoAutorizacao.filtrarPontosParaLoteDeAutorizacaoServico(null, q, servicoTipo, _page);
			} else {
				colecaoPontoConsumo = controladorServicoAutorizacao.filtrarPontosParaLoteDeAutorizacaoServico(q, null, servicoTipo, _page);
			}
		
		List<HashMap<String, String>> collect = colecaoPontoConsumo.stream()
				.map(r -> new HashMap<String, String>() {{
					put("id", String.valueOf(r.getChavePrimaria()));
					put("text",
							String.format("%s - %s - %s",
											r.getCodigoLegado(),
											r.getDescricao().trim(),
											r.getImovel().getNome()));
				}}).collect(Collectors.toList());
		results.put("results",collect);
		if(colecaoPontoConsumo.size() > 0) {
			results.put("pagination",new HashMap<String, Boolean>(){{put("more", true);}});
		}
		return results;
	}
	
	
	@RequestMapping("incluirPontoGridAS")
	public ModelAndView incluirPontoGridAS(HttpServletRequest request, Model model)
			throws NumberFormatException, NegocioException {
		ModelAndView gridPontoConsumo = new ModelAndView("gridPontoConsumoAS");

		Collection<PontoConsumo> listaPontoConsumoInserido = (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontoConsumoInserido");

		if (listaPontoConsumoInserido == null || listaPontoConsumoInserido.isEmpty()) {
			listaPontoConsumoInserido = new ArrayList<>();
		}

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(Long.valueOf(request.getParameter("idPontoConsumo")));

		boolean contemPontoConsumo = listaPontoConsumoInserido.stream()
				.filter(o -> o.getChavePrimaria() == pontoConsumo.getChavePrimaria()).findFirst().isPresent();

		if (pontoConsumo != null && !contemPontoConsumo) {
			listaPontoConsumoInserido.add(pontoConsumo);
		}

		request.getSession().setAttribute("listaPontoConsumoInserido", listaPontoConsumoInserido);

		gridPontoConsumo.addObject("listaPontoConsumoInserido", listaPontoConsumoInserido);

		return gridPontoConsumo;
	}
	
	
	@RequestMapping("removerPontosGridAS")
	public ModelAndView removerPontosGridAS(HttpServletRequest request, Model model) {
		ModelAndView gridPontoConsumo = new ModelAndView("gridPontoConsumoAS");

		Collection<PontoConsumo> listaPontoConsumoInserido = (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontoConsumoInserido");

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		if (listaPontoConsumoInserido != null && indexLista != null) {
			PontoConsumo pontoConsumo = ((ArrayList<PontoConsumo>) listaPontoConsumoInserido).get(indexLista.intValue());
			listaPontoConsumoInserido.remove(pontoConsumo);
		}

		request.getSession().setAttribute("listaPontoConsumoInserido", listaPontoConsumoInserido);

		gridPontoConsumo.addObject("listaPontoConsumoInserido", listaPontoConsumoInserido);

		return gridPontoConsumo;
	}
	
	@RequestMapping("gerarPlanilhaExcelAS")
	public void gerarPlanilhaExcelAS(@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam("servicoTipo") ServicoTipo servicoTipo, HttpServletRequest request, Model model,
			HttpServletResponse response) throws NegocioException {

		String tipoGeracaoLote = request.getParameter("tipoGeracaoLote");
		
		
		FiltroGeracaoServicoAutorizacaoLoteDTO filtroGeracaoServicoAutorizacaoLoteDTO = new FiltroGeracaoServicoAutorizacaoLoteDTO();
		filtroGeracaoServicoAutorizacaoLoteDTO.setServicoTipo(servicoTipo);
		
		Collection<PontoConsumo> colecaoPontoConsumo = obterColecaoPontoConsumo(request, tipoGeracaoLote);
		List<Long> chavesPontoConsumo = obterChavesPontoConsumo(chavesPrimarias, request, tipoGeracaoLote, colecaoPontoConsumo);
		
		Collection<PontoConsumo> colecaoPontoConsumoSelecionado = colecaoPontoConsumo.stream().filter(p -> chavesPontoConsumo.contains(p.getChavePrimaria())).collect(Collectors.toList());

		byte[] relatorio = controladorComunicacao.montarPlanilhaListagemPontosComunicacao(colecaoPontoConsumoSelecionado);

		FormatoImpressao formatoImpressao = FormatoImpressao.XLS;

		SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();

		fileName.append(" attachment; filename=relatorioListagemAS_");
		fileName.append(diaMesAno);
		fileName.append("_");
		fileName.append(hora);
		fileName.append(".xls");

		try {
			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(CONTENT_DISPOSITION, fileName.toString());
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e.getMessage());
		}

	}
	
	
	@RequestMapping("exibirRelatorioASLote")
	public String exibirRelatorioASLote(HttpServletRequest request, HttpServletResponse response)
			throws IOException, GGASException {
		List<byte[]> relatorios = (List<byte[]>) request.getSession().getAttribute("relatorioAutorizacaoServicoLote");

		gerarRelatorio(relatorios, response);
		request.getSession().removeAttribute("relatorioAutorizacaoServicoLote");

		return null;
	}
	
	private void gerarRelatorio(List<byte[]> relatorios, HttpServletResponse response) throws GGASException {

		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;

		SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();

		fileName.append(" attachment; filename=relatorioAS_");
		fileName.append(diaMesAno);
		fileName.append("_");
		fileName.append(hora);
		fileName.append(".pdf");

		byte[] relatorio = RelatorioUtil.unificarRelatoriosPdf(relatorios);

		try {
			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(CONTENT_DISPOSITION, fileName.toString());
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new GGASException(e);
		}
	}
	
	@RequestMapping("exibirAcompanhamentoFuncionario")
	public ModelAndView exibirAcompanhamentoFuncionario(HttpSession session, HttpServletResponse response)
			throws GGASException {
		ModelAndView model = new ModelAndView("exibirAcompanhamentoFuncionario");

		Collection<Funcionario> listaFuncionarios = obterFuncionarios();
		List<Object[]> listaLocalizacao = controladorServicoAutorizacao.consultarLocalizacaoOperadorMobile(null);
		List<String> nomes = listaLocalizacao.stream().map(array -> (String) array[0]).collect(Collectors.toList());

		Long periodicidadeLocalizacao = Long.valueOf(controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Periodicidade Registro Localização Autorização Serviço").iterator().next().getDescricao());

		listaFuncionarios = listaFuncionarios.stream().filter(funcionario -> nomes.contains(funcionario.getNome()))
				.collect(Collectors.toList());
		
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		model.addObject("listaFuncionario", listaFuncionarios);
		model.addObject("listaLocalizacao", listaLocalizacao);
		model.addObject("periodicidadeLocalizacao", periodicidadeLocalizacao);
		

		return model;
	}
	
	/**
	 * Obter funcionarios.
	 *
	 * @return collection
	 * @throws GGASException
	 */
	private Collection<Funcionario> obterFuncionarios() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put("habilitado", true);

		return controladorFuncionario.consultarFuncionarios(filtro);

	}
	
	
	/**
	 * 
	 * @param numeroSerie
	 * @param marcaMedidor
	 * @param tipoMedidor
	 * @param modelo
	 * @param modoUso
	 * @param session
	 * @return
	 * @throws GGASException
	 */
	@RequestMapping("consultarMedidorMobile")
	public ModelAndView consultarMedidorMobile(@RequestParam(value = "numeroSerie", required = false) String numeroSerie,
			@RequestParam(value = "marcaMedidor", required = false) Long marcaMedidor,
			@RequestParam(value = "tipoMedidor", required = false) Long tipoMedidor,
			@RequestParam(value = "modelo", required = false) Long modelo,
			@RequestParam(value = "modoUso", required = false) Long modoUso,
			@RequestParam(value = "somenteDisponivel", required = false) Boolean somenteDisponivel,
			HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridMedidorMobileBootstrap");
		
		Map<String, Object> filtro = new HashMap<String, Object>();
		prepararFiltroMedidor(filtro, numeroSerie, marcaMedidor, tipoMedidor, modelo, modoUso);
		Collection<Medidor> listaMedidor = controladorMedidor.consultarMedidorDisponivel(filtro);
		
		
		session.setAttribute("listaMedidor", listaMedidor);
		return model;
	}
	
	@RequestMapping("exibirGerarAutorizacaoServicoComunicacao")
	public ModelAndView exibirGerarAutorizacaoServicoComunicacao(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request) throws NegocioException {
		
		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.SERVICO_TIPO_PROTOCOLO);
		
		ServicoTipo servicoProtocolo = controladorServicoTipo.obterServicoTipo(constanteSistema.getValorLong());
		LoteComunicacao lote = (LoteComunicacao) controladorComunicacao.obter(chavePrimaria);
		Long[] chavesPrimarias = null;
		
		if(lote != null) {
			chavesPrimarias = lote.getItens().stream().filter(p -> !p.getDocumentoNaoEntregue().isIndicadorRetornado()).map(p -> p.getPontoConsumo().getChavePrimaria()).toArray(Long[]::new);
			request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SESSAO, lote.getItens().stream().map(p -> p.getPontoConsumo()).collect(Collectors.toList()));
			request.getSession().setAttribute("loteComunicacaoASProtocolo", lote);
		}
		
		return exibirGerarAutorizacaoServicoLote(chavesPrimarias, servicoProtocolo, request);
	}

}
