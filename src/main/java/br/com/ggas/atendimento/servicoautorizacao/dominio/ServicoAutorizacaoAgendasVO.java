package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.EntidadeConteudo;


/**
 * Servico Autorizacao Agendas VO
 */
public class ServicoAutorizacaoAgendasVO {
	private Long chavePrimaria;
	private Date dataAbertura;
	private Date dataAgendamento;
	private String tipoServicoDescricao;
	private String turnoDescricao;
	private String equipeDescricao;
	private String status;
	private Long statusID;
	private Long numeroProtocolo;
	
	private ServicoTipo servicoTipo;
	private EntidadeConteudo turno;
	private Equipe equipe;
	private ChamadoProtocolo protocolo;
	private EntidadeConteudo statusAtualAS;
	
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		if(dataAbertura != null) {
			this.dataAbertura = (Date) dataAbertura.clone();
		} else {
			this.dataAbertura = null;
		}
	}
	public String getDataAberturaFormatada() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(dataAbertura != null){
			return sdf.format(dataAbertura);
		}
		return "-";
	}
	public Date getDataAgendamento() {
		return dataAgendamento;
	}
	public void setDataAgendamento(Date dataAgendamento) {
		if(dataAgendamento != null) {
			this.dataAgendamento = (Date) dataAgendamento.clone();
		} else {
			this.dataAgendamento = null;
		}
	}
	public String getDataAgendamentoFormatada() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(dataAgendamento != null){
			return sdf.format(dataAgendamento);
		}
		return "-";
	}
	public String getTipoServicoDescricao() {
		return tipoServicoDescricao;
	}
	public void setTipoServicoDescricao(String tipoServicoDescricao) {
		this.tipoServicoDescricao = tipoServicoDescricao;
	}
	public String getTurnoDescricao() {
		return turnoDescricao;
	}
	public void setTurnoDescricao(String turnoDescricao) {
		this.turnoDescricao = turnoDescricao;
	}
	public String getEquipeDescricao() {
		return equipeDescricao;
	}
	public void setEquipeDescricao(String equipeDescricao) {
		this.equipeDescricao = equipeDescricao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getNumeroProtocolo() {
		return numeroProtocolo;
	}
	public void setNumeroProtocolo(Long numeroProtocolo) {
		this.numeroProtocolo = numeroProtocolo;
	}
	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}
	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}
	public EntidadeConteudo getTurno() {
		return turno;
	}
	public void setTurno(EntidadeConteudo turno) {
		this.turno = turno;
	}
	public Equipe getEquipe() {
		return equipe;
	}
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
	public ChamadoProtocolo getProtocolo() {
		return protocolo;
	}
	public void setProtocolo(ChamadoProtocolo protocolo) {
		this.protocolo = protocolo;
	}
	public EntidadeConteudo getStatusAtualAS() {
		return statusAtualAS;
	}
	public void setStatusAtualAS(EntidadeConteudo statusAtualAS) {
		this.statusAtualAS = statusAtualAS;
	}
	public Long getStatusID() {
		return statusID;
	}
	public void setStatusID(Long statusID) {
		this.statusID = statusID;
	}
	
	/**
	* Contem os Fields de posssiveis valores
	* 
	*/
	public enum Fields{
		chavePrimaria("chavePrimaria"),
		dataAbertura("dataAbertura"),
		dataAgendamento("dataAgendamento"),
		tipoServicoDescricao("tipoServicoDescricao"),
		turnoDescricao("turnoDescricao"),
		equipeDescricao("equipeDescricao"),
		status("status"),
		statusID("statusID"),
		numeroProtocolo("numeroProtocolo")
		;
		
		private String descricao;
		private Fields(String descricao){
			this.descricao = descricao;
		}
		
		@Override
		public String toString() {
			return this.descricao;
		}
	}
	

}
