/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 14:41:28
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela representação da autorização de serviços agendados
 */
public class ServicoAutorizacaoAgenda extends EntidadeNegocioImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5053563789975320227L;

	private Date dataAgenda;

	private ServicoAutorizacao servicoAutorizacao;

	private EntidadeConteudo turno;

	private Boolean indicadorConfirmado;
	
	private Funcionario funcionario;

	public Date getDataAgenda() {

		return dataAgenda;
	}

	public void setDataAgenda(Date dataAgenda) {
		if(dataAgenda != null){
			this.dataAgenda = (Date) dataAgenda.clone();
		} else {
			this.dataAgenda = null;
		}
	}

	public ServicoAutorizacao getServicoAutorizacao() {

		return servicoAutorizacao;
	}

	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {

		this.servicoAutorizacao = servicoAutorizacao;
	}

	public EntidadeConteudo getTurno() {

		return turno;
	}

	public void setTurno(EntidadeConteudo turno) {

		this.turno = turno;
	}

	public Boolean getIndicadorConfirmado() {

		return indicadorConfirmado;
	}

	public void setIndicadorConfirmado(Boolean indicadorConfirmado) {

		this.indicadorConfirmado = indicadorConfirmado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
