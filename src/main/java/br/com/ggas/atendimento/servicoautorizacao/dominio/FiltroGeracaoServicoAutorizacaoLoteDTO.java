package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

public class FiltroGeracaoServicoAutorizacaoLoteDTO extends EntidadeNegocioImpl implements EntidadeNegocio{
	/**
	 * 
	 */
	private static final long serialVersionUID = -332299469542736582L;
	
	private ServicoTipo servicoTipo;
	private String dataServico;
	private String horaServico;
	private Long tempoExecucao;
	private EntidadeConteudo[] turno;

	private static final int LIMITE_CAMPO = 2;

	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}

	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}

	public String getDataServico() {
		return dataServico;
	}

	public void setDataServico(String dataServico) {
		this.dataServico = dataServico;
	}

	public String getHoraServico() {
		return horaServico;
	}

	public void setHoraServico(String horaServico) {
		this.horaServico = horaServico;
	}

	@Override
	public Map<String, Object> validarDados() {
		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (servicoTipo == null || servicoTipo.getChavePrimaria() < 0l) {
			stringBuilder.append("Servico Tipo");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(dataServico)) {
			stringBuilder.append("Data Serviço");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(horaServico)) {
			stringBuilder.append("Hora Serviço");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (tempoExecucao == null || tempoExecucao < 0) {
			stringBuilder.append("Tempo Execução");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	public Long getTempoExecucao() {
		return tempoExecucao;
	}

	public void setTempoExecucao(Long tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}

	public EntidadeConteudo[] getTurno() {
		return turno;
	}

	public void setTurno(EntidadeConteudo[] turno) {
		this.turno = turno;
	}
}
