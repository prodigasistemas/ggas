/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/10/2013 10:41:38
 @author vtavares
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;

/**
 * ServicoAutorizacaoVO
 *
 */
public class ServicoAutorizacaoVO implements Serializable {

	private static final long serialVersionUID = -8099915761648856816L;

	private Long chavePrimaria;

	private String dataGeracaoInicio;

	private String dataGeracaoFim;

	private String dataPrevisaoInicio;

	private String dataPrevisaoFim;

	private String dataEncerramentoInicio;

	private String dataEncerramentoFim;

	private String dataExecucaoInicio;

	private String dataExecucaoFim;

	private Long numeroProtocoloChamado;
	
	private Long numeroOS;

	private Equipe equipe;

	private List<String> listaStatus;

	private ServicoTipo servicoTipo;
	
	private String descricaoServicoTipo;

	private ServicoTipoPrioridade servicoTipoPrioridade;

	private Boolean servicoAtraso;

	private ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento;
	
	private EntidadeConteudo turno;

	// atributos de contrato
	private Integer numeroContrato;

	// atributos do ponto consumo	
	private String siglaUnidadeFederacaoPontoConsumo;
	
	private String nomeMunicipioPontoConsumo;
	
	private String bairroPontoConsumo;
	
	private String lougradoroPontoConsumo;
	
	private String cepPontoConsumo;
	
	private String descricaoPontoConsumo;

	// atributos de imovel
	private String cepImovel;

	private String numeroImovel;

	private String descricaoComplemento;

	private String nomeImovel;

	private Long matriculaImovel;

	private Boolean condominioImovel;

	private List<Long> idsCondominioImovel;

	// atributos de cliente
	private String cpfCliente;

	private String rgCliente;

	private String nomeCliente;

	private String numeroPassaporte;

	private String cnpjCliente;

	private String nomeFantasiaCliente;

	private Integer pagina;

	private PontoConsumo pontoConsumo;

	private Boolean servicoAgendado;

	private int mes;

	private int ano;
	
	private String dataSelecionada;

	// atributosChamado

	private ChamadoVO chamadoVO;

	private String nomeSolicitanteChamado;

	private String cpfCnpjSolicitanteChamado;

	private Long idChamadoTipo;

	private List<ChamadoAssunto> chamadoAssuntos = new ArrayList<>();

	private List<ServicoTipo> servicosTipos = new ArrayList<>();

	private EntidadeConteudo statusChamado;

	private CanalAtendimento canalAtendimento;

	private UnidadeOrganizacional unidadeOrganizacionalChamado;

	private Usuario usuarioResponsavelChamado;

	private Boolean indicadorAssociado;

	private Boolean encerradoAposPrazo;

	private Boolean indicadorSucessoExecucao;
	
	private Usuario solicitante;

	private String enderecoImovel;
	
	private Boolean isChamadoPendente; 
	
	private Boolean podeSerAgendado;
	
	private Long idTipoChamado;
	
	private Long chamadoAssunto;
	
	private String tipagemRelatorio;
	
	private Boolean isAgendamento;
	
	/**
	 * @return chavePrimaria
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return dataGeracaoInicio
	 */
	public String getDataGeracaoInicio() {

		return dataGeracaoInicio;
	}

	/**
	 * @param dataGeracaoInicio
	 */
	public void setDataGeracaoInicio(String dataGeracaoInicio) {

		this.dataGeracaoInicio = dataGeracaoInicio;
	}

	/**
	 * @return dataGeracaoFim
	 */
	public String getDataGeracaoFim() {

		return dataGeracaoFim;
	}

	/**
	 * @param dataGeracaoFim
	 */
	public void setDataGeracaoFim(String dataGeracaoFim) {

		this.dataGeracaoFim = dataGeracaoFim;
	}

	/**
	 * @return dataPrevisaoInicio
	 */
	public String getDataPrevisaoInicio() {

		return dataPrevisaoInicio;
	}

	/**
	 * @param dataPrevisaoInicio
	 */
	public void setDataPrevisaoInicio(String dataPrevisaoInicio) {

		this.dataPrevisaoInicio = dataPrevisaoInicio;
	}

	/**
	 * @return dataPrevisaoFim
	 */
	public String getDataPrevisaoFim() {

		return dataPrevisaoFim;
	}

	/**
	 * @param dataPrevisaoFim
	 */
	public void setDataPrevisaoFim(String dataPrevisaoFim) {

		this.dataPrevisaoFim = dataPrevisaoFim;
	}

	/**
	 * @return dataEncerramentoInicio
	 */
	public String getDataEncerramentoInicio() {

		return dataEncerramentoInicio;
	}

	/**
	 * @param dataEncerramentoInicio
	 */
	public void setDataEncerramentoInicio(String dataEncerramentoInicio) {

		this.dataEncerramentoInicio = dataEncerramentoInicio;
	}

	/**
	 * @return dataEncerramentoFim
	 */
	public String getDataEncerramentoFim() {

		return dataEncerramentoFim;
	}

	/**
	 * @param dataEncerramentoFim
	 */
	public void setDataEncerramentoFim(String dataEncerramentoFim) {

		this.dataEncerramentoFim = dataEncerramentoFim;
	}

	/**
	 * @return numeroProtocoloChamado
	 */
	public Long getNumeroProtocoloChamado() {

		return numeroProtocoloChamado;
	}

	/**
	 * @param numeroProtocoloChamado
	 */
	public void setNumeroProtocoloChamado(Long numeroProtocoloChamado) {

		this.numeroProtocoloChamado = numeroProtocoloChamado;
	}

	/**
	 * @return equipe
	 */
	public Equipe getEquipe() {

		return equipe;
	}

	/**
	 * @param equipe
	 */
	public void setEquipe(Equipe equipe) {

		this.equipe = equipe;
	}

	/**
	 * @return listaStatus
	 */
	public List<String> getListaStatus() {

		return listaStatus;
	}

	/**
	 * @param listaStatus
	 */
	public void setListaStatus(List<String> listaStatus) {

		this.listaStatus = listaStatus;
	}

	/**
	 * @return servicoTipo
	 */
	public ServicoTipo getServicoTipo() {

		return servicoTipo;
	}

	/**
	 * @param servicoTipo
	 */
	public void setServicoTipo(ServicoTipo servicoTipo) {

		this.servicoTipo = servicoTipo;
	}

	/**
	 * @return servicoTipoPrioridade
	 */
	public ServicoTipoPrioridade getServicoTipoPrioridade() {

		return servicoTipoPrioridade;
	}

	/**
	 * @param servicoTipoPrioridade
	 */
	public void setServicoTipoPrioridade(ServicoTipoPrioridade servicoTipoPrioridade) {

		this.servicoTipoPrioridade = servicoTipoPrioridade;
	}

	/**
	 * @return servicoAtraso
	 */
	public Boolean getServicoAtraso() {

		return servicoAtraso;
	}

	/**
	 * @param servicoAtraso
	 */
	public void setServicoAtraso(Boolean servicoAtraso) {

		this.servicoAtraso = servicoAtraso;
	}

	/**
	 * @return numeroContrato
	 */
	public Integer getNumeroContrato() {

		return numeroContrato;
	}

	/**
	 * @param numeroContrato
	 */
	public void setNumeroContrato(Integer numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	/**
	 * @return cepImovel
	 */
	public String getCepImovel() {

		return cepImovel;
	}

	/**
	 * @param cepImovel
	 */
	public void setCepImovel(String cepImovel) {

		this.cepImovel = cepImovel;
	}

	/**
	 * @return numeroImovel
	 */
	public String getNumeroImovel() {

		return numeroImovel;
	}

	/**
	 * @param numeroImovel
	 */
	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	/**
	 * @return descricaoComplemento
	 */
	public String getDescricaoComplemento() {

		return descricaoComplemento;
	}

	/**
	 * @param descricaoComplemento
	 */
	public void setDescricaoComplemento(String descricaoComplemento) {

		this.descricaoComplemento = descricaoComplemento;
	}

	/**
	 * @return nomeImovel
	 */
	public String getNomeImovel() {

		return nomeImovel;
	}

	/**
	 * @param nomeImovel
	 */
	public void setNomeImovel(String nomeImovel) {

		this.nomeImovel = nomeImovel;
	}

	/**
	 * @return matriculaImovel
	 */
	public Long getMatriculaImovel() {

		return matriculaImovel;
	}

	/**
	 * @param matriculaImovel
	 */
	public void setMatriculaImovel(Long matriculaImovel) {

		this.matriculaImovel = matriculaImovel;
	}

	/**
	 * @return condominioImovel
	 */
	public Boolean getCondominioImovel() {

		return condominioImovel;
	}

	/**
	 * @param condominioImovel
	 */
	public void setCondominioImovel(Boolean condominioImovel) {

		this.condominioImovel = condominioImovel;
	}

	/**
	 * @return cpfCliente
	 */
	public String getCpfCliente() {

		return cpfCliente;
	}

	/**
	 * @param cpfCliente
	 */
	public void setCpfCliente(String cpfCliente) {

		this.cpfCliente = cpfCliente;
	}

	/**
	 * @return rgCliente
	 */
	public String getRgCliente() {

		return rgCliente;
	}

	/**
	 * @param rgCliente
	 */
	public void setRgCliente(String rgCliente) {

		this.rgCliente = rgCliente;
	}

	/**
	 * @return nomeCliente
	 */
	public String getNomeCliente() {

		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/**
	 * @return numeroPassaporte
	 */
	public String getNumeroPassaporte() {

		return numeroPassaporte;
	}

	/**
	 * @param numeroPassaporte
	 */
	public void setNumeroPassaporte(String numeroPassaporte) {

		this.numeroPassaporte = numeroPassaporte;
	}

	/**
	 * @return cnpjCliente
	 */
	public String getCnpjCliente() {

		return cnpjCliente;
	}

	/**
	 * @param cnpjCliente
	 */
	public void setCnpjCliente(String cnpjCliente) {

		this.cnpjCliente = cnpjCliente;
	}

	/**
	 * @return nomeFantasiaCliente
	 */
	public String getNomeFantasiaCliente() {

		return nomeFantasiaCliente;
	}

	/**
	 * @param nomeFantasiaCliente
	 */
	public void setNomeFantasiaCliente(String nomeFantasiaCliente) {

		this.nomeFantasiaCliente = nomeFantasiaCliente;
	}

	/**
	 * @return serialVersionUID
	 */
	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/**
	 * @return servicoAgendado
	 */
	public Boolean getServicoAgendado() {

		return servicoAgendado;
	}

	/**
	 * @param servicoAgendado
	 */
	public void setServicoAgendado(Boolean servicoAgendado) {

		this.servicoAgendado = servicoAgendado;
	}

	/**
	 * @return mes
	 */
	public int getMes() {

		return mes;
	}

	/**
	 * @param mes
	 */
	public void setMes(int mes) {

		this.mes = mes;
	}

	/**
	 * @return ano
	 */
	public int getAno() {

		return ano;
	}

	/**
	 * @param ano
	 */
	public void setAno(int ano) {

		this.ano = ano;
	}

	/**
	 * @return pagina
	 */
	public Integer getPagina() {

		return pagina;
	}

	/**
	 * @param pagina
	 */
	public void setPagina(Integer pagina) {

		this.pagina = pagina;
	}

	/**
	 * @return vchamadoVO
	 */
	public ChamadoVO getChamadoVO() {

		return chamadoVO;
	}

	/**
	 * @param chamadoVO
	 */
	public void setChamadoVO(ChamadoVO chamadoVO) {

		this.chamadoVO = chamadoVO;
	}

	/**
	 * @return idChamadoTipo
	 */
	public Long getIdChamadoTipo() {

		return idChamadoTipo;
	}

	/**
	 * @param idChamadoTipo
	 */
	public void setIdChamadoTipo(Long idChamadoTipo) {

		this.idChamadoTipo = idChamadoTipo;
	}

	/**
	 * @return chamadoAssuntos
	 */
	public List<ChamadoAssunto> getChamadoAssuntos() {

		return chamadoAssuntos;
	}

	/**
	 * @param chamadoAssunto
	 */
	public void setChamadoAssuntos(List<ChamadoAssunto> chamadoAssunto) {

		this.chamadoAssuntos = chamadoAssunto;
	}

	/**
	 * @return statusChamado
	 */
	public EntidadeConteudo getStatusChamado() {

		return statusChamado;
	}

	/**
	 * @param statusChamado
	 */
	public void setStatusChamado(EntidadeConteudo statusChamado) {

		this.statusChamado = statusChamado;
	}

	/**
	 * @return canalAtendimento
	 */
	public CanalAtendimento getCanalAtendimento() {

		return canalAtendimento;
	}

	/**
	 * @param canalAtendimento
	 */
	public void setCanalAtendimento(CanalAtendimento canalAtendimento) {

		this.canalAtendimento = canalAtendimento;
	}

	/**
	 * @return nomeSolicitanteChamado
	 */
	public String getNomeSolicitanteChamado() {

		return nomeSolicitanteChamado;
	}

	/**
	 * @param nomeSolicitanteChamado
	 */
	public void setNomeSolicitanteChamado(String nomeSolicitanteChamado) {

		this.nomeSolicitanteChamado = nomeSolicitanteChamado;
	}

	/**
	 * @return cpfCnpjSolicitanteChamado
	 */
	public String getCpfCnpjSolicitanteChamado() {

		return cpfCnpjSolicitanteChamado;
	}

	/**
	 * @param cpfCnpjSolicitanteChamado
	 */
	public void setCpfCnpjSolicitanteChamado(String cpfCnpjSolicitanteChamado) {

		this.cpfCnpjSolicitanteChamado = cpfCnpjSolicitanteChamado;
	}

	/**
	 * @return unidadeOrganizacionalChamado
	 */
	public UnidadeOrganizacional getUnidadeOrganizacionalChamado() {

		return unidadeOrganizacionalChamado;
	}

	/**
	 * @param unidadeOrganizacionalChamado
	 */
	public void setUnidadeOrganizacionalChamado(UnidadeOrganizacional unidadeOrganizacionalChamado) {

		this.unidadeOrganizacionalChamado = unidadeOrganizacionalChamado;
	}

	/**
	 * @return usuarioResponsavelChamado
	 */
	public Usuario getUsuarioResponsavelChamado() {

		return usuarioResponsavelChamado;
	}

	/**
	 * @param usuarioResponsavelChamado
	 */
	public void setUsuarioResponsavelChamado(Usuario usuarioResponsavelChamado) {

		this.usuarioResponsavelChamado = usuarioResponsavelChamado;
	}

	/**
	 * @return indicadorAssociado
	 */
	public Boolean getIndicadorAssociado() {

		return indicadorAssociado;
	}

	/**
	 * @param indicadorAssociado
	 */
	public void setIndicadorAssociado(Boolean indicadorAssociado) {

		this.indicadorAssociado = indicadorAssociado;
	}

	/**
	 * @return encerradoAposPrazo
	 */
	public Boolean getEncerradoAposPrazo() {

		return encerradoAposPrazo;
	}

	/**
	 * @param encerradoAposPrazo
	 */
	public void setEncerradoAposPrazo(Boolean encerradoAposPrazo) {

		this.encerradoAposPrazo = encerradoAposPrazo;
	}

	/**
	 * @return servicosTipos
	 */
	public List<ServicoTipo> getServicosTipos() {

		return servicosTipos;
	}

	/**
	 * @param servicosTipos
	 */
	public void setServicosTipos(List<ServicoTipo> servicosTipos) {

		this.servicosTipos = servicosTipos;
	}

	/**
	 * @return pontoConsumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return idsCondominioImovel
	 */
	public List<Long> getIdsCondominioImovel() {

		return idsCondominioImovel;
	}

	/**
	 * @param idsCondominioImovel
	 */
	public void setIdsCondominioImovel(List<Long> idsCondominioImovel) {

		this.idsCondominioImovel = idsCondominioImovel;
	}

	/**
	 * @return dataSelecionada
	 */
	public String getDataSelecionada() {
		return dataSelecionada;
	}

	/**
	 * @param dataSelecionada
	 */
	public void setDataSelecionada(String dataSelecionada) {
		this.dataSelecionada = dataSelecionada;
	}

	/**
	 * @return dataExecucaoInicio
	 */
	public String getDataExecucaoInicio() {
		return dataExecucaoInicio;
	}

	/**
	 * @param dataExecucaoInicio
	 */
	public void setDataExecucaoInicio(String dataExecucaoInicio) {
		this.dataExecucaoInicio = dataExecucaoInicio;
	}

	/**
	 * @return dataExecucaoFim
	 */
	public String getDataExecucaoFim() {
		return dataExecucaoFim;
	}

	/**
	 * @param dataExecucaoFim
	 */
	public void setDataExecucaoFim(String dataExecucaoFim) {
		this.dataExecucaoFim = dataExecucaoFim;
	}

	/**
	 * @return the indicadorSucessoExecucao
	 */
	public Boolean getIndicadorSucessoExecucao() {
		return indicadorSucessoExecucao;
	}

	/**
	 * @param indicadorSucessoExecucao the indicadorSucessoExecucao to set
	 */
	public void setIndicadorSucessoExecucao(Boolean indicadorSucessoExecucao) {
		this.indicadorSucessoExecucao = indicadorSucessoExecucao;
	}

	/**
	 * @return siglaUnidadeFederacaoPontoConsumo
	 */
	public String getSiglaUnidadeFederacaoPontoConsumo() {
		return siglaUnidadeFederacaoPontoConsumo;
	}

	/**
	 * @param siglaUnidadeFederacaoPontoConsumo
	 */
	public void setSiglaUnidadeFederacaoPontoConsumo(String siglaUnidadeFederacaoPontoConsumo) {
		this.siglaUnidadeFederacaoPontoConsumo = siglaUnidadeFederacaoPontoConsumo;
	}

	/**
	 * @return nomeMunicipioPontoConsumo
	 */
	public String getNomeMunicipioPontoConsumo() {
		return nomeMunicipioPontoConsumo;
	}

	/**
	 * @param nomeMunicipioPontoConsumo
	 */
	public void setNomeMunicipioPontoConsumo(String nomeMunicipioPontoConsumo) {
		this.nomeMunicipioPontoConsumo = nomeMunicipioPontoConsumo;
	}

	/**
	 * @return bairroPontoConsumo
	 */
	public String getBairroPontoConsumo() {
		return bairroPontoConsumo;
	}

	/**
	 * @param bairroPontoConsumo
	 */
	public void setBairroPontoConsumo(String bairroPontoConsumo) {
		this.bairroPontoConsumo = bairroPontoConsumo;
	}

	/**
	 * @return lougradoroPontoConsumo
	 */
	public String getLougradoroPontoConsumo() {
		return lougradoroPontoConsumo;
	}

	/**
	 * @param lougradoroPontoConsumo
	 */
	public void setLougradoroPontoConsumo(String lougradoroPontoConsumo) {
		this.lougradoroPontoConsumo = lougradoroPontoConsumo;
	}

	/**
	 * @return cepPontoConsumo
	 */
	public String getCepPontoConsumo() {
		return cepPontoConsumo;
	}

	/**
	 * @param cepPontoConsumo
	 */
	public void setCepPontoConsumo(String cepPontoConsumo) {
		this.cepPontoConsumo = cepPontoConsumo;
	}

	/**
	 * @return servicoAutorizacaoMotivoEncerramento
	 */
	public ServicoAutorizacaoMotivoEncerramento getServicoAutorizacaoMotivoEncerramento() {
		return servicoAutorizacaoMotivoEncerramento;
	}

	/**
	 * @param servicoAutorizacaoMotivoEncerramento
	 */
	public void setServicoAutorizacaoMotivoEncerramento(ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento) {
		this.servicoAutorizacaoMotivoEncerramento = servicoAutorizacaoMotivoEncerramento;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getDescricaoServicoTipo() {
		return descricaoServicoTipo;
	}

	public void setDescricaoServicoTipo(String descricaoServicoTipo) {
		this.descricaoServicoTipo = descricaoServicoTipo;
	}

	public EntidadeConteudo getTurno() {
		return turno;
	}

	public void setTurno(EntidadeConteudo turno) {
		this.turno = turno;
	}
	
	public String getEnderecoImovel() {
		return enderecoImovel;
	}

	public void setEnderecoImovel(String enderecoImovel) {
		this.enderecoImovel = enderecoImovel;
	}
	
	public Usuario getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Usuario solicitante) {
		this.solicitante = solicitante;
	}

	public Long getNumeroOS() {
		return numeroOS;
	}

	public void setNumeroOS(Long numeroOS) {
		this.numeroOS = numeroOS;
	}

	public Boolean getIsChamadoPendente() {
		return isChamadoPendente;
	}

	public void setIsChamadoPendente(Boolean isChamadoPendente) {
		this.isChamadoPendente = isChamadoPendente;
	}

	public Boolean getPodeSerAgendado() {
		return podeSerAgendado;
	}

	public void setPodeSerAgendado(Boolean podeSerAgendado) {
		this.podeSerAgendado = podeSerAgendado;
	}

	public Long getIdTipoChamado() {
		return idTipoChamado;
	}

	public void setIdTipoChamado(Long idTipoChamado) {
		this.idTipoChamado = idTipoChamado;
	}

	public Long getChamadoAssunto() {
		return chamadoAssunto;
	}

	public void setChamadoAssunto(Long chamadoAssunto) {
		this.chamadoAssunto = chamadoAssunto;
	}
	
	public String getTipagemRelatorio() {
		return tipagemRelatorio;
	}

	public void setTipagemRelatorio(String tipagemRelatorio) {
		this.tipagemRelatorio = tipagemRelatorio;
	}

	public Boolean getIsAgendamento() {
		return isAgendamento;
	}

	public void setIsAgendamento(Boolean isAgendamento) {
		this.isAgendamento = isAgendamento;
	}
	
}
