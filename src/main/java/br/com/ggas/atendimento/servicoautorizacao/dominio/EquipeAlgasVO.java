package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Date;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.EntidadeConteudo;

public class EquipeAlgasVO {
	
	private String nome;
	private EntidadeConteudo turno;
	private Equipe equipe;
	private Date dataInicial;
	private Date dataFinal;
	private Long duracao;
	private Funcionario funcionario;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public EntidadeConteudo getTurno() {
		return turno;
	}
	public void setTurno(EntidadeConteudo turno) {
		this.turno = turno;
	}
	public Equipe getEquipe() {
		return equipe;
	}
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
	public Date getDataInicial() {
		return dataInicial;
	}
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	public Date getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	public Long getDuracao() {
		return duracao;
	}
	public void setDuracao(Long duracao) {
		this.duracao = duracao;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
}
