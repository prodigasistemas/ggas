package br.com.ggas.atendimento.servicoautorizacao.batch;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.atendimento.servicoautorizacao.dominio.AcaoServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cobranca.entregadocumento.ControladorEntregaDocumento;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.impl.ContratoAnexo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorInstalacaoMedidor;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DadosAuditoriaUtil;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

public class EncerrarASAppBatch {
	
	public static final String BEAN_ID_ENCERRAR_AS_APP_BATCH = "encerrarASAppBatch";
	
	private static final Logger LOG = Logger.getLogger(EncerrarASAppBatch.class);
	
	private ControladorServicoAutorizacao controladorServicoAutorizacao;
	
	private ControladorMedidor controladorMedidor;
	
	private ControladorConstanteSistema controladorConstanteSistema;
	
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	private ControladorPontoConsumo controladorPontoConsumo;
	
	private ControladorInstalacaoMedidor controladorInstalacaoMedidor;
	
	private ControladorParametroSistema controladorParametroSistema;
	
	private ControladorContrato controladorContrato;
	
	private ControladorEntregaDocumento controladorEntregaDocumento;

	
	private final ServiceLocator serviceLocator = ServiceLocator.getInstancia();
	
	private final Fachada fachada = Fachada.getInstancia();


	public EncerrarASAppBatch() {
		controladorServicoAutorizacao = serviceLocator.getControladorServicoAutorizacao();
		controladorMedidor = serviceLocator.getControladorMedidor();
		controladorConstanteSistema = serviceLocator.getControladorConstanteSistema();
		controladorEntidadeConteudo = serviceLocator.getControladorEntidadeConteudo();
		controladorPontoConsumo = serviceLocator.getControladorPontoConsumo();
		controladorInstalacaoMedidor = (ControladorInstalacaoMedidor) serviceLocator
				.getControladorNegocio(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);
		controladorParametroSistema = serviceLocator.getControladorParametroSistema();
		controladorContrato = serviceLocator.getControladorContrato();
		controladorEntregaDocumento = (ControladorEntregaDocumento) serviceLocator
				.getControladorNegocio(ControladorEntregaDocumento.BEAN_ID_CONTROLADOR_ENTRAGA_DOCUMENTO);
		
	}
	
	public void encerrarASApp() throws GGASException {
		LOG.info("Iniciando rotina para encerrar autorização de serviço encerrada pelo Aplicativo Mobile ...");

		Collection<ServicoAutorizacaoTemp> listaServicoAutorizacaoTemp = controladorServicoAutorizacao
				.listarAutorizacoesServicoEncerramentoAPP();
		String codigoRifReativacao = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SERVICO_RIF_REATIVACAO);
		
		Long codigoServicoTipoProtocolo = Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.SERVICO_TIPO_PROTOCOLO));
		
		DadosAuditoria dadosAuditoria = null;
		
		if (!listaServicoAutorizacaoTemp.isEmpty()) {

			String enderecoIP = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP);
			Operacao operacao = Fachada.getInstancia().obterOperacaoSistema(1890l);

			Usuario usuario = Fachada.getInstancia().obterUsuario(1l);
			
			dadosAuditoria = Util.getDadosAuditoria(usuario, operacao, enderecoIP);
		}
		  

		for (ServicoAutorizacaoTemp servicoAutorizacaoTemp : listaServicoAutorizacaoTemp) {
			ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoTemp.getServicoAutorizacao();
			ServicoTipo servicoTipo = servicoAutorizacao.getServicoTipo();
			String status = "";
			
			if(servicoAutorizacao.getServicoTipo().getChavePrimaria() != codigoServicoTipoProtocolo) {
				confirmarInformacoesTemp(servicoAutorizacaoTemp);
			}
		
			if(servicoAutorizacao.getServicoTipo().getChavePrimaria() == codigoServicoTipoProtocolo) {
				try {
					if(retornarDataProtocolo(servicoAutorizacaoTemp)) {
						status = Constantes.C_STATUS_SERV_AUT_ENCERRADO;
					} else {
						status = Constantes.C_STATUS_SERV_AUT_PENDENTE;
					}
				} catch (GGASException e) {
					LOG.info(e);
					LOG.error(e);
					e.printStackTrace();
					status = Constantes.C_STATUS_SERV_AUT_PENDENTE;
				}
			} else if (isServicoTipoAtualizacaoCadastral(servicoTipo)
					|| verificarSubstituicaoMedidor(servicoAutorizacaoTemp, servicoTipo)
					|| (!isServicoTipoAtualizacaoCadastral(servicoTipo)
							&& servicoAutorizacaoTemp.possuiNovoMedidor())) {
				
				atribuirOperacaoSubstituicaoMedidor(servicoAutorizacaoTemp, servicoTipo);
				
				Boolean isContratoAtualizado = Boolean.TRUE;
				if(isAtualizaContrato(servicoAutorizacaoTemp)) {
					isContratoAtualizado = this.atualizarContrato(servicoAutorizacaoTemp, dadosAuditoria);
				}
				
				if (isContratoAtualizado && servicoTipo.getChavePrimaria() != Long.valueOf(codigoRifReativacao)
						&& validarMedidores(servicoAutorizacaoTemp)) {
					try {

						associarMedidorPontoConsumo(servicoAutorizacaoTemp);
						status = Constantes.C_STATUS_SERV_AUT_ENCERRADO;
					} catch (GGASException e) {
						LOG.info(e);
						LOG.error(e);
						e.printStackTrace();
						status = Constantes.C_STATUS_SERV_AUT_PENDENTE;
					}
				} else {
					status = Constantes.C_STATUS_SERV_AUT_PENDENTE;
				}

			} else if(isAtualizaContrato(servicoAutorizacaoTemp)) {
				
				if(atualizarContrato(servicoAutorizacaoTemp, dadosAuditoria)) {
					status = Constantes.C_STATUS_SERV_AUT_ENCERRADO;
				} else {
					status = Constantes.C_STATUS_SERV_AUT_PENDENTE;
				}
				
			} else {
				status = Constantes.C_STATUS_SERV_AUT_ENCERRADO;
			}

			this.setarStatusAutorizacaoServico(servicoAutorizacaoTemp, servicoAutorizacao, status);
			controladorServicoAutorizacao.atualizarServicoAutorizacaoTempSemValidar(servicoAutorizacaoTemp);
			if(status.equals(Constantes.C_STATUS_SERV_AUT_ENCERRADO)) {
				controladorServicoAutorizacao.enviarEmailEncerramentoApp(AcaoServicoAutorizacao.ENCERRAR, servicoAutorizacao);
				Map<String, Object> dados =new HashMap<String, Object>();
				dados.put("usuario", servicoAutorizacaoTemp.getFuncionario().getUsuario());
				dados.put("isPontoCicloLeitura", false);
				dados.put("indicadorVazamentoConfirmado", false);
				dados.put("dataPrevisaoEncerramento", new Date());
				
				controladorServicoAutorizacao.verificaEncerramentoChamado(servicoAutorizacao, dados);
			}
		}

		LOG.info("Terminando rotina para encerrar autorização de serviço encerrada pelo Aplicativo Mobile ...");
	}

	private void atribuirOperacaoSubstituicaoMedidor(ServicoAutorizacaoTemp servicoAutorizacaoTemp,
			ServicoTipo servicoTipo) throws GGASException, NegocioException {
		if(!isServicoTipoAtualizacaoCadastral(servicoTipo) && servicoAutorizacaoTemp.possuiNovoMedidor()) {
			if(servicoAutorizacaoTemp.getMedidorOperacao() == null) {
				OperacaoMedidor medidorOperacao = fachada.obterOperacaoMedidor(OperacaoMedidor.CODIGO_SUBSTITUICAO);
				servicoAutorizacaoTemp.setMedidorOperacao(medidorOperacao);
			}
			
			if(servicoAutorizacaoTemp.getMotivoOperacaoMedidor() == null) {
				MotivoOperacaoMedidor medidorOperacao = fachada.obterMotivoOperacaoMedidor(6l);
				servicoAutorizacaoTemp.setMotivoOperacaoMedidor(medidorOperacao);
			}
		}
	}

	private Boolean retornarDataProtocolo(ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws GGASException {
		EntregaDocumento protocolo = controladorEntregaDocumento.consultarProtocoloPorAutorizacaoServicoProtocolo(servicoAutorizacaoTemp.getServicoAutorizacao().getChavePrimaria());
		
		Collection<EntregaDocumento> colecaoProtocolo = new ArrayList<EntregaDocumento>();
		colecaoProtocolo.add(protocolo);
		
		if(protocolo == null) {
			return Boolean.FALSE;
		}
		
		Long[] chaveProtocolo = {protocolo.getChavePrimaria()};
		
		
		return controladorEntregaDocumento.popularDataMensagemRetornoProtocolo(chaveProtocolo, colecaoProtocolo, DataUtil.converterDataParaString(servicoAutorizacaoTemp.getFimExecucao()), servicoAutorizacaoTemp.getObservacao(), servicoAutorizacaoTemp.getFuncionario(), Boolean.FALSE);
	}

	private boolean isServicoTipoAtualizacaoCadastral(ServicoTipo servicoTipo) {
		return !StringUtils.isEmpty(servicoTipo.getIndicadorAtualizacaoCadastral())
				&& !"N".equals(servicoTipo.getIndicadorAtualizacaoCadastral());
	}
	
	
	private boolean isAtualizaContrato(ServicoAutorizacaoTemp servicoAutorizacaoTemp) {
		return servicoAutorizacaoTemp.getVazaoFornecimentoNova() != null || servicoAutorizacaoTemp.getPressaoFornecimentoNova() != null;
	}
	

	private void confirmarInformacoesTemp(ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws GGASException {
		if(servicoAutorizacaoTemp.getMedidorNovo() == null && servicoAutorizacaoTemp.getMedidorNumeroSerieNovo() != null) {
			List<Medidor> medidor = (List<Medidor>) fachada.obterMedidorPorNumeroSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieNovo());
			
			if(medidor != null && !medidor.isEmpty()) {
				servicoAutorizacaoTemp.setMedidorNovo(medidor.get(medidor.size() - 1));
			}
		} 
		
		if(servicoAutorizacaoTemp.getMedidorOriginal() == null && servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal() != null) {
			List<Medidor> medidor = (List<Medidor>) fachada.obterMedidorPorNumeroSerie(servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal());
			if(medidor != null && !medidor.isEmpty()) {
				servicoAutorizacaoTemp.setMedidorOriginal(medidor.get(medidor.size() - 1));
			}
		}
		
		if(servicoAutorizacaoTemp.getMedidorNovo() != null && servicoAutorizacaoTemp.getMedidorNumeroSerieNovo() == null) {
			servicoAutorizacaoTemp.setMedidorNumeroSerieNovo(servicoAutorizacaoTemp.getMedidorNovo().getNumeroSerie());
		}
		
		if(servicoAutorizacaoTemp.getMedidorOriginal() != null && servicoAutorizacaoTemp.getMedidorNumeroSerieOriginal() == null) {
			servicoAutorizacaoTemp.setMedidorNumeroSerieOriginal(servicoAutorizacaoTemp.getMedidorOriginal().getNumeroSerie());
		}
		
		ServicoTipo servicoTipo = servicoAutorizacaoTemp.getServicoAutorizacao().getServicoTipo();
		if(servicoTipo != null) {
			EntidadeConteudo ocorrencia = servicoAutorizacaoTemp.getOcorrencia();
			
			if (ocorrencia != null && !ocorrencia.getCodigo().isEmpty()
					&& Long.valueOf(ocorrencia.getCodigo()) != servicoTipo.getChavePrimaria()) {
				ServicoTipo servicoTipoTroca = fachada.consultarServicoTipo(Long.valueOf(ocorrencia.getCodigo()));
				ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoTemp.getServicoAutorizacao();
				servicoAutorizacao.setServicoTipo(servicoTipoTroca);
				
				controladorServicoAutorizacao.atualizarSemValidar(servicoAutorizacao);
			}
			
		}
		
	}

	private boolean verificarSubstituicaoMedidor(ServicoAutorizacaoTemp servicoAutorizacaoTemp,
			ServicoTipo servicoTipo) throws NegocioException {
		if(servicoAutorizacaoTemp.getMedidorNovo() != null && servicoTipo.getIndicadorNovoMedidor()) {
			
			OperacaoMedidor operacaoSubstituicao = controladorMedidor.obterOperacaoMedidor(OperacaoMedidor.CODIGO_SUBSTITUICAO);
			
			servicoAutorizacaoTemp.setMedidorOperacao(operacaoSubstituicao);
			
			if(servicoAutorizacaoTemp.getMotivoOperacaoMedidor() == null) {
				MotivoOperacaoMedidor motivoOperacaoMedidor = controladorMedidor.listarMotivoOperacaoMedidor().stream()
						.filter(p -> p.getChavePrimaria() == 6l).findFirst().orElse(null);
				
				if(motivoOperacaoMedidor != null) {
					servicoAutorizacaoTemp.setMotivoOperacaoMedidor(motivoOperacaoMedidor);
				}
			}
			
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}

	private void associarMedidorPontoConsumo(ServicoAutorizacaoTemp servicoAutorizacaoTemp) throws GGASException {
		
		Map<String, Object> dados = obterDadosAssociacao(servicoAutorizacaoTemp);
		PontoConsumo pontoConsumo = controladorMedidor.inserirDadosAssociacaoPontoConsumoMedidor(dados);
		
		this.atualizarPontoConsumo(pontoConsumo, servicoAutorizacaoTemp);
	}

	private void atualizarPontoConsumo(PontoConsumo pontoConsumo, ServicoAutorizacaoTemp servicoAutorizacaoTemp)
			throws ConcorrenciaException, NegocioException {
		OperacaoMedidor operacaoMedidor = servicoAutorizacaoTemp.getMedidorOperacao();
		
		controladorPontoConsumo.atualizarSaveOrUpdate(pontoConsumo, PontoConsumoImpl.class);

		if (operacaoMedidor != null && (OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO == operacaoMedidor.getChavePrimaria()
				|| OperacaoMedidor.CODIGO_SUBSTITUICAO == operacaoMedidor.getChavePrimaria())) {


			InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
			instalacaoMedidor.setDataAtivacao(servicoAutorizacaoTemp.getDataLeituraNovo());

			controladorInstalacaoMedidor.atualizarSaveOrUpdate(instalacaoMedidor, InstalacaoMedidorImpl.class);

		}

	}

	private Map<String, Object> obterDadosAssociacao(ServicoAutorizacaoTemp servicoAutorizacaoTemp) {
		Map<String, Object> dados = new HashMap<String, Object>();
		PontoConsumo pontoConsumo = servicoAutorizacaoTemp.getServicoAutorizacao().getPontoConsumo();
		Medidor medidorOriginal = servicoAutorizacaoTemp.getMedidorOriginal();
		Medidor medidorNovo = servicoAutorizacaoTemp.getMedidorNovo();

		if (pontoConsumo != null) {
			dados.put("idPontoConsumo", pontoConsumo.getChavePrimaria());
			dados.put("idImovel", pontoConsumo.getImovel().getChavePrimaria());
		}

		dados.put("tipoAssociacao", ControladorMedidor.CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR);

		dados.put("funcionario", servicoAutorizacaoTemp.getFuncionario().getChavePrimaria());

		
		Date dataMedidor = null;
		
		if(servicoAutorizacaoTemp.getDataLeituraOriginal() != null) {
			dataMedidor = servicoAutorizacaoTemp.getDataLeituraOriginal();
		} else {
			dataMedidor = servicoAutorizacaoTemp.getDataLeituraNovo();
		}
		
		dados.put("dataMedidor", dataMedidor);

		if (medidorOriginal != null) {
			dados.put("chaveMedidorAnterior", medidorOriginal.getChavePrimaria());
		}
		dados.put("leituraAnterior", String.valueOf(servicoAutorizacaoTemp.getNumeroLeituraOriginal()));

		if (medidorNovo != null) {
			dados.put("chaveMedidorAtual", medidorNovo.getChavePrimaria());
		}
		dados.put("leituraAtual", String.valueOf(servicoAutorizacaoTemp.getNumeroLeituraNovo()));

		dados.put("idOperacaoMedidor", servicoAutorizacaoTemp.getMedidorOperacao().getChavePrimaria());
		
		if(servicoAutorizacaoTemp.getMotivoOperacaoMedidor() != null) {
			dados.put("medidorMotivoOperacao", servicoAutorizacaoTemp.getMotivoOperacaoMedidor().getChavePrimaria());
		}

		dados.put("instalarCorretorVazao", Boolean.FALSE);

		
		return dados;
	}
	
	
	private Boolean validarMedidores(ServicoAutorizacaoTemp servicoAutorizacaoTemp) {

		OperacaoMedidor operacaoMedidor = servicoAutorizacaoTemp.getMedidorOperacao();

		if (operacaoMedidor == null || servicoAutorizacaoTemp.getMotivoOperacaoMedidor() == null) {
			return Boolean.FALSE;
		}

		if ((OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO == operacaoMedidor.getChavePrimaria()
				|| OperacaoMedidor.CODIGO_INSTALACAO == operacaoMedidor.getChavePrimaria())
				&& (servicoAutorizacaoTemp.getMedidorNovo() == null || servicoAutorizacaoTemp.getNumeroLeituraNovo() == null
						|| !verificarDisponibilidadeMedidor(servicoAutorizacaoTemp))) {
			return Boolean.FALSE;
		} else if (OperacaoMedidor.CODIGO_SUBSTITUICAO == operacaoMedidor.getChavePrimaria()
				&& ((servicoAutorizacaoTemp.getMedidorOriginal() != null
						&& servicoAutorizacaoTemp.getMedidorNovo() != null
						&& servicoAutorizacaoTemp.getMedidorOriginal().getChavePrimaria() == servicoAutorizacaoTemp
								.getMedidorNovo().getChavePrimaria())
						|| servicoAutorizacaoTemp.getMedidorOriginal() == null
						|| servicoAutorizacaoTemp.getNumeroLeituraOriginal() == null
						|| servicoAutorizacaoTemp.getNumeroLeituraNovo() == null
						|| servicoAutorizacaoTemp.getMedidorNovo() == null
						|| !verificarDisponibilidadeMedidor(servicoAutorizacaoTemp))) {
			return Boolean.FALSE;

		} else if (OperacaoMedidor.CODIGO_ATIVACAO == operacaoMedidor.getChavePrimaria()
				&& (servicoAutorizacaoTemp.getMedidorOriginal() == null || servicoAutorizacaoTemp.getNumeroLeituraOriginal() == null)) {
			return Boolean.FALSE;

		} else if ((OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO != operacaoMedidor.getChavePrimaria() && OperacaoMedidor.CODIGO_INSTALACAO != operacaoMedidor.getChavePrimaria()) && (servicoAutorizacaoTemp.getMedidorOriginal() == null || servicoAutorizacaoTemp.getNumeroLeituraOriginal() == null)) {
			return Boolean.FALSE;
		} else if (OperacaoMedidor.CODIGO_REATIVACAO == operacaoMedidor.getChavePrimaria() && servicoAutorizacaoTemp.getServicoAutorizacao().getPontoConsumo() != null) {
			Date dataBloqueio = controladorMedidor.consultarUltimoHistoricoOperacaoPorOperacao(OperacaoMedidor.CODIGO_BLOQUEIO,
					OperacaoMedidor.CODIGO_BLOQUEIO, servicoAutorizacaoTemp.getServicoAutorizacao().getPontoConsumo().getChavePrimaria(), true, null);
			
			if(dataBloqueio == null) {
				return Boolean.FALSE;
			}
		}

		return Boolean.TRUE;
	}

	private Boolean verificarDisponibilidadeMedidor(ServicoAutorizacaoTemp servicoAutorizacaoTemp) {
		if (servicoAutorizacaoTemp.getMedidorNovo() != null) {
			Long codigoMedidorProntoInstalacao = Long.valueOf(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_PRONTO_INSTALAR));

			if (servicoAutorizacaoTemp.getMedidorNovo() != null
					&& servicoAutorizacaoTemp.getMedidorNovo().getSituacaoMedidor() != null
					&& !codigoMedidorProntoInstalacao
							.equals(servicoAutorizacaoTemp.getMedidorNovo().getSituacaoMedidor().getChavePrimaria())) {
				return Boolean.FALSE;
			}
		}
		
		return Boolean.TRUE;
	}
	
	private void setarStatusAutorizacaoServico(ServicoAutorizacaoTemp servicoAutorizacaoTemp, ServicoAutorizacao servicoAutorizacao, String status) {
		
		ConstanteSistema constante = null;
		EntidadeConteudo statusAS = null;
		
		if (status != null && status.equals(Constantes.C_STATUS_SERV_AUT_ENCERRADO)) {
			constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_ENCERRADO);
			statusAS = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		} else if (status != null && status.equals(Constantes.C_STATUS_SERV_AUT_PENDENTE)) {
			constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_PENDENTE);
			statusAS = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		}
		
		servicoAutorizacaoTemp.setStatus(statusAS);
		
	}
	
	public Boolean atualizarContrato(ServicoAutorizacaoTemp servicoAutorizacaoTemp, DadosAuditoria dadosAuditoria) throws GGASException {
		ServicoAutorizacao servicoAutorizacao = servicoAutorizacaoTemp.getServicoAutorizacao();
		Contrato contrato = null;
		ContratoPontoConsumo contratoPontoConsumo = null;

		if (servicoAutorizacao.getContrato() != null) {
			contrato = servicoAutorizacao.getContrato();
			contratoPontoConsumo = contrato.getListaContratoPontoConsumo().iterator().next();
		} else if (servicoAutorizacao.getPontoConsumo() != null) {
			contratoPontoConsumo = controladorContrato
					.obterContratoAtivoPontoConsumo(servicoAutorizacao.getPontoConsumo());

			if (contratoPontoConsumo != null) {
				contrato = contratoPontoConsumo.getContrato();
			}
		} else if (servicoAutorizacao.getCliente() != null) {
			contratoPontoConsumo = controladorContrato
					.obterContratoPontoConsumoAtivoCliente(servicoAutorizacao.getCliente());

			if (contratoPontoConsumo != null) {
				contrato = contratoPontoConsumo.getContrato();
			}
		}
		
		Contrato novo = null;
		try {
			novo = controladorContrato.popularEncerrarContrato(contrato, dadosAuditoria);
		} catch (IllegalAccessException | InvocationTargetException | InstantiationException
				| NoSuchMethodException e) {
			e.printStackTrace();
		}
		
		if(novo == null) {
			return Boolean.FALSE;
		}
		
		ContratoPontoConsumo contratoPontoConsumoNovo = novo.getListaContratoPontoConsumo().iterator().next();

		if (contrato == null) {
			return Boolean.FALSE;
		}

		if (servicoAutorizacaoTemp.getVazaoFornecimentoNova() != null) {
			contratoPontoConsumoNovo
					.setMedidaVazaoMaximaInstantanea(servicoAutorizacaoTemp.getVazaoFornecimentoNova());
		}

		if (servicoAutorizacaoTemp.getPressaoFornecimentoNova() != null) {
			contratoPontoConsumoNovo.setMedidaPressao(servicoAutorizacaoTemp.getPressaoFornecimentoNova());
		}
		
		novo.setDadosAuditoria(dadosAuditoria);
		contrato.setDadosAuditoria(dadosAuditoria);
		
		controladorContrato.atualizarContrato(novo, contrato, true);

		return Boolean.TRUE;

	}
	



}
