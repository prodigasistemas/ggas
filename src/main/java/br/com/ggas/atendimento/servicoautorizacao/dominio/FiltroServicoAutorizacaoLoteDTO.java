package br.com.ggas.atendimento.servicoautorizacao.dominio;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;

public class FiltroServicoAutorizacaoLoteDTO {
	private CityGate cityGate;
	private ServicoTipo servicoTipo;
	private String cepComunicacao;
	private Long quadra;
	private Tronco tronco;
	private Long[] segmentosSelecionados;
	private String numeroMedidor;
	private String dataAquisicaoInicial;
	private String dataAquisicaoFinal;
	private String dataInstalacaoInicial;
	private String dataInstalacaoFinal;
	private SituacaoMedidor situacaoMedidor;
	private Long situacaoConsumo;
	private Boolean tipoMedicao;
	private Long situacaoContrato;
	private RamoAtividade ramoAtividade;
	private ModeloMedidor modeloMedidor;

	public CityGate getCityGate() {
		return cityGate;
	}

	public void setCityGate(CityGate cityGate) {
		this.cityGate = cityGate;
	}

	public Long getQuadra() {
		return quadra;
	}

	public void setQuadra(Long quadra) {
		this.quadra = quadra;
	}

	public Tronco getTronco() {
		return tronco;
	}

	public void setTronco(Tronco tronco) {
		this.tronco = tronco;
	}

	public String getNumeroMedidor() {
		return numeroMedidor;
	}

	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}

	public String getDataAquisicaoInicial() {
		return dataAquisicaoInicial;
	}

	public void setDataAquisicaoInicial(String dataAquisicaoInicial) {
		this.dataAquisicaoInicial = dataAquisicaoInicial;
	}

	public String getDataAquisicaoFinal() {
		return dataAquisicaoFinal;
	}

	public void setDataAquisicaoFinal(String dataAquisicaoFinal) {
		this.dataAquisicaoFinal = dataAquisicaoFinal;
	}

	public String getDataInstalacaoInicial() {
		return dataInstalacaoInicial;
	}

	public void setDataInstalacaoInicial(String dataInstalacaoInicial) {
		this.dataInstalacaoInicial = dataInstalacaoInicial;
	}

	public String getDataInstalacaoFinal() {
		return dataInstalacaoFinal;
	}

	public void setDataInstalacaoFinal(String dataInstalacaoFinal) {
		this.dataInstalacaoFinal = dataInstalacaoFinal;
	}

	public String getCepComunicacao() {
		return cepComunicacao;
	}

	public void setCepComunicacao(String cepComunicacao) {
		this.cepComunicacao = cepComunicacao;
	}

	public boolean isFiltroCityGatePreenchido() {
		return this.cityGate != null;
	}

	public boolean isFiltroCepComunicacaoPreenchido() {
		return this.cepComunicacao != null && !this.cepComunicacao.isEmpty();
	}

	public boolean isFiltroQuadraPreenchido() {
		return this.quadra != null && this.quadra > 0;
	}

	public boolean isFiltroTroncoPreenchido() {
		return this.tronco != null;
	}

	public boolean isFiltroSegmentoPreenchido() {
		return this.segmentosSelecionados != null && this.segmentosSelecionados.length > 0;
	}

	public boolean isFiltroNumeroMedidorPreenchido() {
		return this.numeroMedidor != null && !this.numeroMedidor.isEmpty();
	}

	public boolean isFiltroDataAquisicaoPreenchido() {
		return ((this.dataAquisicaoInicial != null && !this.dataAquisicaoInicial.equals(""))
				&& (this.dataAquisicaoFinal != null && !this.dataAquisicaoFinal.equals("")));
	}

	public boolean isFiltroDataInstalacaoPreenchido() {
		return ((this.dataInstalacaoInicial != null && !this.dataInstalacaoInicial.equals(""))
				&& (this.dataInstalacaoFinal != null && !this.dataInstalacaoFinal.equals("")));
	}

	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}

	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}

	public SituacaoMedidor getSituacaoMedidor() {
		return situacaoMedidor;
	}

	public void setSituacaoMedidor(SituacaoMedidor situacaoMedidor) {
		this.situacaoMedidor = situacaoMedidor;
	}

	public boolean isFiltroSituacaoMedidorPreenchido() {
		return this.situacaoMedidor != null;
	}

	public Long getSituacaoConsumo() {
		return situacaoConsumo;
	}

	public void setSituacaoConsumo(Long situacaoConsumo) {
		this.situacaoConsumo = situacaoConsumo;
	}

	public Boolean getTipoMedicao() {
		return tipoMedicao;
	}

	public void setTipoMedicao(Boolean tipoMedicao) {
		this.tipoMedicao = tipoMedicao;
	}

	public Long[] getSegmentosSelecionados() {
		return segmentosSelecionados;
	}

	public void setSegmentosSelecionados(Long[] segmentosSelecionados) {
		this.segmentosSelecionados = segmentosSelecionados;
	}

	public boolean isFiltroSituacaoConsumo() {
		return this.situacaoConsumo != null && this.situacaoConsumo > 0;
	}

	public boolean isFiltroTipoMedicao() {
		return this.tipoMedicao != null;
	}

	public Long getSituacaoContrato() {
		return situacaoContrato;
	}

	public void setSituacaoContrato(Long situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	public RamoAtividade getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(RamoAtividade ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}

	public ModeloMedidor getModeloMedidor() {
		return modeloMedidor;
	}

	public void setModeloMedidor(ModeloMedidor modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}

	public boolean isFiltroRamoAtividadePreenchido() {
		return this.ramoAtividade != null && this.ramoAtividade.getChavePrimaria() > 0;
	}

	public boolean isFiltroModeloMedidorPreenchido() {
		return this.modeloMedidor != null && this.modeloMedidor.getChavePrimaria() > 0;
	}
}
