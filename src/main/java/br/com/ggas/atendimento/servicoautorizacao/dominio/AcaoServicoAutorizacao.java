package br.com.ggas.atendimento.servicoautorizacao.dominio;

/**
 * Classe responsável por representar uma acao na autorização de serviço
 *
 */
public enum AcaoServicoAutorizacao {
	INSERIR("foi cadastrado"), EXECUTANDO("está em execução"), EXECUTAR("foi executado"), ENCERRAR("foi encerrado"),
	ALTERAR("foi alterado"), AGENDAR("foi agendado");
	
	private String descricao;

	/**
	 * @param descricao
	 */
	private AcaoServicoAutorizacao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
}

