package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.io.Serializable;
import java.math.BigDecimal;

public class IndicadorEmergenciaVO implements Serializable {

	private String mesAno;
	private String horasMaxima;
	private Integer totalRegistros;
	private String descricaoTipoServico;
	private Integer hora1 = 0;
	private Integer hora2 = 0;
	private Integer hora3 = 0;
	private Integer horaMaior1 = 0;
	private Integer horaMaior3 = 0;
	private Integer minutos;
	private String descricaoPontoConsumo;
	private Integer mesAnoAuxiliar;
	private Integer mesMaximo;
	private String tipoInstalacao;
	private String chamadoProtocolo;

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}

	public String getHorasMaxima() {
		return horasMaxima;
	}

	public void setHorasMaxima(String horasMaxima) {
		this.horasMaxima = horasMaxima;
	}

	public Integer getTotalRegistros() {
		return totalRegistros;
	}

	public void setTotalRegistros(Integer totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public String getDescricaoTipoServico() {
		return descricaoTipoServico;
	}

	public void setDescricaoTipoServico(String descricaoTipoServico) {
		this.descricaoTipoServico = descricaoTipoServico;
	}

	public Integer getHora1() {
		return hora1;
	}

	public void setHora1(Integer hora1) {
		this.hora1 = hora1;
	}

	public Integer getHora2() {
		return hora2;
	}

	public void setHora2(Integer hora2) {
		this.hora2 = hora2;
	}

	public Integer getHora3() {
		return hora3;
	}

	public void setHora3(Integer hora3) {
		this.hora3 = hora3;
	}

	public Integer getHoraMaior1() {
		return horaMaior1;
	}

	public void setHoraMaior1(Integer horaMaior1) {
		this.horaMaior1 = horaMaior1;
	}

	public Integer getHoraMaior3() {
		return horaMaior3;
	}

	public void setHoraMaior3(Integer horaMaior3) {
		this.horaMaior3 = horaMaior3;
	}

	public Integer getMinutos() {
		return minutos;
	}

	public void setMinutos(Integer minutos) {
		this.minutos = minutos;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public Integer getMesAnoAuxiliar() {
		return mesAnoAuxiliar;
	}

	public void setMesAnoAuxiliar(Integer mesAnoAuxiliar) {
		this.mesAnoAuxiliar = mesAnoAuxiliar;
	}

	public Integer getMesMaximo() {
		return mesMaximo;
	}

	public void setMesMaximo(Integer mesMaximo) {
		this.mesMaximo = mesMaximo;
	}

	public String getTipoInstalacao() {
		return tipoInstalacao;
	}

	public void setTipoInstalacao(String tipoInstalacao) {
		this.tipoInstalacao = tipoInstalacao;
	}

	public String getChamadoProtocolo() {
		return chamadoProtocolo;
	}

	public void setChamadoProtocolo(String chamadoProtocolo) {
		this.chamadoProtocolo = chamadoProtocolo;
	}
}
