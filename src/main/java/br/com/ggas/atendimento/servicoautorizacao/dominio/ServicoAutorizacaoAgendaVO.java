/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *05/02/2014
 * vpessoa
 * 10:26:43
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Classe ServicoAutorizacaoAgendaVO
 */
public class ServicoAutorizacaoAgendaVO {

	private Long turnoManha;

	private Long turnoTarde;

	private Long turnoNoite;
	
	private Date data;

	/**
	 * Instantiates a new servico autorizacao agenda vo.
	 */
	public ServicoAutorizacaoAgendaVO() {

		super();
		this.turnoManha = 0l;
		this.turnoTarde = 0l;
		this.turnoNoite = 0l;
		
	}

	/**
	 * Instantiates a new servico autorizacao agenda vo.
	 * 
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @param dia
	 *            the dia
	 * @param total
	 *            the total
	 * @param disponiveis
	 *            the disponiveis
	 */
	public ServicoAutorizacaoAgendaVO(int ano, int mes, int dia, Long turnoManha, Long turnoTarde, Long turnoNoite) {

		super();
		Calendar cal = Calendar.getInstance();
		cal.set(ano, mes, dia);
		this.data = cal.getTime();
		this.turnoManha = turnoManha;
		this.turnoTarde = turnoTarde;
		this.turnoNoite = turnoNoite;
	}

	/**
	 * Instantiates a new servico autorizacao agenda vo.
	 * 
	 * @param agendados
	 *            the agendados
	 * @param total
	 *            the total
	 * @param disponiveis
	 *            the disponiveis
	 * @param data
	 *            the data
	 * @throws ParseException
	 *             the parse exception
	 */
	public ServicoAutorizacaoAgendaVO(Long turnoManha, Long turnoTarde, Long turnoNoite, String data) throws ParseException {

		super();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		this.turnoNoite = turnoNoite;
		this.turnoManha = turnoManha;
		this.turnoTarde = turnoTarde;
		this.data = formatter.parse(data);
	}

	public Long getTurnoNoite() {

		return turnoNoite;
	}

	public void setTurnoNoite(Long turnoNoite) {

		this.turnoNoite = turnoNoite;
	}

	public Long getTurnoManha() {

		return turnoManha;
	}

	public void setTurnoManha(Long turnoManha) {

		this.turnoManha = turnoManha;
	}

	public Long getTurnoTarde() {

		return turnoTarde;
	}

	public void setTurnoTarde(Long turnoTarde) {

		this.turnoTarde = turnoTarde;
	}

	public Date getData() {
		Date dataTmp = null;
		if(data != null){
			dataTmp = (Date) data.clone();
		}		
		return dataTmp;
	}

	public void setData(Date data) {
		if(data != null){
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	public int getDataFormatada() {

		if(data != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(data);
			return cal.get(Calendar.DAY_OF_MONTH);
		} else {
			return 0;
		}
	}

	public Boolean getHabilitado() {

		Calendar dataAtual = Calendar.getInstance();
		Calendar dat = Calendar.getInstance();
		dataAtual.set(Calendar.DAY_OF_MONTH, dataAtual.get(Calendar.DAY_OF_MONTH) - 1);
		dataAtual.set(Calendar.HOUR_OF_DAY, 0);
		dataAtual.set(Calendar.MINUTE, 0);
		dataAtual.set(Calendar.SECOND, 0);
		dataAtual.set(Calendar.MILLISECOND, 0);
		if(this.data != null) {
			dat.setTime(this.data);
			dat.set(Calendar.HOUR_OF_DAY, 0);
			dat.set(Calendar.MINUTE, 0);
			dat.set(Calendar.SECOND, 0);
			dat.set(Calendar.MILLISECOND, 0);
			if(dat.after(dataAtual) && (turnoManha != null && turnoManha > 0) && turnoTarde > 0) {
				return Boolean.TRUE;
			} else {
				return Boolean.FALSE;
			}
		} else {
			return Boolean.FALSE;
		}
	}

}
