/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/11/2013 14:47:06
 @author mroberto
 */

package br.com.ggas.atendimento.servicoautorizacao.dominio;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pelo representação do histórico da autorização de serviços
 */
public class ServicoAutorizacaoHistorico extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -9186684718996605388L;

	private Date dataPrevisaoEncerramento;

	private Equipe equipe;

	private ServicoAutorizacao servicoAutorizacao;

	private EntidadeConteudo operacao;

	private Usuario usuarioSistema;

	private EntidadeConteudo status;

	private String descricao;

	private Collection<ServicoAutorizacaoHistoricoAnexo> anexos = new HashSet<>();

	private Collection<QuestionarioPerguntaResposta> respostasQuestionario = new HashSet<>();
	
	private ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento;

	/**
	 * Retorna descricao
	 * 
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * Atribui descricao
	 * 
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * Retorna dataPrevisaoEncerramento
	 * 
	 * @return dataPrevisaoEncerramento
	 */
	public Date getDataPrevisaoEncerramento() {
		Date dataPrevisaoEncerramentoTmp = null;
		if(dataPrevisaoEncerramento != null){
			dataPrevisaoEncerramentoTmp = (Date) dataPrevisaoEncerramento.clone();
		} 
		return dataPrevisaoEncerramentoTmp;
	}

	/**
	 * Atribui dataPrevisaoEncerramento
	 * 
	 * @param dataPrevisaoEncerramento
	 */
	public void setDataPrevisaoEncerramento(Date dataPrevisaoEncerramento) {
		if(dataPrevisaoEncerramento != null){
			this.dataPrevisaoEncerramento = (Date) dataPrevisaoEncerramento.clone();
		} else {
			this.dataPrevisaoEncerramento = null;
		}
	}

	/**
	 * Retorna equipe
	 * 
	 * @return equipe
	 */
	public Equipe getEquipe() {

		return equipe;
	}

	/**
	 * Atribui equipe
	 * 
	 * @param equipe
	 */
	public void setEquipe(Equipe equipe) {

		this.equipe = equipe;
	}

	/**
	 * Retorna servicoAutorizacao
	 * 
	 * @return servicoAutorizacao
	 */
	public ServicoAutorizacao getServicoAutorizacao() {

		return servicoAutorizacao;
	}

	/**
	 * Atribui servicoAutorizacao
	 * 
	 * @param servicoAutorizacao
	 */
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {

		this.servicoAutorizacao = servicoAutorizacao;
	}

	/**
	 * Retorna operacao
	 * 
	 * @return operacao
	 */
	public EntidadeConteudo getOperacao() {

		return operacao;
	}

	/**
	 * Atribui operacao
	 * 
	 * @param operacao
	 */
	public void setOperacao(EntidadeConteudo operacao) {

		this.operacao = operacao;
	}

	/**
	 * Retorna usuarioSistema
	 * 
	 * @return usuarioSistema
	 */
	public Usuario getUsuarioSistema() {

		return usuarioSistema;
	}

	/**
	 * Atribui usuarioSistema
	 * 
	 * @param usuarioSistema
	 */
	public void setUsuarioSistema(Usuario usuarioSistema) {

		this.usuarioSistema = usuarioSistema;
	}

	/**
	 * Retorna status
	 * 
	 * @return status
	 */
	public EntidadeConteudo getStatus() {

		return status;
	}

	/**
	 * Atribui status
	 * 
	 * @param status
	 */
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/**
	 * @return the anexos
	 */
	public Collection<ServicoAutorizacaoHistoricoAnexo> getAnexos() {
		return anexos;
	}

	/**
	 * @param anexos the anexos to set
	 */
	public void setAnexos(Collection<ServicoAutorizacaoHistoricoAnexo> anexos) {
		this.anexos = anexos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/**
	 * Retorna respostasQuestionario
	 * 
	 * @return respostasQuestionario
	 */
	public Collection<QuestionarioPerguntaResposta> getRespostasQuestionario() {
		return respostasQuestionario;
	}

	/**
	 * Atribui respostasQuestionario
	 * 
	 * @param respostasQuestionario
	 */
	public void setRespostasQuestionario(Collection<QuestionarioPerguntaResposta> respostasQuestionario) {
		this.respostasQuestionario = respostasQuestionario;
	}

	/**
	 * Retorna servicoAutorizacaoMotivoEncerramento
	 * 
	 * @return servicoAutorizacaoMotivoEncerramento
	 */
	public ServicoAutorizacaoMotivoEncerramento getServicoAutorizacaoMotivoEncerramento() {
		return servicoAutorizacaoMotivoEncerramento;
	}

	/**
	 * Atribui servicoAutorizacaoMotivoEncerramento
	 * 
	 * @param servicoAutorizacaoMotivoEncerramento
	 */
	public void setServicoAutorizacaoMotivoEncerramento(
			ServicoAutorizacaoMotivoEncerramento servicoAutorizacaoMotivoEncerramento) {
		this.servicoAutorizacaoMotivoEncerramento = servicoAutorizacaoMotivoEncerramento;
	}

}
