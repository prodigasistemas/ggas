
package br.com.ggas.atendimento.equipe.negocio;

import java.util.Collection;
import java.util.List;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ControladorEquipe
 * 
 * @author Procenge
 */
public interface ControladorEquipe {

	/**
	 * Inserir.
	 * 
	 * @param equipe the equipe
	 * @return the equipe
	 * @throws NegocioException the negocio exception
	 */
	Equipe inserir(Equipe equipe) throws NegocioException;

	/**
	 * Listar.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EntidadeNegocio> listar() throws NegocioException;

	/**
	 * Consultar equipe.
	 * 
	 * @param equipe the equipe
	 * @param habilitado the habilitado
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Equipe> consultarEquipe(Equipe equipe, Boolean habilitado) throws NegocioException;

	/**
	 * Obter equipe.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the equipe
	 * @throws NegocioException the negocio exception
	 */
	Equipe obterEquipe(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir equipe componente.
	 * 
	 * @param listaComponentesEquipe the lista componentes equipe
	 * @param equipe the equipe
	 * @throws NegocioException the negocio exception
	 */
	void inserirEquipeComponente(List<EquipeComponente> listaComponentesEquipe, Equipe equipe) throws NegocioException;

	/**
	 * Remover equipe componente.
	 * 
	 * @param listaComponentesRemover the lista componentes remover
	 * @throws NegocioException the negocio exception
	 */
	void removerEquipeComponente(List<EquipeComponente> listaComponentesRemover) throws NegocioException;

	/**
	 * Listar equipe componente.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EquipeComponente> listarEquipeComponente(long chavePrimaria) throws NegocioException;

	/**
	 * Verificar componentes duplicados.
	 * 
	 * @param listaComponentes the lista componentes
	 * @param funcionario the funcionario
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	boolean verificarComponentesDuplicados(List<EquipeComponente> listaComponentes, Funcionario funcionario) throws NegocioException;

	/**
	 * Remover equipe.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	void removerEquipe(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Verificar equipe existente.
	 * 
	 * @param equipe the equipe
	 * @throws NegocioException the negocio exception
	 */
	void verificarEquipeExistente(Equipe equipe) throws NegocioException;

	/**
	 * Atualizar equipe.
	 * 
	 * @param equipe the equipe
	 * @throws NegocioException the negocio exception
	 * @throws GGASException the GGASException exception
	 */
	void atualizarEquipe(Equipe equipe) throws GGASException;

	/**
	 * Verificar existencia componente responsavel.
	 * 
	 * @param listaComponentes the lista componentes
	 * @throws NegocioException the negocio exception
	 */
	void verificarExistenciaComponenteResponsavel(List<EquipeComponente> listaComponentes) throws NegocioException;

	/**
	 * Listar equipe.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Equipe> listarEquipe() throws NegocioException;

	Equipe consultarEquipePorDescricao(String descricao) throws NegocioException;

	Collection<EquipeComponente> listarTodasEquipeComponente() throws NegocioException;

}
