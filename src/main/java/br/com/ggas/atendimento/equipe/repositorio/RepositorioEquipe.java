
package br.com.ggas.atendimento.equipe.repositorio;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Util;

/**
 * Classe RepositorioEquipe
 * 
 * @author Procenge
 */
@Repository
public class RepositorioEquipe extends RepositorioGenerico {

	private static final String FROM = " from ";
	private static final String WHERE = " where ";
	private static final String UNIDADE_ORGANIZACIONAL = "unidadeOrganizacional";

	/**
	 * Instantiates a new repositorio equipe.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioEquipe(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Equipe();
	}

	@Override
	public Class<Equipe> getClasseEntidade() {

		return Equipe.class;
	}

	/**
	 * Obter equipe por nome.
	 * 
	 * @param nome
	 *            the nome
	 * @return the equipe
	 */
	public Equipe obterEquipePorNome(String nome) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" equipe ");
		hql.append(WHERE);

		hql.append(" equipe.nome = :nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("nome", nome);

		return (Equipe) query.uniqueResult();
	}

	/**
	 * Consultar equipe.
	 * 
	 * @param equipe
	 *            the equipe
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Equipe> consultarEquipe(Equipe equipe, Boolean habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(UNIDADE_ORGANIZACIONAL, FetchMode.JOIN);

		String nome = equipe.getNome();
		if(nome != null && !StringUtils.isEmpty(nome)) {
			criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(nome)));
		}
		UnidadeOrganizacional unidadeOrganizacional = equipe.getUnidadeOrganizacional();
		if(unidadeOrganizacional != null) {
			criteria.createAlias(UNIDADE_ORGANIZACIONAL, UNIDADE_ORGANIZACIONAL);
			criteria.add(Restrictions.eq("unidadeOrganizacional.chavePrimaria", unidadeOrganizacional.getChavePrimaria()));
		}
		if(habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		return criteria.list();

	}

	/**
	 * Obter equipe.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the equipe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Equipe obterEquipe(Long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" equipe ");
		hql.append(" inner join fetch equipe.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(WHERE);
		hql.append(" equipe.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return (Equipe) query.uniqueResult();
	}

	/**
	 * Listar equipe componente.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<EquipeComponente> listarEquipeComponente(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(EquipeComponente.class.getSimpleName());
		hql.append(" equipeComponente ");
		hql.append(" inner join fetch equipeComponente.equipe equipe ");
		hql.append(" inner join fetch equipeComponente.funcionario funcionario ");
		hql.append(WHERE);
		hql.append(" equipeComponente.equipe.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return query.list();
	}

	/**
	 * Listar equipe.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<Equipe> listarEquipe() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());
		
		criteria.add(Restrictions.eq("habilitado", true));

		criteria.addOrder(Order.asc("nome"));

		return criteria.list();
	}
	
	
	public Equipe consultarEquipePorDescricao(String descricao) throws NegocioException {
		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" equipe ");
		hql.append(WHERE);
		hql.append(" equipe.nome = :descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("descricao", descricao);
		query.setMaxResults(1);

		return (Equipe) query.uniqueResult();
	}

	public Collection<EquipeComponente> listarTodasEquipeComponente() {
		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(EquipeComponente.class.getSimpleName());
		hql.append(" equipeComponente ");
		hql.append(" inner join fetch equipeComponente.equipe equipe ");
		hql.append(" inner join fetch equipeComponente.funcionario funcionario ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

}
