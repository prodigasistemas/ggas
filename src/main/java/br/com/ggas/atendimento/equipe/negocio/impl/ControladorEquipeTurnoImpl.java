package br.com.ggas.atendimento.equipe.negocio.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.GregorianCalendar;
import com.ibm.icu.util.TimeZone;

import br.com.ggas.atendimento.equipe.EquipeTurno;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.FiltroEquipeTurnoDTO;
import br.com.ggas.atendimento.equipe.dominio.FuncionarioEquipeVO;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipeTurno;
import br.com.ggas.atendimento.equipe.repositorio.RepositorioEquipe;
import br.com.ggas.atendimento.equipe.repositorio.RepositorioEquipeTurno;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.repositorio.RepositorioServicoAutorizacao;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.FuncionarioAfastamento;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

@Service("controladorEquipeTurno")
@Transactional
public class ControladorEquipeTurnoImpl implements ControladorEquipeTurno{

	
	@Autowired
	private RepositorioEquipeTurno repositorioEquipeTurno;
	
	@Autowired
	ControladorEquipe controladorEquipe;
	
	@Autowired
	ControladorFeriado controladorFeriado;
	
	@Autowired
	private RepositorioEquipe repositorioEquipe;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EquipeTurnoItem.BEAN_ID_EQUIPE_TURNO_ITEM);
	}
	
	
	public EntidadeNegocio criarEquipeTurno() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EquipeTurno.BEAN_ID_EQUIPE_TURNO);
	}

	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(EquipeTurno.BEAN_ID_EQUIPE_TURNO);
	}
	
	public Class<?> getClasseEntidadeEquipeTurnoItem() {
		return ServiceLocator.getInstancia().getClassPorID(EquipeTurnoItem.BEAN_ID_EQUIPE_TURNO_ITEM);
	}
	

	@Override
	public List<EquipeTurnoItem> gerarEquipeTurno(FiltroEquipeTurnoDTO filtroEquipeTurno, Integer[] diasSemana) throws NegocioException {

		
		List<EquipeTurnoItem> listaEquipeTurno = new ArrayList<EquipeTurnoItem>();
		List<Integer> listaDiasSemana = Arrays.asList(diasSemana);
		Date dataInicio = DataUtil.converterParaData(filtroEquipeTurno.getDataInicio());
		Date dataFim = DataUtil.converterParaData(filtroEquipeTurno.getDataFim());
		
		Collection<EquipeTurnoItem> listaEscalaExistente = this.consultarEscalaExistente(filtroEquipeTurno.getDataInicio(),
				filtroEquipeTurno.getDataFim(), filtroEquipeTurno.getFuncionarios());
		
		for (int indice = 0; indice < filtroEquipeTurno.getHoraInicio().length; indice++) {

			String[] horaInicio = filtroEquipeTurno.getHoraInicio()[indice].split(":");
			String[] horaFim = filtroEquipeTurno.getHoraFim()[indice].split(":");
			GregorianCalendar dataReferencia = new GregorianCalendar();
			dataReferencia.setTime(dataInicio);
			dataReferencia.set(Calendar.SECOND, 0);
			EntidadeConteudo turno = calcularTurno(Integer.valueOf(horaInicio[0]), Integer.valueOf(horaFim[0]));

			Municipio municipio = filtroEquipeTurno.getFuncionarios()[0].getEmpresa().getCliente()
					.getEnderecoPrincipal().getMunicipio();

			Funcionario[] funcionarios = filtroEquipeTurno.getFuncionarios();

			dataReferencia.setTime(calcularProximoDia(listaDiasSemana, dataReferencia, municipio,
					filtroEquipeTurno.getIsConsiderarFeriado()));
			

			while (!DataUtil.maiorQue(dataReferencia.getTime(), dataFim, Boolean.TRUE)) {
				int i = 0;

				for (Funcionario funcionario : funcionarios) {
					EquipeTurnoItem equipeTurno = (EquipeTurnoItem) this.criar();
					equipeTurno.setFuncionario(funcionario);
					equipeTurno.setEquipe(filtroEquipeTurno.getEquipe());
					equipeTurno.setTurno(turno);
					equipeTurno.setVeiculo(filtroEquipeTurno.getVeiculo()[i]);

					equipeTurno.setDataInicio(
							Util.adicionarHoraMinutoData(dataReferencia.getTime(), horaInicio[0], horaInicio[1]));

					equipeTurno
							.setDataFim(Util.adicionarHoraMinutoData(dataReferencia.getTime(), horaFim[0], horaFim[1]));

					if (!verificarAfastamentoFuncionario(funcionario.getListaAfastamento(),
							equipeTurno.getDataInicio()) && !validarEscalaExistente(equipeTurno, listaEscalaExistente)) {
						listaEquipeTurno.add(equipeTurno);
					}

					i++;
				}

				funcionarios = rotacionarFuncionarios(funcionarios);
				dataReferencia.set(Calendar.DAY_OF_YEAR, dataReferencia.get(Calendar.DAY_OF_YEAR) + 1);
				dataReferencia.setTime(calcularProximoDia(listaDiasSemana, dataReferencia, municipio,
						filtroEquipeTurno.getIsConsiderarFeriado()));

			}
		}

		return listaEquipeTurno;
	}
	
	private Boolean validarEscalaExistente(EquipeTurnoItem equipeTurno, Collection<EquipeTurnoItem> listaEquipeTurno) {
		return listaEquipeTurno.stream()
				.anyMatch(p -> Util.compararDatas(p.getDataInicio(), equipeTurno.getDataInicio()) == 0
						&& p.getTurno().getChavePrimaria() == equipeTurno.getTurno().getChavePrimaria()
						&& p.getFuncionario().getChavePrimaria() == equipeTurno.getFuncionario().getChavePrimaria());
	}


	private EntidadeConteudo calcularTurno(Integer horaInicio, Integer horaFim) throws NegocioException {
		
		Collection<EntidadeConteudo> listaTurnos = controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno");
		String turno = "";
		
		
		if(horaInicio >= 7 && horaInicio <= 12 && horaFim >= 7 && horaFim <=12) {
			 turno = "MANHÃ";
		} else if(horaInicio >= 13 && horaInicio <= 18 && horaFim >= 13 && horaFim <=18){
			turno = "TARDE";
		} else {
			turno = "NOITE";
		}
		
		String turnoStream = turno;
		
		return listaTurnos.stream().filter(p -> turnoStream.equals(p.getDescricao())).findFirst().orElse(null);
	}


	private Funcionario[] rotacionarFuncionarios(Funcionario[] funcionarios) {

		int i = funcionarios.length;
		int j = 1;

		Funcionario[] retorno = new Funcionario[i];

		for (Funcionario funcionario : funcionarios) {
			if (j == i) {
				retorno[0] = funcionario;
			} else {
				retorno[j] = funcionario;
				j++;
			}
		}

		return retorno;
	}


	private Date calcularProximoDia(List<Integer> listaDiasSemana, Calendar dataReferencia, Municipio municipio,
			Boolean isConsiderarFeriado) throws NegocioException {
		if (!listaDiasSemana.contains(dataReferencia.get(Calendar.DAY_OF_WEEK) - 1)
				|| (isConsiderarFeriado && !controladorFeriado.isDiaUtil(dataReferencia.getTime(),
						municipio.getChavePrimaria(), Boolean.FALSE))) {
			do {
				dataReferencia.set(Calendar.DAY_OF_YEAR, dataReferencia.get(Calendar.DAY_OF_YEAR) + 1);
			} while (!listaDiasSemana.contains(dataReferencia.get(Calendar.DAY_OF_WEEK) - 1)
					|| (isConsiderarFeriado && !controladorFeriado.isDiaUtil(dataReferencia.getTime(),
							municipio.getChavePrimaria(), Boolean.FALSE)));
		}

		return dataReferencia.getTime();
	}
	
	@Transactional
	@Override
	public Boolean salvarEquipeTurno(List<EquipeTurnoItem> listaEquipeTurno, FiltroEquipeTurnoDTO filtro) throws NegocioException {
		
		
		Date dataMinima = listaEquipeTurno.stream().map(EquipeTurnoItem::getDataInicio).min(Date::compareTo).get();
		Date dataMaxima = listaEquipeTurno.stream().map(EquipeTurnoItem::getDataInicio).max(Date::compareTo).get();

		EquipeTurno equipeTurno = (EquipeTurno) this.criarEquipeTurno();
		equipeTurno.setEquipe(filtro.getEquipe());
		equipeTurno.setDataInicio(DataUtil.gerarDataHmsZerados(dataMinima));
		equipeTurno.setDataFim(DataUtil.gerarDataHmsZerados(dataMaxima));
		equipeTurno.setIsEscalaSobreaviso(filtro.getIsEscalaSobreaviso());

		
		long chavePrimaria = repositorioEquipe.inserir(equipeTurno);
		equipeTurno.setChavePrimaria(chavePrimaria);
		
		for(EquipeTurnoItem equipeTurnoItem : listaEquipeTurno) {
			
			equipeTurnoItem.setEquipeTurno(equipeTurno);
			repositorioEquipe.inserir(equipeTurnoItem);
		}
		
		
		return Boolean.TRUE;
	}


	@Override
	public void atualizarEquipeTurnoItem(EquipeTurnoItem equipeTurnoItem) throws ConcorrenciaException, NegocioException {
		repositorioEquipe.atualizar(equipeTurnoItem, getClasseEntidadeEquipeTurnoItem());
	}
	
	private Boolean verificarAfastamentoFuncionario(Collection<FuncionarioAfastamento> listaAfastamento,
			Date dataEscala) {

		for (FuncionarioAfastamento funcionarioAfastamento : listaAfastamento) {
			if (funcionarioAfastamento.getDataFimAfastamento() == null
					&& DataUtil.maiorOuIgualQue(dataEscala, funcionarioAfastamento.getDataInicioAfastamento(), true)) {
				return Boolean.TRUE;
			} else if (funcionarioAfastamento.getDataFimAfastamento() != null && DataUtil.maiorOuIgualQue(funcionarioAfastamento.getDataInicioAfastamento(), dataEscala, true)
					&& DataUtil.menorOuIgualQue(dataEscala, funcionarioAfastamento.getDataFimAfastamento(), true)) {
				return Boolean.TRUE;
			}
		}

		return Boolean.FALSE;
	}


	@Override
	public Boolean removerDiaEscala(Collection<EquipeTurnoItem> listaEquipeTurnoItem, Long[] chavesPrimarias) throws NegocioException {
		List<Long> chavesPrimariasList = Arrays.asList(chavesPrimarias);
		Collection<EquipeTurnoItem> listaEquipeTurnoItemRemocao = listaEquipeTurnoItem.stream().filter(p -> chavesPrimariasList.contains(p.getChavePrimaria())).collect(Collectors.toList());
		
		if (listaEquipeTurnoItemRemocao.stream().filter(p -> DataUtil.menorOuIgualQue(p.getDataInicio(),
				DataUtil.minimizarHorario(new Date()), Boolean.TRUE)).findAny().isPresent()) {
			return Boolean.FALSE;
		}
		
		listaEquipeTurnoItem.removeAll(listaEquipeTurnoItemRemocao);
		
		for(EquipeTurnoItem equipeTurnoItem : listaEquipeTurnoItemRemocao) {
			repositorioEquipeTurno.remover(equipeTurnoItem);
		}
		
		
		return Boolean.TRUE;
	}


	@Transactional
	@Override
	public Boolean alterarEscalaLote(Collection<EquipeTurnoItem> listaEquipeTurnoItem, Long idEquipeTurnoItem,
			Funcionario funcionarioLote, String horarioInicioLote, String horarioFimLote, Veiculo veiculoLote) throws ConcorrenciaException, NegocioException {
		EquipeTurnoItem equipeTurnoItem = listaEquipeTurnoItem.stream()
				.filter(p -> p.getChavePrimaria() == idEquipeTurnoItem).findFirst().orElse(null);

		if (equipeTurnoItem == null) {
			return Boolean.FALSE;
		}

		Collection<EquipeTurnoItem> listaAlteracaoLoteEscala = listaEquipeTurnoItem.stream()
				.filter(p -> DataUtil.maiorOuIgualQue(
						DataUtil.minimizarHorario(equipeTurnoItem.getDataInicio()),p.getDataInicio(), Boolean.TRUE) && p.getTurno().getChavePrimaria() == equipeTurnoItem.getTurno().getChavePrimaria())
				.collect(Collectors.toList());
		
		
		for(EquipeTurnoItem equipeTurnoItemAlteracao : listaAlteracaoLoteEscala) {
			
			String[] horaInicio = horarioInicioLote.split(":");
			String[] horaFim = horarioFimLote.split(":");

			equipeTurnoItemAlteracao.setFuncionario(funcionarioLote);
			equipeTurnoItemAlteracao.setVeiculo(veiculoLote);
			equipeTurnoItemAlteracao.setDataInicio(Util.adicionarHoraMinutoData(equipeTurnoItemAlteracao.getDataInicio(), horaInicio[0], horaInicio[1]));
			equipeTurnoItemAlteracao.setDataFim(Util.adicionarHoraMinutoData(equipeTurnoItemAlteracao.getDataFim(), horaFim[0], horaFim[1]));
			
			repositorioEquipeTurno.atualizar(equipeTurnoItemAlteracao, equipeTurnoItem.getClass());
		}
		
		listaEquipeTurnoItem.addAll(listaAlteracaoLoteEscala);

		return Boolean.TRUE;
	}


	@Override
	public Collection<EquipeTurno> consultarEquipeTurno(FiltroEquipeTurnoDTO filtro)
			throws HibernateException, NegocioException {
		return repositorioEquipeTurno.consultarEquipeTurno(filtro);
	}


	@Override
	public EquipeTurno obterEquipeTurno(Long idChavePrimaria) {
		return repositorioEquipeTurno.obterEquipeTurno(idChavePrimaria);
	}


	@Override
	public EquipeTurnoItem obterEquipeTurnoItem(Long idChavePrimaria) {
		return repositorioEquipeTurno.obterEquipeTurnoItem(idChavePrimaria);
	}


	@Override
	public Collection<EquipeTurnoItem> obterListaEquipeTurnoItem(int mes, int ano, Long chaveEquipe) {
		return repositorioEquipeTurno.obterListaEquipeTurnoItem(mes, ano, chaveEquipe);
	}


	@Override
	public Collection<EquipeTurnoItem> consultarEscalaExistente(String dataInicio, String dataFim, Funcionario[] funcionarios)
			throws HibernateException, NegocioException {
		return repositorioEquipeTurno.consultarEscalaExistente(dataInicio, dataFim, funcionarios);
	}

	@Override 
	public List<FuncionarioEquipeVO> obterEquipeTurnoItemPorChaveEquipe(Long chaveEquipe) {
		return repositorioEquipeTurno.obterEquipeTurnoItemPorChaveEquipe(chaveEquipe);
	}

}
