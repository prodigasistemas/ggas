
package br.com.ggas.atendimento.equipe.negocio.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.repositorio.RepositorioEquipe;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.atendimento.servicoautorizacao.repositorio.RepositorioServicoAutorizacao;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;

/**
 * Classe ControladorEquipeImpl
 * 
 * @author Procenge
 */
@Service("controladorEquipe")
@Transactional
public class ControladorEquipeImpl implements ControladorEquipe {

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	@Autowired
	private RepositorioEquipe repositorioEquipe;

	@Autowired
	private RepositorioServicoAutorizacao repositorioServicoAutorizacao;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#inserir(br.com.ggas.atendimento.equipe.dominio.Equipe)
	 */
	@Transactional
	@Override
	public Equipe inserir(Equipe equipe) throws NegocioException {

		repositorioEquipe.inserir(equipe);

		return equipe;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#listar()
	 */
	@Transactional
	@Override
	public Collection<EntidadeNegocio> listar() throws NegocioException {

		return repositorioEquipe.obterTodas();
	}

	public ControladorCliente getControladorCliente() {

		return controladorCliente;
	}

	public void setControladorCliente(ControladorCliente controladorCliente) {

		this.controladorCliente = controladorCliente;
	}

	public RepositorioEquipe getRepositorio() {

		return repositorioEquipe;
	}

	public void setRepositorio(RepositorioEquipe repositorio) {

		this.repositorioEquipe = repositorio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#consultarEquipe(br.com.ggas.atendimento.equipe.dominio.Equipe,
	 * java.lang.Boolean)
	 */
	@Override
	public Collection<Equipe> consultarEquipe(Equipe equipe, Boolean habilitado) throws NegocioException {

		return repositorioEquipe.consultarEquipe(equipe, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#obterEquipe(java.lang.Long)
	 */
	@Override
	public Equipe obterEquipe(Long chavePrimaria) throws NegocioException {

		return repositorioEquipe.obterEquipe(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#inserirEquipeComponente(java.util.List,
	 * br.com.ggas.atendimento.equipe.dominio.Equipe)
	 */
	@Override
	public void inserirEquipeComponente(List<EquipeComponente> listaComponentesEquipe, Equipe equipe) throws NegocioException {

		List<EquipeComponente> listaEquipeComponenteOriginal = (List<EquipeComponente>) repositorioEquipe.listarEquipeComponente(equipe
						.getChavePrimaria());

		this.removerEquipeComponente(listaEquipeComponenteOriginal);
		for (EquipeComponente equipeComponente : listaComponentesEquipe) {
			equipeComponente.setEquipe(equipe);
			repositorioEquipe.inserir(equipeComponente);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#removerEquipeComponente(java.util.List)
	 */
	@Override
	public void removerEquipeComponente(List<EquipeComponente> listaComponentesRemover) throws NegocioException {

		for (EquipeComponente equipeComponente : listaComponentesRemover) {
			repositorioEquipe.remover(equipeComponente, EquipeComponente.class.getClass(), true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#listarEquipeComponente(long)
	 */
	@Override
	public Collection<EquipeComponente> listarEquipeComponente(long chavePrimaria) throws NegocioException {

		return repositorioEquipe.listarEquipeComponente(chavePrimaria);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#verificarComponentesDuplicados(java.util.List,
	 * br.com.ggas.cadastro.funcionario.Funcionario)
	 */
	@Override
	public boolean verificarComponentesDuplicados(List<EquipeComponente> listaComponentes, Funcionario funcionario) throws NegocioException {

		for (EquipeComponente equipeComponente : listaComponentes) {
			if(equipeComponente.getFuncionario().getChavePrimaria() == (funcionario.getChavePrimaria())) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Verificar existencia de algum componente.
	 * 
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarExistenciaDeAlgumComponente() throws NegocioException {
		//Método vazio.
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#verificarExistenciaComponenteResponsavel(java.util.List)
	 */
	@Override
	public void verificarExistenciaComponenteResponsavel(List<EquipeComponente> listaComponentes) throws NegocioException {

		int cont = 0;
		if(listaComponentes != null) {
			for (EquipeComponente componente : listaComponentes) {
				if(!componente.isIndicadorResponsavel()) {
					cont++;
				}
			}
			if(cont == listaComponentes.size()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_EQUIPE_RESPONSAVEL);
			}
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_EQUIPE_SEM_COMPONENTES);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#verificarEquipeExistente(br.com.ggas.atendimento.equipe.dominio.Equipe)
	 */
	@Override
	public void verificarEquipeExistente(Equipe equipe) throws NegocioException {

		Equipe equipeTemporario = (Equipe) repositorioEquipe.criar();
		equipeTemporario.setChavePrimaria(equipe.getChavePrimaria());
		equipeTemporario.setNome(equipe.getNome());
		equipe.isHabilitado();
		Long chavePrimaria = equipeTemporario.getChavePrimaria();

		Equipe equipeConsulta = repositorioEquipe.obterEquipePorNome(equipe.getNome());

		if(equipeConsulta != null && equipeConsulta.getChavePrimaria() != chavePrimaria) {
			
			throw new NegocioException(Constantes.ERRO_NEGOCIO_EQUIPE_EXISTENTE, equipeTemporario.getNome());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#removerEquipe(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerEquipe(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		validarRemocao(chavesPrimarias);

		List<EquipeComponente> listaComponentes = null;

		for (Long chave : chavesPrimarias) {
			if(chave != null) {
				listaComponentes = (List<EquipeComponente>) repositorioEquipe.listarEquipeComponente(chave);
			}

			if(listaComponentes != null) {
				for (EquipeComponente equipeComponente : listaComponentes) {
					repositorioEquipe.remover(equipeComponente, EquipeComponente.class.getClass());
				}
			}

		}
		repositorioEquipe.remover(chavesPrimarias, dadosAuditoria);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#atualizarEquipe(br.com.ggas.atendimento.equipe.dominio.Equipe)
	 */
	@Override
	public void atualizarEquipe(Equipe equipe) throws NegocioException, ConcorrenciaException {

		repositorioEquipe.atualizar(equipe);
	}

	/**
	 * Validar remocao.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarRemocao(Long[] chavesPrimarias) throws NegocioException {

		for (Long chave : chavesPrimarias) {
			if(chave != null && repositorioServicoAutorizacao.obterEquipeServicoAutorizacao(chave)) {
				
				throw new NegocioException(Constantes.ERRO_NEGOCIO_EQUIPE_RELACIONADA);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#listarEquipe()
	 */
	@Override
	public Collection<Equipe> listarEquipe() throws NegocioException {

		return repositorioEquipe.listarEquipe();
	}
	
	@Override
	public Equipe consultarEquipePorDescricao(String descricao) throws NegocioException {
		return repositorioEquipe.consultarEquipePorDescricao(descricao);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.negocio.ControladorEquipe#listarEquipeComponente(long)
	 */
	@Override
	public Collection<EquipeComponente> listarTodasEquipeComponente() throws NegocioException {

		return repositorioEquipe.listarTodasEquipeComponente();

	}
}
