package br.com.ggas.atendimento.equipe;

import java.util.Date;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface EquipeTurnoItem extends EntidadeNegocio{
	
	String BEAN_ID_EQUIPE_TURNO_ITEM= "equipeTurnoItem";
	
	/**
	 * 
	 * @return the Equipe
	 */
	public Equipe getEquipe();
	
	/**
	 * 
	 * @param equipe - {@link Equipe}
	 */
	public void setEquipe(Equipe equipe);
	
	/**
	 * 
	 * @return the Funcionario
	 */
	public Funcionario getFuncionario();
	
	/**
	 * 
	 * @param funcionario - {@link Funcionario}
	 */
	public void setFuncionario(Funcionario funcionario);
	
	/**
	 * 
	 * @return the Veiculo
	 */
	public Veiculo getVeiculo();
	
	/**
	 * 
	 * @param veiculo - {@link Veiculo}
	 */
	public void setVeiculo(Veiculo veiculo);
	
	/**
	 * 
	 * @return the Turno
	 */
	public EntidadeConteudo getTurno();
	
	/**
	 * 
	 * @param turno - {@link EntidadeConteudo}
	 */
	public void setTurno(EntidadeConteudo turno);
	
	/**
	 * 
	 * @return the DataInicio
	 */
	public Date getDataInicio();
	
	/**
	 * 
	 * @param the dataInicio - {@link Date}
	 */
	public void setDataInicio(Date dataInicio);
	
	/**
	 * 
	 * @return the dataFim 
	 */
	public Date getDataFim();
	
	/**
	 * 
	 * @param dataFim - {@link Date}
	 */
	public void setDataFim(Date dataFim);
	
	
	public EquipeTurno getEquipeTurno();
	
	public void setEquipeTurno(EquipeTurno equipeTurno);

	Long retornarTempoEmMilisegundos();
	

}
