package br.com.ggas.atendimento.equipe;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface EquipeTurno extends EntidadeNegocio {
	
	String BEAN_ID_EQUIPE_TURNO = "equipeTurno";
	
	
	Equipe getEquipe();
	
	void setEquipe(Equipe equipe);
	
	Collection<EquipeTurnoItem> getItens();

	void setItens(Collection<EquipeTurnoItem> itens);

	EntidadeConteudo obterTurno();

	void setDataFim(Date dataFim);

	Date getDataFim();

	void setDataInicio(Date dataInicio);

	Date getDataInicio();
	
	void setIsEscalaSobreaviso(Boolean isEscalaSobreaviso);
	
	Boolean getIsEscalaSobreaviso();
	
	

}
