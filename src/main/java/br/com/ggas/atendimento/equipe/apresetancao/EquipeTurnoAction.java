package br.com.ggas.atendimento.equipe.apresetancao;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import br.com.ggas.atendimento.equipe.EquipeTurno;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.dominio.FiltroEquipeTurnoDTO;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipeTurno;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.veiculo.ControladorVeiculo;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;

@Controller
public class EquipeTurnoAction extends GenericAction {
	
	private static final String SUCESSO_INSERIR_EQUIPE_TURNO = "Inserido a escalda da equipe com sucesso!";
	
	private static final String SUCESSO_EXCLUIR_EQUIPE_TURNO = "Removido o(s) dia(s) da escala com sucesso!";
	
	private static final String ERRO_EXCLUIR_EQUIPE_TURNO = "Não pode excluir um dia da escala com a data atual ou anterior!";
	
	private static final String SUCESSO_ALTERAR_EQUIPE_TURNO_LOTE = "Alteração em lote com sucesso!";
	
	private static final String ERRO_ALTERAR_EQUIPE_TURNO_LOTE = "Ocorreu algum erro durante a atualização!";

	@Autowired
	private ControladorEquipe controladorEquipe;
	
	@Autowired
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorEquipeTurno controladorEquipeTurno;
	
	@Autowired
	private ControladorVeiculo controladorVeiculo;
	
	@Autowired
	private ControladorFuncionario controladorFuncionario;
	
	
	@RequestMapping("exibirPesquisaEquipeTurno")
	public ModelAndView exibirPesquisaEquipeTurno(HttpSession session, HttpServletResponse response, HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaEquipeTurno");
		
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		
		model.addObject("listaFuncionario", obterFuncionarios());
		
		request.getSession().removeAttribute("listaEquipeTurnoReq");
		
		return model;
	}
	
	
	@RequestMapping("pesquisarEquipeTurno")
	public ModelAndView pesquisarEquipeTurno(@ModelAttribute("FiltroEquipeTurnoDTO") FiltroEquipeTurnoDTO filtroEquipeTurno, HttpSession session, HttpServletResponse response, HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaEquipeTurno");
		
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		model.addObject("equipeTurno", filtroEquipeTurno);
		model.addObject("listaEquipeTurno", controladorEquipeTurno.consultarEquipeTurno(filtroEquipeTurno));
		model.addObject("listaFuncionario", obterFuncionarios());
		
		return model;
	}
	
	
	@RequestMapping("exibirIncluirEquipeTurno")
	public ModelAndView exibirIncluirEquipeTurno(HttpSession session, HttpServletResponse response, HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirIncluirEquipeTurno");
		
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		model.addObject("listaVeiculos", controladorVeiculo.listaVeiculos());
		
		request.getSession().removeAttribute("listaEquipeTurnoReq");
		
		return model;
	}
	
	@RequestMapping("gerarEquipeTurno")
	public ModelAndView gerarEquipeTurno(@ModelAttribute("FiltroEquipeTurnoDTO") FiltroEquipeTurnoDTO filtroEquipeTurno, @RequestParam(value="diasSemana", required = false) Integer[] diasSemana,  HttpServletRequest request, HttpServletResponse response) throws GGASException {
		
		ModelAndView model = new ModelAndView("exibirIncluirEquipeTurno");
		
		model.addObject("equipeTurno", filtroEquipeTurno);
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		model.addObject("diasSemana", Arrays.asList(diasSemana));
		model.addObject("listaVeiculos", controladorVeiculo.listaVeiculos());

		
		Map<String, Object> erros = filtroEquipeTurno.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			
			List<EquipeTurnoItem> listaEquipeTurno = controladorEquipeTurno.gerarEquipeTurno(filtroEquipeTurno, diasSemana);
			model.addObject("listaEquipeTurno", listaEquipeTurno);
			request.getSession().setAttribute("listaEquipeTurnoReq", listaEquipeTurno);
			mensagemSucesso(model, "Gerado a escala para os dias possíveis");
		}
		
		return model;
	}
	
	
	@RequestMapping(value = "alterarEquipeTurnoAJAX", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Map<String, Object> alterarEquipeTurnoAJAX(@RequestBody JSONEquipeTurno json, HttpServletRequest request) throws NegocioException {

		if (!"".equals(json.horarioInicio) && !"".equals(json.horarioFim)) {
			List<EquipeTurnoItem> listaEquipeTurno = (List<EquipeTurnoItem>) request.getSession()
					.getAttribute("listaEquipeTurnoReq");

			String[] horaInicio = json.horarioInicio.split(":");
			String[] horaFim = json.horarioFim.split(":");

			EquipeTurnoItem equipeTurnoAlterar = listaEquipeTurno.get(json.chavePrimaria.intValue());

			equipeTurnoAlterar.setDataInicio(Util.adicionarHoraMinutoData(equipeTurnoAlterar.getDataInicio(), horaInicio[0], horaInicio[1]));
			equipeTurnoAlterar.setDataFim(Util.adicionarHoraMinutoData(equipeTurnoAlterar.getDataFim(), horaFim[0], horaFim[1]));

			
			if (json.veiculo != null && json.veiculo > 0) {
				equipeTurnoAlterar.setVeiculo((Veiculo) controladorVeiculo.obter(json.veiculo));
			} else {
				equipeTurnoAlterar.setVeiculo(null);
			} 

			request.getSession().setAttribute("listaEquipeTurnoReq", listaEquipeTurno);

		}
		return Maps.newHashMap(ImmutableMap.of("ok", "Equipe Turno Alterado!"));
	}
	
	
	@RequestMapping("salvarEquipeTurno")
	public ModelAndView salvarEquipeTurno(@ModelAttribute("FiltroEquipeTurnoDTO") FiltroEquipeTurnoDTO filtroEquipeTurno, HttpServletRequest request, HttpServletResponse response) throws GGASException {
		
		ModelAndView model = new ModelAndView("exibirPesquisaEquipeTurno");
		
		model.addObject("equipeTurno", filtroEquipeTurno);
		model.addObject("listaTurnos", controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		model.addObject("listaEquipe", controladorEquipe.listarEquipe());
		
		controladorEquipeTurno
				.salvarEquipeTurno((List<EquipeTurnoItem>) request.getSession().getAttribute("listaEquipeTurnoReq"), filtroEquipeTurno);
		
		model.addObject("listaEquipeTurno", controladorEquipeTurno.consultarEquipeTurno(filtroEquipeTurno));

		
		request.getSession().removeAttribute("listaEquipeTurnoReq");

		mensagemSucesso(model, SUCESSO_INSERIR_EQUIPE_TURNO);
		
		return model;
	}
	
	@RequestMapping("exibirDetalhamentoEquipeTurno")
	public ModelAndView exibirDetalhamentoEquipeTurno(
			@ModelAttribute("FiltroEquipeTurnoDTO") FiltroEquipeTurnoDTO filtroEquipeTurno,
			@RequestParam("idEquipeTurno") Long idEquipeTurno, HttpSession session, HttpServletResponse response,
			HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirDetalhamentoEquipeTurno");
		EquipeTurno equipeTurno = controladorEquipeTurno.obterEquipeTurno(idEquipeTurno);
		Equipe equipe = equipeTurno.getEquipe();
		Collection<EquipeComponente> listaEquipeComponente = controladorEquipe.listarEquipeComponente(equipe.getChavePrimaria());
		
		Collection<EquipeTurnoItem> listaEquipeTurnoItem = equipeTurno.getItens();
		
		if(filtroEquipeTurno.getFuncionario() != null && filtroEquipeTurno.getFuncionario().getChavePrimaria() > 0) {
			listaEquipeTurnoItem = listaEquipeTurnoItem.stream().filter(p -> p.getFuncionario().getChavePrimaria() == filtroEquipeTurno.getFuncionario().getChavePrimaria()).collect(Collectors.toList());
		}

		model.addObject("listaEquipeTurno", listaEquipeTurnoItem);
		model.addObject("listaFuncionarios", listaEquipeComponente.stream().map(p -> p.getFuncionario())
				.sorted(Comparator.comparing(p -> p.getNome())).collect(Collectors.toList()));
		model.addObject("listaVeiculos", controladorVeiculo.listaVeiculos());
		model.addObject("equipeTurno", filtroEquipeTurno);
		model.addObject("idEquipeTurno", idEquipeTurno);

		return model;
	}
	
	
	@RequestMapping(value = "atualizarEquipeTurnoAJAX", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Map<String, Object> atualizarEquipeTurnoAJAX(@RequestBody JSONEquipeTurno json, HttpServletRequest request) {
		try {
			if (!"".equals(json.horarioInicio) && !"".equals(json.horarioFim)) {

				String[] horaInicio = json.horarioInicio.split(":");
				String[] horaFim = json.horarioFim.split(":");

				EquipeTurnoItem equipeTurnoAlterar = controladorEquipeTurno.obterEquipeTurnoItem(json.chavePrimaria);

				equipeTurnoAlterar.setDataInicio(Util.adicionarHoraMinutoData(equipeTurnoAlterar.getDataInicio(), horaInicio[0], horaInicio[1]));
				equipeTurnoAlterar.setDataFim(Util.adicionarHoraMinutoData(equipeTurnoAlterar.getDataFim(), horaFim[0], horaFim[1]));

				equipeTurnoAlterar.setFuncionario(Fachada.getInstancia().obterFuncionario(Long.valueOf(json.funcionario)));
				
				if(json.veiculo != null && json.veiculo > 0) {
					equipeTurnoAlterar.setVeiculo((Veiculo) controladorVeiculo.obter(json.veiculo));
				} else {
					equipeTurnoAlterar.setVeiculo(null);
				}

				controladorEquipeTurno.atualizarEquipeTurnoItem(equipeTurnoAlterar);

			}
			return Maps.newHashMap(ImmutableMap.of("ok", "Equipe Turno Alterado!"));

		} catch (GGASException ge) {
			return Maps.newHashMap(ImmutableMap.of("erro", ge));
		}
	}
	
	/**
	 * Obter funcionarios.
	 *
	 * @return collection
	 * @throws GGASException
	 */
	private Collection<Funcionario> obterFuncionarios() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put("habilitado", true);

		return controladorFuncionario.consultarFuncionarios(filtro);

	}
	
	
	@RequestMapping("excluirDiaEscala")
	public ModelAndView excluirDiaEscala(
			@ModelAttribute("FiltroEquipeTurnoDTO") FiltroEquipeTurnoDTO filtroEquipeTurno, @RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam("idEquipeTurno") Long idEquipeTurno, HttpSession session, HttpServletResponse response,
			HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirDetalhamentoEquipeTurno");
		EquipeTurno equipeTurno = controladorEquipeTurno.obterEquipeTurno(idEquipeTurno);
		Equipe equipe = equipeTurno.getEquipe();
		Collection<EquipeComponente> listaEquipeComponente = controladorEquipe.listarEquipeComponente(equipe.getChavePrimaria());
		
		Collection<EquipeTurnoItem> listaEquipeTurnoItem = equipeTurno.getItens();
		
		if(filtroEquipeTurno.getFuncionario() != null && filtroEquipeTurno.getFuncionario().getChavePrimaria() > 0) {
			listaEquipeTurnoItem = listaEquipeTurnoItem.stream().filter(p -> p.getFuncionario().getChavePrimaria() == filtroEquipeTurno.getFuncionario().getChavePrimaria()).collect(Collectors.toList());
		}
		
		if (controladorEquipeTurno.removerDiaEscala(listaEquipeTurnoItem, chavesPrimarias)) {
			mensagemSucesso(model, SUCESSO_EXCLUIR_EQUIPE_TURNO);
		} else {
			mensagemErro(model, ERRO_EXCLUIR_EQUIPE_TURNO);
		}

		model.addObject("listaEquipeTurno", listaEquipeTurnoItem);
		model.addObject("listaFuncionarios", listaEquipeComponente.stream().map(p -> p.getFuncionario())
				.sorted(Comparator.comparing(p -> p.getNome())).collect(Collectors.toList()));
		model.addObject("listaVeiculos", controladorVeiculo.listaVeiculos());
		model.addObject("equipeTurno", filtroEquipeTurno);
		model.addObject("idEquipeTurno", idEquipeTurno);


		return model;
	}
	
	
	@RequestMapping("atualizacaoEmLote")
	public ModelAndView atualizacaoEmLote(
			@ModelAttribute("FiltroEquipeTurnoDTO") FiltroEquipeTurnoDTO filtroEquipeTurno, 
			@RequestParam("idEquipeTurno") Long idEquipeTurno, @RequestParam("idEquipeTurnoItem") Long idEquipeTurnoItem, 
			@RequestParam(required = false, value = "funcionarioLote") Funcionario funcionarioLote,
			@RequestParam(required = false, value = "horarioInicioLote") String horarioInicioLote,
			@RequestParam(required = false, value = "horarioFimLote") String horarioFimLote,
			@RequestParam(required = false, value = "veiculoLote") Veiculo veiculoLote,
			HttpSession session, HttpServletResponse response,
			HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirDetalhamentoEquipeTurno");
		EquipeTurno equipeTurno = controladorEquipeTurno.obterEquipeTurno(idEquipeTurno);
		Equipe equipe = equipeTurno.getEquipe();
		Collection<EquipeComponente> listaEquipeComponente = controladorEquipe.listarEquipeComponente(equipe.getChavePrimaria());
		
		Collection<EquipeTurnoItem> listaEquipeTurnoItem = equipeTurno.getItens();
		
		if(filtroEquipeTurno.getFuncionario() != null && filtroEquipeTurno.getFuncionario().getChavePrimaria() > 0) {
			listaEquipeTurnoItem = listaEquipeTurnoItem.stream().filter(p -> p.getFuncionario().getChavePrimaria() == filtroEquipeTurno.getFuncionario().getChavePrimaria()).collect(Collectors.toList());
		}
		
		if (controladorEquipeTurno.alterarEscalaLote(listaEquipeTurnoItem, idEquipeTurnoItem, funcionarioLote, horarioInicioLote, horarioFimLote, veiculoLote)) {
			mensagemSucesso(model, SUCESSO_ALTERAR_EQUIPE_TURNO_LOTE);
		} else {
			mensagemErro(model, ERRO_ALTERAR_EQUIPE_TURNO_LOTE);
		}

		model.addObject("listaEquipeTurno", listaEquipeTurnoItem);
		model.addObject("listaFuncionarios", listaEquipeComponente.stream().map(p -> p.getFuncionario())
				.sorted(Comparator.comparing(p -> p.getNome())).collect(Collectors.toList()));
		model.addObject("listaVeiculos", controladorVeiculo.listaVeiculos());
		model.addObject("equipeTurno", filtroEquipeTurno);
		model.addObject("idEquipeTurno", idEquipeTurno);


		return model;
	}
		

}
