
package br.com.ggas.atendimento.equipe.dominio;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela representação de uma Equipe
 *
 */
public class Equipe extends EntidadeNegocioImpl {

	private static final int HORAS_DIA = 24;

	private static final int LIMITE_CAMPO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nome;

	private String descricaoPlacaVeiculo;

	private Long quantidadeHorasDia;

	private UnidadeOrganizacional unidadeOrganizacional;

	/**
	 * @return nome
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return descricaoPlacaVeiculo
	 */
	public String getDescricaoPlacaVeiculo() {

		return descricaoPlacaVeiculo;
	}

	/**
	 * @param descricaoPlacaVeiculo
	 */
	public void setDescricaoPlacaVeiculo(String descricaoPlacaVeiculo) {

		this.descricaoPlacaVeiculo = descricaoPlacaVeiculo;
	}

	/**
	 * @return quantidadeHorasDia
	 */
	public Long getQuantidadeHorasDia() {

		return quantidadeHorasDia;
	}

	/**
	 * @param quantidadeHorasDia
	 */
	public void setQuantidadeHorasDia(Long quantidadeHorasDia) {

		this.quantidadeHorasDia = quantidadeHorasDia;
	}

	/**
	 * @return unidadeOrganizacional
	 */
	public UnidadeOrganizacional getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	/**
	 * @param unidadeOrganizacional
	 */
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(nome) || nome.trim().length() == 0) {
			stringBuilder.append(Constantes.DESCRICAO_EQUIPE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(quantidadeHorasDia == null || quantidadeHorasDia < 0) {
			stringBuilder.append(Constantes.CARGA_TRABALHO_DIARIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(unidadeOrganizacional == null) {
			stringBuilder.append(Constantes.ATRIBUTO_UNIDADE_ORGANIZACIONAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			if(StringUtils.isEmpty(nome) || (quantidadeHorasDia == null || quantidadeHorasDia < 0) || unidadeOrganizacional == null) {
				erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
								camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
			}

		} else if(camposObrigatorios.length() == 0 && (quantidadeHorasDia > HORAS_DIA || quantidadeHorasDia == 0)) {
			erros.put(Constantes.LIMITE_CARGA_TRABALHO_DIARIA, null);
		}

		return erros;
	}

}
