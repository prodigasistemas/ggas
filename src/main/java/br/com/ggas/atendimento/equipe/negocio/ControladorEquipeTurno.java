package br.com.ggas.atendimento.equipe.negocio;

import java.util.Collection;
import java.util.List;

import org.hibernate.HibernateException;

import br.com.ggas.atendimento.equipe.EquipeTurno;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.FiltroEquipeTurnoDTO;
import br.com.ggas.atendimento.equipe.dominio.FuncionarioEquipeVO;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

public interface ControladorEquipeTurno {
	
	String BEAN_ID_CONTROLADOR_EQUIPE_TURNO = "controladorEquipeTurno";

	List<EquipeTurnoItem> gerarEquipeTurno(FiltroEquipeTurnoDTO filtroEquipeTurno, Integer[] diasSemana) throws NegocioException;

	Boolean salvarEquipeTurno(List<EquipeTurnoItem> listaEquipeTurno, FiltroEquipeTurnoDTO filtro) throws NegocioException;

	Collection<EquipeTurno> consultarEquipeTurno(FiltroEquipeTurnoDTO filtro) throws HibernateException, NegocioException;

	EquipeTurno obterEquipeTurno(Long idChavePrimaria);

	EquipeTurnoItem obterEquipeTurnoItem(Long idChavePrimaria);

	void atualizarEquipeTurnoItem(EquipeTurnoItem equipeTurnoItem) throws ConcorrenciaException, NegocioException;

	Boolean removerDiaEscala(Collection<EquipeTurnoItem> listaEquipeTurnoItem,
			Long[] chavesPrimarias) throws NegocioException;

	Boolean alterarEscalaLote(Collection<EquipeTurnoItem> listaEquipeTurnoItem, Long idEquipeTurnoItem, Funcionario funcionarioLote, String horarioInicioLote, String horarioFimLote, Veiculo veiculoLote) throws ConcorrenciaException, NegocioException;

	Collection<EquipeTurnoItem> obterListaEquipeTurnoItem(int mes, int ano, Long chaveEquipe);

	Collection<EquipeTurnoItem> consultarEscalaExistente(String dataInicio, String dataFim, Funcionario[] funcionarios) throws HibernateException, NegocioException;

	List<FuncionarioEquipeVO> obterEquipeTurnoItemPorChaveEquipe(Long chaveEquipe);

}
