package br.com.ggas.atendimento.equipe.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.atendimento.equipe.EquipeTurno;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class EquipeTurnoImpl  extends EntidadeNegocioImpl implements EquipeTurno {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6667415318839947777L;
	private Equipe equipe;
	private Date dataInicio;
	private Date dataFim;
	Collection<EquipeTurnoItem> itens;
	private Boolean isEscalaSobreaviso;


	@Override
	public Equipe getEquipe() {
		return this.equipe;
	}

	@Override
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
		
	}

	@Override
	public Date getDataInicio() {
		return dataInicio;
	}

	@Override
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	@Override
	public Date getDataFim() {
		return dataFim;
	}

	@Override
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	@Override
	public Collection<EquipeTurnoItem> getItens(){
		return this.itens;
	}
	
	@Override
	public void setItens(Collection<EquipeTurnoItem> itens) {
		this.itens = itens;
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public EntidadeConteudo obterTurno() {
		return this.itens.iterator().next().getTurno();
	}

	@Override
	public Boolean getIsEscalaSobreaviso() {
		return isEscalaSobreaviso;
	}

	@Override
	public void setIsEscalaSobreaviso(Boolean isEscalaSobreaviso) {
		this.isEscalaSobreaviso = isEscalaSobreaviso;
	}
}
