package br.com.ggas.atendimento.equipe.apresetancao;

import java.io.Serializable;


public class JSONEquipeTurno implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2347009796613144004L;
	protected String horarioInicio;
	protected String horarioFim;
	protected Long chavePrimaria;
	protected String funcionario;
	protected Long veiculo;
	
	public String getHorarioInicio() {
		return horarioInicio;
	}
	public void setHorarioInicio(String horarioInicio) {
		this.horarioInicio = horarioInicio;
	}
	public String getHorarioFim() {
		return horarioFim;
	}
	public void setHorarioFim(String horarioFim) {
		this.horarioFim = horarioFim;
	}
	
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	
	public String getFuncionario() {
		return this.funcionario;
	}
	
	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}
	
	public void setVeiculo(Long veiculo) {
		this.veiculo = veiculo;
	}
	
	public Long getVeiculo() {
		return this.veiculo;
	}
	

}
