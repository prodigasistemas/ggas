package br.com.ggas.atendimento.equipe.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import br.com.ggas.atendimento.equipe.EquipeTurno;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class EquipeTurnoItemImpl extends EntidadeNegocioImpl implements EquipeTurnoItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6238408891818438314L;
	private Equipe equipe;
	private Funcionario funcionario;
	private Veiculo veiculo;
	private EntidadeConteudo turno;
	private Date dataInicio;
	private Date dataFim;
	private EquipeTurno equipeTurno;

	@Override
	public Equipe getEquipe() {
		return this.equipe;
	}

	@Override
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
		
	}

	@Override
	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
		
	}

	@Override
	public Veiculo getVeiculo() {
		return this.veiculo;
	}

	@Override
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
		
	}

	@Override
	public EntidadeConteudo getTurno() {
		return this.turno;
	}

	@Override
	public void setTurno(EntidadeConteudo turno) {
		this.turno = turno;
		
	}

	@Override
	public Date getDataInicio() {
		return this.dataInicio;
	}

	@Override
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	@Override
	public Date getDataFim() {
		return this.dataFim;
	}

	@Override
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EquipeTurno getEquipeTurno() {
		return this.equipeTurno;
	}

	@Override
	public void setEquipeTurno(EquipeTurno equipeTurno) {
		this.equipeTurno = equipeTurno;
	}

	
	
	@Override
	public Long retornarTempoEmMilisegundos() {
		Calendar c = Calendar.getInstance();
		c.setTime(this.dataInicio);
		
		return c.getTimeInMillis();
	}
}
