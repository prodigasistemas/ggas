package br.com.ggas.atendimento.equipe.dominio;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.util.Constantes;

public class FiltroEquipeTurnoDTO {

	private Equipe equipe;
	private EntidadeConteudo turno;
	private String[] horaInicio;
	private String[] horaFim;
	private String dataInicio;
	private String dataFim;
	private Funcionario[] funcionarios;
	private Veiculo[] veiculo;
	private Boolean isConsiderarFeriado;
	private Boolean isEscalaSobreaviso;
	private Funcionario funcionario;
	
	private static final int LIMITE_CAMPO = 2;

	public Equipe getEquipe() {
		return equipe;
	}

	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}

	public EntidadeConteudo getTurno() {
		return turno;
	}

	public void setTurno(EntidadeConteudo turno) {
		this.turno = turno;
	}

	public String[] getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String[] horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String[] getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim[]) {
		this.horaFim = horaFim;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Funcionario[] getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Funcionario[] funcionarios) {
		this.funcionarios = funcionarios;
	}

	public Veiculo[] getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo[] veiculo) {
		this.veiculo = veiculo;
	}

	public Map<String, Object> validarDados() {
		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;


		if(equipe == null || equipe.getChavePrimaria() < 0l) {
			stringBuilder.append("Equipe");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if (horaInicio.length < 0) {
			stringBuilder.append("Horário inicial");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			for (String inicio : horaInicio) {
				if (StringUtils.isEmpty(inicio)) {
					stringBuilder.append("Horário inicial");
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
					break;
				}
			}
		}

		if (horaFim.length < 0) {
			stringBuilder.append("Horário Final");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			for (String fim : horaFim) {
				if (StringUtils.isEmpty(fim)) {
					stringBuilder.append("Horário Final");
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
					break;
				}
			}
		}
		
		if(StringUtils.isEmpty(dataInicio)) {
			stringBuilder.append("Data Inicio");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(StringUtils.isEmpty(dataFim)) {
			stringBuilder.append("Data Fim");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	public Boolean getIsConsiderarFeriado() {
		return isConsiderarFeriado;
	}

	public void setIsConsiderarFeriado(Boolean isConsiderarFeriado) {
		this.isConsiderarFeriado = isConsiderarFeriado;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public Boolean getIsEscalaSobreaviso() {
		return isEscalaSobreaviso;
	}

	public void setIsEscalaSobreaviso(Boolean isEscalaSobreaviso) {
		this.isEscalaSobreaviso = isEscalaSobreaviso;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
