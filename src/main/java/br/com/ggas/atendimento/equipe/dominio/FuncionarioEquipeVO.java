package br.com.ggas.atendimento.equipe.dominio;

import java.io.Serializable;

public class FuncionarioEquipeVO implements Serializable {

	private Long chavePrimariaEquipe;
	private Long chavePrimariaFuncionario;

	public Long getChavePrimariaEquipe() {
		return chavePrimariaEquipe;
	}

	public void setChavePrimariaEquipe(Long chavePrimariaEquipe) {
		this.chavePrimariaEquipe = chavePrimariaEquipe;
	}

	public Long getChavePrimariaFuncionario() {
		return chavePrimariaFuncionario;
	}

	public void setChavePrimariaFuncionario(Long chavePrimariaFuncionario) {
		this.chavePrimariaFuncionario = chavePrimariaFuncionario;
	}

}
