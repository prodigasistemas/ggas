/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 17/03/2014 14:48:03
 @author ifrancisco
 */

package br.com.ggas.atendimento.equipe.dominio;

import java.util.List;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;

/**
 * EquipeVO
 */
public class EquipeVO {

	private Equipe equipe;

	private Cliente cliente;

	private List<PontoConsumoVO> listaPontoConsumoVO;

	public Equipe getEquipe() {

		return equipe;
	}

	public void setEquipe(Equipe equipe) {

		this.equipe = equipe;
	}

	public List<PontoConsumoVO> getListaPontoConsumoVO() {

		return listaPontoConsumoVO;
	}

	public void setListaPontoConsumoVO(List<PontoConsumoVO> listaPontoConsumoVO) {

		this.listaPontoConsumoVO = listaPontoConsumoVO;
	}

	public Cliente getCliente() {

		return cliente;
	}

	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

}
