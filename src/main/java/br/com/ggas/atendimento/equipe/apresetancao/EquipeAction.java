package br.com.ggas.atendimento.equipe.apresetancao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.equipe.negocio.impl.ControladorEquipeImpl;
import br.com.ggas.atendimento.equipecomponente.dominio.EquipeComponente;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelas telas relacionadas a equipe.
 */
@Controller
public class EquipeAction extends GenericAction {

	private static final String FUNCIONARIOS = "funcionarios";

	private static final String GRID_COMPONENTES_EQUIPE = "gridComponentesEquipe";

	private static final String EQUIPE3 = "equipe";

	private static final String TELA_EXIBIR_INCLUSAO_EQUIPE = "exibirInclusaoEquipe";

	private static final String LISTA_UNIDADE_ORGANIZACIONAL = "listaUnidadeOrganizacional";

	private static final String EQUIPE2 = "Equipe";

	private static final String LISTA_COMPONENTES_EQUIPE = "listaComponentesEquipe";

	private static final String TELA_FORWARD_PESQUISAR_EQUIPE = "forward:/pesquisarEquipe";

	@Autowired
	private ControladorEquipe controladorEquipe;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	public ControladorEquipe getControladorEquipe() {

		return controladorEquipe;
	}

	public void setControladorEquipe(ControladorEquipeImpl controladorEquipe) {

		this.controladorEquipe = controladorEquipe;
	}

	/**
	 * Alterar equipe.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param nome
	 *            the nome
	 * @param unidade
	 *            the unidade
	 * @param quantidadeHorasDia
	 *            the quantidade horas dia
	 * @param descricaoPlacaVeiculo
	 *            the descricao placa veiculo
	 * @param habilitado
	 *            the habilitado
	 * @param sessao
	 *            the sessao
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("alterarEquipe")
	public ModelAndView alterarEquipe(@RequestParam("chavePrimaria") Long chavePrimaria,
					@RequestParam(value = "nome", required = false) String nome,
					@RequestParam(value = "unidadeOrganizacional", required = false) String unidade,
					@RequestParam(value = "quantidadeHorasDia", required = false) String quantidadeHorasDia,
					@RequestParam(value = "descricaoPlacaVeiculo", required = false) String descricaoPlacaVeiculo,
					@RequestParam(value = EntidadeNegocio.ATRIBUTO_HABILITADO, required = false) String habilitado, HttpSession sessao,
					HttpServletRequest request) throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_EQUIPE);

		List<EquipeComponente> listaComponentesEquipe = (List<EquipeComponente>) sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);

		Equipe equipe = controladorEquipe.obterEquipe(chavePrimaria);

		this.popularEquipe(nome, unidade, quantidadeHorasDia, descricaoPlacaVeiculo, habilitado, equipe);

		try {

			controladorEquipe.verificarEquipeExistente(equipe);
			controladorEquipe.verificarExistenciaComponenteResponsavel(listaComponentesEquipe);
			controladorEquipe.atualizarEquipe(equipe);
			controladorEquipe.inserirEquipeComponente(listaComponentesEquipe, equipe);
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
			sessao.removeAttribute(LISTA_COMPONENTES_EQUIPE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, EQUIPE2);

		} catch (GGASException e) {
			model = new ModelAndView("forward:/exibirAlteracaoEquipe");
			model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
			request.setAttribute("equipeModificada", equipe);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Incluir equipe.
	 * 
	 * @param nome
	 *            the nome
	 * @param unidade
	 *            the unidade
	 * @param quantidadeHorasDia
	 *            the quantidade horas dia
	 * @param descricaoPlacaVeiculo
	 *            the descricao placa veiculo
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("incluirEquipe")
	public ModelAndView incluirEquipe(@RequestParam(value = "nome", required = false) String nome,
					@RequestParam(value = "unidadeOrganizacional", required = false) String unidade,
					@RequestParam(value = "quantidadeHorasDia", required = false) String quantidadeHorasDia,
					@RequestParam(value = "descricaoPlacaVeiculo", required = false) String descricaoPlacaVeiculo, HttpSession sessao)
					throws NegocioException {

		Equipe equipe = new Equipe();

		this.popularEquipe(nome, unidade, quantidadeHorasDia, descricaoPlacaVeiculo, "true", equipe);

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_EQUIPE);

		List<EquipeComponente> listaComponentesEquipe = (List<EquipeComponente>) sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);

		try {
			controladorEquipe.verificarEquipeExistente(equipe);
			controladorEquipe.verificarExistenciaComponenteResponsavel(listaComponentesEquipe);
			controladorEquipe.inserir(equipe);
			controladorEquipe.inserirEquipeComponente(listaComponentesEquipe, equipe);
			sessao.removeAttribute(LISTA_COMPONENTES_EQUIPE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, EQUIPE2);

		} catch(GGASException e) {

			model = new ModelAndView(TELA_EXIBIR_INCLUSAO_EQUIPE);
			model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
			model.addObject(EQUIPE3, equipe);
			mensagemErroParametrizado(model, e);

		}

		return model;

	}

	/**
	 * Popular equipe.
	 * 
	 * @param nome
	 *            the nome
	 * @param unidade
	 *            the unidade
	 * @param quantidadeHorasDia
	 *            the quantidade horas dia
	 * @param descricaoPlacaVeiculo
	 *            the descricao placa veiculo
	 * @param habilitado
	 *            the habilitado
	 * @param equipe
	 *            the equipe
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularEquipe(String nome, String unidade, String quantidadeHorasDia, String descricaoPlacaVeiculo, String habilitado,
					Equipe equipe) throws NegocioException {

		if(nome != null && !StringUtils.isEmpty(nome)) {
			equipe.setNome(Util.rtrim(Util.ltrim(nome)));
		} else {
			equipe.setNome(null);
		}
		if(descricaoPlacaVeiculo != null && !StringUtils.isEmpty(descricaoPlacaVeiculo)) {
			equipe.setDescricaoPlacaVeiculo(descricaoPlacaVeiculo);
		}
		if(habilitado != null && !StringUtils.isEmpty(habilitado)) {
			equipe.setHabilitado(Boolean.parseBoolean(habilitado));
		}
		if(!"-1".equals(unidade)) {
			UnidadeOrganizacional unidadeOrganizacional = (UnidadeOrganizacional) controladorUnidadeOrganizacional.obter(Long
							.parseLong(unidade));
			equipe.setUnidadeOrganizacional(unidadeOrganizacional);
		} else {
			equipe.setUnidadeOrganizacional(null);
		}

		if(quantidadeHorasDia != null && !StringUtils.isEmpty(quantidadeHorasDia)) {
			equipe.setQuantidadeHorasDia(Long.parseLong(quantidadeHorasDia));
		} else {
			equipe.setQuantidadeHorasDia(null);
		}

	}

	/**
	 * Pesquisar equipe.
	 * 
	 * @param nome
	 *            the nome
	 * @param habilitado
	 *            the habilitado
	 * @param unidade
	 *            the unidade
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarEquipe")
	public ModelAndView pesquisarEquipe(@RequestParam(value = "nome", required = false) String nome,
					@RequestParam(value = EntidadeNegocio.ATRIBUTO_HABILITADO, required = false) String habilitado,
					@RequestParam(value = "unidadeOrganizacional", required = false) String unidade) throws GGASException {

		Equipe equipe = new Equipe();
		if(nome != null) {
			equipe.setNome(nome.trim());
		}

		if (!StringUtils.isEmpty(unidade) && !"-1".equals(unidade)) {
			UnidadeOrganizacional unidadeOrganizacional = (UnidadeOrganizacional) controladorUnidadeOrganizacional.obter(Long
							.parseLong(unidade));
			equipe.setUnidadeOrganizacional(unidadeOrganizacional);
		}
		Boolean equipeHabilitada = null;
		if(habilitado != null && !habilitado.isEmpty()) {
			equipeHabilitada = Boolean.parseBoolean(habilitado);
		}

		Collection<Equipe> listaEquipe = controladorEquipe.consultarEquipe(equipe, equipeHabilitada);

		ModelAndView model = new ModelAndView("exibirPesquisaEquipe");
		model.addObject("listaEquipe", listaEquipe);
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject(EQUIPE3, equipe);
		if(habilitado == null) {
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");
		} else {
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		}

		return model;
	}

	/**
	 * Exibir pesquisa equipe.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirPesquisaEquipe")
	public ModelAndView exibirPesquisaEquipe(HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaEquipe");
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");
		sessao.removeAttribute(LISTA_COMPONENTES_EQUIPE);
		sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);

		return model;
	}

	/**
	 * Exibir inclusao equipe.
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_EQUIPE)
	public ModelAndView exibirInclusaoEquipe() throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_EQUIPE);
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());

		return model;
	}

	/**
	 * Exibir alteracao equipe.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoEquipe")
	public ModelAndView exibirAlteracaoEquipe(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request,
					HttpSession sessao) throws GGASException {

		Equipe equipeModificada = (Equipe) request.getAttribute("equipeModificada");
		Long chave = chavePrimaria;

		ModelAndView model = new ModelAndView("exibirAlteracaoEquipe");

		Collection<EquipeComponente> listaComponentesEquipe = controladorEquipe.listarEquipeComponente(chavePrimaria);
		if(equipeModificada == null) {
			model.addObject(EQUIPE3, controladorEquipe.obterEquipe(chave));
		} else {
			model.addObject(EQUIPE3, equipeModificada);
		}
		List<EquipeComponente> listaComponentesEquipeSessao = (List<EquipeComponente>) sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);
		if(listaComponentesEquipeSessao == null) {
			sessao.setAttribute(LISTA_COMPONENTES_EQUIPE, listaComponentesEquipe);
		} else {
			sessao.setAttribute(LISTA_COMPONENTES_EQUIPE, listaComponentesEquipeSessao);
		}

		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());

		return model;
	}

	/**
	 * Exibir detalhamento equipe.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoEquipe")
	public ModelAndView exibirDetalhamentoEquipe(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoEquipe");

		Collection<EquipeComponente> listaComponentesEquipe = controladorEquipe.listarEquipeComponente(chavePrimaria);
		model.addObject(EQUIPE3, controladorEquipe.obterEquipe(chavePrimaria));
		model.addObject(LISTA_COMPONENTES_EQUIPE, listaComponentesEquipe);

		return model;
	}

	/**
	 * Adicionar componente.
	 * 
	 * @param idFuncionario
	 *            the id funcionario
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("adicionarComponente")
	public ModelAndView adicionarComponente(@RequestParam("idFuncionario") Long idFuncionario, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_COMPONENTES_EQUIPE);
		model.addObject("teste", 0);

		@SuppressWarnings("unchecked")
		List<EquipeComponente> listaComponentesEquipe = (List<EquipeComponente>) sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);

		try {
			Funcionario funcionario = (Funcionario) controladorFuncionario.obter(idFuncionario);
			EquipeComponente equipeComponente = new EquipeComponente();

			if(listaComponentesEquipe == null || listaComponentesEquipe.isEmpty()) {
				listaComponentesEquipe = new ArrayList<>();
				equipeComponente.setFuncionario(funcionario);
				equipeComponente.setIndicadorResponsavel(Boolean.TRUE);
				listaComponentesEquipe.add(equipeComponente);

				sessao.setAttribute(LISTA_COMPONENTES_EQUIPE, listaComponentesEquipe);
			} else {
				boolean condicao = controladorEquipe.verificarComponentesDuplicados(listaComponentesEquipe, funcionario);
				if(!condicao) {
					equipeComponente.setFuncionario(funcionario);
					listaComponentesEquipe.add(equipeComponente);

					sessao.setAttribute(LISTA_COMPONENTES_EQUIPE, listaComponentesEquipe);

				} else {
					model = new ModelAndView("ajaxErro");
					mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_EQUIPE_COMPONENTE_EXISTENTE);
				}

			}

			model.getModel().get(FUNCIONARIOS);
		} catch(GGASException e) {

			model = new ModelAndView(TELA_EXIBIR_INCLUSAO_EQUIPE);
			model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
			model.addObject(FUNCIONARIOS, null);
			mensagemErroParametrizado(model, e);

		}

		return model;
	}

	/**
	 * Remover componente.
	 * 
	 * @param indexListaComponentesEquipe
	 *            the index lista componentes equipe
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerComponente")
	public ModelAndView removerComponente(@RequestParam("indexListaComponentesEquipe") int indexListaComponentesEquipe, HttpSession sessao)
					throws GGASException {

		ModelAndView model = new ModelAndView(GRID_COMPONENTES_EQUIPE);

		List<EquipeComponente> componentesModel = (List<EquipeComponente>) sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);

		if(componentesModel != null) {
			componentesModel.remove(indexListaComponentesEquipe);
			sessao.setAttribute(LISTA_COMPONENTES_EQUIPE, componentesModel);

			if(componentesModel.size() == 1) {
				componentesModel.get(0).setIndicadorResponsavel(Boolean.TRUE);
			}
		}

		return model;

	}

	/**
	 * Atualizar responsavel.
	 * 
	 * @param indexListaComponentesEquipe
	 *            the index lista componentes equipe
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("atualizarResponsavel")
	public ModelAndView atualizarResponsavel(@RequestParam("indexListaComponentesEquipe") int indexListaComponentesEquipe,
					HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(GRID_COMPONENTES_EQUIPE);

		List<EquipeComponente> listaComponentesEquipe = (List<EquipeComponente>) sessao.getAttribute(LISTA_COMPONENTES_EQUIPE);

		if(listaComponentesEquipe != null && !listaComponentesEquipe.isEmpty()) {
			for (EquipeComponente equipeComponente : listaComponentesEquipe) {
				if(equipeComponente.isIndicadorResponsavel()) {
					equipeComponente.setIndicadorResponsavel(false);
				}
			}
			listaComponentesEquipe.get(indexListaComponentesEquipe).setIndicadorResponsavel(true);
		}

		return model;
	}

	/**
	 * Pesquisar funcionario.
	 * 
	 * @param nome
	 *            the nome
	 * @param matricula
	 *            the matricula
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarFuncionario")
	public ModelAndView pesquisarFuncionario(@RequestParam(value = "nome", required = false) String nome,
					@RequestParam(value = "matricula", required = false) String matricula) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();

		ModelAndView model = new ModelAndView("popupPesquisarFuncionario");

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		if(nome != null && !StringUtils.isEmpty(nome)) {
			filtro.put("nome", nome);
			funcionario.setNome(nome);
		}
		if(matricula != null && !StringUtils.isEmpty(matricula)) {
			filtro.put("matricula", matricula);
			funcionario.setMatricula(matricula);
		}
		filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
		model.addObject(FUNCIONARIOS, controladorFuncionario.consultarFuncionarios(filtro));
		model.addObject("funcionario", funcionario);

		return model;

	}

	/**
	 * Remover equipe.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerEquipe")
	public ModelAndView removerEquipe(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_EQUIPE);

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorEquipe.removerEquipe(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, EQUIPE2);
		} catch(GGASException e) {

			model.addObject("listaUnidadeMedida", controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir popup pesquisa funcionario.
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirPopupPesquisaFuncionario")
	public ModelAndView exibirPopupPesquisaFuncionario() throws GGASException {

		return new ModelAndView("popupPesquisarFuncionario");
	}

}
