package br.com.ggas.atendimento.equipe.repositorio;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.equipe.EquipeTurno;
import br.com.ggas.atendimento.equipe.EquipeTurnoItem;
import br.com.ggas.atendimento.equipe.dominio.FiltroEquipeTurnoDTO;
import br.com.ggas.atendimento.equipe.dominio.FuncionarioEquipeVO;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;


@Repository
public class RepositorioEquipeTurno extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio equipe.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioEquipeTurno(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EquipeTurnoItem.BEAN_ID_EQUIPE_TURNO_ITEM);
	}
	
	
	public EntidadeNegocio criarEquipeTurno() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EquipeTurno.BEAN_ID_EQUIPE_TURNO);
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(EquipeTurno.BEAN_ID_EQUIPE_TURNO);
	}
	
	public Class<?> getClasseEntidadeEquipeTurnoItem() {
		return ServiceLocator.getInstancia().getClassPorID(EquipeTurnoItem.BEAN_ID_EQUIPE_TURNO_ITEM);
	}
	
	public Collection<EquipeTurnoItem> consultarEscalaExistente(String dataInicio, String dataFim,
			Funcionario[] funcionarios) throws HibernateException, NegocioException {
		
		Collection<Funcionario> listaFuncionario = Arrays.asList(funcionarios);
		Long[] chavesFuncionarios = Util.collectionParaArrayChavesPrimarias(listaFuncionario);
		
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT  item ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" item ");
		hql.append(" WHERE 1=1 ");
		hql.append(" and ((item.dataInicio >= :dataInicio ");
		hql.append(" and item.dataFim <= :dataFim)) and ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "item.funcionario.chavePrimaria", "FT_CONS", Arrays.asList(chavesFuncionarios));			
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("dataInicio", DataUtil.converterParaData(dataInicio));
		query.setParameter("dataFim", DataUtil.converterParaData(dataFim));
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		
		return query.list();
	}


	public Collection<EquipeTurno> consultarEquipeTurno(FiltroEquipeTurnoDTO filtro) throws HibernateException, NegocioException {
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT  equipeTurno ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" item ");
		hql.append(" inner join item.equipeTurno equipeTurno ");
		hql.append(" inner join item.equipe equipe ");
		hql.append(" inner join item.turno turno ");
		hql.append(" WHERE 1=1 ");
		
		if(filtro.getEquipe() != null && filtro.getEquipe().getChavePrimaria() > 0) {
			hql.append(" AND equipe.chavePrimaria = :equipe ");
		}
		
		if(filtro.getFuncionario() != null && filtro.getFuncionario().getChavePrimaria() > 0) {
			hql.append(" AND item.funcionario.chavePrimaria = :funcionario ");
		}
		
		if(!StringUtils.isEmpty(filtro.getDataInicio()) && !StringUtils.isEmpty(filtro.getDataFim())) {
			hql.append(" and ((equipeTurno.dataInicio >= :dataInicio ");
			hql.append(" and equipeTurno.dataInicio <= :dataFim)) ");
			hql.append(" or (equipeTurno.dataFim >= :dataInicio ");
			hql.append(" and equipeTurno.dataFim <= :dataFim)) ");
		}
		
		hql.append(" ORDER BY equipe.nome, item.equipeTurno.dataInicio ");
		
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		if(filtro.getEquipe() != null && filtro.getEquipe().getChavePrimaria() > 0) {
			query.setParameter("equipe", filtro.getEquipe().getChavePrimaria());
		}
		
		if(filtro.getFuncionario() != null && filtro.getFuncionario().getChavePrimaria() > 0) {
			query.setParameter("funcionario", filtro.getFuncionario().getChavePrimaria());
		}
		
		if(!StringUtils.isEmpty(filtro.getDataInicio()) && !StringUtils.isEmpty(filtro.getDataFim())) {
			query.setParameter("dataInicio", DataUtil.converterParaData(filtro.getDataInicio()));
			query.setParameter("dataFim", DataUtil.converterParaData(filtro.getDataFim()));

		}
		
		return (Collection<EquipeTurno>) query.list().stream().distinct().collect(Collectors.toList());
	}
	
	public EquipeTurno obterEquipeTurno(Long idChavePrimaria) {
		
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT equipeTurno ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" equipeTurno ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND equipeTurno.chavePrimaria = :chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", idChavePrimaria);
		
		return (EquipeTurno) query.uniqueResult();
		
		
	}
	
	public EquipeTurnoItem obterEquipeTurnoItem(Long idChavePrimaria) {
		
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT  item ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" item ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND item.chavePrimaria = :chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", idChavePrimaria);
		
		return (EquipeTurnoItem) query.uniqueResult();
	}	
	
	
	public List<FuncionarioEquipeVO> obterEquipeTurnoItemPorChaveEquipe(Long chaveEquipe) {
		
		StringBuilder hql = new StringBuilder();
		
		hql = new StringBuilder("SELECT item.equipe.chavePrimaria AS chavePrimariaEquipe, item.funcionario.chavePrimaria AS chavePrimariaFuncionario");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" item ");
		hql.append(" WHERE ");
		hql.append(" item.equipe.chavePrimaria = :chaveEquipe ");
		hql.append(" GROUP BY item.equipe.chavePrimaria, item.funcionario.chavePrimaria ");
		
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(FuncionarioEquipeVO.class));
		query.setParameter("chaveEquipe", chaveEquipe);
		
		
		return query.list();
	}
	
	public Collection<EquipeTurnoItem> obterListaEquipeTurnoItem(int mes, int ano, Long chaveEquipe) {
		
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT  item ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEquipeTurnoItem().getSimpleName());
		hql.append(" item ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND item.equipe.chavePrimaria = :chaveEquipe ");
		hql.append(" AND TO_CHAR(item.dataInicio, 'MM/yyyy') = :mesAno ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveEquipe", chaveEquipe);
		query.setParameter("mesAno",  (mes < 10 ? "0"  + mes : mes)  + "/" + ano);
		
		return query.list();
	}	
	
	

}
