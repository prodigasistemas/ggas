/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/09/2013 14:50:02
 @author vtavares
 */

package br.com.ggas.atendimento.documentoimpressaolayout.dominio;

import java.util.Map;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação do layout de impressão do documento
 */
public class DocumentoImpressaoLayout extends EntidadeNegocioImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nome;

	private String descricao;

	private String nomeArquivo;

	private EntidadeConteudo tipoDocumentoImpressao;

	/**
	 * Instantiates a new documento impressao layout.
	 */
	public DocumentoImpressaoLayout() {

		super();
	}

	/**
	 * Instantiates a new documento impressao layout.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 */
	public DocumentoImpressaoLayout(Long chavePrimaria) {

		super();
		setChavePrimaria(chavePrimaria);

	}

	/**
	 * @return nome
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return nomeArquivo
	 */
	public String getNomeArquivo() {

		return nomeArquivo;
	}

	/**
	 * @param nomeArquivo
	 */
	public void setNomeArquivo(String nomeArquivo) {

		this.nomeArquivo = nomeArquivo;
	}

	/**
	 * @return tipoDocumentoImpressao
	 */
	public EntidadeConteudo getTipoDocumentoImpressao() {

		return tipoDocumentoImpressao;
	}

	/**
	 * @param tipoDocumentoImpressao
	 */
	public void setTipoDocumentoImpressao(EntidadeConteudo tipoDocumentoImpressao) {

		this.tipoDocumentoImpressao = tipoDocumentoImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
