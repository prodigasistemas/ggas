/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 23/09/2013 vpessoa 18:15:16
 */

package br.com.ggas.atendimento.documentoimpressaolayout.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * RepositorioDocumentoImpressaoLayout
 */
@Repository
public class RepositorioDocumentoImpressaoLayout extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio documento impressao layout.
	 * 
	 * @param sessionFactory the session factory
	 * 
	 */
	@Autowired
	public RepositorioDocumentoImpressaoLayout(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new DocumentoImpressaoLayout();
	}

	@Override
	public Class<DocumentoImpressaoLayout> getClasseEntidade() {

		return DocumentoImpressaoLayout.class;

	}

	/**
	 * Obter todos layouts ordenado.
	 * 
	 * @param habilitado the habilitado
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * 
	 */
	public Collection<DocumentoImpressaoLayout> obterTodosLayoutsOrdenado(Boolean habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if (habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		criteria.addOrder(Order.asc("nome"));

		return criteria.list();
	}

	/**
	 * Consultar documento impressao layout.
	 * 
	 * @param tipoDocumentoImpresso the tipo documento impresso
	 * @return the documento impressao layout
	 * @throws NegocioException the negocio exception
	 */
	public DocumentoImpressaoLayout consultarDocumentoImpressaoLayout(EntidadeConteudo tipoDocumentoImpresso) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if (tipoDocumentoImpresso != null) {
			criteria.add(Restrictions.eq("tipoDocumentoImpressao.chavePrimaria", tipoDocumentoImpresso.getChavePrimaria()));

			criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
			criteria.addOrder(Order.asc("nome"));

			return (DocumentoImpressaoLayout) criteria.uniqueResult();
		}

		return null;
	}
	
	
	/**
	 * Obter todos layouts pela entidade conteudo.
	 * @param tipoDocumentoImpresso the tipo documento impressão
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * 
	 */
	public Collection<DocumentoImpressaoLayout> obterTodosLayoutPelaEntidade(EntidadeConteudo tipoDocumentoImpresso) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" documentoImpressaoLayout ");
		hql.append(" WHERE ");

		hql.append(" documentoImpressaoLayout.tipoDocumentoImpressao.chavePrimaria = :chaveTipoDocumento ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveTipoDocumento", tipoDocumentoImpresso.getChavePrimaria());

		return query.list();
	}

}
