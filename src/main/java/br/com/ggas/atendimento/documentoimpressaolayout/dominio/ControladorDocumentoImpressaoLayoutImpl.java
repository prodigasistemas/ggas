/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 12/11/2014 17:14:08
 @author aantonio
 */

package br.com.ggas.atendimento.documentoimpressaolayout.dominio;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.documentoimpressaolayout.repositorio.RepositorioDocumentoImpressaoLayout;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;

@Service("controladorDocumentoImpressaoLayout")
@Transactional
class ControladorDocumentoImpressaoLayoutImpl implements ControladorDocumentoImpressaoLayout {

	@Autowired
	@Qualifier(value = "repositorioDocumentoImpressaoLayout")
	private RepositorioDocumentoImpressaoLayout repositorioDocumentoImpressaoLayout;

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(value = "controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout#obterDocumentoImpressaoLayoutPorConstante
	 * (java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public String obterDocumentoImpressaoLayoutPorConstante(String constante) throws NegocioException {

		if(constante != null && !constante.isEmpty()) {
			ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(constante);
			if(constanteSistema != null) {
				EntidadeConteudo tipoDocumentoImpresso = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteSistema
								.getValor()));
				DocumentoImpressaoLayout documentoImpressaoLayout = repositorioDocumentoImpressaoLayout
								.consultarDocumentoImpressaoLayout(tipoDocumentoImpresso);

				String arquivoRelatorio = null;

				if(documentoImpressaoLayout != null) {
					arquivoRelatorio = documentoImpressaoLayout.getNomeArquivo();
				}

				return arquivoRelatorio;

			}
		}

		return null;
	}
	
	
	@Override
	public Collection<DocumentoImpressaoLayout> obterDocumentosLayoutImpressaoPorConstante(String constante) throws NegocioException {
		if(constante != null && !constante.isEmpty()) {
			ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(constante);
			if(constanteSistema != null) {
				EntidadeConteudo tipoDocumentoImpresso = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteSistema
						.getValor()));
				
				if(tipoDocumentoImpresso != null) {
					return repositorioDocumentoImpressaoLayout.obterTodosLayoutPelaEntidade(tipoDocumentoImpresso);
				}
			}
		}
		
		return null;
	}

}
