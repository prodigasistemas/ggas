
package br.com.ggas.atendimento.servicotipoequipamento.repositorio;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Repositório responsável pelo tipo de equipamento usado no tipo de serviço 
 */
@Repository
public class RepositorioServicoTipoEquipamento extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio servico tipo equipamento.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioServicoTipoEquipamento(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ServicoTipoEquipamento();
	}

	@Override
	public Class<ServicoTipoEquipamento> getClasseEntidade() {

		return ServicoTipoEquipamento.class;
	}
}
