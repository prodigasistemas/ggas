package br.com.ggas.atendimento.questionario.dominio;

/**
 * 
 * Classe responsável por representar uma Situação do chamado
 *
 *
 */
public enum SituacaoQuestionario {
	NAO_POSSUI("Não Possui", 1), NAO_RESPONDIDO("Não Respondido", 2), RESPONDIDO("Respondido", 3);

	private String descricao;
	private Integer codigo;

	/**
	 * 
	 * @param descricao
	 * 
	 */
	private SituacaoQuestionario(String descricao, Integer codigo) {
		this.descricao = descricao;
		this.codigo = codigo;
		
	}
	
	/**
	 * Situação questionário pelo codigo
	 * 
	 * @param codigo {@link Integer}
	 * @return Situacao do questionario
	 */
	public static SituacaoQuestionario deCodigo(Integer codigo) {
		SituacaoQuestionario retorno = null;

		for (SituacaoQuestionario situacao : SituacaoQuestionario.values()) {
			if (situacao.getCodigo().equals(codigo)) {
				retorno = situacao;
				break;
			}
		}

		return retorno;
	}


	/**
	 * 
	 * @return descricao
	 * 
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @return codigo
	 */
	public Integer getCodigo() {
		return codigo;
	}
}
