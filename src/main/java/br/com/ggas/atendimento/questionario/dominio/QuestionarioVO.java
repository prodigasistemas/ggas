/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 09:22:48
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.dominio;

import java.io.Serializable;
import java.util.Collection;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.imovel.Segmento;
/**
 * Classe responsável pela representação de um objeto de valor relacionado a um questionário
 */
public class QuestionarioVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nomeQuestionario;

	private Segmento segmento;

	private ServicoTipo tipoServico;

	private Boolean habilitado;

	private Boolean tipoQuestionarioChamado;

	private Boolean tipoQuestionarioAS;

	private String codigoQuestionario;

	private Boolean comChamadoQuestionario;

	private String descricaoQuestionario;

	private String dataEncerramentoQuestionario;

	private String dataPrevisaoEncerramentoQuestionario;

	private String servicoTipoQuestionario;

	private String equipeQuestionario;

	private String protocoloChamadoQuestionario;

	private String statusQuestionario;

	private String nomeClienteQuestionario;

	private String nomeFantasiaClienteQuestionario;

	private String matriculaImovelQuestionario;

	private String nomeFantasiaImovelQuestionario;

	private String cepImovelQuestionario;

	private String enderecoImovelQuestionario;

	private String descricaoPontoConsumoQuestionario;

	private String dataGeracaoQuestionario;

	private String prioridadeServicoTipoQuestionario;

	private String nomePerguntaQuestionario;

	private boolean objetivaQuestionario;

	private String alternativaQuestionario;

	private String listaAlternativaQuestionario;

	private Integer index;

	private Integer notaMinimaQuestionario;

	private String notaPerguntaQuestionario;

	private String nomeRespostaExecutada;

	private Collection<QuestionarioAlternativa> nomeAlternativaQuestionario;

	/**
	 * Retorna nomeQuestionario
	 * 
	 * @return nomeQuestionario
	 */
	public String getNomeQuestionario() {
		return nomeQuestionario;
	}

	/**
	 * Atribui nomeQuestionario
	 * 
	 * @param nomeQuestionario
	 */
	public void setNomeQuestionario(String nomeQuestionario) {
		this.nomeQuestionario = nomeQuestionario;
	}

	/**
	 * Retorna tipoServico
	 * 
	 * @return tipoServico
	 */
	public ServicoTipo getTipoServico() {
		return tipoServico;
	}

	/**
	 * Atribui tipoServico
	 * 
	 * @param tipoServico
	 */
	public void setTipoServico(ServicoTipo tipoServico) {
		this.tipoServico = tipoServico;
	}

	/**
	 * Retorna segmento
	 * 
	 * @return segmento
	 */
	public Segmento getSegmento() {
		return segmento;
	}

	/**
	 * Atribui segmento
	 * 
	 * @param segmento
	 */
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	/**
	 * Retorna habilitado
	 * 
	 * @return habilitado
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}

	/**
	 * Atribui habilitado
	 * 
	 * @param habilitado
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	/**
	 * Retorna tipoQuestionarioChamado
	 * 
	 * @return tipoQuestionarioChamado
	 */
	public Boolean getTipoQuestionarioChamado() {
		return tipoQuestionarioChamado;
	}

	/**
	 * Atribui tipoQuestionarioChamado
	 * 
	 * @param tipoQuestionarioChamado
	 */
	public void setTipoQuestionarioChamado(Boolean tipoQuestionarioChamado) {
		this.tipoQuestionarioChamado = tipoQuestionarioChamado;
	}

	/**
	 * Retorna tipoQuestionarioAS
	 * 
	 * @return tipoQuestionarioAS
	 */
	public Boolean getTipoQuestionarioAS() {
		return tipoQuestionarioAS;
	}

	/**
	 * Atribui tipoQuestionarioAS
	 * 
	 * @param tipoQuestionarioAS
	 */
	public void setTipoQuestionarioAS(Boolean tipoQuestionarioAS) {
		this.tipoQuestionarioAS = tipoQuestionarioAS;
	}

	/**
	 * Retorna codigoQuestionario
	 * 
	 * @return codigoQuestionario
	 */
	public String getCodigoQuestionario() {
		return codigoQuestionario;
	}

	/**
	 * Atribui codigoQuestionario
	 * 
	 * @param codigoQuestionario
	 */
	public void setCodigoQuestionario(String codigoQuestionario) {
		this.codigoQuestionario = codigoQuestionario;
	}

	/**
	 * Retorna comChamadoQuestionario
	 * 
	 * @return comChamadoQuestionario
	 */
	public Boolean getComChamadoQuestionario() {
		return comChamadoQuestionario;
	}

	/**
	 * Atribui comChamadoQuestionario
	 * 
	 * @param comChamadoQuestionario
	 */
	public void setComChamadoQuestionario(Boolean comChamadoQuestionario) {
		this.comChamadoQuestionario = comChamadoQuestionario;
	}

	/**
	 * Retorna descricaoQuestionario
	 * 
	 * @return descricaoQuestionario
	 */
	public String getDescricaoQuestionario() {
		return descricaoQuestionario;
	}

	/**
	 * Atribui DescricaoQuestionario
	 * 
	 * @param descricaoQuestionario
	 */
	public void setDescricaoQuestionario(String descricaoQuestionario) {
		this.descricaoQuestionario = descricaoQuestionario;
	}

	/**
	 * Retorna encerramentoQuestionario
	 * 
	 * @return encerramentoQuestionario
	 */
	public String getDataEncerramentoQuestionario() {
		return dataEncerramentoQuestionario;
	}

	/**
	 * Atribui dataEncerramentoQuestionario
	 * 
	 * @param dataEncerramentoQuestionario
	 */
	public void setDataEncerramentoQuestionario(String dataEncerramentoQuestionario) {
		this.dataEncerramentoQuestionario = dataEncerramentoQuestionario;
	}

	/**
	 * Retorna dataPrevisaoEncerramentoQuestionario
	 * 
	 * @return dataPrevisaoEncerramentoQuestionario
	 */
	public String getDataPrevisaoEncerramentoQuestionario() {
		return dataPrevisaoEncerramentoQuestionario;
	}

	/**
	 * Atribui dataPrevisaoEncerramentoQuestionario
	 * 
	 * @param dataPrevisaoEncerramentoQuestionario
	 */
	public void setDataPrevisaoEncerramentoQuestionario(String dataPrevisaoEncerramentoQuestionario) {
		this.dataPrevisaoEncerramentoQuestionario = dataPrevisaoEncerramentoQuestionario;
	}

	/**
	 * Retorna servicoTipoQuestionario
	 * 
	 * @return servicoTipoQuestionario
	 */
	public String getServicoTipoQuestionario() {
		return servicoTipoQuestionario;
	}

	/**
	 * Atribui servicoTipoQuestionario
	 * 
	 * @param servicoTipoQuestionario
	 */
	public void setServicoTipoQuestionario(String servicoTipoQuestionario) {
		this.servicoTipoQuestionario = servicoTipoQuestionario;
	}

	/**
	 * Retorna equipeQuestionario
	 * 
	 * @return equipeQuestionario
	 */
	public String getEquipeQuestionario() {
		return equipeQuestionario;
	}

	/**
	 * Atribui equipeQuestionario
	 * 
	 * @param equipeQuestionario
	 */
	public void setEquipeQuestionario(String equipeQuestionario) {
		this.equipeQuestionario = equipeQuestionario;
	}

	/**
	 * Retorna protocoloChamadoQuestionario
	 * 
	 * @return protocoloChamadoQuestionario
	 */
	public String getProtocoloChamadoQuestionario() {
		return protocoloChamadoQuestionario;
	}

	/**
	 * Atribui protocoloChamadoQuestionario
	 * 
	 * @param protocoloChamadoQuestionario
	 */
	public void setProtocoloChamadoQuestionario(String protocoloChamadoQuestionario) {
		this.protocoloChamadoQuestionario = protocoloChamadoQuestionario;
	}

	/**
	 * Retorna statusQuestionario
	 * 
	 * @return statusQuestionario
	 */
	public String getStatusQuestionario() {
		return statusQuestionario;
	}

	/**
	 * Atribui statusQuestionario
	 * 
	 * @param statusQuestionario
	 */
	public void setStatusQuestionario(String statusQuestionario) {
		this.statusQuestionario = statusQuestionario;
	}

	/**
	 * retorna nomeClienteQuestionario
	 * 
	 * @return nomeClienteQuestionario
	 */
	public String getNomeClienteQuestionario() {
		return nomeClienteQuestionario;
	}

	/**
	 * Atribui nomeClienteQuestionario
	 * 
	 * @param nomeClienteQuestionario
	 */
	public void setNomeClienteQuestionario(String nomeClienteQuestionario) {
		this.nomeClienteQuestionario = nomeClienteQuestionario;
	}

	/**
	 * Retorna nomeFantasiaClienteQuestionario
	 * 
	 * @return nomeFantasiaClienteQuestionario
	 */
	public String getNomeFantasiaClienteQuestionario() {
		return nomeFantasiaClienteQuestionario;
	}

	/**
	 * Atribui nomeFantasiaClienteQuestionario
	 * 
	 * @param nomeFantasiaClienteQuestionario
	 */
	public void setNomeFantasiaClienteQuestionario(String nomeFantasiaClienteQuestionario) {
		this.nomeFantasiaClienteQuestionario = nomeFantasiaClienteQuestionario;
	}

	/**
	 * Retorna matriculaImovelQuestionario
	 * 
	 * @return matriculaImovelQuestionario
	 */
	public String getMatriculaImovelQuestionario() {
		return matriculaImovelQuestionario;
	}

	/**
	 * Atribui matriculaImovelQuestionario
	 * 
	 * @param matriculaImovelQuestionario
	 */
	public void setMatriculaImovelQuestionario(String matriculaImovelQuestionario) {
		this.matriculaImovelQuestionario = matriculaImovelQuestionario;
	}

	/**
	 * Retorna nomeFantasiaImovelQuestionario
	 * 
	 * @return nomeFantasiaImovelQuestionario
	 */
	public String getNomeFantasiaImovelQuestionario() {
		return nomeFantasiaImovelQuestionario;
	}

	/**
	 * Atribui nomeFantasiaImovelQuestionario
	 * 
	 * @param nomeFantasiaImovelQuestionario
	 */
	public void setNomeFantasiaImovelQuestionario(String nomeFantasiaImovelQuestionario) {
		this.nomeFantasiaImovelQuestionario = nomeFantasiaImovelQuestionario;
	}

	/**
	 * Retorna cepImovelQuestionario
	 * 
	 * @return cepImovelQuestionario
	 */
	public String getCepImovelQuestionario() {
		return cepImovelQuestionario;
	}

	/**
	 * Atribui cepImovelQuestionario
	 * 
	 * @param cepImovelQuestionario
	 */
	public void setCepImovelQuestionario(String cepImovelQuestionario) {
		this.cepImovelQuestionario = cepImovelQuestionario;
	}

	/**
	 * Retorna enderecoImovelQuestionario
	 * 
	 * @return enderecoImovelQuestionario
	 */
	public String getEnderecoImovelQuestionario() {
		return enderecoImovelQuestionario;
	}

	/**
	 * Atribui enderecoImovelQuestionario
	 * 
	 * @param enderecoImovelQuestionario
	 */
	public void setEnderecoImovelQuestionario(String enderecoImovelQuestionario) {
		this.enderecoImovelQuestionario = enderecoImovelQuestionario;
	}

	/**
	 * Retorna descricaoPontoConsumoQuestionario
	 * 
	 * @return descricaoPontoConsumoQuestionario
	 */
	public String getDescricaoPontoConsumoQuestionario() {
		return descricaoPontoConsumoQuestionario;
	}

	/**
	 * Atribui descricaoPontoConsumoQuestionario
	 * 
	 * @param descricaoPontoConsumoQuestionario
	 */
	public void setDescricaoPontoConsumoQuestionario(String descricaoPontoConsumoQuestionario) {
		this.descricaoPontoConsumoQuestionario = descricaoPontoConsumoQuestionario;
	}

	/**
	 * Retorna dataGeracaoQuestionario
	 * 
	 * @return dataGeracaoQuestionario
	 */
	public String getDataGeracaoQuestionario() {
		return dataGeracaoQuestionario;
	}

	/**
	 * Atribui dataGeracaoQuestionario
	 * 
	 * @param dataGeracaoQuestionario
	 */
	public void setDataGeracaoQuestionario(String dataGeracaoQuestionario) {
		this.dataGeracaoQuestionario = dataGeracaoQuestionario;
	}

	/**
	 * Retorna prioridadeServicoTipoQuestionario
	 * 
	 * @return prioridadeServicoTipoQuestionario
	 */
	public String getPrioridadeServicoTipoQuestionario() {
		return prioridadeServicoTipoQuestionario;
	}

	/**
	 * Atribui prioridadeServicoTipoQuestionario
	 * 
	 * @param prioridadeServicoTipoQuestionario
	 */
	public void setPrioridadeServicoTipoQuestionario(String prioridadeServicoTipoQuestionario) {
		this.prioridadeServicoTipoQuestionario = prioridadeServicoTipoQuestionario;
	}

	/**
	 * Retorna nomePerguntaQuestionario
	 * 
	 * @return nomePerguntaQuestionario
	 */
	public String getNomePerguntaQuestionario() {
		return nomePerguntaQuestionario;
	}

	/**
	 * Atribui nomePerguntaQuestionario
	 * 
	 * @param nomePerguntaQuestionario
	 */
	public void setNomePerguntaQuestionario(String nomePerguntaQuestionario) {
		this.nomePerguntaQuestionario = nomePerguntaQuestionario;
	}

	/**
	 * Retorna objetivaQuestionario
	 * 
	 * @return objetivaQuestionario
	 */
	public boolean isObjetivaQuestionario() {
		return objetivaQuestionario;
	}

	/**
	 * Atribui objetivaQuestionario
	 * 
	 * @param objetivaQuestionario
	 */
	public void setObjetivaQuestionario(boolean objetivaQuestionario) {
		this.objetivaQuestionario = objetivaQuestionario;
	}

	/**
	 * Retorna index
	 * 
	 * @return index
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * Atribui index
	 * 
	 * @param index
	 */
	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * Retorna notaMinimaQuestionario
	 * 
	 * @return notaMinimaQuestionario
	 */
	public Integer getNotaMinimaQuestionario() {
		return notaMinimaQuestionario;
	}

	/**
	 * Atribui notaMinimaQuestionario
	 * 
	 * @param notaMinimaQuestionario
	 */
	public void setNotaMinimaQuestionario(Integer notaMinimaQuestionario) {
		this.notaMinimaQuestionario = notaMinimaQuestionario;
	}

	/**
	 * Retorna listaAlternativaQuestionario
	 * 
	 * @return listaAlternativaQuestionario
	 */
	public String getListaAlternativaQuestionario() {
		return listaAlternativaQuestionario;
	}

	/**
	 * Atribui listaAlternativaQuestionario
	 * 
	 * @param listaAlternativaQuestionario
	 */
	public void setListaAlternativaQuestionario(String listaAlternativaQuestionario) {
		this.listaAlternativaQuestionario = listaAlternativaQuestionario;
	}

	/**
	 * Retorna nomeAlternativaQuestionario
	 * 
	 * @return nomeAlternativaQuestionario
	 */
	public Collection<QuestionarioAlternativa> getNomeAlternativaQuestionario() {
		return nomeAlternativaQuestionario;
	}

	/**
	 * Atribui nomeAlternativaQuestionario
	 * 
	 * @param nomeAlternativaQuestionario
	 */
	public void setNomeAlternativaQuestionario(Collection<QuestionarioAlternativa> nomeAlternativaQuestionario) {
		this.nomeAlternativaQuestionario = nomeAlternativaQuestionario;
	}

	/**
	 * Retorna alternativaQuestionario
	 * 
	 * @return alternativaQuestionario
	 */
	public String getAlternativaQuestionario() {
		return alternativaQuestionario;
	}

	/**
	 * Atribui alternativaQuestionario
	 * 
	 * @param alternativaQuestionario
	 */
	public void setAlternativaQuestionario(String alternativaQuestionario) {
		this.alternativaQuestionario = alternativaQuestionario;
	}

	/**
	 * Retorna notaPerguntaQuestionario
	 * 
	 * @return notaPerguntaQuestionario
	 */
	public String getNotaPerguntaQuestionario() {
		return notaPerguntaQuestionario;
	}

	/**
	 * Atribui notaPerguntaQuestionario
	 * 
	 * @param notaPerguntaQuestionario
	 */
	public void setNotaPerguntaQuestionario(String notaPerguntaQuestionario) {
		this.notaPerguntaQuestionario = notaPerguntaQuestionario;
	}

	public String getNomeRespostaExecutada() {
		return nomeRespostaExecutada;
	}

	public void setNomeRespostaExecutada(String nomeRespostaExecutada) {
		this.nomeRespostaExecutada = nomeRespostaExecutada;
	}



}
