/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *26/02/2014
 * vpessoa
 * 16:58:03
 */

package br.com.ggas.atendimento.questionario.batch;

import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * GerarPesquisaSatisfacaoLoteBatch
 */
@Component
public class GerarPesquisaSatisfacaoLoteBatch implements Batch {

	private static final String PROCESSO = "processo";

	private static final String ID_COMANDO = "idComando";

	private ControladorAcaoComando controladorAcaoComando;

	private ControladorQuestionario controladorQuestionario;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		controladorAcaoComando = ServiceLocator.getInstancia().getControladorAcaoComando();
		Processo processo = (Processo) parametros.get(PROCESSO);
		AcaoComando acaoComando = null;
		if(parametros.containsKey(ID_COMANDO)) {
			acaoComando = controladorAcaoComando.obterAcaoComando(Long.valueOf(parametros.get(ID_COMANDO).toString()), "acao",
							"grupoFaturamento", "localidade", "setorComercial", "municipio", "marcaMedidor", "modeloMedidor",
							"marcaCorretor", "modeloCorretor", "situacaoConsumo", "segmento", "acao.servicoTipo.servicoTipoPrioridade",
							"acao.servicoTipo.listaEquipamentoEspecial", "acao.servicoTipo.listaMateriais",
							"acao.servicoTipo.indicadorGeraLote", "acao.servicoTipoPesquisa.indicadorPesquisaSatisfacao",
							"acao.chamadoAssuntoPesquisa.indicadorGeraServicoAutorizacao", "acao.chamadoAssuntoPesquisa");
		}

		StringBuilder logProcessamento = new StringBuilder();

		try {
			Map<String, Object> dados = new HashMap<>();

			dados.put("usuario", processo.getUsuario());
			controladorQuestionario = ServiceLocator.getInstancia().getControladorQuestionario();
			controladorQuestionario.gerarChamadoPesquisaSatisfacaoLote(acaoComando, logProcessamento, dados);

		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}
}
