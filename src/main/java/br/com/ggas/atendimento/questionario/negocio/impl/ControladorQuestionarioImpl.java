/*
  Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 10:52:03
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.negocio.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamado.repositorio.RepositorioChamado;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.questionario.repositorio.RepositorioQuestionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.resposta.negocio.ControladorResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;

/**
 * ControladorQuestionarioImpl
 */
@Service("controladorQuestionario")
@Transactional
public class ControladorQuestionarioImpl implements ControladorQuestionario {
	
	private static final Logger LOG = Logger.getLogger(ControladorQuestionarioImpl.class);

	@Autowired
	private RepositorioQuestionario repositorioQuestionario;

	@Autowired
	private RepositorioChamado repositorioChamado;

	@Autowired
	private ControladorChamado controladorChamado;

	@Autowired
	private ControladorResposta controladorResposta;

	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;

	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(value = "controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier(value = "controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#consultarQuestionario(br.com.ggas.atendimento.questionario.dominio
	 * .
	 * QuestionarioVO)
	 */
	@Override
	public Collection<Questionario> consultarQuestionario(QuestionarioVO questionarioVO) throws NegocioException {

		return repositorioQuestionario.consultarQuestionario(questionarioVO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#consultarQuestionarioAssociadosServicoTipo(br.com.ggas.atendimento
	 * .servicotipo
	 * .dominio.ServicoTipo)
	 */
	@Override
	public Collection<Questionario> consultarQuestionarioAssociadosServicoTipo(ServicoTipo servicoTipo) throws NegocioException {

		QuestionarioVO questionarioVO = new QuestionarioVO();
		questionarioVO.setTipoServico(servicoTipo);

		return repositorioQuestionario.consultarQuestionario(questionarioVO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#incluirQuestionario(br.com.ggas.atendimento.questionario.dominio.
	 * Questionario
	 * )
	 */
	@Transactional
	@Override
	public void incluirQuestionario(Questionario questionario) throws NegocioException {

		repositorioQuestionario.validarDadosEntidade(questionario);
		verificarQuestionarioExistente(questionario);
		if(questionario.getListaPerguntas() == null || questionario.getListaPerguntas().isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA, true);
		}
		repositorioQuestionario.inserir(questionario);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#consultarPorCodico(java.lang.Long, java.lang.String[])
	 */
	@Override
	public Questionario consultarPorCodico(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Questionario) repositorioQuestionario.obter(chavePrimaria, propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#removerQuestionario(br.com.ggas.atendimento.questionario.dominio.
	 * Questionario
	 * )
	 */
	@Transactional
	@Override
	public void removerQuestionario(Questionario questionario) throws NegocioException {

		verificarExclusaoQuestionario(questionario);
		repositorioQuestionario.remover(questionario);

	}

	/**
	 * Metodo que realiza a verificação se o questionario pode ser excluido fisicamente ou não.
	 * 
	 * @param questionario
	 *            the questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarExclusaoQuestionario(Questionario questionario) throws NegocioException {

		if(controladorChamado.existeChamadoQuestionario(questionario)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REMOCAO_QUESTIONARIO, "Chamado");
		} else if (controladorChamadoAssunto.existeChamadoAssuntoQuestionario(questionario)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REMOCAO_QUESTIONARIO, "Chamado Assunto");
		} else if (controladorServicoAutorizacao.existeASQuestionario(questionario)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REMOCAO_QUESTIONARIO, Constantes.TIPO_SERVICO);
		}

	}

	/**
	 * Metodo que verifica se existe um questionario cadastrado no banco.
	 * 
	 * @param questionario
	 *            the questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarQuestionarioExistente(Questionario questionario) throws NegocioException {

		if(repositorioQuestionario.existenteQuestionario(questionario.getNomeQuestionario()) != null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ENTIDADE_CADASTRADA, Constantes.QUESTIONARIO);
		}
	}

	/**
	 * Metodo que verifica se exixte a pergunta existe em uma determinada lista.
	 * 
	 * @param pergunta
	 *            the pergunta
	 * @param listaPerguntaAtual
	 *            the lista pergunta atual
	 * @param indexAtual
	 *            the index atual
	 * @throws NegocioException
	 *             the negocio exception
	 * @parami indexAtual
	 */
	@Override
	public void verificarSeExistePergunta(QuestionarioPergunta pergunta, Collection<QuestionarioPergunta> listaPerguntaAtual,
					Integer indexAtual) throws NegocioException {

		List<QuestionarioPergunta> lista = new ArrayList<>(listaPerguntaAtual);
		// verifica se a pergunta existe
		if(lista.contains(pergunta)) {
			// caso o indexAtual for nulo siginifica e a perunta que esta tendando adicionar e nova e como existe na lista e
			// leventada um NegocioException
			if(indexAtual != null) {
				// caso o indexAtual não for nullo significa que esta havendo uma alteração de alguna
				// pergunta da lista com isso e verificado e o index de alteração e diferente
				if(lista.indexOf(pergunta) != indexAtual) {
					lista = null;
					throw new NegocioException(Constantes.ERRO_NEGOCIO_ENTIDADE_CADASTRADA, Constantes.PERGUNTA);
				}
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ENTIDADE_CADASTRADA, Constantes.PERGUNTA);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#alterarQuestionario(br.com.ggas.atendimento.questionario.dominio.
	 * Questionario
	 * , java.util.List)
	 */
	@Transactional
	@Override
	public void alterarQuestionario(Questionario questionario, List<Long> listaIdsPerguntaRemovido) throws NegocioException,
					ConcorrenciaException {

		if(listaIdsPerguntaRemovido != null && !listaIdsPerguntaRemovido.isEmpty()) {
			for (Long long1 : listaIdsPerguntaRemovido) {
				repositorioQuestionario.remover(repositorioQuestionario.obter(long1, QuestionarioPergunta.class),
								QuestionarioPergunta.class);
			}
		}

		Collection<QuestionarioPergunta> listaPerguntas = questionario.getListaPerguntas();
		for (QuestionarioPergunta pergunta : listaPerguntas) {
			Collection<QuestionarioAlternativa> listaAlternativa = repositorioQuestionario.obterAlternativas(pergunta.getChavePrimaria());
			for (QuestionarioAlternativa alternativa : listaAlternativa) {
				repositorioQuestionario.remover(alternativa, QuestionarioAlternativa.class);
			}
		}

		repositorioQuestionario.atualizar(questionario);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#consultarQuestionarioPorTipoServico(br.com.ggas.atendimento.
	 * servicoautorizacao
	 * .dominio.ServicoAutorizacao, br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public Questionario consultarQuestionarioPorTipoServico(ServicoAutorizacao servicoAutorizacao, Segmento segmento)
					throws NegocioException {

		Questionario questionario = repositorioQuestionario.consultarQuestionarioPorTipoServico(servicoAutorizacao, segmento);
		if(questionario == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_QUESTIONARIO_NAO_EXISTE, true);
		}

		return questionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#validarExclusaoPergunta(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void validarExclusaoPergunta(Long chavePrimariaPergunta, Long chavePrimariaQuestionario) throws NegocioException {

		this.verificarQuestionarioRespondido(chavePrimariaQuestionario);
		if(controladorResposta.existeResposta(chavePrimariaPergunta)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_REMOCAO_PERGUNTA);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#gerarChamadoPesquisaSatisfacaoLote(br.com.ggas.batch.acaocomando.
	 * dominio
	 * .AcaoComando, java.lang.StringBuilder, java.util.Map)
	 */
	@Override
	public void gerarChamadoPesquisaSatisfacaoLote(AcaoComando acaoComando, StringBuilder logProcessamento, Map<String, Object> dados)
					throws NegocioException {

		Acao acao = acaoComando.getAcao();
		Chamado chamado = null;
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_ABERTO);
		EntidadeConteudo statusAberto = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CANAL_ATENDIMENTO_PESQUISA);
		CanalAtendimento canal = (CanalAtendimento) controladorUnidadeOrganizacional.obter(Long.parseLong(constante.getValor()),
						CanalAtendimentoImpl.class);
		int contadorGeradas = 0;
		int contadorNaoGeradas = 0;
		if(acao.getIndicadorGerarChamadoPesquisa()) {
			if(acao.getServicoTipoPesquisa().getIndicadorPesquisaSatisfacao()) {
				Collection<ServicoAutorizacao> listaServicoAutorizacao = controladorServicoAutorizacao
								.consultarServicoAutorizacaoPorComandoAcao(acaoComando);
				for (ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacao) {
					chamado = new Chamado();
					chamado.setChamadoAssunto(acao.getChamadoAssuntoPesquisa());
					chamado.setChamadoTipo(chamado.getChamadoAssunto().getChamadoTipo());
					chamado.setUnidadeOrganizacional(acao.getUnidadeOrganizacionalPesquisa());
					chamado.setQuestionario(acao.getQuestionario());
					chamado.setIndicadorPesquisa(Boolean.TRUE);
					chamado.setDescricao("Pesquisa de Satisfação em Lote");
					chamado.setCanalAtendimento(canal);
					
					adicionaClienteContratoEImovel(chamado, servicoAutorizacao);
					
					chamado.setPontoConsumo(servicoAutorizacao.getPontoConsumo());
					chamado.setStatus(statusAberto);
					ChamadoProtocolo protocolo = controladorChamado.gerarProtocolo(null);
					chamado.setProtocolo(protocolo);

					try {

						this.repositorioChamado.validarDadosEntidade(chamado);

						if (!acaoComando.getIndicadorSimulacao()) {
							this.controladorChamado.inserirChamado(chamado, Boolean.FALSE, null, dados, null);
							servicoAutorizacao.setChamadoTipoPesquisa(chamado);
							this.controladorServicoAutorizacao.atualizarSemValidar(servicoAutorizacao);
						}

						logProcessamento.append(Constantes.PULA_LINHA);
						logProcessamento.append("Chamado de pesquisa gerado para o ponto de consumo: ");
						logProcessamento.append(chamado.getPontoConsumo().getDescricaoFormatada());
						contadorGeradas++;

					} catch (NegocioException e) {

						LOG.error(e.getMessage(), e);
						preencheLogProcessamento(logProcessamento, chamado.getPontoConsumo().getDescricaoFormatada(), e);
						contadorNaoGeradas++;

					} catch (ConcorrenciaException e) {

						LOG.error(e.getMessage(), e);
						preencheLogProcessamento(logProcessamento, chamado.getPontoConsumo().getDescricaoFormatada(), e);

					} catch (GGASException e) {

						LOG.error(e.getMessage(), e);
						preencheLogProcessamento(logProcessamento, chamado.getPontoConsumo().getDescricaoFormatada(), e);
						contadorNaoGeradas++;
					}
				}
				
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Total de autorizações de serviço processados: ");
				logProcessamento.append(listaServicoAutorizacao.size());
				
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Total de chamados gerados: ");
				logProcessamento.append(contadorGeradas);
				
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Total de chamados não gerados: ");
				logProcessamento.append(contadorNaoGeradas);
				
			} else {
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Tipo de Serviço não permite gerar pesquisa de satisfação em lote.");
			}
		} else {
			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("Ação não permite gerar pesquisa de satisfação em lote.");
		}
	}
	
	/**
	 * Método responsável por adicionar cliente, contrato e imóvel.
	 * 
	 * @param chamado
	 * @param servicoAutorizacao
	 */
	private void adicionaClienteContratoEImovel(Chamado chamado, ServicoAutorizacao servicoAutorizacao) {
		if(servicoAutorizacao.getCliente() != null) {
			chamado.setCliente(servicoAutorizacao.getCliente());
		}
		if(servicoAutorizacao.getContrato() != null) {
			chamado.setContrato(servicoAutorizacao.getContrato());
		}
		if(servicoAutorizacao.getImovel() != null) {
			chamado.setImovel(servicoAutorizacao.getImovel());
		}
	}
	
	private void preencheLogProcessamento(StringBuilder logProcessamento, String descricaoFormatada, GGASException e) {

		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append("Chamado de pesquisa não gerado para o ponto de consumo: ");
		logProcessamento.append(descricaoFormatada);
		logProcessamento.append(". Motivo: ");
		logProcessamento.append(MensagemUtil.obterMensagem(ResourceBundle.getBundle("mensagens"), e.getChaveErro(), e.getParametrosErro()));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#verificarQuestionarioRespondido(java.lang.Long)
	 */
	@Override
	public void verificarQuestionarioRespondido(Long chavePrimariaQuestionario) throws NegocioException {

		if(chavePrimariaQuestionario != null && chavePrimariaQuestionario > 0
						&& controladorResposta.questionarioRespondido(chavePrimariaQuestionario)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_QUESTIONARIO_RESPONDIDO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#obterRespostas(long, long)
	 */
	@Override
	public Collection<QuestionarioPerguntaResposta> obterRespostas(long chavePrimariaQuestionario, long chavePrimariaChamado) {
		return repositorioQuestionario.obterRespostas(chavePrimariaQuestionario, chavePrimariaChamado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#obterRespostasFormulario(long, long)
	 */
	@Override
	public Collection<QuestionarioPerguntaResposta> obterRespostasFormulario(long chavePrimariaQuestionario,
			long chavePrimariaServicoAutorizacaoHistorico) {
		return repositorioQuestionario.obterRespostasFormulario(chavePrimariaQuestionario, chavePrimariaServicoAutorizacaoHistorico);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#obterAlternativas(long)
	 */
	@Override
	public Collection<QuestionarioAlternativa> obterAlternativas(long chavePrimariaPergunta) {
		return repositorioQuestionario.obterAlternativas(chavePrimariaPergunta);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#obterAlternativas(java.lang.Long[])
	 */
	@Override
	public Collection<QuestionarioAlternativa> obterAlternativas(Long[] listaIdAlternativa) {
		return repositorioQuestionario.obterAlternativas(listaIdAlternativa);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario#obterPergunta(java.lang.Long)
	 */
	@Override
	public QuestionarioPergunta obterPergunta(Long chavePrimaria) {
		return repositorioQuestionario.obterPergunta(chavePrimaria);
	}

}
