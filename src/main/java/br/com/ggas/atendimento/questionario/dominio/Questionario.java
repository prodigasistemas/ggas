/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 22/01/2014 16:47:04
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe Questionario
 * 
 * @author Procenge
 */
public class Questionario extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 1285331903219778841L;

	private String nomeQuestionario;

	private ServicoTipo tipoServico;

	private Segmento segmento;

	private Boolean tipoQuestionarioChamado;

	private Boolean tipoQuestionarioAS;

	private QuestionarioPergunta pergunta;

	private Collection<QuestionarioPergunta> listaPerguntas = new HashSet<>();

	private Collection<QuestionarioAlternativa> listaAlternativas = new HashSet<>();


	/**
	 * Retorna nomeQuestionario
	 * 
	 * @return nomeQuestionario
	 */
	public String getNomeQuestionario() {

		return nomeQuestionario;
	}

	/**
	 * Atribui nomeQuestionario
	 * 
	 * @param nomeQuestionario
	 */
	public void setNomeQuestionario(String nomeQuestionario) {

		this.nomeQuestionario = nomeQuestionario;
	}

	/**
	 * Retorna tipoServico
	 * 
	 * @return tipoServico
	 */
	public ServicoTipo getTipoServico() {

		return tipoServico;
	}

	/**
	 * Atribui tipoServico
	 * 
	 * @param tipoServico
	 */
	public void setTipoServico(ServicoTipo tipoServico) {

		this.tipoServico = tipoServico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.nomeQuestionario == null || this.nomeQuestionario.trim().isEmpty()) {
			stringBuilder.append(Constantes.CAMPO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if ((this.tipoQuestionarioAS == null || !this.tipoQuestionarioAS)
				&& (this.tipoQuestionarioChamado == null || !this.tipoQuestionarioChamado)) {
			stringBuilder.append("Tipo Questionário");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * Retorna segmento
	 * 
	 * @return segmento
	 */
	public Segmento getSegmento() {

		return segmento;
	}

	/**
	 * Atribui segmento
	 * 
	 * @param segmento
	 */
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/**
	 * Retorna listaPerguntas
	 * 
	 * @return listaPerguntas
	 */
	public Collection<QuestionarioPergunta> getListaPerguntas() {

		return listaPerguntas;
	}

	/**
	 * Atribui listaPerguntas
	 * 
	 * @param listaPerguntas
	 */
	public void setListaPerguntas(Collection<QuestionarioPergunta> listaPerguntas) {

		this.listaPerguntas = listaPerguntas;
	}

	/**
	 * Retorna o total de perguntas
	 * 
	 * @return total de perguntas
	 */
	public int getTotalPerguntas() {

		if(this.listaPerguntas != null) {
			return listaPerguntas.size();
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		
		if (nomeQuestionario == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + nomeQuestionario.hashCode();
		}
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Questionario other = (Questionario) obj;
		if (nomeQuestionario == null) {
			if (other.nomeQuestionario != null) {
				return false;
			}
		} else if (!nomeQuestionario.trim().equals(other.nomeQuestionario.trim())) {
			return false;
		}
		return true;
	}

	/**
	 * Retorna tipoQuestionarioChamado
	 * 
	 * @return tipoQuestionarioChamado
	 */
	public Boolean getTipoQuestionarioChamado() {
		return tipoQuestionarioChamado;
	}

	/**
	 * Atribui tipoQuestionarioChamado
	 * 
	 * @param tipoQuestionarioChamado
	 */
	public void setTipoQuestionarioChamado(Boolean tipoQuestionarioChamado) {
		this.tipoQuestionarioChamado = tipoQuestionarioChamado;
	}

	/**
	 * Retorna tipoQuestionarioAS
	 * 
	 * @return tipoQuestionarioAS
	 */
	public Boolean getTipoQuestionarioAS() {
		return tipoQuestionarioAS;
	}

	/**
	 * Atribui tipoQuestionarioAS
	 * 
	 * @param tipoQuestionarioAS
	 */
	public void setTipoQuestionarioAS(Boolean tipoQuestionarioAS) {
		this.tipoQuestionarioAS = tipoQuestionarioAS;
	}

	public Collection<QuestionarioAlternativa> getListaAlternativas() {
		return listaAlternativas;
	}

	public void setListaAlternativas(Collection<QuestionarioAlternativa> listaAlternativas) {
		this.listaAlternativas = listaAlternativas;
	}

	public QuestionarioPergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(QuestionarioPergunta pergunta) {
		this.pergunta = pergunta;
	}


}
