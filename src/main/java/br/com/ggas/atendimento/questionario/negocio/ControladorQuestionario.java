/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 10:49:50
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.negocio;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;

import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
/**
 * Controlador responsável por ações relacionadas a um questionário
 * 
 */
public interface ControladorQuestionario {

	/**
	 * Consultar questionario.
	 * 
	 * @param questionaVo
	 *            the questiona vo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Questionario> consultarQuestionario(QuestionarioVO questionaVo) throws NegocioException;

	/**
	 * Incluir questionario.
	 * 
	 * @param questionario
	 *            the questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void incluirQuestionario(Questionario questionario) throws NegocioException;

	/**
	 * Consultar por codico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Questionario consultarPorCodico(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Alterar questionario.
	 * 
	 * @param questionario
	 *            the questionario
	 * @param lista
	 *            the lista
	 * @throws GGASException
	 *             the GGASException exception
	 */
	void alterarQuestionario(Questionario questionario, List<Long> lista) throws GGASException;

	/**
	 * Remover questionario.
	 * 
	 * @param questionario
	 *            the questionario
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConstraintViolationException
	 *             the constraint violation exception
	 */
	void removerQuestionario(Questionario questionario) throws NegocioException, ConstraintViolationException;

	/**
	 * Verificar se existe pergunta.
	 * 
	 * @param pergunta
	 *            the pergunta
	 * @param listaPerguntaAtual
	 *            the lista pergunta atual
	 * @param indexAtual
	 *            the index atual
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarSeExistePergunta(QuestionarioPergunta pergunta, Collection<QuestionarioPergunta> listaPerguntaAtual, Integer indexAtual)
					throws NegocioException;

	/**
	 * Consultar questionario por tipo servico.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param segmento
	 *            the segmento
	 * @return the questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Questionario consultarQuestionarioPorTipoServico(ServicoAutorizacao servicoAutorizacao, Segmento segmento) throws NegocioException;

	/**
	 * Validar exclusao pergunta.
	 * 
	 * @param chavePrimariaPergunta
	 *            the chave primaria pergunta
	 * @param chavePrimariaQuestionario
	 *            the chave primaria questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarExclusaoPergunta(Long chavePrimariaPergunta, Long chavePrimariaQuestionario) throws NegocioException;

	/**
	 * Gerar chamado pesquisa satisfacao lote.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @param logProcessamento
	 *            the log processamento
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void gerarChamadoPesquisaSatisfacaoLote(AcaoComando acaoComando, StringBuilder logProcessamento, Map<String, Object> dados)
					throws NegocioException;

	/**
	 * Verificar questionario respondido.
	 * 
	 * @param chavePrimariaQuestionario
	 *            the chave primaria questionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarQuestionarioRespondido(Long chavePrimariaQuestionario) throws NegocioException;

	/**
	 * Consultar questionario associados servico tipo.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Questionario> consultarQuestionarioAssociadosServicoTipo(ServicoTipo servicoTipo) throws NegocioException;

	/**
	 * Lista as respostas do questionario para o chamado
	 * 
	 * @param chavePrimariaQuestionario {@link long}
	 * @param chavePrimariaChamado {@link long}
	 * 
	 * @return respostas {@link Collection}
	 */
	Collection<QuestionarioPerguntaResposta> obterRespostas(long chavePrimariaQuestionario, long chavePrimariaChamado);

	/**
	 * Lista as respostas do formulario para o Autorização de Serviço Histórico
	 * 
	 * @param chavePrimariaQuestionario {@link long}
	 * @param chavePrimariaServicoAutorizacaoHistorico {@link long}
	 * 
	 * @return respostas
	 */
	Collection<QuestionarioPerguntaResposta> obterRespostasFormulario(long chavePrimariaQuestionario,
			long chavePrimariaServicoAutorizacaoHistorico);

	/**
	 * Retorna a lista de alternativa da pergunta
	 * 
	 * @param chavePrimariaPergunta {@link long}
	 * @return listaAlternativa {@link Collection}
	 */
	Collection<QuestionarioAlternativa> obterAlternativas(long chavePrimariaPergunta);

	/**
	 * Retorna lista de alternativa
	 * 
	 * @param listaIdAlternativa {@link Long}
	 * @return listaAlternativa {@link Collection}
	 */
	Collection<QuestionarioAlternativa> obterAlternativas(Long[] listaIdAlternativa);

	/**
	 * Retorna pergunta pela chavePrimaria
	 * 
	 * @param chavePrimaria {@link Long}
	 * @return pergunta {@link QuestionarioPergunta}
	 */
	public QuestionarioPergunta obterPergunta(Long chavePrimaria);

}
