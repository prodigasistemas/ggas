/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 22/01/2014 15:21:57
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.apresentacao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;

/**
 * Classe QuestionarioAction
 * 
 * @author Procenge
 */
@Controller
public class QuestionarioAction extends GenericAction {
	
	private static final String TELA_GRID_PERGUNTAS = "gridPerguntas";

	private static final String QUESTIONARIO = "questionario";

	private static final String ERROR = "ERROR";

	private static final String LISTA_REMOVIDA = "listaRemovida";

	private static final String LISTA_PERGUNTAS = "listaPerguntas";

	private static final Logger LOG = Logger.getLogger(QuestionarioAction.class);

	@Autowired
	private ControladorQuestionario controladorQuestionario;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	/**
	 * Exibir pesquisa questionario.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirPesquisaQuestionario")
	public ModelAndView exibirPesquisaQuestionario(HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView("exibirPesquisaQuestionario");
		sessao.removeAttribute(LISTA_PERGUNTAS);
		sessao.removeAttribute(LISTA_REMOVIDA);
		carregarCombos(model);
		return model;
	}

	/**
	 * Pesquisar questionario.
	 * 
	 * @param questionarioVO
	 *            the questionario vo
	 * @param indicador
	 *            the indicador
	 * @return the model and view
	 */
	@RequestMapping("pesquisarQuestionario")
	public ModelAndView pesquisarQuestionario(@ModelAttribute("QuestionarioVO") QuestionarioVO questionarioVO,
					@RequestParam(value = "habilitado", required = false) Boolean indicador) {

		questionarioVO.setHabilitado(indicador);
		ModelAndView modelAndView = new ModelAndView("exibirPesquisaQuestionario");
		modelAndView.addObject("questionarioVO", questionarioVO);
		try {
			List<Questionario> listaQuestionario = (List<Questionario>) controladorQuestionario.consultarQuestionario(questionarioVO);
			modelAndView.addObject("listaQuestionarios", listaQuestionario);
			carregarCombos(modelAndView);
		} catch (NegocioException e) {
			Logger.getLogger(ERROR).error(e.getMessage(), e);
			carregarCombos(modelAndView);
		}

		return modelAndView;

	}

	/**
	 * Exibir inclusao questionario.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirInclusaoQuestionario")
	public ModelAndView exibirInclusaoQuestionario(HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirInclusaoQuestionario");
		Questionario questionario = new Questionario();
		QuestionarioPergunta pergunta = new QuestionarioPergunta();
		carregarCombos(model);

		model.addObject(QUESTIONARIO, questionario);
		model.addObject("pergunta", pergunta);
		model.addObject("acao", "incluir");
		sessao.removeAttribute(LISTA_PERGUNTAS);

		return model;
	}

	/**
	 * Carregar adicionar pergunta.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @param nomePergunta the nome pergunta
	 * @param objetiva the objetiva
	 * @param notaMinima the nota minima
	 * @param notaMaxima the nota maxima
	 * @param indexList the index list
	 * @param habilitado the habilitado
	 * @param respostaObrigatoria the respostaObrigatoria
	 * @param respostaObrigatoria
	 * @param multiplaEscolha the multiplaEscolha
	 * @param alternativas the alternativas
	 * @param valorPersonalizado the valorPerosnalizado
	 * @param sessao the sessao
	 * @param request the request
	 * @param questionario the questionario
	 * @return the model and view
	 */
	@RequestMapping("carregarAdicionarPergunta")
	public ModelAndView carregarAdicionarPergunta(@RequestParam("chavePrimariaQuestionario") Long chavePrimaria,
			@RequestParam("nomePergunta") String nomePergunta, @RequestParam(value = "objetiva", required = false) boolean objetiva,
			@RequestParam(value = "notaMinima", required = false) Integer notaMinima,
			@RequestParam(value = "notaMaxima", required = false) Integer notaMaxima, @RequestParam("indexList") Integer indexList,
			@RequestParam(value = "habilitado", required = false) boolean habilitado,
			@RequestParam(value = "respostaObrigatoria", required = false) boolean respostaObrigatoria,
			@RequestParam(value = "multiplaEscolha", required = false) boolean multiplaEscolha,
			@RequestParam("alternativas") String[] alternativas,
			@RequestParam(value = "valorPersonalizado", required = false) boolean valorPersonalizado, HttpSession sessao,
			HttpServletRequest request, @ModelAttribute("Questionario") Questionario questionario) {

		ModelAndView model = new ModelAndView(TELA_GRID_PERGUNTAS);

		QuestionarioPergunta pergunta = new QuestionarioPergunta();
		pergunta.setMultiplaEscolha(multiplaEscolha);
		pergunta.setValoresPersonalizados(valorPersonalizado);
		pergunta.setNomePergunta(nomePergunta);
		pergunta.setNotaMaxima(notaMaxima);
		pergunta.setNotaMinima(notaMinima);
		pergunta.setObjetiva(objetiva);
		pergunta.setHabilitado(habilitado);
		pergunta.setRespostaObrigatoria(respostaObrigatoria);

		Collection<QuestionarioAlternativa> listaAlternativa = new HashSet<>();

		for (String alternativa : alternativas) {
			QuestionarioAlternativa questionarioAlternativa = new QuestionarioAlternativa();
			questionarioAlternativa.setNomeAlternativa(alternativa);
			listaAlternativa.add(questionarioAlternativa);
		}

		pergunta.setListaAlternativas(listaAlternativa);

		// pega a lista atual da tela que se encontra na sesão
		Collection<QuestionarioPergunta> listaAtual = (Collection<QuestionarioPergunta>) sessao.getAttribute(LISTA_PERGUNTAS);
		try {
			if(chavePrimaria != null) {
				controladorQuestionario.verificarQuestionarioRespondido(chavePrimaria);
			}
			// caso seja uma alteracao de uma pergunta ja existente
			if(indexList != null) {
				List<QuestionarioPergunta> lista = new ArrayList<>(listaAtual);
				lista.get(indexList).setNomePergunta(nomePergunta);
				lista.get(indexList).setNotaMaxima(notaMaxima);
				lista.get(indexList).setNotaMinima(notaMinima);
				lista.get(indexList).setObjetiva(objetiva);
				lista.get(indexList).setRespostaObrigatoria(respostaObrigatoria);
				lista.get(indexList).setHabilitado(habilitado);
				lista.get(indexList).setRespostaObrigatoria(respostaObrigatoria);
				lista.get(indexList).setListaAlternativas(listaAlternativa);
				lista.get(indexList).setValoresPersonalizados(valorPersonalizado);
				validarEntidade(pergunta);
				controladorQuestionario.verificarSeExistePergunta(pergunta, listaAtual, indexList);
				listaAtual.clear();
				Collections.sort(lista);
				listaAtual.addAll(lista);
			} else {
				// verifica se a lista esta nula ou vazia
				if(listaAtual != null && !listaAtual.isEmpty()) {
					validarEntidade(pergunta);
					controladorQuestionario.verificarSeExistePergunta(pergunta, listaAtual, null);
					pergunta.setNumeroOrdem(listaAtual.size() + 1);
					listaAtual.add(pergunta);
				} else {
					validarEntidade(pergunta);
					listaAtual = new HashSet<>();
					pergunta.setNumeroOrdem(1);
					listaAtual.add(pergunta);
				}
			}
		} catch(NegocioException e) {
			model = new ModelAndView("ajaxErro");
			mensagemErroParametrizado(model, e);
		}

		if(listaAtual != null) {
			List<QuestionarioPergunta> listaNovaOrdenada = new ArrayList<>(listaAtual);
			Collections.sort(listaNovaOrdenada);
			sessao.setAttribute(LISTA_PERGUNTAS, listaNovaOrdenada);
			model.addObject(LISTA_PERGUNTAS, listaNovaOrdenada);
		}

		return model;

	}

	/**
	 * Inserir questionario.
	 * @param questionario the questionario
	 * @param sessao the sessao
	 * @param request the request
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("inserirQuestionario")
	public ModelAndView inserirQuestionario(@ModelAttribute("Questionario") Questionario questionario, HttpSession sessao,
			HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView("forward:/pesquisarQuestionario");
		Collection<QuestionarioPergunta> listaPerguntas = null;
		if(sessao.getAttribute(LISTA_PERGUNTAS) != null) {
			listaPerguntas = new HashSet<>((Collection<QuestionarioPergunta>) sessao.getAttribute(LISTA_PERGUNTAS));
		}

		if(listaPerguntas != null) {
			for (QuestionarioPergunta pergunta : listaPerguntas) {
				pergunta.setQuestionario(questionario);
				pergunta.setUltimaAlteracao(new Date());
				Collection<QuestionarioAlternativa> alternativas = pergunta.getListaAlternativas();
				for (QuestionarioAlternativa alternativa : alternativas) {
					alternativa.setPergunta(pergunta);
					alternativa.setUltimaAlteracao(new Date());
				}
			}
			questionario.setListaPerguntas(listaPerguntas);
			questionario.setDadosAuditoria(getDadosAuditoria(request));
		}
		try {
			controladorQuestionario.incluirQuestionario(questionario);
			sessao.removeAttribute(LISTA_PERGUNTAS);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Constantes.QUESTIONARIO);
		} catch(NegocioException e) {
			model = new ModelAndView("exibirInclusaoQuestionario");
			carregarCombos(model);
			model.addObject(QUESTIONARIO, questionario);
			sessao.setAttribute(LISTA_PERGUNTAS, listaPerguntas);
			model.addObject("acao", "incluir");
			mensagemErroParametrizado(model, e);
		}
		return model;

	}

	/**
	 * Alterar questionario.
	 * 
	 * @param questionario {@link Questionario}
	 * @param session {@link HttpSession}
	 * @return model {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("alterarQuestionario")
	public ModelAndView alterarQuestionario(@ModelAttribute("Questionario") Questionario questionario, HttpSession session)
			throws GGASException {

		Collection<QuestionarioPergunta> listaPerguntas = new HashSet<>(
						(Collection<QuestionarioPergunta>) session.getAttribute(LISTA_PERGUNTAS));
		for (QuestionarioPergunta pergunta : listaPerguntas) {
			pergunta.setQuestionario(questionario);
			pergunta.setUltimaAlteracao(new Date());
			Collection<QuestionarioAlternativa> listaAlternativas = pergunta.getListaAlternativas();
			for (QuestionarioAlternativa questionarioAlternativa : listaAlternativas) {
				questionarioAlternativa.setPergunta(pergunta);
				questionarioAlternativa.setUltimaAlteracao(new Date());
			}
		}
		questionario.setListaPerguntas(listaPerguntas);
		List<Long> listaRemover = (List<Long>) session.getAttribute(LISTA_REMOVIDA);
		ModelAndView model = this.exibirAlteracaoQuestionario(questionario.getChavePrimaria(), session);
		try {
			controladorQuestionario.alterarQuestionario(questionario, listaRemover);
			session.removeAttribute(LISTA_REMOVIDA);
			model = this.exibirPesquisaQuestionario(session);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, Constantes.QUESTIONARIO);
		} catch(NegocioException e) {
			carregarCombos(model);
			model.addObject(QUESTIONARIO, questionario);
			mensagemErroParametrizado(model, e);
		} catch(ConcorrenciaException e) {
			carregarCombos(model);
			model.addObject(QUESTIONARIO, questionario);
			mensagemErroParametrizado(model, e);
		}

		return model;

	}

	/**
	 * Exibir alteracao questionario.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirAlteracaoQuestionario")
	public ModelAndView exibirAlteracaoQuestionario(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirAlteracaoQuestionario");
		try {
			Questionario questionario =
					controladorQuestionario.consultarPorCodico(chavePrimaria, LISTA_PERGUNTAS);

			QuestionarioPergunta pergunta = new QuestionarioPergunta();
			model.addObject(QUESTIONARIO, questionario);
			pergunta.setHabilitado(true);
			model.addObject("pergunta", pergunta);
			List<QuestionarioPergunta> lista = new ArrayList<>(questionario.getListaPerguntas());
			Collections.sort(lista);
			
			for (QuestionarioPergunta questionarioPergunta : lista) {
				questionarioPergunta
						.setListaAlternativas(controladorQuestionario.obterAlternativas(questionarioPergunta.getChavePrimaria()));
			}
			
			sessao.setAttribute(LISTA_PERGUNTAS, lista);
			carregarCombos(model);
			model.addObject("acao", "alterar");
		} catch(NegocioException e) {
			Logger.getLogger(ERROR).error(e.getMessage(), e);
		}

		return model;
	}

	/**
	 * Remover questionario.
	 * 
	 * @param questionarioVO
	 *            the questionario vo
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("removerQuestionario")
	public ModelAndView removerQuestionario(@ModelAttribute("QuestionarioVO") QuestionarioVO questionarioVO,
					@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao) {

		ModelAndView model = null;
		try {
			model = this.exibirPesquisaQuestionario(sessao);
			model.addObject("questionarioVO", questionarioVO);
			Questionario questionario = controladorQuestionario.consultarPorCodico(chavePrimaria, LISTA_PERGUNTAS);
			controladorQuestionario.removerQuestionario(questionario);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, Constantes.QUESTIONARIO);

			tryConsultarQuestionario(questionarioVO, model);
		} catch (GGASException e) {
			Logger.getLogger(ERROR).error(e.getMessage(), e);
			mensagemErroParametrizado(model, e);
		}
		return model;

	}

	private void tryConsultarQuestionario(QuestionarioVO questionarioVO, ModelAndView model) {
		List<Questionario> listaQuestionario = null;
		try {
			listaQuestionario = (List<Questionario>) controladorQuestionario.consultarQuestionario(questionarioVO);
			model.addObject("listaQuestionarios", listaQuestionario);
		} catch(NegocioException e1) {
			Logger.getLogger(ERROR).error(e1.getMessage(), e1);
			mensagemErroParametrizado(model, e1);
		}
	}

	/**
	 * Exibir detalhar questionario.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirDetalharQuestionario")
	public ModelAndView exibirDetalharQuestionario(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirDetalharQuestionario");
		try {
			Questionario questionario = controladorQuestionario.consultarPorCodico(chavePrimaria, LISTA_PERGUNTAS);
			model.addObject(QUESTIONARIO, questionario);
			model.addObject("acao", "detalhar");
			List<QuestionarioPergunta> lista = new ArrayList<>(questionario.getListaPerguntas());
			Collections.sort(lista);
			sessao.setAttribute(LISTA_PERGUNTAS, lista);
			carregarCombos(model);
		} catch(NegocioException e) {
			Logger.getLogger(ERROR).debug(e.getStackTrace(), e);
		}
		return model;
	}

	/**
	 * Remover pergunta.
	 * 
	 * @param indexPergunta
	 *            the index pergunta
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("removerPergunta")
	public ModelAndView removerPergunta(@RequestParam("indexPergunta") int indexPergunta, HttpSession sessao) {

		List<Long> removida = (List<Long>) sessao.getAttribute(LISTA_REMOVIDA);
		if(removida == null) {
			removida = new ArrayList<>();
		}

		ModelAndView model = new ModelAndView(TELA_GRID_PERGUNTAS);
		Collection<QuestionarioPergunta> lista = (Collection<QuestionarioPergunta>) sessao.getAttribute(LISTA_PERGUNTAS);
		if(lista != null && !lista.isEmpty()) {
			List<QuestionarioPergunta> lista1 = new ArrayList<>(lista);
			try {

				if (lista1.get(indexPergunta).getQuestionario() != null) {
					controladorQuestionario.validarExclusaoPergunta(lista1.get(indexPergunta).getChavePrimaria(),
							lista1.get(indexPergunta).getQuestionario().getChavePrimaria());
					removida.add(lista1.get(indexPergunta).getChavePrimaria());
				}
				for (int i = indexPergunta; i < lista.size(); i++) {
					Integer numero = lista1.get(i).getNumeroOrdem();
					lista1.get(i).setNumeroOrdem(numero - 1);
				}
				lista1.remove(indexPergunta);
				Collections.sort(lista1);
				sessao.setAttribute(LISTA_PERGUNTAS, lista1);
			} catch(NegocioException e) {
				LOG.error(e.getMessage(), e);
				model = new ModelAndView("ajaxErro");
				mensagemAdvertencia(model, e.getChaveErro());
			}
		}
		sessao.setAttribute(LISTA_REMOVIDA, removida);

		return model;
	}

	/**
	 * Cancelar operacao.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("cancelarOperacao")
	public ModelAndView cancelarOperacao(HttpSession sessao) throws NegocioException {

		return this.exibirPesquisaQuestionario(sessao);
	}

	/**
	 * Alterar ordem lista perguntas.
	 * 
	 * @param indexPergunta
	 *            the index pergunta
	 * @param acao
	 *            the acao
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("alterarOrdemListaPerguntas")
	public ModelAndView alterarOrdemListaPerguntas(@RequestParam(value = "indexPergunta", required = false) int indexPergunta,
					@RequestParam(value = "acao", required = false) String acao, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_PERGUNTAS);
		Collection<QuestionarioPergunta> perguntasSessao = (Collection<QuestionarioPergunta>) sessao.getAttribute(LISTA_PERGUNTAS);
		List<QuestionarioPergunta> listaPerguntas = new ArrayList<>(perguntasSessao);
		QuestionarioPergunta pergunta1 = null;
		QuestionarioPergunta pergunta2 = null;
		Integer ordem1 = null;
		Integer ordem2 = null;

		if("subirPosicao".equals(acao)) {
			pergunta1 = listaPerguntas.get(indexPergunta);
			pergunta2 = listaPerguntas.get(indexPergunta - 1);
			ordem1 = pergunta1.getNumeroOrdem();
			ordem2 = pergunta2.getNumeroOrdem();
			pergunta1.setNumeroOrdem(ordem2);
			pergunta2.setNumeroOrdem(ordem1);

			listaPerguntas.set(indexPergunta, pergunta2);
			listaPerguntas.set(indexPergunta - 1, pergunta1);

		} else if("descerPosicao".equals(acao)) {
			pergunta1 = listaPerguntas.get(indexPergunta);
			pergunta2 = listaPerguntas.get(indexPergunta + 1);
			ordem1 = pergunta1.getNumeroOrdem();
			ordem2 = pergunta2.getNumeroOrdem();
			pergunta1.setNumeroOrdem(ordem2);
			pergunta2.setNumeroOrdem(ordem1);

			listaPerguntas.set(indexPergunta, pergunta2);
			listaPerguntas.set(indexPergunta + 1, pergunta1);
		}
		sessao.setAttribute(LISTA_PERGUNTAS, listaPerguntas);

		return model;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model
	 *            the model
	 */
	private void carregarCombos(ModelAndView model) {

		try {

			model.addObject("listaTipoServico", controladorServicoTipo.obterTodosServicoTipoComPesquisaSatisfacao());
			model.addObject("listaSegmento", controladorSegmento.consultarSegmento(null));

		} catch(NegocioException e) {
			Logger.getLogger(ERROR).error(e.getMessage(), e);
		}

	}

}
