/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 16:49:57
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pela representação de uma pergunta de um questionário
 */
public class QuestionarioPergunta extends EntidadeNegocioImpl implements Comparable<QuestionarioPergunta> {

	private static final int LIMITE_CAMPO = 2;

	private static final Logger LOG = Logger.getLogger(ChamadoAssunto.class);

	private static final long serialVersionUID = 115571642508215264L;

	private Questionario questionario;

	private String nomePergunta;

	private Integer notaMaxima;

	private Integer notaMinima;

	private boolean objetiva;

	private boolean valoresPersonalizados;

	private boolean multiplaEscolha;

	private Boolean respostaObrigatoria = false;

	private Integer notaPergunta;

	private String descricaoResposta;

	private Integer numeroOrdem;

	private QuestionarioAlternativa questionarioPerguntaAlternativa;

	private ServicoAutorizacao servicoAutorizacao;

	private QuestionarioPerguntaResposta questionarioPerguntaResposta;

	private Collection<QuestionarioAlternativa> listaAlternativas = new HashSet<>();

	private Collection<QuestionarioAlternativa> listaAlternativasSelecionadas = new HashSet<>();

	private Collection<QuestionarioPerguntaResposta> listaRespostas = new HashSet<>();

	private ServicoAutorizacaoHistorico servicoHistorico;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.nomePergunta == null || this.nomePergunta.trim().isEmpty()) {
			stringBuilder.append(Constantes.CAMPO_NOME_PERGUNTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.objetiva) {
			if (this.getValoresPersonalizados()) {
				if (listaAlternativas == null || listaAlternativas.isEmpty()) {
					stringBuilder.append("Alternativas");
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			} else {
				if (this.notaMinima == null) {
					stringBuilder.append(Constantes.CAMPO_NOTA_MINIMA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				if (this.notaMaxima == null) {
					stringBuilder.append(Constantes.CAMPO_NOTA_MAXIMA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		camposObrigatorios = stringBuilder.toString();
		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		} else {
			if(this.notaMinima != null && this.notaMaxima != null) {
				verificaNotaMinima(erros);
			}
		}
		return erros;
	}
	
	/**
	 * Método responsável pela verificação da nota mínima.
	 * 
	 * @param erros
	 */
	private void verificaNotaMinima(Map<String, Object> erros) {
		if(notaMinima < 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPO_NOTA_NEGATIVO, Constantes.CAMPO_NOTA_MINIMA);
		} else {
			if(notaMinima > notaMaxima) {
				erros.put(Constantes.ERRO_NEGOCIO_CAMPO_NOTA_MINIMA_NOTA_MAXIMA, Constantes.CAMPO_NOTA_MINIMA);
			}
		}
	}

	/**
	 * Retorna questionario
	 * 
	 * @return questionario
	 */
	public Questionario getQuestionario() {

		return questionario;
	}

	/**
	 * Atribui questionario
	 * 
	 * @param questionario
	 */
	public void setQuestionario(Questionario questionario) {

		this.questionario = questionario;
	}

	/**
	 * Retorna nomePergunta
	 * 
	 * @return nomePergunta
	 */
	public String getNomePergunta() {

		return nomePergunta;
	}

	/**
	 * Atribui nomePergunta
	 * 
	 * @param nomePergunta
	 */
	public void setNomePergunta(String nomePergunta) {

		this.nomePergunta = nomePergunta;
	}

	/**
	 * Retorna notaMaxima
	 * 
	 * @return notaMaxima
	 */
	public Integer getNotaMaxima() {

		return notaMaxima;
	}

	/**
	 * Atribui notaMaxima
	 * 
	 * @param notaMaxima
	 */
	public void setNotaMaxima(Integer notaMaxima) {

		this.notaMaxima = notaMaxima;
	}

	/**
	 * Retorna notaMinima
	 * 
	 * @return notaMinima
	 */
	public Integer getNotaMinima() {

		return notaMinima;
	}

	/**
	 * Atribui notaMinima
	 * 
	 * @param notaMinima
	 */
	public void setNotaMinima(Integer notaMinima) {

		this.notaMinima = notaMinima;
	}

	/**
	 * Retorna objetiva
	 * 
	 * @return objetiva
	 */
	public boolean getObjetiva() {

		return objetiva;
	}

	/**
	 * Atribui objetiva
	 * 
	 * @param objetiva
	 */
	public void setObjetiva(boolean objetiva) {

		this.objetiva = objetiva;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = super.hashCode();
		if(nomePergunta == null){
			result = prime * result + 0 ;
		} else {
			result = prime * result + nomePergunta.hashCode() ;
		}
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj) {
			return true;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		QuestionarioPergunta other = (QuestionarioPergunta) obj;
		if(nomePergunta == null) {
			if(other.nomePergunta != null) {
				return false;
			}
		} else if(!nomePergunta.trim().equals(other.nomePergunta.trim())) {
			return false;
		}
		return true;
	}

	/**
	 * Retorna notaPergunta
	 * 
	 * @return notaPergunta
	 */
	public Integer getNotaPergunta() {

		return notaPergunta;
	}

	/**
	 * Atribui notaPergunta
	 * 
	 * @param notaPergunta
	 */
	public void setNotaPergunta(Integer notaPergunta) {

		this.notaPergunta = notaPergunta;
	}

	/**
	 * Retorna descricaoResposta
	 * 
	 * @return descricaoResposta
	 */
	public String getDescricaoResposta() {

		return descricaoResposta;
	}

	/**
	 * Atribui descricaoResposta
	 * 
	 * @param descricaoResposta
	 */
	public void setDescricaoResposta(String descricaoResposta) {

		this.descricaoResposta = descricaoResposta;
	}

	/**
	 * Retorna numeroOrdem
	 * 
	 * @return numeroOrdem
	 */
	public Integer getNumeroOrdem() {

		return numeroOrdem;
	}

	/**
	 * Atribui numeroOrdem
	 * 
	 * @param numeroOrdem
	 */
	public void setNumeroOrdem(Integer numeroOrdem) {

		this.numeroOrdem = numeroOrdem;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(QuestionarioPergunta o) {

		if(this.numeroOrdem < o.getNumeroOrdem()) {
			return -1;
		}
		if(this.numeroOrdem > o.getNumeroOrdem()) {
			return 1;
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return "QuestionarioPergunta [nomePergunta= " + nomePergunta + ", numeroOrdem= " + numeroOrdem + "]";
	}

	/**
	 * Retornar respostaObrigatoria
	 * 
	 * @return respostaObrigatoria
	 */
	public Boolean getRespostaObrigatoria() {
		return respostaObrigatoria;
	}

	/**
	 * Atribui respostaObrigatoria
	 * 
	 * @param respostaObrigatoria
	 */
	public void setRespostaObrigatoria(Boolean respostaObrigatoria) {
		this.respostaObrigatoria = respostaObrigatoria;
	}

	/**
	 * Retorna servicoAutorizacao
	 * 
	 * @return servicoAutorizacao
	 */
	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}

	/**
	 * Atribui servicoAutorizacao
	 * 
	 * @param servicoAutorizacao
	 */
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
	}

	/**
	 * Retorna valoresPersonalizados
	 * 
	 * @return valoresPersonalizados
	 */
	public boolean getValoresPersonalizados() {
		return valoresPersonalizados;
	}

	/**
	 * Atribui valoresPersonalizados
	 * 
	 * @param valoresPersonalizados
	 */
	public void setValoresPersonalizados(boolean valoresPersonalizados) {
		this.valoresPersonalizados = valoresPersonalizados;
	}

	/**
	 * Retorna multiplaEscolha
	 * 
	 * @return multiplaEscolha
	 */
	public boolean getMultiplaEscolha() {
		return multiplaEscolha;
	}

	/**
	 * Atribui multiplaEscolha
	 * 
	 * @param multiplaEscolha
	 */
	public void setMultiplaEscolha(boolean multiplaEscolha) {
		this.multiplaEscolha = multiplaEscolha;
	}

	/**
	 * Retorna listaAlternativas
	 * 
	 * @return listaAlternativas
	 */
	public Collection<QuestionarioAlternativa> getListaAlternativas() {
		return listaAlternativas;
	}

	/**
	 * Atribui listaAlternativas
	 * 
	 * @param listaAlternativas
	 */
	public void setListaAlternativas(Collection<QuestionarioAlternativa> listaAlternativas) {
		this.listaAlternativas = listaAlternativas;
	}

	public String getPerguntaJSON() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("nomePergunta", this.getNomePergunta());
			json.put("respostaObrigatoria", this.getRespostaObrigatoria());
			json.put("objetiva", this.getObjetiva());
			json.put("notaMinima", this.getNotaMinima());
			json.put("notaMaxima", this.getNotaMaxima());
			json.put("habilitado", this.isHabilitado());
			json.put("valorPersonalizado", this.getValoresPersonalizados());
			json.put("multiplaEscolha", this.getMultiplaEscolha());

			JSONArray alternativas = new JSONArray();
			Collection<QuestionarioAlternativa> listaAlternativa = getListaAlternativas();
			for (QuestionarioAlternativa questionarioAlternativa : listaAlternativa) {
				JSONObject alternativa = new JSONObject();
				alternativa.put("nomeAlternativa", questionarioAlternativa.getNomeAlternativa());
				alternativas.put(alternativa);
			}

			json.put("alternativas", alternativas);
		} catch (JSONException e) {
			LOG.error(e);
		}

		return json.toString();
	}

	/**
	 * Retorna listaAlternativasSelecionadas
	 * 
	 * @return listaAlternativasSelecionadas
	 */
	public Collection<QuestionarioAlternativa> getListaAlternativasSelecionadas() {
		return listaAlternativasSelecionadas;
	}

	/**
	 * Atribui listaAlternativasSelecionadas
	 * 
	 * @param listaAlternativasSelecionadas
	 */
	public void setListaAlternativasSelecionadas(Collection<QuestionarioAlternativa> listaAlternativasSelecionadas) {
		this.listaAlternativasSelecionadas = listaAlternativasSelecionadas;
	}



	public Collection<QuestionarioPerguntaResposta> getListaRespostas() {
		return listaRespostas;
	}

	public void setListaRespostas(Collection<QuestionarioPerguntaResposta> listaRespostas) {
		this.listaRespostas = listaRespostas;
	}

	public QuestionarioPerguntaResposta getQuestionarioPerguntaResposta() {
		return questionarioPerguntaResposta;
	}

	public void setQuestionaioPerguntaResposta(QuestionarioPerguntaResposta questionarioPerguntaResposta) {
		this.questionarioPerguntaResposta = questionarioPerguntaResposta;
	}

	public QuestionarioAlternativa getQuestionarioPerguntaAlternativa() {
		return questionarioPerguntaAlternativa;
	}

	public void setQuestionarioPerguntaAlternativa(QuestionarioAlternativa questionarioPerguntaAlternativa) {
		this.questionarioPerguntaAlternativa = questionarioPerguntaAlternativa;
	}

	public ServicoAutorizacaoHistorico getServicoHistorico() {
		return servicoHistorico;
	}

	public void setServicoHistorico(ServicoAutorizacaoHistorico servicoHistorico) {
		this.servicoHistorico = servicoHistorico;
	}
}
