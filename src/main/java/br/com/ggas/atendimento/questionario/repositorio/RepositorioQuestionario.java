/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/01/2014 10:55:57
 @author ifrancisco
 */

package br.com.ggas.atendimento.questionario.repositorio;

import java.util.Collection;
import java.util.Iterator;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioAlternativa;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioPergunta;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.resposta.dominio.QuestionarioPerguntaResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * RepositorioQuestionario
 */
@Repository
public class RepositorioQuestionario extends RepositorioGenerico {

	private static final String NOME_QUESTIONARIO = "nomeQuestionario";
	private static final String SEGMENTO = "segmento";
	private static final String TIPO_SERVICO = "tipoServico";
	private static final String PERGUNTA = "pergunta";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String TIPOSERVICO_CHAVEPRIMARIA = "tipoServico.chavePrimaria";
	private static final String SEGMENTO_CHAVEPRIMARIA = "segmento.chavePrimaria";
	

	/**
	 * Instantiates a new repositorio questionario.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioQuestionario(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Questionario();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return Questionario.class;
	}

	/**
	 * Consultar questionario.
	 * 
	 * @param questionarioVO
	 *            the questionario vo
	 * @return the collection
	 */
	public Collection<Questionario> consultarQuestionario(QuestionarioVO questionarioVO) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(TIPO_SERVICO, FetchMode.JOIN);
		criteria.setFetchMode(SEGMENTO, FetchMode.JOIN);

		if(questionarioVO != null) {
			if(questionarioVO.getNomeQuestionario() != null && !questionarioVO.getNomeQuestionario().isEmpty()) {
				criteria.add(Restrictions.ilike(NOME_QUESTIONARIO, "%" + questionarioVO.getNomeQuestionario() + "%"));
			}
			if(questionarioVO.getTipoServico() != null) {
				criteria.createAlias(TIPO_SERVICO, TIPO_SERVICO);
				criteria.add(Restrictions.eq(TIPOSERVICO_CHAVEPRIMARIA, questionarioVO.getTipoServico().getChavePrimaria()));
			}
			if(questionarioVO.getSegmento() != null) {
				criteria.createAlias(SEGMENTO, SEGMENTO);
				criteria.add(Restrictions.eq(SEGMENTO_CHAVEPRIMARIA, questionarioVO.getSegmento().getChavePrimaria()));
			}
			if(questionarioVO.getHabilitado() != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, questionarioVO.getHabilitado()));
			}
			if (questionarioVO.getTipoQuestionarioAS() != null && questionarioVO.getTipoQuestionarioAS()) {
				criteria.add(Restrictions.eq("tipoQuestionarioAS", questionarioVO.getTipoQuestionarioAS()));
			}
			if (questionarioVO.getTipoQuestionarioChamado() != null && questionarioVO.getTipoQuestionarioChamado()) {
				criteria.add(Restrictions.eq("tipoQuestionarioChamado", questionarioVO.getTipoQuestionarioChamado()));
			}
		}

		Collection<Questionario> lista = criteria.list();
		if(lista != null) {
			for (Questionario questionario : lista) {
				inicializarLazy(questionario, "listaPerguntas");
			}
		}
		return lista;
	}

	/**
	 * Metodo que verifica se existe algum questionario cadastrado com o mesmo nome que estaja ativo.
	 * 
	 * @param nomeQuestionario
	 *            the nome questionario
	 * @return the questionario
	 */
	public Questionario existenteQuestionario(String nomeQuestionario) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.add(Restrictions.ilike(NOME_QUESTIONARIO, nomeQuestionario));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

		return (Questionario) criteria.uniqueResult();
	}

	/**
	 * Metodo que verifica se existe algum questionario cadastrado com o mesmo nome que estaja ativo para as operações de alteração.
	 * 
	 * @param questionario
	 *            the questionario
	 * @return true, if successful
	 */
	public boolean existenteQuestionario(Questionario questionario) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.add(Restrictions.ne(CHAVE_PRIMARIA, questionario.getChavePrimaria()));
		criteria.add(Restrictions.like(NOME_QUESTIONARIO, questionario.getNomeQuestionario()));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		Questionario quest = (Questionario) criteria.uniqueResult();

		return quest != null;
	}

	/**
	 * Metodo que veirifca se ja existe um questionario cadastrado para o tipo de serviço especifico.
	 * 
	 * @param questionario
	 *            the questionario
	 * @return true, if successful
	 */
	public boolean exiteQuestionarioParaTipoServicoOuSegmento(Questionario questionario) {

		boolean retorno = false;

		Criteria criteria = createCriteria(getClasseEntidade());
		if (questionario.getTipoServico() != null && questionario.getSegmento() != null) {
			criteria.add(Restrictions.eq(TIPOSERVICO_CHAVEPRIMARIA, questionario.getTipoServico().getChavePrimaria()));
			criteria.add(Restrictions.eq(SEGMENTO_CHAVEPRIMARIA, questionario.getSegmento().getChavePrimaria()));
		}
		if (questionario.getTipoServico() != null && questionario.getSegmento() == null) {
			criteria.add(Restrictions.eq(TIPOSERVICO_CHAVEPRIMARIA, questionario.getTipoServico().getChavePrimaria()));
			criteria.createAlias(SEGMENTO, SEGMENTO, Criteria.LEFT_JOIN);
			criteria.add(Restrictions.isNull(SEGMENTO_CHAVEPRIMARIA));
		}
		if (questionario.getTipoServico() == null && questionario.getSegmento() == null) {
			criteria.add(Restrictions.isNull(TIPOSERVICO_CHAVEPRIMARIA));
			criteria.createAlias(SEGMENTO, SEGMENTO, Criteria.LEFT_JOIN);
			criteria.add(Restrictions.isNull(SEGMENTO_CHAVEPRIMARIA));
		}

		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		Questionario quest = (Questionario) criteria.uniqueResult();
		if (quest != null && questionario.getChavePrimaria() != quest.getChavePrimaria()) {
			retorno = true;
		}

		return retorno;
	}

	/**
	 * Obter perguntas.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	public Collection<QuestionarioPergunta> obterPerguntas(long chavePrimaria) {

		String consulta = "select p from Pergunta where p.questionario = :chavePrimaria";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(consulta);

		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		return query.list();
	}

	/**
	 * Consultar questionario por tipo servico.
	 * 
	 * @param servicoAutorizacao
	 *            the servico autorizacao
	 * @param segmento
	 *            the segmento
	 * @return the questionario
	 */
	public Questionario consultarQuestionarioPorTipoServico(ServicoAutorizacao servicoAutorizacao, Segmento segmento) {

		Criteria criteria = createCriteria(getClasseEntidade());
		Questionario questionario = null;
		if(servicoAutorizacao.getServicoTipo() != null && segmento != null) {
			criteria.add(Restrictions.eq(TIPOSERVICO_CHAVEPRIMARIA, servicoAutorizacao.getServicoTipo().getChavePrimaria()));
			criteria.add(Restrictions.eq(SEGMENTO_CHAVEPRIMARIA, segmento.getChavePrimaria()));
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
			questionario = (Questionario) criteria.uniqueResult();
		}

		if(questionario == null) {
			criteria = createCriteria(getClasseEntidade());
			criteria.add(Restrictions.eq(TIPOSERVICO_CHAVEPRIMARIA, servicoAutorizacao.getServicoTipo().getChavePrimaria()));
			criteria.createAlias(SEGMENTO, SEGMENTO, Criteria.LEFT_JOIN);
			criteria.add(Restrictions.isNull(SEGMENTO_CHAVEPRIMARIA));
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
			questionario = (Questionario) criteria.uniqueResult();
		}

		return questionario;

	}

	/**
	 * Lista as respostas do questionario para o chamado
	 * 
	 * @param chavePrimariaQuestionario {@link long}
	 * @param chavePrimariaChamado {@link long}
	 * 
	 * @return respostas {@link Collection}
	 */
	public Collection<QuestionarioPerguntaResposta> obterRespostas(long chavePrimariaQuestionario, long chavePrimariaChamado) {
		Criteria criteria = createCriteria(QuestionarioPerguntaResposta.class);

		criteria.createAlias(PERGUNTA, PERGUNTA);
		criteria.add(Restrictions.eq("pergunta.questionario.chavePrimaria", chavePrimariaQuestionario));
		criteria.add(Restrictions.eq("chamado.chavePrimaria", chavePrimariaChamado));
		criteria.addOrder(Order.asc("pergunta.numeroOrdem"));

		return criteria.list();
	}

	/**
	 * Lista as respostas do formulário para a Autorização de Serviço Historico
	 * 
	 * @param chavePrimariaQuestionario {@link long}
	 * @param chavePrimariaServicoAutorizacaoHistorico {@link long}
	 * 
	 * @return respostas {@link Collection}
	 */
	public Collection<QuestionarioPerguntaResposta> obterRespostasFormulario(long chavePrimariaQuestionario,
			long chavePrimariaServicoAutorizacaoHistorico) {
		Criteria criteria = createCriteria(QuestionarioPerguntaResposta.class);

		criteria.createAlias(PERGUNTA, PERGUNTA);
		criteria.add(Restrictions.eq("pergunta.questionario.chavePrimaria", chavePrimariaQuestionario));
		criteria.add(Restrictions.eq("servicoAutorizacaoHistorico.chavePrimaria", chavePrimariaServicoAutorizacaoHistorico));
		criteria.addOrder(Order.asc("pergunta.numeroOrdem"));

		return criteria.list();
	}

	/**
	 * Retorna lista de alternativa da pergunta
	 * 
	 * @param chavePrimariaPergunta - long
	 * @return listaAlternativa
	 */
	public Collection<QuestionarioAlternativa> obterAlternativas(long chavePrimariaPergunta) {
		Criteria criteria = createCriteria(QuestionarioAlternativa.class);
		criteria.add(Restrictions.eq("pergunta.chavePrimaria", chavePrimariaPergunta));
		return criteria.list();
	}

	/**
	 * Retorna lista de alternativa
	 * 
	 * @param listaIdAlternativa {@link Long[]}
	 * @return listaIdAlternativa {@link Collection}
	 */
	public Collection<QuestionarioAlternativa> obterAlternativas(Long[] listaIdAlternativa) {
		Criteria criteria = createCriteria(QuestionarioAlternativa.class);
		criteria.add(Restrictions.in(CHAVE_PRIMARIA, listaIdAlternativa));
		return criteria.list();
	}

	/**
	 * Remove Questionario
	 * 
	 * @param questionario {@link Questionario}
	 * @throws NegocioException {@link NegocioException}
	 */
	public void remover(Questionario questionario) throws NegocioException {
		Collection<QuestionarioPergunta> listaPerguntas = questionario.getListaPerguntas();

		if (listaPerguntas != null && !listaPerguntas.isEmpty()) {
			for (QuestionarioPergunta pergunta : listaPerguntas) {
				removerAlternativas(pergunta);
			}
		}

		super.remover(questionario);
	}

	/**
	 * Remover alternativas
	 * 
	 * @param alternativas
	 * @throws NegocioException
	 */
	private void removerAlternativas(QuestionarioPergunta pergunta) throws NegocioException {
		Collection<QuestionarioAlternativa> alternativas = obterAlternativas(pergunta.getChavePrimaria());
		if (alternativas != null && !alternativas.isEmpty()) {
			for (Iterator<QuestionarioAlternativa> iterator = alternativas.iterator(); iterator.hasNext();) {
				QuestionarioAlternativa alternativa = iterator.next();
				remover(alternativa);
			}
		}
	}

	/**
	 * Obtem a pergunta
	 * 
	 * @param chavePrimaria {@link Long}
	 * @return pergunta {@link QuestionarioPergunta}
	 */
	public QuestionarioPergunta obterPergunta(Long chavePrimaria) {
		Criteria criteria = createCriteria(QuestionarioPergunta.class);
		criteria.setFetchMode("questionario", FetchMode.JOIN);
		criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
		Object result = criteria.uniqueResult();

		if (result != null) {
			return (QuestionarioPergunta) result;
		}
		return null;
	}
}
