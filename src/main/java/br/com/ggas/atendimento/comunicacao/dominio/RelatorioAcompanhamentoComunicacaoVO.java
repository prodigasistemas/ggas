package br.com.ggas.atendimento.comunicacao.dominio;

public class RelatorioAcompanhamentoComunicacaoVO {
	private String codigoLegado;
	private String nomePontoConsumo;
	private String numeroSequencial;

	
	private String tipoServico;
	private String possuiAutorizacaoDeServico;
	private String cartaCancelada;
	private String dataHoraPlanejada;
	

	private String bairro;
	private String pressao;
	private String vazao;
	private String numeroLote;
	private String situacaoCarta;
	private String numeroOS;
	private String numeroAS;
	private String dataEmissaoAS;
	private String dataAgendamentoAS;
	private String dataExecucaoAS;
	private String statusAS;
	private String tipoMedidor;
	private String numeroSerie;
	private String numeroBP;
	
	public String getCodigoLegado() {
		return codigoLegado;
	}
	public void setCodigoLegado(String codigoLegado) {
		this.codigoLegado = codigoLegado;
	}
	public String getNomePontoConsumo() {
		return nomePontoConsumo;
	}
	public void setNomePontoConsumo(String nomePontoConsumo) {
		this.nomePontoConsumo = nomePontoConsumo;
	}
	public String getNumeroSequencial() {
		return numeroSequencial;
	}
	public void setNumeroSequencial(String numeroSequencial) {
		this.numeroSequencial = numeroSequencial;
	}
	public String getTipoServico() {
		return tipoServico;
	}
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	public String getPossuiAutorizacaoDeServico() {
		return possuiAutorizacaoDeServico;
	}
	public void setPossuiAutorizacaoDeServico(String possuiAutorizacaoDeServico) {
		this.possuiAutorizacaoDeServico = possuiAutorizacaoDeServico;
	}
	public String getCartaCancelada() {
		return cartaCancelada;
	}
	public void setCartaCancelada(String cartaCancelada) {
		this.cartaCancelada = cartaCancelada;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getPressao() {
		return pressao;
	}
	public void setPressao(String pressao) {
		this.pressao = pressao;
	}
	public String getVazao() {
		return vazao;
	}
	public void setVazao(String vazao) {
		this.vazao = vazao;
	}
	public String getNumeroLote() {
		return numeroLote;
	}
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}
	public String getSituacaoCarta() {
		return situacaoCarta;
	}
	public void setSituacaoCarta(String situacaoCarta) {
		this.situacaoCarta = situacaoCarta;
	}
	public String getNumeroOS() {
		return numeroOS;
	}
	public void setNumeroOS(String numeroOS) {
		this.numeroOS = numeroOS;
	}
	public String getNumeroAS() {
		return numeroAS;
	}
	public void setNumeroAS(String numeroAS) {
		this.numeroAS = numeroAS;
	}
	public String getDataEmissaoAS() {
		return dataEmissaoAS;
	}
	public void setDataEmissaoAS(String dataEmissaoAS) {
		this.dataEmissaoAS = dataEmissaoAS;
	}
	public String getDataAgendamentoAS() {
		return dataAgendamentoAS;
	}
	public void setDataAgendamentoAS(String dataAgendamentoAS) {
		this.dataAgendamentoAS = dataAgendamentoAS;
	}
	public String getDataExecucaoAS() {
		return dataExecucaoAS;
	}
	public void setDataExecucaoAS(String dataExecucaoAS) {
		this.dataExecucaoAS = dataExecucaoAS;
	}
	public String getStatusAS() {
		return statusAS;
	}
	public void setStatusAS(String statusAS) {
		this.statusAS = statusAS;
	}
	public String getTipoMedidor() {
		return tipoMedidor;
	}
	public void setTipoMedidor(String tipoMedidor) {
		this.tipoMedidor = tipoMedidor;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getNumeroBP() {
		return numeroBP;
	}
	public void setNumeroBP(String numeroBP) {
		this.numeroBP = numeroBP;
	}
	public String getDataHoraPlanejada() {
		return dataHoraPlanejada;
	}
	public void setDataHoraPlanejada(String dataHoraPlanejada) {
		this.dataHoraPlanejada = dataHoraPlanejada;
	}

	
}
