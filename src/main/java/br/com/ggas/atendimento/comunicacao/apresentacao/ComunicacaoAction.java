package br.com.ggas.atendimento.comunicacao.apresentacao;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.comunicacao.dominio.FiltroLoteComunicacaoDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroRelatorioComunicacaoInterrupcaoDTO;
import br.com.ggas.atendimento.comunicacao.impl.LoteComunicacaoImpl;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.ComunicacaoLoteVO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroAcompanhamentoLoteDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroGeracaoComunicaoLoteDTO;
import br.com.ggas.atendimento.comunicacao.negocio.ControladorComunicacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoHistorico;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.Util;


/**
 * Action responsável pelas telas relacionadas as comunicações da Autorização de Serviço
 */
@Controller
public class ComunicacaoAction extends GenericAction{
	private static final String LISTA_CITY_GATE = "listaCityGate";
	private static final String LISTA_SEGMENTO = "listaSegmento";
	private static final String LISTA_PONTO_CONSUMO = "listaPontoConsumo";
	private static final String LISTA_PONTO_CONSUMO_SESSAO = "listaPontoConsumoSessao";
	private static final String LISTA_SERVICO_TIPO = "listaServicoTipo";
	private static final String LISTA_SITUACAO_MEDIDOR = "listaSituacaoMedidor";
	private static final String COMUNICACAO = "comunicacao";
	private static final String DD_MM_YYYY = "dd/MM/yyyy";
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	private static final String MSG_SUCESSO_LOTE_INCLUIDO = "Gerado o lote de comunicação com sucesso.";
	private static final String INDEX_LISTA = "indexLista";
	private static final String LISTA_SITUACAO_PONTO_CONSUMO = "listaSituacaoPontoConsumo";
	private static final String MSG_CANCELAMENTO_LOTE_SUCESSO = "Lote cancelado com sucesso.";
	private static final String MSG_CANCELAMENTO_LOTE_ERRO = "Lote já foi a campo e não é possível cancelar.";
	private static final String MSG_RELATORIO_SUCESSO = "Relatório Gerado com Sucesso!";
	private static final String MSG_RELATORIO_FALHA = "Não foi encontrado dados para gerar o relatório!";
	private static final String LISTA_SITUACAO_CONTRATO = "listaSituacaoContrato";
	private static final String MSG_CANCELAMENTO_ITEM_LOTE_SUCESSO = "Itens do Lote cancelado com sucesso.";
	private static final String LISTA_TURNO = "listaTurno";
	private static final String LISTA_RAMO_ATIVIDADE = "listaRamoAtividade";
	private static final String LISTA_MODELO_MEDIDOR = "listaModeloMedidor";	

	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();
	
	@Autowired
	ControladorComunicacao controladorComunicacao;
	
	@Autowired
	ControladorServicoTipo controladorServicoTipo;
	
	@Autowired
	ControladorFeriado controladorFeriado;
	
	@Autowired
	ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	ControladorModeloContrato controladorModeloContrato;
	
	@Autowired
	ControladorEntidadeConteudo controladorEntidadeConteudo;	

	@Autowired
	ControladorServicoAutorizacao controladorServicoAutorizacao;
	
	@Autowired
	ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	ControladorImovel controladorImovel;
	
	@RequestMapping("exibirPesquisaComunicacao")
	public ModelAndView exibirPesquisaComunicacao(HttpSession session, HttpServletResponse response) throws GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaComunicacao");
		
		model.addObject(LISTA_CITY_GATE, fachada.listarCityGate());
		model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject(LISTA_SITUACAO_MEDIDOR, fachada.listarSituacaoMedidor());
		model.addObject(LISTA_SITUACAO_PONTO_CONSUMO, fachada.listarSituacoesPontoConsumo());
		model.addObject(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addObject(LISTA_MODELO_MEDIDOR, fachada.listarModeloMedidor());
		model.addObject(LISTA_RAMO_ATIVIDADE, fachada.listarRamoAtividade());

		
		session.removeAttribute("listaPontoConsumoInserido");

		
		return model;
	}
	
	@RequestMapping("pesquisarComunicacao")
	public ModelAndView pesquisarComunicacao(@ModelAttribute("FiltroLoteComunicacaoDTO") FiltroLoteComunicacaoDTO filtroLoteComunicacaoDTO, BindingResult result,
			HttpServletRequest request) throws HibernateException, GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaComunicacao");
		Collection<PontoConsumo> colecaoPontoConsumo = controladorComunicacao.consultarPontosLoteComunicacao(filtroLoteComunicacaoDTO);
		request.getSession().removeAttribute("listaPontoConsumoInserido");
		request.getSession().removeAttribute("fluxoAlteracao");
		
		model.addObject(LISTA_CITY_GATE, fachada.listarCityGate());
		model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
		model.addObject(LISTA_PONTO_CONSUMO, colecaoPontoConsumo);
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject(LISTA_SITUACAO_MEDIDOR, fachada.listarSituacaoMedidor());
		model.addObject(LISTA_SITUACAO_PONTO_CONSUMO, fachada.listarSituacoesPontoConsumo());
		model.addObject(LISTA_SITUACAO_CONTRATO, controladorModeloContrato.obterListaSituacaoContrato());
		model.addObject(LISTA_MODELO_MEDIDOR, fachada.listarModeloMedidor());
		model.addObject(LISTA_RAMO_ATIVIDADE, fachada.listarRamoAtividade());
		model.addObject("isPesquisaComunicacao", Boolean.TRUE);

		request.getSession().setAttribute(LISTA_PONTO_CONSUMO_SESSAO, colecaoPontoConsumo);
		model.addObject(COMUNICACAO, filtroLoteComunicacaoDTO);
		
		
		return model;
	}
	
	
	@RequestMapping("exibirGerarComunicacaoLote")
	public ModelAndView exibirGerarComunicacaoLote(@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias, @RequestParam("servicoTipo") ServicoTipo servicoTipo, HttpServletRequest request) throws NegocioException {
		ModelAndView model = new ModelAndView("exibirGerarComunicacaoLote");
		String tipoGeracaoLote = request.getParameter("tipoGeracaoLote");
		
		FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO = new FiltroGeracaoComunicaoLoteDTO();
		filtroGeracaoComunicacaoLoteDTO.setServicoTipo(servicoTipo);
		
		Collection<PontoConsumo> colecaoPontoConsumo = obterColecaoPontoConsumo(request, tipoGeracaoLote);
		List<Long> chavesPontoConsumo = obterChavesPontoConsumo(chavesPrimarias, request, tipoGeracaoLote, colecaoPontoConsumo);
		Collection<PontoConsumo> colecaoPontoConsumoSelecionado = colecaoPontoConsumo.stream().filter(p -> chavesPontoConsumo.contains(p.getChavePrimaria())).collect(Collectors.toList());
		

		
		request.getSession().setAttribute("listaPontosSelecionados", colecaoPontoConsumoSelecionado);
		
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject("filtroGeracaoComunicacaoLoteDTO", filtroGeracaoComunicacaoLoteDTO);
		model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));

		return model;
	}
	
	@RequestMapping("exibirGerarComunicacaoLoteAlteracao")
	public ModelAndView exibirGerarComunicacaoLoteAlteracao(
			@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO,
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			@RequestParam("servicoTipo") ServicoTipo servicoTipo, HttpServletRequest request) throws HibernateException, GGASException {
		
		ModelAndView model = new ModelAndView("exibirGerarComunicacaoLote");
		
		FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO = new FiltroGeracaoComunicaoLoteDTO();
		filtroGeracaoComunicacaoLoteDTO.setServicoTipo(servicoTipo);
		
		Collection<LoteComunicacao> colecaoLoteComunicacao = controladorComunicacao.consultarLoteComunicacao(filtroAcompanhamentoLoteDTO);
		
		LoteComunicacao lote = colecaoLoteComunicacao.stream().filter(p -> p.getChavePrimaria() == chavePrimaria).findFirst().get();
		Collection<ItemLoteComunicacao> items = lote.getItens();
		Collection<PontoConsumo> colecaoPontoConsumoSelecionado = items.stream().map(p -> p.getPontoConsumo()).collect(Collectors.toList());
		
		request.getSession().setAttribute("listaPontosSelecionados", colecaoPontoConsumoSelecionado);
		
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject("filtroGeracaoComunicacaoLoteDTO", filtroGeracaoComunicacaoLoteDTO);
		model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		
		return model;
	}

	private Collection<PontoConsumo> obterColecaoPontoConsumo(HttpServletRequest request, String tipoGeracaoLote) {
		if("tab-automatica".equals(tipoGeracaoLote)) {
			return (Collection<PontoConsumo>) request.getSession().getAttribute(LISTA_PONTO_CONSUMO_SESSAO);
		} else {
			return (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontoConsumoInserido");
		}
	}

	private List<Long> obterChavesPontoConsumo(Long[] chavesPrimarias, HttpServletRequest request,
			String tipoGeracaoLote, Collection<PontoConsumo> colecaoPontoConsumo) {
		List<Long> chavesPontoConsumoTipoGeracaoLote;
		if("tab-automatica".equals(tipoGeracaoLote)) {
			chavesPontoConsumoTipoGeracaoLote = Arrays.asList(chavesPrimarias);
		} else {
			chavesPontoConsumoTipoGeracaoLote = Arrays.asList(Util.collectionParaArrayChavesPrimarias(colecaoPontoConsumo));
		}
		return chavesPontoConsumoTipoGeracaoLote;
	}
	
	@RequestMapping("gerarComunicacaoLote")
	public ModelAndView gerarComunicacaoLote(@ModelAttribute("FiltroGeracaoComunicaoLoteDTO") FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO, HttpServletRequest request, HttpServletResponse response) throws GGASException {
		
		ModelAndView model = new ModelAndView("exibirPesquisaComunicacao");
		model.addObject("isGerarComunicacao", Boolean.TRUE);
		
		Collection<ComunicacaoLoteVO> listaComunicacaoLoteVO = (Collection<ComunicacaoLoteVO>) request.getSession().getAttribute("listaPontoConsumoRoteirizado");
		
		controladorComunicacao.gerarLote(listaComunicacaoLoteVO, filtroGeracaoComunicacaoLoteDTO.getServicoTipo(), filtroGeracaoComunicacaoLoteDTO.getIsGerarAutorizacaoServico());
		
		List<byte[]> relatorios =
				controladorComunicacao.gerarRelatorio(listaComunicacaoLoteVO, filtroGeracaoComunicacaoLoteDTO.getServicoTipo());
		
		request.getSession().removeAttribute("listaPontoConsumoRoteirizado");
		request.getSession().removeAttribute(LISTA_PONTO_CONSUMO_SESSAO);
		request.getSession().removeAttribute("listaPontosSelecionados");
		request.getSession().setAttribute("relatorioComunicacaoLote", relatorios);
		model.addObject(LISTA_CITY_GATE, fachada.listarCityGate());
		model.addObject(LISTA_SEGMENTO, fachada.listarSegmento());
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject(LISTA_SITUACAO_MEDIDOR, fachada.listarSituacaoMedidor());
		
		mensagemSucesso(model, MSG_SUCESSO_LOTE_INCLUIDO);
		
		request.getSession().removeAttribute("listaPontoConsumoInserido");
		
		return model;
	}

	@RequestMapping("roteirizarComunicacaoLote")
	public ModelAndView roteirizarComunicacaoLote(@ModelAttribute("FiltroGeracaoComunicaoLoteDTO") FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO, @RequestParam(value="diasSemana", required = false) Integer[] diasSemana , @RequestParam(value = "tempoExecucao", required = false) Long tempoExecucao,  @RequestParam Map<String, String> parametroHorariosTurno, HttpServletRequest request, HttpServletResponse response) throws GGASException, IOException {
		ModelAndView model = new ModelAndView("exibirGerarComunicacaoLote");

		model.addObject("filtroGeracaoComunicacaoLoteDTO", filtroGeracaoComunicacaoLoteDTO);
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
		model.addObject("isGerarComunicacao", Boolean.TRUE);

		
		EntidadeConteudo turnos[] = filtroGeracaoComunicacaoLoteDTO.getTurno();
		
        List<Long> turnosSelecionados = new ArrayList<Long>();

		if (turnos != null && turnos.length > 0) {
			for (EntidadeConteudo turno : turnos) {
				turnosSelecionados.add(turno.getChavePrimaria());
			}
		}
        
        model.addObject("turnosSelecionados", turnosSelecionados);

		Map<String, String> horariosTurno = new HashMap<>();
		for (Map.Entry<String, String> entry : parametroHorariosTurno.entrySet()) {
			if (entry.getKey().startsWith("horario_")) {
				String turno = entry.getKey().replace("horario_", "");
				horariosTurno.put(turno, entry.getValue());
			}
		}

		if(request.getSession().getAttribute("fluxoAlteracao") == Boolean.TRUE) {
			request.getSession().setAttribute("tempoExecucao", tempoExecucao);
		}else {
			request.getSession().removeAttribute("tempoExecucao");
		}
		
		Map<String, Object> erros = filtroGeracaoComunicacaoLoteDTO.validarDados();
		if (!erros.isEmpty()) {
			mensagemErro(model, new NegocioException(erros));
		} else {
			ServicoTipo servicoTipo = filtroGeracaoComunicacaoLoteDTO.getServicoTipo();
			
			
			if(servicoTipo.getListaAgendamentosTurno() == null || servicoTipo.getListaAgendamentosTurno().isEmpty() || !consultarExistenciaTurno(filtroGeracaoComunicacaoLoteDTO.getTurno(), servicoTipo.getListaAgendamentosTurno())) {
				mensagemErro(model, new ArrayList<>(Arrays.asList("Não existe equipe/turno Cadastrada para esse Serviço!")));
			} else {
				Collection<PontoConsumo> colecaoPontoConsumo = (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontosSelecionados");
				
				Collection<ComunicacaoLoteVO> listaPontoConsumoRoteirizado = controladorComunicacao.roterizacaoComunicacao(colecaoPontoConsumo);

				Collection<ComunicacaoLoteVO> listaPontoConsumoRoteirizadoPopulado = new ArrayList<ComunicacaoLoteVO>();
				
				model.addObject("diasSemana", Arrays.asList(diasSemana));

				try {
					listaPontoConsumoRoteirizadoPopulado = popularComunicacaoLoteVO(listaPontoConsumoRoteirizado,
							filtroGeracaoComunicacaoLoteDTO, diasSemana, horariosTurno);
				} catch (NegocioException e) {
					mensagemErro(model, e.getMessage());
				}
				
				
				model.addObject("listaPontoConsumo", listaPontoConsumoRoteirizadoPopulado);
				
				request.getSession().setAttribute("listaPontoConsumoRoteirizado", listaPontoConsumoRoteirizadoPopulado);
				
			}
			
		}
		
		return model;
	}
	
	private boolean consultarExistenciaTurno(EntidadeConteudo turnos[],
			Collection<ServicoTipoAgendamentoTurno> listaAgendamentosTurno) {
		
		if(turnos != null && turnos.length > 0) {
			return listaAgendamentosTurno.stream()
		            .anyMatch(p -> Arrays.stream(turnos)
                            .anyMatch(turno -> p.getTurno().getChavePrimaria() == turno.getChavePrimaria()));
		}
		
		return Boolean.TRUE;
	}

	private Collection<ComunicacaoLoteVO> popularComunicacaoLoteVO(
			Collection<ComunicacaoLoteVO> listaPontoConsumoRoteirizado,
			FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO, Integer[] diasSemana, Map<String, String> horariosTurno)
			throws GGASException, IOException {
		Boolean isMudouTurno = Boolean.TRUE;
		Integer sequencial = 1;
		LocalTime fimTurnoManha = LocalTime.of(12, 0);
		LocalTime fimTurnoTarde = LocalTime.of(17, 0);
		LocalTime fimTurnoNoite = LocalTime.of(23, 59);

		Integer quantidadePontosRoteirizados = listaPontoConsumoRoteirizado.size();
		Boolean isMetadadeTempoExecResid = filtroGeracaoComunicacaoLoteDTO.getServicoTipo()
				.getIndicadorMetadeTempoExecucao();
		List<Integer> listaDiasSemana = Arrays.asList(diasSemana);
		Boolean isMudouDia = Boolean.TRUE;

		Calendar c = Calendar.getInstance();
		Calendar dataAtual = Calendar.getInstance();
		c.setTime(Util.converterCampoStringParaData("Data Execução Inicial",
				filtroGeracaoComunicacaoLoteDTO.getDataServico(), Constantes.FORMATO_DATA_BR));

		Integer anoExecucao = c.get(Calendar.YEAR);

		ItemLoteComunicacao ultimoItem = controladorComunicacao
				.obterUltimoItemLoteComunicacao(anoExecucao);
		Integer tentativas = 0;

		String idMunicipio = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MUNICIPIO_IMPLANTADOS);

		if (ultimoItem != null) {
			String[] sequencialItem = ultimoItem.getSequencialItem().split("\\.");
			Integer anoSequencial = Integer.valueOf(sequencialItem[1]);

			if (anoSequencial.compareTo(dataAtual.get(Calendar.YEAR)) == 0) {
				sequencial = Integer.valueOf(sequencialItem[0]) + 1;
			}

		}

		String anoCorrente = "." + c.get(Calendar.YEAR);

		Collection<ComunicacaoLoteVO> listaRetorno = new ArrayList<ComunicacaoLoteVO>();

		do {
			Boolean isDiaUtil = controladorFeriado.isDiaUtil(c.getTime(), Long.valueOf(idMunicipio), Boolean.FALSE);
			if (!listaDiasSemana.contains(c.get(Calendar.DAY_OF_WEEK) - 1) || !isDiaUtil) {
				do {
					c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) + 1);

					isDiaUtil = controladorFeriado.isDiaUtil(c.getTime(), Long.valueOf(idMunicipio), Boolean.FALSE);

				} while (!listaDiasSemana.contains(c.get(Calendar.DAY_OF_WEEK) - 1) && !isDiaUtil);
			}

			dataAtual.setTime(c.getTime());

			if(anoExecucao != dataAtual.get(Calendar.YEAR)) {
				anoExecucao = dataAtual.get(Calendar.YEAR);

				ultimoItem = controladorComunicacao
						.obterUltimoItemLoteComunicacao(anoExecucao);

				if (ultimoItem != null) {
					String[] sequencialItem = ultimoItem.getSequencialItem().split("\\.");
					Integer anoSequencial = Integer.valueOf(sequencialItem[1]);

					if (anoSequencial.compareTo(dataAtual.get(Calendar.YEAR)) == 0) {
						sequencial = Integer.valueOf(sequencialItem[0]) + 1;
					}

				} else {
					sequencial = 1;
				}

				anoCorrente = "." + dataAtual.get(Calendar.YEAR);
			}

			Map<String, List<ServicoTipoAgendamentoTurno>> mapa = controladorComunicacao
					.montarMapaServicoTipoAgendamento(filtroGeracaoComunicacaoLoteDTO.getServicoTipo(), filtroGeracaoComunicacaoLoteDTO.getTurno());

			for (Entry<String, List<ServicoTipoAgendamentoTurno>> entry : mapa.entrySet()) {

				List<ServicoTipoAgendamentoTurno> listaRemover = new ArrayList<ServicoTipoAgendamentoTurno>();

				if (!horariosTurno.isEmpty()) {

					String[] horario = null;

					if (!isMudouDia) {
						horario = horariosTurno.get(entry.getKey()).split(":");
					} else {
						horario = horariosTurno.get("MANHÃ").split(":");
						c.set(Calendar.DAY_OF_YEAR, dataAtual.get(Calendar.DAY_OF_YEAR));
					}

					c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horario[0]));
					c.set(Calendar.MINUTE, Integer.valueOf(horario[1]));

				} else {
					if (entry.getKey().equals("TARDE") && !isMudouDia) {
						c.set(Calendar.HOUR_OF_DAY, 13);
						c.set(Calendar.MINUTE, 0);
					} else if (entry.getKey().equals("NOITE") && !isMudouDia) {
						c.set(Calendar.HOUR_OF_DAY, 18);
						c.set(Calendar.MINUTE, 0);
					} else {
						String[] hora = filtroGeracaoComunicacaoLoteDTO.getHoraServico().split(":");
						c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hora[0]));
						c.set(Calendar.MINUTE, Integer.valueOf(hora[1]));
						c.set(Calendar.DAY_OF_YEAR, dataAtual.get(Calendar.DAY_OF_YEAR));
					}
				}


				do {
					List<ServicoTipoAgendamentoTurno> listaTurno = entry.getValue();

					for (ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno : listaTurno) {

						if (quantidadePontosRoteirizados == 0) {
							break;
						}

						ComunicacaoLoteVO comunicacaoLoteVO = listaPontoConsumoRoteirizado.iterator().next();

						if (servicoTipoAgendamentoTurno.getQuantidadeAgendamentosTurno() == 0) {
							listaRemover.add(servicoTipoAgendamentoTurno);
							break;
						}

						servicoTipoAgendamentoTurno.setQuantidadeAgendamentosTurno(
								servicoTipoAgendamentoTurno.getQuantidadeAgendamentosTurno() - 1);


						Long tempoExecucao = filtroGeracaoComunicacaoLoteDTO.getTempoExecucao();
						PontoConsumo ponto = comunicacaoLoteVO.getPontoConsumo();

						if (isMetadadeTempoExecResid && "RESIDENCIAL".equals(ponto.getSegmento().getDescricao())
								&& (ponto.getImovel().isIndividual() != null && ponto.getImovel().isIndividual())) {
							tempoExecucao = tempoExecucao / 2;
						}

						if (isMudouTurno) {
							controladorComunicacao.calcularDistanciaPontoConsumo(comunicacaoLoteVO);

							isMudouTurno = Boolean.FALSE;
						}

						c.add(Calendar.MINUTE, Integer.valueOf(comunicacaoLoteVO.getTempoDistancia()));

						String periodoExecucao = "";

						Calendar calendarioHoraFinal = Calendar.getInstance();
						calendarioHoraFinal.setTime(c.getTime());

						calendarioHoraFinal.add(Calendar.MINUTE, 60);

						Date dataIncrementada = calendarioHoraFinal.getTime();
						String horaInicialExecucao = Util
								.converterDataParaStringHoraMinutoComCaracteresEspeciais(c.getTime());
						Date dataExecucao = c.getTime();
						String horaFinalExecucao = Util
								.converterDataParaStringHoraMinutoComCaracteresEspeciais(dataIncrementada);
						LocalTime horarioFinal = LocalTime.of(calendarioHoraFinal.get(Calendar.HOUR_OF_DAY),
								calendarioHoraFinal.get(Calendar.MINUTE));

						c.add(Calendar.MINUTE, tempoExecucao.intValue());

						if ((servicoTipoAgendamentoTurno.getTurno().getDescricao().equals("TARDE")
								&& horarioFinal.isAfter(fimTurnoTarde))
								|| (servicoTipoAgendamentoTurno.getTurno().getDescricao().equals("MANHÃ")
								&& horarioFinal.isAfter(fimTurnoManha))
								|| (servicoTipoAgendamentoTurno.getTurno().getDescricao().equals("NOITE")
								&& horarioFinal.isAfter(fimTurnoNoite))) {
							isMudouTurno = Boolean.TRUE;
							listaRemover.add(servicoTipoAgendamentoTurno);
							break;
						}

						if (isMudouDia) {
							isMudouDia = Boolean.FALSE;
						}

						periodoExecucao = horaInicialExecucao + " horas e " + horaFinalExecucao + " horas";

						comunicacaoLoteVO.setSequencial(sequencial.toString() + anoCorrente);
						comunicacaoLoteVO.setTempoExecucao(String.valueOf((tempoExecucao)));
						comunicacaoLoteVO.setDataExecucao(DataUtil.converterDataParaString(dataExecucao));
						comunicacaoLoteVO.setPeriodoExecucao(periodoExecucao);
						comunicacaoLoteVO.setHoraInicialExecucao(horaInicialExecucao);
						comunicacaoLoteVO.setServicoTipoAgendamentoTurno(servicoTipoAgendamentoTurno);
						comunicacaoLoteVO.setCodigoLegado(comunicacaoLoteVO.getPontoConsumo().getCodigoLegado());
						comunicacaoLoteVO.setEquipe(servicoTipoAgendamentoTurno.getEquipe().getNome());
						comunicacaoLoteVO.setSegmento(comunicacaoLoteVO.getPontoConsumo().getSegmento().getDescricao());

						quantidadePontosRoteirizados--;
						listaRetorno.add(comunicacaoLoteVO);
						listaPontoConsumoRoteirizado.remove(comunicacaoLoteVO);

						sequencial++;
						tentativas = 0;

					}

					listaTurno.removeAll(listaRemover);

				} while (!entry.getValue().isEmpty() && quantidadePontosRoteirizados != 0);
				isMudouTurno = Boolean.TRUE;
			}

			Date proximoDiaUtil = Util.adicionarDiasData(c.getTime(), 1);
			c.setTime(proximoDiaUtil);
			dataAtual.setTime(c.getTime());
			isMudouDia = Boolean.TRUE;
			tentativas++;

			if(tentativas >= 3) {
				throw new NegocioException("Não foi possível roteirizar com esses parâmetros, tente novamente com outros!");
			}

		} while (quantidadePontosRoteirizados != 0);

		return listaRetorno;

	}

	@RequestMapping("exibirRelatorioComunicacaoLote")
	public String exibirRelatorioComunicacaoLote(HttpServletRequest request, HttpServletResponse response)
			throws IOException, GGASException {
		List<byte[]> relatorios = (List<byte[]>) request.getSession().getAttribute("relatorioComunicacaoLote");

		gerarRelatorio(relatorios, response, Boolean.TRUE);
		request.getSession().removeAttribute("relatorioComunicacaoLote");

		return null;
	}

	@RequestMapping("exibirPesquisaAcompanhamentoComunicacao")
	public ModelAndView exibirPesquisaAcompanhamentoComunicacao(HttpSession session, HttpServletResponse response, HttpServletRequest request) throws GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaAcompanhamentoComunicacao");
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());

		request.getSession().removeAttribute("listaLoteAcompanhamentoReq");
		request.getSession().removeAttribute("loteComunicacaoASProtocolo");
		
		return model;
	}
	
	@RequestMapping("pesquisarAcompanhamentoComunicacao")
	public ModelAndView pesquisarAcompanhamentoComunicacao(@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO, BindingResult result,
			HttpServletRequest request) throws HibernateException, GGASException {
		ModelAndView model = new ModelAndView("exibirPesquisaAcompanhamentoComunicacao");
		
		Collection<LoteComunicacao> colecaoLoteComunicacao = controladorComunicacao.consultarLoteComunicacao(filtroAcompanhamentoLoteDTO);
		
		request.getSession().removeAttribute("listaLoteAcompanhamentoReq");
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject("listaLoteAcompanhamento", colecaoLoteComunicacao);
		model.addObject("acompanhamento", filtroAcompanhamentoLoteDTO);
		request.getSession().setAttribute("listaLoteAcompanhamentoReq", colecaoLoteComunicacao);
		
		if(request.getSession().getAttribute("loteCancelado") != null) {
			Boolean loteCancelado = (Boolean) request.getSession().getAttribute("loteCancelado");
			
			if(loteCancelado) {
				mensagemSucesso(model, MSG_CANCELAMENTO_LOTE_SUCESSO);
			} else {
				mensagemErro(model, MSG_CANCELAMENTO_LOTE_ERRO);
			}
			
			request.getSession().removeAttribute("loteCancelado");
		}
		
		if(request.getSession().getAttribute("itemCancelado") != null) {
			mensagemSucesso(model, MSG_CANCELAMENTO_ITEM_LOTE_SUCESSO);
			request.getSession().removeAttribute("itemCancelado");
		}
		
		
		
		return model;
	}
	
	@RequestMapping("exibirDetalhamentoLoteComunicacao")
	public ModelAndView exibirDetalhamentoLoteComunicacao(@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO, 
			@RequestParam("idLoteComunicacao") Long idLoteComunicacao,BindingResult result,
			HttpServletRequest request) {
		ModelAndView model = new ModelAndView("exibirPesquisaAcompanhamentoComunicacao");
		Collection<LoteComunicacao> colecaoLoteComunicacao = (Collection<LoteComunicacao>) request.getSession().getAttribute("listaLoteAcompanhamentoReq");
		LoteComunicacao loteComunicacao = colecaoLoteComunicacao.stream().filter(p -> p.getChavePrimaria() == idLoteComunicacao).findFirst().get();
		
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		model.addObject("acompanhamento", filtroAcompanhamentoLoteDTO);
		model.addObject("listaLoteAcompanhamento", colecaoLoteComunicacao);
		model.addObject("listaItensLoteComunicacao", loteComunicacao.getItens().stream()
				.sorted(Comparator.comparing(ItemLoteComunicacao::getChavePrimaria)).collect(Collectors.toList()));
		model.addObject("numeroLote", idLoteComunicacao);
		
		return model;
	}
	
	private void gerarRelatorio(List<byte[]> relatorios, HttpServletResponse response, Boolean tipoRelatorio) throws GGASException {

		FormatoImpressao formatoImpressao = tipoRelatorio ? FormatoImpressao.PDF : FormatoImpressao.XLS;

		SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();

		fileName.append(" attachment; filename=relatorioComunicacao_");
		fileName.append(diaMesAno);
		fileName.append("_");
		fileName.append(hora);
		fileName.append(FormatoImpressao.obterFormatoRelatorio(formatoImpressao));


		byte[] relatorio = null;
		if(tipoRelatorio) {
			relatorio = RelatorioUtil.unificarRelatoriosPdf(relatorios);
		} else {
			relatorio = relatorios.get(0);
		}
		
		try {
			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(CONTENT_DISPOSITION, fileName.toString());
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new GGASException(e);
		}
	}
	
	
	@RequestMapping("imprimirSegundaViaComunicacao")
	public void imprimirSegundaViaComunicacao(@RequestParam("idLoteComunicacao") Long idLoteComunicacao,
			HttpServletRequest request, HttpServletResponse response) throws GGASException {
		
		LoteComunicacao loteComunicacao = (LoteComunicacao) controladorComunicacao.obter(idLoteComunicacao, LoteComunicacaoImpl.class);
		
		Collection<ComunicacaoLoteVO> dadosRelatorio = this.montarObjetoRelatorio(loteComunicacao);
		
		List<byte[]> relatorio = controladorComunicacao.gerarRelatorio(dadosRelatorio, loteComunicacao.getServicoTipo());
		
		this.gerarRelatorio(relatorio, response, Boolean.TRUE);
		
	}
	
	private Collection<ComunicacaoLoteVO> montarObjetoRelatorio(LoteComunicacao loteComunicacao) {
		Collection<ComunicacaoLoteVO> retorno = new ArrayList<>();
		Collection<ItemLoteComunicacao> itens = loteComunicacao.getItens().stream().filter(p -> p.isHabilitado())
				.sorted(Comparator.comparing(ItemLoteComunicacao::getChavePrimaria)).collect(Collectors.toList());
		
		
		for(ItemLoteComunicacao item : itens) {
			
			ComunicacaoLoteVO comunicacaoLoteVO = new ComunicacaoLoteVO();
			comunicacaoLoteVO.setServicoTipoAgendamentoTurno(item.getServicoTipoAgendamentoTurno());
			comunicacaoLoteVO.setPontoConsumo(item.getPontoConsumo());
			comunicacaoLoteVO.setSequencial(item.getSequencialItem());
			comunicacaoLoteVO.setTempoExecucao(item.getTempoExecucao());
			comunicacaoLoteVO.setDataExecucao(DataUtil.converterDataParaString(item.getDataHoraExecucao()));
			comunicacaoLoteVO.setDescricaoPontoConsumo(item.getPontoConsumo().getDescricao());
			comunicacaoLoteVO.setEnderecoFormatado(item.getPontoConsumo().getEnderecoFormatado());
			comunicacaoLoteVO.setDataExecucaoAnterior(item.getDataHoraExecucaoAnterior());
			comunicacaoLoteVO.setSequencialAnterior(item.getSequencialItemAnterior());
			
			
			Date horaExecucaoFormatada = DataUtil.arredondarMinutosHora(item.getDataHoraExecucao());
			
			String horaInicialExecucao = Util.converterDataParaStringHoraMinutoComCaracteresEspeciais(horaExecucaoFormatada);
			comunicacaoLoteVO.setHoraInicialExecucao(horaInicialExecucao);
			
			Calendar c = Calendar.getInstance();
			c.setTime(horaExecucaoFormatada);
			c.add(Calendar.MINUTE, 60);
			Date dataIncrementada = c.getTime();
			
			String periodoExecucao = horaInicialExecucao + " horas e "
					+ Util.converterDataParaStringHoraMinutoComCaracteresEspeciais(dataIncrementada) + " horas";
			comunicacaoLoteVO.setPeriodoExecucao(periodoExecucao);
			
			retorno.add(comunicacaoLoteVO);
		}
		
		
		return retorno;
	}
	
	@RequestMapping(value = "buscarPontosConsumo", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Map<String, Object> buscarPontosConsumo(@RequestParam String q, @RequestParam String servicoTipo, @RequestParam(required = false, value="page") Integer page) throws HibernateException, GGASException {
		
		Integer _page = page != null ? page : 0;
		Map<String, Object> results = new HashMap<String, Object>();
		Collection<PontoConsumo> colecaoPontoConsumo = new ArrayList<>();
		
			if(q.matches("[0-9]+")) {
				colecaoPontoConsumo = controladorComunicacao.filtrarPontosParaLoteDeComunicacao(null, q, servicoTipo, _page);
			} else {
				colecaoPontoConsumo = controladorComunicacao.filtrarPontosParaLoteDeComunicacao(q, null, servicoTipo, _page);
			}
		
		List<HashMap<String, String>> collect = colecaoPontoConsumo.stream()
				.map(r -> new HashMap<String, String>() {{
					put("id", String.valueOf(r.getChavePrimaria()));
					put("text",
							String.format("%s - %s - %s",
											r.getCodigoLegado(),
											r.getDescricao().trim(),
											r.getImovel().getNome()));
				}}).collect(Collectors.toList());
		results.put("results",collect);
		if(colecaoPontoConsumo.size() > 0) {
			results.put("pagination",new HashMap<String, Boolean>(){{put("more", true);}});
		}
		return results;
	}
	
	
	@RequestMapping("incluirPontoGrid")
	public ModelAndView incluirPontoGrid(HttpServletRequest request, Model model)
			throws NumberFormatException, NegocioException {
		ModelAndView gridPontoConsumo = new ModelAndView("gridPontoConsumo");

		Collection<PontoConsumo> listaPontoConsumoInserido = (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontoConsumoInserido");

		if (listaPontoConsumoInserido == null || listaPontoConsumoInserido.isEmpty()) {
			listaPontoConsumoInserido = new ArrayList<>();
		}

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(Long.valueOf(request.getParameter("idPontoConsumo")));

		boolean contemPontoConsumo = listaPontoConsumoInserido.stream()
				.filter(o -> o.getChavePrimaria() == pontoConsumo.getChavePrimaria()).findFirst().isPresent();

		if (pontoConsumo != null && !contemPontoConsumo) {
			listaPontoConsumoInserido.add(pontoConsumo);
		}

		request.getSession().setAttribute("listaPontoConsumoInserido", listaPontoConsumoInserido);

		gridPontoConsumo.addObject("listaPontoConsumoInserido", listaPontoConsumoInserido);

		return gridPontoConsumo;
	}
	
	
	@RequestMapping("removerPontosGrid")
	public ModelAndView removerPontosGrid(HttpServletRequest request, Model model) {
		ModelAndView gridPontoConsumo = new ModelAndView("gridPontoConsumo");

		Collection<PontoConsumo> listaPontoConsumoInserido = (Collection<PontoConsumo>) request.getSession().getAttribute("listaPontoConsumoInserido");

		Integer indexLista = null;
		String indexListaStr = request.getParameter(INDEX_LISTA);
		if (StringUtils.isNotBlank(indexListaStr)) {
			indexLista = Integer.valueOf(indexListaStr);
		}

		if (listaPontoConsumoInserido != null && indexLista != null) {
			PontoConsumo pontoConsumo = ((ArrayList<PontoConsumo>) listaPontoConsumoInserido).get(indexLista.intValue());
			listaPontoConsumoInserido.remove(pontoConsumo);
		}

		request.getSession().setAttribute("listaPontoConsumoInserido", listaPontoConsumoInserido);

		gridPontoConsumo.addObject("listaPontoConsumoInserido", listaPontoConsumoInserido);

		return gridPontoConsumo;
	}
	
	@RequestMapping("gerarPlanilhaExcel")
	public void gerarPlanilhaExcel(@RequestParam(required = false, value = "chavesPrimarias") Long[] chavesPrimarias,
			@RequestParam("servicoTipo") ServicoTipo servicoTipo, HttpServletRequest request, Model model,
			HttpServletResponse response) throws NegocioException {

		String tipoGeracaoLote = request.getParameter("tipoGeracaoLote");
		
		
		FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO = new FiltroGeracaoComunicaoLoteDTO();
		filtroGeracaoComunicacaoLoteDTO.setServicoTipo(servicoTipo);
		
		Collection<PontoConsumo> colecaoPontoConsumo = obterColecaoPontoConsumo(request, tipoGeracaoLote);
		
		if(colecaoPontoConsumo == null) {
			colecaoPontoConsumo = controladorPontoConsumo.listarPontoConsumoChavesPrimarias(chavesPrimarias);
		}
		
		List<Long> chavesPontoConsumo = obterChavesPontoConsumo(chavesPrimarias, request, tipoGeracaoLote, colecaoPontoConsumo);
		
		Collection<PontoConsumo> colecaoPontoConsumoSelecionado = colecaoPontoConsumo.stream().filter(p -> chavesPontoConsumo.contains(p.getChavePrimaria())).collect(Collectors.toList());

		byte[] relatorio = controladorComunicacao.montarPlanilhaListagemPontosComunicacao(colecaoPontoConsumoSelecionado);

		FormatoImpressao formatoImpressao = FormatoImpressao.XLS;

		SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();

		fileName.append(" attachment; filename=relatorioListagemComunicacao_");
		fileName.append(diaMesAno);
		fileName.append("_");
		fileName.append(hora);
		fileName.append(".xls");

		try {
			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(CONTENT_DISPOSITION, fileName.toString());
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e.getMessage());
		}

	}
	
	@RequestMapping("cancelarLoteComunicacao")
	public ModelAndView cancelarLoteComunicacao(@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO, 
			@RequestParam("chavePrimaria") Long chavePrimaria,BindingResult result,
			HttpServletRequest request) throws HibernateException, GGASException, IOException {
		Collection<LoteComunicacao> colecaoLoteComunicacao = (Collection<LoteComunicacao>) request.getSession().getAttribute("listaLoteAcompanhamentoReq");
		LoteComunicacao loteComunicacao = colecaoLoteComunicacao.stream().filter(p -> p.getChavePrimaria() == chavePrimaria).findFirst().get();
		request.getSession().removeAttribute("listaLoteAcompanhamentoReq");
		
		
			if(filtroAcompanhamentoLoteDTO.getIsGerarLoteCancelamento()) {
				String dataAtual = DataUtil.converterDataParaString(DataUtil.zerarHoraParaData(new Date()));
				List<byte[]> relatorios = controladorComunicacao.gerarLoteCancelamento(loteComunicacao.getItens(), dataAtual);
				request.getSession().setAttribute("relatorioComunicacaoLote", relatorios);
				
				filtroAcompanhamentoLoteDTO = new FiltroAcompanhamentoLoteDTO();
				filtroAcompanhamentoLoteDTO.setDataGeracaoInicial(dataAtual);
				filtroAcompanhamentoLoteDTO.setDataGeracaoFinal(dataAtual);
				filtroAcompanhamentoLoteDTO.setLotesCartasCanceladas(Boolean.TRUE);
			}
		
		
		request.getSession().setAttribute("loteCancelado",
				controladorComunicacao.cancelarLote(loteComunicacao, getDadosAuditoria(request)));
		
		return pesquisarAcompanhamentoComunicacao(filtroAcompanhamentoLoteDTO, result, request);
	}
	
	public void cancelarLoteComunicacaoUnico(FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO,Long chavePrimaria,
			BindingResult result, HttpServletRequest request) throws HibernateException, IOException, GGASException {
		
		LoteComunicacao loteComunicacao = (LoteComunicacao) controladorComunicacao.obter(chavePrimaria, LoteComunicacaoImpl.class);;
		request.getSession().setAttribute("loteCancelado",
				controladorComunicacao.cancelarLote(loteComunicacao, getDadosAuditoria(request)));
	}
	
	@RequestMapping("exibirPesquisaRelatorioSinteticoComunicacaoInterrupcao")
	public String exibirPesquisaRelatorioSinteticoComunicacaoInterrupcao(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		model.addAttribute(LISTA_SERVICO_TIPO, controladorServicoTipo.consultarServicoTipoComunicacao());
		
		return "exibirPesquisaRelatorioSinteticoComunicacaoInterrupcao";
		
	}
	
	@RequestMapping("gerarRelatorioSinteticoComunicacaoInterrupcao")
	public String gerarRelatorioSinteticoComunicacaoInterrupcao(
			@ModelAttribute("FiltroRelatorioComunicacaoInterrupcaoDTO") FiltroRelatorioComunicacaoInterrupcaoDTO filtroRelatorioComunicacaoInterrupcaoDTO,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) throws HibernateException, GGASException {
		
		byte[] relatorio = controladorComunicacao
				.gerarRelatorioSinteticoComunicacaoInterrupcao(filtroRelatorioComunicacaoInterrupcaoDTO);	
		
		if(relatorio != null) {
			request.getSession().setAttribute("relatorioComunicacaoInterrupacao", relatorio);
			mensagemSucesso(model, MSG_RELATORIO_SUCESSO);

		} else {
			mensagemErro(model, MSG_RELATORIO_FALHA);
		}
		
		
		model.addAttribute(COMUNICACAO, filtroRelatorioComunicacaoInterrupcaoDTO);
		
		return exibirPesquisaRelatorioSinteticoComunicacaoInterrupcao(model, request, response);
	}
	
	
	private void gerarRelatorioComunicacaoInterrupcao(byte[] relatorio,
			FiltroRelatorioComunicacaoInterrupcaoDTO filtroRelatorioComunicacaoInterrupcaoDTO,
			HttpServletResponse response) throws GGASException {

		FormatoImpressao formatoImpressao = filtroRelatorioComunicacaoInterrupcaoDTO.getFormatoImpressao()
				? FormatoImpressao.PDF
				: FormatoImpressao.XLS;

		SimpleDateFormat df = new SimpleDateFormat(DD_MM_YYYY);
		String diaMesAno = df.format(Calendar.getInstance().getTime());
		df = new SimpleDateFormat("HH'h'mm");
		String hora = df.format(Calendar.getInstance().getTime());
		StringBuilder fileName = new StringBuilder();

		fileName.append(" attachment; filename=relatorioComunicacao_");
		fileName.append(diaMesAno);
		fileName.append("_");
		fileName.append(hora);
		fileName.append(filtroRelatorioComunicacaoInterrupcaoDTO.getFormatoImpressao() ? ".pdf" : ".xls");

		try {
			ServletOutputStream servletOutputStream;
			servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(CONTENT_DISPOSITION, fileName.toString());
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new GGASException(e);
		}
	}
	
	
	@RequestMapping("exibirRelatorioComunicacaoInterrupcao")
	public String exibirRelatorioComunicacaoInterrupcao(@ModelAttribute("FiltroRelatorioComunicacaoInterrupcaoDTO") FiltroRelatorioComunicacaoInterrupcaoDTO filtroRelatorioComunicacaoInterrupcaoDTO, HttpServletRequest request, HttpServletResponse response)
			throws IOException, GGASException {
		byte[] relatorio = (byte[]) request.getSession().getAttribute("relatorioComunicacaoInterrupacao");

		gerarRelatorioComunicacaoInterrupcao(relatorio, filtroRelatorioComunicacaoInterrupcaoDTO, response);
		request.getSession().removeAttribute("relatorioComunicacaoInterrupacao");

		return null;
	}
	
	
	@RequestMapping("exibirPesquisaRelatorioAntecendenciaMinimaAviso")
	public String exibirPesquisaRelatorioAntecendenciaMinimaAviso(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return "exibirPesquisaRelatorioAntecendenciaMinimaAviso";
		
	}
	
	@RequestMapping("gerarRelatorioRelatorioAntecendenciaMinimaAviso")
	public String gerarRelatorioRelatorioAntecendenciaMinimaAviso(
			@ModelAttribute("FiltroRelatorioComunicacaoInterrupcaoDTO") FiltroRelatorioComunicacaoInterrupcaoDTO filtroRelatorioComunicacaoInterrupcaoDTO,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, Model model) throws HibernateException, GGASException {
		
		byte[] relatorio = controladorComunicacao
				.gerarRelatorioAntecedenciaAviso(filtroRelatorioComunicacaoInterrupcaoDTO);	
		
		if(relatorio != null) {
			request.getSession().setAttribute("relatorioComunicacaoInterrupacao", relatorio);
			mensagemSucesso(model, MSG_RELATORIO_SUCESSO);

		} else {
			mensagemErro(model, MSG_RELATORIO_FALHA);
		}
		
		
		model.addAttribute(COMUNICACAO, filtroRelatorioComunicacaoInterrupcaoDTO);
		
		return exibirPesquisaRelatorioAntecendenciaMinimaAviso(model, request, response);
	}
	
	
	
	@RequestMapping("cancelarItemLoteComunicacao")
	public ModelAndView cancelarItemLoteComunicacao(
			@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO,
			@RequestParam("chavesPrimariasItem") Long[] chavesPrimariasItem,
			@RequestParam("idLoteComunicacao") Long idLoteComunicacao, BindingResult result, HttpServletRequest request)
			throws HibernateException, GGASException, IOException {
		
		Collection<LoteComunicacao> colecaoLoteComunicacao = (Collection<LoteComunicacao>) request.getSession().getAttribute("listaLoteAcompanhamentoReq");
		LoteComunicacao loteComunicacao = colecaoLoteComunicacao.stream().filter(p -> p.getChavePrimaria() == idLoteComunicacao).findFirst().get();
		
		List<Long> listaChavesItem = Arrays.asList(chavesPrimariasItem);
		
		Collection<ItemLoteComunicacao> itensRemover = loteComunicacao.getItens().stream()
				.filter(p -> listaChavesItem.contains(p.getChavePrimaria())).collect(Collectors.toList());
		
		if(filtroAcompanhamentoLoteDTO.getIsGerarLoteCancelamento()) {
			String dataAtual = DataUtil.converterDataParaString(DataUtil.zerarHoraParaData(new Date()));
			List<byte[]> relatorios = controladorComunicacao.gerarLoteCancelamento(itensRemover, dataAtual);
			request.getSession().setAttribute("relatorioComunicacaoLote", relatorios);
			
			filtroAcompanhamentoLoteDTO = new FiltroAcompanhamentoLoteDTO();
			filtroAcompanhamentoLoteDTO.setDataGeracaoInicial(dataAtual);
			filtroAcompanhamentoLoteDTO.setDataGeracaoFinal(dataAtual);
			filtroAcompanhamentoLoteDTO.setLotesCartasCanceladas(Boolean.TRUE);

		}
		
		Boolean removerLote = itensRemover.size() == loteComunicacao.getItens().size();
		
		controladorComunicacao.removerItens(itensRemover, removerLote);
		
		request.getSession().setAttribute("itemCancelado", Boolean.TRUE);

		return pesquisarAcompanhamentoComunicacao(filtroAcompanhamentoLoteDTO, result, request);
	}
	
	
	@RequestMapping("imprimirSegundaViaComunicacaoAS")
	public void imprimirSegundaViaComunicacaoAS(@RequestParam("idLoteComunicacao") Long idLoteComunicacao,
			HttpServletRequest request, HttpServletResponse response) throws GGASException {
		
		LoteComunicacao loteComunicacao = (LoteComunicacao) controladorComunicacao.obter(idLoteComunicacao, LoteComunicacaoImpl.class);
		
		Collection<Long> chavesServicoAutorizacao = loteComunicacao.getItens().stream()
				.filter(p -> p.getServicoAutorizacao() != null).map(p -> p.getServicoAutorizacao().getChavePrimaria())
				.collect(Collectors.toList());
		
		List<byte[]> relatorio = new ArrayList<byte[]>();
		
		
		for(Long chavePrimaria : chavesServicoAutorizacao) {
			relatorio.add(controladorServicoAutorizacao.gerarRelatorio(chavePrimaria, Boolean.FALSE, new HashSet<ServicoAutorizacaoHistorico>()));
		}
		
		this.gerarRelatorio(relatorio, response, Boolean.TRUE);
		
	}
	
	@RequestMapping("exibirAlteracaoLoteComunicacao")
	public ModelAndView exibirAlteracaoLoteComunicacao(
			@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
			@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO,
	        HttpServletRequest request, BindingResult result) throws HibernateException, GGASException, IOException {
	    
		ModelAndView model = new ModelAndView("exibirGerarComunicacaoLote");
		request.getSession().setAttribute("fluxoAlteracao", Boolean.TRUE);
		
		ServicoTipo servicoTipo;
	    LoteComunicacao loteComunicacao = (LoteComunicacao) controladorComunicacao.obter(chavePrimaria, LoteComunicacaoImpl.class);
		FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO = new FiltroGeracaoComunicaoLoteDTO();
	    if(loteComunicacao != null) {
	    	servicoTipo = loteComunicacao.getServicoTipo();
	    	request.getSession().setAttribute("chavePrimaria", chavePrimaria);
	    	request.getSession().setAttribute("loteComunicacao", loteComunicacao);
	    	request.getSession().setAttribute("servicoTipo", servicoTipo);
	    	filtroGeracaoComunicacaoLoteDTO.setServicoTipo(servicoTipo);
			
			Collection<LoteComunicacao> colecaoLoteComunicacao = controladorComunicacao.consultarLoteComunicacao(filtroAcompanhamentoLoteDTO);
			LoteComunicacao lote = colecaoLoteComunicacao.stream().filter(p -> p.getChavePrimaria() == chavePrimaria).findFirst().get();
			Collection<ItemLoteComunicacao> items = lote.getItens();
			Collection<PontoConsumo> colecaoPontoConsumoSelecionado = items.stream().map(p -> p.getPontoConsumo()).collect(Collectors.toList());
			
			request.getSession().setAttribute("listaPontosSelecionados", colecaoPontoConsumoSelecionado);
			model.addObject(LISTA_TURNO, controladorEntidadeConteudo.listarEntidadeConteudoPorDescricaoEntidadeClasse("Turno"));
			Long quantidadeTempoMedio =  ((LoteComunicacao) controladorComunicacao.obter(chavePrimaria, LoteComunicacaoImpl.class)).getServicoTipo().getQuantidadeTempoMedio();
			model.addObject("quantidadeTempoMedio",quantidadeTempoMedio);
	    }

	    return model;
	}
	
	
	@RequestMapping("alterarLote")
	public ModelAndView alterarLoteComunicacao(@ModelAttribute("FiltroGeracaoComunicaoLoteDTO") FiltroGeracaoComunicaoLoteDTO filtroGeracaoComunicacaoLoteDTO,
			@ModelAttribute("FiltroAcompanhamentoLoteDTO") FiltroAcompanhamentoLoteDTO filtroAcompanhamentoLoteDTO,
			HttpServletRequest request, HttpServletResponse response, BindingResult result) throws GGASException, HibernateException, IOException {
		Long chavePrimaria = (Long) request.getSession().getAttribute("chavePrimaria");
		cancelarLoteComunicacaoUnico(filtroAcompanhamentoLoteDTO, chavePrimaria, result, request);
		request.getSession().removeAttribute("fluxoAlteracao");
    	request.getSession().removeAttribute("chavePrimaria");
    	request.getSession().removeAttribute("loteComunicacao");
    	request.getSession().removeAttribute("servicoTipo");
    	
    	return gerarComunicacaoLote(filtroGeracaoComunicacaoLoteDTO, request, response);

	}
	
	@RequestMapping("gerarRelatorioComunicacao")
	public void gerarRelatorioComunicacao(@RequestParam("chavesPrimarias") Long[] idsLoteComunicacao,
			@RequestParam("tipoRelatorio") Boolean tipoRelatorio, @RequestParam("tipoImpressao") Boolean tipoImpressao,
			HttpServletResponse response) throws GGASException {
		
		Collection<LoteComunicacao> lotesComunicacao = controladorComunicacao.consultarLoteComunicacaoPorChavesPrimarias(idsLoteComunicacao);
		
		List<byte[]> relatorio = new ArrayList<byte[]>();
		
		relatorio.add(controladorComunicacao.gerarRelatorioAcompanhamentoComunicacao(lotesComunicacao, tipoRelatorio, tipoImpressao));
		
		this.gerarRelatorio(relatorio, response, tipoImpressao);
		
	}
	
	
	@RequestMapping(value = "buscarCondominio", method = RequestMethod.GET, consumes = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Map<String, Object> buscarCondominio(@RequestParam String q, @RequestParam(required = false, value="page") Integer page) throws HibernateException, GGASException {
		
		Integer _page = page != null ? page : 0;
		Map<String, Object> results = new HashMap<String, Object>();
		Collection<Imovel> colecaoImovel = controladorImovel.consultarImovelCondominio(q, _page);
		
		List<HashMap<String, String>> collect = colecaoImovel.stream()
				.map(r -> new HashMap<String, String>() {{
					put("id", String.valueOf(r.getChavePrimaria()));
					put("text",
							String.format("%s - %s",
											r.getChavePrimaria(),
											r.getNome()));
				}}).collect(Collectors.toList());
		results.put("results",collect);
		if(colecaoImovel.size() > 0) {
			results.put("pagination",new HashMap<String, Boolean>(){{put("more", true);}});
		}
		return results;
	}
	
	
	
}
