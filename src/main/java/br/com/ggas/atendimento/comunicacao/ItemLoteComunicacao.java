package br.com.ggas.atendimento.comunicacao;

import java.util.Date;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ItemLoteComunicacao extends EntidadeNegocio {
	String BEAN_ID_ITEM_LOTE_COMUNICACAO = "itemLoteComunicacao";
	
	/**
	 * @return the sequencialLote
	 */
	String getSequencialItem();

	/**
	 * @param sequencialItem
	 *            the sequencialItem to
	 *            set
	 */
	void setSequencialItem(String sequencialItem);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to
	 *            set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the loteComunicacao
	 */
	LoteComunicacao getLoteComunicacao();

	/**
	 * @param loteComunicacao
	 *            the loteComunicacao to
	 *            set
	 */
	void setLoteComunicacao(LoteComunicacao loteComunicacao);
	
	/**
	 * @return the dataHoraExecucao
	 */
	Date getDataHoraExecucao();
	
	/**
	 * @param dataHoraExecucao
	 *            the dataHoraExecucao to
	 *            set
	 */
	void setDataHoraExecucao(Date dataHoraExecucao);
	
	/**
	 * @return the servicoAutorizacao
	 */
	ServicoAutorizacao getServicoAutorizacao();
	
	/**
	 * @param servicoAutorizacao
	 *            the servicoAutorizacao to
	 *            set
	 */
	void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao);

	/**
	 * @return the servicoTipoAgendamentoTurno
	 */
	ServicoTipoAgendamentoTurno getServicoTipoAgendamentoTurno();

	/**
	 * @param servicoTipoAgendamentoTurno
	 *            the servicoTipoAgendamentoTurno to
	 *            set
	 */
	void setServicoTipoAgendamentoTurno(ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno);

	/**
	 * 
	 * @return - {@link EntregaDocumento}
	 */
	EntregaDocumento getDocumentoNaoEntregue();

	/**
	 * 
	 * @param documentoNaoEntregue - {@link EntregaDocumento}
	 */
	void setDocumentoNaoEntregue(EntregaDocumento documentoNaoEntregue);

	String getTempoExecucao();

	void setTempoExecucao(String tempoExecucao);

	Integer getMesProtocoloGerado();

	Integer getDiferencaDiasGeracaoRetornoProtocolo();

	String getSequencialItemAnterior();

	void setSequencialItemAnterior(String sequencialItemAnterior);

	Date getDataHoraExecucaoAnterior();

	void setDataHoraExecucaoAnterior(Date dataHoraExecucaoAnterior);
	
	ServicoAutorizacao getServicoAutorizacaoProtocolo();
	
	void setServicoAutorizacaoProtocolo (ServicoAutorizacao servicoAutorizacaoProtocolo);

}
