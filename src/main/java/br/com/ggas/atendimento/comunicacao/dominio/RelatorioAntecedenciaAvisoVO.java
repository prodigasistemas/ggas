package br.com.ggas.atendimento.comunicacao.dominio;

public class RelatorioAntecedenciaAvisoVO {
	private String mes;
	private Integer dias0 = 0;
	private Integer dias1 = 0;
	private Integer dias2 = 0;
	private Integer dias3 = 0;
	private Integer diasMaior3 = 0;
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Integer getDias0() {
		return dias0;
	}
	public void setDias0(Integer dias0) {
		this.dias0 = dias0;
	}
	public Integer getDias1() {
		return dias1;
	}
	public void setDias1(Integer dias1) {
		this.dias1 = dias1;
	}
	public Integer getDias2() {
		return dias2;
	}
	public void setDias2(Integer dias2) {
		this.dias2 = dias2;
	}
	public Integer getDias3() {
		return dias3;
	}
	public void setDias3(Integer dias3) {
		this.dias3 = dias3;
	}
	public Integer getDiasMaior3() {
		return diasMaior3;
	}
	public void setDiasMaior3(Integer diasMaior3) {
		this.diasMaior3 = diasMaior3;
	}

}
