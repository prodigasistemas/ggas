package br.com.ggas.atendimento.comunicacao.repositorio;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroAcompanhamentoLoteDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroLoteComunicacaoDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroRelatorioComunicacaoInterrupcaoDTO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

@Repository
public class RepositorioComunicacao extends RepositorioGenerico {

	
	/**
	 * Instantiates a new repositorio comunicacao.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioComunicacao(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}
	
	@Override
	public EntidadeNegocio criar() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public EntidadeNegocio criarLoteComunicacao() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LoteComunicacao.BEAN_ID_LOTE_COMUNICACAO);
	}
	
	public EntidadeNegocio criarItemLoteComunicacao() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ItemLoteComunicacao.BEAN_ID_ITEM_LOTE_COMUNICACAO);
	}

	@Override
	public Class<?> getClasseEntidade() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Class<?> getClasseEntidadePontoConsumoCityGate() {
		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoCityGate.BEAN_ID_PONTO_CONSUMO_CITY_GATE);
	}
	
	public Class<?> getClasseEntidadeTronco() {
		return ServiceLocator.getInstancia().getClassPorID(Tronco.BEAN_ID_TRONCO);
	}
	
	public Class<?> getClasseEntidadeLoteComunicacao() {
		return ServiceLocator.getInstancia().getClassPorID(LoteComunicacao.BEAN_ID_LOTE_COMUNICACAO);
	}

	public Class<?> getClasseEntidadeItemLoteComunicacao() {
		return ServiceLocator.getInstancia().getClassPorID(ItemLoteComunicacao.BEAN_ID_ITEM_LOTE_COMUNICACAO);
	}
	
	public Class<?> getClasseEntidadeContratoPontoConsumo() {
		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}
	
	public Class<?> getClasseEntidadePontoConsumo() {
		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}
	
	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {
		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}
	
	public Class<?> getClasseEntidadeServicoAutorizacao() {
		return ServicoAutorizacao.class;
	}
	
	public Class<?> getClasseEntidadeServicoTipo() {
		 return ServicoTipo.class;
	}		
	
	public Class<?> getClasseEntidadeEntregaDocumento(){
		return ServiceLocator.getInstancia().getClassPorID(EntregaDocumento.BEAN_ID_ENTREGA_DOCUMENTO);
	}
	
	/*
	 * @Override public Class<ServicoAutorizacao>
	 * getClasseEntidadeServicoAutorizacao() {
	 * 
	 * return ServicoAutorizacao.class; }
	 */
	
	/**
	 * Metodo que faz o filtro dos pontos de consumo a serem iseridos em um lote de comunicacao
	 * chavePrimaria e a descricao.
	 *
	 * @return Retorna a lista de ChamadoTipo com os atributos chavePrimaria e a descricao, preenchido.
	 * @throws GGASException 
	 * @throws HibernateException 
	 */
	@SuppressWarnings("unchecked")
	public Collection<PontoConsumo> filtrarPontosParaLoteDeComunicacao(FiltroLoteComunicacaoDTO filtroDTO) throws HibernateException, GGASException {
		
		StringBuilder hql = new StringBuilder("SELECT pc ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" cpc ");
		hql.append(" inner join cpc.contrato c ");
		hql.append(" inner join cpc.pontoConsumo pc ");
		hql.append(" inner join pc.quadraFace.quadra quadra ");
		hql.append(" inner join pc.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join pc.instalacaoMedidor.medidor medidor ");
		hql.append(" inner join pc.situacaoConsumo situacaoConsumo ");
		hql.append(" inner join pc.imovel imovel ");
		hql.append(" WHERE 1=1 ");
		
		if(filtroDTO.getSituacaoContrato() != null && filtroDTO.getSituacaoContrato() > 0) {
			hql.append(" and c.situacao.chavePrimaria = :situacao ");
		}
		
		hql.append(" and cpc.chavePrimaria = ");
		hql.append(" ( select max(cpc2.chavePrimaria) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" cpc2 ");
		hql.append(" WHERE ");
		hql.append(" cpc.pontoConsumo.chavePrimaria = cpc2.pontoConsumo.chavePrimaria )");
		
		if (filtroDTO.isFiltroCityGatePreenchido()) {
			hql.append(" and pc.chavePrimaria in (select pcg.pontoConsumo.chavePrimaria from " ); 
			hql.append(getClasseEntidadePontoConsumoCityGate().getSimpleName());
			hql.append(" pcg where pcg.cityGate.chavePrimaria = :idCityGate) ");
		}
			
		
		if (filtroDTO.isFiltroCepComunicacaoPreenchido())
			hql.append(" and pc.cep.cep = :numeroCep ");
		
		if (filtroDTO.isFiltroQuadraPreenchido())
			hql.append(" and quadra.chavePrimaria = :idQuadra ");
		
		if (filtroDTO.isFiltroTroncoPreenchido()) {
			hql.append(" and EXISTS (select tronco from ");
			hql.append(getClasseEntidadeTronco().getSimpleName());
			hql.append(" tronco inner join tronco.cityGate cityGate where tronco.chavePrimaria = :idTronco and cityGate.chavePrimaria = :idCityGate) ");
		}
		
		if (filtroDTO.isFiltroSegmentoPreenchido())
			hql.append(" and pc.segmento.chavePrimaria in (:chavesSegmentos) ");
		
		if (filtroDTO.isFiltroNumeroMedidorPreenchido())
			hql.append(" and medidor.numeroSerie = :numMedidor ");
		
		if (filtroDTO.isFiltroDataAquisicaoPreenchido()) {
			hql.append(" and medidor.dataAquisicao >= :dataAquisicaoInicial ");
			hql.append(" and medidor.dataAquisicao <= :dataAquisicaoFinal ");
		}
		
		if (filtroDTO.isFiltroDataInstalacaoPreenchido()) {
			hql.append(" and instalacaoMedidor.data >= :dataInstalacaoInicial ");
			hql.append(" and instalacaoMedidor.data <= :dataInstalacaoFinal ");
		}
		
		if(filtroDTO.isFiltroDataAquisicaoPreenchido() || filtroDTO.isFiltroDataInstalacaoPreenchido()) {
			hql.append(" and medidor.chavePrimaria not in ( ");
			hql.append(" select medidor2.chavePrimaria ");
			hql.append(" FROM ");
			hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
			hql.append(" historicoOperacaoMedidor ");
			hql.append(" INNER JOIN historicoOperacaoMedidor.medidor medidor2");
			hql.append(" WHERE ");
			hql.append(" historicoOperacaoMedidor.operacaoMedidor.chavePrimaria in (2,5))");
		}
		
		
		if (filtroDTO.isFiltroSituacaoMedidorPreenchido()) {
			hql.append(" and medidor.situacaoMedidor.chavePrimaria = :situacaoMedidor ");
		}
		
		if (filtroDTO.isFiltroSituacaoConsumo()) {
			hql.append(" and situacaoConsumo.chavePrimaria = :situacaoConsumo ");
		}
		
		if(filtroDTO.isFiltroTipoMedicao()) {
			hql.append(" and imovel.modalidadeMedicaoImovel = :tipoMedicao ");
			
			if(!filtroDTO.getTipoMedicao()) {
				hql.append(" and imovel.condominio = true ");
			}
		}
		
		if(filtroDTO.isFiltroRamoAtividadePreenchido()) {
			hql.append(" and pc.ramoAtividade.chavePrimaria = :idRamoAtividade ");
		}
		
		if(filtroDTO.isFiltroModeloMedidorPreenchido()) {
			hql.append(" and medidor.modelo.chavePrimaria = :idModeloMedidor ");

		}
		
		if(filtroDTO.isFiltroCondomoniosPreenchido()) {
			hql.append(" and imovel.imovelCondominio.chavePrimaria in (:condominios) ");
		}
		
		hql.append(" and pc.chavePrimaria not in ( ");
		hql.append(" select ilc.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" ilc ");
		hql.append(" inner join ilc.loteComunicacao ");
		hql.append(" lcm ");
		hql.append(" left join ilc.servicoAutorizacao ");
		hql.append(" seau ");
		hql.append(" where 1=1 ");
		hql.append(" and (ilc.habilitado = true ");
		hql.append(" and lcm.servicoTipo.chavePrimaria = :servicoTipo ");
		hql.append(" and (seau is null ");
		hql.append(" or seau.dataEncerramento is null))");
		hql.append(" or EXISTS(SELECT sa from ");
		hql.append(getClasseEntidadeServicoAutorizacao().getSimpleName());
		hql.append(" sa WHERE ");
		hql.append(" sa.pontoConsumo.chavePrimaria =  pc.chavePrimaria ");
		hql.append(" and sa.dataEncerramento is null  ");
		hql.append(" and sa.servicoTipo.chavePrimaria in (  ");
		hql.append(" SELECT st.chavePrimaria from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st  ");
		hql.append(" WHERE ");
		hql.append(" st.indicadorOperacaoMedidor in ( ");
		hql.append(" SELECT st1.indicadorOperacaoMedidor from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st1  ");
		hql.append(" WHERE ");
		hql.append(" st1.chavePrimaria = :servicoTipo ");
		hql.append(") ) ) )");
		
		hql.append(" ORDER BY pc.chavePrimaria ");
		
		Query query = getQuery(hql.toString());
		
		if (filtroDTO.isFiltroCityGatePreenchido() )
			query.setParameter("idCityGate", filtroDTO.getCityGate().getChavePrimaria());
		
		if (filtroDTO.isFiltroCepComunicacaoPreenchido())
			query.setParameter("numeroCep", filtroDTO.getCepComunicacao());
		
		if (filtroDTO.isFiltroQuadraPreenchido())
			query.setParameter("idQuadra", filtroDTO.getQuadra());
		
		if (filtroDTO.isFiltroTroncoPreenchido())
			query.setParameter("idTronco", filtroDTO.getTronco().getChavePrimaria());
		
		if (filtroDTO.isFiltroSegmentoPreenchido())
			query.setParameterList("chavesSegmentos", filtroDTO.getSegmentosSelecionados());
		
		if (filtroDTO.isFiltroNumeroMedidorPreenchido())
			query.setParameter("numMedidor", filtroDTO.getNumeroMedidor());
		
		if (filtroDTO.isFiltroDataAquisicaoPreenchido()) {
			query.setParameter("dataAquisicaoInicial", Util.converterCampoStringParaData("Data Aquisição Início", filtroDTO.getDataAquisicaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataAquisicaoFinal", Util.converterCampoStringParaData("Data Aquisição Final", filtroDTO.getDataAquisicaoFinal(), Constantes.FORMATO_DATA_BR));
		}
		
		if (filtroDTO.isFiltroDataInstalacaoPreenchido()) {
			query.setParameter("dataInstalacaoInicial", Util.converterCampoStringParaData("Data Instalação Início", filtroDTO.getDataInstalacaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataInstalacaoFinal", Util.converterCampoStringParaData("Data Instalação Final", filtroDTO.getDataInstalacaoFinal(), Constantes.FORMATO_DATA_BR));
		}
		
		if (filtroDTO.getServicoTipo() != null) {
			query.setLong("servicoTipo", filtroDTO.getServicoTipo().getChavePrimaria());
		}
		
		if (filtroDTO.isFiltroSituacaoMedidorPreenchido()) {
			query.setLong("situacaoMedidor", filtroDTO.getSituacaoMedidor().getChavePrimaria());
		}
		
		if (filtroDTO.isFiltroSituacaoConsumo()) {
			query.setLong("situacaoConsumo", filtroDTO.getSituacaoConsumo());
		}
		
		if (filtroDTO.isFiltroTipoMedicao()) {
			query.setLong("tipoMedicao", filtroDTO.getTipoMedicao() ? 1l : 2l);
		}
		
		if(filtroDTO.getSituacaoContrato() != null && filtroDTO.getSituacaoContrato() > 0) {
			query.setLong("situacao", filtroDTO.getSituacaoContrato());
		}
		
		if(filtroDTO.isFiltroRamoAtividadePreenchido()) {
			query.setLong("idRamoAtividade", filtroDTO.getRamoAtividade().getChavePrimaria());

		}
		
		if(filtroDTO.isFiltroModeloMedidorPreenchido()) {
			query.setLong("idModeloMedidor", filtroDTO.getModeloMedidor().getChavePrimaria());

		}
		
		if(filtroDTO.isFiltroCondomoniosPreenchido()) {
			query.setParameterList("condominios", filtroDTO.getCondominios());
		}
		
		return (Collection<PontoConsumo>) query.list().stream().distinct().collect(Collectors.toList());
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<PontoConsumo> filtrarPontosParaLoteDeComunicacao(String descricaoPonto, String codigoLegado, String servicoTipo, Integer offset) {
		
		StringBuilder hql = new StringBuilder("SELECT pc ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pc ");
		hql.append(" inner join pc.quadraFace.quadra quadra ");
		hql.append(" inner join pc.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join pc.instalacaoMedidor.medidor medidor ");
		hql.append(" WHERE 1=1 ");
		
		hql.append(" and pc.chavePrimaria not in ( ");
		hql.append(" select ilc.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" ilc ");
		hql.append(" inner join ilc.loteComunicacao ");
		hql.append(" lcm ");
		hql.append(" left join ilc.servicoAutorizacao ");
		hql.append(" seau ");
		hql.append(" where 1=1 ");
		hql.append(" and (ilc.habilitado = true ");
		hql.append(" and lcm.servicoTipo.chavePrimaria = :servicoTipo ");
		hql.append(" and (seau is null ");
		hql.append(" or seau.dataEncerramento is null))");
		hql.append(" or EXISTS(SELECT sa from ");
		hql.append(getClasseEntidadeServicoAutorizacao().getSimpleName());
		hql.append(" sa WHERE ");
		hql.append(" sa.pontoConsumo.chavePrimaria =  pc.chavePrimaria ");
		hql.append(" and sa.dataEncerramento is null  ");
		hql.append(" and sa.servicoTipo.chavePrimaria in (  ");
		hql.append(" SELECT st.chavePrimaria from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st  ");
		hql.append(" WHERE ");
		hql.append(" st.indicadorOperacaoMedidor in ( ");
		hql.append(" SELECT st1.indicadorOperacaoMedidor from  ");
		hql.append(getClasseEntidadeServicoTipo().getSimpleName());
		hql.append(" st1  ");
		hql.append(" WHERE ");
		hql.append(" st1.chavePrimaria = :servicoTipo ");
		hql.append(") ) ) )");
		
		if(descricaoPonto != null && !descricaoPonto.isEmpty()) {
			hql.append(" and upper(pc.descricao) like :descricaoPontoConsumo " );
		}
		
		if(codigoLegado != null && !codigoLegado.isEmpty()) {
			hql.append(" and pc.codigoLegado like :codigoLegado " );
		}
		
		hql.append(" ORDER BY pc.descricao ");
		
		Query query = getQuery(hql.toString());
		
		if(descricaoPonto != null && !descricaoPonto.isEmpty()) {
			query.setString("descricaoPontoConsumo",
					Util.removerAcentuacao(Util.formatarTextoConsulta(descricaoPonto).toUpperCase()));
		}
		
		if(codigoLegado != null && !codigoLegado.isEmpty()) {
			query.setString("codigoLegado", "%" + codigoLegado + "%");
		}
		
		query.setLong("servicoTipo", Long.valueOf(servicoTipo));
		
		query.setMaxResults(50);
		
		if(offset != 0) {
			offset = (50 * offset) + 1;
		}
		
		query.setFirstResult(offset);

		
		return (Collection<PontoConsumo>) query.list().stream().distinct().collect(Collectors.toList());
	}
	
	/**
	 * Metodo que faz o filtro dos lotes de comunicacao sem AS gerada 
	 * chavePrimaria e a descricao.
	 *
	 * @return Retorna a lista de ChamadoTipo com os atributos chavePrimaria e a descricao, preenchido.
	 * @throws GGASException 
	 * @throws HibernateException 
	 */
	@SuppressWarnings("unchecked")
	public Collection<LoteComunicacao> consultarLoteComunicacaoNaoGerado() throws NegocioException, HibernateException, GGASException {

		
		StringBuilder hql = new StringBuilder("SELECT item.loteComunicacao ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" item ");
		hql.append(" WHERE item.servicoAutorizacao is null ");
		hql.append(" AND item.habilitado = true ");
		hql.append(" AND item.loteComunicacao.servicoTipo.chavePrimaria is not null ");
		hql.append(" AND item.loteComunicacao.gerarAutorizacaoServico = true ");

		hql.append(" ORDER BY item.loteComunicacao.dataGeracao ");
		
		Query query = getQuery(hql.toString());
		
		return (Collection<LoteComunicacao>) query.list().stream().distinct().collect(Collectors.toList());
	
	}
	
	/**
	 * Metodo que faz o verifica se o servido tipo agendamento está sendo utilizado
	 *
	 * @return Retorna a quantidade de servico tipo agendamento no item lote
	 * @throws GGASException 
	 * @throws HibernateException 
	 */
	public Long verificaServicoTipoAgendamento(Long chaveServicoTipoAgendamento) throws HibernateException {

		
		StringBuilder hql = new StringBuilder("SELECT count(*) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" item ");
		hql.append(" WHERE item.servicoTipoAgendamentoTurno.chavePrimaria = :chaveServicoTipoAgendamento");
		
		Query query = getQuery(hql.toString());
		query.setLong("chaveServicoTipoAgendamento", chaveServicoTipoAgendamento);
		
		return (Long) query.uniqueResult();
	
	}


	@SuppressWarnings("unchecked")
	public Collection<LoteComunicacao> filtrarLoteComunicacao(FiltroAcompanhamentoLoteDTO filtroDTO) throws HibernateException, GGASException {
		StringBuilder hql = new StringBuilder("SELECT itemLote.loteComunicacao ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" itemLote ");
		hql.append(" inner join itemLote.pontoConsumo ponto ");
		hql.append(" inner join ponto.rota rota ");
		hql.append(" inner join itemLote.loteComunicacao lote ");
		hql.append(" left join itemLote.documentoNaoEntregue documento ");
		hql.append(" left join lote.servicoTipo servicoTipo ");
		hql.append(" WHERE 1=1 ");
		
		if(filtroDTO.getLotesCartasCanceladas() != null && filtroDTO.getLotesCartasCanceladas()) {
			hql.append(" and servicoTipo.chavePrimaria is null " ); 
		} else if (filtroDTO.getServicoTipo() != null) {
			hql.append(" and servicoTipo.chavePrimaria = :servicoTipo " ); 
		} else if (filtroDTO.getLotesCartasCanceladas() != null) {
			hql.append(" and servicoTipo.chavePrimaria is not null " ); 
		}
		
		if(filtroDTO.getDescricaoPontoConsumo() != null && !filtroDTO.getDescricaoPontoConsumo().isEmpty()) {
			hql.append(" and upper(ponto.descricao) like :descricaoPontoConsumo " );
		}
		
		if(filtroDTO.isFiltroDataGeracaoLotePreenchido()) {
			hql.append(" and lote.dataGeracao >= :dataGeracaoInicial ");
			hql.append(" and lote.dataGeracao <= :dataGeracaoFinal ");
		}
		
		if(filtroDTO.getProtocoloRetornado() != null) {
			hql.append(" and documento.indicadorRetornado ");

			if(filtroDTO.getProtocoloRetornado()) {
				hql.append(" = true ");
			} else {
				hql.append(" = false ");
			}
			
		}
		
		if(filtroDTO.getAutorizacaoGerada() != null) {
			hql.append(" and itemLote.servicoAutorizacao ");

			if(filtroDTO.getAutorizacaoGerada()) {
				hql.append(" is not null ");
			} else {
				hql.append(" is null ");
			}
		}
		
		if(filtroDTO.getIdGrupoFaturamento() != null && filtroDTO.getIdGrupoFaturamento() > 0) {
			hql.append(" and rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
		}
		
		if(filtroDTO.getLoteCancelado() != null) {
			hql.append(" and lote.habilitado = :loteCancelado ");
		}
		
		if(filtroDTO.getItemLoteCancelado() != null) {
			hql.append(" and itemLote.habilitado = :itemLoteCancelado ");
		}
			
		hql.append(" ORDER BY lote.dataGeracao ");
		
		Query query = getQuery(hql.toString());
		
		if (filtroDTO.getServicoTipo() != null) {
			query.setLong("servicoTipo", filtroDTO.getServicoTipo().getChavePrimaria());
		}
		
		if(filtroDTO.getDescricaoPontoConsumo() != null && !filtroDTO.getDescricaoPontoConsumo().isEmpty()) {
			query.setString("descricaoPontoConsumo", Util.removerAcentuacao(Util.formatarTextoConsulta(filtroDTO.getDescricaoPontoConsumo()).toUpperCase()));
		}
		
		if(filtroDTO.isFiltroDataGeracaoLotePreenchido()) {
			query.setParameter("dataGeracaoInicial", Util.converterCampoStringParaData("Data Geração Início", filtroDTO.getDataGeracaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataGeracaoFinal", Util.adicionarDiasData(Util.converterCampoStringParaData("Data Geração Final", filtroDTO.getDataGeracaoFinal(), Constantes.FORMATO_DATA_BR), 1));
		}
		
		if(filtroDTO.getIdGrupoFaturamento() != null && filtroDTO.getIdGrupoFaturamento() > 0) {
			query.setLong("idGrupoFaturamento", filtroDTO.getIdGrupoFaturamento());
		}
		
		if(filtroDTO.getLoteCancelado() != null) {
			query.setParameter("loteCancelado", filtroDTO.getLoteCancelado());
		}
		
		if(filtroDTO.getItemLoteCancelado() != null) {
			query.setParameter("itemLoteCancelado", filtroDTO.getItemLoteCancelado());
		}		
		
		return (Collection<LoteComunicacao>) query.list().stream().distinct().collect(Collectors.toList()); 
	}
	
	
	public ItemLoteComunicacao obterUltimoItemLoteComunicacao(Integer anoExecucao) {
		StringBuilder hql = new StringBuilder("SELECT itemLote ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" itemLote ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND EXTRACT(YEAR FROM itemLote.dataHoraExecucao) = :anoExecucao ");
		hql.append(" ORDER BY itemLote.chavePrimaria desc ");
		
		
		Query query = getQuery(hql.toString());
		
		query.setParameter("anoExecucao", anoExecucao);
		query.setMaxResults(1);
		
		return (ItemLoteComunicacao) query.uniqueResult();
		
	}
	
	public Collection<ItemLoteComunicacao> obterItensLoteComunicacaoRelatorioSintetico(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) throws HibernateException, GGASException {
		StringBuilder hql = new StringBuilder("SELECT itemLote ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" itemLote ");
		hql.append(" inner join itemLote.loteComunicacao lote ");
		hql.append(" left join itemLote.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join lote.servicoTipo servicoTipo ");
		hql.append(" WHERE 1=1 ");
		
		if(filtro.getServicoTipo() != null) {
			hql.append(" and servicoTipo.chavePrimaria = :chaveServicoTipo ");
		}
		
		if (filtro.isFiltroDataGeracaoPreenchido()) {
			hql.append(" and lote.dataGeracao >= :dataGeracaoInicial ");
			hql.append(" and lote.dataGeracao <= :dataGeracaoFinal ");
		}
		
		if (filtro.isFiltroDataPrevistaPreenchido()) {
			hql.append(" and itemLote.dataHoraExecucao >= :dataPrevistaInicial ");
			hql.append(" and itemLote.dataHoraExecucao <= :dataPrevistaFinal ");
		}
		
		if (filtro.isFiltroDataExecucaoPreenchido()) {
			hql.append(" and ( itemLote.servicoAutorizacao != null and ");
			hql.append(" ( servicoAutorizacao.dataExecucao >= :dataExecucaoInicial ");
			hql.append(" and servicoAutorizacao.dataExecucao <= :dataExecucaoFinal ) )");
		}
		
		
		hql.append(" ORDER BY itemLote.dataHoraExecucao asc ");
		
		
		Query query = getQuery(hql.toString());
		
		if(filtro.getServicoTipo() != null) {
			query.setParameter("chaveServicoTipo", filtro.getServicoTipo().getChavePrimaria());
		}
		
		if (filtro.isFiltroDataGeracaoPreenchido()) {
			query.setParameter("dataGeracaoInicial", Util.converterCampoStringParaData("Data Geração Início", filtro.getDataGeracaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataGeracaoFinal", Util.adicionarDiasData(Util.converterCampoStringParaData("Data Geração Final", filtro.getDataGeracaoFinal(), Constantes.FORMATO_DATA_BR), 1));
		}
		
		if (filtro.isFiltroDataPrevistaPreenchido()) {
			query.setParameter("dataPrevistaInicial", Util.converterCampoStringParaData("Data Prevista Início", filtro.getDataPrevisaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataPrevistaFinal", Util.adicionarDiasData(Util.converterCampoStringParaData("Data Prevista Final", filtro.getDataPrevisaoFinal(), Constantes.FORMATO_DATA_BR), 1));
		}
		
		if (filtro.isFiltroDataExecucaoPreenchido()) {
			query.setParameter("dataExecucaoInicial", Util.converterCampoStringParaData("Data Execução Início", filtro.getDataExecucaoInicial(), Constantes.FORMATO_DATA_BR));
			query.setParameter("dataExecucaoFinal", Util.adicionarDiasData(Util.converterCampoStringParaData("Data Execução Final", filtro.getDataExecucaoFinal(), Constantes.FORMATO_DATA_BR), 1));
		}	
		
		
		return query.list();
		
		
	}
	
	public Collection<ItemLoteComunicacao> consultarAntecedenciaAvisosPorAno(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) {
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT itemLote ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" itemLote ");
		hql.append(" inner join itemLote.loteComunicacao lote ");
		hql.append(" left join itemLote.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join itemLote.documentoNaoEntregue documento ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND documento.indicadorRetornado = true ");
		
		if(filtro.getMesAnoCorrente() != null && !filtro.getMesAnoCorrente().isEmpty()) {
			hql.append(" AND lote.dataGeracao between :dataInicio and :dataFim ");
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		if(filtro.getMesAnoCorrente() != null && !filtro.getMesAnoCorrente().isEmpty()) {
			Date dataInicio = DataUtil.obterPrimeiroDiaAno(filtro.getMesAnoCorrente());
			Date dataFim = DataUtil.obterPrimeiroDiaProximoMes(filtro.getMesAnoCorrente()); //.obterPrimeiroDiaProximosDoisMeses(filtro.getMesAnoCorrente());
			
			query.setDate("dataInicio", dataInicio);
			query.setDate("dataFim", dataFim);
			
		}
		
		return query.list();
		
	}
	
	
	public Collection<ItemLoteComunicacao> consultarItensPendentesAntecedenciaAvisosPorAno(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) {
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT itemLote ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" itemLote ");
		hql.append(" inner join itemLote.loteComunicacao lote ");
		hql.append(" left join itemLote.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join itemLote.documentoNaoEntregue documento ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND documento.indicadorRetornado = true ");
		hql.append(" AND itemLote.servicoAutorizacao is null ");
		
		if(filtro.getMesAnoCorrente() != null && !filtro.getMesAnoCorrente().isEmpty()) {
			hql.append(" AND lote.dataGeracao between :dataInicio and :dataFim ");
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		if(filtro.getMesAnoCorrente() != null && !filtro.getMesAnoCorrente().isEmpty()) {
			Date dataInicio = DataUtil.obterPrimeiroDiaMes(filtro.getMesAnoCorrente());
			Date dataFim = DataUtil.obterPrimeiroDiaProximoMes(filtro.getMesAnoCorrente());
			
			query.setDate("dataInicio", dataInicio);
			query.setDate("dataFim", dataFim);
			
		}
		
		return query.list();
		
	}
	
	public Collection<ItemLoteComunicacao> consultarItensPendentesAntecedenciaAvisosPorAnoDataEncerramentoNaoNula(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) {
		StringBuilder hql = new StringBuilder();

		hql = new StringBuilder("SELECT itemLote ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeItemLoteComunicacao().getSimpleName());
		hql.append(" itemLote ");
		hql.append(" inner join itemLote.loteComunicacao lote ");
		hql.append(" left join itemLote.servicoAutorizacao servicoAutorizacao ");
		hql.append(" inner join itemLote.documentoNaoEntregue documento ");
		hql.append(" WHERE 1=1 ");
		hql.append(" AND documento.indicadorRetornado = true ");
		hql.append(" AND itemLote.servicoAutorizacao is not null ");
		hql.append(" AND servicoAutorizacao.dataEncerramento is not null ");

		
		if(filtro.getMesAnoCorrente() != null && !filtro.getMesAnoCorrente().isEmpty()) {
			hql.append(" AND lote.dataGeracao between :dataInicio and :dataFim ");
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		if(filtro.getMesAnoCorrente() != null && !filtro.getMesAnoCorrente().isEmpty()) {
			Date dataInicio = DataUtil.obterPrimeiroDiaMes(filtro.getMesAnoCorrente());
			Date dataFim = DataUtil.obterPrimeiroDiaProximoMes(filtro.getMesAnoCorrente());
			
			query.setDate("dataInicio", dataInicio);
			query.setDate("dataFim", dataFim);
			
		}
	   
		return query.list();
		
	}
	
	public Long consultarEntregaDocumento(Long chavePrimaria) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT loteComunicacao.chavePrimaria ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeEntregaDocumento().getSimpleName());
		hql.append(" documentoNaoEntregue ");
		hql.append(" INNER JOIN documentoNaoEntregue.itemLoteComunicacao itemLoteComunicacao ");
		hql.append(" INNER JOIN itemLoteComunicacao.loteComunicacao loteComunicacao ");
		hql.append(" WHERE loteComunicacao.chavePrimaria = :chavePrimaria ");
		hql.append(" AND documentoNaoEntregue.indicadorRetornado = false ");
		hql.append(" GROUP BY loteComunicacao.chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (Long) query.setMaxResults(1).uniqueResult();
	}

	public Collection<LoteComunicacao> consultarLoteComunicacaoPorChavesPrimarias(Long[] chavesPrimarias) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT loteComunicacao ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeLoteComunicacao().getSimpleName());
		hql.append(" loteComunicacao ");
		hql.append(" WHERE ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "loteComunicacao.chavePrimaria", "LC_CONS", Arrays.asList(chavesPrimarias));
		hql.append( " ORDER BY loteComunicacao.chavePrimaria ASC ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		
		return query.list();
	}
}
