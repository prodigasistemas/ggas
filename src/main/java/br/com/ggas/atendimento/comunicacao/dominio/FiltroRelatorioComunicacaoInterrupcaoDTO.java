package br.com.ggas.atendimento.comunicacao.dominio;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;

public class FiltroRelatorioComunicacaoInterrupcaoDTO {
	private ServicoTipo servicoTipo;
	private String dataGeracaoInicial;
	private String dataGeracaoFinal;
	private String dataPrevisaoInicial;
	private String dataPrevisaoFinal;
	private String dataExecucaoInicial;
	private String dataExecucaoFinal;
	private Boolean formatoImpressao;
	private String mesAnoCorrente;
	
	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}
	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}
	public String getDataGeracaoInicial() {
		return dataGeracaoInicial;
	}
	public void setDataGeracaoInicial(String dataGeracaoInicial) {
		this.dataGeracaoInicial = dataGeracaoInicial;
	}
	public String getDataGeracaoFinal() {
		return dataGeracaoFinal;
	}
	public void setDataGeracaoFinal(String dataGeracaoFinal) {
		this.dataGeracaoFinal = dataGeracaoFinal;
	}
	public String getDataPrevisaoInicial() {
		return dataPrevisaoInicial;
	}
	public void setDataPrevisaoInicial(String dataPrevisaoInicial) {
		this.dataPrevisaoInicial = dataPrevisaoInicial;
	}
	public String getDataPrevisaoFinal() {
		return dataPrevisaoFinal;
	}
	public void setDataPrevisaoFinal(String dataPrevisaoFinal) {
		this.dataPrevisaoFinal = dataPrevisaoFinal;
	}
	public String getDataExecucaoInicial() {
		return dataExecucaoInicial;
	}
	public void setDataExecucaoInicial(String dataExecucaoInicial) {
		this.dataExecucaoInicial = dataExecucaoInicial;
	}
	public String getDataExecucaoFinal() {
		return dataExecucaoFinal;
	}
	public void setDataExecucaoFinal(String dataExecucaoFinal) {
		this.dataExecucaoFinal = dataExecucaoFinal;
	}
	public Boolean getFormatoImpressao() {
		return formatoImpressao;
	}
	public void setFormatoImpressao(Boolean formatoImpressao) {
		this.formatoImpressao = formatoImpressao;
	}
	
	public boolean isFiltroDataGeracaoPreenchido() {
		return ((this.dataGeracaoInicial != null && !this.dataGeracaoInicial.equals("")) && (this.dataGeracaoFinal != null && !this.dataGeracaoFinal.equals("")));
	}
	
	public boolean isFiltroDataPrevistaPreenchido() {
		return ((this.dataPrevisaoInicial != null && !this.dataPrevisaoInicial.equals("")) && (this.dataPrevisaoFinal != null && !this.dataPrevisaoFinal.equals("")));
	}
	
	public boolean isFiltroDataExecucaoPreenchido() {
		return ((this.dataExecucaoInicial != null && !this.dataExecucaoInicial.equals("")) && (this.dataExecucaoFinal != null && !this.dataExecucaoFinal.equals("")));
	}
	public String getMesAnoCorrente() {
		return mesAnoCorrente;
	}
	public void setMesAnoCorrente(String mesAnoCorrente) {
		this.mesAnoCorrente = mesAnoCorrente;
	}
	
}
