package br.com.ggas.atendimento.comunicacao.dominio;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;

public class FiltroAcompanhamentoLoteDTO {
	
	private ServicoTipo servicoTipo;
	private String dataGeracaoInicial;
	private String dataGeracaoFinal;
	private String descricaoPontoConsumo;
	private Boolean protocoloRetornado;
	private Boolean autorizacaoGerada;
	private Long idGrupoFaturamento;
	private Boolean loteCancelado;
	private Boolean itemLoteCancelado;
	private Boolean isGerarLoteCancelamento;
	private Boolean lotesCartasCanceladas;
	
	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}
	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}
	public String getDataGeracaoInicial() {
		return dataGeracaoInicial;
	}
	public void setDataGeracaoInicial(String dataGeracaoInicial) {
		this.dataGeracaoInicial = dataGeracaoInicial;
	}
	public String getDataGeracaoFinal() {
		return dataGeracaoFinal;
	}
	public void setDataGeracaoFinal(String dataGeracaoFinal) {
		this.dataGeracaoFinal = dataGeracaoFinal;
	}
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public Boolean getProtocoloRetornado() {
		return protocoloRetornado;
	}
	public void setProtocoloRetornado(Boolean protocoloRetornado) {
		this.protocoloRetornado = protocoloRetornado;
	}
	public Boolean getAutorizacaoGerada() {
		return autorizacaoGerada;
	}
	public void setAutorizacaoGerada(Boolean autorizacaoGerada) {
		this.autorizacaoGerada = autorizacaoGerada;
	}
	
	public boolean isFiltroDataGeracaoLotePreenchido() {
		return ((this.dataGeracaoInicial != null && !this.dataGeracaoInicial.equals("")) && (this.dataGeracaoFinal != null && !this.dataGeracaoFinal.equals("")));
	}
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	public Boolean getLoteCancelado() {
		return loteCancelado;
	}
	public void setLoteCancelado(Boolean loteCancelado) {
		this.loteCancelado = loteCancelado;
	}
	public Boolean getItemLoteCancelado() {
		return itemLoteCancelado;
	}
	public void setItemLoteCancelado(Boolean itemLoteCancelado) {
		this.itemLoteCancelado = itemLoteCancelado;
	}
	public Boolean getIsGerarLoteCancelamento() {
		return isGerarLoteCancelamento;
	}
	public void setIsGerarLoteCancelamento(Boolean isGerarLoteCancelamento) {
		this.isGerarLoteCancelamento = isGerarLoteCancelamento;
	}
	public Boolean getLotesCartasCanceladas() {
		return lotesCartasCanceladas;
	}
	public void setLotesCartasCanceladas(Boolean lotesCartasCanceladas) {
		this.lotesCartasCanceladas = lotesCartasCanceladas;
	}
	
}
