package br.com.ggas.atendimento.comunicacao.dominio;

public class RelatorioPendenciasAntecedenciaAvisoVO {
	private String descricaoPontoConsumo;
	private String dataRetornoProtocolo;
	private String dataPrevistaExecucaoServico;
	
	public RelatorioPendenciasAntecedenciaAvisoVO() {
		
	}
	
	public RelatorioPendenciasAntecedenciaAvisoVO(String descricaoPontoConsumo,String dataRetornoProtocolo,String dataPrevistaExecucaoServico) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
		this.dataRetornoProtocolo = dataRetornoProtocolo;
		this.dataPrevistaExecucaoServico = dataPrevistaExecucaoServico;
	}
	
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public String getDataRetornoProtocolo() {
		return dataRetornoProtocolo;
	}
	public void setDataRetornoProtocolo(String dataRetornoProtocolo) {
		this.dataRetornoProtocolo = dataRetornoProtocolo;
	}
	public String getDataPrevistaExecucaoServico() {
		return dataPrevistaExecucaoServico;
	}
	public void setDataPrevistaExecucaoServico(String dataPrevistaExecucaoServico) {
		this.dataPrevistaExecucaoServico = dataPrevistaExecucaoServico;
	}
	
	
}
