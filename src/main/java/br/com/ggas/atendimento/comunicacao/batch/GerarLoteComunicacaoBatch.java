package br.com.ggas.atendimento.comunicacao.batch;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import br.com.ggas.atendimento.comunicacao.negocio.ControladorComunicacao;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

@Component
public class GerarLoteComunicacaoBatch implements Batch{

	private static final String PROCESSO = "processo";

	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);

		try {
			
			Map<String, Object> dados = new HashMap<>();
			dados.put("usuario", processo.getUsuario());
			dados.put("processo", processo);

			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("[");
			logProcessamento.append(Util.converterDataHoraParaString(Calendar.getInstance().getTime()));
			logProcessamento.append("] ");
			logProcessamento.append("Iniciando geração de Comunicado de Serviço em lote.");

			ControladorComunicacao controladorComunicacao = (ControladorComunicacao) ServiceLocator.getInstancia().getBeanPorID(
					ControladorComunicacao.BEAN_ID_CONTROLADOR_COMUNICACAO);

			controladorComunicacao.processarEmissaoComunicacaoEmLoteBatch(logProcessamento, dados);

		} catch (IOException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		}
		return logProcessamento.toString();
	}
}
