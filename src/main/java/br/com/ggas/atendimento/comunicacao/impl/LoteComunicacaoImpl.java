package br.com.ggas.atendimento.comunicacao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class LoteComunicacaoImpl extends EntidadeNegocioImpl implements LoteComunicacao {
	private static final long serialVersionUID = 1L;

	Date dataGeracao;
	ServicoTipo servicoTipo;
	Collection<ItemLoteComunicacao> itens;
	Boolean gerarAutorizacaoServico;

	@Override
	public Date getDataGeracao() {
		return dataGeracao;
	}

	@Override
	public void setDataGeracao(Date dataGeracao) {
		this.dataGeracao = dataGeracao; 
		
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}

	@Override
	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
		
	}

	@Override
	public Collection<ItemLoteComunicacao> getItens() {
		return itens;
	}
	@Override
	public void setItens(Collection<ItemLoteComunicacao> itens) {
		this.itens = itens;
	}
	
	public boolean isTodosProtocolosRetornados() {
		for (ItemLoteComunicacao item : itens) {
			if (item.getDocumentoNaoEntregue().isIndicadorRetornado()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Collection<ItemLoteComunicacao> getItensProtocoloRetornado() {
		Collection<ItemLoteComunicacao> listaProtocolos = new ArrayList<>();
		
		for (ItemLoteComunicacao item : itens) {
			if (item.getDocumentoNaoEntregue().isIndicadorRetornado()) {
				listaProtocolos.add(item);
			}
		}
		return listaProtocolos;
	}
	
	@Override
	public Boolean getIsTodosProtocolosRetornados() {
		return this.itens.stream().allMatch(p -> p.getDocumentoNaoEntregue() != null && 
				p.getDocumentoNaoEntregue().isIndicadorRetornado());
	}
	
	@Override
	public Boolean getisExisteASParaTodosItens() {
		return this.itens.stream().allMatch(p -> p.getServicoAutorizacao() != null);
	}
	
	@Override
	public Boolean getisExistePeloMenosUmaASParaItens() {
		return this.itens.stream().anyMatch(p -> p.getServicoAutorizacao() != null);
	}
	
	@Override
	public Boolean getIsPeloMenosUmProtocoloRetornado() {
		return this.itens.stream().anyMatch(p -> p.getDocumentoNaoEntregue() != null && 
				p.getDocumentoNaoEntregue().isIndicadorRetornado());
	}
	
	@Override
	public Boolean getGerarAutorizacaoServico() {
		return this.gerarAutorizacaoServico;
	}
	
	@Override
	public void setGerarAutorizacaoServico(Boolean gerarAutorizacaoServico) {
        this.gerarAutorizacaoServico = gerarAutorizacaoServico;
    }
	
	@Override
	public Boolean getIsPeloMenosUmaAutorizacaoProtocolo() {
		return this.itens.stream().anyMatch(p -> p.getServicoAutorizacaoProtocolo() != null);
	}
	
	@Override
	public Boolean getIsExisteTodasUmaAutorizacaoProtocolo() {
		return this.itens.stream().allMatch(p -> p.getServicoAutorizacaoProtocolo() != null);
	}
	
	
}
