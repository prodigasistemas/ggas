package br.com.ggas.atendimento.comunicacao.dominio;

public class RelatorioSinteticoComunicacaoInterrupcaoVO {
	private String descricaoPontoConsumo;
	private String tipoServico;
	private String dataProtocolo;
	private String dataPrevistaExecucao;
	private String dataExecucao;
	private String dataRetornoProtocolo;
	
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public String getTipoServico() {
		return tipoServico;
	}
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	public String getDataProtocolo() {
		return dataProtocolo;
	}
	public void setDataProtocolo(String dataProtocolo) {
		this.dataProtocolo = dataProtocolo;
	}
	public String getDataPrevistaExecucao() {
		return dataPrevistaExecucao;
	}
	public void setDataPrevistaExecucao(String dataPrevistaExecucao) {
		this.dataPrevistaExecucao = dataPrevistaExecucao;
	}
	public String getDataExecucao() {
		return dataExecucao;
	}
	public void setDataExecucao(String dataExecucao) {
		this.dataExecucao = dataExecucao;
	}
	public String getDataRetornoProtocolo() {
		return dataRetornoProtocolo;
	}
	public void setDataRetornoProtocolo(String dataRetornoProtocolo) {
		this.dataRetornoProtocolo = dataRetornoProtocolo;
	}
}
