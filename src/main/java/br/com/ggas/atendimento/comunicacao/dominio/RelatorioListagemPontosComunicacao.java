package br.com.ggas.atendimento.comunicacao.dominio;

public class RelatorioListagemPontosComunicacao {
	private String descricaoPontoConsumo;
	private String codigoLegado;
	private String endereco;
	private String bairro;
	private String modelo;
	private String numeroSerie;
	private String pressaoEntrega;
	private String vazaoMaxima;
	private String dataInstalacao;
	private String dataAquisicao;
	private String situacaoConsumo;
	
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}
	public String getCodigoLegado() {
		return codigoLegado;
	}
	public void setCodigoLegado(String codigoLegado) {
		this.codigoLegado = codigoLegado;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getPressaoEntrega() {
		return pressaoEntrega;
	}
	public void setPressaoEntrega(String pressaoEntrega) {
		this.pressaoEntrega = pressaoEntrega;
	}
	public String getVazaoMaxima() {
		return vazaoMaxima;
	}
	public void setVazaoMaxima(String vazaoMaxima) {
		this.vazaoMaxima = vazaoMaxima;
	}
	public String getDataInstalacao() {
		return dataInstalacao;
	}
	public void setDataInstalacao(String dataInstalacao) {
		this.dataInstalacao = dataInstalacao;
	}
	public String getDataAquisicao() {
		return dataAquisicao;
	}
	public void setDataAquisicao(String dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}
	public String getSituacaoConsumo() {
		return situacaoConsumo;
	}
	public void setSituacaoConsumo(String situacaoConsumo) {
		this.situacaoConsumo = situacaoConsumo;
	}
	
}
