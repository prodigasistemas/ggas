package br.com.ggas.atendimento.comunicacao;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface LoteComunicacao extends EntidadeNegocio {
	String BEAN_ID_LOTE_COMUNICACAO = "loteComunicacao";

	/**
	 * @return the dataGeracao
	 */
	public Date getDataGeracao();


	/**
	 * @param dataGeracao
	 *            the dataGeracao to
	 *            set
	 */
	public void setDataGeracao(Date dataGeracao);

	/**
	 * @return the servicoTipo
	 */
	public ServicoTipo getServicoTipo();
	
	/**
	 * @param servicoTipo
	 *            the servicoTipo to
	 *            set
	 */
	public void setServicoTipo(ServicoTipo servicoTipo);

	/**
	 * @return the itens
	 */
	public Collection<ItemLoteComunicacao> getItens();

	/**
	 * @param itens
	 *            the itens to
	 *            set
	 */
	public void setItens(Collection<ItemLoteComunicacao> itens);
	
	/**
	 * @return a informacao se todos os protocolos foram retornados
	 */
	public boolean isTodosProtocolosRetornados();

	/**
	 * @return the itens com protocolo retornado
	 */
	public Collection<ItemLoteComunicacao> getItensProtocoloRetornado();


	Boolean getIsTodosProtocolosRetornados();


	Boolean getisExisteASParaTodosItens();


	Boolean getisExistePeloMenosUmaASParaItens();


	Boolean getIsPeloMenosUmProtocoloRetornado();


	void setGerarAutorizacaoServico(Boolean gerarAutorizacaoServico);


	Boolean getGerarAutorizacaoServico();


	Boolean getIsPeloMenosUmaAutorizacaoProtocolo();


	Boolean getIsExisteTodasUmaAutorizacaoProtocolo();
	

}
