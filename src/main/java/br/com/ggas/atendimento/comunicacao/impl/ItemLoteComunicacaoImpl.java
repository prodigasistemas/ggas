package br.com.ggas.atendimento.comunicacao.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.DataUtil;

public class ItemLoteComunicacaoImpl extends EntidadeNegocioImpl implements ItemLoteComunicacao {

	private static final long serialVersionUID = 1L;
	
	PontoConsumo pontoConsumo;
	LoteComunicacao loteComunicacao;
	String sequencialItem;
	Date dataHoraExecucao;
	ServicoAutorizacao servicoAutorizacao;
	ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno;
	EntregaDocumento documentoNaoEntregue;
	String tempoExecucao;
	Date dataHoraExecucaoAnterior;
	String sequencialItemAnterior;
	ServicoAutorizacao servicoAutorizacaoProtocolo;


	@Override
	public PontoConsumo getPontoConsumo() {
		return this.pontoConsumo;
	}

	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSequencialItem() {
		return sequencialItem;
	}

	@Override
	public void setSequencialItem(String sequencialItem) {
		this.sequencialItem = sequencialItem; 
		
	}

	@Override
	public LoteComunicacao getLoteComunicacao() {
		return loteComunicacao;
	}
	
	@Override
	public void setLoteComunicacao(LoteComunicacao loteComunicacao) {
		this.loteComunicacao = loteComunicacao;
	}

	@Override
	public Date getDataHoraExecucao() {
		return dataHoraExecucao;
	}

	@Override
	public void setDataHoraExecucao(Date dataHoraExecucao) {
		this.dataHoraExecucao = dataHoraExecucao;
		
	}

	@Override
	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}

	@Override
	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
		
	}
	
	@Override
	public ServicoTipoAgendamentoTurno getServicoTipoAgendamentoTurno() {
		return servicoTipoAgendamentoTurno;
	}

	@Override
	public void setServicoTipoAgendamentoTurno(ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno) {
		this.servicoTipoAgendamentoTurno = servicoTipoAgendamentoTurno;
	}
	
	@Override
	public EntregaDocumento getDocumentoNaoEntregue() {
		return documentoNaoEntregue;
	}

	@Override
	public void setDocumentoNaoEntregue(EntregaDocumento documentoNaoEntregue) {
		this.documentoNaoEntregue = documentoNaoEntregue;
	}
	
	@Override
	public String getTempoExecucao() {
		return this.tempoExecucao;
	}
	
	@Override
	public void setTempoExecucao(String tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}
	
	@Override
	public Integer getMesProtocoloGerado() {
		return DataUtil.obterMes(this.documentoNaoEntregue.getDataSituacao());
	}
	
	@Override 
	public Integer getDiferencaDiasGeracaoRetornoProtocolo() {
		return DataUtil.diferencaDiasEntreDatas(this.loteComunicacao.getDataGeracao(),
				this.documentoNaoEntregue.getDataSituacao());
	}
	
	@Override
	public String getSequencialItemAnterior() {
		return sequencialItemAnterior;
	}

	@Override
	public void setSequencialItemAnterior(String sequencialItemAnterior) {
		this.sequencialItemAnterior = sequencialItemAnterior; 
		
	}
	
	@Override
	public Date getDataHoraExecucaoAnterior() {
		return dataHoraExecucaoAnterior;
	}

	@Override
	public void setDataHoraExecucaoAnterior(Date dataHoraExecucaoAnterior) {
		this.dataHoraExecucaoAnterior = dataHoraExecucaoAnterior;
		
	}

	@Override
	public ServicoAutorizacao getServicoAutorizacaoProtocolo() {
		return this.servicoAutorizacaoProtocolo;
	}

	@Override
	public void setServicoAutorizacaoProtocolo(ServicoAutorizacao servicoAutorizacaoProtocolo) {
		this.servicoAutorizacaoProtocolo = servicoAutorizacaoProtocolo;
	}
	
}
