package br.com.ggas.atendimento.comunicacao.negocio;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.ComunicacaoLoteVO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroAcompanhamentoLoteDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroLoteComunicacaoDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroRelatorioComunicacaoInterrupcaoDTO;
import br.com.ggas.atendimento.comunicacao.dominio.RelatorioAcompanhamentoComunicacaoVO;
import br.com.ggas.atendimento.comunicacao.dominio.RelatorioAntecedenciaAvisoVO;
import br.com.ggas.atendimento.comunicacao.dominio.RelatorioComunicacaoVO;
import br.com.ggas.atendimento.comunicacao.dominio.RelatorioListagemPontosComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.RelatorioPendenciasAntecedenciaAvisoVO;
import br.com.ggas.atendimento.comunicacao.dominio.RelatorioSinteticoComunicacaoInterrupcaoVO;
import br.com.ggas.atendimento.comunicacao.repositorio.RepositorioComunicacao;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;



public class ControladorComunicacaoImpl extends ControladorNegocioImpl implements ControladorComunicacao{

	private static final String IMAGEM = "imagem";
	
	private Fachada fachada = Fachada.getInstancia();
	
	@Autowired
	private RepositorioComunicacao repositorioComunicacao;
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	private ControladorCliente controladorCliente;
	
	@Autowired 
	private ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	private ControladorTipoDocumento controladorTipoDocumento;
	
	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;
	
	@Autowired
	private ControladorContrato controladorContrato;
	
	@Autowired
	private ControladorConstanteSistema controladorConstanteSistema;
	
	private static String RELATORIO_COMUNICACAO_CANCELADA = "relatorioCartaCancelamento.jasper";
	
	@Override
	public EntidadeNegocio criar() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(LoteComunicacao.BEAN_ID_LOTE_COMUNICACAO);
	}
	
	@Override
	public Collection<PontoConsumo> consultarPontosLoteComunicacao(FiltroLoteComunicacaoDTO filtroDTO) throws HibernateException, GGASException {
		return repositorioComunicacao.filtrarPontosParaLoteDeComunicacao(filtroDTO);
	}
	
	@Override
	public List<byte[]> gerarRelatorio(Collection<ComunicacaoLoteVO> listaComunicacaoLoteVO, ServicoTipo servicoTipo)
			throws GGASException {

		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		List<byte[]> relatorios = new ArrayList<>();

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
		
		listaComunicacaoLoteVO.parallelStream().forEach((p) -> {
			String[] horaExecucao = p.getHoraInicialExecucao().split(":");
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaExecucao[0]));
			c.set(Calendar.MINUTE, Integer.valueOf(horaExecucao[1]));
			Date horaExecucaoFormatada = DataUtil.arredondarMinutosHora(c.getTime());
			String horaInicialExecucao = Util
					.converterDataParaStringHoraMinutoComCaracteresEspeciais(horaExecucaoFormatada);

			p.setHoraInicialExecucao(horaInicialExecucao);
			c.setTime(horaExecucaoFormatada);
			c.add(Calendar.MINUTE, 60);
			Date dataIncrementada = c.getTime();

			String periodoExecucao = horaInicialExecucao + " horas e "
					+ Util.converterDataParaStringHoraMinutoComCaracteresEspeciais(dataIncrementada) + " horas";

			p.setPeriodoExecucao(periodoExecucao);

		});
		
		
		String local = "";
		if (empresa != null && empresa.getCliente() != null && empresa.getCliente().getEnderecoPrincipal() != null) {
			local = empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatadoMunicipioUF();
		}
		
		ServicoTipoAgendamentoTurno servicoTurno = listaComunicacaoLoteVO.iterator().next().getServicoTipoAgendamentoTurno();
		
		
		if(servicoTurno != null) {
			
			Map<Equipe, List<ComunicacaoLoteVO>> mapaComunicacaoLoteEquipe = listaComunicacaoLoteVO.stream().collect(Collectors.groupingBy(p -> p.getServicoTipoAgendamentoTurno().getEquipe(), LinkedHashMap::new, Collectors.toList()));
			
			for (Entry<Equipe, List<ComunicacaoLoteVO>> entry : mapaComunicacaoLoteEquipe.entrySet()) {
				
				byte[] retorno = null;
				Collection<Object> collRelatorio = new ArrayList<>();
				Map<String, Object> parametros = new HashMap<>();
				
				parametros.put(IMAGEM,
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
				
				parametros.put("enderecoEmpresa", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatado());
				
				parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());
				
				parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
				
				parametros.put("listagemComunicacao", entry.getValue());
				
				parametros.put("descricaoTipoServico", servicoTipo != null ? servicoTipo.getDescricao() : "");
	
				parametros.put("equipe", entry.getKey().getNome());
				
				parametros.put("imagemInformacao",
						Constantes.URL_LOGOMARCA_EMPRESA.replace("exibirLogoEmpresa/", "imagens/informacao-publica.jpg"));
	
	
				for(ComunicacaoLoteVO comunicacaoLoteVO : entry.getValue()) {
					
					RelatorioComunicacaoVO relatorio = new RelatorioComunicacaoVO();
					
					montarInformacoesRelatorioCarta(comunicacaoLoteVO, relatorio);
					
					collRelatorio.add(relatorio);
				}
				
				retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, servicoTipo != null ? servicoTipo.getDocumentoImpressaoAviso().getNomeArquivo() : RELATORIO_COMUNICACAO_CANCELADA, formatoImpressao);
				
				relatorios.add(retorno);
				
			}
		} else {
			byte[] retorno = null;
			Collection<Object> collRelatorio = new ArrayList<>();
			
			Map<String, Object> parametros = new HashMap<>();
			parametros.put(IMAGEM,
					Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			parametros.put("imagemInformacao",
					Constantes.URL_LOGOMARCA_EMPRESA.replace("exibirLogoEmpresa/", "imagens/informacao-publica.jpg"));
			
			parametros.put("dataFormatada", local + ", " + Util.dataAtualPorExtenso());
			
			parametros.put("enderecoEmpresa", empresa.getCliente().getEnderecoPrincipal().getEnderecoFormatado());
			
			parametros.put("listagemComunicacao", listaComunicacaoLoteVO);
			
			for(ComunicacaoLoteVO comunicacaoLoteVO : listaComunicacaoLoteVO) {
				
				RelatorioComunicacaoVO relatorio = new RelatorioComunicacaoVO();
				
				montarInformacoesRelatorioCarta(comunicacaoLoteVO, relatorio);
				
				collRelatorio.add(relatorio);
				
			}
			
			retorno = RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_COMUNICACAO_CANCELADA, formatoImpressao);
			
			relatorios.add(retorno);
		}
			
		
		return relatorios;
		
	}
	
	
	private void montarInformacoesRelatorioCarta(ComunicacaoLoteVO comunicacaoLoteVO, RelatorioComunicacaoVO relatorio)
			throws NegocioException {

		PontoConsumo pontoConsumo = comunicacaoLoteVO.getPontoConsumo();

		
		relatorio.setNome(pontoConsumo.getImovel().getNome());
		relatorio.setEndereco(pontoConsumo.getEnderecoFormatado());
		relatorio.setTempoExecucao(comunicacaoLoteVO.getTempoExecucao());
		relatorio.setDataExecucao(comunicacaoLoteVO.getDataExecucao());
		relatorio.setHoraExecucao(comunicacaoLoteVO.getPeriodoExecucao());
		
		Long tempoExecucao = comunicacaoLoteVO.getTempoExecucao() != null ? Long.parseLong(comunicacaoLoteVO.getTempoExecucao()) : 0l;
		Long tempoExecucaoMinutos = 0L;
		
		if(tempoExecucao >= 60) {
			tempoExecucaoMinutos = tempoExecucao % 60;
			tempoExecucao = tempoExecucao/60;
		}else {
			tempoExecucaoMinutos = tempoExecucao;
			tempoExecucao = 00L;
		}
		
		Date dataInicio = DataUtil.converterParaData(comunicacaoLoteVO.getDataExecucao());
		
		if(comunicacaoLoteVO.getServicoTipoAgendamentoTurno() != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(dataInicio);
			c.add(Calendar.MINUTE, Integer.valueOf(comunicacaoLoteVO.getTempoExecucao()));
			
			Date dataFim = c.getTime();
			
			relatorio.setSequencial("ALGÁS/COMUNICAÇÃO - Carta GGAS n° " + comunicacaoLoteVO.getSequencial());
			relatorio.setDataInicioExecucao(comunicacaoLoteVO.getDataExecucao() + " (" + DataUtil.obterDiaSemana(dataInicio)+ ")");
			relatorio.setDataFimExecucao(DataUtil.converterDataParaString(dataFim) + " (" + DataUtil.obterDiaSemana(dataFim)+ ")");
			relatorio.setTempoExecucaoMinutos(String.format("%02d", tempoExecucaoMinutos));
			relatorio.setTempoExecucao(String.format("%02d", tempoExecucao));

		} else {
			relatorio.setSequencial("ALGÁS/COMUNICAÇÃO - Carta GGAS n° " + comunicacaoLoteVO.getSequencial());
			relatorio.setDataExecucaoAnterior(DataUtil.converterDataParaString(comunicacaoLoteVO.getDataExecucaoAnterior()));
			relatorio.setSequencialAnterior(comunicacaoLoteVO.getSequencialAnterior());
		}
		
	}

	@Override
	public void gerarLote(Collection<ComunicacaoLoteVO> listaComunicacaoLote, ServicoTipo servicoTipo, Boolean isGerarAutorizacaoDeServico) throws GGASException {
		
		if (!listaComunicacaoLote.isEmpty()) {
			EntidadeConteudo situacaoPendente = controladorEntidadeConteudo.listarEntidadeConteudoPorEntidadeClasseEDescricao(
					"Situação da Entrega Documentos", "Pendente");
			TipoDocumento tipoDocumentoComunicacao = controladorTipoDocumento.obterTipoDocumentoDescricao("COMUNICAÇÃO");
			
			LoteComunicacao lote = (LoteComunicacao) repositorioComunicacao.criarLoteComunicacao();
			lote.setChavePrimaria(0);
			lote.setServicoTipo(servicoTipo);
			
			lote.setDataGeracao(Calendar.getInstance().getTime());
			lote.setGerarAutorizacaoServico(isGerarAutorizacaoDeServico);
			
			long chavePrimaria = repositorioComunicacao.inserir(lote);
			lote.setChavePrimaria(chavePrimaria);

			
			for (ComunicacaoLoteVO comunicacaoLoteVO : listaComunicacaoLote) {
				PontoConsumo pontoConsumo = comunicacaoLoteVO.getPontoConsumo();
				Cliente cliente = controladorCliente.obterClientePorPontoConsumo(pontoConsumo.getChavePrimaria());
				String[] horaExecucao = comunicacaoLoteVO.getHoraInicialExecucao().split(":");
				Date dataExec = Util.converterCampoStringParaData("Data Execução", comunicacaoLoteVO.getDataExecucao(), Constantes.FORMATO_DATA_BR);
				Calendar c = Calendar.getInstance();
				c.setTime(dataExec);
				c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(horaExecucao[0]));
				c.set(Calendar.MINUTE, Integer.valueOf(horaExecucao[1]));
				
				ItemLoteComunicacao item = (ItemLoteComunicacao) repositorioComunicacao.criarItemLoteComunicacao();
				item.setPontoConsumo(pontoConsumo);
				item.setLoteComunicacao(lote);
				item.setSequencialItem(comunicacaoLoteVO.getSequencial());
				item.setDataHoraExecucao(c.getTime());
				item.setUltimaAlteracao(Calendar.getInstance().getTime());
				item.setHabilitado(true);
				item.setServicoTipoAgendamentoTurno(comunicacaoLoteVO.getServicoTipoAgendamentoTurno());
				item.setTempoExecucao(comunicacaoLoteVO.getTempoExecucao());
				item.setDataHoraExecucaoAnterior(comunicacaoLoteVO.getDataExecucaoAnterior());
				item.setSequencialItemAnterior(comunicacaoLoteVO.getSequencialAnterior());
				
				repositorioComunicacao.inserir(item);
				
				if(servicoTipo == null || servicoTipo.getIndicadorProtocolo()) {
					EntregaDocumento documentoComunicacao = fachada.criarEntregaDocumento();
					documentoComunicacao.setCliente(cliente);
					documentoComunicacao.setImovel(pontoConsumo.getImovel());
					documentoComunicacao.setDataEmissao(Calendar.getInstance().getTime());
					documentoComunicacao.setDataSituacao(Calendar.getInstance().getTime());
					documentoComunicacao.setIndicadorRetornado(false);
					documentoComunicacao.setSituacaoEntrega(situacaoPendente);
					documentoComunicacao.setTipoDocumento(tipoDocumentoComunicacao);
					documentoComunicacao.setItemLoteComunicacao(item);
					
					fachada.inserirEntregaDocumento(documentoComunicacao);
				}
				
			}
			
			
		}
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ComunicacaoLoteVO> roterizacaoComunicacao(Collection<PontoConsumo> listaPontoConsumo) throws NegocioException, IOException{
		List<PontoConsumo> listaPontoConsumoAux = (List<PontoConsumo>) SerializationUtils.clone((Serializable) listaPontoConsumo.stream().collect(Collectors.toList()));
		Double latitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
		Double longitudeComparar = Double.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());		
		Collection<ComunicacaoLoteVO> listaRoteirizada = new ArrayList<>();
		
		while (!listaPontoConsumoAux.isEmpty()) {
			ComunicacaoLoteVO comunicacaoLoteVO = calculaPontoConsumoMenorDistancia(listaPontoConsumoAux, latitudeComparar, longitudeComparar);
			
			latitudeComparar = comunicacaoLoteVO.getPontoConsumo().getLatitudeGrau().doubleValue();
			longitudeComparar = comunicacaoLoteVO.getPontoConsumo().getLongitudeGrau().doubleValue();
			
			listaRoteirizada.add(comunicacaoLoteVO);
			listaPontoConsumoAux.remove(comunicacaoLoteVO.getPontoConsumo());
			
		}
		
		return listaRoteirizada;
	}
	
	@Override
	public void calcularDistanciaPontoConsumo(ComunicacaoLoteVO comunicacaoLoteVO) throws NegocioException, IOException {
		Double latitudeComparar = Double
				.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LATITUDE_EMPRESA).getValor());
		Double longitudeComparar = Double
				.valueOf(controladorParametroSistema.obterParametroPorCodigo(Constantes.LONGITUDE_EMPRESA).getValor());
		List<PontoConsumo> listaPontoConsumo = new ArrayList<>();
		listaPontoConsumo.add(comunicacaoLoteVO.getPontoConsumo());
		
		ComunicacaoLoteVO retorno = this.calculaPontoConsumoMenorDistancia(listaPontoConsumo, latitudeComparar, longitudeComparar);
		comunicacaoLoteVO.setDistanciaEntrePontos(retorno.getDistanciaEntrePontos());
		comunicacaoLoteVO.setTempoDistancia(retorno.getTempoDistancia());

	}
	
	private ComunicacaoLoteVO calculaPontoConsumoMenorDistancia(List<PontoConsumo> listaPontoConsumo,
			Double latitudeComparar, Double longitudeComparar) throws IOException {
		ComunicacaoLoteVO comunicacaoLoteVO = new ComunicacaoLoteVO();
		Double menorDistancia = -1.0;
		PontoConsumo pontoConsumo = listaPontoConsumo.iterator().next();
		Long tempoDistancia = 0l;
		
		LOG.info("Iniciando chamada da API do Google (Comunicação) ");
		Map<String, Object> mapaRetornado = Util.obterMenorDistanciaTempoEntrePontosConsumo(listaPontoConsumo, latitudeComparar, longitudeComparar, menorDistancia, pontoConsumo, tempoDistancia);
		LOG.info("Finalizando chamada da API do Google (Comunicação) ");
		pontoConsumo = (PontoConsumo) mapaRetornado.get("pontoConsumo");
		tempoDistancia = (Long) mapaRetornado.get("tempoDistancia");
		menorDistancia = (Double) mapaRetornado.get("menorDistancia");
		
		BigDecimal distancia = new BigDecimal(menorDistancia).divide(new BigDecimal(1000)).setScale(2, RoundingMode.HALF_UP);
		
		comunicacaoLoteVO.setPontoConsumo(pontoConsumo);
		comunicacaoLoteVO.setDistanciaEntrePontos(distancia.doubleValue());
		
		
		tempoDistancia = tempoDistancia / 60;
		
		if(tempoDistancia.compareTo(1l) < 0) {
			tempoDistancia = 1l;
		}

		comunicacaoLoteVO.setTempoDistancia(String.valueOf(tempoDistancia));
		
		comunicacaoLoteVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
		comunicacaoLoteVO.setEnderecoFormatado(pontoConsumo.getEnderecoFormatado());
		
		return comunicacaoLoteVO;

	}
	

	@Override
	public void processarEmissaoComunicacaoEmLoteBatch(StringBuilder logProcessamento, Map<String, Object> dados)
			throws IOException, GGASException {
		Collection<ItemLoteComunicacao> itens = consultarItensComunicacaoParaGeracao();
		Collection<ServicoAutorizacaoVO> colecaoRelatorio = new ArrayList<>();
		Map<String, Collection<String>> logSucessoErro = new LinkedHashMap<>();
		logSucessoErro.put("SUCESSO", new ArrayList<>());
		logSucessoErro.put("ERRO", new ArrayList<>());
		
		for (ItemLoteComunicacao item : itens) {
			if (item.getLoteComunicacao().getServicoTipo().getIndicadorGeraLote()) {
				dados.put("dataPrevisaoComunicacao", item.getDataHoraExecucao());
				
				colecaoRelatorio.addAll(controladorServicoAutorizacao.gerarAutorizacaoServicoItemComunicacao(
						item, dados, logSucessoErro));

			} else {
				logProcessamento.append(Constantes.PULA_LINHA);
				logProcessamento.append("Tipo de Servico nao permite gerar autorizacoes de servico em lote.");
			}
			
		}
		
		if(!colecaoRelatorio.isEmpty()) {
			controladorServicoAutorizacao.gerarRelatorioListagemAS(colecaoRelatorio, dados);
		}
		
		
		for(Entry<String, Collection<String>> entry : logSucessoErro.entrySet()) {
			logProcessamento.append(Constantes.PULA_LINHA);
			if(entry.getKey().equals("SUCESSO")) {
				logProcessamento.append("Autorização de Serviços Geradas com Sucesso: ");
			} else {
				logProcessamento.append("Autorização de Serviços Geradas com Erro: ");
			}
			logProcessamento.append(Constantes.PULA_LINHA);
			for(String log : entry.getValue()) {
				logProcessamento.append(log);
				logProcessamento.append(Constantes.PULA_LINHA);
			}
		}
		
		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append("Total de itens de comunicacao processados: ");
		logProcessamento.append(itens.size());

		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append("Total de autorizacoes de servico geradas: ");
		logProcessamento.append(colecaoRelatorio.size());

	}

	@Override
	public Collection<ItemLoteComunicacao> consultarItensComunicacaoParaGeracao() throws NegocioException, HibernateException, GGASException {
		Collection<LoteComunicacao> lotes = consultarLoteComunicacaoNaoGerado();
		
		Collection<ItemLoteComunicacao> itensGeracao = new ArrayList<>();
		for (LoteComunicacao lote : lotes) {
			ServicoTipo servicoTipo = lote.getServicoTipo();
			Date dataAtual = new Date();
			Long tempoConclusaoProtocolo = servicoTipo.getTempoEmissaoAS();
			Integer diferenca = 0;

			if (servicoTipo.getIndicadorGeraLote()) {

				if (servicoTipo.getIndicadorProtocolo() && servicoTipo.getIndicadorProtocoloObrigatorio()) {
					if ((!servicoTipo.getIndicadorTipoProtocolo() && lote.isTodosProtocolosRetornados())
							|| servicoTipo.getIndicadorTipoProtocolo()) {

						for (ItemLoteComunicacao item : lote.getItens()) {
							if (item.isHabilitado() && item.getDocumentoNaoEntregue().isIndicadorRetornado()) {
								diferenca = DataUtil.diferencaDiasEntreDatas(
										item.getDocumentoNaoEntregue().getDataSituacao(), dataAtual);
								if (diferenca >= tempoConclusaoProtocolo) {
									itensGeracao.add(item);
								}
							}
						}
					}
				} else {
					Date dataGeracao = lote.getDataGeracao();
					diferenca = DataUtil.diferencaDiasEntreDatas(dataGeracao, dataAtual);

					if (diferenca >= tempoConclusaoProtocolo) {
						itensGeracao.addAll(lote.getItens());
					}
				}

			}
		}
		
		itensGeracao.removeIf(item -> item.getServicoAutorizacao() != null);
		
		return itensGeracao;
	}
	
	@Override
	public Collection<LoteComunicacao> consultarLoteComunicacaoNaoGerado() throws NegocioException, HibernateException, GGASException {
		return repositorioComunicacao.consultarLoteComunicacaoNaoGerado();
	}
	
	@Override
	public Map<String, List<ServicoTipoAgendamentoTurno>> montarMapaServicoTipoAgendamento(ServicoTipo servicoTipo, EntidadeConteudo[] turno) {
		List<ServicoTipoAgendamentoTurno> listaServicoTipoAgendamentoTurno = servicoTipo.getListaAgendamentosTurno().stream()
				.sorted(Comparator.comparing(p -> p.getTurno().getDescricao())).collect(Collectors.toList());
		
		if (turno != null && turno.length > 0) {
	        listaServicoTipoAgendamentoTurno = listaServicoTipoAgendamentoTurno.stream()
	                .filter(p -> Arrays.stream(turno)
	                        .anyMatch(t -> t.getChavePrimaria() == p.getTurno().getChavePrimaria()))
	                .collect(Collectors.toList());
		}
		
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoAgendamentoTurno> listaAuxiliar = (Collection<ServicoTipoAgendamentoTurno>) SerializationUtils
				.clone((Serializable) listaServicoTipoAgendamentoTurno);
		
		
		List<String> ordemDesejada = Arrays.asList("MANHÃ", "TARDE", "NOITE");
		
		Map<String, List<ServicoTipoAgendamentoTurno>> retorno = listaAuxiliar.stream().collect(
				Collectors.groupingBy(p -> p.getTurno().getDescricao(), LinkedHashMap::new, Collectors.toList()));
		
        Map<String, List<ServicoTipoAgendamentoTurno>> retornoOrdenado = new LinkedHashMap<>();
        ordemDesejada.forEach(chave -> {
            if (retorno.containsKey(chave)) {
                retornoOrdenado.put(chave, retorno.get(chave));
            }
        });
		
		return retornoOrdenado;

	}

	@Override
	public Long verificaServicoTipoAgendamento(Long chaveServicoTipoAgendamento) {
		return this.repositorioComunicacao.verificaServicoTipoAgendamento(chaveServicoTipoAgendamento);
	}
	
	@Override
	public Collection<LoteComunicacao> consultarLoteComunicacao(FiltroAcompanhamentoLoteDTO filtroDTO) throws HibernateException, GGASException {
		return repositorioComunicacao.filtrarLoteComunicacao(filtroDTO);
	}
	
	@Override
	public ItemLoteComunicacao obterUltimoItemLoteComunicacao(Integer anoExecucao) {
		return repositorioComunicacao.obterUltimoItemLoteComunicacao(anoExecucao);
	}
	
	@Override
	public Collection<PontoConsumo> filtrarPontosParaLoteDeComunicacao(String descricaoPonto, String codigoLegado, String servicoTipo, Integer offset) {
		return repositorioComunicacao.filtrarPontosParaLoteDeComunicacao(descricaoPonto, codigoLegado, servicoTipo, offset);
	}
	
	@Override
	public byte[] montarPlanilhaListagemPontosComunicacao(Collection<PontoConsumo> listaPontoConsumo) throws NegocioException {
		
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		FormatoImpressao formatoImpressao = FormatoImpressao.XLS;
		
		Map<String, Object> parametros = new HashMap<>();
		
		Collection<Object> collRelatorio = new ArrayList<>();
		
		parametros.put(IMAGEM,
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		Collection<RelatorioListagemPontosComunicacao> colecaoCamposPontosConsumo = new ArrayList<RelatorioListagemPontosComunicacao>();
		
		for(PontoConsumo pontoConsumo : listaPontoConsumo) {
			
			RelatorioListagemPontosComunicacao relatorio = new RelatorioListagemPontosComunicacao();
			
			montarInformacoesPontosConsumoComunicacao(pontoConsumo, relatorio);
			
			colecaoCamposPontosConsumo.add(relatorio);
			
		}
				
		parametros.put("listagemPontosConsumo", colecaoCamposPontosConsumo);
		collRelatorio.add(colecaoCamposPontosConsumo);
		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, "relatorioListagemMedidores.jasper", formatoImpressao);
		
	}


	private void montarInformacoesPontosConsumoComunicacao(PontoConsumo pontoConsumo,
			RelatorioListagemPontosComunicacao relatorio) {

		relatorio.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
		relatorio.setCodigoLegado(pontoConsumo.getCodigoLegado());
		relatorio.setEndereco(pontoConsumo.getEnderecoFormatadoRuaNumeroComplemento());
		relatorio.setBairro(pontoConsumo.getCep().getBairro());
		relatorio.setSituacaoConsumo(pontoConsumo.getSituacaoConsumo() != null ? pontoConsumo.getSituacaoConsumo().getDescricao() : 
			"");
		
		Medidor medidor = (pontoConsumo.getInstalacaoMedidor() != null ? pontoConsumo.getInstalacaoMedidor().getMedidor() : null);
		InstalacaoMedidor medidorInstalacao = pontoConsumo.getInstalacaoMedidor();
		
		if(medidor != null) {
			relatorio.setModelo(medidor.getModelo().getDescricao());
			relatorio.setNumeroSerie(medidor.getNumeroSerie());
			relatorio.setDataInstalacao(DataUtil.converterDataParaString(medidorInstalacao.getData()));
			relatorio.setDataAquisicao(DataUtil.converterDataParaString(medidorInstalacao.getMedidor().getDataAquisicao()));
		}
		
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
		
		if(contratoPontoConsumo != null) {
			StringBuilder pressaoContrato = new StringBuilder();
			pressaoContrato.append(contratoPontoConsumo.getMedidaPressao());
			pressaoContrato.append(" ");
			pressaoContrato.append(contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());
			
			relatorio.setPressaoEntrega(pressaoContrato.toString());
			
			StringBuilder vazaoContrato = new StringBuilder();
			vazaoContrato.append("");
			
			if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {
				vazaoContrato.append(contratoPontoConsumo.getMedidaVazaoMaximaInstantanea());
				vazaoContrato.append(" ");
				vazaoContrato.append(contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricaoAbreviada());
			}
			
			relatorio.setVazaoMaxima(vazaoContrato.toString());
		}
		
		
	}
	
	@Override
	public Boolean cancelarLote(LoteComunicacao loteComunicacao, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {
		Collection<ItemLoteComunicacao> itensLote = loteComunicacao.getItens().stream().filter(p -> p.getServicoAutorizacao() == null || (p.getServicoAutorizacao().getStatus().getDescricao().equals("ABERTA") || p.getServicoAutorizacao().getStatus().getDescricao().equals("CANCELADA"))).collect(Collectors.toList());
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_CANCELADO);
		EntidadeConteudo statusAS = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		
		
		if (!itensLote.isEmpty()) {
			
			for (ItemLoteComunicacao item : itensLote) {
				
				EntregaDocumento documento = item.getDocumentoNaoEntregue();
				documento.setHabilitado(false);
				
				this.atualizar(documento);
				
				item.setHabilitado(false);
				this.atualizar(item);
				
				if(item.getServicoAutorizacao() != null && !item.getServicoAutorizacao().getStatus().equals("CANCELADA")) {
					ServicoAutorizacao servicoAutorizacao = item.getServicoAutorizacao();
					servicoAutorizacao.setStatus(statusAS);
					servicoAutorizacao.setDataEncerramento(new Date());
					
					this.atualizar(servicoAutorizacao);
				}
			}
			
			loteComunicacao.setHabilitado(false);
			this.atualizar(loteComunicacao);
			
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	@Override
		public Collection<ItemLoteComunicacao> obterItensLoteComunicacaoRelatorioSintetico(
			FiltroRelatorioComunicacaoInterrupcaoDTO filtro) throws HibernateException, GGASException {
		return repositorioComunicacao.obterItensLoteComunicacaoRelatorioSintetico(filtro);
	}
	
	
	@Override
	public byte[] gerarRelatorioSinteticoComunicacaoInterrupcao(FiltroRelatorioComunicacaoInterrupcaoDTO filtro)
			throws HibernateException, GGASException {
		Collection<ItemLoteComunicacao> listaItemLoteComunicacao = this
				.obterItensLoteComunicacaoRelatorioSintetico(filtro);
		
		if(listaItemLoteComunicacao == null || listaItemLoteComunicacao.isEmpty()) {
			return null;
		}

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		FormatoImpressao formatoImpressao = filtro.getFormatoImpressao() ? FormatoImpressao.PDF : FormatoImpressao.XLS;

		Collection<RelatorioSinteticoComunicacaoInterrupcaoVO> listagemComunicacao = new ArrayList<RelatorioSinteticoComunicacaoInterrupcaoVO>();

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();

		Collection<Object> collRelatorio = new ArrayList<>();
		Map<String, Object> parametros = new HashMap<>();

		parametros.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));

		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));

		for (ItemLoteComunicacao item : listaItemLoteComunicacao) {
			listagemComunicacao.add(montarInformacoesRelatorioSinteticoComunicacao(item));
		}

		parametros.put("listagemComunicacao", listagemComunicacao);
		collRelatorio.add(listagemComunicacao);

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros,
				"relatorioSinteticoComunicacacoesDeInterrupcoes.jasper", formatoImpressao);
	}
	
	private RelatorioSinteticoComunicacaoInterrupcaoVO montarInformacoesRelatorioSinteticoComunicacao(ItemLoteComunicacao item) {
		RelatorioSinteticoComunicacaoInterrupcaoVO relatorio = new RelatorioSinteticoComunicacaoInterrupcaoVO();
		
		relatorio.setTipoServico(item.getLoteComunicacao().getServicoTipo().getDescricao());
		relatorio.setDescricaoPontoConsumo(item.getPontoConsumo().getDescricao());
		relatorio.setDataProtocolo(DataUtil.converterDataParaString(item.getLoteComunicacao().getDataGeracao()));
		relatorio.setDataPrevistaExecucao(DataUtil.converterDataParaString(item.getDataHoraExecucao()));
		
		String dataExecucao = "";
		if(item.getServicoAutorizacao() != null && item.getServicoAutorizacao().getDataExecucao() != null) {
			dataExecucao = DataUtil.converterDataParaString(item.getServicoAutorizacao().getDataExecucao());
		}
		
		String dataRetornoProtocolo = "";
		
		if(item.getDocumentoNaoEntregue() != null && item.getDocumentoNaoEntregue().isIndicadorRetornado()) {
			dataRetornoProtocolo = DataUtil.converterDataParaString(item.getDocumentoNaoEntregue().getDataSituacao());
		}
			
		relatorio.setDataExecucao(dataExecucao);	
		relatorio.setDataRetornoProtocolo(dataRetornoProtocolo);
		
		return relatorio;
	}
	
	
	@Override
	public Collection<ItemLoteComunicacao> consultarItemLoteRelatorioAntecedenciaAviso(
			FiltroRelatorioComunicacaoInterrupcaoDTO filtro) {
		return repositorioComunicacao.consultarAntecedenciaAvisosPorAno(filtro);
	}
	
	@Override
	public byte[] gerarRelatorioAntecedenciaAviso(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) throws NegocioException {
	
		Collection<ItemLoteComunicacao> listaItemLoteComunicacao = this.consultarItemLoteRelatorioAntecedenciaAviso(filtro);
		
		if(listaItemLoteComunicacao == null || listaItemLoteComunicacao.isEmpty()) {
			return null;
		}

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		FormatoImpressao formatoImpressao = filtro.getFormatoImpressao() ? FormatoImpressao.PDF : FormatoImpressao.XLS;

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();

		Collection<Object> collRelatorio = new ArrayList<>();
		Map<String, Object> parametros = new HashMap<>();

		parametros.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		parametros.put("logoArsal", Constantes.URL_IMAGENS_GGAS + "logoArsal.png");
		
		parametros.put("newLogoAlgas", Constantes.URL_IMAGENS_GGAS + "newLogoAlgas.png");

		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		parametros.put("listagemItensAntecedenciaAviso", montarListaItensRelatorioAntecedencia(listaItemLoteComunicacao, filtro));
		
		parametros.put("dataReferencia", filtro.getMesAnoCorrente());

		Collection<ItemLoteComunicacao> listaPendenciasItens = this.consultarItensPendentesAntecedenciaAvisosPorAno(filtro);
		
		Collection<RelatorioPendenciasAntecedenciaAvisoVO> listaPendenciasItensRelatorio = this.montarListaPendenciasAntecedenciaAviso(listaPendenciasItens);
		
		if(listaPendenciasItensRelatorio.size() <= 0) {
			for (int i = 0; i < 10; i++) {
				RelatorioPendenciasAntecedenciaAvisoVO relatorioTemp = new RelatorioPendenciasAntecedenciaAvisoVO(" "," "," ");
				listaPendenciasItensRelatorio.add(relatorioTemp);
			}
		}
		
		parametros.put("listaPendenciaItens", listaPendenciasItensRelatorio);
		
		collRelatorio.add(listaPendenciasItensRelatorio);
		
		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros,
				"relatorioAntecedenciaMinimaAviso.jasper", formatoImpressao);	
		
		}
	
	
	private List<RelatorioAntecedenciaAvisoVO> montarListaItensRelatorioAntecedencia(
			Collection<ItemLoteComunicacao> itensLote, FiltroRelatorioComunicacaoInterrupcaoDTO filtro) {
		List<RelatorioAntecedenciaAvisoVO> listaRelatorio = new ArrayList<RelatorioAntecedenciaAvisoVO>();
		
		int mesFiltro = Integer.valueOf(filtro.getMesAnoCorrente().split("/")[0]);

		for (int i = 1; i <= mesFiltro; i++) {
			RelatorioAntecedenciaAvisoVO relatorio = new RelatorioAntecedenciaAvisoVO();
			relatorio.setMes(DataUtil.obterNomeMesPeloNumero(i));
			listaRelatorio.add(relatorio);
		}

		for (ItemLoteComunicacao item : itensLote) {
			RelatorioAntecedenciaAvisoVO relatorio;
			
			if(item.getMesProtocoloGerado() == 0) {
				relatorio = listaRelatorio.get(item.getMesProtocoloGerado());
			}else {
				relatorio = listaRelatorio.get(item.getMesProtocoloGerado()-1);
			}
			
			Integer dias = item.getDiferencaDiasGeracaoRetornoProtocolo();
			if (dias == 0) {
				relatorio.setDias0(relatorio.getDias0() + 1);
			} else if (dias == 1) {
				relatorio.setDias1(relatorio.getDias1() + 1);
			} else if (dias == 2) {
				relatorio.setDias2(relatorio.getDias2() + 1);
			} else if (dias == 3) {
				relatorio.setDias3(relatorio.getDias3() + 1);
			} else if (dias > 3) {
				relatorio.setDiasMaior3(relatorio.getDiasMaior3() + 1);
			}

		}

		return listaRelatorio;
	}
	
	@Override
	public Collection<ItemLoteComunicacao> consultarItensPendentesAntecedenciaAvisosPorAno(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) {
		return repositorioComunicacao.consultarItensPendentesAntecedenciaAvisosPorAnoDataEncerramentoNaoNula(filtro);
	}
	
	
	private Collection<RelatorioPendenciasAntecedenciaAvisoVO> montarListaPendenciasAntecedenciaAviso(Collection<ItemLoteComunicacao> itensPendentes) {
		Collection<RelatorioPendenciasAntecedenciaAvisoVO> listaPendenciasItens = new ArrayList<>();
		
		for(ItemLoteComunicacao item : itensPendentes) {
			
			RelatorioPendenciasAntecedenciaAvisoVO itemPendente = new RelatorioPendenciasAntecedenciaAvisoVO();
			
			itemPendente.setDescricaoPontoConsumo(item.getPontoConsumo().getDescricao());

			itemPendente.setDataPrevistaExecucaoServico(DataUtil.converterDataParaString(item.getDataHoraExecucao()));	
			
			itemPendente.setDataRetornoProtocolo(DataUtil.converterDataParaString(item.getDocumentoNaoEntregue().getDataSituacao()));
			
			listaPendenciasItens.add(itemPendente);
			
		}
		
		return listaPendenciasItens;
	}


	@Override
	public void removerItens(Collection<ItemLoteComunicacao> itensRemover, Boolean removerLote)
			throws NegocioException, ConcorrenciaException {
		
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_SERV_AUT_CANCELADO);
		EntidadeConteudo statusAS = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constante.getValor()));

		for (ItemLoteComunicacao itemLoteComunicacao : itensRemover) {
			EntregaDocumento documento = itemLoteComunicacao.getDocumentoNaoEntregue();
			documento.setHabilitado(false);

			this.atualizar(documento);

			itemLoteComunicacao.setHabilitado(false);
			this.atualizar(itemLoteComunicacao);
			
			if(itemLoteComunicacao.getServicoAutorizacao() != null && !itemLoteComunicacao.getServicoAutorizacao().getStatus().equals("CANCELADA")) {
				ServicoAutorizacao servicoAutorizacao = itemLoteComunicacao.getServicoAutorizacao();
				servicoAutorizacao.setStatus(statusAS);
				servicoAutorizacao.setDataEncerramento(new Date());
				
				this.atualizar(servicoAutorizacao);
			}
		}

		if (removerLote) {

			LoteComunicacao loteComunicacao = itensRemover.iterator().next().getLoteComunicacao();
			loteComunicacao.setHabilitado(false);
			this.atualizar(loteComunicacao);
		}

	}
	
	
	@Override
	public void removerProtocolo(ItemLoteComunicacao itemLote) throws NegocioException, ConcorrenciaException {
		EntregaDocumento documento = itemLote.getDocumentoNaoEntregue();
		documento.setHabilitado(false);
		
		this.atualizar(documento);
		
		itemLote.setDocumentoNaoEntregue(null);
		this.atualizar(itemLote);
	}
	
	
	@Override
	public List<byte[]>  gerarLoteCancelamento(Collection<ItemLoteComunicacao> listaItemLoteComunicacao, String dataAtual) throws IOException, GGASException {
		Collection<PontoConsumo> listaPontoConsumo = listaItemLoteComunicacao.stream().filter(p -> p.getServicoAutorizacao() == null || p.getServicoAutorizacao().getStatus().getDescricao().equals("ABERTA")).map(p -> p.getPontoConsumo()).collect(Collectors.toList());
		
		Collection<ComunicacaoLoteVO> listaItemCancelado = this.roterizacaoComunicacao(listaPontoConsumo);
		
		Calendar dataExecucao = Calendar.getInstance();
		dataExecucao.setTime(Util.converterCampoStringParaData("Data Execução Inicial",
				dataAtual, Constantes.FORMATO_DATA_BR));
		
		ItemLoteComunicacao ultimoItem = this.obterUltimoItemLoteComunicacao(dataExecucao.get(Calendar.YEAR));
		Integer sequencial = 1;
		String anoCorrente = "." + Calendar.getInstance().get(Calendar.YEAR);
		
		if (ultimoItem != null) {
			String[] sequencialItem = ultimoItem.getSequencialItem().split("\\.");
			Integer anoSequencial = Integer.valueOf(sequencialItem[1]);

			if (anoSequencial.compareTo(dataExecucao.get(Calendar.YEAR)) == 0) {
				sequencial = Integer.valueOf(sequencialItem[0]);
			}

		}
		
		for(ComunicacaoLoteVO item : listaItemCancelado) {
			
			ItemLoteComunicacao itemCancelado = listaItemLoteComunicacao.stream().filter(p -> p.getPontoConsumo().getChavePrimaria() == item.getPontoConsumo().getChavePrimaria()).findFirst().get();
			
			sequencial = sequencial + 1;
			
			item.setDataExecucao(dataAtual);
			item.setHoraInicialExecucao("00:00");
			item.setSequencial(sequencial + anoCorrente);
			item.setSequencialAnterior(itemCancelado.getSequencialItem());
			item.setDataExecucaoAnterior(itemCancelado.getDataHoraExecucao());
		}
		
		
		this.gerarLote(listaItemCancelado, null, Boolean.FALSE);
		
		List<byte[]> relatorios =
				this.gerarRelatorio(listaItemCancelado, null);
		
		return relatorios;
	}


	@Override
	public Long consultarEntregaDocumento(Long chavePrimaria) {
		return repositorioComunicacao.consultarEntregaDocumento(chavePrimaria);
	}

	
	@Override
	public Collection<LoteComunicacao> consultarLoteComunicacaoPorChavesPrimarias(Long[] chavesPrimarias) {
		return repositorioComunicacao.consultarLoteComunicacaoPorChavesPrimarias(chavesPrimarias);
	}
	
	@Override
	public byte[] gerarRelatorioAcompanhamentoComunicacao(Collection<LoteComunicacao> lotesComunicacao, Boolean tipoRelatorio, Boolean tipoImpressao) throws NegocioException {
		byte[] relatorio = null;
		
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		FormatoImpressao formatoImpressao = tipoImpressao ? FormatoImpressao.PDF : FormatoImpressao.XLS;
		
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		Collection<Object> collRelatorio = new ArrayList<>();
		Map<String, Object> parametros = new HashMap<>();
		
		parametros.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));

		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		Collection<RelatorioAcompanhamentoComunicacaoVO> listaParametrosRelatorio = this.montarInformacoesRelatorioAcompanhamentoComunicacao(lotesComunicacao, tipoRelatorio);
		
		parametros.put("listaParametrosRelatorio", listaParametrosRelatorio);
		
		collRelatorio.add(listaParametrosRelatorio);
		
		String nomeRelatorio = tipoRelatorio ? "relatorioComunicacaoAnalitico.jasper"
				: "relatorioComunicacaoSintetico.jasper";
		
		relatorio = RelatorioUtil.gerarRelatorio(collRelatorio, parametros,
				nomeRelatorio, formatoImpressao);
		
		
		return relatorio;
	}


	private Collection<RelatorioAcompanhamentoComunicacaoVO> montarInformacoesRelatorioAcompanhamentoComunicacao(
			Collection<LoteComunicacao> lotesComunicacao, Boolean tipoRelatorio) throws NegocioException {
		Collection<RelatorioAcompanhamentoComunicacaoVO> retorno = new ArrayList<RelatorioAcompanhamentoComunicacaoVO>();
		
		for(LoteComunicacao loteComunicacao : lotesComunicacao) {
			Collection<ItemLoteComunicacao> itens = loteComunicacao.getItens();
			ServicoTipo servicoTipo = loteComunicacao.getServicoTipo();
			
			for(ItemLoteComunicacao item : itens) {
				RelatorioAcompanhamentoComunicacaoVO relatorioAcompanhamentoComunicacaoVO = new RelatorioAcompanhamentoComunicacaoVO();
				
				PontoConsumo pontoConsumo = item.getPontoConsumo();
				
				if(pontoConsumo != null) {
					relatorioAcompanhamentoComunicacaoVO.setCodigoLegado(!StringUtils.isEmpty(pontoConsumo.getCodigoLegado()) ? pontoConsumo.getCodigoLegado() : "");
                    relatorioAcompanhamentoComunicacaoVO.setNomePontoConsumo(!StringUtils.isEmpty(pontoConsumo.getDescricao()) ? pontoConsumo.getDescricao() : "");
				}
				
				relatorioAcompanhamentoComunicacaoVO.setNumeroSequencial(item.getSequencialItem());
				relatorioAcompanhamentoComunicacaoVO.setNumeroLote(String.valueOf(loteComunicacao.getChavePrimaria()));
				
				
				if(tipoRelatorio) {
					
					relatorioAcompanhamentoComunicacaoVO.setBairro(pontoConsumo != null && pontoConsumo.getCep() != null ? pontoConsumo.getCep().getBairro() : "");
					
					if(!item.isHabilitado()) {
						relatorioAcompanhamentoComunicacaoVO.setSituacaoCarta("Cancelada");
					} else if (item.getDocumentoNaoEntregue().isIndicadorRetornado()) {
						relatorioAcompanhamentoComunicacaoVO.setSituacaoCarta("Entregue");
					} else {
						relatorioAcompanhamentoComunicacaoVO.setSituacaoCarta("Emitida");
					}
					
					ServicoAutorizacao servicoAutorizacao = item.getServicoAutorizacao();
					
					
					if(servicoAutorizacao != null) {
						relatorioAcompanhamentoComunicacaoVO.setNumeroOS(item.getServicoAutorizacao().getNumeroOS() != null ? String.valueOf(item.getServicoAutorizacao().getNumeroOS()) : "");
						relatorioAcompanhamentoComunicacaoVO.setNumeroAS(String.valueOf(item.getServicoAutorizacao().getChavePrimaria()));
						relatorioAcompanhamentoComunicacaoVO.setDataEmissaoAS(DataUtil.converterDataParaString(servicoAutorizacao.getDataGeracao()));
						relatorioAcompanhamentoComunicacaoVO.setDataExecucaoAS(servicoAutorizacao.getDataExecucao() != null ? DataUtil.converterDataParaString(servicoAutorizacao.getDataExecucao()) : "");
						relatorioAcompanhamentoComunicacaoVO.setStatusAS(servicoAutorizacao.getStatus() != null ? servicoAutorizacao.getStatus().getDescricao() : "");

						ServicoAutorizacaoAgenda servicoAutorizacaoAgenda = controladorServicoAutorizacao.obterServicoAutorizacaoAgendaPorServicoAutorizacao(servicoAutorizacao.getChavePrimaria());
						
						if(servicoAutorizacaoAgenda != null && servicoAutorizacaoAgenda.getDataAgenda() != null) {
							relatorioAcompanhamentoComunicacaoVO.setDataAgendamentoAS(DataUtil.converterDataParaString(servicoAutorizacaoAgenda.getDataAgenda(), true));
						}
					}
					if(pontoConsumo != null) {
						ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterUltimoContratoPontoConsumoPorPontoConsumo(item.getPontoConsumo().getChavePrimaria());
						
						if(contratoPontoConsumo != null) {
							
							if(contratoPontoConsumo.getMedidaPressao() != null && contratoPontoConsumo.getUnidadePressao() != null) {
								relatorioAcompanhamentoComunicacaoVO.setPressao(contratoPontoConsumo.getMedidaPressao().toString() +  " " + contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada());
							}
							
							if(contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null && contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea() != null) {
								relatorioAcompanhamentoComunicacaoVO.setVazao(contratoPontoConsumo.getMedidaVazaoMaximaInstantanea().toString() +  " " + contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricaoAbreviada());
							}
							
						}
						
						if(servicoAutorizacao != null && servicoAutorizacao.getDataExecucao() != null && pontoConsumo.getInstalacaoMedidor() != null) {
							Medidor medidor = pontoConsumo.getInstalacaoMedidor().getMedidor();
							relatorioAcompanhamentoComunicacaoVO.setNumeroSerie(medidor.getNumeroSerie());
							relatorioAcompanhamentoComunicacaoVO.setNumeroBP(medidor.getTombamento());
							relatorioAcompanhamentoComunicacaoVO.setTipoMedidor(medidor.getTipoMedidor().getDescricao());
						}
					}

					
				} else {
					relatorioAcompanhamentoComunicacaoVO.setTipoServico(servicoTipo != null ? servicoTipo.getDescricao() : "");
					relatorioAcompanhamentoComunicacaoVO.setPossuiAutorizacaoDeServico(item.getServicoAutorizacao() != null ? "Sim" : "Não");
					relatorioAcompanhamentoComunicacaoVO.setCartaCancelada(item.isHabilitado() ? "Não" : "Sim");
					relatorioAcompanhamentoComunicacaoVO.setDataHoraPlanejada(DataUtil.converterDataParaString(item.getDataHoraExecucao(), true));
				}
				
				retorno.add(relatorioAcompanhamentoComunicacaoVO);
				
			}
		}
		
		
		return retorno;
	}

	
}
