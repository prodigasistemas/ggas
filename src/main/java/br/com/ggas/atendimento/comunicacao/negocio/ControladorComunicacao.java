package br.com.ggas.atendimento.comunicacao.negocio;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import br.com.ggas.atendimento.comunicacao.dominio.FiltroLoteComunicacaoDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroRelatorioComunicacaoInterrupcaoDTO;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.atendimento.comunicacao.ItemLoteComunicacao;
import br.com.ggas.atendimento.comunicacao.LoteComunicacao;
import br.com.ggas.atendimento.comunicacao.dominio.ComunicacaoLoteVO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroAcompanhamentoLoteDTO;
import br.com.ggas.atendimento.comunicacao.dominio.FiltroGeracaoComunicaoLoteDTO;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.entregadocumento.EntregaDocumento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import okhttp3.Response;

public interface ControladorComunicacao extends ControladorNegocio{
	
	String BEAN_ID_CONTROLADOR_COMUNICACAO = "controladorComunicacao";
	
	/**
	 * Metodo responsavel por consultar os pontos para gerar o lote de comunicacao
	 * 
	 * @param filtro O filtro com os parametros para a pesquisa.
	 * @return Uma colecao de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na invocacao do metodo.
	 * @throws GGASException 
	 * @throws HibernateException 
	 */
	Collection<PontoConsumo> consultarPontosLoteComunicacao(FiltroLoteComunicacaoDTO loteDTO) throws NegocioException, HibernateException, GGASException;
	
	/**
	 * Metodo responsavel por gerar o lote de comunicacao
	 * 
	 * @param listaComunicacaoLote Lista da comunicacao em lote a serem inseridos no lote
	 * @param servicoTipo tipo de serviço gerado
	 * @throws NegocioException Caso ocorra algum erro na invocacao do metodo.
	 * @throws GGASException 
	 */
	void gerarLote(Collection<ComunicacaoLoteVO> listaComunicacaoLote, ServicoTipo servicoTipo , Boolean isGerarAutorizacaoDeServico) throws NegocioException, GGASException;
	

	/**
	 * Metodo responsavel por gerar os relatórios
	 * 
	 * @param listaComunicacaoLoteVO - lista comunicacao em lote
	 * @param ServicoTipo = servico tipo relatorio
	 * @return relatorio
	 * @throws GGASException - exceção lançada caso algum erro
	 */
	List<byte[]> gerarRelatorio(Collection<ComunicacaoLoteVO> listaComunicacaoLoteVO, ServicoTipo servicoTipo) throws GGASException;

	/**
	 * Método para gerar a roterização dos pontos de consumo para a comunicação
	 * 
	 * @param listaPontoConsumo - {@link PontoConsumo}
	 * @return Roteirizacao Comunicacao - {@link ComunicacaoLoteVO}
	 * @throws NegocioException - {@link NegocioException}
	 * @throws IOException 
	 */
	Collection<ComunicacaoLoteVO> roterizacaoComunicacao(Collection<PontoConsumo> listaPontoConsumo)
			throws NegocioException, IOException;

	/**
	 * Processar emissao da comunicacao em lote batch.
	 *
	 * @param logProcessamento the log processamento
	 * @param dados the acao comando
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 */
	void processarEmissaoComunicacaoEmLoteBatch(StringBuilder logProcessamento, Map<String, Object> dados)
			throws IOException, GGASException;
	
	/**
	 * Metodo responsavel por consultar os lotes sem AS's geradas
	 * 
	 * @return Uma colecao de LoteComunicacao.
	 * @throws NegocioException Caso ocorra algum erro na invocacao do metodo.
	 * @throws GGASException 
	 * @throws HibernateException 
	 */
	Collection<LoteComunicacao> consultarLoteComunicacaoNaoGerado() throws NegocioException, HibernateException, GGASException;
	
	/**
	 * Metodo responsavel por consultar itens para gerar AS 
	 * 
	 * @return Uma colecao de LoteComunicacao.
	 * @throws NegocioException Caso ocorra algum erro na invocacao do metodo.
	 * @throws GGASException 
	 * @throws HibernateException 
	 */
	Collection<ItemLoteComunicacao> consultarItensComunicacaoParaGeracao() throws NegocioException, HibernateException, GGASException;

	Long verificaServicoTipoAgendamento(Long chaveServicoTipoAgendamento);

	Collection<LoteComunicacao> consultarLoteComunicacao(FiltroAcompanhamentoLoteDTO filtroDTO)
			throws HibernateException, GGASException;

	void calcularDistanciaPontoConsumo(ComunicacaoLoteVO comunicacaoLoteVO) throws NegocioException, IOException;

	ItemLoteComunicacao obterUltimoItemLoteComunicacao(Integer anoExecucao);

	Collection<PontoConsumo> filtrarPontosParaLoteDeComunicacao(String descricaoPonto, String codigoLegado, String servicoTipo, Integer offset);

	byte[] montarPlanilhaListagemPontosComunicacao(Collection<PontoConsumo> listaPontoConsumo) throws NegocioException;

	Boolean cancelarLote(LoteComunicacao loteComunicacao, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	Collection<ItemLoteComunicacao> obterItensLoteComunicacaoRelatorioSintetico(
			FiltroRelatorioComunicacaoInterrupcaoDTO filtro) throws HibernateException, GGASException;

	byte[] gerarRelatorioSinteticoComunicacaoInterrupcao(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) throws HibernateException, GGASException;

	Collection<ItemLoteComunicacao> consultarItemLoteRelatorioAntecedenciaAviso(
			FiltroRelatorioComunicacaoInterrupcaoDTO filtro);

	byte[] gerarRelatorioAntecedenciaAviso(FiltroRelatorioComunicacaoInterrupcaoDTO filtro) throws NegocioException;

	Collection<ItemLoteComunicacao> consultarItensPendentesAntecedenciaAvisosPorAno(
			FiltroRelatorioComunicacaoInterrupcaoDTO filtro);

	Map<String, List<ServicoTipoAgendamentoTurno>> montarMapaServicoTipoAgendamento(ServicoTipo servicoTipo, EntidadeConteudo[] turno);

	void removerItens(Collection<ItemLoteComunicacao> itensRemover, Boolean removerLote) throws NegocioException, ConcorrenciaException;

	void removerProtocolo(ItemLoteComunicacao itemLote) throws NegocioException, ConcorrenciaException;

	List<byte[]> gerarLoteCancelamento(Collection<ItemLoteComunicacao> itens, String dataAtual) throws IOException, GGASException;

	Long consultarEntregaDocumento(Long chavePrimaria);

	Collection<LoteComunicacao> consultarLoteComunicacaoPorChavesPrimarias(Long[] chavesPrimarias);

	byte[] gerarRelatorioAcompanhamentoComunicacao(Collection<LoteComunicacao> lotesComunicacao, Boolean tipoRelatorio, Boolean tipoImpressao) throws NegocioException;
}
