package br.com.ggas.atendimento.comunicacao.dominio;

public class RelatorioComunicacaoVO {

	private String nome;
	private String endereco;
	private String sequencial;
	private String dataExecucao;
	private String horaExecucao;
	private String tempoExecucao;
	private String dataInicioExecucao;
	private String dataFimExecucao;
	private String dataExecucaoAnterior;
	private String sequencialAnterior;
	private String tempoExecucaoMinutos;
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getSequencial() {
		return sequencial;
	}
	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}
	public String getDataExecucao() {
		return dataExecucao;
	}
	public void setDataExecucao(String dataExecucao) {
		this.dataExecucao = dataExecucao;
	}
	public String getHoraExecucao() {
		return horaExecucao;
	}
	public void setHoraExecucao(String horaExecucao) {
		this.horaExecucao = horaExecucao;
	}
	public String getTempoExecucao() {
		return tempoExecucao;
	}
	public void setTempoExecucao(String tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}
	public String getDataInicioExecucao() {
		return dataInicioExecucao;
	}
	public void setDataInicioExecucao(String dataInicioExecucao) {
		this.dataInicioExecucao = dataInicioExecucao;
	}
	public String getDataFimExecucao() {
		return dataFimExecucao;
	}
	public void setDataFimExecucao(String dataFimExecucao) {
		this.dataFimExecucao = dataFimExecucao;
	}
	public String getDataExecucaoAnterior() {
		return dataExecucaoAnterior;
	}
	public void setDataExecucaoAnterior(String dataExecucaoAnterior) {
		this.dataExecucaoAnterior = dataExecucaoAnterior;
	}
	public String getSequencialAnterior() {
		return sequencialAnterior;
	}
	public void setSequencialAnterior(String sequencialAnterior) {
		this.sequencialAnterior = sequencialAnterior;
	}
	public String getTempoExecucaoMinutos() {
		return tempoExecucaoMinutos;
	}
	public void setTempoExecucaoMinutos(String tempoExecucaoMinutos) {
		this.tempoExecucaoMinutos = tempoExecucaoMinutos;
	}
	
}

