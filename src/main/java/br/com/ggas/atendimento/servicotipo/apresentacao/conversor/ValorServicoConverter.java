/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.atendimento.servicotipo.apresentacao.conversor;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável por implementar a conversão de uma String em um BigDecimal
 */
public class ValorServicoConverter implements Converter<String, BigDecimal> {

	private static final Logger LOG = Logger.getLogger(ValorServicoConverter.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public BigDecimal convert(String valor) {

		BigDecimal valorServico = null;
		
		if (valor != null && !"".equals(valor) && Long.parseLong(valor) > 0) {
			try {
				valorServico = Util.converterCampoStringParaValorBigDecimal("Valor", valor, Constantes.FORMATO_VALOR_NUMERO,
						Constantes.LOCALE_PADRAO);
			} catch (FormatoInvalidoException e) {
				LOG.error(e.getMessage(), e);
			}
		}

		return valorServico;
	}
}
