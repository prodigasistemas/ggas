/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *24/09/2013
 * vpessoa
 * 18:32:43
 */

package br.com.ggas.atendimento.servicotipo.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.material.dominio.Material;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.relatorio.ControladorRelatorio;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.StringUtil;
import br.com.ggas.util.Util;

/**
 * Action responsável pelas telas relacionadas aos tipos de Serviço
 * 
 * @author vpessoa
 */
@Controller
public class ServicoTipoAction extends GenericAction {

	private static final String LISTA_MATERIAIS = "listaMateriais";

	private static final String TELA_FORWARD_PESQUISAR_SERVICO_TIPO = "forward:/pesquisarServicoTipo";

	private static final String TELA_GRID_EQUIPAMENTOS = "gridEquipamentos";

	private static final String TELA_AJAX_ERRO = "ajaxErro";

	private static final String LISTA_EQUIPAMENTOS = "listaEquipamentos";

	private static final String LISTA_AGENDAMENTOS = "listaAgendamentos";

	private static final String SERVICO_TIPO = "servicoTipo";
	
	private static final String LISTA_EQUIPE = "listaEquipes";
	
	private static final String LISTA_MEDIDOR_MOTIVO_OPERACAO = "listaMedidorMotivoOperacao";


	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorMedidor")
	private ControladorMedidor controladorMedidor;
	
	@Autowired
	@Qualifier("controladorDocumentoImpressaoLayout")
	private ControladorDocumentoImpressaoLayout controladorDocumentoImpressaoLayout;
	
	@Autowired
	private ControladorQuestionario controladorQuestionario;
	
	@Autowired
	private ControladorEquipe controladorEquipe;
	
	@Autowired
	private ControladorModeloContrato controladorModeloContrato;
	
	@Autowired
	private ControladorRelatorio controladorRelatorio;

	private static final Logger LOG = Logger.getLogger(ServicoTipoAction.class);

	/**
	 * Exibir pesquisa servico tipo.
	 * 
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirPesquisaServicoTipo")
	public ModelAndView exibirPesquisaServicoTipo(HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaServicoTipo");
		ServicoTipo servicoTipo = new ServicoTipo();
		servicoTipo.setHabilitado(Boolean.TRUE);
		model.addObject(SERVICO_TIPO, servicoTipo);
		populaCombos(model);
		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TELA_ATUALIZACAO);
		model.addObject("listaTelaAtualizacao",
						controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));

		carregarDescricaoTelaAtualizacaoPontoConsumo(model);
		removeDadosSessao(session);
		return model;
	}

	/**
	 * Carregar descricao tela atualizacao ponto consumo.
	 * 
	 * @param model
	 *            the model
	 */
	private void carregarDescricaoTelaAtualizacaoPontoConsumo(ModelAndView model) {

		ConstanteSistema constanteTelaAtualizacao = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO);
		EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(constanteTelaAtualizacao
						.getValor()));
		model.addObject("descricaoTelaAtualizacaoPontoConsumo", entidadeConteudo.getDescricao());
	}

	/**
	 * Método responsável por realizar a pesquisa de Tipos de Serviço de acordo com os parâmetros informados na tela de pesquisa.
	 * 
	 * @param descricao the descricao
	 * @param servicoTipoPrioridade the servico tipo prioridade
	 * @param layoutdocumentoimpressao the layoutdocumentoimpressao
	 * @param quantidadeTempoMedio the quantidade tempo medio
	 * @param quantidadePrazoExecucao the quantidade prazo execucao
	 * @param indicadorEquipamento the indicador equipamento
	 * @param habilitado the habilitado
	 * @param indicadorServicoRegulamento the indicador servico regulamento
	 * @param indicadorAgendamento the indicador agendamento
	 * @param indicadorVeiculo the indicador veiculo
	 * @param indicadorEncerramentoAuto the indicadorEncerramentoAuto
	 * @param indicadorPesquisaSatisfacao the indicador pesquisa satisfacao
	 * @param indicadorGeraLote the indicador gera lote
	 * @param indicadorAtualizacaoCadastral the indicador atualizacao cadastral
	 * @param telaAtualizacao the tela atualizacao
	 * @param indicadorOperacaoMedidor the indicador operacao medidor
	 * @param indicadorTipoAssociacaoPontoConsumo the indicador tipo associacao ponto consumo
	 * @param indicadorClienteObrigatorio the indicador cliente obrigatorio
	 * @param indicadorImovelObrigatorio the indicador imovel obrigatorio
	 * @param indicadorContratoObrigatorio the indicador contrato obrigatorio
	 * @param indicadorPontoConsumoObrigatorio the indicador ponto consumo obrigatorio
	 * @param tamanhoPagina the tamanho da pagina
	 * @param orientacao the orientacao
	 * @param numeroMaximoExecucacao  quantidade de execuções permitidas para o tipo de serviço
	 * @param prazoGarantia prazo de garantia em dias para execução do tipo de serviço
	 * @return Uma lista com os Tipos de serviço localizados para os filtros ou uma mensagem de exceção caso não retorne resultados
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("pesquisarServicoTipo")
	public ModelAndView pesquisarServicoTipo(@RequestParam(value = "descricao", required = false) String descricao,
			@RequestParam(value = "servicoTipoPrioridade", required = false) String servicoTipoPrioridade,
			@RequestParam(value = "documentoImpressaoLayout", required = false) String layoutdocumentoimpressao,
			@RequestParam(value = "quantidadeTempoMedio", required = false) String quantidadeTempoMedio,
			@RequestParam(value = "quantidadePrazoExecucao", required = false) String quantidadePrazoExecucao,
			@RequestParam(value = "indicadorEquipamento", required = false) String indicadorEquipamento,
			@RequestParam(value = "habilitado", required = false) String habilitado,
			@RequestParam(value = "indicadorServicoRegulamento", required = false) String indicadorServicoRegulamento,
			@RequestParam(value = "indicadorAgendamento", required = false) String indicadorAgendamento,
			@RequestParam(value = "indicadorVeiculo", required = false) String indicadorVeiculo,
			@RequestParam(value = "indicadorEncerramentoAuto", required = false) String indicadorEncerramentoAuto,
			@RequestParam(value = "indicadorPesquisaSatisfacao", required = false) String indicadorPesquisaSatisfacao,
			@RequestParam(value = "indicadorGeraLote", required = false) String indicadorGeraLote,
			@RequestParam(value = "indicadorAtualizacaoCadastral", required = false) String indicadorAtualizacaoCadastral,
			@RequestParam(value = "telaAtualizacao", required = false) String telaAtualizacao,
			@RequestParam(value = "indicadorOperacaoMedidor", required = false) String indicadorOperacaoMedidor,
			@RequestParam(value = "indicadorTipoAssociacaoPontoConsumo", required = false) String indicadorTipoAssociacaoPontoConsumo,
			@RequestParam(value = "indicadorClienteObrigatorio", required = false) String indicadorClienteObrigatorio,
			@RequestParam(value = "indicadorImovelObrigatorio", required = false) String indicadorImovelObrigatorio,
			@RequestParam(value = "indicadorContratoObrigatorio", required = false) String indicadorContratoObrigatorio,
			@RequestParam(value = "indicadorPontoConsumoObrigatorio", required = false) String indicadorPontoConsumoObrigatorio,
			@RequestParam(value = "orientacao", required = false) String orientacao,
			@RequestParam(value = "tamanhoPagina", required = false) Integer tamanhoPagina,
			@RequestParam(value = "numeroMaximoExecucacao", required = false) String numeroMaximoExecucacao,
			@RequestParam(value = "prazoGarantia", required = false) String prazoGarantia,
			@RequestParam(value = "indicadorNovoMedidor", required = false) String indicadorNovoMedidor,
			@RequestParam(value = "indicadorServicoMobile", required = false) String indicadorServicoMobile,
			@RequestParam(value = "indicadorAssinatura", required = false) String indicadorAssinatura)
	
			throws GGASException {

		ServicoTipo servicoTipo = new ServicoTipo();
		popularServicoTipoPesquisa(descricao, servicoTipoPrioridade, layoutdocumentoimpressao, quantidadeTempoMedio,
				quantidadePrazoExecucao, indicadorEquipamento, habilitado, indicadorServicoRegulamento, indicadorAgendamento,
				indicadorVeiculo, indicadorEncerramentoAuto, indicadorPesquisaSatisfacao, indicadorGeraLote, indicadorAtualizacaoCadastral,
				telaAtualizacao, indicadorOperacaoMedidor, indicadorTipoAssociacaoPontoConsumo, indicadorClienteObrigatorio,
				indicadorImovelObrigatorio, indicadorContratoObrigatorio, indicadorPontoConsumoObrigatorio, orientacao,
				numeroMaximoExecucacao, prazoGarantia, indicadorNovoMedidor, servicoTipo, indicadorServicoMobile, indicadorAssinatura);

		Boolean flagHabilitado = null;
		if (habilitado != null && !habilitado.isEmpty()) {
			flagHabilitado = Boolean.valueOf(habilitado);
		}
		ModelAndView model = new ModelAndView("exibirPesquisaServicoTipo");
		carregaCombos(model);

		int tamanhoPaginacao = Constantes.PAGINACAO_PADRAO;

		if (tamanhoPagina != null) {
			tamanhoPaginacao = tamanhoPagina;
		}

		model.addObject("tamanhoPagina", tamanhoPaginacao);

		try {
			model.addObject("listaServicoTipo", controladorServicoTipo.consultarServicoTipo(servicoTipo, flagHabilitado));
			model.addObject(SERVICO_TIPO, servicoTipo);

		} catch (GGASException e) {
			model.addObject(SERVICO_TIPO, servicoTipo);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Método responsável por buscar o Tipo de serviço atraves da sua chavePrimaria e popular a tela de detalhamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoServicoTipo")
	public ModelAndView exibirDetalhamentoServicoTipo(@RequestParam("chavePrimaria") Long chavePrimaria) throws GGASException {
		
		ModelAndView model = new ModelAndView("exibirDetalhamentoServicoTipo");
		ServicoTipo servicoTipo = controladorServicoTipo.obterServicoTipo(chavePrimaria);
		model.addObject(SERVICO_TIPO, servicoTipo);
		carregaListas(model, servicoTipo);
		model.addObject("listaOperacaoMedidor", controladorMedidor.listarOperacaoMedidor());
		model.addObject(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());

		return model;
	}

	/**
	 * Método responsável por popular a tela de inclusão de Tipo de Serviço.
	 * 
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirInclusaoServicoTipo")
	public ModelAndView exibirInclusaoServicoTipo(HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("exibirInclusaoServicoTipo");
		ServicoTipo servicoTipo = new ServicoTipo();
		servicoTipo.setIndicadorEquipamento(Boolean.FALSE);
		servicoTipo.setIndicadorServicoRegulamento(Boolean.FALSE);
		servicoTipo.setIndicadorAgendamento(Boolean.TRUE);
		servicoTipo.setIndicadorVeiculo(Boolean.FALSE);
		servicoTipo.setIndicadorEncerramentoAuto(Boolean.TRUE);
		servicoTipo.setIndicadorPesquisaSatisfacao(Boolean.TRUE);
		servicoTipo.setIndicadorGeraLote(Boolean.FALSE);
		servicoTipo.setIndicadorAtualizacaoCadastral(Constantes.ATUALIZACAO_CADASTRAL_NAO);
		servicoTipo.setIndicadorClienteObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorImovelObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorContratoObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorPontoConsumoObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorCobranca(Boolean.FALSE);
		servicoTipo.setIndicadorEmailCadastrar(Boolean.FALSE);
		servicoTipo.setIndicadorEmailEmExecucao(Boolean.FALSE);
		servicoTipo.setIndicadorEmailExecucao(Boolean.FALSE);
		servicoTipo.setIndicadorEmailEncerrar(Boolean.FALSE);
		servicoTipo.setIndicadorEmailAlterar(Boolean.FALSE);
		servicoTipo.setIndicadorEmailAgendar(Boolean.FALSE);
		servicoTipo.setIndicadorFormularioObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorAviso(Boolean.FALSE);
		servicoTipo.setIndicadorIntegracao(Boolean.FALSE);
		servicoTipo.setIndicadorProtocolo(Boolean.FALSE);
		servicoTipo.setIndicadorProtocoloObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorNotificacao(Boolean.FALSE);
		servicoTipo.setIndicadorTipoProtocolo(Boolean.FALSE);
		servicoTipo.setIndicadorMetadeTempoExecucao(Boolean.FALSE);
		servicoTipo.setIndicadorNovoMedidor(Boolean.FALSE);
		servicoTipo.setIndicadorServicoMobile(Boolean.FALSE);
		

		model.addObject(SERVICO_TIPO, servicoTipo);
		carregaCombos(model);
		adicionarParametroCaixaAlta(session);

		return model;
	}

	/**
	 * Método responsável por popular a tela de alteração de Tipo de Serviço.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoServicoTipo")
	public ModelAndView exibirAlteracaoServicoTipo(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoServicoTipo");
		model.addObject("fluxoAlteracao", true);
		ServicoTipo servicoTipo = controladorServicoTipo.obterServicoTipo(chavePrimaria);
		model.addObject(SERVICO_TIPO, servicoTipo);
		session.setAttribute(LISTA_MATERIAIS, servicoTipo.getListaMateriais());
		session.setAttribute(LISTA_AGENDAMENTOS, servicoTipo.getListaAgendamentosTurno());
		session.setAttribute(LISTA_EQUIPAMENTOS, servicoTipo.getListaEquipamentoEspecial());
		
		ConstanteSistema constanteTipoRelatorio = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENTIDADE_CONTEUDO_TIPO_RELATORIO_EMAIL_AS);
		Long tipoRelatorio = constanteTipoRelatorio != null ? constanteTipoRelatorio.getValorLong() : 0l;
		

		Collection<RelatorioToken> tokens = controladorRelatorio.obterListaRelatorioTokens(tipoRelatorio);
		
		model.addObject("listaTokens", tokens);
		
		carregaCombos(model);
		carregaListas(model, servicoTipo);

		boolean existeQuestionarioAssociado = controladorServicoTipo.existeQuestionarioAssociadoServicoTipo(servicoTipo);
		model.addObject("existeQuestionarioAssociado", existeQuestionarioAssociado);

		if(servicoTipo.getIndicadorOperacaoMedidor() == null || servicoTipo.getIndicadorOperacaoMedidor().isEmpty()) {
			servicoTipo.setIndicadorOperacaoMedidor("-1");
		}

		return model;
	}

	/**
	 * Método responsável por incluir um material na lista de materiais necessários.
	 * Método utilizado tanto na alteração como inclusão e um novo material.
	 * Chamado na tela gridMateriais.jsp
	 * 
	 * @param chaveMaterial
	 *            the chave material
	 * @param quantidadeMaterial
	 *            the quantidade material
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param indexList
	 *            the index list
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirMaterialServicoTipo")
	public ModelAndView incluirMaterialServicoTipo(@RequestParam("material") Long chaveMaterial,
					@RequestParam("quantidadeMaterial") String quantidadeMaterial, @RequestParam("chavePrimaria") Long chavePrimaria,
					@RequestParam("indexList") Integer indexList, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridMateriais");

		ServicoTipoMaterial servicoTipoMaterial = new ServicoTipoMaterial();
		Material material = (Material) controladorServicoTipo.obterMaterial(chaveMaterial);
		servicoTipoMaterial.setMaterial(material);
		servicoTipoMaterial.setQuantidadeMaterial(Long.parseLong(quantidadeMaterial));
		Collection<ServicoTipoMaterial> materiais = new ArrayList<>();
		if(session.getAttribute(LISTA_MATERIAIS) != null) {
			materiais.addAll((Collection<ServicoTipoMaterial>) session.getAttribute(LISTA_MATERIAIS));
			session.setAttribute(LISTA_MATERIAIS, materiais);
		} else {
			if(session.getAttribute(LISTA_MATERIAIS) == null) {
				session.setAttribute(LISTA_MATERIAIS, materiais);
			}
		}

		// caso seja inclusão de um novo material, o indexList não é informado
		if(indexList == null || indexList < 0) {
			if(!validarMaterialExistente(materiais, servicoTipoMaterial)) {
				((Collection<ServicoTipoMaterial>) session.getAttribute(LISTA_MATERIAIS)).add(servicoTipoMaterial);
			} else {
				model = new ModelAndView(TELA_AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_SERVICO_MATERIAL_EXISTENTE);
			}
		} else {
			this.alterarMaterial(indexList, servicoTipoMaterial, session);
		}

		carregaCombos(model);

		return model;
	}

	/**
	 * Método responsável por remover um material da lista de materiais necessários.
	 * Chamado na tela gridMateriais.jsp
	 * 
	 * @param chavePrimariaMaterial
	 *            the chave primaria material
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerMaterialServicoTipo")
	public ModelAndView removerMaterialServicoTipo(@RequestParam("chavePrimariaMaterial") Long chavePrimariaMaterial, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView("gridMateriais");
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoMaterial> listaMateriais = (Collection<ServicoTipoMaterial>) session.getAttribute(LISTA_MATERIAIS);
		for (ServicoTipoMaterial material : listaMateriais) {
			if(material.getMaterial().getChavePrimaria() == chavePrimariaMaterial) {
				listaMateriais.remove(material);
				break;
			}
		}

		carregaCombos(model);
		return model;
	}

	/**
	 * Método responsável por incluir um novo equipamento a lista de equipamentos especiais necessários.
	 * Chamado na tela gridEquipamentos.jsp
	 * 
	 * @param chaveEquipamento
	 *            the chave equipamento
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirEquipamento")
	public ModelAndView incluirEquipamento(@RequestParam("equipamento") Long chaveEquipamento,
					@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_EQUIPAMENTOS);
		ServicoTipoEquipamento servicoTipoEquipamento = new ServicoTipoEquipamento();

		EntidadeConteudo equipamento = controladorEntidadeConteudo.obter(chaveEquipamento);
		servicoTipoEquipamento.setEquipamento(equipamento);

		Collection<ServicoTipoEquipamento> equipamentos = new ArrayList<>();
		if(session.getAttribute(LISTA_EQUIPAMENTOS) != null) {
			equipamentos.addAll((Collection<ServicoTipoEquipamento>) session.getAttribute(LISTA_EQUIPAMENTOS));
			session.setAttribute(LISTA_EQUIPAMENTOS, equipamentos);
		} else {
			if(session.getAttribute(LISTA_EQUIPAMENTOS) == null) {
				session.setAttribute(LISTA_EQUIPAMENTOS, equipamentos);
			}
		}

		if(!validarEquipamentoExistente(equipamentos, servicoTipoEquipamento)) {
			((Collection<ServicoTipoEquipamento>) session.getAttribute(LISTA_EQUIPAMENTOS)).add(servicoTipoEquipamento);
		} else {
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_SERVICO_EQUIPAMENTO_EXISTENTE);
		}

		carregaCombos(model);
		return model;
	}

	/**
	 * Alterar material.
	 * 
	 * @param indexLista
	 *            the index lista
	 * @param material
	 *            the material
	 * @param session
	 *            the session
	 */
	@SuppressWarnings("unchecked")
	private void alterarMaterial(Integer indexLista, ServicoTipoMaterial material, HttpSession session) {

		Collection<ServicoTipoMaterial> listaMateriais = new ArrayList<>();
		listaMateriais.addAll((Collection<ServicoTipoMaterial>) session.getAttribute(LISTA_MATERIAIS));
	
		if (CollectionUtils.isNotEmpty(listaMateriais)) {

			for (int i = 0; i < listaMateriais.size(); i++) {
				ServicoTipoMaterial materialExistente = ((ArrayList<ServicoTipoMaterial>) listaMateriais).get(i);
				if(i == indexLista && materialExistente != null) {
					materialExistente.setQuantidadeMaterial(material.getQuantidadeMaterial());
					materialExistente.setMaterial(material.getMaterial());
					break;
				}
			}

		}

		session.setAttribute(LISTA_MATERIAIS, listaMateriais);

	}

	/**
	 * Remover equipamento.
	 * 
	 * @param chavePrimariaEquipamento
	 *            the chave primaria equipamento
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerEquipamento")
	public ModelAndView removerEquipamento(@RequestParam("chavePrimariaEquipamento") Long chavePrimariaEquipamento, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_EQUIPAMENTOS);
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoEquipamento> listaEquipamentos = (Collection<ServicoTipoEquipamento>) session
						.getAttribute(LISTA_EQUIPAMENTOS);
		for (ServicoTipoEquipamento equipamento : listaEquipamentos) {
			if(equipamento.getEquipamento().getChavePrimaria() == chavePrimariaEquipamento) {
				listaEquipamentos.remove(equipamento);
				break;
			}
		}
		carregaCombos(model);
		return model;
	}

	/**
	 * Remover todos equipamento.
	 * 
	 * @param chavePrimariaEquipamento
	 *            the chave primaria equipamento
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerTodosEquipamento")
	public ModelAndView removerTodosEquipamento(@RequestParam("chavePrimariaEquipamento") Long chavePrimariaEquipamento, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_EQUIPAMENTOS);
		Collection<ServicoTipoEquipamento> listaEquipamentos = (Collection<ServicoTipoEquipamento>) session
						.getAttribute(LISTA_EQUIPAMENTOS);

		if(listaEquipamentos != null) {

			listaEquipamentos.clear();

		}

		carregaCombos(model);
		return model;
	}

	/**
	 * Incluir turno.
	 * 
	 * @param chaveTurno
	 *            the chave turno
	 * @param quantidadeAgendamentosTurno
	 *            the quantidade agendamentos turno
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param indexList
	 *            the index list
	 * @param session
	 *            the session
	 * 
	 * @param chaveEquipe
	 * 			the chave Equipe
	 *            
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("incluirTurno")
	public ModelAndView incluirTurno(@RequestParam("equipe") Long chaveEquipe, @RequestParam("turno") Long chaveTurno,
					@RequestParam(value = "quantidadeAgendamentosTurno") String quantidadeAgendamentosTurno,
					@RequestParam("chavePrimaria") Long chavePrimaria, @RequestParam("indexList") Integer indexList, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView("gridAgendamentos");

		ServicoTipoAgendamentoTurno servicoTipoTurno = new ServicoTipoAgendamentoTurno();

		EntidadeConteudo turno = controladorEntidadeConteudo.obter(chaveTurno);
		Equipe equipe = controladorEquipe.obterEquipe(chaveEquipe);
		
		servicoTipoTurno.setTurno(turno);
		servicoTipoTurno.setQuantidadeAgendamentosTurno(Long.parseLong(quantidadeAgendamentosTurno));
		servicoTipoTurno.setEquipe(formatarEquipe(equipe));

		Collection<ServicoTipoAgendamentoTurno> agendamentos = new ArrayList<>();
		if(session.getAttribute(LISTA_AGENDAMENTOS) != null) {
			agendamentos.addAll((Collection<ServicoTipoAgendamentoTurno>) session.getAttribute(LISTA_AGENDAMENTOS));
			session.setAttribute(LISTA_AGENDAMENTOS, agendamentos);
		} else {
			if(session.getAttribute(LISTA_AGENDAMENTOS) == null) {
				session.setAttribute(LISTA_AGENDAMENTOS, agendamentos);
			}
		}

		if(indexList == null || indexList < 0) {
			if(!validarTurnoExistente(agendamentos, servicoTipoTurno)) {
				((Collection<ServicoTipoAgendamentoTurno>) session.getAttribute(LISTA_AGENDAMENTOS)).add(servicoTipoTurno);
			} else {
				model = new ModelAndView(TELA_AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_SERVICO_TURNO_EXISTENTE);
			}
		} else {
			this.alterarTurno(indexList, servicoTipoTurno, session);
		}

		carregaCombos(model);
		return model;
	}

	
	/**
	 * Alterar turno.
	 * 
	 * @param indexLista
	 *            the index lista
	 * @param servicoTipoTurno
	 *            the servico tipo turno
	 * @param session
	 *            the session
	 */
	@SuppressWarnings("unchecked")
	private void alterarTurno(Integer indexLista, ServicoTipoAgendamentoTurno servicoTipoTurno, HttpSession session) {

		Collection<ServicoTipoAgendamentoTurno> listaAgendamentos = new ArrayList<>();
		listaAgendamentos.addAll((Collection<ServicoTipoAgendamentoTurno>) session.getAttribute(LISTA_AGENDAMENTOS));
		
		if(CollectionUtils.isNotEmpty(listaAgendamentos) && indexLista != null) {

			for (int i = 0; i < listaAgendamentos.size(); i++) {
				ServicoTipoAgendamentoTurno turno = ((ArrayList<ServicoTipoAgendamentoTurno>) listaAgendamentos).get(i);
				if(i == indexLista && turno != null) {
					turno.setQuantidadeAgendamentosTurno(servicoTipoTurno.getQuantidadeAgendamentosTurno());
					turno.setTurno(servicoTipoTurno.getTurno());
					turno.setEquipe(servicoTipoTurno.getEquipe());
					break;
				}
			}

		}
		session.setAttribute(LISTA_AGENDAMENTOS, listaAgendamentos);

	}

	/**
	 * Remover turno.
	 * 
	 * @param chavePrimariaTurno
	 *            the chave primaria turno
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerTurno")
	public ModelAndView removerTurno(@RequestParam("chavePrimariaTurno") Long chavePrimariaTurno, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridAgendamentos");
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoAgendamentoTurno> listaAgendamentos = (Collection<ServicoTipoAgendamentoTurno>) session
						.getAttribute(LISTA_AGENDAMENTOS);
		for (ServicoTipoAgendamentoTurno agendamentoTurno : listaAgendamentos) {
			if(agendamentoTurno.getTurno().getChavePrimaria() == chavePrimariaTurno) {
				listaAgendamentos.remove(agendamentoTurno);
				break;
			}
		}
		carregaCombos(model);
		return model;
	}

	/**
	 * Alterar servico tipo.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("alterarServicoTipo")
	public ModelAndView alterarServicoTipo(@ModelAttribute("ServicoTipo") ServicoTipo servicoTipo, HttpSession session)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_SERVICO_TIPO);

		try {

			@SuppressWarnings("unchecked")
			Collection<ServicoTipoMaterial> listaMateriais = (Collection<ServicoTipoMaterial>) session.getAttribute(LISTA_MATERIAIS);
			@SuppressWarnings("unchecked")
			Collection<ServicoTipoEquipamento> listaEquipamentos = (Collection<ServicoTipoEquipamento>) session
							.getAttribute(LISTA_EQUIPAMENTOS);
			@SuppressWarnings("unchecked")
			Collection<ServicoTipoAgendamentoTurno> listaAgendamentos = (Collection<ServicoTipoAgendamentoTurno>) session
							.getAttribute(LISTA_AGENDAMENTOS);
			
			controladorServicoTipo.atualizarServicoTipo(servicoTipo, listaMateriais, listaEquipamentos, listaAgendamentos);
			removeDadosSessao(session);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GenericAction.obterMensagem(Constantes.SERVICO_TIPO));
		} catch(GGASException e) {
			model = new ModelAndView("exibirAlteracaoServicoTipo");
			carregaCombos(model);
			model.addObject(SERVICO_TIPO, servicoTipo);
			model.addObject("fluxoAlteracao", true);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Incluir servico tipo.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @param session
	 *            the session
	 * @param request the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("incluirServicoTipo")
	public ModelAndView incluirServicoTipo(@ModelAttribute("ServicoTipo") ServicoTipo servicoTipo, HttpSession session,
			HttpServletRequest request)
			throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_SERVICO_TIPO);
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoMaterial> listaMateriais = (Collection<ServicoTipoMaterial>) session.getAttribute(LISTA_MATERIAIS);
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoEquipamento> listaEquipamentos = (Collection<ServicoTipoEquipamento>) session
				.getAttribute(LISTA_EQUIPAMENTOS);
		@SuppressWarnings("unchecked")
		Collection<ServicoTipoAgendamentoTurno> listaAgendamentos = (Collection<ServicoTipoAgendamentoTurno>) session
				.getAttribute(LISTA_AGENDAMENTOS);
		try {
			servicoTipo.setDadosAuditoria(getDadosAuditoria(request));
			ServicoTipo servicoTipoNovo = controladorServicoTipo.inserir(servicoTipo, listaMateriais, listaEquipamentos, listaAgendamentos);
			removeDadosSessao(session);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, GenericAction.obterMensagem(Constantes.SERVICO_TIPO));
			model.addObject("listaServicoTipo", controladorServicoTipo.consultarServicoTipo(servicoTipoNovo.getChavePrimaria()));
			model.addObject(SERVICO_TIPO, new ServicoTipo());
		} catch(GGASException e) {
			model = new ModelAndView("exibirInclusaoServicoTipo");
			carregaCombos(model);
			model.addObject(SERVICO_TIPO, servicoTipo);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Remover servico tipo.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerServicoTipo")
	public ModelAndView removerServicoTipo(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_SERVICO_TIPO);
		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorServicoTipo.removerServicoTipo(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, GenericAction.obterMensagem(Constantes.SERVICO_TIPO));
		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_RELACIONADA, true);
			} catch(NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		}

		return model;
	}

	/**
	 * Popular servico tipo pesquisa.
	 * 
	 * @param descricao the descricao
	 * @param prioridade the prioridade
	 * @param layoutdocumentoimpressao the layoutdocumentoimpressao
	 * @param quantidadeTempoMedio the quantidade tempo medio
	 * @param quantidadePrazoExecucao the quantidade prazo execucao
	 * @param indicadorEquipamento the indicador equipamento
	 * @param habilitado the habilitado
	 * @param indicadorServicoRegulamento the indicador servico regulamento
	 * @param indicadorAgendamento the indicador agendamento
	 * @param indicadorVeiculo the indicador veiculo
	 * @param indicadorEncerramentoAuto the indicador encerramento automático
	 * @param indicadorPesquisaSatisfacao the indicador pesquisa satisfacao
	 * @param indicadorGeraLote the indicador gera lote
	 * @param indicadorAtualizacaoCadastral the indicador atualizacao cadastral
	 * @param telaAtualizacao the tela atualizacao
	 * @param operacaoMedidorIndicador the indicador operacao medidor
	 * @param tipoAssociacaoPontoConsumoIndicador the indicador tipo associacao ponto consumo
	 * @param indicadorClienteObrigatorio the indicador cliente obrigatorio
	 * @param indicadorImovelObrigatorio the indicador imovel obrigatorio
	 * @param indicadorContratoObrigatorio the indicador contrato obrigatorio
	 * @param indicadorPontoConsumoObrigatorio the indicador ponto consumo obrigatorio
	 * @param numeroMaximoExecucao the indicador número máximo de execuções
	 * @param prazoGarantia the indicador prazo de garantia em dias
	 * @param servicoTipo the servico tipo
	 * @throws NumberFormatException the number format exception
	 * @throws NegocioException the negocio exception
	 */
	private void popularServicoTipoPesquisa(String descricao, String prioridade, String layoutdocumentoimpressao,
			String quantidadeTempoMedio, String quantidadePrazoExecucao, String indicadorEquipamento, String habilitado,
			String indicadorServicoRegulamento, String indicadorAgendamento, String indicadorVeiculo, String indicadorEncerramentoAuto,
			String indicadorPesquisaSatisfacao, String indicadorGeraLote, String indicadorAtualizacaoCadastral, String telaAtualizacao,
			String operacaoMedidorIndicador, String tipoAssociacaoPontoConsumoIndicador, String indicadorClienteObrigatorio,
			String indicadorImovelObrigatorio, String indicadorContratoObrigatorio, String indicadorPontoConsumoObrigatorio,
			String orientacao, String numeroMaximoExecucao, String prazoGarantia, String indicadorNovoMedidor, 
			ServicoTipo servicoTipo,
			String indicadorServicoMobile,
			String indicadorAssinatura) throws NegocioException {

		String indicadorOperacaoMedidor = operacaoMedidorIndicador;
		String indicadorTipoAssociacaoPontoConsumo = tipoAssociacaoPontoConsumoIndicador;
		ServicoTipoPrioridade servicoTipoPrioridade = null;
		DocumentoImpressaoLayout documentoImpressao = null;
		if (prioridade != null && !"-1".equals(prioridade)) {
			servicoTipoPrioridade = new ServicoTipoPrioridade(Long.valueOf(prioridade));
			servicoTipo.setServicoTipoPrioridade(servicoTipoPrioridade);
		}

		if (layoutdocumentoimpressao != null && !"-1".equals(layoutdocumentoimpressao)) {
			documentoImpressao = new DocumentoImpressaoLayout(Long.valueOf(layoutdocumentoimpressao));
			servicoTipo.setDocumentoImpressaoLayout(documentoImpressao);
		}
		if (descricao != null && !descricao.isEmpty()) {
			servicoTipo.setDescricao(Util.rtrim(Util.ltrim(descricao)));
		}
		if (servicoTipoPrioridade != null) {
			servicoTipo.setServicoTipoPrioridade(servicoTipoPrioridade);
		}
		if (quantidadeTempoMedio != null && !quantidadeTempoMedio.isEmpty()) {
			servicoTipo.setQuantidadeTempoMedio(Long.valueOf(quantidadeTempoMedio));
		}
		if (quantidadePrazoExecucao != null && !quantidadePrazoExecucao.isEmpty()) {
			servicoTipo.setQuantidadePrazoExecucao(Long.valueOf(quantidadePrazoExecucao));
		}
		if (indicadorEquipamento != null && !indicadorEquipamento.isEmpty()) {
			servicoTipo.setIndicadorEquipamento(Boolean.valueOf(indicadorEquipamento));
		}
		if (habilitado != null && !habilitado.isEmpty()) {
			servicoTipo.setHabilitado(Boolean.valueOf(habilitado));
		}
		if (indicadorServicoRegulamento != null && !indicadorServicoRegulamento.isEmpty()) {
			servicoTipo.setIndicadorServicoRegulamento(Boolean.valueOf(indicadorServicoRegulamento));
		}

		if (indicadorAgendamento != null && !indicadorAgendamento.isEmpty()) {
			servicoTipo.setIndicadorAgendamento(Boolean.valueOf(indicadorAgendamento));
		}

		if (indicadorVeiculo != null && !indicadorVeiculo.isEmpty()) {
			servicoTipo.setIndicadorVeiculo(Boolean.valueOf(indicadorVeiculo));
		}

		if (indicadorEncerramentoAuto != null && !indicadorEncerramentoAuto.isEmpty()) {
			servicoTipo.setIndicadorEncerramentoAuto(Boolean.valueOf(indicadorEncerramentoAuto));
		}

		if (indicadorPesquisaSatisfacao != null && !indicadorPesquisaSatisfacao.isEmpty()) {
			servicoTipo.setIndicadorPesquisaSatisfacao(Boolean.valueOf(indicadorPesquisaSatisfacao));
		}
		if (indicadorGeraLote != null && !indicadorGeraLote.isEmpty()) {
			servicoTipo.setIndicadorGeraLote(Boolean.valueOf(indicadorGeraLote));
		}
		if (indicadorAtualizacaoCadastral != null && !indicadorAtualizacaoCadastral.isEmpty()
				&& !"-1".equals(indicadorAtualizacaoCadastral)) {
			servicoTipo.setIndicadorAtualizacaoCadastral(indicadorAtualizacaoCadastral);
		}
		if (telaAtualizacao != null && !"-1".equals(telaAtualizacao)) {
			EntidadeConteudo tela = (EntidadeConteudo) controladorEntidadeConteudo.obter(Long.parseLong(telaAtualizacao));
			servicoTipo.setTelaAtualizacao(tela);
			ConstanteSistema constanteSistema =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO);
			if (!constanteSistema.getValor().equals(String.valueOf(tela.getChavePrimaria()))) {
				indicadorOperacaoMedidor = null;
				indicadorTipoAssociacaoPontoConsumo = null;
			}
		}
		if (indicadorOperacaoMedidor != null && !indicadorOperacaoMedidor.isEmpty() && !"-1".equals(indicadorOperacaoMedidor)) {
			servicoTipo.setIndicadorOperacaoMedidor(indicadorOperacaoMedidor);
		}
		if (indicadorTipoAssociacaoPontoConsumo != null && !indicadorTipoAssociacaoPontoConsumo.isEmpty()) {
			servicoTipo.setIndicadorTipoAssociacaoPontoConsumo(indicadorTipoAssociacaoPontoConsumo);
		}

		if (indicadorClienteObrigatorio != null && !indicadorClienteObrigatorio.isEmpty()) {
			servicoTipo.setIndicadorClienteObrigatorio(Boolean.valueOf(indicadorClienteObrigatorio));
		}

		if (indicadorImovelObrigatorio != null && !indicadorImovelObrigatorio.isEmpty()) {
			servicoTipo.setIndicadorImovelObrigatorio(Boolean.valueOf(indicadorImovelObrigatorio));
		}

		if (indicadorContratoObrigatorio != null && !indicadorContratoObrigatorio.isEmpty()) {
			servicoTipo.setIndicadorContratoObrigatorio(Boolean.valueOf(indicadorContratoObrigatorio));
		}

		if (indicadorPontoConsumoObrigatorio != null && !indicadorPontoConsumoObrigatorio.isEmpty()) {
			servicoTipo.setIndicadorPontoConsumoObrigatorio(Boolean.valueOf(indicadorPontoConsumoObrigatorio));
		}

		if (StringUtils.isNotBlank(orientacao)) {
			servicoTipo.setOrientacao(orientacao);
		}

		if (numeroMaximoExecucao != null && !numeroMaximoExecucao.isEmpty()) {
			servicoTipo.setNumeroMaximoExecucoes(Long.valueOf(numeroMaximoExecucao));
		}
		if (prazoGarantia != null && !prazoGarantia.isEmpty()) {
			servicoTipo.setPrazoGarantia(Long.valueOf(prazoGarantia));
		}
		
		if (indicadorNovoMedidor != null && !indicadorNovoMedidor.isEmpty()) {
			servicoTipo.setIndicadorNovoMedidor(Boolean.valueOf(indicadorNovoMedidor));
		}
		
		if (indicadorServicoMobile != null && !indicadorServicoMobile.isEmpty()) {
			servicoTipo.setIndicadorServicoMobile(Boolean.valueOf(indicadorServicoMobile));
		}
		
		if (indicadorAssinatura != null && !indicadorAssinatura.isEmpty()) {
			servicoTipo.setIndicadorAssinatura(Boolean.valueOf(indicadorAssinatura));
		}
		
	}

	/**
	 * Popula os combos de Layout do Documento e Prioridade.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void populaCombos(ModelAndView model) throws NegocioException {

		model.addObject("listaDocumentos",
				controladorServicoTipo.obterTodosLayoutsOrdenado(true).stream()
						.filter(p -> p.getTipoDocumentoImpressao().getDescricao().equals("Autorização de Serviço"))
						.collect(Collectors.toList()));
		model.addObject("listaPrioridade", controladorServicoTipo.obterTodasPrioridadesOrdenadas(true));
		model.addObject("listaTipoAssociacao", controladorMedidor.obterTiposAssociacoes());
		model.addObject("listaProtocolo", controladorDocumentoImpressaoLayout.obterDocumentosLayoutImpressaoPorConstante(Constantes.RELATORIO_PROTOCOLO));
		model.addObject("listaEquipes", controladorEquipe.listarEquipe());

		QuestionarioVO questionaVo = new QuestionarioVO();
		questionaVo.setHabilitado(true);
		questionaVo.setTipoQuestionarioAS(true);

		model.addObject("listaFormulario", controladorQuestionario.consultarQuestionario(questionaVo));
	}

	/**
	 * Removes the dados sessao.
	 * 
	 * @param session
	 *            the session
	 */
	private void removeDadosSessao(HttpSession session) {

		session.removeAttribute(LISTA_MATERIAIS);
		session.removeAttribute(LISTA_EQUIPAMENTOS);
		session.removeAttribute(LISTA_AGENDAMENTOS);
	}

	/**
	 * Carrega os combos dos grids de materiais, equipamentos e agendamentos de turnos.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregaCombos(ModelAndView model) throws NegocioException {

		populaCombos(model);
		model.addObject("listaMateriaisCadastro", controladorServicoTipo.obterTodosMateriais(true));
		ConstanteSistema constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_EQUIPAMENTO);
		model.addObject("listaEquipamentosCadastro",
						controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TURNO);
		model.addObject("listaTurnosCadastro",
						controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));

		model.addObject(LISTA_EQUIPE, controladorServicoTipo.obterTodasEquipes());
		constanteSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TELA_ATUALIZACAO);
		model.addObject("listaTelaAtualizacao",
						controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema.getValor())));
		
		carregarDescricaoTelaAtualizacaoPontoConsumo(model);
	}

	/**
	 * Carrega as listas que populam os grids de materiais, equipamentos e agendamentos de turnos.
	 * 
	 * @param model
	 *            the model
	 * @param servicoTipo
	 *            the servico tipo
	 */
	private void carregaListas(ModelAndView model, ServicoTipo servicoTipo) {
		
		model.addObject("listaEquipamentoEspecial", servicoTipo.getListaEquipamentoEspecial());
		model.addObject(LISTA_MATERIAIS, servicoTipo.getListaMateriais());
		model.addObject("listaAgendamentosTurno", servicoTipo.getListaAgendamentosTurno());
	}

	/**
	 * Validar equipamento existente.
	 * 
	 * @param equipamentos
	 *            the equipamentos
	 * @param servicoTipoEquipamento
	 *            the servico tipo equipamento
	 * @return true, if successful
	 */
	private boolean validarEquipamentoExistente(Collection<ServicoTipoEquipamento> equipamentos,
					ServicoTipoEquipamento servicoTipoEquipamento) {

		for (ServicoTipoEquipamento equipamento : equipamentos) {
			if(equipamento.getEquipamento().getChavePrimaria() == servicoTipoEquipamento.getEquipamento().getChavePrimaria()) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Validar turno existente.
	 * 
	 * @param agendamentos
	 *            the agendamentos
	 * @param servicoTipoAgendamentoTurno
	 *            the servico tipo agendamento turno
	 * @return true, if successful
	 */
	private boolean validarTurnoExistente(Collection<ServicoTipoAgendamentoTurno> agendamentos,
					ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno) {

		for (ServicoTipoAgendamentoTurno agendamento : agendamentos) {
			if ((agendamento.getTurno().getChavePrimaria() == servicoTipoAgendamentoTurno.getTurno().getChavePrimaria())
					&& (agendamento.getEquipe().getChavePrimaria() == servicoTipoAgendamentoTurno.getEquipe()
							.getChavePrimaria())) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Validar material existente.
	 * 
	 * @param materiais
	 *            the materiais
	 * @param servicoTipoMaterial
	 *            the servico tipo material
	 * @return true, if successful
	 */
	private boolean validarMaterialExistente(Collection<ServicoTipoMaterial> materiais, ServicoTipoMaterial servicoTipoMaterial) {

		for (ServicoTipoMaterial material : materiais) {
			if(material.getMaterial().getChavePrimaria() == servicoTipoMaterial.getMaterial().getChavePrimaria()) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Obter operacoes.
	 * 
	 * @param tipoAssociacao
	 *            the tipo associacao
	 * @param chaveOperacao
	 *            the chave operacao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("obterOperacoes")
	public ModelAndView obterOperacoes(@RequestParam("tipoAssociacao") Integer tipoAssociacao,
					@RequestParam("chaveOperacao") Integer chaveOperacao, @RequestParam("chaveMedidorMotivoOperacao") Long chaveMedidorMotivoOperacao) throws GGASException {

		ModelAndView model = new ModelAndView("divOperacaoMedidor");

		List<OperacaoMedidor> listaOperacoesPermitidas = new ArrayList<>();

		if(tipoAssociacao != null) {
			listaOperacoesPermitidas = controladorMedidor.obterOperacoesMedidorPorTipoAssociacao(tipoAssociacao, "", "", "");
		}

		model.addObject("listaOperacaoMedidor", listaOperacoesPermitidas);
		model.addObject("chaveOperacao", chaveOperacao);
		
		model.addObject(LISTA_MEDIDOR_MOTIVO_OPERACAO, controladorMedidor.listarMotivoOperacaoMedidor());
		model.addObject("chaveMedidorMotivoOperacao", chaveMedidorMotivoOperacao);

		return model;
	}
	
	/**
	 * Formatar equipe.
	 * 
	 * @param Equipe
	 *            the equipe
	 * @return the formated equipe
	 */
	private Equipe formatarEquipe(Equipe equipe) {
			
		Equipe equipeFormatada = equipe;
		
		equipeFormatada.setNome(StringUtil.capitalize(equipe.getNome()));
		
		return equipeFormatada;
	}
}
