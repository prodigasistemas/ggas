/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *23/09/2013
 * vpessoa
 * 18:13:18
 */

package br.com.ggas.atendimento.servicotipo.negocio.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout;
import br.com.ggas.atendimento.documentoimpressaolayout.repositorio.RepositorioDocumentoImpressaoLayout;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.repositorio.RepositorioEquipe;
import br.com.ggas.atendimento.material.repositorio.RepositorioMaterial;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipo.repositorio.RepositorioServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoagendamentoturno.repositorio.RepositorioServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipoequipamento.repositorio.RepositorioServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.repositorio.RepositorioServicoTipoPrioridade;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de ControladorServicoTipoImpl
 * 
 * @author vpessoa
 */
@Service("controladorServicoTipo")
@Transactional
public class ControladorServicoTipoImpl implements ControladorServicoTipo {

	private static final int LIMITE_CAMPO = 2;

	@Autowired
	private RepositorioServicoTipo repositorioServicoTipo;

	@Autowired
	private RepositorioServicoTipoPrioridade repositorioServicoTipoPrioridade;

	@Autowired
	private RepositorioMaterial repositorioMaterial;

	@Autowired
	private RepositorioServicoTipoAgendamentoTurno repositorioAgendamentoTurno;

	@Autowired
	private RepositorioServicoTipoEquipamento repositorioEquipamento;

	@Autowired
	private RepositorioDocumentoImpressaoLayout repositorioDocumentoImpressaoLayout;

	@Autowired
	@Qualifier(value = "controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorQuestionario")
	private ControladorQuestionario controladorQuestionario;
	
	@Autowired
	private RepositorioEquipe repositorioEquipe;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodasPrioridades(boolean)
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodasPrioridades(boolean habilitado) throws NegocioException {

		return repositorioServicoTipoPrioridade.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodasPrioridadesOrdenadas(boolean)
	 */
	@Override
	@Transactional
	public Collection<ServicoTipoPrioridade> obterTodasPrioridadesOrdenadas(boolean habilitado) throws NegocioException {

		return repositorioServicoTipoPrioridade.obterTodosServicoTipoPrioridadeOrdenado(habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodas(boolean)
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodas(boolean habilitado) throws NegocioException {

		return repositorioServicoTipo.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#consultarServicoTipo(br.com.ggas.atendimento.servicotipo.dominio.
	 * ServicoTipo
	 * , java.lang.Boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> consultarServicoTipo(ServicoTipo servicoTipo, Boolean habilitado) throws NegocioException {

		return repositorioServicoTipo.consultarServicoTipo(servicoTipo, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterServicoTipo(long)
	 */
	@Override
	@Transactional
	public ServicoTipo obterServicoTipo(long chavePrimaria) throws NegocioException {

		return (ServicoTipo) this.repositorioServicoTipo.obter(chavePrimaria, Constantes.SERVICO_TIPO_PRIORIDADE,
				Constantes.LISTA_EQUIPAMENTOS, Constantes.LISTA_MATERIAIS, Constantes.LISTA_AGENDAMENTOS,
				Constantes.DOCUMENTO_IMPRESSOA_LAYOUT, "formulario");
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodosMateriais(boolean)
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodosMateriais(boolean habilitado) throws NegocioException {

		return repositorioMaterial.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodosTurnos(boolean)
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodosTurnos(boolean habilitado) throws NegocioException {

		return repositorioAgendamentoTurno.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodosEquipamentos(boolean)
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodosEquipamentos(boolean habilitado) throws NegocioException {

		return repositorioEquipamento.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterMaterial(java.lang.Long)
	 */
	@Override
	@Transactional
	public EntidadeNegocio obterMaterial(Long chavePrimaria) throws NegocioException {

		return repositorioMaterial.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#atualizarServicoTipo(br.com.ggas.atendimento.servicotipo.dominio.
	 * ServicoTipo
	 * , java.util.Collection, java.util.Collection, java.util.Collection)
	 */
	@Transactional
	@Override
	public void atualizarServicoTipo(ServicoTipo servicoConsultaTipo, Collection<ServicoTipoMaterial> listaMateriais,
					Collection<ServicoTipoEquipamento> listaEquipamentos, Collection<ServicoTipoAgendamentoTurno> listaAgendamentos)
					throws NegocioException, ConcorrenciaException {
		
		ServicoTipo servicoTipo = servicoConsultaTipo;
		
		if (servicoTipo != null) {
			ServicoTipo servicoTipoconsulta = repositorioServicoTipo
					.obterServicoTipoPorDescricao(servicoTipo.getDescricao());

			validarTipoAssociacaoAcao(servicoTipo);

			if (servicoTipoconsulta != null
					&& servicoTipoconsulta.getChavePrimaria() != servicoTipo.getChavePrimaria()) {

				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_EXISTENTE, servicoTipo.getDescricao());
			}
			List<ServicoAutorizacaoAgenda> listaAutorizacoesAgendadasEmAndamento = ServiceLocator.getInstancia()
					.getControladorServicoAutorizacao()
					.consultarServicoAutorizacaoAgendaEmAndamento(servicoTipo.getChavePrimaria());
			if (!servicoTipo.getIndicadorAgendamento() && listaAutorizacoesAgendadasEmAndamento != null
					&& !listaAutorizacoesAgendadasEmAndamento.isEmpty()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_JA_EXISTE_AGENDAMENTO_PARA_O_TIPO_SERVICO,
						servicoTipo.getDescricao());
			}

			servicoTipoconsulta = (ServicoTipo) repositorioServicoTipo.obter(servicoTipo.getChavePrimaria());

			servicoTipoconsulta.setDescricao(servicoTipo.getDescricao());
			servicoTipoconsulta.setDocumentoImpressaoLayout(servicoTipo.getDocumentoImpressaoLayout());
			servicoTipoconsulta.setHabilitado(servicoTipo.isHabilitado());
			servicoTipoconsulta.setQuantidadePrazoExecucao(servicoTipo.getQuantidadePrazoExecucao());
			servicoTipoconsulta.setQuantidadeTempoMedio(servicoTipo.getQuantidadeTempoMedio());
			servicoTipoconsulta.setIndicadorEquipamento(servicoTipo.getIndicadorEquipamento());
			servicoTipoconsulta.setIndicadorAgendamento(servicoTipo.getIndicadorAgendamento());
			servicoTipoconsulta.setIndicadorGeraLote(servicoTipo.getIndicadorGeraLote());
			servicoTipoconsulta.setIndicadorEncerramentoAuto(servicoTipo.getIndicadorEncerramentoAuto());
			servicoTipoconsulta.setIndicadorPesquisaSatisfacao(servicoTipo.getIndicadorPesquisaSatisfacao());
			servicoTipoconsulta.setIndicadorServicoRegulamento(servicoTipo.getIndicadorServicoRegulamento());
			servicoTipoconsulta.setIndicadorVeiculo(servicoTipo.getIndicadorVeiculo());
			servicoTipoconsulta.setIndicadorAtualizacaoCadastral(servicoTipo.getIndicadorAtualizacaoCadastral());
			servicoTipoconsulta.setIndicadorClienteObrigatorio(servicoTipo.getIndicadorClienteObrigatorio());
			servicoTipoconsulta.setIndicadorImovelObrigatorio(servicoTipo.getIndicadorImovelObrigatorio());
			servicoTipoconsulta.setIndicadorContratoObrigatorio(servicoTipo.getIndicadorContratoObrigatorio());
			servicoTipoconsulta.setIndicadorPontoConsumoObrigatorio(servicoTipo.getIndicadorPontoConsumoObrigatorio());
			servicoTipoconsulta.setTelaAtualizacao(servicoTipo.getTelaAtualizacao());
			servicoTipoconsulta.setIndicadorOperacaoMedidor(servicoTipo.getIndicadorOperacaoMedidor());
			servicoTipoconsulta
					.setIndicadorTipoAssociacaoPontoConsumo(servicoTipo.getIndicadorTipoAssociacaoPontoConsumo());
			servicoTipoconsulta.setServicoTipoPrioridade(servicoTipo.getServicoTipoPrioridade());

			servicoTipoconsulta.setIndicadorProximaFatura(servicoTipo.getIndicadorProximaFatura());
			servicoTipoconsulta.setIndicadorCobranca(servicoTipo.getIndicadorCobranca());
			servicoTipoconsulta.setIndicadorEmailCadastrar(servicoTipo.getIndicadorEmailCadastrar());
			servicoTipoconsulta.setIndicadorEmailEmExecucao(servicoTipo.getIndicadorEmailEmExecucao());
			servicoTipoconsulta.setIndicadorEmailExecucao(servicoTipo.getIndicadorEmailExecucao());
			servicoTipoconsulta.setIndicadorEmailEncerrar(servicoTipo.getIndicadorEmailEncerrar());
			servicoTipoconsulta.setIndicadorEmailAlterar(servicoTipo.getIndicadorEmailAlterar());
			servicoTipoconsulta.setIndicadorEmailAgendar(servicoTipo.getIndicadorEmailAgendar());
			servicoTipoconsulta.setIndicadorEmailCliente(servicoTipo.getIndicadorEmailCliente());
			servicoTipoconsulta.setOrientacao(servicoTipo.getOrientacao());
			servicoTipoconsulta.setFormulario(servicoTipo.getFormulario());
			servicoTipoconsulta.setIndicadorFormularioObrigatorio(servicoTipo.getIndicadorFormularioObrigatorio());
			servicoTipoconsulta.setNumeroMaximoExecucoes(servicoTipo.getNumeroMaximoExecucoes());
			servicoTipoconsulta.setPrazoGarantia(servicoTipo.getPrazoGarantia());
			
			servicoTipoconsulta.setIndicadorProtocolo(servicoTipo.getIndicadorProtocolo());
			servicoTipoconsulta.setIndicadorProtocoloObrigatorio(servicoTipo.getIndicadorProtocoloObrigatorio());
			servicoTipoconsulta.setIndicadorTipoProtocolo(servicoTipo.getIndicadorTipoProtocolo());
			servicoTipoconsulta.setIndicadorNotificacao(servicoTipo.getIndicadorNotificacao());
			servicoTipoconsulta.setIndicadorIntegracao(servicoTipo.getIndicadorIntegracao());
			servicoTipoconsulta.setTempoRetornoProtocolo(servicoTipo.getTempoRetornoProtocolo());
			servicoTipoconsulta.setTempoEmissaoAS(servicoTipo.getTempoEmissaoAS());
			
			servicoTipoconsulta.setIndicadorAviso(servicoTipo.getIndicadorAviso());
			servicoTipoconsulta.setDocumentoImpressaoAviso(servicoTipo.getDocumentoImpressaoAviso());
			servicoTipoconsulta.setIndicadorMetadeTempoExecucao(servicoTipo.getIndicadorMetadeTempoExecucao());
			servicoTipoconsulta.setIndicadorNovoMedidor(servicoTipo.getIndicadorNovoMedidor());
			servicoTipoconsulta.setEquipePrioritaria(servicoTipo.getEquipePrioritaria());
			servicoTipoconsulta.setIndicadorServicoMobile(servicoTipo.getIndicadorServicoMobile());
			servicoTipoconsulta.setIndicadorAssinatura(servicoTipo.getIndicadorAssinatura());
			
			servicoTipoconsulta.setMotivoOperacaoMedidor(servicoTipo.getMotivoOperacaoMedidor());
			
			servicoTipoconsulta.setIndicadorEmailPersonalizado(servicoTipo.getIndicadorEmailPersonalizado());
			servicoTipoconsulta.setLayoutEmail(servicoTipo.getLayoutEmail());
			servicoTipoconsulta.setListaEmailPersonalizado(servicoTipo.getListaEmailPersonalizado());
			servicoTipoconsulta.setAssuntoEmail(servicoTipo.getAssuntoEmail());
			

			repositorioServicoTipo.getHibernateTemplate().evict(servicoTipoconsulta);
			servicoTipo = servicoTipoconsulta;

			if (listaEquipamentos != null && servicoTipo.getIndicadorEquipamento() && listaEquipamentos.isEmpty()) {

				throw new NegocioException(Constantes.QUANTIDADE_MINIMA_DE_EQUIPAMENTO);
			}

			if (listaEquipamentos != null) {
				servicoTipo.getListaEquipamentoEspecial().clear();
				servicoTipo.getListaEquipamentoEspecial().addAll(listaEquipamentos);
				for (ServicoTipoEquipamento servicoTipoEquipamento : servicoTipo.getListaEquipamentoEspecial()) {
					servicoTipoEquipamento.setServicoTipo(servicoTipo);
					servicoTipoEquipamento.setHabilitado(Boolean.TRUE);
					servicoTipoEquipamento.setUltimaAlteracao(Calendar.getInstance().getTime());
				}
			}
			if (listaMateriais != null) {
				servicoTipo.getListaMateriais().clear();
				servicoTipo.getListaMateriais().addAll(listaMateriais);
				for (ServicoTipoMaterial servicoTipoMaterial : servicoTipo.getListaMateriais()) {
					servicoTipoMaterial.setServicoTipo(servicoTipo);
					servicoTipoMaterial.setHabilitado(Boolean.TRUE);
					servicoTipoMaterial.setUltimaAlteracao(Calendar.getInstance().getTime());
				}

			}

			if (listaAgendamentos != null) {
				servicoTipo.getListaAgendamentosTurno().clear();
				servicoTipo.getListaAgendamentosTurno().addAll(listaAgendamentos);
				for (ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno : servicoTipo
						.getListaAgendamentosTurno()) {
					servicoTipoAgendamentoTurno.setServicoTipo(servicoTipo);
					servicoTipoAgendamentoTurno.setHabilitado(Boolean.TRUE);
					servicoTipoAgendamentoTurno.setUltimaAlteracao(Calendar.getInstance().getTime());
				}
			}
			repositorioServicoTipo.atualizar(servicoTipo);
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#inserir(br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo,
	 * java.util.Collection, java.util.Collection, java.util.Collection)
	 */
	@Override
	@Transactional
	public ServicoTipo inserir(ServicoTipo servicoTipo, Collection<ServicoTipoMaterial> listaMateriais,
					Collection<ServicoTipoEquipamento> listaEquipamentos, Collection<ServicoTipoAgendamentoTurno> listaAgendamentos)
					throws NegocioException {

		validarTipoAssociacaoAcao(servicoTipo);

		if(servicoTipo.getIndicadorEquipamento() && listaEquipamentos == null || servicoTipo.getIndicadorEquipamento()
						&& listaEquipamentos.isEmpty()) {
			throw new NegocioException(Constantes.QUANTIDADE_MINIMA_DE_EQUIPAMENTO);
		}

		ServicoTipo servicoTipoRetorno = repositorioServicoTipo.obterServicoTipoPorDescricao(servicoTipo.getDescricao());
		if(servicoTipoRetorno != null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_EXISTENTE, servicoTipo.getDescricao());
		}
		if(listaEquipamentos != null && !listaEquipamentos.isEmpty()) {
			servicoTipo.getListaEquipamentoEspecial().clear();
			servicoTipo.getListaEquipamentoEspecial().addAll(listaEquipamentos);
			for (ServicoTipoEquipamento servicoTipoEquipamento : servicoTipo.getListaEquipamentoEspecial()) {
				servicoTipoEquipamento.setServicoTipo(servicoTipo);
				servicoTipoEquipamento.setHabilitado(Boolean.TRUE);
				servicoTipoEquipamento.setUltimaAlteracao(Calendar.getInstance().getTime());
			}
		}
		if(listaMateriais != null && !listaMateriais.isEmpty()) {
			servicoTipo.getListaMateriais().clear();
			servicoTipo.getListaMateriais().addAll(listaMateriais);
			for (ServicoTipoMaterial servicoTipoMaterial : servicoTipo.getListaMateriais()) {
				servicoTipoMaterial.setServicoTipo(servicoTipo);
				servicoTipoMaterial.setHabilitado(Boolean.TRUE);
				servicoTipoMaterial.setUltimaAlteracao(Calendar.getInstance().getTime());
			}

		}

		if(listaAgendamentos != null && !listaAgendamentos.isEmpty()) {
			servicoTipo.getListaAgendamentosTurno().clear();
			servicoTipo.getListaAgendamentosTurno().addAll(listaAgendamentos);
			for (ServicoTipoAgendamentoTurno servicoTipoAgendamentoTurno : servicoTipo.getListaAgendamentosTurno()) {
				servicoTipoAgendamentoTurno.setServicoTipo(servicoTipo);
				servicoTipoAgendamentoTurno.setHabilitado(Boolean.TRUE);
				servicoTipoAgendamentoTurno.setUltimaAlteracao(Calendar.getInstance().getTime());
			}
		}

		repositorioServicoTipo.inserir(servicoTipo);

		return servicoTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodosLayouts(boolean)
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> obterTodosLayouts(boolean habilitado) throws NegocioException {

		return repositorioDocumentoImpressaoLayout.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodosLayoutsOrdenado(boolean)
	 */
	@Override
	@Transactional
	public Collection<DocumentoImpressaoLayout> obterTodosLayoutsOrdenado(boolean habilitado) throws NegocioException {

		return repositorioDocumentoImpressaoLayout.obterTodosLayoutsOrdenado(habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#removerServicoTipo(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Transactional
	@Override
	public void removerServicoTipo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.repositorioServicoTipo.remover(chavesPrimarias, dadosAuditoria);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterServicoTipoPrioridade(long)
	 */
	@Override
	@Transactional
	public ServicoTipoPrioridade obterServicoTipoPrioridade(long chavePrimaria) throws NegocioException {

		return (ServicoTipoPrioridade) this.repositorioServicoTipoPrioridade.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterDocumentoImpressaoLayout(long)
	 */
	@Override
	@Transactional
	public DocumentoImpressaoLayout obterDocumentoImpressaoLayout(long chavePrimaria) throws NegocioException {

		return (DocumentoImpressaoLayout) this.repositorioDocumentoImpressaoLayout.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#listarServicoTipo()
	 */
	@Override
	@Transactional
	public Collection<ServicoTipo> listarServicoTipo() throws NegocioException {

		return repositorioServicoTipo.listarServicoTipo();
	}

	@Override
	@Transactional
	public Collection<ServicoTipo> listarServicosTipoAtivos() throws NegocioException {

		return repositorioServicoTipo.listarServicosTipoAtivos();
	}

	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obter(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ServicoTipo> obter(Long chavePrimaria) throws NegocioException {

		return (Collection<ServicoTipo>) repositorioServicoTipo.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#listarTodos()
	 */
	@Override
	@Transactional
	public Collection<ServicoTipo> listarTodos() throws NegocioException {

		return repositorioServicoTipo.listarServicoTipo();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#consultarServicoTipo(java.lang.Long)
	 */
	@Override
	@Transactional
	public ServicoTipo consultarServicoTipo(Long chavePrimaria) {

		return repositorioServicoTipo.consultarServicoTipo(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#consultarServicoTipoComPrioridade(java.lang.Long)
	 */
	@Override
	@Transactional
	public ServicoTipo consultarServicoTipoComPrioridade(Long chavePrimaria) {

		return repositorioServicoTipo.consultarServicoTipoComPrioridade(chavePrimaria);
	}

	/**
	 * Validar tipo associacao acao.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarTipoAssociacaoAcao(ServicoTipo servicoTipo) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		ConstanteSistema constanteTelaAtualizacao = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_ATUALIZACAO_TELA_ASSOCI_MEDIDO_PONTO_CONSUMO);
		if (servicoTipo != null) {
			if (servicoTipo.getTelaAtualizacao() != null
					&& servicoTipo.getTelaAtualizacao().getChavePrimaria() == Long.parseLong(constanteTelaAtualizacao.getValor())) {
				if (servicoTipo.getIndicadorTipoAssociacaoPontoConsumo() == null
						|| servicoTipo.getIndicadorTipoAssociacaoPontoConsumo().isEmpty()) {
					stringBuilder.append(Constantes.TIPO_ASSOCIACAO_PONTO_CONSUMO);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				if (servicoTipo.getIndicadorOperacaoMedidor() == null || servicoTipo.getIndicadorOperacaoMedidor().isEmpty()
						|| "-1".equals(servicoTipo.getIndicadorOperacaoMedidor())) {
					stringBuilder.append(Constantes.TIPO_OPERACAO_MEDIDOR);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			} else {
				servicoTipo.setIndicadorOperacaoMedidor(null);
				servicoTipo.setIndicadorTipoAssociacaoPontoConsumo(null);
			}
		}
	
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_TIPO_ASSOCIACAO_ACAO,
							camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
			throw new NegocioException(erros);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#listarServicoTipoAgendamento(java.lang.Boolean)
	 */
	@Override
	public Collection<ServicoTipo> listarServicoTipoAgendamento(Boolean indicadorAgendamento, Boolean temServicoAutorizacao) {

		return repositorioServicoTipo.listarServicoTipoAgendamento(indicadorAgendamento, temServicoAutorizacao);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterTodosServicoTipoComPesquisaSatisfacao()
	 */
	@Override
	public Collection<ServicoTipo> obterTodosServicoTipoComPesquisaSatisfacao() {

		return repositorioServicoTipo.obterTodosServicoTipoComPesquisaSatisfacao();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#existeQuestionarioAssociadoServicoTipo(br.com.ggas.atendimento.
	 * servicotipo
	 * .dominio.ServicoTipo)
	 */
	@Override
	public boolean existeQuestionarioAssociadoServicoTipo(ServicoTipo servicoTipo) throws NegocioException {

		boolean retorno = false;

		Collection<Questionario> listaQuestionarioServicoTipo = controladorQuestionario
						.consultarQuestionarioAssociadosServicoTipo(servicoTipo);

		if(!listaQuestionarioServicoTipo.isEmpty()) {
			retorno = true;
		} else {
			retorno = false;
		}

		return retorno;
	}
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.equipe.dominio.Equipe#obterTodasEquipes()
	 */
	@Override
	public Collection<Equipe> obterTodasEquipes() throws NegocioException {
		
		return repositorioEquipe.listarEquipe();
	}
	
	
	@Override
	public Collection<ServicoTipo> consultarServicoTipoComunicacao() {

		return repositorioServicoTipo.consultarServicoTipoComunicacao();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo#obterEquipePrioritaria(long)
	 */
	@Override
	@Transactional
	public Equipe obterEquipePrioritaria(Long idServicoTipo) throws NegocioException {
		return repositorioServicoTipo.obterEquipePrioritaria(idServicoTipo);
	}
	
	@Override
	public Collection<ServicoTipo> listarServicosTipoAtivosParaNovoMedidor(){
		return repositorioServicoTipo.listarServicosTipoAtivosParaNovoMedidor();
	}

}
