/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *23/09/2013
 * vpessoa
 * 18:14:20
 */

package br.com.ggas.atendimento.servicotipo.negocio;

import java.util.Collection;

import br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Controlador Servico Tipo
 * 
 * @author vpessoa
 */
public interface ControladorServicoTipo {

	/**
	 * Obter todas prioridades.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodasPrioridades(boolean habilitado) throws NegocioException;

	/**
	 * Obter todas.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodas(boolean habilitado) throws NegocioException;

	/**
	 * Consultar servico tipo.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> consultarServicoTipo(ServicoTipo servicoTipo, Boolean habilitado) throws NegocioException;

	/**
	 * Obter servico tipo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ServicoTipo obterServicoTipo(long chavePrimaria) throws NegocioException;

	/**
	 * Obter todos materiais.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodosMateriais(boolean habilitado) throws NegocioException;

	/**
	 * Obter todos turnos.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodosTurnos(boolean habilitado) throws NegocioException;

	/**
	 * Obter todos equipamentos.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodosEquipamentos(boolean habilitado) throws NegocioException;

	/**
	 * Obter material.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeNegocio obterMaterial(Long chavePrimaria) throws NegocioException;

	/**
	 * Atualizar servico tipo.
	 * 
	 * @param servicoTipo {@link ServicoTipo}
	 * @param listaMateriais {@link Collection}
	 * @param listaEquipamentos {@link Collection}
	 * @param listaAgendamentos {@link Collection}
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarServicoTipo(ServicoTipo servicoTipo, Collection<ServicoTipoMaterial> listaMateriais,
					Collection<ServicoTipoEquipamento> listaEquipamentos, Collection<ServicoTipoAgendamentoTurno> listaAgendamentos)
					throws GGASException;

	/**
	 * Inserir.
	 * 
	 * @param servicoTipo {@link ServicoTipo}
	 * @param listaMateriais {@link Collection}
	 * @param listaEquipamentos {@link Collection}
	 * @param listaAgendamentos {@link Collection}
	 * @return servicoTipo
	 * @throws NegocioException {@link NegocioException}
	 */
	ServicoTipo inserir(ServicoTipo servicoTipo, Collection<ServicoTipoMaterial> listaMateriais,
					Collection<ServicoTipoEquipamento> listaEquipamentos, Collection<ServicoTipoAgendamentoTurno> listaAgendamentos)
					throws NegocioException;

	/**
	 * Obter todos layouts.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodosLayouts(boolean habilitado) throws NegocioException;

	/**
	 * Remover servico tipo.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerServicoTipo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Obter servico tipo prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo prioridade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ServicoTipoPrioridade obterServicoTipoPrioridade(long chavePrimaria) throws NegocioException;

	/**
	 * Obter documento impressao layout.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the documento impressao layout
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DocumentoImpressaoLayout obterDocumentoImpressaoLayout(long chavePrimaria) throws NegocioException;

	/**
	 * Listar servico tipo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipo> listarServicoTipo() throws NegocioException;

	/**
	 * Obter.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipo> obter(Long chavePrimaria) throws NegocioException;

	/**
	 * Listar todos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipo> listarTodos() throws NegocioException;

	/**
	 * Consultar servico tipo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo
	 */
	public ServicoTipo consultarServicoTipo(Long chavePrimaria);

	/**
	 * Consultar servico tipo com prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo
	 */
	public ServicoTipo consultarServicoTipoComPrioridade(Long chavePrimaria);

	/**
	 * Listar servico tipo agendamento.
	 * 
	 * @param indicadorAgendamento
	 *            the indicador agendamento
	 * @param temServicoAutorizacao {@link Boolean}
	 * @return the collection
	 */
	public Collection<ServicoTipo> listarServicoTipoAgendamento(Boolean indicadorAgendamento, Boolean temServicoAutorizacao);

	/**
	 * Obter todos layouts ordenado.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<DocumentoImpressaoLayout> obterTodosLayoutsOrdenado(boolean habilitado) throws NegocioException;

	/**
	 * Obter todas prioridades ordenadas.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipoPrioridade> obterTodasPrioridadesOrdenadas(boolean habilitado) throws NegocioException;

	/**
	 * Obter todos servico tipo com pesquisa satisfacao.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipo> obterTodosServicoTipoComPesquisaSatisfacao() throws NegocioException;

	/**;
	 * Existe questionario associado servico tipo.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean existeQuestionarioAssociadoServicoTipo(ServicoTipo servicoTipo) throws NegocioException;
	
	/**
	 * Obter todas as equipes.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Equipe> obterTodasEquipes() throws NegocioException;

	/**
	 * Obter todos os servicos relacionados a comunicacao
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoTipo> consultarServicoTipoComunicacao();

	/**
	 * Obter todos os tipos de servico ativos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoTipo> listarServicosTipoAtivos() throws NegocioException;
	/**
	 * Obter a equipe prioritaria do tipo de servico.
	 * 
	 * @return the Equipe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Equipe obterEquipePrioritaria(Long idServicoTipo) throws NegocioException;
	
	/**
	 * Obter todos os tipos de servico ativos para medidores novos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ServicoTipo> listarServicosTipoAtivosParaNovoMedidor();
}
