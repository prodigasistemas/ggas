/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *23/09/2013
 * vpessoa
 * 18:15:16
 */

package br.com.ggas.atendimento.servicotipo.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoAgenda;
import br.com.ggas.atendimento.servicoautorizacao.repositorio.RepositorioServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Util;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de ControladorServicoTipoImpl
 * 
 * @author vpessoa
 */
@Repository
public class RepositorioServicoTipo extends RepositorioGenerico {

	private static final String TELA_ATUALIZACAO = "telaAtualizacao";
	private static final String INDICADOR_AGENDAMENTO = "indicadorAgendamento";
	private static final String SERVICO_TIPO_PRIORIDADE = "servicoTipoPrioridade";
	private static final String DOCUMENTO_IMPRESSAO_LAYOUT = "documentoImpressaoLayout";
	private static final String FROM = " from ";
	private static final String WHERE = " where ";
	private static final String SERVICO_TIPO =" servicoTipo ";
	/**
	 * Auto injeração para uso do repositório. 
	 */
	@Autowired
	public RepositorioServicoAutorizacao repositorioServicoAutorizacao;

	/**
	 * Classe RepositorioServicoTipo
	 * 
	 * @param sessionFactory - Auto injeção pra criação de conexão.
	 */
	@Autowired
	public RepositorioServicoTipo(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ServicoTipo();
	}

	@Override
	public Class<ServicoTipo> getClasseEntidade() {

		return ServicoTipo.class;

	}

	public Class<ServicoAutorizacao> getClasseEntidadeServicoAutorizacao() {

		return ServicoAutorizacao.class;

	}
	
	public Class<ServicoAutorizacaoAgenda> getClasseEntidadeServicoAgendamento(){
		return ServicoAutorizacaoAgenda.class;
	}

	/**
	 * Consultar servico tipo.
	 * 
	 * @param servicoTipo
	 *            the servico tipo
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<EntidadeNegocio> consultarServicoTipo(ServicoTipo servicoTipo, Boolean habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(SERVICO_TIPO_PRIORIDADE, FetchMode.JOIN);
		criteria.setFetchMode(DOCUMENTO_IMPRESSAO_LAYOUT, FetchMode.JOIN);
		criteria.setFetchMode(TELA_ATUALIZACAO, FetchMode.JOIN);

		criteria.createAlias(TELA_ATUALIZACAO, TELA_ATUALIZACAO, Criteria.LEFT_JOIN);

		if (servicoTipo.getDescricao() != null && !servicoTipo.getDescricao().isEmpty()) {
			criteria.add(Restrictions.ilike("descricao", Util.formatarTextoConsulta(servicoTipo.getDescricao())));
		}
		if (servicoTipo.getServicoTipoPrioridade() != null) {
			criteria.createAlias(SERVICO_TIPO_PRIORIDADE, SERVICO_TIPO_PRIORIDADE);
			criteria.add(Restrictions.eq("servicoTipoPrioridade.chavePrimaria", servicoTipo.getServicoTipoPrioridade().getChavePrimaria()));
		}
		if (servicoTipo.getDocumentoImpressaoLayout() != null) {
			criteria.createAlias(DOCUMENTO_IMPRESSAO_LAYOUT, DOCUMENTO_IMPRESSAO_LAYOUT);
			criteria.add(Restrictions.eq("documentoImpressaoLayout.chavePrimaria", servicoTipo.getDocumentoImpressaoLayout()
							.getChavePrimaria()));
		}
		if (habilitado != null) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
		}

		if (servicoTipo.getQuantidadePrazoExecucao() != null && servicoTipo.getQuantidadePrazoExecucao() != 0) {
			criteria.add(Restrictions.eq("quantidadePrazoExecucao", servicoTipo.getQuantidadePrazoExecucao()));
		}

		if (servicoTipo.getQuantidadeTempoMedio() != null && servicoTipo.getQuantidadeTempoMedio() != 0) {
			criteria.add(Restrictions.eq("quantidadeTempoMedio", servicoTipo.getQuantidadeTempoMedio()));
		}

		if (servicoTipo.getIndicadorEquipamento() != null) {
			criteria.add(Restrictions.eq("indicadorEquipamento", servicoTipo.getIndicadorEquipamento()));
		}

		if (servicoTipo.getIndicadorServicoRegulamento() != null) {
			criteria.add(Restrictions.eq("indicadorServicoRegulamento", servicoTipo.getIndicadorServicoRegulamento()));
		}

		if (servicoTipo.getIndicadorAgendamento() != null) {
			criteria.add(Restrictions.eq(INDICADOR_AGENDAMENTO, servicoTipo.getIndicadorAgendamento()));
		}

		if (servicoTipo.getIndicadorVeiculo() != null) {
			criteria.add(Restrictions.eq("indicadorVeiculo", servicoTipo.getIndicadorVeiculo()));
		}

		if (servicoTipo.getIndicadorEncerramentoAuto() != null) {
			criteria.add(Restrictions.eq("indicadorEncerramentoAuto", servicoTipo.getIndicadorEncerramentoAuto()));
		}

		if (servicoTipo.getIndicadorPesquisaSatisfacao() != null) {
			criteria.add(Restrictions.eq("indicadorPesquisaSatisfacao", servicoTipo.getIndicadorPesquisaSatisfacao()));
		}

		if (servicoTipo.getIndicadorGeraLote() != null) {
			criteria.add(Restrictions.eq("indicadorGeraLote", servicoTipo.getIndicadorGeraLote()));
		}

		if (servicoTipo.getIndicadorAtualizacaoCadastral() != null && !servicoTipo.getIndicadorAtualizacaoCadastral().isEmpty()) {
			criteria.add(Restrictions.ilike("indicadorAtualizacaoCadastral", servicoTipo.getIndicadorAtualizacaoCadastral()));
		}

		if (servicoTipo.getTelaAtualizacao() != null) {
			criteria.add(Restrictions.eq("telaAtualizacao.chavePrimaria", servicoTipo.getTelaAtualizacao().getChavePrimaria()));
		}

		if (servicoTipo.getIndicadorOperacaoMedidor() != null && !"-1".equals(servicoTipo.getIndicadorOperacaoMedidor())) {
			criteria.add(Restrictions.eq("indicadorOperacaoMedidor", servicoTipo.getIndicadorOperacaoMedidor()));
		}

		if (servicoTipo.getIndicadorTipoAssociacaoPontoConsumo() != null) {
			criteria.add(Restrictions.eq("indicadorTipoAssociacaoPontoConsumo", servicoTipo.getIndicadorTipoAssociacaoPontoConsumo()));
		}

		if (servicoTipo.getIndicadorClienteObrigatorio() != null) {
			criteria.add(Restrictions.eq("indicadorClienteObrigatorio", servicoTipo.getIndicadorClienteObrigatorio()));
		}

		if (servicoTipo.getIndicadorImovelObrigatorio() != null) {
			criteria.add(Restrictions.eq("indicadorImovelObrigatorio", servicoTipo.getIndicadorImovelObrigatorio()));
		}

		if (servicoTipo.getIndicadorContratoObrigatorio() != null) {
			criteria.add(Restrictions.eq("indicadorContratoObrigatorio", servicoTipo.getIndicadorContratoObrigatorio()));
		}

		if (servicoTipo.getIndicadorPontoConsumoObrigatorio() != null) {
			criteria.add(Restrictions.eq("indicadorPontoConsumoObrigatorio", servicoTipo.getIndicadorPontoConsumoObrigatorio()));
		}

		if (habilitado != null) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
		}

		if (servicoTipo.getNumeroMaximoExecucoes() != null && servicoTipo.getNumeroMaximoExecucoes() != 0) {
			criteria.add(Restrictions.eq("numeroMaximoExecucoes", servicoTipo.getNumeroMaximoExecucoes()));
		}

		if (servicoTipo.getPrazoGarantia() != null && servicoTipo.getPrazoGarantia() != 0) {
			criteria.add(Restrictions.eq("prazoGarantia", servicoTipo.getPrazoGarantia()));
		}

		if (servicoTipo.getIndicadorNovoMedidor() != null) {
			criteria.add(Restrictions.eq("indicadorNovoMedidor", servicoTipo.getIndicadorNovoMedidor()));
		}
		
		if (servicoTipo.getIndicadorServicoMobile() != null) {
			criteria.add(Restrictions.eq("indicadorServicoMobile", servicoTipo.getIndicadorServicoMobile()));
		}
		
		if (servicoTipo.getIndicadorAssinatura() != null) {
			criteria.add(Restrictions.eq("indicadorAssinatura", servicoTipo.getIndicadorAssinatura()));
		}		
		
		return criteria.list();
	}

	/**
	 * Obter servico tipo por descricao.
	 * 
	 * @param descricao
	 *            the descricao
	 * @return the servico tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ServicoTipo obterServicoTipoPorDescricao(String descricao) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(SERVICO_TIPO);
		hql.append(WHERE);
		hql.append(" servicoTipo.descricao = :descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("descricao", descricao);
		return (ServicoTipo) query.uniqueResult();

	}

	/**
	 * Inserir equipamentos.
	 * 
	 * @param listaEquipamentos
	 *            the lista equipamentos
	 * @param servicoTipo
	 *            the servico tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void inserirEquipamentos(Collection<ServicoTipoEquipamento> listaEquipamentos, ServicoTipo servicoTipo) throws NegocioException {

		for (ServicoTipoEquipamento servicoTipoEquipamento : listaEquipamentos) {
			servicoTipoEquipamento.setServicoTipo(servicoTipo);
			this.inserir(servicoTipoEquipamento);
		}
	}

	/**
	 * Inserir materiais.
	 * 
	 * @param listaMateriais
	 *            the lista materiais
	 * @param servicoTipo
	 *            the servico tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void inserirMateriais(Collection<ServicoTipoMaterial> listaMateriais, ServicoTipo servicoTipo) throws NegocioException {

		for (ServicoTipoMaterial servicoTipoMaterial : listaMateriais) {
			servicoTipoMaterial.setServicoTipo(servicoTipo);
			this.inserir(servicoTipoMaterial);
		}
	}

	/**
	 * Listar servico tipo.
	 * 
	 * @return the collection
	 */
	public Collection<ServicoTipo> listarServicoTipo() {

		StringBuilder hql = new StringBuilder();

		hql.append("SELECT new ServicoTipo(servico.chavePrimaria, servico.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servico");
		hql.append(" inner join servico.servicoTipoPrioridade tipo");
		hql.append(" inner join servico.documentoImpressaoLayout documento");

		hql.append(" order by servico.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}
	
	
	/**
	 * Listar servico tipo.
	 * 
	 * @return the collection
	 */
	public Collection<ServicoTipo> listarServicosTipoAtivos() {

		StringBuilder hql = new StringBuilder();

		hql.append("SELECT new ServicoTipo(servico.chavePrimaria, servico.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servico");
		hql.append(" inner join servico.servicoTipoPrioridade tipo");
		hql.append(" inner join servico.documentoImpressaoLayout documento");
		hql.append(" where servico.habilitado = :habilitado ");
		hql.append(" order by servico.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);

		return query.list();

	}

	/**
	 * Consultar servico tipo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo
	 */
	public ServicoTipo consultarServicoTipo(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servico ");
		hql.append(WHERE);
		hql.append(" servico.chavePrimaria = :chavePrimaria ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return (ServicoTipo) query.uniqueResult();
	}

	/**
	 * Consultar servico tipo com prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo
	 */
	public ServicoTipo consultarServicoTipoComPrioridade(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servico ");
		hql.append(" inner join fetch servico.servicoTipoPrioridade");
		hql.append(" inner join fetch servico.servicoTipoPrioridade.indicadorDiaCorridoUtil");
		hql.append(WHERE);
		hql.append(" servico.chavePrimaria = :chavePrimaria ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return (ServicoTipo) query.uniqueResult();
	}

	/**
	 * Listar servico tipo agendamento.
	 * 
	 * @param indicadorAgendamento
	 *            the indicador agendamento
	 * @param temServicoAutorizacao
	 * 				Indicador de serviço
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ServicoTipo> listarServicoTipoAgendamento(Boolean indicadorAgendamento, Boolean temServicoAutorizacao) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(SERVICO_TIPO);
		hql.append(" where servicoTipo.indicadorAgendamento = :indicadorAgendamento");
		hql.append(" and servicoTipo.habilitado = :habilitado ");
		
		if(temServicoAutorizacao!=null && temServicoAutorizacao){
			hql.append(" and servicoTipo.chavePrimaria in ( ");
			hql.append(" select distinct servicoAutorizacao.servicoTipo.chavePrimaria ");
			hql.append(FROM);
			hql.append(getClasseEntidadeServicoAutorizacao().getSimpleName());
			hql.append(" servicoAutorizacao ");
			hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo");
			hql.append(WHERE);
			hql.append(" servicoAutorizacao.chavePrimaria not in ( ");
			hql.append(" select  agenda.servicoAutorizacao.chavePrimaria ");
			hql.append(FROM);
			hql.append(getClasseEntidadeServicoAgendamento().getSimpleName());
			hql.append(" agenda )");
			hql.append(" )");
		}
				
		hql.append(" order by servicoTipo.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
		query.setParameter(INDICADOR_AGENDAMENTO, indicadorAgendamento);
		return query.list();
	}
	
	/**
	 * Obter todos servico tipo com pesquisa satisfacao.
	 * 
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ServicoTipo> obterTodosServicoTipoComPesquisaSatisfacao() {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(SERVICO_TIPO);
		hql.append(" where 1=1 ");
		hql.append(" and servicoTipo.habilitado = true ");
		hql.append(" and indicadorPesquisaSatisfacao = 1 ");
		hql.append(" order by servicoTipo.descricao asc ");
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString()).list();
	}

	@SuppressWarnings("unchecked")
	public Collection<ServicoTipo> consultarServicoTipoComunicacao() {
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(SERVICO_TIPO);
		hql.append(" where 1=1 ");
		hql.append(" and servicoTipo.documentoImpressaoAviso is not null ");
		hql.append(" order by servicoTipo.descricao asc ");
		
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString()).list();
		
	}
	
	public Equipe obterEquipePrioritaria(Long idServicoTipo) {
		StringBuilder hql = new StringBuilder();
		hql.append("select equipe ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(SERVICO_TIPO);
		hql.append(" inner join  servicoTipo.equipePrioritaria equipe ");
		hql.append(" where servicoTipo.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("chavePrimaria", idServicoTipo);

		return (Equipe) query.uniqueResult();
	}
	
	public Collection<ServicoTipo> listarServicosTipoAtivosParaNovoMedidor() {

		StringBuilder hql = new StringBuilder();

		hql.append("SELECT new ServicoTipo(servico.chavePrimaria, servico.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servico");
		hql.append(" inner join servico.servicoTipoPrioridade tipo");
		hql.append(" inner join servico.documentoImpressaoLayout documento");
		hql.append(" where servico.habilitado = :habilitado ");
		hql.append(" and servico.indicadorNovoMedidor = :indicadorHabilitado");
		hql.append(" order by servico.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
		query.setParameter("indicadorHabilitado", Boolean.TRUE);
		return query.list();

	}
	
}
