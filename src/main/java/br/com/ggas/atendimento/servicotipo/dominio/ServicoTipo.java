/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *23/09/2013
 * vpessoa
 * 18:18:15
 */

package br.com.ggas.atendimento.servicotipo.dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.servicoautorizacao.dominio.AcaoServicoAutorizacao;
import br.com.ggas.atendimento.servicotipoagendamentoturno.dominio.ServicoTipoAgendamentoTurno;
import br.com.ggas.atendimento.servicotipoequipamento.dominio.ServicoTipoEquipamento;
import br.com.ggas.atendimento.servicotipomaterial.dominio.ServicoTipoMaterial;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.util.Constantes;

/**
 * Classe responsavel pelo Servico Tipo
 * 
 * @author vpessoa
 */
public class ServicoTipo extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 1L;

	private String descricao;

	private String orientacao;

	private ServicoTipoPrioridade servicoTipoPrioridade;

	private Long quantidadeTempoMedio;

	private Boolean indicadorServicoRegulamento;

	private Long quantidadePrazoExecucao;

	private Boolean indicadorAgendamento;

	private Boolean indicadorEquipamento;

	private Boolean indicadorVeiculo;

	private Boolean indicadorTramitacao;

	private Boolean indicadorPesquisaSatisfacao;

	private Boolean indicadorGeraLote;

	private DocumentoImpressaoLayout documentoImpressaoLayout;

	private EntidadeConteudo telaAtualizacao;

	private String indicadorAtualizacaoCadastral;

	private String indicadorTipoAssociacaoPontoConsumo;

	private String indicadorOperacaoMedidor;

	private Boolean indicadorClienteObrigatorio;

	private Boolean indicadorImovelObrigatorio;

	private Boolean indicadorContratoObrigatorio;

	private Boolean indicadorPontoConsumoObrigatorio;

	private Boolean indicadorProximaFatura;

	private Boolean indicadorCobranca;
	
	private Boolean indicadorEmailCadastrar;
	
	private Boolean indicadorEmailEmExecucao;

	private Boolean indicadorEmailExecucao;

	private Boolean indicadorEmailEncerrar;

	private Boolean indicadorEmailAlterar;
	
	private Boolean indicadorEmailAgendar;
	
	private Boolean indicadorEmailCliente;
	
	private Boolean indicadorEncerramentoAuto;

	private Boolean indicadorFormularioObrigatorio;

	private Questionario formulario;

	private Collection<ServicoTipoMaterial> listaMateriais = new HashSet<>();

	private Collection<ServicoTipoEquipamento> listaEquipamentoEspecial = new HashSet<>();

	private Collection<ServicoTipoAgendamentoTurno> listaAgendamentosTurno = new HashSet<>();
	
	private Collection<ServicoTipoAgendamentoTurno> listaEquipe = new HashSet<>();

	private Long numeroMaximoExecucoes;
	private Long prazoGarantia;
	
	private Boolean indicadorProtocolo;
	
	private Boolean indicadorProtocoloObrigatorio;
	
	private Boolean indicadorTipoProtocolo;
	
	private Boolean indicadorNotificacao;
	
	private Boolean indicadorIntegracao;
	
	private Long tempoRetornoProtocolo;
	
	private Long tempoEmissaoAS;
	
	private DocumentoImpressaoLayout documentoImpressaoAviso;
	
	private Boolean indicadorAviso;

	private Boolean indicadorMetadeTempoExecucao;
	
	private Boolean indicadorNovoMedidor;

	private Equipe equipePrioritaria;
	
	private Boolean indicadorServicoMobile;
	
	private Boolean indicadorAssinatura;
	
	private MotivoOperacaoMedidor motivoOperacaoMedidor;
	
	private Boolean indicadorEmailPersonalizado;
	
	private String listaEmailPersonalizado;
	
	private String layoutEmail;
	
	private String assuntoEmail;
	
	/** The Constant CORTE. */
	public static final String CORTE = "CORTE";

	/**
	 * Meotodo construtor da classe ServicoTipo com 0 parametros
	 * 
	 */
	public ServicoTipo(){
	}
	
	/**
	 * Meotodo construtor da classe ServicoTipo com N parametros
	 *
	 * @param chavePrimaria {@link Long}
	 * @param descricao {@link String}
	 */
	public ServicoTipo(Long chavePrimaria, String descricao) {
		this.setChavePrimaria(chavePrimaria);
		this.descricao = descricao;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public ServicoTipoPrioridade getServicoTipoPrioridade() {

		return servicoTipoPrioridade;
	}

	public void setServicoTipoPrioridade(ServicoTipoPrioridade servicoTipoPrioridade) {

		this.servicoTipoPrioridade = servicoTipoPrioridade;
	}

	public Long getQuantidadeTempoMedio() {

		return quantidadeTempoMedio;
	}

	public void setQuantidadeTempoMedio(Long quantidadeTempoMedio) {

		this.quantidadeTempoMedio = quantidadeTempoMedio;
	}

	public Boolean getIndicadorServicoRegulamento() {

		return indicadorServicoRegulamento;
	}

	public void setIndicadorServicoRegulamento(Boolean indicadorServicoRegulamento) {

		this.indicadorServicoRegulamento = indicadorServicoRegulamento;
	}

	public Long getQuantidadePrazoExecucao() {

		return quantidadePrazoExecucao;
	}

	public void setQuantidadePrazoExecucao(Long quantidadePrazoExecucao) {

		this.quantidadePrazoExecucao = quantidadePrazoExecucao;
	}

	public Boolean getIndicadorAgendamento() {

		return indicadorAgendamento;
	}

	public void setIndicadorAgendamento(Boolean indicadorAgendamento) {

		this.indicadorAgendamento = indicadorAgendamento;
	}

	public Boolean getIndicadorEquipamento() {

		return indicadorEquipamento;
	}

	public void setIndicadorEquipamento(Boolean indicadorEquipamento) {

		this.indicadorEquipamento = indicadorEquipamento;
	}

	public Boolean getIndicadorVeiculo() {

		return indicadorVeiculo;
	}

	public void setIndicadorVeiculo(Boolean indicadorVeiculo) {

		this.indicadorVeiculo = indicadorVeiculo;
	}

	public Boolean getIndicadorTramitacao() {

		return indicadorTramitacao;
	}

	public void setIndicadorTramitacao(Boolean indicadorTramitacao) {

		this.indicadorTramitacao = indicadorTramitacao;
	}

	public Boolean getIndicadorPesquisaSatisfacao() {

		return indicadorPesquisaSatisfacao;
	}

	public void setIndicadorPesquisaSatisfacao(Boolean indicadorPesquisaSatisfacao) {

		this.indicadorPesquisaSatisfacao = indicadorPesquisaSatisfacao;
	}

	public Boolean getIndicadorGeraLote() {

		return indicadorGeraLote;
	}

	public void setIndicadorGeraLote(Boolean indicadorGeraLote) {

		this.indicadorGeraLote = indicadorGeraLote;
	}

	public DocumentoImpressaoLayout getDocumentoImpressaoLayout() {

		return documentoImpressaoLayout;
	}

	public void setDocumentoImpressaoLayout(DocumentoImpressaoLayout documentoImpressaoLayout) {

		this.documentoImpressaoLayout = documentoImpressaoLayout;
	}

	public Collection<ServicoTipoMaterial> getListaMateriais() {

		return listaMateriais;
	}

	public void setListaMateriais(Collection<ServicoTipoMaterial> listaMateriais) {

		this.listaMateriais = listaMateriais;
	}

	public Collection<ServicoTipoEquipamento> getListaEquipamentoEspecial() {

		return listaEquipamentoEspecial;
	}

	public void setListaEquipamentoEspecial(Collection<ServicoTipoEquipamento> listaEquipamentoEspecial) {

		this.listaEquipamentoEspecial = listaEquipamentoEspecial;
	}

	public Collection<ServicoTipoAgendamentoTurno> getListaAgendamentosTurno() {

		return listaAgendamentosTurno;
	}

	public void setListaAgendamentosTurno(Collection<ServicoTipoAgendamentoTurno> listaAgendamentosTurno) {

		this.listaAgendamentosTurno = listaAgendamentosTurno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(descricao == null || descricao.isEmpty() || descricao.trim().length() == 0) {
			stringBuilder.append(Constantes.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(servicoTipoPrioridade == null) {
			stringBuilder.append(Constantes.PRIORIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(documentoImpressaoLayout == null) {
			stringBuilder.append(Constantes.DOCUMENTO_LAYOUT);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if((indicadorAtualizacaoCadastral.equals(Constantes.ATUALIZACAO_CADASTRAL_IMEDIATA) || indicadorAtualizacaoCadastral
						.equals(Constantes.ATUALIZACAO_CADASTRAL_POSTERIOR)) && telaAtualizacao == null) {
			stringBuilder.append(Constantes.TELA_ATUALIZACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (indicadorFormularioObrigatorio && formulario == null) {
			stringBuilder.append("Formulário");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(indicadorProtocolo && (tempoEmissaoAS == null || tempoEmissaoAS < 0)) {
			stringBuilder.append("Prazo para Emissão da AS após conclusão do protocolo");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(indicadorProtocoloObrigatorio && (tempoRetornoProtocolo == null || tempoRetornoProtocolo < 0)) {
			stringBuilder.append("Prazo de Retorno do Protocolo");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(indicadorAviso && (documentoImpressaoAviso == null || documentoImpressaoAviso.getChavePrimaria() <= 0)) {
			stringBuilder.append("Layout do Protocolo");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(quantidadeTempoMedio == null || quantidadeTempoMedio < 0) {
			stringBuilder.append("Tempo médio para execução do serviço");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(indicadorEmailPersonalizado && (StringUtils.isEmpty(listaEmailPersonalizado) || StringUtils.isEmpty(layoutEmail) || StringUtils.isEmpty(assuntoEmail)) ) {
			stringBuilder.append("Lista Email Personalizada, Layout Email e Assunto Email");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;

	}

	public String getIndicadorAtualizacaoCadastral() {

		return indicadorAtualizacaoCadastral;
	}

	public void setIndicadorAtualizacaoCadastral(String indicadorAtualizacaoCadastral) {

		this.indicadorAtualizacaoCadastral = indicadorAtualizacaoCadastral;
	}

	public EntidadeConteudo getTelaAtualizacao() {

		return telaAtualizacao;
	}

	public void setTelaAtualizacao(EntidadeConteudo telaAtualizacao) {

		this.telaAtualizacao = telaAtualizacao;
	}

	public String getIndicadorOperacaoMedidor() {

		return indicadorOperacaoMedidor;
	}

	public void setIndicadorOperacaoMedidor(String indicadorOperacaoMedidor) {

		this.indicadorOperacaoMedidor = indicadorOperacaoMedidor;
	}

	public String getIndicadorTipoAssociacaoPontoConsumo() {

		return indicadorTipoAssociacaoPontoConsumo;
	}

	public void setIndicadorTipoAssociacaoPontoConsumo(String indicadorTipoAssociacaoPontoConsumo) {

		this.indicadorTipoAssociacaoPontoConsumo = indicadorTipoAssociacaoPontoConsumo;
	}

	public Boolean getIndicadorClienteObrigatorio() {

		return indicadorClienteObrigatorio;
	}

	public void setIndicadorClienteObrigatorio(Boolean indicadorClienteObrigatorio) {

		this.indicadorClienteObrigatorio = indicadorClienteObrigatorio;
	}

	public Boolean getIndicadorImovelObrigatorio() {

		return indicadorImovelObrigatorio;
	}

	public void setIndicadorImovelObrigatorio(Boolean indicadorImovelObrigatorio) {

		this.indicadorImovelObrigatorio = indicadorImovelObrigatorio;
	}

	public Boolean getIndicadorContratoObrigatorio() {

		return indicadorContratoObrigatorio;
	}

	public void setIndicadorContratoObrigatorio(Boolean indicadorContratoObrigatorio) {

		this.indicadorContratoObrigatorio = indicadorContratoObrigatorio;
	}

	public Boolean getIndicadorPontoConsumoObrigatorio() {

		return indicadorPontoConsumoObrigatorio;
	}

	public void setIndicadorPontoConsumoObrigatorio(Boolean indicadorPontoConsumoObrigatorio) {

		this.indicadorPontoConsumoObrigatorio = indicadorPontoConsumoObrigatorio;
	}

	public Boolean getIndicadorProximaFatura() {

		return indicadorProximaFatura;
	}

	public void setIndicadorProximaFatura(Boolean indicadorProximaFatura) {

		this.indicadorProximaFatura = indicadorProximaFatura;
	}

	public Boolean getIndicadorCobranca() {

		return indicadorCobranca;
	}

	public void setIndicadorCobranca(Boolean indicadorCobranca) {

		this.indicadorCobranca = indicadorCobranca;
	}

	/**
	 * @return the indicadorEmailEncerrar
	 */
	public Boolean getIndicadorEmailEncerrar() {
		return indicadorEmailEncerrar;
	}

	/**
	 * @param indicadorEmailEncerrar the indicadorEmailEncerrar to set
	 */
	public void setIndicadorEmailEncerrar(Boolean indicadorEmailEncerrar) {
		this.indicadorEmailEncerrar = indicadorEmailEncerrar;
	}

	/**
	 * @return the indicadorEmailCadastrar
	 */
	public Boolean getIndicadorEmailCadastrar() {
		return indicadorEmailCadastrar;
	}

	/**
	 * @param indicadorEmailCadastrar the indicadorEmailCadastrar to set
	 */
	public void setIndicadorEmailCadastrar(Boolean indicadorEmailCadastrar) {
		this.indicadorEmailCadastrar = indicadorEmailCadastrar;
	}

	/**
	 * @return the indicadorEmailEmExecucao
	 */
	public Boolean getIndicadorEmailEmExecucao() {
		return indicadorEmailEmExecucao;
	}

	/**
	 * @param indicadorEmailEmExecucao the indicadorEmailEmExecucao to set
	 */
	public void setIndicadorEmailEmExecucao(Boolean indicadorEmailEmExecucao) {
		this.indicadorEmailEmExecucao = indicadorEmailEmExecucao;
	}

	/**
	 * @return the indicadorEmailExecucao
	 */
	public Boolean getIndicadorEmailExecucao() {
		return indicadorEmailExecucao;
	}

	/**
	 * @param indicadorEmailExecucao the indicadorEmailExecucao to set
	 */
	public void setIndicadorEmailExecucao(Boolean indicadorEmailExecucao) {
		this.indicadorEmailExecucao = indicadorEmailExecucao;
	}

	/**
	 * @return the indicadorEmailAlterar
	 */
	public Boolean getIndicadorEmailAlterar() {
		return indicadorEmailAlterar;
	}

	/**
	 * @param indicadorEmailAlterar the indicadorEmailAlterar to set
	 */
	public void setIndicadorEmailAlterar(Boolean indicadorEmailAlterar) {
		this.indicadorEmailAlterar = indicadorEmailAlterar;
	}
	
	/**
	 * @param getIndicadorEmailAgendar the getIndicadorEmailAgendar to set
	 */
	
	public Boolean getIndicadorEmailAgendar() {
		return indicadorEmailAgendar;
	}
	/**
	 * @return the indicadorEmailAgendar
	 */
	public void setIndicadorEmailAgendar(Boolean indicadorEmailAgendar) {
		this.indicadorEmailAgendar = indicadorEmailAgendar;
	}
	
	/**
	 * @return the indicadorEmailCliente 
	 */
	public Boolean getIndicadorEmailCliente() {
		return indicadorEmailCliente;
	}
	/**
	 * @param indicadorEmailCliente the indicadorEmailCliente to set
	 */
	public void setIndicadorEmailCliente(Boolean indicadorEmailCliente) {
		this.indicadorEmailCliente = indicadorEmailCliente;
	}

	/**
	 * @return lista acoes serviço autorização
	 */
	public Collection<AcaoServicoAutorizacao> getListaAcaoComEmail() {
		HashSet<AcaoServicoAutorizacao> listaAcoes = new HashSet<>();

		if (getIndicadorEmailCadastrar()) {
			listaAcoes.add(AcaoServicoAutorizacao.INSERIR);
		}
		if (getIndicadorEmailEmExecucao()) {
			listaAcoes.add(AcaoServicoAutorizacao.EXECUTANDO);
		}
		if (getIndicadorEmailExecucao()) {
			listaAcoes.add(AcaoServicoAutorizacao.EXECUTAR);
		}
		if (getIndicadorEmailEncerrar()) {
			listaAcoes.add(AcaoServicoAutorizacao.ENCERRAR);
		}
		if (getIndicadorEmailAlterar()) {
			listaAcoes.add(AcaoServicoAutorizacao.ALTERAR);
		}

		return listaAcoes;
	}

	/**
	 * @return the indicadorEncerramentoAuto
	 */
	public Boolean getIndicadorEncerramentoAuto() {
		return indicadorEncerramentoAuto;
	}

	/**
	 * @param indicadorEncerramentoAuto the indicadorEncerramentoAuto to set
	 */
	public void setIndicadorEncerramentoAuto(Boolean indicadorEncerramentoAuto) {
		this.indicadorEncerramentoAuto = indicadorEncerramentoAuto;
	}

	/**
	 * @return the orientacao
	 */
	public String getOrientacao() {
		return orientacao;
	}

	/**
	 * @param orientacao the orientacao to set
	 */
	public void setOrientacao(String orientacao) {
		this.orientacao = orientacao;
	}

	/**
	 * @return indicadorFormularioObrigatorio
	 */
	public Boolean getIndicadorFormularioObrigatorio() {
		return indicadorFormularioObrigatorio;
	}

	/**
	 * @param indicadorFormularioObrigatorio
	 */
	public void setIndicadorFormularioObrigatorio(Boolean indicadorFormularioObrigatorio) {
		this.indicadorFormularioObrigatorio = indicadorFormularioObrigatorio;
	}

	/**
	 * @return formulario
	 */
	public Questionario getFormulario() {
		return formulario;
	}

	/**
	 * @param formulario
	 */
	public void setFormulario(Questionario formulario) {
		this.formulario = formulario;
	}

	public Long getNumeroMaximoExecucoes() {
		return numeroMaximoExecucoes;
	}

	public void setNumeroMaximoExecucoes(Long numeroMaximoExecucoes) {
		this.numeroMaximoExecucoes = numeroMaximoExecucoes;
	}

	public Long getPrazoGarantia() {
		return prazoGarantia;
	}

	public void setPrazoGarantia(Long prazoGarantia) {
		this.prazoGarantia = prazoGarantia;
	}

	/**
	 * @return the listaEquipe
	 */
	public Collection<ServicoTipoAgendamentoTurno> getListaEquipe() {
		return listaEquipe;
	}

	/**
	 * @param listaEquipe the listaEquipe to set
	 */
	public void setListaEquipe(Collection<ServicoTipoAgendamentoTurno> listaEquipe) {
		this.listaEquipe = listaEquipe;
	}

	public Boolean getIndicadorProtocolo() {
		return indicadorProtocolo;
	}

	public void setIndicadorProtocolo(Boolean indicadorProtocolo) {
		this.indicadorProtocolo = indicadorProtocolo;
	}

	public Boolean getIndicadorProtocoloObrigatorio() {
		return indicadorProtocoloObrigatorio;
	}

	public void setIndicadorProtocoloObrigatorio(Boolean indicadorProtocoloObrigatorio) {
		this.indicadorProtocoloObrigatorio = indicadorProtocoloObrigatorio;
	}

	public Boolean getIndicadorTipoProtocolo() {
		return indicadorTipoProtocolo;
	}

	public void setIndicadorTipoProtocolo(Boolean indicadorTipoProtocolo) {
		this.indicadorTipoProtocolo = indicadorTipoProtocolo;
	}

	public Boolean getIndicadorNotificacao() {
		return indicadorNotificacao;
	}

	public void setIndicadorNotificacao(Boolean indicadorNotificacao) {
		this.indicadorNotificacao = indicadorNotificacao;
	}

	public Boolean getIndicadorIntegracao() {
		return indicadorIntegracao;
	}

	public void setIndicadorIntegracao(Boolean indicadorIntegracao) {
		this.indicadorIntegracao = indicadorIntegracao;
	}

	public Long getTempoRetornoProtocolo() {
		return tempoRetornoProtocolo;
	}

	public void setTempoRetornoProtocolo(Long tempoRetornoProtocolo) {
		this.tempoRetornoProtocolo = tempoRetornoProtocolo;
	}

	public Long getTempoEmissaoAS() {
		return tempoEmissaoAS;
	}

	public void setTempoEmissaoAS(Long tempoEmissaoAS) {
		this.tempoEmissaoAS = tempoEmissaoAS;
	}

	public DocumentoImpressaoLayout getDocumentoImpressaoAviso() {
		return documentoImpressaoAviso;
	}

	public void setDocumentoImpressaoAviso(DocumentoImpressaoLayout documentoImpressaoAviso) {
		this.documentoImpressaoAviso = documentoImpressaoAviso;
	}

	public Boolean getIndicadorAviso() {
		return indicadorAviso;
	}

	public void setIndicadorAviso(Boolean indicadorAviso) {
		this.indicadorAviso = indicadorAviso;
	}

	public Boolean getIndicadorMetadeTempoExecucao() {
		return indicadorMetadeTempoExecucao;
	}

	public void setIndicadorMetadeTempoExecucao(Boolean indicadorMetadeTempoExecucao) {
		this.indicadorMetadeTempoExecucao = indicadorMetadeTempoExecucao;
	}

	public Boolean getIndicadorNovoMedidor() {
		return indicadorNovoMedidor;
	}

	public void setIndicadorNovoMedidor(Boolean indicadorNovoMedidor) {
		this.indicadorNovoMedidor = indicadorNovoMedidor;
	}
	
	public Equipe getEquipePrioritaria() {
		return equipePrioritaria;
	}

	public void setEquipePrioritaria(Equipe equipePrioritaria) {
		this.equipePrioritaria = equipePrioritaria;
	}

	public Boolean getIndicadorServicoMobile() {
		return indicadorServicoMobile;
	}

	public void setIndicadorServicoMobile(Boolean indicadorServicoMobile) {
		this.indicadorServicoMobile = indicadorServicoMobile;
	}

	public Boolean getIndicadorAssinatura() {
		return indicadorAssinatura;
	}

	public void setIndicadorAssinatura(Boolean indicadorAssinatura) {
		this.indicadorAssinatura = indicadorAssinatura;
	}

	public MotivoOperacaoMedidor getMotivoOperacaoMedidor() {
		return motivoOperacaoMedidor;
	}

	public void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor) {
		this.motivoOperacaoMedidor = motivoOperacaoMedidor;
	}

	public Boolean getIndicadorEmailPersonalizado() {
		return indicadorEmailPersonalizado;
	}

	public void setIndicadorEmailPersonalizado(Boolean indicadorEmailPersonalizado) {
		this.indicadorEmailPersonalizado = indicadorEmailPersonalizado;
	}

	public String getListaEmailPersonalizado() {
		return listaEmailPersonalizado;
	}

	public void setListaEmailPersonalizado(String listaEmailPersonalizado) {
		this.listaEmailPersonalizado = listaEmailPersonalizado;
	}

	public String getLayoutEmail() {
		return layoutEmail;
	}

	public void setLayoutEmail(String layoutEmail) {
		this.layoutEmail = layoutEmail;
	}

	public String getAssuntoEmail() {
		return assuntoEmail;
	}

	public void setAssuntoEmail(String assuntoEmail) {
		this.assuntoEmail = assuntoEmail;
	}
}
