package br.com.ggas.atendimento.servicotipo.apresentacao;

import java.lang.reflect.Method;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.util.DataUtil;

public class RelatorioServicoTipoHelper {
	private static final Logger LOG = Logger.getLogger(RelatorioServicoTipoHelper.class);
	
	private String layout = null;
	private ServicoAutorizacao servicoAutorizacao;
	private ServicoAutorizacaoTemp servicoAutorizacaoTemp;
	
	/**
	 * Substitui os tokens do modelo de impressao pelos valores do contrato
	 * 
	 * @param tokens os tokens que serão processados
	 * @throws GGASException
	 */
	public void processarLayout(Collection<RelatorioToken> tokens) {
		if (this.layout != null) {
			for (RelatorioToken token : tokens) {
				this.layout = this.layout.replaceAll("\\[\\[" + token.getValor() + "\\]\\]",
						obterValor(token.getAlias()));
			}
		}

	}
	
	/**
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * @param layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}
	
	/**
	 * Obtem o valor do contrato de um token
	 * 
	 * @param alias
	 * @return
	 * @throws GGASException
	 */
	private String obterValor(String alias) {

		String valor = "";

		Class<RelatorioServicoTipoHelper> classe = RelatorioServicoTipoHelper.class;

		try {
			Method metodoDoSeuObjeto = classe.getMethod(alias);
			valor = (String) metodoDoSeuObjeto.invoke(this);
		} catch (Exception e) {
			LOG.error("Método não encontrado", e);
			return "";
		}

		return valor;
	}

	public ServicoAutorizacao getServicoAutorizacao() {
		return servicoAutorizacao;
	}

	public void setServicoAutorizacao(ServicoAutorizacao servicoAutorizacao) {
		this.servicoAutorizacao = servicoAutorizacao;
	}
	
	public String getNomeServico() {
		ServicoTipo servicoTipo = servicoAutorizacao.getServicoTipo();
		
		return servicoTipo != null ? servicoTipo.getDescricao() : "";
	}
	
	public String getNumeroAS() {
		return servicoAutorizacao != null ? String.valueOf(servicoAutorizacao.getChavePrimaria()) : "";
	}
	
	public String getDataGeracao() {
		return servicoAutorizacao != null && servicoAutorizacao.getDataGeracao() != null
				? DataUtil.converterCampoDataParaStringComDiaDaSemana(servicoAutorizacao.getDataGeracao())
				: "";
	}
	
	public String getDataExecucao() {
		return servicoAutorizacao != null && servicoAutorizacao.getDataExecucao() != null
				? DataUtil.converterCampoDataParaStringComDiaDaSemana(servicoAutorizacao.getDataExecucao())
				: "";
	}
	
	public String getDataEncerramento() {
		return servicoAutorizacao != null && servicoAutorizacao.getDataEncerramento() != null
				? DataUtil.converterCampoDataParaStringComDiaDaSemana(servicoAutorizacao.getDataEncerramento())
				: "";
	}
	
	public String getNomeCliente() {
		Cliente cliente = servicoAutorizacao.getCliente();
		
		return cliente != null ? cliente.getNome() : "";
		
	}
	
	public String getPontoConsumo() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		
		return pontoConsumo != null ? pontoConsumo.getDescricao() : "";
	}
	
	public String getNumeroContrato() {
		Contrato contrato = servicoAutorizacao.getContrato();
		
		return contrato != null ? contrato.getNumeroCompletoContrato() : "";
	}
	
	public String getNomeImovel() {
		Imovel imovel = servicoAutorizacao.getImovel();
		
		
		
		return imovel != null ? imovel.getNome() : "";
	}
	
	public String getEndereco() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		Imovel imovel = servicoAutorizacao.getImovel();
		
		if(pontoConsumo != null) {
			return pontoConsumo.getEnderecoFormatado();
		} else if (imovel != null) {
			return imovel.getEnderecoFormatado();
		} else {
			return "";
		}
	}

	public ServicoAutorizacaoTemp getServicoAutorizacaoTemp() {
		return servicoAutorizacaoTemp;
	}

	public void setServicoAutorizacaoTemp(ServicoAutorizacaoTemp servicoAutorizacaoTemp) {
		this.servicoAutorizacaoTemp = servicoAutorizacaoTemp;
	}
	
	
	public String getOperador() {
		return servicoAutorizacaoTemp != null && servicoAutorizacaoTemp.getFuncionario() != null ? servicoAutorizacaoTemp.getFuncionario().getNome() : "";
	}
	
	
	public String getEquipe() {
		return servicoAutorizacao.getEquipe() != null ? servicoAutorizacao.getEquipe().getNome() : "";
	}
	
	public String getProtocolo() {
		return servicoAutorizacao.getChamado() != null ? String.valueOf(servicoAutorizacao.getChamado().getProtocolo().getNumeroProtocolo()) : "";
	}
	
	public String getSegmento() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		
		return pontoConsumo != null && pontoConsumo.getSegmento() != null ? pontoConsumo.getSegmento().getDescricao() : "";
	}
	
	public String getTipoMedicao() {
		Imovel imovel = servicoAutorizacao.getImovel();
		ModalidadeMedicaoImovel modalidadeMedicaoImovel = imovel != null ? imovel.getModalidadeMedicaoImovel() : null;
		
		if(modalidadeMedicaoImovel != null) {
			return  modalidadeMedicaoImovel.getDescricao();
		} else {
			return "";
		}
	}
	
	public String getUsoGas() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		
		return pontoConsumo != null && pontoConsumo.getModalidadeUso() != null ? pontoConsumo.getModalidadeUso().getDescricao() : "";
	}
	
	public String getNumeroSerieMedidor() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		
		InstalacaoMedidor instalacaoMedidor = pontoConsumo != null ? pontoConsumo.getInstalacaoMedidor() : null;
		
		if(instalacaoMedidor != null) {
			return instalacaoMedidor.getMedidor().getNumeroSerie();
		} else {
			return "";
		}
	}
	
	public String getFabricanteMedidor() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		
		InstalacaoMedidor instalacaoMedidor = pontoConsumo != null ? pontoConsumo.getInstalacaoMedidor() : null;
		
		if(instalacaoMedidor != null) {
			return instalacaoMedidor.getMedidor().getMarcaMedidor().getDescricao();
		} else {
			return "";
		}
	}
	
	public String getModeloMedidor() {
		PontoConsumo pontoConsumo = servicoAutorizacao.getPontoConsumo();
		
		InstalacaoMedidor instalacaoMedidor = pontoConsumo != null ? pontoConsumo.getInstalacaoMedidor() : null;
		
		if(instalacaoMedidor != null) {
			return instalacaoMedidor.getMedidor().getModelo().getDescricao();
		} else {
			return "";
		}
	}
	
	public String getPressao() {
		Contrato contrato = servicoAutorizacao.getContrato();
		
		if(contrato != null) {
			ContratoPontoConsumo contratoPontoConsumo = contrato.getListaContratoPontoConsumo().iterator().next();
			
			return contratoPontoConsumo != null && contratoPontoConsumo.getMedidaPressao() != null ? contratoPontoConsumo.getMedidaPressao() + " " + contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada() : "";
		} else {
			return "";
		}
	}
	
	public String getVazao() {
		Contrato contrato = servicoAutorizacao.getContrato();
		
		if(contrato != null) {
			ContratoPontoConsumo contratoPontoConsumo = contrato.getListaContratoPontoConsumo().iterator().next();
			
			return contratoPontoConsumo != null && contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() != null ? contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() + " " + contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria().getDescricaoAbreviada() : "";
		} else {
			return "";
		}
	}
	
	public String getObservacaoAS() {
		
		if (servicoAutorizacaoTemp != null && StringUtils.isNotEmpty(servicoAutorizacaoTemp.getObservacao())) {
			return servicoAutorizacaoTemp.getObservacao();
		} else if(StringUtils.isNotEmpty(servicoAutorizacao.getDescricao())) {
			return servicoAutorizacao.getDescricao();
		} else {
			return "";
		}
		
	}
	
	public String getSituacaoAS() {
		return servicoAutorizacao.getStatus() != null ? servicoAutorizacao.getStatus().getDescricao() : "";
	}
	
}
