
package br.com.ggas.atendimento.chamadotipo.negocio;

import java.util.Collection;

import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pelos métodos do tipo de chamado
 */
public interface ControladorChamadoTipo {

	/**
	 * Listar todos.
	 * 
	 * @param descricao {@link String}
	 * @param indicador {@link Boolean}
	 * @param idSegmento {@link Long}
	 * @param categoria {@link ChamadoCategoria}
	 * @return collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<ChamadoTipo> listarTodos(String descricao, Boolean indicador, Long idSegmento, ChamadoCategoria categoria)
			throws NegocioException;

	/**
	 * Inserir chamado tipo.
	 * 
	 * @param chamadoTipo
	 *            the chamado tipo
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long inserirChamadoTipo(ChamadoTipo chamadoTipo) throws NegocioException;

	/**
	 * Consultar chamado tipo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the chamado tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ChamadoTipo consultarChamadoTipo(Long chavePrimaria) throws NegocioException;

	/**
	 * Remover chamado tipo.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void removerChamadoTipo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar chamado tipo.
	 * 
	 * @param chamadoTipo
	 *            the chamado tipo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGASException exception
	 */
	public void atualizarChamadoTipo(ChamadoTipo chamadoTipo) throws GGASException;

	/**
	 * Verificar descricao.
	 * 
	 * @param descricao
	 *            the descricao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarDescricao(String descricao) throws NegocioException;

	/**
	 * Consultar inserir vazio.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param chamadoAssunto
	 *            the chamado assunto
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void consultarInserirVazio(ChamadoTipo chamado, Collection<ChamadoAssunto> chamadoAssunto) throws NegocioException;

	/**
	 * Validar chamado tipo existente.
	 * 
	 * @param chamado
	 *            the chamado
	 * @return the chamado tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ChamadoTipo validarChamadoTipoExistente(ChamadoTipo chamado) throws NegocioException;

	/**
	 * Obter todos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodos() throws NegocioException;

	/**
	 * Obter.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeNegocio obter(long chavePrimaria) throws NegocioException;

	/**
	 * Obtem chamado tipo por id do segmento.
	 *
	 * @param idSegmento a chave primaria do segmento
	 * @return a coleção
	 * @throws NegocioException the negocio exception
	 */
	Collection<ChamadoTipo> listarPorSegmento(long idSegmento) throws NegocioException;

	/**
	 * Retorna os chamados tipos que estão habilitados
	 * @return uma lista dos chamados tipos habilitados
	 */
	Collection<ChamadoTipo> listarTodosHabilitado();

	/**
	 * Projeta o chamado tipo pela chave primária
	 * @param chavePrimaria utilizada para filtrar o chamado tipo
	 * @return um chamado tipo projetado
	 */
	ChamadoTipo carregarChamadoTipoProjetado(Long chavePrimaria);


}
