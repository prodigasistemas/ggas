
package br.com.ggas.atendimento.chamadotipo.negocio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.repositorio.RepositorioChamadoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelas ações relacionadas ao tipo de chamado
 */
@Service("controladorChamadoTipo")
@Transactional
public class ControladorChamadoTipoImpl implements ControladorChamadoTipo {

	@Autowired
	private RepositorioChamadoTipo repositorio;

	/**
	 * Listar todos.
	 * 
	 *
	 * @param indicador
	 *            the indicador
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<EntidadeNegocio> listarTodos(String indicador) throws NegocioException {

		return repositorio.obterTodas(true);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#inserirChamadoTipo(br.com.ggas.atendimento.chamadotipo.dominio
	 * .ChamadoTipo)
	 */
	@Override
	public Long inserirChamadoTipo(ChamadoTipo chamadoTipo) throws NegocioException {

		return repositorio.inserir(chamadoTipo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#consultarChamadoTipo(java.lang.Long)
	 */
	@Override
	public ChamadoTipo consultarChamadoTipo(Long chavePrimaria) throws NegocioException {

		return (ChamadoTipo) repositorio.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#listarTodos(java.lang.String, java.lang.Boolean,
	 * java.lang.Long, br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria)
	 */
	@Override
	public Collection<ChamadoTipo> listarTodos(String descricao, Boolean indicador, Long idSegmento, ChamadoCategoria categoria)
			throws NegocioException {

		return repositorio.listarTodos(descricao, indicador, idSegmento, categoria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#removerChamadoTipo(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerChamadoTipo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		repositorio.remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#atualizarChamadoTipo(br.com.ggas.atendimento.chamadotipo.dominio
	 * .ChamadoTipo)
	 */
	@Transactional
	@Override
	public void atualizarChamadoTipo(ChamadoTipo chamadoTipo) throws NegocioException, ConcorrenciaException {

		if(!chamadoTipo.getListaAssuntos().isEmpty()) {
			repositorio.atualizar(chamadoTipo);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_VAZIO);
		}
	}

	/**
	 * Verificar existente.
	 * 
	 * @param chamadoTipo
	 *            the chamado tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarExistente(ChamadoTipo chamadoTipo) throws NegocioException {

		ChamadoTipo tipo = repositorio.consultarPorNome(chamadoTipo.getDescricao());

		if (tipo != null && tipo.getChavePrimaria() != chamadoTipo.getChavePrimaria()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_EXISTENTE);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#verificarDescricao(java.lang.String)
	 */
	@Override
	public void verificarDescricao(String descricao) throws NegocioException {

		if(repositorio.consultar(descricao)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_EXISTENTE);
		}
	}

	/**
	 * Verificar chave.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Boolean verificarChave(Long chavePrimaria) throws NegocioException {

		repositorio.obter(chavePrimaria);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#consultarInserirVazio(br.com.ggas.atendimento.chamadotipo.dominio
	 * .ChamadoTipo, java.util.Collection)
	 */
	@Override
	public void consultarInserirVazio(ChamadoTipo chamado, Collection<ChamadoAssunto> chamadoAssunto) throws NegocioException {

		if(chamadoAssunto == null || chamadoAssunto.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_VAZIO);
		}

		if(repositorio.consultarInserirVazio(chamado, chamadoAssunto)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONSULTA_PARAMETRO);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#validarChamadoTipoExistente(br.com.ggas.atendimento.chamadotipo
	 * .dominio.ChamadoTipo)
	 */
	@Override
	public ChamadoTipo validarChamadoTipoExistente(ChamadoTipo chamado) throws NegocioException {

		return repositorio.consultarPorNome(chamado.getDescricao());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#obterTodos()
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodos() throws NegocioException {

		return repositorio.obterTodas(Boolean.TRUE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#obter(long)
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria) throws NegocioException {

		return repositorio.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo#listarTodos(java.lang.String, java.lang.Boolean)
	 */
	@Override
	public Collection<ChamadoTipo> listarPorSegmento(long idSegmento) throws NegocioException {

		return repositorio.listarPorSegmento(idSegmento);
	}

	/**
	 *
	 * @return todos os chamados tipos que estão habilitados, projetando somente os campos de chamado tipo
	 * @throws NegocioException
	 */
	@Override
	public Collection<ChamadoTipo> listarTodosHabilitado() {

		return repositorio.listarTodosHabilitado();
	}

	@Override
	public ChamadoTipo carregarChamadoTipoProjetado(Long chavePrimaria){
		return repositorio.carregarChamadoTipoProjetado(chavePrimaria);
	}
}

