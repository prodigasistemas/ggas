/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/09/2013 13:57:06
 @author vtavares
 */

package br.com.ggas.atendimento.chamadotipo.dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pela representação dos tipos de chamado
 */
public class ChamadoTipo extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;
	private static final long serialVersionUID = 1L;

	private String descricao;
	private ChamadoCategoria categoria;
	private Segmento segmento;

	private Collection<ChamadoAssunto> listaAssuntos = new HashSet<>();

	public ChamadoTipo() {
	}

	/**
	 * Construtor utilizado para projeção de objeto em consulta sql
	 * @param chavePrimaria chave primária do objeto
	 * @param descricao descrição do objeto
	 */
	public ChamadoTipo(Long chavePrimaria, String descricao) {
		this.setChavePrimaria(chavePrimaria);
		this.setDescricao(descricao);
	}

	/**
	 * @param listaAssuntos
	 */
	public void setListaAssuntos(Collection<ChamadoAssunto> listaAssuntos) {

		this.listaAssuntos = listaAssuntos;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return lista de assuntos
	 */
	public Collection<ChamadoAssunto> getListaAssuntos() {

		return listaAssuntos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao) || descricao.trim().length() == 0 || descricao.startsWith(" ")) {
			stringBuilder.append("Descrição");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * @return segmento
	 */
	public Segmento getSegmento() {
		return segmento;
	}

	/**
	 * @param segmento
	 */
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	/**
	 * Retorna categoria
	 * 
	 * @return categoria
	 */
	public ChamadoCategoria getCategoria() {
		return categoria;
	}

	/**
	 * Atribui categoria
	 * 
	 * @param categoria
	 */
	public void setCategoria(ChamadoCategoria categoria) {
		this.categoria = categoria;
	}

}
