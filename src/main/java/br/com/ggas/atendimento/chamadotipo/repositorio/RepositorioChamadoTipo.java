
package br.com.ggas.atendimento.chamadotipo.repositorio;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.chamado.dominio.ChamadoAssuntoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelo repositório do tipo de chamado
 */
@Repository
public class RepositorioChamadoTipo extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio chamado tipo.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioChamadoTipo(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ChamadoTipo();
	}

	@Override
	public Class<ChamadoTipo> getClasseEntidade() {

		return ChamadoTipo.class;
	}

	/**
	 * Listar todos.
	 * 
	 * @param descricao {@link String}
	 * @param indicador {@link Boolean}
	 * @param idSegmento {@link Long}
	 * @param categoria {@link ChamadoCategoria}
	 * @return the Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	@SuppressWarnings("unchecked")
	public Collection<ChamadoTipo> listarTodos(String descricao, Boolean indicador, Long idSegmento, ChamadoCategoria categoria)
			throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		if(!StringUtils.isEmpty(descricao)) {
			criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
		}

		if(indicador != null) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, indicador));
		}

		if (idSegmento != null && idSegmento > -1) {
			criteria.createAlias("segmento", "segmento");
			criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
		}

		if (categoria != null) {
			criteria.add(Restrictions.eq("categoria", categoria));
		}

		return criteria.list();
	}

	/**
	 * Obter todos ordenado.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<ChamadoTipo> obterTodosOrdenado() throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/**
	 * Consultar.
	 * 
	 * @param descricao
	 *            the descricao
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Boolean consultar(String descricao) throws NegocioException {

		Boolean consulta = false;
		Criteria criteria = getCriteria();

		if(descricao != null) {
			criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao));
		}

		if(!criteria.list().isEmpty()) {
			consulta = true;
		}

		return consulta;
	}

	/**
	 * Consultar por nome.
	 * 
	 * @param descricao
	 *            the descricao
	 * @return the chamado tipo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ChamadoTipo consultarPorNome(String descricao) throws NegocioException {

		Criteria criteria = getCriteria();

		if(descricao != null) {
			criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao));
		}

		return (ChamadoTipo) criteria.uniqueResult();
	}

	/**
	 * Consultar inserir vazio.
	 * 
	 * @param chamado
	 *            the chamado
	 * @param chamadoAssunto
	 *            the chamado assunto
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Boolean consultarInserirVazio(ChamadoTipo chamado, Collection<ChamadoAssunto> chamadoAssunto) throws NegocioException {

		Boolean consulta = false;

		if(chamadoAssunto == null || chamado == null || chamadoAssunto.isEmpty()) {
			consulta = true;
		}

		return consulta;
	}

	/**
	 * Listar por segmento.
	 * 
	 * @param idSegmento {@link long}
	 * 
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ChamadoTipo> listarPorSegmento(long idSegmento) {
		StringBuilder hql = new StringBuilder("SELECT new ChamadoTipo(ct.chavePrimaria, ct.descricao) FROM ChamadoTipo ct WHERE ct.");
		hql.append(EntidadeNegocio.ATRIBUTO_HABILITADO);
		hql.append(" = :ativado");
		hql.append(" AND ct.segmento.chavePrimaria = :idSegmento ");
		hql.append(" ORDER BY ");
		hql.append(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
		Query query = getQuery(hql.toString());
		query.setParameter("ativado", Boolean.TRUE);
		query.setParameter("idSegmento", idSegmento);

		return query.list();
	}

	@Override
	public void atualizar(EntidadeNegocio entidadeNegocio) throws ConcorrenciaException, NegocioException {

		ChamadoTipo chamadoTipo = (ChamadoTipo) entidadeNegocio;

		Collection<ChamadoAssunto> listaAssuntos = chamadoTipo.getListaAssuntos();

		for (ChamadoAssunto chamadoAssunto : listaAssuntos) {
			Collection<ChamadoAssuntoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora =
					chamadoAssunto.getListaUnidadeOrganizacionalVisualizadora();
			for (ChamadoAssuntoUnidadeOrganizacional chamadoAssuntoUnidadeOrganizacional : listaUnidadeOrganizacionalVisualizadora) {
				chamadoAssuntoUnidadeOrganizacional.setChamadoAssunto(chamadoAssunto);
			}
		}

		super.atualizar(entidadeNegocio);
	}

	/**
	 * Método que realizará a busca de todos os ChamadoTipo habilitados. O objeto ChamadoTipo terá apenas o preenchimento dos atributos
	 * chavePrimaria e a descricao.
	 *
	 * @return Retorna a lista de ChamadoTipo com os atributos chavePrimaria e a descricao, preenchido.
	 */
	public Collection<ChamadoTipo> listarTodosHabilitado() {
		StringBuilder hql = new StringBuilder("SELECT new ChamadoTipo(ct.chavePrimaria, ct.descricao) FROM ChamadoTipo ct WHERE ct.");
		hql.append(EntidadeNegocio.ATRIBUTO_HABILITADO);
		hql.append(" = :ativado");
		hql.append(" ORDER BY ");
		hql.append(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
		Query query = getQuery(hql.toString());
		query.setParameter("ativado", Boolean.TRUE);
		return query.list();
	}

	/**
	 * Carrega o chamado tipo projetado apenas com os campos necessários
	 * @param chavePrimaria {@link Long}
	 * @return o chamado {@link ChamadoTipo}
	 */
	public ChamadoTipo carregarChamadoTipoProjetado(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new ChamadoTipo(chamado.chavePrimaria, chamado.descricao) ");
		hql.append(" FROM ChamadoTipo chamado");
		hql.append(" WHERE chamado.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimaria);

		return (ChamadoTipo) query.uniqueResult();
	}
}
