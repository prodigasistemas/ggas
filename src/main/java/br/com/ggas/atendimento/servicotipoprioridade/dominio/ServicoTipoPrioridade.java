/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/09/2013 16:34:07
 @author marcilio
 */

package br.com.ggas.atendimento.servicotipoprioridade.dominio;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável por representar a prioridade do tipo de serviço
 */
public class ServicoTipoPrioridade extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -992735839958276285L;

	public static final String BEAN_ID_SERVICO_TIPO_PRIORIDADE = "servicoTipoPrioridade";

	public static final String SERVICO_TIPO_PRIORIDADE = "Prioridade do Tipo de Serviço";

	private String descricao;

	private Integer quantidadeHorasMaxima;

	private EntidadeConteudo indicadorDiaCorridoUtil;

	/**
	 * Instantiates a new servico tipo prioridade.
	 */
	public ServicoTipoPrioridade() {

		super();
	}

	/**
	 * Instantiates a new servico tipo prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 */
	public ServicoTipoPrioridade(Long chavePrimaria) {

		super();
		setChavePrimaria(chavePrimaria);
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public Integer getQuantidadeHorasMaxima() {

		return quantidadeHorasMaxima;
	}

	public void setQuantidadeHorasMaxima(Integer quantidadeHorasMaxima) {

		this.quantidadeHorasMaxima = quantidadeHorasMaxima;
	}

	public EntidadeConteudo getIndicadorDiaCorridoUtil() {

		return indicadorDiaCorridoUtil;
	}

	public void setIndicadorDiaCorridoUtil(EntidadeConteudo indicadorDiaCorridoUtil) {

		this.indicadorDiaCorridoUtil = indicadorDiaCorridoUtil;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao) || descricao.trim().length() == 0) {
			stringBuilder.append(Constantes.SERVICO_TIPO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(quantidadeHorasMaxima == null) {
			stringBuilder.append(Constantes.SERVICO_TIPO_QUANTIDADE_HORAS_MAXIMA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorDiaCorridoUtil == null) {
			stringBuilder.append(Constantes.SERVICO_TIPO_INDICADOR_DIAS_CORRIDO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		} else {
			if(quantidadeHorasMaxima <= 0) {
				erros.put(Constantes.SERVICO_TIPO_QUANTIDADE_HORAS_MINIMA, null);
			}
		}

		return erros;
	}
}
