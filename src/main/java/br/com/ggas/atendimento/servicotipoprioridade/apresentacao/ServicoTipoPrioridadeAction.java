
package br.com.ggas.atendimento.servicotipoprioridade.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.negocio.impl.ControladorServicoTipoPrioridadeImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe reponsável pelas ações relacionadas as telas que manipulam a classe {@link ServicoTipoPrioridade}
 */
@Controller
public class ServicoTipoPrioridadeAction extends GenericAction {

	private static final int INDICE_DIA = 5;

	private static final String TIPO_PRIORIDADE = "tipoPrioridade";

	private static final String LISTA_TIPO_PRIORIDADE = "listaTipoPrioridade";

	@Autowired
	private ControladorServicoTipoPrioridade controladorServicoTipo;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	public ControladorServicoTipoPrioridade getControladorServicoTipo() {

		return controladorServicoTipo;
	}

	public void setControladorTipoPrioridade(ControladorServicoTipoPrioridadeImpl controladorServicoTipo) {

		this.controladorServicoTipo = controladorServicoTipo;
	}

	/**
	 * Inserir.
	 * 
	 * @param dia
	 *            the dia
	 * @param descricao
	 *            the descricao
	 * @param quantidadeHorasMaxima
	 *            the quantidade horas maxima
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("inserirServicoTipoPrioridade")
	public ModelAndView inserir(@RequestParam(value = "indicadorDiaCorridoUtil", required = false) String dia,
					@RequestParam(value = "descricao", required = false) String descricao,
					@RequestParam(value = "quantidadeHorasMaxima", required = false) Integer quantidadeHorasMaxima) throws GGASException {

		ModelAndView model = new ModelAndView("forward:/pesquisarServicoTipoPrioridade");

		Long codigo = this.obterConstante(dia);

		EntidadeConteudo ec = controladorEntidadeConteudo.obter(codigo);

		ServicoTipoPrioridade tipoPrioridade = new ServicoTipoPrioridade();

		tipoPrioridade.setDescricao(Util.rtrim(Util.ltrim(descricao)));
		tipoPrioridade.setQuantidadeHorasMaxima(quantidadeHorasMaxima);
		tipoPrioridade.setIndicadorDiaCorridoUtil(ec);
		tipoPrioridade.setHabilitado(true);

		try {
			controladorServicoTipo.verificarServicoTipoPrioridadeExistente(tipoPrioridade);
			controladorServicoTipo.inserir(tipoPrioridade);
			model.addObject(tipoPrioridade);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, ServicoTipoPrioridade.SERVICO_TIPO_PRIORIDADE);
		} catch(GGASException e) {
			String indicador = null;
			model = new ModelAndView("exibirInclusaoServicoTipoPrioridade");
			model.addObject("servicoTipoPrioridade", tipoPrioridade);
			model.addObject(LISTA_TIPO_PRIORIDADE, controladorServicoTipo.listarTodos(tipoPrioridade, indicador));
			mensagemErroParametrizado(model, e);
		}

		return model;

	}

	/**
	 * Pesquisar servico.
	 * 
	 * @param tipo
	 *            the tipo
	 * @param descricao
	 *            the descricao
	 * @param quantidadeHorasMaxima
	 *            the quantidade horas maxima
	 * @param habilitado
	 *            the habilitado
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarServicoTipoPrioridade")
	public ModelAndView pesquisarServico(@RequestParam(value = "indicadorDiaCorridoUtil", required = false) String tipo,
					@RequestParam(value = "descricao", required = false) String descricao,
					@RequestParam(value = "quantidadeHorasMaxima", required = false) Integer quantidadeHorasMaxima,
					@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisarServicoTipoPrioridade");

		String indicador = null;

		if(habilitado != null) {

			if(habilitado) {
				indicador = "true";
			} else {
				indicador = "false";
			}
		} else {
			indicador = "";
		}

		ServicoTipoPrioridade prioridade = new ServicoTipoPrioridade();

		prioridade.setDescricao(descricao.trim());
		prioridade.setQuantidadeHorasMaxima(quantidadeHorasMaxima);

		Collection<ServicoTipoPrioridade> listaTipoPrioridade = controladorServicoTipo.listarTodos(prioridade, indicador);

		model.addObject(LISTA_TIPO_PRIORIDADE, listaTipoPrioridade);
		model.addObject("servicotipoprioridade", prioridade);

		model.addObject("habilitado", indicador);

		return model;

	}

	/**
	 * Detalhar servico.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 */
	@RequestMapping("exibirDetalhamentoServicoTipoPrioridade")
	public ModelAndView detalharServico(@RequestParam(value = "chavePrimaria", required = false) String chavePrimaria) {

		ModelAndView model = new ModelAndView("exibirDetalhamentoServicoTipoPrioridade");

		ServicoTipoPrioridade servico = controladorServicoTipo.obterServicoTipoPrioridade(Long.parseLong(chavePrimaria));

		model.addObject(TIPO_PRIORIDADE, servico);
		String dias = servico.getIndicadorDiaCorridoUtil().getDescricao().substring(INDICE_DIA);

		String indicador = "Inativo";
		if(servico.isHabilitado()) {
			indicador = "Ativo";
		}

		model.addObject("dias", dias);
		model.addObject("indicador", indicador);

		return model;
	}

	/**
	 * Exibir inclusao.
	 * 
	 * @return the string
	 */
	@RequestMapping("exibirInclusaoServicoTipoPrioridade")
	public String exibirInclusao() {

		return "exibirInclusaoServicoTipoPrioridade";
	}

	/**
	 * Exibir alteracao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 */
	@RequestMapping("exibirAlteracaoServicoTipoPrioridade")
	public ModelAndView exibirAlteracao(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) {

		ModelAndView model = new ModelAndView("exibirAlteracaoServicoTipoPrioridade");

		ServicoTipoPrioridade tipoPrioridade = controladorServicoTipo.obterServicoTipoPrioridade(chavePrimaria);

		ConstanteSistema util = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIA_UTIL);

		String dia = "corrido";
		if(tipoPrioridade.getIndicadorDiaCorridoUtil().getChavePrimaria() == Long.parseLong(util.getValor())) {
			dia = "util";
		}
		tipoPrioridade.setDescricao(tipoPrioridade.getDescricao().trim());
		model.addObject("dia", dia);
		model.addObject(TIPO_PRIORIDADE, tipoPrioridade);

		return model;
	}

	/**
	 * Salva alteracao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param dia
	 *            the dia
	 * @param descricao
	 *            the descricao
	 * @param quantidadeHorasMaxima
	 *            the quantidade horas maxima
	 * @param habilitado
	 *            the habilitado
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("salvarAlteracaoServicoTipoPrioridade")
	public ModelAndView salvaAlteracao(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = "indicadorDiaCorridoUtil", required = false) String dia,
					@RequestParam(value = "descricao", required = false) String descricao,
					@RequestParam(value = "quantidadeHorasMaxima", required = false) Integer quantidadeHorasMaxima,
					@RequestParam(value = "habilitado", required = false) Boolean habilitado) throws GGASException {

		ModelAndView model = new ModelAndView("forward:/pesquisarServicoTipoPrioridade");
		ServicoTipoPrioridade tipoPrioridade = null;

		Long codigo = this.obterConstante(dia);

		EntidadeConteudo ec = controladorEntidadeConteudo.obter(codigo);

		tipoPrioridade = controladorServicoTipo.obterServicoTipoPrioridade(chavePrimaria);

		tipoPrioridade.setHabilitado(habilitado);
		tipoPrioridade.setDescricao(descricao);
		tipoPrioridade.setQuantidadeHorasMaxima(quantidadeHorasMaxima);
		tipoPrioridade.setIndicadorDiaCorridoUtil(ec);
		try {
			controladorServicoTipo.verificarServicoTipoPrioridadeExistente(tipoPrioridade);
			controladorServicoTipo.atualizarServicoTipoPrioridade(tipoPrioridade);
			model.addObject(TIPO_PRIORIDADE, tipoPrioridade);
			model.addObject("dia", dia);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, ServicoTipoPrioridade.SERVICO_TIPO_PRIORIDADE);
		} catch(GGASException e) {
			String indicador = null;
			model = new ModelAndView("exibirAlteracaoServicoTipoPrioridade");
			model.addObject(LISTA_TIPO_PRIORIDADE, controladorServicoTipo.listarTodos(tipoPrioridade, indicador));
			model.addObject(TIPO_PRIORIDADE, tipoPrioridade);
			model.addObject("dia", dia);
			mensagemErroParametrizado(model, e);
		}

		return model;

	}

	/**
	 * Remover prioridade.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerServicoTipoPrioridade")
	public ModelAndView removerPrioridade(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView("forward:/exibirPesquisarServicoTipoPrioridade");

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorServicoTipo.removerServicoTipoPrioridade(chavesPrimarias, dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, ServicoTipoPrioridade.SERVICO_TIPO_PRIORIDADE);

		} catch(GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Obter constante.
	 * 
	 * @param dia
	 *            the dia
	 * @return the long
	 */
	private Long obterConstante(String dia) {

		ConstanteSistema util = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIA_UTIL);
		ConstanteSistema corrido = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIAS_CORRIDOS);

		Long codigo = Long.parseLong(util.getValor());
		if("corrido".equalsIgnoreCase(dia)) {
			codigo = Long.parseLong(corrido.getValor());
		}

		return codigo;
	}

	/**
	 * Abrir pagina.
	 * 
	 * @return the model and view
	 */
	@RequestMapping("exibirPesquisarServicoTipoPrioridade")
	public ModelAndView abrirPagina() {

		ModelAndView model = new ModelAndView("exibirPesquisarServicoTipoPrioridade");
		model.addObject("habilitado", "true");
		return model;
	}

}

