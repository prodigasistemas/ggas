/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/09/2013 16:32:46
 @author marcilio
 */

package br.com.ggas.atendimento.servicotipoprioridade.repositorio;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Util;
/**
 *	Repositório responsável pela prioridade do tipo de serviço
 */
@Repository
public class RepositorioServicoTipoPrioridade extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio servico tipo prioridade.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioServicoTipoPrioridade(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ServicoTipoPrioridade();
	}

	@Override
	public Class<ServicoTipoPrioridade> getClasseEntidade() {

		return ServicoTipoPrioridade.class;
	}

	/**
	 * Listar todos.
	 * 
	 * @param prioridade
	 *            the prioridade
	 * @param indicador
	 *            the indicador
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<ServicoTipoPrioridade> listarTodos(ServicoTipoPrioridade prioridade, String indicador) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		if(prioridade != null) {

			String descricao = prioridade.getDescricao();
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			if(indicador != null && !(indicador.isEmpty())) {
				Boolean habilitado = Boolean.parseBoolean(indicador);
				if(habilitado != null) {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
				}
			}

			Integer quantidadeHorasMaxima = prioridade.getQuantidadeHorasMaxima();
			if(quantidadeHorasMaxima != null && quantidadeHorasMaxima > 0) {
				criteria.add(Restrictions.eq("quantidadeHorasMaxima", quantidadeHorasMaxima));
			}

			criteria.setFetchMode("indicadorDiaCorridoUtil", FetchMode.JOIN);
		}

		return criteria.list();
	}

	/**
	 * Consultar todos.
	 * 
	 * @param prioridade
	 *            the prioridade
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipoPrioridade> consultarTodos(ServicoTipoPrioridade prioridade) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		if(prioridade != null) {

			String descricao = prioridade.getDescricao();
			if(descricao != null) {
				criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao));
			}

			criteria.setFetchMode("indicadorDiaCorridoUtil", FetchMode.JOIN);
		}

		return criteria.list();

	}

	/**
	 * Obter servico tipo prioridade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the servico tipo prioridade
	 */
	public ServicoTipoPrioridade obterServicoTipoPrioridade(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" tipoPrioridade ");
		hql.append(" left join fetch tipoPrioridade.indicadorDiaCorridoUtil ");
		hql.append(" where ");
		hql.append(" tipoPrioridade.chavePrimaria= :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return (ServicoTipoPrioridade) query.uniqueResult();
	}

	/**
	 * Obter servico tipo prioridade relacionado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ServicoTipo> obterServicoTipoPrioridadeRelacionado(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(ServicoTipo.class.getSimpleName());
		hql.append(" servicoTipo ");
		hql.append(" inner join fetch servicoTipo.servicoTipoPrioridade servicoTipoPrioridade");
		hql.append(" where ");
		hql.append(" servicoTipoPrioridade.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		return query.list();

	}

	/**
	 * Listar servico tipo prioridade.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<ServicoTipoPrioridade> listarServicoTipoPrioridade() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/**
	 * Obter todos servico tipo prioridade ordenado.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<ServicoTipoPrioridade> obterTodosServicoTipoPrioridadeOrdenado(Boolean habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if(habilitado != null) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

}
