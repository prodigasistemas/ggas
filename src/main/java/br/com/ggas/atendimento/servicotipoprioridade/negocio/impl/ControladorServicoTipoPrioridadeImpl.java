/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/09/2013 16:31:57
 @author marcilio
 */

package br.com.ggas.atendimento.servicotipoprioridade.negocio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.repositorio.RepositorioServicoTipoPrioridade;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelas ações relacionadas a prioridade do tipo de serviço
 */
@Service
@Transactional
public class ControladorServicoTipoPrioridadeImpl implements ControladorServicoTipoPrioridade {

	@Autowired
	private RepositorioServicoTipoPrioridade repositorio;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#inserir(br.com.ggas.atendimento.
	 * servicotipoprioridade.dominio.ServicoTipoPrioridade)
	 */
	@Override
	@Transactional
	public ServicoTipoPrioridade inserir(ServicoTipoPrioridade tipoPrioridade) throws GGASException {

		this.repositorio.inserir(tipoPrioridade);

		return tipoPrioridade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#listar()
	 */
	@Override
	@Transactional
	public Collection<EntidadeNegocio> listar() throws GGASException {

		return repositorio.obterTodas();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#listarTodos(br.com.ggas.atendimento.
	 * servicotipoprioridade.dominio.ServicoTipoPrioridade, java.lang.String)
	 */
	@Override
	@Transactional
	public Collection<ServicoTipoPrioridade> listarTodos(ServicoTipoPrioridade prioridade, String habilitado) throws GGASException {

		return repositorio.listarTodos(prioridade, habilitado);
	}

	public RepositorioServicoTipoPrioridade getRepositorio() {

		return repositorio;
	}

	public void setRepositorio(RepositorioServicoTipoPrioridade repositorio) {

		this.repositorio = repositorio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#obterServicoTipoPrioridade(java.lang.Long)
	 */
	@Override
	public ServicoTipoPrioridade obterServicoTipoPrioridade(Long chavePrimaria) {

		return repositorio.obterServicoTipoPrioridade(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#atualizarServicoTipoPrioridade(br.com.ggas
	 * .atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade)
	 */
	@Override
	@Transactional
	public ServicoTipoPrioridade atualizarServicoTipoPrioridade(ServicoTipoPrioridade tipoPrioridade) throws GGASException {

		this.repositorio.atualizar(tipoPrioridade, ServicoTipoPrioridade.class);

		return tipoPrioridade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#removerServicoTipoPrioridade(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerServicoTipoPrioridade(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		for (Long chave : chavesPrimarias) {
			if(chave != null) {
				ServicoTipoPrioridade servicoTipoPrioridade = this.obterServicoTipoPrioridade(chave);
				this.verificarItemRelacionado(servicoTipoPrioridade);
			}
		}

		this.repositorio.remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#verificarServicoTipoPrioridadeExistente(br
	 * .com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade)
	 */
	@Override
	public void verificarServicoTipoPrioridadeExistente(ServicoTipoPrioridade tipoPrioridade) throws GGASException {

		ServicoTipoPrioridade tipoPrioridadeTemp = (ServicoTipoPrioridade) repositorio.criar();
		tipoPrioridadeTemp.setChavePrimaria(tipoPrioridade.getChavePrimaria());
		tipoPrioridadeTemp.setDescricao(tipoPrioridade.getDescricao());

		Long chavePrimaria = tipoPrioridadeTemp.getChavePrimaria();

		Collection<ServicoTipoPrioridade> tipoPrioridadeConsulta = null;

		tipoPrioridadeConsulta = this.consultarTodos(tipoPrioridadeTemp);

		if(tipoPrioridadeConsulta != null && !tipoPrioridadeConsulta.isEmpty()) {
			ServicoTipoPrioridade prioridadeExistente = tipoPrioridadeConsulta.iterator().next();
			if(prioridadeExistente.getChavePrimaria() != chavePrimaria) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_EXISTENTE, tipoPrioridadeTemp.getDescricao());
			}
		}

	}

	/**
	 * Verificar item relacionado.
	 * 
	 * @param tipoPrioridade
	 *            the tipo prioridade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarItemRelacionado(ServicoTipoPrioridade tipoPrioridade) throws NegocioException {

		Collection<ServicoTipo> servicoConsulta = repositorio.obterServicoTipoPrioridadeRelacionado(tipoPrioridade.getChavePrimaria());

		if(servicoConsulta != null && !servicoConsulta.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_RELACIONADO, new String[] {});
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#consultarTodos(br.com.ggas.atendimento.
	 * servicotipoprioridade.dominio.ServicoTipoPrioridade)
	 */
	@Override
	public Collection<ServicoTipoPrioridade> consultarTodos(ServicoTipoPrioridade prioridade) throws NegocioException {

		return this.repositorio.consultarTodos(prioridade);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.atendimento.servicotipoprioridade.negocio.ControladorServicoTipoPrioridade#listarServicoTipoPrioridade()
	 */
	@Override
	public Collection<ServicoTipoPrioridade> listarServicoTipoPrioridade() throws NegocioException {

		return repositorio.listarServicoTipoPrioridade();
	}

}
