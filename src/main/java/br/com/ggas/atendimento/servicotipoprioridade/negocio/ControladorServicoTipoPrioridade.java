
package br.com.ggas.atendimento.servicotipoprioridade.negocio;

import java.util.Collection;

import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pelos métodos do controlador de prioridade do tipo de serviço
 */
public interface ControladorServicoTipoPrioridade {

	String ERRO_NEGOCIO_DESCRICAO_VAZIA = "Descrição não pode ser vazia";

	/**
	 * Método utilizado para inserir
	 * 
	 * @param tipoPrioridade {@link ServicoTipoPrioridade}
	 * @return ServicoTipoPrioridade {@link ServicoTipoPrioridade}
	 * @throws GGASException {@link GGASException}
	 */
	public ServicoTipoPrioridade inserir(ServicoTipoPrioridade tipoPrioridade) throws GGASException;

	/**
	 * Listar.
	 * 
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<EntidadeNegocio> listar() throws GGASException;

	/**
	 * Listar todos.
	 * 
	 * @param prioridade the prioridade
	 * @param habilitado the habilitado
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<ServicoTipoPrioridade> listarTodos(ServicoTipoPrioridade prioridade, String habilitado) throws GGASException;

	/**
	 * Obter servico tipo prioridade.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the servico tipo prioridade
	 */
	public ServicoTipoPrioridade obterServicoTipoPrioridade(Long chavePrimaria);

	/**
	 * atualizar Servico Tipo Prioridade
	 * 
	 * @param tipoPrioridade  {@link ServicoTipoPrioridade}
	 * @return ServicoTipoPrioridade {@link ServicoTipoPrioridade}
	 * @throws GGASException {@link GGASException}
	 */
	public ServicoTipoPrioridade atualizarServicoTipoPrioridade(ServicoTipoPrioridade tipoPrioridade) throws GGASException;

	/**
	 * Remover servico tipo prioridade.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param auditoria the auditoria
	 * @throws NegocioException the negocio exception
	 */
	public void removerServicoTipoPrioridade(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException;

	/**
	 * Consultar todos.
	 * 
	 * @param prioridade the prioridade
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ServicoTipoPrioridade> consultarTodos(ServicoTipoPrioridade prioridade) throws NegocioException;

	/**
	 * Verificar servico tipo prioridade existente.
	 * 
	 * @param tipoPrioridade the tipo prioridade
	 * @throws GGASException the GGAS exception
	 */
	public void verificarServicoTipoPrioridadeExistente(ServicoTipoPrioridade tipoPrioridade) throws GGASException;

	/**
	 * Listar servico tipo prioridade.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ServicoTipoPrioridade> listarServicoTipoPrioridade() throws NegocioException;
}
