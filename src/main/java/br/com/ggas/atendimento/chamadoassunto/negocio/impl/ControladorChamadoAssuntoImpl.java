
package br.com.ggas.atendimento.chamadoassunto.negocio.impl;

import br.com.ggas.atendimento.chamado.dominio.ChamadoAssuntoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.repositorio.RepositorioChamadoAssunto;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.repositorio.RepositorioChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável pelas ações relacionadas ao assunto de um chamado
 */
@Service("controladorChamadoAssunto")
@Transactional
public class ControladorChamadoAssuntoImpl implements ControladorChamadoAssunto {

	public static final String INCLUIR = "INCLUIR";
	@Autowired
	private RepositorioChamadoAssunto repositorio;

	@Autowired
	private RepositorioChamadoTipo repositorioChamadoTipo;

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#consultarChamado(java.lang.Long)
	 */
	@Override
	public Collection<ChamadoAssunto> consultarChamado(Long chavePrimaria) throws NegocioException {

		return repositorio.consultarChamado(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#obter(java.lang.Long)
	 */
	@Override
	public EntidadeNegocio obter(Long chavePrimaria) throws NegocioException {

		return repositorio.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#inserirChamadoAssunto(br.com.ggas.atendimento.chamadoassunto
	 * .dominio.ChamadoAssunto)
	 */
	@Override
	public Long inserirChamadoAssunto(ChamadoAssunto chamadoAssunto) throws NegocioException {

		return repositorio.inserir(chamadoAssunto);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#consultarChamadoAssunto(java.lang.Long)
	 */
	@Override
	public ChamadoAssunto consultarChamadoAssunto(Long chavePrimaria) {

		return repositorio.consultarChamadoAssunto(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#inserirTudo(br.com.ggas.atendimento.chamadotipo.dominio.
	 * ChamadoTipo, java.util.Collection)
	 */
	@Transactional
	@Override
	public Long inserirTudo(ChamadoTipo chamadoTipo, Collection<ChamadoAssunto> chamadoAssunto) throws NegocioException {

		controladorChamadoTipo.consultarInserirVazio(chamadoTipo, chamadoAssunto);
		controladorChamadoTipo.verificarDescricao(chamadoTipo.getDescricao());

		for (ChamadoAssunto c : chamadoAssunto) {
			c.setChamadoTipo(chamadoTipo);
			c.setUltimaAlteracao(Calendar.getInstance().getTime());
			c.setHabilitado(Boolean.TRUE);
			c.setDadosAuditoria(chamadoTipo.getDadosAuditoria());
			c.getDadosAuditoria().getOperacao().setDescricao(INCLUIR);

			for (ChamadoAssuntoServicoTipo t : c.getListaServicos()) {
				t.setChamadoAssunto(c);
				t.setUltimaAlteracao(Calendar.getInstance().getTime());
				t.setHabilitado(Boolean.TRUE);
				t.setDadosAuditoria(chamadoTipo.getDadosAuditoria());
				t.getDadosAuditoria().getOperacao().setDescricao(INCLUIR);
			}

			for (ChamadoAssuntoUnidadeOrganizacional unidadeVisualizadora : c.getListaUnidadeOrganizacionalVisualizadora()) {
				unidadeVisualizadora.setChamadoAssunto(c);
				unidadeVisualizadora.setDadosAuditoria(chamadoTipo.getDadosAuditoria());
				unidadeVisualizadora.getDadosAuditoria().getOperacao().setDescricao(INCLUIR);
				unidadeVisualizadora.setUltimaAlteracao(Calendar.getInstance().getTime());
				unidadeVisualizadora.setHabilitado(Boolean.TRUE);
			}
		}

		chamadoTipo.getListaAssuntos().clear();
		chamadoTipo.getListaAssuntos().addAll(chamadoAssunto);

		return repositorioChamadoTipo.inserir(chamadoTipo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#validarDados(br.com.ggas.atendimento.chamadoassunto.dominio
	 * .ChamadoAssunto)
	 */
	@Override
	public void validarDados(ChamadoAssunto chamadoAssunto) throws NegocioException {

		Map<String, Object> erros = chamadoAssunto.validarDados();
		if (erros != null && !erros.isEmpty()) {

			throw new NegocioException(erros);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#verificarServicosDuplicados(java.util.List,
	 * br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo)
	 */
	@Override
	public boolean verificarServicosDuplicados(List<ChamadoAssuntoServicoTipo> listaServicos, ServicoTipo servicoTipo) {

		for (ChamadoAssuntoServicoTipo chamadoAssuntoServicoTipo : listaServicos) {
			if (chamadoAssuntoServicoTipo.getServicoTipo().getChavePrimaria() == servicoTipo.getChavePrimaria()) {
				return true;
			}
		}

		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#obterTodos()
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodos() throws NegocioException {

		return repositorio.obterTodas(Boolean.TRUE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#obterOrdenado()
	 */
	@Override
	public Collection<ChamadoAssunto> obterOrdenado() throws NegocioException {

		return repositorio.obterOrdenado();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#existeChamadoAssuntoQuestionario(br.com.ggas.atendimento.
	 * questionario.dominio.Questionario)
	 */
	@Override
	public boolean existeChamadoAssuntoQuestionario(Questionario questionario) {
		return repositorio.existeChamadoAssuntoQuestionario(questionario);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#existeChamadoAssuntoQuestionario(br.com.ggas.atendimento.
	 * questionario.dominio.Questionario)
	 */
	public Collection<ChamadoAssunto> obterChamadoAssuntoPorChaveChamadoTipo(Long chamadoTipo) {
		return repositorio.obterChamadoAssuntoPorChaveChamadoTipo(chamadoTipo);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#existeChamadoAssuntoAtivoQuestionario(br.com.ggas.atendimento.
	 * questionario.dominio.Questionario)
	 */
	public Collection<ChamadoAssunto> obterChamadoAssuntoAtivoPorChaveChamadoTipo(Long chamadoTipo) {
		return repositorio.obterChamadoAssuntoAtivoPorChaveChamadoTipo(chamadoTipo);
	}
	
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto#existeChamadoAssuntoQuestionario(br.com.ggas.atendimento.
	 * questionario.dominio.Questionario)
	 */
	public Collection<ChamadoAssunto> obterChamadoAssuntoTipo(Long chamadoTipo) {
		return repositorio.obterChamadoAssuntoTipo(chamadoTipo);
	}


	/**
	 *
	 * @param chavePrimaria identifador do chamado assunto
	 * @return  a quantidade de horas prevista do chamado
	 */
	public ChamadoAssunto carregarChamadoAssuntoProjetado(Long chavePrimaria) {

		return repositorio.carregarChamadoAssuntoProjetado(chavePrimaria);
	}

	@Override
	public Usuario carregarUsuarioResponsavelPorChamadoAssunto(Long chavePrimaria) {
		return repositorio.carregarUsuarioResponsavelPorChamadoAssunto(chavePrimaria);
	}

	@Override
	public Collection<UnidadeOrganizacional> obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(Long chaveChamadoAssunto) {
		return repositorio.obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(chaveChamadoAssunto);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<ChamadoAssunto> obterChamadosAssuntoAberturaEmLote(Long tipoChamado) {
		return repositorio.obterChamadosAssuntoAberturaEmLote(tipoChamado);
	}
	
	@Override
	public EntidadeClasse obterEntidadeClassePeloIndicadorRegulatorio() {
		return repositorio.obterEntidadeClassePeloIndicadorRegulatorio();
	}
}
