
package br.com.ggas.atendimento.chamadoassunto.negocio;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

import java.util.Collection;
import java.util.List;
/**
 * Interface responsável pelos métodos relacionados ao assunto de um chamado
 */
public interface ControladorChamadoAssunto {

	/**
	 * Consultar chamado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ChamadoAssunto> consultarChamado(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public EntidadeNegocio obter(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir chamado assunto.
	 * 
	 * @param chamadoAssunto
	 *            the chamado assunto
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long inserirChamadoAssunto(ChamadoAssunto chamadoAssunto) throws NegocioException;

	/**
	 * Inserir tudo.
	 * 
	 * @param chamadoTipo
	 *            the chamado tipo
	 * @param chamadoAssunto
	 *            the chamado assunto
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long inserirTudo(ChamadoTipo chamadoTipo, Collection<ChamadoAssunto> chamadoAssunto) throws NegocioException;

	/**
	 * Verificar servicos duplicados.
	 * 
	 * @param listaServicos
	 *            the lista servicos
	 * @param servicoTipo
	 *            the servico tipo
	 * @return true, if successful
	 */
	boolean verificarServicosDuplicados(List<ChamadoAssuntoServicoTipo> listaServicos, ServicoTipo servicoTipo);

	/**
	 * Validar dados.
	 * 
	 * @param chamadoAssunto
	 *            the chamado assunto
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarDados(ChamadoAssunto chamadoAssunto) throws NegocioException;

	/**
	 * Consultar chamado assunto.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the chamado assunto
	 */
	public ChamadoAssunto consultarChamadoAssunto(Long chavePrimaria);

	/**
	 * Obter todos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodos() throws NegocioException;

	/**
	 * Obter ordenado.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ChamadoAssunto> obterOrdenado() throws NegocioException;

	/**
	 * Verifica se existe questionario para o assunto
	 * 
	 * @param questionario {@link Questionario}
	 * @return true se existir, falso caso contrario
	 */
	public boolean existeChamadoAssuntoQuestionario(Questionario questionario);

	
	/**
	 * Obter a lista de chamado assuntos
	 * 
	 * @param chamadoTipo {@link Long}
	 * @return true se existir, falso caso contrario
	 */
	Collection<ChamadoAssunto> obterChamadoAssuntoPorChaveChamadoTipo(Long chamadoTipo);

	/**
	 * Obter a lista de chamado assuntos ativos
	 * 
	 * @param chamadoTipo {@link Long}
	 * @return true se existir, falso caso contrario
	 */
	Collection<ChamadoAssunto> obterChamadoAssuntoAtivoPorChaveChamadoTipo(Long chamadoTipo);
	
	/**
	 * Obter a lista de chamado assuntos
	 *
	 * @param chamadoTipo {@link Long}
	 * @return true se existir, falso caso contrario
	 */
	Collection<ChamadoAssunto> obterChamadoAssuntoTipo(Long chamadoTipo);


	/**
	 * Carregar o chamado assunto projetado somente com os atributos necessários
	 *
	 * @param chavePrimaria identifador do chamado assunto
	 * @return a quantidade de horas prevista do chamado
	 */
	ChamadoAssunto carregarChamadoAssuntoProjetado(Long chavePrimaria);

	/**
	 * Carregar o usuário responsável pela chave primária do chamado assunto
	 *
	 * @param chavePrimaria identifador do chamado assunto
	 * @return a quantidade de horas prevista do chamado
	 */
	Usuario carregarUsuarioResponsavelPorChamadoAssunto(Long chavePrimaria);

	/**
	 * Consulta as unidades organizacionais visualizadoras
	 *
	 * @param chaveChamadoAssunto identificador do chamado assunto
	 * @return a lista de unidades organizacionais visualizadoras
	 */
	Collection<UnidadeOrganizacional> obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(Long chaveChamadoAssunto);

	/**
	 * Obtém a lista de {@link ChamadoAssunto} que permitem abertura em lote a partir do tipo do chamado
	 * @param tipoChamado tipo do chamado
	 * @return retorna a lista de chamados de assunto
	 */
	Collection<ChamadoAssunto> obterChamadosAssuntoAberturaEmLote(Long tipoChamado);
	

	/**
	 * Obtém a Entidade conteúdo a partir do indicador regulatório
	 * @return retorna a entidade classe completa
	 */
	EntidadeClasse obterEntidadeClassePeloIndicadorRegulatorio();
}
