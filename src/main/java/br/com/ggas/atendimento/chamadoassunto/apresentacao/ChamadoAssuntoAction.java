
package br.com.ggas.atendimento.chamadoassunto.apresentacao;

import br.com.ggas.atendimento.chamado.dominio.ChamadoAssuntoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssuntoVO;
import br.com.ggas.atendimento.chamadoassunto.dominio.OperacaoEnvioEmail;
import br.com.ggas.atendimento.chamadoassunto.dominio.OperacaoEnvioEmailPrioridade;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.impl.ControladorChamadoAssuntoImpl;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.ControladorChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.negocio.impl.ControladorChamadoAssuntoServicoTipoImpl;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.questionario.dominio.QuestionarioVO;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.impl.ControladorServicoTipoImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
import static java.util.stream.Collectors.joining;

/**
 * Classe responsável pelas telas relacionadas aos assuntos de chamados
 **/
@Controller
public class ChamadoAssuntoAction extends GenericAction {

	private static final String HABILITADO = "habilitado";
	private static final String LISTA_CANAL_ATENDIMENTO = "listaCanalAtendimento";
	private static final int LIMITE_DESCRICAO = 5;
	private static final String LISTA_SERVICO_TIPO = "listaServicoTipo";
	private static final String LISTA_RUBRICA_CREDITO = "listaRubricaCredito";
	private static final String LISTA_SERVICOS_ASSUNTO = "listaServicosAssunto";
	private static final String CHAMADO_ASSUNTO = "chamadoAssunto";
	private static final String CHAMADO_ASSUNTO_VO = "chamadoAssuntoVO";
	private static final String LISTA_ASSUNTO = "listaAssunto";
	private static final String FORWARD_PESQUISAR_CHAMADO_ASSUNTO = "forward:/pesquisarChamadoAssunto";
	private static final String LISTA_SERVICOS = "listaServicos";
	private static final String TELA_GRID_COMPONENTES_SERVICO_TIPO = "gridComponentesServicoTipo";
	private static final String CHAMADO_TIPO = "chamadoTipo";
	private static final String INDICADOR_DIA_CORRIDO_UTIL = "indicadorDiaCorridoUtil";
	private static final String LISTA_UNIDADE_ORGANIZACIONAL = "listaUnidadeOrganizacional";
	private static final String LISTA_QUESTIONARIO = "listaQuestionario";
	private static final String LISTA_RUBRICA_DEBITO = "listaRubricaDebito";
	private static final String TELA_EXIBIR_INCLUSAO_CHAMADO_ASSUNTO = "exibirInclusaoChamadoAssunto";
	private static final String LISTA_SEGMENTO = "listaSegmento";
	private static final String LISTA_CATEGORIA = "listaCategoria";
	private static final String INDICADOR_REGULATORIO = "indicadorRegulatorio";
	private static final String INDICADOR_REGULATORIO_VALORES = "indicadorRegulatorioValores";

	private static final Logger LOG = Logger.getLogger(ChamadoAssuntoAction.class);
	public static final String LISTA_OPERACAO_EMAIL = "listaOperacaoEmail";
	public static final String LISTA_OPERACAO_EMAIL_PRIORIDADE = "listaOperacaoEmailPrioridade";

	@Autowired
	private ControladorChamadoAssunto controladorChamadoAssunto;

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	private ControladorChamadoAssuntoServicoTipo controladorChamadoAssuntoServicoTipo;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorRubrica")
	private ControladorRubrica controladorRubrica;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorUsuario")
	private ControladorUsuario controladorUsuario;

	@Autowired
	@Qualifier("controladorFuncionario")
	private ControladorFuncionario controladorFuncionario;

	@Autowired
	private ControladorQuestionario controladorQuestionario;
	
	/**
	 * @return controladorChamadoAssunto
	 */
	public ControladorChamadoAssunto getControladorChamadoAssunto() {

		return controladorChamadoAssunto;
	}

	/**
	 * @param controladorChamadoAssunto
	 */
	public void setControladorChamadoAssunto(ControladorChamadoAssuntoImpl controladorChamadoAssunto) {

		this.controladorChamadoAssunto = controladorChamadoAssunto;
	}

	/**
	 * @return controladorChamadoTipo
	 */
	public ControladorChamadoTipo getControladorChamadoTipo() {

		return controladorChamadoTipo;
	}

	/**
	 * @param controladorServicoTipo
	 */
	public void setControladorChamadoTipo(ControladorServicoTipoImpl controladorServicoTipo) {

		this.controladorServicoTipo = controladorServicoTipo;
	}

	/**
	 * @return controladorServicoTipo
	 */
	public ControladorServicoTipo getControladorServicoTipo() {

		return controladorServicoTipo;
	}

	/**
	 * @param controladorServicoTipo
	 */
	public void setControladorServicoTipo(ControladorServicoTipoImpl controladorServicoTipo) {

		this.controladorServicoTipo = controladorServicoTipo;
	}

	/**
	 * @return controladorChamadoAssuntoServicoTipo
	 */
	public ControladorChamadoAssuntoServicoTipo getControladorChamadoAssuntoServicoTipo() {

		return controladorChamadoAssuntoServicoTipo;
	}

	/**
	 * @param controladorChamadoAssuntoServicoTipo
	 */
	public void setControladorChamadoAssuntoServicoTipo(ControladorChamadoAssuntoServicoTipoImpl controladorChamadoAssuntoServicoTipo) {

		this.controladorChamadoAssuntoServicoTipo = controladorChamadoAssuntoServicoTipo;
	}

	/**
	 * Exibir incluir.
	 *
	 * @param sessao the sessao
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(TELA_EXIBIR_INCLUSAO_CHAMADO_ASSUNTO)
	public ModelAndView exibirIncluir(HttpSession sessao) throws GGASException {


		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO_ASSUNTO);
		model.addObject(LISTA_SEGMENTO, Fachada.getInstancia().listarSegmento());
		model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());
		model.addObject(LISTA_RUBRICA_DEBITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(true));
		model.addObject(LISTA_RUBRICA_CREDITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(false));
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addObject("assunto", popularPadrao());
		model.addObject(INDICADOR_DIA_CORRIDO_UTIL, "util");
		model.addObject(LISTA_OPERACAO_EMAIL, OperacaoEnvioEmail.values());
		model.addObject(LISTA_OPERACAO_EMAIL_PRIORIDADE, OperacaoEnvioEmailPrioridade.values());
		
		EntidadeClasse entidadeClasse = controladorChamadoAssunto.obterEntidadeClassePeloIndicadorRegulatorio();
		EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudoPorChaveEntidadeClasse(entidadeClasse.getChavePrimaria());
		model.addObject(INDICADOR_REGULATORIO,entidadeClasse);
		model.addObject(INDICADOR_REGULATORIO_VALORES,entidadeConteudo);
		
		carregarListaQuestionario(model);

		limparSessao(sessao);
		return model;
	}

	/**
	 * Pesquisar.
	 *
	 * @param descricao {@link String}
	 * @param indicador {@link Boolean}
	 * @param idSegmento {@link Long}
	 * @param tamanhoPagina {@link Integer}
	 * @param categoriaChamado {@link String}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarChamadoAssunto")
	public ModelAndView pesquisar(@RequestParam(value = "descricao", required = false) String descricao,
			@RequestParam(value = HABILITADO, required = false) Boolean indicador,
			@RequestParam(value = "idSegmentoChamadoTipo", required = false) Long idSegmento,
			@RequestParam(value = "categoriaChamado", required = false) String categoriaChamado,
			@RequestParam(value = "tamanhoPagina", required = false) Integer tamanhoPagina) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaChamadoAssunto");

		int tamanhoPaginacao = Constantes.PAGINACAO_PADRAO;

		if (tamanhoPagina != null) {
			tamanhoPaginacao = tamanhoPagina;
		}

		model.addObject("tamanhoPagina", tamanhoPaginacao);

		ChamadoCategoria categoria = null;
		if (categoriaChamado != null && !"-1".equals(categoriaChamado)) {
			categoria = ChamadoCategoria.getByName(categoriaChamado);
		}

		Collection<ChamadoTipo> listaChamadoTipo = controladorChamadoTipo.listarTodos(descricao, indicador, idSegmento, categoria);

		ChamadoTipo chamadoTipo = new ChamadoTipo();
		chamadoTipo.setDescricao(descricao.trim());
		chamadoTipo.setCategoria(categoria);

		model.addObject("listaChamadoAssunto", listaChamadoTipo);

		String habilitado = "";

		if(indicador != null) {

			if(indicador) {
				habilitado = "true";
			} else {
				habilitado = "false";
			}
		}

		model.addObject(CHAMADO_TIPO, chamadoTipo);
		model.addObject(HABILITADO, habilitado);
		model.addObject("idSegmentoChamadoTipo", idSegmento);
		model.addObject(LISTA_SEGMENTO, Fachada.getInstancia().listarSegmento());
		model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());

		return model;
	}

	/**
	 * Adicionar servico.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("adicionarServico")
	public ModelAndView adicionarServico(@RequestParam(value = "servicoTipo", required = false) Long chavePrimaria, HttpSession sessao)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_GRID_COMPONENTES_SERVICO_TIPO);
		ServicoTipo servico = controladorServicoTipo.consultarServicoTipo(chavePrimaria);

		ChamadoAssuntoServicoTipo chamadoServicoTipo = new ChamadoAssuntoServicoTipo();

		List<ChamadoAssuntoServicoTipo> listaServicos = new ArrayList<>();

		if(sessao.getAttribute(LISTA_SERVICOS) != null) {
			listaServicos.addAll((Collection<ChamadoAssuntoServicoTipo>) sessao.getAttribute(LISTA_SERVICOS));
		}

		if(listaServicos.isEmpty()) {
			chamadoServicoTipo.setServicoTipo(servico);
			chamadoServicoTipo.setNumeroOrdem(listaServicos.size() + 1);
			listaServicos.add(chamadoServicoTipo);

		} else {

			boolean registroExistente = controladorChamadoAssunto.verificarServicosDuplicados(listaServicos, servico);

			if(!registroExistente) {
				chamadoServicoTipo.setServicoTipo(servico);
				chamadoServicoTipo.setNumeroOrdem(listaServicos.size() + 1);
				listaServicos.add(chamadoServicoTipo);

			} else {
				model = new ModelAndView("ajaxErro");
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_CHAMADO_SERVICO_EXISTENTE);
			}
		}
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());
		sessao.setAttribute(LISTA_SERVICOS, listaServicos);
		model.addObject(LISTA_SERVICOS, listaServicos);

		return model;

	}

	/**
	 * Alterar ordem lista servicos.
	 *
	 * @param indexServicoTipo
	 *            the index servico tipo
	 * @param acao
	 *            the acao
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("alterarOrdemListaServicos")
	public ModelAndView alterarOrdemListaServicos(@RequestParam(value = "indexServicoTipo", required = false) int indexServicoTipo,
					@RequestParam(value = "acao", required = false) String acao, HttpSession sessao) {
		ModelAndView model = new ModelAndView(TELA_GRID_COMPONENTES_SERVICO_TIPO);
		try {

			List<ChamadoAssuntoServicoTipo> listaServicos = new ArrayList<>();

			if(sessao.getAttribute(LISTA_SERVICOS) != null) {
				listaServicos.addAll((Collection<ChamadoAssuntoServicoTipo>) sessao.getAttribute(LISTA_SERVICOS));
			}

			ChamadoAssuntoServicoTipo chamado1 = null;
			ChamadoAssuntoServicoTipo chamado2 = null;
			Integer ordem1 = null;
			Integer ordem2 = null;

			if("subirPosicao".equals(acao)) {
				chamado1 = listaServicos.get(indexServicoTipo);
				chamado2 = listaServicos.get(indexServicoTipo - 1);
				ordem1 = chamado1.getNumeroOrdem();
				ordem2 = chamado2.getNumeroOrdem();
				chamado1.setNumeroOrdem(ordem2);
				chamado2.setNumeroOrdem(ordem1);

				listaServicos.set(indexServicoTipo, chamado2);
				listaServicos.set(indexServicoTipo - 1, chamado1);

			} else if("descerPosicao".equals(acao)) {
				chamado1 = listaServicos.get(indexServicoTipo);
				chamado2 = listaServicos.get(indexServicoTipo + 1);
				ordem1 = chamado1.getNumeroOrdem();
				ordem2 = chamado2.getNumeroOrdem();
				chamado1.setNumeroOrdem(ordem2);
				chamado2.setNumeroOrdem(ordem1);

				listaServicos.set(indexServicoTipo, chamado2);
				listaServicos.set(indexServicoTipo + 1, chamado1);
			}
			sessao.setAttribute(LISTA_SERVICOS, listaServicos);
			model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());
		} catch(GGASException e) {
			model = new ModelAndView(TELA_GRID_COMPONENTES_SERVICO_TIPO);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Remover servico.
	 *
	 * @param indexServicoTipo
	 *            the index servico tipo
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerServico")
	public ModelAndView removerServico(@RequestParam("indexServicoTipo") int indexServicoTipo, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_COMPONENTES_SERVICO_TIPO);

		List<ChamadoAssuntoServicoTipo> listaServicos = new ArrayList<>();
		listaServicos.addAll((Collection<ChamadoAssuntoServicoTipo>) sessao.getAttribute(LISTA_SERVICOS));

		if(CollectionUtils.isNotEmpty(listaServicos)) {
			listaServicos.remove(indexServicoTipo);
			for (int i = indexServicoTipo; i < listaServicos.size(); i++) {
				ChamadoAssuntoServicoTipo chamado = listaServicos.get(i);
				chamado.setNumeroOrdem(chamado.getNumeroOrdem() - 1);
			}
		}
		sessao.setAttribute(LISTA_SERVICOS, listaServicos);
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());

		return model;

	}

	/**
	 * Remover chamado tipo.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerChamadoTipo")
	public ModelAndView removerChamadoTipo(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView(FORWARD_PESQUISAR_CHAMADO_ASSUNTO);

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorChamadoTipo.removerChamadoTipo(chavesPrimarias, dadosAuditoria);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, GenericAction.obterMensagem(Constantes.CHAMADO_ASSUNTO));
		} catch(DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_RELACIONADO, true);
			} catch(NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		}
		return model;

	}

	/**
	 * Campos obrigatorios.
	 *
	 * @param descricao
	 *            the descricao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("verificarCamposObrigatorios")
	public ModelAndView camposObrigatorios(@RequestParam("chavePrimaria") String descricao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO_ASSUNTO);

		try {
			ChamadoAssunto chamadoAssunto = new ChamadoAssunto();

			chamadoAssunto.setDescricao(descricao);

			controladorChamadoAssunto.validarDados(chamadoAssunto);

		} catch(GGASException e) {
			model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO_ASSUNTO);
			mensagemErroParametrizado(model, e);
		}

		return model;

	}

	/**
	 *
	 * Carregar grid servicos.
	 *
	 * @param indexAssunto the index assunto
	 * @param sessao the sessao
	 * @return the model and view
	 */
	@SuppressWarnings({"unchecked", "static-access"})
	@RequestMapping("carregarGridServicos")
	public ModelAndView carregarGridServicos(@RequestParam("indexAssunto") int indexAssunto, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_COMPONENTES_SERVICO_TIPO);
		List<ChamadoAssunto> listaAssunto = new ArrayList<>();
		if(sessao.getAttribute(LISTA_ASSUNTO) != null) {
			listaAssunto.addAll((Collection<ChamadoAssunto>) sessao.getAttribute(LISTA_ASSUNTO));
		}
		if(CollectionUtils.isNotEmpty(listaAssunto)) {

			List<ChamadoAssuntoServicoTipo> listaServicos = new ArrayList<>();
			listaServicos.addAll(listaAssunto.get(indexAssunto).getListaServicos());
			this.ordenaPorNumero(listaServicos);
			List<ChamadoAssuntoServicoTipo> listaOrdenada = new ArrayList<>();
			listaOrdenada.addAll(controladorChamadoAssuntoServicoTipo.ordernarLista(listaAssunto.get(indexAssunto).getChavePrimaria()));
			model.addObject(LISTA_SERVICOS, listaServicos);
			sessao.setAttribute(LISTA_SERVICOS, listaServicos);

		}

		return model;

	}

	/**
	 *
	 * Carregar responsavel assunto.
	 *
	 * @param chaveUnidadeOrganizacional the chave unidade organizacional
	 * @param chaveUsuarioResponsavel the chave usuario responsavel
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("carregarResponsavelAssunto")
	public ModelAndView carregarResponsavelAssunto(@RequestParam("chaveUnidadeOrganizacional") Long chaveUnidadeOrganizacional,
					@RequestParam("chaveUsuarioResponsavel") Long chaveUsuarioResponsavel) throws NegocioException {

		ModelAndView model = new ModelAndView("divResponsavelAssunto");

		if(chaveUsuarioResponsavel > 0) {
			model.addObject("chaveResponsavel", chaveUsuarioResponsavel);
		}

		if(chaveUnidadeOrganizacional > 0) {
			model.addObject("listaUsuarioResponsavel",
							controladorFuncionario.listarFuncionarioPorUnidadeOrganizacional(chaveUnidadeOrganizacional));
		}

		return model;
	}

	/**
	 *
	 * Detalhe assunto.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param selectedTab the selectedTab
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("detalheAssunto")
	public ModelAndView detalheAssunto(@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam(value = "selectedTab", required = false) String selectedTab) throws GGASException {

		ModelAndView model = new ModelAndView("detalharChamadoAssunto");

		ChamadoAssunto chamadoAssunto = controladorChamadoAssunto.consultarChamadoAssunto(chavePrimaria);
		ChamadoAssuntoVO chamadoAssuntoVO = new ChamadoAssuntoVO();

		chamadoAssuntoVO.setDescricao(chamadoAssunto.getDescricao());
		chamadoAssuntoVO.setOrientacao(chamadoAssunto.getOrientacao());
		chamadoAssuntoVO.setIndicadorClienteObrigatorio(this.converterNome(chamadoAssunto.getIndicadorClienteObrigatorio()));
		chamadoAssuntoVO.setIndicadorCpfCnpjSolicitanteObrigatorio(this.converterNome(chamadoAssunto
						.getIndicadorCpfCnpjSolicitanteObrigatorio()));
		chamadoAssuntoVO.setIndicadorCredito(this.converterNome(chamadoAssunto.getIndicadorCredito()));
		chamadoAssuntoVO.setIndicadorDebito(this.converterNome(chamadoAssunto.getIndicadorDebito()));
		chamadoAssuntoVO.setIndicadorDiaCorridoUtil(chamadoAssunto.getIndicadorDiaCorridoUtil().getDescricao().substring(LIMITE_DESCRICAO));
		chamadoAssuntoVO.setIndicadorImovelObrigatorio(this.converterNome(chamadoAssunto.getIndicadorImovelObrigatorio()));
		chamadoAssuntoVO.setIndicadorNomeSolicitanteObrigatorio(this.converterNome(chamadoAssunto.getIndicadorNomeSolicitanteObrigatorio()));

		chamadoAssuntoVO.setIndicadorRgSolicitanteObrigatorio(this.converterNome(chamadoAssunto.getIndicadorRgSolicitanteObrigatorio()));
		chamadoAssuntoVO.setIndicadorTramitacao(this.converterNome(chamadoAssunto.getIndicadorTramitacao()));
		chamadoAssuntoVO.setQuantidadeHorasPrevistaAtendimento(chamadoAssunto.getQuantidadeHorasPrevistaAtendimento());
		chamadoAssuntoVO.setIndicadorTelefoneSolicitanteObrigatorio(this.converterNome(chamadoAssunto
						.getIndicadorTelefoneSolicitanteObrigatorio()));
		chamadoAssuntoVO.setIndicadorEmailSolicitanteObrigatorio(this.converterNome(chamadoAssunto
						.getIndicadorEmailSolicitanteObrigatorio()));
		chamadoAssuntoVO.setIndicadorGeraServicoAutorizacao(this.converterNome(chamadoAssunto.getIndicadorGeraServicoAutorizacao()));
		chamadoAssuntoVO.setIndicadorFechamentoAutomatico(this.converterNome(chamadoAssunto.getIndicadorFechamentoAutomatico()));
		chamadoAssuntoVO.setIndicadorPrazoDiferenciado(this.converterNome(chamadoAssunto.getIndicadorPrazoDiferenciado()));
		chamadoAssuntoVO.setIndicadorVerificaRestServ(this.converterNome(chamadoAssunto.getIndicadorVerificaRestServ()));
		chamadoAssuntoVO.setIndicadorVerificarVazamento(this.converterNome(chamadoAssunto.getIndicadorVerificarVazamento()));

		chamadoAssuntoVO.setIndicadorEncerraAutoASRel(this.converterNome(chamadoAssunto.getIndicadorEncerraAutoASRel()));

		Optional.ofNullable(chamadoAssunto.getEnviarEmailCadastro()).ifPresent(e -> {
			model.addObject("enviarEmailCadastro", e.stream().map(o -> obterMensagem(o.getChaveMensagem())).collect(joining(", ")));
		});
		Optional.ofNullable(chamadoAssunto.getEnviarEmailTramitar()).ifPresent(e -> {
			model.addObject("enviarEmailTramitar", e.stream().map(o -> obterMensagem(o.getChaveMensagem())).collect(joining(", ")));
		});
		Optional.ofNullable(chamadoAssunto.getEnviarEmailReiterar()).ifPresent(e -> {
			model.addObject("enviarEmailReiterar", e.stream().map(o -> obterMensagem(o.getChaveMensagem())).collect(joining(", ")));
		});
		Optional.ofNullable(chamadoAssunto.getEnviarEmailEncerrar()).ifPresent(e -> {
			model.addObject("enviarEmailEncerrar", e.stream().map(o -> obterMensagem(o.getChaveMensagem())).collect(joining(", ")));
		});
		Optional.ofNullable(chamadoAssunto.getEnviarEmailPrioridadeProtocolo()).ifPresent(e -> {
			model.addObject("enviarEmailPrioridadeProtocolo", e.stream().map(o -> obterMensagem(o.getChaveMensagem())).collect(joining(", ")));
		});

		chamadoAssuntoVO.setIndicadorAcionamentoGasista(this.converterNome(chamadoAssunto.getIndicadorAcionamentoGasista()));
		chamadoAssuntoVO.setPermiteAbrirEmLote(this.converterNome(chamadoAssunto.getPermiteAbrirEmLote()));
		chamadoAssuntoVO.setIndicadorComGarantia(this.converterNome(chamadoAssunto.getIndicadorComGarantia()));

		if(chamadoAssunto.getRubricaCredito() != null) {
			chamadoAssuntoVO.setRubricaCredito(chamadoAssunto.getRubricaCredito().getDescricao());
		}
		if(chamadoAssunto.getRubricaDebito() != null) {
			chamadoAssuntoVO.setRubricaDebito(chamadoAssunto.getRubricaDebito().getDescricao());
		}
		chamadoAssuntoVO.setUnidadeOrganizacional(chamadoAssunto.getUnidadeOrganizacional().getDescricao());

		if(chamadoAssunto.getUsuarioResponsavel() != null) {
			chamadoAssuntoVO.setUsuarioResponsavel(chamadoAssunto.getUsuarioResponsavel().getFuncionario().getNome());
		}

		if (chamadoAssunto.getQuestionario() != null) {
			chamadoAssuntoVO.setQuestionario(chamadoAssunto.getQuestionario().getNomeQuestionario());
		}

		if (chamadoAssunto.getCanalAtendimento() != null) {
			chamadoAssuntoVO.setCanalAtendimento(chamadoAssunto.getCanalAtendimento().getDescricao());
		}

		chamadoAssuntoVO
				.setIndicadorUnidadesVisualizadora(this.converterNome(chamadoAssunto.getIndicadorUnidadesOrganizacionalVisualizadora()));
		chamadoAssuntoVO.setListaUnidadesVisualizadoras(chamadoAssunto.getListaUnidadeOrganizacionalVisualizadora());
		chamadoAssuntoVO.setListaServicos(controladorChamadoAssuntoServicoTipo.ordernarLista(chamadoAssunto.getChavePrimaria()));
		chamadoAssuntoVO.setHabilitado(chamadoAssunto.isHabilitado());
		model.addObject(CHAMADO_ASSUNTO, chamadoAssunto);
		model.addObject(CHAMADO_ASSUNTO_VO, chamadoAssuntoVO);
		model.addObject("selectedTab", selectedTab);

		return model;

	}

	/**
	 *
	 * Converter nome.
	 *
	 * @param indicador the indicador
	 * @return the string
	 */
	private String converterNome(Boolean indicador) {

		String campo = "";
		if(indicador != null) {
			if(indicador) {
				campo = "Sim";
			} else {
				campo = "Não";
			}
		}
		return campo;
	}


	/**
	 *
	 * Adicionar Assunto
	 *
	 * @param chavePrimaria {@link Long}
	 * @param indexList {@link Integer}
	 * @param sessao {@link HttpSession}
	 * @param enviarEmailCadastro {@link Set}
	 * @param enviarEmailTramitar {@link Set}
	 * @param enviarEmailReiterar {@link Set}
	 * @param enviarEmailEncerrar {@link Set}
	 * @param enviarEmailPrioridadeProtocolo {@link Set}
	 * @param request {@link HttpServletRequest}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("adicionarAssunto")
	public ModelAndView adicionarAssunto(@RequestParam("chavePrimaria") Long chavePrimaria,
			@RequestParam("indexList") Integer indexList, HttpSession sessao,
			@RequestParam(value = "enviarEmailCadastro[]", required = false) Set<OperacaoEnvioEmail> enviarEmailCadastro,
			@RequestParam(value = "enviarEmailTramitar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailTramitar,
			@RequestParam(value = "enviarEmailReiterar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailReiterar,
			@RequestParam(value = "enviarEmailEncerrar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailEncerrar,
			@RequestParam(value = "enviarEmailPrioridadeProtocolo[]",
			required = false) Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo,
			@RequestParam(value = "indicadorRegulatorioValores", required = false) String indicadorRegulatorio,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("gridComponentesAssunto");

		try {
			ChamadoAssunto assunto = popularAssunto(request, enviarEmailCadastro, enviarEmailTramitar, enviarEmailReiterar,
					enviarEmailEncerrar, enviarEmailPrioridadeProtocolo);
			
			assunto.setHabilitado(Boolean.valueOf(request.getParameter(HABILITADO)));

			ChamadoTipo chamadoTipo = new ChamadoTipo();
			Collection<ChamadoAssunto> listaAssuntos = (Collection<ChamadoAssunto>) sessao.getAttribute(LISTA_ASSUNTO);
			if (listaAssuntos != null) {
				validarAssuntoExistente(assunto, (ArrayList<ChamadoAssunto>) listaAssuntos, indexList);
			}
			if (chavePrimaria != null) {
				chamadoTipo.setChavePrimaria(chavePrimaria);
				assunto.setChamadoTipo(chamadoTipo);
				assunto.setUltimaAlteracao(Calendar.getInstance().getTime());
			}
			Collection<ChamadoAssuntoServicoTipo> listaServicos;
			if (sessao.getAttribute(LISTA_SERVICOS_ASSUNTO) != null) {
				listaServicos = (Collection<ChamadoAssuntoServicoTipo>) sessao.getAttribute(LISTA_SERVICOS_ASSUNTO);
			} else {
				listaServicos = (Collection<ChamadoAssuntoServicoTipo>) sessao.getAttribute(LISTA_SERVICOS);
			}
			if (listaServicos != null) {
				assunto.getListaServicos().addAll(listaServicos);

				inicializarListaServicos(listaServicos, assunto);
			}
			Collection<ChamadoAssunto> assuntos = new ArrayList<>();
			if (sessao.getAttribute(LISTA_ASSUNTO) != null) {
				assuntos.addAll((Collection<ChamadoAssunto>) sessao.getAttribute(LISTA_ASSUNTO));
			}

			validarEntidade(assunto);
			// caso seja inclusão de um novo Assunto, o indexList não é informado
			if (indexList == null || indexList < 0) {
				assuntos.add(assunto);
			} else {
				this.alterarAssunto(indexList, assunto, sessao);
			}

			listaAssuntos = assuntos;
			sessao.setAttribute(LISTA_ASSUNTO, listaAssuntos);

			sessao.removeAttribute(LISTA_SERVICOS_ASSUNTO);
			model.addObject(LISTA_ASSUNTO, listaAssuntos);

		} catch (GGASException e) {
			model = new ModelAndView("ajaxErro");
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 *
	 * Alterar assunto.
	 *
	 * @param indexLista the index lista
	 * @param assunto the assunto
	 * @param session the session
	 * @throws GGASException the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private void alterarAssunto(Integer indexLista, ChamadoAssunto assunto, HttpSession session) throws GGASException {

		Collection<ChamadoAssunto> assuntos = new ArrayList<>();
		assuntos.addAll((Collection<ChamadoAssunto>) session.getAttribute(LISTA_ASSUNTO));

		if(CollectionUtils.isNotEmpty(assuntos) && indexLista != null) {

			for (int i = 0; i < assuntos.size(); i++) {
				ChamadoAssunto assuntoExistente = ((ArrayList<ChamadoAssunto>) assuntos).get(i);
				if(i == indexLista && assuntoExistente != null) {
					assuntoExistente.setDescricao(assunto.getDescricao());
					assuntoExistente.setOrientacao(assunto.getOrientacao());
					assuntoExistente.setIndicadorClienteObrigatorio(assunto.getIndicadorClienteObrigatorio());
					assuntoExistente.setIndicadorCpfCnpjSolicitanteObrigatorio(assunto.getIndicadorCpfCnpjSolicitanteObrigatorio());
					assuntoExistente.setIndicadorCredito(assunto.getIndicadorCredito());
					assuntoExistente.setIndicadorDebito(assunto.getIndicadorDebito());
					assuntoExistente.setIndicadorDiaCorridoUtil(assunto.getIndicadorDiaCorridoUtil());
					assuntoExistente.setIndicadorImovelObrigatorio(assunto.getIndicadorImovelObrigatorio());
					assuntoExistente.setIndicadorNomeSolicitanteObrigatorio(assunto.getIndicadorNomeSolicitanteObrigatorio());
					assuntoExistente.setIndicadorRgSolicitanteObrigatorio(assunto.getIndicadorRgSolicitanteObrigatorio());
					assuntoExistente.setIndicadorTramitacao(assunto.getIndicadorTramitacao());
					assuntoExistente.setQuantidadeHorasPrevistaAtendimento(assunto.getQuantidadeHorasPrevistaAtendimento());
					assuntoExistente.setRubricaCredito(assunto.getRubricaCredito());
					assuntoExistente.setRubricaDebito(assunto.getRubricaDebito());
					assuntoExistente.setIndicadorEmailSolicitanteObrigatorio(assunto.getIndicadorEmailSolicitanteObrigatorio());
					assuntoExistente.setIndicadorTelefoneSolicitanteObrigatorio(assunto.getIndicadorTelefoneSolicitanteObrigatorio());
					assuntoExistente.setUnidadeOrganizacional(assunto.getUnidadeOrganizacional());
					assuntoExistente.setUsuarioResponsavel(assunto.getUsuarioResponsavel());
					assuntoExistente.setIndicadorGeraServicoAutorizacao(assunto.getIndicadorGeraServicoAutorizacao());
					assuntoExistente.setIndicadorFechamentoAutomatico(assunto.getIndicadorFechamentoAutomatico());
					assuntoExistente.setIndicadorPrazoDiferenciado(assunto.getIndicadorPrazoDiferenciado());
					assuntoExistente.getListaServicos().clear();
					assuntoExistente.setIndicadorVerificaRestServ(assunto.getIndicadorVerificaRestServ());
					assuntoExistente.setIndicadorEncerraAutoASRel(assunto.getIndicadorEncerraAutoASRel());

					assuntoExistente.setIndicadorVerificarVazamento(assunto.getIndicadorVerificarVazamento());
					assuntoExistente
							.setIndicadorUnidadesOrganizacionalVisualizadora(assunto.getIndicadorUnidadesOrganizacionalVisualizadora());
					assuntoExistente.setListaUnidadeOrganizacionalVisualizadora(assunto.getListaUnidadeOrganizacionalVisualizadora());
					inicializarListaServicos(assunto.getListaServicos(), assuntoExistente);
					assuntoExistente.getListaServicos().addAll(assunto.getListaServicos());

					assuntoExistente.setEnviarEmailReiterar(assunto.getEnviarEmailReiterar());
					assuntoExistente.setEnviarEmailTramitar(assunto.getEnviarEmailTramitar());
					assuntoExistente.setEnviarEmailCadastro(assunto.getEnviarEmailCadastro());
					assuntoExistente.setEnviarEmailEncerrar(assunto.getEnviarEmailEncerrar());
					assuntoExistente.setEnviarEmailPrioridadeProtocolo(assunto.getEnviarEmailPrioridadeProtocolo());

					assuntoExistente.setIndicadorAcionamentoGasista(assunto.getIndicadorAcionamentoGasista());
					assuntoExistente.setQuestionario(assunto.getQuestionario());
					assuntoExistente.setCanalAtendimento(assunto.getCanalAtendimento());
					assuntoExistente.setHabilitado(assunto.isHabilitado());
					assuntoExistente.setPermiteAbrirEmLote(assunto.getPermiteAbrirEmLote());
					assuntoExistente.setIndicadorComGarantia(assunto.getIndicadorComGarantia());
					assuntoExistente.setIndicadorRegulatorio(assunto.getIndicadorRegulatorio());
					assuntoExistente.setNomeRelatorioRegulatorio(assunto.getNomeRelatorioRegulatorio());
					assuntoExistente.setHabilitado(assunto.isHabilitado());
					break;
				}
			}
		}

		session.setAttribute(LISTA_ASSUNTO, assuntos);
	}

	/**
	 *
	 * inicializa lista de servicos
	 *
	 * @param assunto
	 * @param assuntoExistente
	 */
	private void inicializarListaServicos(Collection<ChamadoAssuntoServicoTipo> listaServicos, ChamadoAssunto assuntoExistente) {
		for (ChamadoAssuntoServicoTipo t : listaServicos) {
			t.setChamadoAssunto(assuntoExistente);
			t.setUltimaAlteracao(Calendar.getInstance().getTime());
			t.setHabilitado(Boolean.TRUE);
		}
	}

	/**
	 *
	 * Popular assunto.
	 *
	 * @return the request
	 * @throws GGASException
	 */
	private ChamadoAssunto popularAssunto(HttpServletRequest request, Set<OperacaoEnvioEmail> emailCadastro,
			Set<OperacaoEnvioEmail> enviarEmailTramitar, Set<OperacaoEnvioEmail> enviarEmailReiterar,
			Set<OperacaoEnvioEmail> emailEncerrar, Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo) throws GGASException {

		ChamadoAssunto assunto = new ChamadoAssunto();
		String descricao = request.getParameter("descricaoAssunto");
		String orientacao = request.getParameter("orientacao");
		Boolean indicadorCredito = Boolean.valueOf(request.getParameter("indicadorCredito"));
		Boolean indicadorDebito = Boolean.valueOf(request.getParameter("indicadorDebito"));
		Boolean indicadorClienteObrigatorio = Boolean.valueOf(request.getParameter("indicadorClienteObrigatorio"));
		Integer quantidadeHorasPrevistaAtendimento = null;
		Boolean habilitado = Boolean.valueOf(request.getParameter(HABILITADO));
		String nomeRelatorioRegulatorio = request.getParameter("nomeRelatorioRegulatorio");
		
		if (StringUtils.isNotEmpty(request.getParameter("quantidadeHorasPrevistaAtendimento"))) {
			quantidadeHorasPrevistaAtendimento = Integer.valueOf(request.getParameter("quantidadeHorasPrevistaAtendimento"));
		}
		Boolean indicadorUnidadesOrganizacionalVisualizadora = Boolean.valueOf(request.getParameter("indicadorUOVisualizadora"));
		String[] unidadeOrganizacionalVisualizadoras = request.getParameterValues("uoVisualizadoras[]");
		String indicadorDiaCorridoUtil = request.getParameter(INDICADOR_DIA_CORRIDO_UTIL);

		Long rubricaCredito = null;
		if (StringUtils.isNotEmpty(request.getParameter("rubricaCredito"))) {
			rubricaCredito = Long.valueOf(request.getParameter("rubricaCredito"));
		}
		Long rubricaDebito = null;
		if (StringUtils.isNotEmpty(request.getParameter("rubricaDebito"))) {
			rubricaDebito = Long.valueOf(request.getParameter("rubricaDebito"));
		}
		Long questionario = null;
		if (StringUtils.isNotEmpty(request.getParameter("questionario"))) {
			questionario = Long.valueOf(request.getParameter("questionario"));
		}
		Long canalAtendimento = null;
		if (StringUtils.isNotEmpty(request.getParameter("canalAtendimento"))) {
			canalAtendimento = Long.valueOf(request.getParameter("canalAtendimento"));
		}
		Long unidadeOrganizacional = null;
		if (StringUtils.isNotEmpty(request.getParameter("unidadeOrganizacional"))) {
			unidadeOrganizacional = Long.valueOf(request.getParameter("unidadeOrganizacional"));
		}
		Long usuarioResponsavel = null;
		if (StringUtils.isNotEmpty(request.getParameter("usuarioResponsavel"))) {
			usuarioResponsavel = Long.valueOf(request.getParameter("usuarioResponsavel"));
		}
		Long indicadorRegulatorio = null;
		
		if (StringUtils.isNotEmpty(request.getParameter("indicadorRegulatorioValores"))) {
			indicadorRegulatorio = Long.valueOf(request.getParameter("indicadorRegulatorioValores"));
			assunto.setIndicadorRegulatorio(controladorEntidadeConteudo.obterEntidadeConteudo(indicadorRegulatorio));
		}

		assunto.setDescricao(descricao);
		assunto.setOrientacao(orientacao);
		assunto.setIndicadorCredito(indicadorCredito);
		assunto.setIndicadorDebito(indicadorDebito);
		assunto.setIndicadorClienteObrigatorio(indicadorClienteObrigatorio);
		assunto.setQuantidadeHorasPrevistaAtendimento(quantidadeHorasPrevistaAtendimento);
		assunto.setIndicadorTramitacao(Boolean.valueOf(request.getParameter("indicadorTramitacao")));
		assunto.setIndicadorNomeSolicitanteObrigatorio(Boolean.valueOf(request.getParameter("indicadorNomeSolicitanteObrigatorio")));
		assunto.setIndicadorImovelObrigatorio(Boolean.valueOf(request.getParameter("indicadorImovelObrigatorio")));
		assunto.setIndicadorCpfCnpjSolicitanteObrigatorio(Boolean.valueOf(request.getParameter("indicadorCpfCnpjSolicitanteObrigatorio")));
		assunto.setIndicadorRgSolicitanteObrigatorio(Boolean.valueOf(request.getParameter("indicadorRgSolicitanteObrigatorio")));
		assunto.setIndicadorEmailSolicitanteObrigatorio(Boolean.valueOf(request.getParameter("indicadorEmailSolicitanteObrigatorio")));
		assunto.setIndicadorTelefoneSolicitanteObrigatorio(Boolean.valueOf(request.getParameter("indicadorTelefoneSolicitanteObrigatorio")));
		assunto.setIndicadorGeraServicoAutorizacao(Boolean.valueOf(request.getParameter("indicadorGeraServicoAutorizacao")));
		assunto.setIndicadorFechamentoAutomatico(Boolean.valueOf(request.getParameter("indicadorFechamentoAutomatico")));
		assunto.setIndicadorPrazoDiferenciado(Boolean.valueOf(request.getParameter("indicadorPrazoDiferenciado")));
		assunto.setIndicadorUnidadesOrganizacionalVisualizadora(indicadorUnidadesOrganizacionalVisualizadora);
		assunto.setIndicadorVerificaRestServ(Boolean.valueOf(request.getParameter("indicadorVerificaRestServ")));
		assunto.setIndicadorEncerraAutoASRel(Boolean.valueOf(request.getParameter("indicadorEncerraAutoASRel")));
		assunto.setEnviarEmailCadastro(emailCadastro);
		assunto.setEnviarEmailEncerrar(emailEncerrar);
		assunto.setEnviarEmailPrioridadeProtocolo(enviarEmailPrioridadeProtocolo);
		assunto.setEnviarEmailReiterar(enviarEmailReiterar);
		assunto.setEnviarEmailTramitar(enviarEmailTramitar);
		assunto.setHabilitado(habilitado);

		assunto.setIndicadorVerificarVazamento(Boolean.valueOf(request.getParameter("indicadorVerificarVazamento")));
		assunto.setIndicadorAcionamentoGasista(Boolean.valueOf(request.getParameter("indicadorAcionamentoGasista")));
		assunto.setPermiteAbrirEmLote(Boolean.valueOf(request.getParameter("permiteAbrirEmLote")));
		assunto.setIndicadorComGarantia(Boolean.valueOf(request.getParameter("indicadorComGarantia")));
		assunto.setNomeRelatorioRegulatorio(nomeRelatorioRegulatorio);

		if (indicadorUnidadesOrganizacionalVisualizadora != null && indicadorUnidadesOrganizacionalVisualizadora) {
			if (unidadeOrganizacionalVisualizadoras != null && unidadeOrganizacionalVisualizadoras.length > 0) {
				assunto.setListaUnidadeOrganizacionalVisualizadora(new HashSet<ChamadoAssuntoUnidadeOrganizacional>());
				popularUnidadesVisualizadoras(assunto, unidadeOrganizacionalVisualizadoras, unidadeOrganizacional);
			}
		} else {
			assunto.getListaUnidadeOrganizacionalVisualizadora().clear();
		}

		if (indicadorDiaCorridoUtil != null && !indicadorDiaCorridoUtil.isEmpty()) {
			assunto.setIndicadorDiaCorridoUtil(controladorEntidadeConteudo.obterEntidadeConteudo(this
							.obterConstante(indicadorDiaCorridoUtil)));
		}

		if (rubricaCredito != null && rubricaCredito != -1L) {
			assunto.setRubricaCredito((Rubrica) controladorRubrica.obter(rubricaCredito));
		}
		if (rubricaDebito != null && rubricaDebito != -1L) {
			assunto.setRubricaDebito((Rubrica) controladorRubrica.obter(rubricaDebito));
		}
		if (questionario != null && questionario != -1L) {
			assunto.setQuestionario(controladorQuestionario.consultarPorCodico(questionario));
		}
		if (canalAtendimento != null && canalAtendimento != -1L) {
			assunto.setCanalAtendimento(controladorUnidadeOrganizacional.obterCanalAtendimento(canalAtendimento));
		}
		if (unidadeOrganizacional != null && unidadeOrganizacional != -1L) {
			assunto.setUnidadeOrganizacional((UnidadeOrganizacional) controladorUnidadeOrganizacional.obter(unidadeOrganizacional));
		}
		if (usuarioResponsavel != null && usuarioResponsavel != -1L) {
			Usuario usuario = controladorUsuario.obterUsuario(usuarioResponsavel);
			assunto.setUsuarioResponsavel(usuario);
		}
		assunto.setHabilitado(true);
		return assunto;
	}

	/**
	 * Popula as unidades visualizadoras do assunto do chamdo
	 *
	 * @param assunto
	 * @param unidadeOrganizacionalVisualizadoras
	 * @param unidadeOrganizacional
	 * @throws GGASException
	 */
	private void popularUnidadesVisualizadoras(ChamadoAssunto assunto, String[] unidadeOrganizacionalVisualizadoras,
			Long unidadeOrganizacional) throws GGASException {
		for (String cahveUO : unidadeOrganizacionalVisualizadoras) {
			UnidadeOrganizacional uo = Fachada.getInstancia().obterUnidadeOrganizacional(Long.valueOf(cahveUO));

			if (unidadeOrganizacional != null && unidadeOrganizacional == uo.getChavePrimaria()) {
				continue;
			}
			ChamadoAssuntoUnidadeOrganizacional chamadoAssuntoUnidadeOrganizacional = new ChamadoAssuntoUnidadeOrganizacional();
			chamadoAssuntoUnidadeOrganizacional.setChamadoAssunto(assunto);
			chamadoAssuntoUnidadeOrganizacional.setUnidadeOrganizacional(uo);

			assunto.getListaUnidadeOrganizacionalVisualizadora().add(chamadoAssuntoUnidadeOrganizacional);
		}
	}

	/**
	 * Inserir chamado assunto.
	 *
	  * @param descricao {@link String}
	 * @param idSegmentoChamadoTipo {@link Long}
	 * @param indicadorDiaCorridoUtil {@link String}
	 * @param categoriaChamado {@link String}
	 * @param enviarEmailCadastro {@link Set}
	 * @param enviarEmailTramitar {@link Set}
	 * @param enviarEmailReiterar {@link Set}
	 * @param enviarEmailEncerrar {@link Set}
	 * @param enviarEmailPrioridadeProtocolo {@link Set}
	 * @param sessao {@link HttpSession}
	 * @param request {@link HttpServletRequest}
	 * @return the model and view {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("inserirChamadoAssunto")
	public ModelAndView inserirChamadoAssunto(@RequestParam(value = "descricao", required = false) String descricao,
			@RequestParam(value = "idSegmentoChamadoTipo", required = false) Long idSegmentoChamadoTipo,
			@RequestParam(value = "categoriaChamado", required = false) String categoriaChamado,
			@RequestParam(value = INDICADOR_DIA_CORRIDO_UTIL, required = false) String indicadorDiaCorridoUtil,
			HttpSession sessao,
			@RequestParam(value = "enviarEmailCadastro[]", required = false) Set<OperacaoEnvioEmail> enviarEmailCadastro,
			@RequestParam(value = "enviarEmailTramitar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailTramitar,
			@RequestParam(value = "enviarEmailReiterar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailReiterar,
			@RequestParam(value = "enviarEmailEncerrar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailEncerrar,
			@RequestParam(value = "enviarEmailPrioridadeProtocolo[]",
			required = false)Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo,
			@RequestParam(value = "indicadorRegulatorioValores", required = false) String indicadorRegulatorio,
			HttpServletRequest request) throws GGASException {

		ChamadoAssunto assunto = null;

		assunto = popularAssunto(request, enviarEmailCadastro, enviarEmailTramitar, enviarEmailReiterar, enviarEmailEncerrar,
				enviarEmailPrioridadeProtocolo);
		
		ChamadoTipo chamadoTipo = new ChamadoTipo();

		chamadoTipo.setDescricao(Util.rtrim(Util.ltrim(descricao)));
		Segmento segmento = null;
		if (idSegmentoChamadoTipo != null && idSegmentoChamadoTipo != -1L) {
			segmento = Fachada.getInstancia().obterSegmento(idSegmentoChamadoTipo);
		}
		chamadoTipo.setSegmento(segmento);

		if (categoriaChamado != null && !"-1".equals(categoriaChamado)) {
			chamadoTipo.setCategoria(ChamadoCategoria.getByName(categoriaChamado));
		}

		chamadoTipo.setHabilitado(true);
		chamadoTipo.setDadosAuditoria(getDadosAuditoria(request));
		ModelAndView model = new ModelAndView(FORWARD_PESQUISAR_CHAMADO_ASSUNTO);

		try {

			Collection<ChamadoAssunto> listaAssunto = (Collection<ChamadoAssunto>) sessao.getAttribute(LISTA_ASSUNTO);

			controladorChamadoAssunto.inserirTudo(chamadoTipo, listaAssunto);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, GenericAction.obterMensagem(Constantes.CHAMADO_ASSUNTO));
			this.limparSessao(sessao);
		} catch (GGASException e) {

			model = new ModelAndView(TELA_EXIBIR_INCLUSAO_CHAMADO_ASSUNTO);
			model.addObject(CHAMADO_TIPO, chamadoTipo);

			model.addObject(LISTA_RUBRICA_DEBITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(true));
			model.addObject(LISTA_RUBRICA_CREDITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(false));
			model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());
			model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(null));
			model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
			model.addObject(LISTA_SEGMENTO, Fachada.getInstancia().listarSegmento());
			model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());
			model.addObject("assunto", assunto);
			model.addObject(INDICADOR_DIA_CORRIDO_UTIL, indicadorDiaCorridoUtil);

			carregarListaQuestionario(model);

			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 *
	 * Obter constante.
	 *
	 * @param dia the dia
	 * @return the long
	 *
	 */
	private Long obterConstante(String dia) {

		ConstanteSistema util = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIA_UTIL);
		ConstanteSistema corrido = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIAS_CORRIDOS);

		Long codigo = Long.parseLong(util.getValor());
		if("corrido".equalsIgnoreCase(dia)) {
			codigo = Long.parseLong(corrido.getValor());
		}

		return codigo;
	}

	/**
	 *
	 * Remover todos servicos.
	 * Os serviços são removidos da sessão
	 * @param chave the chave
	 * @param sessao the sessao
	 * @return the model and view
	 */
	@RequestMapping("removerTodosServicos")
	public ModelAndView removerTodosServicos(@RequestParam(value = "chave", required = false) Long chave, HttpSession sessao) {

		ModelAndView model = new ModelAndView(TELA_GRID_COMPONENTES_SERVICO_TIPO);

		@SuppressWarnings("unchecked")
		Collection<ChamadoAssuntoServicoTipo> listaServicos = (Collection<ChamadoAssuntoServicoTipo>) sessao.getAttribute(LISTA_SERVICOS);
		sessao.setAttribute(LISTA_SERVICOS_ASSUNTO, listaServicos);
		sessao.removeAttribute(LISTA_SERVICOS);
		return model;
	}

	/**
	 *
	 * Limpar sessao.
	 *
	 * @param sessao the sessao
	 */
	public void limparSessao(HttpSession sessao) {

		sessao.removeAttribute(LISTA_SERVICOS);
		sessao.removeAttribute(LISTA_ASSUNTO);
		sessao.removeAttribute(LISTA_SERVICOS_ASSUNTO);
	}

	/**
	 *
	 * Detalhar.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@RequestMapping("exibirDetalhamentoChamadoAssunto")
	public ModelAndView detalhar(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria) throws NegocioException {

		ChamadoTipo chamadoTipo = controladorChamadoTipo.consultarChamadoTipo(chavePrimaria);

		String indicador = "Inativo";
		if(chamadoTipo.isHabilitado()) {
			indicador = "Ativo";
		}

		ModelAndView model = new ModelAndView("exibirDetalhamentoChamadoAssunto");
		model.addObject("indicador", indicador);
		model.addObject(CHAMADO_TIPO, chamadoTipo);

		return model;
	}

	/**
	 *
	 * Remover assunto.
	 * Remove da sessão
	 *
	 * @param indexAssunto the index assunto
	 * @param sessao the sessao
	 * @return the model and view
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("removerAssunto")
	public ModelAndView removerAssunto(@RequestParam("indexAssunto") int indexAssunto, HttpSession sessao) {

		ModelAndView model = new ModelAndView("gridComponentesAssunto");

		List<ChamadoAssunto> listaAssunto = (List<ChamadoAssunto>) sessao.getAttribute(LISTA_ASSUNTO);

		if(listaAssunto != null && !listaAssunto.isEmpty()) {
			listaAssunto.remove(indexAssunto);
			sessao.setAttribute(LISTA_ASSUNTO, listaAssunto);
		}

		return model;

	}

	/**
	 *
	 * Abrir pagina.
	 *
	 * @param sessao the sessao {@link HttpSession}
	 * @return the model and view {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaChamadoAssunto")
	public ModelAndView abrirPagina(HttpSession sessao) throws GGASException {

		this.limparSessao(sessao);
		ModelAndView model = new ModelAndView("exibirPesquisaChamadoAssunto");
		model.addObject(LISTA_SEGMENTO, Fachada.getInstancia().listarSegmento());
		model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());
		model.addObject(HABILITADO, true);

		return model;
	}

	/**
	 *
	 * Exibir alteracao chamado assunto.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param session the session
	 * @return the model and view
	 * @throws GGASException the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoChamadoAssunto")
	public ModelAndView exibirAlteracaoChamadoAssunto(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoChamadoAssunto");
		ChamadoTipo chamadoTipo = controladorChamadoTipo.consultarChamadoTipo(chavePrimaria);

		List<ChamadoAssunto> listaAssunto = (List<ChamadoAssunto>) session.getAttribute(LISTA_ASSUNTO);

		if (listaAssunto == null || listaAssunto.isEmpty()) {
			listaAssunto = new ArrayList<ChamadoAssunto>();
			listaAssunto.addAll(controladorChamadoAssunto.obterChamadoAssuntoTipo(chavePrimaria));
		}else {

			List<ChamadoAssunto> listaAssuntoBanco = new ArrayList<ChamadoAssunto>();
			listaAssuntoBanco.addAll(controladorChamadoAssunto.obterChamadoAssuntoTipo(chavePrimaria));

			Collections.sort(listaAssuntoBanco, new Comparator<ChamadoAssunto>() {
				@Override
				public int compare(ChamadoAssunto objetoListaAssunto1, ChamadoAssunto objetoListaAssunto2) {

					return objetoListaAssunto1.getDescricao().compareTo(objetoListaAssunto2.getDescricao());

				}
			});

			if(listaAssunto.isEmpty()){
				listaAssunto.clear();
				listaAssunto.addAll(listaAssuntoBanco);
			}

		}

		model.addObject(CHAMADO_TIPO, chamadoTipo);
		model.addObject(CHAMADO_ASSUNTO, new ChamadoAssunto());
		model.addObject(LISTA_SEGMENTO, Fachada.getInstancia().listarSegmento());
		model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());
		session.setAttribute(LISTA_ASSUNTO, listaAssunto);
		model.addObject(LISTA_ASSUNTO, listaAssunto);
		model.addObject(LISTA_RUBRICA_DEBITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(true));
		model.addObject(LISTA_RUBRICA_CREDITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(false));
		model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());
		model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacionalOrdenada());
		model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
		model.addObject(CHAMADO_ASSUNTO, popularPadrao());
		model.addObject(INDICADOR_DIA_CORRIDO_UTIL, "util");
		model.addObject(LISTA_OPERACAO_EMAIL, OperacaoEnvioEmail.values());
		model.addObject(LISTA_OPERACAO_EMAIL_PRIORIDADE, OperacaoEnvioEmailPrioridade.values());
		EntidadeClasse entidadeClasse = controladorChamadoAssunto.obterEntidadeClassePeloIndicadorRegulatorio();
		if(entidadeClasse != null) {
			EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudoPorChaveEntidadeClasse(entidadeClasse.getChavePrimaria());
			model.addObject(INDICADOR_REGULATORIO_VALORES,entidadeConteudo);
		}
		model.addObject(INDICADOR_REGULATORIO,entidadeClasse);
		
		
		
		carregarListaQuestionario(model);
		
		return model;
	}

	/**
	 * Carregar no modelo a lista de questionario do tipo chamado
	 *
	 * @param model
	 * @throws NegocioException
	 */
	private void carregarListaQuestionario(ModelAndView model) throws NegocioException {
		QuestionarioVO questionaVo = new QuestionarioVO();
		questionaVo.setHabilitado(true);
		questionaVo.setTipoQuestionarioChamado(true);

		Collection<Questionario> questionarios = controladorQuestionario.consultarQuestionario(questionaVo);
		model.addObject(LISTA_QUESTIONARIO, questionarios);
	}

	/**
	 * Alterar chamado assunto.
	 * @param chamado {@link ChamadoTipo}
	 * @param session {@link HttpSession}
	 * @param request {@link HttpServletRequest}
	 * @param indicadorDiaCorridoUtil {@link String}
	 * @param idSegmentoChamadoTipo {@link Long}
	 * @param categoriaChamado {@link String}
	 * @param enviarEmailCadastro {@link Set}
	 * @param enviarEmailTramitar {@link Set}
	 * @param enviarEmailReiterar {@link Set}
	 * @param enviarEmailEncerrar {@link Set}
	 * @param enviarEmailPrioridadeProtocolo {@link Set}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarChamadoAssunto")
	public ModelAndView alterarChamadoAssunto(ChamadoTipo chamado, HttpSession session, HttpServletRequest request,
			@RequestParam(value = INDICADOR_DIA_CORRIDO_UTIL, required = false) String indicadorDiaCorridoUtil,
			@RequestParam(value = "idSegmentoChamadoTipo", required = false) Long idSegmentoChamadoTipo,
			@RequestParam(value = "categoriaChamado", required = false) String categoriaChamado,
			@RequestParam(value = "enviarEmailCadastro[]", required = false) Set<OperacaoEnvioEmail> enviarEmailCadastro,
			@RequestParam(value = "enviarEmailTramitar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailTramitar,
			@RequestParam(value = "enviarEmailReiterar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailReiterar,
			@RequestParam(value = "enviarEmailEncerrar[]", required = false) Set<OperacaoEnvioEmail> enviarEmailEncerrar,
			@RequestParam(value = "enviarEmailPrioridadeProtocolo[]",
			required = false) Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo,
			@RequestParam(value = "indicadorRegulatorioValores", required = false) String indicadorRegulatorio)
			throws GGASException {

		ChamadoAssunto chamadoAssunto = popularAssunto(request, enviarEmailCadastro, enviarEmailTramitar, enviarEmailReiterar,
				enviarEmailEncerrar, enviarEmailPrioridadeProtocolo);

		List<ChamadoAssuntoServicoTipo> listaServicos = new ArrayList<>();
		ModelAndView model = new ModelAndView(FORWARD_PESQUISAR_CHAMADO_ASSUNTO);
		ChamadoTipo chamadoTipo = controladorChamadoTipo.consultarChamadoTipo(chamado.getChavePrimaria());
		try {
			chamadoTipo.setDescricao(chamado.getDescricao());
			Segmento segmento = null;
			if (idSegmentoChamadoTipo != null && idSegmentoChamadoTipo != -1L) {
				segmento = Fachada.getInstancia().obterSegmento(idSegmentoChamadoTipo);
			}
			chamadoTipo.setSegmento(segmento);
			if (categoriaChamado != null && !"-1".equals(categoriaChamado)) {
				chamadoTipo.setCategoria(ChamadoCategoria.getByName(categoriaChamado));
			}

			if (session.getAttribute(LISTA_SERVICOS) != null) {
				listaServicos.addAll((Collection<ChamadoAssuntoServicoTipo>) session.getAttribute(LISTA_SERVICOS));
			}

			chamadoTipo.setHabilitado(chamado.isHabilitado());
			@SuppressWarnings("unchecked")
			Collection<ChamadoAssunto> listaAssunto = (Collection<ChamadoAssunto>) session.getAttribute(LISTA_ASSUNTO);
			chamadoTipo.getListaAssuntos().clear();
			chamadoTipo.getListaAssuntos().addAll(listaAssunto);

			chamadoTipo.setDadosAuditoria(getDadosAuditoria(request));
			validarChamadoTipoExistente(chamadoTipo);
			controladorChamadoTipo.atualizarChamadoTipo(chamadoTipo);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GenericAction.obterMensagem(Constantes.CHAMADO_ASSUNTO));
			this.limparSessao(session);
		} catch (GGASException e) {
			model = new ModelAndView("exibirAlteracaoChamadoAssunto");
			model.addObject(LISTA_RUBRICA_DEBITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(true));
			model.addObject(LISTA_RUBRICA_CREDITO, controladorRubrica.listarRubricasPorTipoCreditoDebito(false));
			model.addObject(LISTA_SERVICO_TIPO, controladorServicoTipo.listarServicoTipo());
			model.addObject(LISTA_UNIDADE_ORGANIZACIONAL, controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(null));
			model.addObject(LISTA_CANAL_ATENDIMENTO, controladorUnidadeOrganizacional.listarCanalAtendimento());
			model.addObject(CHAMADO_TIPO, chamadoTipo);
			model.addObject(CHAMADO_ASSUNTO, chamadoAssunto);
			model.addObject(LISTA_SERVICOS, session.getAttribute(LISTA_SERVICOS));
			model.addObject(LISTA_SEGMENTO, Fachada.getInstancia().listarSegmento());
			model.addObject(LISTA_CATEGORIA, ChamadoCategoria.values());
			model.addObject(INDICADOR_DIA_CORRIDO_UTIL, indicadorDiaCorridoUtil);
			List<ChamadoAssunto> listaAssunto = (List<ChamadoAssunto>) session.getAttribute(LISTA_ASSUNTO);
			session.setAttribute(LISTA_ASSUNTO, listaAssunto);
			model.addObject(LISTA_ASSUNTO, listaAssunto);
			model.addObject(CHAMADO_ASSUNTO, popularPadrao());
			model.addObject(LISTA_OPERACAO_EMAIL, OperacaoEnvioEmail.values());
			model.addObject(LISTA_OPERACAO_EMAIL_PRIORIDADE, OperacaoEnvioEmailPrioridade.values());

			carregarListaQuestionario(model);

			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 *
	 * Popular padrao.
	 *
	 * @return the chamado assunto
	 */
	private ChamadoAssunto popularPadrao() {

		ChamadoAssunto assuntoPadrao = new ChamadoAssunto();
		assuntoPadrao.setDescricao("");
		assuntoPadrao.setIndicadorCredito(false);
		assuntoPadrao.setIndicadorDebito(false);
		assuntoPadrao.setIndicadorClienteObrigatorio(false);
		assuntoPadrao.setQuantidadeHorasPrevistaAtendimento(null);
		assuntoPadrao.setIndicadorTramitacao(false);
		assuntoPadrao.setIndicadorNomeSolicitanteObrigatorio(false);
		assuntoPadrao.setIndicadorImovelObrigatorio(false);
		assuntoPadrao.setIndicadorCpfCnpjSolicitanteObrigatorio(false);
		assuntoPadrao.setIndicadorRgSolicitanteObrigatorio(false);
		assuntoPadrao.setIndicadorEmailSolicitanteObrigatorio(false);
		assuntoPadrao.setIndicadorTelefoneSolicitanteObrigatorio(false);
		assuntoPadrao.setIndicadorGeraServicoAutorizacao(false);
		assuntoPadrao.setIndicadorFechamentoAutomatico(false);
		assuntoPadrao.setIndicadorPrazoDiferenciado(false);
		assuntoPadrao.setIndicadorUnidadesOrganizacionalVisualizadora(false);
		assuntoPadrao.setIndicadorVerificaRestServ(false);
		assuntoPadrao.setIndicadorEncerraAutoASRel(false);
		assuntoPadrao.setIndicadorVerificarVazamento(false);
		assuntoPadrao.setIndicadorAcionamentoGasista(false);
		assuntoPadrao.setPermiteAbrirEmLote(false);
		assuntoPadrao.setIndicadorComGarantia(false);
		final Set<OperacaoEnvioEmail> operacoesPadrao = Sets.newHashSet(OperacaoEnvioEmail.RESPONSAVEL,
				OperacaoEnvioEmail.UNIDADES_VISUALIZADORAS);
		assuntoPadrao.setEnviarEmailEncerrar(operacoesPadrao);
		assuntoPadrao.setEnviarEmailCadastro(operacoesPadrao);
		assuntoPadrao.setEnviarEmailTramitar(operacoesPadrao);
		assuntoPadrao.setEnviarEmailReiterar(operacoesPadrao);
		final Set<OperacaoEnvioEmailPrioridade> operacaoPrioridade = Sets.newHashSet(OperacaoEnvioEmailPrioridade.AMBOS);
		assuntoPadrao.setEnviarEmailPrioridadeProtocolo(operacaoPrioridade);
		assuntoPadrao.setHabilitado(true);
		assuntoPadrao.setIndicadorRegulatorio(null);
		
		return assuntoPadrao;
	}

	/**
	 *
	 * Ordena por numero.
	 *
	 * @param lista the lista
	 */
	private static void ordenaPorNumero(List<ChamadoAssuntoServicoTipo> lista) {

		Collections.sort(lista, Comparator.comparing(ChamadoAssuntoServicoTipo::getNumeroOrdem));
	}
}
