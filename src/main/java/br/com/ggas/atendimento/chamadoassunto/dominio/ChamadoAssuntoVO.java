/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/09/2013 14:12:54
 @author vtavares
 */

package br.com.ggas.atendimento.chamadoassunto.dominio;

import br.com.ggas.atendimento.chamado.dominio.ChamadoAssuntoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Classe responsável pela representação de um objeto de valor do assunto de um chamado
 */
public class ChamadoAssuntoVO extends EntidadeNegocioImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String descricao;

	private String orientacao;

	private Integer quantidadeHorasPrevistaAtendimento;

	private String indicadorDiaCorridoUtil;

	private String indicadorCredito;

	private String rubricaCredito;

	private String indicadorDebito;

	private String rubricaDebito;

	private String unidadeOrganizacional;

	private String indicadorClienteObrigatorio;

	private String indicadorImovelObrigatorio;

	private String indicadorRgSolicitanteObrigatorio;

	private String indicadorCpfCnpjSolicitanteObrigatorio;

	private String indicadorTramitacao;

	private String indicadorNomeSolicitanteObrigatorio;

	private String indicadorTelefoneSolicitanteObrigatorio;

	private String indicadorEmailSolicitanteObrigatorio;

	private String indicadorGeraServicoAutorizacao;

	private String usuarioResponsavel;
	
	private String indicadorFechamentoAutomatico;
	
	private String indicadorPrazoDiferenciado;

	private String indicadorUnidadesVisualizadora;

	private String indicadorVerificaRestServ;

	private String indicadorEncerraAutoASRel;

	private String indicadorVerificarVazamento;

	private String indicadorAcionamentoGasista;

	private String questionario;
	
	private String canalAtendimento;

	private Set<OperacaoEnvioEmail> enviarEmailCadastro;
	private Set<OperacaoEnvioEmail> enviarEmailTramitar;
	private Set<OperacaoEnvioEmail> enviarEmailReiterar;
	private Set<OperacaoEnvioEmail> enviarEmailEncerrar;
	private Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo;

	private String permiteAbrirEmLote;
	private String indicadorComGarantia;

	private Collection<ChamadoAssuntoUnidadeOrganizacional> listaUnidadesVisualizadoras =
			new HashSet<>();

	private Collection<ChamadoAssuntoServicoTipo> listaServicos = new HashSet<>();

	/**
	 * @return indicadorTelefoneSolicitanteObrigatorio
	 */
	public String getIndicadorTelefoneSolicitanteObrigatorio() {

		return indicadorTelefoneSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorTelefoneSolicitanteObrigatorio
	 */
	public void setIndicadorTelefoneSolicitanteObrigatorio(String indicadorTelefoneSolicitanteObrigatorio) {

		this.indicadorTelefoneSolicitanteObrigatorio = indicadorTelefoneSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorEmailSolicitanteObrigatorio
	 */
	public String getIndicadorEmailSolicitanteObrigatorio() {

		return indicadorEmailSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorEmailSolicitanteObrigatorio
	 */
	public void setIndicadorEmailSolicitanteObrigatorio(String indicadorEmailSolicitanteObrigatorio) {

		this.indicadorEmailSolicitanteObrigatorio = indicadorEmailSolicitanteObrigatorio;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return quantidadeHorasPrevistaAtendimento
	 */
	public Integer getQuantidadeHorasPrevistaAtendimento() {

		return quantidadeHorasPrevistaAtendimento;
	}

	/**
	 * @param quantidadeHorasPrevistaAtendimento
	 */
	public void setQuantidadeHorasPrevistaAtendimento(Integer quantidadeHorasPrevistaAtendimento) {

		this.quantidadeHorasPrevistaAtendimento = quantidadeHorasPrevistaAtendimento;
	}

	/**
	 * @return indicadorDiaCorridoUtil
	 */
	public String getIndicadorDiaCorridoUtil() {

		return indicadorDiaCorridoUtil;
	}

	/**
	 * @param indicadorDiaCorridoUtil
	 */
	public void setIndicadorDiaCorridoUtil(String indicadorDiaCorridoUtil) {

		this.indicadorDiaCorridoUtil = indicadorDiaCorridoUtil;
	}

	/**
	 * @return indicadorCredito
	 */
	public String getIndicadorCredito() {

		return indicadorCredito;
	}

	/**
	 * @param indicadorCredito
	 */
	public void setIndicadorCredito(String indicadorCredito) {

		this.indicadorCredito = indicadorCredito;
	}

	/**
	 * @return rubricaCredito
	 */
	public String getRubricaCredito() {

		return rubricaCredito;
	}

	/**
	 * @param rubricaCredito
	 */
	public void setRubricaCredito(String rubricaCredito) {

		this.rubricaCredito = rubricaCredito;
	}

	/**
	 * @return indicadorDebito
	 */
	public String getIndicadorDebito() {

		return indicadorDebito;
	}

	/**
	 * @param indicadorDebito
	 */
	public void setIndicadorDebito(String indicadorDebito) {

		this.indicadorDebito = indicadorDebito;
	}

	/**
	 * @return rubricaDebito
	 */
	public String getRubricaDebito() {

		return rubricaDebito;
	}

	/**
	 * @param rubricaDebito
	 */
	public void setRubricaDebito(String rubricaDebito) {

		this.rubricaDebito = rubricaDebito;
	}

	/**
	 * @return unidadeOrganizacional
	 */
	public String getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	/**
	 * @param unidadeOrganizacional
	 */
	public void setUnidadeOrganizacional(String unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * @return indicadorClienteObrigatorio
	 */
	public String getIndicadorClienteObrigatorio() {

		return indicadorClienteObrigatorio;
	}

	/**
	 * @param indicadorClienteObrigatorio
	 */
	public void setIndicadorClienteObrigatorio(String indicadorClienteObrigatorio) {

		this.indicadorClienteObrigatorio = indicadorClienteObrigatorio;
	}

	/**
	 * @return indicadorImovelObrigatorio
	 */
	public String getIndicadorImovelObrigatorio() {

		return indicadorImovelObrigatorio;
	}

	/**
	 * @param indicadorImovelObrigatorio
	 */
	public void setIndicadorImovelObrigatorio(String indicadorImovelObrigatorio) {

		this.indicadorImovelObrigatorio = indicadorImovelObrigatorio;
	}

	/**
	 * @return indicadorRgSolicitanteObrigatorio
	 */
	public String getIndicadorRgSolicitanteObrigatorio() {

		return indicadorRgSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorRgSolicitanteObrigatorio
	 */
	public void setIndicadorRgSolicitanteObrigatorio(String indicadorRgSolicitanteObrigatorio) {

		this.indicadorRgSolicitanteObrigatorio = indicadorRgSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorCpfCnpjSolicitanteObrigatorio
	 */
	public String getIndicadorCpfCnpjSolicitanteObrigatorio() {

		return indicadorCpfCnpjSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorCpfCnpjSolicitanteObrigatorio
	 */
	public void setIndicadorCpfCnpjSolicitanteObrigatorio(String indicadorCpfCnpjSolicitanteObrigatorio) {

		this.indicadorCpfCnpjSolicitanteObrigatorio = indicadorCpfCnpjSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorTramitacao
	 */
	public String getIndicadorTramitacao() {

		return indicadorTramitacao;
	}

	/**
	 * @param indicadorTramitacao
	 */
	public void setIndicadorTramitacao(String indicadorTramitacao) {

		this.indicadorTramitacao = indicadorTramitacao;
	}

	/**
	 * @return indicadorNomeSolicitanteObrigatorio
	 */
	public String getIndicadorNomeSolicitanteObrigatorio() {

		return indicadorNomeSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorNomeSolicitanteObrigatorio
	 */
	public void setIndicadorNomeSolicitanteObrigatorio(String indicadorNomeSolicitanteObrigatorio) {

		this.indicadorNomeSolicitanteObrigatorio = indicadorNomeSolicitanteObrigatorio;
	}

	/**
	 * @return listaServicos
	 */
	public Collection<ChamadoAssuntoServicoTipo> getListaServicos() {

		return listaServicos;
	}

	/**
	 * @param listaServicos
	 */
	public void setListaServicos(Collection<ChamadoAssuntoServicoTipo> listaServicos) {

		this.listaServicos = listaServicos;
	}

	/**
	 * @return indicadorGeraServicoAutorizacao
	 */
	public String getIndicadorGeraServicoAutorizacao() {

		return indicadorGeraServicoAutorizacao;
	}

	/**
	 * @param indicadorGeraServicoAutorizacao
	 */
	public void setIndicadorGeraServicoAutorizacao(String indicadorGeraServicoAutorizacao) {

		this.indicadorGeraServicoAutorizacao = indicadorGeraServicoAutorizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/**
	 * @return usuarioResponsavel
	 */
	public String getUsuarioResponsavel() {

		return usuarioResponsavel;
	}

	/**
	 * @param usuarioResponsavel
	 */
	public void setUsuarioResponsavel(String usuarioResponsavel) {

		this.usuarioResponsavel = usuarioResponsavel;
	}

	/**
	 * @return the indicadorFechamentoAutomatico
	 */
	public String getIndicadorFechamentoAutomatico() {
		return indicadorFechamentoAutomatico;
	}

	/**
	 * @param indicadorFechamentoAutomatico the indicadorFechamentoAutomatico to set
	 */
	public void setIndicadorFechamentoAutomatico(String indicadorFechamentoAutomatico) {
		this.indicadorFechamentoAutomatico = indicadorFechamentoAutomatico;
	}

	/**
	 * Retorna indicadorPrazoDiferenciado
	 * 
	 * @return the indicadorPrazoDiferenciado
	 */
	public String getIndicadorPrazoDiferenciado() {
		return indicadorPrazoDiferenciado;
	}

	/**
	 * Atribui indicadorPrazoDiferenciado
	 * 
	 * @param indicadorPrazoDiferenciado the indicadorPrazoDiferenciado to set
	 */
	public void setIndicadorPrazoDiferenciado(String indicadorPrazoDiferenciado) {
		this.indicadorPrazoDiferenciado = indicadorPrazoDiferenciado;
	}

	/**
	 * Retorna indicadorUnidadesVisualizadora
	 * 
	 * @return indicadorUnidadesVisualizadora
	 */
	public String getIndicadorUnidadesVisualizadora() {
		return indicadorUnidadesVisualizadora;
	}

	/**
	 * Atribui indicadorUnidadesVisualizadora
	 * 
	 * @param indicadorUnidadesVisualizadora
	 */
	public void setIndicadorUnidadesVisualizadora(String indicadorUnidadesVisualizadora) {
		this.indicadorUnidadesVisualizadora = indicadorUnidadesVisualizadora;
	}

	/**
	 * Retorna listaUnidadesVisualizadoras
	 * 
	 * @return listaUnidadesVisualizadoras
	 */
	public Collection<ChamadoAssuntoUnidadeOrganizacional> getListaUnidadesVisualizadoras() {
		return listaUnidadesVisualizadoras;
	}

	/**
	 * Atribui listaUnidadesVisualizadoras
	 * 
	 * @param listaUnidadesVisualizadoras
	 */
	public void setListaUnidadesVisualizadoras(Collection<ChamadoAssuntoUnidadeOrganizacional> listaUnidadesVisualizadoras) {
		this.listaUnidadesVisualizadoras = listaUnidadesVisualizadoras;
	}

	/**
	 * Retorna indicadorVerificaRestServ
	 * 
	 * @return the indicadorVerificaRestServ
	 */
	public String getIndicadorVerificaRestServ() {
		return indicadorVerificaRestServ;
	}

	/**
	 * Atribui indicadorVerificaRestServ
	 * 
	 * @param indicadorVerificaRestServ the indicadorVerificaRestServ to set
	 */
	public void setIndicadorVerificaRestServ(String indicadorVerificaRestServ) {
		this.indicadorVerificaRestServ = indicadorVerificaRestServ;
	}

	/**
	 * Retorna orientacao
	 * 
	 * @return orientacao
	 */
	public String getOrientacao() {
		return orientacao;
	}

	/**
	 * Atribui orientacao
	 * 
	 * @param orientacao
	 */
	public void setOrientacao(String orientacao) {
		this.orientacao = orientacao;
	}

	/**
	 * Retorna indicadorAcionamentoGasista
	 * 
	 * @return indicadorAcionamentoGasista
	 */
	public String getIndicadorAcionamentoGasista() {
		return indicadorAcionamentoGasista;
	}

	/**
	 * Atribui indicadorAcionamentoGasista
	 * 
	 * @param indicadorAcionamentoGasista
	 */
	public void setIndicadorAcionamentoGasista(String indicadorAcionamentoGasista) {
		this.indicadorAcionamentoGasista = indicadorAcionamentoGasista;
	}

	/**
	 * Retorna questionario
	 * 
	 * @return questionario
	 */
	public String getQuestionario() {
		return questionario;
	}

	/**
	 * Atribui questionario
	 * 
	 * @param questionario
	 */
	public void setQuestionario(String questionario) {
		this.questionario = questionario;
	}

	/**
	 * Retorna indicadorVerificarVazamento
	 * 
	 * @return the indicadorVerificarVazamento
	 */
	public String getIndicadorVerificarVazamento() {
		return indicadorVerificarVazamento;
	}

	/**
	 * Atribui indicadorVerificarVazamento
	 * 
	 * @param indicadorVerificarVazamento the indicadorVerificarVazamento to set
	 */
	public void setIndicadorVerificarVazamento(String indicadorVerificarVazamento) {
		this.indicadorVerificarVazamento = indicadorVerificarVazamento;
	}

	/**
	 * Retorna indicadorEncerraAutoASRel
	 * 
	 * @return the indicadorEncerraAutoASRel
	 */
	public String getIndicadorEncerraAutoASRel() {
		return indicadorEncerraAutoASRel;
	}

	/**
	 * Atribui indicadorEncerraAutoASRel
	 * 
	 * @param indicadorEncerraAutoASRel the indicadorEncerraAutoASRel to set
	 */
	public void setIndicadorEncerraAutoASRel(String indicadorEncerraAutoASRel) {
		this.indicadorEncerraAutoASRel = indicadorEncerraAutoASRel;
	}

	/**
	 * Retorna canalAtendimento
	 * 
	 * @return the canalAtendimento
	 */
	public String getCanalAtendimento() {
		return canalAtendimento;
	}

	/**
	 * Atribui canalAtendimento
	 * 
	 * @param canalAtendimento the canalAtendimento to set
	 */
	public void setCanalAtendimento(String canalAtendimento) {
		this.canalAtendimento = canalAtendimento;
	}

	/**
	 * Obtém as configurações de enviar email cadastro
	 * @return enviarEmailCadastro
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailCadastro() {
		return enviarEmailCadastro;
	}

	/**
	 * Configura o email cadastro
	 * @param enviarEmailCadastro
	 */
	public void setEnviarEmailCadastro(Set<OperacaoEnvioEmail> enviarEmailCadastro) {
		this.enviarEmailCadastro = enviarEmailCadastro;
	}

	/**
	 * Obtém as configurações de enviar email tramitar
	 * @return enviarEmailTramitar
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailTramitar() {
		return enviarEmailTramitar;
	}

	/**
	 * Configura o email tramitar
	 * @param enviarEmailTramitar
	 */
	public void setEnviarEmailTramitar(Set<OperacaoEnvioEmail> enviarEmailTramitar) {
		this.enviarEmailTramitar = enviarEmailTramitar;
	}

	/**
	 * Obtém as configurações de enviar email reiterar
	 * @return enviarEmailReiterar
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailReiterar() {
		return enviarEmailReiterar;
	}

	/**
	 * Configura o email reiterar
	 * @param enviarEmailReiterar
	 */
	public void setEnviarEmailReiterar(Set<OperacaoEnvioEmail> enviarEmailReiterar) {
		this.enviarEmailReiterar = enviarEmailReiterar;
	}

	/**
	 * Obtém o email encerrar
	 * @return
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailEncerrar() {
		return enviarEmailEncerrar;
	}

	/**
	 * Configura o email encerrar
	 * @param enviarEmailEncerrar
	 */
	public void setEnviarEmailEncerrar(Set<OperacaoEnvioEmail> enviarEmailEncerrar) {
		this.enviarEmailEncerrar = enviarEmailEncerrar;
	}

	/**
	 * Obtém as opções de prioridade para envio de email
	 * @return
	 */
	public Set<OperacaoEnvioEmailPrioridade> getEnviarEmailPrioridadeProtocolo() {
		return enviarEmailPrioridadeProtocolo;
	}

	/**
	 * Configura a priodade do envio de email do protocolo
	 * @param enviarEmailPrioridadeProtocolo
	 */
	public void setEnviarEmailPrioridadeProtocolo(Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo) {
		this.enviarEmailPrioridadeProtocolo = enviarEmailPrioridadeProtocolo;
	}
	
	/**
	 * Ontém o indicador se permite abrir chamado em lote
	 * @return
	 */
	public String getPermiteAbrirEmLote() {
		return permiteAbrirEmLote;
	}

	/**
	 * Configura a opção se permite abrir chamado em lote
	 * @param permiteAbrirEmLote
	 */
	public void setPermiteAbrirEmLote(String permiteAbrirEmLote) {
		this.permiteAbrirEmLote = permiteAbrirEmLote;
	}

	/**
	 * Obtém o indicador se permite ter ou não garantia
	 * @return
	 */
	public String getIndicadorComGarantia() {
		return indicadorComGarantia;
	}

	/**
	 * Configura a opção se tem ou não garantia
	 * @param indicadorComGarantia
	 */
	public void setIndicadorComGarantia(String indicadorComGarantia) {
		this.indicadorComGarantia = indicadorComGarantia;
	}
}

