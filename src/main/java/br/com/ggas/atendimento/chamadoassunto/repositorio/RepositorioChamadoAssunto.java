package br.com.ggas.atendimento.chamadoassunto.repositorio;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.dominio.OperacaoEnvioEmail;
import br.com.ggas.atendimento.chamadoassunto.dominio.OperacaoEnvioEmailPrioridade;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeClasseImpl;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Repositorio Chamado Assunto
 *
 */
@Repository
public class RepositorioChamadoAssunto extends RepositorioGenerico {

	private static final String CHAMADO = " chamado ";
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String FROM = " from ";
	private static final String WHERE = " where ";

	/**
	 * Instantiates a new repositorio chamado assunto.
	 * 
	 * @param sessionFactory the session factory
	 */
	@Autowired
	public RepositorioChamadoAssunto(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ChamadoAssunto();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.RepositorioGenerico#getClasseEntidade()
	 */
	@Override
	public Class<ChamadoAssunto> getClasseEntidade() {

		return ChamadoAssunto.class;
	}
	
	public Class<EntidadeClasseImpl> getEntidadeClasse(){
		return EntidadeClasseImpl.class;
	}

	/**
	 * Consultar chamado assunto.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the chamado assunto
	 */
	@SuppressWarnings("squid:S1192")
	public ChamadoAssunto consultarChamadoAssunto(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CHAMADO);
		hql.append(" left join fetch chamado.chamadoTipo chamadoTipo");
		hql.append(" left join fetch chamado.rubricaCredito ");
		hql.append(" left join fetch chamado.rubricaDebito ");
		hql.append(" left join fetch chamado.unidadeOrganizacional ");
		hql.append(" left join fetch chamado.indicadorDiaCorridoUtil ");
		hql.append(" left join fetch chamado.listaUnidadeOrganizacionalVisualizadora unidadeVisualizadora ");
		hql.append(" left join fetch unidadeVisualizadora.unidadeOrganizacional ");
		hql.append(" left join fetch chamado.canalAtendimento");
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria= :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		return (ChamadoAssunto) query.uniqueResult();
	}

	/**
	 * Consultar chamado.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<ChamadoAssunto> consultarChamado(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CHAMADO);
		hql.append(" inner join fetch chamado.chamadoTipo chamadoTipo");
		hql.append(WHERE);
		hql.append(" chamado.habilitado = true and ");
		hql.append(" chamadoTipo.chavePrimaria= :chavePrimaria ");
		hql.append(" order by chamado.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		return query.list();
	}

	/**
	 * Obter ordenado.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<ChamadoAssunto> obterOrdenado() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new ChamadoAssunto(chamadoAssunto.chavePrimaria, chamadoAssunto.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamadoAssunto ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Verifica se existe questionario
	 * 
	 * @param questionario {@link Questionario}
	 * @return true se existir, false caso contrario
	 */
	public boolean existeChamadoAssuntoQuestionario(Questionario questionario) {
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias("questionario", "questionario");
		criteria.add(Restrictions.eq("questionario.chavePrimaria", questionario.getChavePrimaria()));

		@SuppressWarnings("unchecked")
		Collection<Questionario> lista = criteria.list();
		return lista != null && !lista.isEmpty();
	}

	/**
	 * Obter lista de chamado assuntos, filtrado por Tipo de chamado e ordenado por
	 * descrição
	 * 
	 * @param chamadoTipo {@link Long}
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<ChamadoAssunto> obterChamadoAssuntoPorChaveChamadoTipo(Long chamadoTipo) {

		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new ChamadoAssunto(chamadoAssunto.chavePrimaria, chamadoAssunto.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamadoAssunto ");
		hql.append(" where chamadoTipo = :chamadoTipo ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chamadoTipo", chamadoTipo);

		return query.list();

	}

	
	public Collection<ChamadoAssunto> obterChamadoAssuntoAtivoPorChaveChamadoTipo(Long chamadoTipo) {
		
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new ChamadoAssunto(chamadoAssunto.chavePrimaria, chamadoAssunto.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamadoAssunto ");
		hql.append(" where chamadoTipo = :chamadoTipo ");
		hql.append(" and habilitado = true ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chamadoTipo", chamadoTipo);
		
		return query.list();

	}
	/**
	 * Obter lista de chamado assuntos, filtrado por Tipo de chamado e ordenado por
	 * descrição
	 *
	 * @param chamadoTipo {@link Long}
	 * @return the collection {@link Collection}
	 * @throws NegocioException the negocio exception {@link NegocioException}
	 */
	public Collection<ChamadoAssunto> obterChamadoAssuntoTipo(Long chamadoTipo) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamadoAssunto ");
		hql.append(" where chamadoTipo = :chamadoTipo ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chamadoTipo", chamadoTipo);

		return query.list();

	}

	/**
	 * Carregar o chamado assunto projetado, através da chave primária
	 *
	 * @param chavePrimaria para filtrar o chamado assunto
	 * @returno chamado assunto projetado
	 */
	public ChamadoAssunto carregarChamadoAssuntoProjetado(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new ChamadoAssunto(chamadoAssunto.chavePrimaria, chamadoAssunto.descricao, ");
		hql.append(" chamadoAssunto.quantidadeHorasPrevistaAtendimento, chamadoAssunto.orientacao, ");
		hql.append(" chamadoAssunto.indicadorAcionamentoGasista, chamadoAssunto.indicadorPrazoDiferenciado, ");
		hql.append(" chamadoAssunto.indicadorUnidadesOrganizacionalVisualizadora, ");
		hql.append(
				" chamadoAssunto.indicadorGeraServicoAutorizacao, chamadoAssunto.indicadorNomeSolicitanteObrigatorio, ");
		hql.append(
				" chamadoAssunto.indicadorCpfCnpjSolicitanteObrigatorio, chamadoAssunto.indicadorRgSolicitanteObrigatorio,");
		hql.append(" chamadoAssunto.indicadorTelefoneSolicitanteObrigatorio, ");
		hql.append(" chamadoAssunto.indicadorEmailSolicitanteObrigatorio, chamadoAssunto.indicadorClienteObrigatorio,");
		hql.append(" chamadoAssunto.indicadorImovelObrigatorio, ");
		hql.append(" chamadoAssunto.indicadorFechamentoAutomatico, ");
		hql.append(" canalAtendimento.chavePrimaria, canalAtendimento.descricao, ");
		hql.append(" indicadorDiaCorridoUtil.chavePrimaria, indicadorDiaCorridoUtil.descricao, ");
		hql.append(" unidadeOrganizacional.chavePrimaria, unidadeOrganizacional.emailContato, ");
		hql.append(
				" unidadeSuperior.chavePrimaria, unidadeSuperior.emailContato, chamadoTipo.chavePrimaria, chamadoTipo.descricao, ");
		hql.append(" chamadoTipo.categoria, segmento.chavePrimaria, segmento.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" chamadoAssunto ");
		hql.append(" left join chamadoAssunto.canalAtendimento canalAtendimento ");
		hql.append(" left join chamadoAssunto.chamadoTipo chamadoTipo ");
		hql.append(" left join chamadoTipo.segmento segmento ");
		hql.append(" left join chamadoAssunto.indicadorDiaCorridoUtil indicadorDiaCorridoUtil ");
		hql.append(" left join chamadoAssunto.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(" left join unidadeOrganizacional.unidadeSuperior unidadeSuperior ");
		hql.append(WHERE);
		hql.append(" chamadoAssunto.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) query.uniqueResult();

		List<OperacaoEnvioEmail> enviarEmailCadastro = this
				.carregarOperacoesEmailChamadoAssunto("CHAS_OPER_EMAIL_CADASTRO", chavePrimaria);
		List<OperacaoEnvioEmail> enviarEmailEncerrar = this
				.carregarOperacoesEmailChamadoAssunto("CHAS_OPER_EMAIL_ENCERRAR", chavePrimaria);
		List<OperacaoEnvioEmail> enviarEmailReiterar = this
				.carregarOperacoesEmailChamadoAssunto("CHAS_OPER_EMAIL_REITERAR", chavePrimaria);
		List<OperacaoEnvioEmail> enviarEmailTramitar = this
				.carregarOperacoesEmailChamadoAssunto("CHAS_OPER_EMAIL_TRAMITAR", chavePrimaria);
		List<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo = this
				.carregarOperacoesEmailChamadoAssuntoPrioridade("CHAS_OPER_EMAIL_PRIORI_PROTOCO", chavePrimaria);

		chamadoAssunto.setEnviarEmailCadastro(new HashSet<>(enviarEmailCadastro));
		chamadoAssunto.setEnviarEmailEncerrar(new HashSet<>(enviarEmailEncerrar));
		chamadoAssunto.setEnviarEmailReiterar(new HashSet<>(enviarEmailReiterar));
		chamadoAssunto.setEnviarEmailTramitar(new HashSet<>(enviarEmailTramitar));
		chamadoAssunto.setEnviarEmailPrioridadeProtocolo(new HashSet<>(enviarEmailPrioridadeProtocolo));

		return chamadoAssunto;
	}

	private List<OperacaoEnvioEmail> carregarOperacoesEmailChamadoAssunto(String tabela, Long chavePrimaria) {
		StringBuilder hql = new StringBuilder("SELECT OPEE_DS_OPERACAO as OPERACAO FROM ");
		hql.append(tabela);
		hql.append(" WHERE CHAS_CD = :chavePrimaria");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		List<Map<String, Object>> rows = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();

		List<OperacaoEnvioEmail> lista = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			OperacaoEnvioEmail operacaoEnvioEmail = OperacaoEnvioEmail.valueOf((String) row.get("OPERACAO"));
			lista.add(operacaoEnvioEmail);
		}
		return lista;
	}

	private List<OperacaoEnvioEmailPrioridade> carregarOperacoesEmailChamadoAssuntoPrioridade(String tabela,
			Long chavePrimaria) {
		StringBuilder hql = new StringBuilder("SELECT OPEE_DS_OPERACAO as OPERACAO FROM ");
		hql.append(tabela);
		hql.append(" WHERE CHAS_CD = :chavePrimaria");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		List<Map<String, Object>> rows = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();

		List<OperacaoEnvioEmailPrioridade> lista = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			OperacaoEnvioEmailPrioridade operacaoEnvioEmailPrioridade = OperacaoEnvioEmailPrioridade
					.valueOf((String) row.get("OPERACAO"));
			lista.add(operacaoEnvioEmailPrioridade);
		}
		return lista;
	}

	/**
	 * Carregar usuário responsável da chave do chamado assunto
	 *
	 * @param chavePrimaria para filtrar o usuário
	 * @return o usuário responsável
	 */
	public Usuario carregarUsuarioResponsavelPorChamadoAssunto(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append(
				"SELECT new UsuarioImpl(chamado.usuarioResponsavel.chavePrimaria, chamado.usuarioResponsavel.funcionario.chavePrimaria, ");
		hql.append(
				" chamado.usuarioResponsavel.funcionario.nome, chamado.usuarioResponsavel.funcionario.usuario.chavePrimaria) ");
		fromChamadoAssuntoWhereChavePrimaria(hql);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		return (Usuario) query.uniqueResult();
	}

	private void fromChamadoAssuntoWhereChavePrimaria(StringBuilder hql) {
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CHAMADO);
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria= :chavePrimaria ");
	}

	/**
	 * Retorna uma lista de unidades organizacionais com os campos projetados
	 *
	 * @param chaveAssuntoChamado para filtrar as unidades
	 * @return uma coleção de unidades organizacionais
	 */
	public Collection<UnidadeOrganizacional> obterUnidadesOrganizacionalVisualizadoraPorChaveChamadoAssunto(
			Long chaveAssuntoChamado) {

		StringBuilder hql = new StringBuilder();
		hql.append(
				" SELECT new UnidadeOrganizacionalImpl(unidadeOrganizacional.chavePrimaria, unidadeOrganizacional.descricao,"
						+ "unidadeOrganizacional.emailContato,unidadeOrganizacional.unidadeSuperior,unidadeOrganizacional.indicadorEmail)");
		hql.append(FROM);
		hql.append(" ChamadoAssuntoUnidadeOrganizacional chamadoAssuntoUnidadeOrganizacional ");
		hql.append(" INNER JOIN chamadoAssuntoUnidadeOrganizacional.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(" LEFT JOIN chamadoAssuntoUnidadeOrganizacional.chamadoAssunto chamadoAssunto ");
		hql.append(" WHERE chamadoAssunto.chavePrimaria = :chaveAssuntoChamado ");
		hql.append(" ORDER BY unidadeOrganizacional.descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveAssuntoChamado", chaveAssuntoChamado);

		@SuppressWarnings("unchecked")
		Collection<UnidadeOrganizacional> unidades = query.list();

		return unidades;
	}

	/**
	 * Obtém a lista de {@link ChamadoAssunto} que permitem abertura em lote a
	 * partir do tipo do chamado
	 * 
	 * @param tipoChamado tipo do chamado
	 * @return retorna a lista de chamados de assunto
	 */
	public Collection<ChamadoAssunto> obterChamadosAssuntoAberturaEmLote(Long tipoChamado) {
		return getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery("select new ChamadoAssunto(c.chavePrimaria, c.descricao) from ChamadoAssunto c "
						+ "where c.chamadoTipo = :tipoChamado and c.permiteAbrirEmLote = true " + "order by descricao")
				.setLong("tipoChamado", tipoChamado).list();
	}
	
	public EntidadeClasse obterEntidadeClassePeloIndicadorRegulatorio() {
		StringBuilder hql = new StringBuilder();
		
		hql.append(FROM);
		hql.append(getEntidadeClasse().getSimpleName());
		hql.append(" WHERE descricao =:indicadorRegulatorio ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("indicadorRegulatorio", "INDICADOR ARSAL");
		
		return (EntidadeClasse) query.uniqueResult();
	}

}
