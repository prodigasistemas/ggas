/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/09/2013 14:12:54
 @author vtavares
 */

package br.com.ggas.atendimento.chamadoassunto.dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.ggas.atendimento.chamado.dominio.AcaoChamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoAssuntoUnidadeOrganizacional;
import br.com.ggas.atendimento.chamado.dominio.ChamadoCategoria;
import br.com.ggas.atendimento.chamadoassuntoservicotipo.dominio.ChamadoAssuntoServicoTipo;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.impl.UnidadeOrganizacionalImpl;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * ChamadoAssunto
 *
 */
public class ChamadoAssunto extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	private static final Logger LOG = Logger.getLogger(ChamadoAssunto.class);

	private static final long serialVersionUID = -7549989817273225343L;

	private String descricao;

	private String orientacao;

	private ChamadoTipo chamadoTipo;

	private Integer quantidadeHorasPrevistaAtendimento;

	private EntidadeConteudo indicadorDiaCorridoUtil;

	private Boolean indicadorCredito;

	private Rubrica rubricaCredito;

	private Boolean indicadorDebito;

	private Rubrica rubricaDebito;

	private UnidadeOrganizacional unidadeOrganizacional;

	private CanalAtendimento canalAtendimento;

	private Boolean indicadorClienteObrigatorio;

	private Boolean indicadorImovelObrigatorio;

	private Boolean indicadorRgSolicitanteObrigatorio;

	private Boolean indicadorCpfCnpjSolicitanteObrigatorio;

	private Boolean indicadorTramitacao;

	private Boolean indicadorNomeSolicitanteObrigatorio;

	private Boolean indicadorTelefoneSolicitanteObrigatorio;

	private Boolean indicadorGeraServicoAutorizacao;

	private Usuario usuarioResponsavel;
	
	private Boolean indicadorEmailSolicitanteObrigatorio;
		
	private Boolean indicadorFechamentoAutomatico;
	private Boolean indicadorPrazoDiferenciado;
	
	private Set<OperacaoEnvioEmail> enviarEmailCadastro;
	private Set<OperacaoEnvioEmail> enviarEmailTramitar;
	private Set<OperacaoEnvioEmail> enviarEmailReiterar;
	private Set<OperacaoEnvioEmail> enviarEmailEncerrar;
	private Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo;

	private Collection<ChamadoAssuntoServicoTipo> listaServicos = new HashSet<>();

	private Boolean indicadorUnidadesOrganizacionalVisualizadora;

	private Collection<ChamadoAssuntoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora = new HashSet<>();

	private Boolean indicadorVerificaRestServ;

	private Boolean indicadorEncerraAutoASRel;

	private Boolean indicadorVerificarVazamento;

	private Boolean indicadorAcionamentoGasista;

	private Questionario questionario;

	private Boolean permiteAbrirEmLote;

	private Boolean indicadorComGarantia;
	
	private EntidadeConteudo indicadorRegulatorio;
	
	private String nomeRelatorioRegulatorio;

	/**
	 * Construtor padrão da classe
	 */
	public ChamadoAssunto() {
	}

	/**
	 * Construtor para projeção de consultas
	 *
	 * @param chavePrimaria {@link Long}
	 * @param descricao {@link String}
	 */
	public ChamadoAssunto(Long chavePrimaria, String descricao){
		this.setChavePrimaria(chavePrimaria);
		this.descricao = descricao;
	}

	/**
	 * Construtor utilizado para projetar chamado assunto, para otimizar consultas
	 *
	 * @param chavePrimaria {@link Long}
	 * @param descricao {@link String}
	 * @param quantidadeHorasPrevistaAtendimento {@link Integer}
	 * @param orientacao {@link String}
	 * @param indicadorAcionamentoGasista {@link Boolean}
	 * @param indicadorPrazoDiferenciado {@link Boolean}
	 * @param indicadorUnidadesOrganizacionalVisualizadora {@link Boolean}
	 * @param indicadorGeraServicoAutorizacao {@link Boolean}
	 * @param indicadorNomeSolicitanteObrigatorio {@link Boolean}
	 * @param indicadorCpfCnpjSolicitanteObrigatorio {@link Boolean}
	 * @param indicadorRgSolicitanteObrigatorio {@link Boolean}
	 * @param indicadorTelefoneSolicitanteObrigatorio {@link Boolean}
	 * @param indicadorEmailSolicitanteObrigatorio {@link Boolean}
	 * @param indicadorClienteObrigatorio {@link Boolean}
	 * @param indicadorImovelObrigatorio {@link Boolean}
	 * @param indicadorFechamentoAutomatico {@link Boolean}
	 * @param chavePrimariaCanalAtendimento {@link Long}
	 * @param descricaoCanalAtendimento {@link String}
	 * @param chavePrimariaIndicadorDiaCorridoUtil {@link Long}
	 * @param descricaoDiaCorridoUtil {@link String}
	 * @param chavePrimariaUnidadeOrganizacional {@link Long}
	 * @param emailContatoUnidadeOrganizacional {@link String}
	 * @param chavePrimariaUnidadeOrganizacionalSuperior {@link Long}
	 * @param chavePrimariaChamadoTipo {@link Long}
	 * @param descricaoChamadoTipo {@link String}
	 * @param categoriaChamadoTipo {@link ChamadoCategoria}
	 * @param chavePrimariaSegmento {@link Long}
	 * @param descricaoSegmento {@link String}
	 * @param emailContatoUnidadeOrganizacionalSuperior {@link String}
	 */
	public ChamadoAssunto(Long chavePrimaria, String descricao,
			Integer quantidadeHorasPrevistaAtendimento, String orientacao,
			Boolean indicadorAcionamentoGasista,
			Boolean indicadorPrazoDiferenciado, Boolean indicadorUnidadesOrganizacionalVisualizadora,
			Boolean indicadorGeraServicoAutorizacao,
			Boolean indicadorNomeSolicitanteObrigatorio, Boolean indicadorCpfCnpjSolicitanteObrigatorio,
			Boolean indicadorRgSolicitanteObrigatorio,
			Boolean indicadorTelefoneSolicitanteObrigatorio,
			Boolean indicadorEmailSolicitanteObrigatorio,
			Boolean indicadorClienteObrigatorio, Boolean indicadorImovelObrigatorio,Boolean indicadorFechamentoAutomatico,
			Long chavePrimariaCanalAtendimento, String descricaoCanalAtendimento,
			Long chavePrimariaIndicadorDiaCorridoUtil, String descricaoDiaCorridoUtil,
			Long chavePrimariaUnidadeOrganizacional, String emailContatoUnidadeOrganizacional,
			Long chavePrimariaUnidadeOrganizacionalSuperior, String emailContatoUnidadeOrganizacionalSuperior,
			Long chavePrimariaChamadoTipo, String descricaoChamadoTipo, ChamadoCategoria categoriaChamadoTipo,
			Long chavePrimariaSegmento, String descricaoSegmento) {
		this.setChavePrimaria(chavePrimaria);
		this.descricao = descricao;
		this.orientacao = orientacao;
		this.quantidadeHorasPrevistaAtendimento = quantidadeHorasPrevistaAtendimento;
		this.indicadorAcionamentoGasista = indicadorAcionamentoGasista;
		this.indicadorPrazoDiferenciado = indicadorPrazoDiferenciado;
		this.indicadorUnidadesOrganizacionalVisualizadora = indicadorUnidadesOrganizacionalVisualizadora;
		this.indicadorGeraServicoAutorizacao = indicadorGeraServicoAutorizacao;
		this.indicadorNomeSolicitanteObrigatorio = indicadorNomeSolicitanteObrigatorio;
		this.indicadorCpfCnpjSolicitanteObrigatorio = indicadorCpfCnpjSolicitanteObrigatorio;
		this.indicadorRgSolicitanteObrigatorio = indicadorRgSolicitanteObrigatorio;
		this.indicadorTelefoneSolicitanteObrigatorio = indicadorTelefoneSolicitanteObrigatorio;
		this.indicadorEmailSolicitanteObrigatorio = indicadorEmailSolicitanteObrigatorio;
		this.indicadorClienteObrigatorio = indicadorClienteObrigatorio;
		this.indicadorImovelObrigatorio = indicadorImovelObrigatorio;
		this.indicadorFechamentoAutomatico = indicadorFechamentoAutomatico;
		if (chavePrimariaCanalAtendimento != null) {
			this.canalAtendimento = new CanalAtendimentoImpl();
			this.canalAtendimento.setChavePrimaria(chavePrimariaCanalAtendimento);
			this.canalAtendimento.setDescricao(descricaoCanalAtendimento);
		}
		if (chavePrimariaIndicadorDiaCorridoUtil != null) {
			this.indicadorDiaCorridoUtil = new EntidadeConteudoImpl();
			this.indicadorDiaCorridoUtil.setChavePrimaria(chavePrimariaIndicadorDiaCorridoUtil);
			this.indicadorDiaCorridoUtil.setDescricao(descricaoDiaCorridoUtil);
		}
		if (chavePrimariaUnidadeOrganizacional != null) {
			this.unidadeOrganizacional = new UnidadeOrganizacionalImpl();
			this.unidadeOrganizacional.setChavePrimaria(chavePrimariaUnidadeOrganizacional);
			this.unidadeOrganizacional.setEmailContato(emailContatoUnidadeOrganizacional);
			if (chavePrimariaUnidadeOrganizacionalSuperior != null) {
				UnidadeOrganizacional unidadeSuperior = new UnidadeOrganizacionalImpl();
				unidadeSuperior.setChavePrimaria(chavePrimariaUnidadeOrganizacionalSuperior);
				unidadeSuperior.setEmailContato(emailContatoUnidadeOrganizacionalSuperior);
				this.unidadeOrganizacional.setUnidadeSuperior(unidadeSuperior);
			}
		}
		if (chavePrimariaChamadoTipo != null) {
			this.chamadoTipo = new ChamadoTipo();
			this.chamadoTipo.setChavePrimaria(chavePrimariaChamadoTipo);
			this.chamadoTipo.setDescricao(descricaoChamadoTipo);
			if (categoriaChamadoTipo != null) {
				this.chamadoTipo.setCategoria(categoriaChamadoTipo);
			}
			if (chavePrimariaSegmento != null) {
				SegmentoImpl segmento = new SegmentoImpl();
				segmento.setChavePrimaria(chavePrimariaSegmento);
				segmento.setDescricao(descricaoSegmento);
				this.chamadoTipo.setSegmento(segmento);
			}
		}
	}

	/**
	 * @return indicadorTelefoneSolicitanteObrigatorio
	 */
	public Boolean getIndicadorTelefoneSolicitanteObrigatorio() {

		return indicadorTelefoneSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorTelefoneSolicitanteObrigatorio
	 */
	public void setIndicadorTelefoneSolicitanteObrigatorio(Boolean indicadorTelefoneSolicitanteObrigatorio) {

		this.indicadorTelefoneSolicitanteObrigatorio = indicadorTelefoneSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorEmailSolicitanteObrigatorio
	 */
	public Boolean getIndicadorEmailSolicitanteObrigatorio() {

		return indicadorEmailSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorEmailSolicitanteObrigatorio
	 */
	public void setIndicadorEmailSolicitanteObrigatorio(Boolean indicadorEmailSolicitanteObrigatorio) {

		this.indicadorEmailSolicitanteObrigatorio = indicadorEmailSolicitanteObrigatorio;
	}

	/**
	 * @return descricao
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return chamadoTipo
	 */
	public ChamadoTipo getChamadoTipo() {

		return chamadoTipo;
	}

	/**
	 * @param chamadoTipo
	 */
	public void setChamadoTipo(ChamadoTipo chamadoTipo) {

		this.chamadoTipo = chamadoTipo;
	}

	/**
	 * @return quantidadeHorasPrevistaAtendimento
	 */
	public Integer getQuantidadeHorasPrevistaAtendimento() {

		return quantidadeHorasPrevistaAtendimento;
	}

	/**
	 * @param quantidadeHorasPrevistaAtendimento
	 */
	public void setQuantidadeHorasPrevistaAtendimento(Integer quantidadeHorasPrevistaAtendimento) {

		this.quantidadeHorasPrevistaAtendimento = quantidadeHorasPrevistaAtendimento;
	}

	/**
	 * @return indicadorDiaCorridoUtil
	 */
	public EntidadeConteudo getIndicadorDiaCorridoUtil() {

		return indicadorDiaCorridoUtil;
	}

	/**
	 * @param indicadorDiaCorridoUtil
	 */
	public void setIndicadorDiaCorridoUtil(EntidadeConteudo indicadorDiaCorridoUtil) {

		this.indicadorDiaCorridoUtil = indicadorDiaCorridoUtil;
	}

	/**
	 * @return indicadorCredito
	 */
	public Boolean getIndicadorCredito() {

		return indicadorCredito;
	}

	/**
	 * @param indicadorCredito
	 */
	public void setIndicadorCredito(Boolean indicadorCredito) {

		this.indicadorCredito = indicadorCredito;
	}

	/**
	 * @return rubricaCredito
	 */
	public Rubrica getRubricaCredito() {

		return rubricaCredito;
	}

	/**
	 * @param rubricaCredito
	 */
	public void setRubricaCredito(Rubrica rubricaCredito) {

		this.rubricaCredito = rubricaCredito;
	}

	/**
	 * @return indicadorDebito
	 */
	public Boolean getIndicadorDebito() {

		return indicadorDebito;
	}

	/**
	 * @param indicadorDebito
	 */
	public void setIndicadorDebito(Boolean indicadorDebito) {

		this.indicadorDebito = indicadorDebito;
	}

	/**
	 * @return rubricaDebito
	 */
	public Rubrica getRubricaDebito() {

		return rubricaDebito;
	}

	/**
	 * @param rubricaDebito
	 */
	public void setRubricaDebito(Rubrica rubricaDebito) {

		this.rubricaDebito = rubricaDebito;
	}

	/**
	 * @return unidadeOrganizacional
	 */
	public UnidadeOrganizacional getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	/**
	 * @param unidadeOrganizacional
	 */
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/**
	 * @return indicadorClienteObrigatorio
	 */
	public Boolean getIndicadorClienteObrigatorio() {

		return indicadorClienteObrigatorio;
	}

	/**
	 * @param indicadorClienteObrigatorio
	 */
	public void setIndicadorClienteObrigatorio(Boolean indicadorClienteObrigatorio) {

		this.indicadorClienteObrigatorio = indicadorClienteObrigatorio;
	}

	/**
	 * @return indicadorImovelObrigatorio
	 */
	public Boolean getIndicadorImovelObrigatorio() {

		return indicadorImovelObrigatorio;
	}

	/**
	 * @param indicadorImovelObrigatorio
	 */
	public void setIndicadorImovelObrigatorio(Boolean indicadorImovelObrigatorio) {

		this.indicadorImovelObrigatorio = indicadorImovelObrigatorio;
	}

	/**
	 * @return indicadorRgSolicitanteObrigatorio
	 */
	public Boolean getIndicadorRgSolicitanteObrigatorio() {

		return indicadorRgSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorRgSolicitanteObrigatorio
	 */
	public void setIndicadorRgSolicitanteObrigatorio(Boolean indicadorRgSolicitanteObrigatorio) {

		this.indicadorRgSolicitanteObrigatorio = indicadorRgSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorCpfCnpjSolicitanteObrigatorio
	 */
	public Boolean getIndicadorCpfCnpjSolicitanteObrigatorio() {

		return indicadorCpfCnpjSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorCpfCnpjSolicitanteObrigatorio
	 */
	public void setIndicadorCpfCnpjSolicitanteObrigatorio(Boolean indicadorCpfCnpjSolicitanteObrigatorio) {

		this.indicadorCpfCnpjSolicitanteObrigatorio = indicadorCpfCnpjSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorTramitacao
	 */
	public Boolean getIndicadorTramitacao() {

		return indicadorTramitacao;
	}

	/**
	 * @param indicadorTramitacao
	 */
	public void setIndicadorTramitacao(Boolean indicadorTramitacao) {

		this.indicadorTramitacao = indicadorTramitacao;
	}

	/**
	 * @return listaServicos
	 */
	public Collection<ChamadoAssuntoServicoTipo> getListaServicos() {

		return listaServicos;
	}

	/**
	 * @param listaServicos
	 */
	public void setListaServicos(Collection<ChamadoAssuntoServicoTipo> listaServicos) {

		this.listaServicos = listaServicos;
	}

	/**
	 * @return indicadorNomeSolicitanteObrigatorio
	 */
	public Boolean getIndicadorNomeSolicitanteObrigatorio() {

		return indicadorNomeSolicitanteObrigatorio;
	}

	/**
	 * @param indicadorNomeSolicitanteObrigatorio
	 */
	public void setIndicadorNomeSolicitanteObrigatorio(Boolean indicadorNomeSolicitanteObrigatorio) {

		this.indicadorNomeSolicitanteObrigatorio = indicadorNomeSolicitanteObrigatorio;
	}

	/**
	 * @return indicadorGeraServicoAutorizacao
	 */
	public Boolean getIndicadorGeraServicoAutorizacao() {

		return indicadorGeraServicoAutorizacao;
	}

	/**
	 * @param indicadorGeraServicoAutorizacao
	 */
	public void setIndicadorGeraServicoAutorizacao(Boolean indicadorGeraServicoAutorizacao) {

		this.indicadorGeraServicoAutorizacao = indicadorGeraServicoAutorizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		StringBuilder stringBuilder2 = new StringBuilder();
		String camposObrigatorios2 = null;

		if(StringUtils.isEmpty(descricao) || descricao.trim().length() == 0) {
			stringBuilder.append("Descrição do Assunto");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(quantidadeHorasPrevistaAtendimento == null) {
			stringBuilder.append("Quantidade de Horas");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(quantidadeHorasPrevistaAtendimento != null && quantidadeHorasPrevistaAtendimento <= 0) {
			stringBuilder2.append("Quantidade de Horas");
			stringBuilder2.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(unidadeOrganizacional == null) {
			stringBuilder.append("Unidade Organizacional");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorDiaCorridoUtil == null) {
			stringBuilder.append("Dias");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorClienteObrigatorio == null) {
			stringBuilder.append("Cliente");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorImovelObrigatorio == null) {
			stringBuilder.append("Imóvel");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorCpfCnpjSolicitanteObrigatorio == null) {
			stringBuilder.append("CPF/CNPJ");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorNomeSolicitanteObrigatorio == null) {
			stringBuilder.append("Nome");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorCredito == null) {
			stringBuilder.append("Gera Crédito");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorDebito == null) {
			stringBuilder.append("Gera Débito");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);

		}

		if(indicadorDebito != null && rubricaDebito == null && indicadorDebito) {
			stringBuilder.append("Rubrica Débito");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorCredito != null && rubricaCredito == null && indicadorCredito) {
			stringBuilder.append("Rubrica Crédito");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorRgSolicitanteObrigatorio == null) {
			stringBuilder.append("RG");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorTramitacao == null) {
			stringBuilder.append("Tramitação");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		camposObrigatorios2 = stringBuilder2.toString();

		if(camposObrigatorios.length() > 0) {

			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		if(camposObrigatorios2.length() > 0) {

			erros.put(Constantes.QUANTIDADE_MINIMA_DE_HORAS, camposObrigatorios2.substring(0, stringBuilder2.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * @return usuarioResponsavel
	 */
	public Usuario getUsuarioResponsavel() {

		return usuarioResponsavel;
	}

	/**
	 * @param usuarioResponsavel
	 */
	public void setUsuarioResponsavel(Usuario usuarioResponsavel) {

		this.usuarioResponsavel = usuarioResponsavel;
	}

	/**
	 * @return the indicadorFechamentoAutomatico
	 */
	public Boolean getIndicadorFechamentoAutomatico() {
		return indicadorFechamentoAutomatico;
	}

	/**
	 * @param indicadorFechamentoAutomatico the indicadorFechamentoAutomatico to set
	 */
	public void setIndicadorFechamentoAutomatico(Boolean indicadorFechamentoAutomatico) {
		this.indicadorFechamentoAutomatico = indicadorFechamentoAutomatico;
	}

	/**
	 * @return listaUnidadeOrganizacionalVisualizadora
	 */
	public Collection<ChamadoAssuntoUnidadeOrganizacional> getListaUnidadeOrganizacionalVisualizadora() {
		return listaUnidadeOrganizacionalVisualizadora;
	}

	/**
	 * setListaUnidadeOrganizacionalVisualizadora
	 * 
	 * @param listaUnidadeOrganizacionalVisualizadora {@link Collection}
	 */
	public void setListaUnidadeOrganizacionalVisualizadora(
			Collection<ChamadoAssuntoUnidadeOrganizacional> listaUnidadeOrganizacionalVisualizadora) {
		this.listaUnidadeOrganizacionalVisualizadora = listaUnidadeOrganizacionalVisualizadora;
	}

	/**
	 * Adiciona um Chamado Unidade Organizacional a uma Lista Unidade Organizacional Visualizadora
	 * 
 	 * @param chamadoUnidadeOrganizacional {@link ChamadoAssuntoUnidadeOrganizacional}
	 */
	public void adicionarChamadoUnidadeOrganizacional(ChamadoAssuntoUnidadeOrganizacional chamadoUnidadeOrganizacional) {
		this.listaUnidadeOrganizacionalVisualizadora.add(chamadoUnidadeOrganizacional);
	}

	/**
	 * Remove chamado unidade organizacional da lista
	 * 
	 * @param chamadoUnidadeOrganizacional the chamadoUnidadeOrganizacional
	 */
	public void removerChamadoUnidadeOrganizacional(ChamadoAssuntoUnidadeOrganizacional chamadoUnidadeOrganizacional) {
		this.listaUnidadeOrganizacionalVisualizadora.remove(chamadoUnidadeOrganizacional);
	}

	/**
	 * @return indicadorUnidadesOrganizacionalVisualizadora {@link Boolean}
	 */
	public Boolean getIndicadorUnidadesOrganizacionalVisualizadora() {
		return indicadorUnidadesOrganizacionalVisualizadora;
	}

	/**
	 * @param indicadorUnidadesOrganizacionalVisualizadora {@link Boolean}
	 */
	public void setIndicadorUnidadesOrganizacionalVisualizadora(Boolean indicadorUnidadesOrganizacionalVisualizadora) {
		this.indicadorUnidadesOrganizacionalVisualizadora = indicadorUnidadesOrganizacionalVisualizadora;
	}

	/**
	 * @return the indicadorPrazoDiferenciado {@link Boolean}
	 */
	public Boolean getIndicadorPrazoDiferenciado() {
		return indicadorPrazoDiferenciado;
	}

	/**
	 * @param indicadorPrazoDiferenciado the indicadorPrazoDiferenciado to set {@link Boolean}
	 */
	public void setIndicadorPrazoDiferenciado(Boolean indicadorPrazoDiferenciado) {
		this.indicadorPrazoDiferenciado = indicadorPrazoDiferenciado;
	}

	/**
	 * @return ids unidades visualizadoras
	 */
	private JSONArray getIdsUnidadesVisualizadoras() {
		JSONArray ids = new JSONArray();
		if (!Util.isNullOrEmpty(listaUnidadeOrganizacionalVisualizadora)) {
			for (ChamadoAssuntoUnidadeOrganizacional chamadoAssuntoUO : listaUnidadeOrganizacionalVisualizadora) {
				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("id", chamadoAssuntoUO.getUnidadeOrganizacional().getChavePrimaria());
					ids.put(jsonObject);
				} catch (JSONException e) {
					LOG.error(e);
				}
			}
		}

		return ids;
	}

	/**
	 * é chamado pela jsp: gridComponentesAssunto no click para editar
	 * @return objeto json
	 */
	public String getChamadoAssuntoJSON() {
		JSONObject json = new JSONObject();

		try {

			json.put("descricao", this.getDescricao());
			json.put("orientacao", this.getOrientacao());
			json.put("quantidadeHorasPrevistaAtendimento", this.getQuantidadeHorasPrevistaAtendimento());
			
			if(this.getNomeRelatorioRegulatorio() != null) {
				json.put("nomeRelatorioRegulatorio", this.getNomeRelatorioRegulatorio());
			}else {
				json.put("nomeRelatorioRegulatorio", "");
			}
			
			if (this.getIndicadorDiaCorridoUtil() != null) {
				json.put("indicadorDiaCorridoUtil", this.getIndicadorDiaCorridoUtil().getChavePrimaria());
			} else {
				json.put("indicadorDiaCorridoUtil", "");
			}

			json.put("indicadorCredito", this.getIndicadorCredito());

			if (this.getRubricaCredito() != null) {
				json.put("rubricaCredito", this.getRubricaCredito().getChavePrimaria());
			} else {
				json.put("rubricaCredito", "");
			}

			json.put("indicadorDebito", this.getIndicadorDebito());

			if (this.getRubricaDebito() != null) {
				json.put("rubricaDebito", this.getRubricaDebito().getChavePrimaria());
			} else {
				json.put("rubricaDebito", "");
			}

			if (this.getQuestionario() != null) {
				json.put("questionario", this.getQuestionario().getChavePrimaria());
			} else {
				json.put("questionario", "");
			}

			if (this.getUnidadeOrganizacional() != null) {
				json.put("unidadeOrganizacional", this.getUnidadeOrganizacional().getChavePrimaria());
			} else {
				json.put("unidadeOrganizacional", "");
			}

			if (this.getCanalAtendimento() != null) {
				json.put("canalAtendimento", this.getCanalAtendimento().getChavePrimaria());
			} else {
				json.put("canalAtendimento", "-1");
			}

			if (this.getUsuarioResponsavel() != null) {
				json.put("usuarioResponsavel", this.getUsuarioResponsavel().getChavePrimaria());
			} else {
				json.put("usuarioResponsavel", "");
			}
			
			if(this.getIndicadorRegulatorio() != null) {
				json.put("indicadorRegulatorio", this.getIndicadorRegulatorio().getEntidadeClasse().getDescricao());
			}else {
				json.put("indicadorRegulatorio", "");
			}

			json.put("indicadorClienteObrigatorio", this.getIndicadorClienteObrigatorio());
			json.put("indicadorImovelObrigatorio", this.getIndicadorImovelObrigatorio());
			json.put("indicadorRgSolicitanteObrigatorio", this.getIndicadorRgSolicitanteObrigatorio());
			json.put("indicadorCpfCnpjSolicitanteObrigatorio", this.getIndicadorCpfCnpjSolicitanteObrigatorio());
			json.put("indicadorTramitacao", this.getIndicadorTramitacao());
			json.put("indicadorNomeSolicitanteObrigatorio", this.getIndicadorNomeSolicitanteObrigatorio());
			json.put("indicadorTelefoneSolicitanteObrigatorio", this.getIndicadorTelefoneSolicitanteObrigatorio());
			json.put("indicadorEmailSolicitanteObrigatorio", this.getIndicadorEmailSolicitanteObrigatorio());
			json.put("indicadorGeraServicoAutorizacao", this.getIndicadorGeraServicoAutorizacao());
			json.put("indicadorFechamentoAutomatico", this.getIndicadorFechamentoAutomatico());
			json.put("indicadorPrazoDiferenciado", this.getIndicadorPrazoDiferenciado());
			json.put("indicadorUOVisualizadora", this.getIndicadorUnidadesOrganizacionalVisualizadora());
			json.put("unidadesVisualizadoras", this.getIdsUnidadesVisualizadoras());
			json.put("indicadorVerificaRestServ", this.getIndicadorVerificaRestServ());
			json.put("indicadorEncerraAutoASRel", this.getIndicadorEncerraAutoASRel());
			json.put("indicadorVerificarVazamento", this.getIndicadorVerificarVazamento());
			json.put("indicadorAcionamentoGasista", this.getIndicadorAcionamentoGasista());
			json.put("permiteAbrirEmLote", this.getPermiteAbrirEmLote());
			json.put("indicadorComGarantia", this.getIndicadorComGarantia());
			json.put("enviarEmailCadastro", this.getEnviarEmailCadastro());
			json.put("enviarEmailEncerrar", this.getEnviarEmailEncerrar());
			json.put("enviarEmailReiterar", this.getEnviarEmailReiterar());
			json.put("enviarEmailTramitar", this.getEnviarEmailTramitar());
			json.put("enviarEmailPrioridadeProtocolo", this.getEnviarEmailPrioridadeProtocolo());
			json.put("habilitado", this.isHabilitado());

		} catch (JSONException e) {
			LOG.error(e);
		}

		return json.toString();

	}

	/**
	 * @return the indicadorVerificaRestServ
	 */
	public Boolean getIndicadorVerificaRestServ() {
		return indicadorVerificaRestServ;
	}

	/**
	 * @param indicadorVerificaRestServ the indicadorVerificaRestServ to set
	 */
	public void setIndicadorVerificaRestServ(Boolean indicadorVerificaRestServ) {
		this.indicadorVerificaRestServ = indicadorVerificaRestServ;
	}

	/**
	 * @return orientacao
	 */
	public String getOrientacao() {
		return orientacao;
	}

	/**
	 * @param orientacao
	 */
	public void setOrientacao(String orientacao) {
		this.orientacao = orientacao;
	}

	/**
	 * @return indicadorAcionamentoGasista
	 */
	public Boolean getIndicadorAcionamentoGasista() {
		return indicadorAcionamentoGasista;
	}

	/**
	 * @param indicadorAcionamentoGasista
	 */
	public void setIndicadorAcionamentoGasista(Boolean indicadorAcionamentoGasista) {
		this.indicadorAcionamentoGasista = indicadorAcionamentoGasista;
	}

	/**
	 * @return questionario
	 */
	public Questionario getQuestionario() {
		return questionario;
	}

	/**
	 * @param questionario
	 */
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	/**
	 * @return the indicadorVerificarVazamento
	 */
	public Boolean getIndicadorVerificarVazamento() {
		return indicadorVerificarVazamento;
	}

	/**
	 * @param indicadorVerificarVazamento the indicadorVerificarVazamento to set
	 */
	public void setIndicadorVerificarVazamento(Boolean indicadorVerificarVazamento) {
		this.indicadorVerificarVazamento = indicadorVerificarVazamento;
	}

	/**
	 * @return the indicadorEncerraAutoASRel
	 */
	public Boolean getIndicadorEncerraAutoASRel() {
		return indicadorEncerraAutoASRel;
	}

	/**
	 * @param indicadorEncerraAutoASRel the indicadorEncerraAutoASRel to set
	 */
	public void setIndicadorEncerraAutoASRel(Boolean indicadorEncerraAutoASRel) {
		this.indicadorEncerraAutoASRel = indicadorEncerraAutoASRel;
	}

	/**
	 * @return canalAtendimento
	 */
	public CanalAtendimento getCanalAtendimento() {
		return canalAtendimento;
	}

	/**
	 * @param canalAtendimento
	 */
	public void setCanalAtendimento(CanalAtendimento canalAtendimento) {
		this.canalAtendimento = canalAtendimento;
	}

	/**
	 * Obtém as configurações de enviar email cadastro
	 * @return enviarEmailCadastro
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailCadastro() {
		return enviarEmailCadastro;
	}

	/**
	 * Configura o email cadastro
	 * @param enviarEmailCadastro
	 */
	public void setEnviarEmailCadastro(Set<OperacaoEnvioEmail> enviarEmailCadastro) {
		this.enviarEmailCadastro = enviarEmailCadastro;
	}

	/**
	 * Obtém as configurações de enviar email tramitar
	 * @return enviarEmailTramitar
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailTramitar() {
		return enviarEmailTramitar;
	}

	/**
	 * Configura o email tramitar
	 * @param enviarEmailTramitar
	 */
	public void setEnviarEmailTramitar(Set<OperacaoEnvioEmail> enviarEmailTramitar) {
		this.enviarEmailTramitar = enviarEmailTramitar;
	}

	/**
	 * Obtém as configurações de enviar email reiterar
	 * @return enviarEmailReiterar
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailReiterar() {
		return enviarEmailReiterar;
	}

	/**
	 * Configura o email reiterar
	 * @param enviarEmailReiterar
	 */
	public void setEnviarEmailReiterar(Set<OperacaoEnvioEmail> enviarEmailReiterar) {
		this.enviarEmailReiterar = enviarEmailReiterar;
	}

	/**
	 * Obtém o email encerrar
	 * @return
	 */
	public Set<OperacaoEnvioEmail> getEnviarEmailEncerrar() {
		return enviarEmailEncerrar;
	}

	/**
	 * Configura o email encerrar
	 * @param enviarEmailEncerrar
	 */
	public void setEnviarEmailEncerrar(Set<OperacaoEnvioEmail> enviarEmailEncerrar) {
		this.enviarEmailEncerrar = enviarEmailEncerrar;
	}

	/**
	 * Obtém as opções de prioridade para envio de email
	 * @return
	 */
	public Set<OperacaoEnvioEmailPrioridade> getEnviarEmailPrioridadeProtocolo() {
		return enviarEmailPrioridadeProtocolo;
	}

	/**
	 * Configura a priodade do envio de email do protocolo
	 * @param enviarEmailPrioridadeProtocolo
	 */
	public void setEnviarEmailPrioridadeProtocolo(Set<OperacaoEnvioEmailPrioridade> enviarEmailPrioridadeProtocolo) {
		this.enviarEmailPrioridadeProtocolo = enviarEmailPrioridadeProtocolo;
	}

	/**
	 * Retorna o atributo se permite abrir o chamado em lote
	 * @return
	 */
	public Boolean getPermiteAbrirEmLote() {
		return permiteAbrirEmLote;
	}

	/**
	 * Seta o atributo se permite abrir o chamado em lote
	 * @param permiteAbrirEmLote
	 */
	public void setPermiteAbrirEmLote(Boolean permiteAbrirEmLote) {
		this.permiteAbrirEmLote = permiteAbrirEmLote;
	}

	/**
	 * Retorna o atributo se o chamado assunto tem garantia
	 * @return
	 */
	public Boolean getIndicadorComGarantia() {
		return indicadorComGarantia;
	}

	/**
	 * Seta o atributo se o chamado assunto tem garantia ou não
	 * @param indicadorComGarantia
	 */
	public void setIndicadorComGarantia(Boolean indicadorComGarantia) {
		this.indicadorComGarantia = indicadorComGarantia;
	}
	
	/**
	 * Retorna o indicador regulatório ARSAL
	 * @return indicadorRegulatorio
	 */
	public EntidadeConteudo getIndicadorRegulatorio() {
		return indicadorRegulatorio;
	}

	/**
	 * Seta o indicador regulatório com o valor pre-definido
	 * @param indicadorRegulatorio
	 */
	public void setIndicadorRegulatorio(EntidadeConteudo indicadorRegulatorio) {
		this.indicadorRegulatorio = indicadorRegulatorio;
	}

	public String getNomeRelatorioRegulatorio() {
		return nomeRelatorioRegulatorio;
	}

	public void setNomeRelatorioRegulatorio(String nomeRelatorioRegulatorio) {
		this.nomeRelatorioRegulatorio = nomeRelatorioRegulatorio;
	}
	
}
