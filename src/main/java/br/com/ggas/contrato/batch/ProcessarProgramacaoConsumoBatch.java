/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/04/2015 11:17:56
 @author aantonio
 */

package br.com.ggas.contrato.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável por implementar o processo de Programação de Consumo. 
 *
 */
@Component
public class ProcessarProgramacaoConsumoBatch implements Batch {

	private static final int DIAS_ACEITE_AUTOMATICO = 1;
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		logProcessamento.append(Constantes.PULA_LINHA);
		logProcessamento.append("[");
		logProcessamento.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(),  true));
		logProcessamento.append("] ");
		logProcessamento.append("Iniciando o processo batch de Programação de Consumo.");

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorProgramacao controladorProgramacao = ServiceLocator.getInstancia().getControladorProgramacao();

		Integer diaLimiteAprovacao = Integer.parseInt((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.LIMITE_APROVACAO_PROG_CONSUMO_MENSAL));

		GregorianCalendar calendar = new GregorianCalendar();
		int dia = calendar.get(GregorianCalendar.DAY_OF_MONTH);
		int mesAtualMesal = calendar.get(GregorianCalendar.MONTH) + 2;
		int mesAtual = calendar.get(GregorianCalendar.MONTH) + 1;
		int ano = calendar.get(GregorianCalendar.YEAR);
		int hora = calendar.get(GregorianCalendar.HOUR_OF_DAY);
		int ultimoDiaMes = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH) - diaLimiteAprovacao;

		if(dia >= ultimoDiaMes) {
			if(mesAtualMesal > Constantes.QUANTIDADE_MESES_NO_ANO) {
				ano = calendar.get(GregorianCalendar.YEAR) + 1;
				mesAtualMesal = 1;

			}

			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("[");
			logProcessamento.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(),  true));
			logProcessamento.append("] ");
			logProcessamento.append(IntervaloProgramacao.MENSAL.mensagemLog());

			Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = controladorContrato
							.listarContratoPontoConsumoModalidadePorHora(null);

			Collection<String> descricoesPontoConsumo = new HashSet<>();
			for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {
				List<ContratoPontoConsumoModalidade> list = new ArrayList<>();
				list.add(contratoPontoConsumoModalidade);

				Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorProgramacao
								.listarSolicitacaoConsumoPontoConsumo(contratoPontoConsumoModalidade.getContratoPontoConsumo(), list, ano,
												mesAtualMesal);
				Long idPontoConsumo = contratoPontoConsumoModalidade.getContratoPontoConsumo().getPontoConsumo().getChavePrimaria();
				Long chaveContratoPontoConsumoModalidade = contratoPontoConsumoModalidade.getChavePrimaria();
				boolean programou = controladorProgramacao.realizaProgramacaoConsumoAutomaticaMensal(
						listaSolicitacaoConsumo, ano, mesAtualMesal, idPontoConsumo, chaveContratoPontoConsumoModalidade);

				if (programou) {
					String descricao = contratoPontoConsumoModalidade
							.getContratoPontoConsumo()
							.getPontoConsumo()
							.getDescricao();
					registrarMensagemLog(logProcessamento, descricoesPontoConsumo, descricao);
				}

			}
		}

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = controladorContrato
						.listarContratoPontoConsumoModalidadePorHora(hora);
		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {
			Long idPontoConsumo = contratoPontoConsumoModalidade.getContratoPontoConsumo().getPontoConsumo().getChavePrimaria();
			Long chaveContratoPontoConsumoModalidade = contratoPontoConsumoModalidade.getChavePrimaria();
			IntervaloProgramacao[] intervalos = new IntervaloProgramacao[]{IntervaloProgramacao.DIARIO, IntervaloProgramacao.INTRADIARIO};
			for (IntervaloProgramacao intervalo : intervalos) {
				Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorProgramacao
						.pesquisarSolicitacaoConsumoPontoConsumoModalidadeDiariaIntradiaria(idPontoConsumo, ano, mesAtual,
								chaveContratoPontoConsumoModalidade, intervalo.indicadorTipo());
				
				filtrarSolicitacoesUrgentes(listaSolicitacaoConsumo, calendar);				
				if(listaSolicitacaoConsumo != null && !listaSolicitacaoConsumo.isEmpty()) {
					controladorProgramacao.realizaProgramacaoConsumoAutomaticaDiariaIntraDiaria(listaSolicitacaoConsumo, ano, mesAtual,
							idPontoConsumo, contratoPontoConsumoModalidade);
					
					logProcessamento.append(Constantes.PULA_LINHA);
					logProcessamento.append("[");
					logProcessamento
							.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true));
					logProcessamento.append("] ");
					logProcessamento.append(intervalo.mensagemLog());
					
					
					logProcessamento.append(Constantes.PULA_LINHA);
					logProcessamento.append("[");
					logProcessamento
							.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true));
					logProcessamento.append("] ");
					logProcessamento.append(contratoPontoConsumoModalidade.getContratoPontoConsumo().getPontoConsumo().getDescricao());
				}
			}
		}

		return logProcessamento.toString();
	}

	private void registrarMensagemLog(StringBuilder logProcessamento, Collection<String> descricoesPontoConsumo, String descricao) {
		
		if (!descricoesPontoConsumo.contains(descricao)) {
		
			logProcessamento.append(Constantes.PULA_LINHA);
			logProcessamento.append("[");
			logProcessamento.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true));
			logProcessamento.append("] ");
			logProcessamento.append(descricao);
			
			descricoesPontoConsumo.add(descricao);
		}
	}
	
	private void filtrarSolicitacoesUrgentes(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo, Calendar agora) {
		if(listaSolicitacaoConsumo != null){
			for (Iterator<SolicitacaoConsumoPontoConsumo> iterator = listaSolicitacaoConsumo.iterator(); iterator.hasNext();) {
				Calendar solicitacao = Calendar.getInstance();
				solicitacao.setTime(iterator.next().getDataSolicitacao());
				if(solicitacao.get(Calendar.DAY_OF_MONTH) > agora.get(Calendar.DAY_OF_MONTH) + DIAS_ACEITE_AUTOMATICO) {
					iterator.remove();
				}
			}
		}
	}

	private static enum IntervaloProgramacao {
		
		MENSAL {
			@Override
			String mensagemLog() {
				return "Mensal ";
			}

			@Override
			String indicadorTipo() {
				return "1";
			}
		}, DIARIO {
			@Override
			String mensagemLog() {
				return "Diário ";
			}

			@Override
			String indicadorTipo() {
				return "2";
			}
		}, INTRADIARIO {
			@Override
			String mensagemLog() {
				return "IntraDiário ";
			}

			@Override
			String indicadorTipo() {
				return "3";
			}
		};
		
		abstract String mensagemLog();
		abstract String indicadorTipo();
	}
}
