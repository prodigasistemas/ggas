/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.programacao.impl;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 */
class ParadaProgramadaImpl extends EntidadeNegocioImpl implements ParadaProgramada {

	private static final int LIMITE_DADOS_INVALIDOS = 2;

	private static final int LIMITE_CAMPOS_ERRO_TAMANHO = 2;

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final int LIMITE_COMENTARIO = 70;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8290113680466362907L;

	private PontoConsumo pontoConsumo;

	private Date dataParada;

	private Date dataOperacao;

	private Usuario usuario;

	private String nomeSolicitante;

	private String comentario;

	private EntidadeConteudo tipoParada;

	private BigDecimal menorPressao;

	private Boolean indicadorParadaApurada;

	private Unidade unidadeMenorPressao;

	private Date dataAviso;

	private BigDecimal volumeFornecimento;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #setPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #getDataParada()
	 */
	@Override
	public Date getDataParada() {

		return dataParada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #setDataParada(java.util.Date)
	 */
	@Override
	public void setDataParada(Date dataParada) {

		this.dataParada = dataParada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #getDataOperacao()
	 */
	@Override
	public Date getDataOperacao() {

		return dataOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #setDataOperacao(java.util.Date)
	 */
	@Override
	public void setDataOperacao(Date dataOperacao) {

		this.dataOperacao = dataOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo#getUsuario()
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ParadaProgramadaPontoConsumo
	 * #setUsuario(br.com
	 * .ggas.controleacesso.Usuario)
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	@Override
	public String getNomeSolicitante() {

		return nomeSolicitante;
	}

	@Override
	public void setNomeSolicitante(String nomeSolicitante) {

		this.nomeSolicitante = nomeSolicitante;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada#getComentario()
	 */
	@Override
	public String getComentario() {

		return comentario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada
	 * #setComentario(java.lang.String)
	 */
	@Override
	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	/**
	 * @return the tipoParada
	 */
	@Override
	public EntidadeConteudo getTipoParada() {

		return tipoParada;
	}

	/**
	 * @param tipoParada
	 *            the tipoParada to set
	 */
	@Override
	public void setTipoParada(EntidadeConteudo tipoParada) {

		this.tipoParada = tipoParada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada#getMenorPressao()
	 */
	@Override
	public BigDecimal getMenorPressao() {

		return menorPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada
	 * #setMenorPressao(java.lang.Integer)
	 */
	@Override
	public void setMenorPressao(BigDecimal menorPressao) {

		this.menorPressao = menorPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada#isIndicadorParadaApurada()
	 */
	@Override
	public Boolean isIndicadorParadaApurada() {

		return indicadorParadaApurada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada
	 * #setIndicadorParadaApurada(java
	 * .lang.Boolean)
	 */
	@Override
	public void setIndicadorParadaApurada(Boolean indicadorParadaApurada) {

		this.indicadorParadaApurada = indicadorParadaApurada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada#getUnidadeMenorPressao()
	 */
	@Override
	public Unidade getUnidadeMenorPressao() {

		return unidadeMenorPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * ParadaProgramada
	 * #setUnidadeMenorPressao(br.com
	 * .ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeMenorPressao(Unidade unidadeMenorPressao) {

		this.unidadeMenorPressao = unidadeMenorPressao;
	}

	@Override
	public Date getDataAviso() {

		return dataAviso;
	}

	@Override
	public void setDataAviso(Date dataAviso) {

		this.dataAviso = dataAviso;
	}

	@Override
	public BigDecimal getVolumeFornecimento() {

		return volumeFornecimento;
	}

	@Override
	public void setVolumeFornecimento(BigDecimal volumeFornecimento) {

		this.volumeFornecimento = volumeFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		StringBuilder caracteresInvalidos = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;
		String camposCaracteresInvalidos = null;

		if(dataAviso == null) {
			stringBuilder.append(ParadaProgramada.PARADA_PROGRAMADA_DATA_AVISO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataParada == null) {
			stringBuilder.append(ParadaProgramada.PARADA_PROGRAMADA_DATA_PARADA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(pontoConsumo == null) {
			stringBuilder.append(ParadaProgramada.PARADA_PROGRAMADA_PONTO_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(nomeSolicitante == null) {
			stringBuilder.append(ParadaProgramada.PARADA_PROGRAMADA_INDICADOR_SOLICITANTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if((!StringUtils.isEmpty(comentario)) && (comentario.length() > LIMITE_COMENTARIO)) {
			tamanhoCamposAcumulados.append(ParadaProgramada.PARADA_PROGRAMADA_COMENTARIO);
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if((StringUtils.contains(comentario, "'")) || (StringUtils.contains(comentario, "\""))) {
			caracteresInvalidos.append(ParadaProgramada.PARADA_PROGRAMADA_COMENTARIO);
			caracteresInvalidos.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();
		camposCaracteresInvalidos = caracteresInvalidos.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE,
					camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPOS_ERRO_TAMANHO));
		}

		if(camposCaracteresInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_DADOS_INVALIDOS,
					camposCaracteresInvalidos.substring(0,
							caracteresInvalidos.toString().length() - LIMITE_DADOS_INVALIDOS));
		}

		return erros;
	}
}
