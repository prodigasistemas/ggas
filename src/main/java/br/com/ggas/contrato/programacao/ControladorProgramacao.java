/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.programacao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.MultiKeyMap;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.programacao.impl.TempSolicitacaoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.web.contrato.programacao.ParadaProgramadaVO;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Programação. 
 *
 */
public interface ControladorProgramacao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PROGRAMACAO = "controladorProgramacao";

	String ERRO_NEGOCIO_PARADA_PROGRAMADA_EXISTENTE = "ERRO_PARADA_PROGRAMADA_EXISTENTE";

	String ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS = "ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS";

	String ERRO_NEGOCIO_QDS_MAIOR_QDC = "ERRO_NEGOCIO_QDS_MAIOR_QDC";

	String ERRO_NEGOCIO_QDP_MAIOR_QDC = "ERRO_NEGOCIO_QDP_MAIOR_QDC";

	String ERRO_NEGOCIO_SOMA_QPNR_QDS_IGUAL_QDP = "ERRO_NEGOCIO_SOMA_QPNR_QDS_IGUAL_QDP";

	String ERRO_NEGOCIO_QDS_MAIOR_QDC_VARIACAO = "ERRO_NEGOCIO_QDS_MAIOR_QDC_VARIACAO";

	String SUCESSO_INCLUSAO_PARADA_PROGRAMADA = "SUCESSO_INCLUSAO_PARADA_PROGRAMADA";

	String SUCESSO_INCLUSAO_PARADA_PROGRAMADA_EXISTENTE = "SUCESSO_INCLUSAO_PARADA_PROGRAMADA_EXISTENTE";

	String ERRO_NEGOCIO_DATA_PARADA_INVALIDA = "ERRO_NEGOCIO_DATA_PARADA_INVALIDA";

	String ERRO_NEGOCIO_DATA_INVALIDA_ANO_MAIUSCULO = "ERRO_NEGOCIO_DATA_INVALIDA_ANO_MAIUSCULO";

	String ERRO_NEGOCIO_DATA_INVALIDA_ANO_MINUSCULO = "ERRO_NEGOCIO_DATA_INVALIDA_ANO_MINUSCULO";

	String ERRO_NEGOCIO_PARAMETRO_INVALIDO = "ERRO_NEGOCIO_PARAMETRO_INVALIDO";

	String SUCESSO_ALTERACAO_PARADA_PROGRAMADA = "SUCESSO_ALTERACAO_PARADA_PROGRAMADA";

	String ERRO_NEGOCIO_PARADA_DADOS_PRESSAO = "ERRO_NEGOCIO_PARADA_DADOS_PRESSAO";

	String ERRO_NEGOCIO_VALOR_QPNR = "ERRO_NEGOCIO_VALOR_QPNR";

	String ERRO_NEGOCIO_SOLICITACAO_CONSUMO_SEM_VARIACAO = "ERRO_NEGOCIO_SOLICITACAO_CONSUMO_SEM_VARIACAO";

	String ERRO_NEGOCIO_VALOR_QDC_NULO = "ERRO_NEGOCIO_VALOR_QDC_NULO";

	String ERRO_NEGOCIO_VALOR_QDS_NULO = "ERRO_NEGOCIO_VALOR_QDS_NULO";

	String ERRO_NEGOCIO_VALOR_QDP_NULO = "ERRO_NEGOCIO_VALOR_QDP_NULO";

	String ERRO_NEGOCIO_SOLICITACAO_CONSUMO_NAO_ATENDE_NENHUMA_REGRA = 
					"ERRO_NEGOCIO_SOLICITACAO_CONSUMO_NAO_ATENDE_NENHUMA_REGRA";

	String ERRO_NEGOCIO_SOLICITACAO_CONSUMO_SO_ALTERADA_MEDIANTE_LOGIN_SENHA = 
					"ERRO_NEGOCIO_SOLICITACAO_CONSUMO_SO_ALTERADA_MEDIANTE_LOGIN_SENHA";

	String ERRO_NEGOCIO_QDP_MAIOR_QDC_MAIS_VARIACAO = "ERRO_NEGOCIO_QDP_MAIOR_QDC_MAIS_VARIACAO";

	/**
	 * Criar solicitacao consumo ponto consumo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSolicitacaoConsumoPontoConsumo();

	/**
	 * Método responsável por consultar as paradas
	 * programadas em comum para todos os pontos
	 * de consumo desejados.
	 * 
	 * @param idPontosConsumo
	 *            Chaves primárias dos pontos de
	 *            consumo.
	 * @param idTipoParada
	 *            the id tipo parada
	 * @return Uma coleção de paradas programadas
	 *         de ponto de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ParadaProgramada> consultarParadasProgramadasEmComum(Long[] idPontosConsumo, Long idTipoParada) throws NegocioException;

	/**
	 * Método responsável por controlar a
	 * inclusão, alteração e exclusão das paradas
	 * programadas.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos pontos de
	 *            consumo.
	 * @param listaDatasParadas
	 *            Datas das paradas a serem
	 *            controladas.
	 * @param usuario
	 *            Usuario que efetuou o controle
	 *            das paradas.
	 * @param dadosAuditoria
	 *            Dados de auditoria das paradas.
	 * @param indicadorSolicitante
	 *            Solicitante da parada
	 *            programada.
	 * @param comentario
	 *            Comentários sobre a parada
	 *            programada.
	 * @param idTipoParada
	 *            the id tipo parada
	 * @param pressoes
	 *            the pressoes
	 * @param datasPressao
	 *            the datas pressao
	 * @param unidadesPressao
	 *            the unidades pressao
	 * @param chavesPontoConsumoParada
	 *            the chaves ponto consumo parada
	 * @param dataAviso
	 *            the data aviso
	 * @param volumeFornecimento
	 *            the volume fornecimento
	 * @return O status da incluão das paradas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	int[] inserirParadasProgramadas(Long[] chavesPrimarias, String[] listaDatasParadas, 
					Usuario usuario, DadosAuditoria dadosAuditoria, String indicadorSolicitante, 
					String comentario, Long idTipoParada, String[] pressoes, String[] datasPressao,
					Long[] unidadesPressao, Long[] chavesPontoConsumoParada, String dataAviso, 
					String volumeFornecimento) throws ParseException, GGASException;

	/**
	 * Método responsável por atualizar a lista de
	 * paradas programadas com as datas
	 * selecionadas.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param listaDatasParadas
	 *            the lista datas paradas
	 * @param usuario
	 *            the usuario
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param indicadorSolicitante
	 *            the indicador solicitante
	 * @param comentario
	 *            the comentario
	 * @param idTipoParada
	 *            the id tipo parada
	 * @param dataAviso
	 *            the data aviso
	 * @param volumeFornecimento
	 *            the volume fornecimento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ParadaProgramadaVO> manterParadasProgramadas(Long[] chavesPrimarias, String[] listaDatasParadas, Usuario usuario,
					DadosAuditoria dadosAuditoria, String indicadorSolicitante, String comentario, Long idTipoParada, String dataAviso,
					String volumeFornecimento) throws NegocioException;

	/**
	 * Método responsável por listar os dados de
	 * solicitação de consumo do ponto de consumo.
	 * 
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param listaContratoPontoConsumoModalidade
	 *            the lista contrato ponto consumo modalidade
	 * @param ano
	 *            Ano
	 * @param mes
	 *            Mes
	 * @return Coleção contendo informações das
	 *         solicitações de consumo.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo,
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Integer ano, Integer mes)
					throws NegocioException;

	/**
	 * Método responsável por atualizar a
	 * solicitação de consumo do ponto de consumo.
	 * 
	 * @param listaSolicitacao
	 *            the lista solicitacao
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @param chaveContratoPontoConsumoModalidade
	 *            the chave contrato ponto consumo modalidade
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarListaSolicitacaoConsumoPontoConsumo(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao, Long idPontoConsumo,
					Integer ano, Integer mes, Long chaveContratoPontoConsumoModalidade) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por listar somente uma
	 * parada para cada data de parada existente
	 * em uma lista de paradas com
	 * datas repetidas.
	 * 
	 * @param listaParadasRepetidas
	 *            Lista de paradas programadas com
	 *            datas de paradas repetidas.
	 * @return Uma coleção de paradas programadas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ParadaProgramada> listarParadasNaoRepetidas(Collection<ParadaProgramada> listaParadasRepetidas) throws NegocioException;

	/**
	 * Método responsável por validar o tipo da
	 * parada que será inserida.
	 * 
	 * @param idTipoParada
	 *            Chave primária do tipo da
	 *            parada.
	 * @return the entidade conteudo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo validarTipoParada(Long idTipoParada) throws NegocioException;

	/**
	 * Método responsável por calcular o valor do
	 * QDC da solicitação de consumo para a data
	 * informada.
	 * 
	 * @param listaContratoPontoConsumoModalidade
	 *            Lista de
	 *            ContratoPontoConsumoModalidade
	 * @param dataSolicitacao
	 *            Data da solicitação.
	 * @return O valor do QDC.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal calcularQDCSolicitacaoConsumo(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Date dataSolicitacao) throws NegocioException;

	/**
	 * Método responsável por ordenar uma lista de
	 * ContratoPontoConsumoModalidadeQDC.
	 * 
	 * @param listaContratoPontoConsumoModalidadeQDC
	 *            Lista de
	 *            ContratoPontoConsumoModalidadeQDC
	 * @return Uma lista de
	 *         ContratoPontoConsumoModalidadeQDC
	 *         ordenada.
	 */
	List<ContratoPontoConsumoModalidadeQDC> ordenarListaModalidadeQDC(
					Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC);

	/**
	 * Método responsável por consultar as paradas
	 * programadas para um período determinado.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos pontos de
	 *            consumo
	 * @param chavesTipoParada
	 *            Tipos de parada
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @param supridora
	 *            the supridora
	 * @return Retorna as paradas programadas
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<ParadaProgramada> consultarParadasProgramadasPeriodo(Long[] chavesPrimarias, Long[] chavesTipoParada, Date dataInicial,
					Date dataFinal, String supridora) throws NegocioException;

	/**
	 * Método responsável por listar volumes de
	 * execução de contrato.
	 * 
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @param chavesPontoConsumo
	 * 			the chaves ponto consumo
	 * @return Retorna colleção de
	 *         SolicitacaoConsumoPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<SolicitacaoConsumoPontoConsumo> consultarVolumeExecucaoContratoSolicitacao(Date dataInicial, Date dataFinal,
					Long[] chavesPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por listar volumes de
	 * execução de contrato.
	 * 
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @param chavesPontoConsumo
	 *            the chaves ponto consumo
	 * @return Retorna colleção de
	 *         ApuracaoQuantidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<ApuracaoQuantidade> consultarVolumeExecucaoContratoApuracao(Date dataInicial, Date dataFinal, Long[] chavesPontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por listar volumes de
	 * execução de contrato.
	 * 
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @param chavesPontoConsumo
	 *            the chaves ponto consumo
	 * @return Retorna colleção de
	 *         QuantidadeRecuperacao
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Recuperacao> consultarVolumeExecucaoContratoRecuperacao(Date dataInicial, Date dataFinal, Long[] chavesPontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por obter o saldo a
	 * recuperar de um ponto de consumo OU um
	 * conjunto de pontos de consumo agrupados por
	 * volume.
	 * 
	 * @param contrato
	 *            the contrato
	 * @param arrayIdPontoConsumo
	 *            lista contendo Id do ponto de
	 *            consumo ou dos pontos de consumo
	 *            agrupados por volume
	 * @param idApuracaoPenalidade
	 *            the id apuracao penalidade
	 * @param anoMesApuracao
	 *            ano /mês de apuração
	 * @param listaAQPP
	 *            lista de AQPP
	 * @return saldo a recuperar
	 * @throws GGASException
	 *             caso ocorra algum erro
	 */
	BigDecimal obterSaldoARecuperar(Contrato contrato, Long[] arrayIdPontoConsumo, Long idApuracaoPenalidade, Integer anoMesApuracao,
					Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP) throws GGASException;

	/**
	 * Método responsável por validar o total das
	 * estimativas com o saldo a recuperar.
	 * 
	 * @param listaValorQPNR
	 *            the lista valor qpnr
	 * @param saldoARecuperar
	 *            the saldo a recuperar
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarValorTotalQPNR(String[] listaValorQPNR, BigDecimal saldoARecuperar) throws GGASException;

	/**
	 * Método responsável por atualizar os dados
	 * das solicitacoes de consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param mapa
	 *            the mapa
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Map<String, String>> atualizarDadosSolicitacoesConsumo(Long idPontoConsumo, Map<String, Map<String, String>> mapa)
					throws NegocioException;

	/**
	 * Método responsável consultar paradas
	 * programadas em comum.
	 * 
	 * @param idPontosConsumo
	 *            the id pontos consumo
	 * @param idTipoParada
	 *            the id tipo parada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ParadaProgramada> consultarParadaProgramadasEmComum(Long idPontosConsumo, Long idTipoParada) throws NegocioException;

	/**
	 * Listar solicitacao consumo ponto consumo historico.
	 * 
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param listaContratoPontoConsumoModalidade
	 *            the lista contrato ponto consumo modalidade
	 * @param anoInformado
	 *            the ano informado
	 * @param mesInformado
	 *            the mes informado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPontoConsumoHistorico(ContratoPontoConsumo contratoPontoConsumo,
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Integer anoInformado,
					Integer mesInformado) throws NegocioException;

	/**
	 * Realiza programacao consumo automatica mensal.
	 * 
	 * @param listaSolicitacaoConsumo
	 *            the lista solicitacao consumo
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param chaveContratoPontoConsumoModalidade
	 *            the chave contrato ponto consumo modalidade
	 * @return true, se realizou alguma programacao automatica
	 * @throws GGASException
	 *             the GGAS exception
	 */
	boolean realizaProgramacaoConsumoAutomaticaMensal(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo, Integer ano,
					Integer mes, Long idPontoConsumo, Long chaveContratoPontoConsumoModalidade)
					throws GGASException;

	/**
	 * Realiza programacao consumo automatica diaria intra diaria.
	 * 
	 * @param listaSolicitacaoConsumo
	 *            the lista solicitacao consumo
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param contratoPontoConsumoModalidade
	 *            the contrato ponto consumo modalidade
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void realizaProgramacaoConsumoAutomaticaDiariaIntraDiaria(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo,
					Integer ano, Integer mes, Long idPontoConsumo, ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
					throws GGASException;

	/**
	 * Pesquisar solicitacao consumo ponto consumo modalidade diaria intradiaria.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @param chaveContratoPontoConsumoModalidade
	 *            the chave contrato ponto consumo modalidade
	 * @param indicadorTipo
	 *            the indicador tipo
	 * @return the collection
	 */
	Collection<SolicitacaoConsumoPontoConsumo> pesquisarSolicitacaoConsumoPontoConsumoModalidadeDiariaIntradiaria(Long chavePontoConsumo,
					Integer ano, Integer mes, Long chaveContratoPontoConsumoModalidade, String indicadorTipo);

	/**
	 * Listar comentario solicitacao consumo.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param dataSolicitacao
	 *            the data solicitacao
	 * @param chaveContratoPontoConsumoModalidade
	 *            the chave contrato ponto consumo modalidade
	 * @return the collection
	 */
	Collection<SolicitacaoConsumoPontoConsumo> listarComentarioSolicitacaoConsumo(Long chavePontoConsumo, Date dataSolicitacao,
					Long chaveContratoPontoConsumoModalidade);

	/**
	 * Listar solicitacao consumo diarias intradiarias.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoDiariasIntradiarias(Map<String, Object> filtro) throws GGASException;

	/**
	 * Listar solicitacao consumo pendentes.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPendentes() throws NegocioException;

	/**
	 * Obter temp solicitacao consmo.
	 * 
	 * @param enderecoRemoto
	 *            o endereco remoto
	 * @param dataSolicitacao
	 *            the data solicitacao
	 * @return the temp solicitacao consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	TempSolicitacaoConsumo obterTempSolicitacaoConsmo(String enderecoRemoto, Date dataSolicitacao) throws NegocioException;

	/**
	 * Remover solicitacao consumo alterar contrato.
	 *
	 * @param contrato the contrato
	 * @return the hash map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<Long, Map<Long, Collection<SolicitacaoConsumoPontoConsumo>>> removerSolicitacaoConsumoAlterarContrato(Contrato contrato)
					throws NegocioException;

	/**
	 * Migrar solicitacao consumo alterar contrato.
	 *
	 * @param chavePrimariaContratoAlterado the chave primaria contrato alterado
	 * @param chavePrimaria 
	 * 			the chave primaria
	 * @throws NegocioException
	 * 		the negocio exception 
	 * @throws ConcorrenciaException 
	 * 			the concorrencia exception 
	 */
	void migrarSolicitacaoConsumoAlterarContrato(
					long chavePrimariaContratoAlterado, long chavePrimaria) 
									throws NegocioException, ConcorrenciaException;

	/**
	 * Consulta a soma do valor QPD das solicitações de consumo, agrupadas
	 * por data de solicitação e chave primária da entidade {@link PontoConsumo}.
	 * 
	 * @param listaPontoConsumo - {@link List}
	 * @return mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 */
	MultiKeyMap consultarValorQDPDiarioPorPontoConsumo(List<PontoConsumo> listaPontoConsumo);

	/**
	 * Pesquisar solicitacao consumo ponto consumo modalidade.
	 *
	 * @param chavePontoConsumo a chave do ponto consumo
	 * @param chaveContratoPontoConsumoModalidade a chave do contrato ponto consumo modalidade
	 * @return a coleção
	 */
	Collection<SolicitacaoConsumoPontoConsumo> pesquisarSolicitacaoConsumoPontoConsumoModalidade(
			Long chavePontoConsumo, Long chaveContratoPontoConsumoModalidade);

}
