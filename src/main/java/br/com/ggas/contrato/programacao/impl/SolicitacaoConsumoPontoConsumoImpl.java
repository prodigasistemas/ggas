/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.programacao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelos atributos e implementação de métodos relacionados a Solicitação do Ponto de Consumo.
 */
public class SolicitacaoConsumoPontoConsumoImpl extends EntidadeNegocioImpl implements SolicitacaoConsumoPontoConsumo {

	private static final int LIMITE_TAMANHO_CAMPOS_INVALIDOS = 2;

	private static final int LIMITE_TAMANHO_DESCRICAO = 200;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3017101326693090845L;

	private PontoConsumo pontoConsumo;

	private Date dataSolicitacao;

	private BigDecimal valorQDC;

	private BigDecimal valorQDS;

	private BigDecimal valorQPNR;

	private BigDecimal valorQDP;

	private Boolean indicadorAceite;

	private Date dataOperacao;

	private Usuario usuario;

	private String descricaoMotivoNaoAceite;

	private ContratoPontoConsumoModalidade contratoPontoConsumoModalidade;

	private boolean indicadorSolicitante;

	private String comentario;

	private Chamado chamado;

	private String indicadorTipoProgramacaoConsumo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #getContratoPontoConsumoModalidade()
	 */
	@Override
	public ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade() {

		return contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #setContratoPontoConsumoModalidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade)
	 */
	@Override
	public void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {

		this.contratoPontoConsumoModalidade = contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #getDescricaoMotivoNaoAceite()
	 */
	@Override
	public String getDescricaoMotivoNaoAceite() {

		return descricaoMotivoNaoAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #setDescricaoMotivoNaoAceite
	 * (java.lang.String)
	 */
	@Override
	public void setDescricaoMotivoNaoAceite(String descricaoMotivoNaoAceite) {

		this.descricaoMotivoNaoAceite = descricaoMotivoNaoAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getDataSolicitacao()
	 */
	@Override
	public Date getDataSolicitacao() {
		Date data = null;
		if(this.dataSolicitacao != null) {
			data = (Date) dataSolicitacao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo #setDataSolicitacao(java.util.Date)
	 */
	@Override
	public void setDataSolicitacao(Date dataSolicitacao) {
		if (dataSolicitacao != null) {
			this.dataSolicitacao = (Date) dataSolicitacao.clone();
		} else {
			this.dataSolicitacao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQDC()
	 */
	@Override
	public BigDecimal getValorQDC() {

		return valorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQDC(java.math.BigDecimal)
	 */
	@Override
	public void setValorQDC(BigDecimal valorQDC) {

		this.valorQDC = valorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQDS()
	 */
	@Override
	public BigDecimal getValorQDS() {

		return valorQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQDS(java.math.BigDecimal)
	 */
	@Override
	public void setValorQDS(BigDecimal valorQDS) {

		this.valorQDS = valorQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQPNR()
	 */
	@Override
	public BigDecimal getValorQPNR() {

		return valorQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQPNR(java.math.BigDecimal)
	 */
	@Override
	public void setValorQPNR(BigDecimal valorQPNR) {

		this.valorQPNR = valorQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getValorQDP()
	 */
	@Override
	public BigDecimal getValorQDP() {

		return valorQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setValorQDP(java.math.BigDecimal)
	 */
	@Override
	public void setValorQDP(BigDecimal valorQDP) {

		this.valorQDP = valorQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getIndicadorAceite()
	 */
	@Override
	public Boolean getIndicadorAceite() {

		return indicadorAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setIndicadorAceite(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorAceite(Boolean indicadorAceite) {

		this.indicadorAceite = indicadorAceite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #getDataOperacao()
	 */
	@Override
	public Date getDataOperacao() {
		Date data = null;
		if(this.dataOperacao != null) {
			data = (Date) dataOperacao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setDataOperacao(java.util.Date)
	 */
	@Override
	public void setDataOperacao(Date dataOperacao) {
		if(dataOperacao != null) {
			this.dataOperacao = (Date) dataOperacao.clone();
		} else {
			this.dataOperacao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo#getUsuario()
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.impl.
	 * SolicitacaoConsumoPontoConsumo
	 * #setUsuario(br
	 * .com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #isIndicadorSolicitante()
	 */
	@Override
	public boolean isIndicadorSolicitante() {

		return indicadorSolicitante;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.
	 * SolicitacaoConsumoPontoConsumo
	 * #setIndicadorSolicitante(boolean)
	 */
	@Override
	public void setIndicadorSolicitante(boolean indicadorSolicitante) {

		this.indicadorSolicitante = indicadorSolicitante;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposInvalidos = null;

		if((descricaoMotivoNaoAceite != null) && (descricaoMotivoNaoAceite.length() > LIMITE_TAMANHO_DESCRICAO)) {
			tamanhoCamposAcumulados.append(Util.converterDataParaStringSemHora(dataSolicitacao, Constantes.FORMATO_DATA_BR));
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposInvalidos.length() > 0) {
			erros.put(ERRO_TAMANHO_LIMITE_MOTIVO, camposInvalidos.substring(0,
					tamanhoCamposAcumulados.toString().length() - LIMITE_TAMANHO_CAMPOS_INVALIDOS));
		}

		return erros;
	}

	@Override
	public String getComentario() {

		return comentario;
	}

	@Override
	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	@Override
	public Chamado getChamado() {

		return chamado;
	}

	@Override
	public void setChamado(Chamado chamado) {

		this.chamado = chamado;
	}

	@Override
	public String getIndicadorTipoProgramacaoConsumo() {

		return indicadorTipoProgramacaoConsumo;
	}

	@Override
	public void setIndicadorTipoProgramacaoConsumo(String indicadorTipoProgramacaoConsumo) {

		this.indicadorTipoProgramacaoConsumo = indicadorTipoProgramacaoConsumo;
	}

	@Override
	public Date getChaveMapeadora() {
		
		return this.getDataSolicitacao();
	}
}
