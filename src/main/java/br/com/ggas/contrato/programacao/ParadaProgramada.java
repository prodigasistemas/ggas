/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.programacao;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.Unidade;
/**
 * Interface responsável pela assinatura dos métodos relacionados a Programação de Parada (de fornecimento) 
 *
 */
public interface ParadaProgramada extends EntidadeNegocio {

	String BEAN_ID_PARADA_PROGRAMADA = "paradaProgramada";

	String PARADA_PROGRAMADA = "PARADA_PROGRAMADA";

	String PARADA_PROGRAMADA_DATA_PARADA = "PARADA_PROGRAMADA_DATA_PARADA";

	String PARADA_PROGRAMADA_PONTO_CONSUMO = "PARADA_PROGRAMADA_PONTO_CONSUMO";

	String PARADA_PROGRAMADA_INDICADOR_SOLICITANTE = "PARADA_PROGRAMADA_INDICADOR_SOLICITANTE";

	String PARADA_PROGRAMADA_COMENTARIO = "PARADA_PROGRAMADA_COMENTARIO";

	String PARADA_PROGRAMADA_DATA_AVISO = "PARADA_PROGRAMADA_DATA_AVISO";

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the dataParada
	 */
	Date getDataParada();

	/**
	 * @param dataParada
	 *            the dataParada to set
	 */
	void setDataParada(Date dataParada);

	/**
	 * @return the dataOperacao
	 */
	Date getDataOperacao();

	/**
	 * @param dataOperacao
	 *            the dataOperacao to set
	 */
	void setDataOperacao(Date dataOperacao);

	/**
	 * @return the usuario
	 */
	Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	void setUsuario(Usuario usuario);

	/**
	 * @return String - retorna nome socilitante. 
	 */
	String getNomeSolicitante();

	/**
	 * @param nomeSolicitante - Set nome socilitante.
	 */
	void setNomeSolicitante(String nomeSolicitante);

	/**
	 * @return the comentario
	 */
	String getComentario();

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	void setComentario(String comentario);

	/**
	 * @return the tipoParada
	 */
	EntidadeConteudo getTipoParada();

	/**
	 * @param tipoParada
	 *            the tipoParada to set
	 */
	void setTipoParada(EntidadeConteudo tipoParada);

	/**
	 * @return the menorPressao
	 */
	BigDecimal getMenorPressao();

	/**
	 * @param menorPressao
	 *            the menorPressao to set
	 */
	void setMenorPressao(BigDecimal menorPressao);

	/**
	 * Checks if is indicador parada apurada.
	 * 
	 * @return the indicadorParadaApurada
	 */
	Boolean isIndicadorParadaApurada();

	/**
	 * @param indicadorParadaApurada
	 *            the indicadorParadaApurada to
	 *            set
	 */
	void setIndicadorParadaApurada(Boolean indicadorParadaApurada);

	/**
	 * @return the unidadeMenorPressao
	 */
	Unidade getUnidadeMenorPressao();

	/**
	 * @param unidadeMenorPressao
	 *            the unidadeMenorPressao to set
	 */
	void setUnidadeMenorPressao(Unidade unidadeMenorPressao);

	/**
	 * @return Date - Retorna data de aviso.
	 */
	Date getDataAviso();

	/**
	 * @param dataAviso - Set data de aviso.
	 */
	void setDataAviso(Date dataAviso);

	/**
	 * @return BigDecimal - Retorna data de aviso.
	 */
	BigDecimal getVolumeFornecimento();

	/**
	 * @param volumeFornecimento - Set Volume fornecimento.
	 */
	void setVolumeFornecimento(BigDecimal volumeFornecimento);

}
