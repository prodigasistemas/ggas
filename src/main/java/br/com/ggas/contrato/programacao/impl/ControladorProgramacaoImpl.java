/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.programacao.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Agrupador;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.programacao.ParadaProgramadaVO;
/**
 * Classe responsável pelos atributos e implementação de métodos relacionados ao Controlador de Programação.
 *
 */
class ControladorProgramacaoImpl extends ControladorNegocioImpl implements ControladorProgramacao {

	private static final String PARADA = " parada ";

	private static final String SOLICITACAO_CONSUMO = " solicitacaoConsumo ";

	private static final String DATA_APURACAÇÃO = "Data Apuracação";

	private static final String DATA_PARADA = "Data Parada";

	private static final String DATA_FINAL = "dataFinal";

	private static final int MES_NOVEMBRO = 10;

	private static final int NOVA_ESCALA = 6;

	private static final int DIA_31 = 31;

	private static final int LIMITE_CASAS_DECIMAIS_PROGRAMACAO_CONSUMO = 6;

	private static final Logger LOG = Logger.getLogger(ControladorProgramacaoImpl.class);

	private static final String CDL = "cdl";

	private static final String SUPRIDORA = "supridora";

	private static final String QDSMAIORQDC = "QDS>=QDC";

	private static final String QDSMENORQDC = "QDS<QDC";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	/**
	 * Auto injeção do controlador para uso na classe.
	 */
	@Autowired
	@Qualifier("controladorParametroSistema")
	public ControladorParametroSistema controladorParametroSistema;

	private static ServiceLocator serviceLocator = ServiceLocator.getInstancia();

	private ControladorPontoConsumo getControladorPontoConsumo() {

		return (ControladorPontoConsumo) serviceLocator.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
	}

	private ControladorEntidadeConteudo getControladorEntidadeConteudo() {

		return (ControladorEntidadeConteudo) serviceLocator
						.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
	}

	private ControladorContrato getControladorContrato() {

		return (ControladorContrato) serviceLocator.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ParadaProgramada.BEAN_ID_PARADA_PROGRAMADA);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ParadaProgramada.BEAN_ID_PARADA_PROGRAMADA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * criarSolicitacaoConsumoPontoConsumo()
	 */
	@Override
	public EntidadeNegocio criarSolicitacaoConsumoPontoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						SolicitacaoConsumoPontoConsumo.BEAN_ID_SOLICITACAO_CONSUMO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeSolicitacaoConsumoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(SolicitacaoConsumoPontoConsumo.BEAN_ID_SOLICITACAO_CONSUMO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeApuracaoQuantidade() {

		return ServiceLocator.getInstancia().getClassPorID(ApuracaoQuantidade.BEAN_ID_APURACAO_QUANTIDADE);
	}

	public Class<?> getClasseEntidadeTempSolicitacaoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID("tempSolicitacaoConsumo");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.
	 * ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio instanceof ParadaProgramada) {
			ParadaProgramada paradaProgramada = (ParadaProgramada) entidadeNegocio;
			Contrato contrato = getControladorContrato().obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo()).getContrato();
			validarDataParadaProgramada(paradaProgramada.getDataParada(), contrato);
		}
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidadeQDC() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidadeQDC.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE_QDC);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);
	}

	public Class<?> getClasseEntidadeRecuperacao() {

		return ServiceLocator.getInstancia().getClassPorID(Recuperacao.BEAN_ID_RECUPERACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #consultarParadasProgramadas (java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ParadaProgramada> consultarParadasProgramadasEmComum(Long[] idPontosConsumo, Long idTipoParada)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select parada ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PARADA);
		hql.append(" inner join fetch parada.pontoConsumo pontoConsumo ");
		hql.append(" left join parada.unidadeMenorPressao unidadeMenorPressao ");
		hql.append(" where ");
		hql.append(" parada.pontoConsumo.chavePrimaria in (:idPontosConsumo) ");

		hql.append(" and parada.tipoParada.chavePrimaria = :idTipoParada ");
		hql.append(" order by parada.dataParada ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("idPontosConsumo", idPontosConsumo);
		query.setLong("idTipoParada", idTipoParada);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #consultarParadasProgramadas (java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ParadaProgramada> consultarParadaProgramadasEmComum(Long idPontosConsumo, Long idTipoParada) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select parada ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PARADA);
		hql.append(" inner join fetch parada.pontoConsumo pontoConsumo ");
		hql.append(" left join parada.unidadeMenorPressao unidadeMenorPressao ");
		hql.append(" where ");
		hql.append(" parada.pontoConsumo.chavePrimaria in (:idPontosConsumo) ");

		hql.append(" and parada.tipoParada.chavePrimaria = :idTipoParada ");
		hql.append(" order by parada.dataParada ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontosConsumo", idPontosConsumo);
		query.setLong("idTipoParada", idTipoParada);

		return query.list();
	}

	/**
	 * Validar paradas existentes.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param listaDatasParadas
	 *            the lista datas paradas
	 * @param idTipoParada
	 *            the id tipo parada
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarParadasExistentes(Long[] chavesPrimarias, String[] listaDatasParadas, Long idTipoParada) throws NegocioException {

		if ((chavesPrimarias != null && chavesPrimarias.length > 0) && (listaDatasParadas != null && listaDatasParadas.length > 0)) {
			Collection<Date> listaDatas = new HashSet<>();
			Date date = null;
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			for (int i = 0; i < listaDatasParadas.length; i++) {
				if (listaDatasParadas[i] != null && !"".equals(listaDatasParadas[i])) {
					try {
						date = new Date(formatter.parse(listaDatasParadas[i]).getTime());
					} catch (ParseException e) {
						LOG.error(e);
					}
					listaDatas.add(date);
				}
			}

			Integer qtd = this.verificarParadaExistente(chavesPrimarias, listaDatas.toArray(new Date[listaDatas.size()]), idTipoParada);
			if (qtd != null && qtd > 0) {
				Collection<ParadaProgramada> listaParadas = this.consultarParadasProgramadas(chavesPrimarias, idTipoParada);
				StringBuilder auxiliar = new StringBuilder();
				for (ParadaProgramada paradaProgramada : listaParadas) {
					for (int i = 0; i < listaDatasParadas.length; i++) {
						try {
							date = new Date(formatter.parse(listaDatasParadas[i]).getTime());
						} catch (ParseException e) {
							LOG.error(e);
						}
						if (paradaProgramada.getDataParada().compareTo(date) == 0) {
							auxiliar.append(paradaProgramada.getPontoConsumo().getDescricao());
							auxiliar.append(" / ");
							auxiliar.append(paradaProgramada.getTipoParada().getDescricao());
							auxiliar.append(" / ");
							auxiliar.append(Util.converterDataParaStringSemHora(paradaProgramada.getDataParada(), Constantes.FORMATO_DATA_BR));
							auxiliar.append(Constantes.STRING_VIRGULA_ESPACO);
						}
					}
				}
				String[] nomesPontoConsumo = {auxiliar.toString()};
				throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_PARADA_PROGRAMADA_EXISTENTE, nomesPontoConsumo);
			}
		}
	}

	/**
	 * Validar dados pressao.
	 *
	 * @param pressoes
	 *            the pressoes
	 * @param unidadesPressao
	 *            the unidades pressao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosPressao(String[] pressoes, Long[] unidadesPressao) throws NegocioException {

		for (int i = 0; i < pressoes.length; i++) {
			if (((!"".equals(pressoes[i])) && (unidadesPressao[i] == -1))
							|| (("".equals(pressoes[i])) && (unidadesPressao[i] != null && unidadesPressao[i] != -1))) {
				throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_PARADA_DADOS_PRESSAO, true);
			}
		}

	}

	/**
	 * Consultar paradas programadas.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param idTipoParada
	 *            the id tipo parada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<ParadaProgramada> consultarParadasProgramadas(Long[] chavesPrimarias, Long idTipoParada) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PARADA);
		hql.append(" inner join fetch parada.pontoConsumo ");
		hql.append(" inner join fetch parada.tipoParada ");
		hql.append(" where ");
		hql.append(" parada.pontoConsumo.chavePrimaria in (:CHAVES_PRIMARIAS)");
		hql.append(" and parada.tipoParada.chavePrimaria <> :idTipoParada ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("CHAVES_PRIMARIAS", chavesPrimarias);
		query.setLong("idTipoParada", idTipoParada);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * consultarParadasProgramadasPeriodo(java.lang.Long[], java.lang.Long[],
	 * java.util.Date, java.util.Date, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ParadaProgramada> consultarParadasProgramadasPeriodo(Long[] chavesPontoConsumo, Long[] chavesTipoParada,
					Date dataInicial, Date dataFinal, String supridora) throws NegocioException {

		// TODO vsm testar método
		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PARADA);
		hql.append(" inner join fetch parada.pontoConsumo ");
		hql.append(" inner join fetch parada.tipoParada ");
		hql.append(" where ");
		hql.append(" parada.pontoConsumo.chavePrimaria in (:chavesPontoConsumo)");

		if (chavesTipoParada != null) {
			hql.append(" and parada.tipoParada.chavePrimaria in (:chavesTipoParada) ");
		}
		if (supridora != null) {
			hql.append(" and parada.nomeSolicitante like :supridora ");
		}

		hql.append(" and parada.dataParada between :dataInicial and :dataFinal");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesPontoConsumo", chavesPontoConsumo);

		if (chavesTipoParada != null) {
			query.setParameterList("chavesTipoParada", chavesTipoParada);
		}
		if (supridora != null) {
			query.setParameter(SUPRIDORA, supridora);
		}

		query.setDate("dataInicial", dataInicial);
		query.setDate(DATA_FINAL, dataFinal);

		return query.list();
	}

	/**
	 * Array contains.
	 *
	 * @param chaves
	 *            the chaves
	 * @param entrada
	 *            the entrada
	 * @return the int
	 */
	public int arrayContains(Long[] chaves, Long entrada) {

		for (int i = 0; i < chaves.length; i++) {
			if (chaves[i] != null && chaves[i].equals(entrada)) {
				// encontrado, retorna posição
				// encontrada
				return i;
			}
		}
		// não encontrado
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #inserirParadasProgramadas (java.lang.Long[], java.lang.String[],
	 * br.com.ggas.controleacesso.Usuario, br.com.ggas.auditoria.DadosAuditoria,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public int[] inserirParadasProgramadas(final Long[] chavesPrimarias, String[] listaDatasParadas, Usuario usuario,
					DadosAuditoria dadosAuditoria, String nomeSolicitante, String comentario, Long idTipoParada, String[] pressoes,
					String[] datasPressao, Long[] unidadesPressao, Long[] chavesPontoConsumoParada, String dataAviso,
					String volumeReferencia) throws ParseException, GGASException {

		Long[] chavesPrimariasLocal = chavesPrimarias;

		this.validarParadasExistentes(chavesPrimariasLocal, listaDatasParadas, idTipoParada);
		if ((pressoes != null && pressoes.length > 0) || (unidadesPressao != null)) {
			this.validarDadosPressao(pressoes, unidadesPressao);
		}

		Collection<ParadaProgramada> paradasProgramadasEmComum = consultarParadasProgramadasEmComum(chavesPrimariasLocal, idTipoParada);

		Collection<ParadaProgramada> paradasProgramadasAtualizacao = new ArrayList<>();
		Collection<String> datasParadasInclusao = new ArrayList<>();
		int[] status = {0, 0};

		Long[] chavesParadasRemocao = null;

		// Separando as paradas que serão
		// removidas das que serão atualizadas.
		for (ParadaProgramada paradaProgramada : paradasProgramadasEmComum) {
			String data = Util.converterDataParaStringSemHora(paradaProgramada.getDataParada(), Constantes.FORMATO_DATA_BR);

			if (ArrayUtils.contains(listaDatasParadas, data)) {

				paradasProgramadasAtualizacao.add(paradaProgramada);

			} else {
				chavesParadasRemocao = (Long[]) ArrayUtils.add(chavesParadasRemocao, paradaProgramada.getChavePrimaria());
			}
		}

		if (chavesPrimariasLocal.length == 1 && (chavesParadasRemocao != null) && (chavesParadasRemocao.length > 0)) {

			this.removerParadas(chavesParadasRemocao);

		}

		if (!paradasProgramadasAtualizacao.isEmpty()) {
			for (ParadaProgramada paradaProgramada : paradasProgramadasAtualizacao) {
				try {
					paradaProgramada.setDadosAuditoria(dadosAuditoria);
					if ((pressoes != null) && (pressoes.length > 0) && (datasPressao != null)
							&& (datasPressao.length > 0)) {
						montarDadosPressao(paradaProgramada, pressoes, datasPressao, unidadesPressao,
								chavesPontoConsumoParada);
					}
					this.atualizar(paradaProgramada);
				} catch (ConcorrenciaException e) {
					throw new NegocioException(e);
				}
			}
		}

		// Separando as paradas que serão
		// inseridas.
		if ((listaDatasParadas != null) && (listaDatasParadas.length > 0)) {
			for (int i = 0; i < listaDatasParadas.length; i++) {

				datasParadasInclusao.add(listaDatasParadas[i]);

			}
		}

		Long[] chaves = null;
		for (int j = 0; j < chavesPrimariasLocal.length; j++) {

			for (ParadaProgramada parada : paradasProgramadasEmComum) {
				if (parada.getPontoConsumo().getChavePrimaria() != chavesPrimariasLocal[j]) {
					if (chaves != null) {
						int posicao = arrayContains(chaves, chavesPrimariasLocal[j]);
						if (posicao == -1) {
							chaves = (Long[]) ArrayUtils.add(chaves, chavesPrimariasLocal[j]);
						}
					} else {
						chaves = (Long[]) ArrayUtils.add(null, chavesPrimariasLocal[j]);
					}
				}
			}
		}

		if (chaves != null) {
			chavesPrimariasLocal = chaves;
		}

		if (!datasParadasInclusao.isEmpty()) {
			Date dataAtual = Calendar.getInstance().getTime();
			EntidadeConteudo tipoParada = getControladorEntidadeConteudo().obter(idTipoParada);
			for (int i = 0; i < chavesPrimariasLocal.length; i++) {

				PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter(chavesPrimariasLocal[i]);
				ContratoPontoConsumo contratoPontoConsumo = getControladorContrato().obterContratoAtivoPontoConsumo(pontoConsumo);

				// TKT 3332 ok
				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = getControladorContrato().consultarContratoPontoConsumoPorPontoConsumoRecente(
									pontoConsumo.getChavePrimaria());
				}

				Contrato contrato = contratoPontoConsumo.getContrato();

				List<Date> listaDatas = new ArrayList<>();
				Date date = null;
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				for (int j = 0; j < listaDatasParadas.length; j++) {
					if (listaDatasParadas[j] != null && !"".equals(listaDatasParadas[j])) {
						try {
							date = new Date(formatter.parse(listaDatasParadas[j]).getTime());
						} catch (ParseException e) {
							LOG.error(e);
						}
						listaDatas.add(date);
					}
				}

				Collections.sort(listaDatas, new Comparator<Date>(){

					@Override
					public int compare(Date o1, Date o2) {

						return o1.compareTo(o2);
					}
				});

				datasParadasInclusao = new ArrayList<>();
				for (Date data : listaDatas) {
					datasParadasInclusao.add(Util.converterDataParaString(data));
				}

				for (String dataNova : datasParadasInclusao) {
					Date dataParada = null;

					dataParada = Util.converterCampoStringParaData(DATA_PARADA, dataNova, Constantes.FORMATO_DATA_BR);

					int qtdParadaExistente = this.obterQuantidadeParadaProgramadaExistente(pontoConsumo, dataParada, idTipoParada);

					if (qtdParadaExistente <= 0) {

						validarDataParadaProgramada(dataParada, contrato);

						ParadaProgramada paradaProgramada = (ParadaProgramada) criar();

						if (nomeSolicitante != null) {
							paradaProgramada.setNomeSolicitante(nomeSolicitante);
						}
						if (!StringUtils.isEmpty(comentario)) {
							paradaProgramada.setComentario(comentario);
						}

						Date dataAvisoDate = Util.converterCampoStringParaData(DATA_PARADA, dataAviso, Constantes.FORMATO_DATA_BR);
						BigDecimal volumeFornecimento = Util.converterCampoStringParaValorBigDecimal("Volume Referência", volumeReferencia,
										Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

						if (volumeFornecimento.compareTo(getControladorContrato().obterSomaQdcContrato(contratoPontoConsumo, dataParada)) > 0) {
							throw new NegocioException(Constantes.ERRO_VOLUME_FORNECIMENTO_MAIOR_SOMAQDC, true);
						}

						paradaProgramada.setDataParada(dataParada);
						paradaProgramada.setPontoConsumo(pontoConsumo);
						paradaProgramada.setUsuario(usuario);
						paradaProgramada.setDataOperacao(dataAtual);
						paradaProgramada.setTipoParada(tipoParada);
						paradaProgramada.setDataAviso(dataAvisoDate);
						paradaProgramada.setVolumeFornecimento(volumeFornecimento);
						paradaProgramada.setDadosAuditoria(dadosAuditoria);

						if (tipoParada.getChavePrimaria() == EntidadeConteudo.CHAVE_PARADA_PROGRAMADA) {

							Boolean indicador = this.isParadaProgramadaApurada(contratoPontoConsumo, paradaProgramada, Boolean.TRUE);
							paradaProgramada.setIndicadorParadaApurada(indicador);

						} else {

							paradaProgramada.setIndicadorParadaApurada(Boolean.TRUE);

						}

						if ((pressoes != null) && (pressoes.length > 0) && (datasPressao != null)
								&& (datasPressao.length > 0)) {
							montarDadosPressao(paradaProgramada, pressoes, datasPressao, unidadesPressao,
									chavesPontoConsumoParada);
						}

						this.inserir(paradaProgramada);
						status[0]++;

					} else {
						status[1]++;
					}

				}

			}
		}

		return status;

	}

	/**
	 * Remover paradas.
	 *
	 * @param chavesParadasRemocao
	 *            the chaves paradas remocao
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	@SuppressWarnings("unchecked")
	private void removerParadas(Long[] chavesParadasRemocao) throws NegocioException, ConcorrenciaException {

		for (int i = 0; i < chavesParadasRemocao.length; i++) {

			ParadaProgramada parada = (ParadaProgramada) this.obter(chavesParadasRemocao[i]);

			if (parada.getTipoParada().getChavePrimaria() == EntidadeConteudo.CHAVE_PARADA_PROGRAMADA) {

				Long idPontoConsumo = parada.getPontoConsumo().getChavePrimaria();

				this.remover(parada);

				Criteria criteria = this.createCriteria(ParadaProgramada.class);
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
				criteria.add(Restrictions.eq("tipoParada.chavePrimaria", EntidadeConteudo.CHAVE_PARADA_PROGRAMADA));
				criteria.addOrder(Order.asc("dataParada"));

				List<ParadaProgramada> listaParadas = criteria.list();

				for (ParadaProgramada paradaProgramada : listaParadas) {

					ContratoPontoConsumo contratoPontoConsumo = getControladorContrato().obterContratoAtivoPontoConsumo(
									paradaProgramada.getPontoConsumo());

					// TKT 3332 ok
					if (contratoPontoConsumo == null) {
						contratoPontoConsumo = getControladorContrato().consultarContratoPontoConsumoPorPontoConsumoRecente(
										paradaProgramada.getPontoConsumo().getChavePrimaria());
					}

					if (contratoPontoConsumo != null) {

						paradaProgramada.setIndicadorParadaApurada(this.isParadaProgramadaApurada(contratoPontoConsumo, paradaProgramada,
										Boolean.FALSE));
						this.atualizar(paradaProgramada);

					}

				}

			} else {

				this.remover(parada);

			}

		}

	}

	/**
	 * Checks if is parada programada apurada.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param paradaProgramada
	 *            the parada programada
	 * @param adicionada
	 *            the adicionada
	 * @return the boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Boolean isParadaProgramadaApurada(ContratoPontoConsumo contratoPontoConsumo, ParadaProgramada paradaProgramada,
					Boolean adicionada) throws NegocioException {

		Boolean retorno = Boolean.TRUE;

		if (contratoPontoConsumo != null && contratoPontoConsumo.getIndicadorParadaProgramada()) {

			Calendar calDataParada = Calendar.getInstance();
			calDataParada.setTime(paradaProgramada.getDataParada());

			Calendar calDataInicioContrato = Calendar.getInstance();
			calDataInicioContrato.setTime(contratoPontoConsumo.getContrato().getDataAssinatura());

			Calendar calDataFinalContrato = Calendar.getInstance();

			if (contratoPontoConsumo.getContrato().getDataVencimentoObrigacoes() != null) {

				calDataFinalContrato.setTime(contratoPontoConsumo.getContrato().getDataVencimentoObrigacoes());

			} else {

				calDataFinalContrato.set(Calendar.MONTH, Calendar.DECEMBER);
				calDataFinalContrato.set(Calendar.DAY_OF_MONTH, DIA_31);

			}

			Integer anoInicial = calDataInicioContrato.get(Calendar.YEAR);
			Integer anoFinal = calDataFinalContrato.get(Calendar.YEAR);
			Integer anoParada = calDataParada.get(Calendar.YEAR);

			Map<Integer, List<ParadaProgramada>> mapParadas = new TreeMap<>();

			for (int i = anoInicial; i <= anoFinal; i++) {

				Date dataInicial = Util.obterPrimeiroDiaMes(i);
				Date dataFinal = Util.obterUltimoDiaMes(i);

				Criteria criteria = this.createCriteria(ParadaProgramada.class);
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", contratoPontoConsumo.getPontoConsumo().getChavePrimaria()));
				criteria.add(Restrictions.eq("tipoParada.chavePrimaria", paradaProgramada.getTipoParada().getChavePrimaria()));
				criteria.add(Restrictions.between("dataParada", dataInicial, dataFinal));
				criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));

				if (!adicionada) {
					criteria.add(Restrictions.ne("chavePrimaria", paradaProgramada.getChavePrimaria()));
					criteria.add(Restrictions.ne("indicadorParadaApurada", Boolean.FALSE));
				}

				criteria.addOrder(Order.asc("dataParada"));

				mapParadas.put(i, criteria.list());

			}

			Integer quantidadeAnosParada = null;
			Integer quantidadeTotalParada = null;
			Integer diasConsecutivosParada = null;
			Integer quantidadeMaximaAnoParada = null;

			if (paradaProgramada.getNomeSolicitante().equals(CDL) || paradaProgramada.getNomeSolicitante().equals(SUPRIDORA)) {

				quantidadeAnosParada = contratoPontoConsumo.getQuantidadeAnosParadaCDL();
				quantidadeTotalParada = contratoPontoConsumo.getQuantidadeTotalParadaCDL();
				diasConsecutivosParada = contratoPontoConsumo.getDiasConsecutivosParadaCDL();
				quantidadeMaximaAnoParada = contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL();

			} else {

				quantidadeAnosParada = contratoPontoConsumo.getQuantidadeAnosParadaCliente();
				quantidadeTotalParada = contratoPontoConsumo.getQuantidadeTotalParadaCliente();
				diasConsecutivosParada = contratoPontoConsumo.getDiasConsecutivosParadaCliente();
				quantidadeMaximaAnoParada = contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente();

			}

			if (this.isPeriodoParadaInvalido(mapParadas, anoParada)) {

				retorno = Boolean.FALSE;

			} else if (this.isLimiteMaximoTotalParadasPorAnoExcedido(quantidadeMaximaAnoParada, mapParadas, anoParada)) {

				retorno = Boolean.FALSE;

			} else if (this.isLimiteMaximoTotalParadasPorPeriodoExcedido(quantidadeAnosParada, quantidadeTotalParada, mapParadas, anoParada)) {

				retorno = Boolean.FALSE;

			} else if (this.isLimiteMaximoDiasConsecutivosParadaProgramadaExcedido(diasConsecutivosParada, mapParadas, anoParada,
							paradaProgramada.getDataParada())) {

				retorno = Boolean.FALSE;

			}
		}

		return retorno;
	}

	/**
	 * Checks if is limite maximo dias consecutivos parada programada excedido.
	 *
	 * @param diasConsecutivosParada
	 *            the dias consecutivos parada
	 * @param mapParadas
	 *            the map paradas
	 * @param anoParada
	 *            the ano parada
	 * @param dataParada
	 *            the data parada
	 * @return the boolean
	 */
	private Boolean isLimiteMaximoDiasConsecutivosParadaProgramadaExcedido(Integer diasConsecutivosParada,
					Map<Integer, List<ParadaProgramada>> mapParadas, Integer anoParada, Date dataParada) {

		Boolean retorno = Boolean.FALSE;

		if (diasConsecutivosParada != null && !mapParadas.isEmpty() && dataParada != null) {

			List<ParadaProgramada> listaParada = mapParadas.get(anoParada);

			if (listaParada != null && !listaParada.isEmpty()) {

				List<Date> datas = new ArrayList<>();

				for (ParadaProgramada paradaProgramada : listaParada) {
					datas.add(paradaProgramada.getDataParada());
				}

				datas.add(dataParada);

				List<Date> datasNaoPermitidas = this.obterDadasNaoPermitidas(datas, diasConsecutivosParada);

				for (Date dataNaoPermitida : datasNaoPermitidas) {

					if (dataNaoPermitida.compareTo(dataParada) == 0) {
						retorno = Boolean.TRUE;
						break;
					}

				}

			}

		}

		return retorno;
	}

	/**
	 * Obter dadas nao permitidas.
	 *
	 * @param datas
	 *            the datas
	 * @param diasConsecutivosParada
	 *            the dias consecutivos parada
	 * @return the list
	 */
	private List<Date> obterDadasNaoPermitidas(List<Date> datas, Integer diasConsecutivosParada) {

		List<Date> datasNaoPermitidas = new ArrayList<>();
		Map<Date, Boolean> mapData = new TreeMap<>();

		for (Date data : datas) {
			mapData.put(data, Boolean.FALSE);
		}

		for (Entry<Date, Boolean> entry : mapData.entrySet()) {
			Date dataInicio = entry.getKey();

			if (!entry.getValue()) {

				int i = 0;

				Calendar cal = Calendar.getInstance();
				cal.setTime(dataInicio);
				cal.add(Calendar.DAY_OF_MONTH, 1);

				while (mapData.containsKey(cal.getTime())) {

					i++;

					if (i >= diasConsecutivosParada) {
						datasNaoPermitidas.add(cal.getTime());
					}

					mapData.put(cal.getTime(), Boolean.TRUE);
					cal.add(Calendar.DAY_OF_MONTH, 1);

				}

			}

		}

		return datasNaoPermitidas;

	}

	/**
	 * Checks if is limite maximo total paradas por periodo excedido.
	 *
	 * @param quantidadeAnosParada
	 *            the quantidade anos parada
	 * @param quantidadeTotalParada
	 *            the quantidade total parada
	 * @param mapParadas
	 *            the map paradas
	 * @param anoParada
	 *            the ano parada
	 * @return the boolean
	 */
	private Boolean isLimiteMaximoTotalParadasPorPeriodoExcedido(Integer quantidadeAnosParada, Integer quantidadeTotalParada,
					Map<Integer, List<ParadaProgramada>> mapParadas, Integer anoParada) {

		Boolean retorno = Boolean.FALSE;

		if (quantidadeAnosParada != null && quantidadeTotalParada != null && !mapParadas.isEmpty()) {

			Map<Integer, List<Integer>> mapGrupos = new TreeMap<>();
			int grupoAdd = 1;
			int qtdaAdd = 1;

			for (Integer ano : mapParadas.keySet()) {

				List<Integer> listaAno = null;

				if (qtdaAdd == 1) {

					listaAno = new ArrayList<>();
					listaAno.add(ano);

				} else {

					listaAno = mapGrupos.get(grupoAdd);
					listaAno.add(ano);

				}

				mapGrupos.put(grupoAdd, listaAno);

				if (qtdaAdd == quantidadeAnosParada) {

					grupoAdd++;
					qtdaAdd = 1;

				} else {

					qtdaAdd++;

				}

			}

			for (Entry<Integer, List<Integer>> entry : mapGrupos.entrySet()) {

				List<Integer> anos = entry.getValue();
				Boolean isAnoGrupo = Boolean.FALSE;

				for (Integer ano : anos) {

					if (ano.intValue() == anoParada.intValue()) {
						isAnoGrupo = Boolean.TRUE;
						break;
					}

				}

				if (isAnoGrupo) {

					Integer qtdaPeriodo = 0;

					for (Integer ano : anos) {

						List<ParadaProgramada> listaParada = mapParadas.get(ano);

						if (listaParada != null && !listaParada.isEmpty()) {
							qtdaPeriodo = qtdaPeriodo + listaParada.size();
						}

					}

					if ((qtdaPeriodo + 1) > quantidadeTotalParada) {

						retorno = Boolean.TRUE;

					}

					break;
				}

			}

		}

		return retorno;
	}

	/**
	 * Checks if is periodo parada invalido.
	 *
	 * @param mapParadas
	 *            the map paradas
	 * @param anoParada
	 *            the ano parada
	 * @return the boolean
	 */
	private Boolean isPeriodoParadaInvalido(Map<Integer, List<ParadaProgramada>> mapParadas, Integer anoParada) {

		Boolean retorno = Boolean.FALSE;

		if (!mapParadas.containsKey(anoParada)) {

			retorno = Boolean.TRUE;

		}

		return retorno;
	}

	/**
	 * Checks if is limite maximo total paradas por ano excedido.
	 *
	 * @param quantidadeTotalParada
	 *            the quantidade total parada
	 * @param mapParadas
	 *            the map paradas
	 * @param anoParada
	 *            the ano parada
	 * @return the boolean
	 */
	private Boolean isLimiteMaximoTotalParadasPorAnoExcedido(Integer quantidadeTotalParada,
					Map<Integer, List<ParadaProgramada>> mapParadas, Integer anoParada) {

		Boolean retorno = Boolean.FALSE;

		if (quantidadeTotalParada != null && !mapParadas.isEmpty()) {

			List<ParadaProgramada> lista = mapParadas.get(anoParada);
			List<ParadaProgramada> listaAux = new ArrayList<>();

			for (ParadaProgramada paradaProgramada : lista) {
				if (paradaProgramada.isIndicadorParadaApurada()) {
					listaAux.add(paradaProgramada);
				}
			}

			if (!listaAux.isEmpty() && listaAux.size() >= quantidadeTotalParada) {

				retorno = Boolean.TRUE;

			}

		}

		return retorno;
	}

	/**
	 * Montar dados pressao.
	 *
	 * @param paradaProgramada
	 *            the parada programada
	 * @param pressoes
	 *            the pressoes
	 * @param datasPressao
	 *            the datas pressao
	 * @param unidadesPressao
	 *            the unidades pressao
	 * @param chavesPontoConsumoParada
	 *            the chaves ponto consumo parada
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void montarDadosPressao(ParadaProgramada paradaProgramada, String[] pressoes, String[] datasPressao, Long[] unidadesPressao,
					Long[] chavesPontoConsumoParada) throws NegocioException {

		ControladorUnidade controladorUnidade = (ControladorUnidade) serviceLocator
						.getControladorNegocio(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);
		List<String> listaDatasPressao = Arrays.asList(datasPressao);

		if (listaDatasPressao.contains(Util.converterDataParaStringSemHora(paradaProgramada.getDataParada(), Constantes.FORMATO_DATA_BR))) {
			for (int i = 0; i < datasPressao.length; i++) {
				if (paradaProgramada.getPontoConsumo().getChavePrimaria() == chavesPontoConsumoParada[i]
								&& (datasPressao[i].equals(Util.converterDataParaStringSemHora(paradaProgramada.getDataParada(),
												Constantes.FORMATO_DATA_BR))) && ((pressoes[i] != null) && (!"".equals(pressoes[i])))) {
					BigDecimal pressao;
					try {
						pressao = Util.converterCampoStringParaValorBigDecimal("Menor Pressão", pressoes[i], Constantes.FORMATO_VALOR_BR,
										Constantes.LOCALE_PADRAO);
					} catch (FormatoInvalidoException e) {
						throw new NegocioException(e);
					}
					paradaProgramada.setMenorPressao(pressao);
					paradaProgramada.setUnidadeMenorPressao((Unidade) controladorUnidade.obter(unidadesPressao[i]));
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * manterParadasProgramadas(java.lang.Long[], java.lang.String[],
	 * br.com.ggas.controleacesso.Usuario, br.com.ggas.auditoria.DadosAuditoria,
	 * java.lang.String, java.lang.String, java.lang.Long, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public Collection<ParadaProgramadaVO> manterParadasProgramadas(Long[] chavesPrimarias, String[] listaDatasParadas, Usuario usuario,
					DadosAuditoria dadosAuditoria, String nomeSolicitante, String comentario, Long idTipoParada, String dataAviso,
					String volumeFornecimento) throws NegocioException {

		this.validarParadasExistentes(chavesPrimarias, listaDatasParadas, idTipoParada);

		Collection<ParadaProgramada> paradasProgramadasEmComum = consultarParadasProgramadasEmComum(chavesPrimarias, idTipoParada);
		Collection<ParadaProgramada> paradasProgramadasAtualizacao = new ArrayList<>();
		Collection<String> datasParadasInclusao = new ArrayList<>();

		Collection<ParadaProgramadaVO> listaParadaProgramadaVO = new ArrayList<>();

		Long[] chavesParadasRemocao = null;

		// Separando as paradas que serão
		// removidas das que serão atualizadas.
		for (ParadaProgramada paradaProgramada : paradasProgramadasEmComum) {
			String data = Util.converterDataParaStringSemHora(paradaProgramada.getDataParada(), Constantes.FORMATO_DATA_BR);
			if (ArrayUtils.contains(listaDatasParadas, data)) {
				paradasProgramadasAtualizacao.add(paradaProgramada);
			} else {
				chavesParadasRemocao = (Long[]) ArrayUtils.add(chavesParadasRemocao, paradaProgramada.getChavePrimaria());
			}
		}

		// Separando as paradas que serão
		// inseridas.
		if ((listaDatasParadas != null) && (listaDatasParadas.length > 0)) {
			for (int i = 0; i < listaDatasParadas.length; i++) {
				if (!contemParadaProgramada(paradasProgramadasEmComum, listaDatasParadas[i])) {
					datasParadasInclusao.add(listaDatasParadas[i]);
				}
			}
		}

		if (!datasParadasInclusao.isEmpty()) {

			for (int i = 0; i < chavesPrimarias.length; i++) {
				PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter(chavesPrimarias[i]);
				ContratoPontoConsumo contratoPontoConsumo = getControladorContrato().obterContratoAtivoPontoConsumo(pontoConsumo);
				// TKT 3332 ok
				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = getControladorContrato().consultarContratoPontoConsumoPorPontoConsumoRecente(
									pontoConsumo.getChavePrimaria());
				}
				Contrato contrato = contratoPontoConsumo.getContrato();
				for (String dataNova : datasParadasInclusao) {
					Date dataParada = null;
					try {
						dataParada = Util.converterCampoStringParaData(DATA_PARADA, dataNova, Constantes.FORMATO_DATA_BR);
					} catch (GGASException e) {
						LOG.error(e.getMessage(), e);
						throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, ParadaProgramada.PARADA_PROGRAMADA_DATA_PARADA);
					}

					int qtdParadaExistente = this.obterQuantidadeParadaProgramadaExistente(pontoConsumo, dataParada, idTipoParada);

					Boolean existeParada = this.validarParada(listaParadaProgramadaVO, pontoConsumo, dataParada);

					if ((qtdParadaExistente <= 0) && (!existeParada)) {

						validarDataParadaProgramada(dataParada, contrato);

						ParadaProgramadaVO paradaProgramadaVO = new ParadaProgramadaVO();

						paradaProgramadaVO.setDataParada(Util.converterDataParaStringSemHora(dataParada, Constantes.FORMATO_DATA_BR));
						paradaProgramadaVO.setNomeSolicitante(nomeSolicitante);
						paradaProgramadaVO.setDataAviso(dataAviso);
						paradaProgramadaVO.setVolumeFornecimento(volumeFornecimento);

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(dataParada);
						int mesData = calendar.get(Calendar.MONTH);
						String mes = String.valueOf(mesData);
						if (mesData < MES_NOVEMBRO) {
							mes = "0" + mes;
						}

						paradaProgramadaVO.setDia(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
						paradaProgramadaVO.setMes(mes);
						paradaProgramadaVO.setAno(String.valueOf(calendar.get(Calendar.YEAR)));
						paradaProgramadaVO.setPontoConsumo(pontoConsumo);

						listaParadaProgramadaVO.add(paradaProgramadaVO);
					}
				}
			}
		}
		return listaParadaProgramadaVO;
	}

	/**
	 * Validar parada.
	 *
	 * @param listaParadaProgramadaVO
	 *            the lista parada programada vo
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dataParada
	 *            the data parada
	 * @return the boolean
	 */
	private Boolean validarParada(Collection<ParadaProgramadaVO> listaParadaProgramadaVO, PontoConsumo pontoConsumo, Date dataParada) {

		Boolean existeParada = Boolean.FALSE;
		String dataParadaAux = Util.converterDataParaStringSemHora(dataParada, Constantes.FORMATO_DATA_BR);

		for (ParadaProgramadaVO paradaProgramadaVO : listaParadaProgramadaVO) {
			if ((paradaProgramadaVO.getPontoConsumo().equals(pontoConsumo)) && (paradaProgramadaVO.getDataParada().equals(dataParadaAux))) {
				existeParada = Boolean.TRUE;
			}

		}

		return existeParada;
	}

	/**
	 * Verificar parada existente.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param listaDatasParadas
	 *            the lista datas paradas
	 * @param idTipoParada
	 *            the id tipo parada
	 * @return the integer
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Integer verificarParadaExistente(Long[] chavesPrimarias, Date[] listaDatasParadas, Long idTipoParada) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" pontoConsumo.chavePrimaria in (:CHAVES_PRIMARIAS) ");
		hql.append(" and dataParada in (:LISTA_DATAS_PARADAS) ");
		hql.append(" and tipoParada.chavePrimaria <> :idTipoParada ");
		hql.append(" and habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("CHAVES_PRIMARIAS", chavesPrimarias);
		query.setParameterList("LISTA_DATAS_PARADAS", listaDatasParadas);
		query.setLong("idTipoParada", idTipoParada);

		Long qtdParadas = (Long) query.uniqueResult();

		return qtdParadas.intValue();
	}

	/**
	 * Contem parada programada.
	 *
	 * @param listaParadas
	 *            the lista paradas
	 * @param dataParada
	 *            the data parada
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private boolean contemParadaProgramada(Collection<ParadaProgramada> listaParadas, String dataParada) {

		boolean retorno = false;

		for (ParadaProgramada parada : listaParadas) {
			String data = Util.converterDataParaStringSemHora(parada.getDataParada(), Constantes.FORMATO_DATA_BR);

			if (dataParada.equals(data)) {
				retorno = true;
				break;
			}
		}

		return retorno;
	}

	/**
	 * Obter quantidade parada programada existente.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dataParada
	 *            the data parada
	 * @param idTipoParada
	 *            the id tipo parada
	 * @return the int
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private int obterQuantidadeParadaProgramadaExistente(PontoConsumo pontoConsumo, Date dataParada, Long idTipoParada)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and dataParada = :dataParada ");
		hql.append(" and tipoParada.chavePrimaria = :idTipoParada ");
		hql.append(" and habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setDate("dataParada", dataParada);
		query.setLong("idTipoParada", idTipoParada);

		Long qtdParadas = (Long) query.uniqueResult();

		return (int) ((qtdParadas != null) ? qtdParadas : 0);


	}

	/*
	 * ano(Minúsculo) -> no primeiro ano o ano(Minúsculo) conta a partir da data
	 * de assinatura do contrato até 31/12. Os anos intermediários vão de 01/01
	 * até 31/12. o último ano vai de 01/01 até a data de encerramento do
	 * contrato. ANO(Maiúsculo) -> equivalente a data de aniversário
	 */

	/**
	 * Validar data parada programada.
	 *
	 * @param dataParada
	 *            the data parada
	 * @param contrato
	 *            the contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDataParadaProgramada(Date dataParada, Contrato contrato) throws NegocioException {

		Calendar calendar = Calendar.getInstance();
		int anoAtual = calendar.get(Calendar.YEAR);
		if (contrato.getIndicadorAnoContratual()) {// ano
													// maiusculo
			calendar.setTime(dataParada);
			int anoParada = calendar.get(Calendar.YEAR);
			anoAtual = anoAtual - 1; // limite
										// mínimo
										// de -1
										// ano
										// para a
										// programação
			if (anoParada < anoAtual) {
				throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_DATA_INVALIDA_ANO_MAIUSCULO, true);
			}

			// ano minusculo
		} else {
			Date dataAssinatura = contrato.getDataAssinatura();
			DateTime dataLimiteParada = new DateTime(dataAssinatura);
			dataLimiteParada.withYear(anoAtual);
			dataLimiteParada = dataLimiteParada.minusYears(1);

			if (Util.compararDatas(dataParada, dataLimiteParada.toDate()) < 0) {
				throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_DATA_INVALIDA_ANO_MINUSCULO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #listarSolicitacaoConsumoPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPontoConsumo(
					ContratoPontoConsumo contratoPontoConsumo,
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Integer anoInformado, Integer mesInformado)
									throws NegocioException {

		Calendar calendar = Calendar.getInstance();

		Integer ano = anoInformado;
		Integer mes = mesInformado;

		if ((ano == null && mes != null) || (ano != null && mes == null)) {

			throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS, false);

		} else if (ano == null) {

			// se ano e mês não foram informados,
			// assumir o corrente.
			ano = calendar.get(Calendar.YEAR);
			mes = calendar.get(Calendar.MONTH) + 1;

		} else {

			// o dia é setado para 1 para que não
			// haja erro nos meses que tem
			// menos de 31 dias.
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			// se foi informado, assumir este
			// valor para a consulta ou calculo
			// da qnt de dias.
			calendar.set(Calendar.YEAR, ano);
			calendar.set(Calendar.MONTH, mes - 1);
		}

		ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = listaContratoPontoConsumoModalidade.iterator().next();

		Long chavePontoConsumo = contratoPontoConsumo.getPontoConsumo().getChavePrimaria();

		long chavePrimariaContratoPontoConsumoModalidade = contratoPontoConsumoModalidade.getChavePrimaria();

		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = pesquisarSolicitacaoConsumoPontoConsumoModalidade(
						chavePontoConsumo, ano, mes, chavePrimariaContratoPontoConsumoModalidade);

		if (listaSolicitacaoConsumo == null || listaSolicitacaoConsumo.isEmpty()) {
			// montar uma lista com todos os dias
			// do mês e valores zerados.
			int qtdDiasMes = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			listaSolicitacaoConsumo = new ArrayList<>();

			for (int cont = 1; cont <= qtdDiasMes; cont++) {

				SolicitacaoConsumoPontoConsumo solicitacao = (SolicitacaoConsumoPontoConsumo) criarSolicitacaoConsumoPontoConsumo();

				// setando dataSolicitacao
				DateTime dataSolicitacao = new DateTime();
				dataSolicitacao = dataSolicitacao.withYear(ano);
				dataSolicitacao = dataSolicitacao.withMonthOfYear(mes);
				dataSolicitacao = dataSolicitacao.withDayOfMonth(cont);
				dataSolicitacao = dataSolicitacao.withHourOfDay(0);
				dataSolicitacao = dataSolicitacao.withMinuteOfHour(0);
				dataSolicitacao = dataSolicitacao.withSecondOfMinute(0);
				dataSolicitacao = dataSolicitacao.withMillisOfSecond(0);

				solicitacao.setDataSolicitacao(dataSolicitacao.toDate());
				solicitacao.setPontoConsumo(contratoPontoConsumo.getPontoConsumo());
				solicitacao.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);

				// obtem a Modalidade do Contrato
				// que permite a configuração da
				// Vigencia
				solicitacao.setValorQDC(this.calcularQDCSolicitacaoConsumo(listaContratoPontoConsumoModalidade, dataSolicitacao.toDate()));
				solicitacao.setIndicadorAceite(Boolean.FALSE);
				listaSolicitacaoConsumo.add(solicitacao);
			}
		} else {
			for (SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo : listaSolicitacaoConsumo) {
				BigDecimal valorQDC = this.calcularQDCSolicitacaoConsumo(listaContratoPontoConsumoModalidade,
								solicitacaoConsumoPontoConsumo.getDataSolicitacao());
				solicitacaoConsumoPontoConsumo.setValorQDC(valorQDC);
			}
		}

		return listaSolicitacaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * listarSolicitacaoConsumoPontoConsumoHistorico(br.com.ggas.contrato.
	 * contrato. ContratoPontoConsumo, java.util.Collection, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPontoConsumoHistorico(
					ContratoPontoConsumo contratoPontoConsumo,
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Integer anoInformado,
					Integer mesInformado) throws NegocioException {

		Calendar calendar = Calendar.getInstance();
		Integer ano = anoInformado;
		Integer mes = mesInformado;

		if ((ano == null && mes != null) || (ano != null && mes == null)) {

			throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS, false);

		} else if (ano == null) {

			// se ano e mês não foram informados,
			// assumir o corrente.
			ano = calendar.get(Calendar.YEAR);
			mes = calendar.get(Calendar.MONTH) + 1;

		} else {

			// o dia é setado para 1 para que não
			// haja erro nos meses que tem
			// menos de 31 dias.
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			// se foi informado, assumir este
			// valor para a consulta ou calculo
			// da qnt de dias.
			calendar.set(Calendar.YEAR, ano);
			calendar.set(Calendar.MONTH, mes - 1);

		}

		Long chavePontoConsumo = contratoPontoConsumo.getPontoConsumo().getChavePrimaria();
		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = pesquisarSolicitacaoConsumoPontoConsumoHistorico(
						chavePontoConsumo, ano, mes);

		if (listaSolicitacaoConsumo == null || listaSolicitacaoConsumo.isEmpty()) {
			// montar uma lista com todos os dias
			// do mês e valores zerados.
			int qtdDiasMes = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			// instancia a lista para add as data de solicitacao
			listaSolicitacaoConsumo = new ArrayList<>();

			for (int cont = 1; cont <= qtdDiasMes; cont++) {

				SolicitacaoConsumoPontoConsumo solicitacao = (SolicitacaoConsumoPontoConsumo) criarSolicitacaoConsumoPontoConsumo();

				// setando dataSolicitacao
				DateTime dataSolicitacao = new DateTime();
				dataSolicitacao = dataSolicitacao.withYear(ano);
				dataSolicitacao = dataSolicitacao.withMonthOfYear(mes);
				dataSolicitacao = dataSolicitacao.withDayOfMonth(cont);
				dataSolicitacao = dataSolicitacao.withHourOfDay(0);
				dataSolicitacao = dataSolicitacao.withMinuteOfHour(0);
				dataSolicitacao = dataSolicitacao.withSecondOfMinute(0);
				dataSolicitacao = dataSolicitacao.withMillisOfSecond(0);

				solicitacao.setDataSolicitacao(dataSolicitacao.toDate());
				solicitacao.setPontoConsumo(contratoPontoConsumo.getPontoConsumo());

				// obtem a Modalidade do Contrato
				// que permite a configuração da
				// Vigencia
				solicitacao.setValorQDC(this.calcularQDCSolicitacaoConsumo(listaContratoPontoConsumoModalidade, dataSolicitacao.toDate()));
				solicitacao.setIndicadorAceite(Boolean.FALSE);
				listaSolicitacaoConsumo.add(solicitacao);
			}
		} else {
			for (SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo : listaSolicitacaoConsumo) {
				BigDecimal valorQDC = this.calcularQDCSolicitacaoConsumo(listaContratoPontoConsumoModalidade,
								solicitacaoConsumoPontoConsumo.getDataSolicitacao());
				solicitacaoConsumoPontoConsumo.setValorQDC(valorQDC);
			}
		}

		return listaSolicitacaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #obterSaldoARecuperar(java.lang.Long[], java.lang.Long,
	 * java.lang.Integer)
	 */
	@Override
	public BigDecimal obterSaldoARecuperar(Contrato contrato, Long[] arrayIdPontoConsumo, Long idApuracaoPenalidade,
					Integer anoMesApuracao, Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP) throws GGASException {

		BigDecimal saldoARecuperar = BigDecimal.ZERO;
		BigDecimal saldoRecuperacao = BigDecimal.ZERO;
		BigDecimal saldoEstimativaRecuperacao = BigDecimal.ZERO;

		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		for (Long idPontoConsumo : arrayIdPontoConsumo) {

			HistoricoConsumo ultimoHistoricoConsumo = controladorHistoricoConsumo.obterHistoricoConsumo(idPontoConsumo, Boolean.TRUE,
							Boolean.TRUE, Boolean.TRUE);
			if (ultimoHistoricoConsumo != null) {
				Date dataFimUltimaApuracao = ultimoHistoricoConsumo.getHistoricoAtual().getDataLeituraFaturada();

				saldoEstimativaRecuperacao = this.obterEstimativaRecuperacao(idPontoConsumo, dataFimUltimaApuracao, anoMesApuracao);
				if (saldoEstimativaRecuperacao == null) {
					saldoEstimativaRecuperacao = BigDecimal.ZERO;
				}
			}
		}

		saldoRecuperacao = this.obterSaldoRecuperacao(contrato, arrayIdPontoConsumo, idApuracaoPenalidade, anoMesApuracao, listaAQPP);

		saldoARecuperar = saldoRecuperacao.subtract(saldoEstimativaRecuperacao);
		if (saldoARecuperar.compareTo(BigDecimal.ZERO) < 0) {
			saldoARecuperar = BigDecimal.ZERO;
		}

		return saldoARecuperar;
	}

	/**
	 * Método reponsável por obter o Saldo a Recuperar, num determinado Ano/Mês,
	 * para um Ponto de Consumo específico ou um grupo de pontos de consumo
	 * agrupados por volume.
	 *
	 * @param contrato
	 *            o contrato
	 * @param arrayIdPontoConsumo
	 *            Id do Ponto de Consumo específico ou de um grupo de pontos de
	 *            consumo agrupados por volume
	 * @param idApuracaoPenalidade
	 *            id da apuração de Penalidade
	 * @param anoMesApuracao
	 *            Ano/Mês de Apuração
	 * @param listaAQPP
	 *            lista de AQPP
	 * @return o aldo a Recuperar
	 * @throws GGASException
	 *             vaso ocorra algum erro
	 */
	private BigDecimal obterSaldoRecuperacao(Contrato contrato, Long[] arrayIdPontoConsumo, Long idApuracaoPenalidade,
					Integer anoMesApuracao, Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP) throws GGASException {

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		Contrato contratoPai = null;
		Contrato contratoAtual = null;
		BigDecimal saldoRecuperacao = BigDecimal.ZERO;

		String anoMesApuracaoAux = "01/" + Util.formatarAnoMes(anoMesApuracao);
		Date dataApuracao = Util.converterCampoStringParaData(DATA_APURACAÇÃO, anoMesApuracaoAux, Constantes.FORMATO_DATA_BR);

		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades = new ArrayList<>();
		Date dataAssinaturaContrato = null;
		if (arrayIdPontoConsumo != null && arrayIdPontoConsumo.length >= 1) {
			PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter(arrayIdPontoConsumo[0]);

			dataAssinaturaContrato = contrato.getDataAssinatura();

			if (contrato.getChavePrimariaPai() != null) {
				contratoPai = (Contrato) getControladorContrato().obter(contrato.getChavePrimariaPai());
			} else {
				contratoPai = contrato;
			}
			contratoAtual = contrato;

			contratoPontoConsumoModalidades.addAll(getControladorContrato().consultarContratoPontoConsumoModalidades(contratoAtual,
							pontoConsumo));
		}

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumoModalidades) {

			if (contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR() != null) {

				DateTime dataAux = new DateTime(dataApuracao);
				dataAux = dataAux.minusYears(contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR());
				Date dataApuracaoAux = dataAux.toDate();

				Long idPontoConsumo ;

				if(arrayIdPontoConsumo.length > 1 ){
					idPontoConsumo = null;
				} else {
					idPontoConsumo = arrayIdPontoConsumo[0];
				}

				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade = controladorApuracaoPenalidade
								.consultarApuracaoQtdPenalidadePeriodicidade(contratoPai, idPontoConsumo, idApuracaoPenalidade,
												contratoPontoConsumoModalidade.getContratoModalidade().getChavePrimaria(), dataApuracaoAux);

				if (listaApuracaoQuantidadePenalidadePeriodicidade != null && !listaApuracaoQuantidadePenalidadePeriodicidade.isEmpty()) {

					for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade :
							listaApuracaoQuantidadePenalidadePeriodicidade) {

						BigDecimal recuperadoAQPP = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
						if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada() != null) {
							recuperadoAQPP = recuperadoAQPP.subtract(apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada());
						}

						listaAQPP.add(apuracaoQuantidadePenalidadePeriodicidade);

						saldoRecuperacao = saldoRecuperacao.add(recuperadoAQPP);
					}
				}

			} else {

				Long idPontoConsumo;

				if (arrayIdPontoConsumo.length > 1) {
					idPontoConsumo = null;
				} else {
					idPontoConsumo = arrayIdPontoConsumo[0];
				}

				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade = controladorApuracaoPenalidade
								.consultarApuracaoQtdPenalidadePeriodicidade(contratoPai, idPontoConsumo, idApuracaoPenalidade,
												contratoPontoConsumoModalidade.getContratoModalidade().getChavePrimaria(),
												dataAssinaturaContrato);

				if (listaApuracaoQuantidadePenalidadePeriodicidade != null && !listaApuracaoQuantidadePenalidadePeriodicidade.isEmpty()) {

					for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade :
							listaApuracaoQuantidadePenalidadePeriodicidade) {
						BigDecimal recuperadoAQPP = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
						if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada() != null) {
							recuperadoAQPP = recuperadoAQPP.subtract(apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada());
						}

						listaAQPP.add(apuracaoQuantidadePenalidadePeriodicidade);

						saldoRecuperacao = saldoRecuperacao.add(recuperadoAQPP);
					}
				}
			}
		}

		return saldoRecuperacao;
	}

	/**
	 * Obter estimativa recuperacao.
	 *
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param dataFimUltimaApuracao
	 *            the data fim ultima apuracao
	 * @param anoMesApuracao
	 *            the ano mes apuracao
	 * @return the big decimal
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private BigDecimal obterEstimativaRecuperacao(Long chavePontoConsumo, Date dataFimUltimaApuracao, Integer anoMesApuracao)
					throws GGASException {

		String anoMesApuracaoAux = "01/" + Util.formatarAnoMes(anoMesApuracao);
		Date dataInicio = Util.converterCampoStringParaData(DATA_APURACAÇÃO, anoMesApuracaoAux, Constantes.FORMATO_DATA_BR);
		String dataAux = Util.obterUltimoDiaMes(dataInicio, Constantes.FORMATO_DATA_BR);
		Date dataFim = Util.converterCampoStringParaData(DATA_APURACAÇÃO, dataAux, Constantes.FORMATO_DATA_BR);

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select sum(pcsc.valorQPNR) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(" pcsc ");
		hql.append(" where pcsc.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and pcsc.dataSolicitacao >= :dataFimUltimaApuracao ");
		hql.append(" and pcsc.dataSolicitacao not between :dataInicio and :dataFim ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", chavePontoConsumo);
		query.setDate("dataFimUltimaApuracao", dataFimUltimaApuracao);
		query.setDate("dataInicio", dataInicio);
		query.setDate("dataFim", dataFim);

		return (BigDecimal) query.uniqueResult();
	}

	/**
	 * Pesquisar solicitacao consumo ponto consumo historico.
	 *
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<SolicitacaoConsumoPontoConsumo> pesquisarSolicitacaoConsumoPontoConsumoHistorico(Long chavePontoConsumo,
					Integer ano, Integer mes) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(" where pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and year(dataSolicitacao) = :ano ");
		hql.append(" and month(dataSolicitacao) = :mes ");
		hql.append(" order by dataSolicitacao , dataOperacao,habilitado");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", chavePontoConsumo);
		query.setInteger("ano", ano);
		query.setInteger("mes", mes);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #validarValorTotalQPNR (java.math.BigDecimal[], java.math.BigDecimal)
	 */
	@Override
	public void validarValorTotalQPNR(String[] listaValorQPNR, BigDecimal saldoARecuperar) throws GGASException {

		BigDecimal totalvalorQPNR = BigDecimal.ZERO;

		Collection<BigDecimal> valorQPNR = new ArrayList<>();
		for (int i = 0; i < listaValorQPNR.length; i++) {
			if (StringUtils.isNotEmpty(listaValorQPNR[i])) {
				valorQPNR.add(Util.converterCampoStringParaValorBigDecimal("Valor QPNR", listaValorQPNR[i], Constantes.FORMATO_VALOR_BR,
								Constantes.LOCALE_PADRAO));
			}
		}

		for (BigDecimal valor : valorQPNR) {
			totalvalorQPNR = totalvalorQPNR.add(valor);
		}

		if (totalvalorQPNR.compareTo(saldoARecuperar) > 0) {
			throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_VALOR_QPNR, false);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #atualizarListaSolicitacaoConsumoPontoConsumo (java.util.Collection)
	 */
	@Override
	public void atualizarListaSolicitacaoConsumoPontoConsumo(Collection<SolicitacaoConsumoPontoConsumo> listaNovaSolicitacao,
					Long idPontoConsumo, Integer ano, Integer mes, Long chaveContratoPontoConsumoModalidade) throws NegocioException,
					ConcorrenciaException {

		if ((ano == null && mes != null) || (ano != null && mes == null)) {
			throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS, false);
		}

		// Verificar se existem registros no banco
		// para esse ponto de consumo,
		// se existir, apagar e gerar novamente
		// com as novas informações.
		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumoRegistrada =
						pesquisarSolicitacaoConsumoPontoConsumoModalidade(
						idPontoConsumo, ano, mes, chaveContratoPontoConsumoModalidade);

		Map<Date, SolicitacaoConsumoPontoConsumo> mapaSolicitacaoConsumoRegistrada = Util
						.trasformarParaMapa(listaSolicitacaoConsumoRegistrada);

		if (CollectionUtils.isNotEmpty(listaSolicitacaoConsumoRegistrada)) {

			for (SolicitacaoConsumoPontoConsumo novaSolicitacao : listaNovaSolicitacao) {
				Date dataSolicitacao = novaSolicitacao.getDataSolicitacao();
				SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo = mapaSolicitacaoConsumoRegistrada.get(dataSolicitacao);
				solicitacaoConsumoPontoConsumo.setHabilitado(false);

				this.atualizar(solicitacaoConsumoPontoConsumo, SolicitacaoConsumoPontoConsumo.class);

			}

		}

		// inserir todos os novos
		for (SolicitacaoConsumoPontoConsumo novaSolicitacao : listaNovaSolicitacao) {

			this.validarSolicitacaoConsumoPontoConsumo(novaSolicitacao, chaveContratoPontoConsumoModalidade);
			this.inserir(novaSolicitacao);
		}

	}

	/**
	 * Validar solicitacao consumo ponto consumo.
	 *
	 * @param solicitacao
	 *            the solicitacao
	 *
	 * @param chaveContratoPontoConsumoModalidade
	 *            the chave contrato ponto consumo modalidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarSolicitacaoConsumoPontoConsumo(SolicitacaoConsumoPontoConsumo solicitacao,
			Long chaveContratoPontoConsumoModalidade) throws NegocioException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = controladorContrato
						.obterContratoPontoConsumoModalidade(chaveContratoPontoConsumoModalidade);

		if (solicitacao.getContratoPontoConsumoModalidade() == null) {
			solicitacao.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);
		}

		BigDecimal valorQDS = solicitacao.getValorQDS();
		BigDecimal valorQDP = solicitacao.getValorQDP();
		BigDecimal valorQPNR = solicitacao.getValorQPNR();

		if (valorQPNR != null && valorQDS != null && valorQDP != null) {

			BigDecimal valorSomaSolicitada = valorQPNR.add(valorQDS);
			if (valorSomaSolicitada.compareTo(valorQDP) < 0) {
				throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_SOMA_QPNR_QDS_IGUAL_QDP, false);
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #listarParadasNaoRepetidas (java.util.Collection)
	 */
	@Override
	public Collection<ParadaProgramada> listarParadasNaoRepetidas(Collection<ParadaProgramada> listaParadasRepetidas) {

		List<ParadaProgramada> paradasNaoRepetidas = new ArrayList<>();

		for (ParadaProgramada paradaProgramada : listaParadasRepetidas) {

			if (!contemParadaProgramadaDate(paradasNaoRepetidas, paradaProgramada)) {
				if (contemParadaDataOperacaoDiferente(listaParadasRepetidas, paradaProgramada)) {
					paradaProgramada.setComentario("");
				}
				paradasNaoRepetidas.add(paradaProgramada);
			}
		}

		return paradasNaoRepetidas;
	}

	/**
	 * Contem parada programada date.
	 *
	 * @param listaParada
	 *            the lista parada
	 * @param paradaProgramada
	 *            the parada programada
	 * @return true, if successful
	 */
	private boolean contemParadaProgramadaDate(List<ParadaProgramada> listaParada, ParadaProgramada paradaProgramada) {

		boolean retorno = false;

		for (ParadaProgramada parada : listaParada) {
			if ((parada.getDataParada().compareTo(paradaProgramada.getDataParada()) == 0)
							&& (parada.getPontoConsumo().getChavePrimaria() == paradaProgramada.getPontoConsumo().getChavePrimaria())) {
				retorno = true;
				break;
			}
		}

		return retorno;
	}

	/**
	 * Contem parada data operacao diferente.
	 *
	 * @param paradasProgramadas
	 *            the paradas programadas
	 * @param parada
	 *            the parada
	 * @return true, if successful
	 */
	private boolean contemParadaDataOperacaoDiferente(Collection<ParadaProgramada> paradasProgramadas, ParadaProgramada parada) {

		boolean retorno = false;

		for (ParadaProgramada paradaProgramada : paradasProgramadas) {
			if ((paradaProgramada.getDataParada().compareTo(parada.getDataParada()) == 0)
							&& (paradaProgramada.getDataOperacao().compareTo(parada.getDataOperacao()) != 0)) {
				retorno = true;
				break;
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #validarTipoParada(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo validarTipoParada(Long idTipoParada) throws NegocioException {

		EntidadeConteudo tipoParada = null;
		if ((idTipoParada != null) && (idTipoParada >= EntidadeConteudo.CHAVE_PARADA_PROGRAMADA)
						&& (idTipoParada <= EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO)) {
			tipoParada = getControladorEntidadeConteudo().obter(idTipoParada);
		} else {
			throw new NegocioException(ControladorProgramacao.ERRO_NEGOCIO_PARAMETRO_INVALIDO, true);
		}
		return tipoParada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #calcularQDCSolicitacaoConsumo (java.util.Collection, java.util.Date)
	 */
	@Override
	public BigDecimal calcularQDCSolicitacaoConsumo(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Date dataSolicitacao) throws NegocioException {

		BigDecimal valorQDC = BigDecimal.ZERO;

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {

			Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC = contratoPontoConsumoModalidade
							.getListaContratoPontoConsumoModalidadeQDC();

			if (Util.isNullOrEmpty(listaContratoPontoConsumoModalidadeQDC)) {
				listaContratoPontoConsumoModalidadeQDC = getControladorContrato()
								.consultarContratoPontoConsumoModalidadeQDCPorModalidade(contratoPontoConsumoModalidade.getChavePrimaria());
			}

			List<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDCOrdenada = this
							.ordenarListaModalidadeQDC(listaContratoPontoConsumoModalidadeQDC);

			BigDecimal volumeModalidadeQDC = null;
			BigDecimal volumeModalidadeAnterior = null;
			for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : listaModalidadeQDCOrdenada) {
				Date dataVigenciaModalidadeQDC = contratoPontoConsumoModalidadeQDC.getDataVigencia();
				if (Util.compararDatas(dataSolicitacao, dataVigenciaModalidadeQDC) < 0) {
					volumeModalidadeQDC = volumeModalidadeAnterior;
					break;
				}
				volumeModalidadeAnterior = contratoPontoConsumoModalidadeQDC.getMedidaVolume();
			}
			if (volumeModalidadeQDC == null) {
				if (volumeModalidadeAnterior == null) {
					volumeModalidadeQDC = BigDecimal.ZERO;
				} else {
					volumeModalidadeQDC = listaModalidadeQDCOrdenada.get(listaModalidadeQDCOrdenada.size() - 1).getMedidaVolume();
				}
			}
			valorQDC = valorQDC.add(volumeModalidadeQDC);
		}

		return valorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #ordenarListaModalidadeQDC (java.util.Collection)
	 */
	@Override
	public List<ContratoPontoConsumoModalidadeQDC> ordenarListaModalidadeQDC(
					Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC) {

		List<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDCOrdenada = new ArrayList<>();
		for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : listaContratoPontoConsumoModalidadeQDC) {
			listaModalidadeQDCOrdenada.add(contratoPontoConsumoModalidadeQDC);
		}
		Collections.sort(listaModalidadeQDCOrdenada, new Comparator<ContratoPontoConsumoModalidadeQDC>(){

			@Override
			public int compare(ContratoPontoConsumoModalidadeQDC o1, ContratoPontoConsumoModalidadeQDC o2) {

				return o1.getDataVigencia().compareTo(o2.getDataVigencia());
			}
		});
		return listaModalidadeQDCOrdenada;
	}


	/**
	 * Consulta a soma do valor QPD das solicitações de consumo, agrupadas
	 * por data de solicitação e chave primária da entidade {@link PontoConsumo}.
	 *
	 * @param listaPontoConsumo - {@link List}
	 * @return mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 */
	@Override
	public MultiKeyMap consultarValorQDPDiarioPorPontoConsumo(List<PontoConsumo> listaPontoConsumo) {

		MultiKeyMap mapaValorQDPPorPontoConsumo = new MultiKeyMap();

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {

			StringBuilder hql = new StringBuilder();

			hql.append(" select pontoConsumo.chavePrimaria, solicitacaoConsumo.dataSolicitacao, ");
			hql.append("  sum(solicitacaoConsumo.valorQDP) ");
			hql.append(" from ");
			hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
			hql.append(SOLICITACAO_CONSUMO);
			hql.append(" inner join solicitacaoConsumo.pontoConsumo pontoConsumo ");
			hql.append(" where pontoConsumo IN (:listaPontoConsumo) ");
			hql.append(" group by pontoConsumo.chavePrimaria, solicitacaoConsumo.dataSolicitacao ");


			Query query = getSession().createQuery(hql.toString());
			query.setParameterList("listaPontoConsumo", listaPontoConsumo);

			List<Object[]> objects = query.list();
			for (Object[] object : objects) {
				mapaValorQDPPorPontoConsumo.put(object[0], object[1], object[2]);
			}
		}

		return mapaValorQDPPorPontoConsumo;
	}



	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * consultarVolumeExecucaoContratoSolicitacao(java.util.Date,
	 * java.util.Date, java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SolicitacaoConsumoPontoConsumo> consultarVolumeExecucaoContratoSolicitacao(Date dataInicial, Date dataFinal,
					Long[] chavesPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(" solicitacao ");
		hql.append(" where ");
		hql.append(" solicitacao.dataSolicitacao between :dataInicial and :dataFinal");
		hql.append(" and solicitacao.pontoConsumo.chavePrimaria in (:chavesPontoConsumo) ");
		hql.append(" order by solicitacao.dataSolicitacao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataInicial", dataInicial);
		query.setDate(DATA_FINAL, dataFinal);
		query.setParameterList("chavesPontoConsumo", chavesPontoConsumo);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * consultarVolumeExecucaoContratoApuracao(java.util.Date, java.util.Date,
	 * java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ApuracaoQuantidade> consultarVolumeExecucaoContratoApuracao(Date dataInicial, Date dataFinal,
					Long[] chavesPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeApuracaoQuantidade().getSimpleName());
		hql.append(" apuracao ");
		hql.append(" where ");
		hql.append(" apuracao.pontoConsumo.chavePrimaria in (:chavesPontoConsumo)");
		hql.append(" and apuracao.dataApuracaoVolumes between :dataInicial and :dataFinal");
		hql.append(" order by apuracao.dataApuracaoVolumes ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesPontoConsumo", chavesPontoConsumo);
		query.setDate("dataInicial", dataInicial);
		query.setDate(DATA_FINAL, dataFinal);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * consultarVolumeExecucaoContratoRecuperacao(java.util.Date,
	 * java.util.Date, java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Recuperacao> consultarVolumeExecucaoContratoRecuperacao(Date dataInicial, Date dataFinal, Long[] chavesPontoConsumo)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeRecuperacao().getSimpleName());
		hql.append(" recuperacao ");
		hql.append(" where ");
		hql.append(" recuperacao.fatura.pontoConsumo.chavePrimaria in (:chavesPontoConsumo)");
		hql.append(" and recuperacao.dataRecuperacao between :dataInicial and :dataFinal");
		hql.append(" order by recuperacao.dataRecuperacao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesPontoConsumo", chavesPontoConsumo);
		query.setDate("dataInicial", dataInicial);
		query.setDate(DATA_FINAL, dataFinal);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao. ControladorProgramacao
	 * #atualizarDadosSolicitacoesConsumo (java.lang.Long, java.util.Map)
	 */
	@Override
	public Map<String, Map<String, String>> atualizarDadosSolicitacoesConsumo(Long idPontoConsumo, Map<String, Map<String, String>> mapa)
					throws NegocioException {

		Map<String, Map<String, String>> retorno = new TreeMap<>();
		Map<String, Map<String, String>> retornoReverso = new LinkedHashMap<>();

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ContratoPontoConsumo contratoPonto = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);

		if (contratoPonto != null) {

			Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = controladorContrato
							.listarContratoPontoConsumoModalidadePorContratoPontoConsumo(contratoPonto.getChavePrimaria());
			BigDecimal percentualVariacao = this.obterPercentualVariacaoSuperiorQDC(listaContratoPontoConsumoModalidade);

			for (Entry<String, Map<String, String>> entry : mapa.entrySet()) {

				Map<String, String> dados = entry.getValue();

				SolicitacaoConsumoPontoConsumo solicitacao = (SolicitacaoConsumoPontoConsumo) this.criarSolicitacaoConsumoPontoConsumo();
				solicitacao.setDataSolicitacao(this.obterDataSolicitacao(entry.getKey()));
				solicitacao.setValorQDC(this.obterValorBigDecimalString(dados.get("qdc")));
				solicitacao.setValorQDS(this.obterValorBigDecimalString(dados.get("qds")));
				solicitacao.setValorQPNR(this.obterValorBigDecimalString(dados.get("qpnr")));
				if (Boolean.valueOf(dados.get("indicadorAceite"))) {
					solicitacao.setValorQDP(this.obterValorBigDecimalString(dados.get("qds")));
				} else {
					solicitacao.setValorQDP(this.obterValorBigDecimalString(dados.get("qdc")));
				}
				solicitacao.setIndicadorAceite(Boolean.valueOf(dados.get("indicadorAceite")));
				String mensagem = null;

				try {

					this.aplicarRegrasSolicitacaoConsumo(solicitacao, percentualVariacao);

				} catch (NegocioException e) {

					mensagem = this.obterMensagemSolicitacaoConsumo(e);

				}

				dados.clear();
				dados.put("qdc", this.obterValorBigDecimalFormatado(solicitacao.getValorQDC()));
				dados.put("qds", this.obterValorBigDecimalFormatado(solicitacao.getValorQDS()));
				dados.put("qpnr", this.obterValorBigDecimalFormatado(solicitacao.getValorQPNR()));
				dados.put("qdp", this.obterValorBigDecimalFormatado(solicitacao.getValorQDP()));
				dados.put("mensagem", mensagem);

				retorno.put(entry.getKey(), dados);
			}

		}

		Object[] keys = retorno.keySet().toArray();

		for (int i = keys.length; i > 0; i--) {
			retornoReverso.put((String) keys[i - 1], retorno.get(keys[i - 1]));
		}

		return retornoReverso;

	}

	/**
	 * Obter percentual variacao superior qdc.
	 *
	 * @param listaContratoPontoConsumoModalidade
	 *            the lista contrato ponto consumo modalidade
	 * @return the big decimal
	 */
	private BigDecimal obterPercentualVariacaoSuperiorQDC(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade) {

		BigDecimal percentualVarSuperiorQDC = null;

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidade) {
			Boolean indicadorQDSMaiorQDC = contratoPontoConsumoModalidade.getIndicadorQDSMaiorQDC();

			if ((indicadorQDSMaiorQDC != null) && (indicadorQDSMaiorQDC)) {

				if (contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC() != null) {
					percentualVarSuperiorQDC = contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC();
				}

				break;
			}
		}

		return percentualVarSuperiorQDC;
	}

	/**
	 * Aplicar regras solicitacao consumo.
	 *
	 * @param solicitacao the solicitacao
	 * @param percentualVariacaoTmp the percentual variacao
	 * @throws NegocioException the negocio exception
	 */
	private void aplicarRegrasSolicitacaoConsumo(SolicitacaoConsumoPontoConsumo solicitacao, BigDecimal percentualVariacaoTmp)
			throws NegocioException {

		BigDecimal percentualVariacao = percentualVariacaoTmp;
		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		String data = df.format(solicitacao.getDataSolicitacao());

		if (percentualVariacao == null) {
			percentualVariacao = BigDecimal.ZERO;
		}

		BigDecimal qdc = solicitacao.getValorQDC();
		BigDecimal qds = solicitacao.getValorQDS();
		BigDecimal qdp = solicitacao.getValorQDP();

		BigDecimal variacao = percentualVariacao.divide(BigDecimal.valueOf(Double.valueOf("100"))).setScale(NOVA_ESCALA, RoundingMode.HALF_UP);
		BigDecimal qdcVariacao = null;
		if (qdc != null) {
			qdcVariacao = qdc.add(qdc.multiply(variacao).setScale(NOVA_ESCALA, RoundingMode.HALF_UP));
		}

		if (qdc == null) {
			throw new NegocioException(ERRO_NEGOCIO_VALOR_QDC_NULO, new Object[] { data });
		}

		if (qds == null) {
			qdp = qdc;
		}

		if (solicitacao.getIndicadorAceite() == null || !solicitacao.getIndicadorAceite()) {

			if (solicitacaoConsumoAtendePrimeiraRegra(qdc, qds, variacao)) {

				qdp = qds;

			} else if (solicitacaoConsumoAtendeSegundaRegra(qdc, qds, variacao)) {

				qdp = qdcVariacao;

			} else if (solicitacaoConsumoAtendeTerceiraRegra(qdc, qds, variacao)) {

				qdp = qds;
			}
		}

		solicitacao.setValorQDC(qdc);
		solicitacao.setValorQDS(qds);
		solicitacao.setValorQDP(qdp);
	}

	/**
	 * Obter mensagem solicitacao consumo.
	 *
	 * @param e the e
	 * @return the string
	 */
	private String obterMensagemSolicitacaoConsumo(NegocioException e) {
		String mensagemSolicitacaoConsumo = null;
		if (e.getChaveErro().equals(ERRO_NEGOCIO_VALOR_QDC_NULO)) {

			mensagemSolicitacaoConsumo = "Valor QDC não informado";

		} else if (e.getChaveErro().equals(ERRO_NEGOCIO_VALOR_QDS_NULO)) {

			mensagemSolicitacaoConsumo = "Valor QDS não informado";

		} else if (e.getChaveErro().equals(ERRO_NEGOCIO_VALOR_QDP_NULO)) {

			mensagemSolicitacaoConsumo = "Valor QDP não informado";

		} else if (e.getChaveErro().equals(ERRO_NEGOCIO_SOLICITACAO_CONSUMO_SO_ALTERADA_MEDIANTE_LOGIN_SENHA)) {

			mensagemSolicitacaoConsumo = "Solicitação de consumo realizada apenas mediante autorização";

		} else if (e.getChaveErro().equals(ERRO_NEGOCIO_SOLICITACAO_CONSUMO_NAO_ATENDE_NENHUMA_REGRA)) {

			mensagemSolicitacaoConsumo = "Solicitação de consumo não atende a nenhuma regra";

		} else if (e.getChaveErro().equals(ERRO_NEGOCIO_SOLICITACAO_CONSUMO_SEM_VARIACAO)) {

			mensagemSolicitacaoConsumo = "O contrato do ponto de consumo não possui variação de QDC";

		}

		return mensagemSolicitacaoConsumo;

	}

	/**
	 * Solicitacao consumo atende primeira regra.
	 *
	 * @param qdc
	 *            the qdc
	 * @param qds
	 *            the qds
	 * @param variacao
	 *            the variacao
	 * @return the boolean
	 */
	private Boolean solicitacaoConsumoAtendePrimeiraRegra(BigDecimal qdc, BigDecimal qds, BigDecimal variacao) {

		Boolean retorno = false;

		if (qdc != null && qds != null && variacao != null) {

			BigDecimal qdcVariacao = qdc.add(qdc.multiply(variacao).setScale(NOVA_ESCALA, RoundingMode.HALF_UP));

			// QDC <= QDS <= (QDC + VARIACAO)
			if (qds.compareTo(qdc) <= 0 && qds.compareTo(qdcVariacao) <= 0) {
				retorno = Boolean.TRUE;
			}

		}

		return retorno;

	}

	/**
	 * Solicitacao consumo atende segunda regra.
	 *
	 * @param qdc
	 *            the qdc
	 * @param qds
	 *            the qds
	 * @param variacao
	 *            the variacao
	 * @return the boolean
	 */
	private Boolean solicitacaoConsumoAtendeSegundaRegra(BigDecimal qdc, BigDecimal qds, BigDecimal variacao) {

		Boolean retorno = false;

		if (qdc != null && qds != null && variacao != null) {

			BigDecimal qdcVariacao = qdc.add(qdc.multiply(variacao).setScale(NOVA_ESCALA, RoundingMode.HALF_UP));

			// QDS > (QDC + VAR)
			if (qds.compareTo(qdcVariacao) > 0) {

				retorno = Boolean.TRUE;

			}

		}

		return retorno;
	}

	/**
	 * Solicitacao consumo atende terceira regra.
	 *
	 * @param qdc
	 *            the qdc
	 * @param qds
	 *            the qds
	 * @return the boolean
	 */
	private Boolean solicitacaoConsumoAtendeTerceiraRegra(BigDecimal qdc, BigDecimal qds, BigDecimal variacao) {

		Boolean retorno = false;

		if (qdc != null && qds != null && variacao != null) {

			BigDecimal qdcVariacao = qdc.add(qdc.multiply(variacao).setScale(
							LIMITE_CASAS_DECIMAIS_PROGRAMACAO_CONSUMO, RoundingMode.HALF_UP));

			if (qds.compareTo(qdcVariacao) < 0) {

				retorno = Boolean.TRUE;

			}
		}

		return retorno;
	}

	/**
	 * Obter data solicitacao.
	 *
	 * @param data
	 *            the data
	 * @return the date
	 */
	private Date obterDataSolicitacao(String data) {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		try {
			return df.parse(data);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Obter valor big decimal string.
	 *
	 * @param valorFormatado
	 *            the valor formatado
	 * @return the big decimal
	 */
	private BigDecimal obterValorBigDecimalString(String valorFormatado) {

		if (valorFormatado != null && StringUtils.isNotEmpty(valorFormatado)) {
			try {
				return Util.converterCampoStringParaValorBigDecimal("Valor", valorFormatado, Constantes.FORMATO_VALOR_BR,
								Constantes.LOCALE_PADRAO);
			} catch (FormatoInvalidoException e) {
				LOG.error(e.getMessage(), e);
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * Obter valor big decimal formatado.
	 *
	 * @param valor the valor
	 * @return the string
	 */
	private String obterValorBigDecimalFormatado(BigDecimal valor) {

		if (valor != null) {

			try {
				return Util.converterCampoValorParaString(valor, Constantes.FORMATO_VALOR_NUMERO_INTEIRO, Constantes.LOCALE_PADRAO);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				return "";
			}

		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao
	 * .ControladorProgramacao
	 * #removerSolicitacaoConsumoAlterarContrato(
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public Map<Long, Map<Long, Collection<SolicitacaoConsumoPontoConsumo>>> removerSolicitacaoConsumoAlterarContrato(Contrato contrato)
					throws NegocioException {

		Map<Long, Map<Long, Collection<SolicitacaoConsumoPontoConsumo>>> mapa =
						new HashMap<>();
		Collection<ContratoPontoConsumo> listaCopc = contrato.getListaContratoPontoConsumo();
		if (CollectionUtils.isNotEmpty(listaCopc)) {
			for (ContratoPontoConsumo copc : listaCopc) {
				Long idPocn = copc.getPontoConsumo().getChavePrimaria();
				Map<Long, Collection<SolicitacaoConsumoPontoConsumo>> mapaComo = new HashMap<>();
				Collection<ContratoPontoConsumoModalidade> listaCopm = getControladorContrato().consultarContratoPontoConsumoModalidades(
								contrato, copc.getPontoConsumo());
				if (CollectionUtils.isNotEmpty(listaCopm)) {
					for (ContratoPontoConsumoModalidade copm : listaCopm) {
						Long idCopm = copm.getChavePrimaria();
						Long idComo = copm.getContratoModalidade().getChavePrimaria();
						Collection<SolicitacaoConsumoPontoConsumo> listaPosc = pesquisarSolicitacaoConsumoPontoConsumoModalidade(idPocn,
										idCopm);
						if (CollectionUtils.isNotEmpty(listaPosc)) {
							for (SolicitacaoConsumoPontoConsumo posc : listaPosc) {
								SolicitacaoConsumoPontoConsumo clonePosc = clonarSolicitacaoConsumoPontoConsumo(posc);
								Util.adicionarElementoMapaCodigoLista(mapaComo, idComo, clonePosc);
								this.remover(posc);
							}
						}
					}
				}
				mapa.put(idPocn, mapaComo);
			}
		}
		return mapa;
	}

	/**
	 * Clonar solicitacao consumo ponto consumo.
	 *
	 * @param clonado the clonado
	 * @return the solicitacao consumo ponto consumo
	 * @throws GGASException the GGAS exception
	 */
	public SolicitacaoConsumoPontoConsumo clonarSolicitacaoConsumoPontoConsumo(SolicitacaoConsumoPontoConsumo clonado) {

		SolicitacaoConsumoPontoConsumo clone = (SolicitacaoConsumoPontoConsumo) criarSolicitacaoConsumoPontoConsumo();

		clone.setPontoConsumo(clonado.getPontoConsumo());
		clone.setContratoPontoConsumoModalidade(clonado.getContratoPontoConsumoModalidade());
		clone.setDataSolicitacao(clonado.getDataSolicitacao());
		clone.setValorQDC(clonado.getValorQDC());
		clone.setValorQDS(clonado.getValorQDS());
		clone.setValorQDP(clonado.getValorQDP());
		clone.setValorQPNR(clonado.getValorQPNR());
		clone.setIndicadorAceite(clonado.getIndicadorAceite());
		clone.setDescricaoMotivoNaoAceite(clonado.getDescricaoMotivoNaoAceite());
		clone.setIndicadorTipoProgramacaoConsumo(clonado.getIndicadorTipoProgramacaoConsumo());
		clone.setHabilitado(clonado.isHabilitado());
		clone.setDataOperacao(clonado.getDataOperacao());
		clone.setUsuario(clonado.getUsuario());
		clone.setIndicadorSolicitante(clonado.isIndicadorSolicitante());
		clone.setChamado(clonado.getChamado());
		clone.setComentario(clonado.getComentario());

		return clone;

	}

	/**
	 * Pesquisar solicitacao consumo ponto consumo modalidade.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chaveContratoPontoConsumoModalidade the chave contrato ponto consumo modalidade
	 * @return the collection
	 */
	@Override
	public Collection<SolicitacaoConsumoPontoConsumo> pesquisarSolicitacaoConsumoPontoConsumoModalidade(Long chavePontoConsumo,
					Long chaveContratoPontoConsumoModalidade) {

		return pesquisarSolicitacaoConsumoPontoConsumoModalidade(chavePontoConsumo, null, null, chaveContratoPontoConsumoModalidade);

	}

	/**
	 * Pesquisar solicitacao consumo ponto consumo modalidade.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param ano the ano
	 * @param mes the mes
	 * @param chaveContratoPontoConsumoModalidade the chave contrato ponto consumo modalidade
	 * @return the collection
	 */
	private Collection<SolicitacaoConsumoPontoConsumo> pesquisarSolicitacaoConsumoPontoConsumoModalidade(Long chavePontoConsumo,
					Integer ano, Integer mes, Long chaveContratoPontoConsumoModalidade) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(SOLICITACAO_CONSUMO);
		hql.append(" inner join fetch solicitacaoConsumo.contratoPontoConsumoModalidade ");
		hql.append(" inner join fetch solicitacaoConsumo.pontoConsumo ");
		hql.append(" where solicitacaoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		if (chaveContratoPontoConsumoModalidade != null) {
			hql.append(
					" and solicitacaoConsumo.contratoPontoConsumoModalidade.chavePrimaria = :chaveContratoPontoConsumoModalidade ");
		}
		if (ano != null) {
			hql.append(" and year(solicitacaoConsumo.dataSolicitacao) = :ano ");
		}

		if (mes != null) {
			hql.append(" and month(solicitacaoConsumo.dataSolicitacao) = :mes ");
		}

		hql.append(" and solicitacaoConsumo.habilitado = true  ");
		hql.append(" order by solicitacaoConsumo.dataSolicitacao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", chavePontoConsumo);
		if (chaveContratoPontoConsumoModalidade != null) {
			query.setLong("chaveContratoPontoConsumoModalidade", chaveContratoPontoConsumoModalidade);
		}
		if (ano != null) {
			query.setInteger("ano", ano);
		}

		if (mes != null) {
			query.setInteger("mes", mes);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * realizaProgramacaoConsumoAutomaticaMensal(java.util.Collection,
	 * java.lang.Integer, java.lang.Integer, java.lang.Long, java.lang.Long)
	 */
	@Override
	public boolean realizaProgramacaoConsumoAutomaticaMensal(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo, Integer ano,
					Integer mes, Long idPontoConsumo, Long chaveContratoPontoConsumoModalidade)
					throws GGASException {

		boolean solicitacaoAceita = false;
		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoPontoConsumoManipulacao = new ArrayList<>();

		for (SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo : listaSolicitacaoConsumo) {
			if (solicitacaoConsumoPontoConsumo.getIndicadorAceite() == null
							|| (solicitacaoConsumoPontoConsumo.getIndicadorAceite() != null && !solicitacaoConsumoPontoConsumo
											.getIndicadorAceite())
							|| (solicitacaoConsumoPontoConsumo.getValorQDP() != null && solicitacaoConsumoPontoConsumo.getValorQDP()
											.compareTo(BigDecimal.ZERO)==0)) {

				if (solicitacaoConsumoPontoConsumo.getValorQDS() != null
								&& solicitacaoConsumoPontoConsumo.getValorQDS().compareTo(solicitacaoConsumoPontoConsumo.getValorQDC()) < 0) {

					solicitacaoConsumoPontoConsumo.setValorQDP(solicitacaoConsumoPontoConsumo.getValorQDS());
				} else {

					solicitacaoConsumoPontoConsumo.setValorQDP(solicitacaoConsumoPontoConsumo.getValorQDC());
				}
				solicitacaoConsumoPontoConsumo.setIndicadorAceite(Boolean.TRUE);
				solicitacaoAceita = true;
			}
			listaSolicitacaoPontoConsumoManipulacao.add(solicitacaoConsumoPontoConsumo);
		}
		this.atualizarListaSolicitacaoConsumoPontoConsumo(listaSolicitacaoPontoConsumoManipulacao, idPontoConsumo, ano, mes,
						chaveContratoPontoConsumoModalidade);
		return solicitacaoAceita;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * realizaProgramacaoConsumoAutomaticaDiariaIntraDiaria(java.util.
	 * Collection, java.lang.Integer, java.lang.Integer, java.lang.Long,
	 * br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade)
	 */
	@Override
	public void realizaProgramacaoConsumoAutomaticaDiariaIntraDiaria(Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo,
					Integer ano, Integer mes, Long idPontoConsumo, ContratoPontoConsumoModalidade contratoPontoConsumoModalidade)
					throws GGASException {

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new ArrayList<>();
		listaContratoPontoConsumoModalidade.add(contratoPontoConsumoModalidade);

		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoPontoConsumoManipulacao = new ArrayList<>();

		for (SolicitacaoConsumoPontoConsumo solicitacaoConsumoPontoConsumo : listaSolicitacaoConsumo) {

			BigDecimal percentualVariacao = this.obterPercentualVariacaoSuperiorQDC(listaContratoPontoConsumoModalidade);

			this.aplicarRegrasSolicitacaoConsumo(solicitacaoConsumoPontoConsumo, percentualVariacao);

			solicitacaoConsumoPontoConsumo.setIndicadorAceite(true);
			listaSolicitacaoPontoConsumoManipulacao.add(solicitacaoConsumoPontoConsumo);
		}

		this.atualizarListaSolicitacaoConsumoPontoConsumo(listaSolicitacaoPontoConsumoManipulacao, idPontoConsumo, ano, mes,
						contratoPontoConsumoModalidade.getChavePrimaria());

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * pesquisarSolicitacaoConsumoPontoConsumoModalidadeDiariaIntradiaria(java.
	 * lang .Long, java.lang.Integer, java.lang.Integer, java.lang.Long,
	 * java.lang.Boolean)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SolicitacaoConsumoPontoConsumo> pesquisarSolicitacaoConsumoPontoConsumoModalidadeDiariaIntradiaria(
					Long chavePontoConsumo, Integer ano, Integer mes, Long chaveContratoPontoConsumoModalidade, String indicadorTipo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(SOLICITACAO_CONSUMO);
		hql.append(" inner join fetch solicitacaoConsumo.contratoPontoConsumoModalidade ");
		hql.append(" inner join fetch solicitacaoConsumo.pontoConsumo ");
		hql.append(" where solicitacaoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and year(solicitacaoConsumo.dataSolicitacao) = :ano ");
		hql.append(" and month(solicitacaoConsumo.dataSolicitacao) = :mes ");
		hql.append(" and solicitacaoConsumo.contratoPontoConsumoModalidade.chavePrimaria = :chaveContratoPontoConsumoModalidade ");
		hql.append(" and solicitacaoConsumo.habilitado = true  ");
		hql.append(" and solicitacaoConsumo.indicadorAceite is null ");
		if (indicadorTipo != null) {
			hql.append(" and solicitacaoConsumo.indicadorTipoProgramacaoConsumo = :indicadorTipo  ");
		} else {
			hql.append(" and solicitacaoConsumo.indicadorTipoProgramacaoConsumo is not null  ");
		}
		hql.append(" order by solicitacaoConsumo.dataSolicitacao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", chavePontoConsumo);
		query.setInteger("ano", ano);
		query.setInteger("mes", mes);
		query.setLong("chaveContratoPontoConsumoModalidade", chaveContratoPontoConsumoModalidade);
		if (indicadorTipo != null) {
			query.setString("indicadorTipo", indicadorTipo);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * listarComentarioSolicitacaoConsumo(java.lang.Long, java.util.Date,
	 * java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SolicitacaoConsumoPontoConsumo> listarComentarioSolicitacaoConsumo(Long chavePontoConsumo, Date dataSolicitacao,
					Long chaveContratoPontoConsumoModalidade) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(SOLICITACAO_CONSUMO);
		hql.append(" inner join fetch solicitacaoConsumo.contratoPontoConsumoModalidade ");
		hql.append(" inner join fetch solicitacaoConsumo.pontoConsumo ");
		hql.append(" where solicitacaoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and  solicitacaoConsumo.dataSolicitacao = :dataSolicitacao ");
		hql.append(" and solicitacaoConsumo.contratoPontoConsumoModalidade.chavePrimaria = :chaveContratoPontoConsumoModalidade ");
		hql.append(" and solicitacaoConsumo.comentario is not null ");
		hql.append(" order by solicitacaoConsumo.dataSolicitacao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", chavePontoConsumo);

		query.setDate("dataSolicitacao", dataSolicitacao);
		query.setLong("chaveContratoPontoConsumoModalidade", chaveContratoPontoConsumoModalidade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * listarSolicitacaoConsumoDiariasIntradiarias(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoDiariasIntradiarias(
					Map<String, Object> filtro) throws GGASException {

		Query query = null;
		if (!filtro.isEmpty()) {
			String dataSolicitacao = (String) filtro.get("dataAtual");
			String opcao = (String) filtro.get("opcao");
			Date data = Util.converterCampoStringParaData("dataAtual", dataSolicitacao, Constantes.FORMATO_DATA_BR);
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
			hql.append(SOLICITACAO_CONSUMO);
			hql.append(" inner join fetch solicitacaoConsumo.contratoPontoConsumoModalidade ");
			hql.append(" inner join fetch solicitacaoConsumo.pontoConsumo ");
			hql.append("  where solicitacaoConsumo.dataSolicitacao = :dataSolicitacao ");
			if (opcao.equalsIgnoreCase(QDSMAIORQDC)) {
				hql.append(" and solicitacaoConsumo.valorQDS >= solicitacaoConsumo.valorQDC  ");
			} else if (opcao.equalsIgnoreCase(QDSMENORQDC)) {
				hql.append(" and solicitacaoConsumo.valorQDS < solicitacaoConsumo.valorQDC  ");
			}
			hql.append(" and solicitacaoConsumo.habilitado = true  ");
			hql.append(" order by solicitacaoConsumo.indicadorTipoProgramacaoConsumo asc , solicitacaoConsumo.dataOperacao desc  ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setDate("dataSolicitacao", data);

		}
		if (query != null) {
			return query.list();
		} else {
			throw new GGASException();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * listarSolicitacaoConsumoPendentes()
	 */
	@Override
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPendentes()
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(SOLICITACAO_CONSUMO);
		hql.append(" inner join fetch solicitacaoConsumo.contratoPontoConsumoModalidade ");
		hql.append(" inner join fetch solicitacaoConsumo.pontoConsumo ");
		hql.append("  where solicitacaoConsumo.descricaoMotivoNaoAceite  is null ");
		hql.append(" and solicitacaoConsumo.habilitado = true  ");
		hql.append(" and solicitacaoConsumo.indicadorAceite  = false  ");
		hql.append(" and solicitacaoConsumo.valorQDP <= 0  ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	/**
	 * Listar Solicitacao Consumo Por Contrato
	 * @param cpContrato the cpContrato
	 * @return the collection
	 */
	public Collection<SolicitacaoConsumoPontoConsumo> listarSolicitacaoConsumoPorContrato(
					long cpContrato) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(SOLICITACAO_CONSUMO);
		hql.append(" inner join fetch solicitacaoConsumo.contratoPontoConsumoModalidade copm ");
		hql.append(" inner join fetch copm.contratoPontoConsumo copc ");
		hql.append(" inner join fetch copc.contrato cont ");
		hql.append(" inner join fetch solicitacaoConsumo.pontoConsumo ");
		hql.append(" where solicitacaoConsumo.habilitado = true  ");
		hql.append(" and cont.chavePrimaria = :cpContrato  ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("cpContrato", cpContrato);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.programacao.ControladorProgramacao#
	 * obterTempSolicitacaoConsmo(java.lang.String,
	 * java.util.Date)
	 */
	@Override
	public TempSolicitacaoConsumo obterTempSolicitacaoConsmo(String enderecoRemoto, Date dataSolicitacao) throws NegocioException {

		Criteria criteria = this.createCriteria(TempSolicitacaoConsumo.class);
		criteria.add(Restrictions.eq("codigoPontoConsumoSupervisorio", enderecoRemoto));
		criteria.add(Restrictions.eq("dataSolicitacao", dataSolicitacao));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		return (TempSolicitacaoConsumo) criteria.uniqueResult();
	}

	@Override
	public void migrarSolicitacaoConsumoAlterarContrato(
					long cpContratoAlterado, long cpContrato) throws NegocioException, ConcorrenciaException {

		ControladorContrato controladorContrato =
						ServiceLocator.getInstancia().getControladorContrato();

		Collection<SolicitacaoConsumoPontoConsumo> poscs =
						listarSolicitacaoConsumoPorContrato(cpContratoAlterado);

		Map<ContratoPontoConsumoModalidade, Collection<SolicitacaoConsumoPontoConsumo>> agruparPoscPorCopm =
						agruparPoscPorCopm(poscs);

		for (Entry<ContratoPontoConsumoModalidade, Collection<SolicitacaoConsumoPontoConsumo>> esPosc : agruparPoscPorCopm.entrySet()) {

			ContratoPontoConsumoModalidade key = esPosc.getKey();
			long cpPontoConsumo = key.getContratoPontoConsumo().getPontoConsumo().getChavePrimaria();
			long cpModalidade = key.getContratoModalidade().getChavePrimaria();

			ContratoPontoConsumoModalidade copm =
							controladorContrato.obterContratoPontoConsumoModalidade(
											cpPontoConsumo, cpContrato, cpModalidade);

			Collection<SolicitacaoConsumoPontoConsumo> value = esPosc.getValue();

			for (SolicitacaoConsumoPontoConsumo posc : value) {

				SolicitacaoConsumoPontoConsumo clone =
								(SolicitacaoConsumoPontoConsumo) SerializationUtils.clone(posc);

				posc.setHabilitado(false);
				atualizar(posc);

				clone.setChavePrimaria(0);
				clone.setContratoPontoConsumoModalidade(copm);
				inserir(clone);

			}

		}

	}


	/**
	 * Agrupar SolicitacaoConsumoPontoConsumo por ContratoPontoConsumoModalidade.
	 *
	 * @param poscs the lista de SolicitacaoConsumoPontoConsumo
	 * @return the hash map
	 */
	private Map<ContratoPontoConsumoModalidade, Collection<SolicitacaoConsumoPontoConsumo>> agruparPoscPorCopm(
					Collection<SolicitacaoConsumoPontoConsumo> poscs) {
		return Util.agrupar(poscs,
				new Agrupador<SolicitacaoConsumoPontoConsumo, ContratoPontoConsumoModalidade>(){

			@Override
			public ContratoPontoConsumoModalidade getChaveAgrupadora(SolicitacaoConsumoPontoConsumo elemento) {

				return elemento.getContratoPontoConsumoModalidade();
			}
		});

	}
}
