/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.programacao;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Mapeavel;
/**
 * Interface responsável pela assinatura dos métodos relacionados a Solicitação de Consumo vinculado ao Ponto de Consumo.
 *
 */
public interface SolicitacaoConsumoPontoConsumo extends EntidadeNegocio, Mapeavel<Date> {

	String BEAN_ID_SOLICITACAO_CONSUMO_PONTO_CONSUMO = "solicitacaoConsumoPontoConsumo";

	String SOLICITACAO_CONSUMO_PONTO_CONSUMO = "SOLICITACAO_CONSUMO_PONTO_CONSUMO_ROTULO";

	String ERRO_TAMANHO_LIMITE_MOTIVO = "ERRO_TAMANHO_LIMITE_MOTIVO";

	String QDC = "QDC";

	String QDS = "QDS";

	String QDP = "QDP";

	String QPNR = "QPNR";

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the dataSolicitacao
	 */
	Date getDataSolicitacao();

	/**
	 * @param dataSolicitacao
	 *            the dataSolicitacao to set
	 */
	void setDataSolicitacao(Date dataSolicitacao);

	/**
	 * @return the valorQDC
	 */
	BigDecimal getValorQDC();

	/**
	 * @param valorQDC
	 *            the valorQDC to set
	 */
	void setValorQDC(BigDecimal valorQDC);

	/**
	 * @return the valorQDS
	 */
	BigDecimal getValorQDS();

	/**
	 * @param valorQDS
	 *            the valorQDS to set
	 */
	void setValorQDS(BigDecimal valorQDS);

	/**
	 * @return the valorQPNR
	 */
	BigDecimal getValorQPNR();

	/**
	 * @param valorQPNR
	 *            the valorQPNR to set
	 */
	void setValorQPNR(BigDecimal valorQPNR);

	/**
	 * @return the valorQDP
	 */
	BigDecimal getValorQDP();

	/**
	 * @param valorQDP
	 *            the valorQDP to set
	 */
	void setValorQDP(BigDecimal valorQDP);

	/**
	 * @return the indicadorAceite
	 */
	Boolean getIndicadorAceite();

	/**
	 * @param indicadorAceite
	 *            the indicadorAceite to set
	 */
	void setIndicadorAceite(Boolean indicadorAceite);

	/**
	 * @return the dataOperacao
	 */
	Date getDataOperacao();

	/**
	 * @param dataOperacao
	 *            the dataOperacao to set
	 */
	void setDataOperacao(Date dataOperacao);

	/**
	 * @return the usuario
	 */
	Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	void setUsuario(Usuario usuario);

	/**
	 * @return the descricaoMotivoNaoAceite
	 */
	String getDescricaoMotivoNaoAceite();

	/**
	 * @param descricaoMotivoNaoAceite
	 *            the descricaoMotivoNaoAceite to
	 *            set
	 */
	void setDescricaoMotivoNaoAceite(String descricaoMotivoNaoAceite);

	/**
	 * @return the contratoPontoConsumoModalidade
	 */
	ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade();

	/**
	 * @param contratoPontoConsumoModalidade
	 *            the
	 *            contratoPontoConsumoModalidade
	 *            to set
	 */
	void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade);

	/**
	 * @return the indicadorSolicitante
	 */
	boolean isIndicadorSolicitante();

	/**
	 * @param indicadorSolicitante
	 *            the indicadorSolicitante to set
	 */
	void setIndicadorSolicitante(boolean indicadorSolicitante);

	/**
	 * @param comentario - Set comentário.
	 */
	void setComentario(String comentario);

	/**
	 * @return String - Retorna comentário.
	 */
	String getComentario();

	/**
	 * @return Chamado - Retorna objeto chamado.
	 */
	Chamado getChamado();

	/**
	 * @param chamado - Set chamado.
	 */
	void setChamado(Chamado chamado);

	/**
	 * @return String - Retorna indicador tipo programação consumo.
	 */
	String getIndicadorTipoProgramacaoConsumo();

	/**
	 * @param indicadorTipoProgramacaoConsumo - Set Indicador tipo programação consumo.
	 */
	void setIndicadorTipoProgramacaoConsumo(String indicadorTipoProgramacaoConsumo);

}
