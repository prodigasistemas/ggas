/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/05/2015 14:55:36
 @author aantonio
 */

package br.com.ggas.contrato.programacao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe temporária responsável pelos atributos da Soliciatação de Consumo. 
 *
 */
public class TempSolicitacaoConsumo extends EntidadeNegocioImpl {

	private static final long serialVersionUID = 1070503960254707509L;

	private Date dataSolicitacao;

	private BigDecimal valorQR;

	private String codigoPontoConsumoSupervisorio;

	public Date getDataSolicitacao() {
		Date data = null;
		if (this.dataSolicitacao != null) {
			data = (Date) this.dataSolicitacao.clone();
		}
		return data;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		if (dataSolicitacao != null) {
			this.dataSolicitacao = (Date) dataSolicitacao.clone();
		} else {
			this.dataSolicitacao = null;
		}
	}

	public BigDecimal getValorQR() {

		return valorQR;
	}

	public void setValorQR(BigDecimal valorQR) {

		this.valorQR = valorQR;
	}

	public String getCodigoPontoConsumoSupervisorio() {

		return codigoPontoConsumoSupervisorio;
	}

	public void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio) {

		this.codigoPontoConsumoSupervisorio = codigoPontoConsumoSupervisorio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}
}
