/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**Classe Responsável pelas ações relacionadas à funcionalidade de  
 * 
 @since 04/11/2014 07:00:02
 @author vpessoa
 */

package br.com.ggas.contrato.contratoadequacaoparceria;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ContratoAdequacaoParceria.
 *
 * @author Crsilva
 */
public interface ContratoAdequacaoParceria extends EntidadeNegocio {

	/** The bean id contrato adequacao parceria. */
	String BEAN_ID_CONTRATO_ADEQUACAO_PARCERIA = "contratoAdequacaoParceria";

	/** The Constant MENSAGEM_SUCESSO. */
	public static final String MENSAGEM_SUCESSO = "CONTRATO ADEQUAÇÃO PARCERIA";

	/**
	 * Gets the contrato.
	 *
	 * @return the contrato
	 */
	public Contrato getContrato();

	/**
	 * Sets the contrato.
	 *
	 * @param contrato the new contrato
	 */
	public void setContrato(Contrato contrato);

	/**
	 * Gets the prazo.
	 *
	 * @return the prazo
	 */
	public Integer getPrazo();

	/**
	 * Sets the prazo.
	 *
	 * @param prazo the new prazo
	 */
	public void setPrazo(Integer prazo);

	/**
	 * Gets the valor empresa.
	 *
	 * @return the valor empresa
	 */
	public BigDecimal getValorEmpresa();

	/**
	 * Sets the valor empresa.
	 *
	 * @param valorEmpresa the new valor empresa
	 */
	public void setValorEmpresa(BigDecimal valorEmpresa);

	/**
	 * Gets the percentual multa.
	 *
	 * @return the percentual multa
	 */
	public BigDecimal getPercentualMulta();

	/**
	 * Sets the percentual multa.
	 *
	 * @param porcentagem the new percentual multa
	 */
	public void setPercentualMulta(BigDecimal porcentagem);

	/**
	 * Gets the ponto consumo.
	 *
	 * @return the ponto consumo
	 */
	public PontoConsumo getPontoConsumo();

	/**
	 * Sets the ponto consumo.
	 *
	 * @param pontoConsumo the new ponto consumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Sets the valor cliente.
	 *
	 * @param valorCliente the new valor cliente
	 */
	public void setValorCliente(BigDecimal valorCliente);

	/**
	 * Gets the valor cliente.
	 *
	 * @return the valor cliente
	 */
	public BigDecimal getValorCliente();

	/**
	 * Sets the indice financeiro.
	 *
	 * @param indiceFinanceiro the new indice financeiro
	 */
	public void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro);

	/**
	 * Gets the indice financeiro.
	 *
	 * @return the indice financeiro
	 */
	public IndiceFinanceiro getIndiceFinanceiro();

	/**
	 * Gets the data cadastro.
	 *
	 * @return the data cadastro
	 */
	public Date getDataCadastro();

	/**
	 * Sets the data cadastro.
	 *
	 * @param dataCadastro the new data cadastro
	 */
	public void setDataCadastro(Date dataCadastro);

	/**
	 * Sets the data inicio vigencia.
	 *
	 * @param inicioVigencia the new data inicio vigencia
	 */
	public void setDataInicioVigencia(Date inicioVigencia);

	/**
	 * Gets the data inicio vigencia.
	 *
	 * @return the data inicio vigencia
	 */
	public Date getDataInicioVigencia();

}
