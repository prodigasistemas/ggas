/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/03/2015 09:37:11
 @author crsilva
 */

package br.com.ggas.contrato.contratoadequacaoparceria.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contratoadequacaoparceria.ContratoAdequacaoParceria;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pelos atributos e implementação de métodos relacionados
 * a Parceria na Adequação de Contrato. 
 *
 */
public class ContratoAdequacaoParceriaImpl extends EntidadeNegocioImpl implements ContratoAdequacaoParceria {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7562301088216863520L;

	private Contrato contrato;

	private Integer prazo;

	private BigDecimal valorEmpresa;

	private BigDecimal valorCliente;

	private String numeroContratoPrincipal;

	private String clienteAssinatura;

	private String valorPagoPelaCdl;

	private String enderecoCliente;

	private String emailCliente;

	private String enderecoImovel;

	private BigDecimal percentualMulta;

	private PontoConsumo pontoConsumo;

	private IndiceFinanceiro indiceFinanceiro;

	private Date dataCadastro;

	private Date dataInicioVigencia;

	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	@Override
	public BigDecimal getPercentualMulta() {

		return percentualMulta;
	}

	@Override
	public void setPercentualMulta(BigDecimal percentualMulta) {

		this.percentualMulta = percentualMulta;
	}

	public String getEnderecoImovel() {

		return enderecoImovel;
	}

	public void setEnderecoImovel(String enderecoImovel) {

		this.enderecoImovel = enderecoImovel;
	}

	public String getEmailCliente() {

		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {

		this.emailCliente = emailCliente;
	}

	public String getEnderecoCliente() {

		return enderecoCliente;
	}

	public void setEnderecoCliente(String enderecoCliente) {

		this.enderecoCliente = enderecoCliente;
	}

	public String getClienteAssinatura() {

		return clienteAssinatura;
	}

	public void setClienteAssinatura(String clienteAssinatura) {

		this.clienteAssinatura = clienteAssinatura;
	}

	public String getValorPagoPelaCdl() {

		return valorPagoPelaCdl;
	}

	public void setValorPagoPelaCdl(String valorPagoPelaCdl) {

		this.valorPagoPelaCdl = valorPagoPelaCdl;
	}

	@Override
	public Contrato getContrato() {

		return contrato;
	}

	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	@Override
	public Integer getPrazo() {

		return prazo;
	}

	@Override
	public void setPrazo(Integer prazo) {

		this.prazo = prazo;
	}

	@Override
	public BigDecimal getValorEmpresa() {

		return valorEmpresa;
	}

	@Override
	public void setValorEmpresa(BigDecimal valorEmpresa) {

		this.valorEmpresa = valorEmpresa;
	}

	@Override
	public BigDecimal getValorCliente() {

		return valorCliente;
	}

	@Override
	public void setValorCliente(BigDecimal valorCliente) {

		this.valorCliente = valorCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	public String getNumeroContratoPrincipal() {

		return numeroContratoPrincipal;
	}

	public void setNumeroContratoPrincipal(String numeroContratoPrincipal) {

		this.numeroContratoPrincipal = numeroContratoPrincipal;
	}

	/**
	 * Gets the valor pago pelo cliente.
	 * 
	 * @return the valor pago pelo cliente
	 */
	public BigDecimal getvalorPagoPeloCliente() {

		return this.valorCliente;
	}

	@Override
	public void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro) {

		this.indiceFinanceiro = indiceFinanceiro;

	}

	@Override
	public IndiceFinanceiro getIndiceFinanceiro() {

		return this.indiceFinanceiro;
	}

	@Override
	public Date getDataCadastro() {

		Date data = null;
		if (this.dataCadastro != null) {
			data = (Date) this.dataCadastro.clone();
		}
		return data;
	}

	@Override
	public void setDataCadastro(Date dataCadastro) {
		if (dataCadastro != null) {
			this.dataCadastro = (Date) dataCadastro.clone();
		} else {
			this.dataCadastro = null;
		}
	}

	@Override
	public void setDataInicioVigencia(Date inicioVigencia) {
		if (inicioVigencia != null) {
			this.dataInicioVigencia = (Date) inicioVigencia.clone();
		} else {
			this.dataInicioVigencia = null;
		}
	}

	@Override
	public Date getDataInicioVigencia() {
		Date data = null;
		if (this.dataInicioVigencia != null) {
			data = (Date) this.dataInicioVigencia.clone();
		}
		return data;
	}

}
