/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/03/2015 16:17:55
 @author crsilva
 */

package br.com.ggas.contrato.contratoadequacaoparceria;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contratoadequacaoparceria.impl.ContratoAdequacaoParceriaImpl;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Map;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador
 * da Adequação de Parceria vinculada ao Contrato.  
 *
 */
public interface ControladorContratoAdequacaoParceria extends ControladorNegocio {

	String BEAN_ID_CONTRATO_ADEQUACAO_PARCERIA = "controladorContratoAdequacaoParceria";

	String ERRO_NUMERO_CONTRATO = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_NUMERO_CONTRATO";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CDL = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CDL";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CLIENTE = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CLIENTE";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PRAZO_ESTABELECIDO = "ERRO_PCONTRATO_ADEQUACAO_PARCERIA_RAZO_ESTABELECIDO";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_CLIENTE = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_CLIENTE";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_IMOVEL = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_IMOVEL";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PERCENTUAL_MULTA = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_PERCENTUAL_MULTA";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_JA_ASSOCIADO = "ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_JA_ASSOCIADO";

	String ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_NAO_PERTENCE_AO_CONTRATO = 
					"ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_NAO_PERTENCE_AO_CONTRATO";

	/**
	 * Remover contrato adequacao parceria.
	 * 
	 * @param contratoAdequacaoParceria
	 *            the contrato adequacao parceria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerContratoAdequacaoParceria(ContratoAdequacaoParceria contratoAdequacaoParceria) throws NegocioException;

	/**
	 * Consultar contratos adequacao parceria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ContratoAdequacaoParceria> consultarContratosAdequacaoParceria(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar contrato adequacao parceria por chave.
	 * 
	 * @param chave
	 *            the chave
	 * @return the contrato adequacao parceria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContratoAdequacaoParceria consultarContratoAdequacaoParceriaPorChave(Long chave) throws NegocioException;

	/**
	 * Atualizar contrato adequacao perceria.
	 * 
	 * @param contratoAdequacaoParceria
	 *            the contrato adequacao parceria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarContratoAdequacaoPerceria(ContratoAdequacaoParceria contratoAdequacaoParceria) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Validar dados inclusao contrato adequacao parceria.
	 * 
	  * @param contratoAdequacaoParceria {@link ContratoAdequacaoParceriaImpl}
	  * @param request  {@link HttpServletRequest}
	  * @param isInclusao  {@link boolean}
	  * @param exists  {@link boolean}
	  * @param contratoSelecionado  {@link Contrato}
	  * @param pontoConsumo  {@link PontoConsumo}
	  * @param isEmptyIdPontoConsumo  {@link boolean}
	  * @throws NegocioException  {@link NegocioException}
	  */
	void validarDadosInclusaoContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria, HttpServletRequest request,
			boolean isInclusao, boolean exists, Contrato contratoSelecionado, PontoConsumo pontoConsumo,
			boolean isEmptyIdPontoConsumo) throws NegocioException;

	/**
	 * Checks if is exists ponto consumo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ContratoAdequacaoParceria> isExistsPontoConsumo(Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Consultar contrato adequacao parceria por ponto consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the contrato adequacao parceria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContratoAdequacaoParceria consultarContratoAdequacaoParceriaPorPontoConsumo(Long idPontoConsumo) throws NegocioException;

}
