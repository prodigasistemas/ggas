/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/03/2015 09:15:45
 @author crsilva
 */

package br.com.ggas.contrato.contratoadequacaoparceria.impl;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contratoadequacaoparceria.ContratoAdequacaoParceria;
import br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pelos métodos que manipulam a adequação de parceria
 * vinculada ao contrato. 
 * 
 */
public class ControladorContratoAdequacaoParceriaImpl extends ControladorNegocioImpl implements ControladorContratoAdequacaoParceria {

	private static final String PONTO_CONSUMO = "pontoConsumo";
	private static final int LIMITE_NUMERO_CONTRATO = 9;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public ContratoAdequacaoParceria criar() {

		return (ContratoAdequacaoParceria) ServiceLocator.getInstancia().getBeanPorID(
						ContratoAdequacaoParceria.BEAN_ID_CONTRATO_ADEQUACAO_PARCERIA);
	}

	public Class<?> getClasseEntidadeContratoAdequacao() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoAdequacaoParceria.BEAN_ID_CONTRATO_ADEQUACAO_PARCERIA);
	}

	/*
	 * Método responável pela consulta
	 * dos contratos adequações parcerias
	 * existentes de acordo com a condição
	 * passada como paramêtros
	 */

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria#consultarContratosAdequacaoParceria(java.util
	 * .Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ContratoAdequacaoParceria> consultarContratosAdequacaoParceria(
					Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContratoAdequacao());
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
		criteria.createAlias("pontoConsumo.imovel", "imovel");
		criteria.createAlias("contrato", "contrato");

		if (filtro != null) {
			Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
			if (idPontoConsumo != null && idPontoConsumo > 0) {
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
			}

			Long idImovel = (Long) filtro.get("idImovel");
			if (idImovel != null && idImovel > 0) {
				criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
			}

			if (filtro.get("idCliente") != null) {
				criteria.createCriteria("imovel.listaClienteImovel").createCriteria("cliente")
								.add(Restrictions.eq("chavePrimaria", filtro.get("idCliente")));
			}

			String numeroContrato = (String) filtro.get("numeroContrato");
			if (numeroContrato != null && !numeroContrato.isEmpty()) {
				criteria.add(Restrictions.like("contrato.numeroCompletoContrato", "%" + numeroContrato + "%"));

			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

		}

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria#consultarContratoAdequacaoParceriaPorPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	public ContratoAdequacaoParceria consultarContratoAdequacaoParceriaPorPontoConsumo(Long idPontoConsumo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContratoAdequacao());
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);

		criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));

		return (ContratoAdequacaoParceria) criteria.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria#atualizarContratoAdequacaoPerceria(br.com.ggas
	 * .contrato.contratoadequacaoparceria.ContratoAdequacaoParceria)
	 */
	@Override
	public void atualizarContratoAdequacaoPerceria(ContratoAdequacaoParceria contratoAdequacaoParceria) throws NegocioException,
					ConcorrenciaException {

		this.atualizar(contratoAdequacaoParceria, getClasseEntidadeContratoAdequacao());

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria#removerContratoAdequacaoParceria(br.com.ggas.
	 * contrato.contratoadequacaoparceria.ContratoAdequacaoParceria)
	 */
	@Override
	public void removerContratoAdequacaoParceria(ContratoAdequacaoParceria contratoAdequacaoParceria) throws NegocioException {

		super.remover(contratoAdequacaoParceria, false);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria#consultarContratoAdequacaoParceriaPorChave(java
	 * .lang.Long)
	 */
	@Override
	public ContratoAdequacaoParceria consultarContratoAdequacaoParceriaPorChave(Long chavePrimaria) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContratoAdequacao());

	/**
	 	criteria.createAlias("pontoConsumo", "pontoConsumo");
		criteria.createAlias("pontoConsumo.imovel", "imovel");
		criteria.createCriteria("imovel.listaClienteImovel").createCriteria("cliente");
		criteria.createAlias("contrato", "contrato");
	 */

		criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));

		return (ContratoAdequacaoParceria) criteria.uniqueResult();

	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	@Override
	public void validarDadosInclusaoContratoAdequacaoParceria(ContratoAdequacaoParceriaImpl contratoAdequacaoParceria,
			HttpServletRequest request, boolean isInclusao, boolean existsPontoConsumo, Contrato contratoSelecionado,
			PontoConsumo pontoConsumo, boolean isEmptyIdConsumo) throws NegocioException {

		String numeroContrato = contratoAdequacaoParceria.getNumeroContratoPrincipal();
		String valorPagoCdl = contratoAdequacaoParceria.getValorPagoPelaCdl();
		String prazoEstabelecido = String.valueOf(contratoAdequacaoParceria.getPrazo());
		String percentualMulta = String.valueOf(contratoAdequacaoParceria.getPercentualMulta());
		String chavePontoConsumo = null;
		Long indiceFinanceiro = null;
		String dataInicioVigencia = String.valueOf(contratoAdequacaoParceria.getDataInicioVigencia());
		if (contratoAdequacaoParceria.getIndiceFinanceiro() != null) {
			indiceFinanceiro = contratoAdequacaoParceria.getIndiceFinanceiro().getChavePrimaria();
		}

		if (isInclusao) {
			if(request.getParameter("checkPontoConsumo") != null) {
				chavePontoConsumo = String.valueOf(request.getParameter("checkPontoConsumo"));
			}
			
			if (StringUtils.isEmpty(numeroContrato)) {
				throw new NegocioException(ControladorContratoAdequacaoParceria.ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO, true);
			}
			if (StringUtils.isEmpty(numeroContrato) || numeroContrato.length() < LIMITE_NUMERO_CONTRATO) {
				throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_NUMERO_CONTRATO, true);
			}

			if (isEmptyIdConsumo) {
				throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_SELECIONE_UM_PONTO_DE_CONSUMO, true);
			}

			if (chavePontoConsumo != null) {
				Collection<ContratoAdequacaoParceria> lista = this.isExistsPontoConsumo(Long.parseLong(chavePontoConsumo));

				if (!lista.isEmpty()) {
					ContratoAdequacaoParceria contratoParceria = lista.iterator().next();
					if (contratoParceria.isHabilitado()) {
						throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_JA_ASSOCIADO, true);
					}
				}
			}

		}

		if (valorPagoCdl == null || valorPagoCdl.length() <= 0) {
			throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_VALOR_CDL, true);
		}

		if ("null".equals(prazoEstabelecido) || prazoEstabelecido.length() <= 0) {
			throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_PRAZO_ESTABELECIDO, true);
		}

		if ("null".equals(percentualMulta) || percentualMulta.length() <= 0) {
			throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_PERCENTUAL_MULTA, true);
		}

		if (contratoSelecionado != null && chavePontoConsumo != null) {
			Collection<ContratoPontoConsumo> contratospConsumos = contratoSelecionado.getListaContratoPontoConsumo();
			boolean pertenceAoContrato = false;
			for (ContratoPontoConsumo cpc : contratospConsumos) {
				if ((cpc.getPontoConsumo().getChavePrimaria()) == Long.parseLong(chavePontoConsumo)) {
					pertenceAoContrato = true;

				}
			}
			if (!pertenceAoContrato) {
				throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_PONTO_CONSUMO_NAO_PERTENCE_AO_CONTRATO, true);
			}
		}

		if ("null".equals(dataInicioVigencia) || dataInicioVigencia.length() <= 0) {
			throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_DATA_INICIO_VIGENCIA, true);
		}

		if (indiceFinanceiro == null || indiceFinanceiro <= 0) {
			throw new NegocioException(Constantes.ERRO_CONTRATO_ADEQUACAO_PARCERIA_INDICE_CORRECAO_MONETARIA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contratoadequacaoparceria.ControladorContratoAdequacaoParceria#isExistsPontoConsumo(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ContratoAdequacaoParceria> isExistsPontoConsumo(Long chavePrimariaPontoConsumo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContratoAdequacao());
		if (chavePrimariaPontoConsumo != null && chavePrimariaPontoConsumo > 0) {
			criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", chavePrimariaPontoConsumo));
		}

		return criteria.list();

	}

}
