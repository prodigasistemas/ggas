/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.proposta;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.web.contrato.proposta.FiltroProposta;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioAcompanhamentoClientesVO;
import br.com.ggas.webservice.PropostaTO;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Proposta. 
 * 
 *
 */
public interface ControladorProposta extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PROPOSTA = "controladorProposta";

	String ERRO_NEGOCIO_PROPOSTA_EXISTENTE = "ERRO_NEGOCIO_PROPOSTA_EXISTENTE";

	String ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA = "ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA";

	String ERRO_NEGOCIO_DATA_ENTREGA_INVALIDA = "ERRO_NEGOCIO_DATA_ENTREGA_INVALIDA";

	String ERRO_NEGOCIO_DATA_VIGENCIA_ENTREGA = "ERRO_NEGOCIO_DATA_VIGENCIA_ENTREGA";

	String ERRO_NEGOCIO_UNIDADE_CONSUMO_OBRIGATORIO = "ERRO_NEGOCIO_UNIDADE_CONSUMO_OBRIGATORIO";

	String ERRO_NEGOCIO_PERCENTUAL_INVALIDO = "ERRO_NEGOCIO_PERCENTUAL_INVALIDO";

	String ERRO_NEGOCIO_DATA_EMISSAO_MAIOR_QUE_DATA_ATUAL = "ERRO_NEGOCIO_DATA_EMISSAO_MAIOR_QUE_DATA_ATUAL";

	String ERRO_DATA_INVALIDA_ANO_MES_REFERENCIA_PRECO = "ERRO_DATA_INVALIDA_ANO_MES_REFERENCIA_PRECO";

	String ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_INVESTIMENTO = "ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_INVESTIMENTO";

	String ERRO_NEGOCIO_PROPOSTA_NAO_EXISTENTE = "ERRO_NEGOCIO_PROPOSTA_NAO_EXISTENTE";

	String ERRO_NEGOCIO_PROPOSTA_NAO_APROVADA = "ERRO_NEGOCIO_PROPOSTA_NAO_APROVADA";

	String ERRO_NEGOCIO_MAXIMO_CAMPO_COMENTARIO = "ERRO_NEGOCIO_MAXIMO_CAMPO_COMENTARIO";

	String ERRO_NEGOCIO_IMPORTACAO_PLANILHA = "ERRO_NEGOCIO_IMPORTACAO_PLANILHA";

	String ERRO_NEGOCIO_REMOVER_PROPOSTA = "ERRO_NEGOCIO_REMOVER_PROPOSTA";

	String ERRO_NEGOCIO_VALOR_NEGATIVO = "ERRO_NEGOCIO_VALOR_NEGATIVO";

	int TAMANHO_MAXIMO_CAMPO_COMENTARIO = 200;

	/**
	 * Criar tarifa.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTarifa();

	/**
	 * Criar situacao proposta.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSituacaoProposta();

	/**
	 * Método responsável por listar as situações
	 * de proposta.
	 * 
	 * @return Uma coleção de situações de
	 *         proposta.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoProposta> listarSituacaoProposta() throws NegocioException;
	
	/**
	 * Método responsável por listar as situações
	 * de proposta passíveis de uso por uma nova proposta.
	 *
	 * @return Uma coleção de situações de
	 *         proposta.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoProposta> listarSituacoesIniciaisProposta() throws GGASException;

	/**
	 * Método responsável por obter uma situação
	 * de proposta.
	 * 
	 * @param chavePrimaria
	 *            Chave primária da situação de
	 *            proposta.
	 * @return Uma situação de proposta.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	SituacaoProposta obterSituacaoProposta(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma tarifa de
	 * proposta.
	 * 
	 * @param chavePrimaria
	 *            Chave primária da tarifa.
	 * @return Uma tarifa de proposta.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Tarifa obterTarifa(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por consultar
	 * proposta(s).
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de proposta
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Proposta> consultarPropostas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * proposta.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias das
	 *            propostas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverPropostas(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por importar os dados de
	 * um arquivo excel.
	 * 
	 * @param bytes
	 *            O arquivo que será importado
	 * @param tipoArquivo
	 *            O tipo do arquivo
	 * @param nomeArquivo
	 *            the nome arquivo
	 * @return Um mapa contendo os valores da
	 *         proposta
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Map<String, Object> importarDadosPropostaExcel(byte[] bytes, String tipoArquivo, String nomeArquivo) throws GGASException;

	/**
	 * Método responsável por listar as diferentes
	 * versões aprovadas de proposta por um número
	 * informado.
	 * 
	 * @param numeroProposta
	 *            Um número de proposta.
	 * @return Uma coleção de propostas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Proposta> listarPropostasAprovadasPorNumero(String numeroProposta) throws NegocioException;

	/**
	 * Método responsável por obter a quantidade
	 * de propostas aprovadas pelo número da
	 * proposta.
	 * 
	 * @param numeroProposta
	 *            Número da proposta.
	 * @return Quantidade de propostas aprovadas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer obterQtdPropostasAprovadasPorNumero(String numeroProposta) throws NegocioException;

	/**
	 * Método responsável por validar a quantidade
	 * de propostas aprovadas.
	 * 
	 * @param numeroProposta
	 *            Número da proposta
	 * @param propostaAprovada
	 * 			Indicador de proposta aprovada
	 * @throws NegocioException
	 *             Caso a quantidade de propostas
	 *             aprovadas seja nula ou igual a
	 *             zero.
	 */
	void validarPropostasContrato(String numeroProposta, Boolean propostaAprovada) throws NegocioException;

	/**
	 * Método responsável por verificar o tamanho
	 * máximo do comentário do campo Solicitações
	 * adicionais.
	 * 
	 * @param comentario
	 *            O comentário a ser validado.
	 * @throws NegocioException
	 *             Caso o comentário tenha mais de
	 *             200 caracteres.
	 */
	void validarTamanhoComentario(String comentario) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * periodicidades de juros do tipo Entidade
	 * Conteudo do sistema.
	 * 
	 * @param chavePrimaria
	 *            da entidade classe
	 * @return uma colecao de entidade conteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> consultarPeriodicidadeJuros(Long chavePrimaria) throws NegocioException;

	/**
	 * Gerar relatorio acompanhamento clientes.
	 * 
	 * @param pesquisaRelatorioAcompanhamentoClientesVO
	 *            the pesquisa relatorio acompanhamento clientes vo
	 * @param formatoImpressao
	 *            the formato impressao
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public byte[] gerarRelatorioAcompanhamentoClientes(PesquisaRelatorioAcompanhamentoClientesVO pesquisaRelatorioAcompanhamentoClientesVO,
					FormatoImpressao formatoImpressao) throws NegocioException;

	/**
	 * Gerar proposta pdf.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public byte[] gerarPropostaPDF(Map<String, Object> parametros) throws NegocioException;

	/**
	 * Enviar proposta por email.
	 * 
	 * @param proposta
	 *            the proposta
	 * @param bytes
	 *            the bytes
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void enviarPropostaPorEmail(Proposta proposta, byte[] bytes) throws GGASException;

	/**
	 * Obter situacao proposta por descricao.
	 * 
	 * @author crsilva
	 *         Método responsável por obter
	 *         situação proposta pela descrição
	 * @param descricao
	 *            the descricao
	 * @return SituacaoProposta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public SituacaoProposta obterSituacaoPropostaPorDescricao(String descricao) throws NegocioException;

	/**
	 * Calcular valores proposta.
	 * 
	 * @author crsilva
	 *         Método responsável por calcular valores vindos
	 *         do levantamento de mercado e imprimir na tela
	 *         de inclusão de proposta
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @return HashMap de valores calculados
	 */
	public Map<String, BigDecimal> calcularValoresProposta(LevantamentoMercado levantamentoMercado);

	/**
	 * Inserir numero ano proposta.
	 * 
	 * @param proposta
	 *            the proposta
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void inserirNumeroAnoProposta(Proposta proposta) throws ConcorrenciaException, NegocioException;

	/**
	 * Converter proposta em proposta to.
	 * 
	 * @param proposta
	 *            the proposta
	 * @return {@link PropostaTO}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public PropostaTO converterPropostaEmPropostaTO(Proposta proposta) throws GGASException;

	/**
	 * Converter proposta t oem proposta.
	 * 
	 * @param propostaTO
	 *            the proposta to
	 * @return {@link Proposta}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Proposta converterPropostaTOemProposta(PropostaTO propostaTO) throws GGASException;

	/**
	 * Validar consumos medios.
	 * 
	 * @param proposta
	 *            the proposta
	 * @param levantamentoMercado
	 * 			  O levantamento de mercado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarConsumosMedios(Proposta proposta, LevantamentoMercado levantamentoMercado) throws NegocioException;

	/**
	 * Método responsável por consultar proposta(s).
	 * 
	 * @param filtro O filtro com os parâmetros para a pesquisa.
	 * @return Uma coleção de proposta
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Proposta> consultarPropostas(FiltroProposta filtro) throws NegocioException;

}
