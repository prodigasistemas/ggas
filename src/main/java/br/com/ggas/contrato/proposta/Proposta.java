/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.proposta;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.proposta.impl.PropostaImpl;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Interface responsável pela assinatura dos métodos relacionados a Proposta. 
 * 
 *
 */
public interface Proposta extends EntidadeNegocio {

	String BEAN_ID_PROPOSTA = "proposta";

	String NUMERO_PROPOSTA = "PROPOSTA_NUMERO_PROPOSTA";

	String VERSAO_PROPOSTA = "PROPOSTA_VERSAO_PROPOSTA";

	String DATA_EMISSAO = "PROPOSTA_DATA_EMISSAO";

	String DATA_VIGENCIA = "PROPOSTA_DATA_VIGENCIA";

	String DATA_ENTREGA = "PROPOSTA_DATA_ENTREGA";

	String PERCENTUAL_TIR = "PROPOSTA_PERCENTUAL_TIR";

	String VALOR_MATERIAL = "PROPOSTA_VALOR_MATERIAL";

	String VALOR_MAO_DE_OBRA = "PROPOSTA_VALOR_MAO_DE_OBRA";

	String VALOR_INVESTIMENTO = "PROPOSTA_VALOR_INVESTIMENTO";

	String INDICADOR_PARTICIPACAO = "PROPOSTA_INDICADOR_PARTICIPACAO";

	String PERCENTUAL_CLIENTE = "PROPOSTA_PERCENTUAL_CLIENTE";

	String PERIODICIDADE_JUROS = "PROPOSTA_PERIODICIDADE_JUROS";

	String VALOR_CLIENTE = "PROPOSTA_VALOR_CLIENTE";

	String QUANTIDADE_PARCELA = "PROPOSTA_QUANTIDADE_PARCELA";

	String VALOR_PARCELA = "PROPOSTA_VALOR_PARCELA";

	String PERCENTUAL_JUROS = "PROPOSTA_PERCENTUAL_JUROS";

	String CONSUMO_MEDIO_ANUAL = "PROPOSTA_CONSUMO_MEDIO_ANUAL";

	String CONSUMO_MEDIO_MENSAL = "PROPOSTA_CONSUMO_MEDIO_MENSAL";

	String CONSUMO_NORMALIZADO_EQUIVALENTE = "PROPOSTA_CONSUMO_NORMALIZADO_EQUIVALENTE";

	String CONSUMO_UNIDADE_CONSUMIDORA = "PROPOSTA_CONSUMO_UNIDADE_CONSUMIDORA";

	String ANO_MES_REFERENCIA_PRECO = "PROPOSTA_ANO_MES_REFERENCIA_PRECO";

	String VALOR_PRECO_PAGO = "PROPOSTA_VALOR_PRECO_PAGO";

	String VALOR_GASTO_MENSAL = "PROPOSTA_VALOR_GASTO_MENSAL";

	String SITUACAO_PROPOSTA = "PROPOSTA_SITUACAO_PROPOSTA";

	String IMOVEL = "PROPOSTA_IMOVEL";

	String TARIFA = "PROPOSTA_TARIFA";

	String PROPOSTA_CAMPO_STRING = "PROPOSTA";

	String UNIDADE_CONSUMO_ANUAL = "PROPOSTA_UNIDADE_CONSUMO_ANUAL";

	String UNIDADE_CONSUMO_MENSAL = "PROPOSTA_UNIDADE_CONSUMO_MENSAL";

	String PROPOSTA_ARQUIVO_SELECAO_INVALIDA = "PROPOSTA_ARQUIVO_SELECAO_INVALIDA";

	String ECONOMIA_MENSAL_GN = "PROPOSTA_ECONOMIA_MENSAL_GN";

	String ECONOMIA_ANUAL_GN = "PROPOSTA_ECONOMIA_ANUAL_GN";

	String PERCENTUAL_ECONOMIA = "PROPOSTA_PERCENTUAL_ECONOMIA";

	String DATA_APRESENTACAO_ASSEMBLEIA = "PROPOSTA_DATA_ASSEMBLEIA_CONDOMINIO";

	String DATA_ELEICAO_SINDICO = "PROPOSTA_DATA_ELEICAO_SINDICO";

	String SOLICITACOES_ADICIONAIS = "PROPOSTA_SOLICITACOES_ADICIONAIS";
	
	String CLIENTE = "PROPOSTA_CLIENTE";

	/**
	 * @return the valorParticipacaoCDL
	 */
	BigDecimal getValorParticipacaoCDL();

	/**
	 * @param valorParticipacaoCDL
	 *            the valorParticipacaoCDL to set
	 */
	void setValorParticipacaoCDL(BigDecimal valorParticipacaoCDL);

	/**
	 * @return the valorMensalGastoGN
	 */
	BigDecimal getValorMensalGastoGN();

	/**
	 * @param valorMensalGastoGN
	 *            the valorMensalGastoGN to set
	 */
	void setValorMensalGastoGN(BigDecimal valorMensalGastoGN);


	/**
	 * @return the indicadorMedicao
	 */
	Boolean getIndicadorMedicao();

	/**
	 * @param indicadorMedicao
	 *            the indicadorMedicao to set
	 */
	void setIndicadorMedicao(Boolean indicadorMedicao);

	/**
	 * @return the quantidadeApartamentos
	 */
	Integer getQuantidadeApartamentos();

	/**
	 * @param quantidadeApartamentos
	 *            the quantidadeApartamentos to
	 *            set
	 */
	void setQuantidadeApartamentos(Integer quantidadeApartamentos);

	/**
	 * @return the consumoMedioMensalGN
	 */
	BigDecimal getConsumoMedioMensalGN();

	/**
	 * @param consumoMedioMensalGN
	 *            the consumoMedioMensalGN to set
	 */
	void setConsumoMedioMensalGN(BigDecimal consumoMedioMensalGN);

	/**
	 * @return the consumoMedioAnualGN
	 */
	BigDecimal getConsumoMedioAnualGN();

	/**
	 * @param consumoMedioAnualGN
	 *            the consumoMedioAnualGN to set
	 */
	void setConsumoMedioAnualGN(BigDecimal consumoMedioAnualGN);

	/**
	 * @return the volumeEstimadoGN
	 */
	BigDecimal getVolumeDiarioEstimadoGN();

	/**
	 * @param volumeDiarioEstimadoGN
	 *            the volumeDiarioEstimadoGN to set
	 */
	void setVolumeDiarioEstimadoGN(BigDecimal volumeDiarioEstimadoGN);

	/**
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return the periodicidadeJuros
	 */
	EntidadeConteudo getPeriodicidadeJuros();

	/**
	 * @param periodicidadeJuros
	 *            the periodicidadeJuros to set
	 */
	void setPeriodicidadeJuros(EntidadeConteudo periodicidadeJuros);

	/**
	 * @return the numeroProposta
	 */
	String getNumeroProposta();

	/**
	 * @param numeroProposta
	 *            the numeroProposta to set
	 */
	void setNumeroProposta(String numeroProposta);

	/**
	 * @return the versaoProposta
	 */
	Integer getVersaoProposta();

	/**
	 * @param versaoProposta
	 *            the versaoProposta to set
	 */
	void setVersaoProposta(Integer versaoProposta);

	/**
	 * @return the dataEmissao
	 */
	Date getDataEmissao();

	/**
	 * @param dataEmissao
	 *            the dataEmissao to set
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * @return the dataVigencia
	 */
	Date getDataVigencia();

	/**
	 * @param dataVigencia
	 *            the dataVigencia to set
	 */
	void setDataVigencia(Date dataVigencia);

	/**
	 * @return the dataEntrega
	 */
	Date getDataEntrega();

	/**
	 * @param dataEntrega
	 *            the dataEntrega to set
	 */
	void setDataEntrega(Date dataEntrega);

	/**
	 * @return the percentualTIR
	 */
	BigDecimal getPercentualTIR();

	/**
	 * @param percentualTIR
	 *            the percentualTIR to set
	 */
	void setPercentualTIR(BigDecimal percentualTIR);

	/**
	 * @return the valorMaterial
	 */
	BigDecimal getValorMaterial();

	/**
	 * @param valorMaterial
	 *            the valorMaterial to set
	 */
	void setValorMaterial(BigDecimal valorMaterial);

	/**
	 * @return the valorMaoDeObra
	 */
	BigDecimal getValorMaoDeObra();

	/**
	 * @param valorMaoDeObra
	 *            the valorMaoDeObra to set
	 */
	void setValorMaoDeObra(BigDecimal valorMaoDeObra);

	/**
	 * @return the valorInvestimento
	 */
	BigDecimal getValorInvestimento();

	/**
	 * @param valorInvestimento
	 *            the valorInvestimento to set
	 */
	void setValorInvestimento(BigDecimal valorInvestimento);

	/**
	 * @return the indicadorParticipacao
	 */
	boolean isIndicadorParticipacao();

	/**
	 * @param indicadorParticipacao
	 *            the indicadorParticipacao to set
	 */
	void setIndicadorParticipacao(boolean indicadorParticipacao);

	/**
	 * @return the percentualCliente
	 */
	BigDecimal getPercentualCliente();

	/**
	 * @param percentualCliente
	 *            the percentualCliente to set
	 */
	void setPercentualCliente(BigDecimal percentualCliente);

	/**
	 * @return the valorCliente
	 */
	BigDecimal getValorCliente();

	/**
	 * @param valorCliente
	 *            the valorCliente to set
	 */
	void setValorCliente(BigDecimal valorCliente);

	/**
	 * @return the quantidadeParcela
	 */
	Integer getQuantidadeParcela();

	/**
	 * @param quantidadeParcela
	 *            the quantidadeParcela to set
	 */
	void setQuantidadeParcela(Integer quantidadeParcela);

	/**
	 * @return the valorParcela
	 */
	BigDecimal getValorParcela();

	/**
	 * @param valorParcela
	 *            the valorParcela to set
	 */
	void setValorParcela(BigDecimal valorParcela);

	/**
	 * @return the percentualJuros
	 */
	BigDecimal getPercentualJuros();

	/**
	 * @param percentualJuros
	 *            the percentualJuros to set
	 */
	void setPercentualJuros(BigDecimal percentualJuros);

	/**
	 * @return the consumoMedioAnual
	 */
	BigDecimal getConsumoMedioAnual();

	/**
	 * @param consumoMedioAnual
	 *            the consumoMedioAnual to set
	 */
	void setConsumoMedioAnual(BigDecimal consumoMedioAnual);

	/**
	 * @return the consumoMedioMensal
	 */
	BigDecimal getConsumoMedioMensal();

	/**
	 * @param consumoMedioMensal
	 *            the consumoMedioMensal to set
	 */
	void setConsumoMedioMensal(BigDecimal consumoMedioMensal);

	/**
	 * @return the consumoUnidadeConsumidora
	 */
	BigDecimal getConsumoUnidadeConsumidora();

	/**
	 * @param consumoUnidadeConsumidora
	 *            the consumoUnidadeConsumidora to
	 *            set
	 */
	void setConsumoUnidadeConsumidora(BigDecimal consumoUnidadeConsumidora);

	/**
	 * @return the anoMesReferenciaPreco
	 */
	Integer getAnoMesReferenciaPreco();

	/**
	 * @param anoMesReferenciaPreco
	 *            the anoMesReferenciaPreco to set
	 */
	void setAnoMesReferenciaPreco(Integer anoMesReferenciaPreco);

	/**
	 * @return the valorGastoMensal
	 */
	BigDecimal getValorGastoMensal();

	/**
	 * @param valorGastoMensal
	 *            the valorGastoMensal to set
	 */
	void setValorGastoMensal(BigDecimal valorGastoMensal);

	/**
	 * @return the situacaoProposta
	 */
	SituacaoProposta getSituacaoProposta();

	/**
	 * @param situacaoProposta
	 *            the situacaoProposta to set
	 */
	void setSituacaoProposta(SituacaoProposta situacaoProposta);

	/**
	 * @return the unidadeConsumoAnual
	 */
	Unidade getUnidadeConsumoAnual();

	/**
	 * @param unidadeConsumoAnual
	 *            the unidadeConsumoAnual to set
	 */
	void setUnidadeConsumoAnual(Unidade unidadeConsumoAnual);

	/**
	 * @return the unidadeConsumoMensal
	 */
	Unidade getUnidadeConsumoMensal();

	/**
	 * @param unidadeConsumoMensal
	 *            the unidadeConsumoMensal to set
	 */
	void setUnidadeConsumoMensal(Unidade unidadeConsumoMensal);

	/**
	 * @return the planilha
	 */
	byte[] getPlanilha();

	/**
	 * @param planilha
	 *            the planilha to set
	 */
	void setPlanilha(byte[] planilha);

	/**
	 * @return the nomePlanilha
	 */
	String getNomePlanilha();

	/**
	 * @param nomePlanilha
	 *            the nomePlanilha to set
	 */
	void setNomePlanilha(String nomePlanilha);

	/**
	 * @return the tipoPlanilha
	 */
	String getTipoPlanilha();

	/**
	 * @param tipoPlanilha
	 *            the tipoPlanilha to set
	 */
	void setTipoPlanilha(String tipoPlanilha);

	/**
	 * @return the imovel
	 */
	Imovel getImovel();

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	void setImovel(Imovel imovel);

	/**
	 * @return the economiaMensalGN
	 */
	BigDecimal getEconomiaMensalGN();

	/**
	 * @param economiaMensalGN
	 *            the economiaMensalGN to set
	 */
	void setEconomiaMensalGN(BigDecimal economiaMensalGN);

	/**
	 * @return the economiaAnualGN
	 */
	BigDecimal getEconomiaAnualGN();

	/**
	 * @param economiaAnualGN
	 *            the economiaAnualGN to set
	 */
	void setEconomiaAnualGN(BigDecimal economiaAnualGN);

	/**
	 * @return the percentualEconomia
	 */
	BigDecimal getPercentualEconomia();

	/**
	 * @param percentualEconomia
	 *            the percentualEconomia to set
	 */
	void setPercentualEconomia(BigDecimal percentualEconomia);

	/**
	 * @return the comentario
	 */
	String getComentario();

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	void setComentario(String comentario);

	/**
	 * @return the dataApresentacaoCondominio
	 */
	Date getDataApresentacaoCondominio();

	/**
	 * @param dataApresentacaoCondominio
	 *            the dataApresentacaoCondominio
	 *            to set
	 */
	void setDataApresentacaoCondominio(Date dataApresentacaoCondominio);

	/**
	 * @return the dataEleicaoSindico
	 */
	Date getDataEleicaoSindico();

	/**
	 * @param dataEleicaoSindico
	 *            the dataEleicaoSindico to set
	 */
	void setDataEleicaoSindico(Date dataEleicaoSindico);

	/**
	 * @return Funcionario - Retorna funcionário.
	 */
	Funcionario getFiscal();

	/**
	 * @param fiscal - Set fiscal.
	 */
	void setFiscal(Funcionario fiscal);

	/**
	 * @return Funcionario - retorna um funcionário.
	 */
	Funcionario getVendedor();

	/**
	 * @param vendedor - Set vendedor.
	 */
	void setVendedor(Funcionario vendedor);

	/**
	 * @author Carlos Romano
	 *         get modalidade de uso
	 * @see PropostaImpl.java
	 * @return {@link EntidadeConteudo}
	 */
	EntidadeConteudo getModalidadeUso();

	/**
	 * @author Carlos Romano
	 *         set modalidade uso
	 * @see PropostaImpl.java
	 * @param modalidadeUso args
	 *            recebe uma EntidadeConteudo
	 */
	
	void setModalidadeUso(EntidadeConteudo modalidadeUso);

	/**
	 * @author Carlos Romano
	 *         get tarida
	 * @see Tarifa
	 * @return {@link Tarifa}
	 */
	Tarifa getTarifa();

	/**
	 * @author Carlos Romano
	 *         set tarifa
	 * @see Tarifa
	 * @param tarifa
	 */
	void setTarifa(Tarifa tarifa);

	/**
	 * @return
	 */
	public Integer getAnoProposta();

	/**
	 * @param anoProposta
	 */
	public void setAnoProposta(Integer anoProposta);

	/**
	 * @return
	 */
	String getNumeroCompletoProposta();

	/**
	 * @param numeroCompletoProposta
	 */
	void setNumeroCompletoProposta(String numeroCompletoProposta);

	/**
	 * @return Bigdecimal
	 */
	BigDecimal getValorMedioGN();

	/**
	 * @param valorMedioGN
	 */
	void setValorMedioGN(BigDecimal valorMedioGN);
	
	/**
	 * @return the cliente
	 */
	public Cliente getCliente();

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente);
}
