/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.contrato.proposta.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel;
import br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.contrato.proposta.SituacaoProposta;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.proposta.FiltroProposta;
import br.com.ggas.web.relatorio.faturamento.AcompanhamentoClientesVO;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioAcompanhamentoClientesVO;
import br.com.ggas.web.relatorio.faturamento.RelatorioAcompanhamentoClientesWrapper;
import br.com.ggas.webservice.ImovelTO;
import br.com.ggas.webservice.PropostaTO;
import br.com.ggas.webservice.TipoComboBoxTO;
import br.com.ggas.util.BigDecimalUtil;

class ControladorPropostaImpl extends ControladorNegocioImpl implements ControladorProposta {

	private static final double PERCENTUAL_100 = 100.00;

	private static final int CONSTANTE_NUMERO_DOIS = 2;

	private static final int CONSTANTE_NUMERO_CINCO = 5;

	private static final int LIMITE_DESCRICAO_COMBUSTIVEIS = 2;

	private static final int TRINTA_DIAS = 30;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final String SUBJECT_EMAIL_RELATORIO_PROPOSTA = "Proposta";

	private static final Logger LOG = Logger.getLogger(ControladorPropostaImpl.class);

	private Fachada fachada = Fachada.getInstancia();

	private static final String VALOR_MENSAL_TOTAL = "valorMensalTotal";

	private static final String PRECO_TOTAL = "precoTotal";

	private static final String CONSUMO_MEDIO_ANUAL_TOTAL = "consumoMedioAnualTotal";

	private static final String CONSUMO_MEDIO_MENSAL_TOTAL = "consumoMedioMensalTotal";

	private static final String NUMERO_PROPOSTA = "numeroProposta";

	private static final String NUMERO_COMPLETO_PROPOSTA = "numeroCompletoProposta";

	private static final String PERCENTUAL_TIR = "percentualTIR";

	private static final String VALOR_CLIENTE = "valorCliente";

	private static final String VALOR_INVESTIMENTO = "valorInvestimento";

	private static final String CONSUMO_MEDIO_MENSAL = "consumoMedioMensal";

	private static final String QUANTIDADE_PARCELA = "quantidadeParcela";

	private static final String PERCENTUAL_JUROS = "percentualJuros";

	private static final String VALOR_PARCELA = "valorParcela";

	private static final String DATA_EMISSAO = "dataEmissao";

	private static final String VALOR_PRECO_PAGO = "valorPrecoPago";

	private static final String ECONOMIA_MENSAL_GN = "economiaMensalGN";

	private static final String ECONOMIA_ANUAL_GN = "economiaAnualGN";

	private static final String PERCENTUAL_ECONOMIA = "percentualEconomia";

	private static final String VALOR_MAO_DE_OBRA = "valorMaoDeObra";

	private static final String VALOR_MATERIAL = "valorMaterial";

	private static String RELATORIO_ACOMPANHAMENTO_CLIENTES = "relatorioAcompanhamentoClientes.jasper";

	private static final String PROPOSTA = "proposta";

	public static final String ENDERECO_EMPRESA = "enderecoEmpresa";

	public static final String NOME_CLIENTE = "nomeCliente";

	public static final String ENDERECO_CLIENTE = "enderecoCliente";

	public static final String CIDADE_DATA = "cidadeData";

	public static final String NOME_EMPRESA = "nomeEmpresa";

	public static final String NOME_IMOVEL = "nomeImovel";

	public static final String RELATORIO_PROPOSTA = "relatorioProposta.jasper";

	public static final String CLIENTE = "cliente";

	public static final int QTD_CASAS_DECIMAIS = 2;

	public static final int ESCALA_DIVISAO_DIAS_MES = 2;

	public static final int QTD_MESES_ANO = 12;

	public static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Proposta.BEAN_ID_PROPOSTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Proposta.BEAN_ID_PROPOSTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.impl.
	 * ControladorProposta#criarTarifa()
	 */
	@Override
	public EntidadeNegocio criarTarifa() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Tarifa.BEAN_ID_TARIFA);
	}

	public Class<?> getClasseEntidadeTarifa() {

		return ServiceLocator.getInstancia().getClassPorID(Tarifa.BEAN_ID_TARIFA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.impl.
	 * ControladorProposta#criarSituacaoProposta()
	 */
	@Override
	public EntidadeNegocio criarSituacaoProposta() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(SituacaoProposta.BEAN_ID_SITUACAO_PROPOSTA);
	}

	public Class<?> getClasseEntidadeSituacaoProposta() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoProposta.BEAN_ID_SITUACAO_PROPOSTA);
	}

	public Class<?> getClasseEntidadeTarifaVigencia() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigencia.BEAN_ID_TARIFA_VIGENCIA);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseEntidadeContrato() {

		return ServiceLocator.getInstancia().getClassPorID(Contrato.BEAN_ID_CONTRATO);
	}

	public Class<?> getClasseEntidadeInstalacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(Medidor.BEAN_ID_MEDIDOR);
	}

	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}

	public Class<ServicoAutorizacao> getClasseEntidadeServicoAutorizacao() {

		return ServicoAutorizacao.class;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl
	 * #preInsercao(br.com.ggas
	 * .geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Proposta proposta = (Proposta) entidadeNegocio;
		validarPropostaExistente(proposta);
		validarDatas(proposta);
		validarPercentuais(proposta);
		validarValores(proposta);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Proposta proposta = (Proposta) entidadeNegocio;
		validarPropostaExistente(proposta);
		validarDatas(proposta);
		validarPercentuais(proposta);
		validarValores(proposta);
	}

	/**
	 * Validar valores.
	 *
	 * @param proposta
	 *            the proposta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarValores(Proposta proposta) throws NegocioException {

		if ((proposta.getValorCliente() != null) && (proposta.getValorCliente().intValue() > 0)) {
			if (proposta.getValorInvestimento() == null) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_INVESTIMENTO, true);
			}
			if (proposta.getValorCliente().compareTo(proposta.getValorInvestimento()) > 0) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_VALOR_CLIENTE_MAIOR_VALOR_INVESTIMENTO, true);
			}

		}

		Boolean blnValidacaoValorNegativo = false;
		String strListaCampos = "";

		if ((proposta.getValorMaterial() != null) && (proposta.getValorMaterial().intValue() < 0)) {
			strListaCampos = "Valor Material";
			blnValidacaoValorNegativo = true;
		}
		if ((proposta.getValorMaoDeObra() != null) && (proposta.getValorMaoDeObra().intValue() < 0)) {
			if (blnValidacaoValorNegativo) {
				strListaCampos = strListaCampos.concat(", Valor da Mão de Obra");
			} else {
				strListaCampos = "Valor da Mão de Obra";
				blnValidacaoValorNegativo = true;
			}
		}
		if ((proposta.getPercentualCliente() != null) && (proposta.getPercentualCliente().intValue() < 0)) {
			if (blnValidacaoValorNegativo) {
				strListaCampos = strListaCampos.concat(", Percentual Clientes");
			} else {
				strListaCampos = "Percentual Clientes";
				blnValidacaoValorNegativo = true;
			}
		}
		if ((proposta.getValorInvestimento() != null) && (proposta.getValorInvestimento().intValue() < 0)) {
			if (blnValidacaoValorNegativo) {
				strListaCampos = strListaCampos.concat(", Valor do Investimento pelo Cliente");
			} else {
				strListaCampos = "Valor do Investimento pelo Cliente";
				blnValidacaoValorNegativo = true;
			}
		}
		if ((proposta.getQuantidadeParcela() != null) && (proposta.getQuantidadeParcela().intValue() < 0)) {
			if (blnValidacaoValorNegativo) {
				strListaCampos = strListaCampos.concat(", Quantidades de Parcelas");
			} else {
				strListaCampos = "Quantidades de Parcelas";
				blnValidacaoValorNegativo = true;
			}
		}
		if ((proposta.getValorParcela() != null) && (proposta.getValorParcela().intValue() < 0)) {
			if (blnValidacaoValorNegativo) {
				strListaCampos = strListaCampos.concat(", Valor das Parcelas");
			} else {
				strListaCampos = "Valor das Parcelas";
				blnValidacaoValorNegativo = true;
			}
		}
		if ((proposta.getPercentualJuros() != null) && (proposta.getPercentualJuros().intValue() < 0)) {
			if (blnValidacaoValorNegativo) {
				strListaCampos = strListaCampos.concat(", Percentual dos Juros");
			} else {
				strListaCampos = "Percentual dos Juros";
				blnValidacaoValorNegativo = true;
			}
		}
		if (blnValidacaoValorNegativo) {
			throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_VALOR_NEGATIVO, strListaCampos);
		}

	}

	/**
	 * Validar proposta existente.
	 *
	 * @param proposta
	 *            the proposta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPropostaExistente(Proposta proposta) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select proposta");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" proposta ");
		hql.append(" where ");
		hql.append(" lower(numeroProposta) = ? ");
		hql.append(" and versaoProposta = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, proposta.getNumeroProposta().toLowerCase());
		query.setLong(1, proposta.getVersaoProposta());

		Proposta propostaVerificador = (Proposta) query.uniqueResult();

		if ((propostaVerificador != null) && (propostaVerificador.getChavePrimaria() != proposta.getChavePrimaria())) {
			throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_PROPOSTA_EXISTENTE, true);
		}
		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(propostaVerificador);
	}

	/**
	 * Validar datas.
	 *
	 * @param proposta
	 *            the proposta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDatas(Proposta proposta) throws NegocioException {

		Date dataEmissao = proposta.getDataEmissao();
		Date dataVigencia = proposta.getDataVigencia();
		Date dataEntrega = proposta.getDataEntrega();
		Integer anoMesReferenciaPreco = proposta.getAnoMesReferenciaPreco();
		Calendar calendar = Calendar.getInstance();

		if (dataEmissao != null) {
			if ((dataVigencia != null) && (dataVigencia.before(dataEmissao))) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_DATA_VIGENCIA_INVALIDA, true);
			}
			if ((dataEntrega != null) && (dataEntrega.before(dataEmissao))) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_DATA_ENTREGA_INVALIDA, true);
			}
			// data de emissão não deve ser
			// posterior a data atual
			if (dataEmissao.compareTo(calendar.getTime()) > 0) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_DATA_EMISSAO_MAIOR_QUE_DATA_ATUAL, true);
			}
		}

		if ((dataVigencia != null) && (dataEntrega != null) && (dataVigencia.before(dataEntrega))) {
			throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_DATA_VIGENCIA_ENTREGA, true);
		}

		if (anoMesReferenciaPreco != null) {
			String anoMesReferenciaPrecoString = String.valueOf(anoMesReferenciaPreco);
			if (anoMesReferenciaPrecoString.length() < CONSTANTE_NUMERO_CINCO) {
				anoMesReferenciaPrecoString = "00".concat(anoMesReferenciaPrecoString);
			} else if (anoMesReferenciaPrecoString.length() == CONSTANTE_NUMERO_CINCO) {
				anoMesReferenciaPrecoString = "0".concat(anoMesReferenciaPrecoString);
			}
			if (Integer.parseInt(anoMesReferenciaPrecoString.substring(0, CONSTANTE_NUMERO_DOIS)) < 1
							|| Integer.parseInt(anoMesReferenciaPrecoString.substring(0, CONSTANTE_NUMERO_DOIS)) > QTD_MESES_ANO) {
				throw new NegocioException(ControladorProposta.ERRO_DATA_INVALIDA_ANO_MES_REFERENCIA_PRECO, true);
			}
			if (Integer.parseInt(anoMesReferenciaPrecoString.substring(CONSTANTE_NUMERO_DOIS)) <= 0) {
				throw new NegocioException(ControladorProposta.ERRO_DATA_INVALIDA_ANO_MES_REFERENCIA_PRECO, true);
			}
		}
	}

	@Override
	public void validarConsumosMedios(Proposta proposta, LevantamentoMercado levantamentoMercado) throws NegocioException {

		boolean validar = Boolean.TRUE;
		if(levantamentoMercado!=null){
			Collection<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = levantamentoMercado.getListaTipoCombustivel();
			if(listaTipoCombustivel!=null){
				validar = this.validarTipoCombustivel(listaTipoCombustivel, validar);
			}
		}

		if(validar){
			if (proposta.getConsumoMedioAnual() != null && proposta.getUnidadeConsumoAnual() == null
							|| ((proposta.getConsumoMedioAnual() == null) && (proposta.getUnidadeConsumoAnual() != null))) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_UNIDADE_CONSUMO_OBRIGATORIO, true);
			}
			if (proposta.getConsumoMedioMensal() != null && proposta.getUnidadeConsumoMensal() == null
							|| (proposta.getConsumoMedioMensal() == null && proposta.getUnidadeConsumoMensal() != null)) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_UNIDADE_CONSUMO_OBRIGATORIO, true);
			}
		}

	}

	private Boolean validarTipoCombustivel(Collection<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel, Boolean validarAux) {
		Boolean validar = validarAux;
		for (LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel : listaTipoCombustivel) {
			if (levantamentoMercadoTipoCombustivel.getTipoCombustivel() == null) {
				validar = Boolean.FALSE;
			}
		}
		return validar;
	}

	/**
	 * Validar percentuais.
	 *
	 * @param proposta
	 *            the proposta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPercentuais(Proposta proposta) throws NegocioException {

		if (proposta.getPercentualCliente() != null) {
			BigDecimal percentualCliente = proposta.getPercentualCliente();
			if (percentualCliente.compareTo(BigDecimal.valueOf(PERCENTUAL_100)) > 0) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_PERCENTUAL_INVALIDO, Proposta.PERCENTUAL_CLIENTE);
			}
		}

		if (proposta.getPercentualTIR() != null) {
			BigDecimal percentualTIR = proposta.getPercentualTIR();
			if (percentualTIR.compareTo(BigDecimal.valueOf(PERCENTUAL_100)) > 0) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_PERCENTUAL_INVALIDO, Proposta.PERCENTUAL_TIR);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #listarSituacaoProposta()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SituacaoProposta> listarSituacaoProposta() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSituacaoProposta().getSimpleName());
		hql.append(" where ");
		hql.append(" habilitado = true ");
		hql.append(" order by descricao asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	@Override
	public Collection<SituacaoProposta> listarSituacoesIniciaisProposta() throws GGASException {
		Collection<SituacaoProposta> todasSituacoes = fachada.listarSituacaoProposta();
		String[] descricoesInvalidas = getDescricoesInvalidas();

		return removerSituacoesInvalidas(todasSituacoes, descricoesInvalidas);
	}

	private String[] getDescricoesInvalidas() throws GGASException {
		String valorConstantePropostaSituacaoAprovada =
				this.fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_PROPOSTA_SITUACAO_APROVADA);
		String valorConstantePropostaSituacaoRejeitada =
				this.fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_PROPOSTA_SITUACAO_REJEITADA);
		return new String[]{valorConstantePropostaSituacaoAprovada,valorConstantePropostaSituacaoRejeitada};
	}

	private Collection<SituacaoProposta> removerSituacoesInvalidas(Collection<SituacaoProposta> todasSituacoes, String[] descricoesInvalidas) {
		Iterator<SituacaoProposta> iterator = todasSituacoes.iterator();
		SituacaoProposta situacao;

		while(iterator.hasNext()) {
			situacao = iterator.next();
			boolean descricaoInvalidaEncontrada = validaSituacaoPorDescricao(descricoesInvalidas, situacao);
			if(descricaoInvalidaEncontrada) {
				iterator.remove();
			}
		}

		return todasSituacoes;
	}

	private boolean validaSituacaoPorDescricao(String[] descricoesInvalidas, SituacaoProposta situacao) {
		String descricao = situacao.getDescricao();
		return procuraDescricaoInvalida(descricoesInvalidas, descricao);
	}

	private boolean procuraDescricaoInvalida(String[] descricoesInvalidas, String descricao) {
		Arrays.sort(descricoesInvalidas);
		int pos = Arrays.binarySearch(descricoesInvalidas, descricao);
		return pos >= 0;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #obterSituacaoProposta(long)
	 */
	@Override
	public SituacaoProposta obterSituacaoProposta(long chavePrimaria) throws NegocioException {

		return (SituacaoProposta) super.obter(chavePrimaria, getClasseEntidadeSituacaoProposta());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta#obterTarifa(long)
	 */
	@Override
	public Tarifa obterTarifa(long chavePrimaria) throws NegocioException {

		return (Tarifa) super.obter(chavePrimaria, getClasseEntidadeTarifa());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #consultarPropostas(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Proposta> consultarPropostas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			Long idImovel = (Long) filtro.get("idImovel");
			if ((idImovel != null) && (idImovel > 0)) {
				criteria.createAlias("imovel", "imovel");
				criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			String numeroProposta = (String) filtro.get(NUMERO_PROPOSTA);
			if (numeroProposta != null) {
				criteria.add(Restrictions.ilike(NUMERO_COMPLETO_PROPOSTA, numeroProposta, MatchMode.ANYWHERE));
			}

			Long idCliente = (Long) filtro.get("idCliente");
			if ((idCliente != null) && (idCliente > 0)) {
				criteria.createAlias(CLIENTE, CLIENTE);
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}


		}
		criteria.setFetchMode("situacaoProposta", FetchMode.JOIN);

		criteria.addOrder(Order.asc(NUMERO_PROPOSTA));
		criteria.addOrder(Order.asc("versaoProposta"));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #validarRemoverPropostas(java.lang.Long[])
	 */
	@Override
	public void validarRemoverPropostas(Long[] chavesPrimarias) throws NegocioException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		Collection<Proposta> listaPropostas = consultarPropostas(filtro);
		boolean remover = true;
		for (Proposta proposta : listaPropostas) {
			if (proposta.getSituacaoProposta() != null && ("APROVADA").equalsIgnoreCase(proposta.getSituacaoProposta().getDescricao())
							|| ("REIJEITADA").equalsIgnoreCase(proposta.getSituacaoProposta().getDescricao())) {
				remover = false;
			}
		}

		if (!remover) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_PROPOSTA_NAO_PODE_SER_REMOVIDA, true);
		}

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		} else {
			Collection<Contrato> listaContrato = controladorContrato.consultarContratosPorProposta(chavesPrimarias);
			if (listaContrato != null && !listaContrato.isEmpty()) {
				throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_REMOVER_PROPOSTA, true);
			}
		}
	}

	/**
	 * Buscar proposta existente.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param numeroProposta
	 *            the numero proposta
	 * @param versaoProposta
	 *            the versao proposta
	 * @return the proposta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Proposta buscarPropostaExistente(Long chavePrimaria, Long numeroProposta, Integer versaoProposta) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select proposta.chavePrimaria");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" proposta ");
		hql.append(" where ");
		hql.append(" numeroProposta = ? ");
		hql.append(" and versaoProposta = ? ");
		hql.append(" and chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, numeroProposta);
		query.setInteger(1, versaoProposta);
		query.setLong(CONSTANTE_NUMERO_DOIS, chavePrimaria);

		return (Proposta) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #importarDadosPropostaExcel(byte[],
	 * java.lang.String)
	 */
	@Override
	public Map<String, Object> importarDadosPropostaExcel(byte[] bytes, String tipoArquivo, String nomeArquivo) throws GGASException {

		Workbook wb = null;
		Map<String, Object> dados = new HashMap<>();

		if (nomeArquivo == null || StringUtils.isEmpty(nomeArquivo)) {
			throw new NegocioException(Proposta.PROPOSTA_ARQUIVO_SELECAO_INVALIDA, true);
		}
		if (bytes != null) {
			try {
				if (Constantes.FORMATO_ARQUIVO_XLSX.equals(tipoArquivo)) {
					wb = new XSSFWorkbook(new ByteArrayInputStream(bytes));
				} else if (Constantes.FORMATO_ARQUIVO_XLS.equals(tipoArquivo)) {
					wb = new HSSFWorkbook(new ByteArrayInputStream(bytes));
				} else {
					throw new NegocioException(Constantes.FALHA_FORMATO_IMPORTACAO, true);
				}

				if (wb != null) {
					Sheet sheet = wb.getSheet(Constantes.PLANILHA_GGAS);
					if (sheet != null) {
						Row row = sheet.getRow(1);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(celula.getNumericCellValue());
								dados.put(NUMERO_PROPOSTA, valor.longValue());

							}
						}

						row = sheet.getRow(CONSTANTE_NUMERO_DOIS);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue() * 100, 0));
								tryAdicionarPercentualTIR(dados, valor);
							}
						}

						row = sheet.getRow(3);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), 0));
								tryAdicionarValorCliente(dados, valor);

							}
						}

						row = sheet.getRow(4);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {

								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), CONSTANTE_NUMERO_DOIS));
								tryAdicionarConsumoMedioMensal(dados, valor);

							}
						}

						row = sheet.getRow(CONSTANTE_NUMERO_CINCO);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(celula.getNumericCellValue());
								dados.put(QUANTIDADE_PARCELA, valor.longValue());

							}
						}

						row = sheet.getRow(6);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue() * 100, 0));
								tryAdicionarPercentualJuros(dados, valor);
							}
						}

						row = sheet.getRow(7);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(celula.getNumericCellValue());
								dados.put(VALOR_PARCELA, Util.converterCampoValorDecimalParaString(Proposta.VALOR_PARCELA, valor,
												Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
							}
						}

						row = sheet.getRow(8);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								dados.put(DATA_EMISSAO, Util.converterDataHoraParaString(DateUtil.getJavaDate(celula.getNumericCellValue())));

							}
						}

						row = sheet.getRow(9);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), 0));
								tryAdicionarValorInvestimento(dados, valor);
							}
						}

						verificarValorCliente(dados);

						row = sheet.getRow(10);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {

								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), 4));
								tryAdicionarValorPrecoPago(dados, valor);
							}
						}

						row = sheet.getRow(11);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {

								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), QTD_CASAS_DECIMAIS));
								tryAdicionarEconomiaMensalGN(dados, valor);
							}
						}

						row = sheet.getRow(12);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {

								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), QTD_CASAS_DECIMAIS));
								tryAdicionarEconomiaAnualGN(dados, valor);

							}
						}

						row = sheet.getRow(13);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue() * 100, 0));
								tryAdicionarPercentualEconomia(dados, valor);
							}
						}

						row = sheet.getRow(14);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {

								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), CONSTANTE_NUMERO_DOIS));
								tryAdicionarValorMaoDeObra(dados, valor);
							}
						}

						row = sheet.getRow(15);
						if (row != null) {
							Cell celula = row.getCell(1);
							if (celula != null
											&& (celula.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || celula.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {

								BigDecimal valor = new BigDecimal(Util.round(celula.getNumericCellValue(), 2));
								tryAdicionarValorMaterial(dados, valor);
							}
						}

					} else {
						throw new NegocioException(Constantes.FALHA_FORMATO_IMPORTACAO, true);
					}
				}
			} catch (IOException e) {
				throw new NegocioException(e);
			}

		}
		return dados;
	}

	private void tryAdicionarPercentualTIR(Map<String, Object> dados, BigDecimal valor) {
		dados.put(PERCENTUAL_TIR, Util.converterCampoPercentualParaString(Proposta.PERCENTUAL_TIR, valor,
						Constantes.LOCALE_PADRAO));
	}

	private void tryAdicionarValorCliente(Map<String, Object> dados, BigDecimal valor) {
		dados.put(VALOR_CLIENTE, Util.converterCampoValorDecimalParaString(Proposta.VALOR_CLIENTE, valor,
						Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	private void tryAdicionarConsumoMedioMensal(Map<String, Object> dados, BigDecimal valor) {
		dados.put(CONSUMO_MEDIO_MENSAL, Util.converterCampoValorDecimalParaString(
						Proposta.CONSUMO_MEDIO_MENSAL, valor, Constantes.LOCALE_PADRAO,
						Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL));
	}

	private void tryAdicionarPercentualJuros(Map<String, Object> dados, BigDecimal valor) {
		dados.put(PERCENTUAL_JUROS, Util.converterCampoPercentualParaString(Proposta.PERCENTUAL_JUROS, valor,
						Constantes.LOCALE_PADRAO));
	}

	private void tryAdicionarValorInvestimento(Map<String, Object> dados, BigDecimal valor) {
		dados.put(VALOR_INVESTIMENTO, Util.converterCampoValorDecimalParaString(Proposta.VALOR_INVESTIMENTO,
						valor, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	private void tryAdicionarValorPrecoPago(Map<String, Object> dados, BigDecimal valor) {
		dados.put(VALOR_PRECO_PAGO, Util.converterCampoValorDecimalParaString(Proposta.VALOR_PRECO_PAGO, valor,
						Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	private void tryAdicionarEconomiaMensalGN(Map<String, Object> dados, BigDecimal valor) {
		dados.put(ECONOMIA_MENSAL_GN, Util.converterCampoValorDecimalParaString(Proposta.ECONOMIA_MENSAL_GN,
							valor, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	private void tryAdicionarEconomiaAnualGN(Map<String, Object> dados, BigDecimal valor) {
		dados.put(ECONOMIA_ANUAL_GN, Util.converterCampoValorDecimalParaString(Proposta.ECONOMIA_ANUAL_GN,
							valor, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	private void tryAdicionarPercentualEconomia(Map<String, Object> dados, BigDecimal valor) {
		dados.put(PERCENTUAL_ECONOMIA, Util.converterCampoPercentualParaString(Proposta.PERCENTUAL_ECONOMIA,
						valor, Constantes.LOCALE_PADRAO));
	}

	private void tryAdicionarValorMaoDeObra(Map<String, Object> dados, BigDecimal valor) {
		dados.put(VALOR_MAO_DE_OBRA, Util.converterCampoValorDecimalParaString(Proposta.VALOR_MAO_DE_OBRA,
						valor, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	private void tryAdicionarValorMaterial(Map<String, Object> dados, BigDecimal valor) {
		dados.put(VALOR_MATERIAL, Util.converterCampoValorDecimalParaString(Proposta.VALOR_MATERIAL, valor,
						Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
	}

	/**
	 * Verificar valor cliente.
	 *
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarValorCliente(Map<String, Object> dados) throws NegocioException {

		String investimentoCliente = (String) dados.get(VALOR_INVESTIMENTO);
		String valorCliente = (String) dados.get(VALOR_CLIENTE);

		BigDecimal investimentoClienteConvertido = null;
		BigDecimal valorClienteConvertido = null;

		try {
			if (!StringUtils.isEmpty(investimentoCliente)) {
				investimentoClienteConvertido = Util.converterCampoStringParaValorBigDecimal("Investimento Cliente", investimentoCliente,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			}
			if (!StringUtils.isEmpty(valorCliente)) {
				valorClienteConvertido = Util.converterCampoStringParaValorBigDecimal("Valor Cliente", valorCliente,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			}
		} catch (FormatoInvalidoException e) {
			throw new NegocioException(e);
		}

		if (valorClienteConvertido != null && investimentoClienteConvertido != null
						&& valorClienteConvertido.compareTo(investimentoClienteConvertido) > 0) {
			throw new NegocioException(ControladorProposta.ERRO_NEGOCIO_IMPORTACAO_PLANILHA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #listarPropostasAprovadasPorNumero
	 * (java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Proposta> listarPropostasAprovadasPorNumero(String numeroProposta) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(
				" where (lower(numeroProposta) = :numeroProposta or lower(((anoProposta * 100000) + numeroProposta)) = :numeroProposta) ");
		hql.append(" and habilitado = :habilitado ");
		hql.append(" and situacaoProposta.chavePrimaria = :situacao ");
		hql.append(" order by numeroProposta, versaoProposta");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(NUMERO_PROPOSTA, numeroProposta.toLowerCase());
		query.setBoolean(EntidadeNegocio.ATRIBUTO_HABILITADO, true);
		query.setLong("situacao", SituacaoProposta.APROVADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #obterQtdPropostasAprovadasPorNumero
	 * (java.lang.String)
	 */
	@Override
	public Integer obterQtdPropostasAprovadasPorNumero(String numeroProposta) throws NegocioException {

		Long resultadoAux = 0L;
		Collection<Proposta> propostas = listarPropostasAprovadasPorNumero(numeroProposta);
		if (propostas == null || propostas.isEmpty()) {
			StringBuilder hql = new StringBuilder();
			hql.append(" select count(chavePrimaria) from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where numeroProposta = :numeroProposta ");
			hql.append(" and habilitado = :habilitado ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(NUMERO_PROPOSTA, numeroProposta);
			query.setBoolean(EntidadeNegocio.ATRIBUTO_HABILITADO, true);
			resultadoAux = (Long) query.uniqueResult();
		} else {
			resultadoAux = (long) propostas.size();
		}
		return resultadoAux.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #validarQtdPropostasAprovadas
	 * (java.lang.String)
	 */
	@Override
	public void validarPropostasContrato(String numeroProposta, Boolean propostaAprovada) throws NegocioException {

		if (!StringUtils.isEmpty(numeroProposta)) {
			if (propostaAprovada) {
				Collection<Proposta> propostas = listarPropostasAprovadasPorNumero(numeroProposta);
				if (Util.isNullOrEmpty(propostas)) {
					throw new NegocioException(ERRO_NEGOCIO_PROPOSTA_NAO_APROVADA, true);
				}
			} else {
				Map<String, Object> filtro = new HashMap<>();
				filtro.put(NUMERO_PROPOSTA, numeroProposta);
				filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, true);
				Collection<Proposta> propostas = consultarPropostas(filtro);
				if (Util.isNullOrEmpty(propostas)) {
					throw new NegocioException(ERRO_NEGOCIO_PROPOSTA_NAO_EXISTENTE, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #validarTamanhoComentario(java.lang.String)
	 */
	@Override
	public void validarTamanhoComentario(String comentario) throws NegocioException {

		if (!StringUtils.isEmpty(comentario) && comentario.length() > TAMANHO_MAXIMO_CAMPO_COMENTARIO) {
			throw new NegocioException(ERRO_NEGOCIO_MAXIMO_CAMPO_COMENTARIO, Proposta.SOLICITACOES_ADICIONAIS);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #consultarPeriodicidadeJuros
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EntidadeConteudo> consultarPeriodicidadeJuros(Long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeConteudo().getSimpleName());
		hql.append(" entidadeConteudo ");
		hql.append(" inner join fetch entidadeConteudo.entidadeClasse entidadeClasse");
		hql.append(" where");
		hql.append(" entidadeClasse.chavePrimaria = :CHAVE_ENTIDADE_CLASE");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_ENTIDADE_CLASE", chavePrimaria);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#gerarRelatorioAcompanhamentoClientes(br.com.ggas.web.relatorio.faturamento.
	 * PesquisaRelatorioAcompanhamentoClientesVO, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorioAcompanhamentoClientes(PesquisaRelatorioAcompanhamentoClientesVO pesquisaRelatorioAcompanhamentoClientesVO,
					FormatoImpressao formatoImpressao) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						getControladorConstanteSistema();
		Long chavePrimariaServicoTipo =
						controladorConstanteSistema.obterValorConstanteServicoTipoHabilitacao();

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append("  proposta, ");
		hql.append("(select max(cont) from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" cont where cont.numeroCompletoContrato = contrato.numeroCompletoContrato ");
		hql.append(") ");
		hql.append(", (select max(instalacaoMedidor.dataAtivacao) from ");
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor where instalacaoMedidor.pontoConsumo.chavePrimaria in (pontoConsumo.chavePrimaria) ");
		hql.append(" ) ");
		hql.append(" , (select instalacaoMedidor.medidor.ultimaAlteracao from ");
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor where instalacaoMedidor.pontoConsumo.chavePrimaria in (pontoConsumo.chavePrimaria) and "
						+ "instalacaoMedidor.dataAtivacao <> null and instalacaoMedidor.medidor.situacaoMedidor.chavePrimaria <> 2 ");
		hql.append(" ) ");
		hql.append(" , (select servicoAutorizacao from ");
		hql.append(getClasseEntidadeServicoAutorizacao().getSimpleName());
		hql.append(" servicoAutorizacao ");
		hql.append(" left join servicoAutorizacao.equipe equipe ");
		hql.append(" left join equipe.unidadeOrganizacional ");
		hql.append(" where servicoAutorizacao.servicoTipo.chavePrimaria = :chavePrimariaServicoTipo and "
						+ "servicoAutorizacao.contrato.numeroCompletoContrato = contrato.numeroCompletoContrato and rownum = 1");
		hql.append(" ) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contrato ");
		hql.append(" right join contrato.proposta proposta");
		hql.append(" left join contrato.clienteAssinatura clienteAssinatura ");
		hql.append(" left join contrato.listaContratoPontoConsumo listaContratoPontoConsumo ");
		hql.append(" left join listaContratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" left join proposta.vendedor vendedor ");
		hql.append(" left join proposta.fiscal fiscal ");
		if (pesquisaRelatorioAcompanhamentoClientesVO.possuiParametros()) {
			hql.append(" where 1=1 ");
		}

		if (pesquisaRelatorioAcompanhamentoClientesVO.getVendedor() != null
						&& pesquisaRelatorioAcompanhamentoClientesVO.getVendedor() != -1) {
			hql.append(" and vendedor.chavePrimaria = :idVendedor ");
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getFiscal() != null && pesquisaRelatorioAcompanhamentoClientesVO.getFiscal() != -1) {
			hql.append(" and fiscal.chavePrimaria = :idFiscal ");
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getVolumeInicial() != null
						&& pesquisaRelatorioAcompanhamentoClientesVO.getVolumeFinal() != null) {
			hql.append(" and proposta.consumoMedioMensal between :volumeInicial and :volumeFinal ");
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getIdCliente() != null) {
			hql.append(" and clienteAssinatura.chavePrimaria = :idCliente ");
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaInicial() != null
						&& !"".equals(pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaInicial())
						&& pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaFinal() != null
						&& "".equals(pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaFinal())) {
			hql.append(" and proposta.dataEmissao between :dataEmissaoInicial and :dataEmissaoFinal ");
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getValorOrcamentoInicial() != null
						&& pesquisaRelatorioAcompanhamentoClientesVO.getValorOrcamentoFinal() != null) {
			hql.append(" and proposta.valorInvestimento between :valorInicial and :valorFinal ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (pesquisaRelatorioAcompanhamentoClientesVO.getVendedor() != null
						&& pesquisaRelatorioAcompanhamentoClientesVO.getVendedor() != -1) {
			query.setLong("idVendedor", pesquisaRelatorioAcompanhamentoClientesVO.getVendedor());
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getFiscal() != null && pesquisaRelatorioAcompanhamentoClientesVO.getFiscal() != -1) {
			query.setLong("idFiscal", pesquisaRelatorioAcompanhamentoClientesVO.getFiscal());
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getVolumeInicial() != null
						&& pesquisaRelatorioAcompanhamentoClientesVO.getVolumeFinal() != null) {
			query.setInteger("volumeInicial", pesquisaRelatorioAcompanhamentoClientesVO.getVolumeInicial());
			query.setInteger("volumeFinal", pesquisaRelatorioAcompanhamentoClientesVO.getVolumeFinal());
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getIdCliente() != null) {
			query.setLong("idCliente", pesquisaRelatorioAcompanhamentoClientesVO.getIdCliente());
		}
		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		if (pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaInicial() != null
						&& !"".equals(pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaInicial())
						&& pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaFinal() != null
						&& "".equals(pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaFinal())) {
			Date dataGeracaoInicio = null;
			Date dataGeracaoFim = null;
			try {
				dataGeracaoInicio = formatoData.parse(pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaInicial());
				dataGeracaoFim = formatoData.parse(pesquisaRelatorioAcompanhamentoClientesVO.getDataEntregaPropostaFinal());
			} catch (ParseException e) {
				Logger.getLogger("ERROR").error(e.getMessage());
			}

			query.setDate("dataEmissaoInicial", dataGeracaoInicio);
			query.setDate("dataEmissaoFinal", dataGeracaoFim);
		}
		if (pesquisaRelatorioAcompanhamentoClientesVO.getValorOrcamentoInicial() != null
						&& pesquisaRelatorioAcompanhamentoClientesVO.getValorOrcamentoFinal() != null) {
			query.setDouble("valorInicial", pesquisaRelatorioAcompanhamentoClientesVO.getValorOrcamentoInicial());
			query.setDouble("valorFinal", pesquisaRelatorioAcompanhamentoClientesVO.getValorOrcamentoFinal());
		}

		query.setParameter("chavePrimariaServicoTipo", chavePrimariaServicoTipo);

		List<Object[][]> objetos = query.list();

		Iterator<Object[][]> it = objetos.iterator();
		List<AcompanhamentoClientesVO> lista = new ArrayList<>();
		AcompanhamentoClientesVO vo;
		Proposta prop = null;
		Contrato cont = null;
		Date dataAtivacao = null;
		Date dataDesligamento = null;
		ServicoAutorizacao servicoAutorizacao = null;
		List<Long> chaves = new ArrayList<>();

		while (it.hasNext()) {
			vo = new AcompanhamentoClientesVO();
			Object[] obj = it.next();
			prop = (Proposta) obj[0];
			cont = (Contrato) obj[1];
			dataAtivacao = (Date) obj[2];
			dataDesligamento = (Date) obj[3];
			servicoAutorizacao = (ServicoAutorizacao) obj[4];
			if ((cont != null && !chaves.contains(cont.getChavePrimaria())) || cont == null) {
				if (cont != null) {
					chaves.add(cont.getChavePrimaria());
				}
				vo.setDataDesligamento(dataDesligamento);
				vo.setDataLiberacao(dataAtivacao);

				this.popularDadosAcompanhamentoClientesVO(prop, vo, cont, servicoAutorizacao);

				lista.add(vo);
			}
		}

		RelatorioAcompanhamentoClientesWrapper wrapper = new RelatorioAcompanhamentoClientesWrapper();
		wrapper.setListaPropostas(lista);

		Map<String, Object> parametros = new HashMap<>();
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		parametros.put("imagem",
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		Collection<Object> collRelatorio = new ArrayList<>();
		collRelatorio.add(wrapper);

		if (lista != null && !lista.isEmpty()) {
			return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_ACOMPANHAMENTO_CLIENTES, formatoImpressao);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}
	}

	private void popularDadosAcompanhamentoClientesVO(Proposta prop, AcompanhamentoClientesVO vo, Contrato cont,
			ServicoAutorizacao servicoAutorizacao) {
		if (prop != null) {
			vo.setDataEntregaProposta(prop.getDataEmissao());
			vo.setParticipacaoCliente(prop.getValorCliente());
			vo.setValorMaoObra(prop.getValorMaoDeObra());
			vo.setValorMaterial(prop.getValorMaterial());
			vo.setPercentualTIR(prop.getPercentualTIR());
			vo.setValorTotal(prop.getValorInvestimento());
			vo.setSituacaoProposta(prop.getSituacaoProposta().getDescricao());
			vo.setVolumeMensal(prop.getConsumoMedioMensal());
			if (prop.getVendedor() != null) {
				vo.setVendedor(prop.getVendedor().getNome());
			}
			if (prop.getFiscal() != null) {
				vo.setFiscal(prop.getFiscal().getNome());
			}

		}
		if (cont != null) {
			vo.setNumeroContrato(String.valueOf(cont.getNumeroCompletoContrato()));
			vo.setNomeCliente(cont.getClienteAssinatura().getNome());
			vo.setDataAssinaturaContrato(cont.getDataAssinatura());
			vo.setDataDistrato(cont.getDataRecisao());
			vo.setDataEnvioMateriais(cont.getClienteAssinatura().getDataEnvioBrindes());
			if (servicoAutorizacao != null) {
				vo.setDataEmissaoAES(servicoAutorizacao.getDataGeracao());
				if (servicoAutorizacao.getEquipe() != null) {
					vo.setExecutor(servicoAutorizacao.getEquipe().getUnidadeOrganizacional().getDescricao());
				}
				vo.setDataSolicitacaoRamal(servicoAutorizacao.getDataSolicitacaoRamal());
			}
		}
	}

	private ControladorConstanteSistema getControladorConstanteSistema() {

		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#gerarPropostaPDF(java.util.Map)
	 */
	@Override
	public byte[] gerarPropostaPDF(Map<String, Object> parametros) throws NegocioException {

		List<Object> listaProposta = new ArrayList<>();
		Proposta proposta = (Proposta) parametros.get(PROPOSTA);
		listaProposta.add(proposta);

		try {
			this.popularRelatorio(parametros, proposta);
		} catch (GGASException e) {
			LOG.error(e);
		}

		FormatoImpressao formato = FormatoImpressao.PDF;

		return RelatorioUtil.gerarRelatorio(listaProposta, parametros, RELATORIO_PROPOSTA, formato);

	}

	/**
	 * Popular relatorio.
	 *
	 * @param parametros
	 *            the parametros
	 * @param proposta
	 *            the proposta
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularRelatorio(Map<String, Object> parametros, Proposta proposta) throws GGASException {

		ControladorLevantamentoMercado controlMercado = (ControladorLevantamentoMercado) ServiceLocator.getInstancia().getBeanPorID(
						"controladorLevantamentoMercado");

		Fachada fachada = Fachada.getInstancia();
		Empresa empresaPrincial = fachada.obterEmpresaPrincipal();
		LevantamentoMercado levaMercado = null;

		if (empresaPrincial.getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresaPrincial.getChavePrimaria()));
		}

		if (empresaPrincial.getCliente().getEnderecoPrincipal()!=null
				&& empresaPrincial.getCliente().getEnderecoPrincipal().getEnderecoFormatadoBairroMunicipioUFCEP() != null) {
			parametros.put(CIDADE_DATA,
							empresaPrincial.getCliente().getEnderecoPrincipal().getMunicipio().getDescricao() + ", "
											+ Util.dataAtualPorExtenso());
			parametros.put(NOME_EMPRESA, empresaPrincial.getCliente().getNomeFantasia());

		}

		if (proposta!=null && proposta.getImovel() != null) {

			Imovel imovel = fachada.buscarImovelPorChave(proposta.getImovel().getChavePrimaria());

			parametros.put(ENDERECO_CLIENTE, imovel.getEnderecoFormatado());
			parametros.put(NOME_IMOVEL, imovel.getNome());
			parametros.put(NUMERO_COMPLETO_PROPOSTA, proposta.getNumeroCompletoProposta());

			ContatoImovel contatoPrincipal = null;
			if (imovel.getContatos() != null && !imovel.getContatos().isEmpty()) {

				contatoPrincipal = this.verificarContatoPrincipalImovel(imovel);
			}

			if (contatoPrincipal != null) {
				parametros.put(NOME_CLIENTE, contatoPrincipal.getNome());
			}

			levaMercado = controlMercado.obterLevantamentoMercadoPorChaveProposta(proposta.getChavePrimaria());

		}

		if (levaMercado!=null && !CollectionUtils.isEmpty(levaMercado.getListaTipoCombustivel())) {
			Collection<String> modalidades = new ArrayList<>();
			BigDecimal totalTodosCombustiveis = BigDecimal.ZERO;
			String modalidadesString = "";
			String descricaoCombustiveis = "";
			StringBuilder builder = new StringBuilder();
			for (LevantamentoMercadoTipoCombustivel leCombustivel : levaMercado.getListaTipoCombustivel()) {
				if (!modalidades.contains(leCombustivel.getModalidadeUso().getDescricao() + ", ")) {
					modalidades.add(leCombustivel.getModalidadeUso().getDescricao() + ", ");
				}
				if(leCombustivel.getTipoCombustivel()!=null){
					builder.append(leCombustivel.getTipoCombustivel().getDescricao());
				}else{
					builder.append("Não se Aplica");
				}
				builder.append(", ");
				if(leCombustivel.getPreco()!=null){
					totalTodosCombustiveis = totalTodosCombustiveis.add(leCombustivel.getPreco());
				}
			}
			descricaoCombustiveis = builder.toString();
			for (String modalidade : modalidades) {
				modalidadesString = modalidadesString.concat(modalidade);
			}
			int tamanhoStringCombustiveis = descricaoCombustiveis.length();
			int tamanho = modalidadesString.length();
			modalidadesString = modalidadesString.substring(0, tamanho - CONSTANTE_NUMERO_DOIS);
			descricaoCombustiveis = descricaoCombustiveis.substring(0,
					tamanhoStringCombustiveis - LIMITE_DESCRICAO_COMBUSTIVEIS);
			parametros.put("modalidadeUso", modalidadesString);
			parametros.put("totalTodosCombustiveis",
							"R$ "
											+ Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor",
															totalTodosCombustiveis, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
			parametros.put("consumoMedio",
							Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", proposta.getConsumoMedioMensal(),
											Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS) + " Kg");
			parametros.put("consumoMedioComUnidadeDeTempo",
							Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", proposta.getConsumoMedioMensal(),
											Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS) + " Kg/Mês");
			parametros.put("descricaoCombustiveis", descricaoCombustiveis);
			if (proposta.getValorMedioGN() != null) {
				BigDecimal valorTotalConsumoMedioVezesValorMedio = proposta.getConsumoMedioMensal().multiply(totalTodosCombustiveis)
								.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
				parametros.put("valorTotalConsumoMedioVezesValorMedio",
								"R$ "
												+ Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Médio GN",
																valorTotalConsumoMedioVezesValorMedio, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
			}
		}
		if (proposta != null) {

			if (proposta.getConsumoMedioMensal() != null) {
				parametros.put("consumoMedio",
								Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", proposta.getConsumoMedioMensal(),
												Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS) + " Kg");
				parametros.put("consumoMedioComUnidadeDeTempo",
								Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", proposta.getConsumoMedioMensal(),
												Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS) + " Kg/Mês");
			}

			if (proposta.getModalidadeUso() != null) {
				parametros.put("modalidadeUso", proposta.getModalidadeUso().getDescricao());
			}

			parametros.put("descricaoCombustiveis", "GLP");


			if (proposta.getDataEmissao() != null) {
				String data = Util.converterDataParaString(proposta.getDataEmissao());
				parametros.put("dataReferencia", data);
			}

			if (proposta.getConsumoMedioMensalGN() != null) {
				parametros.put("consumoMedioMensalGN",
								Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", proposta.getConsumoMedioMensalGN(),
												Constantes.LOCALE_PADRAO, DUAS_CASAS_DECIMAIS) + " m");
			}

			if (proposta.getEconomiaMensalGN() != null) {
				parametros.put("economiaMensalGN",
								"R$ "
												+ Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor",
																proposta.getEconomiaMensalGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
			}

			if (proposta.getEconomiaAnualGN() != null) {
				parametros.put("economiaAnualGN",
								"R$ "
												+ Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor",
																proposta.getEconomiaAnualGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
			}

			if (proposta.getPercentualEconomia() != null) {
				BigDecimal valor = BigDecimalUtil.converterPercentualParaBigDecimal(proposta.getPercentualEconomia());
				parametros.put("percentualEconomiaMensal",
								Util.converterCampoPercentualParaString("Percentual", valor, Constantes.LOCALE_PADRAO) + " %");
			}

			if (proposta.getValorMedioGN() != null) {
				parametros.put("valorMedioGN",
								"R$ "
												+ Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Médio GN",
																proposta.getValorMedioGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
			}

			if (proposta.getConsumoMedioMensalGN() != null && proposta.getValorMedioGN() != null) {
				BigDecimal valorTotalConsumoGN = proposta.getConsumoMedioMensalGN().multiply(proposta.getValorMedioGN())
								.setScale(CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP);
				parametros.put("valorTotalConsumoGN",
								"R$ "
												+ Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Médio GN",
																valorTotalConsumoGN, Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
			}
		}
	}

	private ContatoImovel verificarContatoPrincipalImovel(Imovel imovel) {
		ContatoImovel contatoPrincipal = null;
		for (ContatoImovel contato : imovel.getContatos()) {
			if (contato.isPrincipal()) {
				contatoPrincipal = contato;
			}
		}
		return contatoPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#enviarPropostaPorEmail(br.com.ggas.contrato.proposta.Proposta, byte[])
	 */
	@Override
	public void enviarPropostaPorEmail(Proposta proposta, byte[] bytes) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Imovel imovel = Fachada.getInstancia().buscarImovelPorChave(proposta.getImovel().getChavePrimaria());
		ContatoImovel contato = null;
		if (imovel.getContatos() != null && !imovel.getContatos().isEmpty()) {
			for (ContatoImovel contatoImovel : imovel.getContatos()) {
				if (contatoImovel.isPrincipal()) {
					contato = contatoImovel;
				}
			}
		}

		if (contato == null || contato.getEmail() == null) {
			throw new NegocioException(Constantes.ERRO_EMAIL_CONTATO_IMOVEL, true);
		}

		ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(bytes, "application/pdf");

		String conteudoEmail = conteudoEmailRelatorioProposta(proposta);

		try {
			String valorConstanteSistemaEmailRemetentePadrao =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
											Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);

			String attachmentFilename = nomeArquivoEmailRelatorioProposta(proposta);

			getJavaMailUtil().enviar(valorConstanteSistemaEmailRemetentePadrao,
							contato.getEmail(), SUBJECT_EMAIL_RELATORIO_PROPOSTA,
							conteudoEmail, attachmentFilename, byteArrayDataSource);

		} catch (NegocioException e) {
			throw e;
		} catch (GGASException e) {
			LOG.error(e.getStackTrace(), e);
			throw new NegocioException(Constantes.ERRO_EMAIL_CONTATO_IMOVEL, true);
		}
	}

	private JavaMailUtil getJavaMailUtil() {

		return (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);
	}

	private String conteudoEmailRelatorioProposta(Proposta proposta) {

		StringBuilder conteudoEmail = new StringBuilder();
		conteudoEmail.append("Código do Processo: ");
		conteudoEmail.append(proposta.getChavePrimaria());
		conteudoEmail.append("\r\n");
		conteudoEmail.append("Descrição do Processo: ");
		conteudoEmail.append("Envio da Proposta" + "\r\n");

		return conteudoEmail.toString();
	}

	private String nomeArquivoEmailRelatorioProposta(Proposta proposta) {

		StringBuilder sb = new StringBuilder();
		sb.append("Relatório Proposta");
		sb.append(String.valueOf(proposta.getChavePrimaria()));
		sb.append(Constantes.EXTENSAO_ARQUIVO_PDF);

		return sb.toString();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#obterSituacaoPropostaPorDescricao(java.lang.String)
	 */
	@Override
	public SituacaoProposta obterSituacaoPropostaPorDescricao(String descricao) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeSituacaoProposta().getSimpleName());
		hql.append(" where ");
		hql.append(" upper(descricao) = upper(:DESCRICAO) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("DESCRICAO", descricao);

		return (SituacaoProposta) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.ControladorProposta#calcularValoresProposta(br.com.ggas.cadastro.levantamentomercado.dominio.
	 * LevantamentoMercado)
	 */
	@Override
	public Map<String, BigDecimal> calcularValoresProposta(LevantamentoMercado levantamentoMercado) {

		Map<String, BigDecimal> valores = new HashMap<>();
		BigDecimal consumoTodosCombustiveisMensal = new BigDecimal(0);
		BigDecimal quantidadeDiasMes = new BigDecimal(TRINTA_DIAS);
		BigDecimal precoTotal = new BigDecimal(0);
		BigDecimal restoDivisao = BigDecimal.ZERO;
		BigDecimal valorTotalMensal = BigDecimal.ZERO;
		if(!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())){
			for(LevantamentoMercadoTipoCombustivel levCombustivel: levantamentoMercado.getListaTipoCombustivel()){
				if(levCombustivel.getPeriodicidade() != null){
					restoDivisao = quantidadeDiasMes.divide(new BigDecimal(levCombustivel.
									getPeriodicidade().getQuantidadeDias()), ESCALA_DIVISAO_DIAS_MES, RoundingMode.HALF_UP);
				}

				if(levCombustivel.getConsumo() != null){
					consumoTodosCombustiveisMensal = consumoTodosCombustiveisMensal.add(levCombustivel.getConsumo().multiply(restoDivisao));
				}

				if(levCombustivel.getPreco() != null){
					precoTotal = precoTotal.add(levCombustivel.getPreco());
				}

				if(levCombustivel.getPreco() != null && levCombustivel.getConsumo() != null){
					valorTotalMensal = valorTotalMensal.add(levCombustivel.getPreco().multiply(levCombustivel.getConsumo()));
				}
			}

			valores.put(CONSUMO_MEDIO_MENSAL_TOTAL, consumoTodosCombustiveisMensal);
			valores.put(CONSUMO_MEDIO_ANUAL_TOTAL, consumoTodosCombustiveisMensal.multiply(new BigDecimal(QTD_MESES_ANO)));
			valores.put(PRECO_TOTAL, precoTotal);
			valores.put(VALOR_MENSAL_TOTAL, valorTotalMensal.setScale(DUAS_CASAS_DECIMAIS, BigDecimal.ROUND_HALF_UP));
		}

		return valores;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#inserirNumeroAnoProposta(br.com.ggas.contrato.proposta.Proposta)
	 */
	@Override
	public void inserirNumeroAnoProposta(Proposta proposta) throws ConcorrenciaException, NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ParametroSistema parametroSequenciaProposta = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.SEQUENCIA_NUMERO_PROPOSTA);
		Integer valorSequenciaContrato = Integer.valueOf(parametroSequenciaProposta.getValor());

		if (valorSequenciaContrato != null) {
			int proximo = Integer.valueOf(valorSequenciaContrato + 1);
			proposta.setNumeroProposta(String.valueOf(proximo));
			parametroSequenciaProposta.setValor(String.valueOf(proximo));
			controladorParametroSistema.atualizar(parametroSequenciaProposta);
		}
		Calendar cal = Calendar.getInstance();

		int ano = cal.get(Calendar.YEAR);
		proposta.setAnoProposta(ano);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#converterPropostaTOemProposta(br.com.ggas.webservice.PropostaTO)
	 */
	@Override
	public Proposta converterPropostaTOemProposta(PropostaTO propostaTO) throws GGASException {

		Proposta proposta = null;
		if (propostaTO.getChavePrimaria() > 0) {
			proposta = fachada.buscarPropostaPorChave(propostaTO.getChavePrimaria());
		} else {
			proposta = fachada.criarProposta();
		}

		proposta.setHabilitado(propostaTO.isHabilitado());

		if (!StringUtils.isEmpty(propostaTO.getNumeroProposta())) {
			proposta.setNumeroProposta(propostaTO.getNumeroProposta());
		}

		if (!StringUtils.isEmpty(propostaTO.getVersaoProposta())) {
			proposta.setVersaoProposta(Integer.valueOf(propostaTO.getVersaoProposta()));
		}

		if (!StringUtils.isEmpty(propostaTO.getDataEmissao())) {
			proposta.setDataEmissao(Util.converterCampoStringParaData("Data Emissão", propostaTO.getDataEmissao(),
							Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(propostaTO.getDataVigencia())) {
			proposta.setDataVigencia(Util.converterCampoStringParaData("Data Emissão", propostaTO.getDataVigencia(),
							Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(propostaTO.getDataEntrega())) {
			proposta.setDataEntrega(Util.converterCampoStringParaData("Data Entrega", propostaTO.getDataEntrega(),
							Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(propostaTO.getPercentualTIR())) {
			proposta.setPercentualTIR(Util.converterCampoStringParaValorBigDecimal("Percentual TIR", propostaTO.getPercentualTIR(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorMaterial())) {
			proposta.setValorMaterial(Util.converterCampoStringParaValorBigDecimal("Valor Material", propostaTO.getValorMaterial(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorMaoDeObra())) {
			proposta.setValorMaoDeObra(Util.converterCampoStringParaValorBigDecimal("Valor Mão de Obra", propostaTO.getValorMaoDeObra(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorInvestimento())) {
			proposta.setValorInvestimento(Util.converterCampoStringParaValorBigDecimal("Valor Investimento",
							propostaTO.getValorInvestimento(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		proposta.setIndicadorParticipacao(propostaTO.isIndicadorParticipacao());

		if (!StringUtils.isEmpty(propostaTO.getPercentualCliente())) {
			proposta.setPercentualCliente(Util.converterCampoStringParaValorBigDecimal("Percentual Cliente",
							propostaTO.getPercentualCliente(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getQuantidadeParcela())) {
			proposta.setQuantidadeParcela(Integer.valueOf(propostaTO.getQuantidadeParcela()));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorParcela())) {
			proposta.setValorParcela(Util.converterCampoStringParaValorBigDecimal("Valor Parcela", propostaTO.getValorParcela(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getPercentualJuros())) {
			proposta.setPercentualJuros(Util.converterCampoStringParaValorBigDecimal("Percentual Juros", propostaTO.getPercentualJuros(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getConsumoMedioAnual())) {
			proposta.setConsumoMedioAnual(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Anual",
							propostaTO.getConsumoMedioAnual(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getConsumoMedioMensal())) {
			proposta.setConsumoMedioMensal(Util.converterCampoStringParaValorBigDecimal("COnsumo Médio Mensal",
							propostaTO.getConsumoMedioMensal(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getConsumoUnidadeConsumidora())) {
			proposta.setConsumoUnidadeConsumidora(Util.converterCampoStringParaValorBigDecimal("Consumo Unidades Consumidoras",
							propostaTO.getConsumoUnidadeConsumidora(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getAnoMesReferenciaPreco())) {
			proposta.setAnoMesReferenciaPreco(Integer.valueOf(propostaTO.getAnoMesReferenciaPreco()));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorGastoMensal())) {
			proposta.setValorGastoMensal(Util.converterCampoStringParaValorBigDecimal("Valor gasto Mensal",
							propostaTO.getValorGastoMensal(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (propostaTO.getSituacaoProposta() != null) {
			proposta.setSituacaoProposta(fachada.obterSituacaoProposta(propostaTO.getSituacaoProposta().getChavePrimaria()));
		}

		if (propostaTO.getImovelTO() != null) {
			proposta.setImovel(fachada.buscarImovelPorChave(propostaTO.getImovelTO().getChavePrimaria()));
		}

		if (propostaTO.getUnidadeConsumoAnual() != null) {
			proposta.setUnidadeConsumoAnual(fachada.obterUnidadeMedicao(propostaTO.getUnidadeConsumoAnual().getChavePrimaria()));
		}

		if (propostaTO.getUnidadeConsumoMensal() != null) {
			proposta.setUnidadeConsumoMensal(fachada.obterUnidadeMedicao(propostaTO.getUnidadeConsumoMensal().getChavePrimaria()));
		}

		if (!StringUtils.isEmpty(propostaTO.getNomePlanilha())) {
			proposta.setNomePlanilha(propostaTO.getNomePlanilha());
		}

		converterPropostaTOEmPropostaPasso2(propostaTO, proposta);

		return proposta;
	}

	/**
	 * Converter proposta to em proposta passo2.
	 *
	 * @param propostaTO
	 *            the proposta to
	 * @param proposta
	 *            the proposta
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void converterPropostaTOEmPropostaPasso2(PropostaTO propostaTO, Proposta proposta) throws GGASException {

		if (!StringUtils.isEmpty(propostaTO.getTipoPlanilha())) {
			proposta.setTipoPlanilha(propostaTO.getTipoPlanilha());
		}

		if (!StringUtils.isEmpty(propostaTO.getEconomiaMensalGN())) {
			proposta.setEconomiaMensalGN(Util.converterCampoStringParaValorBigDecimal("Economia Mensal", propostaTO.getEconomiaMensalGN(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getEconomiaAnualGN())) {
			proposta.setEconomiaAnualGN(Util.converterCampoStringParaValorBigDecimal("Economia Anual", propostaTO.getEconomiaAnualGN(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getPercentualEconomia())) {
			proposta.setPercentualEconomia(Util.converterCampoStringParaValorBigDecimal("Percentual Economia",
							propostaTO.getPercentualEconomia(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getComentario())) {
			proposta.setComentario(proposta.getComentario());
		}

		if (!StringUtils.isEmpty(propostaTO.getDataApresentacaoCondominio())) {
			proposta.setDataApresentacaoCondominio(Util.converterCampoStringParaData("Data Apresentação Condomínio",
							propostaTO.getDataApresentacaoCondominio(), Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(propostaTO.getDataEleicaoSindico())) {
			proposta.setDataEleicaoSindico(Util.converterCampoStringParaData("Data Eleição Síndico", propostaTO.getDataEleicaoSindico(),
							Constantes.FORMATO_DATA_BR));
		}

		if (propostaTO.getPeriodicidadeJuros() != null) {
			proposta.setPeriodicidadeJuros(fachada.obterEntidadeConteudo(propostaTO.getPeriodicidadeJuros().getChavePrimaria()));
		}

		proposta.setIndicadorMedicao(propostaTO.isIndicadorMedicao());

		if (!StringUtils.isEmpty(propostaTO.getQuantidadeApartamentos())) {
			proposta.setQuantidadeApartamentos(Integer.valueOf(propostaTO.getQuantidadeApartamentos()));
		}

		if (!StringUtils.isEmpty(propostaTO.getConsumoMedioMensalGN())) {
			proposta.setConsumoMedioMensalGN(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Mensal GN",
							propostaTO.getConsumoMedioMensalGN(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getConsumoMedioAnualGN())) {
			proposta.setConsumoMedioAnualGN(Util.converterCampoStringParaValorBigDecimal("Consumo Médio Anual GN",
							propostaTO.getConsumoMedioAnualGN(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getVolumeDiarioEstimadoGN())) {
			proposta.setVolumeDiarioEstimadoGN(Util.converterCampoStringParaValorBigDecimal("Volume Diário Estimado GN",
							propostaTO.getVolumeDiarioEstimadoGN(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (propostaTO.getSegmento() != null) {
			proposta.setSegmento(fachada.buscarSegmentoChave(propostaTO.getSegmento().getChavePrimaria()));
		}

		converterPropostaTOEmPropostaPasso3(propostaTO, proposta);
	}

	/**
	 * Converter proposta to em proposta passo3.
	 *
	 * @param propostaTO
	 *            the proposta to
	 * @param proposta
	 *            the proposta
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void converterPropostaTOEmPropostaPasso3(PropostaTO propostaTO, Proposta proposta) throws GGASException {

		if (!StringUtils.isEmpty(propostaTO.getValorMensalGastoGN())) {
			proposta.setValorMensalGastoGN(Util.converterCampoStringParaValorBigDecimal("Valor Mensal Gasto GN",
							propostaTO.getValorMensalGastoGN(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorParticipacaoCDL())) {
			proposta.setValorParticipacaoCDL(Util.converterCampoStringParaValorBigDecimal("Valor Participação da CDL",
							propostaTO.getValorParticipacaoCDL(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (proposta.getModalidadeUso() != null) {
			proposta.setModalidadeUso(fachada.obterEntidadeConteudo(propostaTO.getModalidadeUso().getChavePrimaria()));
		}

		if (propostaTO.getTarifa() != null) {
			proposta.setTarifa(fachada.obterTarifa(propostaTO.getTarifa().getChavePrimaria()));
		}

		if (!StringUtils.isEmpty(propostaTO.getNumeroCompletoProposta())) {
			proposta.setNumeroCompletoProposta(propostaTO.getNumeroCompletoProposta());
		}

		if (!StringUtils.isEmpty(propostaTO.getAnoProposta())) {
			proposta.setAnoProposta(Integer.valueOf(propostaTO.getAnoProposta()));
		}

		if (!StringUtils.isEmpty(propostaTO.getValorMedioGN())) {
			proposta.setValorMedioGN(Util.converterCampoStringParaValorBigDecimal("Valor Médio GN", propostaTO.getValorMedioGN(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (propostaTO.getFiscal() != null) {
			proposta.setFiscal(fachada.buscarFuncionarioPorChave(propostaTO.getFiscal().getChavePrimaria()));
		}

		if (propostaTO.getVendedor() != null) {
			proposta.setVendedor(fachada.buscarFuncionarioPorChave(propostaTO.getVendedor().getChavePrimaria()));
		}

		proposta.setHabilitado(propostaTO.isHabilitado());

		proposta.setUltimaAlteracao(new Date());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.ControladorProposta#converterPropostaEmPropostaTO(br.com.ggas.contrato.proposta.Proposta)
	 */
	@Override
	public PropostaTO converterPropostaEmPropostaTO(Proposta proposta) throws GGASException {

		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();

		PropostaTO propostaTO = new PropostaTO();

		if (proposta.getChavePrimaria() > 0) {
			propostaTO.setChavePrimaria(proposta.getChavePrimaria());
		}

		propostaTO.setHabilitado(proposta.isHabilitado());

		if (!StringUtils.isEmpty(proposta.getNumeroProposta())) {
			propostaTO.setNumeroProposta(proposta.getNumeroProposta());
		}

		if (proposta.getVersaoProposta() != null) {
			propostaTO.setVersaoProposta(String.valueOf(proposta.getVersaoProposta()));
		}

		if (proposta.getDataEmissao() != null) {
			propostaTO.setDataEmissao(Util.converterDataParaString(proposta.getDataEmissao()));
		}

		if (proposta.getDataVigencia() != null) {
			propostaTO.setDataVigencia(Util.converterDataParaString(proposta.getDataVigencia()));
		}

		if (proposta.getDataEntrega() != null) {
			propostaTO.setDataEntrega(Util.converterDataParaString(proposta.getDataEntrega()));
		}

		if (proposta.getPercentualTIR() != null) {
			propostaTO.setPercentualTIR(Util.converterCampoValorDecimalParaStringComCasasDecimais("Percentual TIR",
							proposta.getPercentualTIR(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getValorMaterial() != null) {
			propostaTO.setValorMaterial(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Material",
							proposta.getValorMaterial(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getValorMaoDeObra() != null) {
			propostaTO.setValorMaoDeObra(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Mão de Obra",
							proposta.getValorMaoDeObra(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getValorInvestimento() != null) {
			propostaTO.setValorInvestimento(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Investimento",
							proposta.getValorInvestimento(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		proposta.setIndicadorParticipacao(propostaTO.isIndicadorParticipacao());

		if (proposta.getPercentualCliente() != null) {
			propostaTO.setPercentualCliente(Util.converterCampoValorDecimalParaStringComCasasDecimais("Percentual Cliente",
							proposta.getPercentualCliente(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getQuantidadeParcela() != null) {
			propostaTO.setQuantidadeParcela(String.valueOf(proposta.getQuantidadeParcela()));
		}

		if (proposta.getValorParcela() != null) {
			propostaTO.setValorParcela(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Parcela",
							proposta.getValorParcela(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getPercentualJuros() != null) {
			propostaTO.setPercentualJuros(Util.converterCampoValorDecimalParaStringComCasasDecimais("Percentual Juros",
							proposta.getPercentualJuros(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getConsumoMedioAnual() != null) {
			propostaTO.setConsumoMedioAnual(Util.converterCampoValorDecimalParaStringComCasasDecimais("Consumo Médio Anual",
							proposta.getConsumoMedioAnual(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getConsumoMedioMensal() != null) {
			propostaTO.setConsumoMedioMensal(Util.converterCampoValorDecimalParaStringComCasasDecimais("Consumo Médio Mensal",
							proposta.getConsumoMedioMensal(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getConsumoUnidadeConsumidora() != null) {
			propostaTO.setConsumoUnidadeConsumidora(Util.converterCampoValorDecimalParaStringComCasasDecimais(
							"Consumo Unidades Consumidoras", proposta.getConsumoUnidadeConsumidora(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getAnoMesReferenciaPreco() != null) {
			propostaTO.setAnoMesReferenciaPreco(propostaTO.getAnoMesReferenciaPreco());
		}

		if (proposta.getValorGastoMensal() != null) {
			propostaTO.setValorGastoMensal(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Gasto Mensal",
							proposta.getValorGastoMensal(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getSituacaoProposta() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getSituacaoProposta().getChavePrimaria());
			combo.setDescricao(proposta.getSituacaoProposta().getDescricao());
			propostaTO.setSituacaoProposta(combo);
		}

		if (proposta.getImovel() != null) {
			ImovelTO imovelTO = controladorImovel.converterImovelEmImovelTO(proposta.getImovel());
			propostaTO.setImovelTO(imovelTO);
		}

		if (proposta.getUnidadeConsumoAnual() != null) {
			TipoComboBoxTO tipoComboBoxTO = new TipoComboBoxTO();
			tipoComboBoxTO.setChavePrimaria(proposta.getUnidadeConsumoAnual().getChavePrimaria());
			tipoComboBoxTO.setDescricao(proposta.getUnidadeConsumoAnual().getDescricao());
			propostaTO.setUnidadeConsumoAnual(tipoComboBoxTO);
		}

		if (proposta.getUnidadeConsumoMensal() != null) {
			TipoComboBoxTO tipoComboBoxTO = new TipoComboBoxTO();
			tipoComboBoxTO.setChavePrimaria(proposta.getUnidadeConsumoMensal().getChavePrimaria());
			tipoComboBoxTO.setDescricao(proposta.getUnidadeConsumoMensal().getDescricao());
			propostaTO.setUnidadeConsumoMensal(tipoComboBoxTO);
		}

		if (!StringUtils.isEmpty(proposta.getNomePlanilha())) {
			propostaTO.setNomePlanilha(proposta.getNomePlanilha());
		}

		converterPropostaEmPropostaTOPasso2(propostaTO, proposta);

		return propostaTO;
	}

	/**
	 * Converter proposta em proposta to passo2.
	 *
	 * @param propostaTO
	 *            the proposta to
	 * @param proposta
	 *            the proposta
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void converterPropostaEmPropostaTOPasso2(PropostaTO propostaTO, Proposta proposta) throws GGASException {

		if (!StringUtils.isEmpty(proposta.getTipoPlanilha())) {
			propostaTO.setTipoPlanilha(proposta.getTipoPlanilha());
		}

		if (proposta.getEconomiaMensalGN() != null) {
			propostaTO.setEconomiaMensalGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Economia Mensal GN",
							proposta.getEconomiaMensalGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getEconomiaAnualGN() != null) {
			propostaTO.setEconomiaAnualGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Gasto Anual",
							proposta.getEconomiaAnualGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getPercentualEconomia() != null) {
			propostaTO.setPercentualEconomia(Util.converterCampoValorDecimalParaStringComCasasDecimais("Percentual Economia",
							proposta.getPercentualEconomia(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (!StringUtils.isEmpty(proposta.getComentario())) {
			propostaTO.setComentario(proposta.getComentario());
		}

		if (proposta.getDataApresentacaoCondominio() != null) {
			propostaTO.setDataApresentacaoCondominio(Util.converterDataParaString(proposta.getDataApresentacaoCondominio()));
		}

		if (proposta.getDataEleicaoSindico() != null) {
			propostaTO.setDataEleicaoSindico(Util.converterDataParaString(proposta.getDataEleicaoSindico()));
		}

		if (proposta.getPeriodicidadeJuros() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getPeriodicidadeJuros().getChavePrimaria());
			combo.setDescricao(proposta.getPeriodicidadeJuros().getDescricao());
			propostaTO.setPeriodicidadeJuros(combo);
		}

		proposta.setIndicadorMedicao(propostaTO.isIndicadorMedicao());

		if (proposta.getQuantidadeApartamentos() != null) {
			propostaTO.setQuantidadeApartamentos(String.valueOf(propostaTO.getQuantidadeApartamentos()));
		}

		if (proposta.getConsumoMedioMensalGN() != null) {
			propostaTO.setConsumoMedioMensalGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Consumo Médio Mensal ",
							proposta.getConsumoMedioMensalGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getConsumoMedioAnualGN() != null) {
			propostaTO.setConsumoMedioAnualGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Médio Anul",
							proposta.getConsumoMedioAnualGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getVolumeDiarioEstimadoGN() != null) {
			propostaTO.setVolumeDiarioEstimadoGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Volume Diário estimado GN",
							proposta.getVolumeDiarioEstimadoGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getSegmento() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getSegmento().getChavePrimaria());
			combo.setDescricao(proposta.getSegmento().getDescricao());
			propostaTO.setSegmento(combo);
		}

		converterPropostaEmPropostaTO(propostaTO, proposta);
	}

	/**
	 * Converter proposta em proposta to.
	 *
	 * @param propostaTO
	 *            the proposta to
	 * @param proposta
	 *            the proposta
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void converterPropostaEmPropostaTO(PropostaTO propostaTO, Proposta proposta) throws GGASException {

		if (proposta.getValorMensalGastoGN() != null) {
			propostaTO.setValorMensalGastoGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Mensal Gasto GN",
							proposta.getValorMensalGastoGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getValorParticipacaoCDL() != null) {
			propostaTO.setValorParticipacaoCDL(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Participação CDL",
							proposta.getValorParticipacaoCDL(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getModalidadeUso() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getModalidadeUso().getChavePrimaria());
			combo.setDescricao(proposta.getModalidadeUso().getDescricao());
			propostaTO.setModalidadeUso(combo);
		}

		if (proposta.getTarifa() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getTarifa().getChavePrimaria());
			combo.setDescricao(proposta.getTarifa().getDescricao());
			propostaTO.setTarifa(combo);
		}

		if (!StringUtils.isEmpty(proposta.getNumeroCompletoProposta())) {
			propostaTO.setNumeroCompletoProposta(proposta.getNumeroCompletoProposta());
		}

		if (proposta.getAnoProposta() != null) {
			propostaTO.setAnoProposta(String.valueOf(proposta.getAnoProposta()));
		}

		if (proposta.getValorMedioGN() != null) {
			propostaTO.setValorMedioGN(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor Médio GN",
							proposta.getValorMedioGN(), Constantes.LOCALE_PADRAO, QTD_CASAS_DECIMAIS));
		}

		if (proposta.getFiscal() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getFiscal().getChavePrimaria());
			combo.setDescricao(proposta.getFiscal().getNome());
			propostaTO.setFiscal(combo);
		}

		if (proposta.getVendedor() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(proposta.getVendedor().getChavePrimaria());
			combo.setDescricao(proposta.getVendedor().getNome());
			propostaTO.setVendedor(combo);
		}

		propostaTO.setHabilitado(propostaTO.isHabilitado());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.contrato.proposta. ControladorProposta
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Proposta> consultarPropostas(FiltroProposta filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			if ((filtro.getDescricaoImovel() != null && !filtro.getDescricaoImovel().isEmpty())
					|| (filtro.getMatriculaImovel() != null && !filtro.getMatriculaImovel().isEmpty()
							&& Util.isNumero(filtro.getMatriculaImovel()))
					|| (filtro.getNomeCliente() != null && !filtro.getNomeCliente().isEmpty())
					|| (filtro.getCepImovel() != null && !filtro.getCepImovel().isEmpty())
					|| (filtro.getCpfCnpj() != null && !filtro.getCpfCnpj().isEmpty())) {
				criteria.createAlias("imovel", "imovel");
			}

			if ((filtro.getDescricaoImovel() != null && !filtro.getDescricaoImovel().isEmpty())
					|| (filtro.getCepImovel() != null && !filtro.getCepImovel().isEmpty())) {
				criteria.createAlias("imovel.listaPontoConsumo", "ponto_consumo");

			}

			if (filtro.getDescricaoImovel() != null && !filtro.getDescricaoImovel().isEmpty()) {

				Disjunction disjunction = Restrictions.disjunction();
				disjunction.add(Restrictions.ilike("imovel.nome", filtro.getDescricaoImovel(), MatchMode.ANYWHERE));
				disjunction.add(Restrictions.ilike("ponto_consumo.descricao", filtro.getDescricaoImovel(), MatchMode.ANYWHERE));
				criteria.add(disjunction);
			}


			if (filtro.getCepImovel() != null && !filtro.getCepImovel().isEmpty()) {
				criteria.createAlias("ponto_consumo.cep", "cep");

				criteria.add(Restrictions.eq("cep.cep", filtro.getCepImovel()));
			}

			if (filtro.getDataInicio() != null || filtro.getDataFim() != null) {

				Date dataInicio = new Date(0);
				Date dataFim = new Date();
				if (filtro.getDataInicio() != null) {
					dataInicio = filtro.getDataInicio();
				}
				if (filtro.getDataFim() != null) {
					dataFim = filtro.getDataFim();
				}

				criteria.add(Restrictions.between("dataEmissao", dataInicio, dataFim));

			}

			if (filtro.getNumeroProposta() != null && !filtro.getNumeroProposta().isEmpty()) {
				criteria.add(Restrictions.ilike(NUMERO_COMPLETO_PROPOSTA, filtro.getNumeroProposta(), MatchMode.ANYWHERE));
			}

			if ((filtro.getCpfCnpj() != null && !filtro.getCpfCnpj().isEmpty())
					|| (filtro.getNomeCliente() != null && !filtro.getNomeCliente().isEmpty())) {

				criteria.createAlias("imovel.listaClienteImovel", "cliente_imovel");
				criteria.createAlias("cliente_imovel.cliente", CLIENTE);
			}

			if (filtro.getCpfCnpj() != null && !filtro.getCpfCnpj().isEmpty()) {

				Disjunction disjunction = Restrictions.disjunction();
				disjunction.add(Restrictions.ilike("cliente.cpf", filtro.getCpfCnpj(), MatchMode.ANYWHERE));
				disjunction.add(Restrictions.ilike("cliente.cnpj", filtro.getCpfCnpj(), MatchMode.ANYWHERE));
				criteria.add(disjunction);
			}

			if (filtro.getNomeCliente() != null && !filtro.getNomeCliente().isEmpty()) {
				criteria.add(Restrictions.ilike("cliente.nome", filtro.getNomeCliente(), MatchMode.ANYWHERE));
			}

			Boolean habilitado = filtro.getHabilitado();
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

		}
		criteria.setFetchMode("situacaoProposta", FetchMode.JOIN);

		criteria.addOrder(Order.asc(NUMERO_PROPOSTA));
		criteria.addOrder(Order.asc("versaoProposta"));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#remover(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void remover(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
		Collection<Proposta> consultarPropostas = consultarPropostas(filtro);
		for (Proposta proposta : consultarPropostas) {
			proposta.setDadosAuditoria(dadosAuditoria);
			super.remover(proposta, false);
		}
	}
}
