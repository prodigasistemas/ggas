/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.proposta.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.contrato.proposta.SituacaoProposta;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos relacionados a Proposta  
 * 
 */
public class PropostaImpl extends EntidadeNegocioImpl implements Proposta {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -8922998103535860639L;

	private String numeroProposta;

	private Integer versaoProposta;

	private Date dataEmissao;

	private Date dataVigencia;

	private Date dataEntrega;

	private BigDecimal percentualTIR;

	private BigDecimal valorMaterial;

	private BigDecimal valorMaoDeObra;

	private BigDecimal valorInvestimento;

	private boolean indicadorParticipacao;

	private BigDecimal percentualCliente;

	private BigDecimal valorCliente;

	private Integer quantidadeParcela;

	private BigDecimal valorParcela;

	private BigDecimal percentualJuros;

	private BigDecimal consumoMedioAnual;

	private BigDecimal consumoMedioMensal;

	private BigDecimal consumoUnidadeConsumidora;

	private Integer anoMesReferenciaPreco;

	private BigDecimal valorGastoMensal;

	private SituacaoProposta situacaoProposta;

	private Imovel imovel;

	private Unidade unidadeConsumoAnual;

	private Unidade unidadeConsumoMensal;

	private byte[] planilha;

	private String nomePlanilha;

	private String tipoPlanilha;

	private BigDecimal economiaMensalGN;

	private BigDecimal economiaAnualGN;

	private BigDecimal percentualEconomia;

	private String comentario;

	private Date dataApresentacaoCondominio;

	private Date dataEleicaoSindico;

	private EntidadeConteudo periodicidadeJuros;

	private Boolean indicadorMedicao;

	private Integer quantidadeApartamentos;

	private BigDecimal consumoMedioMensalGN;

	private BigDecimal consumoMedioAnualGN;

	private BigDecimal volumeDiarioEstimadoGN;

	private Segmento segmento;

	private BigDecimal valorMensalGastoGN;

	private BigDecimal valorParticipacaoCDL;

	private Funcionario fiscal;

	private Funcionario vendedor;

	private EntidadeConteudo modalidadeUso;

	private Tarifa tarifa;

	private String numeroCompletoProposta;

	private Integer anoProposta;

	private BigDecimal valorMedioGN;
	
	private Cliente cliente;
	
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorParticipacaoCDL()
	 */
	@Override
	public BigDecimal getValorParticipacaoCDL() {

		return valorParticipacaoCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorParticipacaoCDL
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorParticipacaoCDL(BigDecimal valorParticipacaoCDL) {

		this.valorParticipacaoCDL = valorParticipacaoCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorMensalGastoGN()
	 */
	@Override
	public BigDecimal getValorMensalGastoGN() {

		return valorMensalGastoGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorMensalGastoGN(java.math.BigDecimal)
	 */
	@Override
	public void setValorMensalGastoGN(BigDecimal valorMensalGastoGN) {

		this.valorMensalGastoGN = valorMensalGastoGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getIndicadorMedicao()
	 */
	@Override
	public Boolean getIndicadorMedicao() {

		return indicadorMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setIndicadorMedicao(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorMedicao(Boolean indicadorMedicao) {

		this.indicadorMedicao = indicadorMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getQuantidadeApartamentos()
	 */
	@Override
	public Integer getQuantidadeApartamentos() {

		return quantidadeApartamentos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setQuantidadeApartamentos
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeApartamentos(Integer quantidadeApartamentos) {

		this.quantidadeApartamentos = quantidadeApartamentos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getConsumoMedioMensalGN()
	 */
	@Override
	public BigDecimal getConsumoMedioMensalGN() {

		return consumoMedioMensalGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setConsumoMedioMensalGN
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMedioMensalGN(BigDecimal consumoMedioMensalGN) {

		this.consumoMedioMensalGN = consumoMedioMensalGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getConsumoMedioAnualGN()
	 */
	@Override
	public BigDecimal getConsumoMedioAnualGN() {

		return consumoMedioAnualGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setConsumoMedioAnualGN
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMedioAnualGN(BigDecimal consumoMedioAnualGN) {

		this.consumoMedioAnualGN = consumoMedioAnualGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getVolumeEstimadoGN()
	 */
	@Override
	public BigDecimal getVolumeDiarioEstimadoGN() {

		return volumeDiarioEstimadoGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setVolumeEstimadoGN(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeDiarioEstimadoGN(BigDecimal volumeDiarioEstimadoGN) {

		this.volumeDiarioEstimadoGN = volumeDiarioEstimadoGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setSegmento
	 * (br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getPeriodicidadeJuros()
	 */
	@Override
	public EntidadeConteudo getPeriodicidadeJuros() {

		return periodicidadeJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setPeriodicidadeJuros
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setPeriodicidadeJuros(EntidadeConteudo periodicidadeJuros) {

		this.periodicidadeJuros = periodicidadeJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.impl.Proposta
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(numeroProposta)) {
			stringBuilder.append(NUMERO_PROPOSTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (this.versaoProposta == null || this.versaoProposta <= 0) {
			stringBuilder.append(VERSAO_PROPOSTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (this.dataEmissao == null) {
			stringBuilder.append(DATA_EMISSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (this.imovel == null) {
			stringBuilder.append(IMOVEL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (this.indicadorParticipacao) {
			if (this.percentualCliente == null) {
				stringBuilder.append(PERCENTUAL_CLIENTE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (this.valorCliente == null) {
				stringBuilder.append(VALOR_CLIENTE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (this.periodicidadeJuros == null) {
				stringBuilder.append(PERIODICIDADE_JUROS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getNumeroProposta()
	 */
	@Override
	public String getNumeroProposta() {

		return numeroProposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setNumeroProposta(java.lang.String)
	 */
	@Override
	public void setNumeroProposta(String numeroProposta) {

		this.numeroProposta = numeroProposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getVersaoProposta()
	 */
	@Override
	public Integer getVersaoProposta() {

		return versaoProposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setVersaoProposta(java.lang.Integer)
	 */
	@Override
	public void setVersaoProposta(Integer versaoProposta) {

		this.versaoProposta = versaoProposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getDataEmissao()
	 */
	@Override
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setDataEmissao(java.util.Date)
	 */
	@Override
	public void setDataEmissao(Date dataEmissao) {
		if(dataEmissao != null){
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getDataVigencia()
	 */
	@Override
	public Date getDataVigencia() {
		Date data = null;
		if (this.dataVigencia != null) {
			data = (Date) dataVigencia.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setDataVigencia(java.util.Date)
	 */
	@Override
	public void setDataVigencia(Date dataVigencia) {
		if(dataVigencia != null){
			this.dataVigencia = (Date) dataVigencia.clone();
		} else {
			this.dataVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.proposta.Proposta# getDataEntrega()
	 */
	@Override
	public Date getDataEntrega() {
		Date data = null;
		if (this.dataEntrega != null) {
			data = (Date) dataEntrega.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setDataEntrega(java.util.Date)
	 */
	@Override
	public void setDataEntrega(Date dataEntrega) {
		if(dataEntrega != null){
			this.dataEntrega = (Date) dataEntrega.clone();
		} else {
			this.dataEntrega = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getPercentualTIR()
	 */
	@Override
	public BigDecimal getPercentualTIR() {

		return percentualTIR;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setPercentualTIR(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualTIR(BigDecimal percentualTIR) {

		this.percentualTIR = percentualTIR;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorMaterial()
	 */
	@Override
	public BigDecimal getValorMaterial() {

		return valorMaterial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorMaterial(java.math.BigDecimal)
	 */
	@Override
	public void setValorMaterial(BigDecimal valorMaterial) {

		this.valorMaterial = valorMaterial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorMaoDeObra()
	 */
	@Override
	public BigDecimal getValorMaoDeObra() {

		return valorMaoDeObra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorMaoDeObra(java.math.BigDecimal)
	 */
	@Override
	public void setValorMaoDeObra(BigDecimal valorMaoDeObra) {

		this.valorMaoDeObra = valorMaoDeObra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorInvestimento()
	 */
	@Override
	public BigDecimal getValorInvestimento() {

		return valorInvestimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorInvestimento(java.math.BigDecimal)
	 */
	@Override
	public void setValorInvestimento(BigDecimal valorInvestimento) {

		this.valorInvestimento = valorInvestimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * isIndicadorParticipacao()
	 */
	@Override
	public boolean isIndicadorParticipacao() {

		return indicadorParticipacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setIndicadorParticipacao(boolean)
	 */
	@Override
	public void setIndicadorParticipacao(boolean indicadorParticipacao) {

		this.indicadorParticipacao = indicadorParticipacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getPercentualCliente()
	 */
	@Override
	public BigDecimal getPercentualCliente() {

		return percentualCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setPercentualCliente(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualCliente(BigDecimal percentualCliente) {

		this.percentualCliente = percentualCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorCliente()
	 */
	@Override
	public BigDecimal getValorCliente() {

		return valorCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorCliente(java.math.BigDecimal)
	 */
	@Override
	public void setValorCliente(BigDecimal valorCliente) {

		this.valorCliente = valorCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getQuantidadeParcela()
	 */
	@Override
	public Integer getQuantidadeParcela() {

		return quantidadeParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setQuantidadeParcela(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeParcela(Integer quantidadeParcela) {

		this.quantidadeParcela = quantidadeParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorParcela()
	 */
	@Override
	public BigDecimal getValorParcela() {

		return valorParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorParcela(java.math.BigDecimal)
	 */
	@Override
	public void setValorParcela(BigDecimal valorParcela) {

		this.valorParcela = valorParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getPercentualJuros()
	 */
	@Override
	public BigDecimal getPercentualJuros() {

		return percentualJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getConsumoMedioAnual()
	 */
	@Override
	public BigDecimal getConsumoMedioAnual() {

		return consumoMedioAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setConsumoMedioAnual(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMedioAnual(BigDecimal consumoMedioAnual) {

		this.consumoMedioAnual = consumoMedioAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getConsumoUnidadeConsumidora()
	 */
	@Override
	public BigDecimal getConsumoUnidadeConsumidora() {

		return consumoUnidadeConsumidora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setConsumoUnidadeConsumidora
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setConsumoUnidadeConsumidora(BigDecimal consumoUnidadeConsumidora) {

		this.consumoUnidadeConsumidora = consumoUnidadeConsumidora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setPercentualJuros(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualJuros(BigDecimal percentualJuros) {

		this.percentualJuros = percentualJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getAnoMesReferenciaPreco()
	 */
	@Override
	public Integer getAnoMesReferenciaPreco() {

		return anoMesReferenciaPreco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setAnoMesReferenciaPreco(java.lang.Integer)
	 */
	@Override
	public void setAnoMesReferenciaPreco(Integer anoMesReferenciaPreco) {

		this.anoMesReferenciaPreco = anoMesReferenciaPreco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getValorGastoMensal()
	 */
	@Override
	public BigDecimal getValorGastoMensal() {

		return valorGastoMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setValorGastoMensal(java.math.BigDecimal)
	 */
	@Override
	public void setValorGastoMensal(BigDecimal valorGastoMensal) {

		this.valorGastoMensal = valorGastoMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getSituacaoProposta()
	 */
	@Override
	public SituacaoProposta getSituacaoProposta() {

		return situacaoProposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setSituacaoProposta
	 * (br.com.ggas.contrato.proposta
	 * .SituacaoProposta)
	 */
	@Override
	public void setSituacaoProposta(SituacaoProposta situacaoProposta) {

		this.situacaoProposta = situacaoProposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getUnidadeConsumoAnual()
	 */
	@Override
	public Unidade getUnidadeConsumoAnual() {

		return unidadeConsumoAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setUnidadeConsumoAnual
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeConsumoAnual(Unidade unidadeConsumoAnual) {

		this.unidadeConsumoAnual = unidadeConsumoAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getUnidadeConsumoMensal()
	 */
	@Override
	public Unidade getUnidadeConsumoMensal() {

		return unidadeConsumoMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setUnidadeConsumoMensal
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeConsumoMensal(Unidade unidadeConsumoMensal) {

		this.unidadeConsumoMensal = unidadeConsumoMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getConsumoMedioMensal()
	 */
	@Override
	public BigDecimal getConsumoMedioMensal() {

		return consumoMedioMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setConsumoMedioMensal(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMedioMensal(BigDecimal consumoMedioMensal) {

		this.consumoMedioMensal = consumoMedioMensal;
	}

	/**
	 * @return the planilha
	 */
	@Override
	public byte[] getPlanilha() {
		byte[] retorno = null;
		if(this.planilha != null) {
			retorno = this.planilha.clone();
		}
		return retorno;
	}

	/**
	 * @param planilha
	 *            the planilha to set
	 */
	@Override
	public void setPlanilha(byte[] planilha) {
		if (planilha != null) {
			this.planilha = planilha.clone();
		} else {
			this.planilha = null;
		}
	}

	/**
	 * @return the nomePlanilha
	 */
	@Override
	public String getNomePlanilha() {

		return nomePlanilha;
	}

	/**
	 * @param nomePlanilha
	 *            the nomePlanilha to set
	 */
	@Override
	public void setNomePlanilha(String nomePlanilha) {

		this.nomePlanilha = nomePlanilha;
	}

	/**
	 * @return the tipoPlanilha
	 */
	@Override
	public String getTipoPlanilha() {

		return tipoPlanilha;
	}

	/**
	 * @param tipoPlanilha
	 *            the tipoPlanilha to set
	 */
	@Override
	public void setTipoPlanilha(String tipoPlanilha) {

		this.tipoPlanilha = tipoPlanilha;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.proposta.Proposta#
	 * setImovel
	 * (br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/**
	 * @return the economiaMensalGN
	 */
	@Override
	public BigDecimal getEconomiaMensalGN() {

		return economiaMensalGN;
	}

	/**
	 * @param economiaMensalGN
	 *            the economiaMensalGN to set
	 */
	@Override
	public void setEconomiaMensalGN(BigDecimal economiaMensalGN) {

		this.economiaMensalGN = economiaMensalGN;
	}

	/**
	 * @return the economiaAnualGN
	 */
	@Override
	public BigDecimal getEconomiaAnualGN() {

		return economiaAnualGN;
	}

	/**
	 * @param economiaAnualGN
	 *            the economiaAnualGN to set
	 */
	@Override
	public void setEconomiaAnualGN(BigDecimal economiaAnualGN) {

		this.economiaAnualGN = economiaAnualGN;
	}

	/**
	 * @return the percentualEconomia
	 */
	@Override
	public BigDecimal getPercentualEconomia() {

		return percentualEconomia;
	}

	/**
	 * @param percentualEconomia
	 *            the percentualEconomia to set
	 */
	@Override
	public void setPercentualEconomia(BigDecimal percentualEconomia) {

		this.percentualEconomia = percentualEconomia;
	}

	/**
	 * @return the comentario
	 */
	@Override
	public String getComentario() {

		return comentario;
	}

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	@Override
	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	/**
	 * @return the dataApresentacaoCondominio
	 */
	@Override
	public Date getDataApresentacaoCondominio() {
		Date data = null;
		if(this.dataApresentacaoCondominio != null) {
			data = (Date) dataApresentacaoCondominio.clone();
		}
		return data;
	}

	/**
	 * @param dataApresentacaoCondominio
	 *            the dataApresentacaoCondominio
	 *            to set
	 */
	@Override
	public void setDataApresentacaoCondominio(Date dataApresentacaoCondominio) {
		if(dataApresentacaoCondominio != null){
			this.dataApresentacaoCondominio = (Date) dataApresentacaoCondominio.clone();
		} else {
			this.dataApresentacaoCondominio = null;
		}
	}

	/**
	 * @return the dataEleicaoSindico
	 */
	@Override
	public Date getDataEleicaoSindico() {
		Date data = null;
		if(this.dataEleicaoSindico != null) {
			data = (Date) dataEleicaoSindico.clone();
		}
		return data;
	}

	/**
	 * @param dataEleicaoSindico
	 *            the dataEleicaoSindico to set
	 */
	@Override
	public void setDataEleicaoSindico(Date dataEleicaoSindico) {
		if(dataEleicaoSindico != null){
			this.dataEleicaoSindico = (Date) dataEleicaoSindico.clone();
		} else {
			this.dataEleicaoSindico = null;
		}
	}

	@Override
	public Funcionario getFiscal() {

		return fiscal;
	}

	@Override
	public void setFiscal(Funcionario fiscal) {

		this.fiscal = fiscal;
	}

	@Override
	public Funcionario getVendedor() {

		return vendedor;
	}

	@Override
	public void setVendedor(Funcionario vendedor) {

		this.vendedor = vendedor;
	}

	@Override
	public EntidadeConteudo getModalidadeUso() {

		return modalidadeUso;
	}

	@Override
	public void setModalidadeUso(EntidadeConteudo modalidadeUso) {

		this.modalidadeUso = modalidadeUso;
	}

	@Override
	public String getNumeroCompletoProposta() {

		return numeroCompletoProposta;
	}

	@Override
	public void setNumeroCompletoProposta(String numeroCompletoProposta) {

		this.numeroCompletoProposta = numeroCompletoProposta;
	}

	@Override
	public Integer getAnoProposta() {

		return anoProposta;
	}

	@Override
	public void setAnoProposta(Integer anoProposta) {

		this.anoProposta = anoProposta;
	}

	@Override
	public BigDecimal getValorMedioGN() {

		return valorMedioGN;
	}

	@Override
	public void setValorMedioGN(BigDecimal valorMedioGN) {

		this.valorMedioGN = valorMedioGN;
	}

	@Override
	public Cliente getCliente() {
		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
