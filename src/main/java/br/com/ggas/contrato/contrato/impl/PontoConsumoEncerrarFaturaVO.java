/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

/**
 * Classe responsável pela representação da entidade  PontoConsumoEncerrarFatura.
 *
 */
public class PontoConsumoEncerrarFaturaVO {

	private Long chavePrimaria;

	private String descricao;

	private String descricaoSegmento;

	private String descricaoSituacao;

	private Long idFatura;
	
	private String dataLeitura;
	
	private String valorLeitura;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getDescricaoSegmento() {

		return descricaoSegmento;
	}

	public void setDescricaoSegmento(String descricaoSegmento) {

		this.descricaoSegmento = descricaoSegmento;
	}

	public String getDescricaoSituacao() {

		return descricaoSituacao;
	}

	public void setDescricaoSituacao(String descricaoSituacao) {

		this.descricaoSituacao = descricaoSituacao;
	}

	public Long getIdFatura() {

		return idFatura;
	}

	public void setIdFatura(Long idFatura) {

		this.idFatura = idFatura;
	}

	public String getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}

	public String getValorLeitura() {
		return valorLeitura;
	}

	public void setValorLeitura(String valorLeitura) {
		this.valorLeitura = valorLeitura;
	}
}
