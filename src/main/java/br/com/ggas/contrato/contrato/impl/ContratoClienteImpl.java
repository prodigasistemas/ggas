/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos atributos e métodos relacionados ao entre Contrato e Cliente.
 * 
 */
public class ContratoClienteImpl extends EntidadeNegocioImpl implements ContratoCliente {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 1658271200213747621L;

	private Contrato contrato;

	private Cliente cliente;

	private PontoConsumo pontoConsumo;

	private Date relacaoInicio;

	private Date relacaoFim;

	private Boolean indicadorPrincipal;

	private EntidadeConteudo responsabilidade;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente#getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente
	 * #setContrato(br.com.ggas.contrato
	 * .contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente#getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente
	 * #setCliente(br.com.ggas.cadastro
	 * .cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente#getRelacaoInicio()
	 */
	@Override
	public Date getRelacaoInicio() {
		Date data = null;
		if(relacaoInicio != null) {
			data = (Date) relacaoInicio.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente
	 * #setRelacaoInicio(java.util.Date)
	 */
	@Override
	public void setRelacaoInicio(Date relacaoInicio) {
		if(relacaoInicio != null) {
			this.relacaoInicio = (Date) relacaoInicio.clone();
		} else {
			this.relacaoInicio = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente#getRelacaoFim()
	 */
	@Override
	public Date getRelacaoFim() {
		Date data = null;
		if (relacaoFim != null) {
			data = (Date) relacaoFim.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente
	 * #setRelacaoFim(java.util.Date)
	 */
	@Override
	public void setRelacaoFim(Date relacaoFim) {
		if (relacaoFim != null) {
			this.relacaoFim = (Date) relacaoFim.clone();
		} else {
			this.relacaoFim = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente#getIndicadorPrincipal()
	 */
	@Override
	public Boolean getIndicadorPrincipal() {

		return indicadorPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoCliente
	 * #setIndicadorPrincipal(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorPrincipal(Boolean indicadorPrincipal) {

		this.indicadorPrincipal = indicadorPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(cliente == null) {
			stringBuilder.append(CONTRATO_CLIENTE_CLIENTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(responsabilidade == null) {
			stringBuilder.append(CONTRATO_CLIENTE_RESPONSABILIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(relacaoInicio == null) {
			stringBuilder.append(CONTRATO_CLIENTE_RELACAO_INCICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ContratoCliente
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ContratoCliente
	 * #
	 * setPontoConsumo(br.com.ggas.cadastro.imovel
	 * .PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ContratoCliente
	 * #getResponsabilidade()
	 */
	@Override
	public EntidadeConteudo getResponsabilidade() {

		return responsabilidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ContratoCliente
	 * #setResponsabilidade(br.com.ggas.geral.
	 * EntidadeConteudo)
	 */
	@Override
	public void setResponsabilidade(EntidadeConteudo responsabilidade) {

		this.responsabilidade = responsabilidade;
	}

}
