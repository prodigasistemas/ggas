/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSAmostragem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
/**
 * Classe responsável pelos atributos e métodos relacionados 
 * a associação entre Contrato e Ponto de Consumo. 
 *
 */
public class ContratoPontoConsumoImpl extends EntidadeNegocioImpl implements ContratoPontoConsumo {

	private static final long serialVersionUID = 2983057364068634469L;

	private BigDecimal medidaPressaoColetada;

	private Unidade unidadePressaoColetada;

	private BigDecimal medidaPressao;

	private Unidade unidadePressao;

	private BigDecimal medidaPressaoMinima;

	private Unidade unidadePressaoMinima;

	private BigDecimal medidaPressaoMaxima;

	private Unidade unidadePressaoMaxima;

	private BigDecimal medidaPressaoLimite;

	private Unidade unidadePressaoLimite;

	private PontoConsumo pontoConsumo;

	private Contrato contrato;

	private BigDecimal medidaVazaoInstantanea;

	private Unidade unidadeVazaoInstantanea;

	private BigDecimal medidaVazaoMaximaInstantanea;

	private Unidade unidadeVazaoMaximaInstantanea;

	private BigDecimal medidaVazaoMinimaInstantanea;

	private Unidade unidadeVazaoMinimaInstantanea;

	private BigDecimal medidaFornecimentoMaxDiaria;

	private Unidade unidadeFornecimentoMaxDiaria;

	private BigDecimal medidaFornecimentoMinDiaria;

	private Unidade unidadeFornecimentoMinDiaria;

	private BigDecimal medidaFornecimentoMinMensal;

	private Unidade unidadeFornecimentoMinMensal;

	private BigDecimal medidaFornecimentoMinAnual;

	private Unidade unidadeFornecimentoMinAnual;

	private BigDecimal numeroFatorCorrecao;

	private Integer numeroDiasVencimento;

	private AcaoAnormalidadeConsumo acaoAnormalidadeConsumoSemLeitura;

	private TipoMedicao tipoMedicao;

	private Boolean indicadorParadaProgramada;

	private Integer quantidadeAnosParadaCliente;

	private Integer quantidadeTotalParadaCliente;

	private Integer quantidadeMaximaAnoParadaCliente;

	private Integer diasAntecedentesParadaCliente;

	private Integer diasConsecutivosParadaCliente;

	private Integer quantidadeAnosParadaCDL;

	private Integer quantidadeTotalParadaCDL;

	private Integer quantidadeMaximaAnoParadaCDL;

	private Integer diasAntecedentesParadaCDL;

	private Integer diasConsecutivosParadaCDL;

	private Date dataInicioTeste;

	private Date dataFimTeste;

	private String numeroImovel;

	private String complementoEndereco;

	private String emailEntrega;

	private Boolean indicadorCorretorVazao;

	private BigDecimal medidaConsumoTeste;

	private Integer prazoConsumoTeste;

	private Integer diaGarantiaConversao;

	private Date dataInicioGarantiaConversao;

	private EntidadeConteudo regimeConsumo;

	private Cep cep;

	private String emailOperacional;

	private Integer codigoDDDFaxOperacional;

	private Integer numeroFaxOperacional;

	private Integer numeroHoraInicial;

	private EntidadeConteudo contratoCompra;

	private Date dataConsumoTesteExcedido;

	private BigDecimal medidaConsumoTesteUsado;

	private Periodicidade periodicidade;

	private FaixaPressaoFornecimento faixaPressaoFornecimento;

	private Boolean isEmiteNotaFiscalEletronica;

	private Boolean emitirFaturaComNfe;

	private Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new HashSet<>();

	private Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento = 
					new HashSet<>();

	private Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoConsumoPCSAmostragem = 
					new LinkedHashSet<>();

	private Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoConsumoPCSIntervalo = 
					new LinkedHashSet<>();

	@Override
	public String getEnderecoFormatado() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		
		if (this.getCep() != null && this.getCep().getTipoLogradouro() != null) {
			enderecoFormatado.append(this.getCep().getTipoLogradouro());
		}
		
		if (this.getCep() != null && this.getCep().getLogradouro() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getLogradouro());
		}
		
		if (this.getNumeroImovel() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getNumeroImovel());
		}
		
		if (this.getCep() != null && this.getCep().getBairro() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getBairro());
		}
		
		if (this.getCep() != null && this.getCep().getMunicipio() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoRuaNumeroComplemento() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if (this.getCep().getLogradouro() != null) {
			enderecoFormatado.append(this.getCep().getLogradouro());
		}
		if (this.getNumeroImovel() != null) {
			if(enderecoFormatado.length() > 0){
				separador = ", ";
			}else{
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getNumeroImovel());
		}
		if (this.getComplementoEndereco() != null) {
			if(enderecoFormatado.length() > 0){
				separador = ", ";
			}else{
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getComplementoEndereco());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoMunicipioUF() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if (this.getCep().getMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}
		if (this.getCep().getUf() != null) {
			if(enderecoFormatado.length() > 0){
				separador = " - ";
			}else{
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}

		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getEmailOperacional()
	 */
	@Override
	public String getEmailOperacional() {

		return emailOperacional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setEmailOperacional(java.lang.String)
	 */
	@Override
	public void setEmailOperacional(String emailOperacional) {

		this.emailOperacional = emailOperacional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getCodigoDDDFaxOperacional()
	 */
	@Override
	public Integer getCodigoDDDFaxOperacional() {

		return codigoDDDFaxOperacional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setCodigoDDDFaxOperacional
	 * (java.lang.Integer)
	 */
	@Override
	public void setCodigoDDDFaxOperacional(Integer codigoDDDFaxOperacional) {

		this.codigoDDDFaxOperacional = codigoDDDFaxOperacional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getNumeroFaxOperacional()
	 */
	@Override
	public Integer getNumeroFaxOperacional() {

		return numeroFaxOperacional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setNumeroFaxOperacional(java.lang.String)
	 */
	@Override
	public void setNumeroFaxOperacional(Integer numeroFaxOperacional) {

		this.numeroFaxOperacional = numeroFaxOperacional;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaPressao()
	 */
	@Override
	public BigDecimal getMedidaPressao() {

		return medidaPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaPressao(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaPressao(BigDecimal medidaPressao) {

		this.medidaPressao = medidaPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadePressao()
	 */
	@Override
	public Unidade getUnidadePressao() {

		return unidadePressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setUnidadePressao(br.com.ggas.medicao.
	 * vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressao(Unidade unidadePressao) {

		this.unidadePressao = unidadePressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setPontoConsumo(br.com.ggas.cadastro.imovel
	 * .PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setContrato(br.com.ggas.contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaVazaoMaximaInstantanea()
	 */
	@Override
	public BigDecimal getMedidaVazaoMaximaInstantanea() {

		return medidaVazaoMaximaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaVazaoMaximaInstantanea(java.math.
	 * BigDecimal)
	 */
	@Override
	public void setMedidaVazaoMaximaInstantanea(BigDecimal medidaVazaoMaximaInstantanea) {

		this.medidaVazaoMaximaInstantanea = medidaVazaoMaximaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadeVazaoMaximaInstantanea()
	 */
	@Override
	public Unidade getUnidadeVazaoMaximaInstantanea() {

		return unidadeVazaoMaximaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setUnidadeVazaoMaximaInstantanea(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeVazaoMaximaInstantanea(Unidade unidadeVazaoMaximaInstantanea) {

		this.unidadeVazaoMaximaInstantanea = unidadeVazaoMaximaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaFornecimentoMaxDiaria()
	 */
	@Override
	public BigDecimal getMedidaFornecimentoMaxDiaria() {

		return medidaFornecimentoMaxDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaFornecimentoMaxDiaria(java.math.
	 * BigDecimal)
	 */
	@Override
	public void setMedidaFornecimentoMaxDiaria(BigDecimal medidaFornecimentoMaxDiaria) {

		this.medidaFornecimentoMaxDiaria = medidaFornecimentoMaxDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadeFornecimentoMaxDiaria()
	 */
	@Override
	public Unidade getUnidadeFornecimentoMaxDiaria() {

		return unidadeFornecimentoMaxDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setUnidadeFornecimentoMaxDiaria(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeFornecimentoMaxDiaria(Unidade unidadeFornecimentoMaxDiaria) {

		this.unidadeFornecimentoMaxDiaria = unidadeFornecimentoMaxDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaFornecimentoMinDiaria()
	 */
	@Override
	public BigDecimal getMedidaFornecimentoMinDiaria() {

		return medidaFornecimentoMinDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaFornecimentoMinDiaria(java.math.
	 * BigDecimal)
	 */
	@Override
	public void setMedidaFornecimentoMinDiaria(BigDecimal medidaFornecimentoMinDiaria) {

		this.medidaFornecimentoMinDiaria = medidaFornecimentoMinDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadeFornecimentoMinDiaria()
	 */
	@Override
	public Unidade getUnidadeFornecimentoMinDiaria() {

		return unidadeFornecimentoMinDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setUnidadeFornecimentoMinDiaria(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeFornecimentoMinDiaria(Unidade unidadeFornecimentoMinDiaria) {

		this.unidadeFornecimentoMinDiaria = unidadeFornecimentoMinDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaFornecimentoMinMensal()
	 */
	@Override
	public BigDecimal getMedidaFornecimentoMinMensal() {

		return medidaFornecimentoMinMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaFornecimentoMinMensal(java.math.
	 * BigDecimal)
	 */
	@Override
	public void setMedidaFornecimentoMinMensal(BigDecimal medidaFornecimentoMinMensal) {

		this.medidaFornecimentoMinMensal = medidaFornecimentoMinMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadeFornecimentoMinMensal()
	 */
	@Override
	public Unidade getUnidadeFornecimentoMinMensal() {

		return unidadeFornecimentoMinMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setUnidadeFornecimentoMinMensal(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeFornecimentoMinMensal(Unidade unidadeFornecimentoMinMensal) {

		this.unidadeFornecimentoMinMensal = unidadeFornecimentoMinMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaFornecimentoMinAnual()
	 */
	@Override
	public BigDecimal getMedidaFornecimentoMinAnual() {

		return medidaFornecimentoMinAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaFornecimentoMinAnual(java.math.
	 * BigDecimal)
	 */
	@Override
	public void setMedidaFornecimentoMinAnual(BigDecimal medidaFornecimentoMinAnual) {

		this.medidaFornecimentoMinAnual = medidaFornecimentoMinAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadeFornecimentoMinAnual()
	 */
	@Override
	public Unidade getUnidadeFornecimentoMinAnual() {

		return unidadeFornecimentoMinAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setUnidadeFornecimentoMinAnual(br.com.ggas.
	 * medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeFornecimentoMinAnual(Unidade unidadeFornecimentoMinAnual) {

		this.unidadeFornecimentoMinAnual = unidadeFornecimentoMinAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getNumeroFatorCorrecao()
	 */
	@Override
	public BigDecimal getNumeroFatorCorrecao() {

		return numeroFatorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setNumeroFatorCorrecao(java.math.BigDecimal
	 * )
	 */
	@Override
	public void setNumeroFatorCorrecao(BigDecimal numeroFatorCorrecao) {

		this.numeroFatorCorrecao = numeroFatorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getNumeroDiasVencimento()
	 */
	@Override
	public Integer getNumeroDiasVencimento() {

		return numeroDiasVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setNumeroDiasVencimento(java.lang.Integer)
	 */
	@Override
	public void setNumeroDiasVencimento(Integer numeroDiasVencimento) {

		this.numeroDiasVencimento = numeroDiasVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getAcaoAnormalidadeConsumoSemLeitura()
	 */
	@Override
	public AcaoAnormalidadeConsumo getAcaoAnormalidadeConsumoSemLeitura() {

		return acaoAnormalidadeConsumoSemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #
	 * setAcaoAnormalidadeConsumoSemLeitura(br.com
	 * .ggas.medicao.anormalidade.
	 * AcaoAnormalidadeConsumo)
	 */
	@Override
	public void setAcaoAnormalidadeConsumoSemLeitura(AcaoAnormalidadeConsumo acaoAnormalidadeConsumoSemLeitura) {

		this.acaoAnormalidadeConsumoSemLeitura = acaoAnormalidadeConsumoSemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getTipoMedicao()
	 */
	@Override
	public TipoMedicao getTipoMedicao() {

		return tipoMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setTipoMedicao(br.com.ggas
	 * .medicao.leitura.TipoMedicao)
	 */
	@Override
	public void setTipoMedicao(TipoMedicao tipoMedicao) {

		this.tipoMedicao = tipoMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getIndicadorParadaProgramada()
	 */
	@Override
	public Boolean getIndicadorParadaProgramada() {

		return indicadorParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setIndicadorParadaProgramada
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorParadaProgramada(Boolean indicadorParadaProgramada) {

		this.indicadorParadaProgramada = indicadorParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getQuantidadeAnosParadaCliente()
	 */
	@Override
	public Integer getQuantidadeAnosParadaCliente() {

		return quantidadeAnosParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setQuantidadeAnosParadaCliente
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeAnosParadaCliente(Integer quantidadeAnosParadaCliente) {

		this.quantidadeAnosParadaCliente = quantidadeAnosParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getQuantidadeTotalParadaCliente()
	 */
	@Override
	public Integer getQuantidadeTotalParadaCliente() {

		return quantidadeTotalParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setQuantidadeTotalParadaCliente
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeTotalParadaCliente(Integer quantidadeTotalParadaCliente) {

		this.quantidadeTotalParadaCliente = quantidadeTotalParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getQuantidadeMaximaAnoParadaCliente()
	 */
	@Override
	public Integer getQuantidadeMaximaAnoParadaCliente() {

		return quantidadeMaximaAnoParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setQuantidadeMaximaAnoParadaCliente
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeMaximaAnoParadaCliente(Integer quantidadeMaximaAnoParadaCliente) {

		this.quantidadeMaximaAnoParadaCliente = quantidadeMaximaAnoParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDiasAntecedentesParadaCliente()
	 */
	@Override
	public Integer getDiasAntecedentesParadaCliente() {

		return diasAntecedentesParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDiasAntecedentesParadaCliente
	 * (java.lang.Integer)
	 */
	@Override
	public void setDiasAntecedentesParadaCliente(Integer diasAntecedentesParadaCliente) {

		this.diasAntecedentesParadaCliente = diasAntecedentesParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getQuantidadeAnosParadaCDL()
	 */
	@Override
	public Integer getQuantidadeAnosParadaCDL() {

		return quantidadeAnosParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setQuantidadeAnosParadaCDL
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeAnosParadaCDL(Integer quantidadeAnosParadaCDL) {

		this.quantidadeAnosParadaCDL = quantidadeAnosParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getQuantidadeTotalParadaCDL()
	 */
	@Override
	public Integer getQuantidadeTotalParadaCDL() {

		return quantidadeTotalParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setQuantidadeTotalParadaCDL
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeTotalParadaCDL(Integer quantidadeTotalParadaCDL) {

		this.quantidadeTotalParadaCDL = quantidadeTotalParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getQuantidadeMaximaAnoParadaCDL()
	 */
	@Override
	public Integer getQuantidadeMaximaAnoParadaCDL() {

		return quantidadeMaximaAnoParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setQuantidadeMaximaAnoParadaCDL
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeMaximaAnoParadaCDL(Integer quantidadeMaximaAnoParadaCDL) {

		this.quantidadeMaximaAnoParadaCDL = quantidadeMaximaAnoParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDiasAntecedentesParadaCDL()
	 */
	@Override
	public Integer getDiasAntecedentesParadaCDL() {

		return diasAntecedentesParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDiasAntecedentesParadaCDL
	 * (java.lang.Integer)
	 */
	@Override
	public void setDiasAntecedentesParadaCDL(Integer diasAntecedentesParadaCDL) {

		this.diasAntecedentesParadaCDL = diasAntecedentesParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getDataInicioTeste()
	 */
	@Override
	public Date getDataInicioTeste() {
		Date data = null;
		if(dataInicioTeste != null) {
			data = (Date) dataInicioTeste.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDataInicioTeste(java.util.Date)
	 */
	@Override
	public void setDataInicioTeste(Date dataInicioTeste) {
		if (dataInicioTeste != null) {
			this.dataInicioTeste = (Date) dataInicioTeste.clone();
		} else {
			this.dataInicioTeste = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getDataFimTeste()
	 */
	@Override
	public Date getDataFimTeste() {
		Date data = null;
		if (dataFimTeste != null) {
			data = (Date) dataFimTeste.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDataFimTeste(java.util.Date)
	 */
	@Override
	public void setDataFimTeste(Date dataFimTeste) {
		if(dataFimTeste != null){
			this.dataFimTeste = (Date) dataFimTeste.clone();
		} else {
			this.dataFimTeste = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getNumeroImovel()
	 */
	@Override
	public String getNumeroImovel() {

		return numeroImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setNumeroImovel(java.lang.String)
	 */
	@Override
	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getComplementoEndereco()
	 */
	@Override
	public String getComplementoEndereco() {

		return complementoEndereco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setComplementoEndereco(java.lang.String)
	 */
	@Override
	public void setComplementoEndereco(String complementoEndereco) {

		this.complementoEndereco = complementoEndereco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getEmailEntrega()
	 */
	@Override
	public String getEmailEntrega() {

		return emailEntrega;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setEmailEntrega(java.lang.String)
	 */
	@Override
	public void setEmailEntrega(String emailEntrega) {

		this.emailEntrega = emailEntrega;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getIndicadorCorretorVazao()
	 */
	@Override
	public Boolean getIndicadorCorretorVazao() {

		return indicadorCorretorVazao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setIndicadorCorretorVazao
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorCorretorVazao(Boolean indicadorCorretorVazao) {

		this.indicadorCorretorVazao = indicadorCorretorVazao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaConsumoTeste()
	 */
	@Override
	public BigDecimal getMedidaConsumoTeste() {

		return medidaConsumoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaConsumoTeste(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setMedidaConsumoTeste(BigDecimal medidaConsumoTeste) {

		this.medidaConsumoTeste = medidaConsumoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getPrazoConsumoTeste()
	 */
	@Override
	public Integer getPrazoConsumoTeste() {

		return prazoConsumoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setPrazoConsumoTeste(java.lang.Integer)
	 */
	@Override
	public void setPrazoConsumoTeste(Integer prazoConsumoTeste) {

		this.prazoConsumoTeste = prazoConsumoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDiaGarantiaConversao()
	 */
	@Override
	public Integer getDiaGarantiaConversao() {

		return diaGarantiaConversao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDiaGarantiaConversao(java.lang.Integer)
	 */
	@Override
	public void setDiaGarantiaConversao(Integer diaGarantiaConversao) {

		this.diaGarantiaConversao = diaGarantiaConversao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDataInicioGarantiaConversao()
	 */
	@Override
	public Date getDataInicioGarantiaConversao() {
		Date data = null;
		if(dataInicioGarantiaConversao != null) { 
			data = (Date) dataInicioGarantiaConversao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDataInicioGarantiaConversao
	 * (java.util.Date)
	 */
	@Override
	public void setDataInicioGarantiaConversao(Date dataInicioGarantiaConversao) {
		if(dataInicioGarantiaConversao != null){
			this.dataInicioGarantiaConversao = (Date) dataInicioGarantiaConversao.clone();
		} else {
			this.dataInicioGarantiaConversao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getRegimeConsumo()
	 */
	@Override
	public EntidadeConteudo getRegimeConsumo() {

		return regimeConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setRegimeConsumo(br.com
	 * .ggas.contrato.contrato.EntidadeConteudo)
	 */
	@Override
	public void setRegimeConsumo(EntidadeConteudo regimeConsumo) {

		this.regimeConsumo = regimeConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getCep()
	 */
	@Override
	public Cep getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setCep(br.com.ggas.cadastro.endereco.Cep)
	 */
	@Override
	public void setCep(Cep cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getListaContratoPontoConsumoModalidade()
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidade> getListaContratoPontoConsumoModalidade() {

		return listaContratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setListaContratoPontoConsumoModalidade
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumoModalidade(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade) {

		this.listaContratoPontoConsumoModalidade = listaContratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getListaContratoPontoConsumoItemFaturamento
	 * ()
	 */
	@Override
	public Collection<ContratoPontoConsumoItemFaturamento> getListaContratoPontoConsumoItemFaturamento() {

		return listaContratoPontoConsumoItemFaturamento;
	}
	
	@Override
	public ContratoPontoConsumoItemFaturamento getContratoPontoConsumoItemFaturamento(long idItemFatura) {

		Collection<ContratoPontoConsumoItemFaturamento> listaContratosPontoConsumoItemFaturamento = getListaContratoPontoConsumoItemFaturamento();
		for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : listaContratosPontoConsumoItemFaturamento) {
			if (contratoPontoConsumoItemFaturamento.getItemFatura().getChavePrimaria() == idItemFatura) {
				return contratoPontoConsumoItemFaturamento;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setListaContratoPontoConsumoItemFaturamento
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumoItemFaturamento(
					Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento) {

		this.listaContratoPontoConsumoItemFaturamento = listaContratoPontoConsumoItemFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getListaContratoPontoConsumoPCSAmostragem
	 * ()
	 */
	@Override
	public Collection<ContratoPontoConsumoPCSAmostragem> getListaContratoPontoConsumoPCSAmostragem() {

		return listaContratoPontoConsumoPCSAmostragem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setListaContratoPontoConsumoPCSAmostragem
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumoPCSAmostragem(
					Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoConsumoPCSAmostragem) {

		this.listaContratoPontoConsumoPCSAmostragem = listaContratoPontoConsumoPCSAmostragem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getListaContratoPontoConsumoPCSIntervalo()
	 */
	@Override
	public Collection<ContratoPontoConsumoPCSIntervalo> getListaContratoPontoConsumoPCSIntervalo() {

		return listaContratoPontoConsumoPCSIntervalo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setListaContratoPontoConsumoPCSIntervalo
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumoPCSIntervalo(Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoConsumoPCSIntervalo) {

		this.listaContratoPontoConsumoPCSIntervalo = listaContratoPontoConsumoPCSIntervalo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaPressaoMinima()
	 */
	@Override
	public BigDecimal getMedidaPressaoMinima() {

		return medidaPressaoMinima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaPressaoMinima(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setMedidaPressaoMinima(BigDecimal medidaPressaoMinima) {

		this.medidaPressaoMinima = medidaPressaoMinima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getUnidadePressaoMinima()
	 */
	@Override
	public Unidade getUnidadePressaoMinima() {

		return unidadePressaoMinima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setUnidadePressaoMinima
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressaoMinima(Unidade unidadePressaoMinima) {

		this.unidadePressaoMinima = unidadePressaoMinima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaPressaoMaxima()
	 */
	@Override
	public BigDecimal getMedidaPressaoMaxima() {

		return medidaPressaoMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaPressaoMaxima(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setMedidaPressaoMaxima(BigDecimal medidaPressaoMaxima) {

		this.medidaPressaoMaxima = medidaPressaoMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getUnidadePressaoMaxima()
	 */
	@Override
	public Unidade getUnidadePressaoMaxima() {

		return unidadePressaoMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setUnidadePressaoMaxima
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressaoMaxima(Unidade unidadePressaoMaxima) {

		this.unidadePressaoMaxima = unidadePressaoMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaPressaoLimite()
	 */
	@Override
	public BigDecimal getMedidaPressaoLimite() {

		return medidaPressaoLimite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaPressaoLimite(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setMedidaPressaoLimite(BigDecimal medidaPressaoLimite) {

		this.medidaPressaoLimite = medidaPressaoLimite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getUnidadePressaoLimite()
	 */
	@Override
	public Unidade getUnidadePressaoLimite() {

		return unidadePressaoLimite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setUnidadePressaoLimite
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressaoLimite(Unidade unidadePressaoLimite) {

		this.unidadePressaoLimite = unidadePressaoLimite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaVazaoInstantanea()
	 */
	@Override
	public BigDecimal getMedidaVazaoInstantanea() {

		return medidaVazaoInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaVazaoInstantanea
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setMedidaVazaoInstantanea(BigDecimal medidaVazaoInstantanea) {

		this.medidaVazaoInstantanea = medidaVazaoInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getUnidadeVazaoInstantanea()
	 */
	@Override
	public Unidade getUnidadeVazaoInstantanea() {

		return unidadeVazaoInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setUnidadeVazaoInstantanea
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeVazaoInstantanea(Unidade unidadeVazaoInstantanea) {

		this.unidadeVazaoInstantanea = unidadeVazaoInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaVazaoMinimaInstantanea()
	 */
	@Override
	public BigDecimal getMedidaVazaoMinimaInstantanea() {

		return medidaVazaoMinimaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaVazaoMinimaInstantanea
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setMedidaVazaoMinimaInstantanea(BigDecimal medidaVazaoMinimaInstantanea) {

		this.medidaVazaoMinimaInstantanea = medidaVazaoMinimaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getUnidadeVazaoMinimaInstantanea()
	 */
	@Override
	public Unidade getUnidadeVazaoMinimaInstantanea() {

		return unidadeVazaoMinimaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setUnidadeVazaoMinimaInstantanea
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeVazaoMinimaInstantanea(Unidade unidadeVazaoMinimaInstantanea) {

		this.unidadeVazaoMinimaInstantanea = unidadeVazaoMinimaInstantanea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getNumeroHoraInicial()
	 */
	@Override
	public Integer getNumeroHoraInicial() {

		return numeroHoraInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setNumeroHoraInicial(java.lang.Integer)
	 */
	@Override
	public void setNumeroHoraInicial(Integer numeroHoraInicial) {

		this.numeroHoraInicial = numeroHoraInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDiasConsecutivosParadaCliente()
	 */
	@Override
	public Integer getDiasConsecutivosParadaCliente() {

		return diasConsecutivosParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDiasConsecutivosParadaCliente
	 * (java.lang.Integer)
	 */
	@Override
	public void setDiasConsecutivosParadaCliente(Integer diasConsecutivosParadaCliente) {

		this.diasConsecutivosParadaCliente = diasConsecutivosParadaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDiasConsecutivosParadaCDL()
	 */
	@Override
	public Integer getDiasConsecutivosParadaCDL() {

		return diasConsecutivosParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDiasConsecutivosParadaCDL
	 * (java.lang.Integer)
	 */
	@Override
	public void setDiasConsecutivosParadaCDL(Integer diasConsecutivosParadaCDL) {

		this.diasConsecutivosParadaCDL = diasConsecutivosParadaCDL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo#getContratoCompra()
	 */
	@Override
	public EntidadeConteudo getContratoCompra() {

		return contratoCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setContratoCompra(br.com
	 * .ggas.contrato.contrato.EntidadeConteudo)
	 */
	@Override
	public void setContratoCompra(EntidadeConteudo contratoCompra) {

		this.contratoCompra = contratoCompra;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getDataConsumoTesteExcedido()
	 */
	@Override
	public Date getDataConsumoTesteExcedido() {
		Date data = null;
		if(dataConsumoTesteExcedido != null) {
			data = (Date) dataConsumoTesteExcedido.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setDataConsumoTesteExcedido
	 * (java.util.Date)
	 */
	@Override
	public void setDataConsumoTesteExcedido(Date dataConsumoTesteExcedido) {
		if(dataConsumoTesteExcedido != null) {
			this.dataConsumoTesteExcedido = (Date) dataConsumoTesteExcedido.clone();
		} else {
			this.dataConsumoTesteExcedido = null;
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #getMedidaConsumoTesteUsado()
	 */
	@Override
	public BigDecimal getMedidaConsumoTesteUsado() {

		return medidaConsumoTesteUsado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumo
	 * #setMedidaConsumoTesteUsado
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setMedidaConsumoTesteUsado(BigDecimal medidaConsumoTesteUsado) {

		this.medidaConsumoTesteUsado = medidaConsumoTesteUsado;
	}

	/**
	 * @return the periodicidade
	 */
	@Override
	public Periodicidade getPeriodicidade() {

		return periodicidade;
	}

	/**
	 * @param periodicidade
	 *            the periodicidade to set
	 */
	@Override
	public void setPeriodicidade(Periodicidade periodicidade) {

		this.periodicidade = periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ContratoPontoConsumo#equalsNegocio(br.com.ggas.contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public boolean equalsNegocio(ContratoPontoConsumo obj) {

		if (obj != null && this.pontoConsumo != null && this.pontoConsumo.getChavePrimaria() > 0 && obj.getPontoConsumo() != null
						&& obj.getPontoConsumo().getChavePrimaria() == this.pontoConsumo.getChavePrimaria() && this.contrato != null
						&& this.contrato.getChavePrimaria() > 0 && obj.getContrato() != null
						&& obj.getContrato().getChavePrimaria() == this.contrato.getChavePrimaria()) {
			return true;
		}

		return false;
	}

	@Override
	public FaixaPressaoFornecimento getFaixaPressaoFornecimento() {

		return this.faixaPressaoFornecimento;
	}

	@Override
	public void setFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) {

		this.faixaPressaoFornecimento = faixaPressaoFornecimento;
	}

	@Override
	public Boolean getIsEmiteNotaFiscalEletronica() {

		return isEmiteNotaFiscalEletronica;
	}

	@Override
	public void setIsEmiteNotaFiscalEletronica(Boolean isEmiteNotaFiscalEletronica) {

		this.isEmiteNotaFiscalEletronica = isEmiteNotaFiscalEletronica;
	}

	@Override
	public Boolean getEmitirFaturaComNfe() {

		return emitirFaturaComNfe;
	}

	@Override
	public void setEmitirFaturaComNfe(Boolean emitirFaturaComNfe) {

		this.emitirFaturaComNfe = emitirFaturaComNfe;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getMedidaPressaoColetada()
	 */
	@Override
	public BigDecimal getMedidaPressaoColetada() {

		return medidaPressaoColetada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setMedidaPressaoColetada(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaPressaoColetada(BigDecimal medidaPressaoColetada) {

		this.medidaPressaoColetada = medidaPressaoColetada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #getUnidadePressaoColetada()
	 */
	@Override
	public Unidade getUnidadePressaoColetada() {

		return unidadePressaoColetada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ContratoPontoConsumo
	 * #setUnidadePressaoColetada(br.com.ggas.medicao.
	 * vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressaoColetada(Unidade unidadePressaoColetada) {

		this.unidadePressaoColetada = unidadePressaoColetada;
	}

}
