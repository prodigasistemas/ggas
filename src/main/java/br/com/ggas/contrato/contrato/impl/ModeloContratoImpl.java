/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.contrato.contrato.AbaModelo;
import br.com.ggas.contrato.contrato.LayoutRelatorio;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelos atributos e implementação dos métodos relacionados ao Modelo de Contrato.
 *
 */
public class ModeloContratoImpl extends EntidadeNegocioImpl implements ModeloContrato {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1970878184768429824L;

	private String descricao;

	private LayoutRelatorio layoutRelatorio;

	private String descricaoAbreviada;

	private String formaCobranca;

	private String itemFatura;

	private byte[] documento;

	private EntidadeConteudo tipoModelo;

	private ModeloContrato modeloContratoVersaoAnterior;

	private Collection<ModeloAtributo> atributos = new HashSet<>();

	private Collection<AbaModelo> abas = new HashSet<>();
	
	private int geracao = 1;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ModeloContrato
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}
	
	@Override
	public String getDescricao(boolean geracaoDescrita) {
		StringBuilder sbDescricaoModelo = new StringBuilder();
		sbDescricaoModelo.append(getDescricao());
		if (geracaoDescrita) {			
			sbDescricaoModelo.append(Constantes.STRING_HIFEN_ESPACO);
			sbDescricaoModelo.append(String.valueOf(getGeracao()));
		}
		return sbDescricaoModelo.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ModeloContrato
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ModeloContrato
	 * #getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ModeloContrato
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ModeloContrato
	 * #getDocumento()
	 */
	@Override
	public byte[] getDocumento() {
		byte[] retorno = null;
		if(this.documento != null) {
			retorno = documento.clone();
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.ModeloContrato
	 * #setDocumento(byte[])
	 */
	@Override
	public void setDocumento(byte[] documento) {
		if(documento != null) {
			this.documento = documento.clone();
		} else {
			this.documento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ModeloContrato
	 * #getAtributos()
	 */
	@Override
	public Collection<ModeloAtributo> getAtributos() {

		return atributos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ModeloContrato
	 * #setAtributos(java.util.Collection)
	 */
	@Override
	public void setAtributos(Collection<ModeloAtributo> atributos) {

		this.atributos = atributos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ModeloContrato
	 * #getAbas()
	 */
	@Override
	public Collection<AbaModelo> getAbas() {

		return abas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ModeloContrato
	 * #setAbas(java.util.Collection)
	 */
	@Override
	public void setAbas(Collection<AbaModelo> abas) {

		this.abas = abas;
	}

	@Override
	public String getFormaCobranca() {

		return formaCobranca;
	}

	@Override
	public void setFormaCobranca(String formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	@Override
	public String getItemFatura() {

		return itemFatura;
	}

	@Override
	public void setItemFatura(String itemFatura) {

		this.itemFatura = itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(StringUtils.isEmpty(descricaoAbreviada)) {
			stringBuilder.append(DESCRICAO_ABREVIADA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(tipoModelo == null) {
			stringBuilder.append(MODELO_CONTRATO_TIPO_MODELO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if("-1".equals(itemFatura)) {
			stringBuilder.append(ATRIBUTO_OBRIGATORIO_CONSUMO_ITEM_FATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if("-1".equals(formaCobranca)) {
			stringBuilder.append(ATRIBUTO_OBRIGATORIO_FORMA_COBRANCA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		// 1 - Regra
		// Obriga selecionar o campo Local de
		// Amostragem do PCS ou Fator Único de
		// Correção de Volume
		// 2- Regra
		// Obriga selecionar como obrigatório o
		// campo QDC do Contrato (sazonalidade) ou
		// QDC quando selecionar o
		// campo Indicador de Recuperação
		// Automática ToP ou Periodicidade ToP
		// como obrigatório
		// 3- Regra
		// Obriga selecionar como obrigatório o
		// campo Data de Vencimento das Obrigações
		// Contratuais quando selecionar o
		// campo QDC do Contrato (sazonalidade) ou
		// QDC como obrigatório
		boolean validaAtributo = false;
		boolean periodicidadeTakeOrPay = false;
		boolean qDCContrato = false;
		boolean qdc = false;
		boolean dataVencObrigacoesContratuais = false;

		boolean periodicidadeTakeOrPayObg = false;
		boolean qDCContratoObg = false;
		boolean qdcObg = false;
		boolean dataVencObrigacoesContratuaisObg = false;

		for (ModeloAtributo modeloAtributo : atributos) {
			if("localAmostragemPCS".equals(modeloAtributo.getAbaAtributo().getNomeColuna())
							|| "fatorUnicoCorrecao".equals(modeloAtributo.getAbaAtributo().getNomeColuna())) {
				validaAtributo = true;
			}

			if("periodicidadeTakeOrPay".equals(modeloAtributo.getAbaAtributo().getNomeColuna())) {
				periodicidadeTakeOrPay = true;
				if(modeloAtributo.isObrigatorio()) {
					periodicidadeTakeOrPayObg = true;
				}
			}

			if("QDCContrato".equals(modeloAtributo.getAbaAtributo().getNomeColuna())) {
				qDCContrato = true;
				if(modeloAtributo.isObrigatorio()) {
					qDCContratoObg = true;
				}
			}

			if("qdc".equals(modeloAtributo.getAbaAtributo().getNomeColuna())) {
				qdc = true;
				if(modeloAtributo.isObrigatorio()) {
					qdcObg = true;
				}
			}

			if("dataVencObrigacoesContratuais".equals(modeloAtributo.getAbaAtributo().getNomeColuna())) {
				dataVencObrigacoesContratuais = true;
				if(modeloAtributo.isObrigatorio()) {
					dataVencObrigacoesContratuaisObg = true;
				}
			}
		}

		// 1 - Regra
		// Obriga selecionar o campo Local de
		// Amostragem do PCS ou Fator Único de
		// Correção de Volume
		if(!(validaAtributo)) {
			stringBuilder.append(ATRIBUTO_OBRIGATORIO_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		// 2- Regra
		// Obriga selecionar como obrigatório o
		// campo QDC do Contrato (sazonalidade) ou
		// QDC quando selecionar o
		// campo Indicador de Recuperação
		// Automática ToP ou Periodicidade ToP
		// como obrigatório
		if (periodicidadeTakeOrPayObg && !qDCContratoObg && !qdcObg) {
			stringBuilder.append(MODELO_ATRIBUTO_OBRIGATORIO_QDC);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if (periodicidadeTakeOrPay && !qDCContrato && !qdc) {
			stringBuilder.append(MODELO_ATRIBUTO_QDC);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		// 3- Regra
		// Obriga selecionar como obrigatório o
		// campo Data de Vencimento das Obrigações
		// Contratuais quando selecionar o
		// campo QDC do Contrato (sazonalidade) ou
		// QDC como obrigatório
		if ((qDCContratoObg || qdcObg) && !dataVencObrigacoesContratuaisObg) {
			stringBuilder.append(MODELO_ATRIBUTO_OBRIGATORIO_VECIMENTO_CONTRATUAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if ((qDCContrato || qdc) && !dataVencObrigacoesContratuais) {
			stringBuilder.append(MODELO_ATRIBUTO_VECIMENTO_CONTRATUAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		// FIM

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;

	}

	@Override
	public EntidadeConteudo getTipoModelo() {

		return tipoModelo;
	}

	@Override
	public void setTipoModelo(EntidadeConteudo tipoModelo) {

		this.tipoModelo = tipoModelo;
	}

	/**
	 * @return the modeloContratoVersaoAnterior
	 */
	@Override
	public ModeloContrato getModeloContratoVersaoAnterior() {

		return modeloContratoVersaoAnterior;
	}

	/**
	 * @param modeloContratoVersaoAnterior
	 *            the modeloContratoVersaoAnterior to set
	 */
	@Override
	public void setModeloContratoVersaoAnterior(ModeloContrato modeloContratoVersaoAnterior) {

		this.modeloContratoVersaoAnterior = modeloContratoVersaoAnterior;
	}

	
	/**
	 * @return the geracao
	 */
	@Override
	public int getGeracao() {
	
		return geracao;
	}

	
	/**
	 * @param geracao the geracao to set
	 */
	@Override
	public void setGeracao(int geracao) {
	
		this.geracao = geracao;
	}

	public LayoutRelatorio getLayoutRelatorio() {
		return layoutRelatorio;
	}

	public void setLayoutRelatorio(LayoutRelatorio layoutRelatorio) {
		this.layoutRelatorio = layoutRelatorio;
	}

}
