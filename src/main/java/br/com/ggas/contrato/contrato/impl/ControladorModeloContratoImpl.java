/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorModeloContratoImpl representa uma ControladorModeloContratoImpl no sistema.
 *
 * @since 17/12/2009
 *
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.contrato.contrato.AbaModelo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.contrato.contrato.LayoutRelatorio;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Mapeador;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * The Class ControladorModeloContratoImpl.
 */
class ControladorModeloContratoImpl extends ControladorNegocioImpl implements ControladorModeloContrato {

	private static final int PORCENTAGEM = 100;

	private static final String PERCENTUAL_QNR = "percentualQNR";

	private static final String PERIODICIDADE_TAKE_OR_PAY_CONTRATO = "periodicidadeTakeOrPayC";

	private static final String QDC_CONTRATO = "QDCContrato";

	private static final String CONSUMO_REFERENCIA_SOB_DEM = "consumoReferenciaSobDem";

	private static final String DATA_VENC_OBRIGACOES_CONTRATUAIS = "dataVencObrigacoesContratuais";

	private static final String VOLUME_REFERENCIA = "volumeReferencia";

	private static final String DESCRICAO_CONTRATO = "descricaoContrato";

	/** The Constant PERCENTUAL_COB_INT_RET_MAIOR_MENOR. */
	private static final String PERCENTUAL_COB_INT_RET_MAIOR_MENOR = "percentualCobIntRetMaiorMenor";
	
	/** The Constant PERCENTUAL_COB_INT_RET_MAIOR_MENOR_M. */
	private static final String PERCENTUAL_COB_INT_RET_MAIOR_MENOR_M = "percentualCobIntRetMaiorMenorM";
	
	/** The controlador contrato. */
	private ControladorContrato controladorContrato;

	/**
	 * Sets the controlador contrato.
	 *
	 * @param controladorContrato the new controlador contrato
	 */
	public void setControladorContrato(ControladorContrato controladorContrato) {

		this.controladorContrato = controladorContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ModeloContrato.BEAN_ID_MODELO_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#criarModeloAtributo()
	 */
	@Override
	public EntidadeNegocio criarModeloAtributo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ModeloAtributo.BEAN_ID_MODELO_ATRIBUTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ModeloContrato.BEAN_ID_MODELO_CONTRATO);
	}

	/**
	 * Gets the classe entidade aba atributo.
	 *
	 * @return the classe entidade aba atributo
	 */
	public Class<?> getClasseEntidadeAbaAtributo() {

		return ServiceLocator.getInstancia().getClassPorID(AbaAtributo.BEAN_ID_ABA_ATRIBUTO);
	}

	/**
	 * Gets the classe entidade situacao contrato.
	 *
	 * @return the classe entidade situacao contrato
	 */
	public Class<?> getClasseEntidadeSituacaoContrato() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoContrato.BEAN_ID_SITUACAO_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#obterAbaAtributo(long)
	 */
	@Override
	public AbaAtributo obterAbaAtributo(long chavePrimaria) throws NegocioException {

		return (AbaAtributo) super.obter(chavePrimaria, getClasseEntidadeAbaAtributo());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #consultarModelosContrato(java.util.Map)
	 */
	@Override
	public Collection<ModeloContrato> consultarModelosContrato(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

			Long tipoModelo = (Long) filtro.get("tipoModelo");
			if (tipoModelo != null && tipoModelo > 0) {
				criteria.add(Restrictions.eq("tipoModelo.chavePrimaria", tipoModelo));
			}

		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #validarAtualizarModelosContrato
	 * (java.lang.Long)
	 */
	@Override
	public void validarAtualizarModeloContrato(Long chavePrimaria) throws NegocioException {

		if (Util.isEmpty(chavePrimaria)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		validarAtualizarRemoverModelosContrato(chavePrimaria);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #validarRemoverModelosContrato
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverModelosContrato(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		validarAtualizarRemoverModelosContrato(chavesPrimarias);

	}

	/**
	 * valida se os modelos contratos das chaves primarias
	 * do argumento pode ser atualizadas ou removidas.
	 * 
	 * @param chavesPrimarias
	 *            chaves dos modelos contratos a
	 *            a serem atualizados ou removidos
	 * @throws NegocioException
	 *             exceção levantada quando uma das
	 *             chaves pertence a um modelo que nao pode ser
	 *             atualizado ou removido
	 */
	private void validarAtualizarRemoverModelosContrato(Long... chavesPrimarias) throws NegocioException {

		// validação por versão - um modelo de contrato com uma versão
		// atualizada não pode ser alterado ou removido
		for (Long chavePrimaria : chavesPrimarias) {
			ModeloContrato filhoModeloContrato = obterProximaVersaoModeloContrato(chavePrimaria);
			if (filhoModeloContrato != null) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_MODELO_CONTRATO_VINCULADO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#listarAbasAtributos()
	 */
	@Override
	public Collection<AbaAtributo> listarAbasAtributos() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAbaAtributo());
		criteria.setFetchMode("aba", FetchMode.JOIN);
		return criteria.list();
	}

	
	/* (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato
	 * .ControladorModeloContrato
	 * #isAbaAtributoPercentualCobrAvisoInterrupRetirMaiorMenor(
	 * br.com.ggas.contrato.contrato.AbaAtributo)
	 */
	@Override
	public boolean isAbaAtributoPercentualCobrAvisoInterrupRetirMaiorMenor(AbaAtributo abaAtributo) {
		return abaAtributo.getNomeColuna().equals(PERCENTUAL_COB_INT_RET_MAIOR_MENOR);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato
	 * .ControladorModeloContrato
	 * #isAbaAtributoPercCobrAvisoInterrupRetirMaiorMenorModalid(
	 * br.com.ggas.contrato.contrato.AbaAtributo)
	 */
	@Override
	public boolean isAbaAtributoPercCobrAvisoInterrupRetirMaiorMenorModalid(AbaAtributo abaAtributo) {
		return abaAtributo.getNomeColuna().equals(PERCENTUAL_COB_INT_RET_MAIOR_MENOR_M);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#criarAbaModelo()
	 */
	@Override
	public EntidadeNegocio criarAbaModelo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AbaModelo.BEAN_ID_ABA_MODELO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #obterListaSituacaoContrato()
	 */
	@Override
	public Collection<SituacaoContrato> obterListaSituacaoContrato() throws NegocioException {

		return createCriteria(getClasseEntidadeSituacaoContrato()).addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#obterListaSituacaoContratoAlterado()
	 */
	@Override
	public Collection<SituacaoContrato> obterListaSituacaoContratoAlterado() throws NegocioException {

		Long[] chavePrimaria = {SituacaoContrato.ATIVO, SituacaoContrato.DISTRATADO, 
		                        SituacaoContrato.EM_NEGOCIACAO, SituacaoContrato.EM_RECUPERACAO};
		Criteria criteria = createCriteria(getClasseEntidadeSituacaoContrato());
		criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#obterListaSituacaoContratoAditado()
	 */
	@Override
	public Collection<SituacaoContrato> obterListaSituacaoContratoAditado() throws NegocioException {

		Long[] chavePrimaria = {SituacaoContrato.ATIVO, SituacaoContrato.ADITADO};
		Criteria criteria = createCriteria(getClasseEntidadeSituacaoContrato());
		criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		return criteria.list();
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato
	 * .ControladorModeloContrato
	 * #obterAbaAtributo(java.lang.String)
	 */
	@Override
	public AbaAtributo obterAbaAtributo(String nomeColuna) 
			throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAbaAtributo());
		criteria.add(Restrictions.eq("nomeColuna", nomeColuna));
		return (AbaAtributo) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #obterListaSituacaoContratoPadrao()
	 */
	@Override
	public Collection<SituacaoContrato> obterListaSituacaoContratoPadrao() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeSituacaoContrato());
		criteria.add(Restrictions.eq("padrao", Boolean.TRUE));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#atualizar(br.com.ggas.contrato.contrato.ModeloContrato)
	 */
	@Override
	public void atualizar(ModeloContrato modeloContrato) throws ConcorrenciaException, NegocioException {

		// Valida o modelo alterado
		validarDadosEntidade(modeloContrato);
		validarVersaoEntidade(modeloContrato, this.getClasseEntidade());
		this.verificarNomeModeloExistente(modeloContrato);
		modeloContrato.setUltimaAlteracao(Calendar.getInstance().getTime());

		ModeloContrato modeloAnterior = (ModeloContrato) this.obter(modeloContrato.getChavePrimaria());
		modeloAnterior.setUltimaAlteracao(Calendar.getInstance().getTime());
		modeloAnterior.setDadosAuditoria(modeloContrato.getDadosAuditoria());
		modeloAnterior.incrementarVersao();

		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(modeloAnterior);

		// setting modelo anterior
		modeloAnterior.setHabilitado(false);
		// setting modelo atual
		modeloContrato.setChavePrimaria(0);
		modeloContrato.setGeracao(modeloAnterior.getGeracao() + 1);
		modeloContrato.setModeloContratoVersaoAnterior(modeloAnterior);
		inserir(modeloContrato);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		ModeloContrato modeloContrato = (ModeloContrato) entidadeNegocio;
		this.verificarNomeModeloExistente(modeloContrato);
	}

	/**
	 * Verificar nome modelo existente.
	 * 
	 * @param modeloContrato the modelo contrato
	 * @throws NegocioException the negocio exception
	 */
	private void verificarNomeModeloExistente(ModeloContrato modeloContrato) throws NegocioException {

		ModeloContrato modeloContratoVersaoAnterior = 
						modeloContrato.getModeloContratoVersaoAnterior();
		
		Long quantidadeModelos = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(modelo.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" modelo ");
		hql.append(" where habilitado = true ");
		hql.append(" and upper(modelo.descricao) = upper(:descricao) ");
		if (modeloContrato.getChavePrimaria() > 0) {
			hql.append(" and modelo.chavePrimaria != :chavePrimaria ");
		}
		if (modeloContratoVersaoAnterior != null &&
						NumeroUtil.naoNulo(modeloContratoVersaoAnterior.getChavePrimaria())) {
			hql.append(" and modelo.chavePrimaria != :chavePrimariaVersaoAnterior ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!StringUtils.isEmpty(modeloContrato.getDescricao())) {
			query.setString(TabelaAuxiliar.ATRIBUTO_DESCRICAO, modeloContrato.getDescricao().trim());
		}
		if (modeloContrato.getChavePrimaria() > 0) {
			query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, modeloContrato.getChavePrimaria());
		}
		if (modeloContratoVersaoAnterior != null &&
						NumeroUtil.naoNulo(modeloContratoVersaoAnterior.getChavePrimaria())) {
			query.setLong("chavePrimariaVersaoAnterior", modeloContratoVersaoAnterior.getChavePrimaria());
		}

		quantidadeModelos = (Long) query.uniqueResult();

		if (quantidadeModelos != null && quantidadeModelos > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MODELO_CONTRATO_EXISTENTE, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		ModeloContrato modeloContrato = (ModeloContrato) entidadeNegocio;
		this.validarRestricaoIntegridadeModeloContrato(modeloContrato);
	}

	/**
	 * Validar restricao integridade modelo contrato.
	 * 
	 * @param modeloContrato
	 *            the modelo contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarRestricaoIntegridadeModeloContrato(ModeloContrato modeloContrato) throws NegocioException {

		controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		Boolean existeDependencia = controladorContrato.verificarDependenciaContratoModelo(modeloContrato.getChavePrimaria());

		if (existeDependencia) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTRATO_VINCULADO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #validarDadoAtributoModeloContrato
	 * (br.com.procenge
	 * .ggas.contrato.contrato.AbaAtributo,
	 * java.lang.String)
	 */
	@Override
	public void validarDadoAtributoModeloContrato(AbaAtributo abaAtributo, String valorPadrao) throws NegocioException,
					FormatoInvalidoException {

		if ("percentualTarifaDoP".equals(abaAtributo.getNomeColuna()) && !StringUtils.isEmpty(valorPadrao)) {
			BigDecimal valor = Util.converterCampoStringParaValorBigDecimal(abaAtributo.getDescricao(), valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			if (valor.compareTo(BigDecimal.valueOf(PORCENTAGEM)) > 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
								new Object[] {abaAtributo.getDescricao()});
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorModeloContrato
	 * #obterModeloAtributo
	 * (br.com.ggas.contrato.contrato
	 * .ModeloContrato, java.lang.String)
	 */
	@Override
	public ModeloAtributo obterModeloAtributo(
			ModeloContrato modeloContrato, String nomeColunaAtributo) {

		Collection<ModeloAtributo> listaAtributos = modeloContrato.getAtributos();
		ModeloAtributo modeloAtributoPesquisado = null;
		for (ModeloAtributo modeloAtributo : listaAtributos) {
			if (modeloAtributo.getAbaAtributo().getNomeColuna().equals(nomeColunaAtributo)) {
				modeloAtributoPesquisado = modeloAtributo;
			}
		}

		return modeloAtributoPesquisado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#validarDadosRegraFaturamento(java.lang.Boolean, java.lang.Boolean)
	 */
	@Override
	public void validarDadosRegraFaturamento(Boolean selec, Boolean obrig) throws NegocioException {

		if (selec != null && !selec) {
			throw new NegocioException(Constantes.CAMPO_MODALIDADE_SELECIONADO);
		}
		if (obrig != null && !obrig) {
			throw new NegocioException(Constantes.CAMPO_MODALIDADE_OBRIGATORIO);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorModeloContrato#validarModeloContratoComplementar(br.com.ggas.contrato.contrato.ModeloContrato
	 * )
	 */
	@Override
	public void validarModeloContratoComplementar(ModeloContrato modeloContrato) 
			throws NegocioException {
		
		Map<String, ModeloAtributo> listaModeloAtributoMapeadaPorNomeColuna = 
				mapearlistaModeloAtributoPorNomeColuna(modeloContrato);
		
		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, DESCRICAO_CONTRATO, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DESCRICAO);

		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, VOLUME_REFERENCIA, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_VOLUME_REFERENCIA);
		
		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, DATA_VENC_OBRIGACOES_CONTRATUAIS, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_DATA_VENCIMENTO);

		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, CONSUMO_REFERENCIA_SOB_DEM, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_RETIRADA_MENOR);

		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, QDC_CONTRATO, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_QDC_CONTRATO);

		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, PERIODICIDADE_TAKE_OR_PAY_CONTRATO, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_TAKE_OR_PAY);

		validarObrigatoriedade(
				listaModeloAtributoMapeadaPorNomeColuna, PERCENTUAL_QNR, 
				Constantes.ERRO_MODELO_ATRIBUTO_OBRIGATORIO_PERCENTUAL_QNR);

	}

	private void validarObrigatoriedade(
			Map<String, ModeloAtributo> listaModeloAtributoMapeadaPorNomeColuna, 
			String nomeColuna, String msgErro) throws NegocioException {
		
		String nomeColunaBuscar = nomeColuna;
		
		AbaAtributo abaAtributo = obterAbaAtributo(nomeColuna);
		if (BooleanUtils.isTrue(abaAtributo.getDependenciaObrigatoria())) {
			nomeColunaBuscar = abaAtributo.getAbaAtributoPai().getNomeColuna();
		}
		
		ModeloAtributo modeloAtributo = 
				listaModeloAtributoMapeadaPorNomeColuna.get(nomeColunaBuscar);
		
		if (modeloAtributo != null) {
			if (!modeloAtributo.isObrigatorio()) {
				throw new NegocioException(msgErro);
			}
		} else {
			throw new NegocioException(msgErro);
		}
		
	}

	private Map<String, ModeloAtributo> mapearlistaModeloAtributoPorNomeColuna(ModeloContrato modeloContrato) {
		
		return Util.mapear(modeloContrato.getAtributos(), 
						new Mapeador<String, ModeloAtributo>() {
			@Override
			public String getChaveMapeadora(ModeloAtributo elemento) {
				return elemento.getAbaAtributo().getNomeColuna();
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#obterListaModeloConstratoPorDescricao(java.lang.String)
	 */
	@Override
	public Collection<ModeloContrato> obterListaModeloConstratoPorDescricao(
			String descricaoModelo) throws NegocioException {

		Criteria criteria = createCriteria(ModeloContrato.class);
		criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricaoModelo));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#obterProximaVersaoModeloContrato(java.lang.Long)
	 */
	@Override
	public ModeloContrato obterProximaVersaoModeloContrato(Long chavePrimaria) throws NegocioException {

		Criteria criteria = createCriteria(ModeloContrato.class);
		criteria.add(Restrictions.eq("modeloContratoVersaoAnterior.chavePrimaria", chavePrimaria));
		return (ModeloContrato) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.ControladorModeloContrato#criarLayoutRelatorio()
	 */
	@Override
	public EntidadeNegocio criarLayoutRelatorio() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LayoutRelatorio.BEAN_ID_LAYOUT_RELATORIO);
	}

}
