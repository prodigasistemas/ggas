/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 04/09/2014 15:37:21
 @author aantonio
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Map;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pelos atributos e métodos relacionados ao endereço temporário de faturamento. 
 *
 */
public class EnderecoTemporarioFaturamento extends EntidadeNegocioImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5269131620541787438L;

	private ContratoPontoConsumo contratoPontoConsumo;

	private Cep cep;

	private String numeroImovel;

	private String complemento;

	private String email;

	public ContratoPontoConsumo getContratoPontoConsumo() {

		return contratoPontoConsumo;
	}

	public void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) {

		this.contratoPontoConsumo = contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	public Cep getCep() {

		return cep;
	}

	public void setCep(Cep cep) {

		this.cep = cep;
	}

	public String getNumeroImovel() {

		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	public String getComplemento() {

		return complemento;
	}

	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

}
