/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Implementação do repositório da entidade {@link RepositorioAnaliseMedicao}
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Repository
public class RepositorioAnaliseMedicao extends RepositorioGenerico {

	/**
	 * Construtor padrão do repositório de contrato ponto consumo
	 * @param sessionFactory session factory
	 */
	@Autowired
	public RepositorioAnaliseMedicao(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);

	}

	/**
	 * Obtem o contrato de consumo do ponto de consumo informado.
	 * Contém apenas as informações necessárias para a análise de medição
	 *
	 * @param chavePontoConsumo chave do ponto de consumo
	 * @return contrato ponto consumo ou nulo caso não exista
	 */
	public ContratoPontoConsumo obterContratoParaAnaliseMedicao(Long chavePontoConsumo) {
		return (ContratoPontoConsumo) getSession().createQuery("select a.numeroFatorCorrecao as numeroFatorCorrecao, "
					+ "a.medidaPressao as medidaPressao from " + getClasseEntidade().getSimpleName()
					+ " a where a.pontoConsumo.chavePrimaria = :idPontoConsumo and contrato.situacao.faturavel = true")
				.setParameter("idPontoConsumo", chavePontoConsumo)
				.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()))
				.list().stream().findFirst().orElse(null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntidadeNegocio criar() {
		return new ContratoPontoConsumoImpl();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> getClasseEntidade() {
		return ContratoPontoConsumoImpl.class;
	}
}
