/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela reprensentação da entidade QuantidadeVolumes.
 *
 */
public class QuantidadeVolumesVO implements Serializable {


	private static final long serialVersionUID = 1L;

	public static final Integer CHAVE_APURACAO = 1;

	public static final Integer CHAVE_PENALIDADE = 2;

	public static final Integer CHAVE_VOLUME = 3;

	private static final String NOME_APURACAO = "Compromisso";

	private static final String NOME_VOLUME = "Volume";

	private static final String NOME_PENALIDADE = "Penalidade";

	public static final Map<Integer, String> TIPOS_QDA_VOLUME = new TreeMap<Integer, String>(){

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(CHAVE_APURACAO, NOME_APURACAO);
			put(CHAVE_VOLUME, NOME_VOLUME);
			put(CHAVE_PENALIDADE, NOME_PENALIDADE);

		}

	};

	private Integer tipo;

	private List<DadosQuantidadeVolumesVO> dados;

	public Integer getTipo() {

		return tipo;
	}

	public void setTipo(Integer tipo) {

		this.tipo = tipo;
	}

	public List<DadosQuantidadeVolumesVO> getDados() {

		return dados;
	}

	public void setDados(List<DadosQuantidadeVolumesVO> dados) {

		this.dados = dados;
	}

	public boolean getApuracao() {

		return tipo.equals(CHAVE_APURACAO);
	}

	public boolean getVolume() {

		return tipo.equals(CHAVE_VOLUME);
	}

	public boolean getPenalidade() {

		return tipo.equals(CHAVE_PENALIDADE);
	}

	public String getValorQmMedioFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			// TODO implementar

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorQmApuradoFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			// TODO implementar

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalQdcFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {

				if(vo.getQdc() != null) {
					valor = valor.add(vo.getQdc());
				}

			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalQdrFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {

				if(vo.getQdr() != null) {
					valor = valor.add(vo.getQdr());
				}

			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalRetiradaMenorFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {
				valor = valor.add(vo.getRetiradaMenor());
			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalRetiradaMaiorFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {
				valor = valor.add(vo.getRetiradaMaior());
			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalPenalidadeFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {
				valor = valor.add(vo.getPenalidade());
			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalTopFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {
				valor = valor.add(vo.getTop());
			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalSopFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {
				valor = valor.add(vo.getSop());
			}

		}

		return this.obterValorFormatado(valor);
	}

	public String getValorTotalDopFormatado() {

		BigDecimal valor = BigDecimal.ZERO;

		if(dados != null && dados.isEmpty()) {

			for (DadosQuantidadeVolumesVO vo : dados) {
				valor = valor.add(vo.getDop());
			}

		}

		return this.obterValorFormatado(valor);
	}

	/**
	 * Obter valor formatado.
	 * 
	 * @param bigDecimal the big decimal
	 * @return the string
	 */
	private String obterValorFormatado(BigDecimal bigDecimal) {

		BigDecimal valor = bigDecimal;

		if (valor == null) {
			valor = BigDecimal.ZERO;
		}

		return Util.converterCampoCurrencyParaString(valor, Constantes.LOCALE_PADRAO);
	}

}
