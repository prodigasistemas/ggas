/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ContratoPontoConsumoPCSIntervaloImpl representa uma ContratoPontoConsumoPCSIntervaloImpl no sistema.
 *
 * @since 10/11/2009
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Map;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.IntervaloPCS;

/**
 * Classe responsável pelos atributos e métodos relacionados ao Intervalo
 * do Poder Calorífico Superior associado ao Ponto de Consumo vinculado ao Contrato. 
 *
 */
public class ContratoPontoConsumoPCSIntervaloImpl extends EntidadeNegocioImpl implements ContratoPontoConsumoPCSIntervalo {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4511309564944561763L;

	private Integer prioridade;

	private Integer tamanho;

	private IntervaloPCS intervaloPCS;

	private ContratoPontoConsumo contratoPontoConsumo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.
	 * ContratoPontoConsumoPCSIntervalo
	 * #getPrioridade()
	 */
	@Override
	public Integer getPrioridade() {

		return prioridade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.
	 * ContratoPontoConsumoPCSIntervalo
	 * #setPrioridade(java.lang.Integer)
	 */
	@Override
	public void setPrioridade(Integer prioridade) {

		this.prioridade = prioridade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.
	 * ContratoPontoConsumoPCSIntervalo
	 * #getTamanho()
	 */
	@Override
	public Integer getTamanho() {

		return tamanho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.
	 * ContratoPontoConsumoPCSIntervalo
	 * #setTamanho(java.lang.Integer)
	 */
	@Override
	public void setTamanho(Integer tamanho) {

		this.tamanho = tamanho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.
	 * ContratoPontoConsumoPCSIntervalo
	 * #getIntervaloPCS()
	 */
	@Override
	public IntervaloPCS getIntervaloPCS() {

		return intervaloPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.
	 * ContratoPontoConsumoPCSIntervalo
	 * #setIntervaloPCS
	 * (br.com.ggas.medicao.consumo.IntervaloPCS)
	 */
	@Override
	public void setIntervaloPCS(IntervaloPCS intervaloPCS) {

		this.intervaloPCS = intervaloPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.
	 * ContratoPontoConsumoPCSIntervalo
	 * #getContratoPontoConsumo()
	 */
	@Override
	public ContratoPontoConsumo getContratoPontoConsumo() {

		return contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.
	 * ContratoPontoConsumoPCSIntervalo
	 * #setContratoPontoConsumo
	 * (br.com.ggas.contrato.ContratoPontoConsumo)
	 */
	@Override
	public void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) {

		this.contratoPontoConsumo = contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo#equalsNegocio(br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPCSIntervalo)
	 */
	@Override
	public boolean equalsNegocio(ContratoPontoConsumoPCSIntervalo obj) {

		if (obj != null && this.contratoPontoConsumo != null && this.contratoPontoConsumo.getChavePrimaria() > 0
						&& obj.getContratoPontoConsumo() != null
						&& obj.getContratoPontoConsumo().getChavePrimaria() == this.contratoPontoConsumo.getChavePrimaria()
						&& this.prioridade != null && this.prioridade.equals(obj.getPrioridade())) {
			return true;
		}

		return false;
	}
}
