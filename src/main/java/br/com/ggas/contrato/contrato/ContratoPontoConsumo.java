/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
/**
 * Interface responsável pela assinatura dos métodos relacionaodos ao Contrato de ponto de consumo.
 *
 */
public interface ContratoPontoConsumo extends EntidadeNegocio {

	String BEAN_ID_CONTRATO_PONTO_CONSUMO = "contratoPontoConsumo";

	String PERIODICIDADE_TAKE_OR_PAY = "PERIODICIDADE_TAKE_OR_PAY";

	String CONSUMO_REFERENCIA_TAKE_OR_PAY = "CONSUMO_REFERENCIA_TAKE_OR_PAY";

	String MODALIDADE = "MODALIDADE";

	String DATA_VIGENCIA_QDC = "DATA_VIGENCIA_QDC";

	String PRAZO_REVISAO_QDC = "PRAZO_REVISAO_QDC";

	String DIAS_ANTECEDENCIA_SOLICITACAO_CONSUMO = "DIAS_ANTECEDENCIA_SOLICITACAO_CONSUMO";

	String MESES_SOLICITADOS = "MESES_SOLICITADOS";

	String HORAS_ANTECEDENCIA_ENVIO_DIARIO = "HORAS_ANTECEDENCIA_ENVIO_DIARIO";

	String OPCAO_DIA_REPROGRAMACAO_QDP = "OPCAO_DIA_REPROGRAMACAO_QDP";

	String DATA_INICIO_RECUPERACAO = "CONTRATO_DATA_INICIO_RECUPERACAO";

	String DATA_MAXIMA_RECUPERACAO = "CONTRATO_DATA_MAXIMA_RECUPERACAO";

	String CONSUMO_REFERENCIA_SHIP_OR_PAY = "CONSUMO_REFERENCIA_SHIP_OR_PAY";

	String MEDIDA_CONSUMO_TESTE = "CONTRATO_PONTO_CONSUMO_MEDIDA_CONSUMO_TESTE";

	String VAZAO_INSTANTANEA = "CONTRATO_PONTO_CONSUMO_VAZAO_INSTANTANEA";

	String VAZAO_INSTANTANEA_MINIMA = "CONTRATO_PONTO_CONSUMO_VAZAO_INSTANTANEA_MINIMA";

	String VAZAO_INSTANTANEA_MAXIMA = "CONTRATO_PONTO_CONSUMO_VAZAO_INSTANTANEA_MAXIMA";

	String PRESSAO_FORNECIMENTO = "CONTRATO_PONTO_CONSUMO_PRESSAO_FORNECIMENTO";

	String PRESSAO_COLETADA = "CONTRATO_PONTO_CONSUMO_PRESSAO_COLETADA";

	String PRESSAO_MINIMA_FORNECIMENTO = "CONTRATO_PONTO_CONSUMO_PRESSAO_MINIMA_FORNECIMENTO";

	String PRESSAO_MAXIMA_FORNECIMENTO = "CONTRATO_PONTO_CONSUMO_PRESSAO_MAXIMA_FORNECIMENTO";

	String PRESSAO_LIMITE_FORNECIMENTO = "CONTRATO_PONTO_CONSUMO_PRESSAO_LIMITE_FORNECIMENTO";

	String NUM_ANOS_CTRL_PARADA_CLIENTE = "CONTRATO_NUM_ANOS_CTRL_PARADA_CLIENTE";

	String MAX_TOTAL_PARADAS_CLIENTE = "CONTRATO_MAX_TOTAL_PARADAS_CLIENTE";

	String MAX_ANUAL_PARADAS_CLIENTE = "CONTRATO_MAX_ANUAL_PARADAS_CLIENTE";

	String NUM_DIAS_PROGR_PARADA_CLIENTE = "CONTRATO_NUM_DIAS_PROGR_PARADA_CLIENTE";

	String NUM_DIAS_CONSEC_PARADA_CLIENTE = "CONTRATO_NUM_DIAS_CONSEC_PARADA_CLIENTE";

	String NUM_ANOS_CTRL_PARADA_CDL = "CONTRATO_NUM_ANOS_CTRL_PARADA_CDL";

	String MAX_TOTAL_PARADAS_CDL = "CONTRATO_MAX_TOTAL_PARADAS_CDL";

	String MAX_ANUAL_PARADAS_CDL = "CONTRATO_MAX_ANUAL_PARADAS_CDL";

	String NUM_DIAS_PROGR_PARADA_CDL = "CONTRATO_NUM_DIAS_PROGR_PARADA_CDL";

	String NUM_DIAS_CONSEC_PARADA_CDL = "CONTRATO_NUM_DIAS_CONSEC_PARADA_CDL";

	String FATOR_UNICO_CORRECAO = "CONTRATO_FATOR_UNICO_CORRECAO";

	String EMAIL_CONTATO_OPERACIONAL = "CONTRATO_PONTO_EMAIL_CONTATO_OPERACIONAL";

	String DDD_CONTRATO_OPERACIONAL = "CONTRATO_PONTO_DDD_CONTRATO_OPERACIONAL";

	String NUMERO_CONTATO_OPERACIONAL = "CONTRATO_PONTO_NUMERO_CONTATO_OPERACIONAL";

	String FORNECIMENTO_MINIMO_DIARIO = "CONTRATO_PONTO_CONSUMO_FORNECIMENTO_MINIMO_DIARIO";

	String FORNECIMENTO_MAXIMO_DIARIO = "CONTRATO_PONTO_CONSUMO_FORNECIMENTO_MAXIMO_DIARIO";

	/**
	 * @return O endereço de entrega desse ponto
	 *         de consumo para esse contrato.
	 */
	String getEnderecoFormatado();

	/**
	 * @return Nome da rua e número completo desse
	 *         ponto de consumo para esse
	 *         contrato.
	 */
	String getEnderecoFormatadoRuaNumeroComplemento();

	/**
	 * @return Municipio e UF formatado desse
	 *         ponto de consumo para esse
	 *         contrato.
	 */
	String getEnderecoFormatadoMunicipioUF();

	/**
	 * @return the medidaPressao
	 */
	BigDecimal getMedidaPressao();

	/**
	 * @param medidaPressao
	 *            the medidaPressao to set
	 */
	void setMedidaPressao(BigDecimal medidaPressao);

	/**
	 * @return the unidadePressao
	 */
	Unidade getUnidadePressao();

	/**
	 * @param unidadePressao
	 *            the unidadePressao to set
	 */
	void setUnidadePressao(Unidade unidadePressao);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the contrato
	 */
	Contrato getContrato();

	/**
	 * @param contrato
	 *            the contrato to set
	 */
	void setContrato(Contrato contrato);

	/**
	 * @return the medidaVazaoMaximaInstantanea
	 */
	BigDecimal getMedidaVazaoMaximaInstantanea();

	/**
	 * @param medidaVazaoMaximaInstantanea
	 *            the medidaVazaoMaximaInstantanea
	 *            to set
	 */
	void setMedidaVazaoMaximaInstantanea(BigDecimal medidaVazaoMaximaInstantanea);

	/**
	 * @return the unidadeVazaoMaximaInstantanea
	 */
	Unidade getUnidadeVazaoMaximaInstantanea();

	/**
	 * @param unidadeVazaoMaximaInstantanea
	 *            the
	 *            unidadeVazaoMaximaInstantanea to
	 *            set
	 */
	void setUnidadeVazaoMaximaInstantanea(Unidade unidadeVazaoMaximaInstantanea);

	/**
	 * @return the medidaFornecimentoMaxDiaria
	 */
	BigDecimal getMedidaFornecimentoMaxDiaria();

	/**
	 * @param medidaFornecimentoMaxDiaria
	 *            the medidaFornecimentoMaxDiaria
	 *            to set
	 */
	void setMedidaFornecimentoMaxDiaria(BigDecimal medidaFornecimentoMaxDiaria);

	/**
	 * @return the unidadeFornecimentoMaxDiaria
	 */
	Unidade getUnidadeFornecimentoMaxDiaria();

	/**
	 * @param unidadeFornecimentoMaxDiaria
	 *            the unidadeFornecimentoMaxDiaria
	 *            to set
	 */
	void setUnidadeFornecimentoMaxDiaria(Unidade unidadeFornecimentoMaxDiaria);

	/**
	 * @return the medidaFornecimentoMinDiaria
	 */
	BigDecimal getMedidaFornecimentoMinDiaria();

	/**
	 * @param medidaFornecimentoMinDiaria
	 *            the medidaFornecimentoMinDiaria
	 *            to set
	 */
	void setMedidaFornecimentoMinDiaria(BigDecimal medidaFornecimentoMinDiaria);

	/**
	 * @return the unidadeFornecimentoMinDiaria
	 */
	Unidade getUnidadeFornecimentoMinDiaria();

	/**
	 * @param unidadeFornecimentoMinDiaria
	 *            the unidadeFornecimentoMinDiaria
	 *            to set
	 */
	void setUnidadeFornecimentoMinDiaria(Unidade unidadeFornecimentoMinDiaria);

	/**
	 * @return the medidaFornecimentoMinMensal
	 */
	BigDecimal getMedidaFornecimentoMinMensal();

	/**
	 * @param medidaFornecimentoMinMensal
	 *            the medidaFornecimentoMinMensal
	 *            to set
	 */
	void setMedidaFornecimentoMinMensal(BigDecimal medidaFornecimentoMinMensal);

	/**
	 * @return the unidadeFornecimentoMinMensal
	 */
	Unidade getUnidadeFornecimentoMinMensal();

	/**
	 * @param unidadeFornecimentoMinMensal
	 *            the unidadeFornecimentoMinMensal
	 *            to set
	 */
	void setUnidadeFornecimentoMinMensal(Unidade unidadeFornecimentoMinMensal);

	/**
	 * @return the medidaFornecimentoMinAnual
	 */
	BigDecimal getMedidaFornecimentoMinAnual();

	/**
	 * @param medidaFornecimentoMinAnual
	 *            the medidaFornecimentoMinAnual
	 *            to set
	 */
	void setMedidaFornecimentoMinAnual(BigDecimal medidaFornecimentoMinAnual);

	/**
	 * @return the unidadeFornecimentoMinAnual
	 */
	Unidade getUnidadeFornecimentoMinAnual();

	/**
	 * @param unidadeFornecimentoMinAnual
	 *            the unidadeFornecimentoMinAnual
	 *            to set
	 */
	void setUnidadeFornecimentoMinAnual(Unidade unidadeFornecimentoMinAnual);

	/**
	 * @return the numeroFatorCorrecao
	 */
	BigDecimal getNumeroFatorCorrecao();

	/**
	 * @param numeroFatorCorrecao
	 *            the numeroFatorCorrecao to set
	 */
	void setNumeroFatorCorrecao(BigDecimal numeroFatorCorrecao);

	/**
	 * @return the numeroDiasVencimento
	 */
	Integer getNumeroDiasVencimento();

	/**
	 * @param numeroDiasVencimento
	 *            the numeroDiasVencimento to set
	 */
	void setNumeroDiasVencimento(Integer numeroDiasVencimento);

	/**
	 * @return the
	 *         acaoAnormalidadeConsumoSemLeitura
	 */
	AcaoAnormalidadeConsumo getAcaoAnormalidadeConsumoSemLeitura();

	/**
	 * @param acaoAnormalidadeConsumoSemLeitura
	 *            the
	 *            acaoAnormalidadeConsumoSemLeitura
	 *            to set
	 */
	void setAcaoAnormalidadeConsumoSemLeitura(AcaoAnormalidadeConsumo acaoAnormalidadeConsumoSemLeitura);

	/**
	 * @return the tipoMedicao
	 */
	TipoMedicao getTipoMedicao();

	/**
	 * @param tipoMedicao
	 *            the tipoMedicao to set
	 */
	void setTipoMedicao(TipoMedicao tipoMedicao);

	/**
	 * @return the indicadorParadaProgramada
	 */
	Boolean getIndicadorParadaProgramada();

	/**
	 * @param indicadorParadaProgramada
	 *            the indicadorParadaProgramada to
	 *            set
	 */
	void setIndicadorParadaProgramada(Boolean indicadorParadaProgramada);

	/**
	 * @return the quantidadeAnosParadaCliente
	 */
	Integer getQuantidadeAnosParadaCliente();

	/**
	 * @param quantidadeAnosParadaCliente
	 *            the quantidadeAnosParadaCliente
	 *            to set
	 */
	void setQuantidadeAnosParadaCliente(Integer quantidadeAnosParadaCliente);

	/**
	 * @return the quantidadeTotalParadaCliente
	 */
	Integer getQuantidadeTotalParadaCliente();

	/**
	 * @param quantidadeTotalParadaCliente
	 *            the quantidadeTotalParadaCliente
	 *            to set
	 */
	void setQuantidadeTotalParadaCliente(Integer quantidadeTotalParadaCliente);

	/**
	 * @return the
	 *         quantidadeMaximaAnoParadaCliente
	 */
	Integer getQuantidadeMaximaAnoParadaCliente();

	/**
	 * @param quantidadeMaximaAnoParadaCliente
	 *            the
	 *            quantidadeMaximaAnoParadaCliente
	 *            to set
	 */
	void setQuantidadeMaximaAnoParadaCliente(Integer quantidadeMaximaAnoParadaCliente);

	/**
	 * @return the diasAntecedentesParadaCliente
	 */
	Integer getDiasAntecedentesParadaCliente();

	/**
	 * @param diasAntecedentesParadaCliente
	 *            the
	 *            diasAntecedentesParadaCliente to
	 *            set
	 */
	void setDiasAntecedentesParadaCliente(Integer diasAntecedentesParadaCliente);

	/**
	 * @return the quantidadeAnosParadaCDL
	 */
	Integer getQuantidadeAnosParadaCDL();

	/**
	 * @param quantidadeAnosParadaCDL
	 *            the quantidadeAnosParadaCDL to
	 *            set
	 */
	void setQuantidadeAnosParadaCDL(Integer quantidadeAnosParadaCDL);

	/**
	 * @return the quantidadeTotalParadaCDL
	 */
	Integer getQuantidadeTotalParadaCDL();

	/**
	 * @param quantidadeTotalParadaCDL
	 *            the quantidadeTotalParadaCDL to
	 *            set
	 */
	void setQuantidadeTotalParadaCDL(Integer quantidadeTotalParadaCDL);

	/**
	 * @return the quantidadeMaximaAnoParadaCDL
	 */
	Integer getQuantidadeMaximaAnoParadaCDL();

	/**
	 * @param quantidadeMaximaAnoParadaCDL
	 *            the quantidadeMaximaAnoParadaCDL
	 *            to set
	 */
	void setQuantidadeMaximaAnoParadaCDL(Integer quantidadeMaximaAnoParadaCDL);

	/**
	 * @return the diasAntecedentesParadaCDL
	 */
	Integer getDiasAntecedentesParadaCDL();

	/**
	 * @param diasAntecedentesParadaCDL
	 *            the diasAntecedentesParadaCDL to
	 *            set
	 */
	void setDiasAntecedentesParadaCDL(Integer diasAntecedentesParadaCDL);

	/**
	 * @return the dataInicioTeste
	 */
	Date getDataInicioTeste();

	/**
	 * @param dataInicioTeste
	 *            the dataInicioTeste to set
	 */
	void setDataInicioTeste(Date dataInicioTeste);

	/**
	 * @return the dataFimTeste
	 */
	Date getDataFimTeste();

	/**
	 * @param dataFimTeste
	 *            the dataFimTeste to set
	 */
	void setDataFimTeste(Date dataFimTeste);

	/**
	 * @return the numeroImovel
	 */
	String getNumeroImovel();

	/**
	 * @param numeroImovel
	 *            the numeroImovel to set
	 */
	void setNumeroImovel(String numeroImovel);

	/**
	 * @return the complementoEndereco
	 */
	String getComplementoEndereco();

	/**
	 * @param complementoEndereco
	 *            the complementoEndereco to set
	 */
	void setComplementoEndereco(String complementoEndereco);

	/**
	 * @return the emailEntrega
	 */
	String getEmailEntrega();

	/**
	 * @param emailEntrega
	 *            the emailEntrega to set
	 */
	void setEmailEntrega(String emailEntrega);

	/**
	 * @return the indicadorCorretorVazao
	 */
	Boolean getIndicadorCorretorVazao();

	/**
	 * @param indicadorCorretorVazao
	 *            the indicadorCorretorVazao to
	 *            set
	 */
	void setIndicadorCorretorVazao(Boolean indicadorCorretorVazao);

	/**
	 * @return the medidaConsumoTeste
	 */
	BigDecimal getMedidaConsumoTeste();

	/**
	 * @param medidaConsumoTeste
	 *            the medidaConsumoTeste to set
	 */
	void setMedidaConsumoTeste(BigDecimal medidaConsumoTeste);

	/**
	 * @return the prazoConsumoTeste
	 */
	Integer getPrazoConsumoTeste();

	/**
	 * @param prazoConsumoTeste
	 *            the prazoConsumoTeste to set
	 */
	void setPrazoConsumoTeste(Integer prazoConsumoTeste);

	/**
	 * @return the diaGarantiaConversao
	 */
	Integer getDiaGarantiaConversao();

	/**
	 * @param diaGarantiaConversao
	 *            the diaGarantiaConversao to set
	 */
	void setDiaGarantiaConversao(Integer diaGarantiaConversao);

	/**
	 * @return the dataInicioGarantiaConversao
	 */
	Date getDataInicioGarantiaConversao();

	/**
	 * @param dataInicioGarantiaConversao
	 *            the dataInicioGarantiaConversao
	 *            to set
	 */
	void setDataInicioGarantiaConversao(Date dataInicioGarantiaConversao);

	/**
	 * @return the regimeConsumo
	 */
	EntidadeConteudo getRegimeConsumo();

	/**
	 * @param regimeConsumo
	 *            the regimeConsumo to set
	 */
	void setRegimeConsumo(EntidadeConteudo regimeConsumo);

	/**
	 * @return the cep
	 */
	Cep getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(Cep cep);

	/**
	 * @return the emailOperacional
	 */
	String getEmailOperacional();

	/**
	 * @param emailOperacional
	 *            the emailOperacional to set
	 */
	void setEmailOperacional(String emailOperacional);

	/**
	 * @return the codigoDDDFaxOperacional
	 */
	Integer getCodigoDDDFaxOperacional();

	/**
	 * @param codigoDDDFaxOperacional
	 *            the codigoDDDFaxOperacional to
	 *            set
	 */
	void setCodigoDDDFaxOperacional(Integer codigoDDDFaxOperacional);

	/**
	 * @return the numeroFaxOperacional
	 */
	Integer getNumeroFaxOperacional();

	/**
	 * @param numeroFaxOperacional
	 *            the numeroFaxOperacional to set
	 */
	void setNumeroFaxOperacional(Integer numeroFaxOperacional);

	/**
	 * @return the
	 *         listaContratoPontoConsumoModalidade
	 */
	Collection<ContratoPontoConsumoModalidade> getListaContratoPontoConsumoModalidade();

	/**
	 * @param listaContratoPontoConsumoModalidade
	 *            the
	 *            listaContratoPontoConsumoModalidade
	 *            to set
	 */
	void setListaContratoPontoConsumoModalidade(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade);

	/**
	 * @return the
	 *         listaContratoPontoConsumoItemFaturamento
	 */
	Collection<ContratoPontoConsumoItemFaturamento> getListaContratoPontoConsumoItemFaturamento();

	/**
	 * obtem o contrato ponto consumo item faturamento do
	 * contrato ponto consumo com o codigo do item fatura fornecido
	 * 
	 * @param idItemFatura chave primaria do item fatura
	 * @return contrato ponto consumo item faturamento do
	 *         contrato ponto consumo com o codigo do item fatura fornecido
	 */
	ContratoPontoConsumoItemFaturamento getContratoPontoConsumoItemFaturamento(long idItemFatura);

	/**
	 * @param listaContratoPontoConsumoItemFaturamento
	 *            the
	 *            listaContratoPontoConsumoItemFaturamento
	 *            to set
	 */
	void setListaContratoPontoConsumoItemFaturamento(
					Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento);

	/**
	 * @return the
	 *         listaContratoPontoConsumoPCSAmostragem
	 */
	Collection<ContratoPontoConsumoPCSAmostragem> getListaContratoPontoConsumoPCSAmostragem();

	/**
	 * @param listaContratoPontoConsumoPCSAmostragem
	 *            the
	 *            listaContratoPontoConsumoPCSAmostragem
	 *            to set
	 */
	void setListaContratoPontoConsumoPCSAmostragem(Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoConsumoPCSAmostragem);

	/**
	 * @return the
	 *         listaContratoPontoConsumoPCSIntervalo
	 */
	Collection<ContratoPontoConsumoPCSIntervalo> getListaContratoPontoConsumoPCSIntervalo();

	/**
	 * @param listaContratoPontoConsumoPCSIntervalo
	 *            the
	 *            listaContratoPontoConsumoPCSIntervalo
	 *            to set
	 */
	void setListaContratoPontoConsumoPCSIntervalo(Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoConsumoPCSIntervalo);

	/**
	 * @return the medidaPressaoMinima
	 */
	BigDecimal getMedidaPressaoMinima();

	/**
	 * @param medidaPressaoMinima
	 *            the medidaPressaoMinima to set
	 */
	void setMedidaPressaoMinima(BigDecimal medidaPressaoMinima);

	/**
	 * @return the unidadePressaoMinima
	 */
	Unidade getUnidadePressaoMinima();

	/**
	 * @param unidadePressaoMinima
	 *            the unidadePressaoMinima to set
	 */
	void setUnidadePressaoMinima(Unidade unidadePressaoMinima);

	/**
	 * @return the medidaPressaoMaxima
	 */
	BigDecimal getMedidaPressaoMaxima();

	/**
	 * @param medidaPressaoMaxima
	 *            the medidaPressaoMaxima to set
	 */
	void setMedidaPressaoMaxima(BigDecimal medidaPressaoMaxima);

	/**
	 * @return the unidadePressaoMaxima
	 */
	Unidade getUnidadePressaoMaxima();

	/**
	 * @param unidadePressaoMaxima
	 *            the unidadePressaoMaxima to set
	 */
	void setUnidadePressaoMaxima(Unidade unidadePressaoMaxima);

	/**
	 * @return the medidaPressaoLimite
	 */
	BigDecimal getMedidaPressaoLimite();

	/**
	 * @param medidaPressaoLimite
	 *            the medidaPressaoLimite to set
	 */
	void setMedidaPressaoLimite(BigDecimal medidaPressaoLimite);

	/**
	 * @return the unidadePressaoLimite
	 */
	Unidade getUnidadePressaoLimite();

	/**
	 * @param unidadePressaoLimite
	 *            the unidadePressaoLimite to set
	 */
	void setUnidadePressaoLimite(Unidade unidadePressaoLimite);

	/**
	 * @return the medidaVazaoInstantanea
	 */
	BigDecimal getMedidaVazaoInstantanea();

	/**
	 * @param medidaVazaoInstantanea
	 *            the medidaVazaoInstantanea to
	 *            set
	 */
	void setMedidaVazaoInstantanea(BigDecimal medidaVazaoInstantanea);

	/**
	 * @return the unidadeVazaoInstantanea
	 */
	Unidade getUnidadeVazaoInstantanea();

	/**
	 * @param unidadeVazaoInstantanea
	 *            the unidadeVazaoInstantanea to
	 *            set
	 */
	void setUnidadeVazaoInstantanea(Unidade unidadeVazaoInstantanea);

	/**
	 * @return the medidaVazaoMinimaInstantanea
	 */
	BigDecimal getMedidaVazaoMinimaInstantanea();

	/**
	 * @param medidaVazaoMinimaInstantanea
	 *            the medidaVazaoMinimaInstantanea
	 *            to set
	 */
	void setMedidaVazaoMinimaInstantanea(BigDecimal medidaVazaoMinimaInstantanea);

	/**
	 * @return the unidadeVazaoMinimaInstantanea
	 */
	Unidade getUnidadeVazaoMinimaInstantanea();

	/**
	 * @param unidadeVazaoMinimaInstantanea
	 *            the
	 *            unidadeVazaoMinimaInstantanea to
	 *            set
	 */
	void setUnidadeVazaoMinimaInstantanea(Unidade unidadeVazaoMinimaInstantanea);

	/**
	 * @return the numeroHoraInicial
	 */
	Integer getNumeroHoraInicial();

	/**
	 * @param numeroHoraInicial
	 *            the numeroHoraInicial to set
	 */
	void setNumeroHoraInicial(Integer numeroHoraInicial);

	/**
	 * @return the diasConsecutivosParadaCliente
	 */
	Integer getDiasConsecutivosParadaCliente();

	/**
	 * @param diasConsecutivosParadaCliente
	 *            the
	 *            diasConsecutivosParadaCliente to
	 *            set
	 */
	void setDiasConsecutivosParadaCliente(Integer diasConsecutivosParadaCliente);

	/**
	 * @return the diasConsecutivosParadaCDL
	 */
	Integer getDiasConsecutivosParadaCDL();

	/**
	 * @param diasConsecutivosParadaCDL
	 *            the diasConsecutivosParadaCDL to
	 *            set
	 */
	void setDiasConsecutivosParadaCDL(Integer diasConsecutivosParadaCDL);

	/**
	 * @return the contratoCompra
	 */
	EntidadeConteudo getContratoCompra();

	/**
	 * @param contratoCompra
	 *            the contratoCompra to set
	 */
	void setContratoCompra(EntidadeConteudo contratoCompra);

	/**
	 * @return the dataConsumoTesteExcedido
	 */
	Date getDataConsumoTesteExcedido();

	/**
	 * @param dataConsumoTesteExcedido
	 *            the dataConsumoTesteExcedido to
	 *            set
	 */
	void setDataConsumoTesteExcedido(Date dataConsumoTesteExcedido);

	/**
	 * @return the medidaConsumoTesteUsado
	 */
	BigDecimal getMedidaConsumoTesteUsado();

	/**
	 * @param medidaConsumoTesteUsado
	 *            the medidaConsumoTesteUsado to
	 *            set
	 */
	void setMedidaConsumoTesteUsado(BigDecimal medidaConsumoTesteUsado);

	/**
	 * @return the periodicidade
	 */
	Periodicidade getPeriodicidade();

	/**
	 * @param periodicidade
	 *            the periodicidade to set
	 */
	void setPeriodicidade(Periodicidade periodicidade);

	/**
	 * Equals negocio.
	 * 
	 * @param obj
	 *            the obj
	 * @return true, if successful
	 */
	boolean equalsNegocio(ContratoPontoConsumo obj);

	/**
	 * @return FaixaPressaoFornecimento - Retorna Faixa pressão fornecimento.
	 */
	FaixaPressaoFornecimento getFaixaPressaoFornecimento();

	/**
	 * @param faixaPressaoFornecimento - Set Faixa pressão fornecimento.
	 */
	void setFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento);

	/**
	 * @return Boolean - Retorna emite nota fiscal eletrônica.
	 */
	public Boolean getIsEmiteNotaFiscalEletronica();

	/**
	 * @param notaFiscalEletronicaAtiva - Set Emite nota fiscal eletrônica.
	 */
	public void setIsEmiteNotaFiscalEletronica(Boolean notaFiscalEletronicaAtiva);

	/**
	 * @return Boolean - Emitir fatura com NFE.
	 */
	Boolean getEmitirFaturaComNfe();

	/**
	 * @param emitirFaturaComNfe - Set Emitir fatura com NFE.
	 */
	void setEmitirFaturaComNfe(Boolean emitirFaturaComNfe);

	/**
	 * @return BigDecimal - Retorna Medida pressão coletaa.
	 */
	BigDecimal getMedidaPressaoColetada();

	/**
	 * @param medidaPressaoColetada - Set Medida pressão coletada.
	 */
	void setMedidaPressaoColetada(BigDecimal medidaPressaoColetada);

	/**
	 * @return Unidade - Retorna objeto unidade com informações unidade pressão coletada.
	 */
	Unidade getUnidadePressaoColetada();

	/**
	 * @param unidadePressaoColetada - Set Unidade Pressão Coletada.
	 */
	void setUnidadePressaoColetada(Unidade unidadePressaoColetada);

}
