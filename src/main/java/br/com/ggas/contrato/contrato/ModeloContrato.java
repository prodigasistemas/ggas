/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.util.Collection;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Modelo de Contrato.
 *  
 *
 */
public interface ModeloContrato extends EntidadeNegocio {

	String BEAN_ID_MODELO_CONTRATO = "modeloContrato";

	String MODELO_CONTRATO = "MODELO_CONTRATO";

	String MODELOS_CONTRATO = "MODELOS_CONTRATO_ROTULO";

	String DESCRICAO = "MODELO_CONTRATO_DESCRICAO";

	String DESCRICAO_ABREVIADA = "MODELO_CONTRATO_DESCRICAO_ABREV";

	String ATRIBUTO_OBRIGATORIO_CONSUMO = "MODELO_ATRIBUTO_OBRIGATORIO_CONSUMO";

	String MODELO_ATRIBUTO_OBRIGATORIO_QDC = "MODELO_ATRIBUTO_OBRIGATORIO_QDC";

	String MODELO_ATRIBUTO_OBRIGATORIO_VECIMENTO_CONTRATUAL = "MODELO_ATRIBUTO_OBRIGATORIO_VECIMENTO_CONTRATUAL";

	String MODELO_ATRIBUTO_QDC = "MODELO_ATRIBUTO_QDC";

	String MODELO_ATRIBUTO_VECIMENTO_CONTRATUAL = "MODELO_ATRIBUTO_VECIMENTO_CONTRATUAL";

	String ATRIBUTO_OBRIGATORIO_FATURAMENTO_AGRUPADO = "MODELO_ATRIBUTO_OBRIGATORIO_FATURAMENTO_AGRUPADO";

	String ATRIBUTO_OBRIGATORIO_FORMA_COBRANCA = "MODELO_ATRIBUTO_OBRIGATORIO_FORMA_COBRANCA";

	String ATRIBUTO_OBRIGATORIO_CONSUMO_ITEM_FATURA = "MODELO_ATRIBUTO_OBRIGATORIO_CONSUMO_ITEM_FATURA";

	String MODELO_CONTRATO_TIPO_MODELO = "MODELO_CONTRATO_TIPO_MODELO";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @return the layoutRelatorio
	 */
	LayoutRelatorio getLayoutRelatorio();

	/**
	 * Retorna a descricao, confirmando se a última alteração foi descrita
	 * @param ultimaAlteracaoDescrita - Recebe um boolean confirmando se a última alteração foi descrita.
	 * @return String - Retorna última alteração Decrita.
	 */
	String getDescricao(boolean ultimaAlteracaoDescrita);

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @param layoutRelatorio the layoutRelatorio to set
	 */
	void setLayoutRelatorio(LayoutRelatorio layoutRelatorio);

	/**
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * @param descricaoAbreviada
	 *            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * @return o documento
	 */
	byte[] getDocumento();

	/**
	 * @param documento - Set Documento.
	 */
	void setDocumento(byte[] documento);

	/**
	 * @return the atributos
	 */
	Collection<ModeloAtributo> getAtributos();

	/**
	 * @param atributos
	 *            the atributos to set
	 */
	void setAtributos(Collection<ModeloAtributo> atributos);

	/**
	 * @return the abas
	 */
	Collection<AbaModelo> getAbas();

	/**
	 * @param abas
	 *            the abas to set
	 */
	void setAbas(Collection<AbaModelo> abas);

	/**
	 * @return String - Retorna Forma cobrança.
	 */
	String getFormaCobranca();

	/**
	 * @param formaCobranca - Set Forma cobrança.
	 */
	void setFormaCobranca(String formaCobranca);

	/**
	 * @return String - Retorna item fatura.
	 */
	String getItemFatura();

	/**
	 * @param itemFatura - Set Item fatura.
	 */
	void setItemFatura(String itemFatura);

	/**
	 * @return EntidadeConteudo - Retorna objeto Entidade conteudo com informações
	 * sobre tipo modelo.
	 */
	EntidadeConteudo getTipoModelo();

	/**
	 * @param tipoModelo - Set Tipo modelo.
	 */
	void setTipoModelo(EntidadeConteudo tipoModelo);

	/**
	 * @return the modeloContratoVersaoAnterior
	 */
	ModeloContrato getModeloContratoVersaoAnterior();

	/**
	 * @param modeloContratoVersaoAnterior
	 *            the modeloContratoVersaoAnterior to set
	 */
	void setModeloContratoVersaoAnterior(ModeloContrato modeloContratoVersaoAnterior);

	/**
	 * Sets the geracao.
	 *
	 * @param geracao the new geracao
	 */
	void setGeracao(int geracao);

	/**
	 * Gets the geracao.
	 *
	 * @return the geracao
	 */
	int getGeracao();

}
