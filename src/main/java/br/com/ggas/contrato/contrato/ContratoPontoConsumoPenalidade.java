/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Mapeavel;


/**
 * ContratoPontoConsumoPenalidade
 * 
 * @author Pedro
 * 
 * Interface responsável pela assinatura dos métodos relacionados a penalidade 
 * associada ao Contrato do Ponto de Consumo.
 *
 */
public interface ContratoPontoConsumoPenalidade extends EntidadeNegocio, Mapeavel<Long> {

	String BEAN_ID_CONTRATO_PONTO_CONSUMO_PENALIDADE = "contratoPontoConsumoPenalidade";

	String PERCENTUAL_MARGEM_VARIACAO = "CONTRATO_PERCENTUAL_MARGEM_VARIACAO";

	String PERCENTUAL_MAXIMO_QDC = "CONTRATO_PERCENTUAL_MAXIMO_QDC";

	String PERCENTUAL_MINIMO_QDC = "CONTRATO_PERCENTUAL_MINIMO_QDC";

	/**
	 * Gets the percentual margem variacao.
	 *
	 * @return the percentualMargemVariacao
	 */
	BigDecimal getPercentualMargemVariacao();

	/**
	 * Sets the percentual margem variacao.
	 *
	 * @param percentualMargemVariacao            the percentualMargemVariacao to
	 *            set
	 */
	void setPercentualMargemVariacao(BigDecimal percentualMargemVariacao);

	/**
	 * Gets the periodicidade penalidade.
	 *
	 * @return the periodicidadePenalidade
	 */
	EntidadeConteudo getPeriodicidadePenalidade();

	/**
	 * Sets the periodicidade penalidade.
	 *
	 * @param periodicidadePenalidade            the periodicidadePenalidade to
	 *            set
	 */
	void setPeriodicidadePenalidade(EntidadeConteudo periodicidadePenalidade);

	/**
	 * Gets the consumo referencia.
	 *
	 * @return the consumoReferencia
	 */
	EntidadeConteudo getConsumoReferencia();

	/**
	 * Sets the consumo referencia.
	 *
	 * @param consumoReferencia            the consumoReferencia to set
	 */
	void setConsumoReferencia(EntidadeConteudo consumoReferencia);

	/**
	 * Gets the contrato ponto consumo modalidade.
	 *
	 * @return the contratoPontoConsumoModalidade
	 */
	ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade();

	/**
	 * Sets the contrato ponto consumo modalidade.
	 *
	 * @param contratoPontoConsumoModalidade            the
	 *            contratoPontoConsumoModalidade
	 *            to set
	 */
	void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade);

	/**
	 * Gets the penalidade.
	 *
	 * @return the penalidade
	 */
	Penalidade getPenalidade();

	/**
	 * Sets the penalidade.
	 *
	 * @param penalidade            the penalidade to set
	 */
	void setPenalidade(Penalidade penalidade);

	/**
	 * Método rsponsável por retorno o
	 * PercentualMargemVariacao formatado.
	 *
	 * @return PercentualMargemVariacaoFormatado
	 * @throws GGASException the GGAS exception
	 */
	String getPercentualMargemVariacaoFormatado() throws GGASException;

	/**
	 * Equals negocio.
	 * 
	 * @param obj
	 *            the obj
	 * @return true, if successful
	 */
	boolean equalsNegocio(ContratoPontoConsumoPenalidade obj);

	/**
	 * Gets the referencia QF parada programada.
	 *
	 * @return the referencia QF parada programada
	 */
	EntidadeConteudo getReferenciaQFParadaProgramada();

	/**
	 * Sets the referencia QF parada programada.
	 *
	 * @param referenciaQFParadaProgramada the new referencia QF parada programada
	 */
	void setReferenciaQFParadaProgramada(EntidadeConteudo referenciaQFParadaProgramada);

	/**
	 * Gets the data inicio vigencia.
	 *
	 * @return the data inicio vigencia
	 */
	Date getDataInicioVigencia();

	/**
	 * Sets the data inicio vigencia.
	 *
	 * @param dataInicioVigencia the new data inicio vigencia
	 */
	void setDataInicioVigencia(Date dataInicioVigencia);

	/**
	 * Gets the data fim vigencia.
	 *
	 * @return the data fim vigencia
	 */
	Date getDataFimVigencia();

	/**
	 * Sets the data fim vigencia.
	 *
	 * @param dataFimVigencia the new data fim vigencia
	 */
	void setDataFimVigencia(Date dataFimVigencia);

	/**
	 * Gets the percentual nao recuperavel.
	 *
	 * @return the percentual nao recuperavel
	 */
	BigDecimal getPercentualNaoRecuperavel();

	/**
	 * Sets the percentual nao recuperavel.
	 *
	 * @param percentualNaoRecuperavel the new percentual nao recuperavel
	 */
	void setPercentualNaoRecuperavel(BigDecimal percentualNaoRecuperavel);

	/**
	 * Gets the considera parada programada.
	 *
	 * @return the considera parada programada
	 */
	Boolean getConsideraParadaProgramada();

	/**
	 * Sets the considera parada programada.
	 *
	 * @param consideraParadaProgramada the new considera parada programada
	 */
	void setConsideraParadaProgramada(Boolean consideraParadaProgramada);

	/**
	 * Gets the considera falha fornecimento.
	 *
	 * @return the considera falha fornecimento
	 */
	Boolean getConsideraFalhaFornecimento();

	/**
	 * Sets the considera falha fornecimento.
	 *
	 * @param consideraFalhaFornecimento the new considera falha fornecimento
	 */
	void setConsideraFalhaFornecimento(Boolean consideraFalhaFornecimento);

	/**
	 * Gets the considera caso fortuito.
	 *
	 * @return the considera caso fortuito
	 */
	Boolean getConsideraCasoFortuito();

	/**
	 * Sets the considera caso fortuito.
	 *
	 * @param consideraCasoFortuito the new considera caso fortuito
	 */
	void setConsideraCasoFortuito(Boolean consideraCasoFortuito);

	/**
	 * Gets the recuperavel.
	 *
	 * @return the recuperavel
	 */
	Boolean getRecuperavel();

	/**
	 * Sets the recuperavel.
	 *
	 * @param recuperavel the new recuperavel
	 */
	void setRecuperavel(Boolean recuperavel);

	/**
	 * Gets the tipo apuracao.
	 *
	 * @return the tipo apuracao
	 */
	EntidadeConteudo getTipoApuracao();

	/**
	 * Sets the tipo apuracao.
	 *
	 * @param tipoApuracao the new tipo apuracao
	 */
	void setTipoApuracao(EntidadeConteudo tipoApuracao);

	/**
	 * Gets the preco cobranca.
	 *
	 * @return the preco cobranca
	 */
	EntidadeConteudo getPrecoCobranca();

	/**
	 * Sets the preco cobranca.
	 *
	 * @param precoCobranca the new preco cobranca
	 */
	void setPrecoCobranca(EntidadeConteudo precoCobranca);

	/**
	 * Gets the apuracao parada programada.
	 *
	 * @return the apuracao parada programada
	 */
	EntidadeConteudo getApuracaoParadaProgramada();

	/**
	 * Sets the apuracao parada programada.
	 *
	 * @param apuracaoParadaProgramada the new apuracao parada programada
	 */
	void setApuracaoParadaProgramada(EntidadeConteudo apuracaoParadaProgramada);

	/**
	 * Gets the apuracao caso fortuito.
	 *
	 * @return the apuracao caso fortuito
	 */
	EntidadeConteudo getApuracaoCasoFortuito();

	/**
	 * Sets the apuracao caso fortuito.
	 *
	 * @param apuracaoCasoFortuito the new apuracao caso fortuito
	 */
	void setApuracaoCasoFortuito(EntidadeConteudo apuracaoCasoFortuito);

	/**
	 * Gets the apuracao falha fornecimento.
	 *
	 * @return the apuracao falha fornecimento
	 */
	EntidadeConteudo getApuracaoFalhaFornecimento();

	/**
	 * Sets the apuracao falha fornecimento.
	 *
	 * @param apuracaoFalhaFornecimento the new apuracao falha fornecimento
	 */
	void setApuracaoFalhaFornecimento(EntidadeConteudo apuracaoFalhaFornecimento);

	/**
	 * Gets the tipo agrupamento contrato.
	 *
	 * @return the tipo agrupamento contrato
	 */
	EntidadeConteudo getTipoAgrupamentoContrato();

	/**
	 * Sets the tipo agrupamento contrato.
	 *
	 * @param tipoAgrupamento the new tipo agrupamento contrato
	 */
	void setTipoAgrupamentoContrato(EntidadeConteudo tipoAgrupamento);

	/**
	 * Gets the base apuracao.
	 *
	 * @return the base apuracao
	 */
	EntidadeConteudo getBaseApuracao();

	/**
	 * Sets the base apuracao.
	 *
	 * @param baseApuracao the new base apuracao
	 */
	void setBaseApuracao(EntidadeConteudo baseApuracao);

	/**
	 * Gets the valor percentual cob ret maior menor.
	 *
	 * @return the valor percentual cob ret maior menor
	 */
	BigDecimal getValorPercentualCobRetMaiorMenor();

	/**
	 * Sets the valor percentual cob ret maior menor.
	 *
	 * @param valorPercentualCobRetMaiorMenor the new valor percentual cob ret maior menor
	 */
	void setValorPercentualCobRetMaiorMenor(BigDecimal valorPercentualCobRetMaiorMenor);

	/**
	 * Gets the valor percentual cob int ret maior menor.
	 *
	 * @return the valor percentual cob int ret maior menor
	 */
	BigDecimal getValorPercentualCobIntRetMaiorMenor();

	/**
	 * Sets the valor percentual cob int ret maior menor.
	 *
	 * @param valorPercentualCobIntRetMaiorMenor the new valor percentual cob int ret maior menor
	 */
	void setValorPercentualCobIntRetMaiorMenor(BigDecimal valorPercentualCobIntRetMaiorMenor);

	/**
	 * Gets the valor percentual ret maior menor.
	 *
	 * @return the valor percentual ret maior menor
	 */
	BigDecimal getValorPercentualRetMaiorMenor();

	/**
	 * Sets the valor percentual ret maior menor.
	 *
	 * @param valorPercentualRetMaiorMenor the new valor percentual ret maior menor
	 */
	void setValorPercentualRetMaiorMenor(BigDecimal valorPercentualRetMaiorMenor);

	/**
	 * Gets the indicador imposto.
	 *
	 * @return the indicador imposto
	 */
	Boolean getIndicadorImposto();

	/**
	 * Sets the indicador imposto.
	 *
	 * @param indicadorImposto the new indicador imposto
	 */
	void setIndicadorImposto(Boolean indicadorImposto);
}
