/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.relatorio.RelatorioToken;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador do Modelo de Contrato.
 *  
 */
public interface ControladorModeloContrato extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_MODELO_CONTRATO = "controladorModeloContrato";

	/**
	 * Criar modelo atributo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarModeloAtributo();

	/**
	 * Método responsável por obter a entidade
	 * abaAtributo pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade.
	 * @return AbaAtributo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	AbaAtributo obterAbaAtributo(long chavePrimaria) throws NegocioException;

	/**
	 * Obter aba atributo.
	 *
	 * @param nomeColuna the nome coluna
	 * @return the aba atributo
	 * @throws NegocioException the negocio exception
	 */
	AbaAtributo obterAbaAtributo(String nomeColuna) throws NegocioException;

	/**
	 * Método responsável por pesquisar modelos de
	 * contrato cadastrados no sistema.
	 * 
	 * @param filtro
	 *            Filtro com parâmetros a serem
	 *            considerados na pesquisa.
	 * @return Coleção de modelos de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ModeloContrato> consultarModelosContrato(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * modelos de contrato.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos modelos
	 *            de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverModelosContrato(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se a
	 * chave selecionada para a atualização de
	 * modelos de contrato pode ser alterada.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do modelo
	 *            de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarAtualizarModeloContrato(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por listar as abas
	 * atributos do sistema.
	 * 
	 * @return Coleção de abas atributos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<AbaAtributo> listarAbasAtributos() throws NegocioException;

	/**
	 * Obter lista situacao contrato.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SituacaoContrato> obterListaSituacaoContrato() throws NegocioException;

	/**
	 * Método responsável por listar as situações
	 * do contrato que podem ser padrão.
	 * 
	 * @return Coleção de situação contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoContrato> obterListaSituacaoContratoPadrao() throws NegocioException;

	/**
	 * Criar aba modelo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarAbaModelo();

	/**
	 * Método responsável por validar o dado
	 * padrão de uma atributo do modelo.
	 * 
	 * @param abaAtributo
	 *            Atributo de um modelo.
	 * @param valorPadrao
	 *            Valor padrão informado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro de
	 *             formato na invocação do método.
	 */
	void validarDadoAtributoModeloContrato(AbaAtributo abaAtributo, String valorPadrao) throws NegocioException, FormatoInvalidoException;

	/**
	 * Método responsável por alterar um modelo de
	 * contrato.
	 * 
	 * @param modeloContrato
	 *            O modelo do contrato alterado.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro de
	 *             concorrência do método.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void atualizar(ModeloContrato modeloContrato) throws ConcorrenciaException, NegocioException;

	/**
	 * Obter lista situacao contrato alterado.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SituacaoContrato> obterListaSituacaoContratoAlterado() throws NegocioException;

	/**
	 * Obter lista situacao contrato aditado.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SituacaoContrato> obterListaSituacaoContratoAditado() throws NegocioException;

	/**
	 * Obtem o modeloAtributo do modeloContrato.
	 * 
	 * @param modeloContrato
	 *            the modelo contrato
	 * @param nomeColunaAtributo
	 *            the nome coluna atributo
	 * @return the modelo atributo
	 */
	ModeloAtributo obterModeloAtributo(ModeloContrato modeloContrato, String nomeColunaAtributo);

	/**
	 * Validar dados regra faturamento.
	 * 
	 * @param selec
	 *            the selec
	 * @param obrig
	 *            the obrig
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosRegraFaturamento(Boolean selec, Boolean obrig) throws NegocioException;

	/**
	 * Validar modelo contrato complementar.
	 * 
	 * @param modeloContrato
	 *            the modelo contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarModeloContratoComplementar(ModeloContrato modeloContrato) throws NegocioException;

	/**
	 * Obter lista modelo constrato por descricao.
	 * 
	 * @param descricaoModelo
	 *            the descricao modelo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ModeloContrato> obterListaModeloConstratoPorDescricao(String descricaoModelo) throws NegocioException;

	/**
	 * Metodo que obtem a proxima versao do modelo que possui
	 * a chave passada como parametro. Caso nao haja proxima
	 * versao, o metodo retorna objeto nulo
	 * 
	 * @param chavePrimaria
	 *            chave do modelo que se quer obter a
	 *            proxima versao
	 * @return the modelo contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ModeloContrato obterProximaVersaoModeloContrato(Long chavePrimaria) throws NegocioException;

	/**
	 * Checks if is aba atributo perc cobr aviso interrup retir maior menor modalid.
	 *
	 * @param abaAtributo the aba atributo
	 * @return true, if is aba atributo perc cobr aviso interrup retir maior menor modalid
	 */
	boolean isAbaAtributoPercCobrAvisoInterrupRetirMaiorMenorModalid(AbaAtributo abaAtributo);

	/**
	 * Checks if is aba atributo percentual cobr aviso interrup retir maior menor.
	 *
	 * @param abaAtributo the aba atributo
	 * @return true, if is aba atributo percentual cobr aviso interrup retir maior menor
	 */
	boolean isAbaAtributoPercentualCobrAvisoInterrupRetirMaiorMenor(AbaAtributo abaAtributo);

	/**
	 * Criar layout relatorio.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarLayoutRelatorio();

}
