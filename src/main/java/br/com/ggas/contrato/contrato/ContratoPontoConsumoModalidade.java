/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface rsponsável pela assinatura dos métodos relacionados a modalidade do Contrato de Ponto de Consumo. 
 *
 */
public interface ContratoPontoConsumoModalidade extends EntidadeNegocio {

	String BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE = "contratoPontoConsumoModalidade";

	String VALIDADE_RETIRADA_QPNR = "CONTRATO_VALIDADE_RETIRADA_QPNR";

	/**
	 * @return the contratoPontoConsumo
	 */
	ContratoPontoConsumo getContratoPontoConsumo();

	/**
	 * @param contratoPontoConsumo
	 *            the contratoPontoConsumo to set
	 */
	void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo);

	/**
	 * @return the contratoModalidade
	 */
	ContratoModalidade getContratoModalidade();

	/**
	 * @param contratoModalidade
	 *            the contratoModalidade to set
	 */
	void setContratoModalidade(ContratoModalidade contratoModalidade);

	/**
	 * @return the indicadorProgramacaoConsumo
	 */
	Boolean getIndicadorProgramacaoConsumo();

	/**
	 * @param indicadorProgramacaoConsumo
	 *            the indicadorProgramacaoConsumo
	 *            to set
	 */
	void setIndicadorProgramacaoConsumo(Boolean indicadorProgramacaoConsumo);

	/**
	 * @return the indicadorQDSMaiorQDC
	 */
	Boolean getIndicadorQDSMaiorQDC();

	/**
	 * @param indicadorQDSMaiorQDC
	 *            the indicadorQDSMaiorQDC to set
	 */
	void setIndicadorQDSMaiorQDC(Boolean indicadorQDSMaiorQDC);

	/**
	 * @return the diasAntecedenciaQDS
	 */
	Integer getDiasAntecedenciaQDS();

	/**
	 * @param diasAntecedenciaQDS
	 *            the diasAntecedenciaQDS to set
	 */
	void setDiasAntecedenciaQDS(Integer diasAntecedenciaQDS);

	/**
	 * @return the prazoRevisaoQDC
	 */
	Date getPrazoRevisaoQDC();

	/**
	 * @param prazoRevisaoQDC
	 *            the prazoRevisaoQDC to set
	 */
	void setPrazoRevisaoQDC(Date prazoRevisaoQDC);

	/**
	 * @return the mesesQDS
	 */
	Integer getMesesQDS();

	/**
	 * @param mesesQDS
	 *            the mesesQDS to set
	 */
	void setMesesQDS(Integer mesesQDS);

	/**
	 * @return the indicadorRetiradaAutomatica
	 */
	Boolean getIndicadorRetiradaAutomatica();

	/**
	 * @param indicadorRetiradaAutomatica
	 *            the indicadorRetiradaAutomatica
	 *            to set
	 */
	void setIndicadorRetiradaAutomatica(Boolean indicadorRetiradaAutomatica);

	/**
	 * @return the
	 *         indicadorConfirmacaoAutomaticaQDS
	 */
	Boolean getIndicadorConfirmacaoAutomaticaQDS();

	/**
	 * @param indicadorConfirmacaoAutomaticaQDS
	 *            the
	 *            indicadorConfirmacaoAutomaticaQDS
	 *            to set
	 */
	void setIndicadorConfirmacaoAutomaticaQDS(Boolean indicadorConfirmacaoAutomaticaQDS);

	/**
	 * @return the dataInicioRetiradaQPNR
	 */
	Date getDataInicioRetiradaQPNR();

	/**
	 * @param dataInicioRetiradaQPNR
	 *            the dataInicioRetiradaQPNR to
	 *            set
	 */
	void setDataInicioRetiradaQPNR(Date dataInicioRetiradaQPNR);

	/**
	 * @return the dataFimRetiradaQPNR
	 */
	Date getDataFimRetiradaQPNR();

	/**
	 * @param dataFimRetiradaQPNR
	 *            the dataFimRetiradaQPNR to set
	 */
	void setDataFimRetiradaQPNR(Date dataFimRetiradaQPNR);

	/**
	 * @return the consumoRefSOPAnual
	 */
	EntidadeConteudo getConsumoRefSOPAnual();

	/**
	 * @param consumoRefSOPAnual
	 *            the consumoRefSOPAnual to set
	 */
	void setConsumoRefSOPAnual(EntidadeConteudo consumoRefSOPAnual);

	/**
	 * @return the margemVarSOPAnual
	 */
	Integer getMargemVarSOPAnual();

	/**
	 * @param margemVarSOPAnual
	 *            the margemVarSOPAnual to set
	 */
	void setMargemVarSOPAnual(Integer margemVarSOPAnual);

	/**
	 * @return the percentualQDCContratoQPNR
	 */
	BigDecimal getPercentualQDCContratoQPNR();

	/**
	 * @param percentualQDCContratoQPNR
	 *            the percentualQDCContratoQPNR to
	 *            set
	 */
	void setPercentualQDCContratoQPNR(BigDecimal percentualQDCContratoQPNR);

	/**
	 * @return the percentualQDCFimContratoQPNR
	 */
	BigDecimal getPercentualQDCFimContratoQPNR();

	/**
	 * @param percentualQDCFimContratoQPNR
	 *            the percentualQDCFimContratoQPNR
	 *            to set
	 */
	void setPercentualQDCFimContratoQPNR(BigDecimal percentualQDCFimContratoQPNR);

	/**
	 * @return the
	 *         listaContratoPontoConsumoModalidadeQDC
	 */
	Collection<ContratoPontoConsumoModalidadeQDC> getListaContratoPontoConsumoModalidadeQDC();

	/**
	 * @param listaContratoPontoConsumoModalidadeQDC
	 *            the
	 *            listaContratoPontoConsumoModalidadeQDC
	 *            to set
	 */
	void setListaContratoPontoConsumoModalidadeQDC(Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC);

	/**
	 * @return the
	 *         listaContratoPontoConsumoPenalidade
	 */
	Collection<ContratoPontoConsumoPenalidade> getListaContratoPontoConsumoPenalidade();

	/**
	 * @param listaContratoPontoConsumoPenalidade
	 *            the
	 *            listaContratoPontoConsumoPenalidade
	 *            to set
	 */
	void setListaContratoPontoConsumoPenalidade(Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade);

	/**
	 * @return the percentualVarSuperiorQDC
	 */
	BigDecimal getPercentualVarSuperiorQDC();

	/**
	 * @param percentualVarSuperiorQDC
	 *            the percentualVarSuperiorQDC to
	 *            set
	 */
	void setPercentualVarSuperiorQDC(BigDecimal percentualVarSuperiorQDC);

	/**
	 * @return the anosValidadeRetiradaQPNR
	 */
	Integer getAnosValidadeRetiradaQPNR();

	/**
	 * @param anosValidadeRetiradaQPNR
	 *            the anosValidadeRetiradaQPNR to
	 *            set
	 */
	void setAnosValidadeRetiradaQPNR(Integer anosValidadeRetiradaQPNR);

	/**
	 * Equals negocio.
	 * 
	 * @param obj
	 *            the obj
	 * @return true, if successful
	 */
	boolean equalsNegocio(ContratoPontoConsumoModalidade obj);

	/**
	 * @return Integer - Retorna Limite Programaçao Diária.
	 */
	Integer getHoraLimiteProgramacaoDiaria();

	/**
	 * @param horaLimiteProgramacaoDiaria - Set Hora limite da programação diária.
	 */
	void setHoraLimiteProgramacaoDiaria(Integer horaLimiteProgramacaoDiaria);

	/**
	 * @return Integer - Retorna Hora Limite ProgIntraDiária.
	 */
	Integer getHoraLimiteProgIntradiaria();

	/**
	 * @param horaLimiteProgIntraDiaria - Set hora limite Prog intradiária.
	 */
	void setHoraLimiteProgIntradiaria(Integer horaLimiteProgIntraDiaria);

	/**
	 * @return Integer - Retorna Hora Limite AceitIntradiaria.
	 */
	Integer getHoraLimiteAceitIntradiaria();

	/**
	 * @param horaLimiteAceitIntradiaria - Set hora limite aceitação intradiária.
	 */
	void setHoraLimiteAceitIntradiaria(Integer horaLimiteAceitIntradiaria);

	/**
	 * @return Integer - Retorna Hora Limite Aceitação Diária 
	 */
	Integer getHoraLimiteAceitacaoDiaria();

	/**
	 * @param horaLimiteAceitacaoDiaria - Set Hora limite aceitação diária.
	 */
	void setHoraLimiteAceitacaoDiaria(Integer horaLimiteAceitacaoDiaria);

	/**
	 * @return BigDeciaml - Retorna Percenrual QNR.
	 */
	BigDecimal getPercentualQNR();

	/**
	 * @param percentualQNR - sET Percentual QNR.
	 */
	void setPercentualQNR(BigDecimal percentualQNR);

	/**
	 * @return BigDecimal - Retorna Percentual Mínimo QDC Contrato QPNR.
	 */
	BigDecimal getPercentualMinimoQDCContratoQPNR();

	/**
	 * @param percentualMinimoQDCContratoQPNR - Set Percentual mínimo QDC contrato QPNR.
	 */
	void setPercentualMinimoQDCContratoQPNR(BigDecimal percentualMinimoQDCContratoQPNR);

	/**
	 * @return Retorna Descrição Contrato Modalidade
	 */
	String getDescricaoContratoModalidade();
}
