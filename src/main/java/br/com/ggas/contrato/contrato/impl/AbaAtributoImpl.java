/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe AbaAtributoImpl representa uma AbaAtributoImpl no sistema.
 *
 * @since 17/12/2009
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.contrato.contrato.Aba;
import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelos atributos e métodos relacionados a AbaAtributo. 
 *
 */
public class AbaAtributoImpl extends EntidadeNegocioImpl implements AbaAtributo {

	private static final long serialVersionUID = 4946709839573209214L;

	private String descricao;

	private String nomeColuna;

	private Aba aba;

	private AbaAtributo abaAtributoPai;

	private Boolean dependenciaObrigatoria;

	private Collection<AbaAtributo> filhos;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.AbaAtributo
	 * #getFilhos()
	 */
	@Override
	public Collection<AbaAtributo> getFilhos() {

		return filhos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.AbaAtributo
	 * #setFilhos(java.util.Collection)
	 */
	@Override
	public void setFilhos(Collection<AbaAtributo> filhos) {

		this.filhos = filhos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.AbaAtributo
	 * #getDependenciaObrigatoria()
	 */
	@Override
	public Boolean getDependenciaObrigatoria() {

		return dependenciaObrigatoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.AbaAtributo
	 * #setDependenciaObrigatoria
	 * (java.lang.Boolean)
	 */
	@Override
	public void setDependenciaObrigatoria(Boolean dependenciaObrigatoria) {

		this.dependenciaObrigatoria = dependenciaObrigatoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.AbaAtributo
	 * #getAbaAtributoPai()
	 */
	@Override
	public AbaAtributo getAbaAtributoPai() {

		return abaAtributoPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.AbaAtributo
	 * #setAbaAtributoPai
	 * (br.com.ggas.contrato.contrato.AbaAtributo)
	 */
	@Override
	public void setAbaAtributoPai(AbaAtributo abaAtributoPai) {

		this.abaAtributoPai = abaAtributoPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.AbaAtributo
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.AbaAtributo
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.AbaAtributo
	 * #getNomeColuna()
	 */
	@Override
	public String getNomeColuna() {

		return nomeColuna;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.AbaAtributo
	 * #setNomeColuna(java.lang.String)
	 */
	@Override
	public void setNomeColuna(String nomeColuna) {

		this.nomeColuna = nomeColuna;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.AbaAtributo
	 * #getAba()
	 */
	@Override
	public Aba getAba() {

		return aba;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.AbaAtributo
	 * #setAba(br.com.ggas.contrato.contrato.Aba)
	 */
	@Override
	public void setAba(Aba aba) {

		this.aba = aba;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
