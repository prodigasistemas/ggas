/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.joda.time.DateTime;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoAnexo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoModalidadeQDCImpl;
import br.com.ggas.contrato.contrato.impl.QuantidadeVolumesVO;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Pair;
import br.com.ggas.web.contrato.contrato.CalculoIndenizacaoVO;
import br.com.ggas.web.contrato.contrato.CompromissoTakeOrPayVO;
import br.com.ggas.web.contrato.contrato.ModalidadeConsumoFaturamentoVO;

/**
 * The Interface ControladorContrato.
 */
public interface ControladorContrato extends ControladorNegocio {

	/** The bean id controlador contrato. */
	String BEAN_ID_CONTROLADOR_CONTRATO = "controladorContrato";

	/** The erro negocio situacao contrato invalida exclusao. */
	String ERRO_NEGOCIO_SITUACAO_CONTRATO_INVALIDA_EXCLUSAO = "ERRO_NEGOCIO_SITUACAO_CONTRATO_INVALIDA_EXCLUSAO";

	/** The erro negocio campos obrigatorios contrato. */
	String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_CONTRATO = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_CONTRATO";

	/** The erro negocio item vencimento duplicado. */
	String ERRO_NEGOCIO_ITEM_VENCIMENTO_DUPLICADO = "ERRO_NEGOCIO_ITEM_VENCIMENTO_DUPLICADO";

	/** The erro negocio tipo responsabilidade ja existente. */
	String ERRO_NEGOCIO_TIPO_RESPONSABILIDADE_JA_EXISTENTE = "ERRO_NEGOCIO_TIPO_RESPONSABILIDADE_JA_EXISTENTE";

	/** The erro negocio tipo responsabilidade ja existente. */
	String ERRO_CLIENTE_RESPONSABILIDADE_JA_EXISTENTE = "ERRO_CLIENTE_RESPONSABILIDADE_JA_EXISTENTE";

	/** The erro negocio campos obrigatorios pesquisa. */
	String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA";

	/** The erro negocio anomalia leitura faturamento. */
	String ERRO_NEGOCIO_ANOMALIA_LEITURA_FATURAMENTO = "ERRO_NEGOCIO_ANOMALIA_LEITURA_FATURAMENTO";

	/** The erro negocio multi selecao de ponto consumo. */
	String ERRO_NEGOCIO_MULTI_SELECAO_DE_PONTO_CONSUMO = "ERRO_NEGOCIO_MULTI_SELECAO_DE_PONTO_CONSUMO";

	/** The erro negocio contrato data assinatura. */
	String ERRO_NEGOCIO_CONTRATO_DATA_ASSINATURA = "ERRO_NEGOCIO_CONTRATO_DATA_ASSINATURA";

	/** The erro negocio pontos consumo ja possuem contrato ativo. */
	String ERRO_NEGOCIO_PONTOS_CONSUMO_JA_POSSUEM_CONTRATO_ATIVO = "ERRO_NEGOCIO_PONTOS_CONSUMO_JA_POSSUEM_CONTRATO";

	/** The erro negocio periodicidade ja adicionada. */
	String ERRO_NEGOCIO_PERIODICIDADE_JA_ADICIONADA = "ERRO_NEGOCIO_PERIODICIDADE_JA_ADICIONADA";

	/** The erro negocio periodicidade top contrato ja adicionada. */
	String ERRO_NEGOCIO_PERIODICIDADE_TOP_CONTRATO_JA_ADICIONADA = "ERRO_NEGOCIO_PERIODICIDADE_TOP_CONTRATO_JA_ADICIONADA";

	/** The erro negocio periodicidade top contrato nao informada. */
	String ERRO_NEGOCIO_PERIODICIDADE_TOP_CONTRATO_NAO_INFORMADA = "ERRO_NEGOCIO_PERIODICIDADE_TOP_CONTRATO_NAO_INFORMADA";

	/** The erro negocio percentual top qpnr invalido. */
	String ERRO_NEGOCIO_PERCENTUAL_TOP_QPNR_INVALIDO = "ERRO_NEGOCIO_PERCENTUAL_TOP_QPNR_INVALIDO";

	/** The erro negocio valor maximo menor que minimo. */
	String ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO = "ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO";

	/** The erro negocio data inicio relacao menor que assinatura. */
	String ERRO_NEGOCIO_DATA_INICIO_RELACAO_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_DATA_INICIO_RELACAO_MENOR_QUE_ASSINATURA";

	/** The erro negocio data fim relacao menor que assinatura. */
	String ERRO_NEGOCIO_DATA_FIM_RELACAO_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_DATA_FIM_RELACAO_MENOR_QUE_ASSINATURA";

	/** The erro negocio data fim relacao menor que inicio. */
	String ERRO_NEGOCIO_DATA_FIM_RELACAO_MENOR_QUE_INICIO = "ERRO_NEGOCIO_DATA_FIM_RELACAO_MENOR_QUE_INICIO";

	/** The erro negocio data inicio recuperacao menor que assinatura. */
	String ERRO_NEGOCIO_DATA_INICIO_RECUPERACAO_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_DATA_INICIO_RECUPERACAO_MENOR_QUE_ASSINATURA";

	/** The erro negocio data maxima recuperacao menor que assinatura. */
	String ERRO_NEGOCIO_DATA_MAXIMA_RECUPERACAO_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_DATA_MAXIMA_RECUPERACAO_MENOR_QUE_ASSINATURA";

	/** The erro negocio data maxima recuperacao menor que inicio. */
	String ERRO_NEGOCIO_DATA_MAXIMA_RECUPERACAO_MENOR_QUE_INICIO = "ERRO_NEGOCIO_DATA_MAXIMA_RECUPERACAO_MENOR_QUE_INICIO";

	/** The erro negocio data inicio teste menor que assinatura. */
	String ERRO_NEGOCIO_DATA_INICIO_TESTE_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_DATA_INICIO_TESTE_MENOR_QUE_ASSINATURA";

	/** The erro negocio data fim teste menor que assinatura. */
	String ERRO_NEGOCIO_DATA_FIM_TESTE_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_DATA_FIM_TESTE_MENOR_QUE_ASSINATURA";

	/** The erro negocio data inicio garantia conversao menor que assinatura. */
	String ERRO_NEGOCIO_DATA_INICIO_GARANTIA_CONVERSAO_MENOR_QUE_ASSINATURA = 
					"ERRO_NEGOCIO_DATA_INICIO_GARANTIA_CONVERSAO_MENOR_QUE_ASSINATURA";

	/** The erro negocio data inicio teste maior que vencimento. */
	String ERRO_NEGOCIO_DATA_INICIO_TESTE_MAIOR_QUE_VENCIMENTO = "ERRO_NEGOCIO_DATA_INICIO_TESTE_MAIOR_QUE_VENCIMENTO";

	/** The erro negocio data fim teste maior que vencimento. */
	String ERRO_NEGOCIO_DATA_FIM_TESTE_MAIOR_QUE_VENCIMENTO = "ERRO_NEGOCIO_DATA_FIM_TESTE_MAIOR_QUE_VENCIMENTO";

	/** The erro negocio data inicio garantia conversao maior que vencimento. */
	String ERRO_NEGOCIO_DATA_INICIO_GARANTIA_CONVERSAO_MAIOR_QUE_VENCIMENTO = 
					"ERRO_NEGOCIO_DATA_INICIO_GARANTIA_CONVERSAO_MAIOR_QUE_VENCIMENTO";

	/** The erro negocio data fim teste menor que inicio. */
	String ERRO_NEGOCIO_DATA_FIM_TESTE_MENOR_QUE_INICIO = "ERRO_NEGOCIO_DATA_FIM_TESTE_MENOR_QUE_INICIO";

	/** The erro negocio data prazo teste menor que inicio teste. */
	String ERRO_NEGOCIO_DATA_PRAZO_TESTE_MENOR_QUE_INICIO_TESTE = "ERRO_NEGOCIO_DATA_PRAZO_TESTE_MENOR_QUE_INICIO_TESTE";

	/** The erro negocio inicio garantia financiamento menor que assinatura. */
	String ERRO_NEGOCIO_INICIO_GARANTIA_FINANCIAMENTO_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_INICIO_GARANTIA_FINANCIAMENTO_MENOR_QUE_ASSINATURA";

	/** The erro negocio inicio garantia financiamento maior que vencimento. */
	String ERRO_NEGOCIO_INICIO_GARANTIA_FINANCIAMENTO_MAIOR_QUE_VENCIMENTO = "ERRO_NEGOCIO_INICIO_GARANTIA_FINANCIAMENTO_MAIOR_QUE_VENCIMENTO";

	/** The erro negocio fim garantia financiamento menor que assinatura. */
	String ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MENOR_QUE_ASSINATURA = "ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MENOR_QUE_ASSINATURA";

	/** The erro negocio fim garantia financiamento maior que vencimento. */
	String ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MAIOR_QUE_VENCIMENTO = "ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MAIOR_QUE_VENCIMENTO";

	/** The erro negocio fim garantia financiamento menor que inicio. */
	String ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MENOR_QUE_INICIO = "ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MENOR_QUE_INICIO";

	/** The erro negocio data vencimento menor assinatura. */
	String ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA = "ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA";

	/** The erro negocio data vencimento menor assinatura contrato complementar. */
	String ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA_CONTRATO_COMPLEMENTAR = 
					"ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA_CONTRATO_COMPLEMENTAR";

	/** The erro negocio periodo invalido contrato complementar. */
	String ERRO_NEGOCIO_PERIODO_INVALIDO_CONTRATO_COMPLEMENTAR = "ERRO_NEGOCIO_PERIODO_INVALIDO_CONTRATO_COMPLEMENTAR";

	/** The erro negocio base apuracao contrato complementar. */
	String ERRO_NEGOCIO_BASE_APURACAO_CONTRATO_COMPLEMENTAR = "ERRO_NEGOCIO_BASE_APURACAO_CONTRATO_COMPLEMENTAR";

	/** The erro negocio data aditivo menor assinatura. */
	String ERRO_NEGOCIO_DATA_ADITIVO_MENOR_ASSINATURA = "ERRO_NEGOCIO_DATA_ADITIVO_MENOR_ASSINATURA";

	/** The erro negocio data aditivo invalido. */
	String ERRO_NEGOCIO_DATA_ADITIVO_INVALIDO = "ERRO_NEGOCIO_DATA_ADITIVO_INVALIDO";

	/** The erro negocio data inicial extrato. */
	String ERRO_NEGOCIO_DATA_INICIAL_EXTRATO = "ERRO_NEGOCIO_DATA_INICIAL_EXTRATO";

	/** The erro negocio data final extrato. */
	String ERRO_NEGOCIO_DATA_FINAL_EXTRATO = "ERRO_NEGOCIO_DATA_FINAL_EXTRATO";

	/** The erro negocio data inicial maior extrato. */
	String ERRO_NEGOCIO_DATA_INICIAL_MAIOR_EXTRATO = "ERRO_NEGOCIO_DATA_INICIAL_MAIOR_EXTRATO";

	/** The erro negocio situacao contrato invalida aditamento. */
	String ERRO_NEGOCIO_SITUACAO_CONTRATO_INVALIDA_ADITAMENTO = "ERRO_NEGOCIO_SITUACAO_CONTRATO_INVALIDA_ADITAMENTO";

	/** The erro negocio hora dia. */
	String ERRO_NEGOCIO_HORA_DIA = "ERRO_NEGOCIO_HORA_DIA";

	/** The erro negocio renovacao automatica. */
	String ERRO_NEGOCIO_RENOVACAO_AUTOMATICA = "ERRO_NEGOCIO_RENOVACAO_AUTOMATICA";

	/** The erro negocio recuperacao pcs. */
	String ERRO_NEGOCIO_RECUPERACAO_PCS = "ERRO_NEGOCIO_RECUPERACAO_PCS";

	/** The erro negocio soma qdc modalidade maior qdc contrato. */
	String ERRO_NEGOCIO_SOMA_QDC_MODALIDADE_MAIOR_QDC_CONTRATO = "ERRO_NEGOCIO_SOMA_QDC_MODALIDADE_MAIOR_QDC_CONTRATO";

	/** The erro negocio ponto consumo nao informado. */
	String ERRO_NEGOCIO_PONTO_CONSUMO_NAO_INFORMADO = "ERRO_NEGOCIO_PONTO_CONSUMO_NAO_INFORMADO";

	/** The erro negocio pressao fornecimento nao informada. */
	String ERRO_NEGOCIO_PRESSAO_FORNECIMENTO_NAO_INFORMADA = "ERRO_NEGOCIO_PRESSAO_FORNECIMENTO_NAO_INFORMADA";

	/** The erro negocio modalidade ja existente. */
	String ERRO_NEGOCIO_MODALIDADE_JA_EXISTENTE = "ERRO_NEGOCIO_MODALIDADE_JA_EXISTENTE";

	/** The erro negocio todos pontos cunsumo dados informados. */
	String ERRO_NEGOCIO_TODOS_PONTOS_CUNSUMO_DADOS_INFORMADOS = "ERRO_NEGOCIO_TODOS_PONTOS_CUNSUMO_DADOS_INFORMADOS";

	/** The erro negocio data qdc menor data assinatura. */
	String ERRO_NEGOCIO_DATA_QDC_MENOR_DATA_ASSINATURA = "ERRO_NEGOCIO_DATA_QDC_MENOR_DATA_ASSINATURA";

	/** The erro negocio data qdc maior data vencimento. */
	String ERRO_NEGOCIO_DATA_QDC_MAIOR_DATA_VENCIMENTO = "ERRO_NEGOCIO_DATA_QDC_MAIOR_DATA_VENCIMENTO";

	/** The erro negocio data inicio maior data fim qtda volumes. */
	String ERRO_NEGOCIO_DATA_INICIO_MAIOR_DATA_FIM_QTDA_VOLUMES = "ERRO_NEGOCIO_DATA_INICIO_MAIOR_DATA_FIM_QTDA_VOLUMES";

	/** The erro negocio tipo consulta qtda volumes. */
	String ERRO_NEGOCIO_TIPO_CONSULTA_QTDA_VOLUMES = "ERRO_NEGOCIO_TIPO_CONSULTA_QTDA_VOLUMES";

	/** The erro negocio data inicio qtda volumes. */
	String ERRO_NEGOCIO_DATA_INICIO_QTDA_VOLUMES = "ERRO_NEGOCIO_DATA_INICIO_QTDA_VOLUMES";

	/** The erro negocio data fim qtda volumes. */
	String ERRO_NEGOCIO_DATA_FIM_QTDA_VOLUMES = "ERRO_NEGOCIO_DATA_FIM_QTDA_VOLUMES";

	/** The erro contratos situacao. */
	String ERRO_CONTRATOS_SITUACAO = "ERRO_CONTRATOS_SITUACAO";

	/** The erro negocio contrato sem data vencimento. */
	String ERRO_NEGOCIO_CONTRATO_SEM_DATA_VENCIMENTO = "ERRO_NEGOCIO_CONTRATO_SEM_DATA_VENCIMENTO";

	/** The erro negocio dados ausentes calculo recisao. */
	String ERRO_NEGOCIO_DADOS_AUSENTES_CALCULO_RECISAO = "ERRO_NEGOCIO_DADOS_AUSENTES_CALCULO_RECISAO";

	/** The erro negocio multa recisoria ausente. */
	String ERRO_NEGOCIO_MULTA_RECISORIA_AUSENTE = "ERRO_NEGOCIO_MULTA_RECISORIA_AUSENTE";

	/** The erro negocio data investimento ausente. */
	String ERRO_NEGOCIO_DATA_INVESTIMENTO_AUSENTE = "ERRO_NEGOCIO_DATA_INVESTIMENTO_AUSENTE";

	/** The erro negocio valor investimento ausente. */
	String ERRO_NEGOCIO_VALOR_INVESTIMENTO_AUSENTE = "ERRO_NEGOCIO_VALOR_INVESTIMENTO_AUSENTE";

	/** The erro negocio ponto consumo contrato sem item fatura. */
	String ERRO_NEGOCIO_PONTO_CONSUMO_CONTRATO_SEM_ITEM_FATURA = "ERRO_NEGOCIO_PONTO_CONSUMO_CONTRATO_SEM_ITEM_FATURA";

	/** The erro negocio indenizacao qdc contrato vencido. */
	String ERRO_NEGOCIO_INDENIZACAO_QDC_CONTRATO_VENCIDO = "ERRO_NEGOCIO_INDENIZACAO_QDC_CONTRATO_VENCIDO";

	/** The erro negocio calculo recisao indefinido. */
	String ERRO_NEGOCIO_CALCULO_RECISAO_INDEFINIDO = "ERRO_NEGOCIO_CALCULO_RECISAO_INDEFINIDO";

	/** The erro negocio contrato ponto consumo sem qdc. */
	String ERRO_NEGOCIO_CONTRATO_PONTO_CONSUMO_SEM_QDC = "ERRO_NEGOCIO_CONTRATO_PONTO_CONSUMO_SEM_QDC";

	/** The erro negocio indice financeiro inexistente. */
	String ERRO_NEGOCIO_INDICE_FINANCEIRO_INEXISTENTE = "ERRO_NEGOCIO_INDICE_FINANCEIRO_INEXISTENTE";

	/** The erro negocio data recisao maior que atual. */
	String ERRO_NEGOCIO_DATA_RECISAO_MAIOR_QUE_ATUAL = "ERRO_NEGOCIO_DATA_RECISAO_MAIOR_QUE_ATUAL";

	/** The erro negocio data recisao menor que investimento. */
	String ERRO_NEGOCIO_DATA_RECISAO_MENOR_QUE_INVESTIMENTO = "ERRO_NEGOCIO_DATA_RECISAO_MENOR_QUE_INVESTIMENTO";

	/** The erro negocio data vencimento menor que atual. */
	String ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_QUE_ATUAL = "ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_QUE_ATUAL";

	/** The erro negocio data recisao menor que data investimento. */
	String ERRO_NEGOCIO_DATA_RECISAO_MENOR_QUE_DATA_INVESTIMENTO = "ERRO_NEGOCIO_DATA_RECISAO_MENOR_QUE_DATA_INVESTIMENTO";

	/** The erro negocio contrato fatura encerramento gerada. */
	String ERRO_NEGOCIO_CONTRATO_FATURA_ENCERRAMENTO_GERADA = "ERRO_NEGOCIO_CONTRATO_FATURA_ENCERRAMENTO_GERADA";

	/** The erro negocio encerrar reincindir contrato. */
	String ERRO_NEGOCIO_ENCERRAR_REINCINDIR_CONTRATO = "ERRO_NEGOCIO_ENCERRAR_REINCINDIR_CONTRATO";

	/** The erro negocio operacao invalida contrato inativo. */
	String ERRO_NEGOCIO_OPERACAO_INVALIDA_CONTRATO_INATIVO = "ERRO_NEGOCIO_OPERACAO_INVALIDA_CONTRATO_INATIVO";

	/** The erro negocio operacao invalida encerrar contrato inativo. */
	String ERRO_NEGOCIO_OPERACAO_INVALIDA_ENCERRAR_CONTRATO_INATIVO = "ERRO_NEGOCIO_OPERACAO_INVALIDA_ENCERRAR_CONTRATO_INATIVO";

	/** The erro negocio sem contratos a exibir. */
	String ERRO_NEGOCIO_SEM_CONTRATOS_A_EXIBIR = "ERRO_NEGOCIO_SEM_CONTRATOS_A_EXIBIR";

	/** The erro intervalo data vigencia. */
	String ERRO_INTERVALO_DATA_VIGENCIA = "ERRO_INTERVALO_DATA_VIGENCIA";

	/** The erro intervalo data vigencia assinatura vencimento contrato. */
	String ERRO_INTERVALO_DATA_VIGENCIA_ASSINATURA_VENCIMENTO_CONTRATO = "ERRO_INTERVALO_DATA_VIGENCIA_ASSINATURA_VENCIMENTO_CONTRATO";

	/** The erro intervalo data vigencia obrigatorio. */
	String ERRO_INTERVALO_DATA_VIGENCIA_OBRIGATORIO = "ERRO_INTERVALO_DATA_VIGENCIA_OBRIGATORIO";
	
	/** The erro negocio ponto consumo nao informado. */
	String ERRO_CONTRATO_SEM_DATA_ASSINATURA_ARRECADADOR_CONVENIO = "ERRO_CONTRATO_SEM_DATA_ASSINATURA_ARRECADADOR_CONVENIO";

	/**
	 * Criar contrato ponto consumo.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContratoPontoConsumo();

	/**
	 * Criar contrato qdc.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContratoQDC();

	/**
	 * Criar contrato cliente.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContratoCliente();

	/**
	 * Criar contrato ponto consumo item vencimento.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContratoPontoConsumoItemVencimento();

	/**
	 * Criar contrato ponto consumo pcs amostragem.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContratoPontoConsumoPCSAmostragem();

	/**
	 * Criar contrato ponto consumo pcs intervalo.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarContratoPontoConsumoPCSIntervalo();

	/**
	 * Obter contrato ativo ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the contrato ponto consumo
	 */
	ContratoPontoConsumo obterContratoAtivoPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Método responsável por consultar Contrato
	 * para o requisito de manter parada
	 * programada.
	 * 
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de Contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Contrato> consultarContratoParadaProgramada(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Gets the classe entidade contrato ponto consumo.
	 *
	 * @return the classe entidade contrato ponto consumo
	 */
	Class<?> getClasseEntidadeContratoPontoConsumo();

	/**
	 * Gets the classe entidade situacao contrato.
	 *
	 * @return the classe entidade situacao contrato
	 */
	Class<?> getClasseEntidadeSituacaoContrato();

	/**
	 * Gets the classe entidade modelo contrato.
	 *
	 * @return the classe entidade modelo contrato
	 */
	Class<?> getClasseEntidadeModeloContrato();

	/**
	 * Método responsável por consultar
	 * ContratoPontoConsumo para o requisito de
	 * manter programação de consumo.
	 * 
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de
	 *         ContratoPontoConsumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumoProgramacaoConsumo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 * @returnUma coleção de ContratoPontoConsumo.
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumo(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo pelos pontos de
	 * consumo.
	 * 
	 * @param chavesPontoConsumo
	 *            Chaves primárias dos pontos de
	 *            consumo.
	 * @return Uma coleção de
	 *         ContratoPontoConsumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumo(Long[] chavesPontoConsumo) throws NegocioException;


	/**
	 * Método responsável por listar as modalidade
	 * do contrato de acordo com o
	 * Contrato.
	 * 
	 * @param chaveContrato
	 *            Chave primária do contrato.
	 * @return Uma coleção de modalidade de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorContrato(Long chaveContrato)
					throws NegocioException;
	
	/**
	 * Método responsável por listar as modalidade
	 * do contrato de acordo com o
	 * ContratoPontoConsumo.
	 * 
	 * @param chaveContratoPonto
	 *            Chave primária do
	 *            ContratoPontoConsumo.
	 * @return Uma coleção de modalidade de
	 *         contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorContratoPontoConsumo(Long chaveContratoPonto)
					throws NegocioException;

	/**
	 * Método responsável por obter um Contrato
	 * Ponto Consumo Modalidade.
	 *
	 * @param chavePrimaria Chave primária do Contrato Ponto
	 *            Consumo Modalidade
	 * @return ContratoPontoConsumoModalidade
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	ContratoPontoConsumoModalidade obterContratoPontoConsumoModalidade(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * penalidade demanda sob.
	 *
	 * @return Coleção de penalidade demanda
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	Collection<PenalidadeDemanda> obterListaPenalidadeDemandaSob() throws NegocioException;

	/**
	 * Método responsável por obter um Contrato
	 * Ponto Consumo.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @return ContratoPontoConsumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumo(Long chavePontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * penalidade demanda sobre.
	 *
	 * @return Coleção de penalidade demanda
	 * @throws NegocioException Caso ocorra algum erro na
	 *             execução do método.
	 */
	Collection<PenalidadeDemanda> obterListaPenalidadeDemandaSobre() throws NegocioException;

	/**
	 * Método responsável por consultar
	 * modalidades de contrato cadastradas no
	 * sistema.
	 * 
	 * @return Uma coleção de modalidades de
	 *         contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoModalidade> listarContratoModalidade() throws NegocioException;

	/**
	 * Método responsável por obter uma modalidade.
	 *
	 * @param chavePrimaria A chave primaria
	 * @return Uma modalidade
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ContratoModalidade obterContratoModalidade(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar se houve
	 * seleção do modelo de contrato.
	 * 
	 * @param chaveModeloContrato
	 *            Chave primária do modelo de
	 *            contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarModeloContratoSelecionado(Long chaveModeloContrato) throws NegocioException;

	/**
	 * Método responsável por consultar contratos
	 * cadastrados no sistema.
	 * 
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Uma coleção de contratos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Contrato> consultarContratos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar contratos
	 * pela chave da proposta.
	 * 
	 * @param chavesProposta
	 *            As chaves primárias da proposta.
	 * @return Uma coleção de contratos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Contrato> consultarContratosPorProposta(Long[] chavesProposta) throws NegocioException;

	/**
	 * Método responsável por verificar se existe
	 * dependência entre contrato e modelo.
	 * 
	 * @param chavePrimariaModelo
	 *            Chave Primária do Modelo do
	 *            Contrato.
	 * @return True caso exista dependência. False
	 *         caso contrário.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Boolean verificarDependenciaContratoModelo(long chavePrimariaModelo) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * contrato.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos de
	 *            contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverContratos(Collection<Long> chavesPrimarias) throws NegocioException;

	/**
	 * Adaptador do método de remover do
	 * controlador negócio para receber uma
	 * coleção ao invés de um array.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	void remover(Collection<Long> chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por obter a situação do
	 * contrato.
	 * 
	 * @param chavePrimaria
	 *            Chave da situação do contrato.
	 * @return Situação do contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	SituacaoContrato obterSituacaoContrato(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * informado pelo usuário.
	 *
	 * @param dados Os dados informados pelo usuário
	 * @param modeloContrato O modelo do contrato
	 * @param aba A aba que será validada
	 * @param validarDadosAditar the validar dados aditar
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarCamposObrigatoriosContrato(Map<String, Object> dados, ModeloContrato modeloContrato, Aba aba, Boolean validarDadosAditar)
					throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * informado pelo usuário.
	 * 
	 * @param dados
	 *            Os dados informados pelo usuário
	 * @param modeloContrato
	 *            O modelo do contrato
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarCamposObrigatoriosContrato(Map<String, Object> dados, ModeloContrato modeloContrato) throws NegocioException;

	/**
	 * Método responsável por validar somente os
	 * dados dos campos informado pelo usuário.
	 * 
	 * @param dados
	 *            Os dados informados pelo usuário
	 * @param campos
	 *            Os campos informados pelo
	 *            usuário
	 * @param modeloContrato
	 *            O modelo do contrato
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarCamposObrigatoriosContrato(Map<String, Object> dados, List<String> campos, ModeloContrato modeloContrato)
					throws NegocioException;

	/**
	 * Método reponsável se houve uma seleção de
	 * pontos de consumo.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primarias selecionadas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarSelecaoImovelPontoConsumo(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método reponsável por obter a aba do
	 * contrato.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria da aba
	 * @return Uma aba
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Aba obterAbaContrato(long chavePrimaria) throws NegocioException;

	/**
	 * Método reponsável por obter a Penalidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria da aba
	 * @return Uma Penalidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Penalidade obterPenalidade(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma aba de
	 * contrato pelo código.
	 * 
	 * @param codigo
	 *            Código equivalente a descrição
	 *            abreviada.
	 * @return Uma aba de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Aba obterAbaContratoPorCodigo(String codigo) throws NegocioException;

	/**
	 * Método reponsável por validar a adição de
	 * um item de faturamento.
	 * 
	 * @param modeloContrato
	 *            O modelo do contrato
	 * @param dadosItemFaturamento
	 *            Os dados do item de faturamento
	 * @param faseReferencia
	 *            A fase referência
	 * @param opcaoFaseReferencia
	 *            A opção da fase referência
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarAdicaoContratoPontoConsumoItemFaturamento(ModeloContrato modeloContrato, Map<String, Object> dadosItemFaturamento,
					EntidadeConteudo faseReferencia, EntidadeConteudo opcaoFaseReferencia) throws NegocioException;

	/**
	 * Método responsável por obter penalidade
	 * demanda.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da penalidade.
	 * @return Uma penalidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PenalidadeDemanda obterPenalidadeDemanda(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar se já existe
	 * um cliente adicionado com a mesma
	 * responsabilidade.
	 * 
	 * @param listaContratoCliente
	 *            Lista de ContratoCliente já
	 *            adicionados.
	 * @param contratoCliente
	 *            ContratoCliente a ser comparado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRelacaoResponsabilidade(Collection<ContratoCliente> listaContratoCliente, ContratoCliente contratoCliente)
					throws NegocioException;

	/**
	 * Método responsável por validar o dados de
	 * ContratoCliente na inclusão de contrato.
	 * 
	 * @param contratoCliente
	 *            ContratoCliente a ser validado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDadosResponsabilidade(ContratoCliente contratoCliente) throws NegocioException;

	/**
	 * Método responsável por criar um Contrato
	 * Ponto Consumo Modalidade.
	 * 
	 * @return ContratoPontoConsumoModalidade
	 */
	EntidadeNegocio criarContratoPontoConsumoModalidade();

	/**
	 * Método responsável por criar um criar
	 * Contrato Ponto Consumo Modalidade QDC.
	 * 
	 * @return ContratoPontoConsumoModalidadeQDC
	 */
	EntidadeNegocio criarContratoPontoConsumoModalidadeQDC();

	/**
	 * Método responsável por criar um criar
	 * Contrato Ponto Consumo Penalidade.
	 * 
	 * @return ContratoPontoConsumoPenalidade
	 */
	EntidadeNegocio criarContratoPontoConsumoPenalidade();

	/**
	 * Método responsável por listar os contratos
	 * de ponto de consumo do contrato informado.
	 * 
	 * @param chavePrimariaContrato
	 *            A chave primária do contrato.
	 * @return Lista de contrato de ponto de
	 *         consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumo> listarContratoPontoConsumo(Long chavePrimariaContrato) throws NegocioException;

	/**
	 * Método responsável por listar os locais
	 * amostragem pela chave primaria do contrato
	 * ponto consumo.
	 * 
	 * @param contratoPontoConsumo
	 *            O contrato Ponto consumo.
	 * @return Lista de locais amostragem.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> consultarLocalAmostragemPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por listar os intervalos
	 * pcs pela chave primaria do contrato ponto
	 * consumo.
	 * 
	 * @param contratoPontoConsumo
	 *            O contrato Ponto consumo.
	 * @return Lista de intervalo pcs.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IntervaloPCS> consultarIntervaloPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por listar os itens de
	 * vencimento de um contrato de ponto de
	 * consumo informado.
	 * 
	 * @param chavePrimariaContratoPontoConsumo
	 *            A chave primária do contrato de
	 *            ponto de consumo.
	 * @return Lista de item de vencimento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumoItemFaturamento> listarItensVencimentoPorContratoPontoConsumo(Long chavePrimariaContratoPontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por aditar um contrato
	 * no sistema.
	 *
	 * @param contratoAditado O contrato aditado a ser
	 *            inserido.
	 * @param contratoAlterado O contrato que foi alterado.
	 * @return chavePrimeria do contrato inserido
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	Long aditarContrato(Contrato contratoAditado, Contrato contratoAlterado) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por alterar um contrato
	 * no sistema.
	 *
	 * @param contrato O contrato alterado a ser
	 *            inserido.
	 * @param contratoAlterado O contrato que foi alterado.
	 * @return chavePrimeria do contrato inserido
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	Long atualizarContrato(Contrato contrato, Contrato contratoAlterado) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por listar as
	 * Modalidades de um contrato de ponto de
	 * consumo informado.
	 * 
	 * @param chavePrimariaContratoPontoConsumo
	 *            A chave primária do contrato de
	 *            ponto de consumo.
	 * @return Lista de Consumo Modalidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	Collection<ContratoPontoConsumoModalidade> listarModalidadesPorContratoPontoConsumo(Long chavePrimariaContratoPontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por listar os
	 * ContratoCliente de um ponto de consumo
	 * informado.
	 *
	 * @param idPontoConsumo Chave primária do ponto de
	 *            consumo.
	 * @param idContrato the id contrato
	 * @return Uma coleção de ContratoCliente.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoCliente> listarContratoClientePorPontoConsumo(Long idPontoConsumo, Long idContrato) throws NegocioException;

	/**
	 * Método responsável por listar os Consumos
	 * Penalidades de um contrato de ponto de
	 * consumo informado.
	 *
	 * @param chavePrimariaContratoPontoConsumoModalidade the chave primaria contrato ponto consumo modalidade
	 * @return Uma coleção de Contrato Ponto
	 *         Consumo Penalidade
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	Collection<ContratoPontoConsumoPenalidade> listarConsumoPenalidadePorContratoPontoConsumoModalidade(
					Long chavePrimariaContratoPontoConsumoModalidade) throws NegocioException;

	/**
	 * Método responsável por obter o tamanho da
	 * Redução para Recuperação do PCS.
	 * 
	 * @param contratoPontoConsumo
	 *            O contrato ponto de consumo.,
	 * @return O tamanho da redução
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	Integer obterTamanhoIntervaloPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por validar a data de
	 * assinatura do contrato.
	 *
	 * @param data A data de assinatura do
	 *            contrato.
	 * @throws GGASException the GGAS exception
	 */
	void validarDataAssinaturaContrato(String data) throws GGASException;
	
	/**
	 * Método responsável por validar a data de assinatura e arrecadador convênio relativo a situação de contrato.
	 *
	 * @param dataAssinatura A data de assinatura do contrato.
	 * @param arrecadadorConvenio Arrecadador convênio do contrato.
	 * @param situacaoContrato Situacao do contrato.
	 * @throws GGASException the GGAS exception
	 */
	public void validarSituacaoDataAssinaturaArrecadadador(String dataAssinatura, Long arrecadadorConvenio, Long situacaoContrato)
			throws GGASException;
	
	/**
	 * Método responsável por validar as datas da
	 * aba de responsabilidade.
	 * 
	 * @param contratoCliente
	 *            ContratoCliente que contém as
	 *            datas a serem validadas.
	 * @param contrato
	 *            Contrato a ser adicionado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarDatasAbaResponsabilidade(ContratoCliente contratoCliente, Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por validar as datas da
	 * aba de modalidade.
	 *
	 * @param dataInicioRecuperacao Data de início da recuperação.
	 * @param dataFimRecuperacao Data máxima para recuperação.
	 * @param contrato the contrato
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarDatasAbaModalidade(String dataInicioRecuperacao, String dataFimRecuperacao, Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por fazer as validações
	 * das datas da aba principal do contrato.
	 * 
	 * @param abaPrincipal
	 *            Aba principal do contrato.
	 * @param contrato
	 *            Contrato a ser validado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarDatasAbaPrincipal(Map<String, Object> abaPrincipal, Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por fazer as validações
	 * das datas do contrato.
	 * 
	 * @param contrato
	 *            O contrato a ser validade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDatasContrato(Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por verificar se os
	 * pontos de consumo selecionados para a
	 * inclusão do contrato já estão
	 * em algum outro contrato com situação ativa.
	 *
	 * @param chaveContrato the chave contrato
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarPontosSelecionados(Long chaveContrato, Long[] chavesPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por validar a adição de
	 * uma nova penalidade de contrato de ponto de
	 * consumo em uma coleção de
	 * penalidades de um mesmo contrato de ponto
	 * de consumo.
	 * 
	 * @param contratoPontoConsumoPenalidade
	 *            Uma penalidade de contrato de
	 *            ponto de consumo.
	 * @param listaContratoPontoConsumoPenalidade
	 *            Uma coleção de penalidades de
	 *            contrato de ponto de consumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarAdicaoPenalidade(ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade,
					Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) throws NegocioException;

	/**
	 * Método responsável por validar os
	 * percentuais de compromisso de Take Or Pay e
	 * máximo em relação ao QDC
	 * informados.
	 * 
	 * @param periodicidadePenalidade
	 *            A periodicidade da
	 *            penalidade(compromisso).
	 * @param percentualMargemVariacao
	 *            O percentual da margem de
	 *            variação Take Or Pay.
	 * @param percentualMaximoQDC
	 *            O percentual máximo em relação
	 *            ao QDC.
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarCompromissoTakeOrPayQPNR(EntidadeConteudo periodicidadePenalidade, BigDecimal percentualMargemVariacao,
					BigDecimal percentualMaximoQDC) throws NegocioException;

	/**
	 * Método que valida o percentual máximo em
	 * relação ao QDC.
	 *
	 * @param percentualMinimoQDC the percentual minimo qdc
	 * @param percentualMaximoQDC the percentual maximo qdc
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarPercentualMaximoQDC(BigDecimal percentualMinimoQDC, BigDecimal percentualMaximoQDC) throws NegocioException;

	/**
	 * Método que valida o percentual tarifa
	 * delivery or pay.
	 * 
	 * @param percentualTarifaTOP
	 *            O percentual da tarifa delivery
	 *            or pay.
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarPercentualTarifaDOP(BigDecimal percentualTarifaTOP) throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * para os campos que possue um valor mínimo e
	 * um valor máximo.
	 *
	 * @param dados Os dados informados pelo usuário
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	void validarCamposMinimoMaximo(Map<String, Object> dados) throws NegocioException, FormatoInvalidoException;

	/**
	 * Método responsável por obter o endereço
	 * formatado da aba de faturamento.
	 * 
	 * @param cepParam
	 *            Cep do endereço
	 * @param numero
	 *            O número
	 * @return Endereço formatado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	String obterEnderecoFaturamentoFormatado(String cepParam, String numero) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria de um imovel.
	 *
	 * @param chave A chave primaria de uma imovel
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorImovel(Long chave) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria de um imovel.
	 *
	 * @param chave A chave primaria de uma imovel
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumo> consultarContratoClientePontoConsumoPorImovel(Long chave) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * ContratoPontoConsumo a partir da chave
	 * primaria do cliente.
	 * 
	 * @param chave
	 *            A chave primaria de um cliente.
	 * @return ContratoPontoConsumo Uma colecao de
	 *         contrato ponto de consumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorCliente(Long chave) throws NegocioException;

	/**
	 * Método responsável por verificar se o
	 * tamanho de redução para recuperção de PCS é
	 * obrigatório para o contrato a
	 * partir dos intervalos de PCS informados.
	 * 
	 * @param arrayIdsIntervalosPCS
	 *            Array de identificadores dos
	 *            intervalos de PCS.
	 * @return true caso obrigatório
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean isObrigatorioTamanhoReducaoRecuperacaoPCS(Long[] arrayIdsIntervalosPCS) throws NegocioException;

	/**
	 * Método responsável por validar o aditamento
	 * de um contrato.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do de contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarAditamentoContrato(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar a hora
	 * referente ao campo da aba de modalidades.
	 * 
	 * @param data
	 *            A data atual
	 * @param hora
	 *            A hora informada pelo usuário.
	 * @throws NegocioException
	 *             Caso a hora seja inválida.
	 */
	void validarHoraDia(DateTime data, Integer hora) throws NegocioException;

	/**
	 * Método responsável por validar se a soma
	 * dos QDCs das modalidades já adicionadas
	 * para uma determinada data é
	 * maior que o QDC do contrato no aditamento.
	 * 
	 * @param listaModalidadeQDC
	 *            Lista de QDCs dos pontos de
	 *            consumo.
	 * @param listaContratoQDC
	 *            Lista de QDCs do contrato.
	 * @param dataVencimento
	 *            Data de vencimento do contrato.
	 * @return A primeira data onde a soma é maior
	 *         que a QDC do contrato.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	String validarAlteracaoQDCContrato(Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC,
					Collection<ContratoQDC> listaContratoQDC, Date dataVencimento) throws NegocioException;

	/**
	 * Método responsável por validar se a
	 * modalidade já foi adicionada.
	 * 
	 * @param listaModalidades
	 *            Lista de modalidades já
	 *            adicionadas.
	 * @param modalidadeConsumoFaturamentoVO
	 *            Modalidade a ser adicionada.
	 * @throws NegocioException
	 *             Caso a modalidade a ser
	 *             adicionada já tenha sido
	 *             adicionada anteriormente.
	 */
	void validarModalidadeExistente(Collection<ModalidadeConsumoFaturamentoVO> listaModalidades,
					ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO) throws NegocioException;

	/**
	 * Método responsável por obter os
	 * ContratoPontoConsumo pelo cliente.
	 * 
	 * @param idCliente
	 *            Chave primária do cliente.
	 * @return Uma coleção de
	 *         ContratoPontoConsumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumo> obterContratoPontoConsumoPorCliente(Long idCliente) throws NegocioException;

	/**
	 * Método responsável por obter o contrato
	 * ponto de consumo do item da fatura pelos
	 * parametros informado.
	 *
	 * @param chaveContratoPontoConsumo the chave contrato ponto consumo
	 * @param chaveItemFatura the chave item fatura
	 * @return Um
	 *         ContratoPontoConsumoItemFaturamento
	 *         ;
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ContratoPontoConsumoItemFaturamento consultarContratoPontoConsumoItemFaturamento(Long chaveContratoPontoConsumo, Long chaveItemFatura)
					throws NegocioException;

	/**
	 * Método responsável por obter um
	 * contratoPontoConsumo com contrato ativo
	 * pelo ponto de consumo.
	 * 
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @return Um ContratoPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ContratoPontoConsumo obterContratoPontoConsumoParaEnderecoEntrega(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método reponsável por validar a data de
	 * referência cambial e dia da cotação.
	 * 
	 * @param verificarDataReferenciaDiaCotacao
	 *            verifica se a tarifa associada
	 *            ao dolar foi escolhida
	 * @param objetoDataReferenciaCambial
	 *            objeto data de referencia
	 *            cambial
	 * @param objetoDiaCotacao
	 *            objeto dia da cotação
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDataReferenciaDiaCotacao(boolean verificarDataReferenciaDiaCotacao, EntidadeConteudo objetoDataReferenciaCambial,
					EntidadeConteudo objetoDiaCotacao) throws NegocioException;

	/**
	 * *
	 * Método responsável por obter um
	 * ContratoPontoConsumoItemFaturamento.
	 *
	 * @param idPontoConsumo A chave primária de um ponto de
	 *            consumo
	 * @param idItemFatura A chave primária de um
	 *            itemFatura
	 * @return Um
	 *         ContratoPontoConsumoItemFaturamento
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public ContratoPontoConsumoItemFaturamento obterTarifaPontoConsumoItemFaturamento(Long idPontoConsumo, Long idItemFatura)
					throws NegocioException;

	/**
	 * Método responsável por os contratos do
	 * ponto de consumo do item
	 * faturamento pela tarifa.
	 *
	 * @param chavesTarifa the chaves tarifa
	 * @return lista de contrato ponto de consumo
	 *         item faturamento.
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumoItemFaturamento> listarContratoItemFaturamentoPorTarifas(Long[] chavesTarifa) throws NegocioException;

	/**
	 * Método responsável por retornar o contrato
	 * de compra do ponto de consumo.
	 * 
	 * @param pontoConsumo
	 *            Ponto de consumo.
	 * @return Contrato de Compra.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterContratoCompraPorPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter um
	 * ContratoPontoConsumo para um PontoConsumo e
	 * um Contrato específicos.
	 *
	 * @param chaveContrato chavePrimaria do Contrato
	 * @param chavePontoConsumo chavePrimaria do Ponto de
	 *            Consumo
	 * @return ContratoPontoConsumo consultado
	 */
	ContratoPontoConsumo obterContratoPontoConsumoPorContratoPontoConsumo(Long chaveContrato, Long chavePontoConsumo);

	/**
	 * Obter tipo medicao contrato ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param contrato the contrato
	 * @return the tipo medicao
	 */
	TipoMedicao obterTipoMedicaoContratoPontoConsumo(PontoConsumo pontoConsumo, Contrato contrato);

	/**
	 * Método responsável por verificar se o
	 * contrato para o ponto de consumo está em
	 * período de teste.
	 * 
	 * @param contrato
	 *            O contrato
	 * @param pontoConsumo
	 *            O ponto de consumo
	 * @param dataInicial
	 *            A data inicial do período
	 * @param dataFinal
	 *            A data final do período
	 * @return Um booleano
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean verificarContratoPeriodoTeste(Contrato contrato, PontoConsumo pontoConsumo, Date dataInicial, Date dataFinal)
					throws NegocioException;

	/**
	 * Método responsável por consultar
	 * Solicitacoes de consumo de Ponto por
	 * Consumo em um periodo definido.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @return the collection
	 */
	Collection<SolicitacaoConsumoPontoConsumo> consultarSolicitacaoConsumoPontoConsumoPorPeriodo(Long chavePontoConsumo, Date dataInicio,
					Date dataFim);

	/**
	 * Método responsável por validar se todos os
	 * imoveis ja foram adicionados ao contrato.
	 *
	 * @param todosPontosConsumoDadosInformados the todos pontos consumo dados informados
	 * @param pontoConsumoNaoInformado the ponto consumo nao informado
	 * @throws NegocioException the negocio exception
	 */
	void validarTodosPontosConsumoDadosInformados(Boolean todosPontosConsumoDadosInformados, String pontoConsumoNaoInformado)
					throws NegocioException;

	/**
	 * Método responsável por obter o
	 * ContratoPontoConsumoItemFaturamento
	 * prioritário para todos os pontos de consumo
	 * de um contrato.
	 *
	 * @param contrato o contrato
	 * @return ContratoPontoConsumoItemFaturamento
	 *         prioritário
	 * @throws NegocioException caso ocorra algum erro
	 */
	ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamentoPrioritario(Contrato contrato) throws NegocioException;
	
	/**
	 * Método responsável por obter o
	 * ContratoPontoConsumoItemFaturamento
	 * prioritário para todos os pontos de consumo
	 * de um contrato.
	 * 
	 * @param contrato o contrato
	 * @param pontoConsumo o ponto de consumo
	 * @return ContratoPontoConsumoItemFaturamentoPrioritario
	 * @throws NegocioException caso ocorra algum erro
	 */
	ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamentoPrioritario(Contrato contrato,
			PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Método responsável por salvar parcialmente
	 * o contrato.
	 *
	 * @param contrato Contrato a ser salvo
	 *            parcialmente.
	 * @return chave primária do contrato que foi
	 *         salvo parcialmente.
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	Long salvarContratoParcial(Contrato contrato) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por inserir um contrato.
	 * Caso já tenha sido salvo parcialmente, será
	 * alterado respeitando o status que foi
	 * escolhido.
	 *
	 * @param contrato Contrato
	 * @return idContrato
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	Long inserirContrato(Contrato contrato) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por validar os QDcs
	 * associados ao contrato.
	 * 
	 * @param dados
	 *            do contrato.
	 * @param contrato
	 *            Contrato a ser validado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarDataQdcContrato(Map<String, Object> dados, Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por atualiza um
	 * contratoPontoConsumo.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	void atualizarContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por validar renovação
	 * automática do contrato.
	 *
	 * @param renovacaoAutomatica the renovacao automatica
	 * @param diasRenovacao the dias renovacao
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarCampoRenovacaoAutomatica(String renovacaoAutomatica, String diasRenovacao) throws NegocioException;

	/**
	 * Método responsável por validar os pcs
	 * associados ao contrato.
	 *
	 * @param idsLocalAmostragemAssociados the ids local amostragem associados
	 * @param pcs the pcs
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	void validarCampoPCS(Long[] idsLocalAmostragemAssociados, String pcs) throws NegocioException;

	/**
	 * Método responsável por validar os pcs
	 * associados ao contrato.
	 *
	 * @param chave the chave
	 * @return the collection
	 * @throws NegocioException Caso ocorra algum erro de
	 *             concorrência na invocação do
	 *             método.
	 */
	Collection<ContratoPontoConsumo> consultarPontoConsumoPorImovelComContrato(Long chave) throws NegocioException;

	// Em aditar, alterar contrato, após alteração
	// de contrato agrupado por valor para
	// agrupado por volume, uma mensagem de aviso
	// deverá ser exibida no evento de avançar pra
	// segunda página do contrato.

	/**
	 * Método responsável por informar se houve ou
	 * alteração em um contrato agrupado por valor
	 * para por volume.
	 *
	 * @param contrato the contrato
	 * @return true, if is alteracao contrato agrupado por valor para por volume
	 * @throws NegocioException the negocio exception
	 */
	boolean isAlteracaoContratoAgrupadoPorValorParaPorVolume(Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por verificar o
	 * vencimento dos contratos.
	 *
	 * @param logProcessamento the log processamento
	 * @param dadosAuditoria the dados auditoria
	 * @param processo the processo
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InstantiationException the instantiation exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws NoSuchMethodException the no such method exception
	 * @throws GGASException the GGAS exception
	 */
	void verificarVencimentoContrato(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria, Processo processo)
					throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, GGASException;

	/**
	 * Método responsável por obter o historico de
	 * aditivos de um contrato.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<Contrato> obterHistoricoAditivos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter os pontos de
	 * consumo de um contrato.
	 *
	 * @param filtro the filtro
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<PontoConsumo> obterPontosConsumoContrato(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter a quantidade
	 * de volumes de um contrato.
	 *
	 * @param filtro the filtro
	 * @return the quantidade volumes vo
	 * @throws NegocioException the negocio exception
	 */
	QuantidadeVolumesVO obterQuantidadesVolumesVO(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se houve
	 * alguma mudança de titularidade do contrato
	 * ativo de um ponto de consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @return date
	 * @throws NegocioException the negocio exception
	 */
	Date verificarMudancaTitularidadeContratoPontoConsumo(PontoConsumo pontoConsumo, Date dataInicio, Date dataFim) throws NegocioException;

	/**
	 * Carrega o contrado do identificador
	 * informado e valida se ele pode ser
	 * encerrado.
	 * 
	 * @param idContrato
	 *            Identificador do contrato.
	 * @return contrato a encerrar.
	 * @throws NegocioException
	 *             Erro de validação.
	 */
	Contrato carregarContratoEncerramentoRescisao(Long idContrato) throws NegocioException;

	/**
	 * Calcula o valor total da indenizacao de
	 * recisao a partir dos valores informados.
	 *
	 * @param dadosCalculo VO com os valores para o
	 *            cálculo, onde o resultado é
	 *            armazenado.
	 * @throws NegocioException the negocio exception
	 */
	void calcularIndenizacaoRescisao(CalculoIndenizacaoVO dadosCalculo) throws NegocioException;

	/**
	 * Recupera o valor do indice financeiro
	 * encontrado ou null.
	 *
	 * @param dataReferencia Mês/ano de referência do indice
	 *            a recuperar.
	 * @param descricaoAbreviada Descricao abreviada do indice
	 *            cujo valor deve ser recuperado.
	 * @return valorIndice
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal buscarValorIndiceFinanceiroNominal(DateTime dataReferencia, String descricaoAbreviada) throws NegocioException;

	/**
	 * Retorna o período de consumo em meses.
	 *
	 * @param datainvestimento the datainvestimento
	 * @param dataRecisao the data recisao
	 * @return mesesPeriodoConsumo
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal calcularMesesPeriodoConsumo(DateTime datainvestimento, DateTime dataRecisao) throws NegocioException;

	/**
	 * preparação de VO de cálculo.
	 *
	 * @param contrato the contrato
	 * @param idPontoConsumo the id ponto consumo
	 * @return the calculo indenizacao vo
	 * @throws NegocioException the negocio exception
	 */
	CalculoIndenizacaoVO prepararCalculoIndenizacaoRescisao(Contrato contrato, Long idPontoConsumo) throws NegocioException;

	/**
	 * Cria uma cópia do contrato informado
	 * desvinculada do hinernate.
	 *
	 * @param contrato Contrato base.
	 * @param dadosAuditoria the dados auditoria
	 * @return cloneContrato
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InstantiationException the instantiation exception
	 * @throws NoSuchMethodException the no such method exception
	 */
	Contrato popularAditarContrato(Contrato contrato, DadosAuditoria dadosAuditoria) throws IllegalAccessException,
					InvocationTargetException, InstantiationException, NoSuchMethodException;

	/**
	 * Recuperar contrato com as listas tudo
	 * carregadas.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param habilitado Caso nulo, retorna o contrato da
	 *            chave informada independente de
	 *            status
	 * @return contrato
	 */
	Contrato obterContratoListasCarregadas(long chavePrimaria, Boolean habilitado);

	/**
	 * Obter contrato ponto consumo.
	 *
	 * @param contrato O contrato
	 * @param pontoConsumo O ponto de consumo
	 * @return Um Contrato Ponto Consumo
	 * @throws NegocioException Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ContratoPontoConsumo obterContratoPontoConsumo(Contrato contrato, PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * modalidades do ponto de consumo.
	 * 
	 * @param contrato
	 *            O Contrato
	 * @param pontoConsumo
	 *            O ponto de consumo
	 * @return Uma colecao de Contrato Ponto
	 *         Consumo Modalidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ContratoPontoConsumoModalidade> consultarContratoPontoConsumoModalidades(Contrato contrato, PontoConsumo pontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por calcular a QDC média
	 * para uma modalidade num determinado período.
	 *
	 * @param listaContratoPontoConsumoModalidade the lista contrato ponto consumo modalidade
	 * @param primeiroDiaPeriodo data de término do período
	 * @param ultimoDiaPeriodo the ultimo dia periodo
	 * @return valor da QDC média
	 * @throws NegocioException caso ocorra algum erro
	 */
	BigDecimal calcularQDCMedia(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Date primeiroDiaPeriodo,
					Date ultimoDiaPeriodo) throws NegocioException;

	/**
	 * Método responsável por obter
	 * ContratoPontoConsumoModalidade.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param idContrato the id contrato
	 * @param idModalidade the id modalidade
	 * @return ContratoPontoConsumoModalidade
	 * @throws NegocioException the negocio exception
	 */
	ContratoPontoConsumoModalidade obterContratoPontoConsumoModalidade(Long idPontoConsumo, Long idContrato, Long idModalidade)
					throws NegocioException;

	/**
	 * Método responsável por obter
	 * ContratoPontoConsumoPenalidade.
	 *
	 * @param idContratoPontoConsumoModalidade the id contrato ponto consumo modalidade
	 * @param idPenalidade the id penalidade
	 * @param idPeriodicidadePenalidade the id periodicidade penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return ContratoPontoConsumoPenalidade
	 * @throws NegocioException the negocio exception
	 */
	ContratoPontoConsumoPenalidade obterContratoPontoConsumoPenalidade(Long idContratoPontoConsumoModalidade, Long idPenalidade,
					Long idPeriodicidadePenalidade, Date dataInicioApuracao, Date dataFinalApuracao) throws NegocioException;

	/**
	 * Validar datas consultar extrato.
	 *
	 * @param dataIni the data ini
	 * @param dataFim the data fim
	 * @throws GGASException the GGAS exception
	 */
	void validarDatasConsultarExtrato(String dataIni, String dataFim) throws GGASException;

	/**
	 * método para obter uma lista de contratos
	 * que possuem penalidades.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Contrato> obterListaContratoComPenalidades() throws NegocioException;

	/**
	 * Método responsável por validar a data de
	 * vencimento da nota de débito ou crédito.
	 *
	 * @param dataVencimento Data de vencimento que será
	 *            validada.
	 * @throws NegocioException the negocio exception
	 */
	void validarDataVencimento(Date dataVencimento) throws NegocioException;

	/**
	 * Método responsável se o contrato pode ser
	 * encerrado ou rescindido.
	 *
	 * @param chaveContrato the chave contrato
	 * @throws GGASException the GGAS exception
	 */
	void validarEncerrarRescindirContrato(Long chaveContrato) throws GGASException;

	/**
	 * Consultar contrato ponto consumo por ponto consumo recente.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @return the contrato ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumoRecente(Long chavePontoConsumo) throws NegocioException;

	/**
	 * Consultar contrato ponto consumo por ponto consumo antes data emissao.
	 *
	 * @param fatura the fatura
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoAntesDataEmissao(Fatura fatura) throws NegocioException;

	/**
	 * Consultar contrato ponto consumo por ponto consumo ativo ou mais recente.
	 *
	 * @param fatura the fatura
	 * @return the contrato
	 * @throws NegocioException the negocio exception
	 */
	Contrato consultarContratoPontoConsumoPorPontoConsumoAtivoOuMaisRecente(Fatura fatura) throws NegocioException;

	/**
	 * Consultar contrato ativo por contrato pai.
	 *
	 * @param chaveContratoPai the chave contrato pai
	 * @return the contrato
	 * @throws NegocioException the negocio exception
	 */
	Contrato consultarContratoAtivoPorContratoPai(Long chaveContratoPai) throws NegocioException;

	/**
	 * Popular encerrar contrato.
	 *
	 * @param contrato the contrato
	 * @param dadosAuditoria the dados auditoria
	 * @return the contrato
	 * @throws IllegalAccessException the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws InstantiationException the instantiation exception
	 * @throws NoSuchMethodException the no such method exception
	 */
	Contrato popularEncerrarContrato(Contrato contrato, DadosAuditoria dadosAuditoria)
			throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException;
	
	/**
	 * Consultar lista de contrato ponto consumo modalidade qdc por modalidade.
	 * 
	 * @param chaveContratoPontoConsumoModalidade a chave do ContratoPontoConsumoModalidadeQDC
	 * @return the lista de contrato ponto consumo modalidade qdc
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumoModalidadeQDC> consultarContratoPontoConsumoModalidadeQDCPorModalidade(
					Long chaveContratoPontoConsumoModalidade) throws NegocioException;

	/**
	 * Consultar contrato ponto consumo modalidade qdc por modalidade.
	 *
	 * @param listaContratoPontoConsumoModalidade the lista contrato ponto consumo modalidade
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal consultarContratoPontoConsumoModalidadeQDCPorModalidade(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Date dataInicio, Date dataFim)
									throws NegocioException;

	/**
	 * Método responsável por obter um
	 * contratoPontoConsumo pela chave primaria.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the contrato ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	ContratoPontoConsumo obterContratoPontoConsumo(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a soma de
	 * todos os QDCs de um contrato.
	 *
	 * @param idContrato the id contrato
	 * @param data the data
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal obterSomaQdcContrato(Long idContrato, Date data) throws NegocioException;

	/**
	 * Método responsável por obter a soma de
	 * todos os QDCs do contrato em um periodo.
	 *
	 * @param idContrato the id contrato
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal obterSomaQdcContratoPorPeriodo(Long idContrato, Date dataInicial, Date dataFinal) throws NegocioException;

	/**
	 * Atualizar data recisao contrato.
	 *
	 * @param chaveContrato the chave contrato
	 * @param dataRecisao the data recisao
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	void atualizarDataRecisaoContrato(Long chaveContrato, Date dataRecisao, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Método responsável por obter a data de
	 * inicio do contrato.
	 *
	 * @param contrato the contrato
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param dataProgramacao the data programacao
	 * @return the date
	 * @throws NegocioException the negocio exception
	 */
	Date obterDataInicialContrato(Contrato contrato, ContratoPontoConsumo contratoPontoConsumo, Date dataProgramacao)
					throws NegocioException;

	/**
	 * Método responsável por obter a data de fim
	 * do contrato.
	 *
	 * @param contrato the contrato
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param dataProgramacao the data programacao
	 * @return the date
	 * @throws NegocioException the negocio exception
	 */
	Date obterDataFinalContrato(Contrato contrato, ContratoPontoConsumo contratoPontoConsumo, Date dataProgramacao) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * situaçãoes de um contrato.
	 *
	 * @return Collection<EntidadeNegocio>
	 * @throws NegocioException the negocio exception
	 */
	Collection<SituacaoContrato> listarSituacaoContrato() throws NegocioException;

	/**
	 * Método responsável por consultar contrato
	 * por numero.
	 *
	 * @param numeroContrato the numero contrato
	 * @return Collection<EntidadeNegocio>
	 * @throws NegocioException the negocio exception
	 */
	Collection<Contrato> consultarContratoPorNumero(String numeroContrato) throws NegocioException;

	/**
	 * Se necessário altera os relaciomantos dos
	 * clientes, cujos contratos foram removidos,
	 * dos recpectivos imóveis.
	 *
	 * @param idContratoRemovido the id contrato removido
	 * @param chavesPrimarias the chaves primarias
	 * @throws GGASException the GGAS exception
	 */
	void removerClienteImovel(Long idContratoRemovido, Collection<Long> chavesPrimarias) throws GGASException;

	/**
	 * Verifica se existem outros contratos ativos
	 * referentes ao cliente imóvel,
	 * além daqueles cujo id foi informado.
	 *
	 * @param clienteImovel Valores de filtro para o
	 *            cliente, o imóvel e o tipo de
	 *            relacionamento entre eles
	 * @param idsContratosDesconsiderar Identificadores dos contratos a
	 *            desconsiderar
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	boolean existeOutrosContratosAtivosClienteImovel(ClienteImovel clienteImovel, Collection<Long> idsContratosDesconsiderar)
					throws NegocioException;

	/**
	 * Verifica se existem pendencias para os
	 * pontos de consumo vinculado ao contrato,.
	 *
	 * @param chavesPrimariasPontoConsumo the chaves primarias ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	void verificarDependenciaLeituraFaturamento(Collection<PontoConsumo> chavesPrimariasPontoConsumo) throws NegocioException;

	/**
	 * Consultar contrato ponto consumo.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Obter contrato faturavel por ponto consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param faturavel the faturavel
	 * @return the contrato ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	ContratoPontoConsumo obterContratoFaturavelPorPontoConsumo(Long idPontoConsumo, Boolean faturavel) throws NegocioException;

	/**
	 * Consultar contrato por ponto consumo e por data recisao.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Contrato> consultarContratoPorPontoConsumoEPorDataRecisao(PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Validar contrato ativo por data leitura.
	 *
	 * @param dataLeitura the data leitura
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param pontoConsumo the ponto consumo
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	Map<String, Object> validarContratoAtivoPorDataLeitura(Date dataLeitura, ContratoPontoConsumo contratoPontoConsumo,
					PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Obter periodicidade por ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @return the periodicidade
	 * @throws NegocioException the negocio exception
	 */
	Periodicidade obterPeriodicidadePorPontoConsumo(PontoConsumo pontoConsumo, Date dataRealizacaoLeitura) throws NegocioException;

	/**
	 * Consultar contrato ponto consumo por ponto de consumo contrato ativo.
	 *
	 * @param cdPontoConsumo the cd ponto consumo
	 * @return the contrato ponto consumo
	 */
	ContratoPontoConsumo consultarContratoPontoConsumoPorPontoDeConsumoContratoAtivo(Long cdPontoConsumo);

	/**
	 * Consultar cliente por ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param chavePrimariaContrato a chave primaria do contrato
	 * @return the cliente
	 */
	Cliente consultarClientePorPontoConsumo(Long pontoConsumo, Long chavePrimariaContrato);

	/**
	 * Listar contrato por numero.
	 *
	 * @param numero the numero
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Contrato> listarContratoPorNumero(Integer numero) throws NegocioException;

	/**
	 * Listar contrato qdc.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chavePrimariaContrato the chave primaria contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoQDC> listarContratoQDC(Long chavePontoConsumo, Long chavePrimariaContrato) throws NegocioException;

	/**
	 * Listar contrato ponto consumo item faturamento por contrato ponto consumo.
	 *
	 * @param idContratoPontoConsumo the id contrato ponto consumo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumoItemFaturamento> listarContratoPontoConsumoItemFaturamentoPorContratoPontoConsumo(
					long idContratoPontoConsumo) throws NegocioException;

	/**
	 * Listar contrato ponto consumo descricao.
	 *
	 * @param chavePrimariaContrato the chave primaria contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> listarContratoPontoConsumoDescricao(Long chavePrimariaContrato) throws NegocioException;

	/**
	 * Listar contrato ponto consumo por ponto.
	 *
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param chavePrimariaContrato the chave primaria contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> listarContratoPontoConsumoPorPonto(Long chavePontoConsumo, Long chavePrimariaContrato)
					throws NegocioException;

	/**
	 * Listar contrato ponto consumo situacao inativo.
	 *
	 * @param chavePrimariaContrato the chave primaria contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> listarContratoPontoConsumoSituacaoInativo(Long chavePrimariaContrato) throws NegocioException;

	/**
	 *  Teste
	 * @param chavePrimariaContrato O id do contrato
	 * @param tipos os tipos
	 * @return a lista de ContrtoPontoConsumo
	 * @throws NegocioException O NegocioException
	 */
	Collection<ContratoPontoConsumo> listarContratoPontoConsumoPorSituacao(Long chavePrimariaContrato, List<String> tipos)
			throws NegocioException;

	/**
	 * Validar juros mora contrato.
	 *
	 * @param contrato the contrato
	 * @throws NegocioException the negocio exception
	 */
	void validarJurosMoraContrato(Contrato contrato) throws NegocioException;

	/**
	 * Salvar migracao modelo contrato.
	 *
	 * @param mapaContratoMigracao the mapa contrato migracao
	 * @param mapaAtributosAdicionados the mapa atributos adicionados
	 * @param mapaAtributosRemovidos the mapa atributos removidos
	 * @param mapaAtributosComuns the mapa atributos comuns
	 * @param novoModeloContrato the novo modelo contrato
	 * @return the string
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws GGASException the GGAS exception
	 */
	String salvarMigracaoModeloContrato(
					Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao,
					Map<String, Collection<ModeloAtributo>> mapaAtributosAdicionados,
					Map<String, Collection<ModeloAtributo>> mapaAtributosRemovidos,
					Map<String, Collection<ModeloAtributo>> mapaAtributosComuns, ModeloContrato novoModeloContrato) throws GGASException;

	/**
	 * Obter contrato por cliente.
	 *
	 * @param idCliente the id cliente
	 * @return the contrato
	 * @throws NegocioException the negocio exception
	 */
	Contrato obterContratoPorCliente(Long idCliente) throws NegocioException;

	/**
	 * Obter lista contrato por cliente.
	 *
	 * @param idCliente the id cliente
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<Contrato> obterListaContratoPorCliente(Long idCliente) throws NegocioException;

	/**
	 * Obter contrato ponto consumo por cliente.
	 *
	 * @param idCliente the id cliente
	 * @param propriedadesLazy the propriedades lazy
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> obterContratoPontoConsumoPorCliente(Long idCliente, String... propriedadesLazy)
					throws NegocioException;

	/**
	 * Listar contrato ponto consumo modalidade por hora.
	 *
	 * @param hora the hora
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorHora(Integer hora) throws NegocioException;

	/**
	 * Obter qdc estabelecido contrato.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicioVigencia the data inicio vigencia
	 * @param dataFimVigencia the data fim vigencia
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal obterQDCEstabelecidoContrato(PontoConsumo pontoConsumo, Date dataInicioVigencia, Date dataFimVigencia)
					throws NegocioException;

	/**
	 * Validar contrato encerrado.
	 *
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	void validarContratoEncerrado(Long idContrato) throws NegocioException;

	/**
	 * Obter contrato encerrado ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the contrato ponto consumo
	 */
	ContratoPontoConsumo obterContratoEncerradoPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Obter qdc contrato valido por data.
	 *
	 * @param data the data
	 * @param idContrato the id contrato
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal obterQdcContratoValidoPorData(Date data, long idContrato) throws NegocioException;

	/**
	 * Validar datas contrato complementar.
	 *
	 * @param chavePrimariaPrincipal the chave primaria principal
	 * @param contrato the contrato
	 * @throws NegocioException the negocio exception
	 */
	void validarDatasContratoComplementar(Long chavePrimariaPrincipal, Contrato contrato) throws NegocioException;

	/**
	 * Consultar contrato principal e complementar.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Contrato> consultarContratoPrincipalEComplementar(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Calcular qtde retirada complementar.
	 *
	 * @param contrato the contrato
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal calcularQtdeRetiradaComplementar(Contrato contrato, Date dataInicial, Date dataFinal) throws NegocioException;

	/**
	 * Obter soma qdc contrato.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param dataInicioVigencia the data inicio vigencia
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal obterSomaQdcContrato(ContratoPontoConsumo contratoPontoConsumo, Date dataInicioVigencia) throws NegocioException;

	/**
	 * Validar dados contrato complementar.
	 *
	 * @param contrato the contrato
	 * @throws NegocioException the negocio exception
	 */
	void validarDadosContratoComplementar(Contrato contrato) throws NegocioException;

	/**
	 * Criar contrato penalidade.
	 *
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContratoPenalidade();

	/**
	 * Validar adicao contrato penalidade.
	 *
	 * @param contratoPenalidade the contrato penalidade
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @throws NegocioException the negocio exception
	 */
	void validarAdicaoContratoPenalidade(ContratoPenalidade contratoPenalidade, Collection<ContratoPenalidade> listaContratoPenalidade)
					throws NegocioException;

	/**
	 * Listar contrato penalidade retirada maior menor por contrato.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the collection
	 */
	Collection<ContratoPenalidade> listarContratoPenalidadeRetiradaMaiorMenorPorContrato(Long chavePrimaria);

	/**
	 * Obter contrato penalidade vigencia retirada maior menor.
	 *
	 * @param params the params
	 * @return the contrato penalidade
	 */
	ContratoPenalidade obterContratoPenalidadeVigenciaRetiradaMaiorMenor(Map<String, Object> params);

	/**
	 * Obter contrato ponto consumo penalidade vigencia.
	 *
	 * @param params the params
	 * @return the contrato ponto consumo penalidade
	 */
	ContratoPontoConsumoPenalidade obterContratoPontoConsumoPenalidadeVigencia(Map<String, Object> params);

	/**
	 * Obter contrato penalidade.
	 *
	 * @param idContrato the id contrato
	 * @param idPenalidade the id penalidade
	 * @param idPeriodicidadePenalidade the id periodicidade penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the contrato penalidade
	 * @throws NegocioException the negocio exception
	 */
	ContratoPenalidade obterContratoPenalidade(Long idContrato, Long idPenalidade, Long idPeriodicidadePenalidade, Date dataInicioApuracao,
					Date dataFinalApuracao) throws NegocioException;

	/**
	 * Listar contrato penalidade take or pay por contrato.
	 *
	 * @param chavePrimariaContrato the chave primaria contrato
	 * @return the collection
	 */
	Collection<ContratoPenalidade> listarContratoPenalidadeTakeOrPayPorContrato(Long chavePrimariaContrato);

	/**
	 * Validar intervalo data vigencia com data assinatura contrato.
	 *
	 * @param contrato the contrato
	 * @param contratoPenalidade the contrato penalidade
	 * @param contratoPontoConsumoPenalidade the contrato ponto consumo penalidade
	 * @throws NegocioException the negocio exception
	 */
	void validarIntervaloDataVigenciaComDataAssinaturaContrato(Contrato contrato, ContratoPenalidade contratoPenalidade,
					ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws NegocioException;

	/**
	 * Lista valor nominal indice finaceiro.
	 *
	 * @param dataReferenciaInicial the data referencia inicial
	 * @param dataReferenciaFinal the data referencia final
	 * @param descricaoAbreviada the descricao abreviada
	 * @return the collection
	 */
	Collection<IndiceFinanceiroValorNominal> listaValorNominalIndiceFinaceiro(Date dataReferenciaInicial, Date dataReferenciaFinal,
					String descricaoAbreviada);

	/**
	 * Fator correção indice financeiro
	 * 
	 * @param indiceFinanceiro o indice financeiro
	 * @param dataReferenciaInicial a data referencia inicial
	 * @return big decimal
	 */
	BigDecimal fatorCorrecaoIndiceFinanceiro(
					IndiceFinanceiro indiceFinanceiro, Date dataReferenciaInicial);

	/**
	 * Fator correção indice financeiro
	 * 
	 * @param indiceFinanceiro o indice financeiro
	 * @param dataReferenciaInicial a data referencia inicial
	 * @param dataReferenciaFinal a data referencia final
	 * @return big decimal.
	 */
	BigDecimal fatorCorrecaoIndiceFinanceiro(
					IndiceFinanceiro indiceFinanceiro, Date dataReferenciaInicial,
					Date dataReferenciaFinal);

	/**
	 * Consultar contrato por arrecadador convenio contrato.
	 *
	 * @param idArrecadadorConvenioContrato the id arrecadador convenio contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Contrato> consultarContratoPorArrecadadorConvenioContrato(Long idArrecadadorConvenioContrato) throws NegocioException;

	/**
	 * Recupera contratoPontoConsumoItemFaturamento por filtro.
	 *
	 * @param filtro the filtro
	 * @return contratoPontoConsumoItemFaturamento
	 */
	ContratoPontoConsumoItemFaturamento consultarContratoPontoConsumoItemFaturamento(Map<String, Object> filtro);
	
	/**
	 * Obtém o {@code pontoConsumo}, {@code contrato} e {@code situacaoContrato} 
	 * de ContratoPontoConsumo com Contratos Faturáveis, por códigos do Ponto de Consumo no Supervisório
	 * @param codigosPontoConsumoSupervisorio as chaves do ponto de consumo supervisório
	 * @return Map - ContratoPontoConsumo por atributo chavePrimaria do PontoConsumo 
	 **/
	Map<Long, ContratoPontoConsumo> consultarContratoFaturavelPorCodigoSupervisorio(String[] codigosPontoConsumoSupervisorio);

	/**
	 * Verifica se existe algum contrato ativo em uma época de leitura
	 * e obtém os atributos: {@code periodicidade}, {@code numeroHoraInicial},
	 * {@code contrato} e {@code chavePrimaria} do ContratoPontoConsumo 
	 * de um Contrato com maior data de recisão
	 * @param pontoConsumo o ponto de consumo
	 * @param dataLeitura a data de leitura
	 * @return ContratoPontoConsumo - ContratoPontoConsumo do Contrato ativo na época de leitura
	 **/
	ContratoPontoConsumo verificarContratoAtivoEpocaLeitura(PontoConsumo pontoConsumo, Date dataLeitura);
	/**
	 * Realiza a consulta do ContratoPontoConsumo com Contrato mais recente, ordenado
	 * por chave primária do Contrato. Obtém do ContratoPontoConsumo, os seguintes atributos:
	 * {@code chavePrimaria}, {@code periodicidade},{@code acaoAnormalidadeConsumoSemLeitura},
	 * {@code medidaFornecimentoMinMensal}, {@code numeroFatorCorrecao}, {@code unidadePressao},
	 * {@code medidaPressao}, {@code tipoMedicao}. Obtém do PontoConsumo, os seguintes atributos:
	 * {@code chavePrimaria}
	 * @param chavesPontosConsumo as chaves do ponto de consumo
	 * @return mapa de ContratoPontoConsumo por chave primária de PontoConsumo
	 */
	Map<Long, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoRecente(Long[] chavesPontosConsumo);
	
	/**
	 * Obtém o ContratoPontoConsumo de um Contrato Ativo. Obtém do ContratoPontoConsumo, 
	 * os seguintes atributos: {@code chavePrimaria}, {@code periodicidade}, 
	 * {@code acaoAnormalidadeConsumoSemLeitura}, {@code medidaFornecimentoMinMensal}, 
	 * {@code numeroFatorCorrecao}, {@code unidadePressao}, {@code medidaPressao}, 
	 * {@code tipoMedicao}. Obtém do PontoConsumo, os seguintes atributos: {@code chavePrimaria}
	 * @param chavesPontoConsumo as chaves do ponto de consumo
	 * @return mapa de ContratoPontoConsumo por chave primária de PontoConsumo
	 */
	Map<Long, ContratoPontoConsumo> obterContratoAtivoPontoConsumo(Long[] chavesPontoConsumo);
	
	/**
	 * A partir de um conjunto de chaves de ponto de consumo, obtem apenas as chaves dos pontos de consumo com contrato ativo.
	 *
	 * @param chavesPontoConsumo chaves primárias de pontos com contrato ativo
	 * @return as chaves dos pontos de consumo com contrato ativo
	 */
	Collection<Long> consultarPontoConsumoPorContratoAtivo(Long[] chavesPontoConsumo);

	/**
	 * Realiza a consulta do ContratoPontoConsumo com Contrato mais recente, ordenado por chave primária do Contrato, obtendo, além dos
	 * dados do ContratoPontoConsumo. o ponto de consumo e o contrato.
	 * 
	 * @param listaPontoConsumo a lista de ponto de consumo
	 * @return mapa de ContratoPontoConsumo por PontoConsumo
	 */
	Map<PontoConsumo, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoRecente(Collection<PontoConsumo> listaPontoConsumo);

	/**
	 * Consulta o ContratoPontoConsumo por chaves primárias de PontoConsumo.
	 * Obtém os seguintes dados de ContratoPontoConsumo: {@code chavePrimaria},
	 * {@code isEmiteNotaFiscalEletronica}.
	 * @param chavesPontoConsumo as chaves do ponto de consumo
	 * @return mapa de ContratoPontoConsumo por chave de PontoConsumo
	 */
	Map<Long, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoDeConsumoContratoAtivo(List<Long> chavesPontoConsumo);
	
	/**
	 * Consultar contrato ponto consumo programacao consumo.
	 *
	 * @param filtro the filtro
	 * @param listaContratoPontoCanBeEmpty the lista contrato ponto can be empty
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> consultarContratoPontoConsumoProgramacaoConsumo(
			Map<String, Object> filtro, boolean listaContratoPontoCanBeEmpty) 
					throws NegocioException;

	/**
	 * Consulta uma lista de entidades do tipo {@link ContratoPenalidade} por
	 * {@link Contrato} e chave primária de {@link Penalidade}.
	 * 
	 * @param contrato
	 *            - {@link Contrato}
	 * @param chavePrimariaPenalidade
	 *            - {@link Long}
	 * @return lista de penalidades de um contrato - {@link List}
	 */
	List<ContratoPenalidade> consultarContratoPenalidade(Contrato contrato, Long chavePrimariaPenalidade);

	/**
	 * Seleciona de uma lista de entidades do tipo {@link ContratoPenalidade},
	 * aquela com data de inicio da vigência menor ou igual que
	 * {@code dataInicio} e data de fim da vigência maior ou igual que
	 * {@code dataFim}.
	 * 
	 * @param dataInicio
	 *            - {@link Date}
	 * @param dataFim
	 *            - {@link Date}
	 * @param listaContratoPenalidade
	 *            - {@link List}
	 * @return contratoPenalidade - {@link ContratoPenalidade}
	 */
	ContratoPenalidade selecionarContratoPenalidadePorPeriodoEmVigencia(Date dataInicio, Date dataFim,
					List<ContratoPenalidade> listaContratoPenalidade);

	/**
	 * Consulta uma lista de entidades {@link ContratoPontoConsumoModalidadeQDC},
	 * que pertencem a uma coleção de entidades do tipo {@link ContratoPontoConsumoModalidade},
	 * retornando estes com suas respectivas listas de {@link ContratoPontoConsumoModalidadeQDC}
	 * populadas.
	 * 
	 * @param listaContratoPontoConsumoModalidade - {@link ContratoPontoConsumoModalidade}
	 * @return listaContratoPontoConsumoModalidade - {@link Collection}
	 */
	Collection<ContratoPontoConsumoModalidade> popularContratoModalidadeComConsumoQDC(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade);

	/**
	 * Mapeia uma lista de entidades do tipo {@link ContratoPontoConsumoModalidade}
	 * por entidades do tipo {@link PontoConsumo}.
	 * 
	 * @param listaContratoPontoConsumoModalidade - {@link Collection}
	 * @return mapaModalidadesPorPontoConsumo - {@link Map}
	 */
	Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> agruparContratoPontoConsumoModalidadePorPontoConsumo(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade);
	

	/**
	 * Consulta uma lista de entidades {@link Contrato} que pelos seus pontos de Consumo {@link PontoConsumo} e
	 * retorna um mapa Map<K,V> onde K são as chavesPrimarias dos pontos de Consumo.
	 * 
	 * @param listaPontoConsumo A lista de ponto de consumo
	 * @return A lista de contrato
	 */
	Map<Long, Collection<Contrato>> consultarAgrupamentoContratoPorPontoConsumo(Collection<PontoConsumo> listaPontoConsumo);

	/**
	 * Consulta uma lista de entidades {@link PontoConsumo} que pelos seus respectivos Contratos {@link Contrato} e
	 * retorna um mapa Map<K,V> onde K são as chavesPrimarias dos Contratos.
	 * 
	 * @param contratos A lista de contratos
	 * @return A lista de chaves
	 */
	Map<Long, Collection<Long>> consultarChavesPontosConsumoPorContrato(Collection<Contrato> contratos);

	/**
	 * Consulta uma lista de CEPs que pelos seus respectivos Pontos de Consumo {@link PontoConsumo}.
	 * 
	 * @param pontosConsumo O ponto de consumo
	 * @return A lista de CEP
	 * @throws NegocioException O NegocioException
	 */
	Map<Long, Collection<String>> consultarCepDeContratoPontoConsumoPorPontoConsumo(Collection<PontoConsumo> pontosConsumo)
			throws NegocioException;

	/**
	 * Valida Forma de Apuração.
	 * 
	 * @param compromissoTakeOrPayVO o compromissoTakeOrPayVO
	 * @throws GGASException O GGASException
	 */
	void validaFormaDeApuracao(CompromissoTakeOrPayVO compromissoTakeOrPayVO) throws GGASException;

	/**
	 * Verifica se o contrato associada a chave primaria passada como parametro é faturável
	 * 
	 * @param idContrato O id do contrato
	 * @return true se for faturável e false  caso contrário
	 * @throws NegocioException O NegocioException
	 */
	Boolean verificarContratoFaturavel(long idContrato) throws NegocioException;

	/**
	 * Valida dados de anexo do contrato
	 * 
	 * @param contratoAnexo O contratoAnexo
	 * @throws NegocioException o NegocioException
	 */
	void validarDadosContratoAnexo(ContratoAnexo contratoAnexo) throws NegocioException;

	/**
	 * Verifica descricao anexo
	 * 
	 * @param indexLista o indice da lista
	 * @param listaAnexos a lista de anexo
	 * @param contratoAnexo o contrato anexo
	 * @throws NegocioException o NegocioException
	 */
	void verificarDescricaoContratoAnexo(Integer indexLista, Collection<ContratoAnexo> listaAnexos, ContratoAnexo contratoAnexo)
			throws NegocioException;

	/**
	 * Obtem o anexo do contrato
	 * 
	 * @param chavePrimaria A chave primária
	 * @return anexoContrato O contratoAenxo
	 */
	ContratoAnexo obterContratoAnexo(long chavePrimaria);

	/**
	 * Consulta o id arrecadador a partir do id contrato
	 * @param idContrato o id contrato
	 * @return o id do arrecacador
	 */
	Long consultarIdArrecadadorContratoConvenio(Long idContrato);

	/**
	 * Consulta o banco a partir do contrato
	 * @param idContrato O id do contrato
	 * @return O banco do contrato
	 */
	Banco consultarBancoContrato(Long idContrato);


	/**
	 * Obtem uma lista de ContratoPontoConsumoModalidadeQDC
	 * de cada ContratoPontoConsumoModalidade de um ContratoPontoConsumo
	 * @param cdContratoPontoConsumo o id do contrato ponto consumo
	 * @return lista A lista de contratopontoConsumoModalidadeQDCImpl
	 */
	List<ContratoPontoConsumoModalidadeQDCImpl> obterModalidadeQDC(Long cdContratoPontoConsumo);
	
	/**
	 * Obtem uma lista de ContratoPontoConsumoModalidadeQDC por chaves primarias 
	 * de ContratoPontoConsumoModalidade, ContratoPontoConsumo e Contrato Modalidade
	 * @param cdContratoPontoConsumo - {@link Long}
	 * @return lista - {@link List}
	 */
	List<ContratoPontoConsumoModalidadeQDCImpl> obterModalidade(Long cdContratoPontoConsumo);

	/**
	 * Método responsável por consultar
	 * contrato e ponto de consumo 
	 * de um objeto contratoPontoConsumo
	 * 
	 * @param chavesPontoConsumo {@link Long}
	 * @return collection Contrato Ponto consumo {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<ContratoPontoConsumo> consultarDadosContratoPontoConsumoPorPontoConsumo(Long[] chavesPontoConsumo) throws NegocioException;

	/**
	 * Obtem o valor dos tributos do contrato pela chave primaria 
	 * de Cliente
	 * @param cliente - {@link Object}
	 * @return O contrato
	 */
	Contrato obterValorDosTributosDoContrato(Cliente cliente);
	
	/**
	 * Método responsável para validar a migração de contrato.
	 *
	 * @param chaveContrato the chave contrato
	 * @throws GGASException the GGAS exception
	 */
	void validarMigracaoModeloContrato(Long chaveContrato) throws GGASException;

	/**
	 * Método responsável por obter dados do contrato de ponto de consumo por um contrato e um ponto de consumo
	 * 
	 * @param chaveContrato {@link Long}
	 * @param chavePontoConsumo {@link Long}
	 * @return ContratoPontoConsumo {@link ContratoPontoConsumo}
	 */
	public ContratoPontoConsumo obterDadosContratoPontoConsumoPorContratoPontoConsumo(Long chaveContrato, Long chavePontoConsumo);
	
	/**
	 * Obter a chave Primaria, chave Primaria pai e o cliente assinatura de um contrato ativo ponto consumo.
	 *
	 * @param pontoConsumo {@link PontoConsumo}
	 * @return contrato ponto consumo {@link ContratoPontoConsumo}
	 */
	public ContratoPontoConsumo obterCamposContratoAtivoPontoConsumo(PontoConsumo pontoConsumo);
	

	/**
	 * Atualiza contrato
	 *
	 * @param contrato - {@link Contrato}
	 * @param contratoAlterado - {@link contratoAlterado}
	 * @param migracaoContrato - {@link Boolean}
	 * @throws NegocioException - {@link NegocioException}
	 * @return chavePrimaria - {@link Long}
	 */
	Long atualizarContrato(Contrato contrato, Contrato contratoAlterado, Boolean migracaoContrato)
			throws NegocioException;
	
	/**
	 * Obter pontos de consumo com contrato ativo por rota
	 * @param rota - {@link Rota}
	 * @return Lista de Pontos de Consumo - {@link PontoConsumo}
	 */
	List<PontoConsumo> obterPontosConsumoComContratoAtivo(Rota rota);

	/**
	 * Obter Contrato por ponto de consumo
	 * @param contrato - {@link Contrato}
	 * @return Ponto de Consumo - {@link PontoConsumo}
	 */
	PontoConsumo obterPontoConsumoContrato(Contrato contrato);

	ContratoPontoConsumo obterUltimoContratoPontoConsumoPorPontoConsumo(Long chavePontoConsumo);

	void inserirDadosFaturaResidual(Map<Long, List<String>> dadosFaturaResidual , Funcionario funcionario) throws HibernateException, NegocioException, GGASException;

	ContratoPontoConsumo obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(Long chavePontoConsumo);

	ContratoPontoConsumo obterContratoPontoConsumoAtivoCliente(Cliente cliente);
}
