package br.com.ggas.contrato.contrato.impl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.CurrencyWriter;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsavel por montar modelo de impressao do contrato
 *
 */
public class RelatorioContratoHelper {

	private static final Logger LOG = Logger.getLogger(RelatorioContratoHelper.class);

	public static final String LOGOMARCA = "logo";
	public static final String NOME_EMPRESA = "nomeEmpresa";
	public static final String ENDERECO_EMPRESA = "endereco";
	public static final String IDENTIFICACAO_EMPRESA = "identificacaoEmpresa";
	public static final String CONTATOS_EMPRESA = "contatos";
	public static final String TITULO_CONTRATO = "tituloContrato";
	public static final String LAYOUT = "relatorio";
	public static final String BR = "<br />";

	public static final int CASAS_DECIMAIS = 2;
	public static final int FINAL_STRING = 3;
	public static final int QTD_DIAS_ANO = 365;

	private String titulo;
	private String layout = null;
	private Contrato contrato;

	/**
	 * @return titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * @param layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}

	/**
	 * @return contrato
	 */
	public Contrato getContrato() {
		return contrato;
	}

	/**
	 * @param contrato
	 */
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	/**
	 * Substitui os tokens do modelo de impressao pelos valores do contrato
	 * 
	 * @param tokens os tokens que serão processados
	 * @throws GGASException
	 */
	public void processarLayout(Collection<RelatorioToken> tokens) {
		if (this.layout != null) {
			for (RelatorioToken token : tokens) {
				this.layout = this.layout.replaceAll("\\[\\[" + token.getValor() + "\\]\\]",
						obterValor(token.getAlias()));
			}
		}

	}

	/**
	 * Obtem o valor do contrato de um token
	 * 
	 * @param alias
	 * @return
	 * @throws GGASException
	 */
	private String obterValor(String alias) {

		String valor = "";

		Class<RelatorioContratoHelper> classe = RelatorioContratoHelper.class;

		try {
			Method metodoDoSeuObjeto = classe.getMethod(alias);
			valor = (String) metodoDoSeuObjeto.invoke(this);
		} catch (Exception e) {
			LOG.error("Método não encontrado", e);
			return "";
		}

		return valor;
	}

	/**
	 * Nome do cliente
	 * 
	 * @return nome
	 */
	public String getNomeCliente() {
		return this.contrato.getClienteAssinatura().getNome();
	}

	/**
	 * Nome fantasia do cliente
	 * 
	 * @return nomeFantasia
	 */
	public String getNomeFantasiaCliente() {
		if (this.contrato.getClienteAssinatura().getNomeFantasia() != null) {
			return this.contrato.getClienteAssinatura().getNomeFantasia();
		}
		return "";
	}

	/**
	 * Ponto de Consuno
	 * 
	 * @return pontoEntrega
	 */
	public String getPontoEntrega() {
		StringBuilder retorno = new StringBuilder();
		this.contrato.getListaContratoPontoConsumo().forEach(lCPC -> {
			retorno.append(lCPC.getPontoConsumo().getCodigoPontoConsumo()).append("<br>");
		});
		return retorno.toString();

	}

	/**
	 * Cpf ou cnpj do cliente
	 * 
	 * @return documento
	 */
	public String getDocumentoCliente() {
		if (StringUtils.isNotEmpty(this.contrato.getClienteAssinatura().getCpf())) {
			return this.contrato.getClienteAssinatura().getCpfFormatado();
		} else if (StringUtils.isNotEmpty(this.contrato.getClienteAssinatura().getCnpj())) {
			return this.contrato.getClienteAssinatura().getCnpjFormatado();
		} else {
			return "";
		}
	}

	/**
	 * Numero do contrato formatado
	 * 
	 * @return numeroContrato
	 */
	public String getNumeroFormatado() {
		return this.contrato.getNumeroFormatado();
	}

	/**
	 * Nome da empresa
	 * 
	 * @return nomeEmpresa
	 * @throws NegocioException
	 */
	public String getNomeEmpresa() throws NegocioException {
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresaPrincipal = controladorEmpresa.obterEmpresaPrincipal();

		return empresaPrincipal.getCliente().getNomeFantasia();
	}

	/**
	 * Cnpj da empresa
	 * 
	 * @return cnpjEmpresa
	 * @throws NegocioException
	 */
	public String getCnpjEmpresa() throws NegocioException {
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresaPrincipal = controladorEmpresa.obterEmpresaPrincipal();

		if (StringUtils.isNotEmpty(empresaPrincipal.getCliente().getCnpj())) {
			return empresaPrincipal.getCliente().getCnpjFormatado();
		} else {
			return "";
		}
	}

	/**
	 * Inscricao estadual empresa
	 * 
	 * @return inscricaoEstadualEmpresa
	 * @throws NegocioException
	 */
	public String getInscricaoEmpresa() throws NegocioException {
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresaPrincipal = controladorEmpresa.obterEmpresaPrincipal();

		if (StringUtils.isNotEmpty(empresaPrincipal.getCliente().getInscricaoEstadual())) {
			return empresaPrincipal.getCliente().getInscricaoEstadual();
		} else {
			return "";
		}
	}

	/**
	 * Endereco do cliente
	 * 
	 * @return enderecoEmpresa
	 */
	public String getEnderecoCliente() {

		StringBuilder retorno = new StringBuilder();

		if (contrato != null && contrato.getClienteAssinatura() != null
				&& contrato.getClienteAssinatura().getEnderecoPrincipal() != null) {
			retorno.append(contrato.getClienteAssinatura().getEnderecoPrincipal().getEnderecoFormatado());
			if (contrato.getClienteAssinatura().getEnderecoPrincipal().getCep() != null) {
				retorno.append(" | CEP: ");
				retorno.append(contrato.getClienteAssinatura().getEnderecoPrincipal().getCep().getCep());
			}
			return retorno.toString();
		} else {
			return "";
		}
	}

	/**
	 * Endereco da empresa
	 * 
	 * @return enderecoEmpresa
	 * @throws NegocioException
	 */
	public String getEnderecoEmpresa() throws NegocioException {
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresaPrincipal = controladorEmpresa.obterEmpresaPrincipal();
		StringBuilder retorno = new StringBuilder();

		if (empresaPrincipal.getCliente().getEnderecoPrincipal() != null) {
			retorno.append(empresaPrincipal.getCliente().getEnderecoPrincipal().getEnderecoFormatado());
			if (empresaPrincipal.getCliente().getEnderecoPrincipal().getCep() != null) {
				retorno.append(" | CEP: ");
				retorno.append(empresaPrincipal.getCliente().getEnderecoPrincipal().getCep().getCep());
			}
			return retorno.toString();
		} else {
			return "";
		}
	}

	/**
	 * Lista de telefones formatada
	 * 
	 * @return contatosEmpresa
	 * @throws NegocioException
	 */
	public String getContatosEmpresa() throws NegocioException {
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresaPrincipal = controladorEmpresa.obterEmpresaPrincipal();

		if (!Util.isNullOrEmpty(empresaPrincipal.getCliente().getFones())) {
			Collection<ClienteFone> telefones = empresaPrincipal.getCliente().getFones();
			StringBuilder retorno = new StringBuilder();
			for (ClienteFone telefone : telefones) {
				retorno.append(telefone.getTipoFone().getDescricao());
				retorno.append(" (");
				retorno.append(telefone.getCodigoDDD());
				retorno.append(") ");
				retorno.append(telefone.getNumero());
				retorno.append(" | ");
			}
			return retorno.toString().substring(0, retorno.length() - FINAL_STRING);
		} else {
			return "";
		}
	}

	/**
	 * Titulo contrato
	 * 
	 * @return tituloContrato
	 */
	public String getTituloContrato() {
		return this.contrato.getModeloContrato().getDescricao();
	}

	/**
	 * Logradouro do Cliente
	 * 
	 * @return logradouroCliente
	 */
	public String getLogradouroCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getEnderecoLogradouro() != null) {

			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getEnderecoLogradouro();

		} else {
			return "";
		}
	}

	/**
	 * Logradouro do faturamento
	 * 
	 * @return logradouroFaturamento
	 * @throws GGASException
	 */
	public String getLogradouroFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getCep() != null && pontoConsumo.getCep().getLogradouro() != null) {
			if (pontoConsumo.getCep().getTipoLogradouro() != null) {
				return pontoConsumo.getCep().getTipoLogradouro().concat(" ")
						.concat(pontoConsumo.getCep().getLogradouro());
			}
			return pontoConsumo.getCep().getLogradouro();
		}
		return "";
	}

	/**
	 * Numero do endereco do cliente
	 * 
	 * @return numeroEnderecoCliente
	 */
	public String getNumeroEnderecoCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getNumero() != null) {
			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getNumero();
		} else {
			return "";
		}
	}

	/**
	 * Numero do endereco do faturamento
	 * 
	 * @return numeroEnderecoFaturamento
	 * @throws GGASException
	 */
	public String getNumeroEnderecoFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getNumeroImovel() != null) {
			return pontoConsumo.getNumeroImovel();
		}
		return "";
	}

	/**
	 * Clompemento do endereco do cliente
	 * 
	 * @return complementoEnderecoCliente
	 */
	public String getComplementoEnderecoCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getComplemento() != null) {
			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getComplemento();
		} else {
			return "";
		}
	}

	/**
	 * Clompemento do endereco do faturamento
	 * 
	 * @return complementoEnderecoFaturamento
	 * @throws GGASException
	 */
	public String getComplementoEnderecoFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getComplementoEndereco() != null) {
			return pontoConsumo.getComplementoEndereco();
		}
		return "";
	}

	/**
	 * Municipio do endereco do cliente
	 * 
	 * @return municipio
	 */
	public String getMunicipioEnderecoCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getMunicipio() != null && this.contrato
						.getClienteAssinatura().getEnderecoPrincipal().getMunicipio().getDescricaoUpperCase() != null) {
			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getMunicipio().getDescricaoUpperCase();
		} else {
			return "";
		}
	}

	/**
	 * Municipio do endereco do Faturamento
	 * 
	 * @return municipio
	 * @throws GGASException
	 */
	public String getMunicipioEnderecoFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getCep() != null && pontoConsumo.getCep().getNomeMunicipio() != null) {
			return pontoConsumo.getCep().getNomeMunicipio();
		}
		return "";
	}

	/**
	 * Uf do endereco do cliente
	 * 
	 * @return UF
	 */
	public String getUfEnderecoCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep().getUf() != null) {
			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep().getUf();
		} else {
			return "";
		}
	}

	/**
	 * Uf do endereco do faturamento
	 * 
	 * @return UF
	 * @throws GGASException
	 */
	public String getUfEnderecoFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getCep() != null && pontoConsumo.getCep().getUf() != null) {
			return pontoConsumo.getCep().getUf();
		}
		return "";
	}

	/**
	 * @return email
	 */
	public String getEmailPrincipalCliente() {
		if (StringUtils.isNotEmpty(this.contrato.getClienteAssinatura().getEmailPrincipal())) {
			return this.contrato.getClienteAssinatura().getEmailPrincipal();
		} else {
			return "";
		}
	}

	/**
	 * @return emailSecundario
	 */
	public String getEmailSecundarioCliente() {
		if (StringUtils.isNotEmpty(this.contrato.getClienteAssinatura().getEmailSecundario())) {
			return this.contrato.getClienteAssinatura().getEmailSecundario();
		} else {
			return "";
		}
	}

	/**
	 * @return clienteInscricaoEstadual
	 */
	public String getClinteInscricaoEstadual() {
		if (StringUtils.isNotEmpty(this.contrato.getClienteAssinatura().getInscricaoEstadual())) {
			return this.contrato.getClienteAssinatura().getInscricaoEstadual();
		} else if ("1".contentEquals(this.contrato.getClienteAssinatura().getRegimeRecolhimento())) {
			return "ISENTO";
		} else {
			return "";
		}
	}

	/**
	 * @return dataProposta
	 */
	public String getDataEmissaoProposta() {
		if (this.contrato.getProposta() != null && this.contrato.getProposta().getDataEmissao() != null) {
			SimpleDateFormat formato = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			return formato.format(this.contrato.getProposta().getDataEmissao());
		} else {
			return "";
		}
	}

	/**
	 * @return numeroProposta
	 */
	public String getNumeroProposta() {
		if (this.contrato.getProposta() != null && this.contrato.getProposta().getNumeroCompletoProposta() != null) {
			return this.contrato.getProposta().getNumeroCompletoProposta();
		} else {
			return "";
		}
	}

	/**
	 * @return versaoProposta
	 */
	public String getVersaoProposta() {
		if (this.contrato.getProposta() != null && this.contrato.getProposta().getVersaoProposta() != null) {
			return this.contrato.getProposta().getVersaoProposta().toString();
		} else {
			return "";
		}
	}

	/**
	 * @return valorContrato
	 */
	public String getValorContrato() {
		if (this.contrato.getValorContrato() != null) {
			return Util.converterCampoCurrencyParaString(this.contrato.getValorContrato(), Constantes.LOCALE_PADRAO);
		} else {
			return "";
		}
	}

	/**
	 * @return dataAssinatura
	 */
	public String getDataAssinaturaContrato() {
		SimpleDateFormat formato = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		return formato.format(this.contrato.getDataAssinatura());
	}

	/**
	 * @return valorContrato
	 */
	public String getValorContratoPorExtenso() {
		if (this.contrato.getValorContrato() != null) {
			CurrencyWriter cw = CurrencyWriter.getInstance();
			cw.setMoeda(true);
			return cw.write(this.contrato.getValorContrato());
		} else {
			return "";
		}
	}

	/**
	 * @return anosDuracao
	 */
	public String getAnosDuracaoContrato() {
		if (this.contrato.getDataAssinatura() != null && this.contrato.getDataVencimentoObrigacoes() != null) {
			return String.valueOf(
					Util.intervaloAnos(this.contrato.getDataAssinatura(), this.contrato.getDataVencimentoObrigacoes()));
		} else {
			return "";
		}
	}

	/**
	 * @return mesesDuracao
	 */
	public String getMesesDuracaoContrato() {
		if (this.contrato.getDataAssinatura() != null && this.contrato.getDataVencimentoObrigacoes() != null) {
			return String.valueOf(Util.intervaloMeses(this.contrato.getDataAssinatura(),
					this.contrato.getDataVencimentoObrigacoes()));
		} else {
			return "";
		}
	}

	/**
	 * @return mesProposta
	 */
	public String getMesEmissaoProposta() {
		if (this.contrato.getProposta() != null && this.contrato.getProposta().getDataEmissao() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.contrato.getProposta().getDataEmissao());
			int month = cal.get(Calendar.MONTH) + 1;
			Map<Integer, String> meses = Util.carregarMeses();
			return meses.get(month);
		} else {
			return "";
		}
	}

	/**
	 * @return anoProposta
	 */
	public String getAnoEmissaoProposta() {
		if (this.contrato.getProposta() != null && this.contrato.getProposta().getDataEmissao() != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.contrato.getProposta().getDataEmissao());
			return String.valueOf(cal.get(Calendar.YEAR));
		} else {
			return "";
		}
	}

	/**
	 * @return DataTerminoContrato
	 */
	public String getDataTerminoContrato() {
		Locale local = new Locale("pt", "BR");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", local);

		return newFormat.format(this.contrato.getDataVencimentoObrigacoes());
	}

	/**
	 * Ano Atual
	 * 
	 * @return anoAtual
	 */
	public String getAnoAtual() {
		Integer ano = Year.now().getValue();
		return ano.toString();
	}

	/**
	 * @return volume
	 * @throws GGASException
	 */
	public String getVolumeReferencia() {
		if (this.contrato.getVolumeReferencia() != null) {
			return Util.converterCampoValorDecimalParaStringComCasasDecimais("", this.contrato.getVolumeReferencia(),
					Constantes.LOCALE_PADRAO, CASAS_DECIMAIS);
		} else {
			return "";
		}
	}

	/**
	 * @return diasAntecedenciaRenovacaoExtenso
	 */
	public String getDiasAntecedenciaRenovacaoExtenso() {
		if (this.contrato.getDiasAntecedenciaRenovacao() != null) {
			CurrencyWriter cw = CurrencyWriter.getInstance();
			cw.setMoeda(false);
			return cw.write(new BigDecimal(this.contrato.getDiasAntecedenciaRenovacao()));
		} else {
			return "";
		}
	}

	/**
	 * @return diasAntecedenciaRenovacao
	 */
	public String getDiasAntecedenciaRenovacao() {
		if (this.contrato.getDiasAntecedenciaRenovacao() != null) {
			return this.contrato.getDiasAntecedenciaRenovacao().toString();
		} else {
			return "";
		}
	}

	/**
	 * Contato do cliente formatado
	 * 
	 * @return contato2
	 */
	public String getContatoCliente2() {
		if (!Util.isNullOrEmpty(this.contrato.getClienteAssinatura().getContatos())) {
			Iterator<ContatoCliente> iterator = this.getClientes();
			ContatoCliente contato = iterator.next();

			if (iterator.hasNext()) {

				contato = iterator.next();

				return this.montarContato(contato);
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	/**
	 * Montar Contato
	 * 
	 * @return montarContato
	 */
	private String montarContato(ContatoCliente contato) {
		StringBuilder retorno = new StringBuilder();
		retorno.append("<div style='margin-left: 38px;'><p>Nome: ");
		retorno.append(contato.getNome());
		retorno.append(BR);

		if (contato.getProfissao() != null) {
			retorno.append("Cargo: ");
			retorno.append(contato.getProfissao().getDescricao());
			retorno.append(BR);
		}
		retorno.append("Email: ");
		if (contato.getEmail() != null) {
			retorno.append(contato.getEmail());
		}
		retorno.append(BR);
		retorno.append("Telefone: (");
		if (contato.getCodigoDDD() != null) {
			retorno.append(contato.getCodigoDDD());
		} else {
			retorno.append("  ");
		}
		retorno.append(") ");
		if (contato.getFone() != null) {
			retorno.append(contato.getFone());
		}
		retorno.append(BR);
		retorno.append("</p></div>");

		return retorno.toString();
	}

	/**
	 * @return clientes
	 */
	private Iterator<ContatoCliente> getClientes() {
		List<ContatoCliente> contatos = new ArrayList<>(this.contrato.getClienteAssinatura().getContatos());
		Collections.sort(contatos, Comparator.comparing(contato -> (contato.getChavePrimaria())));

		return contatos.iterator();
	}

	/**
	 * Tabela de QDC por ponto de consumo
	 * 
	 * @return tabelaQDC
	 * @throws GGASException
	 */
	public String getTabelaQDCPorPontoConsumo() throws GGASException {
		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {
			StringBuilder retorno = new StringBuilder();
			retorno.append("<div>");
			boolean isVazio = true;

			Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

				if (contratoPontoConsumo.getPontoConsumo() != null
						&& contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea() != null
						&& contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea() != null) {

					contratoPontoConsumo = Fachada.getInstancia()
							.obterContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
					retorno.append("<h4>");
					retorno.append(contratoPontoConsumo.getPontoConsumo().getDescricao());
					retorno.append("</h4>");
					retorno.append(
							"<table><thead><tr><th> ANO </th><th>QUANTIDADE DIÁRIA CONTRATADA(M³)</th><th>VOLUME MÁXIMO (");
					retorno.append(contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricao());
					retorno.append(")</th><th>VOLUME MÍNIMO(");
					retorno.append(contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea().getDescricao());
					retorno.append(")</th></tr></thead><tbody>");

					Calendar cal = Calendar.getInstance();
					List<ContratoQDC> contratoQDCs = new ArrayList<>(this.contrato.getListaContratoQDC());
					Collections.sort(contratoQDCs, Comparator.comparing(ContratoQDC::getData));
					for (ContratoQDC contratoQDC : contratoQDCs) {
						cal.setTime(contratoQDC.getData());

						retorno.append("<tr><td>");
						retorno.append(cal.get(Calendar.YEAR));
						retorno.append("</td><td>");
						retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
								contratoQDC.getMedidaVolume(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS));
						retorno.append("</td> <td>");
						retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
								contratoPontoConsumo.getMedidaVazaoMaximaInstantanea(), Constantes.LOCALE_PADRAO,
								CASAS_DECIMAIS));
						retorno.append("</td><td>");
						retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
								contratoPontoConsumo.getMedidaVazaoMinimaInstantanea(), Constantes.LOCALE_PADRAO,
								CASAS_DECIMAIS));
						retorno.append("</td></tr>");
						isVazio = false;
					}
					retorno.append("</tbody></table></div>");
				}
			}

			if (isVazio) {
				return "";
			} else {
				return retorno.toString();
			}
		} else {
			return "";
		}
	}

	/**
	 * @return cepCliente
	 */
	public String getCepCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep() != null) {
			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep().getCep();
		} else {
			return "";
		}
	}

	/**
	 * @return cepFaturamento
	 * @throws GGASException
	 */
	public String getCepFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getCep() != null && pontoConsumo.getCep().getCep() != null) {
			return pontoConsumo.getCep().getCep();
		}
		return "";
	}

	/**
	 * @return bairroFaturamento
	 * @throws GGASException
	 */
	public String getBairroFaturamento() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getCep() != null && pontoConsumo.getCep().getBairro() != null) {
			return pontoConsumo.getCep().getBairro();
		}
		return "";
	}

	/**
	 * @return bairroCliente
	 */
	public String getBairroCliente() {
		if (this.contrato.getClienteAssinatura().getEnderecoPrincipal() != null
				&& this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep() != null) {
			return this.contrato.getClienteAssinatura().getEnderecoPrincipal().getCep().getBairro();
		} else {
			return "";
		}
	}

	/**
	 * @return vazaoInstantaneaMaxima
	 * @throws GGASException
	 */
	public String getVazaoInstantaneaMaxima() throws GGASException {
		return getVazao(true);
	}

	/**
	 * @return vazaoInstantaneaMinima
	 * @throws GGASException
	 */
	public String getVazaoInstantaneaMinima() throws GGASException {
		return getVazao(false);
	}

	/**
	 * @param maxima
	 * @return vazao
	 * @throws GGASException
	 */
	private String getVazao(boolean maxima) throws GGASException {

		StringBuilder retorno = new StringBuilder();

		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {

			retorno.append("");

			ContratoPontoConsumo contratoPontoConsumo = this.getContratoPontoConsumoUnico();
			if (contratoPontoConsumo != null) {
				if (maxima) {
					if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {

						retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
								contratoPontoConsumo.getMedidaVazaoMaximaInstantanea(), Constantes.LOCALE_PADRAO,
								CASAS_DECIMAIS));
					}

				} else {
					if (contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() != null) {

						retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
								contratoPontoConsumo.getMedidaVazaoMinimaInstantanea(), Constantes.LOCALE_PADRAO,
								CASAS_DECIMAIS));
					}

				}
			} else {
				return "";
			}
		} else {
			return "";
		}

		return retorno.toString();
	}

	/**
	 * @return contratoPontoConsumoUnico
	 * @throws GGASException
	 */
	private ContratoPontoConsumo getContratoPontoConsumoUnico() throws GGASException {
		ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();
		if (contratoPontoConsumo.getPontoConsumo() != null) {
			return Fachada.getInstancia().obterContratoPontoConsumo(contratoPontoConsumo.getChavePrimaria());
		}
		return null;
	}

	/**
	 * @return descPontoConsumoUnico
	 * @throws GGASException
	 */
	public String getDescPontoConsumoUnico() throws GGASException {
		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {

			ContratoPontoConsumo contratoPontoConsumo = this.getContratoPontoConsumoUnico();
			if (contratoPontoConsumo != null) {
				PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
				if (pontoConsumo != null && StringUtils.isNotEmpty(pontoConsumo.getDescricao())) {
					return pontoConsumo.getDescricao();
				}
			}
		}
		return "";
	}

	/**
	 * @return descImovelUnico
	 * @throws GGASException
	 */
	public String getDescImovelUnico() throws GGASException {

		Imovel imovel = getImovelUnicoContrato();
		if (imovel != null && imovel.getNome() != null) {
			return imovel.getNome();
		}
		return "";
	}

	/**
	 * @return descImovelUnico
	 * @throws GGASException
	 */
	public String getDescImovelPai() throws GGASException {

		Imovel imovelPai = getImovelPai();

		if (imovelPai != null && imovelPai.getNome() != null) {
			return "do Condomínio ".concat(imovelPai.getNome());
		}

		return "";
	}

	/**
	 * @return imovel pai
	 * @throws GGASException
	 */
	private Imovel getImovelPai() throws GGASException {

		Imovel imovel = getImovelUnicoContrato();

		if (imovel != null) {
			return imovel.getImovelCondominio();
		}
		return null;
	}

	/**
	 * @return imovelUnico
	 * @throws GGASException
	 */
	private Imovel getImovelUnicoContrato() throws GGASException {
		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {
			ContratoPontoConsumo contratoPontoConsumo = this.getContratoPontoConsumoUnico();
			if (contratoPontoConsumo != null) {
				PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
				if (pontoConsumo != null && pontoConsumo.getImovel() != null) {
					return Fachada.getInstancia().buscarImovelPorChave(pontoConsumo.getImovel().getChavePrimaria(),
							"imovelCondominio");
				}
			}
		}
		return null;
	}

	/**
	 * @return prazo
	 */
	public String getPrazoContrato() {

		if (this.contrato.getDataAssinatura() != null && this.contrato.getDataVencimentoObrigacoes() != null) {

			StringBuilder retorno = new StringBuilder();
			int dias = Util.quantidadeDiasIntervalo(this.contrato.getDataAssinatura(),
					this.contrato.getDataVencimentoObrigacoes());

			if (dias > 0) {
				int anos = dias / QTD_DIAS_ANO;
				dias = dias % QTD_DIAS_ANO;
				boolean temAno = false;

				if (anos > 0) {

					temAno = true;

					if (anos > 1) {
						retorno.append(anos);
						retorno.append(" anos ");
					} else {
						retorno.append("1 ano ");
					}
				}

				if (dias > 0) {

					if (temAno) {
						retorno.append("e ");
					}

					if (dias > 1) {
						retorno.append(dias);
						retorno.append(" dias");
					} else {
						retorno.append("1 dia");
					}
				}
			}
			return retorno.toString();
		}
		return "";
	}

	/**
	 * Retorna prazo do Contrato em Mês
	 *
	 * @return dataVencimento
	 */
	public String getPrazoContratoMes() {
		if (this.contrato.getDataAssinatura() != null && this.contrato.getDataVencimentoObrigacoes() != null) {
			StringBuilder retorno = new StringBuilder();
			int meses = Util.intervaloMeses(this.contrato.getDataAssinatura(),
					this.contrato.getDataVencimentoObrigacoes());

			if (meses > 0) {
				if (meses > 1) {
					retorno.append(meses);
					retorno.append(" meses ");
				} else {
					retorno.append(meses);
					retorno.append(" mês ");
				}

			}

			return retorno.toString();
		}

		return "";
	}

	/**
	 * @return faixaPressaoFornecimentoFormatada
	 * @throws GGASException
	 */
	public String getPressaoFornecimentoPontoConsumo() throws GGASException {
		ContratoPontoConsumo contratoPontoConsumoUnico = this.getContratoPontoConsumoUnico();
		if (contratoPontoConsumoUnico != null && contratoPontoConsumoUnico.getFaixaPressaoFornecimento() != null) {
			FaixaPressaoFornecimento faixaPressaoFornecimento = Fachada.getInstancia().obterFaixaPressaoFornecimento(
					contratoPontoConsumoUnico.getFaixaPressaoFornecimento().getChavePrimaria());
			return Util.converterCampoValorDecimalParaString("", faixaPressaoFornecimento.getMedidaMinimo(),
					Constantes.LOCALE_PADRAO, CASAS_DECIMAIS) + " "
					+ faixaPressaoFornecimento.getUnidadePressao().getDescricaoAbreviada();
		}
		return "";
	}

	/**
	 * @return dataAditamento
	 */
	public String getDataAditamento() {
		SimpleDateFormat formato = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		if (this.contrato.getDataAditivo() != null) {
			return formato.format(this.contrato.getDataAditivo());
		} else {
			return formato.format(this.contrato.getDataAssinatura());
		}
	}

	public String getResponsaveis() {
		StringBuilder retorno = new StringBuilder();

		Collection<ContratoCliente> listaContratoCliente = this.contrato.getListaContratoCliente();
		if (!Util.isNullOrEmpty(listaContratoCliente)) {
			retorno.append("<br/>");
			retorno.append("<table class='tabela' style='width:680px; border:0px solid;' >");
			for (ContratoCliente contratoCliente : listaContratoCliente) {
				retorno.append("<tr>");
				retorno.append(formatarDadosResoponsavel(contratoCliente));
				retorno.append("</tr'>");
			}
			retorno.append("</table>");
			return retorno.toString();
		}

		return "";
	}

	/**
	 * @param contratoCliente
	 * @return dados Responsavel formatado
	 */
	private String formatarDadosResoponsavel(ContratoCliente contratoCliente) {
		StringBuilder retorno = new StringBuilder();
		retorno.append("<td style='border:0px;padding-top: 13px;padding-bottom: 0px;width:50%;text-align:left';>");
		retorno.append("<b>______________________________________________________<br/>");
		retorno.append("Representante Legal: </b>");
		retorno.append(contratoCliente.getCliente().getNome());
		retorno.append("<br /><b>CPF/MF: </b>");
		if (StringUtils.isNotEmpty(contratoCliente.getCliente().getCpf())) {
			retorno.append(contratoCliente.getCliente().getCpfFormatado());
		}
		retorno.append("<br />");
		if (StringUtils.isNotEmpty(contratoCliente.getCliente().getRg())) {
			retorno.append("<b>RG: </b>").append(contratoCliente.getCliente().getRg());
			if (contratoCliente.getCliente().getOrgaoExpedidor() != null) {
				retorno.append(" ").append(contratoCliente.getCliente().getOrgaoExpedidor().getDescricaoAbreviada());
				if (contratoCliente.getCliente().getUnidadeFederacao() != null) {
					retorno.append("-").append(contratoCliente.getCliente().getUnidadeFederacao().getSigla());
				}
			}
		} else if (StringUtils.isNotEmpty(contratoCliente.getCliente().getNumeroPassaporte())) {
			retorno.append("<b>Passaporte/RNE: </b>").append(contratoCliente.getCliente().getNumeroPassaporte());
		}
		retorno.append("<br /></td>");
		return retorno.toString();
	}

	/**
	 * @return telefone 1
	 */
	public String getTelefone1() {
		return getTelefones()[0];
	}

	/**
	 * @return telefone 2
	 */
	public String getTelefone2() {
		return getTelefones()[1];
	}

	/**
	 * @return telefones
	 */
	private String[] getTelefones() {
		String[] telefones = { "", "" };

		Cliente cliente = this.contrato.getClienteAssinatura();
		List<ClienteFone> fones = new ArrayList<>(cliente.getFones());

		Collections.sort(fones, (o1, o2) -> o2.getUltimaAlteracao().compareTo(o1.getUltimaAlteracao()));

		if (fones.size() > 1) {
			telefones[0] = formatarTelefone(fones.get(0));
			telefones[1] = formatarTelefone(fones.get(1));
			for (ClienteFone fone : fones) {
				if (fone.getIndicadorPrincipal() && !telefones[0].equals(formatarTelefone(fone))) {
					telefones[1] = telefones[0];
					telefones[0] = formatarTelefone(fone);
				}
			}
		} else if (!fones.isEmpty()) {
			telefones[0] = formatarTelefone(fones.get(0));
		}
		return telefones;
	}

	/**
	 * @param fone
	 * @return telefone formatado
	 */
	private String formatarTelefone(ClienteFone fone) {
		return "(".concat(String.valueOf(fone.getCodigoDDD())).concat(") ").concat(String.valueOf(fone.getNumero()));
	}

	/**
	 * @return Logradouro, numero e bairro do imovel
	 * @throws GGASException
	 */
	public String getLogNumBairroImov() throws GGASException {

		Imovel imovel = getImovelPaiOuImovel();
		if (imovel != null) {
			return imovel.getEnderecoLogNumBairro();
		}
		return "";
	}

	/**
	 * @return municipio e uf do imovel
	 * @throws GGASException
	 */
	public String getMuniUfImov() throws GGASException {

		Imovel imovel = getImovelPaiOuImovel();
		if (imovel != null) {
			return imovel.getEnderecoMuniUF();
		}
		return "";
	}

	/**
	 * @return imovel pai ou imovel
	 * @throws GGASException
	 */
	private Imovel getImovelPaiOuImovel() throws GGASException {
		Imovel imovel = getImovelPai();
		if (imovel == null) {
			return getImovelUnicoContrato();
		}
		return imovel;
	}

	/**
	 * Modalidade Tarifaria
	 * 
	 * @return descricaoModalidadeTarifaria
	 */
	public String getModalidadeTarifaria() {
		String modalidadeTarifaria = "";
		if (contrato.getListaContratoPontoConsumo().iterator().hasNext()) {
			ContratoPontoConsumo contratoPontoConsumo = contrato.getListaContratoPontoConsumo().iterator().next();
			Collection<ContratoPontoConsumoItemFaturamento> itensFaturamento = contratoPontoConsumo
					.getListaContratoPontoConsumoItemFaturamento();
			if (itensFaturamento.iterator().hasNext()) {
				ContratoPontoConsumoItemFaturamento modalidade = itensFaturamento.iterator().next();
				if (modalidade.getTarifa() != null) {
					modalidadeTarifaria = modalidade.getTarifa().getDescricao();
				}
			}
		}
		return modalidadeTarifaria;
	}

	/**
	 * Segmento do Ponto de Conusmo
	 * 
	 * @return segmentoPontoConsumo
	 */
	public String getSegmentoPontoConsumo() {
		String segmento = "";
		if (contrato.getListaContratoPontoConsumo().iterator().hasNext()) {
			ContratoPontoConsumo contratoPontoConsumo = contrato.getListaContratoPontoConsumo().iterator().next();
			PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
			if (pontoConsumo.getSegmento() != null) {
				segmento = pontoConsumo.getSegmento().getDescricao();
			}
		}
		return segmento;
	}

	/**
	 * Telefone Principal
	 * 
	 * @return telefonePrincipal
	 */
	public String getTelefonePrincipal() {
		String telefone = "";
		Cliente cliente = this.contrato.getClienteAssinatura();
		Collection<ClienteFone> telefones = cliente.getFones();
		Optional<ClienteFone> optTelefone = telefones.stream().filter(t -> t.getIndicadorPrincipal()).findFirst();
		if (optTelefone.isPresent()) {
			telefone = formatarTelefone(optTelefone.get());
		}
		return telefone;
	}

	/**
	 * Nome do Responsavel
	 * 
	 * @return nome
	 */
	public String getClienteResponsavelNome() {

		String retorno = "";

		Collection<ContratoCliente> listaContratoCliente = this.contrato.getListaContratoCliente();

		if (!Util.isNullOrEmpty(listaContratoCliente)) {
			for (ContratoCliente contratoCliente : listaContratoCliente) {
				retorno = contratoCliente.getCliente().getNome();
			}
			return retorno;
		}

		return retorno;
	}

	/**
	 * CPF responsavel
	 * 
	 * @return cpf
	 */
	public String getClienteResponsavelCpf() {
		String retorno = "";

		Collection<ContratoCliente> listaContratoCliente = this.contrato.getListaContratoCliente();
		if (!Util.isNullOrEmpty(listaContratoCliente)) {

			for (ContratoCliente contratoCliente : listaContratoCliente) {
				retorno = contratoCliente.getCliente().getCpfFormatado();
			}
			return retorno;
		}

		return retorno;
	}

	/**
	 * Data Atual
	 * 
	 * @return dataAtual
	 */
	public String getDataAtual() {
		StringBuilder dataCorrente = new StringBuilder();
		dataCorrente.append(Util.getDataCorrente().substring(0, 2));
		dataCorrente.append("/");
		dataCorrente.append(Util.getDataCorrente().substring(2, 4));
		dataCorrente.append("/");
		dataCorrente.append(Util.getDataCorrente().substring(4, 8));
		return dataCorrente.toString();
	}

	/**
	 * Data de Vencimento do Contrato
	 * 
	 * @return dataVencimento
	 */
	public String getDataVencimento() {
		StringBuilder dataFormatada = new StringBuilder();
		dataFormatada.append(new DateTime(this.contrato.getDataVencimentoObrigacoes()).getDayOfMonth());
		dataFormatada.append("/");
		dataFormatada.append(new DateTime(this.contrato.getDataVencimentoObrigacoes()).getMonthOfYear());
		dataFormatada.append("/");
		dataFormatada.append((new DateTime(this.contrato.getDataVencimentoObrigacoes()).getYear()));

		return dataFormatada.toString();
	}

	/**
	 * Descricao Ponto de Consumo
	 * 
	 * @return descricaoPonto de Consumo
	 */
	public StringBuilder getListaDescPontoConsumo() throws GGASException {

		StringBuilder listaPontoConsumo = new StringBuilder();
		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {

			ContratoPontoConsumo contratoPontoConsumo = this.getContratoPontoConsumoUnico();
			if (contratoPontoConsumo != null) {
				PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
				if (pontoConsumo != null && StringUtils.isNotEmpty(pontoConsumo.getDescricao())) {
					listaPontoConsumo.append(pontoConsumo.getDescricao());
				}
			}
		}
		return listaPontoConsumo;
	}

	/**
	 * Nome do Representante legal/Cliente
	 * 
	 * @return nome
	 */

	public String getNome() {
		String retorno = "";
		String tipo = "REPRESENTANTE LEGAL";

		Collection<ContatoCliente> listaContratoCliente = this.contrato.getClienteAssinatura().getContatos();

		if (!Util.isNullOrEmpty(listaContratoCliente)) {
			for (ContatoCliente contratoCliente : listaContratoCliente) {
				if (contratoCliente.getTipoContato().getDescricao().equals(tipo)) {
					retorno = contratoCliente.getNome();
					break;
				} else {
					retorno = contrato.getClienteAssinatura().getNome();
				}
			}
			return retorno;
		} else {
			retorno = contrato.getClienteAssinatura().getNome();
		}
		return retorno;
	}

	/**
	 * CPF/CNPJ Representante Legal/Cliente
	 * 
	 * @return cadastro
	 */
	public String getCadastroCliente() {
		String retorno = "";
		String tipo = "REPRESENTANTE LEGAL";

		Collection<ContatoCliente> listaContratoCliente = this.contrato.getClienteAssinatura().getContatos();
		if (!Util.isNullOrEmpty(listaContratoCliente)) {
			for (ContatoCliente contratoCliente : listaContratoCliente) {
				if (contratoCliente.getTipoContato().getDescricao().equals(tipo)) {
					if (StringUtils.isNotEmpty(contratoCliente.getCpf())) {
						retorno = contratoCliente.getCpf();
						// break;
					} else {
						retorno = "";
					}
				} else {
					retorno = this.contrato.getClienteAssinatura().getCnpjFormatado();
				}
			}
		} else {
			retorno = this.contrato.getClienteAssinatura().getCnpjFormatado();
		}
		return retorno;
	}

	/**
	 * Lista Descricao do Ponto de Consumo
	 * 
	 * @return lista Descricao Ponto de Consumo
	 */
	public String getListaDescricaoPontoConsumo() {
		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {

			StringBuilder retorno = new StringBuilder();
			Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
				if (StringUtils.isNotEmpty(contratoPontoConsumo.getPontoConsumo().getDescricao())) {
					retorno.append(contratoPontoConsumo.getPontoConsumo().getDescricao()).append("<br />");
				}
			}
			return retorno.toString();
		} else {
			return ("-<br />");
		}
	}

	/**
	 * Lista Endereco do Ponto de Consumo
	 * 
	 * @return lista Endereco Ponto de Consumo
	 */
	public String getListaEnderecoPontoConsumo() {
		if (!Util.isNullOrEmpty(this.contrato.getListaContratoPontoConsumo())) {

			StringBuilder retorno = new StringBuilder();
			Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
				if (StringUtils.isNotEmpty(contratoPontoConsumo.getPontoConsumo().getEnderecoFormatado())) {
					retorno.append(contratoPontoConsumo.getPontoConsumo().getEnderecoLogradouro()).append(", ")
							.append(contratoPontoConsumo.getPontoConsumo().getEnderecoFormatadoBairro()).append(", ")
							.append(contratoPontoConsumo.getPontoConsumo().getModalidadeUso().getDescricao())
							.append("<br />");
				}
			}
			return retorno.toString();
		} else {
			return ("-<br />");
		}
	}

	/**
	 * Lista QDC do Ponto de Consumo
	 * 
	 * @return lista QDC Ponto Consumo
	 */
	public String getListaQDCPontoConsumo() {
		StringBuilder retorno = new StringBuilder();
		DecimalFormat df = new DecimalFormat("#,###.00");

		Collection<ContratoPontoConsumo> listacontratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listacontratoPontoConsumo) {
			int contador = 0;
			List<ContratoPontoConsumoModalidadeQDCImpl> listaContratoPontoConsumoModalidadeQDC = ServiceLocator
					.getInstancia().getControladorContrato()
					.obterModalidadeQDC(contratoPontoConsumo.getChavePrimaria());
			for (ContratoPontoConsumoModalidadeQDCImpl contratoPontoConsumoQDC : listaContratoPontoConsumoModalidadeQDC) {
				if (contratoPontoConsumoQDC.getMedidaVolume() != null && contador < 1) {
					retorno.append(df.format(contratoPontoConsumoQDC.getMedidaVolume())).append("<br />");
					contador++;
				} else if (contratoPontoConsumoQDC.getMedidaVolume() == null) {
					retorno.append("-");
				}
			}
		}
		return retorno.toString();
	}

	/**
	 * Lista de Data de Vigencia QDC do Ponto de Consumo
	 * 
	 * @return lista Data Vigencia QDC Ponto Consumo
	 */
	public String getListaDataVigenciaQDCPontoConsumo() {
		StringBuilder retorno = new StringBuilder();
		DateFormat ano = new SimpleDateFormat("yyyy");

		Collection<ContratoPontoConsumo> listacontratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listacontratoPontoConsumo) {
			int contador = 0;
			List<ContratoPontoConsumoModalidadeQDCImpl> listaContratoPontoConsumoModalidadeQDC = ServiceLocator
					.getInstancia().getControladorContrato()
					.obterModalidadeQDC(contratoPontoConsumo.getChavePrimaria());

			for (ContratoPontoConsumoModalidadeQDCImpl contratoPontoConsumoQDC : listaContratoPontoConsumoModalidadeQDC) {
				if (contratoPontoConsumoQDC.getDataVigencia() != null && contador < 1) {
					retorno.append(ano.format(contratoPontoConsumoQDC.getDataVigencia())).append("<br />");
					contador++;
				} else if (contratoPontoConsumoQDC.getMedidaVolume() == null) {
					retorno.append("-");
				}
			}
		}
		return retorno.toString();
	}

	/**
	 * Lista Pressao Minima de Fornecimento
	 * 
	 * @return Lista Pressao Minima
	 */
	public String getListaPressaoMinFornecimento() {
		StringBuilder retorno = new StringBuilder();

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			if (contratoPontoConsumo.getMedidaPressaoMinima() != null) {
				retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
						contratoPontoConsumo.getMedidaPressaoMinima(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS))
						.append("<br />");
			} else {
				retorno.append("-<br />");
			}
		}
		return retorno.toString();
	}

	/**
	 * Lista Pressao Maxima de Fornecimento
	 * 
	 * @return lista Pressao Maxima
	 */
	public String getListaPressaoMaxFornecimento() {
		StringBuilder retorno = new StringBuilder();

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			if (contratoPontoConsumo.getMedidaPressaoMaxima() != null) {
				retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
						contratoPontoConsumo.getMedidaPressaoMaxima(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS))
						.append("<br />");
			} else {
				retorno.append("-<br />");
			}
		}
		return retorno.toString();
	}

	/**
	 * Lista Vazao Maxima de Fornecimento
	 * 
	 * @return lista Vazao Maxima
	 */
	public String getListaVazaoMaxFornecimento() {
		StringBuilder retorno = new StringBuilder();

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {
				retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
						contratoPontoConsumo.getMedidaVazaoMaximaInstantanea(), Constantes.LOCALE_PADRAO,
						CASAS_DECIMAIS)).append("<br />");
			} else {
				retorno.append("-<br />");
			}
		}
		return retorno.toString();
	}

	/**
	 * Data da Assinatura do Contrato Por Extenso
	 *
	 * @return dataAssinaturaContratoPorExtenso
	 */
	public String getDataAssinaturaContratoPorExtenso() {
		if (this.contrato.getDataAssinatura() != null) {
			DateFormat dataPorExtenso = new SimpleDateFormat("d 'de' MMMM 'de' yyyy");
			return dataPorExtenso.format(this.contrato.getDataAssinatura());
		} else {
			return "";
		}
	}

	/**
	 * Ano Inicial e Final do Contrato
	 *
	 * @return anoInicioETerminoContrato
	 */
	public String getAnoInicioETerminoContrato() {
		if (this.contrato.getDataAssinatura() != null && this.contrato.getDataVencimentoObrigacoes() != null) {
			DateFormat ano = new SimpleDateFormat("yyyy");
			return ano.format(this.contrato.getDataAssinatura()) + " a "
					+ ano.format(this.contrato.getDataVencimentoObrigacoes());
		} else {
			return "";
		}
	}

	/**
	 * Lista Regime de Operação (dia/semana)
	 *
	 * @return listaRegimeOperacao
	 * @throws GGASException
	 */
	public String getListaRegimeOperacao() throws GGASException {
		StringBuilder retorno = new StringBuilder();
		ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

		EntidadeConteudo regimeConsumoEntidade = Fachada.getInstancia()
				.obterEntidadeConteudo(contratoPontoConsumo.getRegimeConsumo().getChavePrimaria());
		for (ContratoPontoConsumo contratos : contrato.getListaContratoPontoConsumo()) {
			contratos.setRegimeConsumo(regimeConsumoEntidade);

			if (contratos.getRegimeConsumo() != null) {
				retorno.append(contratos.getRegimeConsumo().getDescricao()).append("<br />");
			} else {
				retorno.append("-");
			}
		}
		return retorno.toString();
	}

	/**
	 * Fornecimento Minimo Diario
	 *
	 * @return fornecimento Minimo Diario
	 */
	public String getFornecimentoMinDiario() throws GGASException {
		StringBuilder retorno = new StringBuilder();

		ContratoPontoConsumo contratoPontoConsumoUnico = this.getContratoPontoConsumoUnico();
		if (contratoPontoConsumoUnico != null && contratoPontoConsumoUnico.getMedidaFornecimentoMinDiaria() != null) {
			CurrencyWriter cw = CurrencyWriter.getInstance();
			cw.setMoeda(false);

			retorno.append(contratoPontoConsumoUnico.getMedidaFornecimentoMinDiaria())
					.append(contratoPontoConsumoUnico.getUnidadeFornecimentoMinDiaria().getDescricaoAbreviada())
					.append("(").append(cw.write(contratoPontoConsumoUnico.getMedidaFornecimentoMinDiaria()))
					.append(contratoPontoConsumoUnico.getUnidadeFornecimentoMinDiaria().getDescricao().toLowerCase())
					.append(")");
		} else {
			return "";
		}
		return retorno.toString();
	}

	/**
	 * Valor do Investimento
	 * 
	 * @return valorInvestimento
	 */
	public String getValorInvestimento() {
		StringBuilder retorno = new StringBuilder();
		DecimalFormat df = new DecimalFormat("#,###.00");
		Contrato contrato = this.contrato;

		if (contrato.getValorInvestimento() != null) {
			CurrencyWriter cw = CurrencyWriter.getInstance();
			cw.setMoeda(true);

			retorno.append(df.format(contrato.getValorInvestimento())).append(" (")
					.append(cw.write(contrato.getValorInvestimento())).append(")");
		} else {
			return "";
		}
		return retorno.toString();
	}

	/**
	 * Lista de Fornecimento Maximo Diario
	 *
	 * @return lista de fornecimento Maximo Diario
	 */
	public String getListaFornecimentoMaxDiario() {
		StringBuilder retorno = new StringBuilder();

		Collection<ContratoPontoConsumo> contratoPontoConsumoUnico = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contrato : contratoPontoConsumoUnico) {
			if (contrato.getMedidaFornecimentoMaxDiaria() != null) {
				retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
						contrato.getMedidaFornecimentoMaxDiaria(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS))
						.append("<br />");
			} else {
				return ("-<br />");
			}
		}
		return retorno.toString();
	}

	/**
	 * Data de QDC do Contrato
	 * 
	 * @return dataQDCContrato
	 */
	public String getDataQDCContrato() {
		StringBuilder retorno = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		boolean isVazio = true;

		List<ContratoQDC> contratoQDCs = new ArrayList<>(this.contrato.getListaContratoQDC());
		for (ContratoQDC contratoQDC : contratoQDCs) {
			if (contratoQDC.getData() != null) {
				Collections.sort(contratoQDCs, Comparator.comparing(ContratoQDC::getData));
				cal.setTime(contratoQDC.getData());
				retorno.append(cal.get(Calendar.YEAR));
				isVazio = false;
			}
		}
		if (isVazio) {
			return "";
		} else {
			return retorno.toString();
		}
	}

	/**
	 * Medida Volume de QDC do Contrato
	 * 
	 * @return medidaVolume
	 */
	public String getMedidaVolumeQDCContrato() {
		StringBuilder retorno = new StringBuilder();
		boolean isVazio = true;

		List<ContratoQDC> contratoQDCs = new ArrayList<>(this.contrato.getListaContratoQDC());
		for (ContratoQDC contratoQDC : contratoQDCs) {
			if (contratoQDC.getMedidaVolume() != null) {
				retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
						contratoQDC.getMedidaVolume(), Constantes.LOCALE_PADRAO, CASAS_DECIMAIS));
				isVazio = false;
			}
		}
		if (isVazio) {
			return "";
		} else {
			return retorno.toString();
		}
	}

	/**
	 * Nome e CPF do Diretor Presidente
	 * 
	 * @return NomeCPF
	 */
	public String getDiretorPresidente() {
		StringBuilder retorno = new StringBuilder();
		String tipo = "DIRETOR PRESIDENTE";

		if (!Util.isNullOrEmpty(this.contrato.getClienteAssinatura().getContatos())) {
			Collection<ContatoCliente> listaContatosClientes = this.contrato.getClienteAssinatura().getContatos();
			for (ContatoCliente lista : listaContatosClientes) {
				if (lista.getTipoContato().getDescricao().equals(tipo)) {
					retorno.append(formatarTextoStrong(lista.getNome())).append(", inscrito no CPF nº ")
							.append(formatarTextoStrong(lista.getCpf()));
				}
			}
		} else {
			return "";
		}
		return retorno.toString();
	}

	/**
	 * Nome e CPF do Diretor Comercial
	 * 
	 * @return NomeCPF
	 */
	public String getDiretorComercial() {
		StringBuilder retorno = new StringBuilder();
		String tipo = "DIRETOR COMERCIAL";

		if (!Util.isNullOrEmpty(this.contrato.getClienteAssinatura().getContatos())) {
			Collection<ContatoCliente> listaContatosClientes = this.contrato.getClienteAssinatura().getContatos();
			for (ContatoCliente lista : listaContatosClientes) {
				if (lista.getTipoContato().getDescricao().equals(tipo)) {
					retorno.append(formatarTextoStrong(lista.getNome())).append(", inscrito no CPF nº ")
							.append(formatarTextoStrong(lista.getCpf()));
				}
			}
		} else {
			return "";
		}
		return retorno.toString();
	}

	/**
	 * Data de QDC
	 * 
	 * @return DATAQDC
	 * @throws GGASException
	 */
	public String getDataQDC() {
		StringBuilder retorno = new StringBuilder();

		if (!Util.isNullOrEmpty(this.contrato.getListaContratoQDC())) {
			retorno.append(getDataQDCContrato());
		} else {
			retorno.append(getListaDataVigenciaQDCPontoConsumo()).append("<br />");
		}
		return retorno.toString();
	}

	/**
	 * Medida Volume QDC
	 * 
	 * @return MedidaVolumeQDC
	 * @throws GGASException
	 */
	public String getMedidaVolumeQDC() {
		StringBuilder retorno = new StringBuilder();

		if (!Util.isNullOrEmpty(this.contrato.getListaContratoQDC())) {
			retorno.append(getMedidaVolumeQDCContrato());
		} else {
			retorno.append(getListaQDCPontoConsumo()).append("<br />");
		}
		return retorno.toString();
	}

	private String formatarTextoStrong(String texto) {
		if (StringUtils.isNotBlank(texto)) {
			String strong = "<strong>$texto</strong>";
			return strong.replace("$texto", texto);
		}
		return StringUtils.EMPTY;
	}

	/**
	 * Endereco do Ponto de Entrega
	 * 
	 * @return endereco
	 * @throws GGASException
	 */
	public String getEnderecoPontoEntrega() throws GGASException {
		ContratoPontoConsumo pontoConsumo = getContratoPontoConsumoUnico();
		if (pontoConsumo != null && pontoConsumo.getCep() != null && pontoConsumo.getCep().getNomeMunicipio() != null) {
			return pontoConsumo.getEnderecoFormatado();
		}
		return "";
	}

	/**
	 * Lista Vazao Mínima de Fornecimento
	 * 
	 * @return lista Vazao Minima
	 */
	public String getListaVazaoMinFornecimento() {
		StringBuilder retorno = new StringBuilder();

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			if (contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() != null) {
				retorno.append(Util.converterCampoValorDecimalParaStringComCasasDecimais("",
						contratoPontoConsumo.getMedidaVazaoMinimaInstantanea(), Constantes.LOCALE_PADRAO,
						CASAS_DECIMAIS)).append("<br />");
			} else {
				retorno.append("-<br />");
			}
		}
		return retorno.toString();
	}

	/**
	 * Lista QDC de Modalidade Firme Flexivel
	 * 
	 * @return lista QDC Modalidade Firme Flexivel
	 */
	public String getListaQDCModalidadeFirmeFlexivel() {
		DecimalFormat df = new DecimalFormat("#,###.00");
		boolean cont = false;
		String modalidade = "Firme Inflexível";

		Collection<ContratoPontoConsumo> listacontratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listacontratoPontoConsumo) {
			List<ContratoPontoConsumoModalidadeQDCImpl> listaContratoPontoConsumoModalidadeQDC = ServiceLocator
					.getInstancia().getControladorContrato().obterModalidade(contratoPontoConsumo.getChavePrimaria());

			for (ContratoPontoConsumoModalidadeQDCImpl contratoPontoConsumoQDC : listaContratoPontoConsumoModalidadeQDC) {
				if (contratoPontoConsumoQDC.getContratoPontoConsumoModalidade().getContratoModalidade().getDescricao()
						.equals(modalidade) && !cont) {
					cont = true;
					return (df.format(contratoPontoConsumoQDC.getMedidaVolume()) + ("<br>"));
				}
			}
		}
		return "";
	}

	/**
	 * Lista QDC de Modalidade Interruptivel
	 * 
	 * @return lista QDC Modalidade Interruptivel
	 */
	public String getListaQDCModalidadeInterruptivel() {
		DecimalFormat df = new DecimalFormat("#,###.00");
		boolean cont = false;
		String modalidade = "Interruptível";

		Collection<ContratoPontoConsumo> listacontratoPontoConsumo = this.contrato.getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listacontratoPontoConsumo) {
			List<ContratoPontoConsumoModalidadeQDCImpl> listaContratoPontoConsumoModalidadeQDC = ServiceLocator
					.getInstancia().getControladorContrato().obterModalidade(contratoPontoConsumo.getChavePrimaria());

			for (ContratoPontoConsumoModalidadeQDCImpl contratoPontoConsumoQDC : listaContratoPontoConsumoModalidadeQDC) {
				if (contratoPontoConsumoQDC.getContratoPontoConsumoModalidade().getContratoModalidade().getDescricao()
						.equals(modalidade) && !cont) {
					cont = true;
					return (df.format(contratoPontoConsumoQDC.getMedidaVolume()) + ("<br>"));
				}
			}
		}
		return "";
	}

	/**
	 * Data do Periodo de Vigencia do Contrato
	 *
	 * @return dataPeriodoVigenciaContrato
	 */
	public String getPeriodoVigenciaContrato() {
		DateFormat ano = new SimpleDateFormat("yyyy");

		if (this.contrato.getDataAssinatura() != null) {
			return ano.format(this.contrato.getDataAssinatura()) + " a "
					+ ano.format(this.contrato.getDataVencimentoObrigacoes());
		} else {
			return "";
		}
	}
	
	public String getNumeroInscricaoEstadual() {
		if(this.contrato.getClienteAssinatura() != null && this.contrato.getClienteAssinatura().getInscricaoEstadual() != null) {
			return this.contrato.getClienteAssinatura().getInscricaoEstadual();
		}
		return "";
	}
	
	public String getNumeroInscricaoMunicipal() {
		if(this.contrato.getClienteAssinatura() != null && this.contrato.getClienteAssinatura().getInscricaoMunicipal() != null) {
			return this.contrato.getClienteAssinatura().getInscricaoMunicipal();
		}
		return "";
	}
	
	public String getNumeroTelefone() {
		if(this.contrato.getClienteAssinatura() != null && !this.contrato.getClienteAssinatura().getFones().isEmpty()) {
			ClienteFone clienteFone = this.contrato.getClienteAssinatura().getFones().stream().filter(p -> p.getIndicadorPrincipal()).findFirst().orElse(null);
			
			if(clienteFone != null) {
				return "("+clienteFone.getCodigoDDD()+") "+clienteFone.getNumero();
			}
			
		}
		return "";
	}
	
	public String getQuantidadeMaximaDiaria() {

		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if (contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() != null) {
				return contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() + " "
						+ contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria().getDescricaoAbreviada();
			}
		}

		return "";
	}
	
	public String getVazaoMinimaInstantanea() {

		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if (contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() != null) {
				return contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() + " "
						+ contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea().getDescricaoAbreviada();
			}
		}

		return "";
	}
	
	public String getVazaoMaximaInstantanea() {

		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if (contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() != null) {
				return contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() + " "
						+ contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea().getDescricaoAbreviada();
			}
		}

		return "";
	}
	
	public String getPressaoEntrega() {
		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if (contratoPontoConsumo.getMedidaPressao() != null) {
				return contratoPontoConsumo.getMedidaPressao() + " "
						+ contratoPontoConsumo.getUnidadePressao().getDescricaoAbreviada();
				
			}
		}

		return "";
	}
	
	public String getTarifaAplicada() {
		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if(!contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().isEmpty()) {
				ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento = contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento().iterator().next();
                return contratoPontoConsumoItemFaturamento.getTarifa().getDescricao();
			}
		}

		return "";
	}
	
	public String getNomeImovel() {
		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if (contratoPontoConsumo.getPontoConsumo() != null && contratoPontoConsumo.getPontoConsumo().getImovel() != null) {
				return contratoPontoConsumo.getPontoConsumo().getImovel().getNome();
			}
		}

		return "";
	}
	
	public String getNomePontoConsumo() {
		if (!this.contrato.getListaContratoPontoConsumo().isEmpty()) {
			ContratoPontoConsumo contratoPontoConsumo = this.contrato.getListaContratoPontoConsumo().iterator().next();

			if (contratoPontoConsumo.getPontoConsumo() != null) {
				return contratoPontoConsumo.getPontoConsumo().getDescricao();
			}
		}

		return "";
	}
	
	
	public String getNomeGestorContrato() {
		if(!this.contrato.getListaContratoCliente().isEmpty()) {
			ContratoCliente contratoCliente = this.contrato.getListaContratoCliente().iterator().next();
			Cliente cliente = contratoCliente.getCliente();
			
			return cliente.getNome();
		}
		
		return "";
	}
	
	public String getPrazoVigencia() {
		return this.contrato.getPrazoVigencia() != null ? String.valueOf(this.contrato.getPrazoVigencia()) + " meses" : "Não Aplicável";
	}
	
	public String getIncentivosComerciais() {
		return this.contrato.getIncentivosComerciais()!= null ? Util.converterBigDecimalValorMonetario(this.contrato.getIncentivosComerciais()) : "Não Aplicável";
	}
	
	public String getIncentivosInfraestrutura() {
		return this.contrato.getIncentivosInfraestrutura()!= null ? Util.converterBigDecimalValorMonetario(this.contrato.getIncentivosInfraestrutura()) : "Não Aplicável";
	}

	
	public String getNomeAnexo() {
		Collection<ContratoAnexo> contratoAnexo = contrato.getAnexos();
		
	    if (!contratoAnexo.isEmpty()) {
	        StringBuilder sb = new StringBuilder();
	        
	        for (ContratoAnexo anexo : contratoAnexo) {
	            if (sb.length() > 0) {
	                sb.append(", ");
	            }
	            sb.append(anexo.getDescricaoAnexo());
	        }
	        
	        return sb.toString();
	    }
		
		return "";
	}

}
