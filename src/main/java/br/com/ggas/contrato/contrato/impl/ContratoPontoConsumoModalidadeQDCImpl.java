/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelos atributos e métodos relacionados a modalidade 
 * da Quantidade Diária Contratada (QDC) associada ao Ponto de Consumo vinculado ao Contrato. 
 *
 */
public class ContratoPontoConsumoModalidadeQDCImpl extends EntidadeNegocioImpl implements ContratoPontoConsumoModalidadeQDC {

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6956361005935851467L;

	private Date dataVigencia;

	private ContratoPontoConsumoModalidade contratoPontoConsumoModalidade;

	private BigDecimal medidaVolume;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidadeQDC
	 * #getDataVigencia()
	 */
	@Override
	public Date getDataVigencia() {
		Date data = null;
		if (dataVigencia != null) {
			data = (Date) dataVigencia.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidadeQDC #setDataVigencia(java.util.Date)
	 */
	@Override
	public void setDataVigencia(Date dataVigencia) {
		if (dataVigencia != null) {
			this.dataVigencia = (Date) dataVigencia.clone();
		} else {
			this.dataVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidadeQDC
	 * #getMedidaVolume()
	 */
	@Override
	public BigDecimal getMedidaVolume() {

		return medidaVolume;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidadeQDC
	 * #setMedidaVolume(java.lang.Integer)
	 */
	@Override
	public void setMedidaVolume(BigDecimal medidaVolume) {

		this.medidaVolume = medidaVolume;
	}

	/**
	 * @return the contratoPontoConsumoModalidade
	 */
	@Override
	public ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade() {

		return contratoPontoConsumoModalidade;
	}

	/**
	 * @param contratoPontoConsumoModalidade
	 *            the
	 *            contratoPontoConsumoModalidade
	 *            to set
	 */
	@Override
	public void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {

		this.contratoPontoConsumoModalidade = contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC#equalsNegocio(br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidadeQDC)
	 */
	@Override
	public boolean equalsNegocio(ContratoPontoConsumoModalidadeQDC obj) {

		if (obj != null
						&& this.contratoPontoConsumoModalidade != null
						&& this.contratoPontoConsumoModalidade.getChavePrimaria() > 0
						&& obj.getContratoPontoConsumoModalidade() != null
						&& obj.getContratoPontoConsumoModalidade().getChavePrimaria() == this.contratoPontoConsumoModalidade
										.getChavePrimaria() && this.dataVigencia != null && this.dataVigencia.equals(obj.getDataVigencia())) {
			return true;
		}

		return false;
	}

}
