/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Map;

import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelos atributos e métodos relacionados com a Situação do Contrato.
 * 
 */
public class SituacaoContratoImpl extends EntidadeNegocioImpl implements SituacaoContrato {

	/** serialVersionUID */
	private static final long serialVersionUID = 2422882976736915362L;

	private String descricao;

	private boolean padrao;

	private boolean permiteExclusao;

	private boolean permiteAditamento;

	private boolean faturavel;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.SituacaoContrato
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.SituacaoContrato
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #isPadrao()
	 */
	@Override
	public boolean isPadrao() {

		return padrao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #setPadrao(boolean)
	 */
	@Override
	public void setPadrao(boolean padrao) {

		this.padrao = padrao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #isPermiteExclusao()
	 */
	@Override
	public boolean isPermiteExclusao() {

		return permiteExclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #setPermiteExclusao(boolean)
	 */
	@Override
	public void setPermiteExclusao(boolean permiteExclusao) {

		this.permiteExclusao = permiteExclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #isPermiteAditamento()
	 */
	@Override
	public boolean isPermiteAditamento() {

		return permiteAditamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #setPermiteAditamento(boolean)
	 */
	@Override
	public void setPermiteAditamento(boolean permiteAditamento) {

		this.permiteAditamento = permiteAditamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #isFaturavel()
	 */
	@Override
	public boolean isFaturavel() {

		return faturavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.SituacaoContrato
	 * #setFaturavel(boolean)
	 */
	@Override
	public void setFaturavel(boolean faturavel) {

		this.faturavel = faturavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDadosAuditoria()
	 */
	@Override
	public Map<String, Object> validarDadosAuditoria() {

		return null;
	}
}
