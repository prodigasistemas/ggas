/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pelos atributos e métodos relacionados 
 * ao Item de Faturamento associado ao Ponto de Consumo vinculado ao Contrato. 
 *
 */
public class ContratoPontoConsumoItemFaturamentoImpl extends EntidadeNegocioImpl implements ContratoPontoConsumoItemFaturamento {

	private static final long serialVersionUID = -2573927557145699050L;

	private ContratoPontoConsumo contratoPontoConsumo;

	private Integer numeroDiaVencimento;

	private EntidadeConteudo itemFatura;

	private EntidadeConteudo faseReferencia;

	private EntidadeConteudo opcaoFaseReferencia;

	private Boolean vencimentoDiaUtil;

	private Tarifa tarifa;

	private Boolean indicadorDepositoIdentificado;

	private EntidadeConteudo dataReferenciaCambial;

	private EntidadeConteudo diaCotacao;

	private BigDecimal percminimoQDC;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemVencimento
	 * #getContratoPontoConsumo()
	 */
	@Override
	public ContratoPontoConsumo getContratoPontoConsumo() {

		return contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemVencimento
	 * #setContratoPontoConsumo
	 * (br.com.ggas.contrato
	 * .contrato.ContratoPontoConsumo)
	 */
	@Override
	public void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) {

		this.contratoPontoConsumo = contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #getNumeroDiaVencimento()
	 */
	@Override
	public Integer getNumeroDiaVencimento() {

		return numeroDiaVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #setNumeroDiaVencimento(java.lang.Integer)
	 */
	@Override
	public void setNumeroDiaVencimento(Integer numeroDiaVencimento) {

		this.numeroDiaVencimento = numeroDiaVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #getItemFatura()
	 */
	@Override
	public EntidadeConteudo getItemFatura() {

		return itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #setItemFatura
	 * (br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setItemFatura(EntidadeConteudo itemFatura) {

		this.itemFatura = itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #getFaseReferencia()
	 */
	@Override
	public EntidadeConteudo getFaseReferencia() {

		return faseReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #setFaseReferencia
	 * (br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setFaseReferencia(EntidadeConteudo faseReferencia) {

		this.faseReferencia = faseReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #getOpcaoFaseReferencia()
	 */
	@Override
	public EntidadeConteudo getOpcaoFaseReferencia() {

		return opcaoFaseReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoItemVencimento
	 * #setOpcaoFaseReferencia
	 * (br.com.ggas.contrato
	 * .contrato.EntidadeConteudo)
	 */
	@Override
	public void setOpcaoFaseReferencia(EntidadeConteudo opcaoFaseReferencia) {

		this.opcaoFaseReferencia = opcaoFaseReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemVencimento
	 * #getVencimentoDiaUtil()
	 */
	@Override
	public Boolean getVencimentoDiaUtil() {

		return vencimentoDiaUtil;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemVencimento
	 * #setVencimentoDiaUtil(java.lang.Boolean)
	 */
	@Override
	public void setVencimentoDiaUtil(Boolean vencimentoDiaUtil) {

		this.vencimentoDiaUtil = vencimentoDiaUtil;
	}

	/**
	 * @return the tarifa
	 */
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	/**
	 * @param tarifa
	 *            the tarifa to set
	 */
	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/**
	 * @return the indicadorDepositoIdentificado
	 */
	@Override
	public Boolean getIndicadorDepositoIdentificado() {

		return indicadorDepositoIdentificado;
	}

	/**
	 * @param indicadorDepositoIdentificado
	 *            the
	 *            indicadorDepositoIdentificado to
	 *            set
	 */
	@Override
	public void setIndicadorDepositoIdentificado(Boolean indicadorDepositoIdentificado) {

		this.indicadorDepositoIdentificado = indicadorDepositoIdentificado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/**
	 * @return the dataReferenciaCambial
	 */
	@Override
	public EntidadeConteudo getDataReferenciaCambial() {

		return dataReferenciaCambial;
	}

	/**
	 * @param dataReferenciaCambial
	 *            the dataReferenciaCambial to set
	 */
	@Override
	public void setDataReferenciaCambial(EntidadeConteudo dataReferenciaCambial) {

		this.dataReferenciaCambial = dataReferenciaCambial;
	}

	/**
	 * @return the diaCotacao
	 */
	@Override
	public EntidadeConteudo getDiaCotacao() {

		return diaCotacao;
	}

	/**
	 * @param diaCotacao
	 *            the diaCotacao to set
	 */
	@Override
	public void setDiaCotacao(EntidadeConteudo diaCotacao) {

		this.diaCotacao = diaCotacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento#equalsNegocio(br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemFaturamento)
	 */
	@Override
	public boolean equalsNegocio(ContratoPontoConsumoItemFaturamento obj) {

		if (obj != null && this.contratoPontoConsumo != null && this.contratoPontoConsumo.getChavePrimaria() > 0
						&& obj.getContratoPontoConsumo() != null
						&& obj.getContratoPontoConsumo().getChavePrimaria() == this.contratoPontoConsumo.getChavePrimaria()
						&& this.itemFatura != null && this.itemFatura.getChavePrimaria() > 0 && obj.getItemFatura() != null
						&& obj.getItemFatura().getChavePrimaria() == this.itemFatura.getChavePrimaria()) {
			return true;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemFaturamento
	 * #getPercminimoQDC()
	 */
	@Override
	public BigDecimal getPercminimoQDC() {

		return percminimoQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoItemFaturamento
	 * #setPercminimoQDC(java.math.BigDecimal)
	 */
	@Override
	public void setPercminimoQDC(BigDecimal percminimoQDC) {

		this.percminimoQDC = percminimoQDC;
	}
}
