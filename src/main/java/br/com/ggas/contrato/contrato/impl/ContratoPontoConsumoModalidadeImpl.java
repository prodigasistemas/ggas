/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.LazyInitializationException;

import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pelos atributos relacionados a modalidade 
 * associada ao Ponto de Consumo vinculado ao Contrato. 
 *
 */
public class ContratoPontoConsumoModalidadeImpl extends EntidadeNegocioImpl implements ContratoPontoConsumoModalidade {

	private static final long serialVersionUID = -2553711417645992367L;

	private static final Logger LOG = Logger.getLogger(ContratoPontoConsumoModalidadeImpl.class);
	
	private ContratoPontoConsumo contratoPontoConsumo; 

	private ContratoModalidade contratoModalidade;

	private Boolean indicadorProgramacaoConsumo;

	private Boolean indicadorQDSMaiorQDC;

	private Integer diasAntecedenciaQDS;

	private Date prazoRevisaoQDC;

	private Integer mesesQDS;

	private Boolean indicadorRetiradaAutomatica;

	private Boolean indicadorConfirmacaoAutomaticaQDS;

	private Date dataInicioRetiradaQPNR;

	private Date dataFimRetiradaQPNR;

	private Integer horaLimiteProgramacaoDiaria;

	private Integer horaLimiteProgIntradiaria;

	private Integer horaLimiteAceitacaoDiaria;

	private Integer horaLimiteAceitIntradiaria;

	private EntidadeConteudo consumoRefSOPAnual; // Referencia
													// de
													// consumo
													// Ship
													// Or
													// Pay
													// anual

	private Integer margemVarSOPAnual;

	// Percentual
	// da
	// margem
	// de
	// variação
	// Ship Or
	// Pay
	// anual

	private BigDecimal percentualQDCContratoQPNR;

	private BigDecimal percentualMinimoQDCContratoQPNR;

	private BigDecimal percentualQDCFimContratoQPNR;

	private BigDecimal percentualVarSuperiorQDC;

	private Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC = 
					new HashSet<>();

	private Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = new HashSet<>();

	private Integer anosValidadeRetiradaQPNR;

	private BigDecimal percentualQNR;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getContratoPontoConsumo()
	 */
	@Override
	public ContratoPontoConsumo getContratoPontoConsumo() {

		return contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setContratoPontoConsumo
	 * (br.com.ggas.contrato
	 * .contrato.ContratoPontoConsumo)
	 */
	@Override
	public void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) {

		this.contratoPontoConsumo = contratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getContratoModalidade()
	 */
	@Override
	public ContratoModalidade getContratoModalidade() {

		return contratoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setContratoModalidade
	 * (br.com.ggas.contrato.
	 * contrato.ContratoModalidade)
	 */
	@Override
	public void setContratoModalidade(ContratoModalidade contratoModalidade) {

		this.contratoModalidade = contratoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getDescricaoContratoModalidade()
	 */
	@Override
	public String getDescricaoContratoModalidade() {
		
		try {
			return getContratoModalidade().getDescricao();
		} catch (LazyInitializationException e) {
			LOG.error(e.getStackTrace(), e);
			return StringUtils.EMPTY;
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getIndicadorProgramacaoConsumo()
	 */
	@Override
	public Boolean getIndicadorProgramacaoConsumo() {

		return indicadorProgramacaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setIndicadorProgramacaoConsumo
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorProgramacaoConsumo(Boolean indicadorProgramacaoConsumo) {

		this.indicadorProgramacaoConsumo = indicadorProgramacaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getIndicadorQDSMaiorQDC()
	 */
	@Override
	public Boolean getIndicadorQDSMaiorQDC() {

		return indicadorQDSMaiorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setIndicadorQDSMaiorQDC(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorQDSMaiorQDC(Boolean indicadorQDSMaiorQDC) {

		this.indicadorQDSMaiorQDC = indicadorQDSMaiorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getDiasAntecedenciaQDS()
	 */
	@Override
	public Integer getDiasAntecedenciaQDS() {

		return diasAntecedenciaQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setDiasAntecedenciaQDS(java.lang.Integer)
	 */
	@Override
	public void setDiasAntecedenciaQDS(Integer diasAntecedenciaQDS) {

		this.diasAntecedenciaQDS = diasAntecedenciaQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getPrazoRevisaoQDC()
	 */
	@Override
	public Date getPrazoRevisaoQDC() {
		Date data = null;
		if(prazoRevisaoQDC != null) {
			data = (Date) prazoRevisaoQDC.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setPrazoRevisaoQDC(java.lang.Integer)
	 */
	@Override
	public void setPrazoRevisaoQDC(Date prazoRevisaoQDC) {
		if(prazoRevisaoQDC != null){
			this.prazoRevisaoQDC = (Date) prazoRevisaoQDC.clone();
		} else {
			this.prazoRevisaoQDC = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getMesesQDS()
	 */
	@Override
	public Integer getMesesQDS() {

		return mesesQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setMesesQDS(java.lang.Integer)
	 */
	@Override
	public void setMesesQDS(Integer mesesQDS) {

		this.mesesQDS = mesesQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getIndicadorRetiradaAutomatica()
	 */
	@Override
	public Boolean getIndicadorRetiradaAutomatica() {

		return indicadorRetiradaAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setIndicadorRetiradaAutomatica
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorRetiradaAutomatica(Boolean indicadorRetiradaAutomatica) {

		this.indicadorRetiradaAutomatica = indicadorRetiradaAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getIndicadorConfirmacaoAutomaticaQDS()
	 */
	@Override
	public Boolean getIndicadorConfirmacaoAutomaticaQDS() {

		return indicadorConfirmacaoAutomaticaQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setIndicadorConfirmacaoAutomaticaQDS
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorConfirmacaoAutomaticaQDS(Boolean indicadorConfirmacaoAutomaticaQDS) {

		this.indicadorConfirmacaoAutomaticaQDS = indicadorConfirmacaoAutomaticaQDS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getDataInicioRetiradaQPNR()
	 */
	@Override
	public Date getDataInicioRetiradaQPNR() {
		Date data = null;
		if(dataInicioRetiradaQPNR != null) {
			data = (Date) dataInicioRetiradaQPNR.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setDataInicioRetiradaQPNR(java.util.Date)
	 */
	@Override
	public void setDataInicioRetiradaQPNR(Date dataInicioRetiradaQPNR) {
		if(dataInicioRetiradaQPNR != null){
			this.dataInicioRetiradaQPNR = (Date) dataInicioRetiradaQPNR.clone();
		} else {
			this.dataInicioRetiradaQPNR = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.impl. ContratoPontoConsumoModalidade
	 * #getDataFimRetiradaQPNR()
	 */
	@Override
	public Date getDataFimRetiradaQPNR() {
		Date data = null;
		if (dataFimRetiradaQPNR != null) {
			data = (Date) dataFimRetiradaQPNR.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setDataFimRetiradaQPNR(java.util.Date)
	 */
	@Override
	public void setDataFimRetiradaQPNR(Date dataFimRetiradaQPNR) {
		if(dataFimRetiradaQPNR != null){
			this.dataFimRetiradaQPNR = (Date) dataFimRetiradaQPNR.clone();
		} else {
			this.dataFimRetiradaQPNR = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getConsumoRefSOPAnual()
	 */
	@Override
	public EntidadeConteudo getConsumoRefSOPAnual() {

		return consumoRefSOPAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setConsumoRefSOPAnual
	 * (br.com.ggas.contrato.
	 * contrato.EntidadeConteudo)
	 */
	@Override
	public void setConsumoRefSOPAnual(EntidadeConteudo consumoRefSOPAnual) {

		this.consumoRefSOPAnual = consumoRefSOPAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #getMargemVarSOPAnual()
	 */
	@Override
	public Integer getMargemVarSOPAnual() {

		return margemVarSOPAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoModalidade
	 * #setMargemVarSOPAnual(java.lang.Integer)
	 */
	@Override
	public void setMargemVarSOPAnual(Integer margemVarSOPAnual) {

		this.margemVarSOPAnual = margemVarSOPAnual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getPercentualQDCContratoQPNR()
	 */
	@Override
	public BigDecimal getPercentualQDCContratoQPNR() {

		return percentualQDCContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setPercentualQDCContratoQPNR
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualQDCContratoQPNR(BigDecimal percentualQDCContratoQPNR) {

		this.percentualQDCContratoQPNR = percentualQDCContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getPercentualQDCFimContratoQPNR()
	 */
	@Override
	public BigDecimal getPercentualQDCFimContratoQPNR() {

		return percentualQDCFimContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setPercentualQDCFimContratoQPNR
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualQDCFimContratoQPNR(BigDecimal percentualQDCFimContratoQPNR) {

		this.percentualQDCFimContratoQPNR = percentualQDCFimContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getListaContratoPontoConsumoModalidadeQDC
	 * ()
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidadeQDC> getListaContratoPontoConsumoModalidadeQDC() {

		return listaContratoPontoConsumoModalidadeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setListaContratoPontoConsumoModalidadeQDC
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumoModalidadeQDC(
					Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC) {

		this.listaContratoPontoConsumoModalidadeQDC = listaContratoPontoConsumoModalidadeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getListaContratoPontoConsumoPenalidade()
	 */
	@Override
	public Collection<ContratoPontoConsumoPenalidade> getListaContratoPontoConsumoPenalidade() {

		return listaContratoPontoConsumoPenalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setListaContratoPontoConsumoPenalidade
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumoPenalidade(Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) {

		this.listaContratoPontoConsumoPenalidade = listaContratoPontoConsumoPenalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #getPercentualVarSuperiorQDC()
	 */
	@Override
	public BigDecimal getPercentualVarSuperiorQDC() {

		return percentualVarSuperiorQDC;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade
	 * #setPercentualVarSuperiorQDC(java.math.
	 * BigDecimal)
	 */
	@Override
	public void setPercentualVarSuperiorQDC(BigDecimal percentualVarSuperiorQDC) {

		this.percentualVarSuperiorQDC = percentualVarSuperiorQDC;
	}

	/**
	 * @return the anosValidadeRetiradaQPNR
	 */
	@Override
	public Integer getAnosValidadeRetiradaQPNR() {

		return anosValidadeRetiradaQPNR;
	}

	/**
	 * @param anosValidadeRetiradaQPNR
	 *            the anosValidadeRetiradaQPNR to
	 *            set
	 */
	@Override
	public void setAnosValidadeRetiradaQPNR(Integer anosValidadeRetiradaQPNR) {

		this.anosValidadeRetiradaQPNR = anosValidadeRetiradaQPNR;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade#equalsNegocio(br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade
	 * )
	 */
	@Override
	public boolean equalsNegocio(ContratoPontoConsumoModalidade obj) {

		if (obj != null && this.contratoPontoConsumo != null && this.contratoPontoConsumo.getChavePrimaria() > 0
						&& obj.getContratoPontoConsumo() != null
						&& obj.getContratoPontoConsumo().getChavePrimaria() == this.contratoPontoConsumo.getChavePrimaria()
						&& this.contratoModalidade != null && this.contratoModalidade.getChavePrimaria() > 0
						&& obj.getContratoModalidade() != null
						&& obj.getContratoModalidade().getChavePrimaria() == this.contratoModalidade.getChavePrimaria()) {
			return true;
		}

		return false;
	}

	@Override
	public Integer getHoraLimiteProgramacaoDiaria() {

		return horaLimiteProgramacaoDiaria;
	}

	@Override
	public void setHoraLimiteProgramacaoDiaria(Integer horaLimiteProgramacaoDiaria) {

		this.horaLimiteProgramacaoDiaria = horaLimiteProgramacaoDiaria;
	}

	@Override
	public Integer getHoraLimiteProgIntradiaria() {

		return horaLimiteProgIntradiaria;
	}

	@Override
	public void setHoraLimiteProgIntradiaria(Integer horaLimiteProgIntradiaria) {

		this.horaLimiteProgIntradiaria = horaLimiteProgIntradiaria;
	}

	@Override
	public Integer getHoraLimiteAceitacaoDiaria() {

		return horaLimiteAceitacaoDiaria;
	}

	@Override
	public void setHoraLimiteAceitacaoDiaria(Integer horaLimiteAceitacaoDiaria) {

		this.horaLimiteAceitacaoDiaria = horaLimiteAceitacaoDiaria;
	}

	@Override
	public Integer getHoraLimiteAceitIntradiaria() {

		return horaLimiteAceitIntradiaria;
	}

	@Override
	public void setHoraLimiteAceitIntradiaria(Integer horaLimiteAceitIntradiaria) {

		this.horaLimiteAceitIntradiaria = horaLimiteAceitIntradiaria;
	}

	@Override
	public BigDecimal getPercentualQNR() {

		return percentualQNR;
	}

	@Override
	public void setPercentualQNR(BigDecimal percentualQNR) {

		this.percentualQNR = percentualQNR;
	}

	@Override
	public BigDecimal getPercentualMinimoQDCContratoQPNR() {

		return percentualMinimoQDCContratoQPNR;
	}

	@Override
	public void setPercentualMinimoQDCContratoQPNR(BigDecimal percentualMinimoQDCContratoQPNR) {

		this.percentualMinimoQDCContratoQPNR = percentualMinimoQDCContratoQPNR;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		ContratoPontoConsumoModalidadeImpl copm = (ContratoPontoConsumoModalidadeImpl) obj;
		if (copm.getChavePrimaria() > 0) {			
			return this.getChavePrimaria() == copm.getChavePrimaria();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(getChavePrimaria());
	}

}
