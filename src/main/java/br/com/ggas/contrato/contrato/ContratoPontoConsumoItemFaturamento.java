/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.math.BigDecimal;

import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao item de faturamento do Contrato Ponto de consumo.
 * 
 */
public interface ContratoPontoConsumoItemFaturamento extends EntidadeNegocio {

	String BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO = "contratoPontoConsumoItemFaturamento";

	String ITEM_FATURA = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_ITEM_FATURA";

	String DIA_VENCIMENTO = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_DIA_VENCIMENTO";

	String FASE_REFERENCIA = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_FASE_REFERENCIA";

	String OPCAO_FASE_REFERENCIA = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_OPCAO_FASE_REFERENCIA";

	String VENCIMENTO_NAO_DIA_UTIL = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_VENCIMENTO_NAO_DIA_UTIL";

	String DIA_COTACAO = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_DIA_COTACAO";

	String DATA_REFERENCIA_CAMBIAL = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_DATA_REFERENCIA_CAMBIAL";

	String PERCENTUAL_MINIMO_QDC = "CONTRATO_PONTO_CONSUMO_ITEM_VENCIMENTO_PERCENTUAL_MINIMO_QDC";

	/**
	 * @return the contratoPontoConsumo
	 */
	ContratoPontoConsumo getContratoPontoConsumo();

	/**
	 * @param contratoPontoConsumo
	 *            the contratoPontoConsumo to set
	 */
	void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo);

	/**
	 * @return the numeroDiaVencimento
	 */
	Integer getNumeroDiaVencimento();

	/**
	 * @param numeroDiaVencimento
	 *            the numeroDiaVencimento to set
	 */
	void setNumeroDiaVencimento(Integer numeroDiaVencimento);

	/**
	 * @return the itemFatura
	 */
	EntidadeConteudo getItemFatura();

	/**
	 * @param itemFatura
	 *            the itemFatura to set
	 */
	void setItemFatura(EntidadeConteudo itemFatura);

	/**
	 * @return the faseReferencia
	 */
	EntidadeConteudo getFaseReferencia();

	/**
	 * @param faseReferencia
	 *            the faseReferencia to set
	 */
	void setFaseReferencia(EntidadeConteudo faseReferencia);

	/**
	 * @return the opcaoFaseReferencia
	 */
	EntidadeConteudo getOpcaoFaseReferencia();

	/**
	 * @param opcaoFaseReferencia
	 *            the opcaoFaseReferencia to set
	 */
	void setOpcaoFaseReferencia(EntidadeConteudo opcaoFaseReferencia);

	/**
	 * @return the vencimentoDiaUtil
	 */
	Boolean getVencimentoDiaUtil();

	/**
	 * @param vencimentoDiaUtil
	 *            the vencimentoDiaUtil to set
	 */
	void setVencimentoDiaUtil(Boolean vencimentoDiaUtil);

	/**
	 * @return the tarifa
	 */
	Tarifa getTarifa();

	/**
	 * @param tarifa
	 *            the tarifa to set
	 */
	void setTarifa(Tarifa tarifa);

	/**
	 * @return the indicadorDepositoIdentificado
	 */
	Boolean getIndicadorDepositoIdentificado();

	/**
	 * @param indicadorDepositoIdentificado
	 *            the
	 *            indicadorDepositoIdentificado to
	 *            set
	 */
	void setIndicadorDepositoIdentificado(Boolean indicadorDepositoIdentificado);

	/**
	 * @return the dataReferenciaCambial
	 */
	EntidadeConteudo getDataReferenciaCambial();

	/**
	 * @param dataReferenciaCambial
	 *            the dataReferenciaCambial to set
	 */
	void setDataReferenciaCambial(EntidadeConteudo dataReferenciaCambial);

	/**
	 * @return the diaCotacao
	 */
	EntidadeConteudo getDiaCotacao();

	/**
	 * @param diaCotacao
	 *            the diaCotacao to set
	 */
	void setDiaCotacao(EntidadeConteudo diaCotacao);

	/**
	 * Equals negocio.
	 * 
	 * @param obj
	 *            the obj
	 * @return true, if successful
	 */
	boolean equalsNegocio(ContratoPontoConsumoItemFaturamento obj);

	/**
	 * @return
	 */
	BigDecimal getPercminimoQDC();

	/**
	 * @param percminimoQDC
	 */
	void setPercminimoQDC(BigDecimal percminimoQDC);

}
