/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a associação Contrato Cliente.
 *
 */
public interface ContratoCliente extends EntidadeNegocio {

	String BEAN_ID_CONTRATO_CLIENTE = "contratoCliente";

	String CONTRATO_CLIENTE_CLIENTE = "CONTRATO_CLIENTE_CLIENTE";

	String CONTRATO_CLIENTE_RELACAO_INCICIO = "CONTRATO_CLIENTE_RELACAO_INCICIO";

	String CONTRATO_CLIENTE_RELACAO_FIM = "CONTRATO_CLIENTE_RELACAO_FIM";

	String CONTRATO_CLIENTE_RESPONSABILIDADE = "CONTRATO_CLIENTE_RESPONSABILIDADE";

	/**
	 * @return the contrato
	 */
	Contrato getContrato();

	/**
	 * @param contrato
	 *            the contrato to set
	 */
	void setContrato(Contrato contrato);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the relacaoInicio
	 */
	Date getRelacaoInicio();

	/**
	 * @param relacaoInicio
	 *            the relacaoInicio to set
	 */
	void setRelacaoInicio(Date relacaoInicio);

	/**
	 * @return the relacaoFim
	 */
	Date getRelacaoFim();

	/**
	 * @param relacaoFim
	 *            the relacaoFim to set
	 */
	void setRelacaoFim(Date relacaoFim);

	/**
	 * @return the indicadorPrincipal
	 */
	Boolean getIndicadorPrincipal();

	/**
	 * @param indicadorPrincipal
	 *            the indicadorPrincipal to set
	 */
	void setIndicadorPrincipal(Boolean indicadorPrincipal);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the responsabilidade
	 */
	EntidadeConteudo getResponsabilidade();

	/**
	 * @param responsabilidade
	 *            the responsabilidade to set
	 */
	void setResponsabilidade(EntidadeConteudo responsabilidade);

}
