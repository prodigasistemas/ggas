/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela representação dos Dados da Quantidade de Volume. 
 *
 */
public class DadosQuantidadeVolumesVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8647369845386823496L;

	private Date data;

	private BigDecimal qdc;

	private BigDecimal qds;

	private BigDecimal qdp;

	private BigDecimal qdr;

	private BigDecimal qpnr;

	private BigDecimal qr;

	private BigDecimal retiradaMenor;

	private BigDecimal retiradaMaior;

	private BigDecimal penalidade;

	private BigDecimal top;

	private BigDecimal sop;

	private BigDecimal dop;

	public Date getData() {
		Date dataTmp = null;
		if(this.data != null) {
			dataTmp = (Date) this.data.clone();
		}
		return dataTmp;
	}

	public void setData(Date data) {
		if (data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	public BigDecimal getQdc() {

		return qdc;
	}

	public void setQdc(BigDecimal qdc) {

		this.qdc = qdc;
	}

	public BigDecimal getQds() {

		return qds;
	}

	public void setQds(BigDecimal qds) {

		this.qds = qds;
	}

	public BigDecimal getQdp() {

		return qdp;
	}

	public void setQdp(BigDecimal qdp) {

		this.qdp = qdp;
	}

	public BigDecimal getQdr() {

		return qdr;
	}

	public void setQdr(BigDecimal qdr) {

		this.qdr = qdr;
	}

	public BigDecimal getQpnr() {

		return qpnr;
	}

	public void setQpnr(BigDecimal qpnr) {

		this.qpnr = qpnr;
	}

	public BigDecimal getQr() {

		return qr;
	}

	public void setQr(BigDecimal qr) {

		this.qr = qr;
	}

	public BigDecimal getRetiradaMenor() {

		return retiradaMenor;
	}

	public void setRetiradaMenor(BigDecimal retiradaMenor) {

		this.retiradaMenor = retiradaMenor;
	}

	public BigDecimal getRetiradaMaior() {

		return retiradaMaior;
	}

	public void setRetiradaMaior(BigDecimal retiradaMaior) {

		this.retiradaMaior = retiradaMaior;
	}

	public BigDecimal getPenalidade() {

		return penalidade;
	}

	public void setPenalidade(BigDecimal penalidade) {

		this.penalidade = penalidade;
	}

	public BigDecimal getTop() {

		return top;
	}

	public void setTop(BigDecimal top) {

		this.top = top;
	}

	public BigDecimal getSop() {

		return sop;
	}

	public void setSop(BigDecimal sop) {

		this.sop = sop;
	}

	public BigDecimal getDop() {

		return dop;
	}

	public void setDop(BigDecimal dop) {

		this.dop = dop;
	}

	public String getValorQdcFormatado() {

		return this.obterValorFormatado(qdc);
	}

	public String getValorQdsFormatado() {

		return this.obterValorFormatado(qds);
	}

	public String getValorQdpFormatado() {

		return this.obterValorFormatado(qdp);
	}

	public String getValorQdrFormatado() {

		return this.obterValorFormatado(qdr);
	}

	public String getValorQpnrFormatado() {

		return this.obterValorFormatado(qpnr);
	}

	public String getValorQrFormatado() {

		return this.obterValorFormatado(qr);
	}

	public String getValorRetiradaMenorFormatado() {

		return this.obterValorFormatado(retiradaMenor);
	}

	public String getValorRetiradaMaiorFormatado() {

		return this.obterValorFormatado(retiradaMaior);
	}

	public String getValorPenalidadeFormatado() {

		return this.obterValorFormatado(penalidade);
	}

	public String getValorTopFormatado() {

		return this.obterValorFormatado(top);
	}

	public String getValorSopFormatado() {

		return this.obterValorFormatado(sop);
	}

	public String getValorDopFormatado() {

		return this.obterValorFormatado(dop);
	}

	/**
	 * Obter valor formatado.
	 * 
	 * @param bigDecimal the big decimal
	 * @return the string
	 */
	private String obterValorFormatado(BigDecimal bigDecimal) {

		BigDecimal valor = bigDecimal;

		if (valor == null) {
			valor = BigDecimal.ZERO;
		}

		return Util.converterCampoCurrencyParaString(valor, Constantes.LOCALE_PADRAO);
	}

}

