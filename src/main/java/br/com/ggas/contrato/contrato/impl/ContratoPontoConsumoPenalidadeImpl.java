/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * 
 *
 */
class ContratoPontoConsumoPenalidadeImpl extends EntidadeNegocioImpl implements ContratoPontoConsumoPenalidade {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8211889297653499681L;

	private BigDecimal percentualMargemVariacao;

	private EntidadeConteudo periodicidadePenalidade;

	private EntidadeConteudo consumoReferencia;

	private ContratoPontoConsumoModalidade contratoPontoConsumoModalidade;

	private Penalidade penalidade;

	private EntidadeConteudo referenciaQFParadaProgramada;

	private Date dataInicioVigencia;

	private Date dataFimVigencia;

	private BigDecimal percentualNaoRecuperavel;

	private Boolean consideraParadaProgramada;

	private Boolean consideraFalhaFornecimento;

	private Boolean consideraCasoFortuito;

	private Boolean recuperavel;

	private EntidadeConteudo tipoApuracao;

	private EntidadeConteudo precoCobranca;

	private EntidadeConteudo apuracaoParadaProgramada;

	private EntidadeConteudo apuracaoCasoFortuito;

	private EntidadeConteudo apuracaoFalhaFornecimento;

	private EntidadeConteudo tipoAgrupamentoContrato;

	private EntidadeConteudo baseApuracao;

	private BigDecimal valorPercentualCobRetMaiorMenor;

	private BigDecimal valorPercentualCobIntRetMaiorMenor;

	private BigDecimal valorPercentualRetMaiorMenor;

	private Boolean indicadorImposto;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade
	 * #getContratoPontoConsumoModalidade()
	 */
	@Override
	public ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade() {

		return contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade
	 * #setContratoPontoConsumoModalidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade)
	 */
	@Override
	public void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {

		this.contratoPontoConsumoModalidade = contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoPenalidade
	 * #getPercentualMargemVariacao()
	 */
	@Override
	public BigDecimal getPercentualMargemVariacao() {

		return percentualMargemVariacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoPenalidade
	 * #setPercentualMargemVariacao
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualMargemVariacao(BigDecimal percentualMargemVariacao) {

		this.percentualMargemVariacao = percentualMargemVariacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoPenalidade
	 * #getPeriodicidadePenalidade()
	 */
	@Override
	public EntidadeConteudo getPeriodicidadePenalidade() {

		return periodicidadePenalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoPenalidade
	 * #setPeriodicidadePenalidade
	 * (br.com.ggas.contrato
	 * .contrato.EntidadeConteudo)
	 */
	@Override
	public void setPeriodicidadePenalidade(EntidadeConteudo periodicidadePenalidade) {

		this.periodicidadePenalidade = periodicidadePenalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoPenalidade
	 * #getConsumoReferencia()
	 */
	@Override
	public EntidadeConteudo getConsumoReferencia() {

		return consumoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ContratoPontoConsumoPenalidade
	 * #setConsumoReferencia
	 * (br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setConsumoReferencia(EntidadeConteudo consumoReferencia) {

		this.consumoReferencia = consumoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade
	 * #getPenalidade()
	 */
	@Override
	public Penalidade getPenalidade() {

		return penalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade
	 * #setPenalidade
	 * (br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade)
	 */
	@Override
	public void setPenalidade(Penalidade penalidade) {

		this.penalidade = penalidade;
	}

	@Override
	public EntidadeConteudo getReferenciaQFParadaProgramada() {

		return referenciaQFParadaProgramada;
	}

	@Override
	public void setReferenciaQFParadaProgramada(EntidadeConteudo referenciaQFParadaProgramada) {

		this.referenciaQFParadaProgramada = referenciaQFParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade
	 * #getPercentualMargemVariacaoFormatado()
	 */
	@Override
	public String getPercentualMargemVariacaoFormatado() throws GGASException {

		if (percentualMargemVariacao != null) {
			return Util.converterCampoCurrencyParaString(percentualMargemVariacao, Constantes.LOCALE_PADRAO) + " %";
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade#equalsNegocio(br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade
	 * )
	 */
	@Override
	public boolean equalsNegocio(ContratoPontoConsumoPenalidade obj) {

		if (obj != null
						&& this.contratoPontoConsumoModalidade != null
						&& this.contratoPontoConsumoModalidade.getChavePrimaria() > 0
						&& obj.getContratoPontoConsumoModalidade() != null
						&& obj.getContratoPontoConsumoModalidade().getChavePrimaria() == this.contratoPontoConsumoModalidade
										.getChavePrimaria() && this.penalidade != null && this.penalidade.getChavePrimaria() > 0
						&& obj.getPenalidade() != null && obj.getPenalidade().getChavePrimaria() == this.penalidade.getChavePrimaria()) {
			return true;
		}

		return false;
	}

	@Override
	public Date getDataInicioVigencia() {

		return dataInicioVigencia;
	}

	@Override
	public void setDataInicioVigencia(Date dataInicioVigencia) {

		this.dataInicioVigencia = dataInicioVigencia;
	}

	@Override
	public Date getDataFimVigencia() {

		return dataFimVigencia;
	}

	@Override
	public void setDataFimVigencia(Date dataFimVigencia) {

		this.dataFimVigencia = dataFimVigencia;
	}

	@Override
	public BigDecimal getPercentualNaoRecuperavel() {

		return percentualNaoRecuperavel;
	}

	@Override
	public void setPercentualNaoRecuperavel(BigDecimal percentualNaoRecuperavel) {

		this.percentualNaoRecuperavel = percentualNaoRecuperavel;
	}

	@Override
	public Boolean getConsideraParadaProgramada() {

		return consideraParadaProgramada;
	}

	@Override
	public void setConsideraParadaProgramada(Boolean consideraParadaProgramada) {

		this.consideraParadaProgramada = consideraParadaProgramada;
	}

	@Override
	public Boolean getConsideraFalhaFornecimento() {

		return consideraFalhaFornecimento;
	}

	@Override
	public void setConsideraFalhaFornecimento(Boolean consideraFalhaFornecimento) {

		this.consideraFalhaFornecimento = consideraFalhaFornecimento;
	}

	@Override
	public Boolean getConsideraCasoFortuito() {

		return consideraCasoFortuito;
	}

	@Override
	public void setConsideraCasoFortuito(Boolean consideraCasoFortuito) {

		this.consideraCasoFortuito = consideraCasoFortuito;
	}

	@Override
	public Boolean getRecuperavel() {

		return recuperavel;
	}

	@Override
	public void setRecuperavel(Boolean recuperavel) {

		this.recuperavel = recuperavel;
	}

	@Override
	public EntidadeConteudo getTipoApuracao() {

		return tipoApuracao;
	}

	@Override
	public void setTipoApuracao(EntidadeConteudo tipoApuracao) {

		this.tipoApuracao = tipoApuracao;
	}

	@Override
	public EntidadeConteudo getPrecoCobranca() {

		return precoCobranca;
	}

	@Override
	public void setPrecoCobranca(EntidadeConteudo precoCobranca) {

		this.precoCobranca = precoCobranca;
	}

	@Override
	public EntidadeConteudo getApuracaoParadaProgramada() {

		return apuracaoParadaProgramada;
	}

	@Override
	public void setApuracaoParadaProgramada(EntidadeConteudo apuracaoParadaProgramada) {

		this.apuracaoParadaProgramada = apuracaoParadaProgramada;
	}

	@Override
	public EntidadeConteudo getApuracaoCasoFortuito() {

		return apuracaoCasoFortuito;
	}

	@Override
	public void setApuracaoCasoFortuito(EntidadeConteudo apuracaoCasoFortuito) {

		this.apuracaoCasoFortuito = apuracaoCasoFortuito;
	}

	@Override
	public EntidadeConteudo getApuracaoFalhaFornecimento() {

		return apuracaoFalhaFornecimento;
	}

	@Override
	public void setApuracaoFalhaFornecimento(EntidadeConteudo apuracaoFalhaFornecimento) {

		this.apuracaoFalhaFornecimento = apuracaoFalhaFornecimento;
	}

	@Override
	public EntidadeConteudo getTipoAgrupamentoContrato() {

		return tipoAgrupamentoContrato;
	}

	@Override
	public void setTipoAgrupamentoContrato(EntidadeConteudo tipoAgrupamentoContrato) {

		this.tipoAgrupamentoContrato = tipoAgrupamentoContrato;
	}

	@Override
	public EntidadeConteudo getBaseApuracao() {

		return baseApuracao;
	}

	@Override
	public void setBaseApuracao(EntidadeConteudo baseApuracao) {

		this.baseApuracao = baseApuracao;
	}

	@Override
	public BigDecimal getValorPercentualCobRetMaiorMenor() {

		return valorPercentualCobRetMaiorMenor;
	}

	@Override
	public void setValorPercentualCobRetMaiorMenor(BigDecimal valorPercentualCobRetMaiorMenor) {

		this.valorPercentualCobRetMaiorMenor = valorPercentualCobRetMaiorMenor;
	}

	@Override
	public BigDecimal getValorPercentualCobIntRetMaiorMenor() {

		return valorPercentualCobIntRetMaiorMenor;
	}

	@Override
	public void setValorPercentualCobIntRetMaiorMenor(BigDecimal valorPercentualCobIntRetMaiorMenor) {

		this.valorPercentualCobIntRetMaiorMenor = valorPercentualCobIntRetMaiorMenor;
	}

	@Override
	public BigDecimal getValorPercentualRetMaiorMenor() {

		return valorPercentualRetMaiorMenor;
	}

	@Override
	public void setValorPercentualRetMaiorMenor(BigDecimal valorPercentualRetMaiorMenor) {

		this.valorPercentualRetMaiorMenor = valorPercentualRetMaiorMenor;
	}

	@Override
	public Boolean getIndicadorImposto() {

		return indicadorImposto;
	}

	@Override
	public void setIndicadorImposto(Boolean indicadorImposto) {

		this.indicadorImposto = indicadorImposto;
	}

	@Override
	public Long getChaveMapeadora() {
		return this.penalidade.getChavePrimaria();
	}

}
