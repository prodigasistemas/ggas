/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação de um contrato
 */

public class ContratoImpl extends EntidadeNegocioImpl implements Contrato {

	/** serialVersionUID */
	private static final long serialVersionUID = 2300640050174848795L;

	private EntidadeConteudo garantiaFinanceira;

	private SituacaoContrato situacao;

	private ModeloContrato modeloContrato;

	private Proposta proposta;

	private Cliente clienteAssinatura;

	private String descricaoGarantiaFinanceira;

	private String descricaoAditivo;

	private Integer numero;

	private Integer anoContrato;

	private String numeroAnterior;

	private Integer numeroAditivo;

	private Integer numeroDiaVencimentoFinanciamento;

	private String numeroEmpenho;

	private Integer numeroDiasRenovacaoAutomatica;

	private Date dataAditivo;

	private Date dataFimGarantiaFinanciamento;

	private Date dataInicioGarantiaFinanciamento;

	private BigDecimal valorGastoMensal;

	private BigDecimal medidaEconomiaMensalGN;

	private BigDecimal medidaEconomiaAnualGN;

	private BigDecimal percentualEconomiaGN;

	private BigDecimal valorGarantiaFinanceira;

	private Boolean renovacaoGarantiaFinanceira;

	private boolean renovacaoAutomatica;

	private boolean propostaAprovada;

	private boolean enderecoPadrao;

	private boolean exigeAprovacao;

	private Boolean agrupamentoConta;

	private Boolean agrupamentoCobranca;

	private Date dataVencimentoObrigacoes;

	private Date dataAssinatura;

	private BigDecimal valorContrato;

	private Boolean indicadorMulta;

	private Boolean indicadorJuros;

	private IndiceFinanceiro indiceFinanceiro;

	private EntidadeConteudo consumoReferenciaSobreDemanda;

	private Collection<ContratoPontoConsumo> listaContratoPontoConsumo = new HashSet<>();

	private Collection<ContratoPenalidade> listaContratoPenalidade = new HashSet<>();

	private Collection<ContratoCliente> listaContratoCliente = new HashSet<>();

	private Collection<ContratoQDC> listaContratoQDC = new HashSet<>();

	private Integer diasAntecedenciaRevisaoGarantiaFinanceira;

	private Integer periodicidadeReavaliacaoGarantias;

	private Integer diasAntecedenciaRenovacao;

	private BigDecimal valorParticipacaoCliente;

	private Integer qtdParcelasFinanciamento;

	private BigDecimal percentualJurosFinanciamento;

	private EntidadeConteudo sistemaAmortizacao;

	private BigDecimal percentualTarifaDoP;

	private boolean indicadorAnoContratual;
	
	private boolean indicadorDebitoAutomatico;

	private Boolean indicadorParticipanteECartas;

	private EntidadeConteudo formaCobranca;

	private Long chavePrimariaPai;

	private EntidadeConteudo tipoAgrupamento;

	private EntidadeConteudo contratoCompra;

	private String numeroCompletoContrato;

	private BigDecimal percentualSobreTariGas;

	private BigDecimal valorInvestimento;

	private Date dataInvestimento;

	private EntidadeConteudo multaRecisoria;

	private Boolean faturaEncerramentoGerada;

	private Date dataRecisao;

	private BigDecimal percentualJurosMora;

	private BigDecimal percentualMulta;

	private Boolean tipoPeriodicidadePenalidade;

	private ArrecadadorContratoConvenio arrecadadorContratoConvenio;
	
	private ArrecadadorContratoConvenio arrecadadorContratoConvenioDebitoAutomatico;

	private EntidadeConteudo periodicidadeMaiorMenor;

	private Collection<Fatura> listaFatura = new HashSet<>();

	private Collection<ServicoAutorizacao> listaServicoAutorizacao = new HashSet<>();

	private String descricaoContrato;

	private BigDecimal volumeReferencia;

	private Integer ordemFaturamento;

	private Long chavePrimariaPrincipal;

	private Date dataInicioRetiradaQPNR;

	private Date dataFimRetiradaQPNR;

	private BigDecimal percentualQDCContratoQPNR;

	private BigDecimal percentualMinimoQDCContratoQPNR;

	private BigDecimal percentualQDCFimContratoQPNR;

	private Integer anosValidadeRetiradaQPNR;

	private Collection<ContratoAnexo> anexos = new HashSet<>();

	private Banco banco;

	private String agencia;
	private String contaCorrente;
	
	private BigDecimal incentivosComerciais;
	
	private BigDecimal incentivosInfraestrutura;
	
	private Integer prazoVigencia;

	/**
	 * @return the contratoCompra
	 */
	public EntidadeConteudo getContratoCompra() {

		return contratoCompra;
	}

	public void setContratoCompra(EntidadeConteudo contratoCompra) {

		this.contratoCompra = contratoCompra;
	}

	/**
	 * @return the numeroCompletoContrato
	 */
	@Override
	public String getNumeroCompletoContrato() {

		return numeroCompletoContrato;
	}

	/**
	 * @param numeroCompletoContrato
	 *            the numeroCompletoContrato to
	 *            set
	 */
	@Override
	public void setNumeroCompletoContrato(String numeroCompletoContrato) {

		this.numeroCompletoContrato = numeroCompletoContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDataVencimentoObrigacoes()
	 */
	@Override
	public Date getDataVencimentoObrigacoes() {
		Date data = null;
		if (dataVencimentoObrigacoes != null) {
			data = (Date) dataVencimentoObrigacoes.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDataVencimentoObrigacoes(java.util.Date)
	 */
	@Override
	public void setDataVencimentoObrigacoes(Date dataVencimentoObrigacoes) {
		if(dataVencimentoObrigacoes != null){
			this.dataVencimentoObrigacoes = (Date) dataVencimentoObrigacoes.clone();
		} else {
			this.dataVencimentoObrigacoes = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getAgrupamentoConta()
	 */
	@Override
	public Boolean getAgrupamentoConta() {

		return agrupamentoConta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setAgrupamentoConta(java.lang.Boolean)
	 */
	@Override
	public void setAgrupamentoConta(Boolean agrupamentoConta) {

		this.agrupamentoConta = agrupamentoConta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getAgrupamentoCobranca()
	 */
	@Override
	public Boolean getAgrupamentoCobranca() {

		return agrupamentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setAgrupamentoCobranca(java.lang.Boolean)
	 */
	@Override
	public void setAgrupamentoCobranca(Boolean agrupamentoCobranca) {

		this.agrupamentoCobranca = agrupamentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getNumeroEmpenho()
	 */
	@Override
	public String getNumeroEmpenho() {

		return numeroEmpenho;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setNumeroEmpenho(java.lang.String)
	 */
	@Override
	public void setNumeroEmpenho(String numeroEmpenho) {

		this.numeroEmpenho = numeroEmpenho;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getNumeroDiasRenovacaoAutomatica()
	 */
	@Override
	public Integer getNumeroDiasRenovacaoAutomatica() {

		return numeroDiasRenovacaoAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setNumeroDiasRenovacaoAutomatica
	 * (java.lang.Integer)
	 */
	@Override
	public void setNumeroDiasRenovacaoAutomatica(Integer numeroDiasRenovacaoAutomatica) {

		this.numeroDiasRenovacaoAutomatica = numeroDiasRenovacaoAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.Contrato#getNumero
	 * ()
	 */
	@Override
	public Integer getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.Contrato#setNumero
	 * (java.lang.Integer)
	 */
	@Override
	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.Contrato#
	 * getAnoContrato()
	 */
	@Override
	public Integer getAnoContrato() {

		return anoContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.Contrato#
	 * setAnoContrato(java.lang.Integer)
	 */
	@Override
	public void setAnoContrato(Integer anoContrato) {

		this.anoContrato = anoContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.impl.Contrato#
	 * getNumeroAnterior()
	 */
	@Override
	public String getNumeroAnterior() {

		return numeroAnterior;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.impl.Contrato# setNumeroAnterior(java.lang.String)
	 */
	@Override
	public void setNumeroAnterior(String numeroAnterior) {

		this.numeroAnterior = numeroAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.Contrato#getSituacao
	 * ()
	 */
	@Override
	public SituacaoContrato getSituacao() {

		return situacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.Contrato#setSituacao
	 * (br.com.ggas.contrato.SituacaoContrato)
	 */
	@Override
	public void setSituacao(SituacaoContrato situacao) {

		this.situacao = situacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.Contrato#getModelo
	 * ()
	 */
	@Override
	public ModeloContrato getModeloContrato() {

		return modeloContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.impl.Contrato#setModelo
	 * (br.com.ggas.contrato.ModeloContrato)
	 */
	@Override
	public void setModeloContrato(ModeloContrato modeloContrato) {

		this.modeloContrato = modeloContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getNumeroAditivo()
	 */
	@Override
	public Integer getNumeroAditivo() {

		return numeroAditivo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setNumeroAditivo(java.lang.Integer)
	 */
	@Override
	public void setNumeroAditivo(Integer numeroAditivo) {

		this.numeroAditivo = numeroAditivo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDataAditivo()
	 */
	@Override
	public Date getDataAditivo() {
		Date data = null;
		if (dataAditivo != null) {
			data = (Date) dataAditivo.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDataAditivo(java.util.Date)
	 */
	@Override
	public void setDataAditivo(Date dataAditivo) {
		if(dataAditivo != null){
			this.dataAditivo = (Date) dataAditivo.clone();
		} else {
			this.dataAditivo = null; 
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getProposta()
	 */
	@Override
	public Proposta getProposta() {

		return proposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setProposta
	 * (br.com.ggas.contrato.proposta.Proposta)
	 */
	@Override
	public void setProposta(Proposta proposta) {

		this.proposta = proposta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getValorGastoMensal()
	 */
	@Override
	public BigDecimal getValorGastoMensal() {

		return valorGastoMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setValorGastoMensal(java.math.BigDecimal)
	 */
	@Override
	public void setValorGastoMensal(BigDecimal valorGastoMensal) {

		this.valorGastoMensal = valorGastoMensal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getMedidaEconomiaMensalGN()
	 */
	@Override
	public BigDecimal getMedidaEconomiaMensalGN() {

		return medidaEconomiaMensalGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setMedidaEconomiaMensalGN
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setMedidaEconomiaMensalGN(BigDecimal medidaEconomiaMensalGN) {

		this.medidaEconomiaMensalGN = medidaEconomiaMensalGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getMedidaEconomiaAnualGN()
	 */
	@Override
	public BigDecimal getMedidaEconomiaAnualGN() {

		return medidaEconomiaAnualGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setMedidaEconomiaAnualGN
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setMedidaEconomiaAnualGN(BigDecimal medidaEconomiaAnualGN) {

		this.medidaEconomiaAnualGN = medidaEconomiaAnualGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getPercentualEconomiaGN()
	 */
	@Override
	public BigDecimal getPercentualEconomiaGN() {

		return percentualEconomiaGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setPercentualEconomiaGN
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualEconomiaGN(BigDecimal percentualEconomiaGN) {

		this.percentualEconomiaGN = percentualEconomiaGN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getNumeroDiaVencimentoFinanciamento()
	 */
	@Override
	public Integer getNumeroDiaVencimentoFinanciamento() {

		return numeroDiaVencimentoFinanciamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setNumeroDiaVencimentoFinanciamento
	 * (java.lang.Integer)
	 */
	@Override
	public void setNumeroDiaVencimentoFinanciamento(Integer numeroDiaVencimentoFinanciamento) {

		this.numeroDiaVencimentoFinanciamento = numeroDiaVencimentoFinanciamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDataFimGarantiaFinanciamento()
	 */
	@Override
	public Date getDataFimGarantiaFinanciamento() {
		Date data = null;
		if (dataFimGarantiaFinanciamento != null) {
			data = (Date) dataFimGarantiaFinanciamento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDataFimGarantiaFinanciamento
	 * (java.util.Date)
	 */
	@Override
	public void setDataFimGarantiaFinanciamento(Date dataFimGarantiaFinanciamento) {
		if(dataFimGarantiaFinanciamento != null){
			this.dataFimGarantiaFinanciamento = (Date) dataFimGarantiaFinanciamento.clone();
		} else {
			this.dataFimGarantiaFinanciamento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDataInicioGarantiaFinanciamento()
	 */
	@Override
	public Date getDataInicioGarantiaFinanciamento() {
		Date data = null;
		if (dataInicioGarantiaFinanciamento != null) {
			data = (Date) dataInicioGarantiaFinanciamento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDataInicioGarantiaFinanciamento
	 * (java.util.Date)
	 */
	@Override
	public void setDataInicioGarantiaFinanciamento(Date dataInicioGarantiaFinanciamento) {
		if(dataInicioGarantiaFinanciamento != null){
			this.dataInicioGarantiaFinanciamento = (Date) dataInicioGarantiaFinanciamento.clone();
		} else {
			this.dataInicioGarantiaFinanciamento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getValorGarantiaFinanceira()
	 */
	@Override
	public BigDecimal getValorGarantiaFinanceira() {

		return valorGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setValorGarantiaFinanceira
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorGarantiaFinanceira(BigDecimal valorGarantiaFinanceira) {

		this.valorGarantiaFinanceira = valorGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDescricaoGarantiaFinanceira()
	 */
	@Override
	public String getDescricaoGarantiaFinanceira() {

		return descricaoGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDescricaoGarantiaFinanceira
	 * (java.lang.String)
	 */
	@Override
	public void setDescricaoGarantiaFinanceira(String descricaoGarantiaFinanceira) {

		this.descricaoGarantiaFinanceira = descricaoGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getGarantiaFinanceira()
	 */
	@Override
	public EntidadeConteudo getGarantiaFinanceira() {

		return garantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setGarantiaFinanceira
	 * (br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setGarantiaFinanceira(EntidadeConteudo garantiaFinanceira) {

		this.garantiaFinanceira = garantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDescricaoAditivo()
	 */
	@Override
	public String getDescricaoAditivo() {

		return descricaoAditivo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDescricaoAditivo(java.lang.String)
	 */
	@Override
	public void setDescricaoAditivo(String descricaoAditivo) {

		this.descricaoAditivo = descricaoAditivo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getClienteAssinatura()
	 */
	@Override
	public Cliente getClienteAssinatura() {

		return clienteAssinatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setClienteAssinatura
	 * (br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void setClienteAssinatura(Cliente clienteAssinatura) {

		this.clienteAssinatura = clienteAssinatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getRenovacaoGarantiaFinanceira()
	 */
	@Override
	public Boolean getRenovacaoGarantiaFinanceira() {

		return renovacaoGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setRenovacaoGarantiaFinanceira
	 * (java.lang.Boolean)
	 */
	@Override
	public void setRenovacaoGarantiaFinanceira(Boolean renovacaoGarantiaFinanceira) {

		this.renovacaoGarantiaFinanceira = renovacaoGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getRenovacaoAutomatica()
	 */
	@Override
	public boolean getRenovacaoAutomatica() {

		return renovacaoAutomatica;
	}

	@Override
	public void setRenovacaoAutomatica(boolean renovacaoAutomatica) {

		this.renovacaoAutomatica = renovacaoAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDataAssinatura()
	 */
	@Override
	public Date getDataAssinatura() {
		Date data = null;
		if(dataAssinatura != null) {
			data = (Date) dataAssinatura.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDataAssinatura(java.util.Date)
	 */
	@Override
	public void setDataAssinatura(Date dataAssinatura) {
		if(dataAssinatura != null){
			this.dataAssinatura = (Date) dataAssinatura.clone();
		} else {
			this.dataAssinatura = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getValorContrato()
	 */
	@Override
	public BigDecimal getValorContrato() {

		return valorContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setValorContrato(java.math.BigDecimal)
	 */
	@Override
	public void setValorContrato(BigDecimal valorContrato) {

		this.valorContrato = valorContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getIndicadorMulta()
	 */
	@Override
	public Boolean getIndicadorMulta() {

		return indicadorMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setIndicadorMulta(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorMulta(Boolean indicadorMulta) {

		this.indicadorMulta = indicadorMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getIndicadorJuros()
	 */
	@Override
	public Boolean getIndicadorJuros() {

		return indicadorJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setIndicadorJuros(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorJuros(Boolean indicadorJuros) {

		this.indicadorJuros = indicadorJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getIndiceFinanceiro()
	 */
	@Override
	public IndiceFinanceiro getIndiceFinanceiro() {

		return indiceFinanceiro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setIndiceFinanceiro
	 * (br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro) {

		this.indiceFinanceiro = indiceFinanceiro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getListaContratoPontoConsumo()
	 */
	@Override
	public Collection<ContratoPontoConsumo> getListaContratoPontoConsumo() {

		return listaContratoPontoConsumo;
	}

	@Override
	public ContratoPontoConsumo getContratoPontoConsumo(long idContratoPontoConsumo) {

		Collection<ContratoPontoConsumo> listaContratosPontoConsumo = getListaContratoPontoConsumo();
		for (ContratoPontoConsumo contratoPontoConsumo : listaContratosPontoConsumo) {
			if (contratoPontoConsumo.getChavePrimaria() == idContratoPontoConsumo) {
				return contratoPontoConsumo;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setListaContratoPontoConsumo
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoPontoConsumo(Collection<ContratoPontoConsumo> listaContratoPontoConsumo) {

		this.listaContratoPontoConsumo = listaContratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getListaContratoCliente()
	 */
	@Override
	public Collection<ContratoCliente> getListaContratoCliente() {

		return listaContratoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setListaContratoCliente
	 * (java.util.Collection)
	 */
	@Override
	public void setListaContratoCliente(Collection<ContratoCliente> listaContratoCliente) {

		this.listaContratoCliente = listaContratoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getListaContratoQDC()
	 */
	@Override
	public Collection<ContratoQDC> getListaContratoQDC() {

		return listaContratoQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setListaContratoQDC(java.util.Collection)
	 */
	@Override
	public void setListaContratoQDC(Collection<ContratoQDC> listaContratoQDC) {

		this.listaContratoQDC = listaContratoQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getConsumoReferenciaSobreDemanda
	 * ()
	 */
	public EntidadeConteudo getConsumoReferenciaSobreDemanda() {

		return consumoReferenciaSobreDemanda;
	}
	
	/**
	 * Setar consumoReferenciaSobreDemanda
	 * @param consumoReferenciaSobreDemanda - {@link EntidadeConteudo}
	 */
	public void setConsumoReferenciaSobreDemanda( EntidadeConteudo consumoReferenciaSobreDemanda) {
		this.consumoReferenciaSobreDemanda = consumoReferenciaSobreDemanda;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDiasAntecedenciaRevisaoGarantiaFinanceira
	 * ()
	 */
	@Override
	public Integer getDiasAntecedenciaRevisaoGarantiaFinanceira() {

		return diasAntecedenciaRevisaoGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDiasAntecedenciaRevisaoGarantiaFinanceira
	 * (java.lang.Integer)
	 */
	@Override
	public void setDiasAntecedenciaRevisaoGarantiaFinanceira(Integer diasAntecedenciaRevisaoGarantiaFinanceira) {

		this.diasAntecedenciaRevisaoGarantiaFinanceira = diasAntecedenciaRevisaoGarantiaFinanceira;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getPeriodicidadeReavaliacaoGarantias()
	 */
	@Override
	public Integer getPeriodicidadeReavaliacaoGarantias() {

		return periodicidadeReavaliacaoGarantias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setPeriodicidadeReavaliacaoGarantias
	 * (java.lang.Integer)
	 */
	@Override
	public void setPeriodicidadeReavaliacaoGarantias(Integer periodicidadeReavaliacaoGarantias) {

		this.periodicidadeReavaliacaoGarantias = periodicidadeReavaliacaoGarantias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getDiasAntecedenciaRenovacao()
	 */
	@Override
	public Integer getDiasAntecedenciaRenovacao() {

		return diasAntecedenciaRenovacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setDiasAntecedenciaRenovacao
	 * (java.lang.Integer)
	 */
	@Override
	public void setDiasAntecedenciaRenovacao(Integer diasAntecedenciaRenovacao) {

		this.diasAntecedenciaRenovacao = diasAntecedenciaRenovacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getValorParticipacaoCliente()
	 */
	@Override
	public BigDecimal getValorParticipacaoCliente() {

		return valorParticipacaoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setValorParticipacaoCliente
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorParticipacaoCliente(BigDecimal valorParticipacaoCliente) {

		this.valorParticipacaoCliente = valorParticipacaoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getQtdParcelasFinanciamento()
	 */
	@Override
	public Integer getQtdParcelasFinanciamento() {

		return qtdParcelasFinanciamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setQtdParcelasFinanciamento
	 * (java.lang.Integer)
	 */
	@Override
	public void setQtdParcelasFinanciamento(Integer qtdParcelasFinanciamento) {

		this.qtdParcelasFinanciamento = qtdParcelasFinanciamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getPercentualJurosFinanciamento()
	 */
	@Override
	public BigDecimal getPercentualJurosFinanciamento() {

		return percentualJurosFinanciamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setPercentualJurosFinanciamento
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualJurosFinanciamento(BigDecimal percentualJurosFinanciamento) {

		this.percentualJurosFinanciamento = percentualJurosFinanciamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getSistemaAmortizacao()
	 */
	@Override
	public EntidadeConteudo getSistemaAmortizacao() {

		return sistemaAmortizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setSistemaAmortizacao
	 * (br.com.ggas.geral.EntidadeConteudo
	 * )
	 */
	@Override
	public void setSistemaAmortizacao(EntidadeConteudo sistemaAmortizacao) {

		this.sistemaAmortizacao = sistemaAmortizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getPercentualTarifaDoP()
	 */
	@Override
	public BigDecimal getPercentualTarifaDoP() {

		return percentualTarifaDoP;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setPercentualTarifaDoP
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualTarifaDoP(BigDecimal percentualTarifaDoP) {

		this.percentualTarifaDoP = percentualTarifaDoP;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * isIndicadorAnoContratual()
	 */
	@Override
	public boolean getIndicadorAnoContratual() {

		return indicadorAnoContratual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setIndicadorAnoContratual(boolean)
	 */
	@Override
	public void setIndicadorAnoContratual(boolean indicadorAnoContratual) {

		this.indicadorAnoContratual = indicadorAnoContratual;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getIndicadorDebitoAutomatico()
	 */
	@Override
	public boolean getIndicadorDebitoAutomatico() {
		
		return indicadorDebitoAutomatico;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setIndicadorDebitoAutomatico(boolean)
	 */
	@Override
	public void setIndicadorDebitoAutomatico(boolean indicadorDebitoAutomatico) {
		
		this.indicadorDebitoAutomatico = indicadorDebitoAutomatico;
	}

	@Override public Boolean getIndicadorParticipanteECartas() {
		return indicadorParticipanteECartas;
	}

	@Override public void setIndicadorParticipanteECartas(Boolean indicadorParticipanteECartas) {
		this.indicadorParticipanteECartas = indicadorParticipanteECartas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getFormaCobranca()
	 */
	@Override
	public EntidadeConteudo getFormaCobranca() {

		return formaCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setFormaCobranca
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setFormaCobranca(EntidadeConteudo formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getChavePrimariaPai()
	 */
	@Override
	public Long getChavePrimariaPai() {

		return chavePrimariaPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setChavePrimariaPai(java.lang.Integer)
	 */
	@Override
	public void setChavePrimariaPai(Long chavePrimariaPai) {

		this.chavePrimariaPai = chavePrimariaPai;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getNumeroFormatado() {

		StringBuilder numeroFormatado = new StringBuilder();
		if (this.getAnoContrato() != null) {
			numeroFormatado.append(this.getAnoContrato().toString());
		}
		numeroFormatado.append(String.format("%05d", this.getNumero()));
		numeroFormatado.append("-");
		numeroFormatado.append(String.format("%02d", this.getNumeroAditivo()));
		return numeroFormatado.toString();
	}

	@Override
	public String getNumeroContratoComAditivo() {

		StringBuilder numeroFormatado = new StringBuilder();
		numeroFormatado.append(this.getNumero());
		numeroFormatado.append(this.getNumeroAditivo());
		return numeroFormatado.toString();
	}

	@Override
	public String getNumeroFormatadoSemAditivo() {

		StringBuilder numeroFormatado = new StringBuilder();
		if (this.getAnoContrato() != null) {
			numeroFormatado.append(this.getAnoContrato().toString());
		}
		numeroFormatado.append(String.format("%05d", this.getNumero()));
		return numeroFormatado.toString();
	}

	/**
	 * @return listaFatura
	 */
	@Override
	public Collection<Fatura> getListaFatura() {

		return listaFatura;
	}

	/**
	 * @param listaFatura
	 */
	@Override
	public void setListaFatura(Collection<Fatura> listaFatura) {

		this.listaFatura = listaFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getTipoAgrupamento()
	 */
	@Override
	public EntidadeConteudo getTipoAgrupamento() {

		return tipoAgrupamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setTipoAgrupamento
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoAgrupamento(EntidadeConteudo tipoAgrupamento) {

		this.tipoAgrupamento = tipoAgrupamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * getPercentualSobreTariGas()
	 */
	@Override
	public BigDecimal getPercentualSobreTariGas() {

		return percentualSobreTariGas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.Contrato#
	 * setPercentualSobreTariGas
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualSobreTariGas(BigDecimal percentualSobreTariGas) {

		this.percentualSobreTariGas = percentualSobreTariGas;
	}

	/**
	 * @param valorInvestimento
	 *            the valorInvestimento to set
	 */
	@Override
	public void setValorInvestimento(BigDecimal valorInvestimento) {

		this.valorInvestimento = valorInvestimento;
	}

	/**
	 * @return the valorInvestimento
	 */
	@Override
	public BigDecimal getValorInvestimento() {

		return valorInvestimento;
	}

	/**
	 * @param dataInvestimento
	 *            the dataInvestimento to set
	 */
	@Override
	public void setDataInvestimento(Date dataInvestimento) {
		if(dataInvestimento != null){
			this.dataInvestimento = (Date) dataInvestimento.clone();
		} else {
			this.dataInvestimento = null;
		}
	}

	/**
	 * @return the dataInvestimento
	 */
	@Override
	public Date getDataInvestimento() {
		Date data = null;
		if(dataInvestimento != null) {
			data = (Date) dataInvestimento.clone();
		}
		return data;
	}

	/**
	 * @param multaRecisoria
	 *            the multaRecisoria to set
	 */
	@Override
	public void setMultaRecisoria(EntidadeConteudo multaRecisoria) {

		this.multaRecisoria = multaRecisoria;
	}

	/**
	 * @return the multaRecisoria
	 */
	@Override
	public EntidadeConteudo getMultaRecisoria() {

		return multaRecisoria;
	}

	/**
	 * @return the faturaEncerramentoGerada
	 */
	@Override
	public Boolean getFaturaEncerramentoGerada() {

		return faturaEncerramentoGerada;
	}

	/**
	 * @param faturaEncerramentoGerada
	 *            the faturaEncerramentoGerada to
	 *            set
	 */
	@Override
	public void setFaturaEncerramentoGerada(Boolean faturaEncerramentoGerada) {

		this.faturaEncerramentoGerada = faturaEncerramentoGerada;
	}

	/**
	 * @param dataRecisao
	 *            the dataRecisao to set
	 */
	@Override
	public void setDataRecisao(Date dataRecisao) {
		if(dataRecisao != null) {
			this.dataRecisao = (Date) dataRecisao.clone();
		} else {
			this.dataRecisao = null;
		}
	}

	/**
	 * @return the dataRecisao
	 */
	@Override
	public Date getDataRecisao() {
		Date data = null;
		if (dataRecisao != null) {
			data = (Date) dataRecisao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getPercentualJurosMora()
	 */
	@Override
	public BigDecimal getPercentualJurosMora() {

		return percentualJurosMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPercentualJurosMora(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualJurosMora(BigDecimal percentualJurosMora) {

		this.percentualJurosMora = percentualJurosMora;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getPercentualMulta()
	 */
	@Override
	public BigDecimal getPercentualMulta() {

		return percentualMulta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPercentualMulta(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualMulta(BigDecimal percentualMulta) {

		this.percentualMulta = percentualMulta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getTipoPeriodicidadePenalidade()
	 */
	@Override
	public Boolean getTipoPeriodicidadePenalidade() {

		return tipoPeriodicidadePenalidade;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setTipoPeriodicidadePenalidade(java.lang.Boolean)
	 */
	@Override
	public void setTipoPeriodicidadePenalidade(Boolean tipoPeriodicidadePenalidade) {

		this.tipoPeriodicidadePenalidade = tipoPeriodicidadePenalidade;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getArrecadadorContratoConvenio()
	 */
	@Override
	public ArrecadadorContratoConvenio getArrecadadorContratoConvenio() {

		return arrecadadorContratoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setArrecadadorContratoConvenio(br.com.ggas.arrecadacao.ArrecadadorContratoConvenio)
	 */
	@Override
	public void setArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) {

		this.arrecadadorContratoConvenio = arrecadadorContratoConvenio;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getArrecadadorContratoConvenioDebitoAutomatico()
	 */
	@Override
	public ArrecadadorContratoConvenio getArrecadadorContratoConvenioDebitoAutomatico() {
		
		return arrecadadorContratoConvenioDebitoAutomatico;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setArrecadadorContratoConvenioDebitoAutomatico(br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio)
	 */
	@Override
	public void setArrecadadorContratoConvenioDebitoAutomatico(ArrecadadorContratoConvenio arrecadadorContratoConvenioDebitoAutomatico) {
		
		this.arrecadadorContratoConvenioDebitoAutomatico = arrecadadorContratoConvenioDebitoAutomatico;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getListaServicoAutorizacao()
	 */
	@Override
	public Collection<ServicoAutorizacao> getListaServicoAutorizacao() {

		return listaServicoAutorizacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setListaServicoAutorizacao(java.util.Collection)
	 */
	@Override
	public void setListaServicoAutorizacao(Collection<ServicoAutorizacao> listaServicoAutorizacao) {

		this.listaServicoAutorizacao = listaServicoAutorizacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getPeriodicidadeMaiorMenor()
	 */
	@Override
	public EntidadeConteudo getPeriodicidadeMaiorMenor() {

		return periodicidadeMaiorMenor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPeriodicidadeMaiorMenor(br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setPeriodicidadeMaiorMenor(EntidadeConteudo periodicidadeMaiorMenor) {

		this.periodicidadeMaiorMenor = periodicidadeMaiorMenor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getDescricaoContrato()
	 */
	@Override
	public String getDescricaoContrato() {

		return descricaoContrato;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setDescricaoContrato(java.lang.String)
	 */
	@Override
	public void setDescricaoContrato(String descricaoContrato) {

		this.descricaoContrato = descricaoContrato;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getVolumeReferencia()
	 */
	@Override
	public BigDecimal getVolumeReferencia() {

		return volumeReferencia;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setVolumeReferencia(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeReferencia(BigDecimal volumeReferencia) {

		this.volumeReferencia = volumeReferencia;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getChavePrimariaPrincipal()
	 */
	@Override
	public Long getChavePrimariaPrincipal() {

		return chavePrimariaPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setChavePrimariaPrincipal(java.lang.Long)
	 */
	@Override
	public void setChavePrimariaPrincipal(Long chavePrimariaPrincipal) {

		this.chavePrimariaPrincipal = chavePrimariaPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getOrdemFaturamento()
	 */
	@Override
	public Integer getOrdemFaturamento() {

		return ordemFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setOrdemFaturamento(java.lang.Integer)
	 */
	@Override
	public void setOrdemFaturamento(Integer ordemFaturamento) {

		this.ordemFaturamento = ordemFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getListaContratoPenalidade()
	 */
	@Override
	public Collection<ContratoPenalidade> getListaContratoPenalidade() {

		return listaContratoPenalidade;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setListaContratoPenalidade(java.util.Collection)
	 */
	@Override
	public void setListaContratoPenalidade(Collection<ContratoPenalidade> listaContratoPenalidade) {

		this.listaContratoPenalidade = listaContratoPenalidade;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getDataInicioRetiradaQPNR()
	 */
	@Override
	public Date getDataInicioRetiradaQPNR() {
		Date data = null;
		if (dataInicioRetiradaQPNR != null) {
			data = (Date) dataInicioRetiradaQPNR.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setDataInicioRetiradaQPNR(java.util.Date)
	 */
	@Override
	public void setDataInicioRetiradaQPNR(Date dataInicioRetiradaQPNR) {
		if (dataInicioRetiradaQPNR != null) {
			this.dataInicioRetiradaQPNR = (Date) dataInicioRetiradaQPNR.clone();
		} else {
			this.dataInicioRetiradaQPNR = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getDataFimRetiradaQPNR()
	 */
	@Override
	public Date getDataFimRetiradaQPNR() {
		Date data = null;
		if (dataFimRetiradaQPNR != null) {
			data = (Date) dataFimRetiradaQPNR.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setDataFimRetiradaQPNR(java.util.Date)
	 */
	@Override
	public void setDataFimRetiradaQPNR(Date dataFimRetiradaQPNR) {
		if (dataFimRetiradaQPNR != null) {
			this.dataFimRetiradaQPNR = (Date) dataFimRetiradaQPNR.clone();
		} else {
			this.dataFimRetiradaQPNR = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getPercentualQDCContratoQPNR()
	 */
	@Override
	public BigDecimal getPercentualQDCContratoQPNR() {

		return percentualQDCContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPercentualQDCContratoQPNR(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualQDCContratoQPNR(BigDecimal percentualQDCContratoQPNR) {

		this.percentualQDCContratoQPNR = percentualQDCContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getPercentualMinimoQDCContratoQPNR()
	 */
	@Override
	public BigDecimal getPercentualMinimoQDCContratoQPNR() {

		return percentualMinimoQDCContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPercentualMinimoQDCContratoQPNR(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualMinimoQDCContratoQPNR(BigDecimal percentualMinimoQDCContratoQPNR) {

		this.percentualMinimoQDCContratoQPNR = percentualMinimoQDCContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getPercentualQDCFimContratoQPNR()
	 */
	@Override
	public BigDecimal getPercentualQDCFimContratoQPNR() {

		return percentualQDCFimContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPercentualQDCFimContratoQPNR(java.math.BigDecimal)
	 */
	@Override
	public void setPercentualQDCFimContratoQPNR(BigDecimal percentualQDCFimContratoQPNR) {

		this.percentualQDCFimContratoQPNR = percentualQDCFimContratoQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getAnosValidadeRetiradaQPNR()
	 */
	@Override
	public Integer getAnosValidadeRetiradaQPNR() {

		return anosValidadeRetiradaQPNR;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setAnosValidadeRetiradaQPNR(java.lang.Integer)
	 */
	@Override
	public void setAnosValidadeRetiradaQPNR(Integer anosValidadeRetiradaQPNR) {

		this.anosValidadeRetiradaQPNR = anosValidadeRetiradaQPNR;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#isPropostaAprovada()
	 */
	public boolean isPropostaAprovada() {
		return propostaAprovada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setPropostaAprovada(boolean)
	 */
	public void setPropostaAprovada(boolean propostaAprovada) {
		this.propostaAprovada = propostaAprovada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#isEnderecoPadrao()
	 */
	public boolean isEnderecoPadrao() {
		return enderecoPadrao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setEnderecoPadrao(boolean)
	 */
	public void setEnderecoPadrao(boolean enderecoPadrao) {
		this.enderecoPadrao = enderecoPadrao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#isExigeAprovacao()
	 */
	public boolean isExigeAprovacao() {
		return exigeAprovacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setExigeAprovacao(boolean)
	 */
	public void setExigeAprovacao(boolean exigeAprovacao) {
		this.exigeAprovacao = exigeAprovacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#getAnexos()
	 */
	public Collection<ContratoAnexo> getAnexos() {
		return anexos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#setAnexos(java.util.Collection)
	 */
	public void setAnexos(Collection<ContratoAnexo> anexos) {
		this.anexos = anexos;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.contrato.contrato.Contrato#substituirListaAnexo()
	 */
	@Override
	public void substituirListaAnexo() {

		HashSet<ContratoAnexo> listaAnexo = new HashSet<>();

		for (ContratoAnexo anexo : this.anexos) {
			ContratoAnexo contratoAnexo = new ContratoAnexo();

			contratoAnexo.setDadosAuditoria(anexo.getDadosAuditoria());
			contratoAnexo.setUltimaAlteracao(new Date());
			contratoAnexo.setVersao(anexo.getVersao());
			contratoAnexo.setChavePrimaria(0);
			contratoAnexo.setContrato(this);
			contratoAnexo.setDescricaoAnexo(anexo.getDescricaoAnexo());
			contratoAnexo.setDocumentoAnexo(anexo.getDocumentoAnexo());
			contratoAnexo.setHabilitado(true);
			contratoAnexo.setNomeArquivo(anexo.getNomeArquivo());

			listaAnexo.add(contratoAnexo);
		}

		this.setAnexos(listaAnexo);
	}

	@Override
	public Banco getBanco() {
		return banco;
	}

	@Override
	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	@Override public String getAgencia() {
		return agencia;
	}

	@Override public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	@Override public String getContaCorrente() {
		return contaCorrente;
	}

	@Override public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	@Override
	public BigDecimal getIncentivosComerciais() {
		return incentivosComerciais;
	}

	@Override
	public void setIncentivosComerciais(BigDecimal incentivosComerciais) {
		this.incentivosComerciais = incentivosComerciais;
	}

	@Override
	public BigDecimal getIncentivosInfraestrutura() {
		return incentivosInfraestrutura;
	}

	@Override
	public void setIncentivosInfraestrutura(BigDecimal incentivosInfraestrutura) {
		this.incentivosInfraestrutura = incentivosInfraestrutura;
	}

	@Override
	public Integer getPrazoVigencia() {
		return prazoVigencia;
	}

	@Override
	public void setPrazoVigencia(Integer prazoVigencia) {
		this.prazoVigencia = prazoVigencia;
	}
	
	
}
