/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ContratoPenalidade.
 */
public interface ContratoPenalidade extends EntidadeNegocio {

	/** The bean id contrato penalidade. */
	String BEAN_ID_CONTRATO_PENALIDADE = "contratoPenalidade";

	/** The percentual margem variacao. */
	String PERCENTUAL_MARGEM_VARIACAO = "CONTRATO_PERCENTUAL_MARGEM_VARIACAO";

	/** The percentual maximo qdc. */
	String PERCENTUAL_MAXIMO_QDC = "CONTRATO_PERCENTUAL_MAXIMO_QDC";

	/**
	 * Gets the percentual margem variacao.
	 *
	 * @return the percentual margem variacao
	 */
	BigDecimal getPercentualMargemVariacao();

	/**
	 * Sets the percentual margem variacao.
	 *
	 * @param percentualMargemVariacao the new percentual margem variacao
	 */
	void setPercentualMargemVariacao(BigDecimal percentualMargemVariacao);

	/**
	 * Gets the periodicidade penalidade.
	 *
	 * @return the periodicidade penalidade
	 */
	EntidadeConteudo getPeriodicidadePenalidade();

	/**
	 * Sets the periodicidade penalidade.
	 *
	 * @param periodicidadePenalidade the new periodicidade penalidade
	 */
	void setPeriodicidadePenalidade(EntidadeConteudo periodicidadePenalidade);

	/**
	 * Gets the consumo referencia.
	 *
	 * @return the consumo referencia
	 */
	EntidadeConteudo getConsumoReferencia();

	/**
	 * Sets the consumo referencia.
	 *
	 * @param consumoReferencia the new consumo referencia
	 */
	void setConsumoReferencia(EntidadeConteudo consumoReferencia);

	/**
	 * Gets the contrato.
	 *
	 * @return the contrato
	 */
	Contrato getContrato();

	/**
	 * Sets the contrato.
	 *
	 * @param contrato the new contrato
	 */
	void setContrato(Contrato contrato);

	/**
	 * Gets the penalidade.
	 *
	 * @return the penalidade
	 */
	Penalidade getPenalidade();

	/**
	 * Sets the penalidade.
	 *
	 * @param penalidade the new penalidade
	 */
	void setPenalidade(Penalidade penalidade);

	/**
	 * Gets the percentual margem variacao formatado.
	 *
	 * @return the percentual margem variacao formatado
	 * @throws GGASException the GGAS exception
	 */
	String getPercentualMargemVariacaoFormatado() throws GGASException;

	/**
	 * Gets the referencia qf parada programada.
	 *
	 * @return the referencia qf parada programada
	 */
	EntidadeConteudo getReferenciaQFParadaProgramada();

	/**
	 * Sets the referencia qf parada programada.
	 *
	 * @param referenciaQFParadaProgramada the new referencia qf parada programada
	 */
	void setReferenciaQFParadaProgramada(EntidadeConteudo referenciaQFParadaProgramada);

	/**
	 * Gets the data inicio vigencia.
	 *
	 * @return the data inicio vigencia
	 */
	Date getDataInicioVigencia();

	/**
	 * Sets the data inicio vigencia.
	 *
	 * @param dataInicioVigencia the new data inicio vigencia
	 */
	void setDataInicioVigencia(Date dataInicioVigencia);

	/**
	 * Gets the data fim vigencia.
	 *
	 * @return the data fim vigencia
	 */
	Date getDataFimVigencia();

	/**
	 * Sets the data fim vigencia.
	 *
	 * @param dataFimVigencia the new data fim vigencia
	 */
	void setDataFimVigencia(Date dataFimVigencia);

	/**
	 * Gets the percentual nao recuperavel.
	 *
	 * @return the percentual nao recuperavel
	 */
	BigDecimal getPercentualNaoRecuperavel();

	/**
	 * Sets the percentual nao recuperavel.
	 *
	 * @param percentualNaoRecuperavel the new percentual nao recuperavel
	 */
	void setPercentualNaoRecuperavel(BigDecimal percentualNaoRecuperavel);

	/**
	 * Gets the considera parada programada.
	 *
	 * @return the considera parada programada
	 */
	Boolean getConsideraParadaProgramada();

	/**
	 * Sets the considera parada programada.
	 *
	 * @param consideraParadaProgramada the new considera parada programada
	 */
	void setConsideraParadaProgramada(Boolean consideraParadaProgramada);

	/**
	 * Gets the considera falha fornecimento.
	 *
	 * @return the considera falha fornecimento
	 */
	Boolean getConsideraFalhaFornecimento();

	/**
	 * Sets the considera falha fornecimento.
	 *
	 * @param consideraFalhaFornecimento the new considera falha fornecimento
	 */
	void setConsideraFalhaFornecimento(Boolean consideraFalhaFornecimento);

	/**
	 * Gets the considera caso fortuito.
	 *
	 * @return the considera caso fortuito
	 */
	Boolean getConsideraCasoFortuito();

	/**
	 * Sets the considera caso fortuito.
	 *
	 * @param consideraCasoFortuito the new considera caso fortuito
	 */
	void setConsideraCasoFortuito(Boolean consideraCasoFortuito);

	/**
	 * Gets the recuperavel.
	 *
	 * @return the recuperavel
	 */
	Boolean getRecuperavel();

	/**
	 * Sets the recuperavel.
	 *
	 * @param recuperavel the new recuperavel
	 */
	void setRecuperavel(Boolean recuperavel);

	/**
	 * Gets the tipo apuracao.
	 *
	 * @return the tipo apuracao
	 */
	EntidadeConteudo getTipoApuracao();

	/**
	 * Sets the tipo apuracao.
	 *
	 * @param tipoApuracao the new tipo apuracao
	 */
	void setTipoApuracao(EntidadeConteudo tipoApuracao);

	/**
	 * Gets the preco cobranca.
	 *
	 * @return the preco cobranca
	 */
	EntidadeConteudo getPrecoCobranca();

	/**
	 * Sets the preco cobranca.
	 *
	 * @param precoCobranca the new preco cobranca
	 */
	void setPrecoCobranca(EntidadeConteudo precoCobranca);

	/**
	 * Gets the apuracao parada programada.
	 *
	 * @return the apuracao parada programada
	 */
	EntidadeConteudo getApuracaoParadaProgramada();

	/**
	 * Sets the apuracao parada programada.
	 *
	 * @param apuracaoParadaProgramada the new apuracao parada programada
	 */
	void setApuracaoParadaProgramada(EntidadeConteudo apuracaoParadaProgramada);

	/**
	 * Gets the apuracao caso fortuito.
	 *
	 * @return the apuracao caso fortuito
	 */
	EntidadeConteudo getApuracaoCasoFortuito();

	/**
	 * Sets the apuracao caso fortuito.
	 *
	 * @param apuracaoCasoFortuito the new apuracao caso fortuito
	 */
	void setApuracaoCasoFortuito(EntidadeConteudo apuracaoCasoFortuito);

	/**
	 * Gets the apuracao falha fornecimento.
	 *
	 * @return the apuracao falha fornecimento
	 */
	EntidadeConteudo getApuracaoFalhaFornecimento();

	/**
	 * Sets the apuracao falha fornecimento.
	 *
	 * @param apuracaoFalhaFornecimento the new apuracao falha fornecimento
	 */
	void setApuracaoFalhaFornecimento(EntidadeConteudo apuracaoFalhaFornecimento);

	/**
	 * Gets the base apuracao.
	 *
	 * @return the base apuracao
	 */
	EntidadeConteudo getBaseApuracao();

	/**
	 * Sets the base apuracao.
	 *
	 * @param baseApuracao the new base apuracao
	 */
	void setBaseApuracao(EntidadeConteudo baseApuracao);

	/**
	 * Gets the valor percentual cob ret maior menor.
	 *
	 * @return the valor percentual cob ret maior menor
	 */
	BigDecimal getValorPercentualCobRetMaiorMenor();

	/**
	 * Sets the valor percentual cob ret maior menor.
	 *
	 * @param valorPercentualCobRetMaiorMenor the new valor percentual cob ret maior menor
	 */
	void setValorPercentualCobRetMaiorMenor(BigDecimal valorPercentualCobRetMaiorMenor);

	/**
	 * Gets the valor percentual cob int ret maior menor.
	 *
	 * @return the valor percentual cob int ret maior menor
	 */
	BigDecimal getValorPercentualCobIntRetMaiorMenor();

	/**
	 * Sets the valor percentual cob int ret maior menor.
	 *
	 * @param valorPercentualCobIntRetMaiorMenor the new valor percentual cob int ret maior menor
	 */
	void setValorPercentualCobIntRetMaiorMenor(BigDecimal valorPercentualCobIntRetMaiorMenor);

	/**
	 * Gets the valor percentual ret maior menor.
	 *
	 * @return the valor percentual ret maior menor
	 */
	BigDecimal getValorPercentualRetMaiorMenor();

	/**
	 * Sets the valor percentual ret maior menor.
	 *
	 * @param valorPercentualRetMaiorMenor the new valor percentual ret maior menor
	 */
	void setValorPercentualRetMaiorMenor(BigDecimal valorPercentualRetMaiorMenor);

	/**
	 * Gets the indicador imposto.
	 *
	 * @return the indicador imposto
	 */
	Boolean getIndicadorImposto();

	/**
	 * Sets the indicador imposto.
	 *
	 * @param indicadorImposto the new indicador imposto
	 */
	void setIndicadorImposto(Boolean indicadorImposto);

}
