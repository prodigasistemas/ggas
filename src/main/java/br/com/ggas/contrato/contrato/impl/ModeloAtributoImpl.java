/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ModeloAtributoImpl representa uma ModeloAtributoImpl no sistema.
 *
 * @since 17/12/2009
 * 
 */

package br.com.ggas.contrato.contrato.impl;

import java.util.Map;

import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelos atributos e implementação dos métodos relacionados ao Modelo de Atributo.
 *
 */
public class ModeloAtributoImpl extends EntidadeNegocioImpl implements ModeloAtributo {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6677161199160152492L;

	private boolean obrigatorio;

	private boolean valorFixo;

	private String padrao;

	private ModeloContrato modeloContrato;

	private AbaAtributo abaAtributo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo#isObrigatorio()
	 */
	@Override
	public boolean isObrigatorio() {

		return obrigatorio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo#setObrigatorio(boolean)
	 */
	@Override
	public void setObrigatorio(boolean obrigatorio) {

		this.obrigatorio = obrigatorio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo#getPadrao()
	 */
	@Override
	public String getPadrao() {

		return padrao;
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ModeloAtributo#isPadraoNaoNulo()
	 */
	@Override
	public boolean isPadraoNulo() {
		return padrao == null || "-1".equals(padrao);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo#setPadrao(java.lang.String)
	 */
	@Override
	public void setPadrao(String padrao) {

		this.padrao = padrao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo#getModeloContrato()
	 */
	@Override
	public ModeloContrato getModeloContrato() {

		return modeloContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo
	 * #setModeloContrato(br.com.ggas
	 * .contrato.contrato.ModeloContrato)
	 */
	@Override
	public void setModeloContrato(ModeloContrato modeloContrato) {

		this.modeloContrato = modeloContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo#getAbaAtributo()
	 */
	@Override
	public AbaAtributo getAbaAtributo() {

		return abaAtributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.impl.
	 * ModeloAtributo
	 * #setAbaAtributo(br.com.ggas.contrato
	 * .contrato.AbaAtributo)
	 */
	@Override
	public void setAbaAtributo(AbaAtributo abaAtributo) {

		this.abaAtributo = abaAtributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public boolean isValorFixo() {
		return this.valorFixo;
	}

	@Override
	public void setValorFixo(boolean valorFixo) {
		this.valorFixo = valorFixo;
	}

}
