/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato.impl;

import static br.com.ggas.util.Constantes.FORMATO_DATA_BR;
import static br.com.ggas.web.contrato.contrato.ContratoAction.ALTERADO;
import static br.com.ggas.web.contrato.contrato.ContratoAction.ATIVO;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Aba;
import br.com.ggas.contrato.contrato.AbaAtributo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSAmostragem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.ControladorModeloContrato;
import br.com.ggas.contrato.contrato.ModeloAtributo;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.contrato.contrato.PenalidadeDemanda;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.UnidadeConversao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Constantes.NomeClienteAbreviado;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.contrato.CalculoIndenizacaoVO;
import br.com.ggas.web.contrato.contrato.CompromissoTakeOrPayVO;
import br.com.ggas.web.contrato.contrato.ModalidadeConsumoFaturamentoVO;
import groovy.lang.GroovyShell;
/**
 * Classe responsável pelos atributos e implementação dos métodos relacionados ao Controlado de Contrato.
 *
 */
public class ControladorContratoImpl extends ControladorNegocioImpl implements ControladorContrato {

	private static final double CONSTANTE_TRINTA_DIAS = 30D;

	private static final long CONSTANTE_NUMERO_DOIS = 2L;

	private static final String VAZÃO_INSTANTÂNEA = "Vazão Instantânea";

	private static final String REGIME_CONSUMO = "regimeConsumo";

	private static final String CONSUMO_REFERENCIA = "consumoReferencia";

	private static final String INDICE_FINANCEIRO = "indiceFinanceiro";

	private static final String PONTO_CONSUMO_CONTRATO = "contrato";

	private static final String CONTRATO_PONTO_CONSUMO_MODALIDADE = "contratoPontoConsumoModalidade";

	private static final String DATA_CONSULTA_FINAL = "dataConsultaFinal";

	private static final String DATA_CONSULTA_INICIAL = "dataConsultaInicial";

	private static final String CONTRATO_PONTO_MODALIDADE = " contratoPontoModalidade ";

	private static final String CONTRATO_PONTO_CONSUMO2 = " contratoPontoConsumo";

	private static final String CONTRATO_MODALIDADE = "contratoModalidade";

	private static final String DATA_REFERENCIA = "dataReferencia";

	private static final String CONTRATO_PONTO_CONSUMO = " contratoPontoConsumo ";

	private static final String CONTRATO_PONTO = " contratoPonto ";

	private static final String CONTRATO = " contrato ";

	private static final String WHERE = " where ";

	private static final String FROM = " from ";

	private static final String AND = " and ";

	private static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";

	private static final String PT_CONS = "PT_CONS";

	private static final int QUATRO_CASAS_DECIMAIS = 4;

	private static final String HABILITADO_UPPER = "HABILITADO";

	private static final String PERCENTUAL_DE_MULTA = "Percentual de Multa";

	private static final String PERCENTUAL_DE_JUROS_DE_MORA = "Percentual de Juros de Mora";

	private static final String ZERO = "0";

	private static final String NUM_DIAS_RENO_AUTO_CONTRATO = "numDiasRenoAutoContrato";

	private static final String RENOVACAO_AUTOMATICA = "renovacaoAutomatica";

	private static final String UNIDADE_FORNEC_MINIMO_DIARIO = "unidadeFornecMinimoDiario";

	private static final String UNIDADE_FORNEC_MAXIMO_DIARIO = "unidadeFornecMaximoDiario";

	private static final String UNIDADE_PRESSAO_MAXIMA_FORNEC = "unidadePressaoMaximaFornec";

	private static final String UNIDADE_PRESSAO_MINIMA_FORNEC = "unidadePressaoMinimaFornec";

	private static final String UNIDADE_VAZAO_INSTAN_MINIMA = "unidadeVazaoInstanMinima";

	private static final String PARAMETRO = "parametro";

	private static final String RETURN = "return";

	private static final String UNIDADE_VAZAO_INSTAN_MAXIMA = "unidadeVazaoInstanMaxima";

	private static final String UNIDADE_VAZAO_INSTAN = "unidadeVazaoInstan";

	private static final String FORNECIMENTO_MAXIMO = "Fornecimento Máximo";

	private static final String FORNECIMENTO_MINIMO = "Fornecimento Mínimo";

	private static final String PRESSAO_MAXIMA = "Pressão Máxima";

	private static final String PRESSAO_MINIMA = "Pressão Mínima";

	private static final String VAZAO_MAXIMA = "Vazão Máxima";

	private static final String VAZAO_MINIMA = "Vazão Mínima";

	private static final String VAZAO_INSTANTANEA2 = "Vazão Instantanea";

	private static final String FORNECIMENTO_MAXIMO_DIARIO = "fornecimentoMaximoDiario";

	private static final String FORNECIMENTO_MINIMO_DIARIO = "fornecimentoMinimoDiario";

	private static final String FAIXA_PRESSAO_FORNECIMENTO = "faixaPressaoFornecimento";

	private static final String PRESSAO_LIMITE_FORNECIMENTO = "pressaoLimiteFornecimento";

	private static final String PRESSAO_MAXIMA_FORNECIMENTO = "pressaoMaximaFornecimento";

	private static final String PRESSAO_MINIMA_FORNECIMENTO = "pressaoMinimaFornecimento";

	private static final String VAZAO_INSTANTANEA_MAXIMA = "vazaoInstantaneaMaxima";

	private static final String VAZAO_INSTANTANEA_MINIMA = "vazaoInstantaneaMinima";

	private static final String VAZAO_INSTANTANEA = "vazaoInstantanea";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String CHAVE_PRIMARIA_CONTRATO_PONTO_CONSUMO_MODALIDADE = "chavePrimariaContratoPontoConsumoModalidade";

	private static final String ANO_CONTRATO = "anoContrato";

	private static final String NUMERO = "numero";

	private static final String CHAVE_PRIMARIA_CONTRATO_PONTO_CONSUMO = "chavePrimariaContratoPontoConsumo";

	private static final String CHAVE_PRIMARIA_CONTRATO = "chavePrimariaContrato";

	private static final String CHAVE_PONTO_CONSUMO = "chavePontoConsumo";

	private static final String CHAVE_CONTRATO_PRINCIPAL = "chaveContratoPrincipal";

	private static final String CHAVE_CONTRATO = "chaveContrato";

	private static final String SITUACAO_ATIVA = "situacaoAtiva";

	private static final String PERCMINIMO_QDC = "percminimoQDC";

	private static final String OPCAO_VENCIMENTO_ITEM_FATURA = "opcaoVencimentoItemFatura";

	private static final String INDICADOR_VENC_DIA_NAO_UTIL = "indicadorVencDiaNaoUtil";

	private static final String TARIFA_CONSUMO = "tarifaConsumo";

	private static final String DEPOSITO_IDENTIFICADO = "depositoIdentificado";

	private static final String DIA_VENCIMENTO_ITEM_FATURA = "diaVencimentoItemFatura";

	private static final String ITEM_FATURA = "itemFatura";

	private static final String PENALIDADE_RET_MAIOR_MENOR_M = "penalidadeRetMaiorMenorM";

	private static final String PENALIDADE_RET_MAIOR_MENOR = "penalidadeRetMaiorMenor";

	private static final String DATA = "Data";

	private static final String IDS_CONTRATO = "idsContrato";

	private static final String RELACIONAMENTO = "relacionamento";

	private static final String CLIENTE_ASSINATURA = "clienteAssinatura";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String CHAVE_PRIMARIA_PRINCIPAL = "chavePrimariaPrincipal";

	private static final String CHAVE_PRIMARIA_PAI = "chavePrimariaPai";

	private static final String NUMERO_FORMATADO = "numeroFormatado";

	private static final String COLECAO_PAGINADA = "colecaoPaginada";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String IMOVEL = "imovel";

	private static final String IMOVEIS = "imoveis";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String PONTOS_CONSUMO = "pontosConsumo";

	private static final String CONT_PONTOS_CONSUMO = "contPontosConsumo";

	private static final String CONT_PONTOS_CONSUMO_PONTO_CONSUMO = "contPontosConsumo.pontoConsumo";

	private static final String PONTOS_CONSUMO_IMOVEL = "pontosConsumo.imovel";

	private static final String PONTOS_CONSUMO_ROTA = "pontosConsumo.rota";

	private static final String PONTOS_CONSUMO_INSTALACAO_MEDIDOR = "pontosConsumo.instalacaoMedidor";

	private static final String INSTALACAO_MEDIDORES_MEDIDOR = "instalacaoMedidores.medidor";

	private static final String LISTA_CONTRATO_PONTO_CONSUMO = "listaContratoPontoConsumo";

	private static final String ID_IMOVEL = "idImovel";

	private static final String FLUXO_MIGRACAO = "fluxoMigracao";

	private static final String CHAVES_MODELO_CONTRATO = "chavesModeloContrato";

	private static final String TIPO_MODELO = "tipoModelo";

	private static final String ID_TIPO_CONTRATO = "idTipoContrato";

	private static final String ID_MODELO_CONTRATO = "idModeloContrato";

	private static final String ID_SITUACAO_CONTRATO = "idSituacaoContrato";

	private static final String ID_CONTRATO_MODALIDADE = "idContratoModalidade";

	private static final String DATA_VIGENCIA = "dataVigencia";

	private static final String DATA_FIM_VIGENCIA = "dataFimVigencia";

	private static final String DATA_INICIO_VIGENCIA = "dataInicioVigencia";

	private static final String ID_PENALIDADE_RETIRADA_MAIOR_MENOR = "idPenalidadeRetiradaMaiorMenor";

	private static final String ID_CONTRATO_PONTO_CONSUMO = "idContratoPontoConsumo";

	private static final String ID_PERIODICIDADE = "idPeriodicidade";

	private static final String SITUACAO = "situacao";

	private static final String ID_ITEM_FATURA = "idItemFatura";

	private static final String ID_CONTRATO = "idContrato";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ID_SITUACAO = "idSituacao";

	private static final String ID_CLIENTE = "idCliente";

	private static final String HABILITADO = "habilitado";

	private static final String NUMERO_COMPLETO_CONTRATO = "numeroCompletoContrato";

	private static final Logger LOG = Logger.getLogger(ControladorContratoImpl.class);

	public static final String INDICE_FINANCEIRO_IGPM_ABREVIADO = "IGP-M B";

	public static final String CONTRATO_DATA_RECISAO = "CONTRATO_DATA_RECISAO";

	private static final String ATRIBUTO_CONTRATO_DATA_RECISAO = "contrato.dataRecisao";

	private static final String ATRIBUTO_CONTRATO_COMPRA = "contratoCompra";

	private static final String ATRIBUTO_NUMERO_HORA_INICIAL = "numeroHoraInicial";

	private static final String ATRIBUTO_PERIODICIDADE = "periodicidade";

	private static final String ATRIBUTO_PERIODICIDADE_RET_MAIOR_MENOR = "periodicidadeRetMaiorMenor";

	private static final String ALIAS_CONTRATO_CLIENTE = " contratoCliente ";

	private static final String ATRIBUTO_CHAVE_CONTRATO = "contrato.chavePrimaria";

	private static final String ATRIBUTO_CHAVE_PENALIDADE = "penalidade.chavePrimaria";

	private static final String ROTA = "rota";

	private static final String INSTALACAO_MEDIDOR = "instalacaoMedidor";

	private static final String CODIGO_PONTO_CONSUMO_SUPERVISORIO = "codigoPontoConsumoSupervisorio";

	private static final int SOBRE_NFE = 2;

	private static final int TAMANHO_MAXIMO_NOME = 50;

	private static final int DIAS_ANO = 365;

	@Deprecated
	private static final Long CONTRATO_MULTA_POR_QTD_MEDIA = 214L;

	@Deprecated
	private static final Long CONTRATO_MULTA_POR_QTD_MINIMA_CONTRATADA = 213L;

	@Deprecated
	private static final Long CONTRATO_MULTA_POR_TEMPO_RELACIONAMENTO = 212L;

	private static final String NUMERO_ADITIVO = "numeroAditivo";

	private static final String DATA_ADITIVO = "dataAditivo";

	private static final String DESCRICAO_ADITIVO = "descricaoAditivo";

	// Aba Modalidade Consumo Faturamento campos
	// de validação especial
	private static final String QDS_MAIOR_QDC = "qdsMaiorQDC";

	private static final String VARIACAO_SUPERIOR_QDC = "variacaoSuperiorQDC";

	private static final String CONTRATOS_VENCIDOS_NAO_RENOVADOS = "Contratos Vencidos não renovados";

	private static final String CONTRATOS_PROXIMO_VENCIMENTO = "Contratos próximo ao prazo de vencimento";

	private static final String CONTRATOS_RENOVADOS_AUTOMATICAMENTE = "Contratos renovados automaticamente";

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	private static final String BOOLEAN_VERDADE_1 = "1";

	private static final String UNIDADE_PRESSAO_LIMITE_FORNEC = "unidadePressaoLimiteFornec";

	private static final String CHAVE = "chave";

	private static final String ATRIBUTO_LISTA_CONTRATO_CLIENTE = "listaContratoCliente";

	private static final String ATRIBUTO_LISTA_CONTRATO_QDC = "listaContratoQDC";

	private static final String FATURAVEL = "faturavel";
	
	private static final String ID_OPERACAO_MEDIDOR = "idOperacaoMedidor";
	
	private static final String TIPO_ASSOCIACAO = "tipoAssociacao";

	private static final int INDEX_ID_CONTRATO_PONTO_CONUSMO = 0;
	private static final int INDEX_PERIODICIDADE = 1;
	private static final int INDEX_ACAO_ANOR_CONS = 2;
	private static final int INDEX_MEDIDA_FORNEC_MIN_MENSAL = 3;
	private static final int INDEX_NUM_FATOR_CORRECAO = 4;
	private static final int INDEX_UNIDADE_PRESSAO = 5;
	private static final int INDEX_MEDIDA_PRESSAO = 6;
	private static final int INDEX_TIPO_MEDICAO = 7;
	private static final int INDEX_ID_PONTO_CONSUMO = 8;
	private static final int INDEX_ID_CONTRATO = 9;
	private static final int INDEX_FORMA_COBRANCA = 10;
	private static final int INDEX_ID_CLIENTE = 11;
	private static final int INDEX_NOME_CLIENTE = 12;
	private static final int INDEX_UNIDADE_PRESSAO_COLETADA = 13;
	private static final int INDEX_MEDIDA_PRESSAO_COLETADA= 14;
	private static final int INDEX_SITUACAO_CONTRATO = 15;
	private static final int INDEX_FAIXA_PRESSA_FORNECIMENTO = 16;

	private static final int REDUCAO_CAMPOS_OBRIGATORIOS = 2;
	private static final int ESCALA_INVESTIMENTO = 2;
	private static final int VERIFICAR_PONTOS_INVALIDOS = 2;
	private static final int VALOR_CONSUMO_APURADO = 200;
	private static final int ESCALA_DIVISAO_QDC_DIAS = 4;
	private static final int ESCALA_CONVERSAO_GAS = 4;
	private static final int CONVERSAO_QTD_DIARIA_CONTRATADA = 1800;
	private static final int ESCALA_CORRECAO_INVESTIMENTO = 6;
	private static final int DIVISOR = 60;
	private static final int LIMITE_HORA = 23;
	private static final int LIMITE_MINUTOS = 59;
	private static final int LIMITE_SEGUNDOS = 59;
	private static final int INDICE_CONTRATO = 2;
	private static final int MESES_ANO = 12;
	private static final int PORCENTAGEM = 100;
	private static final int CONVERSAO_QTD_DIARIA_MINIMA_CONTRATADA = 2;
	private static final int ESCALA_TARIFA_GAS = 2;
	private static final String SITUACAO_FATURAVEL = "situacao.faturavel";
	private static final String CLIENTE_RESPONSAVEL = "clienteResponsavel";
	private static final String CONTRATOPONTOCONSUMO= "contratoPontoConsumo";
	private static final String CONTRATOMODALIDADE= CONTRATO_MODALIDADE;
	private static final String LISTACONTRATOPONTOCONSUMOMODALIDADEQDC = "listaContratoPontoConsumoModalidadeQDC";
	private static final String[] PROPRIEDADESLAZY =
			new String[] {CONTRATOPONTOCONSUMO, CONTRATOMODALIDADE, LISTACONTRATOPONTOCONSUMOMODALIDADEQDC};
	
	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	@Qualifier("controladorMedidor")
	private ControladorMedidor controladorMedidor;

	/**
	 * Gets the contrato multa por qtd media.
	 *
	 * @return the contrato multa por qtd media
	 */
	public String getContratoMultaPorQtdMedia() {

		return CONTRATO_MULTA_POR_QTD_MEDIA.toString();
	}

	/**
	 * Gets the contrato multa por qtd minima contratada.
	 *
	 * @return the contrato multa por qtd minima contratada
	 */
	public String getContratoMultaPorQtdMinimaContratada() {

		return CONTRATO_MULTA_POR_QTD_MINIMA_CONTRATADA.toString();
	}

	/**
	 * Gets the contrato multa por tempo relacionamento.
	 *
	 * @return the contrato multa por tempo relacionamento
	 */
	public String getContratoMultaPorTempoRelacionamento() {

		return CONTRATO_MULTA_POR_TEMPO_RELACIONAMENTO.toString();
	}

	/**
	 * Classe privada reponsável por criar uma
	 * ordenação especial Numero do Documento.
	 *
	 * @author gmatos
	 */
	public class OrdenacaoEspecialNumeroDocumento implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {

			criteria.createAlias(CLIENTE_ASSINATURA, "cliente");
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("cliente.cpf"));
				criteria.addOrder(Order.asc("cliente.cnpj"));
			} else {
				criteria.addOrder(Order.desc("cliente.cpf"));
				criteria.addOrder(Order.desc("cliente.cnpj"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	/**
	 * Classe privada reponsável por criar uma
	 * ordenação especial do cumero do contrato.
	 *
	 * @author gmatos
	 */
	public class OrdenacaoEspecialNumeroContratoFormatado implements OrdenacaoEspecial {

		private static final String CLIENTE = "cliente";

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			criteria.createAlias(CLIENTE_ASSINATURA, CLIENTE);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc(NUMERO_COMPLETO_CONTRATO));
			} else {
				criteria.addOrder(Order.desc(NUMERO_COMPLETO_CONTRATO));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	/**
	 * Classe privada reponsável por criar uma
	 * ordenação pelo nome do cliente.
	 *
	 * @author gmatos
	 */
	public class OrdenacaoEspecialNomeClienteContrato implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {

			criteria.createAlias(CLIENTE_ASSINATURA, "cliente");
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("cliente.nome"));
			} else {
				criteria.addOrder(Order.desc("cliente.nome"));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Contrato.BEAN_ID_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Contrato.BEAN_ID_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#criarContratoPontoConsumo()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#getClasseEntidadeContratoPontoConsumo()
	 */
	@Override
	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	/**
	 * Gets the classe entidade contrato QDC.
	 *
	 * @return the classe entidade contrato QDC
	 */
	public Class<?> getClasseEntidadeContratoQDC() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoQDC.BEAN_ID_CONTRATO_QDC);
	}

	/**
	 * Gets the classe entidade parada programada.
	 *
	 * @return the classe entidade parada programada
	 */
	public Class<?> getClasseEntidadeParadaProgramada() {

		return ServiceLocator.getInstancia().getClassPorID(ParadaProgramada.BEAN_ID_PARADA_PROGRAMADA);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo PCS amostragem.
	 *
	 * @return the classe entidade contrato ponto consumo PCS amostragem
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoPCSAmostragem() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPCSAmostragem.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_AMOSTRAGEM);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo intervalo PCS.
	 *
	 * @return the classe entidade contrato ponto consumo intervalo PCS
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoIntervaloPCS() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPCSIntervalo.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_INTERVALO);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo item vencimento.
	 *
	 * @return the classe entidade contrato ponto consumo item vencimento
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoItemVencimento() {

		return ServiceLocator.getInstancia().getClassPorID(
						ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo item faturamento.
	 *
	 * @return the classe entidade contrato ponto consumo item faturamento
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoItemFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(
						ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#criarContratoQDC()
	 */
	@Override
	public EntidadeNegocio criarContratoQDC() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContratoQDC.BEAN_ID_CONTRATO_QDC);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#criarContratoCliente()
	 */
	@Override
	public EntidadeNegocio criarContratoCliente() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContratoCliente.BEAN_ID_CONTRATO_CLIENTE);
	}

	/**
	 * Criar periodicidade.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarPeriodicidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Periodicidade.BEAN_ID_PERIODICIDADE);
	}

	/**
	 * Gets the classe entidade solicitacao consumo ponto consumo.
	 *
	 * @return the classe entidade solicitacao consumo ponto consumo
	 */
	public Class<?> getClasseEntidadeSolicitacaoConsumoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(SolicitacaoConsumoPontoConsumo.BEAN_ID_SOLICITACAO_CONSUMO_PONTO_CONSUMO);
	}

	/**
	 * Gets the classe entidade contrato penalidade.
	 *
	 * @return the classe entidade contrato penalidade
	 */
	public Class<?> getClasseEntidadeContratoPenalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPenalidade.BEAN_ID_CONTRATO_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #criarContratoPontoConsumoPCSAmostragem()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumoPCSAmostragem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ContratoPontoConsumoPCSAmostragem.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_AMOSTRAGEM);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #criarContratoPontoConsumoPCSIntervalo()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumoPCSIntervalo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ContratoPontoConsumoPCSIntervalo.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_INTERVALO);
	}

	/**
	 * Gets the classe entidade contrato cliente.
	 *
	 * @return the classe entidade contrato cliente
	 */
	public Class<?> getClasseEntidadeContratoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoCliente.BEAN_ID_CONTRATO_CLIENTE);
	}

	/**
	 * Gets the classe entidade ponto consumo.
	 *
	 * @return the classe entidade ponto consumo
	 */
	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	/**
	 * Gets the classe entidade contrato modalidade.
	 *
	 * @return the classe entidade contrato modalidade
	 */
	public Class<?> getClasseEntidadeContratoModalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoModalidade.BEAN_ID_CONTRATO_MODALIDADE);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo modalidade.
	 *
	 * @return the classe entidade contrato ponto consumo modalidade
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoModalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo modalidade QDC.
	 *
	 * @return the classe entidade contrato ponto consumo modalidade QDC
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoModalidadeQDC() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidadeQDC.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE_QDC);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo penalidade.
	 *
	 * @return the classe entidade contrato ponto consumo penalidade
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumoPenalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPenalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_PENALIDADE);
	}

	/**
	 * Gets the classe entidade penalidade demanda.
	 *
	 * @return the classe entidade penalidade demanda
	 */
	public Class<?> getClasseEntidadePenalidadeDemanda() {

		return ServiceLocator.getInstancia().getClassPorID(PenalidadeDemanda.BEAN_ID_PENALIDADE_DEMANDA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#getClasseEntidadeSituacaoContrato()
	 */
	@Override
	public Class<?> getClasseEntidadeSituacaoContrato() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoContrato.BEAN_ID_SITUACAO_CONTRATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#getClasseEntidadeModeloContrato()
	 */
	@Override
	public Class<?> getClasseEntidadeModeloContrato() {

		return ServiceLocator.getInstancia().getClassPorID(ModeloContrato.BEAN_ID_MODELO_CONTRATO);
	}

	/**
	 * Gets the classe entidade aba.
	 *
	 * @return the classe entidade aba
	 */
	public Class<?> getClasseEntidadeAba() {

		return ServiceLocator.getInstancia().getClassPorID(Aba.BEAN_ID_ABA);
	}

	/**
	 * Gets the classe entidade aba atributo.
	 *
	 * @return the classe entidade aba atributo
	 */
	public Class<?> getClasseEntidadeAbaAtributo() {

		return ServiceLocator.getInstancia().getClassPorID(AbaAtributo.BEAN_ID_ABA_ATRIBUTO);
	}

	/**
	 * Gets the classe entidade penalidade.
	 *
	 * @return the classe entidade penalidade
	 */
	public Class<?> getClasseEntidadePenalidade() {

		return ServiceLocator.getInstancia().getClassPorID(Penalidade.BEAN_ID_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#obterAbaContrato(long)
	 */
	@Override
	public Aba obterAbaContrato(long chavePrimaria) throws NegocioException {

		return (Aba) super.obter(chavePrimaria, getClasseEntidadeAba());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#obterPenalidade(long)
	 */
	@Override
	public Penalidade obterPenalidade(long chavePrimaria) throws NegocioException {

		return (Penalidade) super.obter(chavePrimaria, getClasseEntidadePenalidade());
	}

	/**
	 * Obtem o Contrato ativo de um Ponto de Consumo.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the contrato ponto consumo
	 */
	@Override
	public ContratoPontoConsumo obterContratoAtivoPontoConsumo(PontoConsumo pontoConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch contratoPonto.contrato contrato ");
		hql.append(" left join fetch contrato.arrecadadorContratoConvenio arrecadadorContratoConvenio ");
		hql.append(" inner join fetch pontoConsumo.segmento segmento ");
		hql.append(" inner join fetch contrato.clienteAssinatura cliente ");
		hql.append(" left join fetch contratoPonto.contrato.listaContratoQDC listaContratoQDC ");
		hql.append(" left join fetch contratoPonto.contrato.listaFatura listaFatura ");
		hql.append(" inner join fetch contrato.situacao situacaoContrato ");
		hql.append(" left join fetch contratoPonto.tipoMedicao tipoMedicao ");
		hql.append(" left join fetch contratoPonto.faixaPressaoFornecimento faixaPressaoFornecimento ");
		hql.append(" left join fetch faixaPressaoFornecimento.unidadePressao unidadePressao ");
		hql.append(" left join fetch contrato.multaRecisoria multaRecisoria ");
		hql.append(" left join fetch contratoPonto.unidadeVazaoMaximaInstantanea unidadeVazaoMaximaInstantanea ");
		hql.append(" left join fetch contratoPonto.unidadeVazaoMinimaInstantanea unidadeVazaoMinimaInstantanea ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");
		hql.append(" and contratoPonto.contrato.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, pontoConsumo.getChavePrimaria());
		// FIXME : Utilizar a constante C_CONTRATO_ATIVO
		query.setLong(1, SituacaoContrato.ATIVO);
		return (ContratoPontoConsumo) query.uniqueResult();
	}

	/**
	 * Obtém o ContratoPontoConsumo de um Contrato Ativo. Obtém do ContratoPontoConsumo,
	 * os seguintes atributos: {@code chavePrimaria}, {@code periodicidade},
	 * {@code acaoAnormalidadeConsumoSemLeitura}, {@code medidaFornecimentoMinMensal},
	 * {@code numeroFatorCorrecao}, {@code unidadePressao}, {@code medidaPressao},
	 * {@code tipoMedicao}, {@code medidaPressaoColetada}. Obtém do PontoConsumo,
	 * os seguintes atributos: {@code chavePrimaria}. Obtém do Contrato, os seguintes
	 * atributos: {@code chavePrimaria}, {@code formaCobranca},
	 * Para o Cliente do Contrato, obtém os seguintes atributos: {@code chavePrimaria},
	 * {@code nome}.
	 * @param chavesPontoConsumo as chaves do ponto de consumo
	 * @return mapa de ContratoPontoConsumo por chave primária de PontoConsumo
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Map<Long, ContratoPontoConsumo> obterContratoAtivoPontoConsumo(Long[] chavesPontoConsumo) {

		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumo = new HashMap<>();
		if (chavesPontoConsumo != null && chavesPontoConsumo.length > 0) {
			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" select contratoPonto.chavePrimaria,  periodicidade, ");
			hql.append(" acaoAnormalidade, contratoPonto.medidaFornecimentoMinMensal, ");
			hql.append(" contratoPonto.numeroFatorCorrecao, unidadePressao, contratoPonto.medidaPressao, ");
			hql.append(" tipoMedicao, pontoConsumo.chavePrimaria, ");
			hql.append(" contrato.chavePrimaria, formaCobranca, ");
			hql.append(" cliente.chavePrimaria, cliente.nome,");
			hql.append(" unidadePressaoColetada, contratoPonto.medidaPressaoColetada, contrato.situacao,");
			hql.append(" contratoPonto.faixaPressaoFornecimento");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join contratoPonto.contrato contrato ");
			hql.append(" inner join contrato.clienteAssinatura cliente ");
			hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" inner join pontoConsumo.segmento segmento ");
			hql.append(" inner join contrato.situacao situacaoContrato ");
			hql.append(" left join contratoPonto.acaoAnormalidadeConsumoSemLeitura acaoAnormalidade ");
			hql.append(" left join contratoPonto.unidadePressao unidadePressao ");
			hql.append(" left join contratoPonto.unidadePressaoColetada unidadePressaoColetada ");
			hql.append(" left join contratoPonto.periodicidade periodicidade ");
			hql.append(" left join contratoPonto.tipoMedicao tipoMedicao");
			hql.append(" left join contrato.listaContratoQDC listaContratoQDC ");
			hql.append(" left join contrato.listaFatura listaFatura ");
			hql.append(" left join contratoPonto.faixaPressaoFornecimento faixaPressaoFornecimento ");
			hql.append(" left join contrato.multaRecisoria multaRecisoria ");
			hql.append(" left join contrato.formaCobranca formaCobranca ");
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades = 
					HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));				
			hql.append(" and situacaoContrato.chavePrimaria = :situacao ");
			hql.append(" and contrato.chavePrimariaPrincipal is null ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
			query.setLong(SITUACAO, SituacaoContrato.ATIVO);

			mapaContratoPontoConsumo.putAll( this.construirMapaContratoPontoConsumo(query.list()));
		}
		return mapaContratoPontoConsumo;
	}

	/**
	 * Constrói o mapa de ContratoPontoConsumo por chave primária do
	 * PontoConsumo. Obtém do ContratoPontoConsumo, os seguintes atributos:
	 * {@code chavePrimaria}, {@code periodicidade},
	 * {@code acaoAnormalidadeConsumoSemLeitura},
	 * {@code medidaFornecimentoMinMensal}, {@code numeroFatorCorrecao},
	 * {@code unidadePressao}, {@code medidaPressao}, {@code tipoMedicao}, ,
	 * {@code medidaPressaoColetada}. Obtém do PontoConsumo, os seguintes
	 * atributos: {@code chavePrimaria}. Obtém do Contrato, os seguintes
	 * atributos: {@code chavePrimaria}, {@code formaCobranca}, Para o Cliente
	 * do Contrato, obtém os seguintes atributos: {@code chavePrimaria},
	 * {@code nome}.
	 *
	 * @param chavesPontosConsumo
	 * @return
	 */
	private Map<Long, ContratoPontoConsumo> construirMapaContratoPontoConsumo(List<Object[]> listaAtributos) {
		Map<Long, ContratoPontoConsumo> mapa = new HashMap<>();
		for (Object[] dadosContratoPontoConsumo : listaAtributos) {
			ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) criarContratoPontoConsumo();
			contratoPontoConsumo.setChavePrimaria((long) dadosContratoPontoConsumo[INDEX_ID_CONTRATO_PONTO_CONUSMO]);
			contratoPontoConsumo.setPeriodicidade((Periodicidade) dadosContratoPontoConsumo[INDEX_PERIODICIDADE]);
			AcaoAnormalidadeConsumo acaoAnormalidade = (AcaoAnormalidadeConsumo) dadosContratoPontoConsumo[INDEX_ACAO_ANOR_CONS];
			contratoPontoConsumo.setAcaoAnormalidadeConsumoSemLeitura(acaoAnormalidade);
			contratoPontoConsumo.setMedidaFornecimentoMinMensal((BigDecimal) dadosContratoPontoConsumo[INDEX_MEDIDA_FORNEC_MIN_MENSAL]);
			contratoPontoConsumo.setNumeroFatorCorrecao((BigDecimal) dadosContratoPontoConsumo[INDEX_NUM_FATOR_CORRECAO]);
			contratoPontoConsumo.setUnidadePressao((Unidade) dadosContratoPontoConsumo[INDEX_UNIDADE_PRESSAO]);
			contratoPontoConsumo.setMedidaPressao((BigDecimal) dadosContratoPontoConsumo[INDEX_MEDIDA_PRESSAO]);
			contratoPontoConsumo.setTipoMedicao((TipoMedicao) dadosContratoPontoConsumo[INDEX_TIPO_MEDICAO]);
			if (INDEX_FAIXA_PRESSA_FORNECIMENTO < dadosContratoPontoConsumo.length) {
				contratoPontoConsumo.setFaixaPressaoFornecimento(
						(FaixaPressaoFornecimento) dadosContratoPontoConsumo[INDEX_FAIXA_PRESSA_FORNECIMENTO]);
			}
			Long chavePrimariaPontoConsumo = (Long) dadosContratoPontoConsumo[INDEX_ID_PONTO_CONSUMO];

			Contrato contrato = new ContratoImpl();
			contratoPontoConsumo.setContrato(contrato);
			contrato.setChavePrimaria((long) dadosContratoPontoConsumo[INDEX_ID_CONTRATO]);
			contrato.setFormaCobranca((EntidadeConteudo) dadosContratoPontoConsumo[INDEX_FORMA_COBRANCA]);
			contrato.setSituacao((SituacaoContrato) dadosContratoPontoConsumo[INDEX_SITUACAO_CONTRATO]);

			Cliente cliente = new ClienteImpl();
			cliente.setChavePrimaria((long) dadosContratoPontoConsumo[INDEX_ID_CLIENTE]);
			cliente.setNome((String) dadosContratoPontoConsumo[INDEX_NOME_CLIENTE]);
			contrato.setClienteAssinatura(cliente);

			contratoPontoConsumo.setUnidadePressaoColetada((Unidade) dadosContratoPontoConsumo[INDEX_UNIDADE_PRESSAO_COLETADA]);
			contratoPontoConsumo.setMedidaPressaoColetada((BigDecimal) dadosContratoPontoConsumo[INDEX_MEDIDA_PRESSAO_COLETADA]);

			if ( !mapa.containsKey(chavePrimariaPontoConsumo) ) {
				mapa.put(chavePrimariaPontoConsumo, contratoPontoConsumo);
			}
		}
		return mapa;
	}

	/**
	 * obtem o Contrato ativo de um Ponto de Consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the contrato ponto consumo
	 */
	@Override
	public ContratoPontoConsumo obterContratoEncerradoPontoConsumo(PontoConsumo pontoConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato contrato ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.segmento segmento ");
		hql.append(" left join fetch contratoPonto.contrato.listaContratoQDC listaContratoQDC ");
		hql.append(" left join fetch contratoPonto.contrato.listaFatura listaFatura ");
		hql.append(" inner join fetch contrato.situacao situacaoContrato ");
		hql.append(" left join fetch contratoPonto.tipoMedicao tipoMedicao ");
		hql.append(" left join fetch contratoPonto.faixaPressaoFornecimento faixaPressaoFornecimento ");
		hql.append(" left join fetch contrato.multaRecisoria multaRecisoria ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, pontoConsumo.getChavePrimaria());
		// FIXME : Utilizar a constante C_CONTRATO_ATIVO
		query.setLong(1, SituacaoContrato.ENCERRADO);

		return (ContratoPontoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoPontoConsumoModalidade
	 * (java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumoModalidade obterContratoPontoConsumoModalidade(Long chavePrimaria) throws NegocioException {

		return (ContratoPontoConsumoModalidade) super.obter(chavePrimaria, getClasseEntidadeContratoPontoConsumoModalidade(),PROPRIEDADESLAZY);
	}

	/**
	 * Consulta o ContratoPontoConsumo por código do ponto de consumo e retorna o contratoPontoConsumo que contem
	 * o contrato ativo.
	 *
	 * @param cdPontoConsumo
	 *            [obrigatorio]
	 * @return the contrato ponto consumo
	 */
	@Override
	public ContratoPontoConsumo consultarContratoPontoConsumoPorPontoDeConsumoContratoAtivo(Long cdPontoConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(ContratoPontoConsumoImpl.class.getName());
		hql.append(CONTRATO_PONTO_CONSUMO2);
		hql.append(" where contratoPontoConsumo.pontoConsumo.chavePrimaria = :cdPontoConsumo");
		hql.append(" and contratoPontoConsumo.contrato.situacao = 1 ");
		hql.append(" and contratoPontoConsumo.contrato.habilitado = true ");
		hql.append(" and contratoPontoConsumo.contrato.chavePrimariaPrincipal is null ");
		hql.append(" and contratoPontoConsumo.contrato.situacao.chavePrimaria = 1 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("cdPontoConsumo", cdPontoConsumo);

		return (ContratoPontoConsumo) query.uniqueResult();
	}

	/**
	 * Consulta o ContratoPontoConsumo por chaves primárias de PontoConsumo.
	 * Obtém os seguintes dados de ContratoPontoConsumo: {@code chavePrimaria},
	 * {@code isEmiteNotaFiscalEletronica}.
	 *
	 * @param chavesPontoConsumo
	 * @return mapa de ContratoPontoConsumo por chave de PontoConsumo
	 */
	@Override
	public Map<Long, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoDeConsumoContratoAtivo(List<Long> chavesPontoConsumo) {

		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoPorPontoConsumo = new HashMap<>();

		if (chavesPontoConsumo != null && !chavesPontoConsumo.isEmpty()) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo.chavePrimaria, contratoPontoConsumo.chavePrimaria, ");
			hql.append(" contratoPontoConsumo.isEmiteNotaFiscalEletronica ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO_CONSUMO2);
			hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo  ");
			hql.append(" inner join contratoPontoConsumo.contrato contrato ");
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades =
					HibernateHqlUtil.adicionarClausulaIn(hql, PONTO_CONSUMO_CHAVE_PRIMARIA, PT_CONS, chavesPontoConsumo);
			hql.append(" and contrato.habilitado = true ");
			hql.append(" and contrato.chavePrimariaPrincipal is null ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
			mapaContratoPontoConsumoPorPontoConsumo.putAll(construirMapaContratoPontoConsumoPorPontoConsumo(query.list()));
		}

		return mapaContratoPontoConsumoPorPontoConsumo;
	}

	/**
	 * Constró um mapa de ContratoPontoConsumo por chaves primárias de PontoConsumo.
	 * Obtém os seguintes dados de ContratoPontoConsumo: {@code chavePrimaria},
	 * {@code isEmiteNotaFiscalEletronica}.
	 *
	 * @param atributos
	 * @return mapa de ContratoPontoConsumo por chave de PontoConsumo
	 */
	private Map<Long, ContratoPontoConsumo> construirMapaContratoPontoConsumoPorPontoConsumo(List<Object[]> atributos) {

		Map<Long, ContratoPontoConsumo> mapa = new HashMap<>();

		for (Object[] dadosContratoPontoConsumo : atributos) {
			Long chavePontoConsumo = (Long) dadosContratoPontoConsumo[0];
			ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) criarContratoPontoConsumo();
			contratoPontoConsumo.setChavePrimaria((long) dadosContratoPontoConsumo[1]);
			contratoPontoConsumo.setIsEmiteNotaFiscalEletronica((Boolean) dadosContratoPontoConsumo[SOBRE_NFE]);
			if (!mapa.containsKey(chavePontoConsumo)) {
				mapa.put(chavePontoConsumo, contratoPontoConsumo);
			}
		}
		return mapa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoCliente
	 * (java.lang.Integer,
	 * java.lang.Long)
	 */
	@Override
	public Collection<Contrato> consultarContratoParadaProgramada(Map<String, Object> filtro) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		Query query = null;
		Collection<Contrato> listaContratoPonto = null;

		if (!filtro.isEmpty()) {

			Integer numeroContrato = (Integer) filtro.get(NUMERO_CONTRATO);
			Long idCliente = (Long) filtro.get(ID_CLIENTE);

			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(CONTRATO);
			hql.append(" left join fetch contrato.listaContratoPontoConsumo contratoPonto ");
			hql.append(" left join fetch contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" left join fetch pontoConsumo.imovel imovel ");
			hql.append(" left join fetch contrato.listaContratoCliente contratoCliente ");
			hql.append(WHERE);
			hql.append(" contrato.situacao = :situacao ");
			hql.append(" and contratoPonto.indicadorParadaProgramada = true ");
			hql.append(" and contrato.habilitado = true ");

			if ((numeroContrato != null) && (numeroContrato > 0)) {
				hql.append(" and cast((contrato.anoContrato*100000)+contrato.numero, string) like :numero ");
			}

			if ((idCliente != null) && (idCliente > 0)) {
				hql.append(" and ( contrato.clienteAssinatura.chavePrimaria = :idCliente ");
				hql.append(" or ( select count(contratoCliente.chavePrimaria) from ");
				hql.append(getClasseEntidadeContratoCliente().getSimpleName());
				hql.append(ALIAS_CONTRATO_CLIENTE);
				hql.append(" inner join  contratoCliente.contrato contratoCli ");
				hql.append(" inner join  contratoCliente.pontoConsumo pontoConsumoCli ");
				hql.append(" inner join  contratoCliente.cliente clienteCli ");
				hql.append(WHERE);
				hql.append(" contratoCli.chavePrimaria = contratoPonto.contrato.chavePrimaria ");
				hql.append(" and pontoConsumoCli.chavePrimaria = contratoPonto.pontoConsumo.chavePrimaria ");
				hql.append(" and clienteCli.chavePrimaria = :idCliente ");
				hql.append(" ) > 0 ) ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(SITUACAO, situacao);
			if ((numeroContrato != null) && (numeroContrato > 0)) {
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append("%");
				stringBuilder.append(String.valueOf(numeroContrato));
				stringBuilder.append("%");
				query.setString(NUMERO, stringBuilder.toString());
			}

			if ((idCliente != null) && (idCliente > 0)) {
				query.setLong(ID_CLIENTE, idCliente);
			}

			listaContratoPonto = query.list();
		} else {
			throw new NegocioException(ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA, true);
		}

		Set<Contrato> listaContratos = new HashSet<>();
		listaContratos.addAll(listaContratoPonto);
		return listaContratos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * consultarContratoPontoConsumoProgramacaoConsumo
	 * (java
	 * .util.Map)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoProgramacaoConsumo(
			Map<String, Object> filtro) throws NegocioException {
		return consultarContratoPontoConsumoProgramacaoConsumo(filtro, false);
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato
	 * .ControladorContrato
	 * #consultarContratoPontoConsumoProgramacaoConsumo(
	 * java.util.Map, boolean)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoProgramacaoConsumo(
			Map<String, Object> filtro, boolean listaContratoPontoCanBeEmpty)
					throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		Query query = null;
		Collection<ContratoPontoConsumo> listaContratoPonto = null;

		if (!filtro.isEmpty()) {

			String numeroContrato = (String) filtro.get(NUMERO_CONTRATO);
			Long idCliente = (Long) filtro.get(ID_CLIENTE);

			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" inner join fetch pontoConsumo.imovel imovel ");
			hql.append(" inner join fetch pontoConsumo.ramoAtividade ramoAtividade ");
			hql.append(" inner join fetch pontoConsumo.segmento segmento ");
			hql.append(" inner join fetch contratoPonto.contrato contr ");
			hql.append(WHERE);
			hql.append(" contr.situacao = :situacao ");
			hql.append(" and exists (select contratoPontoConsumo from ");
			hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
			hql.append(" modalidade ");
			hql.append(WHERE);
			hql.append(" modalidade.contratoPontoConsumo = contratoPonto ");
			hql.append(" and modalidade.indicadorProgramacaoConsumo = true)  ");

			hql.append(" and contr.habilitado = true ");

			if (numeroContrato != null && !numeroContrato.isEmpty()) {
				hql.append(" and cast((contr.anoContrato*100000)+contr.numero, string) = :numero ");
			}

			if ((idCliente != null) && (idCliente > 0)) {
				hql.append(" and ( contr.clienteAssinatura.chavePrimaria = :idCliente ");
				hql.append(" or ( select count(contratoCliente.chavePrimaria) from ");
				hql.append(getClasseEntidadeContratoCliente().getSimpleName());
				hql.append(ALIAS_CONTRATO_CLIENTE);
				hql.append(WHERE);
				hql.append(" contratoCliente.contrato.chavePrimaria = contratoPonto.contrato.chavePrimaria ");
				hql.append(" and contratoCliente.pontoConsumo.chavePrimaria = contratoPonto.pontoConsumo.chavePrimaria ");
				hql.append(" and contratoCliente.cliente.chavePrimaria = :idCliente ");
				hql.append(" ) > 0 ) ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(SITUACAO, situacao);
			if (numeroContrato != null && !numeroContrato.isEmpty()) {
				query.setString(NUMERO, numeroContrato);
			}

			if ((idCliente != null) && (idCliente > 0)) {
				query.setLong(ID_CLIENTE, idCliente);
			}

			listaContratoPonto = query.list();
			if (listaContratoPonto.isEmpty() && !listaContratoPontoCanBeEmpty) {
				throw new NegocioException(ERRO_NEGOCIO_SEM_CONTRATOS_A_EXIBIR, true);
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PESQUISA, true);
		}

		return listaContratoPonto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumo()
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumo(Long[] chavesPrimarias) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo ");
		hql.append(WHERE);
		hql.append(" contratoPonto.chavePrimaria in (:chavesContratoPonto) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesContratoPonto", chavesPrimarias);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoPorPontoConsumo
	 * (java.lang.Long[])
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumo(Long[] chavesPontoConsumo)
			throws NegocioException {

		Query query = null;

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato contrato ");
		hql.append(" inner join fetch contratoPonto.contrato.clienteAssinatura ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel ");
		hql.append(" inner join fetch pontoConsumo.quadraFace ");
		hql.append(" inner join fetch pontoConsumo.rota ");
		hql.append(" inner join fetch pontoConsumo.ramoAtividade ");
		hql.append(" left join fetch contrato.formaCobranca ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor instalacaoMedidor ");
		
		hql.append(WHERE);
		
		Map<String, List<Long>> mapaPropriedades =
				HibernateHqlUtil.adicionarClausulaIn(hql, "contratoPonto.pontoConsumo.chavePrimaria", PT_CONS,
						Arrays.asList(chavesPontoConsumo));
		hql.append(" and contratoPonto.contrato.situacao = :situacao ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		query.setString(SITUACAO, situacao);

		return query.list();
	}

	/**
	 * A partir de um conjunto de chaves de ponto de consumo, obtem apenas
	 * as chaves dos pontos de consumo com contrato ativo.
	 *
	 * @param chavesPontoConsumo
	 * @param chaves primárias de pontos com contrato ativo
	 */
	@Override
	public List<Long> consultarPontoConsumoPorContratoAtivo(Long[] chavesPontoConsumo) {

		List<Long> chavesPontosDeContratoAtivo = new ArrayList<>();

		if (chavesPontoConsumo != null && chavesPontoConsumo.length > 0) {
			Query query = null;

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo.chavePrimaria");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join  contratoPonto.contrato ");
			hql.append(" inner join  contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" inner join  contratoPonto.pontoConsumo.imovel ");
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades =
					HibernateHqlUtil.adicionarClausulaIn(hql, "contratoPonto.pontoConsumo.chavePrimaria", PT_CONS,
							Arrays.asList(chavesPontoConsumo));
			hql.append(" and contratoPonto.contrato.situacao = :situacao ");
			hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
			query.setString(SITUACAO, situacao);

			chavesPontosDeContratoAtivo.addAll(query.list());
		}

		return chavesPontosDeContratoAtivo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPontoConsumoPorPontoConsumo(java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumo(Long chavePontoConsumo) throws NegocioException {

		Query query = null;

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato ");
		hql.append(" inner join fetch contratoPonto.periodicidade ");
		// utilizado
		// na
		// validação
		// de
		// dias
		// à
		// legislação.
		hql.append(" inner join fetch contratoPonto.contrato.clienteAssinatura ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel ");
		hql.append(" left join fetch contratoPonto.unidadePressao ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :situacao ");
		hql.append(" and contratoPonto.contrato.habilitado = true ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(SITUACAO, Long.parseLong(situacao));
		query.setLong(CHAVE_PONTO_CONSUMO, chavePontoConsumo);

		return (ContratoPontoConsumo) query.uniqueResult();
	}


	@Override
	public Map<Long, Collection<String>> consultarCepDeContratoPontoConsumoPorPontoConsumo(Collection<PontoConsumo> pontosConsumo)
			throws NegocioException {

		Map<Long, Collection<String>> cepPorPontoConsumo = null;

		if (!Util.isNullOrEmpty(pontosConsumo)) {

			String situacao = getControladorConstanteSistema()
					.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria, ");
			hql.append(" cep.cep as cep_cep ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join contratoPonto.contrato contrato ");
			hql.append(" inner join contratoPonto.periodicidade ");
			hql.append(" inner join contratoPonto.contrato.clienteAssinatura ");
			hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" inner join pontoConsumo.imovel ");
			hql.append(" left join contratoPonto.cep cep ");
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades = 
					HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Util.recuperarChavesPrimarias(pontosConsumo));	
			hql.append(" and contrato.situacao.chavePrimaria = :situacao ");
			hql.append(" and contrato.habilitado = true ");
			hql.append(" and contrato.chavePrimariaPrincipal is null ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			query.setResultTransformer(new GGASTransformer(String.class,
					"pontoConsumo_chavePrimaria"));

			query.setLong(SITUACAO, Long.parseLong(situacao));

			cepPorPontoConsumo = (Map<Long, Collection<String>>) query.list().get(0);
		}
		return cepPorPontoConsumo;
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorContrato
	 * consultarContratoPontoConsumoPorPontoConsumoAntesDataEmissao(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoAntesDataEmissao(Fatura fatura)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato ");
		hql.append(" inner join fetch contratoPonto.periodicidade ");
		hql.append(" inner join fetch contratoPonto.contrato.clienteAssinatura ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel ");
		hql.append(" left join fetch contratoPonto.unidadePressao ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");
		hql.append(" and contratoPonto.contrato.dataAssinatura < :dataEmissaoFatura ");
		hql.append(" order by contratoPonto.contrato.dataAssinatura desc ");
		hql.append(" , contratoPonto.contrato.ultimaAlteracao desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(CHAVE_PONTO_CONSUMO, fatura.getPontoConsumo().getChavePrimaria());
		query.setDate("dataEmissaoFatura", fatura.getDataEmissao());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorContrato#
	 * consultarContratoPontoConsumoPorPontoConsumoAtivoOuMaisRecente(br.com.ggas.faturamento.Fatura
	 * )
	 */
	@Override
	public Contrato consultarContratoPontoConsumoPorPontoConsumoAtivoOuMaisRecente(Fatura fatura) throws NegocioException {
		Contrato resultadoDeConsultaPontoConsumoPorPontoConsumoAtivoOuMaisRecente = null;
		if (fatura.getContratoAtual() != null) {
			resultadoDeConsultaPontoConsumoPorPontoConsumoAtivoOuMaisRecente = fatura.getContratoAtual();
		} else {
			if (fatura.getPontoConsumo() != null) {
				ContratoPontoConsumo contratoAtivoPontoConsumo = obterContratoAtivoPontoConsumo(fatura.getPontoConsumo());
				if (contratoAtivoPontoConsumo != null) {
					resultadoDeConsultaPontoConsumoPorPontoConsumoAtivoOuMaisRecente = contratoAtivoPontoConsumo.getContrato();
				} else {
					Collection<ContratoPontoConsumo> listacontratoPontoConsumo =
							consultarContratoPontoConsumoPorPontoConsumoAntesDataEmissao(fatura);
					if (CollectionUtils.isNotEmpty(listacontratoPontoConsumo)) {
						ContratoPontoConsumo contratoPontoConsumo = Util.primeiroElemento(listacontratoPontoConsumo);
						resultadoDeConsultaPontoConsumoPorPontoConsumoAtivoOuMaisRecente = contratoPontoConsumo.getContrato();
					}
				}
			}
		}
		return resultadoDeConsultaPontoConsumoPorPontoConsumoAtivoOuMaisRecente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#
	 * listarContratoPontoConsumoModalidadePorContratoPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorContrato(Long chaveContrato)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
		hql.append(CONTRATO_PONTO_MODALIDADE);
		hql.append(WHERE);
		hql.append(" contratoPontoModalidade.contratoPontoConsumo.contrato.chavePrimaria = :chaveContrato ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(CHAVE_CONTRATO, chaveContrato);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * listarContratoModalidadePorContratoPontoConsumo
	 * (java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorContratoPontoConsumo(Long chaveContratoPonto)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
		hql.append(CONTRATO_PONTO_MODALIDADE);
		hql.append(" inner join fetch contratoPontoModalidade.contratoModalidade ");
		hql.append(" inner join fetch contratoPontoModalidade.contratoPontoConsumo ");
		hql.append(" left join fetch contratoPontoModalidade.listaContratoPontoConsumoModalidadeQDC ");
		hql.append(WHERE);
		hql.append(" contratoPontoModalidade.contratoPontoConsumo.chavePrimaria = :chaveContratoPonto ");
		hql.append(" and contratoPontoModalidade.indicadorProgramacaoConsumo = true ");
		hql.append(" order by contratoPontoModalidade.contratoModalidade.sequenciaApuracaoVolume asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveContratoPonto", chaveContratoPonto);

		List<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidadeRepetido = query.list();
		List<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new ArrayList<>();

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidadeRepetido) {
			if (!listaContratoPontoConsumoModalidade.contains(contratoPontoConsumoModalidade)) {
				listaContratoPontoConsumoModalidade.add(contratoPontoConsumoModalidade);
			}
		}

		return listaContratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterListaPenalidadeDemandaSob()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<PenalidadeDemanda> obterListaPenalidadeDemandaSob() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadePenalidadeDemanda());
		criteria.add(Restrictions.eq("indicadorSobreSobDemanda", Boolean.TRUE));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterListaPenalidadeDemandaSobre()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<PenalidadeDemanda> obterListaPenalidadeDemandaSobre() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadePenalidadeDemanda());
		criteria.add(Restrictions.eq("indicadorSobreSobDemanda", Boolean.FALSE));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarContratoModalidade()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ContratoModalidade> listarContratoModalidade() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoModalidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true ");
		hql.append(" order by descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoModalidade(long)
	 */
	@Override
	public ContratoModalidade obterContratoModalidade(long chavePrimaria) throws NegocioException {

		return (ContratoModalidade) this.obter(chavePrimaria, getClasseEntidadeContratoModalidade());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #verificarDependenciaContratoModelo(long)
	 */
	@Override
	public Boolean verificarDependenciaContratoModelo(long chavePrimariaModelo) throws NegocioException {

		Boolean retorno = false;
		Long quantidade = 0L;

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(contrato.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" contrato where");
		hql.append(" contrato.modeloContrato.chavePrimaria = :chavePrimariaModelo ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaModelo", chavePrimariaModelo);

		quantidade = (Long) query.uniqueResult();

		if (quantidade > 0) {
			retorno = true;
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarModeloContratoSelecionado
	 * (java.lang.Long)
	 */
	@Override
	public void validarModeloContratoSelecionado(Long chaveModeloContrato) throws NegocioException {

		if (chaveModeloContrato == null || chaveModeloContrato < 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, ModeloContrato.MODELO_CONTRATO);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratos(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Contrato> consultarContratos(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		Boolean imovelAlias = false;

		criteria.createAlias("modeloContrato", "modeloContrato");

		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long[] idSituacaoContrato = (Long[]) filtro.get(ID_SITUACAO_CONTRATO);
			if ((idSituacaoContrato != null) && (idSituacaoContrato.length > 0)) {
				criteria.createAlias(SITUACAO, SITUACAO).add(Restrictions.in("situacao.chavePrimaria", idSituacaoContrato));
			} else {
				criteria.createAlias(SITUACAO, SITUACAO);
			}

			Boolean faturavel = (Boolean) filtro.get(FATURAVEL);

			if (faturavel != null) {
				Junction faturavelJunction = null;
				imovelAlias = true;
				if (faturavel) {

					faturavelJunction = Restrictions.conjunction();

					faturavelJunction.add(Restrictions.eq(SITUACAO_FATURAVEL, true));

					faturavelJunction.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

					criteria.createAlias(LISTA_CONTRATO_PONTO_CONSUMO, CONT_PONTOS_CONSUMO);

					criteria.createAlias(CONT_PONTOS_CONSUMO_PONTO_CONSUMO, PONTOS_CONSUMO);

					faturavelJunction.add(Restrictions.eq("pontosConsumo.habilitado", true));

					criteria.createAlias(PONTOS_CONSUMO_IMOVEL, IMOVEIS);

					faturavelJunction.add(Restrictions.eq("imoveis.habilitado", true));

					criteria.createAlias(PONTOS_CONSUMO_ROTA, "rotas");

					faturavelJunction.add(Restrictions.eq("rotas.habilitado", true));

					criteria.createAlias(PONTOS_CONSUMO_INSTALACAO_MEDIDOR, "instalacaoMedidores");

					faturavelJunction.add(Restrictions.or(Restrictions.isNull("pontosConsumo.codigoPontoConsumoSupervisorio"),
							Restrictions.isNotNull("instalacaoMedidores.vazaoCorretor")));

					criteria.createAlias(INSTALACAO_MEDIDORES_MEDIDOR, "medidores");

					faturavelJunction.add(Restrictions.eq("medidores.habilitado", true));

				} else {

					faturavelJunction = Restrictions.disjunction();

					faturavelJunction.add(Restrictions.ne(SITUACAO_FATURAVEL, true));

					faturavelJunction.add(Restrictions.ne(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

					criteria.createAlias(LISTA_CONTRATO_PONTO_CONSUMO, CONT_PONTOS_CONSUMO, Criteria.LEFT_JOIN);

					criteria.createAlias(CONT_PONTOS_CONSUMO_PONTO_CONSUMO, PONTOS_CONSUMO, Criteria.LEFT_JOIN);
					faturavelJunction.add(Restrictions.isNull(CONT_PONTOS_CONSUMO_PONTO_CONSUMO));

					faturavelJunction.add(Restrictions.ne("pontosConsumo.habilitado", true));

					criteria.createAlias(PONTOS_CONSUMO_IMOVEL, IMOVEIS, Criteria.LEFT_JOIN);
					faturavelJunction.add(Restrictions.isNull(PONTOS_CONSUMO_IMOVEL));

					faturavelJunction.add(Restrictions.ne("imoveis.habilitado", true));

					criteria.createAlias(PONTOS_CONSUMO_ROTA, "rotas", Criteria.LEFT_JOIN);
					faturavelJunction.add(Restrictions.isNull(PONTOS_CONSUMO_ROTA));
					faturavelJunction.add(Restrictions.ne("rotas.habilitado", true));

					criteria.createAlias(PONTOS_CONSUMO_INSTALACAO_MEDIDOR, "instalacaoMedidores", Criteria.LEFT_JOIN);
					faturavelJunction.add(Restrictions.isNull(PONTOS_CONSUMO_INSTALACAO_MEDIDOR));

					faturavelJunction.add(Restrictions.and(Restrictions.isNotNull("pontosConsumo.codigoPontoConsumoSupervisorio"),
							Restrictions.isNull("instalacaoMedidores.vazaoCorretor")));

					criteria.createAlias(INSTALACAO_MEDIDORES_MEDIDOR, "medidores", Criteria.LEFT_JOIN);
					faturavelJunction.add(Restrictions.isNull(INSTALACAO_MEDIDORES_MEDIDOR));

					faturavelJunction.add(Restrictions.ne("medidores.habilitado", true));
				}
				criteria.add(faturavelJunction);
			}else{
				Long idImovel = (Long) filtro.get(ID_IMOVEL);
				if (idImovel == null) {
					Junction faturavelJunction = null;
					faturavelJunction = Restrictions.disjunction();
					criteria.createAlias(LISTA_CONTRATO_PONTO_CONSUMO, CONT_PONTOS_CONSUMO, Criteria.LEFT_JOIN);
					criteria.createAlias(CONT_PONTOS_CONSUMO_PONTO_CONSUMO, PONTOS_CONSUMO, Criteria.LEFT_JOIN);
					criteria.createAlias(PONTOS_CONSUMO_IMOVEL, IMOVEIS, Criteria.LEFT_JOIN);
					criteria.createAlias(PONTOS_CONSUMO_ROTA, "rotas", Criteria.LEFT_JOIN);
					criteria.add(faturavelJunction);
				}
			}

			Integer numero = (Integer) filtro.get(NUMERO);
			if (numero != null && numero > 0) {
				criteria.add(Restrictions.like(NUMERO_COMPLETO_CONTRATO, "%" + numero + "%"));
			}

			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if (idCliente != null && idCliente > 0) {

				Criteria clienteCriteria = criteria.createCriteria(CLIENTE_ASSINATURA);
				clienteCriteria.createAlias(CLIENTE_RESPONSAVEL, CLIENTE_RESPONSAVEL, Criteria.LEFT_JOIN);
				clienteCriteria.add(Restrictions.or(Restrictions.eq(CHAVE_PRIMARIA, idCliente),
						Restrictions.eq("clienteResponsavel.chavePrimaria", idCliente)));

			} else {
				criteria.setFetchMode(CLIENTE_ASSINATURA, FetchMode.JOIN);
			}

			Long idModeloContrato = (Long) filtro.get(ID_MODELO_CONTRATO);
			if (!Util.isEmpty(idModeloContrato)) {

				DetachedCriteria idsVersoesAnteriores = DetachedCriteria.forClass(getClasseEntidadeModeloContrato());

				idsVersoesAnteriores.setProjection(Projections.projectionList().add(Projections.property(CHAVE_PRIMARIA)));

				StringBuilder sqlIdsVersoesAnteriors = new StringBuilder();
				sqlIdsVersoesAnteriors.append(" 1=1 START WITH come_cd = ");
				sqlIdsVersoesAnteriors.append(idModeloContrato);
				sqlIdsVersoesAnteriors.append(" CONNECT BY PRIOR ");
				sqlIdsVersoesAnteriors.append("come_cd_versao_anterior = come_cd");

				idsVersoesAnteriores.add(Restrictions.sqlRestriction(sqlIdsVersoesAnteriors.toString()));

				criteria.add(Subqueries.propertyIn("modeloContrato.chavePrimaria", idsVersoesAnteriores));

			}

			Long idTipoContrato = (Long) filtro.get(ID_TIPO_CONTRATO);
			if (idTipoContrato != null && idTipoContrato > 0) {
				criteria.createAlias("modeloContrato.tipoModelo", TIPO_MODELO);
				criteria.add(Restrictions.eq("tipoModelo.chavePrimaria", idTipoContrato));
			}

			Long[] chavesModeloContrato = (Long[]) filtro.get(CHAVES_MODELO_CONTRATO);
			String fluxoMigracao = (String) filtro.get(FLUXO_MIGRACAO);
			if (chavesModeloContrato != null && chavesModeloContrato.length > 0 && fluxoMigracao != null) {
				criteria.add(Restrictions.in("modeloContrato.chavePrimaria", chavesModeloContrato));
			}

			Long idImovel = (Long) filtro.get(ID_IMOVEL);
			if (idImovel != null && idImovel > 0) {

				if (imovelAlias.equals(false)) {
					criteria.createAlias(LISTA_CONTRATO_PONTO_CONSUMO, CONT_PONTOS_CONSUMO);

					criteria.createAlias(CONT_PONTOS_CONSUMO_PONTO_CONSUMO, PONTOS_CONSUMO);

					criteria.createAlias(PONTOS_CONSUMO_IMOVEL, IMOVEIS);
				}
				criteria.createAlias("imoveis.imovelCondominio", "imovelCondominio", Criteria.LEFT_JOIN);
				criteria.add(Restrictions.or(Restrictions.eq("imoveis.chavePrimaria", idImovel),
						Restrictions.eq("imovelCondominio.chavePrimaria", idImovel)));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			// Paginação em banco dados
			if (filtro.containsKey(COLECAO_PAGINADA)) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
				colecaoPaginada.adicionarOrdenacaoEspecial("clienteAssinatura.numeroDocumentoFormatado",
								new OrdenacaoEspecialNumeroDocumento());
				colecaoPaginada.adicionarOrdenacaoEspecial(NUMERO_FORMATADO, new OrdenacaoEspecialNumeroContratoFormatado());
				colecaoPaginada.adicionarOrdenacaoEspecial("clienteAssinatura.nome", new OrdenacaoEspecialNomeClienteContrato());

				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					criteria.addOrder(Order.asc(NUMERO_COMPLETO_CONTRATO));
				}
			} else {
				criteria.addOrder(Order.asc(NUMERO_COMPLETO_CONTRATO));
			}
		}

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratos(java.util.Map)
	 */
	@Override
	public Boolean verificarContratoFaturavel(long idContrato) throws NegocioException {

		Criteria criteria = getCriteria();
		Criteria pontoConsumoCriteria = null;
		Criteria imovelCriteria = null;

		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idContrato));

		criteria.createAlias(SITUACAO, SITUACAO);

		criteria.add(Restrictions.eq(SITUACAO_FATURAVEL, true));

		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

		pontoConsumoCriteria = criteria.createCriteria(LISTA_CONTRATO_PONTO_CONSUMO).createCriteria(PONTO_CONSUMO);

		pontoConsumoCriteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

		imovelCriteria = pontoConsumoCriteria.createCriteria(IMOVEL);

		imovelCriteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

		pontoConsumoCriteria.createCriteria(ROTA)
			.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

		pontoConsumoCriteria.createAlias(INSTALACAO_MEDIDOR, "IM");

		pontoConsumoCriteria
				.add(Restrictions.or(Restrictions.isNull(CODIGO_PONTO_CONSUMO_SUPERVISORIO), Restrictions.isNotNull("IM.vazaoCorretor")));

		pontoConsumoCriteria.add(Restrictions.isNotNull("IM.medidor"));


		criteria.addOrder(Order.asc(NUMERO_COMPLETO_CONTRATO));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		Collection<Contrato> contrato = criteria.list();

		return !contrato.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoAtivoPorContratoPai(java.lang.Long)
	 */
	@Override
	public Contrato consultarContratoAtivoPorContratoPai(Long chaveContratoPai) throws NegocioException {

		Contrato ativo = null;
		Criteria criteria = getCriteria();

		if (chaveContratoPai != null) {
			criteria.createAlias(SITUACAO, SITUACAO).add(Restrictions.eq("situacao.chavePrimaria", SituacaoContrato.ATIVO));
			criteria.add(Restrictions.eq(CHAVE_PRIMARIA_PAI, chaveContratoPai));
			criteria.add(Restrictions.isNull(CHAVE_PRIMARIA_PRINCIPAL));

			ativo = (Contrato) criteria.uniqueResult();
		}

		return ativo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarRemoverContratos(java.lang.Long[])
	 */
	@Override
	public void validarRemoverContratos(Collection<Long> chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(contrato) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(" where contrato.situacao.permiteExclusao = 0 ");
		hql.append(" and contrato.chavePrimaria in (:chavesPrimarias) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList(CHAVES_PRIMARIAS, chavesPrimarias);

		Long quantidade = (Long) query.uniqueResult();

		if (quantidade != null && quantidade > 0) {
			hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidadeSituacaoContrato().getSimpleName());
			hql.append(" where permiteExclusao = 0 ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			Collection<SituacaoContrato> listaSituacoes = query.list();

			StringBuilder descricaoSituacoes = new StringBuilder();
			if (listaSituacoes != null && !listaSituacoes.isEmpty()) {
				for (SituacaoContrato situacao : listaSituacoes) {
					if (descricaoSituacoes.length() > 0) {
						descricaoSituacoes.append(", ");
					}
					descricaoSituacoes.append(situacao.getDescricao());
				}
			}
			throw new NegocioException(ERRO_NEGOCIO_SITUACAO_CONTRATO_INVALIDA_EXCLUSAO, new Object[] {descricaoSituacoes.toString()});
		}
	}

	@Override
	public Long consultarIdArrecadadorContratoConvenio(Long idContrato) {


		StringBuilder hql = new StringBuilder();
		hql.append(" select contrato.arrecadadorContratoConvenioDebitoAutomatico.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(" where contrato.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA,idContrato);

		return (Long) query.uniqueResult();

	}

	@Override
	public Banco consultarBancoContrato(Long idContrato) {


		StringBuilder hql = new StringBuilder();
		hql.append(" select contrato.banco from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(" where contrato.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA,idContrato);

		return (Banco) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #remover(java.util.Collection,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void remover(Collection<Long> chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		// Atualiza as associações de cliente com
		// imóvel.
		for (Long idContratoRemovido : chavesPrimarias) {
			removerClienteImovel(idContratoRemovido, chavesPrimarias);
			controladorPontoConsumo.removerPontoConsumoVencimento(idContratoRemovido);
			Contrato contrato = (Contrato) obter(idContratoRemovido);
			ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia()
					.getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
			controladorIntegracao.excluirIntegracaoContratoPorPontoConsumo(contrato,
					new ArrayList<>(contrato.getListaContratoPontoConsumo()));
		}

		super.remover(chavesPrimarias.toArray(new Long[] {}), dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #removerClienteImovel(java.lang.Long,
	 * java.util.Collection)
	 */
	@Override
	public void removerClienteImovel(Long idContratoRemovido, Collection<Long> chavesPrimarias) throws GGASException {

		ControladorClienteImovel controladorClienteImovel = (ControladorClienteImovel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorClienteImovel.BEAN_ID_CONTROLADOR_CLIENTE_IMOVEL);
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		Contrato removido = (Contrato) obter(idContratoRemovido, LISTA_CONTRATO_PONTO_CONSUMO, CLIENTE_ASSINATURA);

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroResponsavelCliente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_RELACIONAMENTO_CLIENTE_RESPONSAVEL_CONTRATO);

		for (ContratoPontoConsumo contratoPontoConsumo : removido.getListaContratoPontoConsumo()) {

			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(contratoPontoConsumo.getPontoConsumo()
							.getChavePrimaria(), IMOVEL);
			ClienteImovel clienteImovel = controladorClienteImovel.obterClienteImovel(pontoConsumo.getImovel().getChavePrimaria(), removido
							.getClienteAssinatura().getChavePrimaria(), Long.valueOf(parametroResponsavelCliente));

			if (clienteImovel != null && !existeOutrosContratosAtivosClienteImovel(clienteImovel, chavesPrimarias)) {
				// Checa se não há outro contrato
				// ativo do cliente para esse
				// imóvel
				controladorClienteImovel.remover(clienteImovel);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #existeOutrosContratosAtivosClienteImovel
	 * (br.com.ggas.cadastro.imovel.ClienteImovel,
	 * java.util.Collection)
	 */
	@Override
	public boolean existeOutrosContratosAtivosClienteImovel(ClienteImovel clienteImovel, Collection<Long> idsContratosDesconsiderar)
					throws NegocioException {

		boolean existe = false;

		if (clienteImovel != null && clienteImovel.getCliente() != null && clienteImovel.getImovel() != null) {
			StringBuilder hql = new StringBuilder();
			hql.append(" select count(contrato.chavePrimaria) from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(CONTRATO);

			hql.append(" inner join contrato.situacao situacao ");
			hql.append(" inner join contrato.listaContratoPontoConsumo listaContratoPontoConsumo ");
			hql.append(" inner join listaContratoPontoConsumo.pontoConsumo pontoConsumo ");
			hql.append(" inner join pontoConsumo.imovel imovel ");
			hql.append(" inner join imovel.listaClienteImovel clienteImovel ");

			hql.append(WHERE);
			hql.append(" clienteImovel.cliente = contrato.clienteAssinatura ");
			hql.append(" and contrato.situacao.chavePrimaria = 1 ");

			hql.append(" and imovel.chavePrimaria = :idImovel ");
			hql.append(" and clienteImovel.cliente.chavePrimaria = :idCliente ");

			if (clienteImovel.getTipoRelacionamentoClienteImovel() != null
							&& clienteImovel.getTipoRelacionamentoClienteImovel().getChavePrimaria() > 0) {
				hql.append(" and clienteImovel.tipoRelacionamentoClienteImovel = :relacionamento ");
			}

			if (idsContratosDesconsiderar != null && !idsContratosDesconsiderar.isEmpty()) {
				hql.append(" and contrato.chavePrimaria not in (:idsContrato) ");
			}

			hql.append(" group by contrato.chavePrimaria ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong(ID_IMOVEL, clienteImovel.getImovel().getChavePrimaria());
			query.setLong(ID_CLIENTE, clienteImovel.getCliente().getChavePrimaria());

			if (clienteImovel.getTipoRelacionamentoClienteImovel() != null
							&& clienteImovel.getTipoRelacionamentoClienteImovel().getChavePrimaria() > 0) {
				query.setLong(RELACIONAMENTO, clienteImovel.getTipoRelacionamentoClienteImovel().getChavePrimaria());
			}

			if (idsContratosDesconsiderar != null && !idsContratosDesconsiderar.isEmpty()) {
				query.setParameterList(IDS_CONTRATO, idsContratosDesconsiderar);
			}

			Long qtdContratos = (Long) query.uniqueResult();
			if(qtdContratos != null){
				existe = qtdContratos > 0;
			}else{
				existe = false;
			}
		}

		return existe;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterSituacaoContrato(long)
	 */
	@Override
	public SituacaoContrato obterSituacaoContrato(long chavePrimaria) throws NegocioException {

		return (SituacaoContrato) super.obter(chavePrimaria, getClasseEntidadeSituacaoContrato());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarSelecaoImovelPontoConsumo
	 * (java.lang.Long[])
	 */
	@Override
	public void validarSelecaoImovelPontoConsumo(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_MULTI_SELECAO_DE_PONTO_CONSUMO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarCamposObrigatoriosContrato
	 * (java.util.Map,
	 * br.com.ggas.contrato.contrato
	 * .ModeloContrato,
	 * br.com.ggas.contrato.contrato.Aba)
	 */
	@Override
	public void validarDatasConsultarExtrato(String dataIni, String dataFim) throws GGASException {

		if (dataIni.isEmpty()) {
			throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_DATA_INICIAL_EXTRATO, dataIni);
		}

		if (dataFim.isEmpty()) {
			throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_DATA_FINAL_EXTRATO, dataFim);
		}
		Date dataInicial = Util.converterCampoStringParaData(DATA, dataIni, Constantes.FORMATO_DATA_BR);
		Date dataFinal = Util.converterCampoStringParaData(DATA, dataFim, Constantes.FORMATO_DATA_BR);

		if (Util.compararDatas(dataInicial, dataFinal) > 0) {
			throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_DATA_INICIAL_MAIOR_EXTRATO, dataFim);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarTodosPontosConsumoDadosInformados(java.lang.Boolean, java.lang.String)
	 */
	@Override
	public void validarTodosPontosConsumoDadosInformados(Boolean todosPontosConsumoDadosInformados, String pontoConsumoNaoInformado)
					throws NegocioException {

		if (!todosPontosConsumoDadosInformados) {
			throw new NegocioException(ERRO_NEGOCIO_TODOS_PONTOS_CUNSUMO_DADOS_INFORMADOS, pontoConsumoNaoInformado);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarCamposObrigatoriosContrato
	 * (java.util.Map,
	 * br.com.ggas.contrato.contrato
	 * .ModeloContrato,
	 * br.com.ggas.contrato.contrato.Aba)
	 */
	@Override
	public void validarCamposObrigatoriosContrato(Map<String, Object> dados, ModeloContrato modeloContrato, Aba aba,
					Boolean validarDadosAditar) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (dados != null && modeloContrato != null) {
			Collection<ModeloAtributo> modeloAtributos = null;
			modeloAtributos = modeloContrato.getAtributos();

			if (aba != null) {
				validarCamposModelo(dados, aba, validarDadosAditar, stringBuilder, modeloAtributos);
			} else {
				validarCamposModelo(dados, stringBuilder, modeloAtributos);
			}

			camposObrigatorios = stringBuilder.toString();
			if (camposObrigatorios.length() > 0) {
				LOG.error(" Campos obrigatorios da aba " + (aba !=null && aba.getDescricao()!=null? aba.getDescricao(): "") + ": "
						+ camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));

				if(aba != null && "Ger".equals(aba.getDescricaoAbreviada())) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
									camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));
				} else {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_ABA,
						new String[] {
								camposObrigatorios.substring(0,
								stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS),(aba !=null && aba.getDescricao()!=null)? aba.getDescricao(): ""});
				}
			}

		}
	}

	private void validarCamposModelo(Map<String, Object> dados, StringBuilder stringBuilder, Collection<ModeloAtributo> modeloAtributos) {

		for (ModeloAtributo modeloAtributo : modeloAtributos) {
			validarAtributo(dados, stringBuilder, modeloAtributo);
		}
	}

	private void validarCamposModelo(Map<String, Object> dados, Aba aba, Boolean validarDadosAditar, StringBuilder stringBuilder,
					Collection<ModeloAtributo> modeloAtributos) {

		for (ModeloAtributo modeloAtributo : modeloAtributos) {
			if (modeloAtributo.getAbaAtributo().getAba().equals(aba)) {

				if(ignorarOperacaoAditar(validarDadosAditar, modeloAtributo)) {
					continue;
				}

				// Tratamento de
				// validações específicas
				// da aba de Modalidade
				// Consumo Faturamento
				if (Aba.REGRAS_FATURAMENTO.equals(aba.getDescricaoAbreviada())
								&& VARIACAO_SUPERIOR_QDC.equals(modeloAtributo.getAbaAtributo().getNomeColuna())
								&& dados.containsKey(QDS_MAIOR_QDC) && "false".equals(dados.get(QDS_MAIOR_QDC))) {
					continue;
				}

				validarAtributo(dados, stringBuilder, modeloAtributo);
			}
		}
	}

	private void validarAtributo(Map<String, Object> dados, StringBuilder stringBuilder, ModeloAtributo modeloAtributo) {

		if (modeloAtributo.isObrigatorio() || isCampoPaiDependenciaObrigatoriaPreenchido(modeloAtributo, dados)) {
			Object valor = dados.get(modeloAtributo.getAbaAtributo().getNomeColuna());
			if (((!dados.containsKey(modeloAtributo.getAbaAtributo().getNomeColuna())) || !this.isCampoContratoPreenchido(valor))
					&& modeloAtributo.getAbaAtributo().isHabilitado()) {
				stringBuilder.append(modeloAtributo.getAbaAtributo().getDescricao());
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
	}

	private boolean ignorarOperacaoAditar(Boolean validarDadosAditar, ModeloAtributo modeloAtributo) {
		// Ignora validação de
		// campos específicos da
		// operação de aditar
		boolean ignorar = false;
		if (!validarDadosAditar) {
			String nomeColuna = modeloAtributo.getAbaAtributo().getNomeColuna();

			if ((nomeColuna.equals(NUMERO_ADITIVO)) || (nomeColuna.equals(DATA_ADITIVO))
							|| (nomeColuna.equals(DESCRICAO_ADITIVO))) {
				ignorar = true;
			}
		}
		return ignorar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarCampoRenovacaoAutomatica(java.lang.String, java.lang.String)
	 */
	@Override
	public void validarCampoRenovacaoAutomatica(String renovacaoAutomatica, String diasRenovacao) throws NegocioException {

		if ("true".equals(renovacaoAutomatica) && StringUtils.isEmpty(diasRenovacao)) {
			throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_RENOVACAO_AUTOMATICA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarCampoPCS(java.lang.Long[], java.lang.String)
	 */
	@Override
	public void validarCampoPCS(Long[] idsLocalAmostragemAssociados, String pcs) throws NegocioException {

		if (!(idsLocalAmostragemAssociados.length == 0)) {
			for (int i = 0; i < idsLocalAmostragemAssociados.length; i++) {
				if (idsLocalAmostragemAssociados[i].compareTo(CONSTANTE_NUMERO_DOIS) == 0 && StringUtils.isEmpty(pcs)) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_RECUPERACAO_PCS, true);
				}
			}
		}
	}

	/**
	 * Método responsável por validar se ,caso
	 * atributo possui pai, a dependência é
	 * obrigatória e o pai foi preenchido.
	 *
	 * @param modeloAtributo
	 *            Um atributo do modelo.
	 * @param dados
	 *            Map com os dados informados.
	 * @return true caso a condição seja
	 *         verdadeira.
	 */
	private boolean isCampoPaiDependenciaObrigatoriaPreenchido(ModeloAtributo modeloAtributo, Map<String, Object> dados) {

		boolean retorno = false;
		if (modeloAtributo.getAbaAtributo().getAbaAtributoPai() != null
						&& modeloAtributo.getAbaAtributo().getDependenciaObrigatoria() != null
						&& modeloAtributo.getAbaAtributo().getDependenciaObrigatoria()) {
			Object valor = dados.get(modeloAtributo.getAbaAtributo().getAbaAtributoPai().getNomeColuna());
			if (dados.containsKey(modeloAtributo.getAbaAtributo().getAbaAtributoPai().getNomeColuna())
							&& this.isCampoContratoPreenchido(valor)) {
				retorno = true;
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarCamposObrigatoriosContrato
	 * (java.util.Map, java.util.List,
	 * br.com.ggas.
	 * contrato.contrato.ModeloContrato)
	 */
	@Override
	public void validarCamposObrigatoriosContrato(Map<String, Object> dados, List<String> campos, ModeloContrato modeloContrato)
					throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderAbas = new StringBuilder();

		String camposObrigatorios = null;
		if (dados != null && modeloContrato != null) {
			Collection<ModeloAtributo> modeloAtributos = null;
			modeloAtributos = modeloContrato.getAtributos();

			for (ModeloAtributo modeloAtributo : modeloAtributos) {

				ArrayList<ModeloAtributo> camposObrigatoriosIgnorados = ignorarCamposObrigatorios(modeloAtributo);

				if ((modeloAtributo.isObrigatorio() || isCampoPaiDependenciaObrigatoriaPreenchido(modeloAtributo, dados))
						&& campos.contains(modeloAtributo.getAbaAtributo().getNomeColuna())
						&& !camposObrigatoriosIgnorados.contains(modeloAtributo)) {
					Object valor = dados.get(modeloAtributo.getAbaAtributo().getNomeColuna());

					if (!dados.containsKey(modeloAtributo.getAbaAtributo().getNomeColuna()) || !this.isCampoContratoPreenchido(valor)) {

						if (isAbaAtributoPercentualCobrAvisoInterrupRetirMaiorMenor(modeloAtributo)) {
							if ("3".equals(dados.get(PENALIDADE_RET_MAIOR_MENOR))) {
								stringBuilder.append(modeloAtributo.getAbaAtributo().getDescricao());
								stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
							}
						} else if (isAbaAtributoPercCobrAvisoInterrupRetirMaiorMenorModalid(modeloAtributo)) {
							if (!isValorConstantePenalidadeRetiradaAMenor(dados)) {
								stringBuilder.append(modeloAtributo.getAbaAtributo().getDescricao());
								stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
							}
						} else {
							stringBuilder.append(modeloAtributo.getAbaAtributo().getDescricao());
							stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
						}

						Aba aba = modeloAtributo.getAbaAtributo().getAba();
						if(!stringBuilderAbas.toString().contains(aba.getDescricao()) && !"Ger".equals(aba.getDescricaoAbreviada()) ) {
							stringBuilderAbas.append(modeloAtributo.getAbaAtributo().getAba().getDescricao());
							stringBuilderAbas.append(Constantes.STRING_VIRGULA_ESPACO);
						}

					}
				}
			}
		}
		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			LOG.error(" Campos obrigatorios: "
					+ camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));

			if(stringBuilderAbas.length() > 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_ABA,
						new String[] {camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS),
						             stringBuilderAbas.toString().substring(0, stringBuilderAbas.length() - REDUCAO_CAMPOS_OBRIGATORIOS)}
						);
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS)
						);
			}

		}

	}

	private ArrayList<ModeloAtributo> ignorarCamposObrigatorios(ModeloAtributo modeloAtributo) {
		ArrayList<String> camposObrigatoriosASeremIgnorados = new ArrayList<>();
		camposObrigatoriosASeremIgnorados.add("apuracaoParadaProgramadaC");
		camposObrigatoriosASeremIgnorados.add("apuracaoFalhaFornecimentoC");
		camposObrigatoriosASeremIgnorados.add("apuracaoCasoFortuitoC");
		camposObrigatoriosASeremIgnorados.add("apuracaoParadaProgramada");
		camposObrigatoriosASeremIgnorados.add("apuracaoFalhaFornecimento");
		camposObrigatoriosASeremIgnorados.add("apuracaoCasoFortuito");
		ArrayList<ModeloAtributo> camposObrigatoriosIgnorados = new ArrayList<>();

		for (String nomeCampo : camposObrigatoriosASeremIgnorados) {
			if(modeloAtributo.getAbaAtributo().getNomeColuna().equals(nomeCampo)){
				camposObrigatoriosIgnorados.add(modeloAtributo);
			}
		}
		return camposObrigatoriosIgnorados;
	}

	/**
	 * Checks if is aba atributo percentual cobr aviso interrup retir maior menor.
	 *
	 * @param modeloAtributo the modelo atributo
	 * @return true, if is aba atributo percentual cobr aviso interrup retir maior menor
	 */
	private boolean isAbaAtributoPercentualCobrAvisoInterrupRetirMaiorMenor(ModeloAtributo modeloAtributo) {

		return ServiceLocator.getInstancia().getControladorModeloContrato()
						.isAbaAtributoPercentualCobrAvisoInterrupRetirMaiorMenor(modeloAtributo.getAbaAtributo());
	}

	/**
	 * Checks if is aba atributo perc cobr aviso interrup retir maior menor modalid.
	 *
	 * @param modeloAtributo the modelo atributo
	 * @return true, if is aba atributo perc cobr aviso interrup retir maior menor modalid
	 */
	private boolean isAbaAtributoPercCobrAvisoInterrupRetirMaiorMenorModalid(ModeloAtributo modeloAtributo) {

		return ServiceLocator.getInstancia().getControladorModeloContrato()
						.isAbaAtributoPercCobrAvisoInterrupRetirMaiorMenorModalid(modeloAtributo.getAbaAtributo());
	}

	/**
	 * Checks if is valor constante penalidade retirada A menor.
	 *
	 * @param dados the dados
	 * @return true, if is valor constante penalidade retirada A menor
	 * @throws NegocioException the negocio exception
	 */
	private boolean isValorConstantePenalidadeRetiradaAMenor(Map<String, Object> dados) throws NegocioException {

		String penalidadeRetMaiorMenor = (String) dados.get(PENALIDADE_RET_MAIOR_MENOR_M);
		long codigoTipoPenalidadeRetMaiorMenor = Long.parseLong(penalidadeRetMaiorMenor);
		return getControladorConstanteSistema()
						.isValorConstantePenalidadeRetiradaAMenor(codigoTipoPenalidadeRetMaiorMenor);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarCamposObrigatoriosContrato
	 * (java.util.Map,
	 * br.com.ggas.contrato.contrato
	 * .ModeloContrato)
	 */
	@Override
	public void validarCamposObrigatoriosContrato(Map<String, Object> dados, ModeloContrato modeloContrato) throws NegocioException {

		this.validarCamposObrigatoriosContrato(dados, modeloContrato, null, true);
	}

	/**
	 * Checks if is campo contrato preenchido.
	 *
	 * @param campo
	 *            the campo
	 * @return true, if is campo contrato preenchido
	 */
	private boolean isCampoContratoPreenchido(Object campo) {

		boolean campoPreenchido = false;
		if (campo != null) {
			if (campo instanceof String && !StringUtils.isEmpty((String) campo) && !"-1".equals(campo)) {
				campoPreenchido = true;
			} else if (campo instanceof Long && (((Long) campo) > 0)) {
				campoPreenchido = true;
			} else if (campo instanceof Integer && (((Integer) campo) > 0)) {
				campoPreenchido = true;
			} else if (campo instanceof BigDecimal && (((BigDecimal) campo).longValue() > 0)) {
				campoPreenchido = true;
			} else if (campo.getClass().isArray() && (((Object[]) campo).length > 0)) {
				campoPreenchido = true;
			} else if (campo instanceof Collection<?> && !(((Collection<?>) campo).isEmpty())) {
				campoPreenchido = true;
			} else if (campo instanceof Boolean) {
				campoPreenchido = true;
			}
		}
		return campoPreenchido;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterAbaContratoPorCodigo
	 * (java.lang.String)
	 */
	@Override
	public Aba obterAbaContratoPorCodigo(String codigo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeAba().getSimpleName());
		hql.append(" where descricaoAbreviada = :descricaoAbreviada ");
		hql.append(" and habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(DESCRICAO_ABREVIADA, codigo);
		query.setMaxResults(1);

		return (Aba) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #criarContratoPontoConsumoItemVencimento()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumoItemVencimento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #criarContratoPontoConsumoModalidade()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumoModalidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #criarContratoPontoConsumoModalidadeQDC()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumoModalidadeQDC() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ContratoPontoConsumoModalidadeQDC.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE_QDC);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #criarContratoPontoConsumoPenalidade()
	 */
	@Override
	public EntidadeNegocio criarContratoPontoConsumoPenalidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						ContratoPontoConsumoPenalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * validarAdicaoContratoPontoConsumoItemFaturamento
	 * (
	 * br.com.ggas.contrato.contrato.ModeloContrato
	 * , java.util.Map,
	 * br.com.ggas.geral.EntidadeConteudo,
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void validarAdicaoContratoPontoConsumoItemFaturamento(ModeloContrato modeloContrato, Map<String, Object> dadosItemFaturamento,
					EntidadeConteudo faseReferencia, EntidadeConteudo opcaoFaseReferencia) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		this.validarCamposObrigatoriosContrato(
						dadosItemFaturamento,
						Arrays.asList(new String[] {ITEM_FATURA, DIA_VENCIMENTO_ITEM_FATURA,
						                            OPCAO_VENCIMENTO_ITEM_FATURA,
						                            INDICADOR_VENC_DIA_NAO_UTIL, TARIFA_CONSUMO,
						                            DEPOSITO_IDENTIFICADO}),
						modeloContrato);

		if (faseReferencia == null
						&& (opcaoFaseReferencia != null && controladorEntidadeConteudo
										.permiteFaseReferencialVencimentoPorOpcaoVencimento(opcaoFaseReferencia.getChavePrimaria()))) {
			stringBuilder.append(ContratoPontoConsumoItemFaturamento.FASE_REFERENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorItemFAturaMargemDistribuicao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO);

		if (dadosItemFaturamento.get(ITEM_FATURA) != null
						&& valorItemFAturaMargemDistribuicao.equals(dadosItemFaturamento.get(ITEM_FATURA))) {

			String percminimoQDC = null;

			if (dadosItemFaturamento.get(PERCMINIMO_QDC) != null) {

				percminimoQDC = (String) dadosItemFaturamento.get(PERCMINIMO_QDC);

			}

			if (percminimoQDC == null || percminimoQDC.isEmpty()) {

				stringBuilder.append(ContratoPontoConsumoItemFaturamento.PERCENTUAL_MINIMO_QDC);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterPenalidadeDemanda(long)
	 */
	@Override
	public PenalidadeDemanda obterPenalidadeDemanda(long chavePrimaria) throws NegocioException {

		return (PenalidadeDemanda) super.obter(chavePrimaria, getClasseEntidadePenalidadeDemanda());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Contrato contrato = (Contrato) entidadeNegocio;

		if (contrato.getNumero() == null) {

			try {

				this.aplicarNumeroAnoContrato(contrato);

			} catch (ConcorrenciaException e) {
				throw new NegocioException(e);
			}
		}

		if (contrato.getNumeroAditivo() == null) {
			contrato.setNumeroAditivo(0);
		}

		if ((contrato.getDataAssinatura() == null || contrato.getArrecadadorContratoConvenio() == null)
				&& contrato.getSituacao().getChavePrimaria() != SituacaoContrato.EM_CRIACAO  && contrato.isHabilitado()) {
			throw new NegocioException(ERRO_CONTRATO_SEM_DATA_ASSINATURA_ARRECADADOR_CONVENIO, true);
		}

		this.associarTipoMedicao(contrato);

	}

	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {
		if (entidadeNegocio instanceof Contrato) {
			Contrato contrato = (Contrato) entidadeNegocio;
			if ((contrato.getDataAssinatura() == null || contrato.getArrecadadorContratoConvenio() == null)
					&& contrato.getSituacao().getChavePrimaria() != SituacaoContrato.EM_CRIACAO && contrato.isHabilitado()) {
				throw new NegocioException(ERRO_CONTRATO_SEM_DATA_ASSINATURA_ARRECADADOR_CONVENIO, true);
			}
		}
		super.preAtualizacao(entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarPontosSelecionados
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void validarPontosSelecionados(Long chaveContrato, Long[] chavesPontoConsumo) throws NegocioException {

		if (chavesPontoConsumo != null && chavesPontoConsumo.length > 0) {

			ServiceLocator.getInstancia().getControladorParametroSistema();

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			String valorContratoAtivo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

			Long chaveContratoPrincipal = 0L;
			if (chaveContrato != null && chaveContrato > 0) {
				Contrato contrato = (Contrato) super.obter(chaveContrato);
				if (contrato != null && contrato.getChavePrimariaPrincipal() != null && contrato.getChavePrimariaPrincipal() > 0) {
					chaveContratoPrincipal = contrato.getChavePrimariaPrincipal();
				}
			}

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select contratoPonto.pontoConsumo from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades =
					HibernateHqlUtil.adicionarClausulaIn(hql, "contratoPonto.pontoConsumo.chavePrimaria", PT_CONS,
							Arrays.asList(chavesPontoConsumo));
			if (chaveContrato != null) {
				hql.append(" and contratoPonto.contrato.chavePrimaria != :chaveContrato ");
			}
			if (chaveContratoPrincipal > 0) {
				hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal != :chaveContratoPrincipal ");
			} else {
				hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");
			}

			hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :situacaoAtiva ");
			hql.append(" and contratoPonto.contrato.habilitado = true ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			try {
				query.setLong(SITUACAO_ATIVA, Util.converterCampoStringParaValorLong(Contrato.SITUACAO_CONTRATO, valorContratoAtivo));
			} catch (FormatoInvalidoException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, Contrato.SITUACAO_CONTRATO);
			}

			if (chaveContrato != null) {
				query.setLong(CHAVE_CONTRATO, chaveContrato);
			}
			if (chaveContratoPrincipal > 0) {
				query.setLong(CHAVE_CONTRATO_PRINCIPAL, chaveContratoPrincipal);
			}
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			List<PontoConsumo> listaPontoConsumo = query.list();

			if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {

				StringBuilder pontosInvalidos = new StringBuilder();
				for (PontoConsumo pontoConsumo : listaPontoConsumo) {
					pontosInvalidos.append(pontoConsumo.getDescricao());
					pontosInvalidos.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				String pontosConsumo =
						pontosInvalidos.toString().substring(0, pontosInvalidos.toString().length() - VERIFICAR_PONTOS_INVALIDOS);

				throw new NegocioException(ERRO_NEGOCIO_PONTOS_CONSUMO_JA_POSSUEM_CONTRATO_ATIVO, pontosConsumo);
			}
		} else {
			this.validarSelecaoImovelPontoConsumo(chavesPontoConsumo);
		}
	}

	/**
	 * Associar tipo medicao.
	 *
	 * @param contrato
	 *            the contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void associarTipoMedicao(Contrato contrato) throws NegocioException {

		for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
			Class<?> tipoMedicaoClasse = ServiceLocator.getInstancia().getClassPorID(TipoMedicao.BEAN_ID_TIPO_MEDICAO);
			PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
			boolean isMedidorVirtual = isMedidorVirtual(pontoConsumo);
			if (isMedidorVirtual) {
				contratoPontoConsumo.setTipoMedicao((TipoMedicao) this.obter(TipoMedicao.CODIGO_DIARIA, tipoMedicaoClasse));
			} else if (possuiCorretorVazao(pontoConsumo)) {
				contratoPontoConsumo.setTipoMedicao((TipoMedicao) this.obter(TipoMedicao.CODIGO_DIARIA, tipoMedicaoClasse));
			} else {
				contratoPontoConsumo.setTipoMedicao((TipoMedicao) this.obter(TipoMedicao.CODIGO_PERIODICA, tipoMedicaoClasse));
			}
		}
	}

	/**
	 * Checks if is medidor virtual.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return true, if is medidor virtual
	 */
	private boolean isMedidorVirtual(PontoConsumo pontoConsumo) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
		Medidor medidor = null;
		if (instalacaoMedidor != null) {
			medidor = instalacaoMedidor.getMedidor();
		}
		boolean isMedidorVirtual = false;
		if (medidor != null && medidor.getModoUso().getChavePrimaria() == Long
				.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL))) {
			isMedidorVirtual = true;
		}
		return isMedidorVirtual;
	}

	/**
	 * Método responsável por atribuir o ano e o
	 * número do contrato de acordo com os
	 * parÂmetros do sistema.
	 *
	 * @param contrato
	 *            O contrato
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro de
	 *             concorrência.
	 */
	private synchronized void aplicarNumeroAnoContrato(Contrato contrato) throws NegocioException, ConcorrenciaException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String valorIndicadorReinicio = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REINICIO_ANUAL_SEQUENCIA_CONTRATO);

		ParametroSistema parametroSequenciaContrato = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_SEQUENCIA_CONTRATO);

		Integer valorSequenciaContrato = Integer.valueOf(parametroSequenciaContrato.getValor());

		if (valorIndicadorReinicio != null && (Boolean.valueOf(valorIndicadorReinicio) || "1".equals(valorIndicadorReinicio))) {
			ParametroSistema parametroAnoContrato = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_ANO_CONTRATO);
			String valorAnoContratoParametro = parametroAnoContrato.getValor();
			Integer valorAnoContrato = Integer.valueOf(valorAnoContratoParametro);

			Calendar cal = Calendar.getInstance();
			contrato.setAnoContrato(cal.get(Calendar.YEAR));

			if (!contrato.getAnoContrato().equals(valorAnoContrato)) {

				contrato.setNumero(1);
				parametroAnoContrato.setValor(String.valueOf(cal.get(Calendar.YEAR)));
				parametroAnoContrato.setDadosAuditoria(contrato.getDadosAuditoria());
				parametroAnoContrato.setUltimaAlteracao(Calendar.getInstance().getTime());
				controladorParametroSistema.atualizar(parametroAnoContrato);

			} else {
				contrato.setNumero( valorSequenciaContrato + 1);
			}

			atualizarValorParametroSequenciaContrato(contrato.getNumero().toString(), contrato.getDadosAuditoria(),
							parametroSequenciaContrato);

		} else {

			contrato.setNumero(valorSequenciaContrato + 1);
			atualizarValorParametroSequenciaContrato(contrato.getNumero().toString(), contrato.getDadosAuditoria(),
							parametroSequenciaContrato);

		}
	}

	/**
	 * Atualizar valor parametro sequencia contrato.
	 *
	 * @param valor
	 *            the valor
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param parametro
	 *            the parametro
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarValorParametroSequenciaContrato(String valor, DadosAuditoria dadosAuditoria, ParametroSistema parametro)
					throws ConcorrenciaException, NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		parametro.setValor(valor);
		parametro.setDadosAuditoria(dadosAuditoria);
		parametro.setUltimaAlteracao(Calendar.getInstance().getTime());
		controladorParametroSistema.atualizar(parametro);

	}

	/**
	 * Possui corretor vazao.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return true, if successful
	 */
	private boolean possuiCorretorVazao(PontoConsumo pontoConsumo) {

		boolean retorno = false;
		Class<?> pontoConsumoClasse = ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(medidor) from ");
		hql.append(pontoConsumoClasse.getSimpleName());
		hql.append(" pontoConsumo");
		hql.append(" inner join pontoConsumo.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join instalacaoMedidor.medidor medidor ");
		hql.append(" where instalacaoMedidor.vazaoCorretor is not null ");
		hql.append(" and pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PONTO_CONSUMO, pontoConsumo.getChavePrimaria());

		Long quantidade = (Long) query.uniqueResult();

		if (quantidade != null && quantidade > 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarRelacaoResponsabilidade
	 * (java.util.Collection,
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void validarRelacaoResponsabilidade(Collection<ContratoCliente> listaContratoCliente, ContratoCliente contratoCliente)
					throws NegocioException {

		if ((listaContratoCliente != null) && (!listaContratoCliente.isEmpty())) {
			for (ContratoCliente contratoClienteExistente : listaContratoCliente) {
				if (contratoClienteExistente.getCliente().getChavePrimaria() == contratoCliente.getCliente().getChavePrimaria()) {
					throw new NegocioException(ERRO_CLIENTE_RESPONSABILIDADE_JA_EXISTENTE, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDadosResponsabilidade
	 * (br.com.ggas.contrato
	 * .contrato.ContratoCliente)
	 */
	@Override
	public void validarDadosResponsabilidade(ContratoCliente contratoCliente) throws NegocioException {

		Map<String, Object> errosContratoCliente = contratoCliente.validarDados();

		if (errosContratoCliente != null && !errosContratoCliente.isEmpty()) {
			throw new NegocioException(errosContratoCliente);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarContratoPontoConsumo(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumo(Long chavePrimariaContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO2);
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch contratoPontoConsumo.unidadePressao unidadePressao ");
		hql.append(" left join fetch contratoPontoConsumo.periodicidade periodicidade ");
		hql.append(" left join fetch contratoPontoConsumo.unidadePressaoMinima unidadePressaoMinima ");
		hql.append(" left join fetch contratoPontoConsumo.unidadePressaoMaxima unidadePressaoMaxima ");
		hql.append(" left join fetch contratoPontoConsumo.unidadePressaoLimite unidadePressaoLimite ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeVazaoInstantanea unidadeVazaoInstantanea ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeVazaoMaximaInstantanea unidadeVazaoMaximaInstantanea ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeVazaoMinimaInstantanea unidadeVazaoMinimaInstantanea ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMaxDiaria unidadeFornecimentoMaxDiaria ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMinDiaria unidadeFornecimentoMinDiaria ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMinMensal unidadeFornecimentoMinMensal ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMinAnual unidadeFornecimentoMinAnual ");
		hql.append(" left join fetch contratoPontoConsumo.regimeConsumo regimeConsumo ");
		hql.append(" left join fetch contratoPontoConsumo.cep cep ");
		hql.append(" where contratoPontoConsumo.contrato.chavePrimaria = :chavePrimariaContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO, chavePrimariaContrato);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarContratoPontoConsumo(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumoPorPonto(Long chavePontoConsumo, Long chavePrimariaContrato)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO2);
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch contratoPontoConsumo.unidadePressao unidadePressao ");
		hql.append(" left join fetch contratoPontoConsumo.periodicidade periodicidade ");
		hql.append(" left join fetch contratoPontoConsumo.unidadePressaoMinima unidadePressaoMinima ");
		hql.append(" left join fetch contratoPontoConsumo.unidadePressaoMaxima unidadePressaoMaxima ");
		hql.append(" left join fetch contratoPontoConsumo.unidadePressaoLimite unidadePressaoLimite ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeVazaoInstantanea unidadeVazaoInstantanea ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeVazaoMaximaInstantanea unidadeVazaoMaximaInstantanea ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeVazaoMinimaInstantanea unidadeVazaoMinimaInstantanea ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMaxDiaria unidadeFornecimentoMaxDiaria ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMinDiaria unidadeFornecimentoMinDiaria ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMinMensal unidadeFornecimentoMinMensal ");
		hql.append(" left join fetch contratoPontoConsumo.unidadeFornecimentoMinAnual unidadeFornecimentoMinAnual ");
		hql.append(" left join fetch contratoPontoConsumo.regimeConsumo regimeConsumo ");
		hql.append(" left join fetch contratoPontoConsumo.cep cep ");
		hql.append(" where contratoPontoConsumo.contrato.chavePrimaria = :chavePrimariaContrato ");
		hql.append(" and contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO, chavePrimariaContrato);
		query.setLong(CHAVE_PONTO_CONSUMO, chavePontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarContratoPontoConsumo(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<ContratoQDC> listarContratoQDC(Long chavePontoConsumo, Long chavePrimariaContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoQDC from ");
		hql.append(getClasseEntidadeContratoQDC().getSimpleName());
		hql.append(" contratoQDC");
		hql.append(" left join fetch contratoQDC.contrato contrato ");
		hql.append(" left join fetch contrato.listaContratoPontoConsumo contratoPontoConsumo ");
		hql.append(" where contratoPontoConsumo.contrato.chavePrimaria = :chavePrimariaContrato ");
		hql.append(" and contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO, chavePrimariaContrato);
		query.setLong(CHAVE_PONTO_CONSUMO, chavePontoConsumo);

		return query.list();
	}

	/*
	 * [(non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarLocalAmostragemPCSPorCPC
	 * (br.com.ggas
	 * .contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public Collection<EntidadeConteudo> consultarLocalAmostragemPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cpcPCSAmostragem.localAmostragemPCS ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoPCSAmostragem().getSimpleName());
		hql.append(" cpcPCSAmostragem where ");
		hql.append(" cpcPCSAmostragem.contratoPontoConsumo.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, contratoPontoConsumo.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarIntervaloPCSPorCPC
	 * (br.com.ggas.contrato
	 * .contrato.ContratoPontoConsumo)
	 */
	@Override
	public Collection<IntervaloPCS> consultarIntervaloPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cpcIntervaloPCS.intervaloPCS ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoIntervaloPCS().getSimpleName());
		hql.append(" cpcIntervaloPCS where ");
		hql.append(" cpcIntervaloPCS.contratoPontoConsumo.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, contratoPontoConsumo.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterTamanhoIntervaloPCSPorCPC
	 * (br.com.ggas.
	 * contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public Integer obterTamanhoIntervaloPCSPorCPC(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cpcIntervaloPCS.tamanho ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoIntervaloPCS().getSimpleName());
		hql.append(" cpcIntervaloPCS where ");
		hql.append(" cpcIntervaloPCS.contratoPontoConsumo.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);
		query.setLong(0, contratoPontoConsumo.getChavePrimaria());

		return (Integer) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarItensVencimentoPorContratoPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumoItemFaturamento> listarItensVencimentoPorContratoPontoConsumo(
					Long chavePrimariaContratoPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct itemVencimento from ");
		hql.append(getClasseEntidadeContratoPontoConsumoItemVencimento().getSimpleName());
		hql.append(" itemVencimento");
		hql.append(" left join fetch itemVencimento.itemFatura itemFatura ");
		hql.append(" left join fetch itemVencimento.faseReferencia faseReferencia ");
		hql.append(" left join fetch itemVencimento.opcaoFaseReferencia opcaoFaseReferencia ");

		hql.append(" inner join fetch itemVencimento.tarifa tarifa ");

		hql.append(" where itemVencimento.contratoPontoConsumo.chavePrimaria = :chavePrimariaContratoPontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO_PONTO_CONSUMO, chavePrimariaContratoPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #aditarContrato(br.com.ggas
	 * .contrato.contrato.Contrato,
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public Long aditarContrato(Contrato contratoAditado, Contrato contratoAlterado) throws NegocioException, ConcorrenciaException {

		Long chavePrimaria = null;

		this.validarContratoAditado(contratoAditado);

		contratoAlterado.setVersao(contratoAditado.getVersao());
		contratoAlterado.setHabilitado(Boolean.FALSE);
		contratoAlterado.setVersao(contratoAditado.getVersao());

		if (contratoAlterado.getChavePrimariaPai() == null) {
			contratoAditado.setChavePrimariaPai(contratoAlterado.getChavePrimaria());
		}

		SituacaoContrato situacao = this.obterSituacaoContrato(SituacaoContrato.ADITADO);
		contratoAlterado.setSituacao(situacao);

		this.atualizar(contratoAlterado);

		contratoAditado.setNumeroAditivo(obterProximoNumeroAditivo(contratoAlterado.getNumero(), contratoAlterado.getAnoContrato()));
		contratoAditado.setAnoContrato(contratoAlterado.getAnoContrato());
		contratoAditado.substituirListaAnexo();

		if (contratoAditado.getSituacao() != null) {
			validarContratoDataRecisao(contratoAditado);
		}

		SituacaoContrato situacaoCancelado = this.obterSituacaoContrato(SituacaoContrato.CANCELADO);
		if (contratoAditado.getSituacao().getChavePrimaria() == situacaoCancelado.getChavePrimaria()) {
			contratoAditado.setHabilitado(Boolean.FALSE);
			chavePrimaria = this.inserirContratoCanceladoEncerrado(contratoAditado);
		} else {
			SituacaoContrato situacaoAtivo = this.obterSituacaoContrato(SituacaoContrato.ATIVO);
			contratoAditado.setSituacao(situacaoAtivo);
			contratoAditado.setHabilitado(Boolean.TRUE);
			chavePrimaria = this.inserir(contratoAditado);
		}

		return chavePrimaria;
	}

	@Override
	public Long atualizarContrato(Contrato contrato, Contrato contratoAlterado, Boolean migracaoContrato) throws NegocioException {
		Long chavePrimaria = null;
		SituacaoContrato situacaoAnterior = contratoAlterado.getSituacao();

		contratoAlterado.setHabilitado(Boolean.FALSE);
		contratoAlterado.setVersao(contrato.getVersao());
		contratoAlterado.setUltimaAlteracao(Calendar.getInstance().getTime());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		long valorSituacaoContratoAtivo =
				Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));

		if (contratoAlterado.getChavePrimariaPai() == null
				&& contratoAlterado.getSituacao().getChavePrimaria() == valorSituacaoContratoAtivo) {
			contrato.setChavePrimariaPai(contratoAlterado.getChavePrimaria());
		}

		SituacaoContrato situacao = this.obterSituacaoContrato(SituacaoContrato.ALTERADO);
		contratoAlterado.setSituacao(situacao);
		try {

			this.atualizar(contratoAlterado);
			this.getSessionFactory().getCurrentSession().clear();

			if (contratoAlterado.getSituacao().getChavePrimaria() != SituacaoContrato.EM_CRIACAO && contratoAlterado.isHabilitado()
					&& contrato.getDataAssinatura() != null && !contrato.getDataAssinatura().equals(contratoAlterado.getDataAssinatura())) {

				throw new NegocioException("Data de Assinatura só pode ser alterada para contratos com situação EM CRIAÇÃO");
			}
			contrato.setChavePrimaria(0);
			contrato.setAnoContrato(contratoAlterado.getAnoContrato());
			contrato.setNumero(contratoAlterado.getNumero());
			contrato.setNumeroAditivo(contratoAlterado.getNumeroAditivo());
			contrato.setDataAditivo(contratoAlterado.getDataAditivo());
			contrato.setDescricaoAditivo(contratoAlterado.getDescricaoAditivo());
			contrato.setOrdemFaturamento(contratoAlterado.getOrdemFaturamento());
			
			if(!"ENCERRADO".equals(contrato.getSituacao().getDescricao())) {
				contrato.setHabilitado(!contratoAlterado.isHabilitado());
			}
			contrato.substituirListaAnexo();

			if (contrato.getSituacao() != null) {
				validarContratoDataRecisao(contrato);
			}

			if ((contrato.getSituacao().getChavePrimaria() == SituacaoContrato.CANCELADO)
					|| (contrato.getSituacao().getChavePrimaria() == SituacaoContrato.ENCERRADO)) {

				chavePrimaria = this.inserirContratoCanceladoEncerrado(contrato);

			} else {
				if (!migracaoContrato) {
					validarFluxoAprovacao(contrato, situacaoAnterior);
				}
				chavePrimaria = this.inserir(contrato);
			}

			getHibernateTemplate().getSessionFactory().getCurrentSession().clear();

			long chavePrimariaContratoAlterado = contratoAlterado.getChavePrimaria();

			ControladorProgramacao controladorProgramacao = ServiceLocator.getInstancia().getControladorProgramacao();

			controladorProgramacao.migrarSolicitacaoConsumoAlterarContrato(chavePrimariaContratoAlterado, chavePrimaria);
			registrarIntegracaoContrato(contrato, contratoAlterado);
		} catch (ConcorrenciaException e) {
			LOG.error(e);
			throw new NegocioException(e);
		} catch (GGASException e) {
			LOG.error("Erro na atualização doo contrato", e);
			throw new NegocioException(e.getChaveErro(), true);
		}
		return chavePrimaria;
	}

	/**
	 * Registra as alterações no contrato para IntegracaoContrato
	 * @param contrato O novo contrato gerado após a alteração
	 * @param contratoAlterado O contrato original que foi alterado
	 * @throws GGASException O GGASException
	 */
	private void registrarIntegracaoContrato(Contrato contrato, Contrato contratoAlterado) throws GGASException {
		ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		if (contrato.getSituacao().getChavePrimaria() == ATIVO) {
			Fachada fachada = Fachada.getInstancia();
			if (contrato.getClienteAssinatura().equals(contratoAlterado.getClienteAssinatura())) {
				controladorIntegracao.inserirIntegracaoContrato(contrato);
			} else {
				controladorIntegracao.alterarTitularDeContratoAtivo(contrato, contratoAlterado);
			}
			List<PontoConsumo> listaPontosConsumoRemovidos = new ArrayList<>();
			for (ContratoPontoConsumo contratoPontoConsumo : contratoAlterado.getListaContratoPontoConsumo()) {
				listaPontosConsumoRemovidos.add(fachada.obterPontoConsumo(contratoPontoConsumo.getPontoConsumo().getChavePrimaria()));
			}
			List<PontoConsumo> listaPontoConsumoSessao = new ArrayList<>();
			for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
				listaPontoConsumoSessao.add(fachada.obterPontoConsumo(contratoPontoConsumo.getPontoConsumo().getChavePrimaria()));
			}

			listaPontosConsumoRemovidos.removeAll(listaPontoConsumoSessao);
			if (!listaPontosConsumoRemovidos.isEmpty()) {
				List<ContratoPontoConsumo> pontosRemovidos = new ArrayList<>();
				for (ContratoPontoConsumo contratoPontoConsumo : contratoAlterado.getListaContratoPontoConsumo()) {
					if (listaPontosConsumoRemovidos.contains(contratoPontoConsumo.getPontoConsumo())) {
						pontosRemovidos.add(contratoPontoConsumo);
					}
				}
				controladorIntegracao.excluirIntegracaoContratoPorPontoConsumo(contrato, pontosRemovidos);
			}
		} else if (contrato.getSituacao().getChavePrimaria() == SituacaoContrato.ENCERRADO &&
				contratoAlterado.getSituacao().getChavePrimaria() == ALTERADO) {
			controladorIntegracao.excluirIntegracaoContratoPorPontoConsumo(contrato,
					new ArrayList<>(contrato.getListaContratoPontoConsumo()));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.contrato.contrato. ControladorContrato #alterarContrato(br.com.ggas .contrato.contrato.Contrato,
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public Long atualizarContrato(Contrato contrato, Contrato contratoAlterado) throws NegocioException {

		return atualizarContrato(contrato, contratoAlterado, false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#atualizarDataRecisaoContrato(java.lang.Long, java.util.Date,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void atualizarDataRecisaoContrato(Long chaveContrato, Date dataRecisao, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		Contrato contrato = (Contrato) obter(chaveContrato);
		contrato.setDataRecisao(dataRecisao);
		contrato.setDadosAuditoria(dadosAuditoria);

		this.atualizar(contrato);
	}

	/**
	 * Validar contrato aditado.
	 *
	 * @param contrato
	 *            the contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarContratoAditado(Contrato contrato) throws NegocioException {

		if (contrato.getListaContratoPontoConsumo() != null && !contrato.getListaContratoPontoConsumo().isEmpty()) {
			for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
				if (contratoPontoConsumo.getMedidaPressao() == null || contratoPontoConsumo.getUnidadePressao() == null) {
					throw new NegocioException(ERRO_NEGOCIO_PRESSAO_FORNECIMENTO_NAO_INFORMADA, new Object[] {contratoPontoConsumo
									.getPontoConsumo().getDescricao()});
				}
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_PONTO_CONSUMO_NAO_INFORMADO, true);
		}
	}

	/**
	 * Obter proximo numero aditivo.
	 *
	 * @param numero
	 *            the numero
	 * @param anoContrato
	 *            the ano contrato
	 * @return the integer
	 */
	private Integer obterProximoNumeroAditivo(Integer numero, Integer anoContrato) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(numeroAditivo) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where numero = :numero ");
		hql.append(" and anoContrato = :anoContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger(NUMERO, numero);
		query.setInteger(ANO_CONTRATO, anoContrato);

		Integer numeroAditivo = (Integer) query.uniqueResult();
		if (numeroAditivo != null) {
			return numeroAditivo+1;
		}

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarModalidadesPorContratoPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidade> listarModalidadesPorContratoPontoConsumo(Long chavePrimariaContratoPontoConsumo)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPontoConsumoModalidade from ");
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName()).append(" contratoPontoConsumoModalidade");
		hql.append(" left join fetch contratoPontoConsumoModalidade.contratoModalidade contratoModalidade ");
		hql.append(" left join fetch contratoPontoConsumoModalidade.listaContratoPontoConsumoModalidadeQDC ")
				.append("listaContratoPontoConsumoModalidadeQDC ");
		hql.append(" left join fetch contratoPontoConsumoModalidade.listaContratoPontoConsumoPenalidade listaContratoPontoConsumoPenalidade ");
		hql.append(" where contratoPontoConsumoModalidade.contratoPontoConsumo.chavePrimaria = :chavePrimariaContratoPontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO_PONTO_CONSUMO, chavePrimariaContratoPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listaContratoClientePorPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoCliente> listarContratoClientePorPontoConsumo(Long idPontoConsumo, Long idContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoCliente().getSimpleName()).append(ALIAS_CONTRATO_CLIENTE);
		hql.append(" inner join fetch contratoCliente.responsabilidade responsabilidade ");
		hql.append(" inner join fetch contratoCliente.cliente cliente ");
		hql.append(" where contratoCliente.contrato.chavePrimaria = :idContrato ");
		hql.append(" and contratoCliente.pontoConsumo.chavePrimaria = :idPontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_CONTRATO, idContrato);
		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * listarConsumoPenalidadePorContratoPontoConsumoModalidade
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumoPenalidade> listarConsumoPenalidadePorContratoPontoConsumoModalidade(
					Long chavePrimariaContratoPontoConsumoModalidade) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPontoConsumoPenalidade from ");
		hql.append(getClasseEntidadeContratoPontoConsumoPenalidade().getSimpleName()).append(" contratoPontoConsumoPenalidade ");
		hql.append(" left join fetch contratoPontoConsumoPenalidade.periodicidadePenalidade periodicidadePenalidade ");
		hql.append(" left join fetch contratoPontoConsumoPenalidade.consumoReferencia consumoReferencia ");

		hql.append(" where contratoPontoConsumoPenalidade.contratoPontoConsumoModalidade.chavePrimaria = ")
				.append(":chavePrimariaContratoPontoConsumoModalidade ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO_PONTO_CONSUMO_MODALIDADE, chavePrimariaContratoPontoConsumoModalidade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDatasAbaResponsabilidade
	 * (br.com.ggas
	 * .contrato.contrato.ContratoCliente,
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarDatasAbaResponsabilidade(ContratoCliente contratoCliente, Contrato contrato) throws NegocioException {

		if (contrato != null) {
			Date dataInicioRelacao = contratoCliente.getRelacaoInicio();
			Date dataFimRelacao = contratoCliente.getRelacaoFim();

			if ((dataInicioRelacao != null) && (dataFimRelacao != null) && (Util.compararDatas(dataFimRelacao, dataInicioRelacao) < 0)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_RELACAO_MENOR_QUE_INICIO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDatasAbaModalidade
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public void validarDatasAbaModalidade(String inicioRecuperacao, String fimRecuperacao, Contrato contrato) throws NegocioException {

		if (contrato != null) {
			Date dataAssinatura = contrato.getDataAssinatura();
			Date dataInicioRecuperacao = null;
			Date dataFimRecuperacao = null;
			if ((inicioRecuperacao != null) && (!StringUtils.isEmpty(inicioRecuperacao))) {
				try {
					dataInicioRecuperacao = Util.converterCampoStringParaData(DATA, inicioRecuperacao, Constantes.FORMATO_DATA_BR);
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
				}
			}
			if ((fimRecuperacao != null) && (!StringUtils.isEmpty(fimRecuperacao))) {
				try {
					dataFimRecuperacao = Util.converterCampoStringParaData(DATA, fimRecuperacao, Constantes.FORMATO_DATA_BR);
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
				}
			}
			if (dataAssinatura != null) {
				if ((dataInicioRecuperacao != null) && (Util.compararDatas(dataInicioRecuperacao, dataAssinatura) < 0)) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_RECUPERACAO_MENOR_QUE_ASSINATURA, true);
				}
				if ((dataFimRecuperacao != null) && (Util.compararDatas(dataFimRecuperacao, dataAssinatura) < 0)) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_MAXIMA_RECUPERACAO_MENOR_QUE_ASSINATURA, true);
				}
			}
			if ((dataInicioRecuperacao != null) && (dataFimRecuperacao != null)
							&& (Util.compararDatas(dataFimRecuperacao, dataInicioRecuperacao) < 0)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_MAXIMA_RECUPERACAO_MENOR_QUE_INICIO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDataAssinaturaContrato
	 * (java.lang.String)
	 */
	@Override
	public void validarDataAssinaturaContrato(String data) throws GGASException {

		if (!StringUtils.isEmpty(data)) {
			Date dataAssinatura = Util.converterCampoStringParaData(DATA, data, Constantes.FORMATO_DATA_BR);

			DateTime dataAtual = new DateTime(Calendar.getInstance().getTime());
			dataAtual = dataAtual.withHourOfDay(0);
			dataAtual = dataAtual.withMinuteOfHour(0);
			dataAtual = dataAtual.withSecondOfMinute(0);
			dataAtual = dataAtual.withMillisOfSecond(0);
			if (Util.compararDatas(dataAssinatura, dataAtual.toDate()) > 0) {
				throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_CONTRATO_DATA_ASSINATURA, Contrato.DATA_ASSINATURA);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDataAssinaturaContrato
	 * (java.lang.String)
	 */
	@Override
	public void validarSituacaoDataAssinaturaArrecadadador(String dataAssinatura, Long arrecadadorConvenio, Long situacaoContrato)
			throws NegocioException {
		if ((dataAssinatura == null || dataAssinatura.length() == 0 || arrecadadorConvenio == null || arrecadadorConvenio == -1)
				&& situacaoContrato != SituacaoContrato.EM_CRIACAO) {
			throw new NegocioException(ERRO_CONTRATO_SEM_DATA_ASSINATURA_ARRECADADOR_CONVENIO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDatasAbaPrincipal
	 * (java.util.HashMap,
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarDatasAbaPrincipal(Map<String, Object> abaPrincipal, Contrato contrato) throws NegocioException {

		if (abaPrincipal != null) {

			String periodoTesteInicio = (String) abaPrincipal.get("periodoTesteDataInicio");
			String periodoTesteFim = (String) abaPrincipal.get("periodoTesteDateFim");
			String inicioGarantiaConversao = (String) abaPrincipal.get("inicioGarantiaConversao");

			Date periodoTesteDataInicio = null;
			if ((periodoTesteInicio != null) && (!StringUtils.isEmpty(periodoTesteInicio))) {
				try {
					periodoTesteDataInicio = Util.converterCampoStringParaData(DATA, periodoTesteInicio, Constantes.FORMATO_DATA_BR);
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
				}
			}
			Date periodoTesteDateFim = null;
			if ((periodoTesteFim != null) && (!StringUtils.isEmpty(periodoTesteFim))) {
				try {
					periodoTesteDateFim = Util.converterCampoStringParaData(DATA, periodoTesteFim, Constantes.FORMATO_DATA_BR);
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
				}
			}
			Date inicioDataGarantiaConversao = null;
			if ((inicioGarantiaConversao != null) && (!StringUtils.isEmpty(inicioGarantiaConversao))) {
				try {
					inicioDataGarantiaConversao = Util.converterCampoStringParaData(DATA, inicioGarantiaConversao,
									Constantes.FORMATO_DATA_BR);
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
				}
			}
			if (contrato != null) {
				Date dataAssinatura = contrato.getDataAssinatura();
				Date dataVencimento = contrato.getDataVencimentoObrigacoes();
				if (dataAssinatura != null) {
					if ((periodoTesteDataInicio != null) && (Util.compararDatas(periodoTesteDataInicio, dataAssinatura) < 0)) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_TESTE_MENOR_QUE_ASSINATURA, true);
					}
					if ((periodoTesteDateFim != null) && (Util.compararDatas(periodoTesteDateFim, dataAssinatura) < 0)) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_TESTE_MENOR_QUE_ASSINATURA, true);
					}
					if ((inicioDataGarantiaConversao != null) && (Util.compararDatas(inicioDataGarantiaConversao, dataAssinatura) < 0)) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_GARANTIA_CONVERSAO_MENOR_QUE_ASSINATURA, true);
					}
				}
				if (dataVencimento != null) {
					if ((periodoTesteDataInicio != null) && (Util.compararDatas(periodoTesteDataInicio, dataVencimento) > 0)) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_TESTE_MAIOR_QUE_VENCIMENTO, true);
					}
					if ((periodoTesteDateFim != null) && (Util.compararDatas(periodoTesteDateFim, dataVencimento) > 0)) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_TESTE_MAIOR_QUE_VENCIMENTO, true);
					}
					if ((inicioDataGarantiaConversao != null) && (Util.compararDatas(inicioDataGarantiaConversao, dataVencimento) > 0)) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_GARANTIA_CONVERSAO_MAIOR_QUE_VENCIMENTO, true);
					}
				}
			}
			if (periodoTesteDataInicio != null && periodoTesteDateFim != null
							&& (Util.compararDatas(periodoTesteDateFim, periodoTesteDataInicio) < 0)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_TESTE_MENOR_QUE_INICIO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDatasContrato(br.
	 * com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarDatasContrato(Contrato contrato) throws NegocioException {

		if (contrato != null) {
			Date dataAssinatura = contrato.getDataAssinatura();
			Date dataVencimento = contrato.getDataVencimentoObrigacoes();
			validarDataVencimento(dataAssinatura, dataVencimento);
			Date dataAditivo = contrato.getDataAditivo();
			validarDataAditivo(dataAssinatura, dataAditivo, contrato.getNumero(), contrato.getAnoContrato());
			Date inicioGarantiaFinanciamento = contrato.getDataInicioGarantiaFinanciamento();
			Date fimGarantiaFinanciamento = contrato.getDataFimGarantiaFinanciamento();
			validarDatasGarantiaFinanceira(dataAssinatura, dataVencimento, inicioGarantiaFinanciamento, fimGarantiaFinanciamento);
		}
	}

	/**
	 * Validar datas garantia financeira.
	 *
	 * @param dataAssinatura
	 *            the data assinatura
	 * @param dataVencimento
	 *            the data vencimento
	 * @param inicioGarantiaFinanciamento
	 *            the inicio garantia financiamento
	 * @param fimGarantiaFinanciamento
	 *            the fim garantia financiamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDatasGarantiaFinanceira(Date dataAssinatura, Date dataVencimento, Date inicioGarantiaFinanciamento,
					Date fimGarantiaFinanciamento) throws NegocioException {

		if ((inicioGarantiaFinanciamento != null) && (dataAssinatura != null)
						&& (Util.compararDatas(inicioGarantiaFinanciamento, dataAssinatura) < 0)) {
			throw new NegocioException(ERRO_NEGOCIO_INICIO_GARANTIA_FINANCIAMENTO_MENOR_QUE_ASSINATURA, true);
		}

		if ((inicioGarantiaFinanciamento != null) && (dataVencimento != null)
						&& (Util.compararDatas(inicioGarantiaFinanciamento, dataVencimento) > 0)) {
			throw new NegocioException(ERRO_NEGOCIO_INICIO_GARANTIA_FINANCIAMENTO_MAIOR_QUE_VENCIMENTO, true);
		}

		if ((fimGarantiaFinanciamento != null) && (dataAssinatura != null)
						&& (Util.compararDatas(fimGarantiaFinanciamento, dataAssinatura) < 0)) {
			throw new NegocioException(ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MENOR_QUE_ASSINATURA, true);
		}

		if ((fimGarantiaFinanciamento != null) && (dataVencimento != null)
						&& (Util.compararDatas(fimGarantiaFinanciamento, dataVencimento) > 0)) {
			throw new NegocioException(ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MAIOR_QUE_VENCIMENTO, true);
		}

		if ((inicioGarantiaFinanciamento != null) && (fimGarantiaFinanciamento != null)
						&& (Util.compararDatas(fimGarantiaFinanciamento, inicioGarantiaFinanciamento) < 0)) {
			throw new NegocioException(ERRO_NEGOCIO_FIM_GARANTIA_FINANCIAMENTO_MENOR_QUE_INICIO, true);
		}
	}

	/**
	 * Validar data vencimento.
	 *
	 * @param dataAssinatura
	 *            the data assinatura
	 * @param dataVencimento
	 *            the data vencimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDataVencimento(Date dataAssinatura, Date dataVencimento) throws NegocioException {

		if ((dataAssinatura != null) && (dataVencimento != null) && (dataVencimento.compareTo(dataAssinatura) < 0)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA, true);
		}
	}

	/**
	 * Validar data aditivo.
	 *
	 * @param dataAssinatura the data assinatura
	 * @param dataAditivo the data aditivo
	 * @param numero the numero
	 * @param anoContrato the ano contrato
	 * @throws NegocioException the negocio exception
	 */
	private void validarDataAditivo(Date dataAssinatura, Date dataAditivo, Integer numero, Integer anoContrato) throws NegocioException {

		if (dataAditivo != null) {
			if ((dataAssinatura != null) && (Util.compararDatas(dataAditivo, dataAssinatura) < 0)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_ADITIVO_MENOR_ASSINATURA, true);
			}

			StringBuilder hql = new StringBuilder();
			hql.append(" select count(contrato) from ");
			hql.append(getClasseEntidade().getSimpleName()).append(CONTRATO);
			hql.append(WHERE);
			hql.append(" contrato.anoContrato = :anoContrato ");
			hql.append(" and contrato.numero = :numeroContrato ");
			hql.append(" and contrato.dataAditivo >= :dataAditivo ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setInteger(NUMERO_CONTRATO, numero);
			query.setDate(DATA_ADITIVO, dataAditivo);
			query.setInteger(ANO_CONTRATO, anoContrato);

			Long qtdContrato = (Long) query.uniqueResult();

			if ((qtdContrato != null) && (qtdContrato > 0)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_ADITIVO_INVALIDO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarAdicaoPenalidade(
	 * br.com.ggas.contrato
	 * .contrato.ContratoPontoConsumoPenalidade,
	 * java.util.Collection)
	 */
	@Override
	public void validarAdicaoPenalidade(ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade,
					Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) throws NegocioException {

		if (contratoPontoConsumoPenalidade != null && contratoPontoConsumoPenalidade.getPercentualMargemVariacao() != null
						&& contratoPontoConsumoPenalidade.getPercentualMargemVariacao().compareTo(BigDecimal.ONE) > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
							new Object[] {ContratoPontoConsumoPenalidade.PERCENTUAL_MARGEM_VARIACAO});
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarPercentualMaximoQDC
	 * (java.math.BigDecimal)
	 */
	@Override
	public void validarPercentualMaximoQDC(BigDecimal percentualMinimoQDC, BigDecimal percentualMaximoQDC) throws NegocioException {

		if (percentualMaximoQDC != null && percentualMaximoQDC.compareTo(BigDecimal.ZERO) != 0
				&& percentualMaximoQDC.compareTo(BigDecimal.ONE) > 0) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
					new Object[] { ContratoPontoConsumoPenalidade.PERCENTUAL_MAXIMO_QDC });
		}

		if (percentualMinimoQDC != null && percentualMinimoQDC.compareTo(BigDecimal.ZERO) != 0
				&& percentualMinimoQDC.compareTo(BigDecimal.ONE) > 0) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
					new Object[] { ContratoPontoConsumoPenalidade.PERCENTUAL_MINIMO_QDC });
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarCompromissoTakeOrPayQPNR
	 * (br.com.procenge.ggas
	 * .geral.EntidadeConteudo,
	 * java.math.BigDecimal, java.math.BigDecimal)
	 */
	@Override
	public void validarCompromissoTakeOrPayQPNR(EntidadeConteudo periodicidadePenalidade, BigDecimal percentualMargemVariacao,
			BigDecimal percentualMaximoQDC) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String idPeriodicidadeTOP = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PERIODICIDADE_VALIDACAO_COMPROMISSO_TOP_QPNR);

		if (Long.parseLong(idPeriodicidadeTOP) == periodicidadePenalidade.getChavePrimaria() && percentualMargemVariacao != null
				&& percentualMargemVariacao.compareTo(BigDecimal.ZERO) != 0) {

			BigDecimal percentualTotal = percentualMargemVariacao;
			percentualTotal = percentualTotal.add(percentualMaximoQDC);
			if (percentualTotal.compareTo(BigDecimal.ONE) != 0) {
				throw new NegocioException(ERRO_NEGOCIO_PERCENTUAL_TOP_QPNR_INVALIDO, periodicidadePenalidade.getDescricao());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarPercentualTarifaDOP
	 * (java.math.BigDecimal)
	 */
	@Override
	public void validarPercentualTarifaDOP(BigDecimal percentualTarifaTOP) throws NegocioException {

		if (percentualTarifaTOP != null && percentualTarifaTOP.compareTo(BigDecimal.ZERO) != 0
				&& percentualTarifaTOP.compareTo(BigDecimal.ONE) > 0) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
					new Object[] { Contrato.PERCENTUAL_TARIFA_DOP });
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarCamposMinimoMaximo(java.util.Map)
	 */
	@Override
	public void validarCamposMinimoMaximo(Map<String, Object> dados) throws NegocioException, FormatoInvalidoException {

		ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento = ServiceLocator.getInstancia()
						.getControladorFaixaPressaoFornecimento();

		BigDecimal vazaoMinima = null;
		BigDecimal vazaoMaxima = null;
		BigDecimal pressaoMinima = null;
		BigDecimal pressaoMaxima = null;
		BigDecimal pressaoLimite = null;
		BigDecimal pressaoFornecimento = null;
		BigDecimal fornecimentoMinimo = null;
		BigDecimal fornecimentoMaximo = null;
		BigDecimal vazao = null;

		String vazaoInstantanea = (String) dados.get(VAZAO_INSTANTANEA);
		String vazaoInstantaneaMinima = (String) dados.get(VAZAO_INSTANTANEA_MINIMA);
		String vazaoInstantaneaMaxima = (String) dados.get(VAZAO_INSTANTANEA_MAXIMA);
		String pressaoMinimaFornecimento = (String) dados.get(PRESSAO_MINIMA_FORNECIMENTO);
		String pressaoMaximaFornecimento = (String) dados.get(PRESSAO_MAXIMA_FORNECIMENTO);
		String pressaoLimiteFornecimento = (String) dados.get(PRESSAO_LIMITE_FORNECIMENTO);
		String chavePressaoFornecimento = (String) dados.get(FAIXA_PRESSAO_FORNECIMENTO);

		String fornecimentoMinimoDiario = (String) dados.get(FORNECIMENTO_MINIMO_DIARIO);
		String fornecimentoMaximoDiario = (String) dados.get(FORNECIMENTO_MAXIMO_DIARIO);

		if (!StringUtils.isEmpty(vazaoInstantanea)) {
			vazao = Util.converterCampoStringParaValorBigDecimal(VAZAO_INSTANTANEA2,
							vazaoInstantanea, Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO);
		}
		if (!StringUtils.isEmpty(vazaoInstantaneaMinima)) {
			vazaoMinima = Util.converterCampoStringParaValorBigDecimal(VAZAO_MINIMA, vazaoInstantaneaMinima,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		if (!StringUtils.isEmpty(vazaoInstantaneaMaxima)) {
			vazaoMaxima = Util.converterCampoStringParaValorBigDecimal(VAZAO_MAXIMA, vazaoInstantaneaMaxima,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		if (!StringUtils.isEmpty(pressaoMinimaFornecimento)) {
			pressaoMinima = Util.converterCampoStringParaValorBigDecimal(PRESSAO_MINIMA, pressaoMinimaFornecimento,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		if (!StringUtils.isEmpty(pressaoMaximaFornecimento)) {
			pressaoMaxima = Util.converterCampoStringParaValorBigDecimal(PRESSAO_MAXIMA, pressaoMaximaFornecimento,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		if (!StringUtils.isEmpty(fornecimentoMinimoDiario)) {
			fornecimentoMinimo = Util.converterCampoStringParaValorBigDecimal(FORNECIMENTO_MINIMO, fornecimentoMinimoDiario,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		if (!StringUtils.isEmpty(fornecimentoMaximoDiario)) {
			fornecimentoMaximo = Util.converterCampoStringParaValorBigDecimal(FORNECIMENTO_MAXIMO, fornecimentoMaximoDiario,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}
		FaixaPressaoFornecimento faixaPressaoFornecimento = null;
		if (!StringUtils.isEmpty(chavePressaoFornecimento)) {
			faixaPressaoFornecimento = (FaixaPressaoFornecimento) controladorFaixaPressaoFornecimento.obter(Util
							.converterCampoStringParaValorLong(ContratoPontoConsumo.PRESSAO_FORNECIMENTO, chavePressaoFornecimento));
			pressaoFornecimento = faixaPressaoFornecimento.getMedidaMinimo();
		}
		if (!StringUtils.isEmpty(pressaoLimiteFornecimento)) {
			pressaoLimite = Util.converterCampoStringParaValorBigDecimal(PRESSAO_LIMITE_FORNECIMENTO, pressaoLimiteFornecimento,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
		}

		// conversão implementada
		boolean isVazaoMaximaInvalida = vazaoMaxima != null && vazao != null;
		if (isVazaoMaximaInvalida) {
			String unidadeVazaoInstantanea = (String) dados.get(UNIDADE_VAZAO_INSTAN);
			String unidadeVazaoInstanMaxima = (String) dados.get(UNIDADE_VAZAO_INSTAN_MAXIMA);

			if (unidadeVazaoInstantanea.equals(unidadeVazaoInstanMaxima)) {
				if (vazaoMaxima.compareTo(vazao) < 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
									new Object[] {ContratoPontoConsumo.VAZAO_INSTANTANEA, ContratoPontoConsumo.VAZAO_INSTANTANEA_MAXIMA});
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadeVazaoInstantanea),
												Long.parseLong(unidadeVazaoInstanMaxima));

				if (unidadeConversao != null) {

					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY)
									.replace(PARAMETRO, vazaoMaxima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal vazaoInstantaneaMaximaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (vazao.compareTo(vazaoInstantaneaMaximaConvertida) > 0) {
						throw new NegocioException(
										ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
										new Object[] {ContratoPontoConsumo.VAZAO_INSTANTANEA_MAXIMA, ContratoPontoConsumo.VAZAO_INSTANTANEA});
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversão implementada
		boolean isVazaoMinimaInvalida = vazaoMinima != null && vazao != null;
		if (isVazaoMinimaInvalida) {
			String unidadeVazaoInstantanea = (String) dados.get(UNIDADE_VAZAO_INSTAN);
			String unidadeVazaoInstanMinima = (String) dados.get(UNIDADE_VAZAO_INSTAN_MINIMA);

			if (unidadeVazaoInstantanea.equals(unidadeVazaoInstanMinima)) {
				if (vazaoMinima.compareTo(vazao) > 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
									new Object[] {ContratoPontoConsumo.VAZAO_INSTANTANEA, ContratoPontoConsumo.VAZAO_INSTANTANEA_MINIMA});
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadeVazaoInstantanea),
												Long.parseLong(unidadeVazaoInstanMinima));

				if (unidadeConversao != null) {

					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, vazaoMinima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal vazaoInstantaneaMinimaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (vazaoInstantaneaMinimaConvertida.compareTo(vazao) > 0) {
						throw new NegocioException(
										ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
										new Object[] {ContratoPontoConsumo.VAZAO_INSTANTANEA, ContratoPontoConsumo.VAZAO_INSTANTANEA_MINIMA});
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversao implementada
		boolean isVazaoInvalida = vazaoMinima != null && vazaoMaxima != null;
		if (isVazaoInvalida) {
			String unidadeVazaoInstanMaxima = (String) dados.get(UNIDADE_VAZAO_INSTAN_MAXIMA);
			String unidadeVazaoInstanMinima = (String) dados.get(UNIDADE_VAZAO_INSTAN_MINIMA);

			if (unidadeVazaoInstanMaxima.equals(unidadeVazaoInstanMinima)) {
				if (vazaoMinima.compareTo(vazaoMaxima) > 0) {
					throw new NegocioException(
									ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
									new Object[] {ContratoPontoConsumo.VAZAO_INSTANTANEA_MAXIMA, ContratoPontoConsumo.VAZAO_INSTANTANEA_MINIMA});
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadeVazaoInstanMaxima),
												Long.parseLong(unidadeVazaoInstanMinima));

				if (unidadeConversao != null) {

					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, vazaoMinima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal vazaoMinimaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (vazaoMinimaConvertida.compareTo(vazaoMaxima) > 0) {
						throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
								ContratoPontoConsumo.VAZAO_INSTANTANEA_MAXIMA, ContratoPontoConsumo.VAZAO_INSTANTANEA_MINIMA });
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversao implementada
		boolean isPressaoInvalida = pressaoMinima != null && pressaoMaxima != null;
		if (isPressaoInvalida) {
			String unidadePressaoMinimaFornec = (String) dados.get(UNIDADE_PRESSAO_MINIMA_FORNEC);
			String unidadePressaoMaximaForneca = (String) dados.get(UNIDADE_PRESSAO_MAXIMA_FORNEC);

			if (unidadePressaoMaximaForneca.equals(unidadePressaoMinimaFornec)) {
				if (pressaoMinima.compareTo(pressaoMaxima) > 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
							ContratoPontoConsumo.PRESSAO_MAXIMA_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_MINIMA_FORNECIMENTO });
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadePressaoMaximaForneca),
												Long.parseLong(unidadePressaoMinimaFornec));

				if (unidadeConversao != null) {
					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, pressaoMinima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal pressaoMinimaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (pressaoMinimaConvertida.compareTo(pressaoMaxima) > 0) {
						throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
								ContratoPontoConsumo.PRESSAO_MAXIMA_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_MINIMA_FORNECIMENTO });
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversão implementada
		boolean isFornecimentoInvalido = fornecimentoMinimo != null && fornecimentoMaximo != null;
		if (isFornecimentoInvalido) {
			String unidadeFornecMaximoDiario = (String) dados.get(UNIDADE_FORNEC_MAXIMO_DIARIO);
			String unidadeFornecMinimoDiario = (String) dados.get(UNIDADE_FORNEC_MINIMO_DIARIO);

			if (unidadeFornecMaximoDiario.equals(unidadeFornecMinimoDiario)) {
				if (fornecimentoMinimo.compareTo(fornecimentoMaximo) > 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
							ContratoPontoConsumo.FORNECIMENTO_MAXIMO_DIARIO, ContratoPontoConsumo.FORNECIMENTO_MINIMO_DIARIO });
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadeFornecMaximoDiario),
												Long.parseLong(unidadeFornecMinimoDiario));

				if (unidadeConversao != null) {
					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, fornecimentoMinimo.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal fornecimentoMinimoConvertido = (BigDecimal) groovy.evaluate(formula);

					if (fornecimentoMinimoConvertido.compareTo(fornecimentoMaximo) > 0) {
						throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
								ContratoPontoConsumo.FORNECIMENTO_MAXIMO_DIARIO, ContratoPontoConsumo.FORNECIMENTO_MINIMO_DIARIO });
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversão implementada
		boolean isMinimoMenorPressao = pressaoMinima != null && pressaoFornecimento != null;
		if (isMinimoMenorPressao) {
			String unidadePressaoMinimaFornecimento = (String) dados.get(UNIDADE_PRESSAO_MINIMA_FORNEC);
			String unidadeFaixaPressaoFornecimento = String.valueOf(faixaPressaoFornecimento.getUnidadePressao().getChavePrimaria());

			if (unidadeFaixaPressaoFornecimento.equals(unidadePressaoMinimaFornecimento)) {
				if (pressaoMinima.compareTo(pressaoFornecimento) > 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
							new Object[] { ContratoPontoConsumo.PRESSAO_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_MINIMA_FORNECIMENTO });
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadeFaixaPressaoFornecimento),
												Long.parseLong(unidadePressaoMinimaFornecimento));

				if (unidadeConversao != null) {
					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, pressaoMinima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal pressaoMinimaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (pressaoMinimaConvertida.compareTo(pressaoFornecimento) > 0) {
						throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
								ContratoPontoConsumo.PRESSAO_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_MINIMA_FORNECIMENTO });
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversão implementada
		boolean isPressaoMenorMaxima = pressaoFornecimento != null && pressaoMaxima != null;
		if (isPressaoMenorMaxima) {
			String unidadePressaoMaximaForneca = (String) dados.get(UNIDADE_PRESSAO_MAXIMA_FORNEC);
			String unidadeFaixaPressaoFornecimento = String.valueOf(faixaPressaoFornecimento.getUnidadePressao().getChavePrimaria());

			if (unidadeFaixaPressaoFornecimento.equals(unidadePressaoMaximaForneca)) {
				if (pressaoFornecimento.compareTo(pressaoMaxima) > 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO,
							new Object[] { ContratoPontoConsumo.PRESSAO_MAXIMA_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_FORNECIMENTO });
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadeFaixaPressaoFornecimento),
												Long.parseLong(unidadePressaoMaximaForneca));

				if (unidadeConversao != null) {
					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, pressaoMaxima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal pressaoMaximaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (pressaoFornecimento.compareTo(pressaoMaximaConvertida) > 0) {
						throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
								ContratoPontoConsumo.PRESSAO_MAXIMA_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_FORNECIMENTO });
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}

		// conversão implementada
		boolean isPressaoMaximaMenorLimite = pressaoMaxima != null && pressaoLimite != null;
		if (isPressaoMaximaMenorLimite) {
			String unidadePressaoLimiteFornec = (String) dados.get(UNIDADE_PRESSAO_LIMITE_FORNEC);
			String unidadePressaoMaximaForneca = (String) dados.get(UNIDADE_PRESSAO_MAXIMA_FORNEC);

			if (unidadePressaoLimiteFornec.equals(unidadePressaoMaximaForneca)) {
				if (pressaoMaxima.compareTo(pressaoLimite) > 0) {
					throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
							ContratoPontoConsumo.PRESSAO_LIMITE_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_MAXIMA_FORNECIMENTO });
				}
			} else {
				UnidadeConversao unidadeConversao = ServiceLocator
								.getInstancia()
								.getControladorUnidade()
								.consultarUnidadeConversao(Long.parseLong(unidadePressaoLimiteFornec),
												Long.parseLong(unidadePressaoMaximaForneca));

				if (unidadeConversao != null) {
					String formula = unidadeConversao.getFormula();

					formula = formula.replace(RETURN, StringUtils.EMPTY).replace(PARAMETRO, pressaoMaxima.toString());

					GroovyShell groovy = new GroovyShell();

					BigDecimal pressaoMaximaConvertida = (BigDecimal) groovy.evaluate(formula);

					if (pressaoMaximaConvertida.compareTo(pressaoLimite) > 0) {
						throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_VALOR_MAXIMO_MENOR_QUE_MINIMO, new Object[] {
								ContratoPontoConsumo.PRESSAO_LIMITE_FORNECIMENTO, ContratoPontoConsumo.PRESSAO_MAXIMA_FORNECIMENTO });
					}
				} else {
					throw new NegocioException(UnidadeConversao.ERRO_FORMULA_TIPO_CONVERSAO, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarDataQdcContrato(java.util.Map,
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarDataQdcContrato(Map<String, Object> dados, Contrato contrato) throws NegocioException {

		if (contrato.getListaContratoQDC() != null && !contrato.getListaContratoQDC().isEmpty()) {

			Boolean renovacaoAutomatica = Boolean.valueOf((String) dados.get(RENOVACAO_AUTOMATICA));
			String numDias = (String) dados.get(NUM_DIAS_RENO_AUTO_CONTRATO);
			if (numDias.isEmpty()) {
				dados.put(NUM_DIAS_RENO_AUTO_CONTRATO, ZERO);
			}
			Integer numDiasRenoAutoContrato = 0;
			String numDiasRenoAutoContratoTxt = (String) dados.get(NUM_DIAS_RENO_AUTO_CONTRATO);
			if (numDiasRenoAutoContratoTxt.length() > 0) {
				numDiasRenoAutoContrato = Integer.valueOf((String) dados.get(NUM_DIAS_RENO_AUTO_CONTRATO));
			}

			for (ContratoQDC contratoQDC : contrato.getListaContratoQDC()) {
				if (contrato.getDataAssinatura() != null && contratoQDC.getData().before(contrato.getDataAssinatura())) {

					throw new NegocioException(ERRO_NEGOCIO_DATA_QDC_MENOR_DATA_ASSINATURA, true);
				}

				if (contrato.getDataVencimentoObrigacoes() != null) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(contrato.getDataVencimentoObrigacoes());
					if ((renovacaoAutomatica) && (numDiasRenoAutoContrato > 0)) {
						calendar.add(Calendar.DAY_OF_MONTH, numDiasRenoAutoContrato);
					}
					if (contratoQDC.getData().after(calendar.getTime())) {
						throw new NegocioException(ERRO_NEGOCIO_DATA_QDC_MAIOR_DATA_VENCIMENTO, true);
					}
				}

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarJurosMoraContrato(br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarJurosMoraContrato(Contrato contrato) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		Boolean indicadorJuros = contrato.getIndicadorJuros();
		Boolean indicaodrMulta = contrato.getIndicadorMulta();
		BigDecimal percentualJuros = contrato.getPercentualJurosMora();
		BigDecimal percentualMulta = contrato.getPercentualMulta();

		if (indicadorJuros != null && indicadorJuros && percentualJuros == null) {
			stringBuilder.append(PERCENTUAL_DE_JUROS_DE_MORA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (indicaodrMulta != null && indicaodrMulta && percentualMulta == null) {
			stringBuilder.append(PERCENTUAL_DE_MULTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			LOG.error(" Campos obrigatorios: "
					+ camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterEnderecoFaturamentoFormatado
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public String obterEnderecoFaturamentoFormatado(String cepParam, String numero) throws NegocioException {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();

		Cep cep = controladorEndereco.obterCep(cepParam);
		if (cep != null) {
			if (cep.getTipoLogradouro() != null) {
				enderecoFormatado.append(cep.getTipoLogradouro());
			}
			if (cep.getLogradouro() != null) {
				if(enderecoFormatado.length() > 0){
					separador = " ";
				}else{
					separador = "";
				}
				enderecoFormatado.append(separador).append(cep.getLogradouro());
			}
			if (!StringUtils.isEmpty(numero)) {
				if(enderecoFormatado.length() > 0){
					separador = ", ";
				}else{
					separador = "";
				}
				enderecoFormatado.append(separador).append(numero);
			}
			if (cep.getBairro() != null) {
				if(enderecoFormatado.length() > 0){
					separador = ", ";
				}else{
					separador = "";
				}
				enderecoFormatado.append(separador).append(cep.getBairro());
			}
			if (cep.getMunicipio() != null) {
				if( enderecoFormatado.length() > 0){
					separador = ", ";
				}else{
					separador = "";
				}
				enderecoFormatado.append(separador).append(cep.getMunicipio());
			}
			if (cep.getUf() != null) {
				if(enderecoFormatado.length() > 0){
					separador =  ", ";
				}else{
					separador = "";
				}
				enderecoFormatado.append(separador).append(cep.getUf());
			}
		}

		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoPorImovel
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorImovel(Long chave) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		this.montarQueryContratoPontoConsumo(hql);
		hql.append(" where contratoPontoConsumo.pontoConsumo.imovel.chavePrimaria = :chave ");
		hql.append(" and contratoPontoConsumo.contrato.habilitado = :HABILITADO ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE, chave);
		query.setParameter(HABILITADO_UPPER, Boolean.TRUE);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoPorImovel
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoClientePontoConsumoPorImovel(Long chave) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO);
		hql.append(" inner join fetch contratoPontoConsumo.contrato ");
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel ");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" inner join fetch pontoConsumo.ramoAtividade ");
		hql.append(" inner join fetch contratoPontoConsumo.contrato.clienteAssinatura clienteAssinatura ");

		hql.append(" where contratoPontoConsumo.contrato.habilitado = :HABILITADO ");
		if (chave != null && chave > 0) {
			hql.append(" and contratoPontoConsumo.pontoConsumo.imovel.chavePrimaria = :chave ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (chave != null && chave > 0) {
			query.setLong(CHAVE, chave);
		}
		query.setParameter(HABILITADO_UPPER, Boolean.TRUE);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoPorImovel
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarPontoConsumoPorImovelComContrato(Long chave) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct pontoConsumo from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo, ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO);
		hql.append(" inner join fetch pontoConsumo.imovel ");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" inner join fetch pontoConsumo.ramoAtividade ");
		hql.append(" where pontoConsumo = contratoPontoConsumo.pontoConsumo ");
		hql.append(" and contratoPontoConsumo.pontoConsumo.imovel.chavePrimaria = :chave ");
		hql.append(" and contratoPontoConsumo.contrato.habilitado = :HABILITADO ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE, chave);
		query.setParameter(HABILITADO_UPPER, Boolean.TRUE);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoPorCliente
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorCliente(Long chave) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		this.montarQueryContratoPontoConsumo(hql);
		hql.append(" where contratoPontoConsumo.contrato.clienteAssinatura.chavePrimaria = :chave ");
		hql.append(" and contratoPontoConsumo.contrato.habilitado = :HABILITADO ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE, chave);
		query.setParameter(HABILITADO_UPPER, Boolean.TRUE);

		return query.list();
	}

	/**
	 * Montar query contrato ponto consumo.
	 *
	 * @param hql
	 *            the hql
	 */
	private void montarQueryContratoPontoConsumo(StringBuilder hql) {

		hql.append(" select contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO);
		hql.append(" inner join fetch contratoPontoConsumo.contrato ");
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel ");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" inner join fetch pontoConsumo.ramoAtividade ");
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarClientePorPontoConsumo(java.lang.Long)
	 */
	@Override
	public Cliente consultarClientePorPontoConsumo(Long pontoConsumo, Long chaveContrato) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select contrato.clienteAssinatura from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO);
		hql.append(" inner join contratoPontoConsumo.contrato contrato");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" where pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and contrato.habilitado = true ");

		if (Objects.nonNull(chaveContrato)) {
			hql.append(" and contrato.chavePrimaria = ").append(chaveContrato);
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PONTO_CONSUMO, pontoConsumo);

		return (Cliente) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #isObrigatorioTamanhoReducaoRecuperacaoPCS
	 * (java.lang
	 * .Long[])
	 */
	@Override
	public boolean isObrigatorioTamanhoReducaoRecuperacaoPCS(Long[] arrayIdsIntervalosPCS) throws NegocioException {

		boolean obrigatoriedade = false;
		if (arrayIdsIntervalosPCS != null && arrayIdsIntervalosPCS.length > 0) {
			for (Long idIntervaloPCS : arrayIdsIntervalosPCS) {
				if (idIntervaloPCS.equals(IntervaloPCS.INTERVALO_REDUZIDO)) {
					obrigatoriedade = true;
					break;
				}
			}
		}

		return obrigatoriedade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarAditamentoContrato(java.lang.Long)
	 */
	@Override
	public void validarAditamentoContrato(Long chavePrimaria) throws NegocioException {

		if (chavePrimaria == null || chavePrimaria <= 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(contrato) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(" where contrato.situacao.permiteAditamento = false ");
		hql.append(" and contrato.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA, chavePrimaria);

		Long quantidade = (Long) query.uniqueResult();

		if (quantidade != null && quantidade > 0) {
			hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidadeSituacaoContrato().getSimpleName());
			hql.append(" where permiteAditamento = false ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			Collection<SituacaoContrato> listaSituacoes = query.list();

			StringBuilder descricaoSituacoes = new StringBuilder();
			if (listaSituacoes != null && !listaSituacoes.isEmpty()) {
				for (SituacaoContrato situacao : listaSituacoes) {
					if (descricaoSituacoes.length() > 0) {
						descricaoSituacoes.append(", ");
					}
					descricaoSituacoes.append(situacao.getDescricao());

				}
			}
			throw new NegocioException(ERRO_NEGOCIO_SITUACAO_CONTRATO_INVALIDA_ADITAMENTO, new Object[] {descricaoSituacoes.toString()});
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratosPorProposta
	 * (java.lang.Long[])
	 */
	@Override
	public Collection<Contrato> consultarContratosPorProposta(Long[] chavesProposta) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select contrato");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(WHERE);
		hql.append(" contrato.proposta.chavePrimaria in (:CHAVES_PROPOSTAS)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("CHAVES_PROPOSTAS", chavesProposta);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarHoraDia(org.joda.time.DateTime,
	 * java.lang.Integer)
	 */
	@Override
	public void validarHoraDia(DateTime data, Integer hora) throws NegocioException {

		try {
			DateTime d = new DateTime(data.getYear(), data.getMonthOfYear(), data.getDayOfMonth(), hora, 0, 0, 0);
			d.compareTo(new Date());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ControladorContrato.ERRO_NEGOCIO_HORA_DIA, Contrato.HORA_DIA);
		}
	}

	/**
	 * Ordenar lista contrato qdc.
	 *
	 * @param listaContratoQDC
	 *            the lista contrato qdc
	 * @return the list
	 */
	private List<ContratoQDC> ordenarListaContratoQDC(Collection<ContratoQDC> listaContratoQDC) {

		List<ContratoQDC> listaContratoQDCOrdenada = new ArrayList<>();
		for (ContratoQDC contratoQDC : listaContratoQDC) {
			listaContratoQDCOrdenada.add(contratoQDC);
		}
		Collections.sort(listaContratoQDCOrdenada, new Comparator<ContratoQDC>(){

			@Override
			public int compare(ContratoQDC o1, ContratoQDC o2) {

				return o1.getData().compareTo(o2.getData());
			}
		});
		return listaContratoQDCOrdenada;
	}

	/**
	 * Obter soma qdc modalidade adicionadas pela data.
	 *
	 * @param data
	 *            the data
	 * @param listaModalidadeQDC
	 *            the lista modalidade qdc
	 * @return the big decimal
	 */
	private BigDecimal obterSomaQDCModalidadeAdicionadasPelaData(Date data,
			Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC) {

		List<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDCOrdenada = new ArrayList<>();
		listaModalidadeQDCOrdenada.addAll(listaModalidadeQDC);

		Collections.sort(listaModalidadeQDCOrdenada, new Comparator<ContratoPontoConsumoModalidadeQDC>(){

			@Override
			public int compare(ContratoPontoConsumoModalidadeQDC o1, ContratoPontoConsumoModalidadeQDC o2) {

				return o1.getDataVigencia().compareTo(o2.getDataVigencia());
			}
		});

		BigDecimal volumeQDC = null;
		BigDecimal volumeQDCAnterior = null;
		for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : listaModalidadeQDCOrdenada) {
			Date dataModalidadeQDC = contratoPontoConsumoModalidadeQDC.getDataVigencia();
			if (Util.compararDatas(data, dataModalidadeQDC) < 0) {
				volumeQDC = volumeQDCAnterior;
				break;
			}
			volumeQDCAnterior = contratoPontoConsumoModalidadeQDC.getMedidaVolume();
		}

		if (volumeQDC == null) {
			if (volumeQDCAnterior == null) {
				volumeQDC = BigDecimal.ZERO;
			} else {
				volumeQDC = volumeQDCAnterior;
			}
		}

		return volumeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarAlteracaoQDCContrato
	 * (java.util.Collection,
	 * java.util.Collection, java.util.Date)
	 */
	@Override
	public String validarAlteracaoQDCContrato(Collection<ContratoPontoConsumoModalidadeQDC> listaModalidadeQDC,
					Collection<ContratoQDC> listaContratoQDC, Date dataVencimento) throws NegocioException {

		String dataSomaQDC = null;

		Collection<ContratoQDC> listaContratoQDCOrdenada = this.ordenarListaContratoQDC(listaContratoQDC);
		ContratoQDC[] arrayContratoQDC = listaContratoQDCOrdenada.toArray(new ContratoQDC[listaContratoQDCOrdenada.size()]);

		for (int i = 0; i < arrayContratoQDC.length; i++) {
			DateTime dataQDC = new DateTime(arrayContratoQDC[i].getData());

			Date proximaDataQDC = null;
			if (i < arrayContratoQDC.length - 1) {
				proximaDataQDC = arrayContratoQDC[i + 1].getData();
			} else {
				if (dataVencimento == null) {
					proximaDataQDC = dataQDC.plus(1).toDate();
				} else {
					proximaDataQDC = dataVencimento;
				}
			}

			Date data = dataQDC.toDate();
			BigDecimal volumeQDC = arrayContratoQDC[i].getMedidaVolume();
			do {

				if (volumeQDC != null) {
					// Obtendo a soma dos volumes
					// dos QDCs de cada modalidade
					// já adicionada pela data.
					BigDecimal somaValoresQDC = this.obterSomaQDCModalidadeAdicionadasPelaData(data, listaModalidadeQDC);

					if ((volumeQDC.compareTo(somaValoresQDC) < 0) && (dataSomaQDC == null)) {
						dataSomaQDC = Util.converterDataParaStringSemHora(data, Constantes.FORMATO_DATA_BR);
						i = arrayContratoQDC.length;
						break;
					}
				}

				dataQDC = dataQDC.plusDays(1);
				data = dataQDC.toDate();
			} while (Util.compararDatas(data, proximaDataQDC) < 0);
		}

		return dataSomaQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarModalidadeExistente
	 * (java.util.Collection,
	 * br.com.ggas.web.contrato
	 * .contrato.ModalidadeConsumoFaturamentoVO)
	 */
	@Override
	public void validarModalidadeExistente(Collection<ModalidadeConsumoFaturamentoVO> listaModalidades,
					ModalidadeConsumoFaturamentoVO modalidadeConsumoFaturamentoVO) throws NegocioException {

		if (listaModalidades.contains(modalidadeConsumoFaturamentoVO)) {
			throw new NegocioException(ERRO_NEGOCIO_MODALIDADE_JA_EXISTENTE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoPorCliente(java.lang.Long)
	 */
	@Override
	public Contrato obterContratoPorCliente(Long idCliente) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select contratoCliente.contrato ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoCliente().getSimpleName());
		hql.append(ALIAS_CONTRATO_CLIENTE);
		hql.append(WHERE);
		hql.append(" contratoCliente.cliente.chavePrimaria = :idCliente ");
		hql.append(" and rownum = 1 ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_CLIENTE, idCliente);

		return (Contrato) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterListaContratoPorCliente(java.lang.Long)
	 */
	@Override
	public List<Contrato> obterListaContratoPorCliente(Long idCliente) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(WHERE);
		hql.append(" contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		hql.append(" and contrato.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_CLIENTE, idCliente);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoPontoConsumoPorCliente
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumo> obterContratoPontoConsumoPorCliente(Long idCliente) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		ServiceLocator.getInstancia().getControladorParametroSistema();

		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(WHERE);
		hql.append(" contratoPonto.contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :idSituacao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_CLIENTE, idCliente);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacao = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong(ID_SITUACAO, idSituacao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoItemFaturamento
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumoItemFaturamento consultarContratoPontoConsumoItemFaturamento(Long chaveContratoPontoConsumo,
					Long chaveItemFatura) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" cpItemFaturamento ");
		hql.append(WHERE);
		hql.append(" cpItemFaturamento.contratoPontoConsumo.chavePrimaria =:CHAVE_CONTRATO_PONTO_CONSUMO ");
		hql.append(" and cpItemFaturamento.itemFatura.chavePrimaria =:ITEM_FATURA ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("CHAVE_CONTRATO_PONTO_CONSUMO", chaveContratoPontoConsumo);
		query.setParameter("ITEM_FATURA", chaveItemFatura);
		return (ContratoPontoConsumoItemFaturamento) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoPontoConsumoParaEnderecoEntrega(java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo obterContratoPontoConsumoParaEnderecoEntrega(Long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		ServiceLocator.getInstancia().getControladorParametroSistema();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.cep cep ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :idSituacao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacao = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong(ID_SITUACAO, idSituacao);

		List<ContratoPontoConsumo> listaContratoPontos = query.list();

		ContratoPontoConsumo contratoPonto = null;
		if ((listaContratoPontos != null) && (!listaContratoPontos.isEmpty())) {
			contratoPonto = listaContratoPontos.get(0);
		}
		return contratoPonto;
	}

	/**
	 * Obter contrato por chave contrato por ponto.
	 *
	 * @param idContrato
	 *            the id contrato
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Contrato obterContratoPorChaveContratoPorPonto(Long idContrato, Long idPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append(" left join fetch contrato.listaContratoPontoConsumo listaContratoPonto ");
		hql.append(" left join fetch listaContratoPonto.listaContratoPontoConsumoModalidade listaModalidade ");
		hql.append(" left join fetch listaContratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" left join fetch contrato.listaContratoQDC listaContratoQDC ");
		hql.append(WHERE);
		hql.append(" pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and contrato.chavePrimaria = :idContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		query.setLong(ID_CONTRATO, idContrato);

		List<Contrato> listaContrato = query.list();

		Contrato contrato = null;
		if ((listaContrato != null) && (!listaContrato.isEmpty())) {
			contrato = listaContrato.get(0);
		}
		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarDataReferenciaDiaCotacao(boolean, br.com.ggas.geral.EntidadeConteudo,
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void validarDataReferenciaDiaCotacao(boolean verificarDataReferenciaDiaCotacao, EntidadeConteudo objetoDataReferenciaCambial,
					EntidadeConteudo objetoDiaCotacao) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (verificarDataReferenciaDiaCotacao && objetoDiaCotacao == null) {
			stringBuilder.append(ContratoPontoConsumoItemFaturamento.DIA_COTACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (verificarDataReferenciaDiaCotacao && objetoDataReferenciaCambial == null) {
			stringBuilder.append(ContratoPontoConsumoItemFaturamento.DATA_REFERENCIA_CAMBIAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterTarifaPontoConsumoItemFaturamento(java.lang.Long, java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumoItemFaturamento obterTarifaPontoConsumoItemFaturamento(Long idPontoConsumo, Long idItemFatura)
					throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" cpItemFaturamento ");
		hql.append(" inner join fetch cpItemFaturamento.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch cpItemFaturamento.itemFatura itemFatura ");
		hql.append(" inner join fetch contratoPontoConsumo.contrato contrato ");
		hql.append(" inner join fetch contrato.situacao situacao ");
		hql.append(WHERE);
		hql.append(" situacao.chavePrimaria = :situacao");
		hql.append(" and pontoConsumo.chavePrimaria =:idPontoConsumo ");
		hql.append(" and itemFatura.chavePrimaria =:idItemFatura ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(SITUACAO, SituacaoContrato.ATIVO);
		query.setParameter(ID_PONTO_CONSUMO, idPontoConsumo);
		query.setParameter(ID_ITEM_FATURA, idItemFatura);

		return (ContratoPontoConsumoItemFaturamento) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarContratoItemFaturamentoPorTarifas
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumoItemFaturamento> listarContratoItemFaturamentoPorTarifas(Long[] chavesTarifa)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" itemFaturamento");
		hql.append(WHERE);
		hql.append(" itemFaturamento.tarifa.chavePrimaria in (:CHAVES_TARIFA)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("CHAVES_TARIFA", chavesTarifa);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoCompraPorPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public EntidadeConteudo obterContratoCompraPorPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contratoPontoConsumo.contratoCompra ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO);
		hql.append(" inner join  contratoPontoConsumo.contrato contrato");
		hql.append(" where contratoPontoConsumo.pontoConsumo = :idPontoConsumo ");
		hql.append(" and contrato.situacao in ( ");
		hql.append(SituacaoContrato.ATIVO);
		hql.append("   , ");
		hql.append(SituacaoContrato.EM_NEGOCIACAO);
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_PONTO_CONSUMO, pontoConsumo.getChavePrimaria());

		return (EntidadeConteudo) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * obterContratoPontoConsumoPorContratoPontoConsumo
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo obterContratoPontoConsumoPorContratoPontoConsumo(Long chaveContrato, Long chavePontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" cpc ");
		hql.append(" inner join fetch cpc.pontoConsumo ");
		hql.append(WHERE);
		hql.append(" cpc.contrato.chavePrimaria = :chaveContrato ");
		hql.append(" and cpc.pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_CONTRATO, chaveContrato);
		query.setParameter(CHAVE_PONTO_CONSUMO, chavePontoConsumo);

		return (ContratoPontoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterTipoMedicaoContratoPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public TipoMedicao obterTipoMedicaoContratoPontoConsumo(PontoConsumo pontoConsumo, Contrato contrato) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select contratoPonto.tipoMedicao ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join contratoPonto.tipoMedicao tipoMedicao ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

		if (contrato == null) {
			hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :chaveSituacao");
		} else {
			hql.append(" and contratoPonto.contrato.chavePrimaria = :chaveContrato");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PONTO_CONSUMO, pontoConsumo.getChavePrimaria());

		if (contrato == null) {
			query.setLong("chaveSituacao", SituacaoContrato.ATIVO);
		} else {
			query.setLong(CHAVE_CONTRATO, contrato.getChavePrimaria());
		}

		return (TipoMedicao) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #verificarContratoPeriodoTeste
	 * (br.com.ggas.contrato.contrato.Contrato,
	 * br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Date)
	 */
	@Override
	public boolean verificarContratoPeriodoTeste(Contrato contrato, PontoConsumo pontoConsumo, Date dataInicial, Date dataFinal)
					throws NegocioException {

		Long quantidade = null;
		boolean retorno = false;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(contratoPonto) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join contratoPonto.contrato contrato ");
		hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" contrato.chavePrimaria = :idContrato ");
		hql.append(" and pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and ( ");

		// O contrato está em período de teste.
		hql.append(" ( contratoPonto.dataFimTeste > :dataInicial ) ");

		// Ainda existe consumo para teste ou o
		// mesmo não foi informado.
		hql.append(" or ((contratoPonto.medidaConsumoTeste > 0 and (contratoPonto.medidaConsumoTeste - ");
		hql.append("contratoPonto.medidaConsumoTesteUsado) > 0)) ");

		hql.append(" ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_CONTRATO, contrato.getChavePrimaria());
		query.setLong(ID_PONTO_CONSUMO, pontoConsumo.getChavePrimaria());
		query.setDate("dataInicial", dataInicial);

		quantidade = (Long) query.uniqueResult();
		if (quantidade != null && quantidade > 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * consultarSolicitacaoConsumoPontoConsumoPorPeriodo
	 * (java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public Collection<SolicitacaoConsumoPontoConsumo> consultarSolicitacaoConsumoPontoConsumoPorPeriodo(Long chavePontoConsumo,
					Date dataInicio, Date dataFim) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSolicitacaoConsumoPontoConsumo().getSimpleName());
		hql.append(" solicitacaoConsumo");
		hql.append(WHERE);
		hql.append(" solicitacaoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(AND);
		hql.append(" solicitacaoConsumo.dataSolicitacao >= :dataInicio ");
		hql.append(AND);
		hql.append(" solicitacaoConsumo.dataSolicitacao <= :dataFim ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_PONTO_CONSUMO, chavePontoConsumo);
		query.setParameter("dataInicio", dataInicio);
		query.setParameter("dataFim", dataFim);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * obterContratoPontoConsumoItemFaturamentoPrioritario
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Cacheable("contratoPontoConsumoItemFaturamentoPrioritario")
	@Override
	public ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamentoPrioritario(Contrato contrato)
					throws NegocioException {

		return obterContratoPontoConsumoItemFaturamentoPrioritario(contrato, null);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * obterContratoPontoConsumoItemFaturamentoPrioritario
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Cacheable("contratoPontoConsumoItemFaturamento")
	@Override
	public ContratoPontoConsumoItemFaturamento obterContratoPontoConsumoItemFaturamentoPrioritario(Contrato contrato,
					PontoConsumo pontoConsumo) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		ContratoPontoConsumoItemFaturamento cpcItemFaturamentoGas = null;
		ContratoPontoConsumoItemFaturamento cpcItemFaturamentoMargem = null;
		ContratoPontoConsumoItemFaturamento cpcItemFaturamentoTransporte = null;

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = new ArrayList<>();
		for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
			if (pontoConsumo != null) {
				if (contratoPontoConsumo.getPontoConsumo().getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
					listaContratoPontoConsumo.add(contratoPontoConsumo);
				}
			} else {
				listaContratoPontoConsumo.add(contratoPontoConsumo);
			}
		}

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			cpcItemFaturamentoGas = consultarContratoPontoConsumoItemFaturamento(contratoPontoConsumo.getChavePrimaria(), itemFaturaGas);

			if (cpcItemFaturamentoGas == null) {

				Long itemFaturaMargeDistribuicao = Long.valueOf(controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO));

				cpcItemFaturamentoMargem = consultarContratoPontoConsumoItemFaturamento(contratoPontoConsumo.getChavePrimaria(),
								itemFaturaMargeDistribuicao);

				if (cpcItemFaturamentoMargem == null) {
					Long itemFaturaTransporte = Long.valueOf(controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE));

					cpcItemFaturamentoTransporte = consultarContratoPontoConsumoItemFaturamento(contratoPontoConsumo.getChavePrimaria(),
									itemFaturaTransporte);
				}
			} else {
				break;
			}
		}

		ContratoPontoConsumoItemFaturamento retorno = null;
		if (cpcItemFaturamentoGas != null) {
			retorno = cpcItemFaturamentoGas;
		} else if (cpcItemFaturamentoMargem != null) {
			retorno = cpcItemFaturamentoMargem;
		} else {
			retorno = cpcItemFaturamentoTransporte;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #salvarContratoParcial(br
	 * .com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public Long salvarContratoParcial(Contrato contrato) throws NegocioException, ConcorrenciaException {

		long chavePrimaria = contrato.getChavePrimaria();
		SituacaoContrato situacao = this.obterSituacaoContrato(SituacaoContrato.EM_CRIACAO);
		contrato.setSituacao(situacao);
		associarTipoMedicao(contrato);

		if (chavePrimaria == 0) {
			chavePrimaria = this.inserir(contrato);
		} else {
			this.atualizar(contrato);
		}

		return chavePrimaria;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #inserirContrato(br.com.ggas
	 * .contrato.contrato.Contrato)
	 */
	@Override
	public Long inserirContrato(Contrato contrato) throws NegocioException, ConcorrenciaException {

		Contrato contratoValidado = validarFluxoAprovacao(contrato, null);
		long chavePrimaria = contrato.getChavePrimaria();
		if (chavePrimaria == 0) {
			chavePrimaria = this.inserir(contratoValidado);
		} else {
			this.associarTipoMedicao(contratoValidado);
			this.atualizar(contratoValidado);
		}

		return chavePrimaria;
	}

	/**
	 * Valida fluxo Aprovação
	 *
	 * @param contrato
	 * @throws NegocioException
	 */
	private Contrato validarFluxoAprovacao(Contrato contrato, SituacaoContrato situacaoAnterior) throws NegocioException {
		Usuario usuario = contrato.getDadosAuditoria().getUsuario();

		Long situacaoAnteriorId = -1L;
		if (situacaoAnterior != null){
			situacaoAnteriorId = situacaoAnterior.getChavePrimaria();
		}

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorAcesso controladorAcesso =
				(ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		ConstanteSistema constanteOperacaoAprovarContrato =
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_OPERACAO_APROVACAO_CONTRATO);

		Operacao operacao = controladorAcesso.obterOperacaoSistema(constanteOperacaoAprovarContrato.getValorLong());

		Permissao permissao = controladorAcesso.buscarPermissao(usuario, operacao);
		Long aguardandoAprovacaoId = Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_AGUARDANDO_APROVACAO));

		String contratoAtivo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);
		if (contratoAtivo != null && contrato.isExigeAprovacao()) {
			if (contratoAtivo.equals(String.valueOf(contrato.getSituacao().getChavePrimaria()))
					&& (permissao == null || !situacaoAnteriorId.equals(aguardandoAprovacaoId))) {
				SituacaoContrato situacao = obterSituacaoContrato(aguardandoAprovacaoId);
				contrato.setSituacao(situacao);
			}
		}
		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * isAlteracaoContratoAgrupadoPorValorParaPorVolume
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public boolean isAlteracaoContratoAgrupadoPorValorParaPorVolume(Contrato contrato) throws NegocioException {

		boolean retorno = false;

		if (contrato.getChavePrimaria() > 0) {

			Contrato contratoBd = (Contrato) this.obter(contrato.getChavePrimaria(), "tipoAgrupamento");

			if (contratoBd != null && contratoBd.getTipoAgrupamento() != null) {

				ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
				long codigoPorValor = Long.parseLong(controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VALOR));
				long codigoPorVolume = Long.parseLong(controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME));

				if (contratoBd.getTipoAgrupamento().getChavePrimaria() == codigoPorValor && contrato.getTipoAgrupamento() != null
								&& contrato.getTipoAgrupamento().getChavePrimaria() == codigoPorVolume) {

					retorno = true;
				}

			}

		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorContrato#atualizarContratoPontoConsumo(br.com.ggas.contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public void atualizarContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException, ConcorrenciaException {

		this.atualizar(contratoPontoConsumo, ContratoPontoConsumoImpl.class);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #verificarVencimentoContrato()
	 */
	@Override
	public void verificarVencimentoContrato(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria, Processo processo)
					throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, GGASException {

		logProcessamento.append("Verificando contratos vencidos que possuem renovação automatica...\r\n");
		verificandoContratosVencidosRenovacaoAutomatica(dadosAuditoria, processo);

		logProcessamento.append("Verificando contratos vencidos que não possuem renovação automatica...\r\n");
		verificandoContratosVencidosNaoRenovacaoAutomatica(processo);

		logProcessamento.append("Verificando contratos próximo ao prazo de vencimento...\r\n");
		verificandoContratosProximoVencimento(processo);

	}

	/**
	 * Verificando contratos proximo vencimento.
	 *
	 * @param processo
	 *            the processo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void verificandoContratosProximoVencimento(Processo processo) throws GGASException {

		// obtendo a data de hoje
		DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_BR);
		Date dataHoje = new Date();
		dateFormat.format(dataHoje);

		// contratos não vencidos e sem renovação
		// automática
		List<Contrato> listaContratosPertoDeVencerNaoRenovacaoAutomatica = this.obterContratosVencidosNaoVencidos(Boolean.FALSE,
						Boolean.FALSE);
		StringBuilder conteudoEmailProxVend = new StringBuilder();
		conteudoEmailProxVend.append("Data do processamento: ");
		conteudoEmailProxVend.append(DataUtil.converterDataParaString(dataHoje,  true));
		conteudoEmailProxVend.append("\r\n");
		conteudoEmailProxVend.append("Nº Contrato \t Nome \t\t\t\t\t\t\t Data Venc. \t Qtd Dias Prox. Venc.\r\n");
		conteudoEmailProxVend.append("----------------------------------------------------------------------------------------------\r\n");
		for (Contrato contrato : listaContratosPertoDeVencerNaoRenovacaoAutomatica) {
			DateTime dataAntecedenciaRenovacao = new DateTime(contrato.getDataVencimentoObrigacoes());
			// obtendo a data do tempo de
			// Antecedência para Negociação da
			// Renovação
			Integer diasAntecedenciaRenovacao =
					Util.coalescenciaNula(contrato.getDiasAntecedenciaRenovacao(),
							NumeroUtil.INTEGER_ZERO);

			DateTime dataAntecedenciaRenovacaoComSoma =
					dataAntecedenciaRenovacao.minusDays(diasAntecedenciaRenovacao);

			int diferenca = dataHoje.compareTo(dataAntecedenciaRenovacaoComSoma.toDate());
			// se a data de hoje for maior que a
			// data de tempo de Antecedência para
			// Negociação da Renovação
			if (diferenca >= 0) {
				// Montar Corpo do email
				Days dias = Days.daysBetween(dataAntecedenciaRenovacaoComSoma, new DateTime(dataHoje));
				conteudoEmailProxVend.append(contrato.getNumeroFormatado());
				conteudoEmailProxVend.append("\t");
				conteudoEmailProxVend.append(contrato.getClienteAssinatura().getNome());

				for (int tamanhoNome =
						contrato.getClienteAssinatura().getNome().length(); tamanhoNome <= TAMANHO_MAXIMO_NOME; tamanhoNome++) {
					conteudoEmailProxVend.append(" ");
				}

				conteudoEmailProxVend.append("\t");
				conteudoEmailProxVend.append(Util.converterDataParaStringSemHora(contrato.getDataVencimentoObrigacoes(), Constantes.FORMATO_DATA_BR));
				conteudoEmailProxVend.append("\t\t");
				conteudoEmailProxVend.append(dias.getDays());
				conteudoEmailProxVend.append("\r\n");
			}

		}
		// enviar email
		if (processo.getEmailResponsavel() != null) {
			enviarEmail(processo, CONTRATOS_PROXIMO_VENCIMENTO, conteudoEmailProxVend);
		}

	}

	/**
	 * Enviar email.
	 *
	 * @param processo
	 *            the processo
	 * @param subject
	 *            the subject
	 * @param conteudo
	 *            the conteudo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void enviarEmail(Processo processo, String subject, StringBuilder conteudo) throws GGASException {

		JavaMailUtil javaMailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		javaMailUtil.enviar(getAtividadeSistema(processo).getDescricaoEmailRemetente(), processo.getEmailResponsavel(), subject,
						conteudo.toString());
	}

	/**
	 * Gets the atividade sistema.
	 *
	 * @param processo
	 *            the processo
	 * @return the atividade sistema
	 */
	private AtividadeSistema getAtividadeSistema(Processo processo) {

		ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia().getBeanPorID(
						ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
		return controladorProcesso.obterAtividadeSistemaPorChaveOperacao(processo.getOperacao().getChavePrimaria());
	}

	/**
	 * Verificando contratos vencidos nao renovacao automatica.
	 *
	 * @param processo
	 *            the processo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void verificandoContratosVencidosNaoRenovacaoAutomatica(Processo processo) throws GGASException {

		// obtendo a data de hoje
		DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_BR);
		Date dataHoje = new Date();
		dateFormat.format(dataHoje);

		// contratos vencidos e sem renovação
		// automática
		List<Contrato> listaContratosVencidosNaoRenovacaoAutomatica = this.obterContratosVencidosNaoVencidos(Boolean.FALSE, Boolean.TRUE);
		StringBuilder conteudoEmailVenNaoRenv = new StringBuilder();
		conteudoEmailVenNaoRenv.append("Data do processamento: ");
		conteudoEmailVenNaoRenv.append(Util.converterDataHoraParaString(dataHoje));
		conteudoEmailVenNaoRenv.append("\r\n");
		conteudoEmailVenNaoRenv.append("Nº Contrato \t Nome \t\t\t\t\t\t\t Data Venc. \t Qtd Dias Vencidos \r\n");
		conteudoEmailVenNaoRenv
						.append("----------------------------------------------------------------------------------------------\r\n");
		for (Contrato contrato : listaContratosVencidosNaoRenovacaoAutomatica) {
			int diferenca = dataHoje.compareTo(contrato.getDataVencimentoObrigacoes());
			// se a data de hoje for maior que a
			// data de tempo de Antecedência para
			// Negociação da Renovação
			if (diferenca > 0) {
				// Montar Corpo do email
				Days dias = Days.daysBetween(new DateTime(contrato.getDataVencimentoObrigacoes()), new DateTime(dataHoje));
				conteudoEmailVenNaoRenv.append(contrato.getNumeroFormatado());
				conteudoEmailVenNaoRenv.append("\t");
				conteudoEmailVenNaoRenv.append(contrato.getClienteAssinatura().getNome());

				for (int tamanhoNome =
						contrato.getClienteAssinatura().getNome().length(); tamanhoNome <= TAMANHO_MAXIMO_NOME; tamanhoNome++) {
					conteudoEmailVenNaoRenv.append(" ");
				}

				conteudoEmailVenNaoRenv.append("\t");
				conteudoEmailVenNaoRenv.append(Util.converterDataParaStringSemHora(contrato.getDataVencimentoObrigacoes(), Constantes.FORMATO_DATA_BR));
				conteudoEmailVenNaoRenv.append("\t\t");
				conteudoEmailVenNaoRenv.append(dias.getDays());
				conteudoEmailVenNaoRenv.append("\r\n");
			}
		}

		// enviar email
		if (processo.getEmailResponsavel() != null) {
			enviarEmail(processo, CONTRATOS_VENCIDOS_NAO_RENOVADOS, conteudoEmailVenNaoRenv);
		}
	}

	/**
	 * Verificando contratos vencidos renovacao automatica.
	 *
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param processo
	 *            the processo
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InstantiationException
	 *             the instantiation exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NoSuchMethodException
	 *             the no such method exception
	 */
	private void verificandoContratosVencidosRenovacaoAutomatica(DadosAuditoria dadosAuditoria, Processo processo) throws GGASException,
					IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {

		// obtendo a data de hoje
		DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_BR);
		Date dataHoje = new Date();
		dateFormat.format(dataHoje);

		// contratos vencidos e com renovação
		// automática
		List<Contrato> listaContratosVencidosRenovacaoAutomatica = this.obterContratosVencidosNaoVencidos(Boolean.TRUE, Boolean.TRUE);
		StringBuilder conteudoEmailRenv = new StringBuilder();
		conteudoEmailRenv.append("Data do processamento: ");
		conteudoEmailRenv.append(DataUtil.converterDataParaString(dataHoje, true));
		conteudoEmailRenv.append("\r\n");
		conteudoEmailRenv.append("Nº Contrato \t Nome \t\t\t\t\t\t\t Data Venc. Anterior \t Nova Data Venc. \r\n");
		conteudoEmailRenv.append("----------------------------------------------------------------------------------------------\r\n");
		for (Contrato contrato : listaContratosVencidosRenovacaoAutomatica) {
			Contrato contratoAditado = popularAditarContrato(contrato, dadosAuditoria);
			if (contrato.getChavePrimariaPai() == null) {
				contratoAditado.setChavePrimariaPai(contrato.getChavePrimaria());
			}

			// Somando data de vencimento com a
			// quantidade de dias para a renovação
			// automática
			DateTime dateTime = new DateTime(contrato.getDataVencimentoObrigacoes());
			DateTime dataComSoma = dateTime.plusDays(contrato.getNumeroDiasRenovacaoAutomatica());
			contratoAditado.setDataVencimentoObrigacoes(dataComSoma.toDate());

			contrato.setDadosAuditoria(dadosAuditoria);

			this.aditarContrato(contratoAditado, contrato);

			// Montar Corpo do email
			conteudoEmailRenv.append(contrato.getNumeroFormatado());
			conteudoEmailRenv.append("\t");
			conteudoEmailRenv.append(contrato.getClienteAssinatura().getNome());

			for (int tamanhoNome = contrato.getClienteAssinatura().getNome().length(); tamanhoNome <= TAMANHO_MAXIMO_NOME; tamanhoNome++) {
				conteudoEmailRenv.append(" ");
			}

			conteudoEmailRenv.append("\t");
			conteudoEmailRenv
					.append(Util.converterDataParaStringSemHora(contrato.getDataVencimentoObrigacoes(), Constantes.FORMATO_DATA_BR));
			conteudoEmailRenv.append("\t\t");
			conteudoEmailRenv
					.append(Util.converterDataParaStringSemHora(contratoAditado.getDataVencimentoObrigacoes(), Constantes.FORMATO_DATA_BR));
			conteudoEmailRenv.append("\r\n");
		}
		// enviar email
		if (processo.getEmailResponsavel() != null) {
			enviarEmail(processo, CONTRATOS_RENOVADOS_AUTOMATICAMENTE, conteudoEmailRenv);
		}
	}

	/**
	 * Obter contratos vencidos nao vencidos.
	 *
	 * @param isRenovacaoAutomatica
	 *            the is renovacao automatica
	 * @param isVencido
	 *            the is vencido
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Contrato> obterContratosVencidosNaoVencidos(boolean isRenovacaoAutomatica, boolean isVencido) {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		Criteria criteria = getCriteria();
		criteria.createAlias(LISTA_CONTRATO_PONTO_CONSUMO, LISTA_CONTRATO_PONTO_CONSUMO, Criteria.LEFT_JOIN);
		criteria.createAlias(ATRIBUTO_LISTA_CONTRATO_CLIENTE, ATRIBUTO_LISTA_CONTRATO_CLIENTE, Criteria.LEFT_JOIN);
		criteria.createAlias(ATRIBUTO_LISTA_CONTRATO_QDC, ATRIBUTO_LISTA_CONTRATO_QDC, Criteria.LEFT_JOIN);

		DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_BR);
		Date dataHoje = new Date();
		dateFormat.format(dataHoje);

		if (isVencido) {
			criteria.add(Restrictions.lt("dataVencimentoObrigacoes", dataHoje));
		} else {
			criteria.add(Restrictions.ge("dataVencimentoObrigacoes", dataHoje));
		}

		criteria.add(Restrictions.eq(RENOVACAO_AUTOMATICA, isRenovacaoAutomatica));

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoContratoAtivo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		criteria.add(Restrictions.eq("situacao.chavePrimaria", Long.parseLong(codigoSituacaoContratoAtivo)));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#popularEncerrarContrato(br.com.ggas.contrato.contrato.Contrato,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Contrato popularEncerrarContrato(Contrato contrato, DadosAuditoria dadosAuditoria) throws IllegalAccessException,
					InvocationTargetException, InstantiationException, NoSuchMethodException {

		Contrato contratoComListas = this.obterContratoListasCarregadas(contrato.getChavePrimaria(), null);
		return popularAditarContrato(contratoComListas, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#popularAditarContrato(br.com.ggas.contrato.contrato.Contrato,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Contrato popularAditarContrato(Contrato contrato, DadosAuditoria dadosAuditoria)
			throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

		Contrato contratoAditado = (Contrato) ServiceLocator.getInstancia().getBeanPorID(Contrato.BEAN_ID_CONTRATO);
		ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
		Util.copyProperties(contratoAditado, contrato);
		contratoAditado.setChavePrimaria(0);
		contratoAditado.setDadosAuditoria(dadosAuditoria);

		// ContratoCliente
		Collection<ContratoCliente> listaContratoCliente = new HashSet<>();
		for (ContratoCliente contratoCliente : contrato.getListaContratoCliente()) {
			ContratoCliente contratoClienteAditado = (ContratoCliente) ServiceLocator.getInstancia().getBeanPorID(
							ContratoCliente.BEAN_ID_CONTRATO_CLIENTE);

			ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
			Util.copyProperties(contratoClienteAditado, contratoCliente);
			contratoClienteAditado.setChavePrimaria(0);
			contratoClienteAditado.setContrato(contratoAditado);
			contratoClienteAditado.setDadosAuditoria(dadosAuditoria);

			listaContratoCliente.add(contratoClienteAditado);
		}
		contratoAditado.setListaContratoCliente(listaContratoCliente);

		// ContratoPenalidade
		Collection<ContratoPenalidade> listaContratoPenalidade = new HashSet<>();
		for (ContratoPenalidade contratoPenalidadeAditado : contrato.getListaContratoPenalidade()) {
			contratoPenalidadeAditado = (ContratoPenalidade) ServiceLocator.getInstancia().getBeanPorID(
							ContratoPenalidade.BEAN_ID_CONTRATO_PENALIDADE);
			ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
			contratoPenalidadeAditado.setChavePrimaria(0);
			contratoPenalidadeAditado.setContrato(contratoAditado);
			contratoPenalidadeAditado.setDadosAuditoria(dadosAuditoria);
			listaContratoPenalidade.add(contratoPenalidadeAditado);

		}
		contratoAditado.setListaContratoPenalidade(listaContratoPenalidade);

		// ContratoPontoConsumo
		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = new HashSet<>();
		for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
			ContratoPontoConsumo contratoPontoConsumoAditado = (ContratoPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
							ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);

			ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
			Util.copyProperties(contratoPontoConsumoAditado, contratoPontoConsumo);
			contratoPontoConsumoAditado.setChavePrimaria(0);
			contratoPontoConsumoAditado.setContrato(contratoAditado);
			contratoPontoConsumoAditado.setDadosAuditoria(dadosAuditoria);

			listaContratoPontoConsumo.add(contratoPontoConsumoAditado);

			// ContratoPontoConsumoItemFaturamento
			Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento =
							new HashSet<>();
			for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : contratoPontoConsumo
							.getListaContratoPontoConsumoItemFaturamento()) {
				ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamentoAditado = (ContratoPontoConsumoItemFaturamento) ServiceLocator

				.getInstancia().getBeanPorID(ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);

				ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
				Util.copyProperties(contratoPontoConsumoItemFaturamentoAditado, contratoPontoConsumoItemFaturamento);
				contratoPontoConsumoItemFaturamentoAditado.setContratoPontoConsumo(contratoPontoConsumoAditado);
				contratoPontoConsumoItemFaturamentoAditado.setChavePrimaria(0);
				contratoPontoConsumoItemFaturamentoAditado.setDadosAuditoria(dadosAuditoria);

				listaContratoPontoConsumoItemFaturamento.add(contratoPontoConsumoItemFaturamentoAditado);
			}
			contratoPontoConsumoAditado.setListaContratoPontoConsumoItemFaturamento(listaContratoPontoConsumoItemFaturamento);

			// ContratoPontoConsumoModalidade
			Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new HashSet<>();
			for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumo
							.getListaContratoPontoConsumoModalidade()) {
				ContratoPontoConsumoModalidade contratoPontoConsumoModalidadeAditada = (ContratoPontoConsumoModalidade) ServiceLocator
								.getInstancia().getBeanPorID(ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);

				Util.copyProperties(contratoPontoConsumoModalidadeAditada, contratoPontoConsumoModalidade);
				contratoPontoConsumoModalidadeAditada.setChavePrimaria(0);
				contratoPontoConsumoModalidadeAditada.setContratoPontoConsumo(contratoPontoConsumoAditado);
				contratoPontoConsumoModalidadeAditada.setDadosAuditoria(dadosAuditoria);

				listaContratoPontoConsumoModalidade.add(contratoPontoConsumoModalidadeAditada);

				// ContratoPontoConsumoModalidadeQDC
				Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDCAditado =
								new HashSet<>();
				for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : contratoPontoConsumoModalidade
								.getListaContratoPontoConsumoModalidadeQDC()) {
					ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDCAditado = (ContratoPontoConsumoModalidadeQDC) ServiceLocator
									.getInstancia().getBeanPorID(
													ContratoPontoConsumoModalidadeQDC.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE_QDC);

					ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
					Util.copyProperties(contratoPontoConsumoModalidadeQDCAditado, contratoPontoConsumoModalidadeQDC);
					contratoPontoConsumoModalidadeQDCAditado.setChavePrimaria(0);
					contratoPontoConsumoModalidadeQDCAditado.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidadeAditada);
					contratoPontoConsumoModalidadeQDCAditado.setDadosAuditoria(dadosAuditoria);

					listaContratoPontoConsumoModalidadeQDCAditado.add(contratoPontoConsumoModalidadeQDCAditado);
				}
				contratoPontoConsumoModalidadeAditada
								.setListaContratoPontoConsumoModalidadeQDC(listaContratoPontoConsumoModalidadeQDCAditado);

				// ContratoPontoConsumoPenalidade
				Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidadeAditado = new HashSet<>();
				for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : contratoPontoConsumoModalidade
								.getListaContratoPontoConsumoPenalidade()) {
					ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeAditado = (ContratoPontoConsumoPenalidade) ServiceLocator
									.getInstancia().getBeanPorID(ContratoPontoConsumoPenalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_PENALIDADE);

					ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
					Util.copyProperties(contratoPontoConsumoPenalidadeAditado, contratoPontoConsumoPenalidade);
					contratoPontoConsumoPenalidadeAditado.setChavePrimaria(0);
					contratoPontoConsumoPenalidadeAditado.setDadosAuditoria(dadosAuditoria);
					contratoPontoConsumoPenalidadeAditado.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidadeAditada);

					listaContratoPontoConsumoPenalidadeAditado.add(contratoPontoConsumoPenalidadeAditado);
				}
				contratoPontoConsumoModalidadeAditada.setListaContratoPontoConsumoPenalidade(listaContratoPontoConsumoPenalidadeAditado);

			}
			contratoPontoConsumoAditado.setListaContratoPontoConsumoModalidade(listaContratoPontoConsumoModalidade);

			// ContratoPontoConsumoPCSAmostragem
			Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoConsumoPCSAmostragemAditada =
							new HashSet<>();
			for (ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragem : contratoPontoConsumo
							.getListaContratoPontoConsumoPCSAmostragem()) {
				ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragemAditado = (ContratoPontoConsumoPCSAmostragem) ServiceLocator
								.getInstancia().getBeanPorID(
												ContratoPontoConsumoPCSAmostragem.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_AMOSTRAGEM);

				ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
				Util.copyProperties(contratoPontoConsumoPCSAmostragemAditado, contratoPontoConsumoPCSAmostragem);
				contratoPontoConsumoPCSAmostragemAditado.setChavePrimaria(0);
				contratoPontoConsumoPCSAmostragemAditado.setContratoPontoConsumo(contratoPontoConsumoAditado);

				listaContratoPontoConsumoPCSAmostragemAditada.add(contratoPontoConsumoPCSAmostragemAditado);
			}
			contratoPontoConsumoAditado.setListaContratoPontoConsumoPCSAmostragem(listaContratoPontoConsumoPCSAmostragemAditada);

			// ContratoPontoConsumoPCSIntervalo
			Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoConsumoPCSIntervaloAditada =
							new HashSet<>();
			for (ContratoPontoConsumoPCSIntervalo contratoPontoConsumoPCSIntervalo : contratoPontoConsumo
							.getListaContratoPontoConsumoPCSIntervalo()) {
				ContratoPontoConsumoPCSIntervalo contratoPontoConsumoPCSIntervaloAditado =
								(ContratoPontoConsumoPCSIntervalo) ServiceLocator
								.getInstancia().getBeanPorID(ContratoPontoConsumoPCSIntervalo.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_INTERVALO);

				ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
				Util.copyProperties(contratoPontoConsumoPCSIntervaloAditado, contratoPontoConsumoPCSIntervalo);
				contratoPontoConsumoPCSIntervaloAditado.setChavePrimaria(0);
				contratoPontoConsumoPCSIntervaloAditado.setContratoPontoConsumo(contratoPontoConsumoAditado);

				listaContratoPontoConsumoPCSIntervaloAditada.add(contratoPontoConsumoPCSIntervaloAditado);
			}
			contratoPontoConsumoAditado.setListaContratoPontoConsumoPCSIntervalo(listaContratoPontoConsumoPCSIntervaloAditada);

		}
		contratoAditado.setListaContratoPontoConsumo(listaContratoPontoConsumo);

		// ContratoQDC
		Collection<ContratoQDC> listaContratoQDC = new HashSet<>();
		for (ContratoQDC contratoQDC : contrato.getListaContratoQDC()) {
			ContratoQDC contratoQDCAditado = (ContratoQDC) ServiceLocator.getInstancia().getBeanPorID(ContratoQDC.BEAN_ID_CONTRATO_QDC);

			ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
			Util.copyProperties(contratoQDCAditado, contratoQDC);
			contratoQDCAditado.setChavePrimaria(0);
			contratoQDCAditado.setContrato(contratoAditado);

			listaContratoQDC.add(contratoQDCAditado);
		}
		contratoAditado.setListaContratoQDC(listaContratoQDC);

		// Faturas
		contratoAditado.setListaFatura(null);

		return contratoAditado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterHistoricoAditivos(br
	 * .com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public List<Contrato> obterHistoricoAditivos(Map<String, Object> filtro) throws NegocioException {

		List<Contrato> historico = new ArrayList<>();
		Contrato contrato = (Contrato) this.obter((Long) filtro.get(ID_CONTRATO));

		Criteria criteria = this.createCriteria(Contrato.class);
		criteria.setProjection(Projections.max(NUMERO_ADITIVO));
		criteria.add(Restrictions.isNotNull(DATA_ADITIVO));
		criteria.add(Restrictions.eq(NUMERO, contrato.getNumero()));
		criteria.add(Restrictions.eq(ANO_CONTRATO, contrato.getAnoContrato()));

		Integer numeroUltimoAditivo = (Integer) criteria.uniqueResult();

		if (numeroUltimoAditivo != null && numeroUltimoAditivo > 0) {

			List<Long> ids = new ArrayList<>();

			for (int i = 1; i <= numeroUltimoAditivo; i++) {
				criteria = this.createCriteria(Contrato.class);
				criteria.add(Restrictions.isNotNull(DATA_ADITIVO));
				criteria.add(Restrictions.eq(NUMERO, contrato.getNumero()));
				criteria.add(Restrictions.eq(ANO_CONTRATO, contrato.getAnoContrato()));
				criteria.add(Restrictions.eq(NUMERO_ADITIVO, i));
				criteria.setProjection(Projections.min(CHAVE_PRIMARIA));
				ids.add((Long) criteria.uniqueResult());
			}

			criteria = this.createCriteria(Contrato.class);
			criteria.add(Restrictions.in(CHAVE_PRIMARIA, ids));
			criteria.addOrder(Order.asc(NUMERO_ADITIVO));

			historico.addAll(criteria.list());

		}

		return historico;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterPontosConsumoContrato(java.util.Map)
	 */
	@Override
	public List<PontoConsumo> obterPontosConsumoContrato(Map<String, Object> filtro) throws NegocioException {

		List<PontoConsumo> listaPontoConsumo = new ArrayList<>();
		List<ContratoPontoConsumo> listaContratoPontoConsumo = null;

		Long idContrato = (Long) filtro.get(ID_CONTRATO);

		Criteria criteria = this.createCriteria(ContratoPontoConsumo.class);
		criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);
		criteria.createAlias("pontoConsumo.imovel", IMOVEL);
		criteria.createAlias("pontoConsumo.segmento", "segmento");
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		criteria.addOrder(Order.asc("imovel.nome"));

		// Paginação em banco dados
		if (filtro.containsKey(COLECAO_PAGINADA)) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
		}

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		listaContratoPontoConsumo = criteria.list();

		if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {

			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
				listaPontoConsumo.add(contratoPontoConsumo.getPontoConsumo());
			}

		}

		return listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterQuantidadesVolumesVO(java.util.Map)
	 */
	@Override
	public QuantidadeVolumesVO obterQuantidadesVolumesVO(Map<String, Object> filtro) throws NegocioException {

		this.validarFiltroQuantidadesVolume(filtro);

		Integer tipoConsulta = (Integer) filtro.get("tipoConsulta");

		QuantidadeVolumesVO vo = new QuantidadeVolumesVO();
		vo.setTipo(tipoConsulta);

		if (tipoConsulta.equals(QuantidadeVolumesVO.CHAVE_APURACAO)) {

			vo.setDados(this.obterListaApuracao(filtro));

		} else if (tipoConsulta.equals(QuantidadeVolumesVO.CHAVE_PENALIDADE)) {

			vo.setDados(this.obterListaPenalidade(filtro));

		} else if (tipoConsulta.equals(QuantidadeVolumesVO.CHAVE_VOLUME)) {

			vo.setDados(this.obterListaVolume(filtro));

		}

		return vo;
	}

	/**
	 * Validar filtro quantidades volume.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFiltroQuantidadesVolume(Map<String, Object> filtro) throws NegocioException {

		Integer tipoConsulta = (Integer) filtro.get("tipoConsulta");
		Date dataInicio = (Date) filtro.get(DATA_CONSULTA_INICIAL);
		Date dataFim = (Date) filtro.get(DATA_CONSULTA_FINAL);

		if (tipoConsulta != null && !QuantidadeVolumesVO.TIPOS_QDA_VOLUME.containsKey(tipoConsulta)) {

			throw new NegocioException(ERRO_NEGOCIO_TIPO_CONSULTA_QTDA_VOLUMES, true);

		}

		if (dataInicio == null) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_QTDA_VOLUMES, true);

		} else if (dataFim == null) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_QTDA_VOLUMES, true);

		} else {

			if (dataInicio.after(dataFim)) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_MAIOR_DATA_FIM_QTDA_VOLUMES, true);

			}

		}

	}

	/**
	 * Obter lista apuracao.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<DadosQuantidadeVolumesVO> obterListaApuracao(Map<String, Object> filtro) throws NegocioException {

		Date dataInicio = (Date) filtro.get(DATA_CONSULTA_INICIAL);
		Date dataFim = (Date) filtro.get(DATA_CONSULTA_FINAL);
		Long idContrato = (Long) filtro.get(ID_CONTRATO);
		Long idContratoModalidade = (Long) filtro.get(CONTRATO_MODALIDADE);

		Collection<Date> datas = this.gerarChavesQuantidadeVolumesVO(dataInicio, dataFim);
		List<DadosQuantidadeVolumesVO> listaVO = new ArrayList<>();

		for (Date data : datas) {

			boolean add = false;
			SoliticaoPontoConsumoVO soliticaoPontoConsumoVO = this
							.obterValoresSoliticaoPontoConsumo(data, idContrato, idContratoModalidade);

			BigDecimal qdc = this.obterQdcContrato(data, idContrato);
			BigDecimal top = this.obterTopContrato(data, idContrato, idContratoModalidade);
			BigDecimal sop = this.obterSopContrato(data, idContrato, idContratoModalidade);
			BigDecimal dop = this.obterDopContrato(data, idContrato, idContratoModalidade);

			if (qdc != null || soliticaoPontoConsumoVO.getValorQds() != null || soliticaoPontoConsumoVO.getValorQdp() != null
							|| top != null || sop != null || dop != null) {

				add = true;
			}

			if (add) {
				DadosQuantidadeVolumesVO vo = new DadosQuantidadeVolumesVO();
				vo.setData(data);
				vo.setQdc(qdc);
				vo.setQds(soliticaoPontoConsumoVO.getValorQds());
				vo.setQdp(soliticaoPontoConsumoVO.getValorQdp());
				vo.setTop(top);
				vo.setSop(sop);
				vo.setDop(dop);

				listaVO.add(vo);
			}
		}

		return listaVO;
	}

	/**
	 * Obter lista penalidade.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<DadosQuantidadeVolumesVO> obterListaPenalidade(Map<String, Object> filtro) throws NegocioException {

		Date dataInicio = (Date) filtro.get(DATA_CONSULTA_INICIAL);
		Date dataFim = (Date) filtro.get(DATA_CONSULTA_FINAL);
		Long idContrato = (Long) filtro.get(ID_CONTRATO);
		Long idContratoModalidade = (Long) filtro.get(CONTRATO_MODALIDADE);

		Collection<Date> datas = this.gerarChavesQuantidadeVolumesVO(dataInicio, dataFim);
		List<DadosQuantidadeVolumesVO> listaVO = new ArrayList<>();

		for (Date data : datas) {

			boolean add = false;

			BigDecimal qdc = this.obterQdcContrato(data, idContrato);
			BigDecimal qdr = this.obterQdrContrato(data, idContrato, idContratoModalidade);

			if (qdc != null || qdr != null) {

				add = true;

			}

			if (add) {
				DadosQuantidadeVolumesVO vo = new DadosQuantidadeVolumesVO();
				vo.setData(data);
				vo.setQdc(qdc);
				vo.setQdr(qdr);

				listaVO.add(vo);
			}
		}

		return listaVO;
	}

	/**
	 * Obter lista volume.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<DadosQuantidadeVolumesVO> obterListaVolume(Map<String, Object> filtro) throws NegocioException {

		Date dataInicio = (Date) filtro.get(DATA_CONSULTA_INICIAL);
		Date dataFim = (Date) filtro.get(DATA_CONSULTA_FINAL);
		Long idContrato = (Long) filtro.get(ID_CONTRATO);
		Long idContratoModalidade = (Long) filtro.get(CONTRATO_MODALIDADE);

		Collection<Date> datas = this.gerarChavesQuantidadeVolumesVO(dataInicio, dataFim);
		List<DadosQuantidadeVolumesVO> listaVO = new ArrayList<>();

		for (Date data : datas) {

			boolean add = false;
			SoliticaoPontoConsumoVO soliticaoPontoConsumoVO = this
							.obterValoresSoliticaoPontoConsumo(data, idContrato, idContratoModalidade);

			BigDecimal qdc = this.obterQdcContrato(data, idContrato);
			BigDecimal qdr = this.obterQdrContrato(data, idContrato, idContratoModalidade);
			BigDecimal qr = this.obterQrContrato(data, idContrato, idContratoModalidade);

			if (qdc != null || soliticaoPontoConsumoVO.getValorQds() != null || soliticaoPontoConsumoVO.getValorQdp() != null
							|| soliticaoPontoConsumoVO.getValorQpnr() != null || qdr != null || qr != null) {

				add = true;
			}

			if (add) {
				DadosQuantidadeVolumesVO vo = new DadosQuantidadeVolumesVO();
				vo.setData(data);
				vo.setQdc(qdc);
				vo.setQds(soliticaoPontoConsumoVO.getValorQds());
				vo.setQdp(soliticaoPontoConsumoVO.getValorQdp());
				vo.setQdr(qdr);
				vo.setQpnr(soliticaoPontoConsumoVO.getValorQpnr());
				vo.setQr(qr);

				listaVO.add(vo);
			}
		}

		return listaVO;
	}

	/**
	 * Obter valores soliticao ponto consumo.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @return the soliticao ponto consumo vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private SoliticaoPontoConsumoVO obterValoresSoliticaoPontoConsumo(Date data, Long idContrato, Long idContratoModalidade) {

		SoliticaoPontoConsumoVO retorno = new SoliticaoPontoConsumoVO();

		Criteria criteria = this.createCriteria(SolicitacaoConsumoPontoConsumo.class);
		criteria.createAlias(CONTRATO_PONTO_CONSUMO_MODALIDADE, CONTRATO_PONTO_CONSUMO_MODALIDADE);
		criteria.createAlias("contratoPontoConsumoModalidade.contratoPontoConsumo", CONTRATOPONTOCONSUMO);
		criteria.createAlias("contratoPontoConsumo.contrato", PONTO_CONSUMO_CONTRATO);

		if (idContratoModalidade != null) {
			criteria.createAlias("contratoPontoConsumoModalidade.contratoModalidade", CONTRATO_MODALIDADE);
			criteria.add(Restrictions.eq("contratoModalidade.chavePrimaria", idContratoModalidade));
		}

		criteria.add(Restrictions.eq("dataSolicitacao", data));
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));

		List<SolicitacaoConsumoPontoConsumo> lista = criteria.list();

		if (lista != null && !lista.isEmpty()) {

			for (SolicitacaoConsumoPontoConsumo solicitacao : lista) {
				retorno.addValorQds(solicitacao.getValorQDS());
				retorno.addValorQdp(solicitacao.getValorQDP());
				retorno.addValorQpnr(solicitacao.getValorQPNR());
			}

		}

		return retorno;
	}

	/**
	 * Obter qdc contrato.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterQdcContrato(Date data, long idContrato) throws NegocioException {

		BigDecimal qdc = null;

		Criteria criteria = this.createCriteria(ContratoQDC.class);
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		criteria.add(Restrictions.eq("data", data));

		List<ContratoQDC> listaContratoQDC = criteria.list();

		if (listaContratoQDC != null && !listaContratoQDC.isEmpty()) {
			qdc = Util .primeiroElemento(listaContratoQDC).getMedidaVolume();
		}

		return qdc;
	}

	/**
	 * Gerar chaves quantidade volumes vo.
	 *
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @return the collection
	 */
	private Collection<Date> gerarChavesQuantidadeVolumesVO(Date dataInicio, Date dataFim) {

		Collection<Date> datas = new TreeSet<>();
		datas.add(dataInicio);
		datas.addAll(Util.gerarIntervaloDatas(dataInicio, dataFim));

		for (Date data : datas) {

			DadosQuantidadeVolumesVO vo = new DadosQuantidadeVolumesVO();
			vo.setData(data);

		}

		return datas;
	}

	/**
	 * The Class SoliticaoPontoConsumoVO.
	 */
	private class SoliticaoPontoConsumoVO {

		private BigDecimal valorQds;

		private BigDecimal valorQdp;

		private BigDecimal valorQpnr;

		public BigDecimal getValorQds() {

			return valorQds;
		}

		public BigDecimal getValorQdp() {

			return valorQdp;
		}

		public BigDecimal getValorQpnr() {

			return valorQpnr;
		}

		/**
		 * Adds the valor qds.
		 *
		 * @param valor
		 *            the valor
		 */
		public void addValorQds(BigDecimal valor) {

			if (valor != null) {
				if (this.valorQds != null) {
					valorQds = valorQds.add(valor);
				} else {
					valorQds = valor;
				}
			}
		}

		/**
		 * Adds the valor qdp.
		 *
		 * @param valor
		 *            the valor
		 */
		public void addValorQdp(BigDecimal valor) {

			if (valor != null) {
				if (this.valorQdp != null) {
					valorQdp = valorQdp.add(valor);
				} else {
					valorQdp = valor;
				}
			}
		}

		/**
		 * Adds the valor qpnr.
		 *
		 * @param valor
		 *            the valor
		 */
		public void addValorQpnr(BigDecimal valor) {

			if (valor != null) {
				if (this.valorQpnr != null) {
					valorQpnr = valorQpnr.add(valor);
				} else {
					valorQpnr = valor;
				}
			}
		}

	}

	/**
	 * Obter qdr contrato.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterQdrContrato(Date data, Long idContrato, Long idContratoModalidade) {

		return null;
	}

	/**
	 * Obter qr contrato.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterQrContrato(Date data, Long idContrato, Long idContratoModalidade) {

		return null;
	}

	/**
	 * Obter top contrato.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterTopContrato(Date data, Long idContrato, Long idContratoModalidade) {

		return null;
	}

	/**
	 * Obter sop contrato.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterSopContrato(Date data, Long idContrato, Long idContratoModalidade) {

		return null;
	}

	/**
	 * Obter dop contrato.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterDopContrato(Date data, Long idContrato, Long idContratoModalidade) {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * verificarMudancaTitularidadeContratoPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public Date verificarMudancaTitularidadeContratoPontoConsumo(PontoConsumo pontoConsumo, Date paramDataInicio, Date paramDataFim)
					throws NegocioException {

		Date data = null;

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		SimpleDateFormat dfHms = new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_BR);
		Date dataInicio = null;
		Date dataFim = null;
		try {

			if (paramDataInicio != null && paramDataFim != null) {

				dataInicio = dfHms.parse(df.format(paramDataInicio) + " 00:00:00");
				dataFim = dfHms.parse(df.format(paramDataFim) + " 23:59:59");

			}

		} catch (ParseException e) {
			throw new NegocioException("ERRO_INESPERADO", true);
		}

		Criteria criteria = this.createCriteria(ContratoPontoConsumo.class);
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria()));
		criteria.createAlias(PONTO_CONSUMO_CONTRATO, PONTO_CONSUMO_CONTRATO, Criteria.INNER_JOIN);
		criteria.createAlias("contrato.clienteAssinatura", CLIENTE_ASSINATURA, Criteria.INNER_JOIN);

		criteria.add(Restrictions.between("contrato.ultimaAlteracao", dataInicio, dataFim));
		criteria.addOrder(Order.desc("contrato.ultimaAlteracao"));

		List<ContratoPontoConsumo> listaContratoPontoConsumo = criteria.list();

		if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {
			Cliente clienteAssinatura = null;

			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
				if (clienteAssinatura != null) {
					if (clienteAssinatura.getChavePrimaria() != contratoPontoConsumo.getContrato().getClienteAssinatura()
									.getChavePrimaria()) {
						data = contratoPontoConsumo.getContrato().getUltimaAlteracao();
						break;
					}
				} else {
					clienteAssinatura = contratoPontoConsumo.getContrato().getClienteAssinatura();
				}
			}

		}

		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#carregarContratoEncerramentoRescisao(java.lang.Long)
	 */
	@Override
	public Contrato carregarContratoEncerramentoRescisao(Long idContrato) throws NegocioException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		Contrato contrato = (Contrato) controladorContrato.obter(idContrato, SITUACAO, "proposta", LISTA_CONTRATO_PONTO_CONSUMO,
						"listaContratoPontoConsumo.listaContratoPontoConsumoModalidade", ATRIBUTO_LISTA_CONTRATO_QDC,
						ATRIBUTO_LISTA_CONTRATO_CLIENTE, "listaContratoPenalidade");

		if (contrato.getFaturaEncerramentoGerada() != null && contrato.getFaturaEncerramentoGerada()) {
			throw new NegocioException(ERRO_NEGOCIO_CONTRATO_FATURA_ENCERRAMENTO_GERADA, true);
		}

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #prepararCalculoIndenizacaoEncerramentoRescisao
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public CalculoIndenizacaoVO prepararCalculoIndenizacaoRescisao(final Contrato contrato, Long idPontoConsumo) throws NegocioException {

		CalculoIndenizacaoVO dadosCalculo = new CalculoIndenizacaoVO();
		Contrato contratoLocal = contrato;

		if (contratoLocal.getMultaRecisoria() != null) {
			if (contratoLocal.getDataInvestimento() != null) {
				if (contratoLocal.getValorInvestimento() != null) {
					dadosCalculo.setTipoMulta(contratoLocal.getMultaRecisoria().getChavePrimaria());
					dadosCalculo.setValorInvestimento(contratoLocal.getValorInvestimento().setScale(ESCALA_INVESTIMENTO, RoundingMode.HALF_EVEN));
					dadosCalculo.setDataInvestimento(new DateTime(contratoLocal.getDataInvestimento()));

					DateTime dataRecisao;
					if(contratoLocal.getDataRecisao() != null){
						dataRecisao = new DateTime(contratoLocal.getDataRecisao());
					}else{
						dataRecisao = new DateTime();
					}
					dadosCalculo.setDataRecisao(Util.zerarHorario(dataRecisao));

					if (idPontoConsumo != 0 && idPontoConsumo > 0) {
						contratoLocal = obterContratoPorChaveContratoPorPonto(contratoLocal.getChavePrimaria(), idPontoConsumo);
					}

					if (CONTRATO_MULTA_POR_QTD_MEDIA.equals(dadosCalculo.getTipoMulta())) {
						preencherDadosIndenizacaoPorQtdDiariaContratada(contratoLocal, dadosCalculo, idPontoConsumo);
					} else if (CONTRATO_MULTA_POR_QTD_MINIMA_CONTRATADA.equals(dadosCalculo.getTipoMulta())) {
						preencherDadosIndenizacaoPorQtdMinimaContratada(contratoLocal, dadosCalculo);
					} else if (CONTRATO_MULTA_POR_TEMPO_RELACIONAMENTO.equals(dadosCalculo.getTipoMulta())) {
						preencherDadosIndenizacaoPorTempoRelacionamento(dadosCalculo);
					} else {
						throw new NegocioException(ERRO_NEGOCIO_CALCULO_RECISAO_INDEFINIDO, true);
					}
				} else {
					throw new NegocioException(ERRO_NEGOCIO_VALOR_INVESTIMENTO_AUSENTE, true);
				}
			} else {
				throw new NegocioException(ERRO_NEGOCIO_DATA_INVESTIMENTO_AUSENTE, true);
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_MULTA_RECISORIA_AUSENTE, true);
		}

		return dadosCalculo;
	}

	/**
	 * Carrega os valores iniciais para o cálculo
	 * da indenização pela quantidade diária
	 * contratada.
	 *
	 * @param contrato
	 *            the contrato
	 * @param dadosCalculo
	 *            the dados calculo
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void preencherDadosIndenizacaoPorQtdDiariaContratada(Contrato contrato, CalculoIndenizacaoVO dadosCalculo, Long idPontoConsumo)
					throws NegocioException {

		dadosCalculo.setQtdDiariaContratada(calcularQDC(contrato, dadosCalculo.getDataRecisao()));

		DateTime dataVencimento = new DateTime(contrato.getDataVencimentoObrigacoes());
		if (dataVencimento.isBefore(dadosCalculo.getDataRecisao())) {
			throw new NegocioException(ERRO_NEGOCIO_INDENIZACAO_QDC_CONTRATO_VENCIDO, true);
		}

		// Data de vencimento das obrigações -
		// Data da recisão
		Days diasRestantes = Days.daysBetween(dadosCalculo.getDataRecisao(), dataVencimento);
		dadosCalculo.setDiasRestantes(new BigDecimal(diasRestantes.getDays()));

		if (dadosCalculo.getDiasRestantes().intValue() > DIAS_ANO) {
			dadosCalculo.setDiasRestantes(new BigDecimal(DIAS_ANO));
		}

		ContratoPontoConsumo contratoPontoConsumo = consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);

		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);
		ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento = controladorParcelamento
						.obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);

		// Calculo da tarifa do gas a ser usada.
		Date atual = new Date();
		if (contratoPontoConsumoItemFaturamento == null) {
			throw new NegocioException(ERRO_NEGOCIO_PONTO_CONSUMO_CONTRATO_SEM_ITEM_FATURA, true);
		} else {
			EntidadeConteudo itemFatura = contratoPontoConsumoItemFaturamento.getItemFatura();
			Tarifa tarifa = contratoPontoConsumoItemFaturamento.getTarifa();

			ControladorCalculoFornecimentoGas controladorCalculoFornecimentoGas = (ControladorCalculoFornecimentoGas) ServiceLocator
							.getInstancia().getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);
			DadosFornecimentoGasVO dadosFornecimentoGasVO = controladorCalculoFornecimentoGas.calcularValorFornecimentoGas(itemFatura,
							atual, atual, new BigDecimal(VALOR_CONSUMO_APURADO), tarifa, true, false, null, Boolean.FALSE);

			dadosCalculo.setTarifaGas(dadosFornecimentoGasVO.getValorFinalMetroCubicoGas().setScale(ESCALA_CONVERSAO_GAS, RoundingMode.HALF_EVEN));
		}
	}

	/**
	 * Carrega os valores iniciais para o cálculo
	 * da indenização pela quantidade mínima
	 * contratada.
	 *
	 * @param contrato
	 *            the contrato
	 * @param dadosCalculo
	 *            the dados calculo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void preencherDadosIndenizacaoPorQtdMinimaContratada(Contrato contrato, CalculoIndenizacaoVO dadosCalculo)
					throws NegocioException {

		preencherDadosIndenizacaoPorTempoRelacionamento(dadosCalculo);

		dadosCalculo.setQtdDiariaContratada(calcularQDC(contrato, dadosCalculo.getDataRecisao()));
		dadosCalculo.setQtdMinimaContratada(dadosCalculo.getQtdDiariaContratada().multiply(new BigDecimal(CONVERSAO_QTD_DIARIA_CONTRATADA)));

		dadosCalculo.setQtdConsumida(calcularQtdConsumida(contrato, dadosCalculo.getDataRecisao()));
	}

	/**
	 * Carrega os valores iniciais para o cálculo
	 * da indenização pelo tempo de
	 * relacionamento.
	 *
	 * @param dadosCalculo
	 *            the dados calculo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void preencherDadosIndenizacaoPorTempoRelacionamento(CalculoIndenizacaoVO dadosCalculo)
					throws NegocioException {

		dadosCalculo.setMesesPeriodoConsumo(calcularMesesPeriodoConsumo(dadosCalculo.getDataInvestimento(), dadosCalculo.getDataRecisao()));

		dadosCalculo.setIgpmInvestimento(buscarValorIndiceFinanceiroNominal(dadosCalculo.getDataInvestimento(),
						INDICE_FINANCEIRO_IGPM_ABREVIADO));

		try {
			dadosCalculo.setIgpmRecisao(buscarValorIndiceFinanceiroNominal(dadosCalculo.getDataRecisao(), INDICE_FINANCEIRO_IGPM_ABREVIADO));
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			DateTime mesAnteriorRecisao = dadosCalculo.getDataRecisao().minusMonths(1);
			dadosCalculo.setIgpmRecisao(buscarValorIndiceFinanceiroNominal(mesAnteriorRecisao, INDICE_FINANCEIRO_IGPM_ABREVIADO));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #calcularIndenizacaoEncerramentoRescisao
	 * (br.com.ggas.web.contrato.contrato.
	 * CalculoIndenizacaoVO)
	 */
	@Override
	public void calcularIndenizacaoRescisao(CalculoIndenizacaoVO dadosCalculo) throws NegocioException {

		if (!CONTRATO_MULTA_POR_QTD_MEDIA.equals(dadosCalculo.getTipoMulta())) {
			if (dadosCalculo.getValorAdicionalMensal() == null || dadosCalculo.getDataRecisao() == null) {
				throw new NegocioException(ERRO_NEGOCIO_DADOS_AUSENTES_CALCULO_RECISAO, true);
			} else if (dadosCalculo.getDataRecisao().isBefore(dadosCalculo.getDataInvestimento())) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_RECISAO_MENOR_QUE_DATA_INVESTIMENTO, true);
			} else if (dadosCalculo.getDataRecisao().isAfter(new DateTime())) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_RECISAO_MAIOR_QUE_ATUAL, true);
			}
		}

		if (CONTRATO_MULTA_POR_QTD_MEDIA.equals(dadosCalculo.getTipoMulta())) {
			calcularIndenizacaoPorQtdDiariaContratada(dadosCalculo);
		} else if (CONTRATO_MULTA_POR_TEMPO_RELACIONAMENTO.equals(dadosCalculo.getTipoMulta())) {
			calcularIndenizacaoPorTempoRelacionamento(dadosCalculo);
		} else if (CONTRATO_MULTA_POR_QTD_MINIMA_CONTRATADA.equals(dadosCalculo.getTipoMulta())) {
			calcularIndenizacaoPorQtdMinimaContratada(dadosCalculo);
		}
	}

	/**
	 * Calcula com base nos valores informados a
	 * indenização pela quantidade diária
	 * contratada.
	 *
	 * @param dadosCalculo
	 *            the dados calculo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void calcularIndenizacaoPorQtdDiariaContratada(CalculoIndenizacaoVO dadosCalculo) {

		dadosCalculo.setValorTotal(dadosCalculo.getQtdDiariaContratada().multiply(dadosCalculo.getDiasRestantes())
						.multiply(dadosCalculo.getTarifaGas()).setScale(ESCALA_TARIFA_GAS, RoundingMode.HALF_EVEN));
	}

	/**
	 * Calcula com base nos valores informados a
	 * indenização pela quantidade mínima
	 * contratada.
	 *
	 * @param dadosCalculo
	 *            the dados calculo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void calcularIndenizacaoPorQtdMinimaContratada(CalculoIndenizacaoVO dadosCalculo) {

		dadosCalculo.setInvestimentoCorrigido(calcularInvestimentoCorrigido(dadosCalculo));

		dadosCalculo.setValorTotal(dadosCalculo
						.getInvestimentoCorrigido()
						.multiply(dadosCalculo.getQtdMinimaContratada().subtract(dadosCalculo.getQtdConsumida())
										.divide(dadosCalculo.getQtdMinimaContratada(), ESCALA_CORRECAO_INVESTIMENTO, BigDecimal.ROUND_DOWN))
						.setScale(CONVERSAO_QTD_DIARIA_MINIMA_CONTRATADA, RoundingMode.HALF_EVEN));
	}

	/**
	 * Calcula com base nos valores informados a
	 * indenização pelo tempo de relacionamento.
	 *
	 * @param dadosCalculo
	 *            the dados calculo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void calcularIndenizacaoPorTempoRelacionamento(CalculoIndenizacaoVO dadosCalculo){

		dadosCalculo.setInvestimentoCorrigido(calcularInvestimentoCorrigido(dadosCalculo));

		BigDecimal fator = new BigDecimal(DIVISOR).subtract(dadosCalculo.getMesesPeriodoConsumo()).divide(new BigDecimal(DIVISOR),
				ESCALA_CORRECAO_INVESTIMENTO,
						BigDecimal.ROUND_DOWN);
		dadosCalculo.setValorTotal(
				dadosCalculo.getInvestimentoCorrigido().multiply(fator.abs()).setScale(ESCALA_INVESTIMENTO, RoundingMode.HALF_EVEN));
	}

	/**
	 * Calcula o investimento corrigido pelo IGPM
	 * e pelo período conforme formula abaixo:
	 * investimento + (investimento * (
	 * (igpmRecisao/igpmInvestimento) - 1)) +
	 * (investimento * adicionalMensal *
	 * mesesPeriodoConsumo).
	 *
	 * @param contrato
	 *            Contrato referência para o
	 *            cálculo.
	 * @param adicionalMensal
	 *            Percentual mensal informado pelo
	 *            usuário.
	 * @param datainvestimento
	 *            Data de investimento do
	 *            contrato.
	 * @param dataRecisao
	 *            Data do encerramento/recisao.
	 * @return investimentoCorrigido
	 * @throws NegocioException
	 */
	private BigDecimal calcularInvestimentoCorrigido(CalculoIndenizacaoVO dadosCalculo) {

		BigDecimal investimentoCorrigido = null;

		BigDecimal ajusteIgpm = dadosCalculo.getValorInvestimento().multiply(
						dadosCalculo.getIgpmRecisao().divide(dadosCalculo.getIgpmInvestimento(), ESCALA_CORRECAO_INVESTIMENTO, BigDecimal.ROUND_DOWN)
										.subtract(BigDecimal.ONE));

		BigDecimal ajustePeriodo = dadosCalculo.getValorInvestimento().multiply(dadosCalculo.getValorAdicionalMensal())
						.multiply(dadosCalculo.getMesesPeriodoConsumo());

		investimentoCorrigido = dadosCalculo.getValorInvestimento().add(ajusteIgpm).add(ajustePeriodo)
						.setScale(ESCALA_INVESTIMENTO, BigDecimal.ROUND_HALF_EVEN);

		return investimentoCorrigido;
	}

	/**
	 * Calcula a quantidade diária contratada para
	 * o contrato informado.
	 *
	 * @param contrato
	 *            the contrato
	 * @param dataRecisao
	 *            the data recisao
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal calcularQDC(Contrato contrato, DateTime dataRecisao) throws NegocioException {

		BigDecimal somatorioQDC = BigDecimal.ZERO;

		if (contrato != null && contrato.getListaContratoQDC() != null) {
			// FIXME: Tratar esta situaçãopor parâmetro
			ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
							ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
			Empresa principal = controladorEmpresa.obterEmpresaPrincipal();

			if (NomeClienteAbreviado.COMPAGAS.getValor().equalsIgnoreCase(principal.getCliente().getNomeAbreviado())) {
				for (ContratoPontoConsumo cpc : contrato.getListaContratoPontoConsumo()) {
					for (ContratoPontoConsumoModalidade cpcModalidade : cpc.getListaContratoPontoConsumoModalidade()) {
						for (ContratoPontoConsumoModalidadeQDC cpcModalidadeQDC : cpcModalidade.getListaContratoPontoConsumoModalidadeQDC()) {
							somatorioQDC = somatorioQDC.add(cpcModalidadeQDC.getMedidaVolume());
						}
					}
				}
			} else if (NomeClienteAbreviado.ALGAS.getValor().equalsIgnoreCase(principal.getCliente().getNomeAbreviado())) {
				for (ContratoPontoConsumo cpc : contrato.getListaContratoPontoConsumo()) {
					for (ContratoPontoConsumoModalidade cpcModalidade : cpc.getListaContratoPontoConsumoModalidade()) {
						for (ContratoPontoConsumoModalidadeQDC cpcModalidadeQDC : cpcModalidade.getListaContratoPontoConsumoModalidadeQDC()) {
							somatorioQDC = cpcModalidadeQDC.getMedidaVolume();
							break;
						}
						break;
					}
					break;
				}
			}
		}

		if (somatorioQDC.compareTo(BigDecimal.ZERO)==0) {
			throw new NegocioException(ERRO_NEGOCIO_CONTRATO_PONTO_CONSUMO_SEM_QDC, true);
		}

		return somatorioQDC;
	}

	/**
	 * Calcula a quantidade consumida para o
	 * contrato informado.
	 *
	 * @param contrato
	 *            the contrato
	 * @param dataRecisao
	 *            the data recisao
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal calcularQtdConsumida(Contrato contrato, DateTime dataRecisao) throws NegocioException {

		BigDecimal qtdConsumida = BigDecimal.ZERO;

		if (contrato != null && contrato.getDataAssinatura() != null && contrato.getListaContratoPontoConsumo() != null
						&& dataRecisao != null) {

			Collection<Long> idsPonto = new ArrayList<>();
			for (ContratoPontoConsumo cpc : contrato.getListaContratoPontoConsumo()) {
				idsPonto.add(cpc.getPontoConsumo().getChavePrimaria());
			}

			DateTime dataRecisaoFim = dataRecisao.withHourOfDay(LIMITE_HORA);
			dataRecisaoFim = dataRecisaoFim.withMinuteOfHour(LIMITE_MINUTOS);
			dataRecisaoFim = dataRecisaoFim.withSecondOfMinute(LIMITE_SEGUNDOS);

			Criteria criteria = getSession().createCriteria(HistoricoConsumo.class);
			criteria.add(Restrictions.eq("indicadorConsumoCiclo", true));
			criteria.add(Restrictions.eq("indicadorFaturamento", true));

			criteria.createCriteria("historicoAtual").add(
							Restrictions.between("dataLeituraFaturada", contrato.getDataAssinatura(), dataRecisaoFim.toDate()));
			criteria.createCriteria(PONTO_CONSUMO).add(Restrictions.in(CHAVE_PRIMARIA, idsPonto));

			Collection<HistoricoConsumo> historicos = criteria.list();

			for (HistoricoConsumo historico : historicos) {
				qtdConsumida = qtdConsumida.add(historico.getConsumoApurado());
			}
			qtdConsumida = qtdConsumida.setScale(ESCALA_CONVERSAO_GAS, RoundingMode.HALF_EVEN);
		}

		return qtdConsumida;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #calcularMesesPeriodoConsumo
	 * (org.joda.time.DateTime,
	 * org.joda.time.DateTime)
	 */
	@Override
	public BigDecimal calcularMesesPeriodoConsumo(DateTime datainvestimento, DateTime dataRecisao)
			throws NegocioException {

		BigDecimal mesesPeriodoConsumo = null;

		if (datainvestimento != null && dataRecisao != null) {
			if (dataRecisao.isAfter(new DateTime())) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_RECISAO_MAIOR_QUE_ATUAL, true);
			}

			if (datainvestimento.isAfter(dataRecisao)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_RECISAO_MENOR_QUE_INVESTIMENTO, true);
			}

			Days periodoEmDias = Days.daysBetween(datainvestimento, dataRecisao);
			mesesPeriodoConsumo = BigDecimal.valueOf(periodoEmDias.getDays() / CONSTANTE_TRINTA_DIAS)
					.setScale(ESCALA_INVESTIMENTO, RoundingMode.HALF_EVEN);
		}

		return mesesPeriodoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #buscarValorIndiceFinanceiroNominal
	 * (org.joda.time.DateTime, java.lang.String)
	 */
	@Override
	public BigDecimal buscarValorIndiceFinanceiroNominal(DateTime dataReferencia, String descricaoAbreviada) throws NegocioException {

		DateTime data = Util.zerarHorario(dataReferencia);
		data = data.withDayOfMonth(1);

		Criteria criteria = getSession().createCriteria(IndiceFinanceiroValorNominal.class);
		criteria.createCriteria(INDICE_FINANCEIRO).add(Restrictions.eq(DESCRICAO_ABREVIADA, descricaoAbreviada));
		criteria.add(Restrictions.eq(DATA_REFERENCIA, data.toDate()));

		IndiceFinanceiroValorNominal indice = (IndiceFinanceiroValorNominal) criteria.uniqueResult();

		if (indice == null || indice.getValorNominal() == null) {
			throw new NegocioException(ERRO_NEGOCIO_INDICE_FINANCEIRO_INEXISTENTE,
							new Object[] {descricaoAbreviada, Util.converterDataParaString(data.toDate())});
		}

		return indice.getValorNominal();
	}

	/**
	 * Validar contrato data recisao.
	 *
	 * @param contrato
	 *            the contrato
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarContratoDataRecisao(Contrato contrato) throws NegocioException {

		if (contrato.getSituacao().getChavePrimaria() == SituacaoContrato.RESCINDIDO
						|| contrato.getSituacao().getChavePrimaria() == SituacaoContrato.RESCINDIDO && contrato.getDataRecisao() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, " Data de Recisão");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoListasCarregadas
	 * (java.lang.Long)
	 */
	@Override
	public Contrato obterContratoListasCarregadas(long chavePrimaria, Boolean habilitado) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contrato ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" contrato");
		hql.append(" left join fetch contrato.situacao situacao ");
		hql.append(" left join fetch contrato.clienteAssinatura cliente ");
		hql.append(" left join fetch contrato.listaContratoPontoConsumo listaContratoPontoConsumo ");

		hql.append(" left join fetch listaContratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" left join fetch pontoConsumo.segmento segmento ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor instalacaoMedidor ");

		hql.append(" left join fetch listaContratoPontoConsumo.listaContratoPontoConsumoModalidade listaContratoPontoConsumoModalidade ");
		hql.append(" left join fetch listaContratoPontoConsumoModalidade.listaContratoPontoConsumoModalidadeQDC ");
		hql.append("listaContratoPontoConsumoModalidadeQDC ");
		hql.append(" left join fetch listaContratoPontoConsumoModalidade.listaContratoPontoConsumoPenalidade ");
		hql.append("listaContratoPontoConsumoPenalidade ");
		hql.append(" left join fetch listaContratoPontoConsumo.listaContratoPontoConsumoItemFaturamento ");
		hql.append("listaContratoPontoConsumoItemFaturamento ");
		hql.append(" left join fetch listaContratoPontoConsumo.listaContratoPontoConsumoPCSAmostragem ");
		hql.append("listaContratoPontoConsumoPCSAmostragem ");
		hql.append(" left join fetch listaContratoPontoConsumo.listaContratoPontoConsumoPCSIntervalo listaContratoPontoConsumoPCSIntervalo ");
		hql.append(" left join fetch contrato.listaContratoPenalidade listaContratoPenalidade ");

		hql.append(" left join fetch contrato.listaContratoCliente listaContratoCliente ");
		hql.append(" left join fetch contrato.listaContratoQDC listaContratoQDC ");
		hql.append(" left join fetch contrato.listaFatura listaFatura ");
		hql.append(" left join fetch contrato.proposta proposta ");
		hql.append(WHERE);
		hql.append(" contrato.chavePrimaria = :idContrato ");

		if (habilitado != null) {
			hql.append(" and contrato.habilitado = :habilitado ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_CONTRATO, chavePrimaria);

		if (habilitado != null) {
			query.setParameter(HABILITADO, habilitado);
		}

		return (Contrato) query.uniqueResult();
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoPontoConsumo
	 * (br.com.ggas.contrato.contrato.Contrato,
	 * br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public ContratoPontoConsumo obterContratoPontoConsumo(Contrato contrato, PontoConsumo pontoConsumo) throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoPontoConsumo.class);
		criteria.createAlias(PONTO_CONSUMO_CONTRATO, PONTO_CONSUMO_CONTRATO, Criteria.LEFT_JOIN);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, contrato.getChavePrimaria()));
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria()));

		return (ContratoPontoConsumo) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoModalidades
	 * (br.com.ggas.contrato.contrato.Contrato,
	 * br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidade> consultarContratoPontoConsumoModalidades(Contrato contrato, PontoConsumo pontoConsumo)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" Select contratoPontoConsumoModalidade ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
		hql.append(" contratoPontoConsumoModalidade ");
		hql.append(" inner join contratoPontoConsumoModalidade.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join contratoPontoConsumoModalidade.contratoModalidade contratoModalidade ");
		hql.append(" inner join contratoPontoConsumo.contrato contrato ");
		if (pontoConsumo != null) {
			hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		}
		hql.append(WHERE);
		hql.append(" contratoPontoConsumo.contrato.chavePrimaria = ? ");
		if (pontoConsumo != null) {
			hql.append(" and contratoPontoConsumo.pontoConsumo.chavePrimaria = ? ");
		}

		hql.append(" order by contratoModalidade.sequenciaApuracaoVolume asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, contrato.getChavePrimaria());
		if (pontoConsumo != null) {
			query.setLong(1, pontoConsumo.getChavePrimaria());
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #calcularQDCMedia(java.lang.Long,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal calcularQDCMedia(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Date primeiroDiaPeriodo, Date ultimoDiaPeriodo) throws NegocioException {

		final Integer zero = 0;

		BigDecimal somaQDCs = consultarContratoPontoConsumoModalidadeQDCPorModalidade(listaContratoPontoConsumoModalidade,
						primeiroDiaPeriodo, ultimoDiaPeriodo);

		Integer quantidadeDias = Util.quantidadeDiasIntervalo(primeiroDiaPeriodo, ultimoDiaPeriodo);

		BigDecimal mediaQDC = null;
		if (somaQDCs != null && somaQDCs.compareTo(BigDecimal.ZERO) > 0 && quantidadeDias != null && quantidadeDias.compareTo(zero) > 0) {
			mediaQDC = somaQDCs.divide(BigDecimal.valueOf(quantidadeDias), ESCALA_DIVISAO_QDC_DIAS, RoundingMode.HALF_UP);
		} else {
			mediaQDC = BigDecimal.ZERO;
		}

		return mediaQDC;
	}


	/**
	 * Consulta uma lista de entidades {@link ContratoPontoConsumoModalidadeQDC},
	 * que pertencem a uma coleção de entidades do tipo {@link ContratoPontoConsumoModalidade},
	 * retornando estes com suas respectivas listas de {@link ContratoPontoConsumoModalidadeQDC}
	 * populadas.
	 *
	 * @param listaContratoPontoConsumoModalidade - {@link ContratoPontoConsumoModalidade}
	 * @return listaContratoPontoConsumoModalidade - {@link List}
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidade> popularContratoModalidadeComConsumoQDC(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade) {

		Map<Long, Collection<ContratoPontoConsumoModalidadeQDC>> mapaContratoPontoConsumoModalidadeQDC = null;

		mapaContratoPontoConsumoModalidadeQDC = consultarContratoPontoConsumoModalidadeQDCPorModalidade(
						listaContratoPontoConsumoModalidade);

		return popularContratoModalidadeComConsumoQDC(listaContratoPontoConsumoModalidade, mapaContratoPontoConsumoModalidadeQDC);
	}

	/**
	 * Adiciona em uma coleção de entidades do tipo {@link ContratoPontoConsumoModalidade},
	 * para cada entidade, a sua a lista de entidades do tipo {@link ContratoPontoConsumoModalidadeQDC}
	 * passada por parâmetro e mapeada por chave primária de {@link ContratoPontoConsumoModalidade}.
	 *
	 * @param listaContratoPontoConsumoModalidade - {@link Collection}
	 * @param mapaContratoPontoConsumoModalidadeQDC - {@link Map}
	 * @return listaContratoPontoConsumoModalidade - {@link Collection}
	 */
	private Collection<ContratoPontoConsumoModalidade> popularContratoModalidadeComConsumoQDC(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Map<Long, Collection<ContratoPontoConsumoModalidadeQDC>> mapaContratoPontoConsumoModalidadeQDC) {

		if (!Util.isNullOrEmpty(listaContratoPontoConsumoModalidade)) {
			for (ContratoPontoConsumoModalidade consumoModalidade : listaContratoPontoConsumoModalidade) {
				Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC = mapaContratoPontoConsumoModalidadeQDC
								.get(consumoModalidade.getChavePrimaria());
				consumoModalidade.getListaContratoPontoConsumoModalidadeQDC().addAll(listaContratoPontoConsumoModalidadeQDC);
			}
		}
		return listaContratoPontoConsumoModalidade;
	}



	/**
	 * Consulta uma coleção de entidades do tipo {@link ContratoPontoConsumoModalidadeQDC} por
	 * chave primária de um {@link ContratoPontoConsumoModalidade}.
	 *
	 * @param chaveContratoPontoConsumoModalidade - {@link Long}
	 * @return listaContratoPontoConsumoModalidadeQDC - {@link Collection}
	 */
	@Override
	public Collection<ContratoPontoConsumoModalidadeQDC> consultarContratoPontoConsumoModalidadeQDCPorModalidade(
					Long chaveContratoPontoConsumoModalidade) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoModalidadeQDC().getSimpleName());
		hql.append(" contratoPontoModalidadeQdc ");
		hql.append(WHERE);
		hql.append(" contratoPontoModalidadeQdc.contratoPontoConsumoModalidade.chavePrimaria = :chavecontratoPontoConsumoModalidade ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavecontratoPontoConsumoModalidade", chaveContratoPontoConsumoModalidade);

		return query.list();
	}

	/**
	 * Consulta uma lista de entidades do tipo {@link ContratoPontoConsumoModalidadeQDC}
	 * que pertencem a uma coleção de entidades do tipo {@link ContratoPontoConsumoModalidade}.
	 *
	 * @param listaContratoPontoConsumoModalidade - {@link ContratoPontoConsumoModalidade}
	 * @return mapaContratoPontoConsumoModalidadeQDC - {@link Map}
	 */
	private Map<Long, Collection<ContratoPontoConsumoModalidadeQDC>> consultarContratoPontoConsumoModalidadeQDCPorModalidade(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade) {

		Map<Long, Collection<ContratoPontoConsumoModalidadeQDC>> mapaContratoPontoConsumoModalidadeQDC = new HashMap<>();

		if (listaContratoPontoConsumoModalidade != null && !listaContratoPontoConsumoModalidade.isEmpty()) {

			StringBuilder hql = new StringBuilder();
			hql.append(" select contratoPontoConsumoModalidade.chavePrimaria, contratoPontoModalidadeQDC ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumoModalidadeQDC().getSimpleName());
			hql.append(" contratoPontoModalidadeQDC ");
			hql.append(" inner join contratoPontoModalidadeQDC.contratoPontoConsumoModalidade contratoPontoConsumoModalidade ");
			hql.append(WHERE);
			hql.append(" contratoPontoConsumoModalidade IN (:listaContratoPontoConsumoModalidade) ");
			Query query = getSession().createQuery(hql.toString());

			query.setParameterList("listaContratoPontoConsumoModalidade", listaContratoPontoConsumoModalidade);

			mapaContratoPontoConsumoModalidadeQDC.putAll(this.transformarResultadoConsultaContratoPontoConsumoModalidadeQDC( query.list() ));

		}

		return mapaContratoPontoConsumoModalidadeQDC;
	}

	/**
	 * Converte uma lista de objetos resultantes de uma consulta por {@link ContratoPontoConsumoModalidadeQDC} e
	 * chave primária de {@link ContratoPontoConsumoModalidade} em um mapa de {@link ContratoPontoConsumoModalidadeQDC}
	 * por chave primária de {@link ContratoPontoConsumoModalidade}.
	 *
	 * @param resultadoConsulta - {@link List}
	 * @return mapaContratoPontoConsumoModalidadeQDC - {@link Map}
	 */
	private Map<Long, Collection<ContratoPontoConsumoModalidadeQDC>> transformarResultadoConsultaContratoPontoConsumoModalidadeQDC(
					List<Object[]> resultadoConsulta) {

		Map<Long, Collection<ContratoPontoConsumoModalidadeQDC>> mapaContratoPontoConsumoModalidadeQDC = new HashMap<>();
		Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC = null;

		for (Object[] linhaConsultada : resultadoConsulta) {

			Long chaveConsumoModalidade = (Long) linhaConsultada[0];
			ContratoPontoConsumoModalidadeQDC consumoModalidadeQDC = (ContratoPontoConsumoModalidadeQDC) linhaConsultada[1];

			if (!mapaContratoPontoConsumoModalidadeQDC.containsKey(chaveConsumoModalidade)) {
				listaContratoPontoConsumoModalidadeQDC = new HashSet<>();
				mapaContratoPontoConsumoModalidadeQDC.put(chaveConsumoModalidade, listaContratoPontoConsumoModalidadeQDC);
			} else {
				listaContratoPontoConsumoModalidadeQDC = mapaContratoPontoConsumoModalidadeQDC.get(chaveConsumoModalidade);
			}

			listaContratoPontoConsumoModalidadeQDC.add(consumoModalidadeQDC);
		}
		return mapaContratoPontoConsumoModalidadeQDC;
	}


	/**
	 * Mapeia uma lista de entidades do tipo {@link ContratoPontoConsumoModalidade}
	 * por entidades do tipo {@link PontoConsumo}.
	 *
	 * @param listaContratoPontoConsumoModalidade - {@link Collection}
	 * @return mapaModalidadesPorPontoConsumo - {@link Map}
	 */
	@Override
	public Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> agruparContratoPontoConsumoModalidadePorPontoConsumo(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade) {

		Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo = null;
		List<ContratoPontoConsumoModalidade> listaModalidades = null;

		mapaModalidadesPorPontoConsumo = new HashMap<>();

		for (ContratoPontoConsumoModalidade consumoModalidade : listaContratoPontoConsumoModalidade) {

			ContratoPontoConsumo contratoPontoConsumo = consumoModalidade.getContratoPontoConsumo();
			if (contratoPontoConsumo != null) {
				if (mapaModalidadesPorPontoConsumo.containsKey(contratoPontoConsumo.getPontoConsumo())) {
					listaModalidades = mapaModalidadesPorPontoConsumo.get(contratoPontoConsumo.getPontoConsumo());
				} else {
					listaModalidades = new ArrayList<>();
				}
				listaModalidades.add(consumoModalidade);
				mapaModalidadesPorPontoConsumo.put(contratoPontoConsumo.getPontoConsumo(), listaModalidades);
			}
		}

		return mapaModalidadesPorPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * consultarContratoPontoConsumoModalidadeQDCPorModalidade
	 * (java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal consultarContratoPontoConsumoModalidadeQDCPorModalidade(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Date dataInicio, Date dataFim)
					throws NegocioException {

		int qtdDias = Util.intervaloDatas(dataInicio, dataFim);

		ControladorProgramacao controladorProgramacao = (ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		BigDecimal qDCModalidade = BigDecimal.ZERO;

		for (int cont = 0; cont <= qtdDias; cont++) {

			Date data = Util.incrementarDia(dataInicio, cont);
			// setando dataSolicitacao
			DateTime dataSolicitacao = new DateTime(data);

			qDCModalidade = qDCModalidade.add(controladorProgramacao.calcularQDCSolicitacaoConsumo(listaContratoPontoConsumoModalidade,
							dataSolicitacao.toDate()));
		}
		return qDCModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoPontoConsumoModalidade
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumoModalidade obterContratoPontoConsumoModalidade(Long idPontoConsumo, Long idContrato, Long idModalidade)
					throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoPontoConsumoModalidade.class);
		criteria.createAlias(CONTRATOPONTOCONSUMO, CONTRATOPONTOCONSUMO, Criteria.INNER_JOIN);
		criteria.createAlias("contratoPontoConsumo.contrato", PONTO_CONSUMO_CONTRATO, Criteria.INNER_JOIN);
		criteria.createAlias("contratoPontoConsumo.pontoConsumo", PONTO_CONSUMO, Criteria.INNER_JOIN);

		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		if (idModalidade != null) {
			criteria.add(Restrictions.eq("contratoModalidade.chavePrimaria", idModalidade));
		}

		criteria.setMaxResults(1);

		return (ContratoPontoConsumoModalidade) criteria.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoPontoConsumoPenalidade
	 * (java.lang.Long, java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumoPenalidade obterContratoPontoConsumoPenalidade(Long idContratoPontoConsumoModalidade, Long idPenalidade,
					Long idPeriodicidadePenalidade, Date dataInicioApuracao, Date dataFinalApuracao) throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoPontoConsumoPenalidade.class);
		criteria.createAlias(CONSUMO_REFERENCIA, CONSUMO_REFERENCIA, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq("contratoPontoConsumoModalidade.chavePrimaria", idContratoPontoConsumoModalidade));
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_PENALIDADE, idPenalidade));
		criteria.add(Restrictions.eq("periodicidadePenalidade.chavePrimaria", idPeriodicidadePenalidade));

		criteria.setMaxResults(1);
		Collection<ContratoPontoConsumoPenalidade> lista = criteria.list();
		ContratoPontoConsumoPenalidade retorno = null;

		if (lista != null && lista.size() == 1) {
			retorno = lista.iterator().next();
		} else {

			// se data de inicio e fim de apuração não for nula no paramentro, será penalidade TOP que deve checar as vigências.
			if (dataInicioApuracao != null && dataFinalApuracao != null) {
				for (ContratoPontoConsumoPenalidade penalidade : lista) {
					if (dataInicioApuracao.compareTo(penalidade.getDataInicioVigencia()) >= 0
									&& dataFinalApuracao.compareTo(penalidade.getDataFimVigencia()) <= 0) {
						retorno = penalidade;
						break;
					}
				}
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterListaContratoComPenalidades()
	 */
	@Override
	public Collection<Contrato> obterListaContratoComPenalidades() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contrato ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" contrato");
		hql.append(" inner join fetch contrato.situacao situacao ");
		hql.append(" left join fetch contrato.listaContratoPontoConsumo listaContratoPontoConsumo ");
		hql.append(" inner join fetch listaContratoPontoConsumo.listaContratoPontoConsumoModalidade listaContratoPontoConsumoModalidade2 ");
		hql.append(" left join fetch contrato.listaContratoPenalidade listaContratoPenalidade ");
		hql.append(WHERE);
		hql.append(" contrato.habilitado is true ");
		hql.append(" and situacao.faturavel is true ");
		hql.append("  and (contrato.percentualTarifaDoP is not null ");
		hql.append("  or contrato.percentualSobreTariGas is not null ");

		hql.append("  or exists (select contratoPenalidade from ");
		hql.append(getClasseEntidadeContratoPenalidade().getSimpleName());
		hql.append(" contratoPenalidade ");
		hql.append(WHERE);
		hql.append(" contratoPenalidade.contrato.chavePrimaria = contrato.chavePrimaria) ");

		hql.append("  or exists (select contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO);
		hql.append(" inner join  contratoPontoConsumo.listaContratoPontoConsumoModalidade listaContratoPontoConsumoModalidade ");
		hql.append(" inner join  listaContratoPontoConsumoModalidade.listaContratoPontoConsumoPenalidade listaContratoPontoConsumoPenalidade ");
		hql.append(WHERE);
		hql.append(" contratoPontoConsumo.contrato.chavePrimaria = contrato.chavePrimaria)) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarDataVencimento(java.util.Date)
	 */
	@Override
	public void validarDataVencimento(Date dataVencimento) throws NegocioException {

		Date dataAtual = DataUtil.gerarDataHmsZerados(new Date());
		if (dataVencimento.before(dataAtual)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_QUE_ATUAL, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarEncerrarRescindirContrato(java.lang.Long)
	 */
	@Override
	public void validarEncerrarRescindirContrato(Long chaveContrato) throws GGASException {

		Contrato contrato = (Contrato) this.obter(chaveContrato, SITUACAO, "proposta", LISTA_CONTRATO_PONTO_CONSUMO,
						ATRIBUTO_LISTA_CONTRATO_QDC);

		if (!contrato.isHabilitado()) {
			throw new NegocioException(ERRO_NEGOCIO_OPERACAO_INVALIDA_ENCERRAR_CONTRATO_INATIVO, true);
		}

		if (!contrato.getSituacao().isFaturavel()) {
			throw new NegocioException(ERRO_NEGOCIO_ENCERRAR_REINCINDIR_CONTRATO, true);
		}

		Collection<PontoConsumo> pontosConsumo = new ArrayList<>();
		for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
			pontosConsumo.add(contratoPontoConsumo.getPontoConsumo());
		}

		if (!pontosConsumo.isEmpty()) {
			verificarDependenciaLeituraFaturamento(pontosConsumo);
		}
	}

	/**
	 * Realiza a consulta do ContratoPontoConsumo com Contrato mais recente, ordenado
	 * por chave primária do Contrato. Obtém do ContratoPontoConsumo, os seguintes atributos:
	 * {@code chavePrimaria}, {@code periodicidade},{@code acaoAnormalidadeConsumoSemLeitura},
	 * {@code medidaFornecimentoMinMensal}, {@code numeroFatorCorrecao}, {@code unidadePressao},
	 * {@code medidaPressao}, {@code tipoMedicao}, {@code medidaPressaoColetada}.
	 * Obtém do PontoConsumo, os seguintes atributos: {@code chavePrimaria}.Obtém do
	 * Contrato, os seguintes atributos: {@code chavePrimaria},
	 * {@code formaCobranca}. Para o Cliente do Contrato, obtém os seguintes atributos:
	 * {@code chavePrimaria}, {@code nome}.
	 *
	 * @param chavesPontosConsumo
	 * @return
	 */
	@Override
	public Map<Long, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoRecente(Long[] chavesPontosConsumo) {

		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumo = new HashMap<>();
		if (chavesPontosConsumo != null && chavesPontosConsumo.length > 0) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select contratoPonto.chavePrimaria, periodicidade, ");
			hql.append(" acaoAnormalidade, contratoPonto.medidaFornecimentoMinMensal, ");
			hql.append(" contratoPonto.numeroFatorCorrecao, unidadePressao, contratoPonto.medidaPressao, ");
			hql.append(" tipoMedicao, pontoConsumo.chavePrimaria, ");
			hql.append(" contrato.chavePrimaria, formaCobranca, ");
			hql.append(" clienteAssinatura.chavePrimaria, clienteAssinatura.nome, ");
			hql.append(" unidadePressaoColetada, contratoPonto.medidaPressaoColetada, contrato.situacao ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join contratoPonto.contrato contrato ");
			hql.append(" inner join contratoPonto.contrato.clienteAssinatura clienteAssinatura ");
			hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" inner join contratoPonto.pontoConsumo.imovel imovel ");
			hql.append(" left join contratoPonto.unidadePressao unidadePressao ");
			hql.append(" left join contratoPonto.unidadePressaoColetada unidadePressaoColetada ");
			hql.append(" left join contratoPonto.acaoAnormalidadeConsumoSemLeitura acaoAnormalidade ");
			hql.append(" left join contratoPonto.periodicidade periodicidade ");
			hql.append(" left join contratoPonto.tipoMedicao tipoMedicao ");
			hql.append(" left join contrato.formaCobranca formaCobranca ");
			hql.append(WHERE);
			
			Map<String, List<Long>> mapaPropriedades = 
					HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontosConsumo));
			
			hql.append(" and contrato.chavePrimariaPrincipal is null ");
			hql.append(" order by contratoPonto.chavePrimaria desc ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			mapaContratoPontoConsumo.putAll(this.construirMapaContratoPontoConsumo(query.list()));
		}

		return mapaContratoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPontoConsumoPorPontoConsumoRecente(java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumoRecente(Long chavePontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato ");
		hql.append(" inner join fetch contratoPonto.contrato.clienteAssinatura ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel ");
		hql.append(" left join fetch contratoPonto.unidadePressao ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

		hql.append(" order by contratoPonto.chavePrimaria desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(CHAVE_PONTO_CONSUMO, chavePontoConsumo);

		List<ContratoPontoConsumo> listaContatoPontoConsumo = query.list();

		ContratoPontoConsumo retorno = null;
		if (!listaContatoPontoConsumo.isEmpty()) {
			retorno = Util.primeiroElemento(listaContatoPontoConsumo);
		}

		return retorno;
	}


	/**
	 * Realiza a consulta do ContratoPontoConsumo com Contrato mais recente, ordenado
	 * por chave primária do Contrato, obtendo, além dos dados do ContratoPontoConsumo.
	 * o ponto de consumo e o contrato.
	 *
	 * @param chavesPontosConsumo
	 * @return mapa de ContratoPontoConsumo por PontoConsumo
	 */
	@Override
	public Map<PontoConsumo, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoRecente(Collection<PontoConsumo> lista){

		Map<PontoConsumo, ContratoPontoConsumo> mapaContratoPontoConsumo = new HashMap<>();

		if (lista != null && !lista.isEmpty()) {
			Long[] chaves = Util.collectionParaArrayChavesPrimarias(lista);

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo, contratoPonto, contrato ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join  contratoPonto.contrato contrato ");
			hql.append(" inner join  contratoPonto.contrato.situacao situacao");
			hql.append(" inner join  contratoPonto.contrato.clienteAssinatura cliente ");
			hql.append(" inner join  contratoPonto.pontoConsumo pontoConsumo");
			hql.append(" inner join  contratoPonto.pontoConsumo.imovel ");
			hql.append(" left join  contratoPonto.unidadePressao ");
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades =
					HibernateHqlUtil.adicionarClausulaIn(hql, "contratoPonto.pontoConsumo.chavePrimaria", PT_CONS,
							Arrays.asList(chaves));
			hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

			hql.append(" order by contratoPonto.chavePrimaria desc ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			mapaContratoPontoConsumo.putAll(montarMapaContratoPontoConsumo(query.list()));
		}
		return mapaContratoPontoConsumo;

	}

	/**
	 * Constrói um mapa de ContratoPontoConsumo por PontoConsumo obtendo os
	 * objetos de ContratoPontoConsumo, PontoConsumo e Contrato.
	 *
	 * @param list
	 * @return mapa de ContratoPontoConsumo por PontoConsumo
	 */
	private Map<PontoConsumo, ContratoPontoConsumo> montarMapaContratoPontoConsumo(List<Object[]> list) {

		Map<PontoConsumo, ContratoPontoConsumo> mapa = new HashMap<>();
		for (Object[] array : list) {
			PontoConsumo ponto = (PontoConsumo) array[0];
			ContratoPontoConsumo contratoPonto = (ContratoPontoConsumo) array[1];

			Contrato contrato = (Contrato) array[INDICE_CONTRATO];
			contratoPonto.setContrato(contrato);

			if (!mapa.containsKey(ponto)) {
				mapa.put(ponto, contratoPonto);
			}
		}
		return mapa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterContratoPontoConsumo(java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo obterContratoPontoConsumo(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoPontoConsumo.class);
		criteria.createAlias("unidadePressao", "unidadePressao", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadePressaoMinima", "unidadePressaoMinima", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadePressaoMaxima", "unidadePressaoMaxima", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadePressaoLimite", "unidadePressaoLimite", Criteria.LEFT_JOIN);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.LEFT_JOIN);
		criteria.createAlias(PONTO_CONSUMO_CONTRATO, PONTO_CONSUMO_CONTRATO, Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeVazaoInstantanea", "unidadeVazaoInstantanea", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeVazaoMaximaInstantanea", "unidadeVazaoMaximaInstantanea", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeVazaoMinimaInstantanea", "unidadeVazaoMinimaInstantanea", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeFornecimentoMaxDiaria", "unidadeFornecimentoMaxDiaria", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeFornecimentoMinDiaria", "unidadeFornecimentoMinDiaria", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeFornecimentoMinMensal", "unidadeFornecimentoMinMensal", Criteria.LEFT_JOIN);
		criteria.createAlias("unidadeFornecimentoMinAnual", "unidadeFornecimentoMinAnual", Criteria.LEFT_JOIN);
		criteria.createAlias("acaoAnormalidadeConsumoSemLeitura", "acaoAnormalidadeConsumoSemLeitura", Criteria.LEFT_JOIN);
		criteria.createAlias("tipoMedicao", "tipoMedicao", Criteria.LEFT_JOIN);
		criteria.createAlias(REGIME_CONSUMO, REGIME_CONSUMO, Criteria.LEFT_JOIN);
		criteria.createAlias("cep", "cep", Criteria.LEFT_JOIN);
		criteria.createAlias(ATRIBUTO_CONTRATO_COMPRA, ATRIBUTO_CONTRATO_COMPRA, Criteria.LEFT_JOIN);
		criteria.createAlias(ATRIBUTO_PERIODICIDADE, ATRIBUTO_PERIODICIDADE, Criteria.LEFT_JOIN);
		criteria.createAlias("listaContratoPontoConsumoModalidade", "listaContratoPontoConsumoModalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("listaContratoPontoConsumoItemFaturamento", "listaContratoPontoConsumoItemFaturamento", Criteria.LEFT_JOIN);
		criteria.createAlias("listaContratoPontoConsumoPCSAmostragem", "listaContratoPontoConsumoPCSAmostragem", Criteria.LEFT_JOIN);
		criteria.createAlias("listaContratoPontoConsumoPCSIntervalo", "listaContratoPontoConsumoPCSIntervalo", Criteria.LEFT_JOIN);

		criteria.add(Restrictions.idEq(chavePrimaria));

		return (ContratoPontoConsumo) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterSomaQdcContrato(java.lang.Long,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal obterSomaQdcContrato(Long idContrato, Date data) throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoQDC.class);
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		criteria.add(Restrictions.eq("data", data));

		ContratoQDC contratoQDC = (ContratoQDC) criteria.uniqueResult();

		if (contratoQDC != null) {

			return contratoQDC.getMedidaVolume();

		} else {

			return null;

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterSomaQdcContratoPorPeriodo
	 * (java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal obterSomaQdcContratoPorPeriodo(Long idContrato, Date dataInicial, Date dataFinal) throws NegocioException {

		BigDecimal valorTotalQDC = BigDecimal.ZERO;

		int qtdDias = Util.intervaloDatas(dataInicial, dataFinal);

		for (int cont = 0; cont <= qtdDias; cont++) {

			Date data = Util.incrementarDia(dataInicial, cont);
			DateTime dataSolicitacao = new DateTime(data);

			BigDecimal valorQDC = this.obterQdcContratoValidoPorData(dataSolicitacao.toDate(), idContrato);

			valorTotalQDC = valorTotalQDC.add(valorQDC);

		}

		return valorTotalQDC;

	}

	/**
	 * Método para obter o QDC válido para o
	 * contrato e data.
	 *
	 * @param data
	 *            the data
	 * @param idContrato
	 *            the id contrato
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public BigDecimal obterQdcContratoValidoPorData(Date data, long idContrato) throws NegocioException {

		BigDecimal qdc = BigDecimal.ZERO;

		Criteria criteria = this.createCriteria(ContratoQDC.class);
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		criteria.add(Restrictions.le("data", data));
		criteria.addOrder(Order.desc("data"));
		criteria.setMaxResults(1);

		ContratoQDC contratoQDC = (ContratoQDC) criteria.uniqueResult();

		if (contratoQDC != null) {
			qdc = contratoQDC.getMedidaVolume();
		}

		return qdc;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterDataFinalContrato(br
	 * .com.ggas.contrato.contrato.Contrato,
	 * br.com
	 * .ggas.contrato.contrato.ContratoPontoConsumo
	 * , java.util.Date)
	 */
	@Override
	public Date obterDataFinalContrato(Contrato contrato, ContratoPontoConsumo contratoPontoConsumo, Date dataProgramacao)
					throws NegocioException {

		Date dataFinalContrato = null;

		if (contrato.getDataRecisao() != null) {

			dataFinalContrato = contrato.getDataRecisao();

		} else {

			if (contrato.getIndicadorAnoContratual()) {

				Calendar calDataFinalContrato = Calendar.getInstance();
				calDataFinalContrato.set(Calendar.MONTH, Calendar.DECEMBER);
				calDataFinalContrato.set(Calendar.DAY_OF_MONTH, calDataFinalContrato.getMaximum(Calendar.DAY_OF_MONTH));
				dataFinalContrato = calDataFinalContrato.getTime();

			} else {

				Date dataInicioContrato = this.obterDataInicialContrato(contrato, contratoPontoConsumo, dataProgramacao);

				Calendar calDataInicioContrato = Calendar.getInstance();
				calDataInicioContrato.setTime(dataInicioContrato);
				calDataInicioContrato.add(Calendar.MONTH, MESES_ANO);
				dataFinalContrato = calDataInicioContrato.getTime();

			}
		}

		return dataFinalContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #obterDataInicialContrato
	 * (br.com.ggas.contrato.contrato.Contrato,
	 * br.
	 * com.ggas.contrato.contrato.ContratoPontoConsumo
	 * , java.util.Date)
	 */
	@Override
	public Date obterDataInicialContrato(Contrato contrato, ContratoPontoConsumo contratoPontoConsumo, Date dataProgramacao)
					throws NegocioException {

		DateTime dataInicialAno = new DateTime(contrato.getDataAssinatura());
		DateTime dataFinalAno = new DateTime(dataProgramacao);
		Integer anoAssinatura = dataInicialAno.getYear();
		Integer anoFinal = dataFinalAno.getYear();
		Date dataInicial = null;

		Date dataFinalTeste = contratoPontoConsumo.getDataConsumoTesteExcedido();
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		String parametroExpurgarPeriodoTeste = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo("EXPURGAR_PERIODO_TESTE_APURACAO");

		boolean isExpurgarPeriodoTeste;
		if(BOOLEAN_VERDADE_1.equals(parametroExpurgarPeriodoTeste)){
			isExpurgarPeriodoTeste = true;
		}else{
			isExpurgarPeriodoTeste = false;
		}

		// ANO MAIUSCULO
		if (contrato.getIndicadorAnoContratual()) {

			// Se o ano de assinatura for igual ao
			// ano final.
			// se o ano da assinatura e o ano da
			// data final forem iguais, é o
			// mesmo ano( dt assinatura=
			// 01/01/20110 final= 31/12/2011)
			if (anoAssinatura.compareTo(anoFinal) == 0) {

				dataInicial = dataInicialAno.toDate();

				// Se o ano de assinatura for
				// menor que o ano final
			} else if (anoAssinatura.compareTo(anoFinal) < 0) {

				// Criar data com primeiro dia do
				// ano.
				dataInicialAno = dataInicialAno.withYear(dataFinalAno.getYear());
				dataInicialAno = dataInicialAno.withMonthOfYear(1);
				dataInicialAno = dataInicialAno.withDayOfMonth(1);
				dataInicialAno = dataInicialAno.withHourOfDay(0);
				dataInicialAno = dataInicialAno.withMinuteOfHour(0);
				dataInicialAno = dataInicialAno.withSecondOfMinute(0);
				dataInicialAno = dataInicialAno.withMillisOfSecond(0);

				// Se o contrato foi assinado
				// depois do inicio do ano (01
				// jan),
				// usar data assinatura, senão, 01
				// jan.
				if (contrato.getDataAssinatura().after(dataInicialAno.toDate())) {
					dataInicial = contrato.getDataAssinatura();
				} else {
					dataInicial = dataInicialAno.toDate();
				}
			}

			// ano minúsculo
		} else {

			if (anoAssinatura.compareTo(anoFinal) == 0) {

				dataInicial = dataInicialAno.toDate();

			} else if (anoAssinatura.compareTo(anoFinal) < 0) {

				dataInicialAno = dataInicialAno.withYear(anoFinal - 1);
				dataInicialAno = dataInicialAno.withMonthOfYear(dataInicialAno.getMonthOfYear());
				dataInicialAno = dataInicialAno.withDayOfMonth(dataInicialAno.getDayOfMonth());
				dataInicialAno = dataInicialAno.withHourOfDay(0);
				dataInicialAno = dataInicialAno.withMinuteOfHour(0);
				dataInicialAno = dataInicialAno.withSecondOfMinute(0);
				dataInicialAno = dataInicialAno.withMillisOfSecond(0);
				dataInicial = dataInicialAno.toDate();

			}
		}

		int intervaloMeses = Util.intervaloMeses(contrato.getDataAssinatura(), dataInicial);

		if (intervaloMeses <= MESES_ANO && isExpurgarPeriodoTeste && dataFinalTeste != null && (dataFinalTeste.compareTo(dataInicial) > 0)) {

			dataInicial = dataFinalTeste;
		}

		return dataInicial;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #listarSituacaoContrato()
	 */
	@Override
	public Collection<SituacaoContrato> listarSituacaoContrato() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeSituacaoContrato());
		criteria.addOrder(Order.asc("descricao"));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPorNumero(java.lang.String)
	 */
	@Override
	public Collection<Contrato> consultarContratoPorNumero(String numeroContrato) throws NegocioException {

		Criteria criteria = getCriteria();

		criteria.add(Restrictions.like(NUMERO_COMPLETO_CONTRATO, "%" + numeroContrato + "%"));
		criteria.add(Restrictions.eq(HABILITADO, true));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPorArrecadadorConvenioContrato(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Contrato> consultarContratoPorArrecadadorConvenioContrato(Long idArrecadadorConvenioContrato) throws NegocioException {

		Criteria criteria = createCriteria(Contrato.class);
		criteria.add(Restrictions.eq("arrecadadorContratoConvenio.chavePrimaria", idArrecadadorConvenioContrato));
		criteria.add(Restrictions.eq(HABILITADO, true));

		return criteria.list();
	}

	/**
	 * Inserir contrato cancelado encerrado.
	 *
	 * @param contrato the contrato
	 * @return the long
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException A ConcorrenciaException
	 */
	public long inserirContratoCanceladoEncerrado(Contrato contrato) throws NegocioException, ConcorrenciaException {

		Long chavePrimaria = null;

		contrato.setHabilitado(Boolean.FALSE);
		validarDadosEntidade(contrato);
		preInsercao(contrato);
		contrato.setUltimaAlteracao(Calendar.getInstance().getTime());
		chavePrimaria = inserirContrato(contrato);
		contrato.setChavePrimaria(chavePrimaria);
		posInsercao(contrato);

		return chavePrimaria.longValue();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#verificarDependenciaLeituraFaturamento(java.util.Collection)
	 */
	@Override
	public void verificarDependenciaLeituraFaturamento(Collection<PontoConsumo> chavesPrimariasPontoConsumo) throws NegocioException {

		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		
		ParametroSistema parametroConsideAnomaliaFaturamento = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_CONSIDERAR_ANOMALIA_FATURAMENTO_CONTRATO);

		String sVirgula = "";
		String listaPontoConsumoPendentes = "";
		for (PontoConsumo pontoConsumo : chavesPrimariasPontoConsumo) {
			if ((!controladorHistoricoConsumo.consultarPendenciaFaturamentoAnormalidade(pontoConsumo.getChavePrimaria()).isEmpty())
							|| (!controladorHistoricoConsumo.consultarPendenciaFaturamentoAnormalidadeMedicao(
											pontoConsumo.getChavePrimaria()).isEmpty())) {
				listaPontoConsumoPendentes = listaPontoConsumoPendentes.concat(sVirgula).concat(pontoConsumo.getDescricao());
				sVirgula = ",";

			}
		}

		if ((listaPontoConsumoPendentes != null) && (!listaPontoConsumoPendentes.isEmpty())
				&& (!"".equals(listaPontoConsumoPendentes))
				&& parametroConsideAnomaliaFaturamento.getValorInteger() == 1) {
			throw new NegocioException(ERRO_NEGOCIO_ANOMALIA_LEITURA_FATURAMENTO,
					listaPontoConsumoPendentes.toString());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPontoConsumo(java.util.Map)
	 */
	@Override
	public Collection<ContratoPontoConsumo> consultarContratoPontoConsumo(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContratoPontoConsumo());

		if (filtro != null) {
			Long idPeriodicidade = (Long) filtro.get(ID_PERIODICIDADE);
			if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
				criteria.add(Restrictions.eq("periodicidade.chavePrimaria", idPeriodicidade));
			}

		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoFaturavelPorPontoConsumo(java.lang.Long, java.lang.Boolean)
	 */
	@Override
	public ContratoPontoConsumo obterContratoFaturavelPorPontoConsumo(Long idPontoConsumo, Boolean faturavel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.contrato contrato");
		hql.append(" inner join fetch contratoPonto.periodicidade periodicidade ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and contrato.chavePrimariaPrincipal is null ");
		hql.append(" and contrato.situacao.faturavel = :faturavel ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		query.setBoolean("faturavel", faturavel);
		
		if(query.list() !=null && query.list().size()>1){
			return (ContratoPontoConsumo) query.list().get(0);
		}
		return (ContratoPontoConsumo) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPorPontoConsumoEPorDataRecisao(br.com.ggas.cadastro.imovel.
	 * PontoConsumo)
	 */
	@Override
	public Collection<Contrato> consultarContratoPorPontoConsumoEPorDataRecisao(PontoConsumo pontoConsumo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContratoPontoConsumo());
		criteria.createAlias(PONTO_CONSUMO_CONTRATO, PONTO_CONSUMO_CONTRATO, Criteria.LEFT_JOIN);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.isNotNull(ATRIBUTO_CONTRATO_DATA_RECISAO));
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria()));
		criteria.addOrder(Order.asc(ATRIBUTO_CONTRATO_DATA_RECISAO));

		Collection<ContratoPontoConsumo> listarContratoPorPontoConsumo = criteria.list();
		Collection<Contrato> listarContrato = new ArrayList<>();

		if (listarContratoPorPontoConsumo != null && !listarContratoPorPontoConsumo.isEmpty()) {
			for (ContratoPontoConsumo contratoPontoConsumo : listarContratoPorPontoConsumo) {

				listarContrato.add(contratoPontoConsumo.getContrato());
			}
		}

		return listarContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarContratoAtivoPorDataLeitura(java.util.Date,
	 * br.com.ggas.contrato.contrato.ContratoPontoConsumo, br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public Map<String, Object> validarContratoAtivoPorDataLeitura(Date dataLeitura, ContratoPontoConsumo contratoPontoConsumo,
					PontoConsumo pontoConsumo) throws NegocioException {

		Boolean possuiContrato = Boolean.FALSE;
		Map<String, Object> mapaContratoAtivo = new LinkedHashMap<>();

		// Verifica se o contrato ativo atual estava ativo quando a leitura foi realizada
		if (contratoPontoConsumo != null && dataLeitura.compareTo(contratoPontoConsumo.getContrato().getDataAssinatura()) >= 0) {

			mapaContratoAtivo.put(Constantes.HORA_INICIAL_CONTRATO, contratoPontoConsumo.getNumeroHoraInicial());
			possuiContrato = Boolean.TRUE;
			mapaContratoAtivo.put(Constantes.POSSUI_CONTRATO, possuiContrato);
			mapaContratoAtivo.put(Constantes.PERIODICIDADE_CONTRATO, contratoPontoConsumo.getPeriodicidade());
		}

		// Caso não encontre nenhum contrato ativo atual verifica se existia algum contrato ativo na época da leitura
		if (!possuiContrato) {
			ContratoPontoConsumo contratoPonto = this.verificarContratoAtivoEpocaLeitura(pontoConsumo, dataLeitura);
			if (contratoPonto != null) {
				mapaContratoAtivo.put(Constantes.HORA_INICIAL_CONTRATO, contratoPonto.getNumeroHoraInicial());
				possuiContrato = Boolean.TRUE;
				mapaContratoAtivo.put(Constantes.POSSUI_CONTRATO, possuiContrato);
				mapaContratoAtivo.put(Constantes.PERIODICIDADE_CONTRATO, contratoPonto.getPeriodicidade());
			}

		}

		return mapaContratoAtivo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterPeriodicidadePorPontoConsumo(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Date)
	 */
	@Override
	public Periodicidade obterPeriodicidadePorPontoConsumo(PontoConsumo pontoConsumo, Date dataRealizacaoLeitura) throws NegocioException {

		Periodicidade periodicidade = null;

		ContratoPontoConsumo contratoPontoConsumo = this.obterContratoFaturavelPorPontoConsumo(pontoConsumo.getChavePrimaria(),
						Boolean.TRUE);

		Map<String, Object> mapaContratoAtivo = null;
		Boolean possuiContratoFaturavel = null;
		// Alteração para validar o ciclo das leituras diarias. Caso seja chamado a partir do salvarSupervisorioMedicaoDiaria,
		// irá trazer a data de leitura. Caso seja chamado a partir do pesquisar, irá retornar a periodicidade do contrato ativo,
		// se não tiver, segue as opções do fluxo original.
		if (dataRealizacaoLeitura != null) {
			mapaContratoAtivo = this.validarContratoAtivoPorDataLeitura(dataRealizacaoLeitura, contratoPontoConsumo, pontoConsumo);

			possuiContratoFaturavel = (Boolean) mapaContratoAtivo.get(Constantes.POSSUI_CONTRATO);
		} else if (contratoPontoConsumo != null && contratoPontoConsumo.getPeriodicidade() != null) {
			return contratoPontoConsumo.getPeriodicidade();
		}

		// obtém a hora inicial e a periodicidade do contrato se tiver
		if (possuiContratoFaturavel != null && possuiContratoFaturavel && mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO) != null) {
			// Recupera a periodicidade de faturamento do contrato ativo
			Periodicidade periodicidadeAux = (Periodicidade) mapaContratoAtivo.get(Constantes.PERIODICIDADE_CONTRATO);

			periodicidade = this.preencherPeriodicidade(periodicidadeAux);

		} else {
			// Recupera a periodicidade de faturamento do ramo de atividade se o PC não tiver contrato ativo
			if (pontoConsumo.getRamoAtividade().getPeriodicidade() != null) {
				periodicidade = pontoConsumo.getRamoAtividade().getPeriodicidade();

			} else if (pontoConsumo.getSegmento().getPeriodicidade() != null) {

				// Recupera a periodicidade de faturamento do segmento se o ramo de atividade não for informado
				periodicidade = pontoConsumo.getSegmento().getPeriodicidade();
			}
		}

		return periodicidade;
	}

	/**
	 * Preencher periodicidade.
	 *
	 * @param periodicidadeAux
	 *            the periodicidade aux
	 * @return the periodicidade
	 */
	private Periodicidade preencherPeriodicidade(Periodicidade periodicidadeAux) {

		Periodicidade periodicidade = (Periodicidade) this.criarPeriodicidade();
		periodicidade.setDescricao(periodicidadeAux.getDescricao());
		periodicidade.setDescricaoAbreviada(periodicidadeAux.getDescricaoAbreviada());
		periodicidade.setQuantidadeDias(periodicidadeAux.getQuantidadeDias());
		periodicidade.setQuantidadeCiclo(periodicidadeAux.getQuantidadeCiclo());
		periodicidade.setNumeroMaximoDiasCiclo(periodicidadeAux.getNumeroMaximoDiasCiclo());
		periodicidade.setNumeroMinimoDiasCiclo(periodicidadeAux.getNumeroMinimoDiasCiclo());

		return periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPorNumero(java.lang.Integer)
	 */
	@Override
	public Collection<Contrato> listarContratoPorNumero(Integer numero) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.add(Restrictions.eq(NUMERO, numero));

		criteria.addOrder(Order.desc("ultimaAlteracao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPontoConsumoItemFaturamentoPorContratoPontoConsumo(long)
	 */
	@Override
	public Collection<ContratoPontoConsumoItemFaturamento> listarContratoPontoConsumoItemFaturamentoPorContratoPontoConsumo(
					long idContratoPontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" contratoPontoConsumoItemFaturamento ");
		hql.append(" inner join fetch contratoPontoConsumoItemFaturamento.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join fetch contratoPontoConsumoItemFaturamento.tarifa tarifa ");
		hql.append(" inner join fetch contratoPontoConsumoItemFaturamento.itemFatura itemFatura ");
		hql.append(" left join fetch contratoPontoConsumoItemFaturamento.opcaoFaseReferencia opcaoFaseReferencia ");
		hql.append(" left join fetch contratoPontoConsumoItemFaturamento.faseReferencia faseReferencia ");
		hql.append(WHERE);
		hql.append(" contratoPontoConsumo.chavePrimaria = :idContratoPontoConsumo ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_CONTRATO_PONTO_CONSUMO, idContratoPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPontoConsumoDescricao(java.lang.Long)
	 */
	@Override
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumoDescricao(Long chavePrimariaContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO2);
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" where contratoPontoConsumo.contrato.chavePrimaria = :chavePrimariaContrato ");
		hql.append(" order by pontoConsumo.descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO, chavePrimariaContrato);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPontoConsumoSituacaoInativo(java.lang.Long, java.lang.String)
	 */
	@Override
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumoSituacaoInativo(Long chavePrimariaContrato) {

		StringBuilder hql = new StringBuilder()
				.append(" select distinct contratoPontoConsumo from ")
				.append(getClasseEntidadeContratoPontoConsumo().getSimpleName())
				.append(" contratoPontoConsumo inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo"
						+ " where contratoPontoConsumo.contrato.chavePrimaria = :chavePrimariaContrato"
						+ " and contratoPontoConsumo.contrato.chavePrimariaPrincipal is null"
						+ " and ("
							+ " upper(pontoConsumo.situacaoConsumo.descricao) in :bloqueado "
							+ " or pontoConsumo.instalacaoMedidor is null and upper(pontoConsumo.situacaoConsumo.descricao) = :ativo"
						+ ")");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString())
				.setLong(CHAVE_PRIMARIA_CONTRATO, chavePrimariaContrato)
				.setParameterList("bloqueado", Arrays.asList(SituacaoConsumo.BLOQUEADO.toUpperCase(), SituacaoConsumo.SUSPENSO.toUpperCase()))
				.setString("ativo", SituacaoConsumo.ATIVO.toUpperCase())
				.list();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumoPorSituacao(Long chavePrimariaContrato, List<String> tipos) {
		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO_CONSUMO2);
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo");
		hql.append(" where contratoPontoConsumo.contrato.chavePrimaria = :chavePrimariaContrato ");
		hql.append(" and contratoPontoConsumo.contrato.chavePrimariaPrincipal is null ");
		hql.append("and upper(pontoConsumo.situacaoConsumo.descricao) in :situacaoConsumo");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA_CONTRATO, chavePrimariaContrato);
		query.setParameterList("situacaoConsumo", tipos);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#salvarMigracaoModeloContrato(java.util.Map, java.util.Map, java.util.Map,
	 * java.util.Map,
	 * br.com.ggas.contrato.contrato.ModeloContrato)
	 */
	@Override
	public String salvarMigracaoModeloContrato(
					Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao,
					Map<String, Collection<ModeloAtributo>> mapaAtributosAdicionados,
					Map<String, Collection<ModeloAtributo>> mapaAtributosRemovidos,
					Map<String, Collection<ModeloAtributo>> mapaAtributosComuns,
					ModeloContrato novoModeloContrato)
									throws GGASException {

		Map<Contrato, Collection<ModeloAtributo>> mapaContratosComErro =
						new HashMap<>();

		int quantidadeTotalContratos = this.removerContratosImpedidosMigracao(
						mapaContratosComErro, mapaContratoMigracao,
						mapaAtributosAdicionados, novoModeloContrato);

		this.removerContratosImpedidosMigracao(mapaContratosComErro,
						mapaContratoMigracao, mapaAtributosComuns, novoModeloContrato);

		if (mapaContratoMigracao.entrySet().isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_MIGRAR_CONTRATO_SITUACAO_ENCERRADO_RESCINDIDO);
		}
		
		for (Entry<Pair<Long, String>, Collection<Contrato>> entry : mapaContratoMigracao.entrySet()) {

			String descricaoModeloContrato = novoModeloContrato.getDescricao(true);
			Collection<Contrato> contratos = entry.getValue();

			for (Contrato contrato : contratos) {

				if (!mapaContratosComErro.containsKey(contrato)) {

					Contrato contratoAlterado = this.obterContratoListasCarregadas(
									contrato.getChavePrimaria(), null);
					this.getSessionFactory().getCurrentSession().clear();

					Collection<ModeloAtributo> listaAtributosAdicionados =
									mapaAtributosAdicionados.get(descricaoModeloContrato);

					manipularAtributosAlteradosMigracaoContrato(
									contratoAlterado, listaAtributosAdicionados, false);

					Collection<ModeloAtributo> listaAtributosComuns =
									mapaAtributosComuns.get(descricaoModeloContrato);

					manipularAtributosAlteradosMigracaoContrato(
									contratoAlterado, listaAtributosComuns, false);

					Collection<ModeloAtributo> listaAtributosRemovidos =
									mapaAtributosRemovidos.get(descricaoModeloContrato);

					manipularAtributosAlteradosMigracaoContrato(
									contratoAlterado, listaAtributosRemovidos, true);
					contratoAlterado.setModeloContrato(novoModeloContrato);

					contratoAlterado.setChavePrimariaPai(contrato.getChavePrimaria());
					this.removerReferenciaListasContrato(contratoAlterado);

					if (contratoAlterado.getSituacao().getChavePrimaria() == SituacaoContrato.ENCERRADO
							|| contratoAlterado.getSituacao().getChavePrimaria() == SituacaoContrato.RESCINDIDO) {
			 			throw new NegocioException("Não é possivel migrar contrato com situação ENCERRADO/RESCINDIDO.");
				 	}else{
				 		this.atualizarContrato(contratoAlterado, contrato, true);
			 		}
					this.getSessionFactory().getCurrentSession().clear();
				}
			}
		}

		return this.carregarLogErroMigracao(novoModeloContrato, quantidadeTotalContratos, mapaContratosComErro);
	}

	/**
	 * Carregar log erro migracao.
	 *
	 * @param novoModeloContrato
	 *            the novo modelo contrato
	 * @param quantidadeTotalContratos
	 *            the quantidade total contratos
	 * @param mapaContratosComErro
	 *            the mapa contratos com erro
	 * @return the string
	 */
	private String carregarLogErroMigracao(
					ModeloContrato novoModeloContrato, int quantidadeTotalContratos,
					Map<Contrato, Collection<ModeloAtributo>> mapaContratosComErro) {

		StringBuilder logErroMigracao = new StringBuilder();

		int quantidadeContratosMigradosComErro =
						mapaContratosComErro.size();

		int quantidadeContratosMigradosComSucesso =
						quantidadeTotalContratos - quantidadeContratosMigradosComErro;

		String novoModelo = novoModeloContrato.getDescricao();

		String comErroSemErro = "";
		if (quantidadeContratosMigradosComErro == 0) {
			comErroSemErro = "sem erro.";
		} else {
			comErroSemErro = "com erro.";
		}

		logErroMigracao.append("Migração do(s) Contrato(s) realizada ");
		logErroMigracao.append(comErroSemErro);
		logErroMigracao.append("\r\n\r\nNovo Modelo: ");
		logErroMigracao.append(novoModelo);
		logErroMigracao.append("\r\n\r\nQuantidade de Contratos Selecionados para Migração: ");
		logErroMigracao.append(quantidadeTotalContratos);
		logErroMigracao.append("\r\nQuantidade de Contratos Migrados com Sucesso: ");
		logErroMigracao.append(quantidadeContratosMigradosComSucesso);
		logErroMigracao.append("\r\nQuantidade de Contratos Não Migrados: ");
		logErroMigracao.append(quantidadeContratosMigradosComErro);

		if (quantidadeContratosMigradosComErro > 0) {
			logErroMigracao.append("\r\n\r\n*Campo obrigatório sem valor padrão não pode ser migrado.");
			logErroMigracao.append("\r\nLista dos Contratos e Campos com erro na migração:");

			for (Entry<Contrato, Collection<ModeloAtributo>> entry : mapaContratosComErro.entrySet()) {

				Contrato contrato = entry.getKey();
				Collection<ModeloAtributo> atributos = entry.getValue();
				String numeroContrato = contrato.getNumeroCompletoContrato();

				logErroMigracao.append("\r\n\r\nContrato: ");
				logErroMigracao.append(numeroContrato);

				for (ModeloAtributo modeloAtributo : atributos) {

					AbaAtributo atributo = modeloAtributo.getAbaAtributo();
					logErroMigracao.append(montarLogErroAbaAtributo(atributo));

				}

			}
		}

		return logErroMigracao.toString();

	}

	/**
	 * Montar log erro aba atributo.
	 *
	 * @param atributo the atributo
	 * @return the string
	 */
	private String montarLogErroAbaAtributo(AbaAtributo atributo) {

		StringBuilder sb = new StringBuilder();
		sb.append(Constantes.STRING_QUEBRA_LINHA_TABULACAO);
		sb.append(atributo.getAba().getDescricao());
		sb.append(Constantes.STRING_HIFEN_ESPACO);
		sb.append(atributo.getDescricao());

		return sb.toString();

	}

	/**
	 * Remover contratos impedidos migracao.
	 *
	 * @param mapaContratosComErro
	 * @param mapaContratoMigracao
	 * @param mapaAtributos
	 * @param novoModeloContrato
	 * @return
	 * @throws GGASException
	 */
	private int removerContratosImpedidosMigracao(
					Map<Contrato, Collection<ModeloAtributo>> mapaContratosComErro,
					Map<Pair<Long, String>, Collection<Contrato>> mapaContratoMigracao,
					Map<String, Collection<ModeloAtributo>> mapaAtributos, ModeloContrato novoModeloContrato) throws GGASException {

		int quantidadeContratos = 0;

		for (Entry<Pair<Long, String>, Collection<Contrato>> entry : mapaContratoMigracao.entrySet()) {
			Collection<Contrato> contratos = entry.getValue();

			Collection<ModeloAtributo> listaAtributosContrato = new ArrayList<>();
			if (mapaAtributos.get(novoModeloContrato.getDescricao(true)) != null) {
				listaAtributosContrato.addAll(mapaAtributos.get(novoModeloContrato.getDescricao(true)));
			}
			Contrato contrato = null;
			for (Contrato contratoLista : contratos) {
				contrato = this.obterContratoListasCarregadas(contratoLista.getChavePrimaria(), null);

				Collection<ModeloAtributo> listaAtributosContratoComErro = new ArrayList<>();

				for (ModeloAtributo modeloAtributo : listaAtributosContrato) {

					boolean isAtributoComErro = validarAtributo(contrato, modeloAtributo);

					this.carregarListaAtributosComErro(
									listaAtributosContratoComErro, modeloAtributo, isAtributoComErro);

				}

				if (!listaAtributosContratoComErro.isEmpty()) {
					// carrega o grupo de contratos que estiverem com os campos obrigatórios sem valor padrão
					mapaContratosComErro.put(contrato, listaAtributosContratoComErro);
				}

				quantidadeContratos++;

			}

		}

		return quantidadeContratos;
	}

	private boolean validarAtributo(Contrato contrato,
					ModeloAtributo modeloAtributo) throws GGASException {

		boolean isAtributoComErro = false;
		boolean obrigatorio = modeloAtributo.isObrigatorio();
		boolean padraoNulo = modeloAtributo.isPadraoNulo();

		if (obrigatorio && padraoNulo) {
			String nomeCampo = modeloAtributo.getAbaAtributo().getNomeColuna();
			String valorPadrao = modeloAtributo.getPadrao();

			isAtributoComErro = popularDadosAbaGeral(
							contrato, nomeCampo, valorPadrao, true, isAtributoComErro);

			for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {

				isAtributoComErro = popularDadosAbaPrincipal(contratoPontoConsumo,
								nomeCampo, valorPadrao, true, isAtributoComErro);

				isAtributoComErro = popularDadosAbaDadosTecnicos(contratoPontoConsumo,
								nomeCampo, valorPadrao, true, isAtributoComErro);

				isAtributoComErro = popularDadosAbaConsumo(contratoPontoConsumo,
								nomeCampo, valorPadrao, true, isAtributoComErro, modeloAtributo);

				isAtributoComErro = popularDadosAbaFaturamento(contratoPontoConsumo,
								nomeCampo, valorPadrao, true, isAtributoComErro);

				isAtributoComErro = popularDadosAbaModalidades(contratoPontoConsumo,
								nomeCampo, valorPadrao, true, isAtributoComErro);

			}
		}
		return isAtributoComErro;
	}

	/**
	 * Carregar lista atributos com erro.
	 *
	 * @param listaAtributosContratoComErro
	 *            the lista atributos contrato com erro
	 * @param modeloAtributo
	 *            the modelo atributo
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 */
	private void carregarListaAtributosComErro(Collection<ModeloAtributo> listaAtributosContratoComErro, ModeloAtributo modeloAtributo,
					boolean isAtributoComErro) {

		if (isAtributoComErro) {
			listaAtributosContratoComErro.add(modeloAtributo);
		}
	}

	/**
	 * Remover referencia listas contrato.
	 *
	 * @param contrato
	 *            the contrato
	 */
	private void removerReferenciaListasContrato(Contrato contrato) {

		removerReferenciaListasContratoInformacoesBasicas(contrato);
		removerReferenciaListasContratoPontoConsumo(contrato);

	}

	/**
	 * Remover referencia listas contrato ponto consumo.
	 *
	 * @param contrato the contrato
	 */
	private void removerReferenciaListasContratoPontoConsumo(Contrato contrato) {

		Collection<ContratoPontoConsumo> contratosPontoConsumo = new HashSet<>();

		contratosPontoConsumo.addAll(contrato.getListaContratoPontoConsumo());
		contrato.setListaContratoPontoConsumo(null);
		contrato.setListaContratoPontoConsumo(contratosPontoConsumo);

		for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
			contratoPontoConsumo.setChavePrimaria(0);

			Collection<ContratoPontoConsumoItemFaturamento> contratosPontoConsumoItemFaturamento =
							new HashSet<>();

			contratosPontoConsumoItemFaturamento.addAll(contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento());
			contratoPontoConsumo.setListaContratoPontoConsumoItemFaturamento(null);
			contratoPontoConsumo.setListaContratoPontoConsumoItemFaturamento(contratosPontoConsumoItemFaturamento);

			for (ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento : contratoPontoConsumo
							.getListaContratoPontoConsumoItemFaturamento()) {
				contratoPontoConsumoItemFaturamento.setChavePrimaria(0);
			}

			Collection<ContratoPontoConsumoPCSAmostragem> contratosPontoConsumoPCSAmostragem = new HashSet<>();

			contratosPontoConsumoPCSAmostragem.addAll(contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem());
			contratoPontoConsumo.setListaContratoPontoConsumoPCSAmostragem(null);
			contratoPontoConsumo.setListaContratoPontoConsumoPCSAmostragem(contratosPontoConsumoPCSAmostragem);

			for (ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragem : contratoPontoConsumo
							.getListaContratoPontoConsumoPCSAmostragem()) {
				contratoPontoConsumoPCSAmostragem.setChavePrimaria(0);
			}

			Collection<ContratoPontoConsumoPCSIntervalo> contratosPontoConsumoPCSIntervalo = new HashSet<>();

			contratosPontoConsumoPCSIntervalo.addAll(contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo());
			contratoPontoConsumo.setListaContratoPontoConsumoPCSIntervalo(null);
			contratoPontoConsumo.setListaContratoPontoConsumoPCSIntervalo(contratosPontoConsumoPCSIntervalo);

			for (ContratoPontoConsumoPCSIntervalo contratoPontoConsumoPCSIntervalo : contratoPontoConsumo
							.getListaContratoPontoConsumoPCSIntervalo()) {
				contratoPontoConsumoPCSIntervalo.setChavePrimaria(0);
			}

			removerReferenciaListasContratoPontoConsumoModalidade(contratoPontoConsumo);

		}
	}

	/**
	 * Remover referencia listas contrato ponto consumo modalidade.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 */
	private void removerReferenciaListasContratoPontoConsumoModalidade(ContratoPontoConsumo contratoPontoConsumo) {

		Collection<ContratoPontoConsumoModalidade> contratosPontoConsumoModalidade =
						new HashSet<>();

		contratosPontoConsumoModalidade.addAll(contratoPontoConsumo.getListaContratoPontoConsumoModalidade());
		contratoPontoConsumo.setListaContratoPontoConsumoModalidade(null);
		contratoPontoConsumo.setListaContratoPontoConsumoModalidade(contratosPontoConsumoModalidade);

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumo
						.getListaContratoPontoConsumoModalidade()) {
			contratoPontoConsumoModalidade.setChavePrimaria(0);

			Collection<ContratoPontoConsumoPenalidade> contratosPontoConsumoPenalidade = new HashSet<>();

			contratosPontoConsumoPenalidade.addAll(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade());
			contratoPontoConsumoModalidade.setListaContratoPontoConsumoPenalidade(null);
			contratoPontoConsumoModalidade.setListaContratoPontoConsumoPenalidade(contratosPontoConsumoPenalidade);

			for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : contratoPontoConsumoModalidade
							.getListaContratoPontoConsumoPenalidade()) {
				contratoPontoConsumoPenalidade.setChavePrimaria(0);
			}

			Collection<ContratoPontoConsumoModalidadeQDC> contratosPontoConsumoModalidadeQDC = new HashSet<>();

			contratosPontoConsumoModalidadeQDC.addAll(contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC());
			contratoPontoConsumoModalidade.setListaContratoPontoConsumoModalidadeQDC(null);
			contratoPontoConsumoModalidade.setListaContratoPontoConsumoModalidadeQDC(contratosPontoConsumoModalidadeQDC);

			for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : contratoPontoConsumoModalidade
							.getListaContratoPontoConsumoModalidadeQDC()) {
				contratoPontoConsumoModalidadeQDC.setChavePrimaria(0);
			}
		}
	}

	/**
	 * Remover referencia listas contrato informacoes basicas.
	 *
	 * @param contrato the contrato
	 */
	private void removerReferenciaListasContratoInformacoesBasicas(Contrato contrato) {

		Collection<Fatura> faturas = new HashSet<>();

		faturas.addAll(contrato.getListaFatura());
		contrato.setListaFatura(null);
		contrato.setListaFatura(faturas);

		for (Fatura fatura : contrato.getListaFatura()) {
			fatura.setChavePrimaria(0);
		}

		Collection<ContratoCliente> contratosCliente = new HashSet<>();

		contratosCliente.addAll(contrato.getListaContratoCliente());
		contrato.setListaContratoCliente(null);
		contrato.setListaContratoCliente(contratosCliente);

		for (ContratoCliente contratoCliente : contrato.getListaContratoCliente()) {
			contratoCliente.setChavePrimaria(0);
		}

		Collection<ContratoQDC> listaContratoQDC = new HashSet<>();

		listaContratoQDC.addAll(contrato.getListaContratoQDC());
		contrato.setListaContratoQDC(null);
		contrato.setListaContratoQDC(listaContratoQDC);

		for (ContratoQDC contratoQDC : contrato.getListaContratoQDC()) {
			contratoQDC.setChavePrimaria(0);
		}

		Collection<ContratoPenalidade> listaContratoPenalidade = new HashSet<>();

		listaContratoPenalidade.addAll(contrato.getListaContratoPenalidade());
		contrato.setListaContratoPenalidade(null);
		contrato.setListaContratoPenalidade(listaContratoPenalidade);

		for (ContratoPenalidade contratoPenalidade : contrato.getListaContratoPenalidade()) {
			contratoPenalidade.setChavePrimaria(0);
		}
	}

	/**
	 * Manipular atributos alterados migracao contrato.
	 *
	 * @param contrato
	 *            the contrato
	 * @param listaAtributos
	 *            the lista atributos
	 * @param isRemoverCampo
	 *            the is remover campo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void manipularAtributosAlteradosMigracaoContrato(
					Contrato contrato, Collection<ModeloAtributo> listaAtributos,
					boolean isRemoverCampo) throws GGASException {

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo =
						contrato.getListaContratoPontoConsumo();

		if (CollectionUtils.isNotEmpty(listaAtributos)) {
			for (ModeloAtributo modeloAtributo : listaAtributos) {

				String valorPadrao = modeloAtributo.getPadrao();
				AbaAtributo atributo = modeloAtributo.getAbaAtributo();
				String nomeCampo = atributo.getNomeColuna();

				if (isRemoverCampo) {
					removerDadosAbaGeral(contrato, nomeCampo);
					removerDadosAbaResponsabilidade(contrato, nomeCampo);
					for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
						removerDadosAbaPrincipal(contratoPontoConsumo, nomeCampo);
						removerDadosAbaDadosTecnicos(contratoPontoConsumo, nomeCampo);
						removerDadosAbaConsumo(contratoPontoConsumo, nomeCampo);
						removerDadosAbaFaturamento(contratoPontoConsumo, nomeCampo);
						removerDadosAbaModalidades(contratoPontoConsumo, nomeCampo);
					}
				} else if (validarValorPadrao(valorPadrao)) {
					popularDadosAbaGeral(contrato, nomeCampo, valorPadrao, false, false);
					for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
						popularDadosAbaPrincipal(contratoPontoConsumo, nomeCampo, valorPadrao, false, false);
						popularDadosAbaDadosTecnicos(contratoPontoConsumo, nomeCampo, valorPadrao, false, false);
						popularDadosAbaConsumo(contratoPontoConsumo, nomeCampo, valorPadrao, false, false, modeloAtributo);
						popularDadosAbaFaturamento(contratoPontoConsumo, nomeCampo, valorPadrao, false, false);
						popularDadosAbaModalidades(contratoPontoConsumo, nomeCampo, valorPadrao, false, false);
					}
				}
			}
		}
	}

	/**
	 * Popular dados aba principal.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @param valorPadrao
	 *            the valor padrao
	 * @param isValidar
	 *            the is validar
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private boolean popularDadosAbaPrincipal(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo, String valorPadrao,
					boolean isValidar, boolean atributoComErro) {

		boolean isAtributoComErro = atributoComErro;
		if ("numeroDiasGarantia".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getDiaGarantiaConversao() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getDiaGarantiaConversao() == null) {
				Integer numeroDiasGarantia = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setDiaGarantiaConversao(numeroDiasGarantia);
			}
		}

		return isAtributoComErro;
	}

	/**
	 * Popular dados aba dados tecnicos.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @param valorPadrao
	 *            the valor padrao
	 * @param isValidar
	 *            the is validar
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private boolean popularDadosAbaDadosTecnicos(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo, String valorPadrao,
					boolean isValidar, boolean atributoComErro) throws NegocioException, FormatoInvalidoException {

		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();
		boolean isAtributoComErro = atributoComErro;
		if (VAZAO_INSTANTANEA.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getDiaGarantiaConversao() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getDiaGarantiaConversao() == null) {
				BigDecimal vazaoInstantanea = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaVazaoInstantanea(vazaoInstantanea);
			}

		} else if (UNIDADE_VAZAO_INSTAN.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getUnidadeVazaoInstantanea() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadeVazaoInstantanea() == null) {
				Long chaveUnidadeVazao = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidadeVazao);
				contratoPontoConsumo.setUnidadeVazaoInstantanea(unidadeMedida);
			}

		} else if (VAZAO_INSTANTANEA_MAXIMA.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaVazaoMaximaInstantanea() == null) {
				BigDecimal vazaoInstantaneaMaxima = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaVazaoMaximaInstantanea(vazaoInstantaneaMaxima);
			}

		} else if (UNIDADE_VAZAO_INSTAN_MAXIMA.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadeVazaoMaximaInstantanea() == null) {
				Long chaveUnidadeVazao = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidadeVazao);
				contratoPontoConsumo.setUnidadeVazaoMaximaInstantanea(unidadeMedida);
			}

		} else if (VAZAO_INSTANTANEA_MINIMA.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaVazaoMinimaInstantanea() == null) {
				BigDecimal vazaoInstantaneaMinima = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaVazaoMinimaInstantanea(vazaoInstantaneaMinima);
			}
		} else if (UNIDADE_VAZAO_INSTAN_MINIMA.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadeVazaoMinimaInstantanea() == null) {
				Long chaveUnidadeVazao = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidadeVazao);
				contratoPontoConsumo.setUnidadeVazaoMinimaInstantanea(unidadeMedida);
			}
		} else if (FAIXA_PRESSAO_FORNECIMENTO.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getFaixaPressaoFornecimento() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getFaixaPressaoFornecimento() == null) {
				ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento = ServiceLocator.getInstancia()
								.getControladorFaixaPressaoFornecimento();
				Long chavePressaoFornecimento = Long.parseLong(valorPadrao);

				FaixaPressaoFornecimento faixaPressao = controladorFaixaPressaoFornecimento
								.obterFaixaPressaoFornecimento(chavePressaoFornecimento);
				contratoPontoConsumo.setMedidaPressao(faixaPressao.getMedidaMinimo());
				contratoPontoConsumo.setUnidadePressao(faixaPressao.getUnidadePressao());
				contratoPontoConsumo.setFaixaPressaoFornecimento(faixaPressao);
				contratoPontoConsumo.setNumeroFatorCorrecao(faixaPressao.getNumeroFatorCorrecaoPTZPCS());
			}

		} else if (PRESSAO_MINIMA_FORNECIMENTO.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getMedidaPressaoMinima() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaPressaoMinima() == null) {
				BigDecimal pressaoMinimaFornecimento = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaPressaoMinima(pressaoMinimaFornecimento);
			}
		} else if (UNIDADE_PRESSAO_MINIMA_FORNEC.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getUnidadePressaoMinima() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadePressaoMinima() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadePressaoMinima(unidadeMedida);
			}
		} else if (PRESSAO_MAXIMA_FORNECIMENTO.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getMedidaPressaoMaxima() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaPressaoMaxima() == null) {
				BigDecimal pressaoMaximaFornecimento = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaPressaoMaxima(pressaoMaximaFornecimento);
			}
		} else if (UNIDADE_PRESSAO_MAXIMA_FORNEC.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getUnidadePressaoMaxima() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadePressaoMaxima() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadePressaoMaxima(unidadeMedida);
			}
		} else if (PRESSAO_LIMITE_FORNECIMENTO.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getMedidaPressaoLimite() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaPressaoLimite() == null) {
				BigDecimal pressaoLimiteFornecimento = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaPressaoLimite(pressaoLimiteFornecimento);
			}
		} else if (UNIDADE_PRESSAO_LIMITE_FORNEC.equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getUnidadePressaoLimite() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadePressaoLimite() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadePressaoLimite(unidadeMedida);
			}
		} else if ("numAnosCtrlParadaCliente".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getQuantidadeAnosParadaCliente() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getQuantidadeAnosParadaCliente() == null) {
				Integer quantidadeAnosParadaCliente = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setQuantidadeAnosParadaCliente(quantidadeAnosParadaCliente);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("maxTotalParadasCliente".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getQuantidadeTotalParadaCliente() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getQuantidadeTotalParadaCliente() == null) {
				Integer quantidadeTotalParadaCliente = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setQuantidadeTotalParadaCliente(quantidadeTotalParadaCliente);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("maxAnualParadasCliente".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getQuantidadeMaximaAnoParadaCliente() == null) {
				Integer quantidadeMaximaAnoParadaCliente = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setQuantidadeMaximaAnoParadaCliente(quantidadeMaximaAnoParadaCliente);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("numDiasProgrParadaCliente".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getDiasAntecedentesParadaCliente() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getDiasAntecedentesParadaCliente() == null) {
				Integer diasAntecedentesParadaCliente = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setDiasAntecedentesParadaCliente(diasAntecedentesParadaCliente);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("numDiasConsecParadaCliente".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getDiasConsecutivosParadaCliente() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getDiasConsecutivosParadaCliente() == null) {
				Integer diasConsecutivosParadaCliente = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setDiasConsecutivosParadaCliente(diasConsecutivosParadaCliente);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("numAnosCtrlParadaCDL".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getQuantidadeAnosParadaCDL() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getQuantidadeAnosParadaCDL() == null) {
				Integer quantidadeAnosParadaCDL = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setQuantidadeAnosParadaCDL(quantidadeAnosParadaCDL);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("maxTotalParadasCDL".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getQuantidadeTotalParadaCDL() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getQuantidadeTotalParadaCDL() == null) {
				Integer quantidadeTotalParadaCDL = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setQuantidadeTotalParadaCDL(quantidadeTotalParadaCDL);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("maxAnualParadasCDL".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getQuantidadeMaximaAnoParadaCDL() == null) {
				Integer quantidadeMaximaAnoParadaCDL = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setQuantidadeMaximaAnoParadaCDL(quantidadeMaximaAnoParadaCDL);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("numDiasProgrParadaCDL".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getDiasAntecedentesParadaCDL() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getDiasAntecedentesParadaCDL() == null) {
				Integer diasAntecedentesParadaCDL = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setDiasAntecedentesParadaCDL(diasAntecedentesParadaCDL);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		} else if ("numDiasConsecParadaCDL".equals(nomeCampo)) {
			if (isValidar && contratoPontoConsumo.getDiasConsecutivosParadaCDL() == null) {
				isAtributoComErro = true;
			} else if (!isValidar && contratoPontoConsumo.getDiasConsecutivosParadaCDL() == null) {
				Integer diasConsecutivosParadaCDL = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setDiasConsecutivosParadaCDL(diasConsecutivosParadaCDL);
				contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.TRUE);
			}
		}

		return isAtributoComErro;
	}

	/**
	 * Popular dados aba consumo.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param campoAdicionado
	 *            the campo adicionado
	 * @param valorPadrao
	 *            the valor padrao
	 * @param isValidar
	 *            the is validar
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 * @param modeloAtributo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private boolean popularDadosAbaConsumo(ContratoPontoConsumo contratoPontoConsumo, String campoAdicionado, String valorPadrao,
					boolean isValidar, boolean isAtributoComErro, ModeloAtributo modeloAtributo) throws NegocioException, FormatoInvalidoException {

		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia().getControladorHistoricoConsumo();
		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		boolean reotrno = isAtributoComErro;

		if ("horaInicialDia".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getNumeroHoraInicial() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getNumeroHoraInicial() == null) {
				Integer numeroHoraInicial = Integer.parseInt(valorPadrao);
				contratoPontoConsumo.setNumeroHoraInicial(numeroHoraInicial);
			}
		} else if (REGIME_CONSUMO.equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getRegimeConsumo() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getRegimeConsumo() == null) {
				Long chaveRegimeConsumo = Long.parseLong(valorPadrao);
				EntidadeConteudo regimeConsumoEntidade = controladorEntidadeConteudo.obter(chaveRegimeConsumo);
				contratoPontoConsumo.setRegimeConsumo(regimeConsumoEntidade);
			}
		} else if (FORNECIMENTO_MAXIMO_DIARIO.equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaFornecimentoMaxDiaria() == null) {
				BigDecimal fornecimentoMaximoDiario = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaFornecimentoMaxDiaria(fornecimentoMaximoDiario);
			}
		} else if (UNIDADE_FORNEC_MAXIMO_DIARIO.equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadeFornecimentoMaxDiaria() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadeFornecimentoMaxDiaria(unidadeMedida);
			}
		} else if (FORNECIMENTO_MINIMO_DIARIO.equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getMedidaFornecimentoMinDiaria() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaFornecimentoMinDiaria() == null) {
				BigDecimal fornecimentoMinimoDiario = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaFornecimentoMinDiaria(fornecimentoMinimoDiario);
			}
		} else if (UNIDADE_FORNEC_MINIMO_DIARIO.equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getUnidadeFornecimentoMinDiaria() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadeFornecimentoMinDiaria() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadeFornecimentoMinDiaria(unidadeMedida);
			}
		} else if ("fornecimentoMinimoMensal".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getMedidaFornecimentoMinMensal() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaFornecimentoMinMensal() == null) {
				BigDecimal fornecimentoMinimoMensal = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaFornecimentoMinMensal(fornecimentoMinimoMensal);
			}
		} else if ("unidadeFornecMinimoMensal".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getUnidadeFornecimentoMinMensal() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getUnidadeFornecimentoMinMensal() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadeFornecimentoMinMensal(unidadeMedida);
			}
		} else if ("fornecimentoMinimoAnual".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getMedidaFornecimentoMinAnual() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaFornecimentoMinAnual() == null) {
				BigDecimal fornecimentoMinimoAnual = Util.converterCampoStringParaValorBigDecimal(VAZÃO_INSTANTÂNEA, valorPadrao,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				contratoPontoConsumo.setMedidaFornecimentoMinAnual(fornecimentoMinimoAnual);
			}
		} else if ("unidadeFornecMinimoAnual".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getMedidaFornecimentoMinAnual() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getMedidaFornecimentoMinAnual() == null) {
				Long chaveUnidade = Long.parseLong(valorPadrao);
				Unidade unidadeMedida = (Unidade) controladorUnidade.obter(chaveUnidade);
				contratoPontoConsumo.setUnidadeFornecimentoMinAnual(unidadeMedida);
			}
		} else if ("consumoFatFalhaMedicao".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura() == null) {
				reotrno = true;
			} else if (!isValidar && contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura() == null) {
				ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();
				Long chaveAnormalidade = Long.parseLong(valorPadrao);
				AcaoAnormalidadeConsumo consumoFatorFalhaMedicao = controladorAnormalidade.obterAcaoAnormalidadeConsumo(chaveAnormalidade);
				contratoPontoConsumo.setAcaoAnormalidadeConsumoSemLeitura(consumoFatorFalhaMedicao);
			}
		} else if ("localAmostragemPCS".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem() == null) {
				reotrno = true;
			} else if (!isValidar && valorPadrao != null && !valorPadrao.isEmpty()
					&& (contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem() == null
							|| contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().isEmpty())) {

				Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoAmostragem =
								popularDadosAmostragemPCS(contratoPontoConsumo, valorPadrao, controladorEntidadeConteudo);

				contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().clear();
				contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().addAll(listaContratoPontoAmostragem);

			}
		} else if ("intervaloRecuperacaoPCS".equals(campoAdicionado)) {
			if (isValidar && contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo() == null) {
				reotrno = true;
			} else if (!isValidar && valorPadrao != null && !valorPadrao.isEmpty()
					&& (contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo() == null
							|| contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().isEmpty())) {

				Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoIntervalo =
								popularDadosIntervaloPCS(contratoPontoConsumo, valorPadrao, modeloAtributo,
												controladorHistoricoConsumo);

				contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().clear();
				contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().addAll(listaContratoPontoIntervalo);

			}
		}

		return reotrno;

	}

	/**
	 * @param contratoPontoConsumo
	 * @param valorPadrao
	 * @param controladorEntidadeConteudo
	 * @return
	 * @throws FormatoInvalidoException
	 */
	private Collection<ContratoPontoConsumoPCSAmostragem> popularDadosAmostragemPCS(ContratoPontoConsumo contratoPontoConsumo,
					String valorPadrao, ControladorEntidadeConteudo controladorEntidadeConteudo) throws FormatoInvalidoException {

		Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoAmostragem = new
						LinkedHashSet<>();
		String[] locaisAmostragem = valorPadrao.split(Constantes.STRING_VIRGULA);

		for (int i = 0; i < locaisAmostragem.length; i++) {

			EntidadeConteudo entidadeLocalAmostragemPCS = controladorEntidadeConteudo.obter(
							Util.converterCampoStringParaValorLong("Local de Amostragem", locaisAmostragem[i]));

			ContratoPontoConsumoPCSAmostragem contratoPontoAmostragem = (ContratoPontoConsumoPCSAmostragem)
							criarContratoPontoConsumoPCSAmostragem();
			contratoPontoAmostragem.setContratoPontoConsumo(contratoPontoConsumo);
			contratoPontoAmostragem.setLocalAmostragemPCS(entidadeLocalAmostragemPCS);
			contratoPontoAmostragem.setPrioridade(i);
			contratoPontoAmostragem.setHabilitado(true);
			contratoPontoAmostragem.setUltimaAlteracao(Calendar.getInstance().getTime());

			listaContratoPontoAmostragem.add(contratoPontoAmostragem);

		}
		return listaContratoPontoAmostragem;
	}

	/**
	 * @param contratoPontoConsumo
	 * @param valorPadrao
	 * @param modeloAtributo
	 * @param controladorHistoricoConsumo
	 * @param controladorModeloContrato
	 * @return
	 * @throws NegocioException
	 * @throws FormatoInvalidoException
	 */
	private Collection<ContratoPontoConsumoPCSIntervalo> popularDadosIntervaloPCS(ContratoPontoConsumo contratoPontoConsumo,
					String valorPadrao, ModeloAtributo modeloAtributo, ControladorHistoricoConsumo controladorHistoricoConsumo)
									throws NegocioException, FormatoInvalidoException {

		ControladorModeloContrato controladorModeloContrato = ServiceLocator.getInstancia().getControladorModeloContrato();

		ModeloAtributo tamReducaoRecuperacaoPCS = controladorModeloContrato.obterModeloAtributo(
						modeloAtributo.getModeloContrato(), "tamReducaoRecuperacaoPCS");

		Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoIntervalo = new
						LinkedHashSet<>();
		String[] invervalosRecuperacaoPCS = valorPadrao.split(Constantes.STRING_VIRGULA);

		for (int i = 0; i < invervalosRecuperacaoPCS.length; i++) {

			IntervaloPCS intervaloPCS = controladorHistoricoConsumo.obterIntervaloPCS(Util.
							converterCampoStringParaValorLong("Intervalo de Amostragem", invervalosRecuperacaoPCS[i]));

			ContratoPontoConsumoPCSIntervalo contratoPontoConsumoIntervaloPCS = (ContratoPontoConsumoPCSIntervalo)
							criarContratoPontoConsumoPCSIntervalo();
			contratoPontoConsumoIntervaloPCS.setContratoPontoConsumo(contratoPontoConsumo);
			contratoPontoConsumoIntervaloPCS.setIntervaloPCS(intervaloPCS);
			contratoPontoConsumoIntervaloPCS.setPrioridade(i);
			contratoPontoConsumoIntervaloPCS.setHabilitado(true);
			contratoPontoConsumoIntervaloPCS.setUltimaAlteracao(Calendar.getInstance().getTime());

			if ((tamReducaoRecuperacaoPCS != null && tamReducaoRecuperacaoPCS.isObrigatorio())
					&& !StringUtils.isEmpty(tamReducaoRecuperacaoPCS.getPadrao())) {
				contratoPontoConsumoIntervaloPCS.setTamanho(Util.converterCampoStringParaValorInteger(
						"Tamanho da Redução para Recuperação do PCS", tamReducaoRecuperacaoPCS.getPadrao()));
			}

			listaContratoPontoIntervalo.add(contratoPontoConsumoIntervaloPCS);
		}
		return listaContratoPontoIntervalo;
	}

	/**
	 * Popular dados aba modalidades.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @param valorPadrao
	 *            the valor padrao
	 * @param isValidar
	 *            the is validar
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private boolean popularDadosAbaModalidades(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo, String valorPadrao,
					boolean isValidar, boolean isAtributoComErro) throws GGASException {

		boolean isAtrComErro = isAtributoComErro;
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorApuracaoPenalidade controladorPenalidade = ServiceLocator.getInstancia().getControladorApuracaoPenalidade();
		Collection<ContratoPontoConsumoModalidade> listaModalidade = contratoPontoConsumo.getListaContratoPontoConsumoModalidade();

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaModalidade) {
			if ("modalidade".equals(nomeCampo)) {
				isAtrComErro = populaCampoModalidade(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("prazoRevizaoQDC".equals(nomeCampo)) {
				isAtrComErro = populaCampoPrazoRevizaoQDC(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if (QDS_MAIOR_QDC.equals(nomeCampo)) {
				isAtrComErro = populaCampoQdsMaiorQDC(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if (VARIACAO_SUPERIOR_QDC.equals(nomeCampo)) {
				isAtrComErro =
						populaCampoVariacaoSuperiorQDC(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("diasAntecSolicConsumo".equals(nomeCampo)) {
				isAtrComErro =
						populaCampoDiasAntecSolicConsumo(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("numMesesSolicConsumo".equals(nomeCampo)) {
				isAtrComErro =
						populaCampoNumMesesSolicConsumo(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("confirmacaoAutomaticaQDS".equals(nomeCampo)) {
				isAtrComErro =
						populaCampoConfirmacaoAutomaticaQDS(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("percDuranteRetiradaQPNR".equals(nomeCampo)) {
				isAtrComErro =
						populaCampoPercDuranteRetiradaQPNR(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("percFimRetiradaQPNR".equals(nomeCampo)) {
				isAtrComErro =
						populaCampoPercFimRetiradaQPNR(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("tempoValidadeRetiradaQPNR".equals(nomeCampo)) {
				isAtrComErro =
						populaCampoTempoValidadeRetiradaQPNR(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoModalidade);
			} else if ("horaLimiteProgramacaoDiaria".equals(nomeCampo)) {
				if (isValidar && contratoPontoConsumoModalidade.getHoraLimiteProgramacaoDiaria() == null) {
					isAtrComErro = true;
				} else if (!isValidar && contratoPontoConsumoModalidade.getHoraLimiteProgramacaoDiaria() == null) {
					Integer horaLimiteProgramacaoDiaria = Integer.valueOf(valorPadrao);
					contratoPontoConsumoModalidade.setHoraLimiteProgramacaoDiaria(horaLimiteProgramacaoDiaria);
				}
			} else if ("horaLimiteProgcaoIntradiaria".equals(nomeCampo)) {
				if (isValidar && contratoPontoConsumoModalidade.getHoraLimiteProgIntradiaria() == null) {
					isAtrComErro = true;
				} else if (!isValidar && contratoPontoConsumoModalidade.getHoraLimiteProgIntradiaria() == null) {
					Integer horaLimiteProgcaoIntradiaria = Integer.valueOf(valorPadrao);
					contratoPontoConsumoModalidade.setHoraLimiteProgIntradiaria(horaLimiteProgcaoIntradiaria);
				}
			} else if ("horaLimiteAceitacaoDiaria".equals(nomeCampo)) {
				if (isValidar && contratoPontoConsumoModalidade.getHoraLimiteAceitacaoDiaria() == null) {
					isAtrComErro = true;
				} else if (!isValidar && contratoPontoConsumoModalidade.getHoraLimiteAceitacaoDiaria() == null) {
					Integer horaLimiteAceitacaoDiaria = Integer.valueOf(valorPadrao);
					contratoPontoConsumoModalidade.setHoraLimiteAceitacaoDiaria(horaLimiteAceitacaoDiaria);
				}
			} else if ("horaLimiteAceitIntradiaria".equals(nomeCampo)) {
				if (isValidar && contratoPontoConsumoModalidade.getHoraLimiteAceitIntradiaria() == null) {
					isAtrComErro = true;
				} else if (!isValidar && contratoPontoConsumoModalidade.getHoraLimiteAceitIntradiaria() == null) {
					Integer horaLimiteAceitIntradiaria = Integer.valueOf(valorPadrao);
					contratoPontoConsumoModalidade.setHoraLimiteAceitIntradiaria(horaLimiteAceitIntradiaria);
				}
			}

			Collection<ContratoPontoConsumoPenalidade> listaPontoPenalidade = contratoPontoConsumoModalidade
							.getListaContratoPontoConsumoPenalidade();
			isAtrComErro =
					populaDadosPontoConsumoPenalidade(nomeCampo, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							controladorPenalidade, listaPontoPenalidade);
		}

		return isAtrComErro;
	}

	private boolean populaCampoTempoValidadeRetiradaQPNR(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {
		if (isValidar && contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getAnosValidadeRetiradaQPNR() == null) {
			Integer anosValidadeRetiradaQPNR = Integer.valueOf(valorPadrao);
			contratoPontoConsumoModalidade.setAnosValidadeRetiradaQPNR(anosValidadeRetiradaQPNR);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercFimRetiradaQPNR(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoModalidade.getPercentualQDCFimContratoQPNR() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getPercentualQDCFimContratoQPNR() == null) {
			BigDecimal percentualQDCFimContratoQPNR = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoModalidade.setPercentualQDCFimContratoQPNR(percentualQDCFimContratoQPNR);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercDuranteRetiradaQPNR(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoModalidade.getPercentualQDCContratoQPNR() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getPercentualQDCContratoQPNR() == null) {
			BigDecimal percentualQDCContratoQPNR = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoModalidade.setPercentualQDCContratoQPNR(percentualQDCContratoQPNR);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoConfirmacaoAutomaticaQDS(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {
		if (isValidar && contratoPontoConsumoModalidade.getIndicadorConfirmacaoAutomaticaQDS() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getIndicadorConfirmacaoAutomaticaQDS() == null) {
			Boolean indicadorConfirmacaoAutomaticaQDS = Boolean.valueOf(valorPadrao);
			contratoPontoConsumoModalidade.setIndicadorConfirmacaoAutomaticaQDS(indicadorConfirmacaoAutomaticaQDS);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoNumMesesSolicConsumo(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {
		if (isValidar && contratoPontoConsumoModalidade.getMesesQDS() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getMesesQDS() == null) {
			Integer mesesQDS = Integer.valueOf(valorPadrao);
			contratoPontoConsumoModalidade.setMesesQDS(mesesQDS);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoDiasAntecSolicConsumo(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {
		if (isValidar && contratoPontoConsumoModalidade.getDiasAntecedenciaQDS() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getDiasAntecedenciaQDS() == null) {
			Integer diasAntecedenciaQDS = Integer.valueOf(valorPadrao);
			contratoPontoConsumoModalidade.setDiasAntecedenciaQDS(diasAntecedenciaQDS);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoVariacaoSuperiorQDC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getPercentualVarSuperiorQDC() == null) {
			BigDecimal percentualVarSuperiorQDC = Util.converterCampoStringParaValorBigDecimal("Percentual variação superior QDC",
							valorPadrao, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			contratoPontoConsumoModalidade.setPercentualVarSuperiorQDC(percentualVarSuperiorQDC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoQdsMaiorQDC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {
		if (isValidar && contratoPontoConsumoModalidade.getIndicadorQDSMaiorQDC() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getIndicadorQDSMaiorQDC() == null) {
			Boolean indicadorQDSMaiorQDC = Boolean.valueOf(valorPadrao);
			contratoPontoConsumoModalidade.setIndicadorQDSMaiorQDC(indicadorQDSMaiorQDC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPrazoRevizaoQDC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) throws GGASException {
		if (isValidar && contratoPontoConsumoModalidade.getPrazoRevisaoQDC() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getPrazoRevisaoQDC() == null) {
			Date data = Util.converterCampoStringParaData(DATA, valorPadrao, Constantes.FORMATO_DATA_BR);
			contratoPontoConsumoModalidade.setPrazoRevisaoQDC(data);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoModalidade(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) throws NegocioException {
		if (isValidar && contratoPontoConsumoModalidade.getContratoModalidade() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoModalidade.getContratoModalidade() == null) {
			Long chaveModalidade = Long.parseLong(valorPadrao);
			ContratoModalidade contratoModalidade = this.obterContratoModalidade(chaveModalidade);
			contratoPontoConsumoModalidade.setContratoModalidade(contratoModalidade);
		}
		return isAtributoComErro;
	}

	private boolean populaDadosPontoConsumoPenalidade(String nomeCampo, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ControladorApuracaoPenalidade controladorPenalidade,
			Collection<ContratoPontoConsumoPenalidade> listaPontoPenalidade) throws NegocioException, FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaPontoPenalidade) {

			isAtrComErro =
					populaCamposRetiradaMaiorMenor(nomeCampo, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							controladorPenalidade, contratoPontoConsumoPenalidade);
			isAtrComErro =
					populaCamposTakeOrPay(nomeCampo, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);

			isAtrComErro =
					populaCamposPontoConsumoPenalidade(nomeCampo, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);

		}
		return isAtrComErro;
	}

	private boolean populaCamposPontoConsumoPenalidade(String nomeCampo, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade)
			throws FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if ("consideraParadaProgramada".equals(nomeCampo)) {
			populaCampoConsideraParadaProgramada(valorPadrao, contratoPontoConsumoPenalidade);
		} else if ("consideraFalhaFornecimento".equals(nomeCampo)) {
			populaCampoConsideraFalhaFornecimento(valorPadrao, contratoPontoConsumoPenalidade);
		} else if ("consideraCasoFortuito".equals(nomeCampo)) {
			populaCampoConsideraCasoFortuito(valorPadrao, contratoPontoConsumoPenalidade);
		} else if ("recuperavel".equals(nomeCampo)) {
			populaCampoRecuperavel(valorPadrao, contratoPontoConsumoPenalidade);
		} else if ("percentualNaoRecuperavel".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoPercentualNaoRecuperavel(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoPenalidade);
		} else if ("apuracaoParadaProgramada".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoApuracaoParadaProgramada(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("apuracaoCasoFortuito".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoApuracaoCasoFortuito(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("apuracaoFalhaFornecimento".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoApuracaoFalhaFornecimento(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		}
		return isAtrComErro;
	}

	private void populaCampoRecuperavel(String valorPadrao, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if(contratoPontoConsumoPenalidade.getRecuperavel()==null){
			boolean recuperavel = Boolean.parseBoolean(valorPadrao);
			contratoPontoConsumoPenalidade.setRecuperavel(recuperavel);
		}
	}

	private void populaCampoConsideraCasoFortuito(String valorPadrao, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (contratoPontoConsumoPenalidade.getConsideraCasoFortuito() == null) {
			boolean consideraCasoFortuito = Boolean.parseBoolean(valorPadrao);
			contratoPontoConsumoPenalidade.setConsideraCasoFortuito(consideraCasoFortuito);
		}
	}

	private void populaCampoConsideraFalhaFornecimento(String valorPadrao, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if(contratoPontoConsumoPenalidade.getConsideraFalhaFornecimento()==null){
			boolean consideraFalhaFornecimento = Boolean.parseBoolean(valorPadrao);
			contratoPontoConsumoPenalidade.setConsideraFalhaFornecimento(consideraFalhaFornecimento);
		}
	}

	private void populaCampoConsideraParadaProgramada(String valorPadrao, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if(contratoPontoConsumoPenalidade.getConsideraParadaProgramada()==null){
			boolean consideraParadaProgramada = Boolean.parseBoolean(valorPadrao);
			contratoPontoConsumoPenalidade.setConsideraParadaProgramada(consideraParadaProgramada);
		}
	}

	private boolean populaCamposTakeOrPay(String nomeCampo, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade)
			throws FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if ("indicadorImposto".equals(nomeCampo)) {
			populaCampoIndicadorImposto(valorPadrao, contratoPontoConsumoPenalidade);
			// TAKE OR PAY--------------------------------------------------------------------------
		} else if ("periodicidadeTakeOrPay".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposPeriodicidade(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("referenciaQFParadaProgramada".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoReferenciaQFParadaProgramada(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("consumoReferenciaTakeOrPay".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposConsumoReferencia(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("precoCobranca".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposPrecoCobranca(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("tipoApuracao".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposTipoApuracao(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("margemVariacaoTakeOrPay".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoMargemVariacaoTakeOrPay(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoPenalidade);
		}
		return isAtrComErro;
	}

	private void populaCampoIndicadorImposto(String valorPadrao, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if(contratoPontoConsumoPenalidade.getIndicadorImposto()==null){
			boolean indicadorImposto = Boolean.parseBoolean(valorPadrao);
			contratoPontoConsumoPenalidade.setIndicadorImposto(indicadorImposto);
		}
	}

	private boolean populaCamposRetiradaMaiorMenor(String nomeCampo, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ControladorApuracaoPenalidade controladorPenalidade,
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws NegocioException, FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if (PENALIDADE_RET_MAIOR_MENOR_M.equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoPenalidadeRetMaiorMenorM(valorPadrao, isValidar, isAtributoComErro, controladorPenalidade,
							contratoPontoConsumoPenalidade);
		} else if ("periodicidadeRetMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposPeriodicidade(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("baseApuracaoRetMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoBaseApuracaoRetMaiorMenorM(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("precoCobrancaRetirMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposPrecoCobranca(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("tipoApuracaoRetirMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposTipoApuracao(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("percentualCobRetMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoPercentualCobRetMaiorMenorM(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoPenalidade);
		} else if ("percentualCobIntRetMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoPercentualCobIntRetMaiorMenorM(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoPenalidade);
		} else if ("consumoReferenciaRetMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCamposConsumoReferencia(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPontoConsumoPenalidade);
		} else if ("percentualRetMaiorMenorM".equalsIgnoreCase(nomeCampo)) {
			isAtrComErro =
					populaCampoPercentualRetMaiorMenorM(valorPadrao, isValidar, isAtributoComErro, contratoPontoConsumoPenalidade);
		}
		return isAtrComErro;
	}

	private boolean populaCampoReferenciaQFParadaProgramada(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getReferenciaQFParadaProgramada() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getReferenciaQFParadaProgramada() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo referenciaQFParadaProgramada = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setReferenciaQFParadaProgramada(referenciaQFParadaProgramada);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoApuracaoFalhaFornecimento(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getApuracaoFalhaFornecimento() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getApuracaoFalhaFornecimento() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo apuracaoFalhaFornecimento = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setApuracaoFalhaFornecimento(apuracaoFalhaFornecimento);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoApuracaoCasoFortuito(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getApuracaoCasoFortuito() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getApuracaoCasoFortuito() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo apuracaoCasoFortuito = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setApuracaoCasoFortuito(apuracaoCasoFortuito);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoApuracaoParadaProgramada(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getApuracaoParadaProgramada() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getApuracaoParadaProgramada() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo apuracaoParadaProgramada = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setApuracaoParadaProgramada(apuracaoParadaProgramada);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualNaoRecuperavel(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoPenalidade.getPercentualNaoRecuperavel() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getPercentualNaoRecuperavel() == null) {
			BigDecimal percentualNaoRecuperavel = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoPenalidade.setPercentualNaoRecuperavel(percentualNaoRecuperavel);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoMargemVariacaoTakeOrPay(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoPenalidade.getPercentualMargemVariacao() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getPercentualMargemVariacao() == null) {
			BigDecimal margemVariacaoTakeOrPay = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoPenalidade.setPercentualMargemVariacao(margemVariacaoTakeOrPay);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualRetMaiorMenorM(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoPenalidade.getValorPercentualRetMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getValorPercentualRetMaiorMenor() == null) {
			BigDecimal percentualRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoPenalidade.setValorPercentualRetMaiorMenor(percentualRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCamposConsumoReferencia(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getConsumoReferencia() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getConsumoReferencia() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo consumoReferenciaRetMaiorMenor = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setConsumoReferencia(consumoReferenciaRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualCobIntRetMaiorMenorM(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoPenalidade.getValorPercentualCobIntRetMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getValorPercentualCobIntRetMaiorMenor() == null) {
			BigDecimal percentualCobIntRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoPenalidade.setValorPercentualCobIntRetMaiorMenor(percentualCobIntRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualCobRetMaiorMenorM(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPontoConsumoPenalidade.getValorPercentualCobRetMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getValorPercentualCobRetMaiorMenor() == null) {
			BigDecimal percentualCobRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPontoConsumoPenalidade.setValorPercentualCobRetMaiorMenor(percentualCobRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCamposTipoApuracao(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getTipoApuracao() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getTipoApuracao() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo tipoApuracao = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setTipoApuracao(tipoApuracao);
		}
		return isAtributoComErro;
	}

	private boolean populaCamposPrecoCobranca(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getPrecoCobranca() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getPrecoCobranca() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo precoCobranca = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setPrecoCobranca(precoCobranca);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoBaseApuracaoRetMaiorMenorM(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getBaseApuracao() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getBaseApuracao() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo baseApuracaoRetMaiorMenor = controladorEntidadeConteudo.obter(chave);
			contratoPontoConsumoPenalidade.setBaseApuracao(baseApuracaoRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCamposPeriodicidade(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {
		if (isValidar && contratoPontoConsumoPenalidade.getPeriodicidadePenalidade() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getPeriodicidadePenalidade() == null) {
			Long chavePeriodicidade = Long.parseLong(valorPadrao);
			EntidadeConteudo periodicidadeRetMaiorMenor = controladorEntidadeConteudo
							.obter(chavePeriodicidade);
			contratoPontoConsumoPenalidade.setPeriodicidadePenalidade(periodicidadeRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPenalidadeRetMaiorMenorM(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorApuracaoPenalidade controladorPenalidade, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade)
			throws NegocioException {
		if (isValidar && contratoPontoConsumoPenalidade.getPenalidade() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumoPenalidade.getPenalidade() == null) {
			Long chavePenalidade = Long.parseLong(valorPadrao);
			Penalidade penalidadeRetMaiorMenor = controladorPenalidade.obterPenalidade(chavePenalidade);
			contratoPontoConsumoPenalidade.setPenalidade(penalidadeRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	/**
	 * Popular dados aba faturamento.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param campoAdicionado
	 *            the campo adicionado
	 * @param valorPadrao
	 *            the valor padrao
	 * @param isValidar
	 *            the is validar
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private boolean popularDadosAbaFaturamento(ContratoPontoConsumo contratoPontoConsumo, String campoAdicionado, String valorPadrao,
					boolean isValidar, boolean isAtributoComErro) throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		boolean isAtrComErro = isAtributoComErro;
		if ("fatPeriodicidade".equals(campoAdicionado)) {
			isAtrComErro = populaCampoFatPeriodicidade(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro);
		} else if ("indicadorNFE".equals(campoAdicionado)) {
			isAtrComErro = populaCampoIndicadorNFE(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro);
		} else if (ATRIBUTO_CONTRATO_COMPRA.equals(campoAdicionado)) {
			isAtrComErro =
					populaCampoContratoCompra(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);
		} else if ("endFisEntFatCEP".equals(campoAdicionado)) {
			isAtrComErro = populaCampoEndFisEntFatCEP(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro);
		} else if ("endFisEntFatNumero".equals(campoAdicionado)) {
			isAtrComErro = populaCampoEndFisEntFatNumero(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro);
		} else if ("endFisEntFatComplemento".equals(campoAdicionado)) {
			isAtrComErro = populaCampoEndFisEntFatComplemento(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro);
		} else if ("endFisEntFatEmail".equals(campoAdicionado)) {
			isAtrComErro = populaCampoEndFisEntFatEmail(contratoPontoConsumo, valorPadrao, isValidar, isAtributoComErro);
		}

		return isAtrComErro;
	}

	private boolean populaCampoEndFisEntFatComplemento(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) {
		if (isValidar && contratoPontoConsumo.getComplementoEndereco() == null) {
			return populaComplemento(contratoPontoConsumo);
		} else if (!isValidar && contratoPontoConsumo.getComplementoEndereco() == null) {
			contratoPontoConsumo.setComplementoEndereco(valorPadrao);
		}
		return isAtributoComErro;
	}

	private boolean populaComplemento(ContratoPontoConsumo contratoPontoConsumo) {
		String complementoPontoConsumo = contratoPontoConsumo.getPontoConsumo().getDescricaoComplemento();
		if(complementoPontoConsumo!=null){
			contratoPontoConsumo.setComplementoEndereco(complementoPontoConsumo);
		}else{
			String complementoImovel = contratoPontoConsumo.getPontoConsumo().getImovel().getDescricaoComplemento();
			if(complementoImovel!=null){
				contratoPontoConsumo.setComplementoEndereco(complementoImovel);
			}else{
				return true;
			}
		}
		return false;
	}

	private boolean populaCampoEndFisEntFatNumero(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) {
		if (isValidar && contratoPontoConsumo.getNumeroImovel() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumo.getNumeroImovel() == null) {
			contratoPontoConsumo.setNumeroImovel(valorPadrao);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoEndFisEntFatCEP(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) throws NegocioException {
		if (isValidar && contratoPontoConsumo.getCep() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumo.getCep() == null) {
			ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();
			Long chaveCep = Long.parseLong(valorPadrao);
			Cep cep = controladorEndereco.obterCepPorChave(chaveCep);
			contratoPontoConsumo.setCep(cep);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoIndicadorNFE(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao,
			boolean isValidar, boolean isAtributoComErro) {

		if (isValidar && contratoPontoConsumo.getIsEmiteNotaFiscalEletronica() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumo.getIsEmiteNotaFiscalEletronica() == null) {
			boolean notaFiscalAtiva = Boolean.parseBoolean(valorPadrao);
			contratoPontoConsumo.setIsEmiteNotaFiscalEletronica(notaFiscalAtiva);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoFatPeriodicidade(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) throws NegocioException {
		if (isValidar && contratoPontoConsumo.getPeriodicidade() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumo.getPeriodicidade() == null) {
			ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
			Long chavePeriodicidade = Long.parseLong(valorPadrao);
			Periodicidade periodicidadePesquisada = controladorRota.obterPeriodicidade(chavePeriodicidade);
			contratoPontoConsumo.setPeriodicidade(periodicidadePesquisada);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoContratoCompra(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) {
		if (isValidar && contratoPontoConsumo.getContratoCompra() == null) {
			return true;
		} else if (!isValidar && contratoPontoConsumo.getContratoCompra() == null) {
			Long chaveContratoCompra = Long.parseLong(valorPadrao);
			EntidadeConteudo contratoCompra = controladorEntidadeConteudo.obter(chaveContratoCompra);
			contratoPontoConsumo.setContratoCompra(contratoCompra);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoEndFisEntFatEmail(ContratoPontoConsumo contratoPontoConsumo, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) {
		if (isValidar && contratoPontoConsumo.getEmailEntrega() == null) {
			String emailClienteAssinatura = contratoPontoConsumo.getContrato().getClienteAssinatura().getEmailPrincipal();
			if(emailClienteAssinatura!=null){
				contratoPontoConsumo.setEmailEntrega(emailClienteAssinatura);
			}else{
				return true;
			}
		} else if (!isValidar && contratoPontoConsumo.getEmailEntrega() == null) {
			contratoPontoConsumo.setEmailEntrega(valorPadrao);
		}
		return isAtributoComErro;
	}

	/**
	 * Popular dados aba geral.
	 *
	 * @param contrato
	 *            the contrato
	 * @param campoAdicionado
	 *            the campo adicionado
	 * @param valorPadrao
	 *            the valor padrao
	 * @param isValidar
	 *            the is validar
	 * @param isAtributoComErro
	 *            the is atributo com erro
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private boolean popularDadosAbaGeral(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
					boolean isAtributoComErro) throws GGASException {

		boolean isAtrComErro = isAtributoComErro;
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorTarifa controladorTarifa = ServiceLocator.getInstancia().getControladorTarifa();
		ControladorApuracaoPenalidade controladorPenalidade = ServiceLocator.getInstancia().getControladorApuracaoPenalidade();

		isAtrComErro =
				populaDadosContrato(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);

		isAtrComErro =
				populaDadosFaturaAgrupada(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);

		isAtrComErro =
				populaDadosGarantiaFinanceira(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro,
						controladorEntidadeConteudo);

		isAtrComErro =
				populaDadosPagamentoEfetuadoEmAtraso(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro,
						controladorTarifa);

		isAtrComErro =
				populaDadosPenalidadesEMultaRecisoria(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro,
						controladorEntidadeConteudo);

		isAtrComErro =
				populaDadosRetiradaMaiorMenor(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro,
						controladorEntidadeConteudo);

		isAtrComErro =
				populaDadosPenalidadesPorRetiradaOuTakeOrPay(contrato, campoAdicionado, valorPadrao, isValidar, isAtributoComErro,
						controladorEntidadeConteudo, controladorPenalidade);

		return isAtrComErro;
	}

	private boolean populaDadosPenalidadesEMultaRecisoria(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) throws FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if ("percentualTarifaDoP".equals(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualTarifaDoP(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("percentualSobreTariGas".equals(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualSobreTariGas(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("multaRecisoria".equals(campoAdicionado)) {
			isAtrComErro = populaCampoMultaRecisoria(contrato, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);
		}
		return isAtrComErro;
	}

	private boolean populaDadosPenalidadesPorRetiradaOuTakeOrPay(Contrato contrato, String campoAdicionado, String valorPadrao,
			boolean isValidar, boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo,
			ControladorApuracaoPenalidade controladorPenalidade) throws NegocioException, FormatoInvalidoException {
		// PENALIDADES POR RETIRADA E TAKE OR PAY
		boolean isAtrComErro = isAtributoComErro;
		Collection<ContratoPenalidade> listaContratoPenalidade = contrato.getListaContratoPenalidade();
		for (ContratoPenalidade contratoPenalidade : listaContratoPenalidade) {

			isAtrComErro =
					populaDadosPenalidadesRetiradaMaiorMenor(campoAdicionado, valorPadrao, isValidar, isAtributoComErro,
							controladorEntidadeConteudo, controladorPenalidade, contratoPenalidade);

			isAtrComErro =
					populaDadosPenalidadeTakeOrPay(campoAdicionado, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
			if ("recuperavel".equals(campoAdicionado)) {
				populaCampoRecuperavel(valorPadrao, contratoPenalidade);
			} else if ("percentualNaoRecuperavelC".equalsIgnoreCase(campoAdicionado)) {
				isAtrComErro = populaCampoPercentualNaoRecuperavelC(valorPadrao, isValidar, isAtributoComErro, contratoPenalidade);
			} else if ("apuracaoParadaProgramadaC".equalsIgnoreCase(campoAdicionado)) {
				isAtrComErro =
						populaCampoApuracaoParadaProgramadaC(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
								contratoPenalidade);
			} else if ("apuracaoCasoFortuitoC".equalsIgnoreCase(campoAdicionado)) {
				isAtrComErro =
						populaCampoApuracaoCasoFortuitoC(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
								contratoPenalidade);
			} else if ("apuracaoFalhaFornecimentoC".equalsIgnoreCase(campoAdicionado)) {
				isAtrComErro =
						populaCampoApuracaoFalhaFornecimentoC(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
								contratoPenalidade);
			}
		}
		return isAtrComErro;
	}

	private void populaCampoRecuperavel(String valorPadrao, ContratoPenalidade contratoPenalidade) {
		if(contratoPenalidade.getRecuperavel()==null){
			boolean recuperavel = Boolean.parseBoolean(valorPadrao);
			contratoPenalidade.setRecuperavel(recuperavel);
		}
	}

	private boolean populaDadosPenalidadeTakeOrPay(String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade)
			throws FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if ("periodicidadeTakeOrPayC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoPeriodicidadeRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("referenciaQFParadaProgramadaC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoReferenciaQFParadaProgramadaC(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("consumoReferenciaTakeOrPayC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoConsumoReferenciaRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("margemVariacaoTakeOrPayC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoMargemVariacaoTakeOrPayC(valorPadrao, isValidar, isAtributoComErro, contratoPenalidade);
		} else if ("precoCobrancaC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoPrecoCobranca(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("tipoApuracaoC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoTipoApuracao(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("consideraParadaProgramada".equals(campoAdicionado)) {
			populaCampoConsideraParadaProgramada(valorPadrao, contratoPenalidade);
		} else if ("consideraFalhaFornecimento".equals(campoAdicionado)) {
			populaCampoConsideraFalhaFornecimento(valorPadrao, contratoPenalidade);
		} else if ("consideraCasoFortuito".equals(campoAdicionado)) {
			populaCampoConsideraCasoFortuito(valorPadrao, contratoPenalidade);
		}
		return isAtrComErro;
	}

	private void populaCampoConsideraCasoFortuito(String valorPadrao, ContratoPenalidade contratoPenalidade) {
		if(contratoPenalidade.getConsideraCasoFortuito()==null){
			boolean consideraCasoFortuito = Boolean.parseBoolean(valorPadrao);
			contratoPenalidade.setConsideraCasoFortuito(consideraCasoFortuito);
		}
	}

	private void populaCampoConsideraFalhaFornecimento(String valorPadrao, ContratoPenalidade contratoPenalidade) {
		if(contratoPenalidade.getConsideraFalhaFornecimento()==null){
			boolean consideraFalhaFornecimento = Boolean.parseBoolean(valorPadrao);
			contratoPenalidade.setConsideraFalhaFornecimento(consideraFalhaFornecimento);
		}
	}

	private void populaCampoConsideraParadaProgramada(String valorPadrao, ContratoPenalidade contratoPenalidade) {
		if(contratoPenalidade.getConsideraParadaProgramada()==null){
			boolean consideraParadaProgramada = Boolean.parseBoolean(valorPadrao);
			contratoPenalidade.setConsideraParadaProgramada(consideraParadaProgramada);
		}
	}

	private boolean populaDadosPenalidadesRetiradaMaiorMenor(String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo,
			ControladorApuracaoPenalidade controladorPenalidade, ContratoPenalidade contratoPenalidade) throws NegocioException,
			FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if (PENALIDADE_RET_MAIOR_MENOR.equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoPenalidadeRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, controladorPenalidade,
							contratoPenalidade);
		} else if (ATRIBUTO_PERIODICIDADE_RET_MAIOR_MENOR.equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoPeriodicidadeRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		}
		if ("baseApuracaoRetMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoBaseApuracaoRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("precoCobrancaRetirMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoPrecoCobranca(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("tipoApuracaoRetirMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoTipoApuracao(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("percentualCobRetMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualCobRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, contratoPenalidade);
		} else if ("percentualCobIntRetMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualCobIntRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, contratoPenalidade);
		} else if ("consumoReferenciaRetMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoConsumoReferenciaRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo,
							contratoPenalidade);
		} else if ("percentualRetMaiorMenor".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualRetMaiorMenor(valorPadrao, isValidar, isAtributoComErro, contratoPenalidade);
		} else if ("indicadorImposto".equals(campoAdicionado)) {
			populaCampoContratoPenalidadeIndicadorImposto(valorPadrao, contratoPenalidade);
		}
		return isAtrComErro;
	}

	private void populaCampoContratoPenalidadeIndicadorImposto(String valorPadrao, ContratoPenalidade contratoPenalidade) {
		if(contratoPenalidade.getIndicadorImposto()==null){
			boolean indicadorImposto = Boolean.parseBoolean(valorPadrao);
			contratoPenalidade.setIndicadorImposto(indicadorImposto);
		}
	}

	private boolean populaDadosRetiradaMaiorMenor(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) throws GGASException {
		boolean isAtrComErro = isAtributoComErro;
		if ("percMinDuranteRetiradaQPNRC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoPercMinDuranteRetiradaQPNRC(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("percDuranteRetiradaQPNRC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoPercDuranteRetiradaQPNRC(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("percFimRetiradaQPNRC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoPercFimRetiradaQPNRC(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("tempoValidadeRetiradaQPNRC".equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro = populaCampoTempoValidadeRetiradaQPNRC(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if (ATRIBUTO_PERIODICIDADE_RET_MAIOR_MENOR.equalsIgnoreCase(campoAdicionado)) {
			isAtrComErro =
					populaCampoPeriodicidadeRetMaiorMenor(contrato, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);
		}
		return isAtrComErro;
	}

	private boolean populaDadosPagamentoEfetuadoEmAtraso(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorTarifa controladorTarifa) throws FormatoInvalidoException, NegocioException {
		boolean isAtrComErro = isAtributoComErro;
		if ("indicadorMultaAtraso".equals(campoAdicionado)) {
			populaCampoIndicadorMulta(contrato, valorPadrao);
		} else if ("percentualMulta".equals(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualMulta(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("indicadorJurosMora".equals(campoAdicionado)) {
			populaCampoIndicadorJuros(contrato, valorPadrao);
		} else if ("percentualJurosMora".equals(campoAdicionado)) {
			isAtrComErro = populaCampoPercentualJurosMora(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("indiceCorrecaoMonetaria".equals(campoAdicionado)) {
			isAtrComErro = populaCampoIndiceCorrecaoMonetaria(contrato, valorPadrao, isValidar, isAtributoComErro, controladorTarifa);
		}
		return isAtrComErro;
	}

	private void populaCampoIndicadorJuros(Contrato contrato, String valorPadrao) {
		if(contrato.getIndicadorJuros()==null){
			boolean indicadorJuros = Boolean.parseBoolean(valorPadrao);
			contrato.setIndicadorJuros(indicadorJuros);
		}
	}

	private void populaCampoIndicadorMulta(Contrato contrato, String valorPadrao) {
		if(contrato.getIndicadorMulta()==null){
			boolean indicadorMulta = Boolean.parseBoolean(valorPadrao);
			contrato.setIndicadorMulta(indicadorMulta);
		}
	}

	private boolean populaDadosContrato(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) throws NegocioException {
		boolean isAtrComErro = isAtributoComErro;
		if ("situacaoContrato".equals(campoAdicionado)) {
			isAtrComErro = populaCampoSituacaoContrato(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if (RENOVACAO_AUTOMATICA.equals(campoAdicionado)) {
			populaCampoRenovacaoAutomatica(contrato, valorPadrao);
		} else if (NUM_DIAS_RENO_AUTO_CONTRATO.equals(campoAdicionado)) {
			isAtrComErro = populaCampoNumDiasRenoAutoContrato(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("tempoAntecedenciaRenovacao".equals(campoAdicionado)) {
			isAtrComErro = populaCampoTempoAntecedenciaRenovacao(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("formaCobranca".equals(campoAdicionado)) {
			isAtrComErro = populaCampoFormaCobranca(contrato, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);
		} else if ("diaVencFinanciamento".equals(campoAdicionado)) {
			isAtrComErro = populaCampoDiaVencFinanciamento(contrato, valorPadrao, isValidar, isAtributoComErro);
		}
		return isAtrComErro;
	}

	private void populaCampoRenovacaoAutomatica(Contrato contrato, String valorPadrao) {
		if(!contrato.getRenovacaoAutomatica()){
			boolean renovacaoAutomatica = Boolean.parseBoolean(valorPadrao);
			contrato.setRenovacaoAutomatica(renovacaoAutomatica);
		}
	}

	private boolean populaDadosFaturaAgrupada(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) {
		boolean isAtrComErro = isAtributoComErro;
		if ("tipoAgrupamento".equals(campoAdicionado)) {
			isAtrComErro = populaCampoTipoAgrupamento(contrato, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);
		} else if ("emissaoFaturaAgrupada".equals(campoAdicionado) && contrato.getAgrupamentoConta() == null) {
			boolean agrupamentoConta = Boolean.parseBoolean(valorPadrao);
			contrato.setAgrupamentoConta(agrupamentoConta);
		}
		return isAtrComErro;
	}

	private boolean populaDadosGarantiaFinanceira(Contrato contrato, String campoAdicionado, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) throws FormatoInvalidoException {
		boolean isAtrComErro = isAtributoComErro;
		if ("valorGarantiaFinanceira".equals(campoAdicionado)) {
			isAtrComErro = populaCampoValorGarantiaFinanceira(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("descGarantiaFinanc".equals(campoAdicionado)) {
			isAtrComErro = populaCampoDescGarantiaFinanc(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("tipoGarantiaFinanceira".equals(campoAdicionado)) {
			isAtrComErro =
					populaCampoTipoGarantiaFinanceira(contrato, valorPadrao, isValidar, isAtributoComErro, controladorEntidadeConteudo);
		} else if ("garantiaFinanceiraRenovada".equals(campoAdicionado)) {
			populaCampoGarantiaFinanceiraRenovada(contrato, valorPadrao, isValidar);
		} else if ("tempoAntecRevisaoGarantias".equals(campoAdicionado)) {
			isAtrComErro = populaCampoTempoAntecRevisaoGarantias(contrato, valorPadrao, isValidar, isAtributoComErro);
		} else if ("periodicidadeReavGarantias".equals(campoAdicionado)) {
			isAtrComErro = populaCampoPeriodicidadeReavGarantias(contrato, valorPadrao, isValidar, isAtributoComErro);
		}
		return isAtrComErro;
	}

	private boolean populaCampoApuracaoFalhaFornecimentoC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getApuracaoFalhaFornecimento() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getApuracaoFalhaFornecimento() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo apuracaoFalhaFornecimentoC = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setApuracaoFalhaFornecimento(apuracaoFalhaFornecimentoC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoApuracaoCasoFortuitoC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getApuracaoCasoFortuito() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getApuracaoCasoFortuito() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo apuracaoCasoFortuitoC = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setApuracaoCasoFortuito(apuracaoCasoFortuitoC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoApuracaoParadaProgramadaC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getApuracaoParadaProgramada() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getApuracaoParadaProgramada() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo apuracaoParadaProgramadaC = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setApuracaoParadaProgramada(apuracaoParadaProgramadaC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualNaoRecuperavelC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPenalidade contratoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPenalidade.getPercentualNaoRecuperavel() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getPercentualNaoRecuperavel() == null) {
			BigDecimal percentualNaoRecuperavelC = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPenalidade.setPercentualNaoRecuperavel(percentualNaoRecuperavelC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoMargemVariacaoTakeOrPayC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPenalidade contratoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPenalidade.getPercentualMargemVariacao() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getPercentualMargemVariacao() == null) {
			BigDecimal margemVariacaoTakeOrPayC = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPenalidade.setPercentualMargemVariacao(margemVariacaoTakeOrPayC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoReferenciaQFParadaProgramadaC(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getReferenciaQFParadaProgramada() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getReferenciaQFParadaProgramada() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo referenciaQFParadaProgramada = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setReferenciaQFParadaProgramada(referenciaQFParadaProgramada);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPenalidade contratoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPenalidade.getValorPercentualRetMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getValorPercentualRetMaiorMenor() == null) {
			BigDecimal percentualRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPenalidade.setValorPercentualRetMaiorMenor(percentualRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoConsumoReferenciaRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getConsumoReferencia() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getConsumoReferencia() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo consumoReferenciaRetMaiorMenor = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setConsumoReferencia(consumoReferenciaRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualCobIntRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPenalidade contratoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPenalidade.getValorPercentualCobIntRetMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getValorPercentualCobIntRetMaiorMenor() == null) {
			BigDecimal percentualCobIntRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPenalidade.setValorPercentualCobIntRetMaiorMenor(percentualCobIntRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualCobRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ContratoPenalidade contratoPenalidade) throws FormatoInvalidoException {
		if (isValidar && contratoPenalidade.getValorPercentualCobRetMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getValorPercentualCobRetMaiorMenor() == null) {
			BigDecimal percentualCobRetMaiorMenor = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contratoPenalidade.setValorPercentualCobRetMaiorMenor(percentualCobRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoTipoApuracao(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getTipoApuracao() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getTipoApuracao() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo tipoApuracao = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setTipoApuracao(tipoApuracao);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPrecoCobranca(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getPrecoCobranca() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getPrecoCobranca() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo precoCobranca = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setPrecoCobranca(precoCobranca);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoBaseApuracaoRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getBaseApuracao() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getBaseApuracao() == null) {
			Long chave = Long.parseLong(valorPadrao);
			EntidadeConteudo baseApuracaoRetMaiorMenor = controladorEntidadeConteudo.obter(chave);
			contratoPenalidade.setBaseApuracao(baseApuracaoRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPeriodicidadeRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo, ContratoPenalidade contratoPenalidade) {
		if (isValidar && contratoPenalidade.getPeriodicidadePenalidade() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getPeriodicidadePenalidade() == null) {
			Long chavePeriodicidade = Long.parseLong(valorPadrao);
			EntidadeConteudo periodicidadeRetMaiorMenor = controladorEntidadeConteudo.obter(chavePeriodicidade);
			contratoPenalidade.setPeriodicidadePenalidade(periodicidadeRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPenalidadeRetMaiorMenor(String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorApuracaoPenalidade controladorPenalidade, ContratoPenalidade contratoPenalidade) throws NegocioException {
		if (isValidar && contratoPenalidade.getPenalidade() == null) {
			return true;
		} else if (!isValidar && contratoPenalidade.getPenalidade() == null) {
			Long chavePenalidade = Long.parseLong(valorPadrao);
			Penalidade penalidadeRetMaiorMenor = controladorPenalidade.obterPenalidade(chavePenalidade);
			contratoPenalidade.setPenalidade(penalidadeRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPeriodicidadeRetMaiorMenor(Contrato contrato, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro, ControladorEntidadeConteudo controladorEntidadeConteudo) {
		if (isValidar && contrato.getPeriodicidadeMaiorMenor() == null) {
			return true;
		} else if (!isValidar && contrato.getPeriodicidadeMaiorMenor() == null) {
			Long chavePeriodicidade = Long.parseLong(valorPadrao);
			EntidadeConteudo periodicidadeRetMaiorMenor = controladorEntidadeConteudo.obter(chavePeriodicidade);
			contrato.setPeriodicidadeMaiorMenor(periodicidadeRetMaiorMenor);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoTempoValidadeRetiradaQPNRC(Contrato contrato, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) throws GGASException {
		if (isValidar && contrato.getDataFimRetiradaQPNR() == null) {
			return true;
		} else if (!isValidar && contrato.getDataFimRetiradaQPNR() == null) {
			Date tempoValidadeRetiradaQPNRC = Util.converterCampoStringParaData("", valorPadrao, Constantes.FORMATO_DATA_BR);
			contrato.setDataFimRetiradaQPNR(tempoValidadeRetiradaQPNRC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercFimRetiradaQPNRC(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && contrato.getPercentualQDCFimContratoQPNR() == null) {
			return true;
		} else if (!isValidar && contrato.getPercentualQDCFimContratoQPNR() == null) {
			BigDecimal valorPercentualQDCFimContratoQPNR = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualQDCFimContratoQPNR(valorPercentualQDCFimContratoQPNR);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercDuranteRetiradaQPNRC(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && contrato.getPercentualQDCContratoQPNR() == null) {
			return true;
		} else if (!isValidar && contrato.getPercentualQDCContratoQPNR() == null) {
			BigDecimal valorPercDuranteRetiradaQPNRC = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualQDCContratoQPNR(valorPercDuranteRetiradaQPNRC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercMinDuranteRetiradaQPNRC(Contrato contrato, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) throws FormatoInvalidoException {
		if (isValidar && contrato.getPercentualMinimoQDCContratoQPNR() == null) {
			return true;
		} else if (!isValidar && contrato.getPercentualMinimoQDCContratoQPNR() == null) {
			BigDecimal valorpercMinDuranteRetiradaQPNRC = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualMinimoQDCContratoQPNR(valorpercMinDuranteRetiradaQPNRC);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoMultaRecisoria(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo) {
		if (isValidar && contrato.getMultaRecisoria() == null) {
			return true;
		} else if (!isValidar && contrato.getMultaRecisoria() == null) {
			Long chaveMultaRecisoria = Long.parseLong(valorPadrao);
			EntidadeConteudo multaRecisoria = controladorEntidadeConteudo.obter(chaveMultaRecisoria);
			contrato.setMultaRecisoria(multaRecisoria);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualSobreTariGas(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && contrato.getPercentualSobreTariGas() == null) {
			return true;
		} else if (!isValidar && contrato.getPercentualSobreTariGas() == null) {
			BigDecimal valorPercentualTarifaDoP = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualSobreTariGas(valorPercentualTarifaDoP);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualTarifaDoP(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && contrato.getPercentualTarifaDoP() == null) {
			return true;
		} else if (!isValidar && contrato.getPercentualTarifaDoP() == null) {
			BigDecimal valorPercentualTarifaDoP = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualTarifaDoP(valorPercentualTarifaDoP);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoIndiceCorrecaoMonetaria(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorTarifa controladorTarifa) throws NegocioException {
		if (isValidar && contrato.getIndiceFinanceiro() == null) {
			return true;
		} else if (!isValidar && contrato.getIndiceFinanceiro() == null) {
			Long chaveIndiceFinanceiro = Long.parseLong(valorPadrao);
			IndiceFinanceiro indiceFinanceiro = controladorTarifa.obterIndiceFinanceiro(chaveIndiceFinanceiro);
			contrato.setIndiceFinanceiro(indiceFinanceiro);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualJurosMora(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && (contrato.getIndicadorJuros() && contrato.getPercentualJurosMora() == null)) {
			return true;
		} else if (!isValidar && contrato.getPercentualJurosMora() == null) {
			BigDecimal percentualJurosMora = Util.converterCampoStringParaValorBigDecimal("", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualJurosMora(percentualJurosMora);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPercentualMulta(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && (contrato.getIndicadorMulta() && contrato.getPercentualMulta() == null)) {
			return true;
		} else if (!isValidar) {
			BigDecimal percentualMulta = Util.converterCampoStringParaValorBigDecimal("", valorPadrao, Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO).divide(BigDecimal.valueOf(PORCENTAGEM));
			contrato.setPercentualMulta(percentualMulta);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoPeriodicidadeReavGarantias(Contrato contrato, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) {
		if (isValidar && contrato.getPeriodicidadeReavaliacaoGarantias() == null) {
			return true;
		} else if (!isValidar && contrato.getPeriodicidadeReavaliacaoGarantias() == null) {
			Integer periodicidadeReavaliacaoGarantias = Integer.parseInt(valorPadrao);
			contrato.setPeriodicidadeReavaliacaoGarantias(periodicidadeReavaliacaoGarantias);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoTempoAntecRevisaoGarantias(Contrato contrato, String valorPadrao,
			boolean isValidar, boolean isAtributoComErro) {

		if (isValidar && contrato.getDiasAntecedenciaRevisaoGarantiaFinanceira() == null) {
			return true;
		} else if (!isValidar && contrato.getDiasAntecedenciaRevisaoGarantiaFinanceira() == null) {
			Integer tempoAntecRevisaoGarantias = Integer.parseInt(valorPadrao);
			contrato.setDiasAntecedenciaRevisaoGarantiaFinanceira(tempoAntecRevisaoGarantias);
		}
		return isAtributoComErro;
	}

	private void populaCampoGarantiaFinanceiraRenovada(Contrato contrato, String valorPadrao, boolean isValidar) {
		if (!isValidar && contrato.getRenovacaoGarantiaFinanceira()==null) {
			String garantiaFinanceiraRenovada = valorPadrao;
			if ("Renovável".equals(garantiaFinanceiraRenovada)) {
				contrato.setRenovacaoGarantiaFinanceira(Boolean.TRUE);
			} else if ("Renovada".equals(garantiaFinanceiraRenovada)) {
				contrato.setRenovacaoGarantiaFinanceira(Boolean.FALSE);
			}
		}
	}

	private boolean populaCampoTipoGarantiaFinanceira(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo) {
		if (isValidar && contrato.getGarantiaFinanceira() == null) {
			return true;
		} else if (!isValidar  && contrato.getGarantiaFinanceira() == null) {
			Long chaveGarantiaFinanceira = Long.parseLong(valorPadrao);
			EntidadeConteudo garantiaFinanceira = controladorEntidadeConteudo.obter(chaveGarantiaFinanceira);
			contrato.setGarantiaFinanceira(garantiaFinanceira);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoDescGarantiaFinanc(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro) {
		if (isValidar && contrato.getDescricaoGarantiaFinanceira() == null) {
			return true;
		} else if (!isValidar && contrato.getDescricaoGarantiaFinanceira() == null) {
			String descricaoGarantiaFinanceira = valorPadrao;
			contrato.setDescricaoGarantiaFinanceira(descricaoGarantiaFinanceira);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoValorGarantiaFinanceira(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws FormatoInvalidoException {
		if (isValidar && contrato.getValorGarantiaFinanceira() == null) {
			return true;
		} else if (!isValidar && contrato.getValorGarantiaFinanceira() == null) {
			BigDecimal valorGarantiaFinanceira = Util.converterCampoStringParaValorBigDecimal("Valor Garantia", valorPadrao,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			contrato.setValorGarantiaFinanceira(valorGarantiaFinanceira);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoDiaVencFinanciamento(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro) {
		if (isValidar && contrato.getNumeroDiaVencimentoFinanciamento() == null) {
			return true;
		} else if (!isValidar && contrato.getNumeroDiaVencimentoFinanciamento() == null) {
			Integer numeroDiaVencimentoFinanciamento = Integer.parseInt(valorPadrao);
			contrato.setNumeroDiaVencimentoFinanciamento(numeroDiaVencimentoFinanciamento);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoFormaCobranca(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo) throws NegocioException {
		boolean isAtrComErro = isAtributoComErro;
		if (isValidar && contrato.getFormaCobranca() == null) {
			isAtrComErro = true;
		} else if (!isValidar && contrato.getFormaCobranca() == null) {
			Long chaveFormaCobranca = Long.parseLong(valorPadrao);
			EntidadeConteudo formaCobranca = controladorEntidadeConteudo.obter(chaveFormaCobranca);
			contrato.setFormaCobranca(formaCobranca);
		}
		isAtrComErro = populaCampoArrecadadorConvenio(contrato, isValidar, isAtrComErro);
		return isAtrComErro;
	}

	private boolean populaCampoArrecadadorConvenio(Contrato contrato, boolean isValidar, boolean isAtrComErro) throws NegocioException {
		if (isValidar && contrato.getArrecadadorContratoConvenio() == null) {
			return true;
		} else if (!isValidar && contrato.getArrecadadorContratoConvenio() == null) {
			ControladorArrecadadorConvenio controladorArrecadadorConvenio =
							ServiceLocator.getInstancia().getControladorArrecadadorConvenio();
			ArrecadadorContratoConvenio arrecadadorContratoConvenio = null;
			if (contrato.getArrecadadorContratoConvenio() != null) {
				arrecadadorContratoConvenio =
								controladorArrecadadorConvenio.obterArrecadadorConvenio(
												contrato.getArrecadadorContratoConvenio().getChavePrimaria());
			} else {
				arrecadadorContratoConvenio =
								controladorArrecadadorConvenio.obterArrecadadorContratoConvenioPadrao();

			}
			contrato.setArrecadadorContratoConvenio(arrecadadorContratoConvenio);
		}
		return isAtrComErro;
	}

	private boolean populaCampoTipoAgrupamento(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro,
			ControladorEntidadeConteudo controladorEntidadeConteudo) {
		if (isValidar
						&& (contrato.getAgrupamentoCobranca() != null && contrato.getAgrupamentoCobranca() && contrato
										.getTipoAgrupamento() == null)) {
			return true;
		} else if (!isValidar && contrato.getTipoAgrupamento() == null) {
			Long chaveTipoAgrupamento = Long.parseLong(valorPadrao);
			EntidadeConteudo tipoAgrupamento = controladorEntidadeConteudo.obter(chaveTipoAgrupamento);
			contrato.setTipoAgrupamento(tipoAgrupamento);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoTempoAntecedenciaRenovacao(Contrato contrato, String valorPadrao, boolean isValidar,
			boolean isAtributoComErro) {
		if (isValidar && contrato.getDiasAntecedenciaRenovacao() == null) {
			return true;
		} else if (!isValidar && contrato.getDiasAntecedenciaRenovacao() == null) {
			Integer diasAntecedenciaRenovacao = Integer.parseInt(valorPadrao);
			contrato.setDiasAntecedenciaRenovacao(diasAntecedenciaRenovacao);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoNumDiasRenoAutoContrato(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro) {
		if (isValidar && contrato.getNumeroDiasRenovacaoAutomatica() == null) {
			return true;
		} else if (!isValidar && contrato.getNumeroDiasRenovacaoAutomatica() == null) {
			Integer numeroDiasRenovacaoAutomatica = Integer.parseInt(valorPadrao);
			contrato.setNumeroDiasRenovacaoAutomatica(numeroDiasRenovacaoAutomatica);
		}
		return isAtributoComErro;
	}

	private boolean populaCampoSituacaoContrato(Contrato contrato, String valorPadrao, boolean isValidar, boolean isAtributoComErro)
			throws NegocioException {
		if (isValidar && contrato.getSituacao() == null) {
			return true;
		} else if (!isValidar && contrato.getSituacao() == null) {
			Long chaveSituacaoContrato = Long.parseLong(valorPadrao);
			SituacaoContrato situacaoContrato = this.obterSituacaoContrato(chaveSituacaoContrato);
			contrato.setSituacao(situacaoContrato);
		}
		return isAtributoComErro;
	}

	/**
	 * Remover dados aba geral.
	 *
	 * @param contrato the contrato
	 * @param nomeCampo the nome campo
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private void removerDadosAbaGeral(Contrato contrato, String nomeCampo) throws NegocioException {

		if (NUMERO_CONTRATO.equals(nomeCampo)) {
			contrato.setNumeroAnterior(null);
		} else if ("numeroEmpenho".equals(nomeCampo)) {
			contrato.setNumeroEmpenho(null);
		} else if ("dataVencObrigacoesContratuais".equals(nomeCampo)) {
			contrato.setDataVencimentoObrigacoes(null);
		} else if (RENOVACAO_AUTOMATICA.equals(nomeCampo)) {
			contrato.setRenovacaoAutomatica(false);
		} else if ("valorContrato".equals(nomeCampo)) {
			contrato.setValorContrato(null);
		} else if (NUM_DIAS_RENO_AUTO_CONTRATO.equals(nomeCampo)) {
			contrato.setNumeroDiasRenovacaoAutomatica(null);
		} else if ("tempoAntecedenciaRenovacao".equals(nomeCampo)) {
			contrato.setDiasAntecedenciaRenovacao(null);
		} else if ("numeroProposta".equals(nomeCampo)) {
			contrato.setProposta(null);
		} else if ("gastoEstimadoGNMes".equals(nomeCampo)) {
			contrato.setValorGastoMensal(null);
		} else if ("economiaEstimadaGNMes".equals(nomeCampo)) {
			contrato.setMedidaEconomiaMensalGN(null);
		} else if ("economiaEstimadaGNAno".equals(nomeCampo)) {
			contrato.setMedidaEconomiaAnualGN(null);
		} else if ("descontoEfetivoEconomia".equals(nomeCampo)) {
			contrato.setPercentualEconomiaGN(null);
		} else if ("emissaoFaturaAgrupada".equals(nomeCampo)) {
			contrato.setAgrupamentoConta(false);
		} else if ("diaVencFinanciamento".equals(nomeCampo)) {
			contrato.setNumeroDiaVencimentoFinanciamento(null);
		} else if ("valorInvestimento".equals(nomeCampo)) {
			contrato.setValorInvestimento(null);
		} else if ("dataInvestimento".equals(nomeCampo)) {
			contrato.setDataInvestimento(null);
		} else if ("valorParticipacaoCliente".equals(nomeCampo)) {
			contrato.setValorParticipacaoCliente(null);
		} else if ("qtdParcelasFinanciamento".equals(nomeCampo)) {
			contrato.setQtdParcelasFinanciamento(null);
		} else if ("percentualJurosFinanciamento".equals(nomeCampo)) {
			contrato.setPercentualJurosFinanciamento(null);
		} else if ("sistemaAmortizacao".equals(nomeCampo)) {
			contrato.setSistemaAmortizacao(null);
		} else if ("valorGarantiaFinanceira".equals(nomeCampo)) {
			contrato.setValorGarantiaFinanceira(null);
		} else if ("descGarantiaFinanc".equals(nomeCampo)) {
			contrato.setDescricaoGarantiaFinanceira(null);
		} else if ("tipoGarantiaFinanceira".equals(nomeCampo)) {
			contrato.setGarantiaFinanceira(null);
		} else if ("garantiaFinanceiraRenovada".equals(nomeCampo)) {
			contrato.setRenovacaoGarantiaFinanceira(null);
		} else if ("dataInicioGarantiaFinanceira".equals(nomeCampo)) {
			contrato.setDataInicioGarantiaFinanciamento(null);
		} else if ("dataFinalGarantiaFinanceira".equals(nomeCampo)) {
			contrato.setDataFimGarantiaFinanciamento(null);
		} else if ("tempoAntecRevisaoGarantias".equals(nomeCampo)) {
			contrato.setDiasAntecedenciaRevisaoGarantiaFinanceira(null);
		} else if ("periodicidadeReavGarantias".equals(nomeCampo)) {
			contrato.setPeriodicidadeReavaliacaoGarantias(null);
		} else if ("indicadorMultaAtraso".equals(nomeCampo)) {
			contrato.setIndicadorMulta(Boolean.FALSE);
		} else if ("percentualMulta".equals(nomeCampo)) {
			contrato.setPercentualMulta(null);
		} else if ("indicadorJurosMora".equals(nomeCampo)) {
			contrato.setIndicadorJuros(Boolean.FALSE);
		} else if ("percentualJurosMora".equals(nomeCampo)) {
			contrato.setPercentualJurosMora(null);
		} else if ("indiceCorrecaoMonetaria".equals(nomeCampo)) {
			contrato.setIndiceFinanceiro(null);
		} else if ("percentualTarifaDoP".equals(nomeCampo)) {
			contrato.setPercentualTarifaDoP(null);
		} else if ("percentualSobreTariGas".equals(nomeCampo)) {
			contrato.setPercentualSobreTariGas(null);
		} else if ("multaRecisoria".equals(nomeCampo)) {
			contrato.setMultaRecisoria(null);
		} else if ("QDCContrato".equals(nomeCampo)) {
			contrato.getListaContratoQDC().clear();
		} else if ("percMinDuranteRetiradaQPNRC".equals(nomeCampo)) {
			contrato.setPercentualMinimoQDCContratoQPNR(null);
		} else if ("percDuranteRetiradaQPNRC".equals(nomeCampo)) {
			contrato.setPercentualQDCContratoQPNR(null);
		} else if ("percFimRetiradaQPNRC".equals(nomeCampo)) {
			contrato.setPercentualQDCFimContratoQPNR(null);
		} else if ("tempoValidadeRetiradaQPNRC".equals(nomeCampo)) {
			contrato.setDataFimRetiradaQPNR(null);
		} else if (PENALIDADE_RET_MAIOR_MENOR.equals(nomeCampo)) {

			Collection<ContratoPenalidade> listaContratoPenalidade = contrato.getListaContratoPenalidade();

			for (Iterator<ContratoPenalidade> iterator = listaContratoPenalidade.iterator(); iterator.hasNext();) {

				ContratoPenalidade contratoPenalidade = iterator.next();

				if (getControladorConstanteSistema().isContratoPenalidadeRetiradaMaiorMenor(contratoPenalidade)) {

					iterator.remove();
				}
			}

		} else if ("periodicidadeTakeOrPayC".equals(nomeCampo)) {

			Collection<ContratoPenalidade> listaContratoPenalidade = contrato.getListaContratoPenalidade();

			for (Iterator<ContratoPenalidade> iterator = listaContratoPenalidade.iterator(); iterator.hasNext();) {

				ContratoPenalidade contratoPenalidade = iterator.next();

				if (getControladorConstanteSistema().isContratoPenalidadeTakeOrPay(contratoPenalidade)) {
					iterator.remove();
				}
			}

		}

	}

	private ControladorConstanteSistema getControladorConstanteSistema() {

		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	/**
	 * Remover dados aba principal.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private void removerDadosAbaPrincipal(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo) {

		if ("numeroDiasGarantia".equals(nomeCampo)) {
			contratoPontoConsumo.setDiaGarantiaConversao(null);
		} else if ("inicioGarantiaConversao".equals(nomeCampo)) {
			contratoPontoConsumo.setDataInicioGarantiaConversao(null);
		} else if ("periodoTesteDataInicio".equals(nomeCampo)) {
			contratoPontoConsumo.setDataInicioTeste(null);
		} else if ("periodoTesteDataFim".equals(nomeCampo)) {
			contratoPontoConsumo.setDataFimTeste(null);
		} else if ("volumaTeste".equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaConsumoTeste(null);
		} else if ("prazoTeste".equals(nomeCampo)) {
			contratoPontoConsumo.setPrazoConsumoTeste(null);
		} else if ("faxDDD".equals(nomeCampo)) {
			contratoPontoConsumo.setCodigoDDDFaxOperacional(null);
		} else if ("faxNumero".equals(nomeCampo)) {
			contratoPontoConsumo.setNumeroFaxOperacional(null);
		} else if ("email".equals(nomeCampo)) {
			contratoPontoConsumo.setEmailOperacional(null);
		}
	}

	/**
	 * Remover dados aba dados tecnicos.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerDadosAbaDadosTecnicos(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo) {

		if (VAZAO_INSTANTANEA.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaVazaoInstantanea(null);
		} else if (UNIDADE_VAZAO_INSTAN.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeVazaoInstantanea(null);
		} else if (VAZAO_INSTANTANEA_MAXIMA.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaVazaoMaximaInstantanea(null);
		} else if (UNIDADE_VAZAO_INSTAN_MAXIMA.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeVazaoMaximaInstantanea(null);
		} else if (VAZAO_INSTANTANEA_MINIMA.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaVazaoMinimaInstantanea(null);
		} else if (UNIDADE_VAZAO_INSTAN_MINIMA.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeVazaoMinimaInstantanea(null);
		} else if (PRESSAO_MINIMA_FORNECIMENTO.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaPressaoMinima(null);
		} else if (UNIDADE_PRESSAO_MINIMA_FORNEC.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadePressaoMinima(null);
		} else if (PRESSAO_MAXIMA_FORNECIMENTO.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaPressaoMaxima(null);
		} else if (UNIDADE_PRESSAO_MAXIMA_FORNEC.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadePressaoMaxima(null);
		} else if (PRESSAO_LIMITE_FORNECIMENTO.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaPressaoLimite(null);
		} else if (UNIDADE_PRESSAO_LIMITE_FORNEC.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadePressaoLimite(null);
		} else if ("numAnosCtrlParadaCliente".equals(nomeCampo)) {
			contratoPontoConsumo.setQuantidadeAnosParadaCliente(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("maxTotalParadasCliente".equals(nomeCampo)) {
			contratoPontoConsumo.setQuantidadeTotalParadaCliente(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("maxAnualParadasCliente".equals(nomeCampo)) {
			contratoPontoConsumo.setQuantidadeMaximaAnoParadaCliente(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("numDiasProgrParadaCliente".equals(nomeCampo)) {
			contratoPontoConsumo.setDiasAntecedentesParadaCliente(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("numDiasConsecParadaCliente".equals(nomeCampo)) {
			contratoPontoConsumo.setDiasConsecutivosParadaCliente(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("numAnosCtrlParadaCDL".equals(nomeCampo)) {
			contratoPontoConsumo.setQuantidadeAnosParadaCDL(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("maxTotalParadasCDL".equals(nomeCampo)) {
			contratoPontoConsumo.setQuantidadeTotalParadaCDL(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("maxAnualParadasCDL".equals(nomeCampo)) {
			contratoPontoConsumo.setQuantidadeMaximaAnoParadaCDL(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("numDiasProgrParadaCDL".equals(nomeCampo)) {
			contratoPontoConsumo.setDiasAntecedentesParadaCDL(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		} else if ("numDiasConsecParadaCDL".equals(nomeCampo)) {
			contratoPontoConsumo.setDiasConsecutivosParadaCDL(null);
			contratoPontoConsumo.setIndicadorParadaProgramada(Boolean.FALSE);
		}
	}

	/**
	 * Remover dados aba consumo.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerDadosAbaConsumo(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo) {

		if ("horaInicialDia".equals(nomeCampo)) {
			contratoPontoConsumo.setNumeroHoraInicial(null);
		} else if (REGIME_CONSUMO.equals(nomeCampo)) {
			contratoPontoConsumo.setRegimeConsumo(null);
		} else if (FORNECIMENTO_MAXIMO_DIARIO.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaFornecimentoMaxDiaria(null);
		} else if (UNIDADE_FORNEC_MAXIMO_DIARIO.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeFornecimentoMaxDiaria(null);
		} else if (FORNECIMENTO_MINIMO_DIARIO.equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaFornecimentoMinDiaria(null);
		} else if (UNIDADE_FORNEC_MINIMO_DIARIO.equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeFornecimentoMinDiaria(null);
		} else if ("fornecimentoMinimoMensal".equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaFornecimentoMinMensal(null);
		} else if ("unidadeFornecMinimoMensal".equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeFornecimentoMinMensal(null);
		} else if ("fornecimentoMinimoAnual".equals(nomeCampo)) {
			contratoPontoConsumo.setMedidaFornecimentoMinAnual(null);
		} else if ("unidadeFornecMinimoAnual".equals(nomeCampo)) {
			contratoPontoConsumo.setUnidadeFornecimentoMinAnual(null);
		} else if ("consumoFatFalhaMedicao".equals(nomeCampo)) {
			contratoPontoConsumo.setAcaoAnormalidadeConsumoSemLeitura(null);
		} else if ("localAmostragemPCS".equals(nomeCampo)) {
			contratoPontoConsumo.getListaContratoPontoConsumoPCSAmostragem().clear();
			contratoPontoConsumo.getListaContratoPontoConsumoPCSIntervalo().clear();
		}

	}

	/**
	 * Remover dados aba faturamento.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerDadosAbaFaturamento(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo) {

		if ("endFisEntFatCEP".equals(nomeCampo)) {
			contratoPontoConsumo.setCep(null);
		} else if ("endFisEntFatNumero".equals(nomeCampo)) {
			contratoPontoConsumo.setNumeroImovel(null);
		} else if ("endFisEntFatComplemento".equals(nomeCampo)) {
			contratoPontoConsumo.setComplementoEndereco(null);
		} else if ("endFisEntFatEmail".equals(nomeCampo)) {
			contratoPontoConsumo.setEmailEntrega(null);
		}

	}

	/**
	 * Remover dados aba modalidades.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param nomeCampo
	 *            the nome campo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerDadosAbaModalidades(ContratoPontoConsumo contratoPontoConsumo, String nomeCampo) {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<ContratoPontoConsumoModalidade> listaModalidade = contratoPontoConsumo.getListaContratoPontoConsumoModalidade();

		if ("modalidade".equals(nomeCampo)) {
			listaModalidade.clear();
		} else {
			for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaModalidade) {
				if ("prazoRevizaoQDC".equals(nomeCampo)) {
					contratoPontoConsumoModalidade.setPrazoRevisaoQDC(null);
				} else if (QDS_MAIOR_QDC.equals(nomeCampo)) {
					contratoPontoConsumoModalidade.setIndicadorQDSMaiorQDC(null);
					contratoPontoConsumoModalidade.setPercentualVarSuperiorQDC(null);
					contratoPontoConsumoModalidade.setDiasAntecedenciaQDS(null);
					contratoPontoConsumoModalidade.setMesesQDS(null);
					contratoPontoConsumoModalidade.setIndicadorConfirmacaoAutomaticaQDS(Boolean.FALSE);
				} else if ("dataInicioRetiradaQPNR".equals(nomeCampo)) {
					contratoPontoConsumoModalidade.setDataInicioRetiradaQPNR(null);
					contratoPontoConsumoModalidade.setDataFimRetiradaQPNR(null);
					contratoPontoConsumoModalidade.setPercentualQDCContratoQPNR(null);
					contratoPontoConsumoModalidade.setPercentualQDCFimContratoQPNR(null);
					contratoPontoConsumoModalidade.setAnosValidadeRetiradaQPNR(null);
				} else if ("periodicidadeTakeOrPay".equals(nomeCampo)) {
					this.removerDadosListaPenalidadesPorTipoPenalidade(Constantes.C_PENALIDADE_TAKE_OR_PAY,
									contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade());
				} else if ("periodicidadeShipOrPay".equals(nomeCampo)) {
					this.removerDadosListaPenalidadesPorTipoPenalidade(Constantes.C_PENALIDADE_SHIP_OR_PAY,
									contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade());
				} else if (PENALIDADE_RET_MAIOR_MENOR_M.equals(nomeCampo)) {
					this.removerDadosListaPenalidadesPorTipoPenalidade(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR,
									contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade());
					this.removerDadosListaPenalidadesPorTipoPenalidade(Constantes.C_PENALIDADE_RETIRADA_A_MENOR,
									contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade());
				} else if ("qdc".equals(nomeCampo) || "dataVigenciaQDC".equals(nomeCampo)) {
					contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC().clear();
				}

			}
		}

	}

	/**
	 * Remover dados aba responsabilidade.
	 *
	 * @param contrato
	 *            the contrato
	 * @param nomeCampo
	 *            the nome campo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerDadosAbaResponsabilidade(Contrato contrato, String nomeCampo) {

		if (CLIENTE_RESPONSAVEL.equals(nomeCampo)) {
			contrato.getListaContratoCliente().clear();
		}

	}

	/**
	 * Remover dados lista penalidades por tipo penalidade.
	 *
	 * @param tipoPenalidade
	 *            the tipo penalidade
	 * @param listaPenalidade
	 *            the lista penalidade
	 */
	private void removerDadosListaPenalidadesPorTipoPenalidade(String tipoPenalidade,
					Collection<ContratoPontoConsumoPenalidade> listaPenalidade) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<ContratoPontoConsumoPenalidade> lista = new HashSet<>(listaPenalidade);

		for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaPenalidade) {
			String chavePenalidade = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(tipoPenalidade);
			Penalidade penalidade = contratoPontoConsumoPenalidade.getPenalidade();
			if (penalidade.getChavePrimaria() == Long.parseLong(chavePenalidade)) {
				lista.remove(contratoPontoConsumoPenalidade);
			}
		}
		lista.clear();
		lista.addAll(listaPenalidade);
	}

	/**
	 * Validar valor padrao.
	 *
	 * @param valorPadrao
	 *            the valor padrao
	 * @return true, if successful
	 */
	private boolean validarValorPadrao(String valorPadrao) {

		boolean retorno = false;

		if (valorPadrao != null && !valorPadrao.isEmpty() && !"-1".equals(valorPadrao)) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoPontoConsumoPorCliente(java.lang.Long, java.lang.String[])
	 */
	@Override
	public Collection<ContratoPontoConsumo> obterContratoPontoConsumoPorCliente(Long idCliente, String... propriedadesLazy)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		ServiceLocator.getInstancia().getControladorParametroSistema();

		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(WHERE);
		hql.append(" contratoPonto.contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria in(:idSituacao) ");
		hql.append(" and contratoPonto.contrato.habilitado = :habilitado");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_CLIENTE, idCliente);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoAtivo = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		Long idSituacaoEmNegociacao = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo
				(Constantes.C_CONTRATO_EM_NEGOCIACAO));
		Long idSituacaoAguardandoAprovacao = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo
				(Constantes.C_CONTRATO_AGUARDANDO_APROVACAO));
		query.setParameterList(ID_SITUACAO, Arrays.asList(idSituacaoAguardandoAprovacao, idSituacaoAtivo, idSituacaoEmNegociacao));
		query.setParameter(HABILITADO, Boolean.TRUE);
		Collection<Object> lista = query.list();

		inicializarLazyColecao(lista, propriedadesLazy);

		return (Collection<ContratoPontoConsumo>) (Collection<?>) lista;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPontoConsumoModalidadePorHora(java.lang.Integer)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ContratoPontoConsumoModalidade> listarContratoPontoConsumoModalidadePorHora(Integer hora) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
		hql.append(CONTRATO_PONTO_MODALIDADE);
		hql.append(" inner join fetch contratoPontoModalidade.contratoModalidade ");
		hql.append(" inner join fetch contratoPontoModalidade.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join fetch contratoPontoConsumo.pontoConsumo ");
		hql.append(" left join fetch contratoPontoModalidade.listaContratoPontoConsumoModalidadeQDC ");
		hql.append(WHERE);
		hql.append("  contratoPontoModalidade.indicadorProgramacaoConsumo = true ");
		hql.append(" and contratoPontoModalidade.habilitado = true ");
		if (hora != null) {

			hql.append(" and (contratoPontoModalidade.horaLimiteAceitacaoDiaria <= :hora ");
			hql.append("  or contratoPontoModalidade.horaLimiteAceitIntradiaria <= :hora )");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (hora != null) {
			query.setInteger("hora", hora);
		}

		List<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidadeRepetido = query.list();
		List<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new ArrayList<>();

		for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoConsumoModalidadeRepetido) {
			if (!listaContratoPontoConsumoModalidade.contains(contratoPontoConsumoModalidade)) {
				listaContratoPontoConsumoModalidade.add(contratoPontoConsumoModalidade);
			}
		}

		return listaContratoPontoConsumoModalidade;
	}

	/*
	 * Método responsável por
	 * retornar o QDC do contrato
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterQDCEstabelecidoContrato(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal obterQDCEstabelecidoContrato(PontoConsumo pontoConsumo, Date dataInicioVigencia, Date dataFimVigencia)
					throws NegocioException {

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		BigDecimal qdcContrato = null;
		ContratoPontoConsumo contratoPontoConsumo = this.obterContratoAtivoPontoConsumo(pontoConsumo);
		qdcContrato = controladorApuracaoPenalidade.obterQdcContratoPontoConsumoModalidade(
						contratoPontoConsumo.getListaContratoPontoConsumoModalidade(), dataInicioVigencia, dataFimVigencia);
		if ((qdcContrato) == null || (qdcContrato.intValue() == 0)) {
			qdcContrato = obterSomaQdcContratoPorPeriodo(contratoPontoConsumo.getContrato().getChavePrimaria(), dataInicioVigencia,
							dataFimVigencia);
		}

		return qdcContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarContratoEncerrado(java.lang.Long)
	 */
	@Override
	public void validarContratoEncerrado(Long idContrato) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String situacaoEncerradaContrato = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ENCERRADO);

		Contrato contrato = (Contrato) obter(idContrato);

		if (!(String.valueOf(contrato.getSituacao().getChavePrimaria()).equals(situacaoEncerradaContrato))) {
			throw new NegocioException(Constantes.ERRO_CONTRATO_NAO_ENCERRADO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarDatasContratoComplementar(java.lang.Long,
	 * br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarDatasContratoComplementar(Long chavePrimariaPrincipal, Contrato contrato) throws NegocioException {

		if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {
			Contrato contratoPrincipal = (Contrato) this.obter(chavePrimariaPrincipal);

			if (contrato != null) {

				validarDataVencimentoComplementar(contrato.getDataAssinatura(), contrato.getDataVencimentoObrigacoes(),
								contratoPrincipal.getDataAssinatura(), contratoPrincipal.getDataVencimentoObrigacoes());


			}
		}
	}

	/**
	 * Validar data vencimento complementar.
	 *
	 * @param dataAssinatura
	 *            the data assinatura
	 * @param dataVencimento
	 *            the data vencimento
	 * @param dataAssinaturaPrincipal
	 *            the data assinatura principal
	 * @param dataVencimentoPrincipal
	 *            the data vencimento principal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDataVencimentoComplementar(Date dataAssinatura, Date dataVencimento, Date dataAssinaturaPrincipal,
					Date dataVencimentoPrincipal) throws NegocioException {

		if ((dataAssinatura != null) && (dataVencimento != null) && (dataVencimento.compareTo(dataAssinatura) < 0)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA, true);
		}

		if ((dataAssinatura != null) && (dataVencimento != null && dataAssinaturaPrincipal != null && dataVencimentoPrincipal != null)
						&& (Util.compararDatas(dataAssinatura, dataAssinaturaPrincipal) < 0)
						|| (Util.compararDatas(dataVencimentoPrincipal, dataVencimento) < 0)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_MENOR_ASSINATURA_CONTRATO_COMPLEMENTAR, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPrincipalEComplementar(java.util.Map)
	 */
	@Override
	public Collection<Contrato> consultarContratoPrincipalEComplementar(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			Long chavePrimariaPrincipal = (Long) filtro.get(CHAVE_PRIMARIA_PRINCIPAL);
			Long chavePrimariaPontoConsumo = (Long) filtro.get("chavePrimariaPontoConsumo");

			if (chavePrimaria != null && chavePrimaria > 0 && (chavePrimariaPrincipal == null || chavePrimariaPrincipal == 0)) {
				Contrato contrato = (Contrato) super.obter(chavePrimaria);
				chavePrimariaPrincipal = contrato.getChavePrimariaPrincipal();
			}

			if (chavePrimariaPrincipal != null && chavePrimariaPrincipal > 0) {
				criteria.add(Restrictions.or(Restrictions.eq(CHAVE_PRIMARIA, chavePrimariaPrincipal),
								Restrictions.eq(CHAVE_PRIMARIA_PRINCIPAL, chavePrimariaPrincipal)));
			} else if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.or(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria),
								Restrictions.eq(CHAVE_PRIMARIA_PRINCIPAL, chavePrimaria)));
			}

			if (chavePrimariaPontoConsumo != null && chavePrimariaPontoConsumo > 0) {
				criteria.createAlias(LISTA_CONTRATO_PONTO_CONSUMO, LISTA_CONTRATO_PONTO_CONSUMO, Criteria.LEFT_JOIN);
				criteria.createAlias("listaContratoPontoConsumo.pontoConsumo", PONTO_CONSUMO, Criteria.LEFT_JOIN);
				criteria.createAlias("listaContratoPontoConsumo.listaContratoPontoConsumoModalidade", CONTRATO_PONTO_CONSUMO_MODALIDADE,
								Criteria.LEFT_JOIN);
				criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, chavePrimariaPontoConsumo));
			}

			criteria.addOrder(Order.asc("ordemFaturamento"));

		}

		criteria.createAlias(SITUACAO, SITUACAO);
		criteria.add(Restrictions.eq("situacao.chavePrimaria", SituacaoContrato.ATIVO));
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#calcularQtdeRetiradaComplementar(br.com.ggas.contrato.contrato.Contrato,
	 * java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal calcularQtdeRetiradaComplementar(Contrato contrato, Date dataInicial, Date dataFinal) throws NegocioException {

		BigDecimal qtdRetirada = BigDecimal.ZERO;

		if (contrato != null && contrato.getChavePrimariaPrincipal() != null && contrato.getListaContratoPontoConsumo() != null
						&& dataInicial != null && dataFinal != null) {

			Collection<Long> idsPonto = new ArrayList<>();
			for (ContratoPontoConsumo cpc : contrato.getListaContratoPontoConsumo()) {
				idsPonto.add(cpc.getPontoConsumo().getChavePrimaria());
			}

			Criteria criteria = getSession().createCriteria(HistoricoConsumo.class);

			criteria.add(Restrictions.eq("indicadorFaturamento", true));

			criteria.createCriteria("historicoAtual").add(Restrictions.between("dataLeituraFaturada", dataInicial, dataFinal));
			criteria.createCriteria(PONTO_CONSUMO).add(Restrictions.in(CHAVE_PRIMARIA, idsPonto));

			Collection<HistoricoConsumo> historicos = criteria.list();

			for (HistoricoConsumo historico : historicos) {
				if (historico.getHistoricoConsumoSintetizador() != null) {
					qtdRetirada = qtdRetirada.add(historico.getConsumoApurado());
				}
			}
			qtdRetirada = qtdRetirada.setScale(QUATRO_CASAS_DECIMAIS, RoundingMode.HALF_EVEN);

			Integer qtdDias = Util.intervaloDatas(dataInicial, dataFinal);

			BigDecimal qtdRetiradaComplementarVolumeReferencia = BigDecimal.ZERO;
			BigDecimal qtdRetiradaComplementarQDC = BigDecimal.ZERO;
			BigDecimal qdcContratoComplementar = this.obterQdcContrato(contrato.getDataAssinatura(), contrato.getChavePrimaria());

			if (qtdRetirada.compareTo(BigDecimal.ZERO) > 0) {
				qtdRetiradaComplementarVolumeReferencia = qtdRetirada.subtract(contrato.getVolumeReferencia().multiply(
								BigDecimal.valueOf(qtdDias)));
			}
			if (qdcContratoComplementar != null) {
				qtdRetiradaComplementarQDC = qdcContratoComplementar.multiply(BigDecimal.valueOf(qtdDias));
			}

			if (qtdRetiradaComplementarVolumeReferencia.compareTo(qtdRetiradaComplementarQDC) > 0) {
				qtdRetirada = qtdRetiradaComplementarQDC;
			} else {
				qtdRetirada = qtdRetiradaComplementarVolumeReferencia;
			}

		}

		return qtdRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterSomaQdcContrato(br.com.ggas.contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public BigDecimal obterSomaQdcContrato(ContratoPontoConsumo contratoPontoConsumo, Date dataInicioVigencia) throws NegocioException {
		validarNulidadeArgumentos(contratoPontoConsumo, dataInicioVigencia);
		PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
		Contrato contrato = contratoPontoConsumo.getContrato();
		Collection<ContratoPontoConsumoModalidade> lista = this.consultarContratoPontoConsumoModalidades(contrato, pontoConsumo);
		if(lista.isEmpty()) {
			ContratoPontoConsumoModalidade modalidadePadrao = criarModalidadePadrao(contrato);
			lista.add(modalidadePadrao);
		}
		return calcularSomatorioQDCModalidades(lista, dataInicioVigencia);
	}

	private void validarNulidadeArgumentos(ContratoPontoConsumo contratoPontoConsumo, Date dataInicioVigencia) throws NegocioException {
		if(contratoPontoConsumo == null) {
			throw new NegocioException("O ponto de consumo não pode ser vazio.");
		} else if(dataInicioVigencia == null) {
			throw new NegocioException("A data de inicio da vigencia da QDC não pode ser vazia.");
		}
	}

	@SuppressWarnings("unchecked")
	private ContratoPontoConsumoModalidade criarModalidadePadrao(Contrato contrato) {
		Collection<ContratoQDC> listaContratoQDC = contrato.getListaContratoQDC();
		Transformer transformadorBeansQDC = criarTransformadorBeansQDC();
		Collection<ContratoPontoConsumoModalidadeQDC> listaContratoPontoConsumoModalidadeQDC =
						CollectionUtils.collect(listaContratoQDC, transformadorBeansQDC);
		ContratoPontoConsumoModalidade modalidadePadrao =
						(ContratoPontoConsumoModalidade) criarContratoPontoConsumoModalidade();
		modalidadePadrao.setListaContratoPontoConsumoModalidadeQDC(listaContratoPontoConsumoModalidadeQDC);
		return modalidadePadrao;
	}

	private Transformer criarTransformadorBeansQDC() {
		return  new Transformer() {
			@Override
			public Object transform(Object input) {
				ContratoQDC contratoQDC = (ContratoQDC) input;
				ContratoPontoConsumoModalidadeQDC modalidadeQDC = (ContratoPontoConsumoModalidadeQDC) criarContratoPontoConsumoModalidadeQDC();
				modalidadeQDC.setMedidaVolume(contratoQDC.getMedidaVolume());
				modalidadeQDC.setDataVigencia(contratoQDC.getData());
				return modalidadeQDC;
			}
		};
	}

	private BigDecimal calcularSomatorioQDCModalidades(Collection<ContratoPontoConsumoModalidade> lista, Date dataInicioVigencia) {
		BigDecimal somatorioQDC = BigDecimal.ZERO;
		for (ContratoPontoConsumoModalidade cpcModalidade : lista) {
			somatorioQDC = somatorioQDC.add(calcularValorQDCModalidade(cpcModalidade, dataInicioVigencia));
		}
		return somatorioQDC;
	}

	private BigDecimal calcularValorQDCModalidade(ContratoPontoConsumoModalidade cpcModalidade, Date dataInicioVigencia) {
		NavigableSet<ContratoPontoConsumoModalidadeQDC> qdcsOrdenadasVigencia = criarConjuntoQDCsOrdenadasVigencia(cpcModalidade);
		ContratoPontoConsumoModalidadeQDC qdcBusca = (ContratoPontoConsumoModalidadeQDC) criarContratoPontoConsumoModalidadeQDC();
		qdcBusca.setDataVigencia(dataInicioVigencia);
		ContratoPontoConsumoModalidadeQDC qdcSelecionada = qdcsOrdenadasVigencia.floor(qdcBusca);
		return qdcSelecionada.getMedidaVolume();
	}

	private NavigableSet<ContratoPontoConsumoModalidadeQDC> criarConjuntoQDCsOrdenadasVigencia(ContratoPontoConsumoModalidade cpcModalidade) {
		Comparator<? super ContratoPontoConsumoModalidadeQDC> comparador = criarComparadorContratoPontoConsumoModalidadeQDC();
		NavigableSet<ContratoPontoConsumoModalidadeQDC> qdcsOrdenadasVigencia = new TreeSet<>(comparador);
		qdcsOrdenadasVigencia.addAll(cpcModalidade.getListaContratoPontoConsumoModalidadeQDC());
		return qdcsOrdenadasVigencia;
	}

	private Comparator<ContratoPontoConsumoModalidadeQDC> criarComparadorContratoPontoConsumoModalidadeQDC() {
		return new Comparator<ContratoPontoConsumoModalidadeQDC>() {

			@Override
			public int compare(ContratoPontoConsumoModalidadeQDC o1, ContratoPontoConsumoModalidadeQDC o2) {
				Date dataVigencia = o1.getDataVigencia();
				Date dataVigencia2 = o2.getDataVigencia();
				return dataVigencia.compareTo(dataVigencia2);
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarDadosContratoComplementar(br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void validarDadosContratoComplementar(Contrato contrato) throws NegocioException {

		// validar apenas se for um contrato complementar
		if (contrato.getChavePrimariaPrincipal() != null) {
			// Ainda será implementado, Por favor apagar este comentário após a implementação.
		}

	}

	/**
	 * Validar adicao penalidade contrato.
	 *
	 * @param contratoPontoConsumoPenalidade
	 *            the contrato ponto consumo penalidade
	 * @param listaContratoPontoConsumoPenalidade
	 *            the lista contrato ponto consumo penalidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarAdicaoPenalidadeContrato(ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade,
					Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) throws NegocioException {

		if (contratoPontoConsumoPenalidade != null && contratoPontoConsumoPenalidade.getPercentualMargemVariacao() != null
						&& contratoPontoConsumoPenalidade.getPercentualMargemVariacao().compareTo(BigDecimal.ONE) > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
							new Object[] {ContratoPontoConsumoPenalidade.PERCENTUAL_MARGEM_VARIACAO});
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#criarContratoPenalidade()
	 */
	@Override
	public EntidadeNegocio criarContratoPenalidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContratoPenalidade.BEAN_ID_CONTRATO_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorContrato#validarAdicaoContratoPenalidade(br.com.ggas.contrato.contrato.ContratoPenalidade,
	 * java.util.Collection)
	 */
	@Override
	public void validarAdicaoContratoPenalidade(ContratoPenalidade contratoPenalidade,
					Collection<ContratoPenalidade> listaContratoPenalidade) throws NegocioException {

		if (contratoPenalidade != null && (contratoPenalidade.getDataInicioVigencia() == null)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTRATO_DATA_INICIO_VIGENCIA_TOP, true);
		}

		if (contratoPenalidade != null && (contratoPenalidade.getDataFimVigencia() == null)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTRATO_DATA_FIM_VIGENCIA_TOP, true);
		}

		if (contratoPenalidade != null && contratoPenalidade.getPercentualMargemVariacao() != null
						&& contratoPenalidade.getPercentualMargemVariacao().compareTo(BigDecimal.ONE) > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL,
							new Object[] {ContratoPontoConsumoPenalidade.PERCENTUAL_MARGEM_VARIACAO});
		}

		if (contratoPenalidade != null && listaContratoPenalidade != null && contratoPenalidade.getPeriodicidadePenalidade() == null) {
			throw new NegocioException(ERRO_NEGOCIO_PERIODICIDADE_TOP_CONTRATO_NAO_INFORMADA, true);
		}

		if (contratoPenalidade != null) {

			Date dataInicioNovaVigencia = contratoPenalidade.getDataInicioVigencia();
			Date dataFimNovaVigencia = contratoPenalidade.getDataFimVigencia();

			if (listaContratoPenalidade != null) {

				Date dataInicioMenor = dataInicioNovaVigencia;
				Date dataFimMaior = dataFimNovaVigencia;
				for (ContratoPenalidade penalidade : listaContratoPenalidade) {

					Date dataInicioVigencia = penalidade.getDataInicioVigencia();
					Date dataFimVigencia = penalidade.getDataFimVigencia();

					if (dataInicioVigencia.before(dataInicioMenor)) {
						dataInicioMenor = dataInicioVigencia;
					}
					if (dataFimVigencia.after(dataFimMaior)) {
						dataFimMaior = dataFimVigencia;
					}

					if (penalidade.getPenalidade().getChavePrimaria() == contratoPenalidade.getPenalidade().getChavePrimaria()
									&& contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == penalidade
													.getPeriodicidadePenalidade().getChavePrimaria()
									&& (dataInicioNovaVigencia.after(dataInicioVigencia) && dataInicioNovaVigencia.before(dataFimVigencia))
									&& (dataFimNovaVigencia.after(dataInicioVigencia) && dataFimNovaVigencia.before(dataFimVigencia))
									&& (dataInicioNovaVigencia.compareTo(dataInicioVigencia) == 0
													|| dataInicioNovaVigencia.compareTo(dataFimVigencia) == 0
													|| dataFimNovaVigencia.compareTo(dataFimVigencia) == 0 || dataFimNovaVigencia
													.compareTo(dataInicioVigencia) == 0)) {
						throw new NegocioException(ERRO_INTERVALO_DATA_VIGENCIA, true);
					}
				}

				if (dataFimNovaVigencia.after(dataFimMaior)) {
					throw new NegocioException(ERRO_INTERVALO_DATA_VIGENCIA, true);
				}
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.ControladorContrato#validarIntervaloDataVigenciaComDataAssinaturaContrato(br.com.ggas.contrato.contrato.
	 * Contrato,
	 * br.com.ggas.contrato.contrato.ContratoPenalidade, br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade)
	 */
	@Override
	public void validarIntervaloDataVigenciaComDataAssinaturaContrato(Contrato contrato, ContratoPenalidade contratoPenalidade,
					ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) throws NegocioException {

		Date dataAssinaturaContrato = contrato.getDataAssinatura();
		Date dataVencimentoContrato = contrato.getDataVencimentoObrigacoes();
		Date dataInicioNovaVigencia = null;
		Date dataFimNovaVigencia = null;

		if (contratoPenalidade != null) {
			dataInicioNovaVigencia = contratoPenalidade.getDataInicioVigencia();
			dataFimNovaVigencia = contratoPenalidade.getDataFimVigencia();
			vigenciaObrigatoria(dataInicioNovaVigencia, dataFimNovaVigencia);
		}

		if (contratoPontoConsumoPenalidade != null) {
			dataInicioNovaVigencia = contratoPontoConsumoPenalidade.getDataInicioVigencia();
			dataFimNovaVigencia = contratoPontoConsumoPenalidade.getDataFimVigencia();
			vigenciaObrigatoria(dataInicioNovaVigencia, dataFimNovaVigencia);

		}

		if ((dataAssinaturaContrato != null && dataInicioNovaVigencia != null && dataFimNovaVigencia != null 
				&& dataInicioNovaVigencia.before(dataAssinaturaContrato))
						|| (dataVencimentoContrato != null && dataFimNovaVigencia !=null &&
				dataFimNovaVigencia.after(dataVencimentoContrato))) {

			throw new NegocioException(ERRO_INTERVALO_DATA_VIGENCIA_ASSINATURA_VENCIMENTO_CONTRATO, true);
		}

	}

	/**
	 * Vigencia obrigatoria.
	 *
	 * @param dataInicioNovaVigencia
	 *            the data inicio nova vigencia
	 * @param dataFimNovaVigencia
	 *            the data fim nova vigencia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void vigenciaObrigatoria(Date dataInicioNovaVigencia, Date dataFimNovaVigencia) throws NegocioException {

		if (dataInicioNovaVigencia == null || dataFimNovaVigencia == null) {
			throw new NegocioException(ERRO_INTERVALO_DATA_VIGENCIA_OBRIGATORIO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoPenalidade(java.lang.Long, java.lang.Long, java.lang.Long,
	 * java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public ContratoPenalidade obterContratoPenalidade(Long idContrato, Long idPenalidade, Long idPeriodicidadePenalidade,
					Date dataInicioApuracao, Date dataFinalApuracao) throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoPenalidade.class);
		criteria.createAlias(CONSUMO_REFERENCIA, CONSUMO_REFERENCIA, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_PENALIDADE, idPenalidade));
		criteria.add(Restrictions.eq("periodicidadePenalidade.chavePrimaria", idPeriodicidadePenalidade));

		Collection<ContratoPenalidade> lista = criteria.list();
		ContratoPenalidade retorno = null;

		if (lista != null && lista.size() == 1) {
			retorno = lista.iterator().next();
		} else {

			// se data de inicio e fim de apuração não for nula no paramentro, será penalidade TOP que deve checar as vigências.
			if (dataInicioApuracao != null && dataFinalApuracao != null) {
				for (ContratoPenalidade penalidade : lista) {
					if (dataInicioApuracao.compareTo(penalidade.getDataInicioVigencia()) >= 0
									&& dataFinalApuracao.compareTo(penalidade.getDataFimVigencia()) <= 0) {
						retorno = penalidade;
						break;
					}
				}
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPenalidadeRetiradaMaiorMenorPorContrato(java.lang.Long)
	 */
	@Override
	public Collection<ContratoPenalidade> listarContratoPenalidadeRetiradaMaiorMenorPorContrato(Long chavePrimariaContrato) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idPenalidadeRetMaior = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));
		Long idPenalidadeRetMenor = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));

		Long[] chavesPenalidades = {idPenalidadeRetMaior, idPenalidadeRetMenor};

		Criteria criteria = createCriteria(getClasseEntidadeContratoPenalidade());

		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, chavePrimariaContrato));
		criteria.add(Restrictions.in(ATRIBUTO_CHAVE_PENALIDADE, chavesPenalidades));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoPenalidadeVigenciaRetiradaMaiorMenor(java.util.Map)
	 */
	@Override
	public ContratoPenalidade obterContratoPenalidadeVigenciaRetiradaMaiorMenor(Map<String, Object> params) {

		Long idContrato = (Long) params.get(ID_CONTRATO);
		Long idPenalidadeParametro = (Long) params.get(ID_PENALIDADE_RETIRADA_MAIOR_MENOR);

		Date dataInicioVigencia = (Date) params.get(DATA_INICIO_VIGENCIA);
		Date dataFimVigencia = (Date) params.get(DATA_FIM_VIGENCIA);

		Date dataAtual = (Date) params.get(DATA_VIGENCIA);

		Criteria criteria = createCriteria(getClasseEntidadeContratoPenalidade());

		if (idContrato != null) {
			criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, idContrato));
		}

		if (idPenalidadeParametro != null) {
			criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_PENALIDADE, idPenalidadeParametro));
		}
		if (dataAtual != null) {
			criteria.add(Restrictions.le(DATA_INICIO_VIGENCIA, dataAtual));
			criteria.add(Restrictions.ge(DATA_FIM_VIGENCIA, dataAtual));
		}
		if (dataInicioVigencia != null && dataFimVigencia != null) {
			criteria.add(Restrictions.le(DATA_INICIO_VIGENCIA, dataInicioVigencia));
			criteria.add(Restrictions.ge(DATA_FIM_VIGENCIA, dataFimVigencia));
		}

		criteria.addOrder(Order.asc(DATA_FIM_VIGENCIA));
		criteria.setMaxResults(1);

		return (ContratoPenalidade) criteria.uniqueResult();
	}

	/**
	 * Consulta uma lista de entidades do tipo {@link ContratoPenalidade} por
	 * {@link Contrato} e chave primária de {@link Penalidade}.
	 *
	 * @param contrato
	 *            - {@link Contrato}
	 * @param chavePrimariaPenalidade
	 *            - {@link Long}
	 * @return lista de penalidades de um contrato - {@link List}
	 */
	@Override
	public List<ContratoPenalidade> consultarContratoPenalidade(Contrato contrato, Long chavePrimariaPenalidade) {

		Criteria criteria = createCriteria(getClasseEntidadeContratoPenalidade());

		if (contrato != null) {
			criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, contrato.getChavePrimaria()));
		}

		if (NumeroUtil.naoNulo(chavePrimariaPenalidade)) {
			criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_PENALIDADE, chavePrimariaPenalidade));
		}

		criteria.addOrder(Order.asc(DATA_FIM_VIGENCIA));

		return criteria.list();
	}

	/**
	 * Seleciona de uma lista de entidades do tipo {@link ContratoPenalidade},
	 * aquela com data de inicio da vigência menor ou igual que
	 * {@code dataInicio} e data de fim da vigência maior ou igual que
	 * {@code dataFim}.
	 *
	 * @param dataInicio
	 *            - {@link Date}
	 * @param datafim
	 *            - {@link Date}
	 * @param listaContratoPenalidade
	 *            - {@link List}
	 * @return contratoPenalidade - {@link ContratoPenalidade}
	 */
	@Override
	public ContratoPenalidade selecionarContratoPenalidadePorPeriodoEmVigencia(Date dataInicio, Date dataFim,
			List<ContratoPenalidade> listaContratoPenalidade) {

		ContratoPenalidade contratoPenalidadeSelecionado = null;

		if (listaContratoPenalidade != null && !listaContratoPenalidade.isEmpty()) {

			for (ContratoPenalidade contratoPenalidade : listaContratoPenalidade) {

				if (contratoPenalidadePeriodoEmVigencia(contratoPenalidade, dataInicio, dataFim)) {
					contratoPenalidadeSelecionado = contratoPenalidade;
					break;
				}
			}
		}
		return contratoPenalidadeSelecionado;
	}

	/**
	 * Retorna {@code true} caso a a data de inicio da vigência de
	 * {@link ContratoPenalidade} seja menor ou igual que {@code dataInicio}, e
	 * a data de fim da vigência da penalidade seja maior ou igual que
	 * {@code dataFim}.
	 *
	 * @param contratoPenalidade
	 *            - {@link ContratoPenalidade}
	 * @param dataInicio
	 *            - {@link Date}
	 * @param dataFim
	 *            - {@link Date}
	 * @return {@link Boolean}
	 */
	private Boolean contratoPenalidadePeriodoEmVigencia(ContratoPenalidade contratoPenalidade, Date dataInicio,
			Date dataFim) {

		Boolean inicioMenorOuIgual = DataUtil.menorOuIgualQue(contratoPenalidade.getDataInicioVigencia(), dataInicio);
		Boolean fimMaiorOuIgual = DataUtil.menorOuIgualQue(dataFim, contratoPenalidade.getDataFimVigencia());
		return inicioMenorOuIgual && fimMaiorOuIgual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#obterContratoPontoConsumoPenalidadeVigencia(java.util.Map)
	 */
	@Override
	public ContratoPontoConsumoPenalidade obterContratoPontoConsumoPenalidadeVigencia(Map<String, Object> params) {

		Long idContratoModalidade = (Long) params.get(ID_CONTRATO_MODALIDADE);
		Long idPenalidadeParametro = (Long) params.get(ID_PENALIDADE_RETIRADA_MAIOR_MENOR);

		Date dataInicioVigencia = (Date) params.get(DATA_INICIO_VIGENCIA);
		Date dataFimVigencia = (Date) params.get(DATA_FIM_VIGENCIA);

		Criteria criteria = createCriteria(getClasseEntidadeContratoPontoConsumoPenalidade());

		if (idContratoModalidade != null) {
			criteria.add(Restrictions.eq("contratoPontoConsumoModalidade.chavePrimaria", idContratoModalidade));
		}

		if (idPenalidadeParametro != null) {
			criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_PENALIDADE, idPenalidadeParametro));
		}
		if (dataInicioVigencia != null) {
			criteria.add(Restrictions.le(DATA_INICIO_VIGENCIA, dataInicioVigencia));

		}
		if (dataFimVigencia != null) {
			criteria.add(Restrictions.ge(DATA_FIM_VIGENCIA, dataFimVigencia));
		}

		criteria.addOrder(Order.asc(DATA_FIM_VIGENCIA));
		criteria.setMaxResults(1);
		return (ContratoPontoConsumoPenalidade) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listarContratoPenalidadeTakeOrPayPorContrato(java.lang.Long)
	 */
	@Override
	public Collection<ContratoPenalidade> listarContratoPenalidadeTakeOrPayPorContrato(Long chavePrimariaContrato) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idPenalidade = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));

		Criteria criteria = createCriteria(getClasseEntidadeContratoPenalidade());

		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_CONTRATO, chavePrimariaContrato));
		criteria.add(Restrictions.eq(ATRIBUTO_CHAVE_PENALIDADE, idPenalidade));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#listaValorNominalIndiceFinaceiro(java.util.Date, java.util.Date,
	 * java.lang.String)
	 */
	@Override
	public Collection<IndiceFinanceiroValorNominal> listaValorNominalIndiceFinaceiro(Date dataReferenciaInicial, Date dataReferenciaFinal,
					String descricaoAbreviada) {

		Criteria criteria = getSession().createCriteria(IndiceFinanceiroValorNominal.class);
		criteria.createCriteria(INDICE_FINANCEIRO).add(Restrictions.eq(DESCRICAO_ABREVIADA, descricaoAbreviada));
		criteria.add(Restrictions.ge(DATA_REFERENCIA, dataReferenciaInicial));
		criteria.add(Restrictions.le(DATA_REFERENCIA, dataReferenciaFinal));

		criteria.addOrder(Order.asc(DATA_REFERENCIA));

		return criteria.list();
	}

	/**
	 * Obter indice financeiro valor nominal mais recente.
	 *
	 * @param indiceFinanceiro the indice financeiro
	 * @param dataReferenciaFinal the data referencia final
	 * @return the indice financeiro valor nominal
	 */
	private IndiceFinanceiroValorNominal obterIndiceFinanceiroValorNominalMaisRecente(IndiceFinanceiro indiceFinanceiro,
					Date dataReferenciaFinal) {

		return Util.primeiroElemento(this.listaValorNominalIndiceFinanceiro(indiceFinanceiro, null, dataReferenciaFinal, false));
	}

	/**
	 * Lista valor nominal indice financeiro.
	 *
	 * @param indiceFinanceiro the indice financeiro
	 * @param dataReferenciaInicial the data referencia inicial
	 * @param dataReferenciaFinal the data referencia final
	 * @return the collection
	 */
	private Collection<IndiceFinanceiroValorNominal> listaValorNominalIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro,
					Date dataReferenciaInicial, Date dataReferenciaFinal) {

		return this.listaValorNominalIndiceFinanceiro(indiceFinanceiro, dataReferenciaInicial, dataReferenciaFinal, true);
	}

	/**
	 * Lista valor nominal indice financeiro.
	 *
	 * @param indiceFinanceiro the indice financeiro
	 * @param dataReferenciaInicial the data referencia inicial
	 * @param dataReferenciaFinal the data referencia final
	 * @param ordenacaoAscendente the ordenacao ascendente
	 * @return the collection
	 */
	private Collection<IndiceFinanceiroValorNominal> listaValorNominalIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro,
					Date dataReferenciaInicial, Date dataReferenciaFinal, boolean ordenacaoAscendente) {

		Criteria criteria = getSession().createCriteria(IndiceFinanceiroValorNominal.class);
		criteria.createCriteria(INDICE_FINANCEIRO).add(Restrictions.eq(CHAVE_PRIMARIA, indiceFinanceiro.getChavePrimaria()));

		if (dataReferenciaInicial != null) {
			criteria.add(Restrictions.ge(DATA_REFERENCIA, dataReferenciaInicial));
		}

		if (dataReferenciaFinal != null) {
			criteria.add(Restrictions.le(DATA_REFERENCIA, dataReferenciaFinal));
		}

		if (ordenacaoAscendente) {
			criteria.addOrder(Order.asc(DATA_REFERENCIA));
		} else {
			criteria.addOrder(Order.desc(DATA_REFERENCIA));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#fatorCorrecaoIndiceFinanceiro(br.com.ggas.faturamento.tarifa.IndiceFinanceiro,
	 * java.util.Date)
	 */
	@Override
	public BigDecimal fatorCorrecaoIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro, Date dataReferenciaInicial) {

		return this.fatorCorrecaoIndiceFinanceiro(indiceFinanceiro, dataReferenciaInicial, new Date());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#fatorCorrecaoIndiceFinanceiro(br.com.ggas.faturamento.tarifa.IndiceFinanceiro,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public BigDecimal fatorCorrecaoIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro, Date dataReferenciaInicial, Date dataReferenciaFinal) {

		int incrementoRazaoDia = 0;
		int incrementoRazaoMes = 0;
		Date dataIterator = null;
		Date dataFinalIterator = null;

		if (indiceFinanceiro.isIndicadorMensal()) {
			incrementoRazaoDia = 0;
			incrementoRazaoMes = 1;
			dataIterator = DataUtil.gerarDataSemHoraPrimeiroDiaMes(dataReferenciaInicial);
			dataFinalIterator = DataUtil.decrementarDiaMes(DataUtil.gerarDataSemHoraPrimeiroDiaMes(dataReferenciaFinal), 0, 1);
		} else {
			incrementoRazaoDia = 1;
			incrementoRazaoMes = 0;
			dataIterator = DataUtil.incrementarDiaMes(dataReferenciaInicial, 1, 0);
			dataFinalIterator = DataUtil.gerarDataHmsZerados(dataReferenciaFinal);
		}

		Collection<IndiceFinanceiroValorNominal> listaValorNominalIndiceFinanceiro = listaValorNominalIndiceFinanceiro(indiceFinanceiro,
						dataIterator, dataFinalIterator);
		Map<String, IndiceFinanceiroValorNominal> mapaIndiceFinanceiroValorNominal = Util
						.trasformarParaMapaPorHashString(listaValorNominalIndiceFinanceiro);

		BigDecimal fator = BigDecimal.ONE;
		BigDecimal ultimoValorNominalIndiceFinanceiroComputado = null;

		while (DataUtil.menorOuIgualQue(dataIterator, dataFinalIterator)) {
			BigDecimal valorNominal = BigDecimal.ZERO;
			String stringDataIterator = DataUtil.converterDataParaString(dataIterator);

			if (mapaIndiceFinanceiroValorNominal.containsKey(stringDataIterator)) {
				valorNominal = mapaIndiceFinanceiroValorNominal.get(stringDataIterator).getValorNominal();
				ultimoValorNominalIndiceFinanceiroComputado = valorNominal;
			} else {
				if (ultimoValorNominalIndiceFinanceiroComputado != null) {
					valorNominal = ultimoValorNominalIndiceFinanceiroComputado;
				} else {
					Date dataMaximaBusca = DataUtil.decrementarDiaMes(dataIterator, incrementoRazaoDia, incrementoRazaoMes);
					IndiceFinanceiroValorNominal indiceFinanceiroValorNominal = obterIndiceFinanceiroValorNominalMaisRecente(
									indiceFinanceiro, dataMaximaBusca);
					if (indiceFinanceiroValorNominal != null) {
						valorNominal = indiceFinanceiroValorNominal.getValorNominal();
					}
					ultimoValorNominalIndiceFinanceiroComputado = valorNominal;
				}
			}
			fator = fator.multiply(valorNominal.add(BigDecimal.ONE));
			dataIterator = DataUtil.incrementarDiaMes(dataIterator, incrementoRazaoDia, incrementoRazaoMes);
		}

		return fator;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#consultarContratoPontoConsumoItemFaturamento(java.util.Map)
	 */
	@Override
	public ContratoPontoConsumoItemFaturamento consultarContratoPontoConsumoItemFaturamento(Map<String, Object> filtro) {

		Criteria criteria = this.createCriteria(ContratoPontoConsumoItemFaturamento.class);

		DetachedCriteria subconsulta = DetachedCriteria.forClass(this.getClasseEntidadeContratoPontoConsumo(), CONTRATOPONTOCONSUMO);
		if (filtro.get(CHAVE_CONTRATO) != null) {
			subconsulta.add(Restrictions.eq("contratoPontoConsumo.contrato.chavePrimaria", filtro.get(CHAVE_CONTRATO)));
		}

		if (filtro.get(CHAVE_PONTO_CONSUMO) != null) {
			subconsulta.add(Restrictions.eq("contratoPontoConsumo.pontoConsumo.chavePrimaria", filtro.get(CHAVE_PONTO_CONSUMO)));
		}
		subconsulta.setProjection(Property.forName("contratoPontoConsumo.chavePrimaria"));

		criteria.add(Subqueries.propertyIn("contratoPontoConsumo.chavePrimaria", subconsulta));

		if (filtro.get("itemFaturaGas") != null) {
			criteria.add(Restrictions.eq("itemFatura.chavePrimaria", filtro.get("itemFaturaGas")));
		}

		return (ContratoPontoConsumoItemFaturamento) criteria.uniqueResult();
	}

	/**
	 * Obtém o {@code pontoConsumo}, {@code contrato} e {@code situacaoContrato}
	 * de ContratoPontoConsumo com Contratos Faturáveis, por códigos do Ponto de Consumo no Supervisório
	 * @param codigosPontoConsumoSupervisorio
	 * @return Map - ContratoPontoConsumo por atributo chavePrimaria do PontoConsumo
	 **/
	@Override
	public Map<Long, ContratoPontoConsumo> consultarContratoFaturavelPorCodigoSupervisorio(String[] codigosPontoConsumoSupervisorio) {

		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumo = new HashMap<>();

		if (codigosPontoConsumoSupervisorio != null && codigosPontoConsumoSupervisorio.length > 0) {
			Criteria criteria = createCriteria(getClasseEntidadeContratoPontoConsumo());
			criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
			criteria.createAlias(PONTO_CONSUMO_CONTRATO, PONTO_CONSUMO_CONTRATO);
			criteria.createAlias("contrato.situacao", SITUACAO);

			criteria.setProjection(Projections.projectionList()
							.add(Projections.property(PONTO_CONSUMO_CHAVE_PRIMARIA))
							.add(Projections.property(ATRIBUTO_PERIODICIDADE))
							.add(Projections.property(ATRIBUTO_NUMERO_HORA_INICIAL))
							.add(Projections.property(CHAVE_PRIMARIA))
							.add(Projections.property(ATRIBUTO_CHAVE_CONTRATO))
							.add(Projections.property("contrato.dataAssinatura"))
							.add(Projections.property("contrato.situacao")));

			criteria.add(Restrictions.in("pontoConsumo.codigoPontoConsumoSupervisorio", codigosPontoConsumoSupervisorio));
			criteria.add(Restrictions.eq(SITUACAO_FATURAVEL, Boolean.TRUE));
			criteria.add(Restrictions.isNull("contrato.chavePrimariaPrincipal"));

			List<Object[]> listaDeAtributos = criteria.list();
			for (Object[] atributos : listaDeAtributos) {
				Long chavePrimariaPontoConsumo = (Long) atributos[0];
				ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) criarContratoPontoConsumo();
				contratoPontoConsumo.setPeriodicidade((Periodicidade) atributos[1]);
				contratoPontoConsumo.setNumeroHoraInicial((Integer) atributos[2]);
				contratoPontoConsumo.setChavePrimaria((long) atributos[3]);
				Contrato contrato = (Contrato) this.criar();
				contrato.setChavePrimaria((long) atributos[4]);
				contrato.setDataAssinatura((Date) atributos[5]);
				contrato.setSituacao((SituacaoContrato) atributos[6]);
				contratoPontoConsumo.setContrato(contrato);
				if (!mapaContratoPontoConsumo.containsKey(chavePrimariaPontoConsumo)) {
					mapaContratoPontoConsumo.put(chavePrimariaPontoConsumo, contratoPontoConsumo);
				}
			}

		}

		return mapaContratoPontoConsumo;
	}

	/**
	 * Verifica se existe algum contrato ativo em uma época de leitura
	 * e obtém os atributos: {@code periodicidade}, {@code numeroHoraInicial},
	 * {@code contrato} e {@code chavePrimaria} do ContratoPontoConsumo
	 * de um Contrato com maior data de recisão
	 * @param pontoConsumo
	 * @param dataLeitura
	 * @return ContratoPontoConsumo - ContratoPontoConsumo do Contrato ativo na época de leitura
	 **/
	@Override
	public ContratoPontoConsumo verificarContratoAtivoEpocaLeitura(PontoConsumo pontoConsumo, Date dataLeitura) {

		ContratoPontoConsumo contratoPontoConsumo = null;
		Criteria criteria = createCriteria(getClasseEntidadeContratoPontoConsumo());
		criteria.createAlias(PONTO_CONSUMO_CONTRATO, PONTO_CONSUMO_CONTRATO);
		criteria.setProjection(Projections.projectionList()
						.add(Projections.property(ATRIBUTO_PERIODICIDADE), ATRIBUTO_PERIODICIDADE)
						.add(Projections.property(ATRIBUTO_NUMERO_HORA_INICIAL), ATRIBUTO_NUMERO_HORA_INICIAL)
						.add(Projections.property(PONTO_CONSUMO_CONTRATO), PONTO_CONSUMO_CONTRATO)
						.add(Projections.property(CHAVE_PRIMARIA), CHAVE_PRIMARIA));


		criteria.add(Restrictions.isNotNull(ATRIBUTO_CONTRATO_DATA_RECISAO));
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria()));
		criteria.add(Restrictions.gt(ATRIBUTO_CONTRATO_DATA_RECISAO, dataLeitura));
		criteria.addOrder(Order.asc(ATRIBUTO_CONTRATO_DATA_RECISAO));

		criteria.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeContratoPontoConsumo()));

		List<ContratoPontoConsumo> listaContratoPontoConsumo = criteria.list();
		if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {
			contratoPontoConsumo = listaContratoPontoConsumo.get(listaContratoPontoConsumo.size() - 1);
		}
		return contratoPontoConsumo;
	}

	@Override
	public Map<Long, Collection<Long>> consultarChavesPontosConsumoPorContrato(Collection<Contrato> contratos) {

		Map<Long, Collection<Long>> pontosConsumoPorContrato = null;

		if(!Util.isNullOrEmpty(contratos)) {
			StringBuilder hql = new StringBuilder();

			hql.append(" select contrato.chavePrimaria as contrato_chavePrimaria, ");
			hql.append(" pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria ");
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(CONTRATO);
			hql.append(" inner join contrato.listaContratoPontoConsumo contratoPontoConsumo ");
			hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");

			Query query = getSession().createQuery(hql.toString());
			query.setResultTransformer(new GGASTransformer(Long.class,
					"contrato_chavePrimaria"));

			pontosConsumoPorContrato = (Map<Long, Collection<Long>>) query.list().get(0);
		}

		return pontosConsumoPorContrato;
	}

	@Override
	public Map<Long, Collection<Contrato>> consultarAgrupamentoContratoPorPontoConsumo(Collection<PontoConsumo> listaPontoConsumo) {

		Map<Long, Collection<Contrato>> contratoPorPontoConsumo = null;

		if (!Util.isNullOrEmpty(listaPontoConsumo)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria, ");
			hql.append(" contrato.chavePrimaria as chavePrimaria, ");
			hql.append(" tipoAgrupamento as tipoAgrupamento, ");
			hql.append(" contrato.agrupamentoConta as agrupamentoConta ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(CONTRATO_PONTO);
			hql.append(" inner join contratoPonto.contrato contrato ");
			hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
			hql.append(" inner join pontoConsumo.segmento segmento ");
			hql.append(" inner join contrato.clienteAssinatura cliente ");
			hql.append(" inner join contrato.situacao situacaoContrato ");
			hql.append(" left join contrato.tipoAgrupamento tipoAgrupamento ");
			hql.append(WHERE);
			Map<String, List<Long>> mapaPropriedades = 
							HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Util.recuperarChavesPrimarias(listaPontoConsumo));		
			hql.append(" and situacaoContrato.chavePrimaria = :situacaoContrato ");
			hql.append(" and contrato.chavePrimariaPrincipal is null ");

			Query query = super.getSession().createQuery(hql.toString());

			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
			query.setLong("situacaoContrato", SituacaoContrato.ATIVO);
			query.setResultTransformer(
					new GGASTransformer(getClasseEntidade(), getSessionFactory().getAllClassMetadata(), "pontoConsumo_chavePrimaria"));
			contratoPorPontoConsumo = (Map<Long, Collection<Contrato>>) query.list().get(0);

		}
		return contratoPorPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validaFormaDeApuracao(br.com.ggas.web.contrato.contrato.CompromissoTakeOrPayVO)
	 */
	@Override
	public void validaFormaDeApuracao(CompromissoTakeOrPayVO compromissoTakeOrPayVO) throws GGASException {
		StringBuilder camposObrigatorios = new StringBuilder();
		if (compromissoTakeOrPayVO.getConsideraParadaProgramada() && compromissoTakeOrPayVO.getApuracaoParadaProgramada()==null) {
			camposObrigatorios.append(Constantes.FORMA_APURACAO_VOLUME_PARADA_PROGRAMADA);
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (compromissoTakeOrPayVO.getConsideraFalhaFornecimento() && compromissoTakeOrPayVO.getApuracaoFalhaFornecimento()==null){
			camposObrigatorios.append(Constantes.FORMA_APURACAO_VOLUME_FALHA_FORNECIMENTO);
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (compromissoTakeOrPayVO.getConsideraCasoFortuito() && compromissoTakeOrPayVO.getApuracaoCasoFortuito()==null){
			camposObrigatorios.append(Constantes.FORMA_APURACAO_VOLUME_CASO_FORTUITO);
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.toString().substring(0, camposObrigatorios.toString().length() - REDUCAO_CAMPOS_OBRIGATORIOS));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#validarDadosContratoAnexo(br.com.ggas.contrato.contrato.impl.ContratoAnexo)
	 */
	@Override
	public void validarDadosContratoAnexo(ContratoAnexo contratoAnexo) throws NegocioException {
		Map<String, Object> errosContratoAnexo = contratoAnexo.validarDados();

		if (errosContratoAnexo != null && !errosContratoAnexo.isEmpty()) {
			throw new NegocioException(errosContratoAnexo);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.contrato.contrato.ControladorContrato#verificarDescricaoContratoAnexo(java.lang.Integer, java.util.Collection,
	 * br.com.ggas.contrato.contrato.impl.ContratoAnexo)
	 */
	@Override
	public void verificarDescricaoContratoAnexo(Integer indexLista, Collection<ContratoAnexo> listaAnexos, ContratoAnexo contratoAnexo)
			throws NegocioException {

		ArrayList<ContratoAnexo> listaAnexoArray = new ArrayList<>(listaAnexos);
		for (int i = 0; i < listaAnexos.size(); i++) {

			ContratoAnexo contratoAnexoExistente = listaAnexoArray.get(i);

			String descricao = contratoAnexoExistente.getDescricaoAnexo();
			if (descricao != null && descricao.equals(contratoAnexo.getDescricaoAnexo()) && indexLista != i) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE, true);
			}
		}

	}

	@Override
	public ContratoAnexo obterContratoAnexo(long chavePrimaria) {
		Criteria criteria = createCriteria(ContratoAnexo.class);
		criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
		Object result = criteria.uniqueResult();
		if (result != null) {
			return (ContratoAnexo) result;
		}
		return null;
	}

	/**
	 * Constrói uma lista de ContratoPontoConsumoModalidadeQDC por chave primária do
	 * PontoConsumoModalidade e ContratoPontoConsumo. Obtém do ContratoPontoConsumoModalidadeQDC, os seguintes atributos:
	 * {@code chavePrimaria}, {@code dataVigencia},
	 * {@code contratoPontoConsumoModalidade},
	 * {@code medidaVolume}, {@code numeroFatorCorrecao},
	 * {@code unidadePressao}, {@code medidaPressao}, {@code tipoMedicao}, ,
	 * {@code medidaPressaoColetada}. Obtém do PontoConsumoModalidade, os seguintes
	 * atributos: {@code chavePrimaria}. Obtém do ContratoPontoconsumo, os seguintes
	 * atributos: {@code chavePrimaria}.
	 *
	 * @param chavesPontosConsumo
	 * @return listaModalidadeQDC
	 */
	public List<ContratoPontoConsumoModalidadeQDCImpl> obterModalidadeQDC(Long cdContratoPontoConsumo){
		Criteria criteria = createCriteria(ContratoPontoConsumoModalidadeQDCImpl.class, "cpcm_qdc");
		criteria.createAlias("cpcm_qdc.contratoPontoConsumoModalidade", "cpcm", Criteria.INNER_JOIN);
		criteria.createAlias("cpcm.contratoPontoConsumo", "cpc", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("cpc.chavePrimaria", cdContratoPontoConsumo));
		return criteria.list();
	}
	/**
	 * Constrói uma lista de ContratoPontoConsumoModalidadeQDC por chaves primárias do
	 * PontoConsumoModalidade, ContratoPontoConsumo, contratoModalidade . Obtém do ContratoPontoConsumoModalidadeQDC, os seguintes atributos:
	 * {@code chavePrimaria}, {@code dataVigencia},
	 * {@code contratoPontoConsumoModalidade},
	 * {@code medidaVolume}, {@code numeroFatorCorrecao},
	 * {@code unidadePressao}, {@code medidaPressao}, {@code tipoMedicao}, ,
	 * {@code medidaPressaoColetada}. Obtém do PontoConsumoModalidade, os seguintes
	 * atributos: {@code chavePrimaria}. Obtém do ContratoPontoconsumo, os seguintes
	 * atributos: {@code chavePrimaria}.
	 * 
	 * @param chavesPontosConsumo
	 * @return listaModalidadeQDC
	 */
	public List<ContratoPontoConsumoModalidadeQDCImpl> obterModalidade(Long cdContratoPontoConsumo){
		Criteria criteria = createCriteria(ContratoPontoConsumoModalidadeQDCImpl.class, "cpcm_qdc");
		criteria.createAlias("cpcm_qdc.contratoPontoConsumoModalidade", "cpcm", Criteria.INNER_JOIN);
		criteria.createAlias("cpcm.contratoPontoConsumo", "cpc", Criteria.INNER_JOIN);
		criteria.createAlias("cpcm.contratoModalidade", "cm", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("cpc.chavePrimaria", cdContratoPontoConsumo));
		return criteria.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #consultarContratoPontoConsumoPorPontoConsumo
	 * (java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<ContratoPontoConsumo> consultarDadosContratoPontoConsumoPorPontoConsumo(Long[] chavesPontoConsumo)
			throws NegocioException {

		Query query = null;

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		StringBuilder hql = new StringBuilder();
		hql.append(" select  contratoPonto.contrato as contrato,");
		hql.append(" contratoPonto.pontoConsumo as pontoConsumo ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join contratoPonto.contrato contrato ");
		hql.append(" inner join contratoPonto.contrato.clienteAssinatura ");
		hql.append(" inner join contratoPonto.pontoConsumo ");
		hql.append(" inner join contratoPonto.pontoConsumo.imovel ");
		hql.append(" inner join contratoPonto.pontoConsumo.quadraFace ");
		hql.append(" inner join contratoPonto.pontoConsumo.rota ");
		hql.append(" inner join contratoPonto.pontoConsumo.ramoAtividade ");
		hql.append(" left join contrato.formaCobranca ");
		hql.append(" where ");
		Map<String, List<Long>> mapaPropriedades =
				HibernateHqlUtil.adicionarClausulaIn(hql, "contratoPonto.pontoConsumo.chavePrimaria", PT_CONS,
						Arrays.asList(chavesPontoConsumo));
		hql.append(" and contratoPonto.contrato.situacao = :situacao ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		query.setString(SITUACAO, situacao);
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeContratoPontoConsumo(), super.getSessionFactory().getAllClassMetadata()));
		return query.list();
	}

	/**
	 * Verifica se existe algum contrato ativo em uma época de leitura
	 * @param cliente
	 * @return Contrato - ContratoPontoConsumo do Contrato ativo na época de leitura
	 **/
	@Override
	public Contrato obterValorDosTributosDoContrato(Cliente cliente) {
		Criteria criteria = createCriteria(Contrato.class, "cont");
		criteria.createAlias("cont.clienteAssinatura", "clie", Criteria.INNER_JOIN);

		if (cliente.getChavePrimaria() != 0) {
			criteria.add(Restrictions.eq("cont.clienteAssinatura.chavePrimaria", cliente.getChavePrimaria()));
		}
		criteria.setMaxResults(1);
		return (Contrato) criteria.uniqueResult();
	}

	/**
	 * Verificar se o contrato está encerrado ou migrado
	 * @param chaveContrato
	 * @return void
	 **/
	@Override
	public void validarMigracaoModeloContrato(Long chaveContrato) throws NegocioException {
		ConstanteSistema constanteEncerrado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CONTRATO_ENCERRADO);
		ConstanteSistema constanteRescindido = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CONTRATO_RESCINDIDO);
		if (chaveContrato == null || chaveContrato <= 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}

		StringBuilder hql = new StringBuilder();
		hql.append(" select contrato from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(CONTRATO);
		hql.append("where contrato.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chaveContrato);

		List<Contrato> contratos = query.list();
		
		for (Contrato contrato : contratos) {
			if (contrato.getSituacao().getChavePrimaria() == Long.parseLong(constanteEncerrado.getValor())
					|| contrato.getSituacao().getChavePrimaria() == Long.parseLong(constanteRescindido.getValor())) {
				throw new NegocioException(Constantes.ERRO_CONTRATO_ENCERRADO_RESCINDIDO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * obterContratoPontoConsumoPorContratoPontoConsumo
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo obterDadosContratoPontoConsumoPorContratoPontoConsumo(Long chaveContrato, Long chavePontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contratoPocn.chavePrimaria as chavePrimaria, ");
		hql.append(" contratoPocn.complementoEndereco as complementoEndereco, ");
		hql.append(" contratoPocn.numeroImovel as numeroImovel, ");
		hql.append(" contratoPocn.cep as cep, ");
		hql.append(" contratoPocn.pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria, ");
		hql.append(" contratoPocn.pontoConsumo.codigoLegado as pontoConsumo_codigoLegado ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPocn ");
		hql.append(" inner join contratoPocn.pontoConsumo ");
		hql.append(WHERE);
		hql.append(" contratoPocn.contrato.chavePrimaria = :chaveContrato ");
		hql.append(" and contratoPocn.pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeContratoPontoConsumo(), super.getSessionFactory().getAllClassMetadata()));
		query.setParameter(CHAVE_CONTRATO, chaveContrato);
		query.setParameter(CHAVE_PONTO_CONSUMO, chavePontoConsumo);

		return (ContratoPontoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * obterDadosContratoAtivoPontoConsumo
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public ContratoPontoConsumo obterCamposContratoAtivoPontoConsumo(PontoConsumo pontoConsumo) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		
		hql.append(" select ");
		hql.append(" contrato.chavePrimaria as contrato_chavePrimaria, ");
		hql.append(" contrato.chavePrimariaPai as contrato_chavePrimariaPai, ");
		hql.append(" contrato.clienteAssinatura as contrato_clienteAssinatura ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join contratoPonto.contrato contrato ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeContratoPontoConsumo(), super.getSessionFactory().getAllClassMetadata()));
		query.setLong(0, pontoConsumo.getChavePrimaria());
		// FIXME : Utilizar a constante C_CONTRATO_ATIVO
		query.setLong(1, ATIVO);

		return (ContratoPontoConsumo) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.contrato.
	 * ControladorContrato#
	 * obterPontosConsumoComContratoAtivo
	 * (br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public List<PontoConsumo> obterPontosConsumoComContratoAtivo(Rota rota) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select contratoPonto.pontoConsumo");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join  contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join  pontoConsumo.rota rota ");
		hql.append(" inner join  pontoConsumo.situacaoConsumo situacaoConsumo ");
		hql.append(" inner join  contratoPonto.contrato contrato ");
		hql.append(" inner join  contrato.situacao situacao ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and contratoPonto.pontoConsumo.habilitado = :habilitado ");
		hql.append(" and contratoPonto.contrato.habilitado = :habilitado ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :ativo ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");
		hql.append(" and situacaoConsumo.faturavel = :habilitado ");
		hql.append(" and situacaoConsumo.indicadorLeitura = :habilitado ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idRota", rota.getChavePrimaria());
		query.setBoolean("habilitado", Boolean.TRUE);
		query.setLong("ativo", SituacaoContrato.ATIVO);
		return query.list();
	}
	
	
	@Override
	public PontoConsumo obterPontoConsumoContrato(Contrato contrato) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" Select contratoPontoConsumo.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(WHERE);
		hql.append(" contratoPontoConsumo.contrato.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, contrato.getChavePrimaria());
		query.setMaxResults(1);
		return (PontoConsumo) query.uniqueResult();
	}
	
	
	@Override
	public ContratoPontoConsumo obterUltimoContratoPontoConsumoPorPontoConsumo(Long chavePontoConsumo) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT contratoPonto ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join  contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" order by contratoPonto.ultimaAlteracao desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePontoConsumo", chavePontoConsumo);
		query.setMaxResults(1);
		return (ContratoPontoConsumo) query.uniqueResult();
	}
	
	
	@Override
	public void inserirDadosFaturaResidual(Map<Long, List<String>> dadosFaturaResidual, Funcionario funcionario) throws HibernateException, GGASException {
		String nomeQuery = "contrato.inserirDadosFaturaResidual";
		
		for(Entry entry : dadosFaturaResidual.entrySet()) {
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
					.getNamedQuery(nomeQuery);
			Boolean isData = Boolean.TRUE;
			Boolean isLeitura = Boolean.TRUE;
			Date dataLeitura = null;
			String leituraAtual = null;
			String situacaoContrato = null;
			
			List<String> dados = (List<String>) entry.getValue();
			
			ContratoPontoConsumo contratoPontoConsumo = this.consultarContratoPontoConsumoPorPontoConsumo((Long)entry.getKey());
			
			query.setParameter("chaveContratoPontoConsumo", contratoPontoConsumo.getChavePrimaria());
			
			for(String dado : dados) {
				if(isData) {
					dataLeitura = DataUtil.converterParaData(dado);
					query.setParameter("dataLeitura", dataLeitura);
					isData = Boolean.FALSE;
				} else if(isLeitura){
					leituraAtual = dado;
					query.setParameter("valorLeitura", new BigDecimal(leituraAtual));
					isLeitura = Boolean.FALSE;
				} else {
					situacaoContrato = dado;
					query.setParameter("situacaoContrato", dado);
				}
			}
			
			query.executeUpdate();
			
			associarMedidorPontoConsumo(contratoPontoConsumo.getPontoConsumo(), funcionario, dataLeitura, leituraAtual, situacaoContrato);
			
		}

	}
	
	
	private void associarMedidorPontoConsumo(PontoConsumo pontoConsumo, Funcionario funcionario, Date dataLeitura,
			String leituraAtual, String situacaoContrato) throws GGASException {
		
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		
		Map<String, Object> dados = obterDadosAssociacao(pontoConsumo, funcionario, dataLeitura, leituraAtual, situacaoContrato);
		PontoConsumo pontoConsumoAlterado = controladorMedidor.inserirDadosAssociacaoPontoConsumoMedidor(dados);
		controladorPontoConsumo.atualizarSaveOrUpdate(pontoConsumoAlterado, PontoConsumoImpl.class);
	}

	private Map<String, Object> obterDadosAssociacao(PontoConsumo pontoConsumo, Funcionario funcionario,
			Date dataLeitura, String leituraAtual, String situacaoContrato) {
		Map<String, Object> dados = new HashMap<String, Object>();

		dados.put(ID_PONTO_CONSUMO, pontoConsumo.getChavePrimaria());
		dados.put(ID_IMOVEL, pontoConsumo.getImovel().getChavePrimaria());
		dados.put(TIPO_ASSOCIACAO, ControladorMedidor.CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR);
		dados.put("funcionario", funcionario.getChavePrimaria());
		dados.put("dataMedidor", dataLeitura);
		dados.put("leituraAnterior", leituraAtual);
		dados.put("chaveMedidorAnterior", pontoConsumo.getInstalacaoMedidor().getMedidor().getChavePrimaria());

		if(situacaoContrato.equals("E")) {
			dados.put(ID_OPERACAO_MEDIDOR, OperacaoMedidor.CODIGO_BLOQUEIO);
			dados.put("medidorMotivoOperacao", 5l);
		} else {
			dados.put(ID_OPERACAO_MEDIDOR, OperacaoMedidor.CODIGO_REATIVACAO);
			dados.put("medidorMotivoOperacao", 4l);
		}

		
		
		
		return dados;
	}
	
	
	@Override
	public ContratoPontoConsumo obterUltimoContratoCriadoPontoConsumoPorPontoConsumo(Long chavePontoConsumo) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT contratoPonto ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join  contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" order by contratoPonto.contrato.chavePrimaria desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePontoConsumo", chavePontoConsumo);
		query.setMaxResults(1);
		return (ContratoPontoConsumo) query.uniqueResult();
	}
	
	
	@Override
	public ContratoPontoConsumo obterContratoPontoConsumoAtivoCliente(Cliente cliente) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(CONTRATO_PONTO);
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch contratoPonto.contrato contrato ");
		hql.append(" left join fetch contrato.arrecadadorContratoConvenio arrecadadorContratoConvenio ");
		hql.append(" inner join fetch pontoConsumo.segmento segmento ");
		hql.append(" inner join fetch contrato.clienteAssinatura cliente ");
		hql.append(" left join fetch contratoPonto.contrato.listaContratoQDC listaContratoQDC ");
		hql.append(" left join fetch contratoPonto.contrato.listaFatura listaFatura ");
		hql.append(" inner join fetch contrato.situacao situacaoContrato ");
		hql.append(" left join fetch contratoPonto.tipoMedicao tipoMedicao ");
		hql.append(" left join fetch contratoPonto.faixaPressaoFornecimento faixaPressaoFornecimento ");
		hql.append(" left join fetch faixaPressaoFornecimento.unidadePressao unidadePressao ");
		hql.append(" left join fetch contrato.multaRecisoria multaRecisoria ");
		hql.append(" left join fetch contratoPonto.unidadeVazaoMaximaInstantanea unidadeVazaoMaximaInstantanea ");
		hql.append(" left join fetch contratoPonto.unidadeVazaoMinimaInstantanea unidadeVazaoMinimaInstantanea ");
		hql.append(WHERE);
		hql.append(" contratoPonto.contrato.clienteAssinatura.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.chavePrimariaPrincipal is null ");
		hql.append(" and contratoPonto.contrato.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, cliente.getChavePrimaria());
		// FIXME : Utilizar a constante C_CONTRATO_ATIVO
		query.setLong(1, SituacaoContrato.ATIVO);
		return (ContratoPontoConsumo) query.uniqueResult();
	}
}
