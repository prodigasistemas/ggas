/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.contrato.contrato;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.contrato.contrato.impl.ContratoAnexo;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface Contrato.
 */
public interface Contrato extends EntidadeNegocio {

	/** The bean id contrato. */
	String BEAN_ID_CONTRATO = "contrato";

	/** The contrato. */
	String CONTRATO_ROTULO = "CONTRATO_ROTULO";

	/** The contratos. */
	String CONTRATOS = "CONTRATOS_ROTULO";

	/** The numero contrato. */
	String NUMERO_CONTRATO = "CONTRATO_NUMERO";

	/** The numero anterior contrato. */
	String NUMERO_ANTERIOR_CONTRATO = "CONTRATO_NUMERO_ANTERIOR";

	/** The situacao contrato. */
	String SITUACAO_CONTRATO = "CONTRATO_SITUACAO";

	/** The data assinatura. */
	String DATA_ASSINATURA = "CONTRATO_DATA_ASSINATURA";
	
	/** The arrecadador convênio. */
	String ARRECADADOR_CONVENIO = "CONTRATO_ARRECADADOR_CONVENIO";

	/** The numero empenho. */
	String NUMERO_EMPENHO = "CONTRATO_NUMERO_EMPENHO";

	/** The vencimento obg contratuais. */
	String VENCIMENTO_OBG_CONTRATUAIS = "CONTRATO_VENCIMENTO_OBG_CONTRATUAIS";

	/** The tipo garantia financeira. */
	String TIPO_GARANTIA_FINANCEIRA = "CONTRATO_TIPO_GARANTIA_FINANCEIRA";

	/** The data inicio garantia financeira. */
	String DATA_INICIO_GARANTIA_FINANCEIRA = "CONTRATO_DATA_INICIO_GARANTIA_FINANCEIRA";

	/** The data final garantia financeira. */
	String DATA_FINAL_GARANTIA_FINANCEIRA = "CONTRATO_DATA_FINAL_GARANTIA_FINANCEIRA";

	/** The correcao monetaria. */
	String CORRECAO_MONETARIA = "CONTRATO_CORRECAO_MONETARIA";

	/** The dia vencimento financiamento. */
	String DIA_VENCIMENTO_FINANCIAMENTO = "CONTRATO_DIA_VENCIMENTO_FINANCIAMENTO";

	/** The consumo ref sobre demanda. */
	String CONSUMO_REF_SOBRE_DEMANDA = "CONTRATO_CONSUMO_REF_SOBRE_DEMANDA";

	/** The consumo ref sob demanda. */
	String CONSUMO_REF_SOB_DEMANDA = "CONTRATO_CONSUMO_REF_SOB_DEMANDA";

	/** The faixa penalidade sobre dem. */
	String FAIXA_PENALIDADE_SOBRE_DEM = "CONTRATO_FAIXA_PENALIDADE_SOBRE_DEM";

	/** The faixa penalidade sob dem. */
	String FAIXA_PENALIDADE_SOB_DEM = "CONTRATO_FAIXA_PENALIDADE_SOB_DEM";

	/** The periodicidade take or pay. */
	String PERIODICIDADE_TAKE_OR_PAY = "PERIODICIDADE_TAKE_OR_PAY";

	/** The periodicidade ship or pay. */
	String PERIODICIDADE_SHIP_OR_PAY = "PERIODICIDADE_SHIP_OR_PAY";

	/** The consumo referencia take or pay. */
	String CONSUMO_REFERENCIA_TAKE_OR_PAY = "CONSUMO_REFERENCIA_TAKE_OR_PAY";

	/** The tabela preco cobranca. */
	String TABELA_PRECO_COBRANCA = "TABELA_PRECO_COBRANCA";

	/** The forma calculo cobranca recuperacao. */
	String FORMA_CALCULO_COBRANCA_RECUPERACAO = "FORMA_CALCULO_COBRANCA_RECUPERACAO";

	/** The tipo agrupamento contrato. */
	String TIPO_AGRUPAMENTO_CONTRATO = "TIPO_AGRUPAMENTO_CONTRATO";

	/** The apuracao volume parada programada. */
	String APURACAO_VOLUME_PARADA_PROGRAMADA = "APURACAO_VOLUME_PARADA_PROGRAMADA";

	/** The apuracao volume parada nao programada. */
	String APURACAO_VOLUME_PARADA_NAO_PROGRAMADA = "APURACAO_VOLUME_PARADA_NAO_PROGRAMADA";

	/** The apuracao volume caso fortuito. */
	String APURACAO_VOLUME_CASO_FORTUITO = "APURACAO_VOLUME_CASO_FORTUITO";

	/** The apuracao volume falha programacao. */
	String APURACAO_VOLUME_FALHA_PROGRAMACAO = "APURACAO_VOLUME_FALHA_PROGRAMACAO";

	/** The consumo referencia ship or pay. */
	String CONSUMO_REFERENCIA_SHIP_OR_PAY = "CONSUMO_REFERENCIA_SHIP_OR_PAY";

	/** The numero dias renovacao automatica. */
	String NUMERO_DIAS_RENOVACAO_AUTOMATICA = "CONTRATO_NUMERO_DIAS_RENOVACAO_AUTOMATICA";

	/** The numero aditivo. */
	String NUMERO_ADITIVO = "CONTRATO_NUMERO_ADITIVO";

	/** The data aditivo. */
	String DATA_ADITIVO = "CONTRATO_DATA_ADITIVO";

	/** The descricao aditivo. */
	String DESCRICAO_ADITIVO = "CONTRATO_DESCRICAO_ADITIVO";

	/** The contrato ddd. */
	String CONTRATO_DDD = "CONTRATO_DDD";

	/** The tempo garantia conversao. */
	String TEMPO_GARANTIA_CONVERSAO = "CONTRATO_TEMPO_GARANTIA_CONVERSAO";

	/** The data. */
	String DATA = "CONTRATO_DATA";

	/** The dias antecedencia renovacao. */
	String DIAS_ANTECEDENCIA_RENOVACAO = "CONTRATO_DIAS_ANTECEDENCIA_RENOVACAO";

	/** The dias antecedencia revisao garantia. */
	String DIAS_ANTECEDENCIA_REVISAO_GARANTIA = "CONTRATO_DIAS_ANTECEDENCIA_REVISAO_GARANTIA";

	/** The valor participacao cliente. */
	String VALOR_PARTICIPACAO_CLIENTE = "CONTRATO_VALOR_PARTICIPACAO_CLIENTE";

	/** The quantidade parcelas financiamento. */
	String QUANTIDADE_PARCELAS_FINANCIAMENTO = "CONTRATO_QUANTIDADE_PARCELAS_FINANCIAMENTO";

	/** The percentual juros financiamento. */
	String PERCENTUAL_JUROS_FINANCIAMENTO = "CONTRATO_PERCENTUAL_JUROS_FINANCIAMENTO";

	/** The desconto efetivo economia. */
	String DESCONTO_EFETIVO_ECONOMIA = "CONTRATO_DESCONTO_EFETIVO_ECONOMIA";

	/** The sistema amortizacao. */
	String SISTEMA_AMORTIZACAO = "CONTRATO_SISTEMA_AMORTIZACAO";

	/** The percentual tarifa dop. */
	String PERCENTUAL_TARIFA_DOP = "CONTRATO_PERCENTUAL_TARIFA_DOP";

	/** The hora dia. */
	String HORA_DIA = "CONTRATO_HORA_DIA";

	/** The periodicidade reavaliacao garantias. */
	String PERIODICIDADE_REAVALIACAO_GARANTIAS = "CONTRATO_PERIODICIDADE_REAVALIACAO_GARANTIAS";

	/** The tipo agrupamento. */
	String TIPO_AGRUPAMENTO = "CONTRATO_TIPO_AGRUPAMENTO";

	/** The percentual retirada maior. */
	String PERCENTUAL_RETIRADA_MAIOR = "PERCENTUAL_RETIRADA_MAIOR";

	/** The percentual retirada menor. */
	String PERCENTUAL_RETIRADA_MENOR = "PERCENTUAL_RETIRADA_MENOR";

	/** The percentual sobre tarifa gas. */
	String PERCENTUAL_SOBRE_TARIFA_GAS = "PERCENTUAL_SOBRE_TARIFA_GAS";

	/** The data investimento. */
	String DATA_INVESTIMENTO = "CONTRATO_DATA_INVESTIMENTO";

	/** The multa recisoria. */
	String MULTA_RECISORIA = "CONTRATO_MULTA_RECISORIA";

	/** The data recisao. */
	String DATA_RECISAO = "CONTRATO_DATA_RECISAO";

	/** The descricao contrato. */
	String DESCRICAO_CONTRATO = "CONTRATO_DESCRICAO_CONTRATO";

	/** The volume referencia. */
	String VOLUME_REFERENCIA = "CONTRATO_VOLUME_REFERENCIA";

	/** The periodicidade retirada maior menor. */
	String PERIODICIDADE_RETIRADA_MAIOR_MENOR = "PERIODICIDADE_RETIRADA_MAIOR_MENOR";

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	Integer getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero the numero to set
	 */
	void setNumero(Integer numero);

	/**
	 * Gets the ano contrato.
	 *
	 * @return the anoContrato
	 */
	Integer getAnoContrato();

	/**
	 * Sets the ano contrato.
	 *
	 * @param anoContrato the anoContrato to set
	 */
	void setAnoContrato(Integer anoContrato);

	/**
	 * Gets the numero anterior.
	 *
	 * @return the numeroAnterior
	 */
	String getNumeroAnterior();

	/**
	 * Sets the numero anterior.
	 *
	 * @param numeroAnterior the numeroAnterior to set
	 */
	void setNumeroAnterior(String numeroAnterior);

	/**
	 * Gets the situacao.
	 *
	 * @return the situacao
	 */
	SituacaoContrato getSituacao();

	/**
	 * Sets the situacao.
	 *
	 * @param situacao the situacao to set
	 */
	void setSituacao(SituacaoContrato situacao);

	/**
	 * Gets the modelo contrato.
	 *
	 * @return the modelo
	 */
	ModeloContrato getModeloContrato();

	/**
	 * Sets the modelo contrato.
	 *
	 * @param modelo the modelo to set
	 */
	void setModeloContrato(ModeloContrato modelo);

	/**
	 * Gets the numero aditivo.
	 *
	 * @return the numeroAditivo
	 */
	Integer getNumeroAditivo();

	/**
	 * Sets the numero aditivo.
	 *
	 * @param numeroAditivo the numeroAditivo to set
	 */
	void setNumeroAditivo(Integer numeroAditivo);

	/**
	 * Gets the data aditivo.
	 *
	 * @return the dataAditivo
	 */
	Date getDataAditivo();

	/**
	 * Sets the data aditivo.
	 *
	 * @param dataAditivo the dataAditivo to set
	 */
	void setDataAditivo(Date dataAditivo);

	/**
	 * Gets the proposta.
	 *
	 * @return the proposta
	 */
	Proposta getProposta();

	/**
	 * Sets the proposta.
	 *
	 * @param proposta the proposta to set
	 */
	void setProposta(Proposta proposta);

	/**
	 * Gets the valor gasto mensal.
	 *
	 * @return the valorGastoMensal
	 */
	BigDecimal getValorGastoMensal();

	/**
	 * Sets the valor gasto mensal.
	 *
	 * @param valorGastoMensal the valorGastoMensal to set
	 */
	void setValorGastoMensal(BigDecimal valorGastoMensal);

	/**
	 * Gets the medida economia mensal gn.
	 *
	 * @return the medidaEconomiaMensalGN
	 */
	BigDecimal getMedidaEconomiaMensalGN();

	/**
	 * Sets the medida economia mensal gn.
	 *
	 * @param medidaEconomiaMensalGN the medidaEconomiaMensalGN to
	 *            set
	 */
	void setMedidaEconomiaMensalGN(BigDecimal medidaEconomiaMensalGN);

	/**
	 * Gets the medida economia anual gn.
	 *
	 * @return the medidaEconomiaAnualGN
	 */
	BigDecimal getMedidaEconomiaAnualGN();

	/**
	 * Sets the medida economia anual gn.
	 *
	 * @param medidaEconomiaAnualGN the medidaEconomiaAnualGN to set
	 */
	void setMedidaEconomiaAnualGN(BigDecimal medidaEconomiaAnualGN);

	/**
	 * Gets the percentual economia gn.
	 *
	 * @return the percentualEconomiaGN
	 */
	BigDecimal getPercentualEconomiaGN();

	/**
	 * Sets the percentual economia gn.
	 *
	 * @param percentualEconomiaGN the percentualEconomiaGN to set
	 */
	void setPercentualEconomiaGN(BigDecimal percentualEconomiaGN);

	/**
	 * Gets the numero dia vencimento financiamento.
	 *
	 * @return the
	 *         numeroDiaVencimentoFinanciamento
	 */
	Integer getNumeroDiaVencimentoFinanciamento();

	/**
	 * Sets the numero dia vencimento financiamento.
	 *
	 * @param numeroDiaVencimentoFinanciamento the
	 *            numeroDiaVencimentoFinanciamento
	 *            to set
	 */
	void setNumeroDiaVencimentoFinanciamento(Integer numeroDiaVencimentoFinanciamento);

	/**
	 * Gets the data fim garantia financiamento.
	 *
	 * @return the dataFimGarantiaFinanciamento
	 */
	Date getDataFimGarantiaFinanciamento();

	/**
	 * Sets the data fim garantia financiamento.
	 *
	 * @param dataFimGarantiaFinanciamento the dataFimGarantiaFinanciamento
	 *            to set
	 */
	void setDataFimGarantiaFinanciamento(Date dataFimGarantiaFinanciamento);

	/**
	 * Gets the data inicio garantia financiamento.
	 *
	 * @return the dataInicioGarantiaFinanciamento
	 */
	Date getDataInicioGarantiaFinanciamento();

	/**
	 * Sets the data inicio garantia financiamento.
	 *
	 * @param dataInicioGarantiaFinanciamento the
	 *            dataInicioGarantiaFinanciamento
	 *            to set
	 */
	void setDataInicioGarantiaFinanciamento(Date dataInicioGarantiaFinanciamento);

	/**
	 * Gets the valor garantia financeira.
	 *
	 * @return the valorGarantiaFinanceira
	 */
	BigDecimal getValorGarantiaFinanceira();

	/**
	 * Sets the valor garantia financeira.
	 *
	 * @param valorGarantiaFinanceira the valorGarantiaFinanceira to
	 *            set
	 */
	void setValorGarantiaFinanceira(BigDecimal valorGarantiaFinanceira);

	/**
	 * Gets the descricao garantia financeira.
	 *
	 * @return the descricaoGarantiaFinanceira
	 */
	String getDescricaoGarantiaFinanceira();

	/**
	 * Sets the descricao garantia financeira.
	 *
	 * @param descricaoGarantiaFinanceira the descricaoGarantiaFinanceira
	 *            to set
	 */
	void setDescricaoGarantiaFinanceira(String descricaoGarantiaFinanceira);

	/**
	 * Gets the garantia financeira.
	 *
	 * @return the garantiaFinanceira
	 */
	EntidadeConteudo getGarantiaFinanceira();

	/**
	 * Sets the garantia financeira.
	 *
	 * @param garantiaFinanceira the garantiaFinanceira to set
	 */
	void setGarantiaFinanceira(EntidadeConteudo garantiaFinanceira);

	/**
	 * Gets the descricao aditivo.
	 *
	 * @return the descricaoAditivo
	 */
	String getDescricaoAditivo();

	/**
	 * Sets the descricao aditivo.
	 *
	 * @param descricaoAditivo the descricaoAditivo to set
	 */
	void setDescricaoAditivo(String descricaoAditivo);

	/**
	 * Gets the cliente assinatura.
	 *
	 * @return the clienteAssinatura
	 */
	Cliente getClienteAssinatura();

	/**
	 * Sets the cliente assinatura.
	 *
	 * @param clienteAssinatura the clienteAssinatura to set
	 */
	void setClienteAssinatura(Cliente clienteAssinatura);

	/**
	 * Gets the renovacao garantia financeira.
	 *
	 * @return the renovacaoGarantiaFinanceira
	 */
	Boolean getRenovacaoGarantiaFinanceira();

	/**
	 * Sets the renovacao garantia financeira.
	 *
	 * @param renovacaoGarantiaFinanceira the renovacaoGarantiaFinanceira
	 *            to set
	 */
	void setRenovacaoGarantiaFinanceira(Boolean renovacaoGarantiaFinanceira);

	/**
	 * Gets the renovacao automatica.
	 *
	 * @return the renovacaoAutomatica
	 */
	boolean getRenovacaoAutomatica();

	/**
	 * Sets the renovacao automatica.
	 *
	 * @param renovacaoAutomatica the renovacaoAutomatica to set
	 */
	void setRenovacaoAutomatica(boolean renovacaoAutomatica);

	/**
	 * Gets the agrupamento conta.
	 *
	 * @return the agrupamentoConta
	 */
	Boolean getAgrupamentoConta();

	/**
	 * Sets the agrupamento conta.
	 *
	 * @param agrupamentoConta the agrupamentoConta to set
	 */
	void setAgrupamentoConta(Boolean agrupamentoConta);

	/**
	 * Gets the agrupamento cobranca.
	 *
	 * @return the agrupamentoCobranca
	 */
	Boolean getAgrupamentoCobranca();

	/**
	 * Sets the agrupamento cobranca.
	 *
	 * @param agrupamentoCobranca the agrupamentoCobranca to set
	 */
	void setAgrupamentoCobranca(Boolean agrupamentoCobranca);

	/**
	 * Gets the numero empenho.
	 *
	 * @return the numeroEmpenho
	 */
	String getNumeroEmpenho();

	/**
	 * Sets the numero empenho.
	 *
	 * @param numeroEmpenho the numeroEmpenho to set
	 */
	void setNumeroEmpenho(String numeroEmpenho);

	/**
	 * Gets the numero dias renovacao automatica.
	 *
	 * @return the numeroDiasRenovacaoAutomatica
	 */
	Integer getNumeroDiasRenovacaoAutomatica();

	/**
	 * Sets the numero dias renovacao automatica.
	 *
	 * @param numeroDiasRenovacaoAutomatica the
	 *            numeroDiasRenovacaoAutomatica to
	 *            set
	 */
	void setNumeroDiasRenovacaoAutomatica(Integer numeroDiasRenovacaoAutomatica);

	/**
	 * Gets the data vencimento obrigacoes.
	 *
	 * @return the dataVencimentoObrigacoes
	 */
	Date getDataVencimentoObrigacoes();

	/**
	 * Sets the data vencimento obrigacoes.
	 *
	 * @param dataVencimentoObrigacoes the dataVencimentoObrigacoes to
	 *            set
	 */
	void setDataVencimentoObrigacoes(Date dataVencimentoObrigacoes);

	/**
	 * Gets the data assinatura.
	 *
	 * @return the dataAssinatura
	 */
	Date getDataAssinatura();

	/**
	 * Sets the data assinatura.
	 *
	 * @param dataAssinatura the dataAssinatura to set
	 */
	void setDataAssinatura(Date dataAssinatura);

	/**
	 * Gets the valor contrato.
	 *
	 * @return the valorContrato
	 */
	BigDecimal getValorContrato();

	/**
	 * Sets the valor contrato.
	 *
	 * @param valorContrato the valorContrato to set
	 */
	void setValorContrato(BigDecimal valorContrato);

	/**
	 * Gets the indicador multa.
	 *
	 * @return the indicadorMulta
	 */
	Boolean getIndicadorMulta();

	/**
	 * Sets the indicador multa.
	 *
	 * @param indicadorMulta the indicadorMulta to set
	 */
	void setIndicadorMulta(Boolean indicadorMulta);

	/**
	 * Gets the indicador juros.
	 *
	 * @return the indicadorJuros
	 */
	Boolean getIndicadorJuros();

	/**
	 * Sets the indicador juros.
	 *
	 * @param indicadorJuros the indicadorJuros to set
	 */
	void setIndicadorJuros(Boolean indicadorJuros);

	/**
	 * Gets the indice financeiro.
	 *
	 * @return the indiceFinanceiro
	 */
	IndiceFinanceiro getIndiceFinanceiro();

	/**
	 * Sets the indice financeiro.
	 *
	 * @param indiceFinanceiro the indiceFinanceiro to set
	 */
	void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro);

	/**
	 * Gets the lista contrato ponto consumo.
	 *
	 * @return the listaContratoPontoConsumo
	 */
	Collection<ContratoPontoConsumo> getListaContratoPontoConsumo();

	/**
	 * obtem o contrato ponto consumo do contrato com o codigo fornecido.
	 *
	 * @param idContratoPontoConsumo chave primaria do contrato ponto consumo
	 * @return contrato ponto consumo do contrato com o codigo fornecido
	 */
	ContratoPontoConsumo getContratoPontoConsumo(long idContratoPontoConsumo);

	/**
	 * Sets the lista contrato ponto consumo.
	 *
	 * @param listaContratoPontoConsumo the listaContratoPontoConsumo to
	 *            set
	 */
	void setListaContratoPontoConsumo(Collection<ContratoPontoConsumo> listaContratoPontoConsumo);

	/**
	 * Gets the lista contrato cliente.
	 *
	 * @return the listaContratoCliente
	 */
	Collection<ContratoCliente> getListaContratoCliente();

	/**
	 * Sets the lista contrato cliente.
	 *
	 * @param listaContratoCliente the listaContratoCliente to set
	 */
	void setListaContratoCliente(Collection<ContratoCliente> listaContratoCliente);

	/**
	 * Gets the lista contrato qdc.
	 *
	 * @return the listaContratoQDC
	 */
	Collection<ContratoQDC> getListaContratoQDC();

	/**
	 * Sets the lista contrato qdc.
	 *
	 * @param listaContratoQDC the listaContratoQDC to set
	 */
	void setListaContratoQDC(Collection<ContratoQDC> listaContratoQDC);

	/**
	 * @return O número do contrato formatado.
	 */
	String getNumeroFormatado();

	/**
	 * Gets the dias antecedencia revisao garantia financeira.
	 *
	 * @return the
	 *         diasAntecedenciaRevisaoGarantiaFinanceira
	 */
	Integer getDiasAntecedenciaRevisaoGarantiaFinanceira();

	/**
	 * Sets the dias antecedencia revisao garantia financeira.
	 *
	 * @param diasAntecedenciaRevisaoGarantiaFinanceira the
	 *            diasAntecedenciaRevisaoGarantiaFinanceira
	 *            to set
	 */
	void setDiasAntecedenciaRevisaoGarantiaFinanceira(Integer diasAntecedenciaRevisaoGarantiaFinanceira);

	/**
	 * Gets the periodicidade reavaliacao garantias.
	 *
	 * @return the
	 *         periodicidadeReavaliacaoGarantias
	 */
	Integer getPeriodicidadeReavaliacaoGarantias();

	/**
	 * Sets the periodicidade reavaliacao garantias.
	 *
	 * @param periodicidadeReavaliacaoGarantias the
	 *            periodicidadeReavaliacaoGarantias
	 *            to set
	 */
	void setPeriodicidadeReavaliacaoGarantias(Integer periodicidadeReavaliacaoGarantias);

	/**
	 * Gets the dias antecedencia renovacao.
	 *
	 * @return the diasAntecedenciaRenovacao
	 */
	Integer getDiasAntecedenciaRenovacao();

	/**
	 * Sets the dias antecedencia renovacao.
	 *
	 * @param diasAntecedenciaRenovacao the diasAntecedenciaRenovacao to
	 *            set
	 */
	void setDiasAntecedenciaRenovacao(Integer diasAntecedenciaRenovacao);

	/**
	 * Gets the valor participacao cliente.
	 *
	 * @return the valorParticipacaoCliente
	 */
	BigDecimal getValorParticipacaoCliente();

	/**
	 * Sets the valor participacao cliente.
	 *
	 * @param valorParticipacaoCliente the valorParticipacaoCliente to
	 *            set
	 */
	void setValorParticipacaoCliente(BigDecimal valorParticipacaoCliente);

	/**
	 * Gets the qtd parcelas financiamento.
	 *
	 * @return the qtdParcelasFinanciamento
	 */
	Integer getQtdParcelasFinanciamento();

	/**
	 * Sets the qtd parcelas financiamento.
	 *
	 * @param qtdParcelasFinanciamento the qtdParcelasFinanciamento to
	 *            set
	 */
	void setQtdParcelasFinanciamento(Integer qtdParcelasFinanciamento);

	/**
	 * Gets the percentual juros financiamento.
	 *
	 * @return the percentualJurosFinanciamento
	 */
	BigDecimal getPercentualJurosFinanciamento();

	/**
	 * Sets the percentual juros financiamento.
	 *
	 * @param percentualJurosFinanciamento the percentualJurosFinanciamento
	 *            to set
	 */
	void setPercentualJurosFinanciamento(BigDecimal percentualJurosFinanciamento);

	/**
	 * Gets the sistema amortizacao.
	 *
	 * @return the sistemaAmortizacao
	 */
	EntidadeConteudo getSistemaAmortizacao();

	/**
	 * Sets the sistema amortizacao.
	 *
	 * @param sistemaAmortizacao the sistemaAmortizacao to set
	 */
	void setSistemaAmortizacao(EntidadeConteudo sistemaAmortizacao);

	/**
	 * Gets the percentual tarifa do p.
	 *
	 * @return the percentualTarifaDoP
	 */
	BigDecimal getPercentualTarifaDoP();

	/**
	 * Sets the percentual tarifa do p.
	 *
	 * @param percentualTarifaDoP the percentualTarifaDoP to set
	 */
	void setPercentualTarifaDoP(BigDecimal percentualTarifaDoP);

	/**
	 * Gets the indicador ano contratual.
	 *
	 * @return the anoContratual
	 */
	boolean getIndicadorAnoContratual();

	/**
	 * Sets the indicador ano contratual.
	 *
	 * @param indicadorAnoContratual the new indicador ano contratual
	 */
	void setIndicadorAnoContratual(boolean indicadorAnoContratual);

	/**
	 * Gets the forma cobranca.
	 *
	 * @return the formaCobranca
	 */
	EntidadeConteudo getFormaCobranca();

	/**
	 * Sets the forma cobranca.
	 *
	 * @param formaCobranca the formaCobranca to set
	 */
	void setFormaCobranca(EntidadeConteudo formaCobranca);

	/**
	 * Gets the chave primaria pai.
	 *
	 * @return the chavePrimariaPai
	 */
	Long getChavePrimariaPai();

	/**
	 * Sets the chave primaria pai.
	 *
	 * @param chavePrimariaPai the chavePrimariaPai to set
	 */
	void setChavePrimariaPai(Long chavePrimariaPai);

	/**
	 * Gets the lista fatura.
	 *
	 * @return the lista fatura
	 */
	Collection<Fatura> getListaFatura();

	/**
	 * Sets the lista fatura.
	 *
	 * @param listaFatura the new lista fatura
	 */
	void setListaFatura(Collection<Fatura> listaFatura);

	/**
	 * Gets the tipo agrupamento.
	 *
	 * @return the tipoAgrupamento
	 */
	EntidadeConteudo getTipoAgrupamento();

	/**
	 * Sets the tipo agrupamento.
	 *
	 * @param tipoAgrupamento the tipoAgrupamento to set
	 */
	void setTipoAgrupamento(EntidadeConteudo tipoAgrupamento);

	/**
	 * @return the percentualSobreTariGas
	 */
	BigDecimal getPercentualSobreTariGas();

	/**
	 * Sets the percentual sobre tari gas.
	 *
	 * @param percentualSobreTariGas the percentualSobreTariGas to
	 *            set
	 */
	void setPercentualSobreTariGas(BigDecimal percentualSobreTariGas);

	/**
	 * Sets the valor investimento.
	 *
	 * @param valorInvestimento the new valor investimento
	 * @return baseApuracaoRetiradaMaior
	 */

	/**
	 * @param valorInvestimento
	 *            the valorInvestimento to set
	 */
	void setValorInvestimento(BigDecimal valorInvestimento);

	/**
	 * Gets the valor investimento.
	 *
	 * @return the valorInvestimento
	 */
	BigDecimal getValorInvestimento();

	/**
	 * Sets the data investimento.
	 *
	 * @param dataInvestimento the dataInvestimento to set
	 */
	void setDataInvestimento(Date dataInvestimento);

	/**
	 * Gets the data investimento.
	 *
	 * @return the dataInvestimento
	 */
	Date getDataInvestimento();

	/**
	 * Sets the multa recisoria.
	 *
	 * @param multaRecisoria the multaRecisoria to set
	 */
	void setMultaRecisoria(EntidadeConteudo multaRecisoria);

	/**
	 * Gets the multa recisoria.
	 *
	 * @return the multaRecisoria
	 */
	EntidadeConteudo getMultaRecisoria();

	/**
	 * Sets the fatura encerramento gerada.
	 *
	 * @param faturaEncerramentoGerada the faturaEncerramentoGerada to
	 *            set
	 */
	void setFaturaEncerramentoGerada(Boolean faturaEncerramentoGerada);

	/**
	 * Gets the fatura encerramento gerada.
	 *
	 * @return the faturaEncerramentoGerada
	 */
	Boolean getFaturaEncerramentoGerada();

	/**
	 * Sets the data recisao.
	 *
	 * @param dataRecisao the dataRecisao to set
	 */
	void setDataRecisao(Date dataRecisao);

	/**
	 * Gets the data recisao.
	 *
	 * @return the dataRecisao
	 */
	Date getDataRecisao();

	/**
	 * Gets the numero contrato com aditivo.
	 *
	 * @return the numero contrato com aditivo
	 */
	String getNumeroContratoComAditivo();

	/**
	 * Gets the numero completo contrato.
	 *
	 * @return the numero completo contrato
	 */
	String getNumeroCompletoContrato();

	/**
	 * Sets the numero completo contrato.
	 *
	 * @param numeroCompletoContrato the new numero completo contrato
	 */
	void setNumeroCompletoContrato(String numeroCompletoContrato);

	/**
	 * Gets the numero formatado sem aditivo.
	 *
	 * @return the numero formatado sem aditivo
	 */
	String getNumeroFormatadoSemAditivo();

	/**
	 * Gets the percentual juros mora.
	 *
	 * @return the percentual juros mora
	 */
	BigDecimal getPercentualJurosMora();

	/**
	 * Sets the percentual juros mora.
	 *
	 * @param percentualJurosMora the new percentual juros mora
	 */
	void setPercentualJurosMora(BigDecimal percentualJurosMora);

	/**
	 * Gets the percentual multa.
	 *
	 * @return the percentual multa
	 */
	BigDecimal getPercentualMulta();

	/**
	 * Sets the percentual multa.
	 *
	 * @param percentualMulta the new percentual multa
	 */
	void setPercentualMulta(BigDecimal percentualMulta);

	/**
	 * Gets the tipo periodicidade penalidade.
	 *
	 * @return the tipo periodicidade penalidade
	 */
	Boolean getTipoPeriodicidadePenalidade();

	/**
	 * Sets the tipo periodicidade penalidade.
	 *
	 * @param tipoPeriodicidadePenalidade the new tipo periodicidade penalidade
	 */
	void setTipoPeriodicidadePenalidade(Boolean tipoPeriodicidadePenalidade);

	/**
	 * Gets the arrecadador contrato convenio.
	 *
	 * @return the arrecadador contrato convenio
	 */
	ArrecadadorContratoConvenio getArrecadadorContratoConvenio();

	/**
	 * Sets the arrecadador contrato convenio.
	 *
	 * @param arrecadadorContratoConvenio the new arrecadador contrato convenio
	 */
	void setArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio);
	
	/**
	 * Obtém o convênio para débito automático.
	 *
	 * @return o convênio
	 */
	ArrecadadorContratoConvenio getArrecadadorContratoConvenioDebitoAutomatico();
	
	/**
	 * Define o convênio para débito automático.
	 *
	 * @param arrecadadorContratoConvenioDebitoAutomatico o novo convênio
	 */
	void setArrecadadorContratoConvenioDebitoAutomatico(ArrecadadorContratoConvenio arrecadadorContratoConvenioDebitoAutomatico);

	/**
	 * Gets the lista servico autorizacao.
	 *
	 * @return the lista servico autorizacao
	 */
	Collection<ServicoAutorizacao> getListaServicoAutorizacao();

	/**
	 * Sets the lista servico autorizacao.
	 *
	 * @param listaServicoAutorizacao the new lista servico autorizacao
	 */
	void setListaServicoAutorizacao(Collection<ServicoAutorizacao> listaServicoAutorizacao);

	/**
	 * Gets the periodicidade maior menor.
	 *
	 * @return the periodicidade maior menor
	 */
	EntidadeConteudo getPeriodicidadeMaiorMenor();

	/**
	 * Sets the periodicidade maior menor.
	 *
	 * @param periodicidadeMaiorMenor the new periodicidade maior menor
	 */
	void setPeriodicidadeMaiorMenor(EntidadeConteudo periodicidadeMaiorMenor);

	/**
	 * Gets the descricao contrato.
	 *
	 * @return the descricao contrato
	 */
	String getDescricaoContrato();

	/**
	 * Sets the descricao contrato.
	 *
	 * @param descricaoContrato the new descricao contrato
	 */
	void setDescricaoContrato(String descricaoContrato);

	/**
	 * Gets the volume referencia.
	 *
	 * @return the volume referencia
	 */
	BigDecimal getVolumeReferencia();

	/**
	 * Sets the volume referencia.
	 *
	 * @param volumeReferencia the new volume referencia
	 */
	void setVolumeReferencia(BigDecimal volumeReferencia);

	/**
	 * Gets the chave primaria principal.
	 *
	 * @return the chave primaria principal
	 */
	Long getChavePrimariaPrincipal();

	/**
	 * Sets the chave primaria principal.
	 *
	 * @param chavePrimariaPrincipal the new chave primaria principal
	 */
	void setChavePrimariaPrincipal(Long chavePrimariaPrincipal);

	/**
	 * Gets the ordem faturamento.
	 *
	 * @return the ordem faturamento
	 */
	Integer getOrdemFaturamento();

	/**
	 * Sets the ordem faturamento.
	 *
	 * @param ordemFaturamento the new ordem faturamento
	 */
	void setOrdemFaturamento(Integer ordemFaturamento);

	/**
	 * Gets the lista contrato penalidade.
	 *
	 * @return the lista contrato penalidade
	 */
	Collection<ContratoPenalidade> getListaContratoPenalidade();

	/**
	 * Sets the lista contrato penalidade.
	 *
	 * @param listaContratoPenalidade the new lista contrato penalidade
	 */
	void setListaContratoPenalidade(Collection<ContratoPenalidade> listaContratoPenalidade);

	/**
	 * Gets the data inicio retirada qpnr.
	 *
	 * @return the data inicio retirada qpnr
	 */
	Date getDataInicioRetiradaQPNR();

	/**
	 * Sets the data inicio retirada qpnr.
	 *
	 * @param dataInicioRetiradaQPNR the new data inicio retirada qpnr
	 */
	void setDataInicioRetiradaQPNR(Date dataInicioRetiradaQPNR);

	/**
	 * Gets the data fim retirada qpnr.
	 *
	 * @return the data fim retirada qpnr
	 */
	Date getDataFimRetiradaQPNR();

	/**
	 * Sets the data fim retirada qpnr.
	 *
	 * @param dataFimRetiradaQPNR the new data fim retirada qpnr
	 */
	void setDataFimRetiradaQPNR(Date dataFimRetiradaQPNR);

	/**
	 * Gets the percentual qdc contrato qpnr.
	 *
	 * @return the percentual qdc contrato qpnr
	 */
	BigDecimal getPercentualQDCContratoQPNR();

	/**
	 * Sets the percentual qdc contrato qpnr.
	 *
	 * @param percentualQDCContratoQPNR the new percentual qdc contrato qpnr
	 */
	void setPercentualQDCContratoQPNR(BigDecimal percentualQDCContratoQPNR);

	/**
	 * Gets the percentual minimo qdc contrato qpnr.
	 *
	 * @return the percentual minimo qdc contrato qpnr
	 */
	BigDecimal getPercentualMinimoQDCContratoQPNR();

	/**
	 * Sets the percentual minimo qdc contrato qpnr.
	 *
	 * @param percentualMinimoQDCContratoQPNR the new percentual minimo qdc contrato qpnr
	 */
	void setPercentualMinimoQDCContratoQPNR(BigDecimal percentualMinimoQDCContratoQPNR);

	/**
	 * Gets the percentual qdc fim contrato qpnr.
	 *
	 * @return the percentual qdc fim contrato qpnr
	 */
	BigDecimal getPercentualQDCFimContratoQPNR();

	/**
	 * Sets the percentual qdc fim contrato qpnr.
	 *
	 * @param percentualQDCFimContratoQPNR the new percentual qdc fim contrato qpnr
	 */
	void setPercentualQDCFimContratoQPNR(BigDecimal percentualQDCFimContratoQPNR);

	/**
	 * Gets the anos validade retirada qpnr.
	 *
	 * @return the anos validade retirada qpnr
	 */
	Integer getAnosValidadeRetiradaQPNR();

	/**
	 * Sets the anos validade retirada qpnr.
	 *
	 * @param anosValidadeRetiradaQPNR the new anos validade retirada qpnr
	 */
	void setAnosValidadeRetiradaQPNR(Integer anosValidadeRetiradaQPNR);

	/**
	 * Obtém o indicador de debito automatico.
	 *
	 * @return o indicador debito automatico
	 */
	boolean getIndicadorDebitoAutomatico();

	/**
	 * Define o indicador de debito automatico.
	 *
	 * @param indicadorDebitoAutomatico o indicador de debito automatico
	 */
	void setIndicadorDebitoAutomatico(boolean indicadorDebitoAutomatico);

	/**
	 * Obtém o indicador de participação no e-cartas.
	 *
	 * @return o indicador de participação no e-cartas
	 */
	Boolean getIndicadorParticipanteECartas();

	/**
	 * Define o indicador de participação no e-cartas.
	 *
	 * @param indicadorParticipanteECartas o indicador de participação no e-cartas.
	 */
	void setIndicadorParticipanteECartas(Boolean indicadorParticipanteECartas);
	
	/**
	 * @return indicador proposta aprovada
	 */
	public boolean isPropostaAprovada();

	/**
	 * @param propostaAprovada
	 */
	public void setPropostaAprovada(boolean propostaAprovada);

	/**
	 * Obtém o indicador do endereço padrao
	 * 
	 * @return indicador do endereço padrao
	 */
	public boolean isEnderecoPadrao();

	/**
	 * Define o indicador do endereço padrao
	 * 
	 * @param enderecoPadrao
	 */
	public void setEnderecoPadrao(boolean enderecoPadrao);

	/**
	 * @return indicador exige aprovação
	 */
	public boolean isExigeAprovacao();

	/**
	 * @param exigeAprovacao
	 */
	public void setExigeAprovacao(boolean exigeAprovacao);
	
	/**
	 * @return anexos
	 */
	public Collection<ContratoAnexo> getAnexos();

	/**
	 * @param anexos
	 */
	public void setAnexos(Collection<ContratoAnexo> anexos);

	/**
	 * Substitui lista de anexo
	 */
	void substituirListaAnexo();

	Banco getBanco();
	void setBanco(Banco banco);

	String getAgencia();
	void setAgencia(String agencia);

	String getContaCorrente();
	void setContaCorrente(String contaCorrente);

	BigDecimal getIncentivosComerciais();

	void setIncentivosComerciais(BigDecimal incentivosComerciais);

	BigDecimal getIncentivosInfraestrutura();

	void setIncentivosInfraestrutura(BigDecimal incentivosInfraestrutura);

	Integer getPrazoVigencia();

	void setPrazoVigencia(Integer prazoVigencia);
}
