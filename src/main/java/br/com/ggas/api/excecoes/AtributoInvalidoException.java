package br.com.ggas.api.excecoes;

/**
 * Exceção será lançada se algum atributo não estiver preenchido ou se estiver preenchido de forma inadequada
 */
public class AtributoInvalidoException extends Exception {

	/**
	 * Inicializa a exceção com a mensagem correspondente
	 * @param message com o que está inválido
	 */
	public AtributoInvalidoException(String message) {
		super(message);
	}
}
