/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.autenticador;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Autenticador para verificar o token recebido
 */
public class Autenticador {

	private static Optional<Autenticador> autenticator = Optional.empty();

	/**
	 * Guarda os tokens
	 */
	private final Map<Long, TokenAutenticador> tokens = new HashMap<>();

	private Autenticador() {
	}

	/**
	 * Método para retornar o objeto da classe, garantindo que somente uma instância será criada
	 *
	 * @return uma instáncia da própria classe
	 */
	public static Autenticador getInstance() {
		if (autenticator.isPresent()) {
			return autenticator.get();
		}
		autenticator = Optional.of(new Autenticador());
		return autenticator.get();
	}

	/**
	 * Método para criar um novo token de autenticação
	 *
	 * @return uma string aleatória pseudo criptografada
	 */
	public TokenAutenticador buildToken() {
		return TokenAutenticador.buildTokenAutenticador(UUID.randomUUID().toString());
	}

	/**
	 * Salva o token do usuário no mapa de tokens para consultar quando receber uma requisição, evitando ir no banco
	 *
	 * @param chavePrimariaUsuario
	 *            utilizada como chave do mapa de tokens {@link Long}
	 * @param token
	 *            que será salvo para ser verifiado no mapa e evitar ter que
	 *            consultar no banco de dados {@link String}
	 */
	public void salvarToken(Long chavePrimariaUsuario, String token) {
		removerTodosOsTokensDoUsuario(chavePrimariaUsuario);
		tokens.put(chavePrimariaUsuario, TokenAutenticador.buildTokenAutenticador(token));
	}

	private void removerTodosOsTokensDoUsuario(Long chavePrimariaUsuario) {
		List<Long> tokensRemover = new ArrayList<>();
		tokens.forEach((key, chave) -> {
			if (key.equals(chavePrimariaUsuario)) {
				tokensRemover.add(key);
			}
		});
		tokensRemover.forEach(tokens::remove);
	}

	/**
	 * Verifica se um token é válido, para ser válido ele precisa existir no mapa da instância
	 *
	 * @param token para ser verificado se é válido ou não
	 * @return se o token é válido
	 */
	public boolean isTokenValid(String token) {
		Optional<Map.Entry<Long, TokenAutenticador>> tokenAutenticador =
				tokens.entrySet().stream().filter(ta -> ta.getValue().getToken().equals(token)).findFirst();

		if (tokenAutenticador.isPresent()) {
			final long MINUTOS_PARA_EXPIRAR_TOKEN = 30;
			LocalDateTime horarioExpirar =
					tokenAutenticador.get().getValue().getDataHoraCriacao().plus(MINUTOS_PARA_EXPIRAR_TOKEN, ChronoUnit.MINUTES);
			if (horarioExpirar.isBefore(LocalDateTime.now())) {
				return false;
			}

			atualizarTempoExpirarToken(tokenAutenticador);
		}

		return tokenAutenticador.isPresent();
	}

	private void atualizarTempoExpirarToken(Optional<Map.Entry<Long, TokenAutenticador>> tokenAutenticador) {
		if (tokenAutenticador.isPresent()) {
			tokenAutenticador.get().getValue().atualizarDataHoraCriacao();
			tokens.put(tokenAutenticador.get().getKey(), tokenAutenticador.get().getValue());
		}
	}

	/**
	 * Invalidar o token do mapa
	 *
	 * @param chavePrimariaUsuario chave do mapa para ser removido
	 * @param token                para verificar se o token ainda existe
	 */
	public void logout(Long chavePrimariaUsuario, String token) {
		if (isTokenValid(token)) {

			Optional<Map.Entry<Long, TokenAutenticador>> tokenEncontrado =
					tokens.entrySet().stream().filter(tokenAutenticador -> tokenAutenticador.getValue().getToken().equals(token))
							.findFirst();
			if (tokenEncontrado.isPresent() && tokenEncontrado.get().getKey().equals(chavePrimariaUsuario)) {
				tokens.remove(tokenEncontrado.get().getKey());
			}
		}
	}
}
