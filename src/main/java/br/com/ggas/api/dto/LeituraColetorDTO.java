/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.dto;

import br.com.ggas.api.rest.MedicaoRest;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe DTO que representará o objeto de leitura realizado pelo Coletor
 */
public class LeituraColetorDTO implements Serializable {

	private static final Logger LOG = Logger.getLogger(LeituraColetorDTO.class);
	public static final String FORMATO_DATA = "dd/MM/yyyy HH:mm";

	private Integer anoMesFaturamento;
	private Integer ciclo;
	private Long pontoConsumo;
	private String nomeCliente;
	private String descricaoPontoConsumo;
	private String enderecoCliente;
	private Double valorLeitura;
	private Double pressaoInformada;
	private Double temperaturaInformada;
	private String dataLeitura;
	private Integer codigoAnormalidadeLeitura;
	private boolean indicadorConfirmacaoLeitura;
	private Long chaveSituacaoConsumo;
	private Long chavePrimariaLeituraMovimento;
	private String observacao;

	private String mensagem;

	private Double latitude;
	private Double longitude;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getAnoMesFaturamento() {
		return anoMesFaturamento;
	}

	public void setAnoMesFaturamento(Integer anoMesFaturamento) {
		this.anoMesFaturamento = anoMesFaturamento;
	}

	public Integer getCiclo() {
		return ciclo;
	}

	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}

	public Long getPontoConsumo() {
		return pontoConsumo;
	}

	public void setPontoConsumo(Long pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getEnderecoCliente() {
		return enderecoCliente;
	}

	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}

	public Double getValorLeitura() {
		return valorLeitura;
	}

	public void setValorLeitura(Double valorLeitura) {
		this.valorLeitura = valorLeitura;
	}

	public Double getPressaoInformada() {
		return pressaoInformada;
	}

	public void setPressaoInformada(Double pressaoInformada) {
		this.pressaoInformada = pressaoInformada;
	}

	public Double getTemperaturaInformada() {
		return temperaturaInformada;
	}

	public void setTemperaturaInformada(Double temperaturaInformada) {
		this.temperaturaInformada = temperaturaInformada;
	}

	public String getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}

	public Integer getCodigoAnormalidadeLeitura() {
		return codigoAnormalidadeLeitura;
	}

	public void setCodigoAnormalidadeLeitura(Integer codigoAnormalidadeLeitura) {
		this.codigoAnormalidadeLeitura = codigoAnormalidadeLeitura;
	}

	public boolean isIndicadorConfirmacaoLeitura() {
		return indicadorConfirmacaoLeitura;
	}

	public void setIndicadorConfirmacaoLeitura(boolean indicadorConfirmacaoLeitura) {
		this.indicadorConfirmacaoLeitura = indicadorConfirmacaoLeitura;
	}

	public Long getChaveSituacaoConsumo() {
		return chaveSituacaoConsumo;
	}

	public void setChaveSituacaoConsumo(Long chaveSituacaoConsumo) {
		this.chaveSituacaoConsumo = chaveSituacaoConsumo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Long getChavePrimariaLeituraMovimento() {
		return chavePrimariaLeituraMovimento;
	}

	public void setChavePrimariaLeituraMovimento(Long chavePrimariaLeituraMovimento) {
		this.chavePrimariaLeituraMovimento = chavePrimariaLeituraMovimento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	/**
	 * Método para converter o texto do atributo dataLeitura para um objeto Date
	 * @return a data de leitura no formato de data do Java (java.util.Date)
	 * @throws ParseException (se getDataLeitura retornar em um formato diferente do esperado
	 */
	public Date dataLeituraEscrita() throws ParseException {
		return new SimpleDateFormat(FORMATO_DATA).parse(getDataLeitura());
	}

	/**
	 * Converter uma leitura movimento oficial do GGAS para uma leitura que representa os dados do Coletor (Essa própria classe)
	 *
	 * @param leituraMovimento uma instância de leitura movimento do GGAS
	 * @return uma leitura do coletor
	 */
	public static LeituraColetorDTO converterLeituraMovimento(LeituraMovimento leituraMovimento) {
		LeituraColetorDTO leitura;
		try {
			leitura = new LeituraColetorDTO();
			leitura.setAnoMesFaturamento(leituraMovimento.getAnoMesFaturamento());
			leitura.setCiclo(leituraMovimento.getCiclo());
			leitura.setPontoConsumo(leituraMovimento.getPontoConsumo().getChavePrimaria());
			leitura.setNomeCliente(leituraMovimento.getNomeCliente());
			leitura.setDescricaoPontoConsumo(leituraMovimento.getDescicaoPontoConsumo());
			leitura.setEnderecoCliente(leituraMovimento.getEndereco());
			leitura.setMensagem(MedicaoRest.LEITURA_MOVIMENTO_ENCONTRADA);
			leitura.setChavePrimariaLeituraMovimento(leituraMovimento.getChavePrimaria());
			leitura.setChaveSituacaoConsumo(leituraMovimento.getSituacaoLeitura().getChavePrimaria());

			leitura.setPressaoInformada(getValorBigDecimal(leituraMovimento.getPressaoInformada()));
			leitura.setTemperaturaInformada(getValorBigDecimal(leituraMovimento.getTemperaturaInformada()));
			leitura.setValorLeitura(getValorBigDecimal(leituraMovimento.getValorLeitura()));
			if (leituraMovimento.getDataLeitura() != null) {
				leitura.setDataLeitura(new SimpleDateFormat(FORMATO_DATA).format(leituraMovimento.getDataLeitura()));
			}
		} catch (Exception e) {
			LOG.error("Não foi possível converter leitura movimento para uma leitura coletor", e);
			leitura = null;
		}
		return leitura;
	}

	private static Double getValorBigDecimal(BigDecimal valor) {
		if (valor != null) {
			return valor.doubleValue();
		}
		return null;
	}

}
