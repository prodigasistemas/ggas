package br.com.ggas.api.dto;

import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;

import java.io.Serializable;

/**
 * Classe representando uma anormalidade de leitura com campos reduzidos
 */
public class AnormalidadeLeituraDTO implements Serializable {

	private Long id;

	private String nome;

	private Boolean ativo;

	private Boolean aceitaLeitura;

	private Boolean exigeFoto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isAceitaLeitura() {
		return aceitaLeitura;
	}

	public void setAceitaLeitura(Boolean aceitaLeitura) {
		this.aceitaLeitura = aceitaLeitura;
	}

	public Boolean getExigeFoto() {
		return exigeFoto;
	}

	public void setExigeFoto(Boolean exigeFoto) {
		this.exigeFoto = exigeFoto;
	}

	/**
     * Cria um objeto AnormalidadeLeituraDTO a partir de um objeto AnormalidadeLeitura
     * @param anormalidade O objeto do tipo AnormalidadeLeitura
     * @return Um objeto AnormalidadeLeituraDTO
     */
	public static AnormalidadeLeituraDTO valueOf(AnormalidadeLeitura anormalidade) {
		AnormalidadeLeituraDTO anormalidadeLeituraDTO = new AnormalidadeLeituraDTO();
		anormalidadeLeituraDTO.setId(anormalidade.getChavePrimaria());
		anormalidadeLeituraDTO.setNome(anormalidade.getDescricao());
		anormalidadeLeituraDTO.setAtivo(anormalidade.isHabilitado());
		anormalidadeLeituraDTO.setAceitaLeitura(anormalidade.getAceitaLeitura());
		anormalidadeLeituraDTO.setExigeFoto(anormalidade.getExigeFoto());
		return anormalidadeLeituraDTO;
	}

}
