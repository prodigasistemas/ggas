package br.com.ggas.api.dto;

import br.com.ggas.util.Constantes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe representando uma LeituraMovimento nas operações da API
 */
public class LeituraMovimentoDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_BR);

	private Long id;
	private String dataGeracao;
	private Integer anoMesFaturamento;
	private Integer ciclo;
	private Long idPontoConsumo;
	private String descricaoPontoConsumo;
	private String observacaoPontoConsumo;
	private Long idRota;
	private String rota;
	private Integer sequenciaLeitura;
	private String endereco;
	private String complementoEndereco;
	private String bairro;
	private String municipio;
	private String uf;
	private String cep;
	private String numeroSerieMedidor;
	private String localInstalacao;
	private String dataLeituraPrevista;
	private Long situacaoLeitura;
	private Boolean corrigePT;
	private String indicadorCorrecaoPT;
	private String origem;
	private String dataLeitura;
	private BigDecimal valorLeitura;
	private BigDecimal pressaoInformada;
	private BigDecimal temperaturaInformada;
	private Long codigoAnormalidadeLeitura;
	private Long idLeiturista;
	private String observacao;
	private BigDecimal minimoLeituraEsperada;
	private BigDecimal maximoLeituraEsperada;

	/**
	 * Cria um LeituraMovimentoDTO a paritr dos parametros informados
	 * @param id o id da leitura
	 * @param dataGeracao a data de geração da leitura
	 * @param anoMesFaturamento o ano/mês do faturamento
	 * @param ciclo o ciclo
	 * @param idPontoConsumo o id do ponto de consumo
	 * @param descricaoPontoConsumo a descrição do ponto de consumo
	 * @param observacaoPontoConsumo a observação do ponto de consumo
	 * @param idRota o id da rota
	 * @param rota a descrição da rota
	 * @param sequenciaLeitura a sequencia da leitura
	 * @param endereco o endereço do ponto de consumo
	 * @param complementoEndereco o complemento do endereço do ponto de consumo
	 * @param bairro o bairro do ponto de consumo
	 * @param municipio o municipio do ponto de consumo
	 * @param uf a uf do ponto de consumo
	 * @param cep o cep do ponto de consumo
	 * @param numeroSerieMedidor o número de série do medidor
	 * @param localInstalacao o local de instalação do medidoro
	 * @param dataLeituraPrevista a data de leitura prevista
	 * @param situacaoLeitura a situação da leitura
	 * @param corrigePT o indicador de correção de pressão e temperatura
	 * @param indicadorCorrecaoPT o indicador de correção de pressão e temperatura
	 * @param origem a origem
	 * @param dataLeitura a data de leitura
	 * @param valorLeitura o valor da leitura
	 * @param pressaoInformada a pressão informada
	 * @param temperaturaInformada a temperatura informada
	 * @param codigoAnormalidadeLeitura o código da anormalidade de leitura
	 * @param idLeiturista o id do leiturista
	 * @param observacao a observação da leitura
	 * @param minimoLeituraEsperada a leitura mínima esperada
	 * @param maximoLeituraEsperada a leitura máxima esperada
	 */
	public LeituraMovimentoDTO(Long id, Date dataGeracao, Integer anoMesFaturamento, Integer ciclo, Long idPontoConsumo,
			String descricaoPontoConsumo, String observacaoPontoConsumo, Long idRota, String rota, Integer sequenciaLeitura,
			String endereco, String complementoEndereco, String bairro, String municipio, String uf, String cep, String numeroSerieMedidor,
			String localInstalacao, Date dataLeituraPrevista, Long situacaoLeitura, Boolean corrigePT, String indicadorCorrecaoPT,
			String origem, Date dataLeitura, BigDecimal valorLeitura, BigDecimal pressaoInformada, BigDecimal temperaturaInformada,
			Long codigoAnormalidadeLeitura, Long idLeiturista, String observacao, BigDecimal minimoLeituraEsperada,
			BigDecimal maximoLeituraEsperada) {
		this.id = id;
		this.dataGeracao = formatarData(dataGeracao);
		this.anoMesFaturamento = anoMesFaturamento;
		this.ciclo = ciclo;
		this.idPontoConsumo = idPontoConsumo;
		this.descricaoPontoConsumo = descricaoPontoConsumo;
		this.observacaoPontoConsumo = observacaoPontoConsumo;
		this.idRota = idRota;
		this.rota = rota;
		this.sequenciaLeitura = sequenciaLeitura;
		this.endereco = endereco;
		this.complementoEndereco = complementoEndereco;
		this.bairro = bairro;
		this.municipio = municipio;
		this.uf = uf;
		this.cep = cep;
		this.numeroSerieMedidor = numeroSerieMedidor;
		this.localInstalacao = localInstalacao;
		this.dataLeituraPrevista = formatarData(dataLeituraPrevista);
		this.situacaoLeitura = situacaoLeitura;
		this.corrigePT = corrigePT;
		this.indicadorCorrecaoPT = indicadorCorrecaoPT;
		this.origem = origem;
		this.dataLeitura = formatarData(dataLeitura);
		this.valorLeitura = valorLeitura;
		this.pressaoInformada = pressaoInformada;
		this.temperaturaInformada = temperaturaInformada;
		this.codigoAnormalidadeLeitura = codigoAnormalidadeLeitura;
		this.idLeiturista = idLeiturista;
		this.observacao = observacao;
		this.minimoLeituraEsperada = minimoLeituraEsperada;
		this.maximoLeituraEsperada = maximoLeituraEsperada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataGeracao() {
		return dataGeracao;
	}

	public void setDataGeracao(String dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	public Integer getAnoMesFaturamento() {
		return anoMesFaturamento;
	}

	public void setAnoMesFaturamento(Integer anoMesFaturamento) {
		this.anoMesFaturamento = anoMesFaturamento;
	}

	public Integer getCiclo() {
		return ciclo;
	}

	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getObservacaoPontoConsumo() {
		return observacaoPontoConsumo;
	}

	public void setObservacaoPontoConsumo(String observacaoPontoConsumo) {
		this.observacaoPontoConsumo = observacaoPontoConsumo;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public String getRota() {
		return rota;
	}

	public void setRota(String rota) {
		this.rota = rota;
	}

	public Integer getSequenciaLeitura() {
		return sequenciaLeitura;
	}

	public void setSequenciaLeitura(Integer sequenciaLeitura) {
		this.sequenciaLeitura = sequenciaLeitura;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getComplementoEndereco() {
		return complementoEndereco;
	}

	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNumeroSerieMedidor() {
		return numeroSerieMedidor;
	}

	public void setNumeroSerieMedidor(String numeroSerieMedidor) {
		this.numeroSerieMedidor = numeroSerieMedidor;
	}

	public String getLocalInstalacao() {
		return localInstalacao;
	}

	public void setLocalInstalacao(String localInstalacao) {
		this.localInstalacao = localInstalacao;
	}

	public String getDataLeituraPrevista() {
		return dataLeituraPrevista;
	}

	public void setDataLeituraPrevista(String dataLeituraPrevista) {
		this.dataLeituraPrevista = dataLeituraPrevista;
	}

	public Long getSituacaoLeitura() {
		return situacaoLeitura;
	}

	public void setSituacaoLeitura(Long situacaoLeitura) {
		this.situacaoLeitura = situacaoLeitura;
	}

	public Boolean getCorrigePT() {
		return corrigePT;
	}

	public void setCorrigePT(Boolean corrigePT) {
		this.corrigePT = corrigePT;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}

	public BigDecimal getValorLeitura() {
		return valorLeitura;
	}

	public void setValorLeitura(BigDecimal valorLeitura) {
		this.valorLeitura = valorLeitura;
	}

	public BigDecimal getPressaoInformada() {
		return pressaoInformada;
	}

	public void setPressaoInformada(BigDecimal pressaoInformada) {
		this.pressaoInformada = pressaoInformada;
	}

	public BigDecimal getTemperaturaInformada() {
		return temperaturaInformada;
	}

	public void setTemperaturaInformada(BigDecimal temperaturaInformada) {
		this.temperaturaInformada = temperaturaInformada;
	}

	public Long getCodigoAnormalidadeLeitura() {
		return codigoAnormalidadeLeitura;
	}

	public void setCodigoAnormalidadeLeitura(Long codigoAnormalidadeLeitura) {
		this.codigoAnormalidadeLeitura = codigoAnormalidadeLeitura;
	}

	public Long getIdLeiturista() {
		return idLeiturista;
	}

	public void setIdLeiturista(Long idLeiturista) {
		this.idLeiturista = idLeiturista;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getIndicadorCorrecaoPT() {
		return indicadorCorrecaoPT;
	}

	public void setIndicadorCorrecaoPT(String indicadorCorrecaoPT) {
		this.indicadorCorrecaoPT = indicadorCorrecaoPT;
	}

	public BigDecimal getMinimoLeituraEsperada() {
		return minimoLeituraEsperada;
	}

	public void setMinimoLeituraEsperada(BigDecimal minimoLeituraEsperada) {
		this.minimoLeituraEsperada = minimoLeituraEsperada;
	}

	public BigDecimal getMaximoLeituraEsperada() {
		return maximoLeituraEsperada;
	}

	public void setMaximoLeituraEsperada(BigDecimal maximoLeituraEsperada) {
		this.maximoLeituraEsperada = maximoLeituraEsperada;
	}

	private String formatarData(Date data) {
		if (data != null) {
			return format.format(data);
		}

		return null;
	}
}
