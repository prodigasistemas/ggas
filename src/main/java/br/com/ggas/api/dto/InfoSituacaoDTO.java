/*
t Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.dto;

import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Classe utilizada nas operações da api representando uma lista de rotas e o menor ano/Mes de faturamento presente nessa listas
 */
public class InfoSituacaoDTO implements Serializable {

	private Integer menorAnoMesFaturamento;

	private List<RotaDTO> rotasAtivas;

	public Integer getMenorAnoMesFaturamento() {
		return menorAnoMesFaturamento;
	}

	public void setMenorAnoMesFaturamento(Integer menorAnoMesFaturamento) {
		this.menorAnoMesFaturamento = menorAnoMesFaturamento;
	}

	public List<RotaDTO> getRotasAtivas() {
		return rotasAtivas;
	}

	public void setRotasAtivas(List<RotaDTO> rotasAtivas) {
		this.rotasAtivas = rotasAtivas;
	}

	/**
	 * Atualiza as informações da instância a partir de uma lista de leituras
	 * @param leituras A lista de leituras
	 */
	public void preencherDados(List<RotaDTO> leituras) {

		int menorAnoMesReferencia = 0;

		if (CollectionUtils.isNotEmpty(leituras)) {
			menorAnoMesReferencia = leituras.stream().mapToInt(RotaDTO::getAnoMesFaturamento).min().orElse(0);
		}

		this.menorAnoMesFaturamento = menorAnoMesReferencia;

		this.rotasAtivas = leituras;
	}

}
