/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.dto;

import java.io.Serializable;

/**
 * Classe de resposta para as chamadas da API com indicadores sobre as leituras precessadas
 */
public class RespostaSalvarLeituraDTO implements Serializable {

	public static final String ATUALIZADA = "ATUALIZADA";

	public static final String FALHA = "FALHA";

	private Long idLeituraMovimento;

	private String statusMedicao;

	private String excecao;

	/**
	 * Construtor a partir de uma leitura movimento e status de medição
	 * @param idLeituraMovimento A leitura movimento
	 * @param statusMedicao O status de medição
	 */
	public RespostaSalvarLeituraDTO(Long idLeituraMovimento, String statusMedicao) {
		this.idLeituraMovimento = idLeituraMovimento;
		this.statusMedicao = statusMedicao;
	}

	/**
	 * Construtor a partir de uma leitura movimento, status de medição e exceção lançada na operação
	 * @param idLeituraMovimento A leitura movimento
	 * @param statusMedicao O status de medição
	 * @param excecao A exceção lançada durante as operações
	 */
	public RespostaSalvarLeituraDTO(Long idLeituraMovimento, String statusMedicao, String excecao) {
		this.idLeituraMovimento = idLeituraMovimento;
		this.statusMedicao = statusMedicao;
		this.excecao = excecao;
	}

	public Long getIdLeituraMovimento() {
		return idLeituraMovimento;
	}

	public void setIdLeituraMovimento(Long idLeituraMovimento) {
		this.idLeituraMovimento = idLeituraMovimento;
	}

	public String getStatusMedicao() {
		return statusMedicao;
	}

	public void setStatusMedicao(String statusMedicao) {
		this.statusMedicao = statusMedicao;
	}

	public String getExcecao() {
		return excecao;
	}

	public void setExcecao(String excecao) {
		this.excecao = excecao;
	}

}
