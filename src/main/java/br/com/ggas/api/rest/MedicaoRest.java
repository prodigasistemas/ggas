/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.rest;

import br.com.ggas.api.dto.*;
import br.com.ggas.api.excecoes.AtributoInvalidoException;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.impl.TipoAtividadeSistema;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.OrigemLeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.util.ServiceLocator;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

/**
 * Controlador de medições
 */
@Controller @RequestMapping(value = "api/v1/medicoes")
public class MedicaoRest {

	private static final Logger LOG = Logger.getLogger(MedicaoRest.class);

	public static final String SUCESSO_ESCRITA_LEITURA_MOVIMENTO = "Leitura recebida e processada com sucesso";
	public static final String LEITURA_MOVIMENTO_ENCONTRADA = "Leitura movimento encontrada para o cliente informado";
	public static final String CHAVE_LEITURA_MOVIMENTO_OBRIGATORIO =
			"Chave da leitura movimento é obrigatório e deve ser maior do que zero";
	public static final String ANO_MES_FATURAMENTO_OBRIGATORIO = "Ano/Mês é obrigatório";
	private static final String CICLO_OBRIGATORIO = "Ciclo é obrigatório e deve ser maior do que zero";
	private static final String PONTO_CONSUMO_OBRIGATORIO = "Ponto de consumo é obrigatório";
	private static final String VALOR_LEITURA_OBRIGATORIO = "Valor de leitura é obrigatório";
	private static final String PRESSAO_OBRIGATORIO = "Pressão é obrigatório";
	private static final String TEMPERATURA_OBRIGATORIO = "Temperatura é obrigatório";
	private static final String DATA_LEITURA_OBRIGATORIO = "Data de leitura é obrigatório";
	private static final String ANORMALIDADE_LEITURA_NAO_ENCONTRADA = "A anormalidade de leitura não foi encontrada.";
	private static final String LEITURA_MOVIMENTO_JA_FOI_PROCESSADA = "Leitura movimento já foi processada";

	@Autowired
	@Qualifier(ControladorCronogramaAtividadeFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_ATIVIDADE_FATURAMENTO)
	protected ControladorCronogramaAtividadeFaturamento controladorCronAtvFaturamento;

	/**
	 * Recebe uma leitura do coletor e processa para realizar
	 * integração
	 *
	 * @param leitura deve ser preenchido na requisição
	 * @return uma instância de ResponseEntity com o resultado da operação
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity salvar(@RequestBody LeituraColetorDTO leitura) {
		try {
			this.validarLeituraColetor(leitura);
			ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();

			LeituraMovimento leituraMovimento =
					(LeituraMovimento) controladorLeituraMovimento.obter(leitura.getChavePrimariaLeituraMovimento());
			if (SituacaoLeituraMovimento.PROCESSADO == leituraMovimento.getSituacaoLeitura().getChavePrimaria()) {
				throw new AtributoInvalidoException(LEITURA_MOVIMENTO_JA_FOI_PROCESSADA);
			}
			leituraMovimento.setPressaoInformada(BigDecimal.valueOf(leitura.getPressaoInformada()));
			leituraMovimento.setTemperaturaInformada(BigDecimal.valueOf(leitura.getTemperaturaInformada()));
			leituraMovimento.setValorLeitura(BigDecimal.valueOf(leitura.getValorLeitura()));
			leituraMovimento.setDataLeitura(leitura.dataLeituraEscrita());
			leituraMovimento.setSituacaoLeitura(
					controladorLeituraMovimento.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.LEITURA_RETORNADA));
			leituraMovimento.setIndicadorConfirmacaoLeitura(true);
			leituraMovimento.setOrigem(OrigemLeituraMovimento.API.name());
			controladorLeituraMovimento.atualizar(leituraMovimento);

			controladorCronAtvFaturamento.atualizarAtividadeFaturamentoSeExistir(leituraMovimento.getGrupoFaturamento().getChavePrimaria(),
					TipoAtividadeSistema.REGISTRAR_LEITURA.getNumeroSequencia(), leituraMovimento.getGrupoFaturamento().getAnoMesReferencia());

			return new ResponseEntity<>(MedicaoRest.SUCESSO_ESCRITA_LEITURA_MOVIMENTO, HttpStatus.CREATED);
		} catch (Exception e) {
			return tratarErro(e);
		}
	}

	/**
	 * Atualiza uma lista de leituras com os dados de medição
	 * @param leituras A lista de leitura movimento medida
	 * @return o ResponseEntity representando o status das leituras processadas
	 */
	@RequestMapping(value = "/salvarLeituras", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity salvarLeituras(@RequestBody List<LeituraColetorDTO> leituras) {

		List<RespostaSalvarLeituraDTO> statusResposta = Lists.newArrayList();

		for (LeituraColetorDTO leitura : leituras) {

			try {
				this.validarLeituraColetorSGM(leitura);
				ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();

				LeituraMovimento leituraMovimento =
						(LeituraMovimento) controladorLeituraMovimento.obter(leitura.getChavePrimariaLeituraMovimento());
				if (SituacaoLeituraMovimento.PROCESSADO == leituraMovimento.getSituacaoLeitura().getChavePrimaria()) {
					throw new AtributoInvalidoException(LEITURA_MOVIMENTO_JA_FOI_PROCESSADA);
				}

				if (leitura.getCodigoAnormalidadeLeitura() != null) {
					leituraMovimento.setCodigoAnormalidadeLeitura(Long.valueOf(leitura.getCodigoAnormalidadeLeitura()));
				} else {
					leituraMovimento.setCodigoAnormalidadeLeitura(null);
				}

				leituraMovimento.setObservacaoLeitura(leitura.getObservacao());
				leituraMovimento.setTemperaturaInformada(safeParseBigDecimal(leitura.getTemperaturaInformada()));
				leituraMovimento.setPressaoInformada(safeParseBigDecimal(leitura.getPressaoInformada()));
				leituraMovimento.setDataLeitura(leitura.dataLeituraEscrita());
				leituraMovimento.setValorLeitura(safeParseBigDecimal(leitura.getValorLeitura()));
				leituraMovimento.setIndicadorConfirmacaoLeitura(true);
				leituraMovimento.setSituacaoLeitura(
						controladorLeituraMovimento.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.LEITURA_RETORNADA));
				leituraMovimento.setOrigem(OrigemLeituraMovimento.API.name());
				leituraMovimento.setLatitude(safeParseBigDecimal(leitura.getLatitude()));
				leituraMovimento.setLongitude(safeParseBigDecimal(leitura.getLongitude()));

				controladorLeituraMovimento.atualizar(leituraMovimento);

				controladorCronAtvFaturamento.atualizarAtividadeFaturamentoSeExistir(
						leituraMovimento.getGrupoFaturamento().getChavePrimaria(),
						TipoAtividadeSistema.REGISTRAR_LEITURA.getNumeroSequencia(),
						leituraMovimento.getGrupoFaturamento().getAnoMesReferencia());

				statusResposta.add(new RespostaSalvarLeituraDTO(leituraMovimento.getChavePrimaria(), RespostaSalvarLeituraDTO.ATUALIZADA));
			} catch (Exception e) {

				LOG.error("Falha no retorno da leitura", e);

				Long idLeitura = Optional.ofNullable(leitura).map(LeituraColetorDTO::getChavePrimariaLeituraMovimento).orElse(null);

				RespostaSalvarLeituraDTO resposta =
						new RespostaSalvarLeituraDTO(idLeitura, RespostaSalvarLeituraDTO.FALHA, e.getMessage());

				statusResposta.add(resposta);
			}
		}

		Gson gson = new Gson();
		String jsonObj = gson.toJson(statusResposta);
		return new ResponseEntity<>(jsonObj, HttpStatus.OK);
	}

	/**
	 * Monta um objeto ResponseEntity a partir de uma exceção
	 * @param erro A exceção lançada
	 * @return O objeto ResponseEntity
	 */
	private ResponseEntity tratarErro(Exception erro) {
		LOG.error("Erro ao validar medição", erro);
		return new ResponseEntity<>(erro.getMessage(), HttpStatus.BAD_REQUEST);
	}

	/**
	 * Captura as exceções de chamadas para os métodos públicos da API
	 *
	 * @param exception que pode ocorrer caso a chamada seja feita de forma errada
	 */
	@ExceptionHandler @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE) public void handler(Exception exception) {
		LOG.error("Aconteceu um erro ao receber/enviar uma requisição", exception);
	}

	/**
	 * Retorna sempre um ResponseEntity com uma LeituraColetorDTO, para os casos de não encontrar, o status é 400 com o atributo mensagem
	 * com o motivo de não ter encontrado.
	 *
	 * @param idClienteSGM utilizado para buscar a leitura movimento com um ponto de consumo que tenha um código legado igual ao parâmetro
	 *                     informado.
	 * @return sempre uma LeituraColetorDTO, até nos casos que ocorre erro
	 */
	@RequestMapping(value = "/leitura-movimento/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<LeituraColetorDTO> leituraMovimento(@RequestBody Long idClienteSGM) {
		LeituraColetorDTO leituraColetorDTO = new LeituraColetorDTO();
		try {
			ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();
			LeituraMovimento leituraMovimento = controladorLeituraMovimento.consultarLeituraMovimentoPorCodigoLegado(idClienteSGM);

			if (leituraMovimento == null) {
				throw new AtributoInvalidoException("Leitura movimento não encontrada para o id: " + idClienteSGM);
			}

			controladorCronAtvFaturamento.atualizarAtividadeFaturamentoSeExistir(leituraMovimento.getGrupoFaturamento().getChavePrimaria(),
					TipoAtividadeSistema.REALIZAR_LEITURA.getNumeroSequencia(), leituraMovimento.getGrupoFaturamento().getAnoMesReferencia());
			return new ResponseEntity<>(LeituraColetorDTO.converterLeituraMovimento(leituraMovimento), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Erro ao buscar leitura movimento", e);
			leituraColetorDTO.setMensagem(e.getMessage());
			return new ResponseEntity<>(leituraColetorDTO, HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Monta um objeto representando um ano/mes e as rotas ativas para esse período
	 * @return Um objeto representando ano/mes e as rotas ativas para esse período
	 */
	@RequestMapping(value = "/info-situacao/", produces = "application/json; charset=utf-8",
			method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> obterInfoSituacao() {
		Gson gson = new Gson();
		String jsonObj;
		InfoSituacaoDTO infoSituacaoDTO = new InfoSituacaoDTO();
		try {
			ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();
			List<RotaDTO> leituras = (List<RotaDTO>) controladorLeituraMovimento.consultarRotasAtivas();
			infoSituacaoDTO.preencherDados(leituras);

			Gson gsonFormater = new GsonBuilder().setDateFormat("dd/MM/yyyy hh:mm:ss").create();

			jsonObj = gsonFormater.toJson(infoSituacaoDTO);
			return new ResponseEntity<>(jsonObj, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Erro ao buscar info situação", e);
			jsonObj = gson.toJson(infoSituacaoDTO);
			return new ResponseEntity<>(jsonObj , HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Monta uma lista de leituras ativas por rota
	 * @param paramRota A chave primária da rota e anoMesFaturamento
	 * @return A lista com as leituras ativas para essa rota
	 */
	@RequestMapping(value = "/leituras-ativas-por-rota/", produces = "application/json; charset=utf-8",
			method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> consultarLeiturasAtivasPorRota(@RequestBody ParamRota paramRota) {
		Gson gson = new Gson();
		String jsonObj = "";
		try {
			ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();
			List<LeituraMovimentoDTO> leituras = (List<LeituraMovimentoDTO>) controladorLeituraMovimento
					.consultarLeiturasAtivasPorRota(paramRota.getIdRota(), paramRota.getAnoMesFaturamento());
			jsonObj = gson.toJson(leituras);
			ResponseEntity dadosRetorno = new ResponseEntity<>(jsonObj, HttpStatus.OK);
			controladorLeituraMovimento.atualizarLeiturasAtivasPorRota(paramRota.getIdRota(), paramRota.getAnoMesFaturamento());
			return dadosRetorno;
		} catch (Exception e) {
			LOG.error("Erro ao buscar info situação", e);
			return new ResponseEntity<>(jsonObj , HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Valida os dados da leitura vinda do coletor
	 * @param leitura A leitura registrada
	 * @throws AtributoInvalidoException caso haja alguma inconsistencia nos dados
	 */
	private void validarLeituraColetor(LeituraColetorDTO leitura) throws AtributoInvalidoException {

		if (leitura == null) {
			throw new AtributoInvalidoException("Leitura inválida");
		}

		validarChaveLeituraMovimento(leitura);
		validarAnoMesFaturamento(leitura);
		validarCiclo(leitura);
		validarPontoConsumo(leitura);
		validarValorLeitura(leitura, true);
		validarTemperatura(leitura);
		validarPressao(leitura);
		validarDataLeitura(leitura);
	}

	/**
	 * Valida os dados da leitura vinda do coletor SGM
	 * @param leitura A leitura registrada
	 * @throws AtributoInvalidoException caso haja alguma inconsistencia nos dados
	 */
	private void validarLeituraColetorSGM(LeituraColetorDTO leitura) throws AtributoInvalidoException {

		if (leitura == null) {
			throw new AtributoInvalidoException("Leitura inválida");
		}

		validarChaveLeituraMovimento(leitura);

		boolean aceitaLeitura = true;

		Optional<AnormalidadeLeitura> anormalidade = validarAnormalidadeLeitura(leitura);

		if (anormalidade.isPresent()) {
			Boolean anormalidadeAceitaLeitura = anormalidade.get().getAceitaLeitura();
			if (anormalidadeAceitaLeitura != null) {
				aceitaLeitura = anormalidadeAceitaLeitura;
			} else {
				aceitaLeitura = Boolean.FALSE;
			}
		}

		validarValorLeitura(leitura, aceitaLeitura);

		validarTemperatura(leitura);
		validarPressao(leitura);
		validarDataLeitura(leitura);
	}

	/**
	 * Verifica se a anormalidade de leitura informada existe
	 * @param leitura A leitura registrada
	 * @throws AtributoInvalidoException Caso a anormalidade não exista
	 * @throws NegocioException O NegocioException
	 */
	private Optional<AnormalidadeLeitura> validarAnormalidadeLeitura(LeituraColetorDTO leitura) throws AtributoInvalidoException {

		AnormalidadeLeitura anormalidadeLeitura = null;

		if (leitura.getCodigoAnormalidadeLeitura() != null) {

			try {
				ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();
				anormalidadeLeitura = controladorAnormalidade.obterAnormalidadeLeitura(leitura.getCodigoAnormalidadeLeitura().longValue());
			} catch (NegocioException e) {
				LOG.error(e.getMessage());
				throw new AtributoInvalidoException(MedicaoRest.ANORMALIDADE_LEITURA_NAO_ENCONTRADA);
			}

			if (anormalidadeLeitura == null) {
				throw new AtributoInvalidoException(MedicaoRest.ANORMALIDADE_LEITURA_NAO_ENCONTRADA);
			}
		}

		return Optional.ofNullable(anormalidadeLeitura);
	}

	private void validarDataLeitura(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getDataLeitura() == null) {
			throw new AtributoInvalidoException(MedicaoRest.DATA_LEITURA_OBRIGATORIO);
		}

		verificarDataOk(leitura.getDataLeitura());
	}

	private void validarPressao(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getPressaoInformada() == null) {
			throw new AtributoInvalidoException(MedicaoRest.PRESSAO_OBRIGATORIO);
		}
	}

	private void validarTemperatura(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getTemperaturaInformada() == null) {
			throw new AtributoInvalidoException(MedicaoRest.TEMPERATURA_OBRIGATORIO);
		}
	}

	private void validarValorLeitura(LeituraColetorDTO leitura, boolean aceitaLeitura) throws AtributoInvalidoException {

		if (aceitaLeitura && leitura.getValorLeitura() == null) {
			throw new AtributoInvalidoException(MedicaoRest.VALOR_LEITURA_OBRIGATORIO);
		}
	}

	private void validarPontoConsumo(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getPontoConsumo() == null || leitura.getPontoConsumo().equals(0L)) {
			throw new AtributoInvalidoException(MedicaoRest.PONTO_CONSUMO_OBRIGATORIO);
		}
	}

	private void validarCiclo(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getCiclo() == null || leitura.getCiclo() == 0) {
			throw new AtributoInvalidoException(MedicaoRest.CICLO_OBRIGATORIO);
		}
	}

	private void validarAnoMesFaturamento(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getAnoMesFaturamento() == null) {
			throw new AtributoInvalidoException(MedicaoRest.ANO_MES_FATURAMENTO_OBRIGATORIO);
		}
	}

	private void validarChaveLeituraMovimento(LeituraColetorDTO leitura) throws AtributoInvalidoException {
		if (leitura.getChavePrimariaLeituraMovimento() == null || leitura.getChavePrimariaLeituraMovimento() <= 0) {
			throw new AtributoInvalidoException(MedicaoRest.CHAVE_LEITURA_MOVIMENTO_OBRIGATORIO);
		}
	}

	private void verificarDataOk(String data) throws AtributoInvalidoException {

		try {
			new SimpleDateFormat(LeituraColetorDTO.FORMATO_DATA).parse(data);
		} catch (ParseException e) {
			throw new AtributoInvalidoException("Data inválida, informe uma data no seguinte formato " + LeituraColetorDTO.FORMATO_DATA);
		}
	}

	private BigDecimal safeParseBigDecimal(Double number) {
		BigDecimal valor = null;

		if (number != null) {
			valor = BigDecimal.valueOf(number);
		}

		return valor;
	}
}

class ParamRota {

	private Long idRota;
	private Integer anoMesFaturamento;

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public Integer getAnoMesFaturamento() {
		return anoMesFaturamento;
	}

	public void setAnoMesFaturamento(Integer anoMesFaturamento) {
		this.anoMesFaturamento = anoMesFaturamento;
	}
}
