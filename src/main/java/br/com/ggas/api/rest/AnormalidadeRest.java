/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.rest;

import br.com.ggas.api.dto.AnormalidadeLeituraDTO;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Controlador de medições
 */
@Controller @RequestMapping(value = "api/v1/anormalidade")
public class AnormalidadeRest {

	private static final Logger LOG = Logger.getLogger(AnormalidadeRest.class);

	@Autowired
	@Qualifier(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE)
	protected ControladorAnormalidade controladorAnormalidade;

	/**
	 * Captura as exceções de chamadas para os métodos públicos da API
	 *
	 * @param exception que pode ocorrer caso a chamada seja feita de forma errada
	 */
	@ExceptionHandler @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE) public void handler(Exception exception) {
		LOG.error("Aconteceu um erro ao receber/enviar uma requisição", exception);
	}

	/**
	 * Lista todas as anormalidades de leitura ativas
	 * @return O Json representando a lista de anormalidades de leitura
	 */
	@RequestMapping(value = "/leitura/listar", produces = "application/json; charset=utf-8",
			method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> listarAnormalidadesLeitura() {
		Gson gson = new Gson();
		String jsonObj = "";
		try {
			Collection<AnormalidadeLeitura> anormalidades = controladorAnormalidade.listarAnormalidadesLeitura();
			List<AnormalidadeLeituraDTO> anormalidadeDTO = new ArrayList<>();
			anormalidades.forEach(item -> anormalidadeDTO.add(AnormalidadeLeituraDTO.valueOf(item)));
			jsonObj = gson.toJson(anormalidadeDTO);
			return new ResponseEntity<>(jsonObj, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Erro ao buscar anormalidades", e);
			return new ResponseEntity<>(jsonObj , HttpStatus.BAD_REQUEST);
		}
	}

}

