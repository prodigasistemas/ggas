/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.api.rest;

import br.com.ggas.api.autenticador.Autenticador;
import br.com.ggas.api.autenticador.TokenAutenticador;
import br.com.ggas.api.dto.UsuarioDTO;
import br.com.ggas.api.excecoes.AtributoInvalidoException;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.util.ServiceLocator;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controlador para permitir acesso a API Rest
 */
@Controller @RequestMapping(value = "api/v1/login")
public class LoginRest {

	private static final Logger LOG = Logger.getLogger(LoginRest.class);

	/**
	 * Método para autenticar requisições para envio de medições
	 *
	 * @param usuario deve ser preenchida na requisição (campos de login e senha)
	 * @return o token se tiver sido autenticado com o status 200 ou o texto que não foi autenticado com o status 401
	 */
	@RequestMapping(value = "/", produces = "application/json; charset=utf-8", method = {
			RequestMethod.POST }, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> autenticar(@RequestBody UsuarioDTO usuario) {
		try {
			ControladorUsuario controladorUsuario = (ControladorUsuario) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);

			Usuario usuarioLogado = controladorUsuario.autenticarUsuarioProjetado(usuario.getLogin(), usuario.getSenha());

			if (usuarioLogado != null) {

				TokenAutenticador tokenAutenticador = Autenticador.getInstance().buildToken();
				usuarioLogado.setToken(tokenAutenticador.getToken());
				controladorUsuario.atualizarToken(usuarioLogado.getChavePrimaria(), usuarioLogado.getToken());
				Autenticador.getInstance().salvarToken(usuarioLogado.getChavePrimaria(), usuarioLogado.getToken());
				return new ResponseEntity<>(usuarioLogado.getToken(), HttpStatus.OK);
			}
			return usuarioNaoAutenticado();
		} catch (Exception e) {
			LOG.error("Erro ao realizar login", e);
			return usuarioNaoAutenticado();
		}
	}

	private ResponseEntity<String> usuarioNaoAutenticado() {
		return new ResponseEntity<>("Usuário não autenticado", HttpStatus.UNAUTHORIZED);
	}

	/**
	 * Método para realizar logoff e remover acesso concebidos pelo login
	 *
	 * @param token passado como parâmetro para ser removido da sessão
	 * @return uma instância de ResponseEntity com o resultado da operação
	 */
	@RequestMapping(value = "/logoff/", produces = "application/json; charset=utf-8", method = {
			RequestMethod.POST }, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> logoff(@RequestBody String token) {
		try {
			ControladorUsuario controladorUsuario = (ControladorUsuario) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);
			Usuario usuario = controladorUsuario.invalidarToken(token);
			if (usuario != null) {
				Autenticador.getInstance().logout(usuario.getChavePrimaria(), token);
				return new ResponseEntity<>("Logout realizado", HttpStatus.OK);
			}
			throw new AtributoInvalidoException("Usuário nulo para o token " + token);

		} catch (Exception e) {
			LOG.error("Erro ao realizar logoff", e);
			return new ResponseEntity<>("Logout não realizado", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Captura as exceções de chamadas para os métodos públicos da API
	 *
	 * @param exception que pode ocorrer caso a chamada seja feita de forma errada
	 */
	@ExceptionHandler
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	public void handler(Exception exception) {
		LOG.error("Aconteceu um erro ao receber/enviar uma requisição", exception);
	}
}
