/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * @author gmatos
 */
class PenalidadeImpl extends EntidadeNegocioImpl implements Penalidade {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
     * 
     */
	private static final long serialVersionUID = -6186564237899557068L;

	private String descricao;

	private Rubrica rubrica;

	private Rubrica rubricaDesconto;

	private Rubrica rubricaRecuperacao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl.Penalidade#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl
	 * .Penalidade#setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade#getRubrica()
	 */
	@Override
	public Rubrica getRubrica() {

		return rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade
	 * #setRubrica(br.com.ggas.faturamento
	 * .rubrica.Rubrica)
	 */
	@Override
	public void setRubrica(Rubrica rubrica) {

		this.rubrica = rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade#getRubricaDesconto()
	 */
	@Override
	public Rubrica getRubricaDesconto() {

		return rubricaDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade
	 * #setRubricaDesconto(br.com.ggas.faturamento
	 * .rubrica.Rubrica)
	 */
	@Override
	public void setRubricaDesconto(Rubrica rubricaDesconto) {

		this.rubricaDesconto = rubricaDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade#getRubricaRecuperacao()
	 */
	@Override
	public Rubrica getRubricaRecuperacao() {

		return rubricaRecuperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade
	 * #setRubricaRecuperacao(br.com.ggas
	 * .faturamento.rubrica.Rubrica)
	 */
	@Override
	public void setRubricaRecuperacao(Rubrica rubricaRecuperacao) {

		this.rubricaRecuperacao = rubricaRecuperacao;
	}
}
