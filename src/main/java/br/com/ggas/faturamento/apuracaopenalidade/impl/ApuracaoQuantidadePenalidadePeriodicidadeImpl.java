/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class ApuracaoQuantidadePenalidadePeriodicidadeImpl extends EntidadeNegocioImpl implements ApuracaoQuantidadePenalidadePeriodicidade {

	private static final int POSICAO_4 = 4;
	private static final int POSICAO_6 = 6;
	
	private static final long serialVersionUID = 1558421583539988143L;

	private Integer periodoApuracao;

	private BigDecimal qtdaDiariaContratada;

	private BigDecimal qtdaDiariaContratadaMedia;

	private BigDecimal qtdaDiariaProgramada;

	private BigDecimal qtdaDiariaProgramadaMedia;

	private BigDecimal qtdaDiariaRetiradaMaior;

	private BigDecimal qtdaLimite;

	private BigDecimal qtdaNaoRetirada;

	private BigDecimal qtdaPagaNaoRetirada;

	private BigDecimal qtdaRecuperada;

	private BigDecimal valorCalculado;

	private BigDecimal valorCalculadoPercenCobranca;

	private BigDecimal valorTarifaMedia;

	private BigDecimal percentualDescontoAplicado;

	private BigDecimal valorDescontoAplicado;

	private BigDecimal valorAcrescido;

	private BigDecimal valorCobrado;

	private BigDecimal qtdaNaoRetiradaPorParadaProgramada;

	private BigDecimal qtdaNaoRetiradaPorCasoFortuito;

	private BigDecimal qtdaNaoRetiradaPorFalhaFornecimento;

	private BigDecimal qtdaNaoRetiradaPorParadaNaoProgramada;

	private String observacoes;

	private ContratoModalidade contratoModalidade;

	private Penalidade penalidade;

	private PontoConsumo pontoConsumo;

	private EntidadeConteudo periodicidadePenalidade;

	private BigDecimal qtdaDiariaRetirada;

	private Contrato contrato;

	private BigDecimal valorVariacaoCambial;

	private Date dataInicioApuracao;

	private Date dataFimApuracao;

	private BigDecimal qtdaDiariaDisponibilizada;

	private Contrato contratoAtual;

	private Fatura fatura;

	private Collection<Recuperacao> listaRecuperacao;

	@Override
	public Collection<Recuperacao> getListaRecuperacao() {

		return listaRecuperacao;
	}

	@Override
	public void setListaRecuperacao(Collection<Recuperacao> listaRecuperacao) {

		this.listaRecuperacao = listaRecuperacao;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaDiariaContratadaMedia()
	 */
	@Override
	public BigDecimal getQtdaDiariaContratadaMedia() {

		return qtdaDiariaContratadaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaDiariaContratadaMedia
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaContratadaMedia(BigDecimal qtdaDiariaContratadaMedia) {

		this.qtdaDiariaContratadaMedia = qtdaDiariaContratadaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaDiariaProgramadaMedia()
	 */
	@Override
	public BigDecimal getQtdaDiariaProgramadaMedia() {

		return qtdaDiariaProgramadaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaDiariaProgramadaMedia
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaProgramadaMedia(BigDecimal qtdaDiariaProgramadaMedia) {

		this.qtdaDiariaProgramadaMedia = qtdaDiariaProgramadaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaDiariaRetiradaMaior()
	 */
	@Override
	public BigDecimal getQtdaDiariaRetiradaMaior() {

		return qtdaDiariaRetiradaMaior;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaDiariaRetiradaMaior
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaRetiradaMaior(BigDecimal qtdaDiariaRetiradaMaior) {

		this.qtdaDiariaRetiradaMaior = qtdaDiariaRetiradaMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getPeriodoApuracao ()
	 */
	@Override
	public Integer getPeriodoApuracao() {

		return periodoApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setPeriodoApuracao (java.lang.Integer)
	 */
	@Override
	public void setPeriodoApuracao(Integer periodoApuracao) {

		this.periodoApuracao = periodoApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaDiariaContratada ()
	 */
	@Override
	public BigDecimal getQtdaDiariaContratada() {

		return qtdaDiariaContratada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaDiariaContratada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaContratada(BigDecimal qtdaDiariaContratada) {

		this.qtdaDiariaContratada = qtdaDiariaContratada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaDiariaProgramada ()
	 */
	@Override
	public BigDecimal getQtdaDiariaProgramada() {

		return qtdaDiariaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaDiariaProgramada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaProgramada(BigDecimal qtdaDiariaProgramada) {

		this.qtdaDiariaProgramada = qtdaDiariaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaLimite()
	 */
	@Override
	public BigDecimal getQtdaLimite() {

		return qtdaLimite;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaLimite(java .math.BigDecimal)
	 */
	@Override
	public void setQtdaLimite(BigDecimal qtdaLimite) {

		this.qtdaLimite = qtdaLimite;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaNaoRetirada ()
	 */
	@Override
	public BigDecimal getQtdaNaoRetirada() {

		return qtdaNaoRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaNaoRetirada (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetirada(BigDecimal qtdaNaoRetirada) {

		this.qtdaNaoRetirada = qtdaNaoRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaPagaNaoRetirada ()
	 */
	@Override
	public BigDecimal getQtdaPagaNaoRetirada() {

		return qtdaPagaNaoRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaPagaNaoRetirada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaPagaNaoRetirada(BigDecimal qtdaPagaNaoRetirada) {

		this.qtdaPagaNaoRetirada = qtdaPagaNaoRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaRecuperada()
	 */
	@Override
	public BigDecimal getQtdaRecuperada() {

		return qtdaRecuperada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaRecuperada (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaRecuperada(BigDecimal qtdaRecuperada) {

		this.qtdaRecuperada = qtdaRecuperada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getValorCalculado()
	 */
	@Override
	public BigDecimal getValorCalculado() {

		return valorCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setValorCalculado (java.math.BigDecimal)
	 */
	@Override
	public void setValorCalculado(BigDecimal valorCalculado) {

		this.valorCalculado = valorCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getValorTarifaMedia ()
	 */
	@Override
	public BigDecimal getValorTarifaMedia() {

		return valorTarifaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setValorTarifaMedia (java.math.BigDecimal)
	 */
	@Override
	public void setValorTarifaMedia(BigDecimal valorTarifaMedia) {

		this.valorTarifaMedia = valorTarifaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getPercentualDescontoAplicado()
	 */
	@Override
	public BigDecimal getPercentualDescontoAplicado() {

		return percentualDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setPercentualDescontoAplicado
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualDescontoAplicado(BigDecimal percentualDescontoAplicado) {

		this.percentualDescontoAplicado = percentualDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getValorDescontoAplicado()
	 */
	@Override
	public BigDecimal getValorDescontoAplicado() {

		return valorDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setValorDescontoAplicado
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorDescontoAplicado(BigDecimal valorDescontoAplicado) {

		this.valorDescontoAplicado = valorDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getValorAcrescido()
	 */
	@Override
	public BigDecimal getValorAcrescido() {

		return valorAcrescido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setValorAcrescido (java.math.BigDecimal)
	 */
	@Override
	public void setValorAcrescido(BigDecimal valorAcrescido) {

		this.valorAcrescido = valorAcrescido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getValorCobrado()
	 */
	@Override
	public BigDecimal getValorCobrado() {

		return valorCobrado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setValorCobrado (java.math.BigDecimal)
	 */
	@Override
	public void setValorCobrado(BigDecimal valorCobrado) {

		this.valorCobrado = valorCobrado;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaNaoRetiradaPorParadaProgramada()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorParadaProgramada() {

		return qtdaNaoRetiradaPorParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaNaoRetiradaPorParadaProgramada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorParadaProgramada(BigDecimal qtdaNaoRetiradaPorParadaProgramada) {

		this.qtdaNaoRetiradaPorParadaProgramada = qtdaNaoRetiradaPorParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaNaoRetiradaPorCasoFortuito()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorCasoFortuito() {

		return qtdaNaoRetiradaPorCasoFortuito;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaNaoRetiradaPorCasoFortuito
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorCasoFortuito(BigDecimal qtdaNaoRetiradaPorCasoFortuito) {

		this.qtdaNaoRetiradaPorCasoFortuito = qtdaNaoRetiradaPorCasoFortuito;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaNaoRetiradaPorFalhaFornecimento()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorFalhaFornecimento() {

		return qtdaNaoRetiradaPorFalhaFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaNaoRetiradaPorFalhaFornecimento
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorFalhaFornecimento(BigDecimal qtdaNaoRetiradaPorFalhaFornecimento) {

		this.qtdaNaoRetiradaPorFalhaFornecimento = qtdaNaoRetiradaPorFalhaFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getQtdaNaoRetiradaPorParadaNaoProgramada()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorParadaNaoProgramada() {

		return qtdaNaoRetiradaPorParadaNaoProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setQtdaNaoRetiradaPorParadaNaoProgramada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorParadaNaoProgramada(BigDecimal qtdaNaoRetiradaPorParadaNaoProgramada) {

		this.qtdaNaoRetiradaPorParadaNaoProgramada = qtdaNaoRetiradaPorParadaNaoProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getObservacoes()
	 */
	@Override
	public String getObservacoes() {

		return observacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setObservacoes( java.lang.String)
	 */
	@Override
	public void setObservacoes(String observacoes) {

		this.observacoes = observacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getContratoModalidade ()
	 */
	@Override
	public ContratoModalidade getContratoModalidade() {

		return contratoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setContratoModalidade
	 * (br.com.ggas.contrato
	 * .contrato.ContratoModalidade)
	 */
	@Override
	public void setContratoModalidade(ContratoModalidade contratoModalidade) {

		this.contratoModalidade = contratoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getPenalidade()
	 */
	@Override
	public Penalidade getPenalidade() {

		return penalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setPenalidade(br
	 * .com.procenge.ggas.faturamento
	 * .apuracaopenalidade.Penalidade)
	 */
	@Override
	public void setPenalidade(Penalidade penalidade) {

		this.penalidade = penalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getPeriodicidadePenalidade()
	 */
	@Override
	public EntidadeConteudo getPeriodicidadePenalidade() {

		return periodicidadePenalidade;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * setPeriodicidadePenalidade
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setPeriodicidadePenalidade(EntidadeConteudo periodicidadePenalidade) {

		this.periodicidadePenalidade = periodicidadePenalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaDiariaRetirada ()
	 */
	@Override
	public BigDecimal getQtdaDiariaRetirada() {

		return qtdaDiariaRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaDiariaRetirada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaRetirada(BigDecimal qtdaDiariaRetirada) {

		this.qtdaDiariaRetirada = qtdaDiariaRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setContrato(br.
	 * com.procenge.ggas.contrato.
	 * contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getValorVariacaoCambial ()
	 */
	@Override
	public BigDecimal getValorVariacaoCambial() {

		return valorVariacaoCambial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setValorVariacaoCambial
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorVariacaoCambial(BigDecimal valorVariacaoCambial) {

		this.valorVariacaoCambial = valorVariacaoCambial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getDataInicioApuracao ()
	 */
	@Override
	public Date getDataInicioApuracao() {

		return dataInicioApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setDataInicioApuracao (java.util.Date)
	 */
	@Override
	public void setDataInicioApuracao(Date dataInicioApuracao) {

		this.dataInicioApuracao = dataInicioApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getDataFimApuracao ()
	 */
	@Override
	public Date getDataFimApuracao() {

		return dataFimApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setDataFimApuracao (java.util.Date)
	 */
	@Override
	public void setDataFimApuracao(Date dataFimApuracao) {

		this.dataFimApuracao = dataFimApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getQtdaDiariaDisponibilizada()
	 */
	@Override
	public BigDecimal getQtdaDiariaDisponibilizada() {

		return qtdaDiariaDisponibilizada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setQtdaDiariaDisponibilizada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaDisponibilizada(BigDecimal qtdaDiariaDisponibilizada) {

		this.qtdaDiariaDisponibilizada = qtdaDiariaDisponibilizada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getContratoAtual()
	 */
	@Override
	public Contrato getContratoAtual() {

		return contratoAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setContratoAtual
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void setContratoAtual(Contrato contratoAtual) {

		this.contratoAtual = contratoAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade
	 * #setFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	@Override
	public BigDecimal getValorCalculadoPercenCobranca() {

		return valorCalculadoPercenCobranca;
	}

	@Override
	public void setValorCalculadoPercenCobranca(BigDecimal valorCalculadoPercenCobranca) {

		this.valorCalculadoPercenCobranca = valorCalculadoPercenCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidadePeriodicidade#
	 * getPeriodoApuracaoFormatado()
	 */
	@Override
	public String getPeriodoApuracaoFormatado() {

		if (periodoApuracao != null) {

			String periodo = String.valueOf(periodoApuracao);

			if (periodo.length() == POSICAO_6) {
				return periodo.substring(POSICAO_4, POSICAO_6) + "/" + periodo.substring(0, POSICAO_4);
			} else {
				return periodo;
			}

		} else {
			return "";
		}
	}

}
