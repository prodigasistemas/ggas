/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ApuracaoQuantidadeVO
 * 
 * @author jmauricio
 * 
 * Classe responsável por apuração quantidade VO.
 * 
 */
public class ApuracaoQuantidadeVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long chaveContrato;

	private String numeroContrato;

	private Long chavePontoConsumo;

	private String descricaoPontoConsumo;

	private Long chavePenalidade;

	private String descricaoPenalidade;

	private BigDecimal valorCobrado;

	private Integer periodoApuracao;

	private BigDecimal valorCalculado;

	private BigDecimal valorCalculadoPercentual;

	private String nomeClienteAssinatura;

	private String nomeImovel;

	private String periodoApuracaoFormatado;

	private Long chaveModalidade;

	private String descricaoModalidade;

	public Long getChaveContrato() {

		return chaveContrato;
	}

	public void setChaveContrato(Long chaveContrato) {

		this.chaveContrato = chaveContrato;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public Long getChavePontoConsumo() {

		return chavePontoConsumo;
	}

	public void setChavePontoConsumo(Long chavePontoConsumo) {

		this.chavePontoConsumo = chavePontoConsumo;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public Long getChavePenalidade() {

		return chavePenalidade;
	}

	public void setChavePenalidade(Long chavePenalidade) {

		this.chavePenalidade = chavePenalidade;
	}

	public String getDescricaoPenalidade() {

		return descricaoPenalidade;
	}

	public void setDescricaoPenalidade(String descricaoPenalidade) {

		this.descricaoPenalidade = descricaoPenalidade;
	}

	public BigDecimal getValorCobrado() {

		return valorCobrado;
	}

	public void setValorCobrado(BigDecimal valorCobrado) {

		this.valorCobrado = valorCobrado;
	}

	public Integer getPeriodoApuracao() {

		return periodoApuracao;
	}

	public void setPeriodoApuracao(Integer periodoApuracao) {

		this.periodoApuracao = periodoApuracao;
	}

	public BigDecimal getValorCalculado() {

		return valorCalculado;
	}

	public void setValorCalculado(BigDecimal valorCalculado) {

		this.valorCalculado = valorCalculado;
	}

	public String getNomeClienteAssinatura() {

		return nomeClienteAssinatura;
	}

	public void setNomeClienteAssinatura(String nomeClienteAssinatura) {

		this.nomeClienteAssinatura = nomeClienteAssinatura;
	}

	public String getNomeImovel() {

		return nomeImovel;
	}

	public void setNomeImovel(String nomeImovel) {

		this.nomeImovel = nomeImovel;
	}

	public String getPeriodoApuracaoFormatado() {

		return periodoApuracaoFormatado;
	}

	public void setPeriodoApuracaoFormatado(String periodoApuracaoFormatado) {

		this.periodoApuracaoFormatado = periodoApuracaoFormatado;
	}

	public Long getChaveModalidade() {

		return chaveModalidade;
	}

	public void setChaveModalidade(Long chaveModalidade) {

		this.chaveModalidade = chaveModalidade;
	}

	public String getDescricaoModalidade() {

		return descricaoModalidade;
	}

	public void setDescricaoModalidade(String descricaoModalidade) {

		this.descricaoModalidade = descricaoModalidade;
	}

	public BigDecimal getValorCalculadoPercentual() {

		return valorCalculadoPercentual;
	}

	public void setValorCalculadoPercentual(BigDecimal valorCalculadoPercentual) {

		this.valorCalculadoPercentual = valorCalculadoPercentual;
	}

}
