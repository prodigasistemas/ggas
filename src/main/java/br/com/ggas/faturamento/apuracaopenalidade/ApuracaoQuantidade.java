/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface Responsável pela assinatura dos métodos relacionados a Quantidade Apurada.  
 *
 */
public interface ApuracaoQuantidade extends EntidadeNegocio {

	/**
	 * BEAN_ID_APURACAO_QUANTIDADE
	 */
	String BEAN_ID_APURACAO_QUANTIDADE = "apuracaoQuantidade";

	/**
	 * @return contrato
	 */
	Contrato getContrato();

	/**
	 * @param contrato - Set contrato.
	 */
	void setContrato(Contrato contrato);

	/**
	 * @return
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return
	 */
	Date getDataApuracaoVolumes();

	/**
	 * @param dataApuracaoVolumes
	 */
	void setDataApuracaoVolumes(Date dataApuracaoVolumes);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaDisponibilizada();

	/**
	 * @param qtdaDiariaDisponibilizada
	 */
	void setQtdaDiariaDisponibilizada(BigDecimal qtdaDiariaDisponibilizada);

	/**
	 * @return
	 */
	BigDecimal getQtdaNaoRetiradaPorParadaProgramada();

	/**
	 * @param qtdaNaoRetiradaPorParadaProgramada
	 */
	void setQtdaNaoRetiradaPorParadaProgramada(BigDecimal qtdaNaoRetiradaPorParadaProgramada);

	/**
	 * @return
	 */
	BigDecimal getQtdaNaoRetiradaPorCasoFortuito();

	/**
	 * @param qtdaNaoRetiradaPorCasoFortuito
	 */
	void setQtdaNaoRetiradaPorCasoFortuito(BigDecimal qtdaNaoRetiradaPorCasoFortuito);

	/**
	 * @return
	 */
	BigDecimal getQtdaNaoRetiradaPorFalhaFornecimento();

	/**
	 * @param qtdaNaoRetiradaPorFalhaFornecimento
	 */
	void setQtdaNaoRetiradaPorFalhaFornecimento(BigDecimal qtdaNaoRetiradaPorFalhaFornecimento);

	/**
	 * @return
	 */
	BigDecimal getQtdaNaoRetiradaPorParadaNaoProgramada();

	/**
	 * @param qtdaNaoRetiradaPorParadaNaoProgramada
	 */
	void setQtdaNaoRetiradaPorParadaNaoProgramada(BigDecimal qtdaNaoRetiradaPorParadaNaoProgramada);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaRetirada();

	/**
	 * @param qtdaDiariaRetirada
	 */
	void setQtdaDiariaRetirada(BigDecimal qtdaDiariaRetirada);

	/**
	 * @return
	 */
	BigDecimal getQtdaFaltante();

	/**
	 * @param qtdaFaltante
	 */
	void setQtdaFaltante(BigDecimal qtdaFaltante);

	/**
	 * @return
	 */
	BigDecimal getQtdaRetiradaMaior();

	/**
	 * @param qtdaRetiradaMaior
	 */
	void setQtdaRetiradaMaior(BigDecimal qtdaRetiradaMaior);

	/**
	 * @return
	 */
	BigDecimal getQtdaContratada();

	/**
	 * @param qtdaContratada
	 */
	void setQtdaContratada(BigDecimal qtdaContratada);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaProgramada();

	/**
	 * @param qtdaDiariaProgramada
	 */
	void setQtdaDiariaProgramada(BigDecimal qtdaDiariaProgramada);
}
