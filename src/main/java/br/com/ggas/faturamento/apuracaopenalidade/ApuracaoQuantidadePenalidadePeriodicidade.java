/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a Penalidade aplicada
 * de acordo com a Periodicidade e Quantidade Apurada. 
 *
 */
public interface ApuracaoQuantidadePenalidadePeriodicidade extends EntidadeNegocio {

	/**
	 * BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE_PERIODICIDADE
	 */
	String BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE_PERIODICIDADE = "apuracaoQuantidadePenalidadePeriodicidade";

	/**
	 * @return
	 */
	Integer getPeriodoApuracao();

	/**
	 * @param periodoApuracao
	 */
	void setPeriodoApuracao(Integer periodoApuracao);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaContratada();

	/**
	 * @param qtdaDiariaContratada
	 */
	void setQtdaDiariaContratada(BigDecimal qtdaDiariaContratada);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaContratadaMedia();

	/**
	 * @param qtdaDiariaContratadaMedia - Set quantidade diária média contratada.
	 */
	void setQtdaDiariaContratadaMedia(BigDecimal qtdaDiariaContratadaMedia);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaRetiradaMaior();

	/**
	 * @param qtdaDiariaRetiradaMaior - Set quantidade diária retirada maior.
	 */
	void setQtdaDiariaRetiradaMaior(BigDecimal qtdaDiariaRetiradaMaior);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaProgramadaMedia();

	/**
	 * @param qtdaDiariaProgramadaMedia - Set Quantidade diária programada média.
	 */
	void setQtdaDiariaProgramadaMedia(BigDecimal qtdaDiariaProgramadaMedia);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaProgramada();

	/**
	 * @param qtdaDiariaProgramada
	 */
	void setQtdaDiariaProgramada(BigDecimal qtdaDiariaProgramada);

	/**
	 * @return
	 */
	BigDecimal getQtdaLimite();

	/**
	 * @param qtdaLimite
	 */
	void setQtdaLimite(BigDecimal qtdaLimite);

	/**
	 * @return
	 */
	BigDecimal getQtdaNaoRetirada();

	/**
	 * @param qtdaNaoRetirada
	 */
	void setQtdaNaoRetirada(BigDecimal qtdaNaoRetirada);

	/**
	 * @return
	 */
	BigDecimal getQtdaPagaNaoRetirada();

	/**
	 * @param qtdaPagaNaoRetirada
	 */
	void setQtdaPagaNaoRetirada(BigDecimal qtdaPagaNaoRetirada);

	/**
	 * @return
	 */
	BigDecimal getQtdaRecuperada();

	/**
	 * @param qtdaRecuperada
	 */
	void setQtdaRecuperada(BigDecimal qtdaRecuperada);

	/**
	 * @return
	 */
	BigDecimal getValorCalculado();

	/**
	 * @param valorCalculado
	 */
	void setValorCalculado(BigDecimal valorCalculado);

	/**
	 * @return
	 */
	BigDecimal getValorTarifaMedia();

	/**
	 * @param valorTarifaMedia
	 */
	void setValorTarifaMedia(BigDecimal valorTarifaMedia);

	/**
	 * @return
	 */
	BigDecimal getPercentualDescontoAplicado();

	/**
	 * @param percentualDescontoAplicado
	 */
	void setPercentualDescontoAplicado(BigDecimal percentualDescontoAplicado);

	/**
	 * @return
	 */
	BigDecimal getValorDescontoAplicado();

	/**
	 * @param valorDescontoAplicado
	 */
	void setValorDescontoAplicado(BigDecimal valorDescontoAplicado);

	/**
	 * @return
	 */
	BigDecimal getValorAcrescido();

	/**
	 * @param valorAcrescido
	 */
	void setValorAcrescido(BigDecimal valorAcrescido);

	/**
	 * @return
	 */
	BigDecimal getValorCobrado();

	/**
	 * @param valorCobrado
	 */
	void setValorCobrado(BigDecimal valorCobrado);

	/**
	 * @return
	 */
	public BigDecimal getQtdaNaoRetiradaPorParadaProgramada();

	/**
	 * @param qtdaNaoRetiradaPorParadaProgramada
	 */
	public void setQtdaNaoRetiradaPorParadaProgramada(BigDecimal qtdaNaoRetiradaPorParadaProgramada);

	/**
	 * @return
	 */
	public BigDecimal getQtdaNaoRetiradaPorCasoFortuito();

	/**
	 * @param qtdaNaoRetiradaPorCasoFortuito
	 */
	public void setQtdaNaoRetiradaPorCasoFortuito(BigDecimal qtdaNaoRetiradaPorCasoFortuito);

	/**
	 * @return
	 */
	public BigDecimal getQtdaNaoRetiradaPorFalhaFornecimento();

	/**
	 * @param qtdaNaoRetiradaPorFalhaFornecimento
	 */
	public void setQtdaNaoRetiradaPorFalhaFornecimento(BigDecimal qtdaNaoRetiradaPorFalhaFornecimento);

	/**
	 * @return
	 */
	public BigDecimal getQtdaNaoRetiradaPorParadaNaoProgramada();

	/**
	 * @param qtdaNaoRetiradaPorParadaNaoProgramada
	 */
	public void setQtdaNaoRetiradaPorParadaNaoProgramada(BigDecimal qtdaNaoRetiradaPorParadaNaoProgramada);

	/**
	 * @return
	 */
	String getObservacoes();

	/**
	 * @param observacoes
	 */
	void setObservacoes(String observacoes);

	/**
	 * @return
	 */
	ContratoModalidade getContratoModalidade();

	/**
	 * @param contratoModalidade
	 */
	void setContratoModalidade(ContratoModalidade contratoModalidade);

	/**
	 * @return
	 */
	Penalidade getPenalidade();

	/**
	 * @param penalidade
	 */
	void setPenalidade(Penalidade penalidade);

	/**
	 * @return
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return
	 */
	EntidadeConteudo getPeriodicidadePenalidade();

	/**
	 * @param periodicidadePenalidade
	 */
	void setPeriodicidadePenalidade(EntidadeConteudo periodicidadePenalidade);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaRetirada();

	/**
	 * @param qtdaDiariaRetirada
	 */
	void setQtdaDiariaRetirada(BigDecimal qtdaDiariaRetirada);

	/**
	 * @return
	 */
	Contrato getContrato();

	/**
	 * @param contrato
	 */
	void setContrato(Contrato contrato);

	/**
	 * @return
	 */
	BigDecimal getValorVariacaoCambial();

	/**
	 * @param valorVariacaoCambial
	 */
	void setValorVariacaoCambial(BigDecimal valorVariacaoCambial);

	/**
	 * @return
	 */
	Date getDataInicioApuracao();

	/**
	 * @param dataInicioApuracao
	 */
	void setDataInicioApuracao(Date dataInicioApuracao);

	/**
	 * @return
	 */
	Date getDataFimApuracao();

	/**
	 * @param dataFimApuracao
	 */
	void setDataFimApuracao(Date dataFimApuracao);

	/**
	 * @return
	 */
	String getPeriodoApuracaoFormatado();

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaDisponibilizada();

	/**
	 * @param qtdaDiariaDisponibilizada
	 */
	void setQtdaDiariaDisponibilizada(BigDecimal qtdaDiariaDisponibilizada);

	/**
	 * @return the contratoAtual
	 */
	Contrato getContratoAtual();

	/**
	 * @param contratoAtual
	 *            the contratoAtual to set
	 */
	void setContratoAtual(Contrato contratoAtual);

	/**
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return
	 *         ApuracaoQuantidadePenalidadePeriodicidade
	 */
	Collection<Recuperacao> getListaRecuperacao();

	/**
	 * @param listaRecuperacao
	 */
	void setListaRecuperacao(Collection<Recuperacao> listaRecuperacao);

	/**
	 * @return BigDecimal - Retorna valor valculado percentual cobrança.
	 */
	BigDecimal getValorCalculadoPercenCobranca();

	/**
	 * @param valorCalculadoPercenCobranca - Set valor calculado percentual cobrança.
	 */
	void setValorCalculadoPercenCobranca(BigDecimal valorCalculadoPercenCobranca);

}
