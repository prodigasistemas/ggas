/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidade;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class ApuracaoQuantidadeImpl extends EntidadeNegocioImpl implements ApuracaoQuantidade {

	private static final long serialVersionUID = -7556324781760830791L;

	private Contrato contrato;

	private PontoConsumo pontoConsumo;

	private Date dataApuracaoVolumes;

	private BigDecimal qtdaDiariaDisponibilizada;

	private BigDecimal qtdaNaoRetiradaPorParadaProgramada;

	private BigDecimal qtdaNaoRetiradaPorCasoFortuito;

	private BigDecimal qtdaNaoRetiradaPorFalhaFornecimento;

	private BigDecimal qtdaNaoRetiradaPorParadaNaoProgramada;

	private BigDecimal qtdaDiariaRetirada;

	private BigDecimal qtdaFaltante;

	private BigDecimal qtdaRetiradaMaior;

	private BigDecimal qtdaContratada;

	private BigDecimal qtdaDiariaProgramada;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade#getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setContrato(br.com.ggas.
	 * contrato.contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setPontoConsumo(br.com.ggas
	 * .cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getDataApuracaoVolumes()
	 */
	@Override
	public Date getDataApuracaoVolumes() {

		return dataApuracaoVolumes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setDataApuracaoVolumes(java.util.Date)
	 */
	@Override
	public void setDataApuracaoVolumes(Date dataApuracaoVolumes) {

		this.dataApuracaoVolumes = dataApuracaoVolumes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getQtdaDiariaDisponibilizada()
	 */
	@Override
	public BigDecimal getQtdaDiariaDisponibilizada() {

		return qtdaDiariaDisponibilizada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaDiariaDisponibilizada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaDisponibilizada(BigDecimal qtdaDiariaDisponibilizada) {

		this.qtdaDiariaDisponibilizada = qtdaDiariaDisponibilizada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getQtdaNaoRetiradaPorParadaProgramada()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorParadaProgramada() {

		return qtdaNaoRetiradaPorParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaNaoRetiradaPorParadaProgramada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorParadaProgramada(BigDecimal qtdaNaoRetiradaPorParadaProgramada) {

		this.qtdaNaoRetiradaPorParadaProgramada = qtdaNaoRetiradaPorParadaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getQtdaNaoRetiradaPorCasoFortuito()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorCasoFortuito() {

		return qtdaNaoRetiradaPorCasoFortuito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaNaoRetiradaPorCasoFortuito
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorCasoFortuito(BigDecimal qtdaNaoRetiradaPorCasoFortuito) {

		this.qtdaNaoRetiradaPorCasoFortuito = qtdaNaoRetiradaPorCasoFortuito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getQtdaNaoRetiradaPorFalhaFornecimento()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorFalhaFornecimento() {

		return qtdaNaoRetiradaPorFalhaFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaNaoRetiradaPorFalhaFornecimento
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorFalhaFornecimento(BigDecimal qtdaNaoRetiradaPorFalhaFornecimento) {

		this.qtdaNaoRetiradaPorFalhaFornecimento = qtdaNaoRetiradaPorFalhaFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getQtdaNaoRetiradaPorParadaNaoProgramada()
	 */
	@Override
	public BigDecimal getQtdaNaoRetiradaPorParadaNaoProgramada() {

		return qtdaNaoRetiradaPorParadaNaoProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaNaoRetiradaPorParadaNaoProgramada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaNaoRetiradaPorParadaNaoProgramada(BigDecimal qtdaNaoRetiradaPorParadaNaoProgramada) {

		this.qtdaNaoRetiradaPorParadaNaoProgramada = qtdaNaoRetiradaPorParadaNaoProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade#getQtdaDiariaRetirada()
	 */
	@Override
	public BigDecimal getQtdaDiariaRetirada() {

		return qtdaDiariaRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaDiariaRetirada(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaRetirada(BigDecimal qtdaDiariaRetirada) {

		this.qtdaDiariaRetirada = qtdaDiariaRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade#getQtdaFaltante()
	 */
	@Override
	public BigDecimal getQtdaFaltante() {

		return qtdaFaltante;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaFaltante(java.math.BigDecimal)
	 */
	@Override
	public void setQtdaFaltante(BigDecimal qtdaFaltante) {

		this.qtdaFaltante = qtdaFaltante;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade#getQtdaRetiradaMaior()
	 */
	@Override
	public BigDecimal getQtdaRetiradaMaior() {

		return qtdaRetiradaMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaRetiradaMaior(java.math.BigDecimal)
	 */
	@Override
	public void setQtdaRetiradaMaior(BigDecimal qtdaRetiradaMaior) {

		this.qtdaRetiradaMaior = qtdaRetiradaMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade#getQtdaContratada()
	 */
	@Override
	public BigDecimal getQtdaContratada() {

		return qtdaContratada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaContratada(java.math.BigDecimal)
	 */
	@Override
	public void setQtdaContratada(BigDecimal qtdaContratada) {

		this.qtdaContratada = qtdaContratada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #getQtdaDiariaProgramada()
	 */
	@Override
	public BigDecimal getQtdaDiariaProgramada() {

		return qtdaDiariaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidade
	 * #setQtdaDiariaProgramada(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaProgramada(BigDecimal qtdaDiariaProgramada) {

		this.qtdaDiariaProgramada = qtdaDiariaProgramada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}
}
