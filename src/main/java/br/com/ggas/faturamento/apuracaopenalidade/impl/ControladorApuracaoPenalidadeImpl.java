/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorPenalidadeImpl representa um ControladorPenalidadeImpl no sistema.
 *
 * @since 17/12/2010
 *
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import static br.com.ggas.util.Constantes.FORMATO_DATA_BR;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.PenalidadeDemandaFaixa;
import br.com.ggas.contrato.contrato.impl.QuantidadeVolumesVO;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.ParadaProgramada;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadeVO;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoVO;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.faturamento.apuracaopenalidade.QuantidadeRecuperacaoPenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.exception.ContratoDeliveryOrPayInconsistenteException;
import br.com.ggas.faturamento.exception.ContratoGasForaEspecificacaoInconsistenteException;
import br.com.ggas.faturamento.exception.ContratoRetiradaAMaiorInconsistenteException;
import br.com.ggas.faturamento.exception.ContratoRetiradaAMenorInconsistenteException;
import br.com.ggas.faturamento.exception.PenalidadeTakeOrPayException;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.ApuracaoPeriodo;
import br.com.ggas.medicao.consumo.ConsumoDistribuidoModalidade;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.ModalidadeDataQuantidade;
import br.com.ggas.medicao.consumo.impl.PontoConsumoDataConsumoTesteExcedido;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.programacao.ApuracaoRecuperacaoQuantidadeVO;

class ControladorApuracaoPenalidadeImpl extends ControladorNegocioImpl implements ControladorApuracaoPenalidade {

	private static final long CONSTANTE_NUMERO_TRINTA_TRES = 33L;

	private static final String ROTULO_DATA_APURACACAO = "Data Apuracação";

	private static final String PARAM_NAME_ID_CONTRATO_MODALIDADE = "idContratoModalidade";

	private static final String PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR = "idPenalidadeRetiradaMaiorMenor";

	private static final String PARAM_NAME_DATA_FIM_VIGENCIA = "dataFimVigencia";

	private static final String PARAM_NAME_DATA_INICIO_VIGENCIA = "dataInicioVigencia";

	private static final String PARAM_NAME_ID_CONTRATO = "idContrato";

	private static final String QUANT_LIMITE_MODALIDADE = "quantidadeLimiteModalidade";

	private static final String QUANT_REFERENCIA_MODALIDADE = "quantidadeReferenciaModalidade";

	private static final String QUANT_RETIRADA_MAIOR_MODALIDADE = "quantidadeRetiradaMaiorModalidade";

	private static final String QUANT_RESTANTE = "quantidadeRestante";

	private static final Logger LOG = Logger.getLogger(ControladorApuracaoPenalidadeImpl.class);

	private static final Integer ESCALA_TARIFA = 4;

	private static final String BOOLEAN_VERDADE_1 = "1";

	private static final String CDL = "cdl";

	private static final String SUPRIDORA = "supridora";

	private static final Integer ESCALA_VALOR_QDP_MEDIA = 2;
	private static final Integer ESCALA_QDR_MEDIA = 6;
	private static final Integer ESCALA_QNR_PONTO =2;
	private static final Integer ESCALA_DESVIO = 4;
	private static final Integer ESCALA_QTD_DIARIA_RETIRADA = 4;
	private static final Integer ESCALA_VALOR_CALCULADO = 2;
	private static final Integer ESCALA_QDC_MEDIA = 4;
	private static final Integer ESCALA_DESCONTO = 6;
	private static final Integer NOVA_ESCALA_DESCONTO = 4;
	private static final Integer VALOR_MAXIMO = 9999;
	private static final Integer CHAVE_CONTRATO = 2;
	private static final Integer CHAVE_PRIMARIA = 2;
	private static final Integer NUMERO_CONTRATO = 3;
	private static final Integer CHAVE_PONTO_CONSUMO = 4;
	private static final Integer DESCRICAO_PONTO_CONSUMO = 5;
	private static final Integer CHAVE_PENALIDADE = 6;
	private static final Integer DESCRICAO_PENALIDADE = 7;
	private static final Integer NOME_CLIENTE_ASSINATURA = 8;
	private static final Integer NOME_IMOVEL = 9;
	private static final Integer PERIODO_APURACAO = 10;
	private static final Integer CHAVE_MODALIDADE = 11;
	private static final Integer DESCRICAO_MODALIDADE = 12;
	private static final Integer VALOR_CALCULADO_PERCENTUAL = 13;
	private static final Integer QTD_VALORES = 3;
	private static final Integer PORCENTAGEM = 100;
	private static final Integer ESCALA_PERCENTUAL = 2;
	private static final Integer VALOR_FINAL_FORMATADO =2;
	private static final Integer ESCALA_VALOR_UNITARIO =4;
	private static final Integer PERIODO_MES_ANO = 6;
	private static final Integer PERIODO_INICIO_FIM = 4;
	private static final Integer INDICE_FINAL_ANO = 4;
	private static final Integer INDICE_FINAL_MES = 6;
	private static final Integer DIAS_ANO = 365;
	private static final Integer PERIODO_MES = 30;
	private static final Integer PERIODO_TRIMESTRE =60;
	private static final Integer LIMITE_MESES = 12;
	private static final Integer LIMITE_DIAS_MES = 31;
	private static final Integer ESCALA_CONSUMO_DIARIO = 6;
	private static final Integer ESCALA_GAS = 4;
	private static final Integer PERCENTUAL_DESVIO = 99;
	private static final String ERRO_INESPERADO = "ERRO_INESPERADO";

	private Map<PontoConsumo, BigDecimal> mapaQNRGrupoEconomico;

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Penalidade.BEAN_ID_PENALIDADE);
	}

	public Class<?> getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade() {

		return ServiceLocator.getInstancia()
				.getClassPorID(ApuracaoQuantidadePenalidadePeriodicidade.BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE_PERIODICIDADE);
	}

	public Class<?> getClasseEntidadeApuracaoQuantidade() {

		return ServiceLocator.getInstancia().getClassPorID(ApuracaoQuantidade.BEAN_ID_APURACAO_QUANTIDADE);
	}

	public Class<?> getClasseEntidadePenalidadeDemandaFaixa() {

		return ServiceLocator.getInstancia().getClassPorID(PenalidadeDemandaFaixa.BEAN_ID_PENALIDADE_DEMANDA_FAIXA);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoPenalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPenalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_PENALIDADE);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidadeQDC() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidadeQDC.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE_QDC);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);
	}

	private Class<?> getClasseEntidadeRecuperacao() {

		return ServiceLocator.getInstancia().getClassPorID(Recuperacao.BEAN_ID_RECUPERACAO);
	}

	public Class<?> getClasseEntidadeQuantidadeRecuperacaoPenalid() {

		return ServiceLocator.getInstancia().getClassPorID(QuantidadeRecuperacaoPenalidade.BEAN_ID_QUANTIDADE_RECUPERACAO_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Penalidade.BEAN_ID_PENALIDADE);
	}

	/**
	 * Penalidade retirada maior menor.
	 *
	 * @param contrato the contrato
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param dadosAuditoria the dados auditoria
	 * @param logProcessamento the log processamento
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	private BigDecimal penalidadeRetiradaMaiorMenor(final Contrato contrato, final Collection<PontoConsumo> listaPontoConsumo,
			final Date dataInicial, final Date dataFinal, final DadosAuditoria dadosAuditoria, StringBuilder logProcessamento)
			throws GGASException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorTarifa controladorTarifa =
				(ControladorTarifa) ServiceLocator.getInstancia().getBeanPorID(ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);

		ControladorCalculoFornecimentoGas controladorGas = (ControladorCalculoFornecimentoGas) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);

		ServiceLocator.getInstancia().getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		final Long[] chavesPontosConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumo);
		final Long[] chavesTipoParada = new Long[] { EntidadeConteudo.CHAVE_PARADA_PROGRAMADA, EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO,
				EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO };
		PontoConsumo primeiroPontoConsumo = listaPontoConsumo.iterator().next();

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
		String codPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		// Obter tarifas média do gás e
		// transporte.
		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE));

		// Regra para ToP, SoP, RetiradaMaior e
		// Menor: Se no contrato
		// agrupamentoCobranca for true, passar
		// listaPontoConsumo será atribuido null.
		Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>(listaPontoConsumo);
		String codigoAgrupamentoVolume =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME);
		boolean isAgrupadoVolume = Boolean.FALSE;
		if ((contrato != null && contrato.getTipoAgrupamento() != null)
				&& (contrato.getTipoAgrupamento().getChavePrimaria() == Long.parseLong(codigoAgrupamentoVolume))) {
			isAgrupadoVolume = Boolean.TRUE;
		}
		if ((contrato != null) && contrato.getAgrupamentoCobranca() && isAgrupadoVolume) {
			listaPontoConsumoAux = null;
		}

		boolean comAviso = false;
		Boolean temRetiradaMaior = false;
		Boolean temRetiradaMenor = false;
		Map<String, Object> params = new HashMap<>();

		if( contrato != null) {
			params.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		}
		params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicial);
		params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataFinal);
		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));

		Boolean existePenalidadeModalidade = Boolean.FALSE;

		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades =
				controladorContrato.consultarContratoPontoConsumoModalidades(contrato, primeiroPontoConsumo);

		String tabelaPrecoUltimoVigente = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TABELA_PRECOS_VIGENTE_ULTIDO_DIA_PERIODO_PENALIDADE);

		String aplicacaoQDCCascataTarifaria =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_APLICACAO_QDC_CASCATA_TARIFARIA);

		String aplicacaoVolumePenalidadeCascataTarifaria =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_APLICACAO_VOLUME_PENALIDADE_CASCATA_TARIFARIA);

		String tarifaMediaPeriodo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TARIFA_MEDIA_PERIODO);

		for (ContratoPontoConsumoModalidade modalidade : contratoPontoConsumoModalidades) {

			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = this.filtrarListaPorPenalidade(
					modalidade.getListaContratoPontoConsumoPenalidade(), codPenalidadeRetiradaMaior, dataInicial, dataFinal);
			if (listaContratoPontoConsumoPenalidade != null) {
				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidadeMaior =
						this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeRetiradaMaior, null);
				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidadeMenor =
						this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeRetiradaMenor, null);

				// substituir metodo pra pegar o contratopontoconsumopenalidade correto
				params.put(PARAM_NAME_ID_CONTRATO_MODALIDADE, modalidade.getChavePrimaria());

				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMaior =
						controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);

				params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));
				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMenor =
						controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);

				if (contratoPontoConsumoPenalidadeMenor != null) {
					for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPriodMenor : listaApuracaoQtdPenalidadeMenor) {
						temRetiradaMenor = true;
						String dataAux =
								Util.obterUltimoDiaMes(apuracaoQtdPenalPriodMenor.getDataFimApuracao(), Constantes.FORMATO_DATA_BR);
						Date dataFimApuracao =
								Util.converterCampoStringParaData(ROTULO_DATA_APURACACAO, dataAux, Constantes.FORMATO_DATA_BR);

						if (apuracaoQtdPenalPriodMenor.getQtdaNaoRetirada() != null
								&& apuracaoQtdPenalPriodMenor.getQtdaNaoRetirada().compareTo(BigDecimal.ZERO) > 0) {

							BigDecimal qtdFaltante = apuracaoQtdPenalPriodMenor.getQtdaNaoRetirada();
							BigDecimal volumeReferencia =
									this.obterVolumeReferencia(contrato, apuracaoQtdPenalPriodMenor.getQtdaDiariaContratada(),
											apuracaoQtdPenalPriodMenor.getQtdaDiariaProgramada(), codPenalidadeRetiradaMenor, dataInicial,
											dataFinal, contratoPontoConsumoPenalidadeMenor);

							BigDecimal percentualDesvio = qtdFaltante.divide(volumeReferencia, ESCALA_DESVIO, BigDecimal.ROUND_DOWN);

							BigDecimal percentualPenalidade = BigDecimal.ZERO;

							percentualPenalidade = contratoPontoConsumoPenalidadeMenor.getValorPercentualCobRetMaiorMenor();

							BigDecimal volume = apuracaoQtdPenalPriodMenor.getQtdaDiariaRetirada();
							if (volume.compareTo(BigDecimal.ZERO) == 0) {
								volume = qtdFaltante;
							}

							// Verificar os tipos de tabela de cobrança e partir dele calcular a tarifa média

							if (contratoPontoConsumoPenalidadeMenor.getPrecoCobranca().getChavePrimaria() == Long
									.parseLong(tarifaMediaPeriodo)) {
								BigDecimal tarifaMediaGas =
										this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao, volume, true, true);

								if (percentualPenalidade != null) {

									popularApuracaoTarifaMedia(dadosAuditoria, apuracaoQtdPenalPriodMenor, qtdFaltante,
											percentualPenalidade, tarifaMediaGas);
									temRetiradaMenor = true;
									existePenalidadeModalidade = Boolean.TRUE;

								}

							} else if (contratoPontoConsumoPenalidadeMenor.getPrecoCobranca().getChavePrimaria() == Long
									.parseLong(tabelaPrecoUltimoVigente)) {

								ContratoPontoConsumoItemFaturamento cpcif =
										controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);
								Tarifa tarifa = cpcif.getTarifa();
								if (tarifa != null) {

									TarifaVigencia tarifaVigencia = controladorTarifa
											.consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(), dataFinal);
									if (tarifaVigencia != null) {
										if (contratoPontoConsumoPenalidadeMenor.getTipoApuracao().getChavePrimaria() == Long
												.parseLong(aplicacaoQDCCascataTarifaria)) {

											popularTarifaUltimoDiaCascata(dadosAuditoria, apuracaoQtdPenalPriodMenor, qtdFaltante,
													tarifaVigencia, percentualPenalidade);
											temRetiradaMenor = true;
											existePenalidadeModalidade = Boolean.TRUE;

										} else if (contratoPontoConsumoPenalidadeMenor.getTipoApuracao().getChavePrimaria() == Long
												.parseLong(aplicacaoVolumePenalidadeCascataTarifaria)) {

											TarifaVigenciaFaixa tarifaVigenciaFaixa = controladorGas.obterTarifaVigenciaFaixaPorConsumo(
													tarifaVigencia.getTarifaVigenciaFaixas(), qtdFaltante);
											if (tarifaVigenciaFaixa != null) {

												popularTarifaUltimoDiaFaixa(dadosAuditoria, apuracaoQtdPenalPriodMenor, qtdFaltante,
														tarifaVigenciaFaixa, percentualPenalidade, tarifaVigencia);
												temRetiradaMenor = true;
												existePenalidadeModalidade = Boolean.TRUE;

											}

										}
									}

								}

							} else {
								LOG.info(msgErroPercentualPenalidade(percentualDesvio));
							}

						}
					}
				}

				if (contratoPontoConsumoPenalidadeMaior != null) {

					for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriodMaior : listaApuracaoQtdPenalidadeMaior) {
						comAviso = this.verificarAvisoParada(contratoPontoConsumoPenalidadeMaior.getBaseApuracao(), chavesPontosConsumo,
								chavesTipoParada, apuracaoQtdPenalPeriodMaior.getDataInicioApuracao(),
								apuracaoQtdPenalPeriodMaior.getDataFimApuracao());
						// Obter a data do fim da apuracao.
						// Como nesses casos a base de
						// apuração pode ser diária, pegar
						// sempre o uútimo dia do mês.
						temRetiradaMaior = true;
						String dataAux =
								Util.obterUltimoDiaMes(apuracaoQtdPenalPeriodMaior.getDataFimApuracao(), Constantes.FORMATO_DATA_BR);
						Date dataFimApuracao =
								Util.converterCampoStringParaData(ROTULO_DATA_APURACACAO, dataAux, Constantes.FORMATO_DATA_BR);
						BigDecimal percentualDesvio = BigDecimal.valueOf(PERCENTUAL_DESVIO);
						if (apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetiradaMaior() != null
								&& apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetiradaMaior().compareTo(BigDecimal.ZERO) > 0) {

							BigDecimal qtdDiariaRetiradaMaior = apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetiradaMaior();
							BigDecimal qtdDiariaRetirada = apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetirada();

							BigDecimal volumeReferencia =
									this.obterVolumeReferencia(contrato, apuracaoQtdPenalPeriodMaior.getQtdaDiariaContratada(),
											apuracaoQtdPenalPeriodMaior.getQtdaDiariaProgramada(), codPenalidadeRetiradaMaior, dataInicial,
											dataFinal, contratoPontoConsumoPenalidadeMaior);

							if (!(volumeReferencia !=null && volumeReferencia.compareTo(BigDecimal.ZERO)==0)) {
								percentualDesvio = qtdDiariaRetirada.divide(volumeReferencia, ESCALA_DESVIO, BigDecimal.ROUND_HALF_DOWN);
							}
							if (percentualDesvio.compareTo(BigDecimal.valueOf(PERCENTUAL_DESVIO)) > 0) {
								percentualDesvio = BigDecimal.valueOf(PERCENTUAL_DESVIO);
							}

							// Tratar caso percentual
							// penalidade seja nulo. (?)

							BigDecimal percentualPenalidade = BigDecimal.ZERO;

							if (comAviso) {
								percentualPenalidade = contratoPontoConsumoPenalidadeMaior.getValorPercentualCobIntRetMaiorMenor();
							} else {
								percentualPenalidade = contratoPontoConsumoPenalidadeMaior.getValorPercentualCobRetMaiorMenor();
							}
							if (contratoPontoConsumoPenalidadeMaior.getBaseApuracao()
									.getChavePrimaria() == EntidadeConteudo.CHAVE_BASE_APURACAO_PENALIDADE_MENSAL) {
								Integer qtdDiasPeriodo = Util.quantidadeDiasIntervalo(apuracaoQtdPenalPeriodMaior.getDataInicioApuracao(),
										apuracaoQtdPenalPeriodMaior.getDataFimApuracao());
								qtdDiariaRetirada =
										qtdDiariaRetirada.divide(BigDecimal.valueOf(qtdDiasPeriodo), ESCALA_DESVIO, BigDecimal.ROUND_HALF_DOWN);
							}

							if (contratoPontoConsumoPenalidadeMaior.getPrecoCobranca().getChavePrimaria() == Long
									.parseLong(tarifaMediaPeriodo)) {

								BigDecimal tarifaMediaGas = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao,
										qtdDiariaRetirada, true, true);

								if (percentualPenalidade != null) {
									percentualPenalidade = percentualPenalidade.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);
									qtdDiariaRetiradaMaior = qtdDiariaRetiradaMaior.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);
									tarifaMediaGas = tarifaMediaGas.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);

									popularApuracaoTarifaMedia(dadosAuditoria, apuracaoQtdPenalPeriodMaior, qtdDiariaRetiradaMaior,
											percentualPenalidade, tarifaMediaGas);
									temRetiradaMaior = true;
									existePenalidadeModalidade = Boolean.TRUE;
								} else {
									LOG.info(msgErroPercentualPenalidade(percentualDesvio));
								}

							} else if (contratoPontoConsumoPenalidadeMaior.getPrecoCobranca().getChavePrimaria() == Long
									.parseLong(tabelaPrecoUltimoVigente)) {
								ContratoPontoConsumoItemFaturamento cpcif =
										controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);
								Tarifa tarifa = cpcif.getTarifa();
								if (tarifa != null) {
									TarifaVigencia tarifaVigencia = controladorTarifa
											.consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(), dataFinal);
									if (tarifaVigencia != null) {
										if (contratoPontoConsumoPenalidadeMenor.getTipoApuracao().getChavePrimaria() == Long
												.parseLong(aplicacaoQDCCascataTarifaria)) {

											popularTarifaUltimoDiaCascata(dadosAuditoria, apuracaoQtdPenalPeriodMaior,
													qtdDiariaRetiradaMaior, tarifaVigencia, percentualPenalidade);
											temRetiradaMaior = true;
											existePenalidadeModalidade = Boolean.TRUE;
										} else if (contratoPontoConsumoPenalidadeMenor.getTipoApuracao().getChavePrimaria() == Long
												.parseLong(aplicacaoVolumePenalidadeCascataTarifaria)) {
											TarifaVigenciaFaixa tarifaVigenciaFaixa = controladorGas.obterTarifaVigenciaFaixaPorConsumo(
													tarifaVigencia.getTarifaVigenciaFaixas(), qtdDiariaRetiradaMaior);
											if (tarifaVigenciaFaixa != null) {

												popularTarifaUltimoDiaFaixa(dadosAuditoria, apuracaoQtdPenalPeriodMaior,
														qtdDiariaRetiradaMaior, tarifaVigenciaFaixa, percentualPenalidade, tarifaVigencia);
												temRetiradaMaior = true;
												existePenalidadeModalidade = Boolean.TRUE;

											}
										}

									}
								}

							}
						}

					}

				}

			}
		}

		if (existePenalidadeModalidade.equals(Boolean.FALSE)) {
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidadeMaior =
					this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeRetiradaMaior, null);
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidadeMenor =
					this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeRetiradaMenor, null);

			params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));
			ContratoPenalidade contratoPenalidadeRetiradaMaior =
					controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

			params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));

			ContratoPenalidade contratoPenalidadeRetiradaMenor =
					controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

			if (contratoPenalidadeRetiradaMenor != null) {

				comAviso = this.verificarAvisoParada(contratoPenalidadeRetiradaMaior.getBaseApuracao(), chavesPontosConsumo,
						chavesTipoParada, dataInicial, dataFinal);
			}

			if (contratoPenalidadeRetiradaMaior != null) {
				// Tratar penalidades por retirada a maior
				for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriodMaior : listaApuracaoQtdPenalidadeMaior) {

					// Obter a data do fim da apuracao.
					// Como nesses casos a base de
					// apuração pode ser diária, pegar
					// sempre o uútimo dia do mês.
					temRetiradaMaior = true;
					String dataAux = Util.obterUltimoDiaMes(apuracaoQtdPenalPeriodMaior.getDataFimApuracao(), Constantes.FORMATO_DATA_BR);
					Date dataFimApuracao = Util.converterCampoStringParaData(ROTULO_DATA_APURACACAO, dataAux, Constantes.FORMATO_DATA_BR);

					if (apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetiradaMaior() != null
							&& apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetiradaMaior().compareTo(BigDecimal.ZERO) > 0) {

						BigDecimal qtdDiariaRetiradaMaior = apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetiradaMaior();
						BigDecimal qtdDiariaRetirada = apuracaoQtdPenalPeriodMaior.getQtdaDiariaRetirada();

						BigDecimal volumeReferencia =
								this.obterVolumeReferencia(contrato, apuracaoQtdPenalPeriodMaior.getQtdaDiariaContratada(),
										apuracaoQtdPenalPeriodMaior.getQtdaDiariaProgramada(), codPenalidadeRetiradaMaior, dataInicial,
										dataFinal, null);

						BigDecimal percentualDesvio = BigDecimal.valueOf(PERCENTUAL_DESVIO);
						if (!(volumeReferencia.compareTo(BigDecimal.ZERO)==0)) {
							percentualDesvio = qtdDiariaRetirada.divide(volumeReferencia, ESCALA_DESVIO, BigDecimal.ROUND_HALF_DOWN);
						}
						if (percentualDesvio.compareTo(BigDecimal.valueOf(PERCENTUAL_DESVIO)) > 0) {
							percentualDesvio = BigDecimal.valueOf(PORCENTAGEM);
						}

						// Tratar caso percentual
						// penalidade seja nulo. (?)

						//
						BigDecimal percentualPenalidade = BigDecimal.ZERO;

						if (comAviso) {
							percentualPenalidade = contratoPenalidadeRetiradaMaior.getValorPercentualCobIntRetMaiorMenor();
						} else {
							percentualPenalidade = contratoPenalidadeRetiradaMaior.getValorPercentualCobRetMaiorMenor();
						}

						if (contratoPenalidadeRetiradaMaior.getBaseApuracao()
								.getChavePrimaria() == EntidadeConteudo.CHAVE_BASE_APURACAO_PENALIDADE_MENSAL) {
							Integer qtdDiasPeriodo = Util.quantidadeDiasIntervalo(apuracaoQtdPenalPeriodMaior.getDataInicioApuracao(),
									apuracaoQtdPenalPeriodMaior.getDataFimApuracao());
							qtdDiariaRetirada = qtdDiariaRetirada.divide(BigDecimal.valueOf(qtdDiasPeriodo), ESCALA_QTD_DIARIA_RETIRADA,
									BigDecimal.ROUND_HALF_DOWN);
						}

						if (contratoPenalidadeRetiradaMaior.getPrecoCobranca().getChavePrimaria() == Long.parseLong(tarifaMediaPeriodo)) {

							BigDecimal tarifaMediaGas = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao,
									qtdDiariaRetirada, true, true);

							if (percentualPenalidade != null) {
								percentualPenalidade = percentualPenalidade.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);
								qtdDiariaRetiradaMaior = qtdDiariaRetiradaMaior.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);
								tarifaMediaGas = tarifaMediaGas.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);

								popularApuracaoTarifaMedia(dadosAuditoria, apuracaoQtdPenalPeriodMaior, qtdDiariaRetiradaMaior,
										percentualPenalidade, tarifaMediaGas);
								temRetiradaMaior = true;

							} else {
								LOG.info(msgErroPercentualPenalidade(percentualDesvio));
							}

						} else if (contratoPenalidadeRetiradaMaior.getPrecoCobranca().getChavePrimaria() == Long
								.parseLong(tabelaPrecoUltimoVigente)) {
							ContratoPontoConsumoItemFaturamento cpcif =
									controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);
							Tarifa tarifa = cpcif.getTarifa();
							if (tarifa != null) {
								TarifaVigencia tarifaVigencia =
										controladorTarifa.consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(), dataFinal);
								if (tarifaVigencia != null) {
									if (contratoPenalidadeRetiradaMaior.getTipoApuracao().getChavePrimaria() == Long
											.valueOf(aplicacaoQDCCascataTarifaria)) {

										popularTarifaUltimoDiaCascata(dadosAuditoria, apuracaoQtdPenalPeriodMaior, qtdDiariaRetiradaMaior,
												tarifaVigencia, percentualPenalidade);
										temRetiradaMaior = true;

									} else if (contratoPenalidadeRetiradaMaior.getTipoApuracao().getChavePrimaria() == Long
											.valueOf(aplicacaoVolumePenalidadeCascataTarifaria)) {
										TarifaVigenciaFaixa tarifaVigenciaFaixa = controladorGas.obterTarifaVigenciaFaixaPorConsumo(
												tarifaVigencia.getTarifaVigenciaFaixas(), qtdDiariaRetiradaMaior);
										if (tarifaVigenciaFaixa != null) {

											popularTarifaUltimoDiaFaixa(dadosAuditoria, apuracaoQtdPenalPeriodMaior, qtdDiariaRetiradaMaior,
													tarifaVigenciaFaixa, percentualPenalidade, tarifaVigencia);
											temRetiradaMaior = true;

										}
									}

								}
							}
						}
					}
				}
			}

			if (contratoPenalidadeRetiradaMenor != null) {
				// Tratar penalidades por retirada a menor
				for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPriodMenor : listaApuracaoQtdPenalidadeMenor) {

					// Obter a data do fim da apuracao.
					// Como nesses casos a base de
					// apuração pode ser diária, pegar
					// sempre o último dia do mês.
					temRetiradaMenor = true;
					String dataAux = Util.obterUltimoDiaMes(apuracaoQtdPenalPriodMenor.getDataFimApuracao(), Constantes.FORMATO_DATA_BR);
					Date dataFimApuracao = Util.converterCampoStringParaData(ROTULO_DATA_APURACACAO, dataAux, Constantes.FORMATO_DATA_BR);

					if (apuracaoQtdPenalPriodMenor.getQtdaNaoRetirada() != null
							&& apuracaoQtdPenalPriodMenor.getQtdaNaoRetirada().compareTo(BigDecimal.ZERO) > 0) {

						BigDecimal qtdFaltante = apuracaoQtdPenalPriodMenor.getQtdaNaoRetirada();
						BigDecimal volumeReferencia = this.obterVolumeReferencia(contrato,
								apuracaoQtdPenalPriodMenor.getQtdaDiariaContratada(), apuracaoQtdPenalPriodMenor.getQtdaDiariaProgramada(),
								codPenalidadeRetiradaMenor, dataInicial, dataFinal, null);

						BigDecimal percentualDesvio = qtdFaltante.divide(volumeReferencia, ESCALA_DESVIO, BigDecimal.ROUND_DOWN);

						BigDecimal percentualPenalidade = BigDecimal.ZERO;

						if (comAviso) {
							percentualPenalidade = contratoPenalidadeRetiradaMenor.getValorPercentualCobIntRetMaiorMenor();
						} else {
							percentualPenalidade = contratoPenalidadeRetiradaMenor.getValorPercentualCobRetMaiorMenor();
						}

						BigDecimal volume = apuracaoQtdPenalPriodMenor.getQtdaDiariaRetirada();
						if (volume.compareTo(BigDecimal.ZERO)==0) {
							volume = qtdFaltante;
						}

						if (contratoPenalidadeRetiradaMenor.getPrecoCobranca().getChavePrimaria() == Long.valueOf(tarifaMediaPeriodo)) {

							BigDecimal tarifaMediaGas =
									this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao, volume, true, true);

							if (percentualPenalidade != null) {

								popularApuracaoTarifaMedia(dadosAuditoria, apuracaoQtdPenalPriodMenor, qtdFaltante, percentualPenalidade,
										tarifaMediaGas);
								temRetiradaMenor = true;

							} else {
								LOG.info(msgErroPercentualPenalidade(percentualDesvio));
							}

							ContratoPontoConsumoItemFaturamento cpcif =
									controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);
							Tarifa tarifa = cpcif.getTarifa();
							if (tarifa != null) {
								TarifaVigencia tarifaVigencia =
										controladorTarifa.consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(), dataFinal);
								if (tarifaVigencia != null) {
									if (contratoPenalidadeRetiradaMenor.getTipoApuracao().getChavePrimaria() == Long
											.valueOf(aplicacaoQDCCascataTarifaria)) {

										popularTarifaUltimoDiaCascata(dadosAuditoria, apuracaoQtdPenalPriodMenor, qtdFaltante,
												tarifaVigencia, percentualPenalidade);
										temRetiradaMaior = true;

									} else if (contratoPenalidadeRetiradaMenor.getTipoApuracao().getChavePrimaria() == Long
											.valueOf(aplicacaoVolumePenalidadeCascataTarifaria)) {
										TarifaVigenciaFaixa tarifaVigenciaFaixa = controladorGas
												.obterTarifaVigenciaFaixaPorConsumo(tarifaVigencia.getTarifaVigenciaFaixas(), qtdFaltante);
										if (tarifaVigenciaFaixa != null) {

											popularTarifaUltimoDiaFaixa(dadosAuditoria, apuracaoQtdPenalPriodMenor, qtdFaltante,
													tarifaVigenciaFaixa, percentualPenalidade, tarifaVigencia);
											temRetiradaMenor = true;

										}
									}

								}
							}
						}

					}
				}
			}
		}

		logProcessamento.append(stringTemRetiradaMaior(temRetiradaMaior));
		logProcessamento.append(stringTemRetiradaMenor(temRetiradaMenor));

		// Retornar informações para montar
		// relatório.
		return null;
	}

	private String stringTemRetiradaMenor(Boolean temRetiradaMenor) {

		if (temRetiradaMenor) {
			return " Penalidade retirada a menor calculada. \r\n";
		} else {
			return " Não foi registrada nenhuma penalidade retirada a menor para o período. \r\n";
		}
	}

	private String stringTemRetiradaMaior(Boolean temRetiradaMaior) {

		if (temRetiradaMaior) {
			return " Penalidade retirada a maior calculada. \r\n";
		} else {
			return " Não foi registrada nenhuma penalidade retirada a maior para o período. \r\n";
		}
	}

	private String msgErroPercentualPenalidade(BigDecimal percentualDesvio) {

		return MensagemUtil.obterMensagem(Constantes.ERRO_PERCENTUAL_PENALIDADE_NAO_ENCONTRADO, percentualDesvio);
	}

	/**
	 * Popular tarifa ultimo dia faixa.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param apuracaoQtdPenalPriodMenor the apuracao qtd penal priod menor
	 * @param qtdFaltante the qtd faltante
	 * @param tarifaVigenciaFaixa the tarifa vigencia faixa
	 * @param percentualPenalidade the percentual penalidade
	 * @param tarifaVigencia the tarifa vigencia
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void popularTarifaUltimoDiaFaixa(final DadosAuditoria dadosAuditoria,
			ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPriodMenor, BigDecimal qtdFaltante,
			TarifaVigenciaFaixa tarifaVigenciaFaixa, BigDecimal percentualPenalidade, TarifaVigencia tarifaVigencia)
			throws ConcorrenciaException, NegocioException {

		ControladorCalculoFornecimentoGas controladorGas = (ControladorCalculoFornecimentoGas) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);

		BigDecimal valorFixo = tarifaVigenciaFaixa.getValorFixo();
		BigDecimal valorVariavel = tarifaVigenciaFaixa.getValorVariavel();
		BigDecimal valorCalculado = qtdFaltante.multiply(valorVariavel);

		valorCalculado = valorCalculado.add(valorFixo);

		BigDecimal valorTarifaMedia = valorCalculado.divide(qtdFaltante);

		valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);

		apuracaoQtdPenalPriodMenor.setValorCalculado(valorCalculado);

		BigDecimal volumePercentualCobranca = qtdFaltante.multiply(percentualPenalidade);
		TarifaVigenciaFaixa tarifaVigenciaFaixaPercentualCobranca =
				controladorGas.obterTarifaVigenciaFaixaPorConsumo(tarifaVigencia.getTarifaVigenciaFaixas(), volumePercentualCobranca);
		BigDecimal valorCalculadoPercentualCobranca =
				volumePercentualCobranca.multiply(tarifaVigenciaFaixaPercentualCobranca.getValorVariavel());
		valorCalculadoPercentualCobranca = valorCalculadoPercentualCobranca.add(tarifaVigenciaFaixaPercentualCobranca.getValorFixo());

		apuracaoQtdPenalPriodMenor.setValorCalculadoPercenCobranca(valorCalculadoPercentualCobranca);
		apuracaoQtdPenalPriodMenor.setValorTarifaMedia(valorTarifaMedia);
		apuracaoQtdPenalPriodMenor.setDadosAuditoria(dadosAuditoria);
		this.atualizar(apuracaoQtdPenalPriodMenor, apuracaoQtdPenalPriodMenor.getClass());
	}

	/**
	 * Popular tarifa ultimo dia cascata.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param apuracaoQtdPenalPriodMenor the apuracao qtd penal priod menor
	 * @param qtdFaltante the qtd faltante
	 * @param tarifaVigencia the tarifa vigencia
	 * @param percentualPenalidade the percentual penalidade
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void popularTarifaUltimoDiaCascata(final DadosAuditoria dadosAuditoria,
			ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPriodMenor, BigDecimal qtdFaltante, TarifaVigencia tarifaVigencia,
			BigDecimal percentualPenalidade) throws ConcorrenciaException, NegocioException {

		BigDecimal consumoRestante = qtdFaltante;
		BigDecimal valorCalculadoPercentualCobranca = BigDecimal.ZERO;
		BigDecimal valorCalculado = BigDecimal.ZERO;

		for (TarifaVigenciaFaixa faixa : tarifaVigencia.getTarifaVigenciaFaixas()) {
			BigDecimal intervaloFaixa = faixa.getMedidaFim().subtract(faixa.getMedidaInicio());

			BigDecimal consumoApuradoNaFaixa = null;
			if (consumoRestante != null) {
				if (intervaloFaixa.compareTo(consumoRestante) > 0) {
					consumoApuradoNaFaixa = consumoRestante;
					consumoRestante = BigDecimal.ZERO;
				} else {
					consumoApuradoNaFaixa = intervaloFaixa;
					consumoRestante = consumoRestante.subtract(intervaloFaixa);
				}
			}

			if (consumoApuradoNaFaixa != null) {
				valorCalculado = valorCalculado.add(consumoApuradoNaFaixa.multiply(faixa.getValorVariavel()));
				if (faixa.getValorFixo() != null) {
					valorCalculado = valorCalculado.add(faixa.getValorFixo());
				}
			}
			if (consumoRestante != null && consumoRestante.compareTo(BigDecimal.ZERO) == 0) {
				break;
			}
		}
		BigDecimal valorTarifaMedia = valorCalculado.divide(qtdFaltante, BigDecimal.ROUND_UP);
		valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);
		apuracaoQtdPenalPriodMenor.setValorCalculado(valorCalculado);

		valorCalculadoPercentualCobranca = valorCalculado.multiply(percentualPenalidade);

		apuracaoQtdPenalPriodMenor.setValorCalculadoPercenCobranca(valorCalculadoPercentualCobranca);
		apuracaoQtdPenalPriodMenor.setValorTarifaMedia(valorTarifaMedia);
		apuracaoQtdPenalPriodMenor.setDadosAuditoria(dadosAuditoria);
		this.atualizar(apuracaoQtdPenalPriodMenor, apuracaoQtdPenalPriodMenor.getClass());
	}

	/**
	 * Popular apuracao tarifa media.
	 *
	 * @param dadosAuditoria the dados auditoria
	 * @param apuracaoQtdPenalPriodMenor the apuracao qtd penal priod menor
	 * @param qtdFaltante the qtd faltante
	 * @param percentualPenalidade the percentual penalidade
	 * @param tarifaMediaGas the tarifa media gas
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void popularApuracaoTarifaMedia(final DadosAuditoria dadosAuditoria,
			ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPriodMenor, BigDecimal qtdFaltante, BigDecimal percentualPenalidade,
			BigDecimal tarifaMediaGas) throws ConcorrenciaException, NegocioException {

		BigDecimal valorCalculado = tarifaMediaGas.multiply(qtdFaltante);
		BigDecimal valorCalculadoPercentualCobranca = valorCalculado.multiply(percentualPenalidade);
		valorCalculadoPercentualCobranca = valorCalculadoPercentualCobranca.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);
		apuracaoQtdPenalPriodMenor.setValorCalculadoPercenCobranca(valorCalculadoPercentualCobranca);
		apuracaoQtdPenalPriodMenor.setValorCalculado(valorCalculado);
		apuracaoQtdPenalPriodMenor.setValorTarifaMedia(tarifaMediaGas);
		apuracaoQtdPenalPriodMenor.setDadosAuditoria(dadosAuditoria);
		this.atualizar(apuracaoQtdPenalPriodMenor, apuracaoQtdPenalPriodMenor.getClass());
	}

	/**
	 * Verificar aviso parada.
	 *
	 * @param baseApuracao the base apuracao
	 * @param chavesPontosConsumo the chaves pontos consumo
	 * @param chavesTipoParada the chaves tipo parada
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarAvisoParada(EntidadeConteudo baseApuracao, Long[] chavesPontosConsumo, Long[] chavesTipoParada,
			Date dataInicial, Date dataFinal) throws NegocioException {

		boolean retorno = Boolean.FALSE;

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		// Obter parametro que indica tipo de
		// apuracao diária.
		String codigoBaseApuracaoDiaria =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_BASE_APURACAO_PENALIDADE_DIARIA);

		if (baseApuracao != null) {

			if (baseApuracao.getChavePrimaria() == Long.parseLong(codigoBaseApuracaoDiaria)) {

				Collection<ParadaProgramada> listaParadasProgramadas = controladorProgramacao
						.consultarParadasProgramadasPeriodo(chavesPontosConsumo, chavesTipoParada, dataInicial, dataFinal, SUPRIDORA);
				if (!listaParadasProgramadas.isEmpty()) {
					retorno = Boolean.TRUE;
				}
			}

		} else {
			// vsm - O que fazer? erro ao
			// obter base de apuração retirada
			// maior.
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#obterTarifaMedia(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.lang.Long, java.util.Date, java.math.BigDecimal, boolean, boolean)
	 */
	@Override
	public BigDecimal obterTarifaMedia(PontoConsumo pontoConsumo, Long itemFaturaOpcao, Date dataFinal, BigDecimal consumo,
			boolean considerarPrecoGas, boolean considerarImposto) throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorPrecoGas controladorPrecoGas =
				(ControladorPrecoGas) ServiceLocator.getInstancia().getBeanPorID(ControladorPrecoGas.BEAN_ID_CONTROLADOR_PRECO_GAS);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		ControladorCalculoFornecimentoGas controladorCalculoFornecimentoGas = (ControladorCalculoFornecimentoGas) ServiceLocator
				.getInstancia().getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ContratoPontoConsumo contratoPontoConsumo = null;
		contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());

		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
		}

		EntidadeConteudo objItemFatura = controladorEntidadeConteudo.obter(itemFaturaOpcao);

		Long itemFaturaMargem =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO));

		Long idTributoPIS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PIS));
		Long idTributoCOFINS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COFINS));
		Long idTributoICMS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS));

		Tarifa tarifa = null;

		BigDecimal retorno = BigDecimal.ZERO;

		Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento =
				contratoPontoConsumo.getListaContratoPontoConsumoItemFaturamento();
		boolean existeItemMargem = false;

		for (ContratoPontoConsumoItemFaturamento cpcItemFaturamento : listaContratoPontoConsumoItemFaturamento) {
			if (considerarPrecoGas && cpcItemFaturamento.getItemFatura().getChavePrimaria() == itemFaturaMargem) {
				existeItemMargem = false;
			}
			if (cpcItemFaturamento.getItemFatura().getChavePrimaria() == itemFaturaOpcao) {
				tarifa = cpcItemFaturamento.getTarifa();
			}
		}

		if (tarifa != null) {

			BigDecimal aliquotaPIS = BigDecimal.ZERO;
			BigDecimal aliquotaCOFINS = BigDecimal.ZERO;
			BigDecimal aliquotaICMS = BigDecimal.ZERO;

			if (existeItemMargem) {
				EntidadeConteudo contratoCompra = controladorContrato.obterContratoCompraPorPontoConsumo(pontoConsumo);
				if (contratoCompra != null) {
					retorno = controladorPrecoGas.consultarPrecoGas(dataFinal, dataFinal, objItemFatura, contratoCompra);
				}

				TributoAliquota tributoAliquotaPIS =
						controladorCalculoFornecimentoGas.obterTributoAliquota(idTributoPIS, Calendar.getInstance().getTime());
				// aliquota em valor
				aliquotaPIS = tributoAliquotaPIS.getValorAliquota();

				TributoAliquota tributoAliquotaCOFINS =
						controladorCalculoFornecimentoGas.obterTributoAliquota(idTributoCOFINS, Calendar.getInstance().getTime());
				// aliquota em valor
				aliquotaCOFINS = tributoAliquotaCOFINS.getValorAliquota();

				TributoAliquota tributoAliquotaICMS =
						controladorCalculoFornecimentoGas.obterTributoAliquota(idTributoICMS, Calendar.getInstance().getTime());
				// aliquota em valor
				aliquotaICMS = tributoAliquotaICMS.getValorAliquota();

				BigDecimal somaAliquotas = aliquotaPIS.add(aliquotaCOFINS).add(aliquotaICMS);
				BigDecimal valorTributos = retorno.multiply(somaAliquotas);
				retorno = retorno.subtract(valorTributos);

			} else {

				boolean isConsiderarDescontos = Boolean.TRUE;

				// se for inadimplente, não
				// considerar descontos.
				if (controladorFatura.verificarInadimplenciaPontoConsumo(pontoConsumo.getChavePrimaria())) {
					isConsiderarDescontos = Boolean.FALSE;
				}

				DadosFornecimentoGasVO dadosFornecimentoGasVO = controladorCalculoFornecimentoGas.calcularValorFornecimentoGas(
						objItemFatura, dataFinal, dataFinal, consumo, tarifa, isConsiderarDescontos, false, null, Boolean.FALSE);
				retorno = dadosFornecimentoGasVO.getValorFinalMetroCubicoGasSemTributo();

				if (dadosFornecimentoGasVO.getColecaoTributos() != null && !dadosFornecimentoGasVO.getColecaoTributos().isEmpty()) {
					Collection<DadosTributoVO> colecaoTributo = dadosFornecimentoGasVO.getColecaoTributos();
					for (DadosTributoVO dadosTributoVO : colecaoTributo) {
						if (dadosTributoVO.getTributoAliquota().getTributo().getChavePrimaria() == idTributoPIS) {
							// aliquota em valor
							aliquotaPIS = dadosTributoVO.getTributoAliquota().getValorAliquota();
						}
						if (dadosTributoVO.getTributoAliquota().getTributo().getChavePrimaria() == idTributoCOFINS) {
							// aliquota em valor
							aliquotaCOFINS = dadosTributoVO.getTributoAliquota().getValorAliquota();
						}
					}
				}

			}

			retorno = retorno.setScale(ESCALA_TARIFA, RoundingMode.HALF_DOWN);

			// Se não é pra considerar impostos,
			// retirar do valor total o pis e
			// cofins.
			if (considerarImposto) {

				BigDecimal somaTributos = aliquotaPIS;
				somaTributos = somaTributos.add(aliquotaCOFINS);

				retorno = retorno.divide(BigDecimal.ONE.subtract(somaTributos), ESCALA_TARIFA, RoundingMode.HALF_DOWN);
			}
		}

		return retorno;
	}

	/**
	 * Obter volume referencia.
	 *
	 * @param contrato the contrato
	 * @param qtdDiariaContratadaMedia the qtd diaria contratada media
	 * @param qtdDiariaProgramadaMedia the qtd diaria programada media
	 * @param codPenalidade the cod penalidade
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param contratoPontoConsumoPenalidade the contrato ponto consumo penalidade
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal obterVolumeReferencia(Contrato contrato, BigDecimal qtdDiariaContratadaMedia, BigDecimal qtdDiariaProgramadaMedia,
			String codPenalidade, Date dataInicial, Date dataFinal, ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade) {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
		String codPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		Map<String, Object> params = new HashMap<>();

		params.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicial);
		params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataFinal);
		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));

		ContratoPenalidade contratoPenalidadeRetiradaMenor = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		BigDecimal retorno = null;
		BigDecimal qtdDiariaContratadaMediaAux = null;
		BigDecimal qtdDiariaProgramadaMediaAux = null;

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		String parametroPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		String parametroPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		String codigoConsumoReferenciaPenalidadeQDC =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_QDC);

		String codigoConsumoReferenciaPenalidadeQDP =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_QDP);

		String codigoConsumoReferenciaPenalidadeMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_MENOR_QDC_QDP);

		EntidadeConteudo consumoReferencia = null;

		if (qtdDiariaContratadaMedia == null) {
			qtdDiariaContratadaMediaAux = BigDecimal.ZERO;
		} else {
			qtdDiariaContratadaMediaAux = qtdDiariaContratadaMedia;
		}

		if (qtdDiariaProgramadaMedia == null) {
			qtdDiariaProgramadaMediaAux = qtdDiariaContratadaMediaAux;
		} else {
			qtdDiariaProgramadaMediaAux = qtdDiariaProgramadaMedia;
		}

		if (Long.parseLong(codPenalidade) == Long.parseLong(parametroPenalidadeRetiradaMaior)) {

			if (contratoPontoConsumoPenalidade != null) {
				consumoReferencia = contratoPontoConsumoPenalidade.getConsumoReferencia();
			} else {
				consumoReferencia = contratoPenalidadeRetiradaMaior.getConsumoReferencia();
			}
		} else if (Long.parseLong(codPenalidade) == Long.parseLong(parametroPenalidadeRetiradaMenor)) {
			if (contratoPontoConsumoPenalidade != null) {
				consumoReferencia = contratoPontoConsumoPenalidade.getConsumoReferencia();
			} else {
				consumoReferencia = contratoPenalidadeRetiradaMenor.getConsumoReferencia();
			}
		}

		if (consumoReferencia != null && consumoReferencia.getChavePrimaria() == Long.parseLong(codigoConsumoReferenciaPenalidadeQDC)) {
			retorno = qtdDiariaContratadaMediaAux;
		} else if (consumoReferencia != null
				&& consumoReferencia.getChavePrimaria() == Long.parseLong(codigoConsumoReferenciaPenalidadeQDP)) {
			retorno = qtdDiariaProgramadaMediaAux;
		} else if (consumoReferencia != null
				&& consumoReferencia.getChavePrimaria() == Long.parseLong(codigoConsumoReferenciaPenalidadeMenor)) {
			if (qtdDiariaContratadaMediaAux.compareTo(qtdDiariaProgramadaMediaAux) < 0) {
				retorno = qtdDiariaContratadaMediaAux;
			} else {
				retorno = qtdDiariaProgramadaMediaAux;
			}
		}

		return retorno;
	}

	/**
	 * Consultar apuracao qtd penalidade periodicidade.
	 *
	 * @param contrato the contrato
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param codPenalidade the cod penalidade
	 * @param contratoModalidade the contrato modalidade
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> consultarApuracaoQtdPenalidadePeriodicidade(Contrato contrato,
			Collection<PontoConsumo> listaPontoConsumo, String codPenalidade, ContratoModalidade contratoModalidade)
			throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		Contrato contratoPai = contrato;
		if (contrato.getChavePrimariaPai() != null) {
			contratoPai = (Contrato) controladorContrato.obter(contrato.getChavePrimariaPai());
		}

		StringBuilder hql = new StringBuilder();
		hql.append("select apuracaoQtdPenalPeriod ");
		hql.append(" from ");
		hql.append(getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade().getSimpleName());
		hql.append(" apuracaoQtdPenalPeriod ");
		hql.append(" inner join apuracaoQtdPenalPeriod.contrato contrato ");
		hql.append(" inner join apuracaoQtdPenalPeriod.penalidade penalidade ");

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			hql.append(" inner join apuracaoQtdPenalPeriod.pontoConsumo pontoConsumo ");
		}

		hql.append(" where penalidade.chavePrimaria = :codPenalidade ");
		hql.append(" and contrato.chavePrimaria = :idContrato");
		hql.append(" and apuracaoQtdPenalPeriod.valorCobrado is null ");

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			hql.append(" and pontoConsumo.chavePrimaria in (:chavesPontoConsumo) ");
		}

		if (contratoModalidade != null) {
			hql.append(" and apuracaoQtdPenalPeriod.contratoModalidade.chavePrimaria = :idContratoModalidade ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("codPenalidade", Long.parseLong(codPenalidade));
		query.setLong(PARAM_NAME_ID_CONTRATO, contratoPai.getChavePrimaria());

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			query.setParameterList("chavesPontoConsumo", Util.collectionParaArrayChavesPrimarias(listaPontoConsumo));
		}

		if (contratoModalidade != null) {
			query.setLong(PARAM_NAME_ID_CONTRATO_MODALIDADE, contratoModalidade.getChavePrimaria());
		}

		return query.list();
	}

	/**
	 * Penalidade do pe gas fora especificacao.
	 *
	 * @param contrato the contrato
	 * @param listapontoConsumo the listaponto consumo
	 * @param codPenalidade the cod penalidade
	 * @param dadosAuditoria the dados auditoria
	 * @param logProcessamento the log processamento
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade .impl. ControladorPenalidade#penalidadeDeliveryOrPay ()
	 */
	private BigDecimal penalidadeDoPeGasForaEspecificacao(Contrato contrato, Collection<PontoConsumo> listapontoConsumo,
			String codPenalidade, DadosAuditoria dadosAuditoria, StringBuilder logProcessamento) throws GGASException {

		PontoConsumo primeiroPontoConsumo = listapontoConsumo.iterator().next();

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidadePeriodicidade =
				this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listapontoConsumo, codPenalidade, null);

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Boolean temPenalidade = false;

		String parametroPenalidadeDoP =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_DELIVERY_OR_PAY);

		String parametroPenalidadeGasForaEspecif =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO);

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod : listaApuracaoQtdPenalidadePeriodicidade) {

			// Obter a data do fim da apuracao.
			// Como nesses casos a base de
			// apuração pode ser diária, pegar
			// sempre o uútimo dia do mês.
			String dataAux = Util.obterUltimoDiaMes(apuracaoQtdPenalPeriod.getDataFimApuracao(), Constantes.FORMATO_DATA_BR);
			Date dataFimApuracao = Util.converterCampoStringParaData(ROTULO_DATA_APURACACAO, dataAux, Constantes.FORMATO_DATA_BR);

			BigDecimal percentualTarifa = null;
			BigDecimal qtdFaltante = null;

			if (Long.parseLong(codPenalidade) == Long.parseLong(parametroPenalidadeDoP)) {
				qtdFaltante = apuracaoQtdPenalPeriod.getQtdaNaoRetiradaPorParadaNaoProgramada();
				percentualTarifa = contrato.getPercentualTarifaDoP();
			} else if (Long.parseLong(codPenalidade) == Long.parseLong(parametroPenalidadeGasForaEspecif)) {
				qtdFaltante = apuracaoQtdPenalPeriod.getQtdaNaoRetiradaPorFalhaFornecimento();
				percentualTarifa = contrato.getPercentualSobreTariGas();
			}

			if (qtdFaltante != null && qtdFaltante.compareTo(BigDecimal.ZERO) > 0) {

				Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

				BigDecimal tarifaMedia = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao,
						apuracaoQtdPenalPeriod.getQtdaDiariaRetirada(), false, true);

				BigDecimal valorCalculado = qtdFaltante.multiply(tarifaMedia).multiply(percentualTarifa);
				valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);
				apuracaoQtdPenalPeriod.setValorCalculado(valorCalculado);
				apuracaoQtdPenalPeriod.setDadosAuditoria(dadosAuditoria);
				apuracaoQtdPenalPeriod.setValorTarifaMedia(tarifaMedia);
				this.atualizar(apuracaoQtdPenalPeriod, apuracaoQtdPenalPeriod.getClass());
				temPenalidade = true;
			}

		}

		String codPenalidadeGasForaEspecif =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO);
		String codPenalidadeDoP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_DELIVERY_OR_PAY);

		if (temPenalidade) {
			if (codPenalidade.equals(codPenalidadeDoP)) {
				logProcessamento.append(" Penalidade Delivery Or Pay calculada. \r\n");
			} else if (codPenalidade.equals(codPenalidadeGasForaEspecif)) {
				logProcessamento.append("          Penalidade Gás Fora de Especificação calculada. \r\n");
			}
		} else {
			if (codPenalidade.equals(codPenalidadeDoP)) {
				logProcessamento.append("          Não foi registrada nenhuma penalidade Delivery Or Pay para o período. \r\n");
			} else if (codPenalidade.equals(codPenalidadeGasForaEspecif)) {
				logProcessamento.append(" Não foi registrada nenhuma penalidade Gás Fora de Especificação para o período. \r\n");
			}
		}

		// retornar informacoes para relatorio
		return null;

	}

	/**
	 * Obter qdr apuracao quantidade.
	 *
	 * @param contrato the contrato
	 * @param idPontoConsumo the id ponto consumo
	 * @param data the data
	 * @param apuracaoVO the apuracao vo
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal obterQDRApuracaoQuantidade(Contrato contrato, Long idPontoConsumo, Date data, ApuracaoVO apuracaoVO) {

		BigDecimal valorQDR = null;

		for (ApuracaoQuantidade apuracaoQuantidade : apuracaoVO.getListaApuracaoQuantidade()) {
			if (idPontoConsumo != null) {
				if ((data.compareTo(apuracaoQuantidade.getDataApuracaoVolumes()) == 0)
						&& (contrato.getChavePrimaria() == apuracaoQuantidade.getContrato().getChavePrimaria())
						&& (idPontoConsumo == apuracaoQuantidade.getPontoConsumo().getChavePrimaria())) {

					valorQDR = apuracaoQuantidade.getQtdaDiariaRetirada();
					break;
				}
			} else {
				if ((data.compareTo(apuracaoQuantidade.getDataApuracaoVolumes()) == 0)
						&& (contrato.getChavePrimaria() == apuracaoQuantidade.getContrato().getChavePrimaria())) {
					valorQDR = apuracaoQuantidade.getQtdaDiariaRetirada();
					break;
				}
			}

		}

		return valorQDR;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade .impl. ControladorPenalidade#compromissoShipOrPay()
	 */
	private BigDecimal compromissoShipOrPay(Contrato contrato, Collection<PontoConsumo> listaPontoConsumo,
			DadosAuditoria dadosAuditoria, StringBuilder logProcessamento) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		Long itemFaturaTransporte = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE));

		PontoConsumo primeiroPontoConsumo = listaPontoConsumo.iterator().next();

		// Identifica as modalidades existentes no
		// contrato
		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades =
				controladorContrato.consultarContratoPontoConsumoModalidades(contrato, primeiroPontoConsumo);

		String codigoAgrupamentoVolume =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME);
		boolean isAgrupadoVolume = Boolean.FALSE;
		if (contrato != null && contrato.getTipoAgrupamento() != null
				&& contrato.getTipoAgrupamento().getChavePrimaria() == Long.parseLong(codigoAgrupamentoVolume)) {
			isAgrupadoVolume = Boolean.TRUE;
		}

		Boolean temPenalidade = false;
		// Para cada modalidade
		for (ContratoPontoConsumoModalidade modalidade : contratoPontoConsumoModalidades) {

			String codPenalidadeShipOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);

			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade =
					this.filtrarListaPorPenalidade(modalidade.getListaContratoPontoConsumoPenalidade(), codPenalidadeShipOrPay, null, null);

			for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaContratoPontoConsumoPenalidade) {

				// Regra para ToP, SoP,
				// RetiradaMaior e Menor: Se no
				// contrato
				// agrupamentoConta == true,
				// passar listaPontoConsumo =
				// null.
				Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>(listaPontoConsumo);

				if (contrato.getAgrupamentoCobranca() && isAgrupadoVolume) {
					listaPontoConsumoAux = null;
				}

				ContratoModalidade contratoModalidade =
						contratoPontoConsumoPenalidade.getContratoPontoConsumoModalidade().getContratoModalidade();

				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidPeriod =
						this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeShipOrPay,
								contratoModalidade);

				// Para cada elemento, atualizar
				// valores.
				for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalidPeriod : listaApuracaoQtdPenalidPeriod) {

					Date dataFimApuracao = apuracaoQtdPenalidPeriod.getDataFimApuracao();

					// Aplicar fórmula para achar
					// a quantidade de gas a ser
					// faturada
					Boolean considerarImposto = Boolean.TRUE;

					BigDecimal qtdNR = apuracaoQtdPenalidPeriod.getQtdaNaoRetirada();

					if (qtdNR != null && qtdNR.compareTo(BigDecimal.ZERO) > 0) {

						// ver se tem direito a
						// recuperar
						if (modalidade.getDataInicioRetiradaQPNR() != null) {
							// retirar do valor da
							// tarifa o pis e
							// cofins.
							considerarImposto = Boolean.FALSE;
						}

						// Valor de tarifa para
						// transporte.
						BigDecimal valorTarifa = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaTransporte, dataFimApuracao, qtdNR,
								true, considerarImposto);

						// Caso não encontre para
						// transporte, obter para
						// o item
						// de Gás.
						if (valorTarifa.compareTo(BigDecimal.ZERO) == 0) {
							valorTarifa = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao, qtdNR, true,
									considerarImposto);
						}

						BigDecimal valorCalculado = qtdNR.multiply(valorTarifa);
						valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);
						apuracaoQtdPenalidPeriod.setValorCalculado(valorCalculado);
						apuracaoQtdPenalidPeriod.setValorTarifaMedia(valorTarifa);
						apuracaoQtdPenalidPeriod.setDadosAuditoria(dadosAuditoria);

						this.atualizar(apuracaoQtdPenalidPeriod, apuracaoQtdPenalidPeriod.getClass());
						temPenalidade = true;

					}
				}
			}

		}

		if (temPenalidade) {
			logProcessamento.append(" Penalidade de Ship Or Pay calculada. \r\n");
		} else {
			logProcessamento.append(" Não foi registrada nenhuma penalidade Ship Or Pay para o período . \r\n");
		}

		// retornar informacoes para gerar o
		// relatorio.
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade .impl. ControladorPenalidade#compromissoTakeOrPay()
	 */
	private BigDecimal compromissoTakeOrPay(Contrato contrato, Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal,
			DadosAuditoria dadosAuditoria, StringBuilder logProcessamento) throws NegocioException, ConcorrenciaException {

		PontoConsumo primeiroPontoConsumo = listaPontoConsumo.iterator().next();

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorTarifa controladorTarifa =
				(ControladorTarifa) ServiceLocator.getInstancia().getBeanPorID(ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);

		ControladorCalculoFornecimentoGas controladorGas = (ControladorCalculoFornecimentoGas) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);

		String volReferenciaTarifaMedia =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_VOLUME_REF_TARIFA_MEDIA);

		String tabelaPrecoUltimoVigente = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TABELA_PRECOS_VIGENTE_ULTIDO_DIA_PERIODO_PENALIDADE);

		String aplicacaoQDCCascataTarifaria =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_APLICACAO_QDC_CASCATA_TARIFARIA);

		String aplicacaoVolumePenalidadeCascataTarifaria =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_APLICACAO_VOLUME_PENALIDADE_CASCATA_TARIFARIA);

		String tarifaMediaPeriodo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TARIFA_MEDIA_PERIODO);

		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		// Identifica as modalidades existentes no
		// contrato
		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades =
				controladorContrato.consultarContratoPontoConsumoModalidades(contrato, primeiroPontoConsumo);

		String codigoAgrupamentoVolume =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME);
		boolean isAgrupadoVolume = Boolean.FALSE;
		if (contrato != null && contrato.getTipoAgrupamento() != null
				&& contrato.getTipoAgrupamento().getChavePrimaria() == Long.parseLong(codigoAgrupamentoVolume)) {
			isAgrupadoVolume = Boolean.TRUE;
		}

		Boolean temPenalidade = false;

		// Para cada modalidade
		for (ContratoPontoConsumoModalidade modalidade : contratoPontoConsumoModalidades) {

			String codPenalidadeTakeOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade = this.filtrarListaPorPenalidade(
					modalidade.getListaContratoPontoConsumoPenalidade(), codPenalidadeTakeOrPay, dataInicial, dataFinal);

			for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaContratoPontoConsumoPenalidade) {

				// Regra para ToP, SoP,
				// RetiradaMaior e Menor: Se no
				// contrato
				// agrupamentoConta == true,
				// passar listaPontoConsumo =
				// null.
				Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>(listaPontoConsumo);
				if (contrato.getAgrupamentoCobranca() && isAgrupadoVolume) {
					listaPontoConsumoAux = null;
				}

				ContratoModalidade contratoModalidade =
						contratoPontoConsumoPenalidade.getContratoPontoConsumoModalidade().getContratoModalidade();

				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidPeriod =
						this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeTakeOrPay,
								contratoModalidade);

				// para cada elemento, atualizar
				// valores.
				for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalidPeriod : listaApuracaoQtdPenalidPeriod) {

					Date dataFimApuracao = apuracaoQtdPenalidPeriod.getDataFimApuracao();

					Boolean considerarImposto = Boolean.TRUE;
					BigDecimal volumeReferencia = null;
					BigDecimal qtdaNaoRetirada = apuracaoQtdPenalidPeriod.getQtdaNaoRetirada();

					if (qtdaNaoRetirada != null && qtdaNaoRetirada.compareTo(BigDecimal.ZERO) > 0) {

						if (Long.parseLong(volReferenciaTarifaMedia) == EntidadeConteudo.CHAVE_QDC) {
							volumeReferencia = apuracaoQtdPenalidPeriod.getQtdaDiariaContratadaMedia();
						} else if (Long.parseLong(volReferenciaTarifaMedia) == EntidadeConteudo.CHAVE_QNR) {
							volumeReferencia = apuracaoQtdPenalidPeriod.getQtdaNaoRetirada();
						}

						// ver se tem direito a
						// recuperar
						if (modalidade.getDataInicioRetiradaQPNR() != null) {
							considerarImposto = Boolean.FALSE;
						}

						BigDecimal valorTarifa = BigDecimal.ZERO;

						if (contratoPontoConsumoPenalidade.getPrecoCobranca().getChavePrimaria() == Long.parseLong(tarifaMediaPeriodo)) {

							valorTarifa = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao, volumeReferencia,
									true, considerarImposto);

						} else if (contratoPontoConsumoPenalidade.getPrecoCobranca().getChavePrimaria() == Long
								.parseLong(tabelaPrecoUltimoVigente)) {

							ContratoPontoConsumoItemFaturamento cpcif =
									controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);

							Tarifa tarifa = cpcif.getTarifa();
							if (tarifa != null) {

								TarifaVigencia tarifaVigencia =
										controladorTarifa.consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(), dataFinal);

								if (tarifaVigencia != null) {

									if (contratoPontoConsumoPenalidade.getTipoApuracao().getChavePrimaria() == Long
											.parseLong(aplicacaoQDCCascataTarifaria)) {

										BigDecimal consumoRestante = qtdaNaoRetirada;
										BigDecimal valorCalculado = BigDecimal.ZERO;

										for (TarifaVigenciaFaixa faixa : tarifaVigencia.getTarifaVigenciaFaixas()) {
											BigDecimal intervaloFaixa = faixa.getMedidaFim().subtract(faixa.getMedidaInicio());

											BigDecimal consumoApuradoNaFaixa = null;
											if (intervaloFaixa.compareTo(consumoRestante) > 0) {
												consumoApuradoNaFaixa = consumoRestante;
												consumoRestante = BigDecimal.ZERO;
											} else {
												consumoApuradoNaFaixa = intervaloFaixa;
												consumoRestante = consumoRestante.subtract(intervaloFaixa);
											}

											if (consumoApuradoNaFaixa != null) {
												valorCalculado =
														valorCalculado.add(consumoApuradoNaFaixa.multiply(faixa.getValorVariavel()));
												if (faixa.getValorFixo() != null) {
													valorCalculado = valorCalculado.add(faixa.getValorFixo());
												}
											}
											if (consumoRestante.compareTo(BigDecimal.ZERO) == 0) {
												break;
											}
										}

										valorTarifa = valorCalculado.divide(qtdaNaoRetirada, BigDecimal.ROUND_UP);

									} else if (contratoPontoConsumoPenalidade.getTipoApuracao().getChavePrimaria() == Long
											.parseLong(aplicacaoVolumePenalidadeCascataTarifaria)) {

										TarifaVigenciaFaixa tarifaVigenciaFaixa = controladorGas.obterTarifaVigenciaFaixaPorConsumo(
												tarifaVigencia.getTarifaVigenciaFaixas(), qtdaNaoRetirada);

										if (tarifaVigenciaFaixa != null) {

											BigDecimal valorFixo = tarifaVigenciaFaixa.getValorFixo();
											BigDecimal valorVariavel = tarifaVigenciaFaixa.getValorVariavel();
											BigDecimal valorCalculado = qtdaNaoRetirada.multiply(valorVariavel);

											valorCalculado = valorCalculado.add(valorFixo);

											valorTarifa = valorCalculado.divide(qtdaNaoRetirada);

										}

									}

								}

							}

						}

						BigDecimal valorCalculado = qtdaNaoRetirada.multiply(valorTarifa);
						valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);
						apuracaoQtdPenalidPeriod.setValorCalculado(valorCalculado);
						apuracaoQtdPenalidPeriod.setValorTarifaMedia(valorTarifa);
						apuracaoQtdPenalidPeriod.setDadosAuditoria(dadosAuditoria);

						this.atualizar(apuracaoQtdPenalidPeriod, apuracaoQtdPenalidPeriod.getClass());
						temPenalidade = true;

					}
				}
			}
		}

		// Se não existir penalidade para modalidade, utilizar a penalidade do contrato
		if (!temPenalidade) {

			String codPenalidadeTakeOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

			Collection<ContratoPenalidade> listaContratoPenalidade = this.filtrarListaPorContratoPenalidade(
					contrato.getListaContratoPenalidade(), codPenalidadeTakeOrPay, dataInicial, dataFinal);

			for (ContratoPenalidade penalidade : listaContratoPenalidade) {

				// Regra para ToP, SoP,
				// RetiradaMaior e Menor: Se no
				// contrato
				// agrupamentoConta == true,
				// passar listaPontoConsumo =
				// null.
				Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>(listaPontoConsumo);
				if (contrato.getAgrupamentoCobranca() && isAgrupadoVolume) {
					listaPontoConsumoAux = null;
				}

				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQtdPenalidPeriod =
						this.consultarApuracaoQtdPenalidadePeriodicidade(contrato, listaPontoConsumoAux, codPenalidadeTakeOrPay, null);

				// para cada elemento, atualizar
				// valores.
				for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalidPeriod : listaApuracaoQtdPenalidPeriod) {

					Date dataFimApuracao = apuracaoQtdPenalidPeriod.getDataFimApuracao();

					Boolean considerarImposto = Boolean.TRUE;
					BigDecimal volumeReferencia = null;
					BigDecimal qtdaNaoRetirada = apuracaoQtdPenalidPeriod.getQtdaNaoRetirada();

					if (qtdaNaoRetirada != null && qtdaNaoRetirada.compareTo(BigDecimal.ZERO) > 0) {

						if (Long.parseLong(volReferenciaTarifaMedia) == EntidadeConteudo.CHAVE_QDC) {
							volumeReferencia = apuracaoQtdPenalidPeriod.getQtdaDiariaContratadaMedia();
						} else if (Long.parseLong(volReferenciaTarifaMedia) == EntidadeConteudo.CHAVE_QNR) {
							volumeReferencia = apuracaoQtdPenalidPeriod.getQtdaNaoRetirada();
						}

						BigDecimal valorTarifa = BigDecimal.ZERO;

						if (penalidade.getPrecoCobranca().getChavePrimaria() == Long.parseLong(tarifaMediaPeriodo)) {

							valorTarifa = this.obterTarifaMedia(primeiroPontoConsumo, itemFaturaGas, dataFimApuracao, volumeReferencia,
									true, considerarImposto);

						} else if (penalidade.getPrecoCobranca().getChavePrimaria() == Long.parseLong(tabelaPrecoUltimoVigente)) {

							ContratoPontoConsumoItemFaturamento cpcif =
									controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);

							Tarifa tarifa = cpcif.getTarifa();
							if (tarifa != null) {

								TarifaVigencia tarifaVigencia =
										controladorTarifa.consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(), dataFinal);

								if (tarifaVigencia != null) {

									if (penalidade.getTipoApuracao().getChavePrimaria() == Long.parseLong(aplicacaoQDCCascataTarifaria)) {

										BigDecimal consumoRestante = qtdaNaoRetirada;
										BigDecimal valorCalculado = BigDecimal.ZERO;

										for (TarifaVigenciaFaixa faixa : tarifaVigencia.getTarifaVigenciaFaixas()) {
											BigDecimal intervaloFaixa = faixa.getMedidaFim().subtract(faixa.getMedidaInicio());

											BigDecimal consumoApuradoNaFaixa = null;
											if (intervaloFaixa.compareTo(consumoRestante) > 0) {
												consumoApuradoNaFaixa = consumoRestante;
												consumoRestante = BigDecimal.ZERO;
											} else {
												consumoApuradoNaFaixa = intervaloFaixa;
												consumoRestante = consumoRestante.subtract(intervaloFaixa);
											}

											if (consumoApuradoNaFaixa != null) {
												valorCalculado =
														valorCalculado.add(consumoApuradoNaFaixa.multiply(faixa.getValorVariavel()));
												if (faixa.getValorFixo() != null) {
													valorCalculado = valorCalculado.add(faixa.getValorFixo());
												}
											}
											if (consumoRestante.compareTo(BigDecimal.ZERO) == 0) {
												break;
											}
										}

										valorTarifa = valorCalculado.divide(qtdaNaoRetirada, BigDecimal.ROUND_UP);

									} else if (penalidade.getTipoApuracao().getChavePrimaria() == Long
											.parseLong(aplicacaoVolumePenalidadeCascataTarifaria)) {

										TarifaVigenciaFaixa tarifaVigenciaFaixa = controladorGas.obterTarifaVigenciaFaixaPorConsumo(
												tarifaVigencia.getTarifaVigenciaFaixas(), qtdaNaoRetirada);

										if (tarifaVigenciaFaixa != null) {

											BigDecimal valorFixo = tarifaVigenciaFaixa.getValorFixo();
											BigDecimal valorVariavel = tarifaVigenciaFaixa.getValorVariavel();
											BigDecimal valorCalculado = qtdaNaoRetirada.multiply(valorVariavel);

											valorCalculado = valorCalculado.add(valorFixo);

											valorTarifa = valorCalculado.divide(qtdaNaoRetirada);

										}

									}

								}

							}

						}

						BigDecimal valorCalculado = qtdaNaoRetirada.multiply(valorTarifa);
						valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);
						apuracaoQtdPenalidPeriod.setValorCalculado(valorCalculado);
						apuracaoQtdPenalidPeriod.setValorTarifaMedia(valorTarifa);
						apuracaoQtdPenalidPeriod.setDadosAuditoria(dadosAuditoria);

						this.atualizar(apuracaoQtdPenalidPeriod, apuracaoQtdPenalidPeriod.getClass());
						temPenalidade = true;

					}
				}
			}
		}

		if (temPenalidade) {
			logProcessamento.append(" Penalidade de Take Or Pay calculada. \r\n");
		} else {
			logProcessamento.append(" Não foi registrada nenhuma penalidade Take Or Pay para o período. \r\n");
		}
		// retornar informacoes para gerar o
		// relatorio.
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #tratarPenalidades(br.com.ggas.contrato.
	 * contrato.Contrato, java.util.Collection, java.util.Date, java.util.Date)
	 */
	@Override
	public StringBuilder tratarPenalidades(Contrato contrato, Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal,
			DadosAuditoria dadosAuditoria, StringBuilder logProcessamento) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		try {
			logProcessamento.append("Calculando valores para penalidade por Retirada Maior e Menor. \r\n");
			this.penalidadeRetiradaMaiorMenor(contrato, listaPontoConsumo, dataInicial, dataFinal, dadosAuditoria, logProcessamento);
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			logProcessamento.append("Erro: Não foi possível tratar as penalidades por Retirada Maior e Menor. ");
			logProcessamento.append(e.getClass().getSimpleName());
			logProcessamento.append(" \r\n ");
		}

		if (contrato.getPercentualTarifaDoP() != null) {
			try {
				logProcessamento.append("Calculando valores para penalidade Delivery Or Pay. \r\n ");
				String codPenalidadeDoP =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_DELIVERY_OR_PAY);
				this.penalidadeDoPeGasForaEspecificacao(contrato, listaPontoConsumo, codPenalidadeDoP, dadosAuditoria,
						logProcessamento);
			} catch (Exception e) {
				LOG.error(e.getStackTrace(), e);
				logProcessamento.append("Erro: Não foi possível tratar as penalidades Delivery Or Pay. ");
				logProcessamento.append(e.getClass().getSimpleName());
				logProcessamento.append(" \r\n ");
			}
		}

		if (contrato.getPercentualSobreTariGas() != null) {
			try {
				logProcessamento.append("Calculando valores para penalidade de Gás Fora de Especificação. \r\n");
				String codPenalidadeGasForaEspecif =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO);
				this.penalidadeDoPeGasForaEspecificacao(contrato, listaPontoConsumo, codPenalidadeGasForaEspecif, dadosAuditoria,
						logProcessamento);
			} catch (Exception e) {
				LOG.error(e.getStackTrace(), e);
				logProcessamento.append("Erro: Não foi possível tratar as penalidades Gás Fora de Especificação. ");
				logProcessamento.append(e.getClass().getSimpleName());
				logProcessamento.append(" \r\n ");
			}
		}

		try {
			logProcessamento.append("Calculando valores para penalidade de Ship Or Pay. \r\n");
			this.compromissoShipOrPay(contrato, listaPontoConsumo, dadosAuditoria, logProcessamento);
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			logProcessamento.append("Erro: Não foi possível tratar as penalidades Ship Or Pay. ");
			logProcessamento.append(e.getClass().getSimpleName());
			logProcessamento.append(" \r\n ");
		}

		try {
			logProcessamento.append("Calculando valores para penalidade de Take Or Pay. \r\n");
			this.compromissoTakeOrPay(contrato, listaPontoConsumo, dataInicial, dataFinal, dadosAuditoria, logProcessamento);
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			logProcessamento.append("Erro: Não foi possível tratar as penalidades Take Or Pay. ").append(e.getClass().getSimpleName());
			logProcessamento.append(" \r\n ");
		}

		// gerar relatório
		return logProcessamento;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * listarApuracaoQuantidadePenalidadePeriodicidadePorChaveFatura(java .lang.Long[])
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidadePorChaveFatura(
			Long[] chavesFatura) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade().getSimpleName());
		hql.append(" apuracao ");
		hql.append(" where apuracao.fatura.chavePrimaria in (:chavesFatura)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesFatura", chavesFatura);

		return query.list();
	}

	/**
	 * Método responsável por obter o consumo de teste e a data que excedeu o período de teste.
	 *
	 * @param contratoPontoConsumo O Contrato Ponto de Consumo
	 * @param dataInicial A data inicial
	 * @param dataFinal A data Final
	 * @return Um PontoConsumoDataConsumoTesteExcedido
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	private PontoConsumoDataConsumoTesteExcedido obterPontoConsumoDataConsumoTesteExcedido(ContratoPontoConsumo contratoPontoConsumo,
			Date dataInicial, Date dataFinal) throws NegocioException {

		Date dataConsumoTesteExcedido = null;
		BigDecimal consumoUtilizado = BigDecimal.ZERO;
		BigDecimal totalConsumoApurado = BigDecimal.ZERO;
		Date dataFinalPeriodoTeste = contratoPontoConsumo.getDataFimTeste();
		BigDecimal consumoTeste = contratoPontoConsumo.getMedidaConsumoTeste();

		// Se o período de teste é definido APENAS
		// pela data.
		if (dataFinalPeriodoTeste != null && consumoTeste == null) {

			// Se excedeu o periodo de teste.
			if (dataFinalPeriodoTeste.compareTo(dataFinal) <= 0) {
				dataConsumoTesteExcedido = dataFinalPeriodoTeste;
			}

			// Se o período de teste é definido
			// pelo volume.
		} else if (consumoTeste != null) {

			// Se for definido pelo volume E data.
			// Caso tenha excedido o perido pela
			// data, não precisa ver o volume.
			if (dataFinalPeriodoTeste != null && dataFinalPeriodoTeste.compareTo(dataFinal) < 0) {
				dataConsumoTesteExcedido = dataFinalPeriodoTeste;
			}

			if (dataConsumoTesteExcedido == null) {

				StringBuilder hql = new StringBuilder();

				boolean indicadorConsumoCiclo = false;
				if (contratoPontoConsumo.getTipoMedicao().getChavePrimaria() == TipoMedicao.CODIGO_PERIODICA) {
					indicadorConsumoCiclo = true;
				}

				hql.append(" select historicoMedicao.dataLeituraInformada, historicoConsumo.consumoApurado ");
				hql.append(" FROM ");
				hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
				hql.append(" historicoConsumo ");
				hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
				hql.append(" WHERE ");
				hql.append(" historicoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
				hql.append(" and historicoConsumo.habilitado = true ");
				hql.append(" and historicoMedicao.dataLeituraInformada  between :dataInicial and :dataFinal ");
				hql.append(" and historicoConsumo.indicadorConsumoCiclo = :indicadorConsumoCiclo ");

				Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
				query.setLong("chavePontoConsumo", contratoPontoConsumo.getPontoConsumo().getChavePrimaria());
				Util.adicionarRestricaoDataSemHora(query, dataInicial, "dataInicial", Boolean.TRUE);
				Util.adicionarRestricaoDataSemHora(query, dataFinal, "dataFinal", Boolean.FALSE);
				query.setBoolean("indicadorConsumoCiclo", indicadorConsumoCiclo);

				Collection<Object[]> resultado = query.list();

				// Se algum consumo de teste já
				// foi utilizado, subtrair do
				// consumo de teste total.
				if (contratoPontoConsumo.getMedidaConsumoTesteUsado() != null) {
					consumoUtilizado = contratoPontoConsumo.getMedidaConsumoTesteUsado();
					consumoTeste = consumoTeste.subtract(consumoUtilizado);
				}

				// Verifica o retorno da consulta.
				if (resultado != null && !resultado.isEmpty()) {
					Date dataLeituraInformada = null;
					BigDecimal consumoApurado = BigDecimal.ZERO;

					for (Object[] objeto : resultado) {
						dataLeituraInformada = (Date) objeto[0];
						consumoApurado = (BigDecimal) objeto[1];
						totalConsumoApurado = totalConsumoApurado.add(consumoApurado);

						// Se excedeu o consumo
						// reservado para testes..
						if ((totalConsumoApurado.compareTo(consumoTeste) >= 0)
								|| (dataFinalPeriodoTeste != null && dataLeituraInformada.compareTo(dataFinalPeriodoTeste) > 0)) {
							dataConsumoTesteExcedido = dataLeituraInformada;
							break;
						}
					}
				}
			}
		}

		PontoConsumoDataConsumoTesteExcedido pontoConsumoDataConsumoTesteExcedido = (PontoConsumoDataConsumoTesteExcedido) ServiceLocator
				.getInstancia().getBeanPorID(PontoConsumoDataConsumoTesteExcedido.BEAN_ID_PONTO_CONSUMO_DATA_TESTE_EXCEDIDO);

		pontoConsumoDataConsumoTesteExcedido.setData(dataConsumoTesteExcedido);
		pontoConsumoDataConsumoTesteExcedido.setConsumoExcedido(totalConsumoApurado);

		return pontoConsumoDataConsumoTesteExcedido;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade. ControladorPenalidade # obterConsumoDistribuidoPorModalidade(br.com.
	 * ggas.contrato.contrato .Contrato, br.com.ggas.cadastro.imovel.PontoConsumo, java.util.Date, java.util.Date)
	 */
	@Override
	public ApuracaoVO obterConsumoDistribuidoPorModalidade(Contrato contrato, PontoConsumo pontoConsumo, String tipoApuracao,
			Long idPenalidade, Long idPeriodicidade, Date dataInicial, Date dataFinal, StringBuilder logProcessamento)
			throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		String idPenalidadeToP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		String idPenalidadeSoP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		List<ContratoPenalidade> listaContratoPenalidadeRetirada =
				controladorContrato.consultarContratoPenalidade(contrato, Long.valueOf(codPenalidadeRetiradaMaior));

		// se tipo de apuração for FATURAMENTO, é
		// necessário receber a
		// periodicidade MENSAL ou ANUAL.
		Date dataInicioApuracao = dataInicial;
		Date dataFinalApuracao = dataFinal;

		this.validarPenalidadesContrato(contrato, dataInicioApuracao);

		String codigoAgrupamentoVolume =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME);
		boolean isAgrupadoVolume = Boolean.FALSE;
		if (contrato != null && contrato.getTipoAgrupamento() != null
				&& contrato.getTipoAgrupamento().getChavePrimaria() == Long.parseLong(codigoAgrupamentoVolume)) {
			isAgrupadoVolume = Boolean.TRUE;
		}

		// criação do VO de apurações
		ApuracaoVO apuracaoVO = new ApuracaoVO();

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade =
				new ArrayList<>();
		apuracaoVO.setListaApuracaoQuantidadePenalidadePeriodicidade(listaApuracaoQuantidadePenalidadePeriodicidade);

		Collection<Recuperacao> listaRecuperacao = new ArrayList<>();
		apuracaoVO.setListaRecuperacao(listaRecuperacao);

		Collection<ApuracaoQuantidade> listaApuracaoQuantidade = new ArrayList<>();
		apuracaoVO.setListaApuracaoQuantidade(listaApuracaoQuantidade);

		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		ApuracaoPeriodo apuracaoPeriodo =
				(ApuracaoPeriodo) ServiceLocator.getInstancia().getBeanPorID(ApuracaoPeriodo.BEAN_ID_APURACAO_PERIODO);

		// 1.a.b.c Verificando se o contrato está
		// em período de teste
		boolean contratoEmPeriodoTeste = controladorContrato.verificarContratoPeriodoTeste(contrato, pontoConsumo, dataInicial, dataFinal);

		if (contratoEmPeriodoTeste) {

			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoPontoConsumo(contrato, pontoConsumo);
			PontoConsumoDataConsumoTesteExcedido pontoConsumoDataConsumoTesteExcedido =
					obterPontoConsumoDataConsumoTesteExcedido(contratoPontoConsumo, dataInicial, dataFinal);

			if (pontoConsumoDataConsumoTesteExcedido != null && pontoConsumoDataConsumoTesteExcedido.getData() != null) {

				logProcessamento.append("        Período de testes do contrato encerrado.");
				logProcessamento.append("\r\n");
				dataInicioApuracao = Util.incrementarDia(pontoConsumoDataConsumoTesteExcedido.getData(), 1);
				apuracaoPeriodo.setDataVolumeTesteAtingido(pontoConsumoDataConsumoTesteExcedido.getData());
				apuracaoPeriodo.setConsumoUsadoPeriodoTeste(pontoConsumoDataConsumoTesteExcedido.getConsumoExcedido());

				// atualizar contrato se
				// FATURAMENTO
				if (tipoApuracao.equals(DISTRIBUICAO_FATURAMENTO)) {
					contratoPontoConsumo.setDataFimTeste(apuracaoPeriodo.getDataVolumeTesteAtingido());
					contratoPontoConsumo.setDataConsumoTesteExcedido(apuracaoPeriodo.getDataVolumeTesteAtingido());
				}
			} else {
				logProcessamento.append("        Contrato em período de testes.");
				logProcessamento.append("\r\n");
				return null;
			}
		}

		// 3. Identifica as modalidades existentes
		// no contrato
		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades =
				controladorContrato.consultarContratoPontoConsumoModalidades(contrato, pontoConsumo);

		Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade = new LinkedList<>();

		Collection<PontoConsumo> colecaoPontosConsumo = new ArrayList<>();

		if (isAgrupadoVolume && contrato.getListaContratoPontoConsumo() != null && !contrato.getListaContratoPontoConsumo().isEmpty()) {
			for (ContratoPontoConsumo contPontCons : contrato.getListaContratoPontoConsumo()) {
				colecaoPontosConsumo.add(contPontCons.getPontoConsumo());
			}
		} else {
			colecaoPontosConsumo.add(pontoConsumo);
		}

		Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidadesAgrupado = null;
		if (isAgrupadoVolume) {
			contratoPontoConsumoModalidadesAgrupado = controladorContrato.consultarContratoPontoConsumoModalidades(contrato, null);
		} else {
			contratoPontoConsumoModalidadesAgrupado = contratoPontoConsumoModalidades;
		}

		Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo =
				controladorContrato.agruparContratoPontoConsumoModalidadePorPontoConsumo(contratoPontoConsumoModalidades);

		MultiKeyMap mapaValorQDPPorPontoConsumo =
				controladorProgramacao.consultarValorQDPDiarioPorPontoConsumo(new ArrayList<>(mapaModalidadesPorPontoConsumo.keySet()));

		BigDecimal calculoQDPMedia = this.calcularQDPMedia(mapaModalidadesPorPontoConsumo, mapaValorQDPPorPontoConsumo, dataInicioApuracao);

		apuracaoVO.setqDPMediaContratual(calculoQDPMedia);
		BigDecimal saldoQDPMedia = calculoQDPMedia;

		BigDecimal calculoQDCMediaContratual =
				controladorContrato.calcularQDCMedia(contratoPontoConsumoModalidadesAgrupado, dataInicioApuracao, dataFinalApuracao);

		apuracaoVO.setqDCMediaContratual(calculoQDCMediaContratual);

		BigDecimal saldoQDP = BigDecimal.ZERO;
		DateTime dtInicio = new DateTime(dataInicioApuracao);
		DateTime dtFim = new DateTime(dataFinalApuracao);
		DateTime dtAtual = dtInicio;
		while (dtAtual.compareTo(dtFim) <= 0) {
			BigDecimal qdpDiaria = this.obterValorQDP(mapaModalidadesPorPontoConsumo, mapaValorQDPPorPontoConsumo, dtAtual.toDate());
			saldoQDP = saldoQDP.add(qdpDiaria);
			dtAtual = dtAtual.plusDays(1);
		}

		Boolean existeContratoPontoConsumoPenalidade = Boolean.FALSE;

		// 4. Para cada modalidade existente no
		// contrato, indentificar as
		// penalidades e suas configurações
		Iterator<ContratoPontoConsumoModalidade> itModalidades = contratoPontoConsumoModalidades.iterator();
		ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = null;
		while (itModalidades.hasNext()) {

			contratoPontoConsumoModalidade = itModalidades.next();

			ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade =
					criarApuracaoQuantidadePenalidadePeriodicidade();

			// Criar uma
			// ConsumoDistribuidoModalidade para
			// cada modalidade
			ConsumoDistribuidoModalidade consumoDistribuidoModalidade = (ConsumoDistribuidoModalidade) ServiceLocator.getInstancia()
					.getBeanPorID(ConsumoDistribuidoModalidade.BEAN_ID_CONSUMO_DISTRIBUIDO_MODALIDADE);

			consumoDistribuidoModalidade.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);

			// Calcula o Período da Periodicidade
			EntidadeConteudo periodicidade = null;

			if (idPeriodicidade != null) {
				periodicidade = controladorEntidadeConteudo.obter(idPeriodicidade);
			}

			Calendar calendar = new GregorianCalendar();
			int menorDiaPeriodo = 0;
			int maiorDiaPeriodo = 0;

			// Periocidades Quinzenal, Semanal ou
			// Mensal
			if (periodicidade == null || periodicidade.getChavePrimaria() != EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

				calendar.setTime(dataInicioApuracao);

				menorDiaPeriodo = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
				maiorDiaPeriodo = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

			} else {
				// Periodicidade Anual

				calendar.setTime(dataInicioApuracao);

				menorDiaPeriodo = calendar.getActualMinimum(Calendar.DAY_OF_YEAR);
				maiorDiaPeriodo = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
			}

			calendar.set(Calendar.DATE, menorDiaPeriodo);
			Date primeiroDiaPeriodo = calendar.getTime();

			calendar.set(Calendar.DATE, maiorDiaPeriodo);
			Date ultimoDiaPeriodo = calendar.getTime();

			Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade =
					new ArrayList<>();
			for (ContratoPontoConsumoModalidade contPontoCons : contratoPontoConsumoModalidadesAgrupado) {
				if (contPontoCons.getContratoModalidade().getChavePrimaria() == contratoPontoConsumoModalidade.getContratoModalidade()
						.getChavePrimaria()) {
					listaContratoPontoConsumoModalidade.add(contPontoCons);
				}
			}

			// Calcular a QDC média
			BigDecimal calculoQDCMedia =
					controladorContrato.calcularQDCMedia(listaContratoPontoConsumoModalidade, primeiroDiaPeriodo, ultimoDiaPeriodo);
			consumoDistribuidoModalidade.setVolumeQDCMedia(calculoQDCMedia);

			BigDecimal somaQDCs = controladorContrato.consultarContratoPontoConsumoModalidadeQDCPorModalidade(
					listaContratoPontoConsumoModalidade, dataInicioApuracao, dataFinalApuracao);
			consumoDistribuidoModalidade.setVolumeQDC(somaQDCs);

			if (somaQDCs == null || somaQDCs.compareTo(BigDecimal.ZERO)==0) {

				// Calcular a QDC do contrato
				somaQDCs = controladorContrato.obterSomaQdcContratoPorPeriodo(contrato.getChavePrimaria(), primeiroDiaPeriodo,
						ultimoDiaPeriodo);
				consumoDistribuidoModalidade.setVolumeQDC(somaQDCs);

				Integer quantidadeDias = Util.quantidadeDiasIntervalo(primeiroDiaPeriodo, ultimoDiaPeriodo);

				calculoQDCMedia = null;
				if (somaQDCs != null && somaQDCs.compareTo(BigDecimal.ZERO) > 0 && quantidadeDias != null
						&& quantidadeDias.compareTo(0) > 0) {
					calculoQDCMedia = somaQDCs.divide(BigDecimal.valueOf(quantidadeDias), ESCALA_QDC_MEDIA, RoundingMode.HALF_UP);
				} else {
					calculoQDCMedia = BigDecimal.ZERO;
				}

				consumoDistribuidoModalidade.setVolumeQDCMedia(calculoQDCMedia);
				apuracaoVO.setqDCMediaContratual(calculoQDCMedia);

			}

			// Distribuir a QDP entre as
			// modalidades
			if (saldoQDPMedia.compareTo(calculoQDCMedia) >= 0 && itModalidades.hasNext()) {
				consumoDistribuidoModalidade.setVolumeQDPMedia(calculoQDCMedia);
				saldoQDPMedia = saldoQDPMedia.subtract(calculoQDCMedia);
			} else {
				consumoDistribuidoModalidade.setVolumeQDPMedia(saldoQDPMedia);
				saldoQDPMedia = BigDecimal.ZERO;
			}

			if (somaQDCs != null && saldoQDP.compareTo(somaQDCs) >= 0 && itModalidades.hasNext()) {
				consumoDistribuidoModalidade.setVolumeQDP(somaQDCs);
				saldoQDP = saldoQDP.subtract(somaQDCs);
			} else {
				consumoDistribuidoModalidade.setVolumeQDP(saldoQDP);
				saldoQDP = BigDecimal.ZERO;
			}

			// Armazenando dados no VO
			if (!(contrato.getAgrupamentoCobranca() && isAgrupadoVolume)) {
				apuracaoQuantidadePenalidadePeriodicidade.setPontoConsumo(pontoConsumo);
			}
			apuracaoQuantidadePenalidadePeriodicidade.setQtdaDiariaContratadaMedia(consumoDistribuidoModalidade.getVolumeQDCMedia());
			apuracaoQuantidadePenalidadePeriodicidade.setQtdaDiariaProgramadaMedia(consumoDistribuidoModalidade.getVolumeQDPMedia());
			apuracaoQuantidadePenalidadePeriodicidade.setContratoModalidade(contratoPontoConsumoModalidade.getContratoModalidade());

			// Identificar as penalidades e
			// configuracoes Retirada Maior
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeRetiradaMaiorMenorPeriodicidade =
					this.obterPenalidadeComMenorPeriodicidade(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(),
							codPenalidadeRetiradaMaior, dataInicioApuracao, dataFinalApuracao);
			if (contratoPontoConsumoPenalidadeRetiradaMaiorMenorPeriodicidade != null) {
				consumoDistribuidoModalidade
						.setPenalidadeRetiradaMaiorComMenorPeriodicidade(contratoPontoConsumoPenalidadeRetiradaMaiorMenorPeriodicidade);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeRetiradaMaiorMaiorPeriodicidade =
					this.obterPenalidadeComMaiorPeriodicidade(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(),
							codPenalidadeRetiradaMaior, dataInicioApuracao, dataFinalApuracao);

			if (contratoPontoConsumoPenalidadeRetiradaMaiorMaiorPeriodicidade != null) {
				consumoDistribuidoModalidade
						.setPenalidadeRetiradaMaiorComMaiorPeriodicidade(contratoPontoConsumoPenalidadeRetiradaMaiorMaiorPeriodicidade);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			// Identificar as penalidades e
			// configuracoes Retirada Menor
			String codPenalidadeRetiradaMenor =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeRetiradaMenorMenorPeriodicidade =
					this.obterPenalidadeComMenorPeriodicidade(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(),
							codPenalidadeRetiradaMenor, dataInicioApuracao, dataFinalApuracao);
			if (contratoPontoConsumoPenalidadeRetiradaMenorMenorPeriodicidade != null) {
				consumoDistribuidoModalidade
						.setPenalidadeRetiradaMenorComMenorPeriodicidade(contratoPontoConsumoPenalidadeRetiradaMenorMenorPeriodicidade);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeRetiradaMenorMaiorPeriodicidade =
					this.obterPenalidadeComMaiorPeriodicidade(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(),
							codPenalidadeRetiradaMenor, dataInicioApuracao, dataFinalApuracao);

			if (contratoPontoConsumoPenalidadeRetiradaMenorMaiorPeriodicidade != null) {
				consumoDistribuidoModalidade
						.setPenalidadeRetiradaMenorComMaiorPeriodicidade(contratoPontoConsumoPenalidadeRetiradaMenorMaiorPeriodicidade);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			// Identificar as penalidades e
			// configuracoes TOP
			String codPenalidadeToP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeToPMenor =
					this.obterPenalidadeComMenorPeriodicidade(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(),
							codPenalidadeToP, dataInicioApuracao, dataFinalApuracao);

			if (contratoPontoConsumoPenalidadeToPMenor != null) {
				consumoDistribuidoModalidade.setPenalidadeToPComMenorPeriodicidade(contratoPontoConsumoPenalidadeToPMenor);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeToPMaior =
					this.obterPenalidadeComMaiorPeriodicidade(contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(),
							codPenalidadeToP, dataInicioApuracao, dataFinalApuracao);

			if (contratoPontoConsumoPenalidadeToPMaior != null) {
				consumoDistribuidoModalidade.setPenalidadeToPComMaiorPeriodicidade(contratoPontoConsumoPenalidadeToPMaior);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			// Identificar as penalidades e
			// configuracoes SOP
			String codPenalidadeSoP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);
			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeSoPMenor = this.obterPenalidadeComMenorPeriodicidade(
					contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(), codPenalidadeSoP, null, null);
			if (contratoPontoConsumoPenalidadeSoPMenor != null) {
				consumoDistribuidoModalidade.setPenalidadeSoPComMenorPeriodicidade(contratoPontoConsumoPenalidadeSoPMenor);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeSoPMaior = this.obterPenalidadeComMaiorPeriodicidade(
					contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade(), codPenalidadeSoP, null, null);

			if (contratoPontoConsumoPenalidadeSoPMaior != null) {
				consumoDistribuidoModalidade.setPenalidadeSoPComMenorPeriodicidade(contratoPontoConsumoPenalidadeSoPMaior);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			// Periodicidade ToP e SoP usada ao
			// atualizar informações das
			// penalidades.

			ContratoPontoConsumoPenalidade penalidadePeriodicidadeToP =
					controladorContrato.obterContratoPontoConsumoPenalidade(contratoPontoConsumoModalidade.getChavePrimaria(),
							Long.valueOf(codPenalidadeToP), idPeriodicidade, dataInicioApuracao, dataFinalApuracao);
			ContratoPontoConsumoPenalidade penalidadePeriodicidadeSoP = controladorContrato.obterContratoPontoConsumoPenalidade(
					contratoPontoConsumoModalidade.getChavePrimaria(), Long.valueOf(codPenalidadeSoP), idPeriodicidade, null, null);

			if (penalidadePeriodicidadeToP != null) {
				consumoDistribuidoModalidade.setPenalidadePeriodicidadeToP(penalidadePeriodicidadeToP);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}
			if (penalidadePeriodicidadeSoP != null) {
				consumoDistribuidoModalidade.setPenalidadePeriodicidadeSoP(penalidadePeriodicidadeSoP);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			// implementação de novos campos solicitados pela PbGás
			// se não existir penalidade ToP cadastrada por ponto consumo modalidade, usar a penalidade ToP do contrato
			Boolean existePenalidadeModalidade = false;

			if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP() != null
					|| consumoDistribuidoModalidade.getPenalidadeToPComMaiorPeriodicidade() != null
					|| consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade() != null
					|| consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMaior() != null
					|| consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMenor() != null
					|| consumoDistribuidoModalidade.getPenalidadeRetiradaMaiorComMaiorPeriodicidade() != null
					|| consumoDistribuidoModalidade.getPenalidadeRetiradaMaiorComMenorPeriodicidade() != null
					|| consumoDistribuidoModalidade.getPenalidadeRetiradaMenorComMaiorPeriodicidade() != null
					|| consumoDistribuidoModalidade.getPenalidadeRetiradaMenorComMenorPeriodicidade() != null) {
				existePenalidadeModalidade = true;
			}

			if (!existePenalidadeModalidade) {

				// Identificar as penalidades e configuracoes TOP
				ContratoPenalidade penalidadePeriodicidadeContratoToP =
						controladorContrato.obterContratoPenalidade(contrato.getChavePrimaria(), Long.valueOf(codPenalidadeToP),
								idPeriodicidade, dataInicioApuracao, dataFinalApuracao);

				if (penalidadePeriodicidadeContratoToP != null) {
					consumoDistribuidoModalidade.setPenalidadePeriodicidadeToP(
							this.converterPenalidadeContratoPontoConsumoPenalidade(penalidadePeriodicidadeContratoToP));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade contratoPenalidadeToPMenor = this.obterContratoPenalidadeComMenorPeriodicidade(
						contrato.getListaContratoPenalidade(), codPenalidadeToP, dataInicioApuracao, dataFinalApuracao);

				if (contratoPenalidadeToPMenor != null) {
					consumoDistribuidoModalidade.setPenalidadeToPComMenorPeriodicidade(
							this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeToPMenor));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade contratoPenalidadeToPMaior = this.obterContratoPenalidadeComMaiorPeriodicidade(
						contrato.getListaContratoPenalidade(), codPenalidadeToP, dataInicioApuracao, dataFinalApuracao);

				if (contratoPenalidadeToPMaior != null) {
					consumoDistribuidoModalidade.setPenalidadeToPComMaiorPeriodicidade(
							this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeToPMaior));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				// Identificar as penalidades e configuracoes Retirada maior/menor

				ContratoPenalidade penalidadePeriodicidadeRetiradaMaior =
						controladorContrato.obterContratoPenalidade(contrato.getChavePrimaria(), Long.valueOf(codPenalidadeRetiradaMaior),
								idPeriodicidade, dataInicioApuracao, dataFinalApuracao);

				if (penalidadePeriodicidadeRetiradaMaior != null) {
					consumoDistribuidoModalidade.setPenalidadePeriodicidadeRetiradaMaior(
							this.converterPenalidadeContratoPontoConsumoPenalidade(penalidadePeriodicidadeRetiradaMaior));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade penalidadePeriodicidadeRetiradaMenor =
						controladorContrato.obterContratoPenalidade(contrato.getChavePrimaria(), Long.valueOf(codPenalidadeRetiradaMenor),
								idPeriodicidade, dataInicioApuracao, dataFinalApuracao);

				if (penalidadePeriodicidadeRetiradaMenor != null) {
					consumoDistribuidoModalidade.setPenalidadePeriodicidadeRetiradaMaior(
							this.converterPenalidadeContratoPontoConsumoPenalidade(penalidadePeriodicidadeRetiradaMenor));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade contratoPenalidadeRetiradaMaiorComMaiorPeriodicidade = this.obterContratoPenalidadeComMaiorPeriodicidade(
						contrato.getListaContratoPenalidade(), codPenalidadeRetiradaMaior, dataInicioApuracao, dataFinalApuracao);
				if (contratoPenalidadeRetiradaMaiorComMaiorPeriodicidade != null) {
					consumoDistribuidoModalidade.setPenalidadeRetiradaMaiorComMaiorPeriodicidade(
							this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeRetiradaMaiorComMaiorPeriodicidade));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade contratoPenalidadeRetiradaMaiorComMenorPeriodicidade = this.obterContratoPenalidadeComMenorPeriodicidade(
						contrato.getListaContratoPenalidade(), codPenalidadeRetiradaMaior, dataInicioApuracao, dataFinalApuracao);
				if (contratoPenalidadeRetiradaMaiorComMenorPeriodicidade != null) {
					consumoDistribuidoModalidade.setPenalidadeRetiradaMaiorComMenorPeriodicidade(
							this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeRetiradaMaiorComMenorPeriodicidade));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade contratoPenalidadeRetiradaMenorComMaiorPeriodicidade = this.obterContratoPenalidadeComMaiorPeriodicidade(
						contrato.getListaContratoPenalidade(), codPenalidadeRetiradaMenor, dataInicioApuracao, dataFinalApuracao);
				if (contratoPenalidadeRetiradaMenorComMaiorPeriodicidade != null) {
					consumoDistribuidoModalidade.setPenalidadeRetiradaMenorComMaiorPeriodicidade(
							this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeRetiradaMenorComMaiorPeriodicidade));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

				ContratoPenalidade contratoPenalidadeRetiradaMenorComMenorPeriodicidade = this.obterContratoPenalidadeComMaiorPeriodicidade(
						contrato.getListaContratoPenalidade(), codPenalidadeRetiradaMenor, dataInicioApuracao, dataFinalApuracao);
				if (contratoPenalidadeRetiradaMenorComMenorPeriodicidade != null) {
					consumoDistribuidoModalidade.setPenalidadeRetiradaMenorComMenorPeriodicidade(
							this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeRetiradaMenorComMenorPeriodicidade));
					existeContratoPontoConsumoPenalidade = Boolean.TRUE;
				}

			}

			// Periodicidade Retirada Maior/Menor

			Map<String, Object> params = new HashMap<>();
			params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));
			params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicioApuracao);
			params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataFinalApuracao);
			params.put(PARAM_NAME_ID_CONTRATO_MODALIDADE, contratoPontoConsumoModalidade.getChavePrimaria());

			ContratoPontoConsumoPenalidade penalidadeRetiradaMaior =
					controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);

			params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));
			ContratoPontoConsumoPenalidade penalidadeRetiradaMenor =
					controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);

			if (penalidadeRetiradaMaior != null) {
				consumoDistribuidoModalidade.setPenalidadePeriodicidadeRetiradaMaior(penalidadeRetiradaMaior);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}
			if (penalidadeRetiradaMenor != null) {
				consumoDistribuidoModalidade.setPenalidadePeriodicidadeRetiradaMenor(penalidadeRetiradaMenor);
				existeContratoPontoConsumoPenalidade = Boolean.TRUE;
			}

			// Retirada automatica
			consumoDistribuidoModalidade.setRetiradaAutomatica(contratoPontoConsumoModalidade.getIndicadorRetiradaAutomatica());

			listaConsumoDistribuidoModalidade.add(consumoDistribuidoModalidade);

			apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQuantidadePenalidadePeriodicidade);
		}

		// se não existir ContratoPontoConsumoPenalidade cadastrado, e apenas penalidade do contrato
		if (!existeContratoPontoConsumoPenalidade && contrato.getListaContratoPenalidade() != null
				&& !contrato.getListaContratoPenalidade().isEmpty()) {

			ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade =
					criarApuracaoQuantidadePenalidadePeriodicidade();

			// Criar uma
			// ConsumoDistribuidoModalidade para
			// cada modalidade
			ConsumoDistribuidoModalidade consumoDistribuidoModalidade = (ConsumoDistribuidoModalidade) ServiceLocator.getInstancia()
					.getBeanPorID(ConsumoDistribuidoModalidade.BEAN_ID_CONSUMO_DISTRIBUIDO_MODALIDADE);

			consumoDistribuidoModalidade.setContratoPontoConsumoModalidade(null);

			// Calcula o Período da Periodicidade
			EntidadeConteudo periodicidade = null;

			if (idPeriodicidade != null) {
				periodicidade = controladorEntidadeConteudo.obter(idPeriodicidade);
			}

			Calendar calendar = new GregorianCalendar();
			int menorDiaPeriodo = 0;
			int maiorDiaPeriodo = 0;

			// Periocidades Quinzenal, Semanal ou
			// Mensal
			if (periodicidade == null || periodicidade.getChavePrimaria() != EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

				calendar.setTime(dataInicioApuracao);

				menorDiaPeriodo = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
				maiorDiaPeriodo = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

			} else {
				// Periodicidade Anual

				calendar.setTime(dataInicioApuracao);

				menorDiaPeriodo = calendar.getActualMinimum(Calendar.DAY_OF_YEAR);
				maiorDiaPeriodo = calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
			}

			calendar.set(Calendar.DATE, menorDiaPeriodo);
			Date primeiroDiaPeriodo = calendar.getTime();

			calendar.set(Calendar.DATE, maiorDiaPeriodo);
			Date ultimoDiaPeriodo = calendar.getTime();

			// Calcular a QDC do contrato
			BigDecimal somaQDCs =
					controladorContrato.obterSomaQdcContratoPorPeriodo(contrato.getChavePrimaria(), primeiroDiaPeriodo, ultimoDiaPeriodo);
			consumoDistribuidoModalidade.setVolumeQDC(somaQDCs);

			Integer quantidadeDias = Util.quantidadeDiasIntervalo(primeiroDiaPeriodo, ultimoDiaPeriodo);

			BigDecimal calculoQDCMedia = null;
			if (somaQDCs != null && somaQDCs.compareTo(BigDecimal.ZERO) > 0 && quantidadeDias != null && quantidadeDias.compareTo(0) > 0) {
				calculoQDCMedia = somaQDCs.divide(BigDecimal.valueOf(quantidadeDias), ESCALA_QDC_MEDIA, RoundingMode.HALF_UP);
			} else {
				calculoQDCMedia = BigDecimal.ZERO;
			}
			consumoDistribuidoModalidade.setVolumeQDCMedia(calculoQDCMedia);

			calculoQDPMedia = calculoQDCMedia;
			apuracaoVO.setqDPMediaContratual(calculoQDPMedia);
			saldoQDPMedia = calculoQDPMedia;

			calculoQDCMediaContratual = calculoQDCMedia;
			apuracaoVO.setqDCMediaContratual(calculoQDCMediaContratual);

			saldoQDP = somaQDCs;

			// Distribuir a QDP entre as
			// modalidades
			if (itModalidades.hasNext()) {
				consumoDistribuidoModalidade.setVolumeQDPMedia(calculoQDCMedia);
				saldoQDPMedia = saldoQDPMedia.subtract(calculoQDCMedia);
			} else {
				consumoDistribuidoModalidade.setVolumeQDPMedia(saldoQDPMedia);
				saldoQDPMedia = BigDecimal.ZERO;
			}

			if (itModalidades.hasNext()) {
				consumoDistribuidoModalidade.setVolumeQDP(somaQDCs);
				saldoQDP = saldoQDP.subtract(somaQDCs);
			} else {
				consumoDistribuidoModalidade.setVolumeQDP(saldoQDP);
				saldoQDP = BigDecimal.ZERO;
			}

			// Armazenando dados no VO
			if (!(contrato.getAgrupamentoCobranca() && isAgrupadoVolume)) {
				apuracaoQuantidadePenalidadePeriodicidade.setPontoConsumo(pontoConsumo);
			}
			apuracaoQuantidadePenalidadePeriodicidade.setQtdaDiariaContratadaMedia(consumoDistribuidoModalidade.getVolumeQDCMedia());
			apuracaoQuantidadePenalidadePeriodicidade.setQtdaDiariaProgramadaMedia(consumoDistribuidoModalidade.getVolumeQDPMedia());
			apuracaoQuantidadePenalidadePeriodicidade.setContratoModalidade(null);

			// Identificar as penalidades e
			// configuracoes TOP
			String codPenalidadeToP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

			ContratoPenalidade penalidadePeriodicidadeToP = controladorContrato.obterContratoPenalidade(contrato.getChavePrimaria(),
					Long.valueOf(codPenalidadeToP), idPeriodicidade, dataInicioApuracao, dataFinalApuracao);

			if (penalidadePeriodicidadeToP != null) {
				consumoDistribuidoModalidade
						.setPenalidadePeriodicidadeToP(this.converterPenalidadeContratoPontoConsumoPenalidade(penalidadePeriodicidadeToP));
			}

			// Identificar as penalidades e configuracoes TOP
			ContratoPenalidade contratoPenalidadeToPMenor = this.obterContratoPenalidadeComMenorPeriodicidade(
					contrato.getListaContratoPenalidade(), codPenalidadeToP, dataInicioApuracao, dataFinalApuracao);

			if (contratoPenalidadeToPMenor != null) {
				consumoDistribuidoModalidade.setPenalidadeToPComMenorPeriodicidade(
						this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeToPMenor));
			}

			ContratoPenalidade contratoPenalidadeToPMaior = this.obterContratoPenalidadeComMaiorPeriodicidade(
					contrato.getListaContratoPenalidade(), codPenalidadeToP, dataInicioApuracao, dataFinalApuracao);

			if (contratoPenalidadeToPMaior != null) {
				consumoDistribuidoModalidade.setPenalidadeToPComMaiorPeriodicidade(
						this.converterPenalidadeContratoPontoConsumoPenalidade(contratoPenalidadeToPMaior));
			}

			// Retirada automatica
			consumoDistribuidoModalidade.setRetiradaAutomatica(null);
			listaConsumoDistribuidoModalidade.add(consumoDistribuidoModalidade);
			apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQuantidadePenalidadePeriodicidade);
		}

		Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP = new HashSet<>();

		// Verifica se possui faturamento agrupado
		// por valor - mais de um ponto
		// de consumo
		EntidadeConteudo tipoAgrupamentoValor =
				controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_AGRUPAMENTO_POR_VALOR);
		Collection<PontoConsumo> listaPontoConsumoAgrupadosValor =
				controladorPontoConsumo.listarPontoConsumoFaturamentoAgrupado(contrato.getChavePrimaria(), tipoAgrupamentoValor);

		Integer anoMesAtual = Util.obterAnoMes(Calendar.getInstance().getTime());
		Collection<Date> diasFornecimento = Util.obterDatasIntermediarias(dataInicioApuracao, dataFinalApuracao);
		String codigoTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		Map<ConsumoDistribuidoModalidade, BigDecimal> volRetiradosPorModalidadeDia =
				new HashMap<>();
		BigDecimal valorARecuperar = null;

		if (listaPontoConsumoAgrupadosValor.size() > 1) {
			// Se
			// existe
			// faturamento
			// agrupado
			// por
			// Valor

			for (PontoConsumo pontoConsumoAgrupadoValor : listaPontoConsumoAgrupadosValor) {
				try {
					valorARecuperar = controladorProgramacao.obterSaldoARecuperar(contrato, new Long[] { pontoConsumo.getChavePrimaria() },
							Long.valueOf(codigoTakeOrPay), anoMesAtual, listaAQPP);
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
					// rever esse tratamento
					logProcessamento.append("        Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado.");
					logProcessamento.append("\r\n");
					throw new NegocioException("Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado");
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					// rever esse tratamento
					logProcessamento.append(
							"        Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado." + "\r\n");
					throw new NegocioException("Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado");
				}

				this.distribuirVolumePontoConsumo(pontoConsumoAgrupadoValor, dataInicioApuracao, dataFinalApuracao, apuracaoVO,
						apuracaoPeriodo, listaConsumoDistribuidoModalidade, null, listaContratoPenalidadeRetirada);

				this.calcular(apuracaoVO, listaConsumoDistribuidoModalidade, mapaModalidadesPorPontoConsumo, mapaValorQDPPorPontoConsumo,
						dataInicioApuracao, dataFinalApuracao, apuracaoPeriodo, idPeriodicidade, contrato, pontoConsumoAgrupadoValor);

				List<ConsumoDistribuidoModalidade> listaModalidadeConsumoOrdenada =
						this.obterListaModalidadeConsumoOrdenadaPorPrioridade(listaConsumoDistribuidoModalidade);

				/*
				 * Calcula o valor Retirado para uma dada modalidade no dia, preenche o mapa volRetiradosPorModalidadeDia com esse valor e
				 * retorna o saldo que ainda resta a recuperar
				 */

				valorARecuperar =
						this.calcularValorRetiradoPorModalidadeDia(contrato, listaModalidadeConsumoOrdenada, apuracaoVO.getBalde(),
								volRetiradosPorModalidadeDia, valorARecuperar, diasFornecimento, dataInicioApuracao, dataFinalApuracao);

				apuracaoVO.setMapaVolumeRetiradoPorModalidadeDia(volRetiradosPorModalidadeDia);

				apuracaoVO.setListaAQPP(listaAQPP);

				Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>();
				listaPontoConsumoAux.add(pontoConsumoAgrupadoValor);

				EntidadeConteudo periodicidade = null;

				if (idPeriodicidade != null) {
					periodicidade = controladorEntidadeConteudo.obter(idPeriodicidade);
				}

				if (tipoApuracao.equals(DISTRIBUICAO_APURACAO)) {

					if (this.validarPeriodicidadePenalidade(idPeriodicidade, contrato, idPenalidadeToP)) {
						this.atualizarInformacoesTakeOrPay(apuracaoVO, listaConsumoDistribuidoModalidade, listaPontoConsumoAux, dataInicial,
								dataFinal, contrato);
					}
					if (periodicidade != null
							&& periodicidade.getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL) {
						this.atualizarInformacoesRetiradaMaiorMenor(apuracaoVO, listaConsumoDistribuidoModalidade,
								apuracaoPeriodo.getVolumeQDRAcumulada(), contrato, listaPontoConsumoAux, dataInicial, dataFinal);
						this.atualizarInformacoesGasForaEspecificacao(apuracaoVO, contrato, pontoConsumoAgrupadoValor, dataInicial,
								dataFinalApuracao);
						this.atualizarInformacoesDeliveryOrPay(apuracaoVO, contrato, pontoConsumoAgrupadoValor, dataInicial,
								dataFinalApuracao);
					}
					if (this.validarPeriodicidadePenalidade(idPeriodicidade, contrato, idPenalidadeSoP)) {
						this.atualizarInformacoesShipOrPay(apuracaoVO, listaConsumoDistribuidoModalidade, listaPontoConsumoAux, dataInicial,
								dataFinalApuracao);
					}
				}

				apuracaoVO.getListaRecuperacao()
						.addAll(montarRecuperacao(apuracaoVO, dataInicioApuracao, dataFinalApuracao));
			}
		} else {

			// Verifica se possui faturamento
			// agrupado por volume - mais de um
			// ponto de consumo
			EntidadeConteudo tipoAgrupamentoVolume =
					controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_AGRUPAMENTO_POR_VOLUME);
			Collection<PontoConsumo> listaPontoConsumoAgrupadosVolume =
					controladorPontoConsumo.listarPontoConsumoFaturamentoAgrupado(contrato.getChavePrimaria(), tipoAgrupamentoVolume);

			try {
				Long[] chavePontosConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumoAgrupadosVolume);
				if (chavePontosConsumo.length == 0) {
					chavePontosConsumo = new Long[] { pontoConsumo.getChavePrimaria() };
				}
				valorARecuperar = controladorProgramacao.obterSaldoARecuperar(contrato, chavePontosConsumo, Long.valueOf(codigoTakeOrPay),
						anoMesAtual, listaAQPP);
			} catch (NumberFormatException e) {
				LOG.error(e.getMessage(), e);
				// rever esse tratamento
				logProcessamento.append(
						"        Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado." + "\r\n");
				throw new NegocioException("Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado");
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				// rever esse tratamento
				logProcessamento.append(
						"        Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado." + "\r\n");
				throw new NegocioException("Não foi possível obter o valor a recuperar para o ponto de consumo no ano/mês informado");
			}

			this.distribuirVolumePontoConsumo(pontoConsumo, dataInicioApuracao, dataFinalApuracao, apuracaoVO, apuracaoPeriodo,
					listaConsumoDistribuidoModalidade, listaPontoConsumoAgrupadosVolume, listaContratoPenalidadeRetirada);

			this.calcular(apuracaoVO, listaConsumoDistribuidoModalidade, mapaModalidadesPorPontoConsumo, mapaValorQDPPorPontoConsumo,
					dataInicioApuracao, dataFinalApuracao, apuracaoPeriodo, idPeriodicidade, contrato, pontoConsumo);

			List<ConsumoDistribuidoModalidade> listaModalidadeConsumoOrdenada =
					this.obterListaModalidadeConsumoOrdenadaPorPrioridade(listaConsumoDistribuidoModalidade);

			valorARecuperar = calcularValorRetiradoPorModalidadeDia(contrato, listaModalidadeConsumoOrdenada, apuracaoVO.getBalde(),
					volRetiradosPorModalidadeDia, valorARecuperar, diasFornecimento, dataInicioApuracao, dataFinalApuracao);

			apuracaoVO.setMapaVolumeRetiradoPorModalidadeDia(volRetiradosPorModalidadeDia);

			apuracaoVO.setListaAQPP(listaAQPP);

			Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>();
			listaPontoConsumoAux.addAll(listaPontoConsumoAgrupadosVolume);

			if (listaPontoConsumoAux == null || listaPontoConsumoAux.isEmpty()) {
				listaPontoConsumoAux.add(pontoConsumo);
			}

			EntidadeConteudo periodicidade = null;

			if (idPeriodicidade != null) {
				periodicidade = controladorEntidadeConteudo.obter(idPeriodicidade);
			}

			if (tipoApuracao.equals(DISTRIBUICAO_APURACAO)) {

				if (this.validarPeriodicidadePenalidade(idPeriodicidade, contrato, idPenalidadeToP)) {
					this.atualizarInformacoesTakeOrPay(apuracaoVO, listaConsumoDistribuidoModalidade, listaPontoConsumoAux, dataInicial,
							dataFinal, contrato);
				}

				if (periodicidade != null && periodicidade.getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL) {
					this.atualizarInformacoesRetiradaMaiorMenor(apuracaoVO, listaConsumoDistribuidoModalidade,
							apuracaoPeriodo.getVolumeQDRAcumulada(), contrato, listaPontoConsumoAux, dataInicial, dataFinalApuracao);
					this.atualizarInformacoesGasForaEspecificacao(apuracaoVO, contrato, pontoConsumo, dataInicial, dataFinalApuracao);
					this.atualizarInformacoesDeliveryOrPay(apuracaoVO, contrato, pontoConsumo, dataInicial, dataFinalApuracao);
				}
				if (this.validarPeriodicidadePenalidade(idPeriodicidade, contrato, idPenalidadeSoP)) {
					this.atualizarInformacoesShipOrPay(apuracaoVO, listaConsumoDistribuidoModalidade, listaPontoConsumoAux, dataInicial,
							dataFinalApuracao);
				}
			}

			apuracaoVO.getListaRecuperacao()
					.addAll(montarRecuperacao(apuracaoVO, dataInicioApuracao, dataFinalApuracao));

		}

		return apuracaoVO;

	}

	private void validarPenalidadesContrato(Contrato contrato, Date dataInicioApuracao) throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
		String codPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		Map<String, Object> params = new HashMap<>();

		params.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));
		params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicioApuracao);
		params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataInicioApuracao);

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));

		ContratoPenalidade contratoPenalidadeRetiradaMenor = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		// Verifica se possui retirada a maior

		if (contratoPenalidadeRetiradaMaior != null && contratoPenalidadeRetiradaMaior.getConsumoReferencia() != null
				&& (contratoPenalidadeRetiradaMaior.getPenalidade() == null
						|| contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor() == null
						|| contratoPenalidadeRetiradaMaior.getBaseApuracao() == null)) {

			throw new ContratoRetiradaAMaiorInconsistenteException();
		}
		//

		// Verifica se possui retirada a menor

		if (contratoPenalidadeRetiradaMenor != null && contratoPenalidadeRetiradaMenor.getConsumoReferencia() != null
				&& (contratoPenalidadeRetiradaMenor.getPenalidade() == null
						|| contratoPenalidadeRetiradaMenor.getValorPercentualRetMaiorMenor() == null
						|| contratoPenalidadeRetiradaMenor.getBaseApuracao() == null)) {

			throw new ContratoRetiradaAMaiorInconsistenteException();
		}

		// Verifica se possui gas fora de
		// especificação
		if (contrato.getPercentualSobreTariGas() != null && contrato.getPercentualSobreTariGas() == null) {
			throw new ContratoGasForaEspecificacaoInconsistenteException();
			//
			// personalizar
			// exceção
		}

		// Verifica se possui Delivery or Pay
		if (contrato.getPercentualTarifaDoP() != null && contrato.getPercentualTarifaDoP() == null) {
			throw new ContratoDeliveryOrPayInconsistenteException();
			//
			// personalizar
			// exceção
		}

		Contrato carregado = controladorContrato.obterContratoListasCarregadas(contrato.getChavePrimaria(), null);
		if (carregado.getListaContratoPontoConsumo() != null) {

			String parametroTakeOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
			String parametroShipOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);
			for (ContratoPontoConsumo cpc : carregado.getListaContratoPontoConsumo()) {
				if (cpc.getListaContratoPontoConsumoModalidade() != null) {
					for (ContratoPontoConsumoModalidade cpcm : cpc.getListaContratoPontoConsumoModalidade()) {
						if (cpcm.getListaContratoPontoConsumoPenalidade() != null) {
							for (ContratoPontoConsumoPenalidade cpcp : cpcm.getListaContratoPontoConsumoPenalidade()) {
								if (cpcp.getPenalidade().getChavePrimaria() == Long.parseLong(parametroTakeOrPay)) {

									if (cpcp.getConsumoReferencia() == null || cpcp.getPeriodicidadePenalidade() == null
											|| cpcp.getPercentualMargemVariacao() == null || cpcp.getReferenciaQFParadaProgramada() == null
											|| cpcp.getDataInicioVigencia() == null || cpcp.getDataFimVigencia() == null
											|| cpcp.getPercentualNaoRecuperavel() == null || cpcp.getConsideraParadaProgramada() == null
											|| cpcp.getConsideraFalhaFornecimento() == null || cpcp.getConsideraCasoFortuito() == null
											|| cpcp.getRecuperavel() == null || cpcp.getTipoApuracao() == null
											|| cpcp.getPrecoCobranca() == null
											|| (cpcp.getConsideraCasoFortuito() != null && cpcp.getConsideraCasoFortuito()
													&& cpcp.getApuracaoCasoFortuito() == null)
											|| (cpcp.getConsideraFalhaFornecimento() != null && cpcp.getConsideraFalhaFornecimento()
													&& cpcp.getApuracaoFalhaFornecimento() == null)
											|| (cpcp.getConsideraParadaProgramada() != null && cpcp.getConsideraParadaProgramada()
													&& cpcp.getApuracaoParadaProgramada() == null)) {

										throw new PenalidadeTakeOrPayException();

									}

								} else if (cpcp.getPenalidade().getChavePrimaria() == Long.parseLong(parametroShipOrPay)
										&& (cpcp.getConsumoReferencia() == null || cpcp.getPeriodicidadePenalidade() == null
												|| cpcp.getPercentualMargemVariacao() == null)) {

									throw new PenalidadeTakeOrPayException();

								}
							}
						}
					}
				}
			}
		}
		if (carregado.getListaContratoPenalidade() != null) {

			String parametroTakeOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

			for (ContratoPenalidade cp : carregado.getListaContratoPenalidade()) {

				if ((cp.getPenalidade().getChavePrimaria() == Long.parseLong(parametroTakeOrPay)) && (cp.getConsumoReferencia() == null

						|| cp.getPeriodicidadePenalidade() == null || cp.getPercentualMargemVariacao() == null
						|| cp.getReferenciaQFParadaProgramada() == null || cp.getDataInicioVigencia() == null
						|| cp.getDataFimVigencia() == null || cp.getPercentualNaoRecuperavel() == null
						|| cp.getConsideraParadaProgramada() == null || cp.getConsideraFalhaFornecimento() == null
						|| cp.getConsideraCasoFortuito() == null || cp.getRecuperavel() == null || cp.getTipoApuracao() == null
						|| cp.getPrecoCobranca() == null
						|| (cp.getConsideraCasoFortuito() != null && cp.getConsideraCasoFortuito() && cp.getApuracaoCasoFortuito() == null)
						|| (cp.getConsideraFalhaFornecimento() != null && cp.getConsideraFalhaFornecimento()
								&& cp.getApuracaoFalhaFornecimento() == null)
						|| (cp.getConsideraParadaProgramada() != null && cp.getConsideraParadaProgramada()
								&& cp.getApuracaoParadaProgramada() == null))) {

					throw new PenalidadeTakeOrPayException();
				}
			}
		}
	}

	/**
	 * Validar periodicidade penalidade.
	 *
	 * @param idPeriodicidade the id periodicidade
	 * @param contrato the contrato
	 * @param codPenalidade the cod penalidade
	 * @return the boolean
	 * @throws NegocioException the negocio exception
	 */
	private Boolean validarPeriodicidadePenalidade(Long idPeriodicidade, Contrato contrato, String codPenalidade) {

		Boolean gerarPenalidade = Boolean.FALSE;

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		Collection<ContratoPontoConsumoPenalidade> listaPenalidades =
				controladorApuracaoPenalidade.obterListaPenalidasPorContrato(contrato.getChavePrimaria());
		for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaPenalidades) {

			if (contratoPontoConsumoPenalidade.getPenalidade().getChavePrimaria() == Long.parseLong(codPenalidade)
					&& idPeriodicidade == contratoPontoConsumoPenalidade.getPeriodicidadePenalidade().getChavePrimaria()) {
				gerarPenalidade = Boolean.TRUE;
				break;
			}

		}

		for (ContratoPenalidade penalidade : contrato.getListaContratoPenalidade()) {

			if (penalidade.getPenalidade().getChavePrimaria() == Long.parseLong(codPenalidade)
					&& idPeriodicidade == penalidade.getPeriodicidadePenalidade().getChavePrimaria()) {
				gerarPenalidade = Boolean.TRUE;
				break;
			}
		}

		return gerarPenalidade;
	}

	/**
	 * Calcular.
	 *
	 * @param apuracaoVO the apuracao vo
	 * @param listaConsumoDistribuidoModalidade the lista consumo distribuido modalidade
	 * @param mapaModalidadesPorPontoConsumo - {@link Map}
	 * @param mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @param apuracaoPeriodo the apuracao periodo
	 * @param idPeriodicidade the id periodicidade
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	private void calcular(ApuracaoVO apuracaoVO, Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade,
			Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo, MultiKeyMap mapaValorQDPPorPontoConsumo,
			Date dataInicioApuracao, Date dataFinalApuracao, ApuracaoPeriodo apuracaoPeriodo, Long idPeriodicidade, Contrato contrato,
			PontoConsumo pontoConsumo) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorPeriodicidadeMensal =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_MENSAL);

		String parametroTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

		Integer anoMes = 0;

		if (idPeriodicidade.equals(Long.valueOf(valorPeriodicidadeMensal))) {

			anoMes = Util.obterAnoMes(dataInicioApuracao);

		} else {
			anoMes = new DateTime(dataInicioApuracao).getYear();
		}

		Collection<Date> datas = Util.obterDatasIntermediarias(dataInicioApuracao, dataFinalApuracao);

		MultiKeyMap balde = apuracaoVO.getBalde();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		BigDecimal qDRAcumulada = BigDecimal.ZERO;

		StringBuilder log = new StringBuilder();

		if (listaConsumoDistribuidoModalidade != null) {
			for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {
				BigDecimal qDRAcumuladaModalidade = BigDecimal.ZERO;

				BigDecimal qL = obterConsumoReferencia(consumoDistribuidoModalidade, parametroTakeOrPay, Boolean.TRUE);

				if (consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade() != null) {
					qL = qL.multiply(consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade().getPercentualMargemVariacao());
				} else {
					qL = BigDecimal.ZERO;
				}

				consumoDistribuidoModalidade.setVolumeQL(qL);

				Collection<ModalidadeDataQuantidade> modalidadeDataQuantidades = new ArrayList<>();
				consumoDistribuidoModalidade.setModalidadeDataQuantidades(modalidadeDataQuantidades);

				for (Date date : datas) {

					BigDecimal valor = (BigDecimal) balde.get(consumoDistribuidoModalidade, date);

					if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
						log.append("\r\n");
						log.append((consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade()
								.getChavePrimaria()));
					} else {
						log.append("\r\n");
						log.append("Sem Modalidade");
						log.append(" - ");
						log.append(format.format(date));
						log.append(" - ");
						log.append(valor);
						log.append(" || ");
					}

					qDRAcumulada = qDRAcumulada.add(valor);
					qDRAcumuladaModalidade = qDRAcumuladaModalidade.add(valor);
				}
				log.append("\r\n *** ");
				consumoDistribuidoModalidade.setVolumeQDR(qDRAcumuladaModalidade);

				if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
					log.append("\r\n QDRAcumulada - modalidade  ");
					log.append((consumoDistribuidoModalidade.getContratoPontoConsumoModalidade()
							.getContratoModalidade().getChavePrimaria()));
				} else {
					log.append("\r\n QDRAcumulada - modalidade  ");
					log.append("Sem Modalidade");
					log.append(" - ");
					log.append(consumoDistribuidoModalidade.getVolumeQDR());
				}

				if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
					log.append("\r\n QL - modalidade  ");
					log.append((consumoDistribuidoModalidade.getContratoPontoConsumoModalidade()
							.getContratoModalidade().getChavePrimaria()));
				} else {
					log.append("\r\n QL - modalidade  " + "Sem Modalidade");
					log.append(" - ");
					log.append(consumoDistribuidoModalidade.getVolumeQL());
				}

			}

			log.append("\r\n QDRAcumulada - ");
			log.append(qDRAcumulada);
			apuracaoPeriodo.setVolumeQDRAcumulada(qDRAcumulada);

			LOG.info(log);

			if (!listaConsumoDistribuidoModalidade.isEmpty()) {
				this.montarListaApuracaoQuantidade(listaConsumoDistribuidoModalidade, mapaModalidadesPorPontoConsumo,
						mapaValorQDPPorPontoConsumo, datas, balde, apuracaoVO, contrato, pontoConsumo);
			}

			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : apuracaoVO
					.getListaApuracaoQuantidadePenalidadePeriodicidade()) {
				apuracaoQuantidadePenalidadePeriodicidade.setDataFimApuracao(dataFinalApuracao);
				apuracaoQuantidadePenalidadePeriodicidade.setDataInicioApuracao(dataInicioApuracao);
				apuracaoQuantidadePenalidadePeriodicidade.setContrato(contrato);
				apuracaoQuantidadePenalidadePeriodicidade.setPontoConsumo(pontoConsumo);

				apuracaoQuantidadePenalidadePeriodicidade.setPeriodoApuracao(anoMes);
			}
		}
	}

	/**
	 * Montar lista apuracao quantidade.
	 *
	 * @param listaConsumoDistribuidoModalidade the lista consumo distribuido modalidade
	 * @param mapaModalidadesPorPontoConsumo - {@link Map}
	 * @param mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 * @param datas the datas
	 * @param balde the balde
	 * @param apuracaoVO the apuracao vo
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	private void montarListaApuracaoQuantidade(Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade,
			Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo, MultiKeyMap mapaValorQDPPorPontoConsumo,
			Collection<Date> datas, MultiKeyMap balde, ApuracaoVO apuracaoVO, Contrato contrato, PontoConsumo pontoConsumo)
			throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Collection<ApuracaoQuantidade> listaApuracaoQuantidade = new ArrayList<>();

		EntidadeConteudo tipoAgrupamentoVolume =
				controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_AGRUPAMENTO_POR_VOLUME);

		Contrato contratoPai = contrato;
		if (contrato.getChavePrimariaPai() != null) {
			contratoPai = (Contrato) controladorContrato.obter(contrato.getChavePrimariaPai());
		}

		BigDecimal qDCPeriodo = BigDecimal.ZERO;
		BigDecimal qDPPeriodo = BigDecimal.ZERO;

		for (Date data : datas) {
			ApuracaoQuantidade apuracaoQuantidade = criarApuracaoQuantidade();
			apuracaoQuantidade.setDataApuracaoVolumes(data);
			BigDecimal valorQDRAcumulado = BigDecimal.ZERO;

			for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {

				valorQDRAcumulado = valorQDRAcumulado.add((BigDecimal) balde.get(consumoDistribuidoModalidade, data));

				consumoDistribuidoModalidade.getModalidadeDataQuantidades();
			}

			apuracaoQuantidade.setQtdaDiariaRetirada(valorQDRAcumulado);
			apuracaoQuantidade.setContrato(contratoPai);

			if ((contrato.getTipoAgrupamento() == null)
					|| (contrato.getTipoAgrupamento().getChavePrimaria() != tipoAgrupamentoVolume.getChavePrimaria())) {
				apuracaoQuantidade.setPontoConsumo(pontoConsumo);
			}

			BigDecimal qDCDiaria = null;

			Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidadesAgrupado = new ArrayList<>();

			for (List<ContratoPontoConsumoModalidade> modalidadesConsumo : mapaModalidadesPorPontoConsumo.values()) {
				contratoPontoConsumoModalidadesAgrupado.addAll(modalidadesConsumo);
			}

			qDCDiaria = controladorContrato.calcularQDCMedia(contratoPontoConsumoModalidadesAgrupado, data, data);
			if (qDCDiaria == null || qDCDiaria.compareTo(BigDecimal.ZERO) == 0) {
				qDCDiaria = controladorContrato.obterQdcContratoValidoPorData(data, contrato.getChavePrimaria());
			}
			apuracaoQuantidade.setQtdaContratada(qDCDiaria);
			qDCPeriodo = qDCPeriodo.add(qDCDiaria);

			BigDecimal qDPDiaria = this.obterValorQDP(mapaModalidadesPorPontoConsumo, mapaValorQDPPorPontoConsumo, data);
			apuracaoQuantidade.setQtdaDiariaProgramada(qDPDiaria);
			qDPPeriodo = qDPPeriodo.add(qDPDiaria);

			listaApuracaoQuantidade.add(apuracaoQuantidade);
		}

		apuracaoVO.setListaApuracaoQuantidade(listaApuracaoQuantidade);
		apuracaoVO.setqDCAcumulada(qDCPeriodo);
		apuracaoVO.setqDPAcumulada(qDPPeriodo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade#obterQDP(java. util.Collection, java.util.Date,
	 * java.math.BigDecimal)
	 */
	@Override
	public BigDecimal obterQDP(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Date data)
			throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		BigDecimal retorno = BigDecimal.ZERO;

		// Monta um mapa dos pontos de consumo
		Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaPontoConsumo =
				new HashMap<>();
		List<ContratoPontoConsumoModalidade> listaModal = null;
		for (ContratoPontoConsumoModalidade cpcMod : listaContratoPontoConsumoModalidade) {
			if (mapaPontoConsumo.containsKey(cpcMod.getContratoPontoConsumo().getPontoConsumo())) {
				listaModal = mapaPontoConsumo.get(cpcMod.getContratoPontoConsumo().getPontoConsumo());
			} else {
				listaModal = new ArrayList<>();
			}
			listaModal.add(cpcMod);
			mapaPontoConsumo.put(cpcMod.getContratoPontoConsumo().getPontoConsumo(), listaModal);
		}

		for (Map.Entry<PontoConsumo, List<ContratoPontoConsumoModalidade>> entry : mapaPontoConsumo.entrySet()) {
			PontoConsumo pontoConsumo = entry.getKey();

			BigDecimal qDCDiaria = controladorContrato.calcularQDCMedia(entry.getValue(), data, data);

			BigDecimal qDPPontoConsumo = null;
			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" Select sum(solicitacao.valorQDP) ");
			hql.append(" from ");
			hql.append("SolicitacaoConsumoPontoConsumoImpl ");
			hql.append(" solicitacao ");
			hql.append(" where ");
			hql.append(" solicitacao.pontoConsumo.chavePrimaria = :idPontoConsumo ");
			hql.append(" and solicitacao.dataSolicitacao = :data");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
			query.setDate("data", data);

			qDPPontoConsumo = (BigDecimal) query.uniqueResult();

			if (qDPPontoConsumo != null) {
				retorno = retorno.add(qDPPontoConsumo);
			} else {
				retorno = retorno.add(qDCDiaria);
			}
		}

		return retorno;
	}

	/**
	 * Método responsável por calcular a média do valor de QDP.
	 *
	 * @param mapaModalidadesPorPontoConsumo - {@link Map}
	 * @param mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 * @param dataPeriodicidade - {@link Date}
	 * @return média do valor QDP - {@link BigDecimal}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public BigDecimal calcularQDPMedia(Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo,
			MultiKeyMap mapaValorQDPPorPontoConsumo, Date dataPeriodicidade) throws NegocioException {

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(dataPeriodicidade);
		int menorDiaMes = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
		int maiorDiaMes = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		calendar.set(Calendar.DATE, menorDiaMes);
		Date primeiroDiaMes = calendar.getTime();

		calendar.set(Calendar.DATE, maiorDiaMes);
		Date ultimoDiaMes = calendar.getTime();

		BigDecimal retorno = BigDecimal.ZERO;

		DateTime dtInicio = new DateTime(primeiroDiaMes);
		DateTime dtFim = new DateTime(ultimoDiaMes);

		DateTime dtAtual = dtInicio;

		while (dtAtual.compareTo(dtFim) <= 0) {
			BigDecimal qdpDiaria = this.obterValorQDP(mapaModalidadesPorPontoConsumo, mapaValorQDPPorPontoConsumo, dtAtual.toDate());
			retorno = retorno.add(qdpDiaria);
			dtAtual = dtAtual.plusDays(1);
		}

		if (!(retorno.compareTo(BigDecimal.ZERO)==0)) {
			retorno = retorno.divide(new BigDecimal(maiorDiaMes), ESCALA_VALOR_QDP_MEDIA, BigDecimal.ROUND_HALF_UP);
		}

		return retorno;
	}

	/**
	 * Soma o valor diário de QPD das solicitações de consumo ou o valor diário médio de QDC, caso o QDP seja nulo, de uma coleção de pontos
	 * de consumo.
	 *
	 * @param mapaModalidadesPorPontoConsumo - {@link Map}
	 * @param mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 * @param data - {@link Date}
	 * @return valorQDP - {@link BigDecimal}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public BigDecimal obterValorQDP(Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo,
			MultiKeyMap mapaValorQDPPorPontoConsumo, Date data) throws NegocioException {

		BigDecimal valorQDP = BigDecimal.ZERO;

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		for (Map.Entry<PontoConsumo, List<ContratoPontoConsumoModalidade>> entry : mapaModalidadesPorPontoConsumo.entrySet()) {
			PontoConsumo pontoConsumo = entry.getKey();

			BigDecimal valorDiarioQDC = null;

			BigDecimal pontoConsumoQDPDiario =
					(BigDecimal) mapaValorQDPPorPontoConsumo.get(pontoConsumo.getChavePrimaria(), DataUtil.minimizarHorario(data));

			if (pontoConsumoQDPDiario != null) {
				valorQDP = valorQDP.add(pontoConsumoQDPDiario);
			} else {
				valorDiarioQDC = controladorContrato.calcularQDCMedia(entry.getValue(), data, data);
				valorQDP = valorQDP.add(valorDiarioQDC);
			}
		}

		return valorQDP;
	}

	private Collection<Recuperacao> montarRecuperacao(ApuracaoVO apuracaoVO, Date dataInicioApuracao,
			Date dataFinalApuracao) {

		Collection<Recuperacao> listaRecuperacao = new ArrayList<>();

		Recuperacao recuperacao = null;
		if (apuracaoVO != null && apuracaoVO.getMapaVolumeRetiradoPorModalidadeDia() != null
				&& !apuracaoVO.getMapaVolumeRetiradoPorModalidadeDia().isEmpty()) {

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaTodasAQPP = apuracaoVO.getListaAQPP();
			Map<ApuracaoQuantidadePenalidadePeriodicidade, BigDecimal> mapaAQPP =
					new HashMap<>();
			if (listaTodasAQPP != null) {
				for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : listaTodasAQPP) {
					mapaAQPP.put(aqpp, BigDecimal.ZERO);
				}
			}

			Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP;

			Collection<ConsumoDistribuidoModalidade> listaModalidadeData = apuracaoVO.getMapaVolumeRetiradoPorModalidadeDia().keySet();
			Set<ConsumoDistribuidoModalidade> listaModalidades = new HashSet<>();
			for (ConsumoDistribuidoModalidade consDistMod : listaModalidadeData) {
				listaModalidades.add(consDistMod);
			}

			for (ConsumoDistribuidoModalidade modalidade : listaModalidades) {
				recuperacao = criaRecuperacao();
				BigDecimal somaMedicaoQR = BigDecimal.ZERO;
				listaAQPP = new HashSet<>();

				recuperacao.setDataRecuperacao(dataInicioApuracao);
				if (apuracaoVO.getMapaVolumeRetiradoPorModalidadeDia().containsKey(modalidade)) {
					somaMedicaoQR = somaMedicaoQR.add(apuracaoVO.getMapaVolumeRetiradoPorModalidadeDia().get(modalidade));

					if (listaTodasAQPP != null && !listaTodasAQPP.isEmpty()) {
						BigDecimal somaUtilizadoAQPP = BigDecimal.ZERO;
						for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : listaTodasAQPP) {

							if (somaUtilizadoAQPP.compareTo(somaMedicaoQR) < 0) {
								BigDecimal saldoData = somaMedicaoQR.subtract(somaUtilizadoAQPP);

								BigDecimal valorRecuperado = mapaAQPP.get(aqpp);
								BigDecimal valorARecuperar = aqpp.getQtdaPagaNaoRetirada();
								if (aqpp.getQtdaRecuperada() != null) {
									valorARecuperar = valorARecuperar.subtract(aqpp.getQtdaRecuperada());
								}
								BigDecimal saldoAQPPAtual = valorARecuperar.subtract(valorRecuperado);
								if (saldoAQPPAtual.compareTo(BigDecimal.ZERO) > 0) {
									listaAQPP.add(aqpp);
									if (saldoAQPPAtual.compareTo(saldoData) >= 0) {
										saldoAQPPAtual = saldoAQPPAtual.subtract(saldoData);
										somaUtilizadoAQPP = somaUtilizadoAQPP.add(saldoData);
									} else {
										somaUtilizadoAQPP = somaUtilizadoAQPP.add(saldoAQPPAtual);
										saldoAQPPAtual = BigDecimal.ZERO;
									}
									mapaAQPP.put(aqpp, saldoAQPPAtual);
								}
							} else {
								break;
							}
						}
					}
				}

				recuperacao.setMedicaoQR(somaMedicaoQR);
				if (listaAQPP != null && !listaAQPP.isEmpty()) {
					recuperacao.setListaApuracaoQuantidadePenalidadePeriodicidade(listaAQPP);
				} else {
					continue;
				}

				listaRecuperacao.add(recuperacao);
			}
		}

		return listaRecuperacao;
	}

	/**
	 * Distribuir volume ponto consumo.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dataInicioApuracao
	 *            the data inicio apuracao
	 * @param dataFinalApuracao
	 *            the data final apuracao
	 * @param apuracaoVO
	 *            the apuracao vo
	 * @param apuracaoPeriodo
	 *            the apuracao periodo
	 * @param listaConsumoDistribuidoModalidade
	 *            the lista consumo distribuido modalidade
	 * @param listaPontoConsumoAgrupadosVolume
	 *            the lista ponto consumo agrupados volume
	 * @param listaContratoPenalidadeRetirada
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal distribuirVolumePontoConsumo(PontoConsumo pontoConsumo, Date dataInicioApuracao,
			Date dataFinalApuracao, ApuracaoVO apuracaoVO, ApuracaoPeriodo apuracaoPeriodo,
			Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade,
			Collection<PontoConsumo> listaPontoConsumoAgrupadosVolume,
			List<ContratoPenalidade> listaContratoPenalidadeRetirada) throws NegocioException {

		BigDecimal saldoRetirada = BigDecimal.ZERO;

		if (listaConsumoDistribuidoModalidade != null && !listaConsumoDistribuidoModalidade.isEmpty()) {

			Collection<Date> diasFornecimento = Util.obterDatasIntermediarias(dataInicioApuracao, dataFinalApuracao);

			List<ConsumoDistribuidoModalidade> listaModalidadeConsumoOrdenada =
					this.obterListaModalidadeConsumoOrdenadaPorPrioridade(listaConsumoDistribuidoModalidade);

			if (diasFornecimento != null && !diasFornecimento.isEmpty()) {

				MultiKeyMap balde = new MultiKeyMap();

				for (Date data : diasFornecimento) {

					// QDR acomulada
					BigDecimal consumoDiario = BigDecimal.ZERO;

					if (listaPontoConsumoAgrupadosVolume != null && listaPontoConsumoAgrupadosVolume.size() > 1) {
						// Se
						// existe
						// faturamento
						// agrupado
						// por
						// volume
						for (PontoConsumo pontoConsumoAgrupadoVolume : listaPontoConsumoAgrupadosVolume) {
							consumoDiario = consumoDiario.add(this.obterConsumoDiario(pontoConsumoAgrupadoVolume, data));
						}
					} else {
						consumoDiario = consumoDiario.add(this.obterConsumoDiario(pontoConsumo, data));
					}

					saldoRetirada = consumoDiario;

					// o metodo
					// 'distribuirVolumes' deve
					// tambem salvar
					// informacoes de cada
					// modalidade
					// (consumoDistribuidoModalidade)
					saldoRetirada = this.distribuirVolumes(balde, saldoRetirada, listaModalidadeConsumoOrdenada, data,
							pontoConsumo, apuracaoVO, QUANT_LIMITE_MODALIDADE, dataInicioApuracao, dataFinalApuracao,
							listaContratoPenalidadeRetirada);

					if (saldoRetirada.compareTo(BigDecimal.ZERO) > 0) {
						saldoRetirada = this.distribuirVolumes(balde, saldoRetirada, listaModalidadeConsumoOrdenada,
								data, pontoConsumo, apuracaoVO, QUANT_REFERENCIA_MODALIDADE, dataInicioApuracao,
								dataFinalApuracao, listaContratoPenalidadeRetirada);

						if (saldoRetirada.compareTo(BigDecimal.ZERO) > 0) {
							saldoRetirada = this.distribuirVolumes(balde, saldoRetirada, listaModalidadeConsumoOrdenada,
									data, pontoConsumo, apuracaoVO, QUANT_RETIRADA_MAIOR_MODALIDADE, dataInicioApuracao,
									dataFinalApuracao, listaContratoPenalidadeRetirada);

							if (saldoRetirada.compareTo(BigDecimal.ZERO) > 0) {
								saldoRetirada = this.distribuirVolumes(balde, saldoRetirada,
										listaModalidadeConsumoOrdenada, data, pontoConsumo, apuracaoVO, QUANT_RESTANTE,
										dataInicioApuracao, dataFinalApuracao, listaContratoPenalidadeRetirada);

							}
						}
					}
				}

				apuracaoVO.setBalde(balde);

			}

			apuracaoPeriodo.setConsumoDistribuidoModalidade(listaConsumoDistribuidoModalidade);
		}

		// obter QDRACUMULADA por modalidade
		// para cada modalidade
		// calcula a qnr TOP
		// salva na estrutura de qupp

		// QDRACUMULADA do periodo
		// chamar RTM e RTm

		return saldoRetirada;
	}

	/**
	 * Método responsável por calcular o valor Retirado num dia para as modalidades recebidas por parâmetro, preencher o mapa de volumes
	 * Retirados Por Modalidade/Dia com esse valor e retornar o saldo que ainda resta a recuperar.
	 *
	 * @param contrato o contrato
	 * @param listaModalidadeConsumoOrdenada a lista de modalidades de consumo
	 * @param balde o mapa com o consumo diário total por Modalidade/Dia
	 * @param volRetiradosPorModalidadeDia o mapa de volumes Retirados Por Modalidade/Dia
	 * @param valorARecuperarParam a quantidade que ainda resta a recuperar
	 * @param datas the datas
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return o saldo que ainda resta a recuperar
	 * @throws NegocioException caso ocorra algum erro
	 */
	private BigDecimal calcularValorRetiradoPorModalidadeDia(Contrato contrato,
			List<ConsumoDistribuidoModalidade> listaModalidadeConsumoOrdenada, MultiKeyMap balde,
			Map<ConsumoDistribuidoModalidade, BigDecimal> volRetiradosPorModalidadeDia, BigDecimal valorARecuperarParam,
			Collection<Date> datas, Date dataInicioApuracao, Date dataFinalApuracao) throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
		String codPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		Map<String, Object> params = new HashMap<>();

		params.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));
		params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicioApuracao);
		params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataFinalApuracao);

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));

		Long codigoSituacaoContratoAtivo =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));

		Long codigoSituacaoContratoEmRecuperacao =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_EM_RECUPERACAO));

		BigDecimal valorARecuperar = BigDecimal.ZERO.add(valorARecuperarParam);

		if (listaModalidadeConsumoOrdenada != null) {
			for (ConsumoDistribuidoModalidade consDistMod : listaModalidadeConsumoOrdenada) {

				BigDecimal consumoTotalDoDia = BigDecimal.ZERO;
				for (Date data1 : datas) {
					if (balde.containsKey(consDistMod, data1)) {
						consumoTotalDoDia = consumoTotalDoDia.add((BigDecimal) balde.get(consDistMod, data1));
					}
				}

				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = consDistMod.getPenalidadeToPComMaiorPeriodicidade();
				BigDecimal volRef = consDistMod.getVolumeQDC();
				BigDecimal volRefComMargem = volRef;
				BigDecimal quantidadeLimiteMinimo = consDistMod.getVolumeQDC();
				if (contratoPontoConsumoPenalidade != null && contratoPontoConsumoPenalidade.getPercentualMargemVariacao() != null) {
					volRefComMargem = volRefComMargem.multiply(contratoPontoConsumoPenalidade.getPercentualMargemVariacao());
					quantidadeLimiteMinimo = quantidadeLimiteMinimo.multiply(contratoPontoConsumoPenalidade.getPercentualMargemVariacao());
				}

				BigDecimal recuperacaoMaxima = null;
				BigDecimal valorRetiradoNoDia = null;

				if (valorARecuperar.compareTo(BigDecimal.ZERO) > 0 && consumoTotalDoDia.compareTo(quantidadeLimiteMinimo) > 0) {
					// Se
					// houver
					// valor
					// a
					// recuperar

					BigDecimal volumeReferencia = null;
					volumeReferencia = volRef;

					if (contrato.getSituacao().getChavePrimaria() == codigoSituacaoContratoAtivo) {
						// Caso
						// o
						// contrato
						// esteja
						// Ativo

						if (contratoPenalidadeRetiradaMaior != null
								&& contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor() != null
								&& contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor().compareTo(BigDecimal.ZERO) > 0) {

							// Se houver
							// Penalidade Por
							// Retirada A Maior
							// então o
							// valor do limite
							// superior será
							// a quantidade de
							// referencia + %
							// Tolerância
							BigDecimal percentualRecuperacao = null;
							if (consDistMod.getContratoPontoConsumoModalidade() != null
									&& consDistMod.getContratoPontoConsumoModalidade().getPercentualQDCContratoQPNR() != null) {
								percentualRecuperacao = consDistMod.getContratoPontoConsumoModalidade().getPercentualQDCContratoQPNR();
							}
							if (percentualRecuperacao == null) {
								throw new NegocioException(
										"Não foi possível calcular as recuperações pois o contrato não possui Percentual máximo em relação à QDC");
							}

							recuperacaoMaxima = volumeReferencia.multiply(percentualRecuperacao);
						} else {
							// Senão o valor do
							// limite superior
							// será será a própria
							// quantidade de
							// referencia
							recuperacaoMaxima = volumeReferencia;
						}

						BigDecimal qtdPassivelRecuperacao = consumoTotalDoDia.subtract(quantidadeLimiteMinimo);
						if (qtdPassivelRecuperacao.compareTo(recuperacaoMaxima) >= 0) {
							valorRetiradoNoDia = recuperacaoMaxima;
						} else {
							valorRetiradoNoDia = qtdPassivelRecuperacao;
						}

						valorARecuperar = valorARecuperar.subtract(valorRetiradoNoDia);

					} else if (contrato.getSituacao().getChavePrimaria() == codigoSituacaoContratoEmRecuperacao) {
						// Caso o contrato esteja período de recuperação
						// O limite superior será o próprio Volume de Referência
						valorRetiradoNoDia = calcularValorRecuperado(valorARecuperar, quantidadeLimiteMinimo, volumeReferencia);
						valorARecuperar = valorARecuperar.subtract(valorRetiradoNoDia);
					}

				}

				if (valorRetiradoNoDia != null && valorRetiradoNoDia.compareTo(BigDecimal.ZERO) > 0) {
					volRetiradosPorModalidadeDia.put(consDistMod, valorRetiradoNoDia);
				}

			}
		}

		return valorARecuperar;
	}

	/**
	 * Calcular valor recuperado.
	 *
	 * @param valorARecuperar the valor a recuperar
	 * @param quantidadeLimiteMinimo the quantidade limite minimo
	 * @param quantidadeLimiteMaximo the quantidade limite maximo
	 * @return the big decimal
	 */
	private BigDecimal calcularValorRecuperado(BigDecimal valorARecuperar, BigDecimal quantidadeLimiteMinimo,
			BigDecimal quantidadeLimiteMaximo) {

		BigDecimal valorRetiradoNoDia;

		BigDecimal valorPassivelRecuperacao = quantidadeLimiteMaximo.subtract(quantidadeLimiteMinimo);

		if (valorARecuperar.compareTo(valorPassivelRecuperacao) >= 0) {
			valorRetiradoNoDia = valorPassivelRecuperacao;
		} else {
			valorRetiradoNoDia = valorARecuperar;
		}

		return valorRetiradoNoDia;
	}

	/**
	 * Esse metodo deve trazer a penalidade com menor periodicidade. Já deve vir filtrada com o tipo da periodicidade específico.
	 *
	 * @param listaContratoPontoConsumoPenalidade the lista contrato ponto consumo penalidade
	 * @return the contrato ponto consumo penalidade
	 */
	private ContratoPontoConsumoPenalidade obterPenalidadeComMenorPeriodicidade(
			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) {

		ContratoPontoConsumoPenalidade retorno = null;

		// Ordenar por periodicidade e retornar o
		// primeiro elemento.
		if (listaContratoPontoConsumoPenalidade != null && !listaContratoPontoConsumoPenalidade.isEmpty()) {
			BeanComparator comparador = new BeanComparator("periodicidadePenalidade.codigo");
			Collections.sort((List) listaContratoPontoConsumoPenalidade, comparador);
			retorno = listaContratoPontoConsumoPenalidade.iterator().next();
		}

		return retorno;

	}

	/**
	 * Esse metodo deve trazer a penalidade com maior periodicidade. Já deve vir filtrada com o tipo da periodicidade específico.
	 *
	 * @param listaContratoPontoConsumoPenalidade the lista contrato ponto consumo penalidade
	 * @return the contrato ponto consumo penalidade
	 */
	private ContratoPontoConsumoPenalidade obterPenalidadeComMaiorPeriodicidade(
			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade) {

		ContratoPontoConsumoPenalidade retorno = null;

		// Ordenar por periodicidade e retornar o
		// primeiro elemento.
		if (listaContratoPontoConsumoPenalidade != null && !listaContratoPontoConsumoPenalidade.isEmpty()) {
			BeanComparator comparador = new BeanComparator("periodicidadePenalidade.codigo");
			Collections.sort((List) listaContratoPontoConsumoPenalidade, comparador);
			retorno = (ContratoPontoConsumoPenalidade) ((List) listaContratoPontoConsumoPenalidade)
					.get(listaContratoPontoConsumoPenalidade.size() - 1);
		}

		return retorno;

	}

	/**
	 * Esse método irá filtrar a lista de acordo com a penalidade e também retornar a de menor periodicidade;.
	 *
	 * @param listaContratoPontoConsumoPenalidade the lista contrato ponto consumo penalidade
	 * @param codPenalidade the cod penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the contrato ponto consumo penalidade
	 */
	private ContratoPontoConsumoPenalidade obterPenalidadeComMenorPeriodicidade(
			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade, String codPenalidade, Date dataInicioApuracao,
			Date dataFinalApuracao) {

		ContratoPontoConsumoPenalidade retorno = null;
		Collection<ContratoPontoConsumoPenalidade> listaFiltrada =
				this.filtrarListaPorPenalidade(listaContratoPontoConsumoPenalidade, codPenalidade, dataInicioApuracao, dataFinalApuracao);
		retorno = this.obterPenalidadeComMenorPeriodicidade(listaFiltrada);
		return retorno;

	}

	/**
	 * Esse método irá filtrar a lista de acordo com a penalidade e também retornar a de maior periodicidade;.
	 *
	 * @param listaContratoPontoConsumoPenalidade the lista contrato ponto consumo penalidade
	 * @param codPenalidade the cod penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the contrato ponto consumo penalidade
	 * @throws NegocioException the negocio exception
	 */
	private ContratoPontoConsumoPenalidade obterPenalidadeComMaiorPeriodicidade(
			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade, String codPenalidade, Date dataInicioApuracao,
			Date dataFinalApuracao) {

		ContratoPontoConsumoPenalidade retorno = null;
		Collection<ContratoPontoConsumoPenalidade> listaFiltrada =
				this.filtrarListaPorPenalidade(listaContratoPontoConsumoPenalidade, codPenalidade, dataInicioApuracao, dataFinalApuracao);
		retorno = this.obterPenalidadeComMaiorPeriodicidade(listaFiltrada);
		return retorno;

	}

	/**
	 * Esse metodo deve trazer o objeto ContratoPontoConsumoPenalidade referente a penalidade passada por parâmetro.
	 *
	 * @param listaContratoPontoConsumoPenalidade the lista contrato ponto consumo penalidade
	 * @param codPenalidade the cod penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<ContratoPontoConsumoPenalidade> filtrarListaPorPenalidade(
			Collection<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidade, String codPenalidade, Date dataInicioApuracao,
			Date dataFinalApuracao) {

		// Essa lista irá conter as informações
		// referentes a penalidade passada
		// por parâmetro.
		List<ContratoPontoConsumoPenalidade> listaContratoPontoConsumoPenalidadesAux = new ArrayList<>();

		if (listaContratoPontoConsumoPenalidade != null && !listaContratoPontoConsumoPenalidade.isEmpty()) {
			for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaContratoPontoConsumoPenalidade) {
				if (contratoPontoConsumoPenalidade.getPenalidade().getChavePrimaria() == Long.parseLong(codPenalidade)) {

					// se data de inicio e fim de apuração não for nula no paramentro, será penalidade TOP que deve checar as vigências.
					if (dataInicioApuracao != null && dataFinalApuracao != null) {
						if (dataInicioApuracao.compareTo(contratoPontoConsumoPenalidade.getDataInicioVigencia()) >= 0
								&& dataFinalApuracao.compareTo(contratoPontoConsumoPenalidade.getDataFimVigencia()) <= 0) {
							listaContratoPontoConsumoPenalidadesAux.add(contratoPontoConsumoPenalidade);
						}
					} else {
						listaContratoPontoConsumoPenalidadesAux.add(contratoPontoConsumoPenalidade);
					}
				}
			}
		}

		return listaContratoPontoConsumoPenalidadesAux;

	}

	/**
	 * Obter lista modalidade consumo ordenada por prioridade.
	 *
	 * @param modalidadeConsumos the modalidade consumos
	 * @return the list
	 */
	private List<ConsumoDistribuidoModalidade> obterListaModalidadeConsumoOrdenadaPorPrioridade(
			Collection<ConsumoDistribuidoModalidade> modalidadeConsumos) {

		List<ConsumoDistribuidoModalidade> listaOrdenada = null;
		if (modalidadeConsumos != null && !modalidadeConsumos.isEmpty()) {

			listaOrdenada = new ArrayList<>(modalidadeConsumos);

			Collections.sort(listaOrdenada, new Comparator<ConsumoDistribuidoModalidade>() {

				@Override
				public int compare(ConsumoDistribuidoModalidade moda1, ConsumoDistribuidoModalidade moda2) {

					Integer codigo1 = moda1.getContratoPontoConsumoModalidade().getContratoModalidade().getSequenciaApuracaoVolume();
					Integer codigo2 = moda2.getContratoPontoConsumoModalidade().getContratoModalidade().getSequenciaApuracaoVolume();
					return codigo1.compareTo(codigo2);
				}
			});
		}

		return listaOrdenada;
	}

	/**
	 * Método responsável por obter o volume consumido por dia.
	 *
	 * @param pontoConsumo O ponto de consumo
	 * @param data O dia
	 * @return O consumo
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	@Override
	public BigDecimal obterConsumoDiario(PontoConsumo pontoConsumo, Date data) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(historicoConsumo.consumoApurado) ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
		hql.append(" WHERE ");
		hql.append(" historicoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and (historicoMedicao.dataLeituraInformada between :dataInicio and :dataFim) ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = false ");
		hql.append(" and historicoConsumo.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePontoConsumo", pontoConsumo.getChavePrimaria());

		Util.adicionarRestricaoDataSemHora(query, data, "dataInicio", Boolean.TRUE);
		Util.adicionarRestricaoDataSemHora(query, data, "dataFim", Boolean.FALSE);

		BigDecimal consumo = (BigDecimal) query.uniqueResult();

		if (consumo != null) {
			return consumo;
		} else {
			return BigDecimal.ZERO;
		}

	}

	/**
	 *
	 * @param balde
	 * @param saldoRetirada
	 * @param listaModalidadeConsumoOrdenada
	 * @param data
	 * @param pontoConsumo
	 * @param apuracaoVO
	 * @param limiteBalde
	 * @param dataInicioApuracao
	 * @param dataFinalApuracao
	 * @param listaContratoPenalidadeRetirada
	 * @return
	 * @throws NegocioException
	 */
	private BigDecimal distribuirVolumes(MultiKeyMap balde, final BigDecimal saldoRetirada,
			List<ConsumoDistribuidoModalidade> listaModalidadeConsumoOrdenada, Date data,  PontoConsumo pontoConsumo,
			ApuracaoVO apuracaoVO, String limiteBalde, Date dataInicioApuracao, Date dataFinalApuracao,
			List<ContratoPenalidade> listaContratoPenalidadeRetirada) throws NegocioException {

		BigDecimal saldoRetiradaLocal = saldoRetirada;

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato
				.selecionarContratoPenalidadePorPeriodoEmVigencia(dataInicioApuracao, dataFinalApuracao, listaContratoPenalidadeRetirada);

		// Verifica todas as modalidades
		// existentes no contrato
		for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaModalidadeConsumoOrdenada) {

			ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			String parametroTakeOrPay =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

			BigDecimal volumeReferencia = obterConsumoReferencia(consumoDistribuidoModalidade, parametroTakeOrPay, Boolean.TRUE);
			BigDecimal valorLimiteBalde = BigDecimal.ZERO;

			if (limiteBalde.equals(QUANT_LIMITE_MODALIDADE)) {
				if (consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade() != null) {
					valorLimiteBalde = volumeReferencia
							.multiply(consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade().getPercentualMargemVariacao());
				}

			} else if (limiteBalde.equals(QUANT_REFERENCIA_MODALIDADE)) {
				if (consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade() != null) {
					valorLimiteBalde = volumeReferencia.subtract(volumeReferencia
							.multiply(consumoDistribuidoModalidade.getPenalidadeToPComMenorPeriodicidade().getPercentualMargemVariacao()));
				}

			} else if (limiteBalde.equals(QUANT_RETIRADA_MAIOR_MODALIDADE)) {

				if (contratoPenalidadeRetiradaMaior != null && contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor() != null) {
					valorLimiteBalde = (volumeReferencia
							.multiply(contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor().add(BigDecimal.ONE)))
									.subtract(volumeReferencia);
				}

			} else if (limiteBalde.equals(QUANT_RESTANTE)) {
				// verificar mapeamento do ampo percentualRetiradaMaior e menor
				valorLimiteBalde = saldoRetiradaLocal;

				BigDecimal valor = BigDecimal.ZERO;
				if (balde.get(consumoDistribuidoModalidade, data) != null) {
					valor = (BigDecimal) balde.get(consumoDistribuidoModalidade, data);
				}
				balde.put(consumoDistribuidoModalidade, data, saldoRetiradaLocal.add(valor));
				saldoRetiradaLocal = BigDecimal.ZERO;
				break;

			} else {
				// Colocar uma mensagem de erro personalizada.
				throw new IllegalArgumentException();
			}

			BigDecimal valor = BigDecimal.ZERO;
			if (balde.get(consumoDistribuidoModalidade, data) != null) {
				valor = (BigDecimal) balde.get(consumoDistribuidoModalidade, data);
			}

			if (saldoRetiradaLocal.compareTo(valorLimiteBalde) >= 0) {
				balde.put(consumoDistribuidoModalidade, data, valorLimiteBalde.add(valor));
				saldoRetiradaLocal = saldoRetiradaLocal.subtract(valorLimiteBalde);
			} else {
				balde.put(consumoDistribuidoModalidade, data, saldoRetiradaLocal.add(valor));
				saldoRetiradaLocal = BigDecimal.ZERO;

			}

		}

		return saldoRetiradaLocal;
	}

	/**
	 * Obter consumo referencia.
	 *
	 * @param modalidadeConsumo the modalidade consumo
	 * @param tipoPenalidade the tipo penalidade
	 * @param isDistribuicao the is distribuicao
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal obterConsumoReferencia(ConsumoDistribuidoModalidade modalidadeConsumo, String tipoPenalidade, boolean isDistribuicao) {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		BigDecimal consumo = BigDecimal.ZERO;

		String parametroShipOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);
		String parametroTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = null;

		if (isDistribuicao) {
			if (parametroShipOrPay.equals(tipoPenalidade)) {
				contratoPontoConsumoPenalidade = modalidadeConsumo.getPenalidadeSoPComMenorPeriodicidade();
			} else if (parametroTakeOrPay.equals(tipoPenalidade)) {
				contratoPontoConsumoPenalidade = modalidadeConsumo.getPenalidadeToPComMenorPeriodicidade();
			}
		} else {
			if (parametroShipOrPay.equals(tipoPenalidade)) {
				contratoPontoConsumoPenalidade = modalidadeConsumo.getPenalidadePeriodicidadeSoP();
			} else if (parametroTakeOrPay.equals(tipoPenalidade)) {
				contratoPontoConsumoPenalidade = modalidadeConsumo.getPenalidadePeriodicidadeToP();
			}
		}

		if (contratoPontoConsumoPenalidade != null) {

			String codigoConsumoReferenciaPenalidadeQDC =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_QDC);

			String codigoConsumoReferenciaPenalidadeQDP =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_QDP);

			if (contratoPontoConsumoPenalidade.getConsumoReferencia().getChavePrimaria() == Long
					.parseLong(codigoConsumoReferenciaPenalidadeQDC)) {

				consumo = modalidadeConsumo.getVolumeQDC();

			} else if (contratoPontoConsumoPenalidade.getConsumoReferencia().getChavePrimaria() == Long
					.parseLong(codigoConsumoReferenciaPenalidadeQDP)) {

				consumo = modalidadeConsumo.getVolumeQDP();

			} else {

				if (modalidadeConsumo.getVolumeQDC().compareTo(modalidadeConsumo.getVolumeQDP()) <= 0) {
					consumo = modalidadeConsumo.getVolumeQDC();
				} else {
					consumo = modalidadeConsumo.getVolumeQDP();
				}

			}
		}

		return consumo;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #pesquisarPontoConsumoPenalidade(java.util.Map )
	 */
	@Override
	public Collection<PontoConsumoVO> pesquisarPontoConsumoPenalidade(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = this.createCriteria(ContratoPontoConsumo.class).setFetchMode("pontoConsumo.imovel", FetchMode.JOIN);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.INNER_JOIN);
		criteria.createAlias("pontoConsumo.situacaoConsumo", "situacaoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.segmento", "segmento", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato", "contrato", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("contrato.habilitado", Boolean.TRUE));

		if (filtro.get("idImovel") != null) {

			criteria.createCriteria("contrato.listaContratoPontoConsumo").createCriteria("pontoConsumo.imovel")
					.add(Restrictions.eq("chavePrimaria", filtro.get("idImovel")));

		}

		if (filtro.get("idCliente") != null) {

			criteria.add(Restrictions.eq("contrato.clienteAssinatura.chavePrimaria", filtro.get("idCliente")));

		}

		Integer numero = (Integer) filtro.get("numeroContrato");
		if (numero != null && numero > 0) {
			criteria.add(Restrictions.like("contrato.numeroCompletoContrato", "%" + numero + "%"));
		}
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.list();

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("contrato.numero"));
		}

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = criteria.list();
		Collection<PontoConsumoVO> listaPontoConsumoVO = new ArrayList<>();

		if (listaContratoPontoConsumo != null) {

			for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

				PontoConsumoVO pontoConsumoVO = new PontoConsumoVO();
				pontoConsumoVO.setContrato(contratoPontoConsumo.getContrato());
				pontoConsumoVO.setPontoConsumo(contratoPontoConsumo.getPontoConsumo());

				listaPontoConsumoVO.add(pontoConsumoVO);
			}

		}

		return listaPontoConsumoVO;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade#listarPenalidades ()
	 */
	@Override
	public Collection<Penalidade> listarPenalidades() throws NegocioException {

		Criteria criteria = this.createCriteria(Penalidade.class);
		criteria.add(Restrictions.eq("habilitado", true));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade # listarApuracaoQuantidadePenalidadePeriodicidades
	 * (java.util.Map)
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidades(
			Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.createAlias("contrato", "contrato", Criteria.INNER_JOIN);
		criteria.createAlias("contrato.clienteAssinatura", "clienteAssinatura", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.imovel", "imovel", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade", "penalidade", Criteria.INNER_JOIN);

		if (filtro.get("idPontoConsumo") != null) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", filtro.get("idPontoConsumo")));
		}

		if (filtro.get("idImovel") != null) {
			criteria.add(Restrictions.eq("imovel.chavePrimaria", filtro.get("idImovel")));
		}

		if (filtro.get("idCliente") != null) {
			criteria.add(Restrictions.eq("clienteAssinatura.chavePrimaria", filtro.get("idCliente")));
		}

		if (filtro.get(PARAM_NAME_ID_CONTRATO) != null) {
			criteria.add(Restrictions.eq("contrato.chavePrimaria", filtro.get(PARAM_NAME_ID_CONTRATO)));
		}

		if (filtro.get(PARAM_NAME_ID_CONTRATO_MODALIDADE) != null) {
			criteria.add(Restrictions.eq("contratoModalidade.chavePrimaria", filtro.get(PARAM_NAME_ID_CONTRATO_MODALIDADE)));
		}

		if (filtro.get("numeroContrato") != null) {
			criteria.add(Restrictions.eq("contrato.numero", filtro.get("numeroContrato")));
		}

		if (filtro.get("idPenalidadesAssociadas") != null) {
			criteria.add(Restrictions.in("penalidade.chavePrimaria", (Long[]) filtro.get("idPenalidadesAssociadas")));
		}
		if (filtro.get("possuiQPNR") != null) {
			criteria.add(Restrictions.isNotNull("qtdaPagaNaoRetirada"));
		}

		if (filtro.get("cobrada") != null) {

			Boolean cobrada = (Boolean) filtro.get("cobrada");

			if (cobrada) {
				criteria.add(Restrictions.isNotNull("valorCobrado"));
			} else {
				criteria.add(Restrictions.isNull("valorCobrado"));
			}

		}

		Date dataInicio = (Date) filtro.get("dataApuracaoInicio");
		Date dataFim = (Date) filtro.get("dataApuracaoFim");

		if ((dataInicio != null) || (dataFim != null)) {

			if (dataInicio != null && dataFim == null) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO, true);

			} else if (dataFim != null && dataInicio == null) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_APURACAO_OBRIGATORIO, true);

			} else {

				SimpleDateFormat dfAnoMes = new SimpleDateFormat(Constantes.FORMATO_DATA_ANO_MES);
				SimpleDateFormat dfAno = new SimpleDateFormat(Constantes.FORMATO_DATA_ANO);

				Integer anoMesInicio = Integer.valueOf(dfAnoMes.format(dataInicio));
				Integer anoMesFim = Integer.valueOf(dfAnoMes.format(dataFim));

				Integer anoInicio = Integer.valueOf(dfAno.format(dataInicio));
				Integer anoFim = Integer.valueOf(dfAno.format(dataFim));

				Criterion intervaloAnoMes = Restrictions.between("periodoApuracao", anoMesInicio, anoMesFim);
				Criterion intervaloAno = Restrictions.between("periodoApuracao", anoInicio, anoFim);
				Criterion valorMaximoAno = Restrictions.le("periodoApuracao", VALOR_MAXIMO);

				criteria.add(Restrictions.or(intervaloAnoMes, Restrictions.and(valorMaximoAno, intervaloAno)));

			}

		}

		Date dataInicioApuracao = (Date) filtro.get("dataApuracaoInicial");
		Date dataFimApuracao = (Date) filtro.get("dataApuracaoFinal");
		if ((dataInicioApuracao != null) || (dataFimApuracao != null)) {
			if (dataInicioApuracao != null && dataFimApuracao == null) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO, true);

			} else if (dataFimApuracao != null && dataInicioApuracao == null) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_APURACAO_OBRIGATORIO, true);

			} else {
				DateTime datainicioDt = new DateTime(dataInicioApuracao);
				datainicioDt = Util.zerarHorario(datainicioDt);

				DateTime dataFimDt = new DateTime(dataFimApuracao);
				dataFimDt = Util.ultimoHorario(dataFimDt);
				criteria.add(Restrictions.ge("dataInicioApuracao", datainicioDt.toDate()));
				criteria.add(Restrictions.le("dataFimApuracao", dataFimDt.toDate()));
			}
		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			colecaoPaginada.adicionarOrdenacaoEspecial("clienteAssinatura.nome", new OrdenacaoEspecialNomeCliente());
			colecaoPaginada.adicionarOrdenacaoEspecial("pontoConsumo.imovel.nome", new OrdenacaoEspecialDescricaoImovelPontoConsumo());

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("contrato.numero"));
		}
		Boolean ordenarData = (Boolean) filtro.get("ordenarDataApuracao");
		if (ordenarData != null && ordenarData.equals(true)) {
			criteria.addOrder(Order.asc("dataInicioApuracao"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade# listarApuracaoQuantidadeVO(java.util.Map)
	 */
	@Override
	public List<Object[]> listarApuracaoQuantidadeVO(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);

		criteria.createAlias("contratoAtual", "contratoAtual", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato", "contrato", Criteria.INNER_JOIN);
		criteria.createAlias("contrato.clienteAssinatura", "clienteAssinatura", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.imovel", "imovel", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade", "penalidade", Criteria.INNER_JOIN);
		criteria.createAlias("contratoModalidade", "contratoModalidade", Criteria.LEFT_JOIN);
		if (filtro.get("idImovel") != null) {
			criteria.add(Restrictions.eq("imovel.chavePrimaria", filtro.get("idImovel")));
		}

		if (filtro.get("idCliente") != null) {
			criteria.add(Restrictions.eq("clienteAssinatura.chavePrimaria", filtro.get("idCliente")));
		}

		if (filtro.get("idPontoConsumo") != null) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", filtro.get("idPontoConsumo")));
		}

		if (filtro.get(PARAM_NAME_ID_CONTRATO) != null) {
			criteria.add(Restrictions.eq("contrato.chavePrimaria", filtro.get(PARAM_NAME_ID_CONTRATO)));
		}

		if (filtro.get(PARAM_NAME_ID_CONTRATO_MODALIDADE) != null) {
			criteria.add(Restrictions.eq("contratoModalidade.chavePrimaria", filtro.get(PARAM_NAME_ID_CONTRATO_MODALIDADE)));
		}

		Integer numero = (Integer) filtro.get("numeroContrato");
		if (numero != null && numero > 0) {
			criteria.add(Restrictions.like("contrato.numeroCompletoContrato", "%" + numero + "%"));
		}

		if (filtro.get("idPenalidadesAssociadas") != null) {
			criteria.add(Restrictions.in("penalidade.chavePrimaria", (Long[]) filtro.get("idPenalidadesAssociadas")));
		}

		if (filtro.get("cobrada") != null) {

			Boolean cobrada = (Boolean) filtro.get("cobrada");

			if (cobrada) {
				criteria.add(Restrictions.isNotNull("valorCobrado"));
			} else {
				criteria.add(Restrictions.isNull("valorCobrado"));
			}

		}

		Date dataInicio = (Date) filtro.get("dataApuracaoInicio");
		Date dataFim = (Date) filtro.get("dataApuracaoFim");

		if ((dataInicio != null) || (dataFim != null)) {

			if (dataInicio != null && dataFim == null) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO, true);

			} else if (dataFim == null && dataInicio != null) {

				throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_APURACAO_OBRIGATORIO, true);

			} else {

				SimpleDateFormat dfAnoMes = new SimpleDateFormat(Constantes.FORMATO_DATA_ANO_MES);
				SimpleDateFormat dfAno = new SimpleDateFormat(Constantes.FORMATO_DATA_ANO);

				Integer anoMesInicio = Integer.valueOf(dfAnoMes.format(dataInicio));
				Integer anoMesFim = Integer.valueOf(dfAnoMes.format(dataFim));

				Integer anoInicio = Integer.valueOf(dfAno.format(dataInicio));
				Integer anoFim = Integer.valueOf(dfAno.format(dataFim));

				Criterion intervaloAnoMes = Restrictions.between("periodoApuracao", anoMesInicio, anoMesFim);
				Criterion intervaloAno = Restrictions.between("periodoApuracao", anoInicio, anoFim);
				Criterion valorMaximoAno = Restrictions.le("periodoApuracao", VALOR_MAXIMO);

				criteria.add(Restrictions.or(intervaloAnoMes, Restrictions.and(valorMaximoAno, intervaloAno)));

			}

		}

		ProjectionList proList = Projections.projectionList();
		proList.add(Projections.sum("valorCobrado"), "somaValorCobrado");
		proList.add(Projections.sum("valorCalculado"), "somaValorCalculado");

		proList.add(Projections.groupProperty("contratoAtual.chavePrimaria"));
		proList.add(Projections.groupProperty("contrato.numero"));
		proList.add(Projections.groupProperty("pontoConsumo.chavePrimaria"));
		proList.add(Projections.groupProperty("pontoConsumo.descricao"));
		proList.add(Projections.groupProperty("penalidade.chavePrimaria"));
		proList.add(Projections.groupProperty("penalidade.descricao"));
		proList.add(Projections.groupProperty("clienteAssinatura.nome"));
		proList.add(Projections.groupProperty("imovel.nome"));
		proList.add(Projections.groupProperty("periodoApuracao"));
		proList.add(Projections.groupProperty("contratoModalidade.chavePrimaria"));
		proList.add(Projections.groupProperty("contratoModalidade.descricao"));
		proList.add(Projections.sum("valorCalculadoPercenCobranca"), "somaValorCalculadoPercent");
		criteria.setProjection(proList);

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("contrato.numero"));
		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #montarListaApuracaoQuantidadeVO(java.util.List )
	 */
	@Override
	public Collection<ApuracaoQuantidadeVO> montarListaApuracaoQuantidadeVO(List<Object[]> listaObjetos) throws NegocioException {

		Collection<ApuracaoQuantidadeVO> listaApuracaoQuantidadeVO = new ArrayList<>();

		for (Object[] object : listaObjetos) {
			ApuracaoQuantidadeVO apuracaoQuantidadeVO = new ApuracaoQuantidadeVO();

			if (object[0] != null) {
				apuracaoQuantidadeVO.setValorCobrado((BigDecimal) object[0]);
			}
			if (object[1] != null) {
				apuracaoQuantidadeVO.setValorCalculado((BigDecimal) object[1]);
			}
			if (object[CHAVE_CONTRATO] != null) {
				apuracaoQuantidadeVO.setChaveContrato(Long.parseLong(object[CHAVE_CONTRATO].toString()));

				if (object[NUMERO_CONTRATO] != null) {
					ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
					Contrato contrato = (Contrato) controladorContrato.obter(Long.parseLong(object[CHAVE_PRIMARIA].toString()));
					apuracaoQuantidadeVO.setNumeroContrato(contrato.getNumeroFormatado());
				}
			}
			if (object[CHAVE_PONTO_CONSUMO] != null) {
				apuracaoQuantidadeVO.setChavePontoConsumo(Long.parseLong(object[CHAVE_PONTO_CONSUMO].toString()));
			}
			if (object[DESCRICAO_PONTO_CONSUMO] != null) {
				apuracaoQuantidadeVO.setDescricaoPontoConsumo((String) object[DESCRICAO_PONTO_CONSUMO]);
			}
			if (object[CHAVE_PENALIDADE] != null) {
				apuracaoQuantidadeVO.setChavePenalidade(Long.parseLong(object[CHAVE_PENALIDADE].toString()));
			}
			if (object[DESCRICAO_PENALIDADE] != null) {
				apuracaoQuantidadeVO.setDescricaoPenalidade((String) object[DESCRICAO_PENALIDADE]);
			}
			if (object[NOME_CLIENTE_ASSINATURA] != null) {
				apuracaoQuantidadeVO.setNomeClienteAssinatura((String) object[NOME_CLIENTE_ASSINATURA]);
			}
			if (object[NOME_IMOVEL] != null) {
				apuracaoQuantidadeVO.setNomeImovel((String) object[NOME_IMOVEL]);
			}
			if (object[PERIODO_APURACAO] != null) {
				apuracaoQuantidadeVO.setPeriodoApuracao(Integer.parseInt(object[PERIODO_APURACAO].toString()));
				apuracaoQuantidadeVO.setPeriodoApuracaoFormatado(Util.formatarAnoMes((Integer) object[PERIODO_APURACAO]));
			}
			if (object[CHAVE_MODALIDADE] != null) {
				apuracaoQuantidadeVO.setChaveModalidade(Long.parseLong(object[CHAVE_MODALIDADE].toString()));
			}
			if (object[DESCRICAO_MODALIDADE] != null) {
				apuracaoQuantidadeVO.setDescricaoModalidade((String) object[DESCRICAO_MODALIDADE]);
			}
			if (object[VALOR_CALCULADO_PERCENTUAL] != null) {
				apuracaoQuantidadeVO.setValorCalculadoPercentual((BigDecimal) object[VALOR_CALCULADO_PERCENTUAL]);
			}

			listaApuracaoQuantidadeVO.add(apuracaoQuantidadeVO);

		}

		return listaApuracaoQuantidadeVO;
	}

	private class OrdenacaoEspecialNomeCliente implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			criteria.addOrder(Order.asc("clienteAssinatura.nome"));

		}

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	private class OrdenacaoEspecialDescricaoImovelPontoConsumo implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			criteria.addOrder(Order.asc("imovel.nome"));

		}

		/*
		 * (non-Javadoc)
		 *
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #obterApuracaoQuantidadePenalidadePeriodicidade
	 * (java.lang.Long)
	 */
	@Override
	public ApuracaoQuantidadePenalidadePeriodicidade obterApuracaoQuantidadePenalidadePeriodicidade(Long chavePrimaria)
			throws NegocioException {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.createAlias("periodicidadePenalidade", "periodicidadePenalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("fatura", "fatura", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato", "contrato", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato.clienteAssinatura", "clienteAssinatura", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoAtual", "contratoAtual", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoModalidade", "contratoModalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("clienteAssinatura.enderecos", "enderecos", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.imovel", "imovel", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.segmento", "segmento", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade", "penalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade.rubricaDesconto", "rubricaDesconto", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade.rubrica", "rubrica", Criteria.LEFT_JOIN);
		criteria.createAlias("rubrica.lancamentoItemContabil", "lancamentoItemContabil", Criteria.LEFT_JOIN);
		criteria.createAlias("lancamentoItemContabil.tipoCreditoDebito", "tipoCreditoDebito", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.idEq(chavePrimaria));

		return (ApuracaoQuantidadePenalidadePeriodicidade) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #obterQuantidadeContratrada(br.com.procenge. ggas
	 * .faturamento.apuracaopenalidade .ApuracaoQuantidadePenalidadePeriodicidade)
	 */
	@Override
	public Collection<QuantidadeContratadaVO> obterQuantidadeContratrada(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
			throws NegocioException {

		Contrato contrato = this.obterContratoAtual(apuracao);

		Collection<QuantidadeContratadaVO> lista = new ArrayList<>();

		if (apuracao.getPeriodicidadePenalidade().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL) {

			lista.add(this.obterQuantidadeContratadaVOPenalidadePeriodicidadeMensal(apuracao));

		} else if (apuracao.getPeriodicidadePenalidade().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

			if (contrato.getIndicadorAnoContratual()) {

				lista.addAll(this.obterQuantidadeContratadaVOPenalidadePeriodicidadeAnual(apuracao, Boolean.TRUE));

			} else {

				lista.addAll(this.obterQuantidadeContratadaVOPenalidadePeriodicidadeAnual(apuracao, Boolean.FALSE));

			}

		}

		return lista;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade .ControladorApuracaoPenalidade #obterQuantidadeRetiradaPeriodo (br.com.ggas.
	 * faturamento.apuracaopenalidade. ApuracaoQuantidadePenalidadePeriodicidade)
	 */
	@Override
	public Collection<QuantidadeRetiradaPeriodoVO> obterQuantidadeRetiradaPeriodo(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
			throws NegocioException {

		Collection<QuantidadeRetiradaPeriodoVO> lista = new ArrayList<>();
		Contrato contrato = this.obterContratoAtual(apuracao);

		if (apuracao.getPeriodicidadePenalidade().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL) {

			lista.add(this.obterQuantidadeRetiradaPeriodoMensal(apuracao));

		} else if (apuracao.getPeriodicidadePenalidade().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

			if (contrato.getIndicadorAnoContratual()) {

				lista.addAll(this.obterQuantidadeRetiradaPeriodoAnual(apuracao, Boolean.TRUE));

			} else {

				lista.addAll(this.obterQuantidadeRetiradaPeriodoAnual(apuracao, Boolean.FALSE));

			}

		}

		return lista;
	}

	/**
	 * Obter quantidade retirada periodo anual.
	 *
	 * @param apuracao the apuracao
	 * @param isAnoCivil the is ano civil
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<QuantidadeRetiradaPeriodoVO> obterQuantidadeRetiradaPeriodoAnual(ApuracaoQuantidadePenalidadePeriodicidade apuracao,
			Boolean isAnoCivil) throws NegocioException {

		try {

			ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
			Contrato contrato = this.obterContratoAtual(apuracao);
			contrato = (Contrato) controladorContrato.obter(contrato.getChavePrimaria(), "listaContratoPontoConsumo");

			Collection<QuantidadeRetiradaPeriodoVO> lista = new ArrayList<>();

			Map<Integer, Integer> mapaPeriodo =
					Util.obterIntervaloAnoMesPeriodo(apuracao.getDataInicioApuracao(), apuracao.getDataFimApuracao(), isAnoCivil);

			for (Integer anoMes : mapaPeriodo.keySet()) {

				BigDecimal qdr = BigDecimal.ZERO;
				Map<String, Date> mapaData = Util.obterDataInicialDataFimPeriodo(anoMes);

				Collection<Date> datas = Util.gerarIntervaloDatas(mapaData.get("dataInicial"), mapaData.get("dataFinal"));
				datas.add(mapaData.get("dataInicial"));

				for (Date data : datas) {
					qdr = qdr.add(this.obterQdrContrato(contrato, data));
				}

				BigDecimal qtdaDiasPeriodo = BigDecimal.valueOf(Util.obterQuantidadeDiasPeriodo(anoMes));
				BigDecimal qdrMedia = qdr.divide(qtdaDiasPeriodo, ESCALA_QDR_MEDIA, RoundingMode.HALF_UP);

				QuantidadeRetiradaPeriodoVO quantidadeRetiradaPeriodoVO = new QuantidadeRetiradaPeriodoVO();
				quantidadeRetiradaPeriodoVO.setPeriodo(anoMes);
				quantidadeRetiradaPeriodoVO.setQdr(qdr);
				quantidadeRetiradaPeriodoVO.setQdrMedia(qdrMedia);

				lista.add(quantidadeRetiradaPeriodoVO);

			}

			return lista;

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		}

	}

	/**
	 * Obter quantidade retirada periodo mensal.
	 *
	 * @param apuracao the apuracao
	 * @return the quantidade retirada periodo vo
	 * @throws NegocioException the negocio exception
	 */
	private QuantidadeRetiradaPeriodoVO obterQuantidadeRetiradaPeriodoMensal(ApuracaoQuantidadePenalidadePeriodicidade apuracao) {

		BigDecimal qtdaDiasPeriodo = BigDecimal.valueOf(Util.obterQuantidadeDiasPeriodo(apuracao.getPeriodoApuracao()));
		BigDecimal qdrMedia = apuracao.getQtdaDiariaRetirada().divide(qtdaDiasPeriodo, ESCALA_QDR_MEDIA, RoundingMode.HALF_UP);

		QuantidadeRetiradaPeriodoVO quantidadeRetiradaPeriodoVO = new QuantidadeRetiradaPeriodoVO();
		quantidadeRetiradaPeriodoVO.setPeriodo(apuracao.getPeriodoApuracao());
		quantidadeRetiradaPeriodoVO.setQdr(apuracao.getQtdaDiariaRetirada());
		quantidadeRetiradaPeriodoVO.setQdrMedia(qdrMedia);

		return quantidadeRetiradaPeriodoVO;
	}

	/**
	 * Obter quantidade contratada vo penalidade periodicidade mensal.
	 *
	 * @param apuracao the apuracao
	 * @return the quantidade contratada vo
	 * @throws NegocioException the negocio exception
	 */
	private QuantidadeContratadaVO obterQuantidadeContratadaVOPenalidadePeriodicidadeMensal(
			ApuracaoQuantidadePenalidadePeriodicidade apuracao) {

		QuantidadeContratadaVO quantidadeContratadaVO = new QuantidadeContratadaVO();
		quantidadeContratadaVO.setPeriodo(apuracao.getPeriodoApuracao());
		quantidadeContratadaVO.setQdc(apuracao.getQtdaDiariaContratadaMedia());
		quantidadeContratadaVO.setQmc(apuracao.getQtdaDiariaContratada());

		return quantidadeContratadaVO;

	}

	/**
	 * Obter quantidade contratada vo penalidade periodicidade anual.
	 *
	 * @param apuracao the apuracao
	 * @param isAnoCivil the is ano civil
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<QuantidadeContratadaVO> obterQuantidadeContratadaVOPenalidadePeriodicidadeAnual(
			ApuracaoQuantidadePenalidadePeriodicidade apuracao, Boolean isAnoCivil) throws NegocioException {

		try {

			ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
			Contrato contrato = this.obterContratoAtual(apuracao);
			contrato = (Contrato) controladorContrato.obter(contrato.getChavePrimaria(), "listaContratoPontoConsumo");

			Collection<QuantidadeContratadaVO> lista = new ArrayList<>();

			Map<Integer, Integer> mapaPeriodo =
					Util.obterIntervaloAnoMesPeriodo(apuracao.getDataInicioApuracao(), apuracao.getDataFimApuracao(), isAnoCivil);

			for (Integer anoMes : mapaPeriodo.keySet()) {
				BigDecimal numeroDias = BigDecimal.valueOf(Util.obterQuantidadeDiasPeriodo(anoMes));

				Map<String, Date> mapaDataPeriodo = Util.obterDataInicialDataFimPeriodo(anoMes);

				BigDecimal qmc = this.obterQdcContratoPontoConsumoModalidade(this.obterListaContratoPontoConsumoModalidade(contrato),
						mapaDataPeriodo.get("dataInicial"), mapaDataPeriodo.get("dataFinal"));
				BigDecimal qdc = qmc.divide(numeroDias, ESCALA_QDR_MEDIA, RoundingMode.HALF_UP);

				QuantidadeContratadaVO quantidadeContratadaVO = new QuantidadeContratadaVO();
				quantidadeContratadaVO.setPeriodo(anoMes);
				quantidadeContratadaVO.setQdc(qdc);
				quantidadeContratadaVO.setQmc(qmc);

				lista.add(quantidadeContratadaVO);

			}

			return lista;

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #obterQuantidadeNaoRetirada(br.com.procenge. ggas
	 * .faturamento.apuracaopenalidade .ApuracaoQuantidadePenalidadePeriodicidade, long)
	 */
	@Override
	public Collection<QuantidadeNaoRetiradaVO> obterQuantidadeNaoRetirada(ApuracaoQuantidadePenalidadePeriodicidade apuracao,
			long chavePrimariaTipoParada) throws NegocioException {

		Contrato contrato = this.obterContratoAtual(apuracao);

		Collection<QuantidadeNaoRetiradaVO> lista = new ArrayList<>();
		Collection<Long> chavesPontoConsumo = new ArrayList<>();

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		if (apuracao.getPontoConsumo() != null) {
			chavesPontoConsumo.add(apuracao.getPontoConsumo().getChavePrimaria());
		} else {
			Collection<PontoConsumo> collPontoConsumo = controladorPontoConsumo.listaPontoConsumoPorContrato(contrato.getChavePrimaria());

			for (PontoConsumo pontoConsumo : collPontoConsumo) {
				chavesPontoConsumo.add(pontoConsumo.getChavePrimaria());
			}
		}

		Criteria criteria = this.createCriteria(ParadaProgramada.class);
		criteria.add(Restrictions.in("pontoConsumo.chavePrimaria", chavesPontoConsumo));
		criteria.add(Restrictions.eq("tipoParada.chavePrimaria", chavePrimariaTipoParada));
		criteria.add(Restrictions.eq("indicadorParadaApurada", Boolean.TRUE));
		criteria.add(Restrictions.between("dataParada", apuracao.getDataInicioApuracao(), apuracao.getDataFimApuracao()));
		criteria.addOrder(Order.asc("dataParada"));

		Collection<ParadaProgramada> collParadaProgramada = criteria.list();

		if (collParadaProgramada != null && !collParadaProgramada.isEmpty()) {

			for (ParadaProgramada paradaProgramada : collParadaProgramada) {

				QuantidadeNaoRetiradaVO quantidadeNaoRetiradaVO = new QuantidadeNaoRetiradaVO();
				quantidadeNaoRetiradaVO.setData(paradaProgramada.getDataParada());

				criteria = this.createCriteria(ApuracaoQuantidade.class);
				criteria.add(Restrictions.eq("contrato.chavePrimaria", apuracao.getContrato().getChavePrimaria()));
				criteria.add(Restrictions.eq("dataApuracaoVolumes", paradaProgramada.getDataParada()));
				criteria.add(Restrictions.in("pontoConsumo.chavePrimaria", chavesPontoConsumo));

				Collection<ApuracaoQuantidade> collApuracaoQuantidade = criteria.list();

				if (collApuracaoQuantidade != null && !collApuracaoQuantidade.isEmpty()) {

					BigDecimal somaQdr = BigDecimal.ZERO;
					BigDecimal somaQdp = BigDecimal.ZERO;

					for (ApuracaoQuantidade apuracaoQuantidade : collApuracaoQuantidade) {
						if (apuracaoQuantidade.getQtdaDiariaRetirada() != null) {
							somaQdr = somaQdr.add(apuracaoQuantidade.getQtdaDiariaRetirada());
						}

						if (apuracaoQuantidade.getQtdaDiariaProgramada() != null) {
							somaQdp = somaQdp.add(apuracaoQuantidade.getQtdaDiariaProgramada());
						}
					}

					quantidadeNaoRetiradaVO.setQdr(somaQdr);
					quantidadeNaoRetiradaVO.setQdp(somaQdp);
					quantidadeNaoRetiradaVO.setObservacoes(paradaProgramada.getComentario());

				}

				lista.add(quantidadeNaoRetiradaVO);
			}

		}

		return lista;
	}

	/**
	 * Obter contrato atual.
	 *
	 * @param apuracao the apuracao
	 * @return the contrato
	 */
	private Contrato obterContratoAtual(ApuracaoQuantidadePenalidadePeriodicidade apuracao) {

		Contrato contrato = apuracao.getContratoAtual();
		if (contrato == null) {
			contrato = apuracao.getContrato();
		}

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade # listarApuracaoQuantidadePenalidadePeriodicidades
	 * (java.lang.Integer, long, long)
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidades(Integer periodo,
			long idPontoConsumo, long idPenalidade, Contrato contratoAtual) throws NegocioException {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.add(Restrictions.eq("contratoAtual", contratoAtual));
		criteria.createAlias("contrato", "contrato", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoAtual", "contratoAtual", Criteria.LEFT_JOIN);

		criteria.add(Restrictions.eq("periodoApuracao", periodo));
		if (idPontoConsumo > 0) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
		}
		criteria.add(Restrictions.eq("penalidade.chavePrimaria", idPenalidade));
		criteria.addOrder(Order.asc("dataInicioApuracao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #aplicarPercentualDescontoSobreValorTotal(java
	 * .lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String[] aplicarPercentualDescontoSobreValorTotal(String valorTotalFormatado, String percentualDescontoFormatado,
			String valorFinalFormatado, String totalValorFinalFormatado) throws NegocioException {

		String[] dados = new String[QTD_VALORES];

		try {

			BigDecimal valorDesconto = null;
			BigDecimal valorFinal = null;
			BigDecimal valorFinalAnterior;
			if (valorFinalFormatado != null && StringUtils.isNotEmpty(valorFinalFormatado)) {
				valorFinalAnterior = Util.converterCampoStringParaValorBigDecimal("Valor Final Anterior", valorFinalFormatado,
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			} else {
				valorFinalAnterior = null;
			}
			BigDecimal totalValorFinal;
			if (totalValorFinalFormatado != null) {
				totalValorFinal = Util.converterCampoStringParaValorBigDecimal("Total Valor Final", totalValorFinalFormatado,
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			} else {
				totalValorFinal = null;
			}
			BigDecimal valorTotal = Util.converterCampoStringParaValorBigDecimal("Valor Total", valorTotalFormatado,
					Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO);

			if (percentualDescontoFormatado != null && StringUtils.isNotEmpty(percentualDescontoFormatado)) {

				BigDecimal bigDecimal100 = BigDecimal.valueOf(PORCENTAGEM);
				BigDecimal percentualDesconto = Util.converterCampoStringParaValorBigDecimal("Percentual Desconto",
						percentualDescontoFormatado, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

				if (percentualDesconto.compareTo(bigDecimal100) > 0) {

					throw new NegocioException(ERRO_NEGOCIO_PERCENTUAL_DESCONTO_MAIOR_QUE_VALOR_CALCULADO, true);

				}

				valorDesconto = valorTotal.multiply(percentualDesconto.divide(bigDecimal100, ESCALA_PERCENTUAL, BigDecimal.ROUND_HALF_UP));
				valorFinal = valorTotal.subtract(valorDesconto);

			} else {
				valorFinal = valorTotal;
			}

			if (valorDesconto != null) {
				dados[0] =
						Util.converterCampoValorParaString(valorDesconto, Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO);
			} else {
				dados[0] = "";
			}
			if (valorFinal != null) {
				dados[1] = Util.converterCampoValorParaString(valorFinal, Constantes.FORMATO_VALOR_MONETARIO_BR, Constantes.LOCALE_PADRAO);
			} else {
				dados[1] = "";
			}
			dados[VALOR_FINAL_FORMATADO] = this.obterTotalValorFinalFormatado(totalValorFinal, valorFinal, valorFinalAnterior);

		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		}

		return dados;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #aplicarValorDescontoSobreValorTotal(java.lang
	 * .String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String[] aplicarValorDescontoSobreValorTotal(String valorTotalFormatado, String valorDescontoFormatado,
			String valorFinalFormatado, String totalValorFinalFormatado) throws NegocioException {

		String[] dados = new String[QTD_VALORES];

		try {

			BigDecimal valorDesconto = null;
			BigDecimal percentualDesconto = null;
			BigDecimal valorFinal = null;
			BigDecimal valorFinalAnterior;
			if (valorFinalFormatado != null && StringUtils.isNotEmpty(valorFinalFormatado)) {
				valorFinalAnterior = Util.converterCampoStringParaValorBigDecimal("Valor Final Anterior", valorFinalFormatado,
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			} else {
				valorFinalAnterior = null;
			}
			BigDecimal totalValorFinal;
			if (totalValorFinalFormatado != null) {
				totalValorFinal = Util.converterCampoStringParaValorBigDecimal("Total Valor Final", totalValorFinalFormatado,
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			} else {
				totalValorFinal = null;
			}
			BigDecimal valorTotal = Util.converterCampoStringParaValorBigDecimal("Valor Total", valorTotalFormatado,
					Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

			if (valorDescontoFormatado != null && StringUtils.isNotEmpty(valorDescontoFormatado)) {

				valorDesconto = Util.converterCampoStringParaValorBigDecimal("Valor Desconto", valorDescontoFormatado,
						Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

				if (valorDesconto.compareTo(valorTotal) > 0) {
					throw new NegocioException(ERRO_NEGOCIO_VALOR_DESCONTO_MAIOR_QUE_VALOR_CALCULADO, true);
				}

				percentualDesconto = valorDesconto.divide(valorTotal, ESCALA_PERCENTUAL, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100));
				valorFinal = valorTotal.subtract(valorDesconto);

			} else {

				valorFinal = valorTotal;

			}

			if (percentualDesconto != null) {
				dados[0] = Util.converterCampoCurrencyParaString(percentualDesconto, Constantes.LOCALE_PADRAO);
			} else {
				dados[0] = "";
			}
			if (valorFinal != null) {
				dados[1] = Util.converterCampoCurrencyParaString(valorFinal, Constantes.LOCALE_PADRAO);
			} else {
				dados[1] = "";
			}
			dados[VALOR_FINAL_FORMATADO] = this.obterTotalValorFinalFormatado(totalValorFinal, valorFinal, valorFinalAnterior);

		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		}

		return dados;
	}

	/**
	 * Obter total valor final formatado.
	 *
	 * @param totalValorFinal the total valor final
	 * @param valorFinal the valor final
	 * @param valorFinalAnterior the valor final anterior
	 * @return the string
	 */
	private String obterTotalValorFinalFormatado(final BigDecimal totalValorFinal, final BigDecimal valorFinal,
			BigDecimal valorFinalAnterior) {

		BigDecimal totalValorFinalLocal = totalValorFinal;

		if (totalValorFinalLocal != null && valorFinal != null && valorFinalAnterior != null) {
			totalValorFinalLocal = totalValorFinalLocal.subtract(valorFinalAnterior).add(valorFinal);
		}

		if (totalValorFinalLocal != null) {
			return Util.converterCampoValorParaString(totalValorFinalLocal, Constantes.FORMATO_VALOR_MONETARIO_BR,
					Constantes.LOCALE_PADRAO);
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#consultarApuracaoQtdPenalidadePeriodicidade
	 * (br.com.ggas.contrato .contrato .Contrato, java.lang.Long, java.lang.Long, java.lang.Long, java.util.Date)
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> consultarApuracaoQtdPenalidadePeriodicidade(Contrato contrato,
			Long idPontoConsumo, Long idPenalidade, Long idContratoModalidade, Date dataValidadeQNPR) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select apuracaoQtdPenalPeriod ");
		hql.append(" from ");
		hql.append(getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade().getSimpleName());
		hql.append(" apuracaoQtdPenalPeriod ");
		hql.append(" inner join apuracaoQtdPenalPeriod.contrato contrato ");
		hql.append(" inner join apuracaoQtdPenalPeriod.penalidade penalidade ");
		if (idPontoConsumo != null) {
			hql.append(" inner join apuracaoQtdPenalPeriod.pontoConsumo pontoConsumo ");
		}
		hql.append(" inner join apuracaoQtdPenalPeriod.contratoModalidade contratoModalidade ");

		hql.append(" where penalidade.chavePrimaria = :idPenalidade ");
		hql.append(" and contrato.chavePrimaria = :idContrato");
		if (idPontoConsumo != null) {
			hql.append(" and pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}
		hql.append(" and contratoModalidade.chavePrimaria = :idContratoModalidade ");
		hql.append(" and apuracaoQtdPenalPeriod.dataInicioApuracao >= :dataValidadeQNPR ");
		hql.append(" and apuracaoQtdPenalPeriod.qtdaPagaNaoRetirada <> 0 ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPenalidade", idPenalidade);
		query.setLong(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		if (idPontoConsumo != null) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}
		query.setLong(PARAM_NAME_ID_CONTRATO_MODALIDADE, idContratoModalidade);
		query.setDate("dataValidadeQNPR", dataValidadeQNPR);

		return query.list();
	}

	/**
	 * Metodo responsavel por atualizar a quantidade paga não recuperada
	 *
	 * @param recebimento , dadosAuditoria e contratoParamentro
	 * @throws NegocioException Caso ocorra algum erro na invocação do método. ConcorrenciaException Caso ocorra alguma inconsistência no
	 *             sistema
	 */
	@Override
	public void atualizarQuantidadePagaNaoRecuperada(Recebimento recebimento, DadosAuditoria dadosAuditoria, Contrato contratoParamentro)
			throws NegocioException, ConcorrenciaException {

		Map<String, BigDecimal> qtdpagaNaoRetirada = new HashMap();
		ControladorCobranca controladorCobranca =
				(ControladorCobranca) ServiceLocator.getInstancia().getControladorNegocio(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		PontoConsumo pontoConsumo = null;
		Contrato contrato = null;

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoPenaToP =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));
		Long codigoPenaSoP =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY));
		Fatura fatura = null;

		if (recebimento.getFaturaGeral() != null) {
			fatura = (Fatura) controladorCobranca.obterFatura(recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria());
			qtdpagaNaoRetirada = this.obterQuantidadePagaNaoRecuperada(fatura);

			pontoConsumo = fatura.getPontoConsumo();
			contrato = fatura.getContrato();

		} else if (recebimento.getDocumentoCobrancaItem() != null) {

			DocumentoCobrancaItem documentoCobrancaItem =
					controladorCobranca.obterDocumentoCobrancaItem(recebimento.getDocumentoCobrancaItem().getChavePrimaria());

			FaturaGeral faturaGeral = documentoCobrancaItem.getFaturaGeral();
			CreditoDebitoARealizar creditoDebitoARealizar = documentoCobrancaItem.getCreditoDebitoARealizar();

			if (faturaGeral != null) {
				fatura = faturaGeral.getFaturaAtual();
				qtdpagaNaoRetirada = this.obterQuantidadePagaNaoRecuperada(fatura);

				pontoConsumo = fatura.getPontoConsumo();
				contrato = fatura.getContrato();

			} else if (creditoDebitoARealizar != null) {

				BigDecimal qtdpagaNaoRetiradaToP = BigDecimal.ZERO;
				BigDecimal qtdpagaNaoRetiradaSoP = BigDecimal.ZERO;

				Long codigoLAICToP = Long.valueOf(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_TAKE_OR_PAY));

				Long codigoLAICSoP = Long.valueOf(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_SHIP_OR_PAY));

				ControladorCreditoDebito controladorCreditoDebito = (ControladorCreditoDebito) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);

				Collection<CreditoDebitoDetalhamento> creditoDebitoDetalhamentoItens = controladorCreditoDebito
						.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : creditoDebitoDetalhamentoItens) {
					if (creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() == codigoLAICToP) {
						qtdpagaNaoRetiradaToP = qtdpagaNaoRetiradaToP.add(creditoDebitoARealizar.getQuantidade());
					} else if (creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() == codigoLAICSoP) {
						qtdpagaNaoRetiradaSoP = qtdpagaNaoRetiradaSoP.add(creditoDebitoARealizar.getQuantidade());
					}
				}

				qtdpagaNaoRetirada.put("qtdpagaNaoRetiradaToP", qtdpagaNaoRetiradaToP);
				qtdpagaNaoRetirada.put("qtdpagaNaoRetiradaSoP", qtdpagaNaoRetiradaSoP);

				if (creditoDebitoARealizar.getCreditoDebitoNegociado().getPontoConsumo() != null) {
					pontoConsumo = creditoDebitoARealizar.getCreditoDebitoNegociado().getPontoConsumo();
					contrato = contratoParamentro;
				}
			}
		}
		Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItens = null;
		if (pontoConsumo != null && contrato != null && fatura != null) {
			apuraQuanPenPeriItens = this.verificaSaldoQuandPagaNaoRecuperada(this.obterValorCobrado(pontoConsumo, contrato, fatura));
		}

		String penalidade = null;
		BigDecimal qtdQuePodeSerRetirada = BigDecimal.ZERO;
		if (apuraQuanPenPeriItens != null) {
			for (ApuracaoQuantidadePenalidadePeriodicidade apuraQuanPenaPeri : apuraQuanPenPeriItens) {
				if (apuraQuanPenaPeri.getPenalidade().getChavePrimaria() == codigoPenaToP) {
					penalidade = "qtdpagaNaoRetiradaToP";
				} else if (apuraQuanPenaPeri.getPenalidade().getChavePrimaria() == codigoPenaSoP) {
					penalidade = "qtdpagaNaoRetiradaSoP";
				}

				BigDecimal qtdaNaoRetiradaComDesconto = apuraQuanPenaPeri.getQtdaNaoRetirada();
				BigDecimal percentualDesconto;
				if (apuraQuanPenaPeri.getPercentualDescontoAplicado() != null) {
					percentualDesconto = apuraQuanPenaPeri.getPercentualDescontoAplicado().divide(BigDecimal.valueOf(PORCENTAGEM));
				} else {
					percentualDesconto = null;
				}
				BigDecimal qtdaPagaNaoRetirada = apuraQuanPenaPeri.getQtdaPagaNaoRetirada();

				if (percentualDesconto != null) {
					qtdaNaoRetiradaComDesconto = qtdaNaoRetiradaComDesconto.multiply(percentualDesconto);
				}

				if (qtdaPagaNaoRetirada == null) {
					qtdaPagaNaoRetirada = BigDecimal.ZERO;
				}

				qtdQuePodeSerRetirada = qtdaNaoRetiradaComDesconto.subtract(qtdaPagaNaoRetirada);

				if (!(qtdQuePodeSerRetirada.compareTo(qtdpagaNaoRetirada.get(penalidade)) > 0)) {

					apuraQuanPenaPeri.setQtdaPagaNaoRetirada(qtdaPagaNaoRetirada.add(qtdQuePodeSerRetirada));
					qtdpagaNaoRetirada.put(penalidade, qtdpagaNaoRetirada.get(penalidade).subtract(qtdQuePodeSerRetirada));
				} else {
					apuraQuanPenaPeri.setQtdaPagaNaoRetirada(qtdaPagaNaoRetirada.add(qtdpagaNaoRetirada.get(penalidade)));
					qtdpagaNaoRetirada.put(penalidade, BigDecimal.ZERO);
				}

				apuraQuanPenaPeri.setDadosAuditoria(dadosAuditoria);
				atualizar(apuraQuanPenaPeri, apuraQuanPenaPeri.getClass());

			}
		}
	}

	/**
	 * Metodo responsavel por verificar o saldo da quantidade paga não recuperada.
	 *
	 * @param apuraQuanPenPeriItens Coleção de ApuracaoQuantidadePenalidadePeriodicidade
	 * @return Coleção de ApuracaoQuantidadePenalidadePeriodicidade
	 */
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> verificaSaldoQuandPagaNaoRecuperada(
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItens) {

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItensAux =
				new ArrayList<>();

		for (ApuracaoQuantidadePenalidadePeriodicidade apuraQuantPenaPeri : apuraQuanPenPeriItens) {

			BigDecimal aux = BigDecimal.ZERO;
			BigDecimal qNR = apuraQuantPenaPeri.getQtdaNaoRetirada();
			BigDecimal qPNR = apuraQuantPenaPeri.getQtdaPagaNaoRetirada();

			BigDecimal percentualDesconto = null;

			if (apuraQuantPenaPeri.getPercentualDescontoAplicado() != null
					&& apuraQuantPenaPeri.getPercentualDescontoAplicado().compareTo(BigDecimal.ZERO) > 0) {
				percentualDesconto = apuraQuantPenaPeri.getPercentualDescontoAplicado().divide(BigDecimal.valueOf(PORCENTAGEM));
			}

			if (percentualDesconto != null) {
				qNR = qNR.multiply(percentualDesconto);
			}

			if (qPNR == null) {
				qPNR = BigDecimal.ZERO;
			}

			aux = qNR.subtract(qPNR);

			if (aux.compareTo(BigDecimal.ZERO) > 0) {
				apuraQuanPenPeriItensAux.add(apuraQuantPenaPeri);
			}
		}

		return apuraQuanPenPeriItensAux;
	}

	/**
	 * Metodo responsavel por obter a quantidade paga não recuperada
	 *
	 * @param apuraQuanPenPeriItens Coleção de ApuracaoQuantidadePenalidadePeriodicidade
	 * @return Map<String, BigDecimal> Quantidade paga não retirada da penalidade TAKE_OR_PAY e quantidade paga não retirada da penalidade
	 *         SHIP_OR_PAY
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	private Map<String, BigDecimal> obterQuantidadePagaNaoRecuperada(Fatura fatura) throws NegocioException {

		BigDecimal qtdpagaNaoRetiradaToP = BigDecimal.ZERO;
		BigDecimal qtdpagaNaoRetiradaSoP = BigDecimal.ZERO;
		Collection<FaturaItem> faturaItens = fatura.getListaFaturaItem();
		Map<String, BigDecimal> qtdpagaNaoRetirada = new HashMap();

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoLAICToP = Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_TAKE_OR_PAY));
		Long codigoLAICSoP = Long.valueOf(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_SHIP_OR_PAY));

		for (FaturaItem faturaItem : faturaItens) {
			if (faturaItem.getRubrica() != null) {
				if (faturaItem.getRubrica().getLancamentoItemContabil().getChavePrimaria() == codigoLAICToP) {
					qtdpagaNaoRetiradaToP = qtdpagaNaoRetiradaToP.add(faturaItem.getQuantidade());
				} else if (faturaItem.getRubrica().getLancamentoItemContabil().getChavePrimaria() == codigoLAICSoP) {
					qtdpagaNaoRetiradaSoP = qtdpagaNaoRetiradaSoP.add(faturaItem.getQuantidade());
				}
			} else if (faturaItem.getCreditoDebitoARealizar() != null) {
				ControladorCreditoDebito controladorCreditoDebito = (ControladorCreditoDebito) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);

				Collection<CreditoDebitoDetalhamento> creditoDebitoDetalhamentoItens = controladorCreditoDebito
						.consultarCreditoDebitoDetalhamentoPeloCDARealizar(faturaItem.getCreditoDebitoARealizar().getChavePrimaria());
				for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : creditoDebitoDetalhamentoItens) {
					if (creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() == codigoLAICToP) {
						qtdpagaNaoRetiradaToP = qtdpagaNaoRetiradaToP.add(faturaItem.getCreditoDebitoARealizar().getQuantidade());
					} else if (creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() == codigoLAICSoP) {
						qtdpagaNaoRetiradaSoP = qtdpagaNaoRetiradaSoP.add(faturaItem.getCreditoDebitoARealizar().getQuantidade());
					}
				}

			}
		}
		qtdpagaNaoRetirada.put("qtdpagaNaoRetiradaToP", qtdpagaNaoRetiradaToP);
		qtdpagaNaoRetirada.put("qtdpagaNaoRetiradaSoP", qtdpagaNaoRetiradaSoP);
		return qtdpagaNaoRetirada;
	}

	/**
	 * Metodo responsavel realizar uma consulta e obter o valor cobrado
	 *
	 * @param pontoConsumo , contrato
	 * @return apuraQuanPenPeriItens Coleção de ApuracaoQuantidadePenalidadePeriodicidade
	 * @throws NegocioException Caso ocorra algum erro na invocação do método. NumberFormatException Caso ocorra algum número em um formato
	 *             inválido
	 */
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> obterValorCobrado(PontoConsumo pontoConsumo, Contrato contrato,
			Fatura fatura) throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoPenalidadeToP =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));
		Long codigoPenalidadeSoP =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY));

		List<Long> idsPenalidadesPermitidas = new ArrayList<>();
		idsPenalidadesPermitidas.add(codigoPenalidadeToP);
		idsPenalidadesPermitidas.add(codigoPenalidadeSoP);

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.add(Restrictions.eq("contrato.chavePrimaria", contrato.getChavePrimaria()));

		if (pontoConsumo != null) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", pontoConsumo.getChavePrimaria()));
		}
		criteria.add(Restrictions.in("penalidade.chavePrimaria", idsPenalidadesPermitidas));
		criteria.add(Restrictions.gt("valorCobrado", BigDecimal.ZERO));

		if (fatura != null) {
			criteria.add(Restrictions.eq("fatura.chavePrimaria", fatura.getChavePrimaria()));
		}

		criteria.addOrder(Order.asc("dataFimApuracao"));

		return criteria.list();
	}

	/**
	 * Obter apuracoes.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param contrato the contrato
	 * @return the collection
	 * @throws NumberFormatException the number format exception
	 * @throws NegocioException the negocio exception
	 */
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> obterApuracoes(PontoConsumo pontoConsumo, Contrato contrato)
			throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoPenalidadeToP =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));

		List<Long> idsPenalidadesPermitidas = new ArrayList<>();
		idsPenalidadesPermitidas.add(codigoPenalidadeToP);

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.add(Restrictions.eq("contrato.chavePrimaria", contrato.getChavePrimariaPai()));

		if (pontoConsumo != null) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", pontoConsumo.getChavePrimaria()));
		}
		criteria.add(Restrictions.in("penalidade.chavePrimaria", idsPenalidadesPermitidas));

		criteria.addOrder(Order.asc("dataFimApuracao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade# apuracaoAdapter(long, long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public ApuracaoPeriodo apuracaoAdapter(long idContrato, long idPontoConsumo, Date periodoIncial, Date periodoFinal)
			throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		Contrato contrato = (Contrato) controladorContrato.obter(idContrato);
		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);

		this.obterConsumoDistribuidoPorModalidade(contrato, pontoConsumo, DISTRIBUICAO_FATURAMENTO, null, null, periodoIncial, periodoFinal,
				null);

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade. ControladorApuracaoPenalidade #criarApuracaoQuantidadePenalidadePeriodicidade ()
	 */
	@Override
	public ApuracaoQuantidadePenalidadePeriodicidade criarApuracaoQuantidadePenalidadePeriodicidade() {

		return (ApuracaoQuantidadePenalidadePeriodicidade) ServiceLocator.getInstancia()
				.getBeanPorID(ApuracaoQuantidadePenalidadePeriodicidade.BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE_PERIODICIDADE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#criarApuracaoQuantidadePenalidade()
	 */
	@Override
	public ApuracaoQuantidadePenalidade criarApuracaoQuantidadePenalidade() {

		return (ApuracaoQuantidadePenalidade) ServiceLocator.getInstancia()
				.getBeanPorID(ApuracaoQuantidadePenalidade.BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#criaRecuperacao()
	 */
	@Override
	public Recuperacao criaRecuperacao() {

		return (Recuperacao) ServiceLocator.getInstancia().getBeanPorID(Recuperacao.BEAN_ID_RECUPERACAO);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#criarApuracaoQuantidade()
	 */
	@Override
	public ApuracaoQuantidade criarApuracaoQuantidade() {

		return (ApuracaoQuantidade) ServiceLocator.getInstancia().getBeanPorID(ApuracaoQuantidade.BEAN_ID_APURACAO_QUANTIDADE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#criarModalidadeDataQuantidade()
	 */
	@Override
	public ModalidadeDataQuantidade criarModalidadeDataQuantidade() {

		return (ModalidadeDataQuantidade) ServiceLocator.getInstancia()
				.getBeanPorID(ModalidadeDataQuantidade.BEAN_ID_MODALIDADE_DATA_QUANTIDADE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade#aplicarPenalidade (java.util.Collection,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public byte[] aplicarPenalidade(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
			throws GGASException {

		if (apuracoes != null && !apuracoes.isEmpty()) {

			ControladorFatura controladorFatura =
					(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
					.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			Long codigoPenalidadeRetiradaMaior =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));
			Long codigoPenalidadeRetiradaMenor =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));

			Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoesPermitidas =
					new ArrayList<>();

			for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : apuracoes) {

				this.validarAplicarPenalidade(apuracao);

				apuracao.setDadosAuditoria(dadosAuditoria);

				if ((apuracao.getFatura() != null)
						&& controladorFatura.verificarFaturaSituacaoPendente(apuracao.getFatura().getChavePrimaria())) {

					Long idMotivoCancelamento = Long.valueOf((String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_MOTIVO_CANCELAMENTO));

					String descricaoCancelamento = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_MOTIVO_CANCELAMENTO);

					controladorFatura.cancelarNotaDebitoCredito(new Long[] { apuracao.getFatura().getChavePrimaria() },
							idMotivoCancelamento, descricaoCancelamento, dadosAuditoria);

				}

				if (apuracao.getValorDescontoAplicado() != null) {

					if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMaior
							|| apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMenor) {
						apuracao.setValorCobrado(apuracao.getValorCalculadoPercenCobranca().subtract(apuracao.getValorDescontoAplicado()));
					} else {
						apuracao.setValorCobrado(apuracao.getValorCalculado().subtract(apuracao.getValorDescontoAplicado()));
					}

				} else {
					if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMaior
							|| apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMenor) {
						apuracao.setValorCobrado(apuracao.getValorCalculadoPercenCobranca());
					} else {
						apuracao.setValorCobrado(apuracao.getValorCalculado());
					}

				}

				this.atualizar(apuracao, ApuracaoQuantidadePenalidadePeriodicidadeImpl.class);

				getHibernateTemplate().getSessionFactory().getCurrentSession().flush();

				if (apuracao.getValorCobrado() != null && apuracao.getValorCobrado().compareTo(BigDecimal.ZERO) > 0) {

					apuracoesPermitidas.add(apuracao);

				}

			}

			if (apuracoesPermitidas.isEmpty()) {

				return null;

			} else {

				return this.gerarNotaDebitoCredito(apuracoesPermitidas, dadosAuditoria);

			}

		} else {

			return null;

		}

	}

	/**
	 * Gerar nota debito credito.
	 *
	 * @param apuracoes the apuracoes
	 * @param dadosAuditoria the dados auditoria
	 * @return the byte[]
	 * @throws GGASException the GGAS exception
	 */
	private byte[] gerarNotaDebitoCredito(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
			throws GGASException {

		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoPenalidadeRetiradaMaior =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));

		ApuracaoQuantidadePenalidadePeriodicidade apuracao = apuracoes.iterator().next();
		Cliente cliente = apuracao.getContrato().getClienteAssinatura();
		PontoConsumo pontoConsumo = apuracao.getPontoConsumo();
		Rubrica rubrica = apuracao.getPenalidade().getRubrica();
		Contrato contrato = this.obterContratoApuracao(apuracao);
		Contrato contratoAtual = apuracao.getContrato();
		Integer periodoApuracao = apuracao.getPeriodoApuracao();

		Fatura notaDebitoCredito =
				this.obterNotaCreditoDebito(cliente, pontoConsumo, rubrica, contrato, contratoAtual, periodoApuracao, dadosAuditoria);
		FaturaItem faturaItem = this.obterFaturaItem(apuracoes, notaDebitoCredito, rubrica, codigoPenalidadeRetiradaMaior, dadosAuditoria);

		BigDecimal valorTotalNotaCreditoDebito = this.obterValorTotalNotaCreditoDebito(apuracoes, faturaItem);
		notaDebitoCredito.getListaFaturaItem().add(faturaItem);
		notaDebitoCredito.setValorTotal(valorTotalNotaCreditoDebito);

		controladorFatura.validarDadosNotaDebito(notaDebitoCredito);
		byte[] retorno = controladorFatura.inserirNotaDebitoCredito(notaDebitoCredito, null, apuracao.getChavePrimaria()).getFirst();
		Long chaveNotaDebitoCredito = faturaItem.getFatura().getChavePrimaria();
		for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoPenalidade : apuracoes) {
			this.atualizarApuracaoQtdPenalidadePeriod(apuracaoPenalidade.getChavePrimaria(), chaveNotaDebitoCredito);
		}

		List<FaturaItemImpressao> listaFaturaItemImpressao =
				this.obterListaFaturaItemImpressao(apuracoes, faturaItem, codigoPenalidadeRetiradaMaior, dadosAuditoria);

		for (FaturaItemImpressao faturaItemImpressao : listaFaturaItemImpressao) {

			this.inserir(faturaItemImpressao);

		}

		return retorno;

	}

	/**
	 * Obter lista fatura item impressao.
	 *
	 * @param apuracoes the apuracoes
	 * @param faturaItem the fatura item
	 * @param codigoPenalidadeRetiradaMaior the codigo penalidade retirada maior
	 * @param dadosAuditoria the dados auditoria
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	private List<FaturaItemImpressao> obterListaFaturaItemImpressao(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes,
			FaturaItem faturaItem, Long codigoPenalidadeRetiradaMaior, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		List<FaturaItemImpressao> listaFaturaItemImpressao = new ArrayList<>();

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : apuracoes) {

			FaturaItemImpressao faturaItemImpressao = controladorFatura.criarFaturaItemImpressao();
			faturaItemImpressao.setFaturaItem(faturaItem);
			faturaItemImpressao.setDataInicial(apuracao.getDataInicioApuracao());
			faturaItemImpressao.setDataFinal(apuracao.getDataFimApuracao());

			if (apuracao.getPenalidade().getChavePrimaria() != codigoPenalidadeRetiradaMaior) {

				faturaItemImpressao.setConsumo(apuracao.getQtdaNaoRetirada());

			} else {

				faturaItemImpressao.setConsumo(apuracao.getQtdaDiariaRetiradaMaior());

			}

			faturaItemImpressao.setQuantidadeDias(Util.intervaloDatas(apuracao.getDataInicioApuracao(), apuracao.getDataFimApuracao()));
			faturaItemImpressao.setValorUnitario(apuracao.getValorTarifaMedia());
			faturaItemImpressao.setValorTotal(apuracao.getValorCobrado());
			faturaItemImpressao.setIndicadorDesconto(Boolean.FALSE);

			faturaItemImpressao.setDadosAuditoria(dadosAuditoria);

			listaFaturaItemImpressao.add(faturaItemImpressao);

		}

		return listaFaturaItemImpressao;

	}

	/**
	 * Obter contrato apuracao.
	 *
	 * @param apuracao the apuracao
	 * @return the contrato
	 * @throws NegocioException the negocio exception
	 */
	private Contrato obterContratoApuracao(ApuracaoQuantidadePenalidadePeriodicidade apuracao) throws NegocioException {

		Contrato contrato = null;

		if (apuracao.getContrato() != null) {

			if (apuracao.getContrato().getChavePrimariaPai() != null) {

				Contrato contratoPai = (Contrato) obter(apuracao.getContrato().getChavePrimariaPai());
				contrato = contratoPai;

			} else {

				contrato = apuracao.getContrato();

			}

		}

		return contrato;
	}

	/**
	 * Obter valor total nota credito debito.
	 *
	 * @param apuracoes the apuracoes
	 * @param faturaItem the fatura item
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal obterValorTotalNotaCreditoDebito(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes,
			FaturaItem faturaItem) {

		BigDecimal valorTotalNotaCreditoDebito = faturaItem.getValorTotal();

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : apuracoes) {

			if (apuracao.getPercentualDescontoAplicado() != null
					&& apuracao.getPercentualDescontoAplicado().compareTo(new BigDecimal(0)) > 0
					&& apuracao.getValorDescontoAplicado() != null
					&& apuracao.getValorDescontoAplicado().compareTo(new BigDecimal(0)) > 0) {

				BigDecimal percentualDesconto =
						apuracao.getPercentualDescontoAplicado().divide(BigDecimal.valueOf(PORCENTAGEM), ESCALA_DESCONTO, BigDecimal.ROUND_HALF_EVEN);
				BigDecimal quantidadePercentualDesconto = faturaItem.getQuantidade().multiply(percentualDesconto);
				BigDecimal quantidade = faturaItem.getQuantidade().subtract(quantidadePercentualDesconto);
				faturaItem.setQuantidade(quantidade.setScale(NOVA_ESCALA_DESCONTO, BigDecimal.ROUND_HALF_EVEN));
				valorTotalNotaCreditoDebito = valorTotalNotaCreditoDebito.subtract(apuracao.getValorDescontoAplicado());

			}

		}

		return valorTotalNotaCreditoDebito;
	}

	/**
	 * Obter fatura item.
	 *
	 * @param apuracoes the apuracoes
	 * @param notaDebitoCredito the nota debito credito
	 * @param rubrica the rubrica
	 * @param codigoPenalidadeRetiradaMaior the codigo penalidade retirada maior
	 * @param dadosAuditoria the dados auditoria
	 * @return the fatura item
	 * @throws NegocioException the negocio exception
	 */
	private FaturaItem obterFaturaItem(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, Fatura notaDebitoCredito,
			Rubrica rubrica, Long codigoPenalidadeRetiradaMaior, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoPenalidadeRetiradaMenor =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));

		BigDecimal volume = BigDecimal.ZERO;
		BigDecimal totalValorCalculado = BigDecimal.ZERO;

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : apuracoes) {

			if (apuracao.getPenalidade().getChavePrimaria() != codigoPenalidadeRetiradaMaior) {
				volume = volume.add(apuracao.getQtdaNaoRetirada());
			} else {
				volume = volume.add(apuracao.getQtdaDiariaRetiradaMaior());
			}

			if (apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMaior
					|| apuracao.getPenalidade().getChavePrimaria() == codigoPenalidadeRetiradaMenor) {
				if (apuracao.getValorCalculadoPercenCobranca() != null) {
					totalValorCalculado = totalValorCalculado.add(apuracao.getValorCalculadoPercenCobranca());
				}

			} else {
				if (apuracao.getValorCalculado() != null) {
					totalValorCalculado = totalValorCalculado.add(apuracao.getValorCalculado());
				}

			}
		}

		FaturaItem faturaItem = (FaturaItem) controladorFatura.criarFaturaItem();
		faturaItem.setDadosAuditoria(dadosAuditoria);
		faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
		faturaItem.setFatura(notaDebitoCredito);
		faturaItem.setRubrica(rubrica);
		faturaItem.setValorTotal(totalValorCalculado);
		faturaItem.setValorUnitario(totalValorCalculado.divide(volume, ESCALA_VALOR_UNITARIO, BigDecimal.ROUND_HALF_EVEN));
		faturaItem.setQuantidade(volume);
		faturaItem.setMedidaConsumo(volume);
		faturaItem.setNumeroSequencial(1);

		return faturaItem;
	}

	/**
	 * Obter nota credito debito.
	 *
	 * @param cliente the cliente
	 * @param pontoConsumo the ponto consumo
	 * @param rubrica the rubrica
	 * @param contrato the contrato
	 * @param contratoAtual the contrato atual
	 * @param periodoApuracao the periodo apuracao
	 * @param dadosAuditoria the dados auditoria
	 * @return the fatura
	 * @throws NegocioException the negocio exception
	 */
	private Fatura obterNotaCreditoDebito(Cliente cliente, PontoConsumo pontoConsumo, Rubrica rubrica, Contrato contrato,
			Contrato contratoAtual, Integer periodoApuracao, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorCobranca controladorCobranca =
				(ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		ControladorArrecadacao controladorArrecadacao =
				(ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Integer quantidadeDiasVencimento = Integer.valueOf(
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_NUMERO_DIAS_MINIMO_EMISSAO_VENCIMENTO).getValor());

		Fatura notaDebitoCredito = (Fatura) controladorFatura.criar();
		notaDebitoCredito.setDadosAuditoria(dadosAuditoria);
		notaDebitoCredito.setDataEmissao(Util.getDataCorrente(false));
		notaDebitoCredito.setCliente(cliente);
		notaDebitoCredito.setValorConciliado(BigDecimal.ZERO);
		notaDebitoCredito.setDataVencimento(new DateTime().plusDays(quantidadeDiasVencimento).toDate());

		if (pontoConsumo != null) {
			notaDebitoCredito.setPontoConsumo(pontoConsumo);
			notaDebitoCredito.setSegmento(pontoConsumo.getSegmento());
		}

		Long codigoTipoDebito = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO));
		Long codigoTipoCredito = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO));
		Long codigoTipoDebitoPenalidade =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO_PENALIDADE));

		LancamentoItemContabil lancamentoItemContabil = rubrica.getLancamentoItemContabil();

		if (lancamentoItemContabil.getTipoCreditoDebito().getChavePrimaria() == codigoTipoDebito) {

			Long codigoTipoDocumentoNotasDebito =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
			notaDebitoCredito.setTipoDocumento(controladorArrecadacao.obterTipoDocumento(codigoTipoDocumentoNotasDebito));

		} else if (lancamentoItemContabil.getTipoCreditoDebito().getChavePrimaria() == codigoTipoCredito) {

			Long codigoTipoDocumentoNotasCredito =
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));
			notaDebitoCredito.setTipoDocumento(controladorArrecadacao.obterTipoDocumento(codigoTipoDocumentoNotasCredito));

		} else if (lancamentoItemContabil.getTipoCreditoDebito().getChavePrimaria() == codigoTipoDebitoPenalidade) {

			Long codigoTipoDocumentoNotasDebitoPenalidade = Long.valueOf(
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO_PENALIDADE));
			notaDebitoCredito.setTipoDocumento(controladorArrecadacao.obterTipoDocumento(codigoTipoDocumentoNotasDebitoPenalidade));

		}

		Long idSituacaoNormal = Long.valueOf(
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL));
		CreditoDebitoSituacao creditoDebitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(idSituacaoNormal);
		notaDebitoCredito.setCreditoDebitoSituacao(creditoDebitoSituacao);

		Long idSituacaoPendente =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));

		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obterEntidadeConteudo(idSituacaoPendente);
		notaDebitoCredito.setSituacaoPagamento(situacaoPagamento);
		notaDebitoCredito.setCobrada(Boolean.TRUE);

		notaDebitoCredito.setContrato(contrato);
		notaDebitoCredito.setContratoAtual(contratoAtual);
		notaDebitoCredito.setObservacaoNota(this.obterObservacaoNotaDebitoCredito(periodoApuracao));
		notaDebitoCredito.setProvisaoDevedoresDuvidosos(Boolean.FALSE);

		return notaDebitoCredito;
	}

	/**
	 * Obter observacao nota debito credito.
	 *
	 * @param periodoApuracao the periodo apuracao
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String obterObservacaoNotaDebitoCredito(Integer periodoApuracao) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		String retorno = "";
		String periodo = String.valueOf(periodoApuracao);

		if (periodo.length() == PERIODO_MES_ANO) {

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, Integer.parseInt(periodo.substring(0, INDICE_FINAL_ANO)));
			cal.set(Calendar.MONTH, Integer.parseInt(periodo.substring(INDICE_FINAL_ANO, INDICE_FINAL_MES)) - 1);

			String dataInicio = Util.obterPrimeiroDiaMes(cal.getTime(), Constantes.FORMATO_DATA_BR);
			String dataFim = Util.obterUltimoDiaMes(cal.getTime(), Constantes.FORMATO_DATA_BR);

			retorno = dataInicio + " à " + dataFim;

		} else if (periodo.length() == PERIODO_INICIO_FIM) {

			retorno = dateFormat.format(Util.obterPrimeiroDiaMes(periodoApuracao)) + " à "
					+ dateFormat.format(Util.obterUltimoDiaMes(periodoApuracao));

		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #atualizarApuracaoQtdPenalidadePeriod(java.lang .Long,
	 * java.lang.Long)
	 */
	@Override
	public void atualizarApuracaoQtdPenalidadePeriod(Long chaveApuracao, Long chaveFatura) throws NegocioException, ConcorrenciaException {

		ControladorFatura controladorFatura =
				(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Fatura fatura = (Fatura) controladorFatura.obter(chaveFatura);
		ApuracaoQuantidadePenalidadePeriodicidade apuracao = this.obterApuracaoQuantidadePenalidadePeriodicidade(chaveApuracao);
		apuracao.setFatura(fatura);

		this.atualizar(apuracao, ApuracaoQuantidadePenalidadePeriodicidadeImpl.class);
	}

	/**
	 * Validar aplicar penalidade.
	 *
	 * @param apuracao the apuracao
	 * @throws NegocioException the negocio exception
	 */
	private void validarAplicarPenalidade(ApuracaoQuantidadePenalidadePeriodicidade apuracao) throws NegocioException {

		if (apuracao.getPercentualDescontoAplicado() != null) {

			if (apuracao.getValorDescontoAplicado() == null) {
				throw new NegocioException(ERRO_NEGOCIO_APURACAO_PERCENTUAL_DESCONTO_SEM_VALOR_DESCONTO, true);
			}

			if (apuracao.getObservacoes() == null || StringUtils.isEmpty(apuracao.getObservacoes())) {

				throw new NegocioException(ERRO_NEGOCIO_OBSERVACAO_OBRIGATORIO, true);
			}

		}

		if (apuracao.getValorDescontoAplicado() != null && apuracao.getPercentualDescontoAplicado() == null) {
			throw new NegocioException(ERRO_NEGOCIO_APURACAO_VALOR_DESCONTO_SEM_PERCENTUAL_DESCONTO, true);
		}

		if ((apuracao.getQtdaNaoRetirada() == null) && (apuracao.getQtdaDiariaRetiradaMaior() == null)) {

			throw new NegocioException(ERRO_NEGOCIO_APURACAO_SEM_VOLUME, new Object[] { apuracao.getChavePrimaria() });

		}

		if (apuracao.getValorCalculado() == null) {

			throw new NegocioException(ERRO_NEGOCIO_APURACAO_SEM_VALOR_CALCULADO, new Object[] { apuracao.getChavePrimaria() });

		}

		if (apuracao.getValorTarifaMedia() == null) {

			throw new NegocioException(ERRO_NEGOCIO_APURACAO_SEM_VALOR_MEDIA_TARIFA, new Object[] { apuracao.getChavePrimaria() });

		}

	}

	/**
	 * Checks if is AQPP igual.
	 *
	 * @param aqpp1 the aqpp1
	 * @param aqpp2 the aqpp2
	 * @return true, if is AQPP igual
	 */
	public boolean isAQPPIgual(ApuracaoQuantidadePenalidadePeriodicidade aqpp1, ApuracaoQuantidadePenalidadePeriodicidade aqpp2) {

		boolean retorno = Boolean.FALSE;
		if (aqpp1.getContrato().getChavePrimaria() == aqpp2.getContrato().getChavePrimaria()
				&& ((aqpp1.getContratoModalidade() == null && aqpp2.getContratoModalidade() == null)
						|| (aqpp1.getContratoModalidade() != null && aqpp2.getContratoModalidade() != null
								&& aqpp1.getContratoModalidade().getChavePrimaria() == aqpp2.getContratoModalidade().getChavePrimaria()))
				&& aqpp1.getPenalidade().getChavePrimaria() == aqpp2.getPenalidade().getChavePrimaria()
				&& ((aqpp1.getPontoConsumo() == null && aqpp2.getPontoConsumo() == null)
						|| aqpp1.getPontoConsumo().getChavePrimaria() == aqpp2.getPontoConsumo().getChavePrimaria())
				&& aqpp1.getPeriodicidadePenalidade().getChavePrimaria() == aqpp2.getPeriodicidadePenalidade().getChavePrimaria()
				&& aqpp1.getPeriodoApuracao().intValue() == aqpp2.getPeriodoApuracao().intValue()
				&& aqpp1.getDataInicioApuracao().equals(aqpp2.getDataInicioApuracao())
				&& aqpp1.getDataFimApuracao().equals(aqpp2.getDataFimApuracao())) {

			retorno = Boolean.TRUE;
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #inserirApuracao(br.com.ggas.faturamento
	 * .apuracaopenalidade.ApuracaoVO, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void inserirApuracao(ApuracaoVO apuracaoVO, DadosAuditoria dadosAuditoria,
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesNaoCobradas,
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesCobradas, Contrato contrato,
			Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal, long idPeriodicidade) throws NegocioException {

		if (apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade() != null) {

			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : apuracaoVO
					.getListaApuracaoQuantidadePenalidadePeriodicidade()) {
				if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade() != null) {
					if (listaPenalidadesNaoCobradas != null && !listaPenalidadesNaoCobradas.isEmpty()) {

						for (ApuracaoQuantidadePenalidadePeriodicidade aqppNC : listaPenalidadesNaoCobradas) {

							if (this.isAQPPIgual(apuracaoQuantidadePenalidadePeriodicidade, aqppNC)) {
								aqppNC.setDadosAuditoria(dadosAuditoria);
								this.removerApuracaoQuantidadePenalidadePeriodicidade(aqppNC.getChavePrimaria(), dadosAuditoria);
								break;
							}

						}

					}

					boolean isCobrada = Boolean.FALSE;

					if (listaPenalidadesCobradas != null && !listaPenalidadesCobradas.isEmpty()) {
						for (ApuracaoQuantidadePenalidadePeriodicidade aqppC : listaPenalidadesCobradas) {

							if (this.isAQPPIgual(apuracaoQuantidadePenalidadePeriodicidade, aqppC)) {
								isCobrada = Boolean.TRUE;
								break;
							}
						}
					}

					if (!isCobrada) {
						apuracaoQuantidadePenalidadePeriodicidade.setDadosAuditoria(dadosAuditoria);
						this.inserir(apuracaoQuantidadePenalidadePeriodicidade);
					}
				}
			}
		}

		if (apuracaoVO.getListaApuracaoQuantidade() != null) {
			if (idPeriodicidade == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL) {
				this.removerApuracaoQuantidade(contrato, listaPontoConsumo, dataInicial, dataFinal, dadosAuditoria);
			}
			for (ApuracaoQuantidade apuracaoQuantidade : apuracaoVO.getListaApuracaoQuantidade()) {
				apuracaoQuantidade.setDadosAuditoria(dadosAuditoria);
				this.inserir(apuracaoQuantidade);
			}
			getSession().flush();
		}

	}

	/**
	 * Remover apuracao quantidade penalidade periodicidade.
	 *
	 * @param chavePrimaria the chave primaria
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	private void removerApuracaoQuantidadePenalidadePeriodicidade(Long chavePrimaria, DadosAuditoria dadosAuditoria)
			throws NegocioException {

		ApuracaoQuantidadePenalidadePeriodicidade apuracao = this.obterApuracaoQuantidadePenalidadePeriodicidade(chavePrimaria);

		apuracao.setDadosAuditoria(dadosAuditoria);

		this.remover(apuracao);

	}

	/**
	 * Remover apuracao quantidade.
	 *
	 * @param contrato the contrato
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param dadosAuditoria the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	private void removerApuracaoQuantidade(Contrato contrato, Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal,
			DadosAuditoria dadosAuditoria) throws NegocioException {

		Collection<ApuracaoQuantidade> listaApuracaoQuantidade = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Long[] chavesPontosConsumo = null;

		// Se for agrupamentoCobranca, não filtrar
		// por ponto de consumo.
		Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>(listaPontoConsumo);

		String codigoAgrupamentoVolume =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME);
		boolean isAgrupadoVolume = Boolean.FALSE;
		if (contrato != null && contrato.getTipoAgrupamento() != null
				&& contrato.getTipoAgrupamento().getChavePrimaria() == Long.parseLong(codigoAgrupamentoVolume)) {
			isAgrupadoVolume = Boolean.TRUE;
		}
		if (contrato != null && contrato.getAgrupamentoCobranca() && isAgrupadoVolume) {
			listaPontoConsumoAux = null;
		}

		if (listaPontoConsumoAux != null && !listaPontoConsumoAux.isEmpty()) {
			chavesPontosConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumoAux);
		}

		StringBuilder hql = new StringBuilder();
		hql.append("from ");
		hql.append(getClasseEntidadeApuracaoQuantidade().getSimpleName());
		hql.append(" apuracaoQuantidade ");
		hql.append(" where apuracaoQuantidade.contrato.chavePrimaria = :idContrato");
		hql.append(" and apuracaoQuantidade.dataApuracaoVolumes between :dataInicial and :dataFinal");

		if (listaPontoConsumoAux != null) {
			hql.append(" and apuracaoQuantidade.pontoConsumo.chavePrimaria in (:chavesPontosConsumo)");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (contrato != null) {
			if (contrato.getChavePrimariaPai() != null) {
				query.setLong(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimariaPai());
			} else {
				query.setLong(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
			}
		}
		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);

		if (listaPontoConsumoAux != null && !listaPontoConsumoAux.isEmpty()) {
			query.setParameterList("chavesPontosConsumo", chavesPontosConsumo);
		}

		listaApuracaoQuantidade = query.list();

		for (ApuracaoQuantidade apuracaoQuantidade : listaApuracaoQuantidade) {
			apuracaoQuantidade.setDadosAuditoria(dadosAuditoria);
			this.remover(apuracaoQuantidade, getClasseEntidadeApuracaoQuantidade());
		}
		getSession().flush();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #
	 * validarFiltroPesquisaPenalidadesExtratoExecucaoContrato (java.util.Map)
	 */
	@Override
	public void validarFiltroPesquisaPenalidadesExtratoExecucaoContrato(Map<String, Object> filtro) throws NegocioException {

		if (filtro.get("tipoConsulta") == null) {
			throw new NegocioException(ERRO_NEGOCIO_TIPO_CONSULTA_OBRIGATORIO, true);
		}

		Date dataInicio = (Date) filtro.get("dataApuracaoInicio");
		Date dataFim = (Date) filtro.get("dataApuracaoFim");

		if (dataInicio != null && dataFim == null) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO, true);

		} else if (dataFim == null && dataInicio != null) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_OBRIGATORIO, true);

		} else if (dataFim == null && dataInicio == null) {

			throw new NegocioException(ERRO_NEGOCIO_INTERVALO_DATAS_OBRIGATORIO, true);

		}

		if (Util.compararDatas(dataInicio, dataFim) > 0) {
			throw new NegocioException(ControladorApuracaoPenalidade.ERRO_NEGOCIO_DATA_INICIAL_MAIOR_CONTRATO, dataFim);
		}

		int intervaloMaximo = Util.intervaloDatas(dataInicio, dataFim);
		if (intervaloMaximo > DIAS_ANO) {
			throw new NegocioException(ERRO_NEGOCIO_INTERVALO_DATAS_MAIOR_QUE_UM_ANO, true);
		}

		if (QuantidadeVolumesVO.CHAVE_APURACAO.equals(filtro.get("tipoConsulta"))
				&& filtro.get(PARAM_NAME_ID_CONTRATO_MODALIDADE) == null) {
			throw new NegocioException(ERRO_NEGOCIO_CONTRATO_MODALIDADE_OBRIGATORIO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #obterListaPenalidasPorContrato(java.lang.Long )
	 */
	@Override
	public Collection<ContratoPontoConsumoPenalidade> obterListaPenalidasPorContrato(Long idContrato) {

		// Modificar esta consulta para só
		// retornar as penalidades que estão
		// vigentes no período da apuração. A data
		// do aditivo do contrato deve ser menor
		// ou igual a data final do período de
		// apuração. Se não houver aditivo, é a
		// data da assinatura.

		StringBuilder hql = new StringBuilder();
		hql.append("select distinct contratoPontoConsumoPenalidade ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoPenalidade().getSimpleName());
		hql.append(" contratoPontoConsumoPenalidade ");
		hql.append(" inner join contratoPontoConsumoPenalidade.contratoPontoConsumoModalidade contratoPontoConsumoModalidade ");
		hql.append(" inner join contratoPontoConsumoModalidade.contratoPontoConsumo contratoPontoConsumo  ");
		hql.append(" inner join contratoPontoConsumo.contrato contrato ");
		hql.append(" inner join contratoPontoConsumoPenalidade.penalidade penalidade ");
		hql.append(" inner join fetch contratoPontoConsumoPenalidade.periodicidadePenalidade periodicidadePenalidade ");
		hql.append(" where contrato.chavePrimaria = :idContrato");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_NAME_ID_CONTRATO, idContrato);

		return query.list();
	}
	
	@Override
	@Cacheable("penalidadesPorContrato")
	public Long contarPenalidadesPorContrato(Long idContrato) {

		// Modificar esta consulta para só
		// retornar as penalidades que estão
		// vigentes no período da apuração. A data
		// do aditivo do contrato deve ser menor
		// ou igual a data final do período de
		// apuração. Se não houver aditivo, é a
		// data da assinatura.

		StringBuilder hql = new StringBuilder();
		hql.append("select count(distinct contratoPontoConsumoPenalidade) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoPenalidade().getSimpleName());
		hql.append(" contratoPontoConsumoPenalidade ");
		hql.append(" inner join contratoPontoConsumoPenalidade.contratoPontoConsumoModalidade contratoPontoConsumoModalidade ");
		hql.append(" inner join contratoPontoConsumoModalidade.contratoPontoConsumo contratoPontoConsumo  ");
		hql.append(" inner join contratoPontoConsumo.contrato contrato ");
		hql.append(" inner join contratoPontoConsumoPenalidade.penalidade penalidade ");
		hql.append(" inner join contratoPontoConsumoPenalidade.periodicidadePenalidade periodicidadePenalidade ");
		hql.append(" where contrato.chavePrimaria = :idContrato");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_NAME_ID_CONTRATO, idContrato);

		return (Long) query.uniqueResult();
	}

	/**
	 * Atualizar informacoes ship or pay.
	 *
	 * @param apuracaoVO the apuracao vo
	 * @param listaConsumoDistribuidoModalidade the lista consumo distribuido modalidade
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarInformacoesShipOrPay(ApuracaoVO apuracaoVO,
			Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade, Collection<PontoConsumo> listaPontoConsumo,
			Date dataInicial, Date dataFinal) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroShipOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_SHIP_OR_PAY);

		Penalidade penalidadeShipOrPay = controladorContrato.obterPenalidade(Long.parseLong(parametroShipOrPay));

		Long[] chavesPontosConsumo = null;
		if (listaPontoConsumo != null) {
			chavesPontosConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumo);
		}

		Collection<ParadaProgramada> listaParadasProgramadasNoPeriodo = controladorProgramacao.consultarParadasProgramadasPeriodo(
				chavesPontosConsumo, new Long[] { EntidadeConteudo.CHAVE_PARADA_PROGRAMADA }, dataInicial, dataFinal, null);
		Collection<ParadaProgramada> listaParadasProgramadas = new ArrayList<>();
		Collection<ParadaProgramada> listaFalhaFornecimento = controladorProgramacao.consultarParadasProgramadasPeriodo(chavesPontosConsumo,
				new Long[] { EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO }, dataInicial, dataFinal, null);
		Collection<ParadaProgramada> listaCasoFortuito = controladorProgramacao.consultarParadasProgramadasPeriodo(chavesPontosConsumo,
				new Long[] { EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO }, dataInicial, dataFinal, null);

		if (listaParadasProgramadasNoPeriodo != null) {

			for (ParadaProgramada paradaProgramada : listaParadasProgramadasNoPeriodo) {

				if (paradaProgramada.isIndicadorParadaApurada()) {
					listaParadasProgramadas.add(paradaProgramada);
				}

			}

		}

		Contrato contratoAtual = null;
		Contrato contratoPai = null;

		PontoConsumo pontoConsumo = null;

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty() && listaPontoConsumo.size() == 1) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}

		Map<ContratoModalidade, List<ContratoPontoConsumoModalidade>> mapaModalidades =
				new HashMap<>();
		List<ContratoPontoConsumoModalidade> listaModalidades = null;
		for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {
			if (mapaModalidades.containsKey(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade())) {
				listaModalidades =
						mapaModalidades.get(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade());
			} else {
				listaModalidades = new ArrayList<>();
			}
			listaModalidades.add(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade());
			mapaModalidades.put(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade(), listaModalidades);
		}

		for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {
			if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeSoP() != null) {

				EntidadeConteudo periodicidade = consumoDistribuidoModalidade.getPenalidadePeriodicidadeSoP().getPeriodicidadePenalidade();
				if (contratoAtual == null) {
					contratoAtual =
							consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoPontoConsumo().getContrato();
					if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoPontoConsumo().getContrato()
							.getChavePrimariaPai() != null) {
						contratoPai = (Contrato) controladorContrato.obter(contratoAtual.getChavePrimariaPai());
					} else {
						contratoPai = contratoAtual;
					}
				}

				BigDecimal volumeReferencia = obterConsumoReferencia(consumoDistribuidoModalidade, parametroShipOrPay, Boolean.FALSE);
				BigDecimal qtdNaoRetirada = null;
				BigDecimal qtdRecuperada = BigDecimal.ZERO;
				BigDecimal qtdNROutrasPeriodicidades = BigDecimal.ZERO;

				BigDecimal qtdNaoRetiradaParadaProgramada = BigDecimal.ZERO;
				BigDecimal qtdNaoRetiradaFalhaFornecimento = BigDecimal.ZERO;
				BigDecimal qtdNaoRetiradaCasoFortuito = BigDecimal.ZERO;

				if (listaParadasProgramadas != null && !listaParadasProgramadas.isEmpty()) {
					for (ParadaProgramada paradaProgramada : listaParadasProgramadas) {
						ContratoPontoConsumo contratoPontoConsumo =
								controladorContrato.obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo());

						// substituir o método pra pegar o valor da entidade conteudo correspondente
						BigDecimal consumo = this.calcularConsumo(mapaModalidades, consumoDistribuidoModalidade, paradaProgramada,
								contratoPontoConsumo, null);

						qtdNaoRetiradaParadaProgramada = qtdNaoRetiradaParadaProgramada.add(consumo);
					}

				}

				if (listaFalhaFornecimento != null && !listaFalhaFornecimento.isEmpty()) {
					for (ParadaProgramada paradaProgramada : listaFalhaFornecimento) {
						ContratoPontoConsumo contratoPontoConsumo =
								controladorContrato.obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo());

						BigDecimal consumo = this.calcularConsumo(mapaModalidades, consumoDistribuidoModalidade, paradaProgramada,
								contratoPontoConsumo, null);

						qtdNaoRetiradaParadaProgramada = qtdNaoRetiradaParadaProgramada.add(consumo);
					}
				}

				if (listaCasoFortuito != null && !listaCasoFortuito.isEmpty()) {
					for (ParadaProgramada paradaProgramada : listaCasoFortuito) {
						ContratoPontoConsumo contratoPontoConsumo =
								controladorContrato.obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo());

						BigDecimal consumo = this.calcularConsumo(mapaModalidades, consumoDistribuidoModalidade, paradaProgramada,
								contratoPontoConsumo, null);

						qtdNaoRetiradaParadaProgramada = qtdNaoRetiradaParadaProgramada.add(consumo);
					}
				}

				if (periodicidade.getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

					// Consultar na tabela de
					// recuperacao as que foram
					// realizadas dentro do
					// periodo (RECUPERACAO)
					ContratoModalidade modalidade =
							consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();

					Collection<Recuperacao> listaRecuperacoes = this.consultarRecuperacoes(contratoPai, pontoConsumo, penalidadeShipOrPay,
							modalidade, periodicidade, dataInicial, dataFinal);

					for (Recuperacao recuperacao : listaRecuperacoes) {
						qtdRecuperada = qtdRecuperada.add(recuperacao.getMedicaoQR());
					}

					// Consultar os volumes pagos
					// de penalidades geradas para
					// o
					// periodo de apuracao com
					// periodicidade mensal
					// (PENALIDADES)
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades = this.consultarApuracaoPenalidades(contratoPai,
							pontoConsumo, penalidadeShipOrPay, modalidade, periodicidade, dataInicial, dataFinal);

					for (ApuracaoQuantidadePenalidadePeriodicidade penalidade : listaPenalidades) {
						if (penalidade.getQtdaPagaNaoRetirada() != null) {
							qtdNROutrasPeriodicidades = qtdNROutrasPeriodicidades.add(penalidade.getQtdaPagaNaoRetirada());
						}
					}
				}

				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade =
						consumoDistribuidoModalidade.getPenalidadePeriodicidadeSoP();

				BigDecimal qtdNaoRetiradaParadas =
						qtdNaoRetiradaParadaProgramada.add(qtdNaoRetiradaFalhaFornecimento).add(qtdNaoRetiradaCasoFortuito);
				apuracaoVO.setQuantidadeVolumeParadas(qtdNaoRetiradaParadas);
				BigDecimal volumeQL = volumeReferencia.multiply(contratoPontoConsumoPenalidade.getPercentualMargemVariacao())
						.subtract(qtdNaoRetiradaParadas);
				BigDecimal volumeQDR = consumoDistribuidoModalidade.getVolumeQDR();

				qtdNaoRetirada = volumeQL.subtract(volumeQDR).add(qtdRecuperada).subtract(qtdNROutrasPeriodicidades);

				List<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new ArrayList<>();
				listaContratoPontoConsumoModalidade.add(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade());
				if (qtdNaoRetirada.compareTo(BigDecimal.ZERO) > 0) {

					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
							this.criarApuracaoQuantidadePenalidadePeriodicidade();
					apuracaoQtdPenalPeriod.setContrato(contratoPai);
					apuracaoQtdPenalPeriod.setContratoAtual(contratoAtual);
					apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
					apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidade);

					if (periodicidade.getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {
						DateTime data = new DateTime(dataInicial);
						apuracaoQtdPenalPeriod.setPeriodoApuracao(data.getYear());
					} else {
						apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataInicial));
					}

					apuracaoQtdPenalPeriod.setDataInicioApuracao(dataInicial);
					apuracaoQtdPenalPeriod.setDataFimApuracao(dataFinal);
					apuracaoQtdPenalPeriod.setContratoModalidade(
							consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade());
					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorParadaProgramada(qtdNaoRetiradaParadaProgramada);
					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorFalhaFornecimento(qtdNaoRetiradaFalhaFornecimento);
					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorCasoFortuito(qtdNaoRetiradaCasoFortuito);
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramadaMedia(consumoDistribuidoModalidade.getVolumeQDPMedia());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratadaMedia(consumoDistribuidoModalidade.getVolumeQDCMedia());
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(consumoDistribuidoModalidade.getVolumeQDP());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratada(consumoDistribuidoModalidade.getVolumeQDC());
					apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(volumeQDR);
					apuracaoQtdPenalPeriod.setQtdaLimite(volumeQL);
					apuracaoQtdPenalPeriod.setQtdaNaoRetirada(qtdNaoRetirada);
					apuracaoQtdPenalPeriod.setPenalidade(penalidadeShipOrPay);

					apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
				}
			}
		}
	}

	/**
	 * Atualizar informacoes take or pay.
	 *
	 * @param apuracaoVO the apuracao vo
	 * @param listaConsumoDistribuidoModalidade the lista consumo distribuido modalidade
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @param contrato the contrato
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarInformacoesTakeOrPay(ApuracaoVO apuracaoVO,
			Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade, Collection<PontoConsumo> listaPontoConsumo,
			Date dataInicial, Date dataFinal, Contrato contrato) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

		Penalidade penalidadeTakeOrPay = controladorContrato.obterPenalidade(Long.parseLong(parametroTakeOrPay));

		Long[] chavesPontosConsumo = null;
		if (listaPontoConsumo != null) {
			chavesPontosConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumo);
		}

		Collection<ParadaProgramada> listaParadasProgramadasNoPeriodo = controladorProgramacao.consultarParadasProgramadasPeriodo(
				chavesPontosConsumo, new Long[] { EntidadeConteudo.CHAVE_PARADA_PROGRAMADA }, dataInicial, dataFinal, null);
		Collection<ParadaProgramada> listaParadasProgramadas = new ArrayList<>();
		Collection<ParadaProgramada> listaFalhaFornecimento = controladorProgramacao.consultarParadasProgramadasPeriodo(chavesPontosConsumo,
				new Long[] { EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO }, dataInicial, dataFinal, null);
		Collection<ParadaProgramada> listaCasoFortuito = controladorProgramacao.consultarParadasProgramadasPeriodo(chavesPontosConsumo,
				new Long[] { EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO }, dataInicial, dataFinal, null);

		if (listaParadasProgramadasNoPeriodo != null) {

			for (ParadaProgramada paradaProgramada : listaParadasProgramadasNoPeriodo) {

				if (paradaProgramada.isIndicadorParadaApurada()) {
					listaParadasProgramadas.add(paradaProgramada);
				}

			}

		}

		Contrato contratoAtual = null;
		Contrato contratoPai = null;

		PontoConsumo pontoConsumo = null;

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty() && listaPontoConsumo.size() == 1) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}

		Map<ContratoModalidade, List<ContratoPontoConsumoModalidade>> mapaModalidades =
				new HashMap<>();
		List<ContratoPontoConsumoModalidade> listaModalidades = null;
		for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {
			if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null && mapaModalidades
					.containsKey(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade())) {
				listaModalidades =
						mapaModalidades.get(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade());
			} else {
				listaModalidades = new ArrayList<>();
			}
			if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
				listaModalidades.add(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade());
				mapaModalidades.put(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade(),
						listaModalidades);
			}
		}

		for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {

			if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP() != null) {

				EntidadeConteudo periodicidade = consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP().getPeriodicidadePenalidade();
				if (contratoAtual == null) {
					contratoAtual = contrato;
					if (contrato.getChavePrimariaPai() != null) {
						contratoPai = (Contrato) controladorContrato.obter(contratoAtual.getChavePrimariaPai());
					} else {
						contratoPai = contratoAtual;
					}
				}

				BigDecimal volumeReferencia = this.obterConsumoReferencia(consumoDistribuidoModalidade, parametroTakeOrPay, Boolean.FALSE);
				BigDecimal qtdNaoRetirada = null;
				BigDecimal qtdRecuperada = BigDecimal.ZERO;
				BigDecimal qtdNROutrasPeriodicidades = BigDecimal.ZERO;

				BigDecimal qtdNaoRetiradaParadaProgramada = BigDecimal.ZERO;
				BigDecimal qtdNaoRetiradaFalhaFornecimento = BigDecimal.ZERO;
				BigDecimal qtdNaoRetiradaCasoFortuito = BigDecimal.ZERO;

				if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP().getConsideraParadaProgramada()
						&& listaParadasProgramadas != null && !listaParadasProgramadas.isEmpty()) {

					for (ParadaProgramada paradaProgramada : listaParadasProgramadas) {
						ContratoPontoConsumo contratoPontoConsumo =
								controladorContrato.obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo());

						if (contratoPontoConsumo == null) {
							contratoPontoConsumo = controladorContrato
									.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
						}

						BigDecimal consumo = this.calcularQFConsumo(mapaModalidades, consumoDistribuidoModalidade, paradaProgramada,
								contratoPontoConsumo, contratoPai, pontoConsumo, apuracaoVO);

						qtdNaoRetiradaParadaProgramada = qtdNaoRetiradaParadaProgramada.add(consumo);
					}

				}

				if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP().getConsideraFalhaFornecimento()
						&& listaFalhaFornecimento != null && !listaFalhaFornecimento.isEmpty()) {

					for (ParadaProgramada paradaProgramada : listaFalhaFornecimento) {
						ContratoPontoConsumo contratoPontoConsumo =
								controladorContrato.obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo());

						if (contratoPontoConsumo == null) {
							contratoPontoConsumo = controladorContrato
									.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
						}

						BigDecimal consumo = this.calcularConsumo(mapaModalidades, consumoDistribuidoModalidade, paradaProgramada,
								contratoPontoConsumo, apuracaoVO);

						qtdNaoRetiradaFalhaFornecimento = qtdNaoRetiradaFalhaFornecimento.add(consumo);
					}

				}

				if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP().getConsideraCasoFortuito() && listaCasoFortuito != null
						&& !listaCasoFortuito.isEmpty()) {

					for (ParadaProgramada paradaProgramada : listaCasoFortuito) {
						ContratoPontoConsumo contratoPontoConsumo =
								controladorContrato.obterContratoAtivoPontoConsumo(paradaProgramada.getPontoConsumo());

						if (contratoPontoConsumo == null) {
							contratoPontoConsumo = controladorContrato
									.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
						}

						BigDecimal consumo = this.calcularConsumo(mapaModalidades, consumoDistribuidoModalidade, paradaProgramada,
								contratoPontoConsumo, apuracaoVO);

						qtdNaoRetiradaCasoFortuito = qtdNaoRetiradaCasoFortuito.add(consumo);
					}

				}

				if (periodicidade.getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

					// Consultar na tabela de
					// recuperacao as que foram
					// realizadas dentro do
					// periodo (RECUPERACAO)
					ContratoModalidade modalidade = null;
					if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
						modalidade = consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();
					}

					Collection<Recuperacao> listaRecuperacoes = this.consultarRecuperacoes(contratoPai, pontoConsumo, penalidadeTakeOrPay,
							modalidade, periodicidade, dataInicial, dataFinal);

					for (Recuperacao recuperacao : listaRecuperacoes) {
						qtdRecuperada = qtdRecuperada.add(recuperacao.getMedicaoQR());
					}

					// Consultar os volumes pagos
					// de penalidades geradas para
					// o
					// periodo de apuracao com
					// periodicidade mensal
					// (PENALIDADES)
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades = consultarApuracaoPenalidades(contratoPai,
							pontoConsumo, penalidadeTakeOrPay, modalidade, periodicidade, dataInicial, dataFinal);

					for (ApuracaoQuantidadePenalidadePeriodicidade penalidade : listaPenalidades) {
						if (penalidade.getQtdaPagaNaoRetirada() != null) {
							qtdNROutrasPeriodicidades = qtdNROutrasPeriodicidades.add(penalidade.getQtdaPagaNaoRetirada());
						} else if (penalidade.getQtdaNaoRetirada() != null) {
							qtdNROutrasPeriodicidades = qtdNROutrasPeriodicidades.add(penalidade.getQtdaNaoRetirada());
						}
					}
				}

				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade =
						consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP();

				BigDecimal qtdNaoRetiradaParadas =
						qtdNaoRetiradaParadaProgramada.add(qtdNaoRetiradaFalhaFornecimento).add(qtdNaoRetiradaCasoFortuito);
				BigDecimal volumeQL = volumeReferencia.multiply(contratoPontoConsumoPenalidade.getPercentualMargemVariacao())
						.subtract(qtdNaoRetiradaParadas);
				BigDecimal volumeQDR = consumoDistribuidoModalidade.getVolumeQDR();

				qtdNaoRetirada = volumeQL.subtract(volumeQDR).add(qtdRecuperada).subtract(qtdNROutrasPeriodicidades);

				List<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade = new ArrayList<>();
				if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
					listaContratoPontoConsumoModalidade.add(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade());
				}

				if (qtdNaoRetirada.compareTo(BigDecimal.ZERO) > 0) {

					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
							this.criarApuracaoQuantidadePenalidadePeriodicidade();
					apuracaoQtdPenalPeriod.setContrato(contratoPai);
					apuracaoQtdPenalPeriod.setContratoAtual(contratoAtual);
					apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
					apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidade);

					if (periodicidade.getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {
						DateTime data = new DateTime(dataInicial);
						apuracaoQtdPenalPeriod.setPeriodoApuracao(data.getYear());
					} else {
						apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataInicial));
					}

					apuracaoQtdPenalPeriod.setDataInicioApuracao(dataInicial);
					apuracaoQtdPenalPeriod.setDataFimApuracao(dataFinal);
					if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
						apuracaoQtdPenalPeriod.setContratoModalidade(
								consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade());
					}

					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorParadaProgramada(qtdNaoRetiradaParadaProgramada);
					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorFalhaFornecimento(qtdNaoRetiradaFalhaFornecimento);
					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorCasoFortuito(qtdNaoRetiradaCasoFortuito);

					apuracaoQtdPenalPeriod.setQtdaDiariaProgramadaMedia(consumoDistribuidoModalidade.getVolumeQDPMedia());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratadaMedia(consumoDistribuidoModalidade.getVolumeQDCMedia());
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(consumoDistribuidoModalidade.getVolumeQDP());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratada(consumoDistribuidoModalidade.getVolumeQDC());

					apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(volumeQDR);
					apuracaoQtdPenalPeriod.setQtdaLimite(volumeQL);
					BigDecimal qnrGrupoEconomico = this.obterQNRGrupoEconomicoPorPontoConsumo(pontoConsumo);
					if (qnrGrupoEconomico != null) {
						apuracaoQtdPenalPeriod.setQtdaNaoRetirada(qnrGrupoEconomico);
					} else {
						apuracaoQtdPenalPeriod.setQtdaNaoRetirada(qtdNaoRetirada);
					}
					apuracaoQtdPenalPeriod.setPenalidade(penalidadeTakeOrPay);

					apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
				}
			}
		}
	}

	/**
	 * Calcular qf consumo.
	 *
	 * @param mapaModalidades the mapa modalidades
	 * @param consumoDistribuidoModalidade the consumo distribuido modalidade
	 * @param paradaProgramada the parada programada
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param apuracaoVO the apuracao vo
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal calcularQFConsumo(Map<ContratoModalidade, List<ContratoPontoConsumoModalidade>> mapaModalidades,
			ConsumoDistribuidoModalidade consumoDistribuidoModalidade, ParadaProgramada paradaProgramada,
			ContratoPontoConsumo contratoPontoConsumo, Contrato contrato, PontoConsumo pontoConsumo, ApuracaoVO apuracaoVO)
			throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String codigoConsumoReferenciaQFMaiorDentreQDPQDR =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_REFERENCIA_QF_MAIOR_QDP_QDR);

		String codigoConsumoReferenciaQDR = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_REFERENCIA_QF_QDR);

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP();
		BigDecimal consumoQF = BigDecimal.ZERO;

		BigDecimal qdcDoDia = obterQuantidadeDiariaContratada(paradaProgramada.getDataParada(), contratoPontoConsumo);
		if (qdcDoDia == null) {
			qdcDoDia = controladorContrato.obterQdcContratoValidoPorData(paradaProgramada.getDataParada(), contrato.getChavePrimaria());
		}

		BigDecimal qdpDoDia = BigDecimal.ZERO;
		if (consumoDistribuidoModalidade.getContratoPontoConsumoModalidade() != null) {
			ContratoModalidade contratoModalidade =
					consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();
			qdpDoDia = this.obterQDP(mapaModalidades.get(contratoModalidade), paradaProgramada.getDataParada());
		}

		if (qdpDoDia.compareTo(BigDecimal.ZERO)==0) {
			qdpDoDia = this.obterQDPContrato(contrato, pontoConsumo, paradaProgramada.getDataParada());
			qdcDoDia = qdpDoDia;
		}

		MultiKeyMap balde = apuracaoVO.getBalde();
		BigDecimal quantidadeDiariaRetirada =
				(BigDecimal) balde.get(consumoDistribuidoModalidade, new Date(paradaProgramada.getDataParada().getTime()));

		if (contratoPontoConsumoPenalidade.getReferenciaQFParadaProgramada().getChavePrimaria() == Long
				.parseLong(codigoConsumoReferenciaQFMaiorDentreQDPQDR)) {

			if (quantidadeDiariaRetirada.compareTo(paradaProgramada.getVolumeFornecimento()) >= 0) {
				consumoQF = qdcDoDia.subtract(quantidadeDiariaRetirada);
			} else {
				consumoQF = qdcDoDia.subtract(paradaProgramada.getVolumeFornecimento());
			}

		} else if (contratoPontoConsumoPenalidade.getReferenciaQFParadaProgramada().getChavePrimaria() == Long
				.parseLong(codigoConsumoReferenciaQDR)) {
			consumoQF = quantidadeDiariaRetirada;
		}

		return consumoQF;
	}

	/**
	 * Calcular consumo.
	 *
	 * @param mapaModalidades the mapa modalidades
	 * @param consumoDistribuidoModalidade the consumo distribuido modalidade
	 * @param paradaProgramada the parada programada
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param apuracaoVO the apuracao vo
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal calcularConsumo(Map<ContratoModalidade, List<ContratoPontoConsumoModalidade>> mapaModalidades,
			ConsumoDistribuidoModalidade consumoDistribuidoModalidade, ParadaProgramada paradaProgramada,
			ContratoPontoConsumo contratoPontoConsumo, ApuracaoVO apuracaoVO) throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoConsumoReferenciaPenalidadeQDC =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_QDC);

		String codigoConsumoReferenciaPenalidadeQDP =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONSUMO_REFERENCIA_QDP);

		String codigoFormaApuracaoParadaCasoFortuitoQDC =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDC);

		String codigoFormaApuracaoParadaCasoFortuitoQDPRET =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_APURACAO_PARADA_CASO_FORTUITO_QDP_RET);

		String codigoFormaApuracaoParadaFalhaFornecimentoQDPRET = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_APURACAO_PARADA_FALHA_FORNECIMENTO_QDP_RET);

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = consumoDistribuidoModalidade.getPenalidadePeriodicidadeToP();
		BigDecimal consumo = null;

		if (contratoPontoConsumoPenalidade.getConsumoReferencia().getChavePrimaria() == Long
				.parseLong(codigoConsumoReferenciaPenalidadeQDC)) {

			consumo = this.obterQuantidadeDiariaContratada(paradaProgramada.getDataParada(), contratoPontoConsumo);
			if (consumo == null) {
				consumo = controladorContrato.obterQdcContratoValidoPorData(paradaProgramada.getDataParada(),
						contratoPontoConsumo.getContrato().getChavePrimaria());
			}

		} else if (contratoPontoConsumoPenalidade.getConsumoReferencia().getChavePrimaria() == Long
				.parseLong(codigoConsumoReferenciaPenalidadeQDP)) {

			ContratoModalidade contratoModalidade =
					consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();
			consumo = this.obterQDP(mapaModalidades.get(contratoModalidade), paradaProgramada.getDataParada());

		} else {
			BigDecimal qdcDoDia = obterQuantidadeDiariaContratada(paradaProgramada.getDataParada(), contratoPontoConsumo);
			if (qdcDoDia == null) {
				qdcDoDia = controladorContrato.obterQdcContratoValidoPorData(paradaProgramada.getDataParada(),
						contratoPontoConsumo.getContrato().getChavePrimaria());
			}
			ContratoModalidade contratoModalidade =
					consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();
			BigDecimal qdpDoDia = this.obterQDP(mapaModalidades.get(contratoModalidade), paradaProgramada.getDataParada());
			if (qdcDoDia.compareTo(qdpDoDia) <= 0) {
				consumo = qdcDoDia;
			} else {
				consumo = qdpDoDia;
			}

		}

		if (paradaProgramada.getTipoParada().getChavePrimaria() == EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO) {
			if (contratoPontoConsumoPenalidade.getApuracaoCasoFortuito().getChavePrimaria() == Long
					.parseLong(codigoFormaApuracaoParadaCasoFortuitoQDC)) {
				// Possivel Implementação Futura.

			} else if (contratoPontoConsumoPenalidade.getApuracaoCasoFortuito().getChavePrimaria() == Long
					.parseLong(codigoFormaApuracaoParadaCasoFortuitoQDPRET)) {
				MultiKeyMap balde = apuracaoVO.getBalde();
				BigDecimal quantidadeDiariaRetirada =
						(BigDecimal) balde.get(consumoDistribuidoModalidade, new Date(paradaProgramada.getDataParada().getTime()));
				consumo = this.obterQDP(
						mapaModalidades.get(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade()),
						paradaProgramada.getDataParada());
				consumo = consumo.subtract(quantidadeDiariaRetirada);
			}
		}

		if (paradaProgramada.getTipoParada().getChavePrimaria() == EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO
				&& contratoPontoConsumoPenalidade.getApuracaoFalhaFornecimento().getChavePrimaria() == Long
						.parseLong(codigoFormaApuracaoParadaFalhaFornecimentoQDPRET)) {
			MultiKeyMap balde = apuracaoVO.getBalde();
			BigDecimal quantidadeDiariaRetirada =
					(BigDecimal) balde.get(consumoDistribuidoModalidade, new Date(paradaProgramada.getDataParada().getTime()));
			consumo = this.obterQDP(
					mapaModalidades.get(consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade()),
					paradaProgramada.getDataParada());
			consumo = consumo.subtract(quantidadeDiariaRetirada);
		}

		return consumo;
	}

	/**
	 * Método responsável por consultar uma lista de AQPP num determinado período.
	 *
	 * @param contratoPai o contrato do(s) ponto(s) de consumo
	 * @param pontoConsumo o ponto de consumo - virá nulo se o contrato for agrupado por volume
	 * @param penalidade a penalidade para qual houve recuperação
	 * @param modalidade a modalidade para qual houve recuperação
	 * @param periodicidade a recuperação que NÃO fará parte do resultado
	 * @param dataInicial data de início do período
	 * @param dataFinal data de término do período
	 * @return lista de AQPP
	 */
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> consultarApuracaoPenalidades(Contrato contratoPai,
			PontoConsumo pontoConsumo, Penalidade penalidade, ContratoModalidade modalidade, EntidadeConteudo periodicidade,
			Date dataInicial, Date dataFinal) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade().getSimpleName());
		hql.append(" aqpp ");
		hql.append(" where aqpp.dataInicioApuracao between :dataInicio and :dataFim");
		hql.append(" and aqpp.dataFimApuracao between :dataInicio and :dataFim");

		hql.append(" and aqpp.contrato.chavePrimaria = :idContrato");
		if (pontoConsumo != null) {
			hql.append(" and aqpp.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}
		hql.append(" and aqpp.penalidade.chavePrimaria = :idPenalidade ");
		if (modalidade != null) {
			hql.append(" and aqpp.contratoModalidade.chavePrimaria = :idModalidade ");
		}
		hql.append(" and aqpp.periodicidadePenalidade.chavePrimaria != :idPeriodicidade ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataInicio", dataInicial);
		query.setDate("dataFim", dataFinal);
		query.setLong(PARAM_NAME_ID_CONTRATO, contratoPai.getChavePrimaria());
		if (pontoConsumo != null) {
			query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		}
		query.setLong("idPenalidade", penalidade.getChavePrimaria());
		if (modalidade != null) {
			query.setLong("idModalidade", modalidade.getChavePrimaria());
		}
		query.setLong("idPeriodicidade", periodicidade.getChavePrimaria());

		return query.list();
	}

	/**
	 * Método responsável por consultar uma lista de Recuperações ocorridas num período de apuração.
	 *
	 * @param contratoPai o contrato do(s) ponto(s) de consumo
	 * @param pontoConsumo o ponto de consumo - virá nulo se o contrato for agrupado por volume
	 * @param penalidade a penalidade para qual houve recuperação
	 * @param modalidade a modalidade para qual houve recuperação
	 * @param periodicidade a recuperação que NÃO fará parte do resultado
	 * @param dataInicial data de início do período
	 * @param dataFinal data de término do período
	 * @return lista de Recuperações
	 */
	private Collection<Recuperacao> consultarRecuperacoes(Contrato contratoPai, PontoConsumo pontoConsumo, Penalidade penalidade,
			ContratoModalidade modalidade, EntidadeConteudo periodicidade, Date dataInicial, Date dataFinal) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select recuperacao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeRecuperacao().getSimpleName());
		hql.append(" recuperacao ");
		hql.append(" inner join recuperacao.listaApuracaoQuantidadePenalidadePeriodicidade aqpp ");
		hql.append(" where ( recuperacao.dataRecuperacao between :dataInicio and :dataFim ) ");
		hql.append(" and aqpp.contrato.chavePrimaria = :idContrato");
		if (pontoConsumo != null) {
			hql.append(" and aqpp.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}
		hql.append(" and aqpp.penalidade.chavePrimaria = :idPenalidade ");
		if (modalidade != null) {
			hql.append(" and aqpp.contratoModalidade.chavePrimaria = :idModalidade ");
		}
		hql.append(" and aqpp.periodicidadePenalidade.chavePrimaria != :idPeriodicidade ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataInicio", dataInicial);
		query.setDate("dataFim", dataFinal);
		query.setLong(PARAM_NAME_ID_CONTRATO, contratoPai.getChavePrimaria());
		if (pontoConsumo != null) {
			query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		}
		query.setLong("idPenalidade", penalidade.getChavePrimaria());
		if (modalidade != null) {
			query.setLong("idModalidade", modalidade.getChavePrimaria());
		}
		query.setLong("idPeriodicidade", periodicidade.getChavePrimaria());

		return query.list();
	}

	/**
	 * Atualizar informacoes gas fora especificacao.
	 *
	 * @param apuracaoVO the apuracao vo
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarInformacoesGasForaEspecificacao(ApuracaoVO apuracaoVO, Contrato contrato, PontoConsumo pontoConsumo,
			Date dataInicial, Date dataFinal) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroGasForaEspecific =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_GAS_FORA_DE_ESPECIFICACAO);

		Collection<ParadaProgramada> listaParadaGasForaEspecific =
				controladorProgramacao.consultarParadasProgramadasPeriodo(new Long[] { pontoConsumo.getChavePrimaria() },
						new Long[] { EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO }, dataInicial, dataFinal, null);

		ContratoPontoConsumo contratoPontoConsumo =
				controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());

		Penalidade penalidadeGasForaEspecif = controladorContrato.obterPenalidade(Long.parseLong(parametroGasForaEspecific));

		EntidadeConteudo periodicidadeMensal =
				controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL);

		if (listaParadaGasForaEspecific != null && !listaParadaGasForaEspecific.isEmpty()) {

			for (ParadaProgramada paradaProgramada : listaParadaGasForaEspecific) {

				Date dataParada = paradaProgramada.getDataParada();
				BigDecimal qtdFaltante = null;
				BigDecimal qtdDiariaDisponibilizada = null;

				BigDecimal qtdDiariaRecuperada = this.obterConsumoDiario(pontoConsumo, dataParada);
				BigDecimal qtdDiariaProgramada = null;
				BigDecimal qtdDiariaSolicitada = null;

				Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorContrato
						.consultarSolicitacaoConsumoPontoConsumoPorPeriodo(pontoConsumo.getChavePrimaria(), dataParada, dataParada);

				BigDecimal qDCData = obterQuantidadeDiariaContratada(dataParada, contratoPontoConsumo);
				if (qDCData == null) {
					qDCData = controladorContrato.obterQdcContratoValidoPorData(dataParada,
							contratoPontoConsumo.getContrato().getChavePrimaria());
				}

				if (listaSolicitacaoConsumo != null && !listaSolicitacaoConsumo.isEmpty()) {
					qtdDiariaSolicitada = listaSolicitacaoConsumo.iterator().next().getValorQDS();
					if (qtdDiariaSolicitada == null || qtdDiariaSolicitada.compareTo(BigDecimal.ZERO)==0) {
						qtdDiariaSolicitada = qDCData;
					}
					qtdDiariaProgramada = listaSolicitacaoConsumo.iterator().next().getValorQDP();
					if (qtdDiariaProgramada == null || qtdDiariaProgramada.compareTo(BigDecimal.ZERO)==0) {
						qtdDiariaProgramada = qDCData;
					}
				} else {
					qtdDiariaSolicitada = qDCData;
					qtdDiariaProgramada = qDCData;
				}

				BigDecimal menorPressaoParada = paradaProgramada.getMenorPressao();
				Unidade unidadePressaoParada = paradaProgramada.getUnidadeMenorPressao();

				BigDecimal menorPressaoContrato = contratoPontoConsumo.getMedidaPressaoMinima();
				Unidade unidadePressaoContrato = contratoPontoConsumo.getUnidadePressaoMinima();

				if ((unidadePressaoContrato != null) && (!unidadePressaoParada.equals(unidadePressaoContrato))) {
					// converter para a mesma
					// unidade.
					menorPressaoParada = controladorHistoricoConsumo.converterUnidadeMedida(menorPressaoParada, unidadePressaoParada,
							unidadePressaoContrato);
				}

				if (((menorPressaoParada != null) && (menorPressaoContrato != null))
						&& (menorPressaoParada.compareTo(menorPressaoContrato) >= 0)) {

					if (qtdDiariaRecuperada.compareTo(qtdDiariaProgramada) > 0) {
						qtdDiariaDisponibilizada = qtdDiariaRecuperada;
					} else {
						qtdDiariaDisponibilizada = qtdDiariaProgramada;
					}
				} else {
					qtdDiariaDisponibilizada = qtdDiariaRecuperada;
				}

				Contrato contratoPai = contrato;
				if (contrato.getChavePrimariaPai() != null) {
					contratoPai = (Contrato) controladorContrato.obter(contrato.getChavePrimariaPai());
				}

				qtdFaltante = qtdDiariaSolicitada.subtract(qtdDiariaDisponibilizada);

				if (qtdFaltante.compareTo(BigDecimal.ZERO) > 0) {
					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
							this.criarApuracaoQuantidadePenalidadePeriodicidade();
					apuracaoQtdPenalPeriod.setContrato(contratoPai);
					apuracaoQtdPenalPeriod.setContratoAtual(contrato);
					apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorParadaNaoProgramada(qtdFaltante);
					apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidadeMensal);
					apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataParada));
					apuracaoQtdPenalPeriod.setDataInicioApuracao(dataParada);
					apuracaoQtdPenalPeriod.setDataFimApuracao(dataParada);
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(qtdDiariaProgramada);
					apuracaoQtdPenalPeriod.setQtdaDiariaDisponibilizada(qtdDiariaDisponibilizada);
					apuracaoQtdPenalPeriod.setPenalidade(penalidadeGasForaEspecif);
					apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
					apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(qtdDiariaRecuperada);

					apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
				}
			}
		}
	}

	/**
	 * Atualizar informacoes retirada maior menor.
	 *
	 * @param apuracaoVO the apuracao vo
	 * @param listaConsumoDistribuidoModalidade the lista consumo distribuido modalidade
	 * @param qtdDiariaRetiradaAcumulada the qtd diaria retirada acumulada
	 * @param contrato the contrato
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarInformacoesRetiradaMaiorMenor(ApuracaoVO apuracaoVO,
			Collection<ConsumoDistribuidoModalidade> listaConsumoDistribuidoModalidade, BigDecimal qtdDiariaRetiradaAcumulada,
			Contrato contrato, Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		EntidadeConteudo periodicidadeMensal =
				controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL);

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		String codPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		// Verificar se possui con

		Map<String, Object> params = new HashMap<>();

		params.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));
		params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicial);
		params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataFinal);

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));

		ContratoPenalidade contratoPenalidadeRetiradaMenor = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);
		BigDecimal percentualRetiradaMaior = null;
		BigDecimal percentualRetiradaMenor = null;
		EntidadeConteudo baseApuracaoMaior = null;
		EntidadeConteudo baseApuracaoMenor = null;
		if (contratoPenalidadeRetiradaMaior != null) {
			percentualRetiradaMaior = contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor();
			baseApuracaoMaior = contratoPenalidadeRetiradaMaior.getBaseApuracao();

		}

		if (contratoPenalidadeRetiradaMenor != null) {
			percentualRetiradaMenor = contratoPenalidadeRetiradaMenor.getValorPercentualRetMaiorMenor();
			baseApuracaoMenor = contratoPenalidadeRetiradaMenor.getBaseApuracao();
		}

		Contrato contratoPai = contrato;
		if (contrato.getChavePrimariaPai() != null) {
			contratoPai = (Contrato) controladorContrato.obter(contrato.getChavePrimariaPai());
		}

		PontoConsumo pontoConsumo = null;

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty() && listaPontoConsumo.size() == 1) {
			pontoConsumo = listaPontoConsumo.iterator().next();
		}

		String parametroPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		Penalidade penalidadeRetiradaMaior = controladorContrato.obterPenalidade(Long.parseLong(parametroPenalidadeRetiradaMaior));

		String parametroPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		Penalidade penalidadeRetiradaMenor = controladorContrato.obterPenalidade(Long.parseLong(parametroPenalidadeRetiradaMenor));
		ContratoModalidade contratoModalidadeMaior = null;
		ContratoModalidade contratoModalidadeMenor = null;
		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMaior = null;
		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMenor = null;

		for (ConsumoDistribuidoModalidade consumoDistribuidoModalidade : listaConsumoDistribuidoModalidade) {
			if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMaior() != null) {
				percentualRetiradaMaior =
						consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMaior().getValorPercentualRetMaiorMenor();
				baseApuracaoMaior = consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMaior().getBaseApuracao();
				contratoModalidadeMaior = consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();
				contratoPontoConsumoPenalidadeMaior = consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMaior();
			}
			if (consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMenor() != null) {
				percentualRetiradaMenor =
						consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMenor().getValorPercentualRetMaiorMenor();
				baseApuracaoMenor = consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMenor().getBaseApuracao();
				contratoModalidadeMenor = consumoDistribuidoModalidade.getContratoPontoConsumoModalidade().getContratoModalidade();
				contratoPontoConsumoPenalidadeMenor = consumoDistribuidoModalidade.getPenalidadePeriodicidadeRetiradaMenor();

			}

		}

		// Verifica a base de apuracao.
		if (percentualRetiradaMaior != null) {

			if (baseApuracaoMaior != null
					&& baseApuracaoMaior.getChavePrimaria() == EntidadeConteudo.CHAVE_BASE_APURACAO_PENALIDADE_DIARIA) {

				Collection<ApuracaoQuantidade> listaApuracaoQuantidade = apuracaoVO.getListaApuracaoQuantidade();

				for (ApuracaoQuantidade apuracaoQuantidade : listaApuracaoQuantidade) {

					BigDecimal volumeReferenciaRetiradaMaior = this.obterVolumeReferencia(contrato, apuracaoQuantidade.getQtdaContratada(),
							apuracaoQuantidade.getQtdaDiariaProgramada(), codPenalidadeRetiradaMaior, dataInicial, dataFinal,
							contratoPontoConsumoPenalidadeMaior);
					BigDecimal qtdLimiteRetiradaMaiorDiaria = BigDecimal.ZERO;
					
					if(volumeReferenciaRetiradaMaior != null) {
						qtdLimiteRetiradaMaiorDiaria = volumeReferenciaRetiradaMaior.multiply(percentualRetiradaMaior.add(BigDecimal.ONE));
					}

					if (apuracaoQuantidade.getQtdaDiariaRetirada().compareTo(qtdLimiteRetiradaMaiorDiaria) > 0) {

						ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
								this.criarApuracaoQuantidadePenalidadePeriodicidade();
						apuracaoQtdPenalPeriod.setContrato(contratoPai);
						apuracaoQtdPenalPeriod.setContratoAtual(contrato);
						apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
						apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(apuracaoQuantidade.getQtdaDiariaRetirada());
						apuracaoQtdPenalPeriod.setQtdaDiariaRetiradaMaior(
								apuracaoQuantidade.getQtdaDiariaRetirada().subtract(qtdLimiteRetiradaMaiorDiaria));
						apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidadeMensal);
						apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataInicial));
						apuracaoQtdPenalPeriod.setDataInicioApuracao(apuracaoQuantidade.getDataApuracaoVolumes());
						apuracaoQtdPenalPeriod.setDataFimApuracao(apuracaoQuantidade.getDataApuracaoVolumes());
						apuracaoQtdPenalPeriod.setQtdaDiariaProgramadaMedia(apuracaoQuantidade.getQtdaDiariaProgramada());
						apuracaoQtdPenalPeriod.setQtdaDiariaContratadaMedia(apuracaoQuantidade.getQtdaContratada());
						apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(apuracaoQuantidade.getQtdaDiariaProgramada());
						apuracaoQtdPenalPeriod.setQtdaDiariaContratada(apuracaoQuantidade.getQtdaContratada());
						apuracaoQtdPenalPeriod.setQtdaLimite(qtdLimiteRetiradaMaiorDiaria);
						if (contratoModalidadeMaior != null) {
							apuracaoQtdPenalPeriod.setPenalidade(penalidadeRetiradaMaior);
							apuracaoQtdPenalPeriod.setContratoModalidade(contratoModalidadeMaior);
						}

						apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
					}
				}

			} else if (baseApuracaoMaior != null
					&& baseApuracaoMaior.getChavePrimaria() == EntidadeConteudo.CHAVE_BASE_APURACAO_PENALIDADE_MENSAL) {

				BigDecimal volumeReferenciaRetiradaMaior =
						this.obterVolumeReferencia(contrato, apuracaoVO.getqDCAcumulada(), apuracaoVO.getqDPAcumulada(),
								codPenalidadeRetiradaMaior, dataInicial, dataFinal, contratoPontoConsumoPenalidadeMaior);
				BigDecimal qtdLimiteRetiradaMaiorMensal =
						volumeReferenciaRetiradaMaior.multiply(percentualRetiradaMaior.add(BigDecimal.ONE));

				if (qtdDiariaRetiradaAcumulada.compareTo(qtdLimiteRetiradaMaiorMensal) > 0) {

					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
							this.criarApuracaoQuantidadePenalidadePeriodicidade();
					apuracaoQtdPenalPeriod.setContrato(contratoPai);
					apuracaoQtdPenalPeriod.setContratoAtual(contrato);
					apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
					apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(qtdDiariaRetiradaAcumulada);
					apuracaoQtdPenalPeriod.setQtdaDiariaRetiradaMaior(qtdDiariaRetiradaAcumulada.subtract(qtdLimiteRetiradaMaiorMensal));
					apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidadeMensal);
					apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataInicial));
					apuracaoQtdPenalPeriod.setDataInicioApuracao(dataInicial);
					apuracaoQtdPenalPeriod.setDataFimApuracao(dataFinal);
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramadaMedia(apuracaoVO.getqDPMediaContratual());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratadaMedia(apuracaoVO.getqDCMediaContratual());
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(apuracaoVO.getqDPAcumulada());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratada(apuracaoVO.getqDCAcumulada());
					apuracaoQtdPenalPeriod.setQtdaLimite(qtdLimiteRetiradaMaiorMensal);

					apuracaoQtdPenalPeriod.setPenalidade(penalidadeRetiradaMaior);
					if (contratoModalidadeMaior != null) {
						apuracaoQtdPenalPeriod.setContratoModalidade(contratoModalidadeMaior);
					}

					apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
				}
			}
		}

		if (percentualRetiradaMenor != null) {

			// Verifica a base de apuracao.
			if (baseApuracaoMenor != null
					&& baseApuracaoMenor.getChavePrimaria() == EntidadeConteudo.CHAVE_BASE_APURACAO_PENALIDADE_DIARIA) {

				Collection<ApuracaoQuantidade> listaApuracaoQuantidade = apuracaoVO.getListaApuracaoQuantidade();

				for (ApuracaoQuantidade apuracaoQuantidade : listaApuracaoQuantidade) {

					BigDecimal volumeReferenciaRetiradaMenor = this.obterVolumeReferencia(contrato, apuracaoQuantidade.getQtdaContratada(),
							apuracaoQuantidade.getQtdaDiariaProgramada(), codPenalidadeRetiradaMenor, dataInicial, dataFinal,
							contratoPontoConsumoPenalidadeMenor);
					BigDecimal qtdLimiteRetiradaMenorDiaria = BigDecimal.ZERO;
							
					if (volumeReferenciaRetiradaMenor != null) {
						qtdLimiteRetiradaMenorDiaria = volumeReferenciaRetiradaMenor.multiply(percentualRetiradaMenor);
					}
							
					if (apuracaoQuantidade.getQtdaDiariaRetirada() != null
							&& apuracaoQuantidade.getQtdaDiariaRetirada().compareTo(qtdLimiteRetiradaMenorDiaria) < 0) {

						ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
								this.criarApuracaoQuantidadePenalidadePeriodicidade();
						apuracaoQtdPenalPeriod.setContrato(contratoPai);
						apuracaoQtdPenalPeriod.setContratoAtual(contrato);
						apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
						apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(apuracaoQuantidade.getQtdaDiariaRetirada());

						apuracaoQtdPenalPeriod
								.setQtdaNaoRetirada(qtdLimiteRetiradaMenorDiaria.subtract(apuracaoQuantidade.getQtdaDiariaRetirada()));
						apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidadeMensal);
						apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataInicial));
						apuracaoQtdPenalPeriod.setDataInicioApuracao(apuracaoQuantidade.getDataApuracaoVolumes());
						apuracaoQtdPenalPeriod.setDataFimApuracao(apuracaoQuantidade.getDataApuracaoVolumes());
						apuracaoQtdPenalPeriod.setQtdaDiariaProgramadaMedia(apuracaoQuantidade.getQtdaDiariaProgramada());
						apuracaoQtdPenalPeriod.setQtdaDiariaContratadaMedia(apuracaoQuantidade.getQtdaContratada());
						apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(apuracaoQuantidade.getQtdaDiariaProgramada());
						apuracaoQtdPenalPeriod.setQtdaDiariaContratada(apuracaoQuantidade.getQtdaContratada());
						apuracaoQtdPenalPeriod.setPenalidade(penalidadeRetiradaMenor);
						apuracaoQtdPenalPeriod.setQtdaLimite(qtdLimiteRetiradaMenorDiaria);
						if (contratoModalidadeMenor != null) {
							apuracaoQtdPenalPeriod.setContratoModalidade(contratoModalidadeMenor);
						}

						apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
					}

				}

			} else if (baseApuracaoMenor != null
					&& baseApuracaoMenor.getChavePrimaria() == EntidadeConteudo.CHAVE_BASE_APURACAO_PENALIDADE_MENSAL) {

				BigDecimal volumeReferenciaRetiradaMenor =
						this.obterVolumeReferencia(contrato, apuracaoVO.getqDCAcumulada(), apuracaoVO.getqDPAcumulada(),
								codPenalidadeRetiradaMenor, dataInicial, dataFinal, contratoPontoConsumoPenalidadeMenor);
				BigDecimal qtdLimiteRetiradaMenorMensal = volumeReferenciaRetiradaMenor.multiply(percentualRetiradaMenor);

				if (qtdDiariaRetiradaAcumulada.compareTo(qtdLimiteRetiradaMenorMensal) < 0) {

					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
							this.criarApuracaoQuantidadePenalidadePeriodicidade();
					apuracaoQtdPenalPeriod.setContrato(contratoPai);
					apuracaoQtdPenalPeriod.setContratoAtual(contrato);

					apuracaoQtdPenalPeriod.setQtdaNaoRetirada(qtdLimiteRetiradaMenorMensal.subtract(qtdDiariaRetiradaAcumulada));

					apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(qtdDiariaRetiradaAcumulada);
					apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidadeMensal);
					apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataInicial));
					apuracaoQtdPenalPeriod.setDataInicioApuracao(dataInicial);
					apuracaoQtdPenalPeriod.setDataFimApuracao(dataFinal);
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramadaMedia(apuracaoVO.getqDPMediaContratual());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratadaMedia(apuracaoVO.getqDCMediaContratual());
					apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(apuracaoVO.getqDPAcumulada());
					apuracaoQtdPenalPeriod.setQtdaDiariaContratada(apuracaoVO.getqDCAcumulada());
					apuracaoQtdPenalPeriod.setQtdaLimite(qtdLimiteRetiradaMenorMensal);

					apuracaoQtdPenalPeriod.setPenalidade(penalidadeRetiradaMenor);
					if (contratoModalidadeMenor != null) {
						apuracaoQtdPenalPeriod.setContratoModalidade(contratoModalidadeMenor);
					}

					apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
				}
			}
		}
	}

	/**
	 * Atualizar informacoes delivery or pay.
	 *
	 * @param apuracaoVO the apuracao vo
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param dataInicial the data inicial
	 * @param dataFinal the data final
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarInformacoesDeliveryOrPay(ApuracaoVO apuracaoVO, Contrato contrato, PontoConsumo pontoConsumo, Date dataInicial,
			Date dataFinal) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		String parametroPenalidadeDoP =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_DELIVERY_OR_PAY);

		Penalidade penalidadeDeliveryOrPay = controladorContrato.obterPenalidade(Long.parseLong(parametroPenalidadeDoP));
		EntidadeConteudo periodicidadeMensal =
				controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_MENSAL);

		Collection<ParadaProgramada> listaParadaNaoProgramada =
				controladorProgramacao.consultarParadasProgramadasPeriodo(new Long[] { pontoConsumo.getChavePrimaria() },
						new Long[] { EntidadeConteudo.CHAVE_PARADA_NAO_PROGRAMA }, dataInicial, dataFinal, null);

		ContratoPontoConsumo contratoPontoConsumo =
				controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());

		// Se houver registro de parada não
		// programada
		if (listaParadaNaoProgramada != null && !listaParadaNaoProgramada.isEmpty()) {

			for (ParadaProgramada paradaProgramada : listaParadaNaoProgramada) {

				// se for CDL
				if (paradaProgramada.getNomeSolicitante().equals(CDL) || paradaProgramada.getNomeSolicitante().equals(SUPRIDORA)) {

					Date dataParada = paradaProgramada.getDataParada();
					BigDecimal qtdDiariaDisponibilizada = null;
					BigDecimal qtdDiariaSolicitada = null;
					BigDecimal qtdDiariaProgramada = null;
					BigDecimal qtdFaltante = null;

					Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacaoConsumo = controladorContrato
							.consultarSolicitacaoConsumoPontoConsumoPorPeriodo(pontoConsumo.getChavePrimaria(), dataParada, dataParada);

					BigDecimal qDCData = obterQuantidadeDiariaContratada(dataParada, contratoPontoConsumo);
					if (qDCData == null) {
						qDCData = controladorContrato.obterQdcContratoValidoPorData(dataParada, contrato.getChavePrimaria());
					}

					if (listaSolicitacaoConsumo != null && !listaSolicitacaoConsumo.isEmpty()) {
						qtdDiariaSolicitada = listaSolicitacaoConsumo.iterator().next().getValorQDS();
						if (qtdDiariaSolicitada == null || qtdDiariaSolicitada.compareTo(BigDecimal.ZERO)==0) {
							qtdDiariaSolicitada = qDCData;
						}
						qtdDiariaProgramada = listaSolicitacaoConsumo.iterator().next().getValorQDP();
						if (qtdDiariaProgramada == null || qtdDiariaProgramada.compareTo(BigDecimal.ZERO)==0) {
							qtdDiariaProgramada = qDCData;
						}
					} else {
						qtdDiariaSolicitada = qDCData;
					}

					// verifica se exite QDP, se
					// não existir, usar QDC.
					if (qtdDiariaProgramada != null) {
						qtdDiariaDisponibilizada = qtdDiariaProgramada;
					} else {
						qtdDiariaDisponibilizada = this.obterQuantidadeDiariaContratada(dataParada, contratoPontoConsumo);
					}

					if (qtdDiariaDisponibilizada != null) {
						if (qtdDiariaSolicitada == null) {
							qtdDiariaSolicitada = BigDecimal.ZERO;
						}

						qtdFaltante = qtdDiariaSolicitada.subtract(qtdDiariaDisponibilizada);
					} else {
						qtdDiariaDisponibilizada = qtdDiariaSolicitada;
						qtdFaltante = BigDecimal.ZERO;
					}

					Contrato contratoPai = contrato;
					if (contrato.getChavePrimariaPai() != null) {
						contratoPai = (Contrato) controladorContrato.obter(contrato.getChavePrimariaPai());
					}

					if (qtdFaltante.compareTo(BigDecimal.ZERO) > 0) {

						Long idPontoConsumo = null;
						EntidadeConteudo tipoAgrupamentoVolume =
								controladorEntidadeConteudo.obterEntidadeConteudo(EntidadeConteudo.CHAVE_AGRUPAMENTO_POR_VOLUME);
						if ((contrato.getTipoAgrupamento() == null)
								|| (contrato.getTipoAgrupamento().getChavePrimaria() != tipoAgrupamentoVolume.getChavePrimaria())) {
							idPontoConsumo = pontoConsumo.getChavePrimaria();
						}
						BigDecimal qtdaDiariaRetirada =
								this.obterQDRApuracaoQuantidade(contratoPai, idPontoConsumo, dataParada, apuracaoVO);

						ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod =
								this.criarApuracaoQuantidadePenalidadePeriodicidade();
						apuracaoQtdPenalPeriod.setContrato(contratoPai);
						apuracaoQtdPenalPeriod.setContratoAtual(contrato);
						apuracaoQtdPenalPeriod.setQtdaNaoRetiradaPorParadaNaoProgramada(qtdFaltante);
						apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidadeMensal);
						apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(dataParada));
						apuracaoQtdPenalPeriod.setDataInicioApuracao(dataParada);
						apuracaoQtdPenalPeriod.setDataFimApuracao(dataParada);
						apuracaoQtdPenalPeriod.setQtdaDiariaProgramada(qtdDiariaProgramada);
						apuracaoQtdPenalPeriod.setQtdaDiariaDisponibilizada(qtdDiariaDisponibilizada);
						apuracaoQtdPenalPeriod.setPenalidade(penalidadeDeliveryOrPay);
						apuracaoQtdPenalPeriod.setPontoConsumo(pontoConsumo);
						apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(qtdaDiariaRetirada);

						apuracaoVO.getListaApuracaoQuantidadePenalidadePeriodicidade().add(apuracaoQtdPenalPeriod);
					}
				}
			}
		}
	}

	/**
	 * Obter quantidade diaria contratada.
	 *
	 * @param dataParada the data parada
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @return the big decimal
	 */
	private BigDecimal obterQuantidadeDiariaContratada(Date dataParada, ContratoPontoConsumo contratoPontoConsumo) {

		/**
		 * select sum(COPQ_MD_VOLUME) from contrato_ponto_cons_modal_qdc where COPQ_DT = ( select max(COPQ_DT) from
		 * contrato_ponto_cons_modal_qdc where COPQ_DT <= to_date('05/06/2010','dd/MM/yyyy')) and COPM_CD in ( select COPM_CD from
		 * contrato_ponto_consumo_modalid where COPC_CD = 4799);
		 */

		StringBuilder hql = new StringBuilder();
		hql.append(" select sum(cpcModalidadeQDC.medidaVolume) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeContratoPontoConsumoModalidadeQDC().getSimpleName());
		hql.append(" cpcModalidadeQDC ");
		hql.append(" WHERE ");
		hql.append(" cpcModalidadeQDC.dataVigencia =  (");

		hql.append(" 	select max(cpcModal.dataVigencia) from ");
		hql.append(getClasseEntidadeContratoPontoConsumoModalidadeQDC().getSimpleName());
		hql.append(" cpcModal ");
		hql.append(" 	where ");
		hql.append(" 	cpcModal.dataVigencia <= :dataParada ");
		hql.append(" 	and cpcModal.contratoPontoConsumoModalidade.chavePrimaria = "
				+ "cpcModalidadeQDC.contratoPontoConsumoModalidade.chavePrimaria ");
		hql.append(" ) ");

		hql.append(" and  cpcModalidadeQDC.contratoPontoConsumoModalidade in ( ");

		hql.append(" select  cpcModalidade.chavePrimaria ");
		hql.append(" from  ");
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
		hql.append(" cpcModalidade ");
		hql.append(" where cpcModalidade.contratoPontoConsumo.chavePrimaria = :idContratoPontoConsumo)  ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idContratoPontoConsumo", contratoPontoConsumo.getChavePrimaria());

		DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_BR);
		dateFormat.format(dataParada);
		Util.adicionarRestricaoDataSemHora(query, dataParada, "dataParada", Boolean.FALSE);

		return (BigDecimal) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #consultarPenalidadesPorPeriodo(br.com.procenge
	 * .ggas.contrato.contrato.Contrato, br.com.ggas.cadastro.imovel.PontoConsumo, java.util.Date, java.util.Date)
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> consultarPenalidadesPorPeriodo(Contrato contrato,
			PontoConsumo pontoConsumo, Date dataInicial, Date dataFinal, long idPeriodicidade) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade().getSimpleName());
		hql.append(" aqpp ");
		hql.append(" where ");

		hql.append(" aqpp.dataInicioApuracao >= :dataInicial");
		hql.append(" and aqpp.dataFimApuracao <= :dataFinal");
		hql.append(" and aqpp.periodicidadePenalidade.chavePrimaria = :idPeriodicidade");

		if (contrato != null) {
			hql.append(" and aqpp.contrato.chavePrimaria = :chaveContrato");
		}
		if (pontoConsumo != null) {
			hql.append(" and aqpp.pontoConsumo.chavePrimaria = :chavePontoConsumo");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);
		query.setLong("idPeriodicidade", idPeriodicidade);
		if (contrato != null) {
			if (contrato.getChavePrimariaPai() != null) {
				query.setLong("chaveContrato", contrato.getChavePrimariaPai());
			} else {
				query.setLong("chaveContrato", contrato.getChavePrimaria());
			}
		}
		if (pontoConsumo != null) {
			query.setLong("chavePontoConsumo", pontoConsumo.getChavePrimaria());
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#obterApuracaoQuantidadePenalidadePeriodicidade(java.lang
	 * .Long, java.lang.Long, java.lang.Long, java.lang.Integer, java.lang.Long)
	 */
	@Override
	public ApuracaoQuantidadePenalidadePeriodicidade obterApuracaoQuantidadePenalidadePeriodicidade(Long idContrato, Long idPenalidade,
			Long idPontoConsumo, Integer periodoApuracao, Long idModalidade) throws NegocioException {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.createAlias("periodicidadePenalidade", "periodicidadePenalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("fatura", "fatura", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato", "contrato", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato.clienteAssinatura", "clienteAssinatura", Criteria.LEFT_JOIN);
		criteria.createAlias("contrato.listaContratoPontoConsumo", "listaContratoPontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoAtual", "contratoAtual", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoAtual.clienteAssinatura", "clienteAssinaturaAtual", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoAtual.listaContratoPontoConsumo", "listaContratoPontoConsumoAtual", Criteria.LEFT_JOIN);
		criteria.createAlias("contratoModalidade", "contratoModalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("clienteAssinatura.enderecos", "enderecos", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.imovel", "imovel", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.segmento", "segmento", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade", "penalidade", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade.rubricaDesconto", "rubricaDesconto", Criteria.LEFT_JOIN);
		criteria.createAlias("penalidade.rubrica", "rubrica", Criteria.LEFT_JOIN);
		criteria.createAlias("rubrica.lancamentoItemContabil", "lancamentoItemContabil", Criteria.LEFT_JOIN);
		criteria.createAlias("lancamentoItemContabil.tipoCreditoDebito", "tipoCreditoDebito", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq("contratoAtual.chavePrimaria", idContrato));
		criteria.add(Restrictions.eq("penalidade.chavePrimaria", idPenalidade));
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
		}

		if (idModalidade != null) {
			criteria.add(Restrictions.eq("contratoModalidade.chavePrimaria", idModalidade));
		} else {
			criteria.add(Restrictions.isNull("contratoModalidade.chavePrimaria"));
		}
		criteria.add(Restrictions.eq("periodoApuracao", periodoApuracao));
		criteria.setMaxResults(1);

		return (ApuracaoQuantidadePenalidadePeriodicidade) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade #processarPenalidadesContrato(br.com.procenge
	 * .ggas.contrato.contrato.Contrato, br.com.ggas.auditoria.DadosAuditoria, java.lang.StringBuilder)
	 */

	/*
	 * Pega o contrato atual da Apuração quantidade penalidade periodicidade e o ponto de consumo vinculado a esta apuração
	 */
	@Override
	public void processarPenalidadesContrato(Contrato contrato, Collection<PontoConsumo> pontosAProcessar, DadosAuditoria dadosAuditoria,
			StringBuilder logProcessamento) throws GGASException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorPeriodicidadeMensal =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_MENSAL);
		String valorPeriodicidadeTrimestral =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_TRIMESTRAL);
		String valorPeriodicidadeAnual =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_ANUAL);

		// Tipo de periodicidade Penalidade para apuração das penalidades
		Long idPeriodicidadeMensal = Long.valueOf(valorPeriodicidadeMensal);
		Long idPeriodicidadeTrimestral = Long.valueOf(valorPeriodicidadeTrimestral);
		Long idPeriodicidadeAnual = Long.valueOf(valorPeriodicidadeAnual);

		logProcessamento.append("\r\n\r\n");
		Boolean apurarPenalidadeMensal = Boolean.FALSE;
		Boolean apurarPenalidadeTrimestral = Boolean.FALSE;
		Boolean apurarPenalidadeAnual = Boolean.FALSE;
		Collection<ContratoPontoConsumoPenalidade> listaPenalidades = null;
		if (contrato != null) {
			logProcessamento.append("CONTRATO: ");
			logProcessamento.append(contrato.getNumeroFormatado());
			logProcessamento.append("\r\n");
			apurarPenalidadeMensal = this.verificarPenalidadeContrato(contrato);
			// Se a periodicidade do contrato ponto consumo penalidade for igual a mensal ou anual
			// verificar se é igual a trimestral
			listaPenalidades = controladorApuracaoPenalidade.obterListaPenalidasPorContrato(contrato.getChavePrimaria());
		}

		if (listaPenalidades != null && !listaPenalidades.isEmpty()) {
			for (ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade : listaPenalidades) {
				if (contratoPontoConsumoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeMensal) {
					apurarPenalidadeMensal = Boolean.TRUE;
				} else if (contratoPontoConsumoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeTrimestral) {
					apurarPenalidadeTrimestral = Boolean.TRUE;
				} else if (contratoPontoConsumoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeAnual) {
					apurarPenalidadeAnual = Boolean.TRUE;
				}
			}
		} else {

			// nova implementação para atender os requisitos da pbgas

			for (ContratoPenalidade contratoPenalidade : contrato.getListaContratoPenalidade()) {
				if (contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeMensal) {
					apurarPenalidadeMensal = Boolean.TRUE;
				} else if (contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeTrimestral) {
					apurarPenalidadeTrimestral = Boolean.TRUE;
				} else if (contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeAnual) {
					apurarPenalidadeAnual = Boolean.TRUE;
				}
			}

		}

		// nova implementação para atender os requisitos da pbgas
		if (listaPenalidades == null || listaPenalidades.isEmpty()) {

			for (ContratoPenalidade contratoPenalidade : contrato.getListaContratoPenalidade()) {
				if (contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeMensal) {
					apurarPenalidadeMensal = Boolean.TRUE;
				} else if (contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeTrimestral) {
					apurarPenalidadeTrimestral = Boolean.TRUE;
				} else if (contratoPenalidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeAnual) {
					apurarPenalidadeAnual = Boolean.TRUE;
				}
			}

		}

		Map<String, Object> filtro = new HashMap<>();
		if (contrato != null) {
			filtro.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		}

		logProcessamento.append("Pesquisando pontos de consumo do contrato. \r\n");
		Collection<PontoConsumo> listaPontoConsumoContrato = pontosAProcessar;
		if (pontosAProcessar == null || pontosAProcessar.isEmpty()) {
			listaPontoConsumoContrato = controladorContrato.obterPontosConsumoContrato(filtro);
		}

		if (listaPontoConsumoContrato != null && !listaPontoConsumoContrato.isEmpty() && contrato != null) {

			PontoConsumo primeiroPontoConsumo = listaPontoConsumoContrato.iterator().next();
			ContratoPontoConsumo contratoPontoConsumo = controladorContrato
					.obterContratoPontoConsumoPorContratoPontoConsumo(contrato.getChavePrimaria(), primeiroPontoConsumo.getChavePrimaria());

			String codigoAgrupamentoVolume =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AGRUPAMENTO_POR_VOLUME);
			boolean isAgrupadoVolume = Boolean.FALSE;

			if (contrato.getTipoAgrupamento() != null
					&& contrato.getTipoAgrupamento().getChavePrimaria() == Long.parseLong(codigoAgrupamentoVolume)) {
				isAgrupadoVolume = Boolean.TRUE;
			}

			ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamento =
					controladorContrato.obterContratoPontoConsumoItemFaturamentoPrioritario(contrato);

			// Verifica se o ramo de atividade da tarifa é igual ao do ponto de consumo, se não for ele colocar no log
			for (PontoConsumo pontoConsumo : listaPontoConsumoContrato) {
				if (contratoPontoConsumoItemFaturamento != null && contratoPontoConsumoItemFaturamento.getTarifa() != null
						&& contratoPontoConsumoItemFaturamento.getTarifa().getRamoAtividade() != null
						&& pontoConsumo.getRamoAtividade() != null
						&& (!contratoPontoConsumoItemFaturamento.getTarifa().getRamoAtividade().equals(pontoConsumo.getRamoAtividade()))) {
					logProcessamento.append(" O Ramo Atividade da Tarifa diverge do Ramo Atividade do Ponto de Consumo ");
					logProcessamento.append(pontoConsumo.getImovel().getNome());
					logProcessamento.append(" - ");
					logProcessamento.append(pontoConsumo.getDescricao());
					logProcessamento.append("\r\n");
				}
			}

			if (contrato.getAgrupamentoCobranca() && isAgrupadoVolume) {

				logProcessamento.append("    Contrato com faturamento agrupado por volume para os Pontos de Consumo:");
				logProcessamento.append("\r\n");
				for (PontoConsumo pontoConsumo : listaPontoConsumoContrato) {
					logProcessamento.append("      ");
					logProcessamento.append(pontoConsumo.getImovel().getNome());
					logProcessamento.append(" - ");
					logProcessamento.append(pontoConsumo.getDescricao());
					logProcessamento.append("\r\n");
				}
				logProcessamento.append("\r\n");

				// Verifica se esse ponto de consumo possui algum consumo

				HistoricoConsumo ultimoHistoricoConsumo =
						controladorHistoricoConsumo.obterUltimoHistoricoConsumoFaturado(primeiroPontoConsumo.getChavePrimaria());

				Date dataInicialMes = null;
				Date dataFinalMes = null;
				Date dataInicialTrimestre = null;
				Date dataFinalTrimestre = null;
				Date dataInicialAno = null;
				Date dataFinalAno = null;

				// Definir o periodo de apuração...
				// Verificar o tipo de periodicidade penalidade vinculado ao contrato
				// Caso seja Civil permanecer o comportamente atual se for continuo atualizar o código

				// pegar DATA_LEITURA_FATURADa do historico de medicao e a partir dele fazer o calculo das penalidas

				// Verificar aqui se é civil ou não

				if (ultimoHistoricoConsumo != null) {
					if (contrato.getTipoPeriodicidadePenalidade()) {
						// Tipo Periodicidade Civil
						String dataInicialAux = "01/" + Util.formatarAnoMes(ultimoHistoricoConsumo.getAnoMesFaturamento());

						dataInicialMes = Util.converterCampoStringParaData("Data Inicial", dataInicialAux, Constantes.FORMATO_DATA_BR);
						if (dataInicialMes != null) {
							dataFinalMes = Util.converterCampoStringParaData("Data Final",
									Util.obterUltimoDiaMes(dataInicialMes, Constantes.FORMATO_DATA_BR), Constantes.FORMATO_DATA_BR);
							// Periodo Trimestral Civil
							Map<String, Date> params = Util.obterPeriodoTrimestral(dataInicialMes);
							dataInicialTrimestre = params.get("dataInicioTrimestre");
							dataFinalTrimestre = params.get("datafinalTrimestre");
						}

						dataInicialAno = this.obterDataInicialAno(dataFinalMes, contrato, contratoPontoConsumo);
						dataFinalAno = this.obterDataFinalAno(contrato, dataFinalMes);

						if (dataInicialMes == null || dataFinalMes == null) {
							logProcessamento.append("Não foi possível definir o período para tratamento das penalidades. \r\n");
						}
						// Tipo Periodicidade Continuamente
					} else if (!contrato.getTipoPeriodicidadePenalidade()) {

						Date dataFaturada = ultimoHistoricoConsumo.getHistoricoAtual().getDataLeituraFaturada();

						// Mensal Continuo

						dataInicialMes = Util.removerDiasData(dataFaturada, PERIODO_MES);
						dataFinalMes = dataFaturada;

						// Trimestral Continuo

						dataInicialTrimestre = Util.removerDiasData(dataFaturada, PERIODO_TRIMESTRE);
						dataFinalTrimestre = dataFaturada;

						// Anual Continuo

						dataInicialAno = Util.removerAnoData(dataFaturada, 1);
						dataFinalAno = dataFaturada;

					}
				} else {
					logProcessamento.append("Não foi encontrado histórico de consumo para o ponto de consumo ");
					logProcessamento.append(primeiroPontoConsumo.getImovel().getNome());
					logProcessamento.append(" - ");
					logProcessamento.append(primeiroPontoConsumo.getDescricao());
					logProcessamento.append("\r\n");
				}

				// verificar a chamada
				try {
					if (apurarPenalidadeMensal && ((dataInicialMes != null) && (dataFinalMes != null))) {

						this.apurarPenalidade(contrato, null, dadosAuditoria, logProcessamento, controladorApuracaoPenalidade,
								idPeriodicidadeMensal, listaPontoConsumoContrato, primeiroPontoConsumo, dataInicialMes, dataFinalMes);

					}
					if (apurarPenalidadeTrimestral && ((dataInicialTrimestre != null) && (dataFinalTrimestre != null))) {

						this.apurarPenalidade(contrato, null, dadosAuditoria, logProcessamento, controladorApuracaoPenalidade,
								idPeriodicidadeTrimestral, listaPontoConsumoContrato, primeiroPontoConsumo, dataInicialTrimestre,
								dataFinalTrimestre);

					}

					if (apurarPenalidadeAnual && ((dataInicialAno != null) && (dataFinalAno != null))) {

						this.apurarPenalidade(contrato, null, dadosAuditoria, logProcessamento, controladorApuracaoPenalidade,
								idPeriodicidadeAnual, listaPontoConsumoContrato, primeiroPontoConsumo, dataInicialAno, dataFinalAno);
					}

				} catch (ContratoDeliveryOrPayInconsistenteException e) {
					LOG.error(e.getMessage(), e);
					logProcessamento.append("      ");
					logProcessamento.append(e.getMessage());
					logProcessamento.append(" \r\n");
				} catch (ContratoGasForaEspecificacaoInconsistenteException e) {
					LOG.error(e.getMessage(), e);
					logProcessamento.append("      ");
					logProcessamento.append(e.getMessage());
					logProcessamento.append(" \r\n");
				} catch (ContratoRetiradaAMaiorInconsistenteException e) {
					LOG.error(e.getMessage(), e);
					logProcessamento.append("      ");
					logProcessamento.append(e.getMessage());
					logProcessamento.append(" \r\n");
				} catch (ContratoRetiradaAMenorInconsistenteException e) {
					LOG.error(e.getMessage(), e);
					logProcessamento.append("      ");
					logProcessamento.append(e.getMessage());
					logProcessamento.append(" \r\n");
				}

			} else {

				for (PontoConsumo pontoConsumo : listaPontoConsumoContrato) {
					logProcessamento.append("\r\n    PONTO CONSUMO: ");
					logProcessamento.append(primeiroPontoConsumo.getImovel().getNome());
					logProcessamento.append(" - ");
					logProcessamento.append(pontoConsumo.getDescricao());
					logProcessamento.append("\r\n");

					Collection<PontoConsumo> listaPontoConsumo = new ArrayList<>();
					listaPontoConsumo.add(pontoConsumo);

					Date dataInicialMes = null;
					Date dataFinalMes = null;
					Date dataInicialTrimestre = null;
					Date dataFinalTrimestre = null;
					Date dataInicialAno = null;
					Date dataFinalAno = null;

					HistoricoConsumo ultimoHistoricoConsumo =
							controladorHistoricoConsumo.obterUltimoHistoricoConsumoFaturado(pontoConsumo.getChavePrimaria());

					if (ultimoHistoricoConsumo != null) {
						if (contrato.getTipoPeriodicidadePenalidade()) {
							String dataInicialAux = "01/" + Util.formatarAnoMes(ultimoHistoricoConsumo.getAnoMesFaturamento());
							dataInicialMes = Util.converterCampoStringParaData("Data Inicial", dataInicialAux, Constantes.FORMATO_DATA_BR);
							if (dataInicialMes != null) {
								dataFinalMes = Util.converterCampoStringParaData("Data Final",
										Util.obterUltimoDiaMes(dataInicialMes, Constantes.FORMATO_DATA_BR), Constantes.FORMATO_DATA_BR);

								// Periodo Trimestral Civil
								Map<String, Date> params = Util.obterPeriodoTrimestral(dataInicialMes);
								dataInicialTrimestre = params.get("dataInicioTrimestre");
								dataFinalTrimestre = params.get("datafinalTrimestre");

								dataInicialAno = this.obterDataInicialAno(dataFinalMes, contrato, contratoPontoConsumo);
								dataFinalAno = this.obterDataFinalAno(contrato, dataFinalMes);
							}

							if (dataInicialMes == null || dataFinalMes == null) {
								logProcessamento.append("Não foi possível definir o período para tratamento das penalidades. \r\n");
							}
						} else if (!contrato.getTipoPeriodicidadePenalidade()) {
							Date dataFaturada = ultimoHistoricoConsumo.getHistoricoAtual().getDataLeituraFaturada();

							// Mensal Continuo

							dataInicialMes = Util.removerDiasData(dataFaturada, PERIODO_MES);
							dataFinalMes = dataFaturada;

							// Trimestral Continuo

							dataInicialTrimestre = Util.removerDiasData(dataFaturada, PERIODO_TRIMESTRE);
							dataFinalTrimestre = dataFaturada;

							// Anual Continuo

							dataInicialAno = Util.removerAnoData(dataFaturada, 1);
							dataFinalAno = dataFaturada;

						}
					} else {
						logProcessamento.append("Não foi encontrado histórico de consumo para o ponto de consumo ");
						logProcessamento.append(primeiroPontoConsumo.getImovel().getNome());
						logProcessamento.append(" - ");
						logProcessamento.append(pontoConsumo.getDescricao());
						logProcessamento.append("\r\n");
					}

					try {

						if (apurarPenalidadeMensal && ((dataInicialMes != null) && (dataFinalMes != null))) {

							this.apurarPenalidade(contrato, pontoConsumo, dadosAuditoria, logProcessamento, controladorApuracaoPenalidade,
									idPeriodicidadeMensal, listaPontoConsumoContrato, pontoConsumo, dataInicialMes, dataFinalMes);

						}

						if (apurarPenalidadeTrimestral && ((dataInicialTrimestre != null) && (dataFinalTrimestre != null))) {

							this.apurarPenalidade(contrato, null, dadosAuditoria, logProcessamento, controladorApuracaoPenalidade,
									idPeriodicidadeTrimestral, listaPontoConsumoContrato, primeiroPontoConsumo, dataInicialTrimestre,
									dataFinalTrimestre);

						}

						if (apurarPenalidadeAnual && ((dataInicialAno != null) && (dataFinalAno != null))) {

							this.apurarPenalidade(contrato, pontoConsumo, dadosAuditoria, logProcessamento, controladorApuracaoPenalidade,
									idPeriodicidadeAnual, listaPontoConsumoContrato, pontoConsumo, dataInicialAno, dataFinalAno);

						}
					} catch (ContratoDeliveryOrPayInconsistenteException e) {
						LOG.error(e.getMessage(), e);
						logProcessamento.append("      ");
						logProcessamento.append(e.getMessage());
						logProcessamento.append(" \r\n");
					} catch (ContratoGasForaEspecificacaoInconsistenteException e) {
						LOG.error(e.getMessage(), e);
						logProcessamento.append("      ");
						logProcessamento.append(e.getMessage());
						logProcessamento.append(" \r\n");
					} catch (ContratoRetiradaAMaiorInconsistenteException e) {
						LOG.error(e.getMessage(), e);
						logProcessamento.append("      ");
						logProcessamento.append(e.getMessage());
						logProcessamento.append(" \r\n");
					} catch (ContratoRetiradaAMenorInconsistenteException e) {
						LOG.error(e.getMessage(), e);
						logProcessamento.append("      ");
						logProcessamento.append(e.getMessage());
						logProcessamento.append(" \r\n");
					}

				}
			}

		} else {
			logProcessamento.append("Nenhum ponto de consumo foi encontrado. \r\n");
		}
	}

	private void apurarPenalidade(Contrato contrato, PontoConsumo pontoConsumo, DadosAuditoria dadosAuditoria,
			StringBuilder logProcessamento, ControladorApuracaoPenalidade controladorApuracaoPenalidade, Long idPeriodicidade,
			Collection<PontoConsumo> listaPontoConsumoContrato, PontoConsumo primeiroPontoConsumo, Date dataInicial, Date dataFinal)
			throws NegocioException, ConcorrenciaException {

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesPeriodo = controladorApuracaoPenalidade
				.consultarPenalidadesPorPeriodo(contrato, pontoConsumo, dataInicial, dataFinal, idPeriodicidade);
		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesNaoCobradas =
				this.removerPenalidadesCobradas(listaPenalidadesPeriodo);
		Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesCobradas =
				new ArrayList<>();

		if (listaPenalidadesPeriodo != null && !listaPenalidadesPeriodo.isEmpty()) {
			listaPenalidadesCobradas.addAll(listaPenalidadesPeriodo);
			listaPenalidadesCobradas.removeAll(listaPenalidadesNaoCobradas);
		}

		logProcessamento.append("Iniciando distribuição do consumo... \r\n\r\n");
		logProcessamento.append("Periodo de ");
		logProcessamento.append(Util.converterDataParaString(dataInicial));
		logProcessamento.append(" a ");
		logProcessamento.append(Util.converterDataParaString(dataFinal));
		logProcessamento.append("\r\n");
		ApuracaoVO apuracaoVO = controladorApuracaoPenalidade.obterConsumoDistribuidoPorModalidade(contrato, primeiroPontoConsumo,
				ControladorApuracaoPenalidade.DISTRIBUICAO_APURACAO, null, idPeriodicidade, dataInicial, dataFinal, logProcessamento);

		if (apuracaoVO != null) {
			logProcessamento.append("Inserindo informações de consumo... \r\n");
			Collection<PontoConsumo> lPontoConsumo = new ArrayList<>();
			if (pontoConsumo != null) {
				lPontoConsumo.add(pontoConsumo);
			} else {
				lPontoConsumo.addAll(listaPontoConsumoContrato);
			}
			controladorApuracaoPenalidade.inserirApuracao(apuracaoVO, dadosAuditoria, listaPenalidadesNaoCobradas, listaPenalidadesCobradas,
					contrato, lPontoConsumo, dataInicial, dataFinal, idPeriodicidade);

			logProcessamento.append("Tratando penalidades... \r\n");
			controladorApuracaoPenalidade.tratarPenalidades(contrato, lPontoConsumo, dataInicial, dataFinal, dadosAuditoria,
					logProcessamento);
		} else {
			logProcessamento.append("      Nenhuma penalidade foi calculada... \r\n");
		}

	}

	/**
	 * Método responsável por remover as ApuracaoQuantidadePenalidadePeriodicidade já cobradas de uma lista.
	 *
	 * @param listaPenalidadesPeriodo lista contendo todas as ApuracaoQuantidadePenalidadePeriodicidade
	 * @return as ApuracaoQuantidadePenalidadePeriodicidade não cobradas da lista
	 */
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> removerPenalidadesCobradas(
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesPeriodo) {

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> retorno = new ArrayList<>();

		if (listaPenalidadesPeriodo != null && !listaPenalidadesPeriodo.isEmpty()) {

			for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : listaPenalidadesPeriodo) {
				if (aqpp.getValorCobrado() == null) {
					retorno.add(aqpp);
				}
			}
		}

		return retorno;
	}

	/**
	 * Obter data inicial ano.
	 *
	 * @param dataFinal the data final
	 * @param contrato the contrato
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @return the date
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * Método para obter a data inicial do ano a partir do contrato e da data final do mês em apuração(último histórico consumo faturado).
	 */
	private Date obterDataInicialAno(Date dataFinal, Contrato contrato, ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		DateTime dataInicialAno = new DateTime(contrato.getDataAssinatura());
		DateTime dataFinalAno = new DateTime(dataFinal);
		Integer anoAssinatura = dataInicialAno.getYear();
		Integer anoFinal = dataFinalAno.getYear();
		Date dataInicial = null;

		Date dataFinalTeste = contratoPontoConsumo.getDataConsumoTesteExcedido();
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		String parametroExpurgarPeriodoTeste =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo("EXPURGAR_PERIODO_TESTE_APURACAO");

		boolean isExpurgarPeriodoTeste;
		if (BOOLEAN_VERDADE_1.equals(parametroExpurgarPeriodoTeste)) {
			isExpurgarPeriodoTeste = true;
		} else {
			isExpurgarPeriodoTeste = false;
		}

		// ANO MAIUSCULO
		if (contrato.getIndicadorAnoContratual()) {

			// Se o ano de assinatura for igual ao
			// ano final.
			// se o ano da assinatura e o ano da
			// data final forem iguais, é o
			// mesmo ano( dt assinatura=
			// 01/01/20110 final= 31/12/2011)
			if (anoAssinatura.compareTo(anoFinal) == 0) {

				dataInicial = dataInicialAno.toDate();

				// Se o ano de assinatura for
				// menor que o ano final
			} else if (anoAssinatura.compareTo(anoFinal) < 0) {

				// Criar data com primeiro dia do
				// ano.
				dataInicialAno = dataInicialAno.withYear(dataFinalAno.getYear());
				dataInicialAno = dataInicialAno.withMonthOfYear(1);
				dataInicialAno = dataInicialAno.withDayOfMonth(1);
				dataInicialAno = dataInicialAno.withHourOfDay(0);
				dataInicialAno = dataInicialAno.withMinuteOfHour(0);
				dataInicialAno = dataInicialAno.withSecondOfMinute(0);
				dataInicialAno = dataInicialAno.withMillisOfSecond(0);

				// Se o contrato foi assinado
				// depois do inicio do ano (01
				// jan),
				// usar data assinatura, senão, 01
				// jan.
				if (contrato.getDataAssinatura().after(dataInicialAno.toDate())) {
					dataInicial = contrato.getDataAssinatura();
				} else {
					dataInicial = dataInicialAno.toDate();
				}
			}

			// ano minúsculo
		} else {

			if (anoAssinatura.compareTo(anoFinal) == 0) {

				dataInicial = dataInicialAno.toDate();

			} else if (anoAssinatura.compareTo(anoFinal) < 0) {

				dataInicialAno = dataInicialAno.withYear(anoFinal - 1);
				dataInicialAno = dataInicialAno.withMonthOfYear(dataInicialAno.getMonthOfYear());
				dataInicialAno = dataInicialAno.withDayOfMonth(dataInicialAno.getDayOfMonth());
				dataInicialAno = dataInicialAno.withHourOfDay(0);
				dataInicialAno = dataInicialAno.withMinuteOfHour(0);
				dataInicialAno = dataInicialAno.withSecondOfMinute(0);
				dataInicialAno = dataInicialAno.withMillisOfSecond(0);
				dataInicial = dataInicialAno.toDate();

			}
		}

		int intervaloMeses = Util.intervaloMeses(contrato.getDataAssinatura(), dataInicial);

		if (intervaloMeses <= LIMITE_MESES && isExpurgarPeriodoTeste && dataFinalTeste != null && (dataFinalTeste.compareTo(dataInicial) > 0)) {
			dataInicial = dataFinalTeste;
		}

		return dataInicial;

	}

	/**
	 * Obter data final ano.
	 *
	 * @param contrato the contrato
	 * @param dataFinalPeriodo the data final periodo
	 * @return the date
	 */
	/*
	 * Método para obter a data final do ano a partir do contrato e da data final do mês em apuração(último histórico consumo faturado).
	 */
	private Date obterDataFinalAno(Contrato contrato, Date dataFinalPeriodo) {

		Date dataFinal = null;
		DateTime dataFinalAux = new DateTime();
		DateTime dataAux = new DateTime(dataFinalPeriodo);

		dataFinalAux = dataFinalAux.withYear(dataAux.getYear());
		dataFinalAux = dataFinalAux.withMonthOfYear(LIMITE_MESES);
		dataFinalAux = dataFinalAux.withDayOfMonth(LIMITE_DIAS_MES);
		dataFinalAux = dataFinalAux.withHourOfDay(0);
		dataFinalAux = dataFinalAux.withMinuteOfHour(0);
		dataFinalAux = dataFinalAux.withSecondOfMinute(0);
		dataFinalAux = dataFinalAux.withMillisOfSecond(0);

		DateTime dataLimiteAniversario = new DateTime(contrato.getDataAssinatura());
		dataLimiteAniversario = dataLimiteAniversario.withYear(dataAux.getYear()).plusYears(1);
		dataLimiteAniversario = dataLimiteAniversario.minusDays(1);
		dataLimiteAniversario = dataLimiteAniversario.withHourOfDay(0);
		dataLimiteAniversario = dataLimiteAniversario.withMinuteOfHour(0);
		dataLimiteAniversario = dataLimiteAniversario.withSecondOfMinute(0);
		dataLimiteAniversario = dataLimiteAniversario.withMillisOfSecond(0);

		if (contrato.getDataRecisao() != null) {
			dataFinal = contrato.getDataRecisao();
		} else {
			// ANO MAIUSCULO
			if (contrato.getIndicadorAnoContratual()) {
				if (dataFinalPeriodo.equals(dataFinalAux.toDate())) {
					dataFinal = dataFinalAux.toDate();
				}
			} else {
				if (!dataFinalPeriodo.before(dataLimiteAniversario.toDate())) {
					dataFinal = dataLimiteAniversario.toDate();
				}
			}
		}

		return dataFinal;
	}

	/**
	 * Verificar penalidade contrato.
	 *
	 * @param contrato the contrato
	 * @return the boolean
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * Método para verificar a existência de penalidades com periodicidade mensal no contrato.
	 */
	private Boolean verificarPenalidadeContrato(Contrato contrato) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		String codPenalidadeRetiradaMaior =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
		String codPenalidadeRetiradaMenor =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		Map<String, Object> params = new HashMap<>();

		params.put(PARAM_NAME_ID_CONTRATO, contrato.getChavePrimaria());
		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMaior));

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeRetiradaMenor));

		ContratoPenalidade contratoPenalidadeRetiradaMenor = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);
		Boolean existePenaliade = Boolean.FALSE;

		if (contratoPenalidadeRetiradaMaior != null && contratoPenalidadeRetiradaMaior.getPenalidade() != null) {
			existePenaliade = Boolean.TRUE;
		}

		if (contratoPenalidadeRetiradaMenor != null && contratoPenalidadeRetiradaMenor.getPenalidade() != null) {
			existePenaliade = Boolean.TRUE;
		}

		for (ContratoPontoConsumo c : contrato.getListaContratoPontoConsumo()) {
			for (ContratoPontoConsumoModalidade cm : c.getListaContratoPontoConsumoModalidade()) {
				Collection<ContratoPontoConsumoPenalidade> lista =
						controladorContrato.listarConsumoPenalidadePorContratoPontoConsumoModalidade(cm.getChavePrimaria());
				if (lista != null && lista.size() > 1) {
					existePenaliade = Boolean.TRUE;
					break;
				}

			}

		}

		if ((contrato.getPercentualTarifaDoP() != null) || (contrato.getPercentualSobreTariGas() != null)) {
			existePenaliade = Boolean.TRUE;
		}

		return existePenaliade;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seebr.com.ggas.faturamento.apuracaopenalidade . ControladorApuracaoPenalidade# processarPenalidades(java.util.Collection,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void processarPenalidades(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
			throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		this.validarProcessarPenalidades(apuracoes);

		Contrato contratoAtual = null;
		String periodo = null;
		Collection<PontoConsumo> pontosAProcessar = new ArrayList<>();

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : apuracoes) {

			if (contratoAtual == null) {

				contratoAtual = (Contrato) controladorContrato.obter(apuracao.getContratoAtual().getChavePrimaria());

			}

			if (periodo == null) {

				periodo = apuracao.getPeriodoApuracaoFormatado();

			}

			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(apuracao.getPontoConsumo().getChavePrimaria());

			if (!this.existePontoConsumoLista(pontosAProcessar, pontoConsumo)) {

				pontosAProcessar.add(pontoConsumo);

			}

		}

		try {

			StringBuilder logProcessamento = new StringBuilder();
			logProcessamento.append("\r\n ...Recalculando penalidade de ");
			logProcessamento.append(periodo);
			logProcessamento.append(" \r\n\r\n");

			this.processarPenalidadesContrato(contratoAtual, pontosAProcessar, dadosAuditoria, logProcessamento);

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_INESPERADO, true);

		}

	}

	/**
	 * Existe ponto consumo lista.
	 *
	 * @param pontosAProcessar the pontos a processar
	 * @param pontoConsumo the ponto consumo
	 * @return the boolean
	 */
	private Boolean existePontoConsumoLista(Collection<PontoConsumo> pontosAProcessar, PontoConsumo pontoConsumo) {

		Boolean retorno = Boolean.FALSE;

		for (PontoConsumo pontoConsumoProcessar : pontosAProcessar) {

			if (pontoConsumoProcessar.getChavePrimaria() == pontoConsumo.getChavePrimaria()) {

				retorno = Boolean.TRUE;
				break;

			}

		}

		return retorno;

	}

	/**
	 * Validar processar penalidades.
	 *
	 * @param apuracoes the apuracoes
	 * @throws NegocioException the negocio exception
	 */
	private void validarProcessarPenalidades(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes) throws NegocioException {

		if (apuracoes != null && !apuracoes.isEmpty()) {

			for (ApuracaoQuantidadePenalidadePeriodicidade apuracao : apuracoes) {

				SimpleDateFormat dateformat = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

				if (apuracao.getContratoAtual() == null) {

					throw new NegocioException(ERRO_NEGOCIO_APURACAO_PENALIDADE_SEM_CONTRATO_ATUAL,
							new Object[] { dateformat.format(apuracao.getDataInicioApuracao()) });

				}

				if (apuracao.getPontoConsumo() == null) {

					throw new NegocioException(ERRO_NEGOCIO_APURACAO_PENALIDADE_SEM_PONTO_CONSUMO,
							new Object[] { dateformat.format(apuracao.getDataInicioApuracao()) });

				}

			}

		} else {

			throw new NegocioException(ERRO_NEGOCIO_APURACAO_NAO_ENCONTRADA, true);

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade .ControladorApuracaoPenalidade #obterApuracaoRecuperacaoQuantidadeVOs
	 * (java.lang.Long, java.util.Date, java.util.Date, java.lang.Long[])
	 */
	@Override
	public Collection<ApuracaoRecuperacaoQuantidadeVO> obterApuracaoRecuperacaoQuantidadeVOs(Long idContrato, Date dataInicial,
			Date dataFinal, Long[] chavesPontoConsumo) throws NegocioException {

		if (dataInicial != null && dataFinal == null) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO, true);

		} else if (dataFinal == null && dataInicial != null) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_FIM_OBRIGATORIO, true);

		} else if (dataFinal == null && dataInicial == null) {

			throw new NegocioException(ERRO_NEGOCIO_INTERVALO_DATAS_OBRIGATORIO, true);

		}

		if (Util.compararDatas(dataInicial, dataFinal) > 0) {
			throw new NegocioException(ControladorApuracaoPenalidade.ERRO_NEGOCIO_DATA_INICIAL_MAIOR_CONTRATO, dataFinal);
		}

		int intervaloMaximo = Util.intervaloDatas(dataInicial, dataFinal);

		if (intervaloMaximo > DIAS_ANO) {

			throw new NegocioException(ERRO_NEGOCIO_INTERVALO_DATAS_MAIOR_QUE_UM_ANO, true);

		}

		ControladorProgramacao controladorProgramacao = (ControladorProgramacao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		Collection<ApuracaoRecuperacaoQuantidadeVO> listaApuracaoRecuperacaoVO = null;
		Collection<Date> datas = Util.gerarIntervaloDatas(dataInicial, dataFinal);
		datas.add(dataInicial);

		Contrato contrato = (Contrato) controladorContrato.obter(idContrato, "listaContratoPontoConsumo");

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade =
				ServiceLocator.getInstancia().getControladorContrato().listarContratoPontoConsumoModalidadePorContrato(idContrato);

		Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao =
				controladorProgramacao.consultarVolumeExecucaoContratoSolicitacao(dataInicial, dataFinal, chavesPontoConsumo);
		Collection<Recuperacao> listaRecuperacao =
				controladorProgramacao.consultarVolumeExecucaoContratoRecuperacao(dataInicial, dataFinal, chavesPontoConsumo);

		if (datas != null && !datas.isEmpty()) {
			BigDecimal qdc = null;
			listaApuracaoRecuperacaoVO = new HashSet<>();

			for (Date data : datas) {

				ApuracaoRecuperacaoQuantidadeVO apuracaoRecuperacaoVO = new ApuracaoRecuperacaoQuantidadeVO();
				apuracaoRecuperacaoVO.setData(data);

				if (!listaContratoPontoConsumoModalidade.isEmpty()) {
					qdc = this.obterQdcContratoPontoConsumoModalidade(listaContratoPontoConsumoModalidade, data, data);
				}

				if (qdc != null) {
					apuracaoRecuperacaoVO.setValorQDC(String.valueOf(qdc));
				} else {
					apuracaoRecuperacaoVO.setValorQDC(null);
				}
				BigDecimal valorQDRContrato = this.obterQdrContrato(contrato, data);
				if (valorQDRContrato.compareTo(BigDecimal.ZERO) > 0) {
					apuracaoRecuperacaoVO.setValorQDR(String.valueOf(valorQDRContrato));
				}
				this.setarQdsDdpApuracaoRecuperacaoQuantidadeVO(apuracaoRecuperacaoVO, listaSolicitacao);
				this.setarQrApuracaoRecuperacaoQuantidadeVO(apuracaoRecuperacaoVO, listaRecuperacao);

				listaApuracaoRecuperacaoVO.add(apuracaoRecuperacaoVO);
			}

		}

		return listaApuracaoRecuperacaoVO;
	}

	/**
	 * Setar qds ddp apuracao recuperacao quantidade vo.
	 *
	 * @param apuracaoRecuperacaoVO the apuracao recuperacao vo
	 * @param listaSolicitacao the lista solicitacao
	 */
	private void setarQdsDdpApuracaoRecuperacaoQuantidadeVO(ApuracaoRecuperacaoQuantidadeVO apuracaoRecuperacaoVO,
			Collection<SolicitacaoConsumoPontoConsumo> listaSolicitacao) {

		if (listaSolicitacao != null) {

			for (SolicitacaoConsumoPontoConsumo solicitacao : listaSolicitacao) {

				if (solicitacao.getDataSolicitacao().compareTo(apuracaoRecuperacaoVO.getData()) == 0) {

					if (solicitacao.getValorQDS() != null) {
						apuracaoRecuperacaoVO.setValorQDS(String.valueOf(solicitacao.getValorQDS()));
					}

					if (solicitacao.getValorQDP() != null) {
						apuracaoRecuperacaoVO.setValorQDP(String.valueOf(solicitacao.getValorQDP()));
					}

					break;
				}

			}

		}

	}

	/**
	 * Setar qr apuracao recuperacao quantidade vo.
	 *
	 * @param apuracaoRecuperacaoVO the apuracao recuperacao vo
	 * @param listaRecuperacao the lista recuperacao
	 */
	private void setarQrApuracaoRecuperacaoQuantidadeVO(ApuracaoRecuperacaoQuantidadeVO apuracaoRecuperacaoVO,
			Collection<Recuperacao> listaRecuperacao) {

		if (listaRecuperacao != null) {

			for (Recuperacao recuperacao : listaRecuperacao) {

				if (recuperacao.getDataRecuperacao().compareTo(apuracaoRecuperacaoVO.getData()) == 0) {

					if (recuperacao.getMedicaoQR() != null) {
						apuracaoRecuperacaoVO.setValorQR(String.valueOf(recuperacao.getMedicaoQR()));
					}

					break;
				}

			}

		}

	}

	/**
	 * Obter lista contrato ponto consumo modalidade.
	 *
	 * @param contrato the contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<ContratoPontoConsumoModalidade> obterListaContratoPontoConsumoModalidade(Contrato contrato) {

		Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidades = new ArrayList<>();
		if (contrato.getListaContratoPontoConsumo() != null) {
			for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
				listaContratoPontoConsumoModalidades.addAll(contratoPontoConsumo.getListaContratoPontoConsumoModalidade());
			}
		}

		return listaContratoPontoConsumoModalidades;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * obterQdcContratoPontoConsumoModalidade(java.util.Collection, java.util.Date, java.util.Date)
	 */
	@Override
	public BigDecimal obterQdcContratoPontoConsumoModalidade(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
			Date dataInicial, Date dataFinal) throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		BigDecimal qdc = null;

		if (!listaContratoPontoConsumoModalidade.isEmpty()) {
			qdc = controladorContrato.consultarContratoPontoConsumoModalidadeQDCPorModalidade(listaContratoPontoConsumoModalidade,
					dataInicial, dataFinal);
		}

		return qdc;
	}

	/**
	 * Obter qdr contrato.
	 *
	 * @param contrato the contrato
	 * @param data the data
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal obterQdrContrato(Contrato contrato, Date data) throws NegocioException {

		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		BigDecimal consumoDiario = BigDecimal.ZERO;
		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<>();
		Periodicidade periodicidade = null;
		TipoMedicao tipoMedicao = null;

		if (contrato.getListaContratoPontoConsumo() != null) {
			for (ContratoPontoConsumo contratoPontoConsumo : contrato.getListaContratoPontoConsumo()) {
				periodicidade = contratoPontoConsumo.getPeriodicidade();
				tipoMedicao = contratoPontoConsumo.getTipoMedicao();
				listaPontoConsumo.add(contratoPontoConsumo.getPontoConsumo());
			}
		}
		// Se a medição for periodica, deve obter
		// a média da QDR
		if (periodicidade != null && tipoMedicao != null) {
			if (tipoMedicao.getChavePrimaria() == TipoMedicao.CODIGO_PERIODICA) {
				if (listaPontoConsumo.size() > 1) {
					for (PontoConsumo pontoConsumoAgrupadoVolume : listaPontoConsumo) {
						HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
								.obterHistoricoConsumoPorData(pontoConsumoAgrupadoVolume.getChavePrimaria(), data);
						if (historicoConsumo != null) {
							DateTime dataLimite = new DateTime(historicoConsumo.getHistoricoAtual().getDataLeituraInformada())
									.minusDays(historicoConsumo.getDiasConsumo());
							// verificar se a data
							// está dentro da
							// periodicidade
							if (!data.before(dataLimite.toDate())) {
								consumoDiario = consumoDiario.add(historicoConsumo.getConsumoApurado()
										.divide(BigDecimal.valueOf(historicoConsumo.getDiasConsumo()), ESCALA_CONSUMO_DIARIO, RoundingMode.HALF_UP));
							}
						}
					}
				} else {
					HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
							.obterHistoricoConsumoPorData(listaPontoConsumo.iterator().next().getChavePrimaria(), data);
					if (historicoConsumo != null) {
						DateTime dataLimite = new DateTime(historicoConsumo.getHistoricoAtual().getDataLeituraInformada())
								.minusDays(historicoConsumo.getDiasConsumo());
						// verificar se a data
						// está dentro da
						// periodicidade
						if (!data.before(dataLimite.toDate())) {
							consumoDiario = consumoDiario.add(historicoConsumo.getConsumoApurado()
									.divide(BigDecimal.valueOf(historicoConsumo.getDiasConsumo()), ESCALA_CONSUMO_DIARIO, RoundingMode.HALF_UP));
						}
					}
				}

			} else {
				if (listaPontoConsumo.size() > 1) {
					for (PontoConsumo pontoConsumoAgrupadoVolume : listaPontoConsumo) {
						consumoDiario = consumoDiario.add(this.obterConsumoDiario(pontoConsumoAgrupadoVolume, data));
					}
				} else {
					consumoDiario = consumoDiario.add(this.obterConsumoDiario(listaPontoConsumo.iterator().next(), data));
				}
			}
		}

		return consumoDiario;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * verificarRecuperacaoDeVolumesFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public Collection<Recuperacao> verificarRecuperacaoDeVolumesFatura(Fatura fatura) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeRecuperacao().getSimpleName());
		hql.append(" recuperacao ");
		hql.append(" where ");
		hql.append(" recuperacao.fatura.chavePrimaria = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, fatura.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * atualizarQtdaRecuperada(br.com.ggas.faturamento.apuracaopenalidade .ApuracaoQuantidadePenalidadePeriodicidade, java.math.BigDecimal,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void atualizarQtdaRecuperada(ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantPenalidPeri, BigDecimal qtdaRecuperada,
			DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {

		apuracaoQuantPenalidPeri.setQtdaRecuperada(qtdaRecuperada);
		apuracaoQuantPenalidPeri.setDadosAuditoria(dadosAuditoria);

		this.atualizar(apuracaoQuantPenalidPeri, ApuracaoQuantidadePenalidadePeriodicidadeImpl.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * removerQuantRecuperacao(br.com.ggas.faturamento.apuracaopenalidade .Recuperacao, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerQuantRecuperacao(Recuperacao recuperacao, DadosAuditoria dadosAuditoria) throws NegocioException {

		recuperacao.setDadosAuditoria(dadosAuditoria);
		this.remover(recuperacao);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade# popularQPNRPontoConsumo(java.util.Collection,
	 * java.lang.Long)
	 */
	@Override
	public void popularQPNRPontoConsumo(Collection<PontoConsumoVO> listaPontoConsumoVO, Long idContrato) throws GGASException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		Contrato contrato = (Contrato) controladorContrato.obter(idContrato);
		for (PontoConsumoVO pontoConsumoVO : listaPontoConsumoVO) {
			BigDecimal valorQnr = BigDecimal.ZERO;
			BigDecimal valorQpnr = BigDecimal.ZERO;
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItens =
					obterApuracoes(pontoConsumoVO.getPontoConsumo(), contrato);
			if (apuraQuanPenPeriItens != null && !apuraQuanPenPeriItens.isEmpty()) {
				for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : apuraQuanPenPeriItens) {
					BigDecimal qpnr = aqpp.getQtdaPagaNaoRetirada();
					if (qpnr == null) {
						qpnr = BigDecimal.ZERO;
					}
					valorQnr = valorQnr.add(aqpp.getQtdaNaoRetirada());
					valorQpnr = valorQpnr.add(qpnr);
				}
			}

			pontoConsumoVO.setQnr(
					Util.converterCampoValorParaString(valorQnr, Constantes.FORMATO_VALOR_METROS_CUBICOS, Constantes.LOCALE_PADRAO));

			pontoConsumoVO.setQpnr(
					Util.converterCampoValorParaString(valorQpnr, Constantes.FORMATO_VALOR_METROS_CUBICOS, Constantes.LOCALE_PADRAO));
		}
	}

	// O contrato que irá receber o saldo do contrato encerrado deverá possuir um contrato modalidade cadastrado com pelo menos um take or
	// pay
	// configurado
	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#transferirSaldoQPNR(br.com.ggas.faturamento.
	 * apuracaopenalidade .impl .MigrarSaldoQPNRVO)
	 */
	// se não possuir ele não deixará a migração ocorrer
	@Override
	public void transferirSaldoQPNR(MigrarSaldoQPNRVO migrarSaldo) throws NegocioException, ConcorrenciaException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		String idPenalidadeToP = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		validarTransferenciaSaldo(migrarSaldo, controladorContrato, idPenalidadeToP);

		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		controladorEntidadeConteudo.obterEntidadeConteudo(itemFaturaGas);
		//PEGAR A PERIODICIDADE CORRETA
		EntidadeConteudo periodicidade = controladorEntidadeConteudo.obterEntidadeConteudo(CONSTANTE_NUMERO_TRINTA_TRES);

		Penalidade penalidadeTakeOrPay = controladorContrato.obterPenalidade(Long.parseLong(idPenalidadeToP));

		// Criar um novo ApuracaoQuantidadePenalidadePeriodicidade com o ponto de consumo associado
		PontoConsumo pontoReceberSaldo = (PontoConsumo) controladorPontoConsumo.obter(migrarSaldo.getChavePontoRecebeSaldo());
		Contrato contratoReceberSaldo = controladorContrato.obterContratoAtivoPontoConsumo(pontoReceberSaldo).getContrato();

		BigDecimal valorTransferidoQNR = migrarSaldo.getValorTransferidoQNR();
		BigDecimal valorTransferidoQPNR = migrarSaldo.getValorTransferidoQPNR();

		if (valorTransferidoQNR.compareTo(BigDecimal.ZERO) == 0 && valorTransferidoQPNR.compareTo(BigDecimal.ZERO) == 0) {
			throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_MIGRAR_ZERADO, true);
		}
		if (valorTransferidoQNR.compareTo(BigDecimal.ZERO) < 0 || valorTransferidoQPNR.compareTo(BigDecimal.ZERO) < 0) {
			throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_MIGRAR_NEGATIVO, true);
		}

		ContratoModalidade contratoModalidade = null;
		// Verifica as modalidades do ponto de consumo... se o ponto de consumo possuir apenas 1 modalidade e
		// essa for a mesma do ponto de consumo que recebe o saldo a modalidade será setada automaticamente

		for (Long idPonto : migrarSaldo.getIdsPontosConsumoDoaSaldo()) {

			PontoConsumo pontoDoarSaldo = (PontoConsumo) controladorPontoConsumo.obter(idPonto);
			Contrato contrato = controladorContrato.obterContratoEncerradoPontoConsumo(pontoDoarSaldo).getContrato();
			Collection<ContratoPontoConsumoModalidade> listaContratoPontoModalidades =
					controladorContrato.consultarContratoPontoConsumoModalidades(contrato, pontoDoarSaldo);
			if (listaContratoPontoModalidades != null && listaContratoPontoModalidades.size() == 1) {
				ContratoPontoConsumoModalidade cpcm = listaContratoPontoModalidades.iterator().next();
				contratoModalidade = cpcm.getContratoModalidade();
			} else if (listaContratoPontoModalidades != null && listaContratoPontoModalidades.size() > 1) {
				contratoModalidade = null;
				break;
			}
		}
		if (contratoModalidade != null) {
			PontoConsumo pontoRecebeSaldo = (PontoConsumo) controladorPontoConsumo.obter(migrarSaldo.getChavePontoRecebeSaldo());
			Contrato contrato = controladorContrato.obterContratoAtivoPontoConsumo(pontoRecebeSaldo).getContrato();
			Collection<ContratoPontoConsumoModalidade> listaContratoPontoModalidades =
					controladorContrato.consultarContratoPontoConsumoModalidades(contrato, pontoRecebeSaldo);
			if (listaContratoPontoModalidades != null && !listaContratoPontoModalidades.isEmpty()) {
				for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : listaContratoPontoModalidades) {
					if (contratoModalidade != null && contratoPontoConsumoModalidade.getContratoModalidade().getChavePrimaria() == contratoModalidade
							.getChavePrimaria()) {
						break;
					} else {
						contratoModalidade = null;
					}
				}

			}
		}

		ApuracaoQuantidadePenalidadePeriodicidade apuracaoQtdPenalPeriod = this.criarApuracaoQuantidadePenalidadePeriodicidade();

		apuracaoQtdPenalPeriod.setPontoConsumo(pontoReceberSaldo);
		apuracaoQtdPenalPeriod.setContrato(contratoReceberSaldo);
		apuracaoQtdPenalPeriod.setContratoAtual(contratoReceberSaldo);
		apuracaoQtdPenalPeriod.setQtdaNaoRetirada(valorTransferidoQNR);
		apuracaoQtdPenalPeriod.setQtdaPagaNaoRetirada(valorTransferidoQPNR);
		apuracaoQtdPenalPeriod.setDadosAuditoria(migrarSaldo.getDadosAuditoria());
		apuracaoQtdPenalPeriod.setPenalidade(penalidadeTakeOrPay);
		apuracaoQtdPenalPeriod.setPeriodicidadePenalidade(periodicidade);
		apuracaoQtdPenalPeriod.setPeriodoApuracao(Util.obterAnoMes(new Date()));
		apuracaoQtdPenalPeriod.setDataInicioApuracao(new Date());
		apuracaoQtdPenalPeriod.setDataFimApuracao(new Date());
		apuracaoQtdPenalPeriod.setQtdaDiariaRetirada(BigDecimal.ZERO);
		apuracaoQtdPenalPeriod.setContratoModalidade(contratoModalidade);

		BigDecimal valorTarifa =
				this.obterTarifaMedia(pontoReceberSaldo, itemFaturaGas, new Date(), migrarSaldo.getValorTransferidoQNR(), true, true);

		apuracaoQtdPenalPeriod.setValorTarifaMedia(valorTarifa);
		Collection<PontoConsumo> colecaoPontoConsumo = new ArrayList<>();
		colecaoPontoConsumo.add(pontoReceberSaldo);

		this.inserir(apuracaoQtdPenalPeriod);

		for (Long idPonto : migrarSaldo.getIdsPontosConsumoDoaSaldo()) {

			PontoConsumo pontoDoarSaldo = (PontoConsumo) controladorPontoConsumo.obter(idPonto);
			Contrato contrato = controladorContrato.obterContratoEncerradoPontoConsumo(pontoDoarSaldo).getContrato();
			Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItens = this.obterApuracoes(pontoDoarSaldo, contrato);
			BigDecimal valorQNRSerDoado = migrarSaldo.getValorTransferidoQNR();
			BigDecimal valorQPNRSerDoado = migrarSaldo.getValorTransferidoQPNR();
			atualizarQNRMigracao(apuraQuanPenPeriItens, valorQNRSerDoado);
			atualizarQPNRMigracao(apuraQuanPenPeriItens, valorQPNRSerDoado);

		}

	}

	/**
	 * Validar transferencia saldo.
	 *
	 * @param migrarSaldo the migrar saldo
	 * @param controladorContrato the controlador contrato
	 * @param idPenalidadeToP the id penalidade to p
	 * @throws NegocioException the negocio exception
	 */
	private void validarTransferenciaSaldo(MigrarSaldoQPNRVO migrarSaldo, ControladorContrato controladorContrato, String idPenalidadeToP)
			throws NegocioException {

		if (migrarSaldo.getIdsPontosConsumoDoaSaldo() == null || migrarSaldo.getIdsPontosConsumoDoaSaldo().length < 1) {
			throw new NegocioException(Constantes.ERRO_SELECIONE_PONTO_DOAR, true);
		}
		if (migrarSaldo.getChavePontoRecebeSaldo() == null || migrarSaldo.getChavePontoRecebeSaldo() < 1) {
			throw new NegocioException(Constantes.ERRO_SELECIONE_PONTO_RECEBER, true);
		}

		if (migrarSaldo.getValorTransferidoQNR().compareTo(migrarSaldo.getSaldoQNR()) > 0) {
			throw new NegocioException(Constantes.ERRO_VALOR_QNR_MAIOR_SALDO, true);
		}
		if (migrarSaldo.getValorTransferidoQPNR().compareTo(migrarSaldo.getSaldoQPNR()) > 0) {
			throw new NegocioException(Constantes.ERRO_VALOR_QPNR_MAIOR_SALDO, true);
		}

		Contrato contrato =
				controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(migrarSaldo.getChavePontoRecebeSaldo()).getContrato();
		ContratoPontoConsumoModalidade cpcm = controladorContrato
				.obterContratoPontoConsumoModalidade(migrarSaldo.getChavePontoRecebeSaldo(), contrato.getChavePrimaria(), null);
		if (cpcm == null) {
			throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_CONTRATO_SEM_MODALIDADE, true);
		}

		boolean contratoSemTopConfigurado = false;
		for (ContratoPontoConsumoPenalidade contratoPenalidades : cpcm.getListaContratoPontoConsumoPenalidade()) {
			if (contratoPenalidades.getPenalidade().getChavePrimaria() == Long.parseLong(idPenalidadeToP)) {
				contratoSemTopConfigurado = true;
				break;
			}

		}
		if (!contratoSemTopConfigurado) {
			throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_MIGRAR_SALDO_CONTRATO_SEM_MODALIDADE_TOP, true);
		}

	}

	/**
	 * Atualizar qpnr migracao.
	 *
	 * @param apuraQuanPenPeriItens the apura quan pen peri itens
	 * @param valorQPNRSerDoado the valor qpnr ser doado
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarQPNRMigracao(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItens,
			BigDecimal valorQPNRSerDoadoTmp) throws ConcorrenciaException, NegocioException {
		BigDecimal valorQPNRSerDoado = valorQPNRSerDoadoTmp;
		for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : apuraQuanPenPeriItens) {
			// Se o valor da qpnr for maior q o precisa ser doado, atualiza o valor da mesma
			BigDecimal qpnr = aqpp.getQtdaPagaNaoRetirada();
			if (qpnr == null) {
				qpnr = BigDecimal.ZERO;
			}

			if (qpnr.compareTo(valorQPNRSerDoado) > 0) {
				aqpp.setQtdaPagaNaoRetirada(aqpp.getQtdaPagaNaoRetirada().subtract(valorQPNRSerDoado));
				atualizar(aqpp, getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade());
				break;

			} else if (qpnr.compareTo(valorQPNRSerDoado) == 0) {
				aqpp.setQtdaPagaNaoRetirada(BigDecimal.ZERO);
				atualizar(aqpp, getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade());
				break;
			} else {
				BigDecimal qpnrAux = qpnr;
				if (qpnrAux.compareTo(BigDecimal.ZERO) != 0) {
					valorQPNRSerDoado = valorQPNRSerDoado.subtract(qpnrAux);
					aqpp.setQtdaPagaNaoRetirada(BigDecimal.ZERO);
					atualizar(aqpp, getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade());
				}
			}

		}

	}

	/**
	 * Atualizar qnr migracao.
	 *
	 * @param apuraQuanPenPeriItens the apura quan pen peri itens
	 * @param valorQNRSerDoadoTmp the valor qnr ser doado
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarQNRMigracao(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuraQuanPenPeriItens,
			BigDecimal valorQNRSerDoadoTmp) throws ConcorrenciaException, NegocioException {
		BigDecimal valorQNRSerDoado = valorQNRSerDoadoTmp;
		for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : apuraQuanPenPeriItens) {

			if (aqpp.getQtdaNaoRetirada().compareTo(valorQNRSerDoado) > 0) {
				aqpp.setQtdaNaoRetirada(aqpp.getQtdaNaoRetirada().subtract(valorQNRSerDoado));
				atualizar(aqpp, getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade());
				break;

			} else if (aqpp.getQtdaNaoRetirada().compareTo(valorQNRSerDoado) == 0) {
				aqpp.setQtdaNaoRetirada(BigDecimal.ZERO);
				atualizar(aqpp, getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade());
				break;
			} else {
				BigDecimal qnr = aqpp.getQtdaNaoRetirada();
				if (qnr.compareTo(BigDecimal.ZERO) != 0) {
					valorQNRSerDoado = valorQNRSerDoado.subtract(qnr);
					aqpp.setQtdaNaoRetirada(BigDecimal.ZERO);
					atualizar(aqpp, getClasseEntidadeApuracaoQuantidadePenalidadePeriodicidade());
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#obterSaldoRecuperavel(java.util.Date, java.lang.Long)
	 */
	@Override
	public BigDecimal obterSaldoRecuperavel(Date dataInicialApuracao, Long chavePonto) {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.LEFT_JOIN);

		BigDecimal saldoRecuperavel = BigDecimal.ZERO;

		if (chavePonto != null) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", chavePonto));
		}
		if (dataInicialApuracao != null) {
			criteria.add(Restrictions.lt("dataInicioApuracao", dataInicialApuracao));
		}

		criteria.addOrder(Order.asc("dataFimApuracao"));

		Collection<ApuracaoQuantidadePenalidadePeriodicidade> lista = criteria.list();

		if (lista != null && !lista.isEmpty()) {

			for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : lista) {
				if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada() != null) {
					saldoRecuperavel = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada().add(saldoRecuperavel);
				}
			}

		}
		return saldoRecuperavel;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade# atualizarPenalidadeModalidade(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> atualizarPenalidadeModalidade(Long idModalidade, Long idApuracaoPenalidade)
			throws NegocioException, ConcorrenciaException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		if (idModalidade == null || idModalidade <= 0) {
			throw new NegocioException(Constantes.ERRO_SELECIONE_UMA_MODALIDADE, true);
		}
		ApuracaoQuantidadePenalidadePeriodicidade apuracao = obterApuracaoQuantidadePenalidadePeriodicidade(idApuracaoPenalidade);
		controladorContrato.obterContratoModalidade(idModalidade);

		return listarApuracaoQuantidadePenalidadePeriodicidades(apuracao.getPeriodoApuracao(),
				apuracao.getPontoConsumo().getChavePrimaria(), apuracao.getPenalidade().getChavePrimaria(), apuracao.getContratoAtual());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * atualizarApuracaoQuantidadePenalidadePeriodicidade(br.com.ggas.faturamento
	 * .apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade)
	 */
	@Override
	public void atualizarApuracaoQuantidadePenalidadePeriodicidade(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
			throws ConcorrenciaException, NegocioException {

		this.atualizar(apuracao, ApuracaoQuantidadePenalidadePeriodicidadeImpl.class);
	}

	/**
	 * Obter contrato penalidade com menor periodicidade.
	 *
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @param codPenalidade the cod penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the contrato penalidade
	 * @throws NegocioException the negocio exception
	 */
	private ContratoPenalidade obterContratoPenalidadeComMenorPeriodicidade(Collection<ContratoPenalidade> listaContratoPenalidade,
			String codPenalidade, Date dataInicioApuracao, Date dataFinalApuracao) {

		ContratoPenalidade retorno = null;
		Collection<ContratoPenalidade> listaFiltrada =
				this.filtrarListaPorContratoPenalidade(listaContratoPenalidade, codPenalidade, dataInicioApuracao, dataFinalApuracao);
		retorno = this.obterContratoPenalidadeComMenorPeriodicidade(listaFiltrada);
		return retorno;

	}

	/**
	 * Obter contrato penalidade com maior periodicidade.
	 *
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @param codPenalidade the cod penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the contrato penalidade
	 * @throws NegocioException the negocio exception
	 */
	private ContratoPenalidade obterContratoPenalidadeComMaiorPeriodicidade(Collection<ContratoPenalidade> listaContratoPenalidade,
			String codPenalidade, Date dataInicioApuracao, Date dataFinalApuracao) {

		ContratoPenalidade retorno = null;
		Collection<ContratoPenalidade> listaFiltrada =
				this.filtrarListaPorContratoPenalidade(listaContratoPenalidade, codPenalidade, dataInicioApuracao, dataFinalApuracao);
		retorno = this.obterContratoPenalidadeComMaiorPeriodicidade(listaFiltrada);
		return retorno;

	}

	/**
	 * Obter contrato penalidade com menor periodicidade.
	 *
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @return the contrato penalidade
	 */
	private ContratoPenalidade obterContratoPenalidadeComMenorPeriodicidade(Collection<ContratoPenalidade> listaContratoPenalidade) {

		ContratoPenalidade retorno = null;

		// Ordenar por periodicidade e retornar o
		// primeiro elemento.
		if (listaContratoPenalidade != null && !listaContratoPenalidade.isEmpty()) {
			BeanComparator comparador = new BeanComparator("periodicidadePenalidade.codigo");
			Collections.sort((List) listaContratoPenalidade, comparador);
			retorno = listaContratoPenalidade.iterator().next();
		}

		return retorno;

	}

	/**
	 * Obter contrato penalidade com maior periodicidade.
	 *
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @return the contrato penalidade
	 */
	private ContratoPenalidade obterContratoPenalidadeComMaiorPeriodicidade(Collection<ContratoPenalidade> listaContratoPenalidade) {

		ContratoPenalidade retorno = null;

		// Ordenar por periodicidade e retornar o
		// primeiro elemento.
		if (listaContratoPenalidade != null && !listaContratoPenalidade.isEmpty()) {
			BeanComparator comparador = new BeanComparator("periodicidadePenalidade.codigo");
			Collections.sort((List) listaContratoPenalidade, comparador);
			retorno = (ContratoPenalidade) ((List) listaContratoPenalidade).get(listaContratoPenalidade.size() - 1);
		}

		return retorno;

	}

	/**
	 * Filtrar lista por contrato penalidade.
	 *
	 * @param listaContratoPenalidade the lista contrato penalidade
	 * @param codPenalidade the cod penalidade
	 * @param dataInicioApuracao the data inicio apuracao
	 * @param dataFinalApuracao the data final apuracao
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<ContratoPenalidade> filtrarListaPorContratoPenalidade(Collection<ContratoPenalidade> listaContratoPenalidade,
			String codPenalidade, Date dataInicioApuracao, Date dataFinalApuracao) {

		List<ContratoPenalidade> listaContratoPenalidadesAux = new ArrayList<>();

		if (listaContratoPenalidade != null && !listaContratoPenalidade.isEmpty()) {
			for (ContratoPenalidade contratoPenalidade : listaContratoPenalidade) {
				if (contratoPenalidade.getPenalidade().getChavePrimaria() == Long.parseLong(codPenalidade)) {

					// se data de inicio e fim de apuração não for nula no paramentro, será penalidade TOP que deve checar as vigências.
					if (dataInicioApuracao != null && dataFinalApuracao != null) {
						if (dataInicioApuracao.compareTo(contratoPenalidade.getDataInicioVigencia()) >= 0
								&& dataFinalApuracao.compareTo(contratoPenalidade.getDataFimVigencia()) <= 0) {
							listaContratoPenalidadesAux.add(contratoPenalidade);
						}
					} else {
						listaContratoPenalidadesAux.add(contratoPenalidade);
					}
				}
			}
		}

		return listaContratoPenalidadesAux;

	}

	/**
	 * Converter penalidade contrato ponto consumo penalidade.
	 *
	 * @param compromisso the compromisso
	 * @return the contrato ponto consumo penalidade
	 * @throws NegocioException the negocio exception
	 */
	private ContratoPontoConsumoPenalidade converterPenalidadeContratoPontoConsumoPenalidade(ContratoPenalidade compromisso) {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade =
				(ContratoPontoConsumoPenalidade) controladorContrato.criarContratoPontoConsumoPenalidade();

		contratoPontoConsumoPenalidade.setConsumoReferencia(compromisso.getConsumoReferencia());
		contratoPontoConsumoPenalidade.setPercentualMargemVariacao(compromisso.getPercentualMargemVariacao());
		contratoPontoConsumoPenalidade.setPeriodicidadePenalidade(compromisso.getPeriodicidadePenalidade());
		contratoPontoConsumoPenalidade.setReferenciaQFParadaProgramada(compromisso.getReferenciaQFParadaProgramada());
		contratoPontoConsumoPenalidade.setApuracaoCasoFortuito(compromisso.getApuracaoCasoFortuito());
		contratoPontoConsumoPenalidade.setApuracaoFalhaFornecimento(compromisso.getApuracaoFalhaFornecimento());
		contratoPontoConsumoPenalidade.setApuracaoParadaProgramada(compromisso.getApuracaoParadaProgramada());
		contratoPontoConsumoPenalidade.setConsideraCasoFortuito(compromisso.getConsideraCasoFortuito());
		contratoPontoConsumoPenalidade.setConsideraFalhaFornecimento(compromisso.getConsideraFalhaFornecimento());
		contratoPontoConsumoPenalidade.setConsideraParadaProgramada(compromisso.getConsideraParadaProgramada());
		contratoPontoConsumoPenalidade.setDataFimVigencia(compromisso.getDataFimVigencia());
		contratoPontoConsumoPenalidade.setDataInicioVigencia(compromisso.getDataInicioVigencia());
		contratoPontoConsumoPenalidade.setPercentualNaoRecuperavel(compromisso.getPercentualNaoRecuperavel());
		contratoPontoConsumoPenalidade.setPrecoCobranca(compromisso.getPrecoCobranca());
		contratoPontoConsumoPenalidade.setRecuperavel(compromisso.getRecuperavel());
		contratoPontoConsumoPenalidade.setTipoApuracao(compromisso.getTipoApuracao());
		contratoPontoConsumoPenalidade.setValorPercentualRetMaiorMenor(compromisso.getValorPercentualRetMaiorMenor());
		contratoPontoConsumoPenalidade.setValorPercentualCobRetMaiorMenor(compromisso.getValorPercentualCobRetMaiorMenor());
		contratoPontoConsumoPenalidade.setValorPercentualCobIntRetMaiorMenor(compromisso.getValorPercentualCobIntRetMaiorMenor());
		contratoPontoConsumoPenalidade.setBaseApuracao(compromisso.getBaseApuracao());
		contratoPontoConsumoPenalidade.setPenalidade(compromisso.getPenalidade());

		return contratoPontoConsumoPenalidade;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#listarPenalidadesRetMaiorMenor()
	 */
	@Override
	public Collection<Penalidade> listarPenalidadesRetMaiorMenor() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idPenalidadeRetMaior =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));
		Long idPenalidadeRetMenor =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));

		Long[] chavesPenalidades = { idPenalidadeRetMaior, idPenalidadeRetMenor };

		Criteria criteria = this.createCriteria(Penalidade.class);
		criteria.add(Restrictions.eq("habilitado", true));
		criteria.add(Restrictions.in("chavePrimaria", chavesPenalidades));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/**
	 * Obter qdp contrato.
	 *
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param data the data
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	public BigDecimal obterQDPContrato(Contrato contrato, PontoConsumo pontoConsumo, Date data) throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		BigDecimal retorno = BigDecimal.ZERO;

		BigDecimal qDCDiaria = controladorContrato.obterQdcContratoValidoPorData(data, contrato.getChavePrimaria());

		BigDecimal qDPPontoConsumo = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" Select solicitacao.valorQDP ");
		hql.append(" from ");
		hql.append("SolicitacaoConsumoPontoConsumoImpl ");
		hql.append(" solicitacao ");
		hql.append(" where ");
		hql.append(" solicitacao.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and solicitacao.dataSolicitacao = :data");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setDate("data", data);

		qDPPontoConsumo = (BigDecimal) query.uniqueResult();

		if (qDPPontoConsumo != null) {
			retorno = retorno.add(qDPPontoConsumo);
		} else {
			retorno = retorno.add(qDCDiaria);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * aplicarTarifaMediaMomentoCobranca(java.util.Collection, boolean)
	 */
	@Override
	public void aplicarTarifaMediaMomentoCobranca(Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades,
			boolean isAplicarTarifa) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		String tabelaPrecoMomentoCobrancaPenalidade =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TABELA_PRECOS_VIGENTE_MOMENTO_COBRANCA);
		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		Long idPenalidadeRetMaior =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR));
		Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR));

		Long idPenalidadeTop =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY));

		Date dataAtual = new Date();

		for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : listaPenalidades) {

			BigDecimal percentualCobranca = BigDecimal.ZERO;
			BigDecimal valorCalculado = BigDecimal.ZERO;

			Map<String, Object> params = new HashMap<>();
			params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR,
					apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria());
			params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao());
			params.put(PARAM_NAME_DATA_FIM_VIGENCIA, apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());

			ContratoPenalidade contratoPenalidade =
					controladorContrato.obterContratoPenalidade(apuracaoQuantidadePenalidadePeriodicidade.getContrato().getChavePrimaria(),
							apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria(),
							apuracaoQuantidadePenalidadePeriodicidade.getPeriodicidadePenalidade().getChavePrimaria(),
							apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao(),
							apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());
			if (contratoPenalidade != null) {
				percentualCobranca = contratoPenalidade.getValorPercentualCobRetMaiorMenor();
			}

			if (apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade() != null) {
				ContratoPontoConsumoModalidade cpcm = controladorContrato.obterContratoPontoConsumoModalidade(
						apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo().getChavePrimaria(),
						apuracaoQuantidadePenalidadePeriodicidade.getContratoAtual().getChavePrimaria(),
						apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade().getChavePrimaria());

				params.put(PARAM_NAME_ID_CONTRATO_MODALIDADE, cpcm.getChavePrimaria());
				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade =
						controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);
				if (contratoPontoConsumoPenalidade != null) {
					percentualCobranca = contratoPontoConsumoPenalidade.getValorPercentualCobRetMaiorMenor();
				}
			}

			if (tabelaPrecoMomentoCobrancaPenalidade != null) {
				// Se não tem valor tarifa média e valor calculado... o tipo da tarifa é aplicada no momento
				// calcula a tarifa média passando o dia atual como parametro

				BigDecimal volume = BigDecimal.ZERO;

				if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == idPenalidadeRetMaior) {
					volume = apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetiradaMaior();
				} else if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == idPenalidadeTop) {
					volume = apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada();
				}

				if ((apuracaoQuantidadePenalidadePeriodicidade.getValorTarifaMedia() == null
						&& apuracaoQuantidadePenalidadePeriodicidade.getValorCalculado() == null)
						|| (apuracaoQuantidadePenalidadePeriodicidade.getValorTarifaMedia().compareTo(BigDecimal.ZERO) == 0
								&& apuracaoQuantidadePenalidadePeriodicidade.getValorCalculado().compareTo(BigDecimal.ZERO) == 0)) {

					BigDecimal tarifaMediaGas = this.obterTarifaMedia(apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo(),
							itemFaturaGas, dataAtual, volume, true, false);

					if (tarifaMediaGas != null) {

						BigDecimal valorTarifaGasTransporte = tarifaMediaGas;
						valorTarifaGasTransporte = valorTarifaGasTransporte.setScale(ESCALA_GAS, BigDecimal.ROUND_HALF_DOWN);

						valorCalculado = valorTarifaGasTransporte.multiply(volume);
						valorCalculado = valorCalculado.setScale(ESCALA_VALOR_CALCULADO, BigDecimal.ROUND_HALF_DOWN);

						if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == idPenalidadeRetMaior) {
							BigDecimal valorCalculadoPercentualCobranca = valorCalculado.multiply(percentualCobranca);
							apuracaoQuantidadePenalidadePeriodicidade.setValorCalculadoPercenCobranca(valorCalculadoPercentualCobranca);
						}

						apuracaoQuantidadePenalidadePeriodicidade.setValorCalculado(valorCalculado);

						apuracaoQuantidadePenalidadePeriodicidade.setValorTarifaMedia(valorTarifaGasTransporte);

						if (isAplicarTarifa) {
							this.atualizar(apuracaoQuantidadePenalidadePeriodicidade, apuracaoQuantidadePenalidadePeriodicidade.getClass());
						}

					}

				}
			}

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade# validarRecuperacaoPenalidade(java.lang.String[],
	 * java.lang.String[], java.util.List, java.lang.Long, br.com.ggas.faturamento.rubrica.Rubrica)
	 */
	@Override
	public BigDecimal validarRecuperacaoPenalidade(String[] valorQPNR, String[] volumeRecuperacao,
			List<ApuracaoQuantidadePenalidadePeriodicidade> lista, Long idPontoConsumo, Rubrica rubrica)
			throws FormatoInvalidoException, NegocioException {

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		BigDecimal volumeQPNRValor = null;
		BigDecimal volumeRecuperacaoValor = null;
		BigDecimal saldoVolumeRecuperacaoValor = BigDecimal.ZERO;
		if (rubrica == null) {
			throw new NegocioException(Constantes.ERRO_RUBRICA_OBRIGATORIA, true);
		}
		for (int i = 0; i < valorQPNR.length; i++) {
			ApuracaoQuantidadePenalidadePeriodicidade aqpp = lista.get(i);
			String volumeQPNRString = valorQPNR[i];
			String volumeRecuperacaoString = volumeRecuperacao[i];
			volumeQPNRValor = Util.converterCampoStringParaValorBigDecimal("Volume QPNR", volumeQPNRString, Constantes.FORMATO_VALOR_BR,
					Constantes.LOCALE_PADRAO);
			volumeRecuperacaoValor = Util.converterCampoStringParaValorBigDecimal("Volume Recuperação", volumeRecuperacaoString,
					Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			if (volumeRecuperacaoValor.compareTo(volumeQPNRValor) > 0) {
				throw new NegocioException(Constantes.ERRO_VOLUMERECUPERAVEL_MAIOR_QPNR, true);
			}

			aqpp.setQtdaPagaNaoRetirada(aqpp.getQtdaPagaNaoRetirada().subtract(volumeRecuperacaoValor));
			if (aqpp.getQtdaRecuperada() != null) {
				aqpp.setQtdaRecuperada(aqpp.getQtdaRecuperada().add(volumeRecuperacaoValor));
			} else {
				aqpp.setQtdaRecuperada(volumeRecuperacaoValor);
			}

			Set<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP = new HashSet<>();
			listaAQPP.add(aqpp);

			Recuperacao recuperacao = criaRecuperacao();
			if (!listaAQPP.isEmpty()) {
				recuperacao.setListaApuracaoQuantidadePenalidadePeriodicidade(listaAQPP);
			}
			recuperacao.setDataRecuperacao(new Date());
			recuperacao.setMedicaoQR(volumeRecuperacaoValor);
			recuperacao.setUltimaAlteracao(new Date());
			aqpp.getListaRecuperacao().add(recuperacao);

			saldoVolumeRecuperacaoValor = saldoVolumeRecuperacaoValor.add(volumeRecuperacaoValor);

		}

		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);

		if (rubrica.getItemFatura() == null) {
			throw new NegocioException(Constantes.ERRO_RUBRICA_NAO_POSSUI_ITEM_FATURA, true);
		}

		return this.obterTarifaMedia(pontoConsumo, rubrica.getItemFatura().getChavePrimaria(), new Date(), saldoVolumeRecuperacaoValor,
				true, false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#listarApuracaoQuantidadePenalidadePeriodicidadePorChaves(
	 * java .lang .Long[])
	 */
	@Override
	public List<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidadePorChaves(
			Long[] chavesApuracaoQuantidadePenaPeri) throws NegocioException {

		Criteria criteria = this.createCriteria(ApuracaoQuantidadePenalidadePeriodicidade.class);
		criteria.add(Restrictions.in("chavePrimaria", chavesApuracaoQuantidadePenaPeri));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * obterQNRGrupoEconomicoPorPontoConsumo(br.com.ggas.cadastro.imovel. PontoConsumo)
	 */
	@Override
	public BigDecimal obterQNRGrupoEconomicoPorPontoConsumo(PontoConsumo pontoConsumo) {

		BigDecimal valorDesconto = null;

		if (mapaQNRGrupoEconomico != null && mapaQNRGrupoEconomico.containsKey(pontoConsumo)) {
			valorDesconto = mapaQNRGrupoEconomico.get(pontoConsumo);
		}

		return valorDesconto;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#
	 * criarMapaGrupoEconomicoPontoConsumo(java.util.Collection)
	 */
	@Override
	public void criarMapaGrupoEconomicoPontoConsumo(Collection<Contrato> listaContratos) throws GGASException {

		mapaQNRGrupoEconomico = null;

		Map<String, Collection<PontoConsumo>> mapGrupoEconomico = new LinkedHashMap<>();

		mapaQNRGrupoEconomico = prepararCalculoDescontoGrupoEconomico(listaContratos, mapGrupoEconomico);

	}

	/**
	 * Preparar calculo desconto grupo economico.
	 *
	 * @param listaContratos the lista contratos
	 * @param mapGrupoEconomico the map grupo economico
	 * @return the map
	 * @throws GGASException the GGAS exception
	 */
	private Map<PontoConsumo, BigDecimal> prepararCalculoDescontoGrupoEconomico(Collection<Contrato> listaContratos,
			Map<String, Collection<PontoConsumo>> mapGrupoEconomico) throws GGASException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorCliente controladorCliente =
				(ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		String tipoGrupoEconomicoModalidade =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_GRUPO_ECONOMICO);
		String codPenalidadeTop = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		Map<String, Object> params = new HashMap<>();

		for (Contrato contrato : listaContratos) {
			Cliente cliente = (Cliente) controladorCliente.obter(contrato.getClienteAssinatura().getChavePrimaria(), "enderecos");

			if (cliente.getGrupoEconomico() != null && cliente.getGrupoEconomico().isHabilitado()) {

				Collection<PontoConsumo> listaPontosConsumo =
						controladorPontoConsumo.listarPontoConsumoPorChaveContrato(contrato.getChavePrimaria(), cliente.getChavePrimaria());

				if (listaPontosConsumo != null && !listaPontosConsumo.isEmpty()) {

					PontoConsumo primeiroPontoConsumo = listaPontosConsumo.iterator().next();

					HistoricoConsumo ultimoHistoricoConsumo =
							controladorHistoricoConsumo.obterUltimoHistoricoConsumoFaturado(primeiroPontoConsumo.getChavePrimaria());

					if (ultimoHistoricoConsumo != null) {

						Date dataInicialMes = null;
						Date dataFinalMes = null;

						String dataInicialAux = "01/" + Util.formatarAnoMes(ultimoHistoricoConsumo.getAnoMesFaturamento());
						dataInicialMes = Util.converterCampoStringParaData("Data Inicial", dataInicialAux, Constantes.FORMATO_DATA_BR);
						dataFinalMes = Util.converterCampoStringParaData("Data Final",
								Util.obterUltimoDiaMes(dataInicialMes, Constantes.FORMATO_DATA_BR), Constantes.FORMATO_DATA_BR);

						Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades =
								controladorContrato.consultarContratoPontoConsumoModalidades(contrato, primeiroPontoConsumo);

						for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumoModalidades) {

							params.put(PARAM_NAME_ID_CONTRATO_MODALIDADE, contratoPontoConsumoModalidade.getChavePrimaria());

							params.put(PARAM_NAME_DATA_INICIO_VIGENCIA, dataInicialMes);
							params.put(PARAM_NAME_DATA_FIM_VIGENCIA, dataFinalMes);
							params.put(PARAM_NAME_ID_PENALIDADE_RETIRADA_MAIOR_MENOR, Long.valueOf(codPenalidadeTop));

							ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeTOP =
									controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);

							if (contratoPontoConsumoPenalidadeTOP != null
									&& contratoPontoConsumoPenalidadeTOP.getTipoAgrupamentoContrato() != null
									&& contratoPontoConsumoPenalidadeTOP.getTipoAgrupamentoContrato().getChavePrimaria() == Long
											.parseLong(tipoGrupoEconomicoModalidade)) {

								if (mapGrupoEconomico.containsKey(cliente.getGrupoEconomico().getDescricao())) {
									mapGrupoEconomico.get(cliente.getGrupoEconomico().getDescricao()).addAll(listaPontosConsumo);
								} else {
									Collection<PontoConsumo> pontos = new ArrayList<>();
									pontos.addAll(listaPontosConsumo);
									mapGrupoEconomico.put(cliente.getGrupoEconomico().getDescricao(), pontos);
								}

							}
						}
					}
				}

			}
		}

		return calcularQNRGrupoEconomico(mapGrupoEconomico, params, controladorContrato);
	}

	/**
	 * Calcular qnr grupo economico.
	 *
	 * @param mapGrupoEconomico the map grupo economico
	 * @param params the params
	 * @param controladorContrato the controlador contrato
	 * @return the map
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private Map<PontoConsumo, BigDecimal> calcularQNRGrupoEconomico(Map<String, Collection<PontoConsumo>> mapGrupoEconomico,
			Map<String, Object> params, ControladorContrato controladorContrato) throws NegocioException, ConcorrenciaException {

		ControladorProgramacao controladorProgramacao =
				(ControladorProgramacao) ServiceLocator.getInstancia().getBeanPorID(ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Date dataInicioVigencia = (Date) params.get(PARAM_NAME_DATA_INICIO_VIGENCIA);
		Date dataFimVigencia = (Date) params.get(PARAM_NAME_DATA_FIM_VIGENCIA);

		String valorPeriodicidadeMensal =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_MENSAL);
		String codPenalidadeTop = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		Long idPeriodicidadeMensal = Long.valueOf(valorPeriodicidadeMensal);
		Long idPenalidadeTOP = Long.valueOf(codPenalidadeTop);

		BigDecimal qdcContrato = null;

		final Long[] chavesTipoParada = new Long[] { EntidadeConteudo.CHAVE_PARADA_PROGRAMADA, EntidadeConteudo.CHAVE_FALHA_FORNECIMENTO,
				EntidadeConteudo.CHAVE_PARADA_CASO_FORTUITO };
		Collection<Date> diasFornecimento = Util.obterDatasIntermediarias(dataInicioVigencia, dataFimVigencia);

		Map<PontoConsumo, BigDecimal> mapaPontoQnrGrupoEconomico = new HashMap<>();

		Map<PontoConsumo, BigDecimal> mapaPontoQnr = new HashMap<>();
		for (Map.Entry<String, Collection<PontoConsumo>> entry : mapGrupoEconomico.entrySet()) {

			BigDecimal consumoMinimoPeriodoGrupo = BigDecimal.ZERO;
			BigDecimal qRetPontoConsumoGrupo = BigDecimal.ZERO;
			BigDecimal qParadasPorGrupoEconomico = BigDecimal.ZERO;
			BigDecimal qRecGrupo = BigDecimal.ZERO;
			BigDecimal qnrGrupo = BigDecimal.ZERO;

			Collection<PontoConsumo> lista = entry.getValue();
			for (PontoConsumo pontoConsumo : lista) {
				BigDecimal qRetPontoConsumo = BigDecimal.ZERO;
				BigDecimal consumoMinimoPeriodo = BigDecimal.ZERO;

				// 1 Verificar o consumo minimo de cada ponto de consumo e somar por grupo.
				// 2. O somatório da quantidade Retirada e recuperada do grupo
				// 3. volume das paradas programadas,força maior e falha fornecimento

				// qdc multiplicado pelo Compromisso de retirada ToP: pelo periodo
				ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

				Collection<ContratoPontoConsumoModalidade> contratoPontoConsumoModalidades =
						controladorContrato.consultarContratoPontoConsumoModalidades(contratoPontoConsumo.getContrato(),
								contratoPontoConsumo.getPontoConsumo());

				qdcContrato = obterQdcContratoPontoConsumoModalidade(contratoPontoConsumo.getListaContratoPontoConsumoModalidade(),
						dataInicioVigencia, dataFimVigencia);
				if (qdcContrato == null) {
					qdcContrato = controladorContrato.obterSomaQdcContratoPorPeriodo(contratoPontoConsumo.getContrato().getChavePrimaria(),
							dataInicioVigencia, dataFimVigencia);
				}
				for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumoModalidades) {

					params.put(PARAM_NAME_ID_CONTRATO_MODALIDADE, contratoPontoConsumoModalidade.getChavePrimaria());
					// Consumo mínimo no periodo
					ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidadeMaior =
							controladorContrato.obterContratoPontoConsumoPenalidadeVigencia(params);
					consumoMinimoPeriodo = qdcContrato.multiply(contratoPontoConsumoPenalidadeMaior.getPercentualMargemVariacao());

					consumoMinimoPeriodoGrupo = consumoMinimoPeriodoGrupo.add(consumoMinimoPeriodo);

					// quantidade retirada

					for (Date data : diasFornecimento) {
						qRetPontoConsumo = qRetPontoConsumo.add(this.obterConsumoDiario(pontoConsumo, data));
					}
					qRetPontoConsumoGrupo = qRetPontoConsumoGrupo.add(qRetPontoConsumo);

					// quantidade Recuperada

					Long[] chavesPontosConsumo = new Long[] { pontoConsumo.getChavePrimaria() };
					// somatorio das paradas
					Collection<ParadaProgramada> listaParadasProgramadas = controladorProgramacao.consultarParadasProgramadasPeriodo(
							chavesPontosConsumo, chavesTipoParada, dataInicioVigencia, dataFimVigencia, null);
					for (ParadaProgramada parada : listaParadasProgramadas) {
						qParadasPorGrupoEconomico = qParadasPorGrupoEconomico.add(parada.getVolumeFornecimento());
					}

					ApuracaoVO apuracaoVO = controladorApuracaoPenalidade.obterConsumoDistribuidoPorModalidade(
							contratoPontoConsumo.getContrato(), pontoConsumo, ControladorApuracaoPenalidade.DISTRIBUICAO_APURACAO,
							idPenalidadeTOP, idPeriodicidadeMensal, dataInicioVigencia, dataFimVigencia, null);
					if (apuracaoVO != null) {
						BigDecimal qnr = BigDecimal.ZERO;
						for (ApuracaoQuantidadePenalidadePeriodicidade aqpp : apuracaoVO
								.getListaApuracaoQuantidadePenalidadePeriodicidade()) {
							if (aqpp.getQtdaNaoRetirada() != null) {
								qnr = qnr.add(aqpp.getQtdaNaoRetirada());
							}
						}

						mapaPontoQnr.put(pontoConsumo, qnr);
						qnrGrupo = qnrGrupo.add(qnr);
					}

					Long[] arrayPontoConsumo = { pontoConsumo.getChavePrimaria() };
					Collection<Recuperacao> listaRecuperacao = controladorProgramacao
							.consultarVolumeExecucaoContratoRecuperacao(dataInicioVigencia, dataInicioVigencia, arrayPontoConsumo);

					for (Recuperacao recuperacao : listaRecuperacao) {
						qRecGrupo = qRecGrupo.add(recuperacao.getMedicaoQR());
					}
				}
			}
			BigDecimal qnrGrupoEconomico = BigDecimal.ZERO;
			BigDecimal qtdRetiradaRecuperada = qRetPontoConsumoGrupo.subtract(qRecGrupo);
			qnrGrupoEconomico = consumoMinimoPeriodoGrupo.subtract(qtdRetiradaRecuperada).subtract(qParadasPorGrupoEconomico);

			mapearPontoConsumoQNRGrupoEconomico(mapaPontoQnrGrupoEconomico, qnrGrupoEconomico, mapaPontoQnr, qnrGrupo);

		}

		return mapaPontoQnrGrupoEconomico;
	}

	/**
	 * Mapear ponto consumo qnr grupo economico.
	 *
	 * @param mapaPontoQnrGrupoEconomico the mapa ponto qnr grupo economico
	 * @param qnrGrupoEconomicoTotal the qnr grupo economico total
	 * @param mapaPontoQnr the mapa ponto qnr
	 * @param qnrGrupoTotal the qnr grupo total
	 */
	private void mapearPontoConsumoQNRGrupoEconomico(Map<PontoConsumo, BigDecimal> mapaPontoQnrGrupoEconomico,
			BigDecimal qnrGrupoEconomicoTotal, Map<PontoConsumo, BigDecimal> mapaPontoQnr, BigDecimal qnrGrupoTotal) {

		BigDecimal qnrGrupoEconomico = BigDecimal.ZERO;

		BigDecimal qnrPonto = BigDecimal.ZERO;
		for (Map.Entry<PontoConsumo, BigDecimal> entry : mapaPontoQnr.entrySet()) {
			BigDecimal valorProporcionalQnr = BigDecimal.ZERO;
			valorProporcionalQnr = qnrGrupoEconomicoTotal.divide(qnrGrupoTotal, BigDecimal.ROUND_DOWN);

			qnrPonto = entry.getValue();

			qnrGrupoEconomico = qnrPonto.multiply(valorProporcionalQnr, MathContext.DECIMAL128).setScale(ESCALA_QNR_PONTO, RoundingMode.HALF_EVEN);
			mapaPontoQnrGrupoEconomico.put(entry.getKey(), qnrGrupoEconomico);

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade#obterPenalidade(java.lang.Long)
	 */
	@Override
	public Penalidade obterPenalidade(Long chavePrimaria) throws NegocioException {

		return (Penalidade) this.obter(chavePrimaria, Penalidade.class);
	}

}
