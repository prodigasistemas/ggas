/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 13/08/2014 10:43:21
 @author rfilho
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
/**
 * Classe responsável pela representação da entidade MigrarSaldoQPNR.
 *
 */
public class MigrarSaldoQPNRVO {

	private BigDecimal saldoQNR;

	private BigDecimal saldoQPNR;

	private Long chavePontoRecebeSaldo;

	private Long[] idsPontosConsumoDoaSaldo;

	private BigDecimal valorTransferidoQNR;

	private BigDecimal valorTransferidoQPNR;

	private Map<Long, BigDecimal> mapaPontoQNR;

	private Map<Long, BigDecimal> mapaPontoQPNR;

	private DadosAuditoria dadosAuditoria;

	public BigDecimal getSaldoQNR() {

		return saldoQNR;
	}

	public void setSaldoQNR(BigDecimal saldoQNR) {

		this.saldoQNR = saldoQNR;
	}

	public BigDecimal getSaldoQPNR() {

		return saldoQPNR;
	}

	public void setSaldoQPNR(BigDecimal saldoQPNR) {

		this.saldoQPNR = saldoQPNR;
	}

	public BigDecimal getValorTransferidoQNR() {

		return valorTransferidoQNR;
	}

	public void setValorTransferidoQNR(BigDecimal valorTransferidoQNR) {

		this.valorTransferidoQNR = valorTransferidoQNR;
	}

	public BigDecimal getValorTransferidoQPNR() {

		return valorTransferidoQPNR;
	}

	public void setValorTransferidoQPNR(BigDecimal valorTransferidoQPNR) {

		this.valorTransferidoQPNR = valorTransferidoQPNR;
	}

	public Long[] getIdsPontosConsumoDoaSaldo() {
		Long[] retorno = null;
		if (this.idsPontosConsumoDoaSaldo != null) {
			retorno = idsPontosConsumoDoaSaldo.clone();
		}
		return retorno;
	}

	public void setIdsPontosConsumoDoaSaldo(Long[] idsPontosConsumoDoaSaldo) {
		if (idsPontosConsumoDoaSaldo != null) {
			this.idsPontosConsumoDoaSaldo = idsPontosConsumoDoaSaldo.clone();
		} else {
			this.idsPontosConsumoDoaSaldo = null;
		}
	}

	public Map<Long, BigDecimal> getMapaPontoQNR() {

		return mapaPontoQNR;
	}

	public void setMapaPontoQNR(Map<Long, BigDecimal> mapaPontoQNR) {

		this.mapaPontoQNR = mapaPontoQNR;
	}

	public Map<Long, BigDecimal> getMapaPontoQPNR() {

		return mapaPontoQPNR;
	}

	public void setMapaPontoQPNR(Map<Long, BigDecimal> mapaPontoQPNR) {

		this.mapaPontoQPNR = mapaPontoQPNR;
	}

	public Long getChavePontoRecebeSaldo() {

		return chavePontoRecebeSaldo;
	}

	public void setChavePontoRecebeSaldo(Long chavePontoRecebeSaldo) {

		this.chavePontoRecebeSaldo = chavePontoRecebeSaldo;
	}

	public DadosAuditoria getDadosAuditoria() {

		return dadosAuditoria;
	}

	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {

		this.dadosAuditoria = dadosAuditoria;
	}

}
