/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a Recuperação. 
 *
 */
public interface Recuperacao extends EntidadeNegocio {

	/**
	 * BEAN_ID_RECUPERACAO
	 */
	String BEAN_ID_RECUPERACAO = "recuperacao";

	/**
	 * DATA
	 */
	String DATA = "RECUPERACAO_ROTULO_DATA";

	/**
	 * QR
	 */
	String QR = "RECUPERACAO_ROTULO_QR";

	/**
	 * RECUPERACAO
	 */
	String RECUPERACAO_ROTULO_RECUPERACAO = "RECUPERACAO_ROTULO_RECUPERACAO";

	/**
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 *            the fatura to set
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return the medicaoQR
	 */
	BigDecimal getMedicaoQR();

	/**
	 * @param medicaoQR
	 *            the medicaoQR to set
	 */
	void setMedicaoQR(BigDecimal medicaoQR);

	/**
	 * @return the data
	 */
	Date getDataRecuperacao();

	/**
	 * @param data
	 *            the data to set
	 */
	void setDataRecuperacao(Date data);

	/**
	 * @return the
	 *         apuracaoQuantidadePenalidadePeriodicidade
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> getListaApuracaoQuantidadePenalidadePeriodicidade();

	/**
	 * @param listaApuracaoQuantidadePenalidadePeriodicidade
	 *            the
	 *            listaApuracaoQuantidadePenalidadePeriodicidade
	 *            to set
	 */
	void setListaApuracaoQuantidadePenalidadePeriodicidade(
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade);

}
