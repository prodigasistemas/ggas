/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.collections.map.MultiKeyMap;

import br.com.ggas.medicao.consumo.ConsumoDistribuidoModalidade;

/**
 * ApuracaoVO
 * 
 * @author ccarvalho
 * 
 * Classe responsável por apuracao VO.
 */
public class ApuracaoVO implements Serializable {

	private static final long serialVersionUID = -8311787331857631513L;
	private Collection<ApuracaoQuantidade> listaApuracaoQuantidade;

	// 1 por modalidade
	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade;

	private Collection<Recuperacao> listaRecuperacao;

	private Collection<Recuperacao> listaQuantidadeRecuperada;

	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP;

	private Map<ConsumoDistribuidoModalidade, BigDecimal> mapaVolumeRetiradoPorModalidadeDia;

	private MultiKeyMap balde;

	private BigDecimal qDCMediaContratual;

	private BigDecimal qDPMediaContratual;

	private BigDecimal qDCAcumulada;

	private BigDecimal qDPAcumulada;

	private BigDecimal quantidadeVolumeParadas;

	public MultiKeyMap getBalde() {

		return balde;
	}

	public void setBalde(MultiKeyMap balde) {

		this.balde = balde;
	}

	/**
	 * Gets the q dc media contratual.
	 * 
	 * @return the q dc media contratual
	 */
	public BigDecimal getqDCMediaContratual() {

		return qDCMediaContratual;
	}

	/**
	 * Sets the q dc media contratual.
	 * 
	 * @param qDCMediaContratual
	 *            the new q dc media contratual
	 */
	public void setqDCMediaContratual(BigDecimal qDCMediaContratual) {

		this.qDCMediaContratual = qDCMediaContratual;
	}

	/**
	 * Gets the q dp media contratual.
	 * 
	 * @return the q dp media contratual
	 */
	public BigDecimal getqDPMediaContratual() {

		return qDPMediaContratual;
	}

	/**
	 * Sets the q dp media contratual.
	 * 
	 * @param qDPMediaContratual
	 *            the new q dp media contratual
	 */
	public void setqDPMediaContratual(BigDecimal qDPMediaContratual) {

		this.qDPMediaContratual = qDPMediaContratual;
	}

	/**
	 * Gets the q dc acumulada.
	 * 
	 * @return the q dc acumulada
	 */
	public BigDecimal getqDCAcumulada() {

		return qDCAcumulada;
	}

	/**
	 * Sets the q dc acumulada.
	 * 
	 * @param qDCAcumulada
	 *            the new q dc acumulada
	 */
	public void setqDCAcumulada(BigDecimal qDCAcumulada) {

		this.qDCAcumulada = qDCAcumulada;
	}

	/**
	 * Gets the q dp acumulada.
	 * 
	 * @return the q dp acumulada
	 */
	public BigDecimal getqDPAcumulada() {

		return qDPAcumulada;
	}

	/**
	 * Sets the q dp acumulada.
	 * 
	 * @param qDPAcumulada
	 *            the new q dp acumulada
	 */
	public void setqDPAcumulada(BigDecimal qDPAcumulada) {

		this.qDPAcumulada = qDPAcumulada;
	}

	public Collection<Recuperacao> getListaRecuperacao() {

		return listaRecuperacao;
	}

	public void setListaRecuperacao(Collection<Recuperacao> listaRecuperacao) {

		this.listaRecuperacao = listaRecuperacao;
	}

	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> getListaApuracaoQuantidadePenalidadePeriodicidade() {

		return listaApuracaoQuantidadePenalidadePeriodicidade;
	}

	public void setListaApuracaoQuantidadePenalidadePeriodicidade(
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade) {

		this.listaApuracaoQuantidadePenalidadePeriodicidade = listaApuracaoQuantidadePenalidadePeriodicidade;
	}

	public Map<ConsumoDistribuidoModalidade, BigDecimal> getMapaVolumeRetiradoPorModalidadeDia() {

		return mapaVolumeRetiradoPorModalidadeDia;
	}

	public void setMapaVolumeRetiradoPorModalidadeDia(Map<ConsumoDistribuidoModalidade, BigDecimal> mapaVolumeRetiradoPorModalidadeDia) {

		this.mapaVolumeRetiradoPorModalidadeDia = mapaVolumeRetiradoPorModalidadeDia;
	}

	public Collection<ApuracaoQuantidade> getListaApuracaoQuantidade() {

		return listaApuracaoQuantidade;
	}

	public void setListaApuracaoQuantidade(Collection<ApuracaoQuantidade> listaApuracaoQuantidade) {

		this.listaApuracaoQuantidade = listaApuracaoQuantidade;
	}

	public Collection<Recuperacao> getListaQuantidadeRecuperada() {

		return listaQuantidadeRecuperada;
	}

	public void setListaQuantidadeRecuperada(Collection<Recuperacao> listaQuantidadeRecuperada) {

		this.listaQuantidadeRecuperada = listaQuantidadeRecuperada;
	}

	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> getListaAQPP() {

		return listaAQPP;
	}

	public void setListaAQPP(Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaAQPP) {

		this.listaAQPP = listaAQPP;
	}

	public BigDecimal getQuantidadeVolumeParadas() {

		return quantidadeVolumeParadas;
	}

	public void setQuantidadeVolumeParadas(BigDecimal quantidadeVolumeParadas) {

		this.quantidadeVolumeParadas = quantidadeVolumeParadas;
	}

}
