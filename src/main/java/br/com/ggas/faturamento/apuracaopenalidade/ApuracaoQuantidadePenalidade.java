/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.math.BigDecimal;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a Penalidade aplicada 
 * de acordo com a Quantidade Apurada.
 *
 */
public interface ApuracaoQuantidadePenalidade extends EntidadeNegocio {

	/**
	 * BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE
	 */
	String BEAN_ID_APURACAO_QUANTIDADE_PENALIDADE = "apuracaoQuantidadePenalidade";

	/**
	 * @return
	 */
	BigDecimal getQtdaFaltanteCalculada();

	/**
	 * @param qtdaFaltanteCalculada
	 */
	void setQtdaFaltanteCalculada(BigDecimal qtdaFaltanteCalculada);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaRetiradaMaior();

	/**
	 * @param qtdaDiariaRetiradaMaior
	 */
	void setQtdaDiariaRetiradaMaior(BigDecimal qtdaDiariaRetiradaMaior);

	/**
	 * @return
	 */
	BigDecimal getValorCalculado();

	/**
	 * @param valorCalculado
	 */
	void setValorCalculado(BigDecimal valorCalculado);

	/**
	 * @return
	 */
	BigDecimal getValorTarifaMedia();

	/**
	 * @param valorTarifaMedia
	 */
	void setValorTarifaMedia(BigDecimal valorTarifaMedia);

	/**
	 * @return
	 */
	BigDecimal getPercentualDescontoAplicado();

	/**
	 * @param percentualDescontoAplicado
	 */
	void setPercentualDescontoAplicado(BigDecimal percentualDescontoAplicado);

	/**
	 * @return
	 */
	BigDecimal getValorDescontoAplicado();

	/**
	 * @param valorDescontoAplicado
	 */
	void setValorDescontoAplicado(BigDecimal valorDescontoAplicado);

	/**
	 * @return
	 */
	BigDecimal getValorAcrescido();

	/**
	 * @param valorAcrescido
	 */
	void setValorAcrescido(BigDecimal valorAcrescido);

	/**
	 * @return
	 */
	BigDecimal getValorCobrado();

	/**
	 * @param valorCobrado
	 */
	void setValorCobrado(BigDecimal valorCobrado);

	/**
	 * @return
	 */
	String getObservacoes();

	/**
	 * @param observacoes
	 */
	void setObservacoes(String observacoes);

	/**
	 * @return
	 */
	ApuracaoQuantidade getApuracaoQuantidade();

	/**
	 * @param apuracaoQuantidade
	 */
	void setApuracaoQuantidade(ApuracaoQuantidade apuracaoQuantidade);

	/**
	 * @return
	 */
	Penalidade getPenalidade();

	/**
	 * @param penalidade
	 */
	void setPenalidade(Penalidade penalidade);

	/**
	 * @return
	 */
	BigDecimal getQtdaDiariaRetirada();

	/**
	 * @param qtdaDiariaRetirada
	 */
	void setQtdaDiariaRetirada(BigDecimal qtdaDiariaRetirada);

}
