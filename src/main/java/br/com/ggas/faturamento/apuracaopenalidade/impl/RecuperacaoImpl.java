/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.Recuperacao;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * @author gmatos
 */
class RecuperacaoImpl extends EntidadeNegocioImpl implements Recuperacao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
     * 
     */
	private static final long serialVersionUID = -565276643460724232L;

	private Fatura fatura;

	private BigDecimal medicaoQR;

	private Date dataRecuperacao;

	private Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade = 
					new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl.Recuperacao#getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl
	 * .Recuperacao#setFatura(br.com.ggas.faturamento
	 * .Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl.Recuperacao#getMedicaoQR()
	 */
	@Override
	public BigDecimal getMedicaoQR() {

		return medicaoQR;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl
	 * .Recuperacao#setMedicaoQR(java.math.BigDecimal
	 * )
	 */
	@Override
	public void setMedicaoQR(BigDecimal medicaoQR) {

		this.medicaoQR = medicaoQR;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl.Recuperacao#getData()
	 */
	@Override
	public Date getDataRecuperacao() {

		return dataRecuperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .impl.Recuperacao#setData(java.util.Date)
	 */
	@Override
	public void setDataRecuperacao(Date data) {

		this.dataRecuperacao = data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Recuperacao#
	 * getListaApuracaoQuantidadePenalidadePeriodicidade
	 * ()
	 */
	@Override
	public Collection<ApuracaoQuantidadePenalidadePeriodicidade> getListaApuracaoQuantidadePenalidadePeriodicidade() {

		return listaApuracaoQuantidadePenalidadePeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Recuperacao#
	 * setListaApuracaoQuantidadePenalidadePeriodicidade
	 * (java.util.Collection)
	 */
	@Override
	public void setListaApuracaoQuantidadePenalidadePeriodicidade(
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoQuantidadePenalidadePeriodicidade) {

		this.listaApuracaoQuantidadePenalidadePeriodicidade = listaApuracaoQuantidadePenalidadePeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(medicaoQR == null) {
			stringBuilder.append(QR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataRecuperacao == null) {
			stringBuilder.append(DATA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
