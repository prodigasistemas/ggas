/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoDetalhadoVO;
import br.com.ggas.contrato.contrato.Contrato;
/**
 * Classe responsável pela representação da entidade PontoConsumo
 *
 */
public class PontoConsumoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8160179925203237088L;	

	private PontoConsumo pontoConsumo;

	private String qpnr;

	private String qnr;

	private Contrato contrato;

	private String descricao;

	private String situacao;

	private List<Chamado> listaChamado;

	private List<ServicoAutorizacao> listaServicoAutorizacao;

	private List<PontoConsumoDetalhadoVO> listaHistoricoConsumoPeriodo = new ArrayList<>();

	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	public Contrato getContrato() {

		return contrato;
	}

	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/**
	 * @return the listaChamado
	 */
	public List<Chamado> getListaChamado() {

		return listaChamado;
	}

	/**
	 * @param listaChamado
	 *            the listaChamado to set
	 */
	public void setListaChamado(List<Chamado> listaChamado) {

		this.listaChamado = listaChamado;
	}

	public List<ServicoAutorizacao> getListaServicoAutorizacao() {

		return listaServicoAutorizacao;
	}

	public void setListaServicoAutorizacao(List<ServicoAutorizacao> listaServicoAutorizacao) {

		this.listaServicoAutorizacao = listaServicoAutorizacao;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getSituacao() {

		return situacao;
	}

	public void setSituacao(String situacao) {

		this.situacao = situacao;
	}

	public List<PontoConsumoDetalhadoVO> getListaHistoricoConsumoPeriodo() {

		return listaHistoricoConsumoPeriodo;
	}

	public void setListaHistoricoConsumoPeriodo(List<PontoConsumoDetalhadoVO> listaHistoricoConsumoPeriodo) {

		this.listaHistoricoConsumoPeriodo = listaHistoricoConsumoPeriodo;
	}

	public String getQpnr() {

		return qpnr;
	}

	public void setQpnr(String qpnr) {

		this.qpnr = qpnr;
	}

	public String getQnr() {

		return qnr;
	}

	public void setQnr(String qnr) {

		this.qnr = qnr;
	}

}
