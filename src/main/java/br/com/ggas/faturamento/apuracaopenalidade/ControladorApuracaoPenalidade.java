/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.MultiKeyMap;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.impl.MigrarSaldoQPNRVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.QuantidadeContratadaVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.QuantidadeNaoRetiradaVO;
import br.com.ggas.faturamento.apuracaopenalidade.impl.QuantidadeRetiradaPeriodoVO;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.consumo.ApuracaoPeriodo;
import br.com.ggas.medicao.consumo.ModalidadeDataQuantidade;
import br.com.ggas.web.contrato.programacao.ApuracaoRecuperacaoQuantidadeVO;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador da Apuração de Penalidade.
 * 
 *
 */
public interface ControladorApuracaoPenalidade extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE = "controladorApuracaoPenalidade";

	String ERRO_NEGOCIO_DATA_INICIO_APURACAO_OBRIGATORIO = "ERRO_NEGOCIO_DATA_INICIO_APURACAO_OBRIGATORIO";

	String ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO = "ERRO_NEGOCIO_DATA_FIM_APURACAO_OBRIGATORIO";

	String ERRO_NEGOCIO_PERCENTUAL_DESCONTO_MAIOR_QUE_VALOR_CALCULADO = "ERRO_NEGOCIO_PERCENTUAL_DESCONTO_MAIOR_QUE_VALOR_CALCULADO";

	String ERRO_NEGOCIO_VALOR_DESCONTO_MAIOR_QUE_VALOR_CALCULADO = "ERRO_NEGOCIO_VALOR_DESCONTO_MAIOR_QUE_VALOR_CALCULADO";

	String ERRO_NEGOCIO_OBSERVACAO_OBRIGATORIO = "ERRO_NEGOCIO_OBSERVACAO_OBRIGATORIO";

	String ERRO_NEGOCIO_APURACAO_PENALIDADE_JA_COBRADA = "ERRO_NEGOCIO_APURACAO_PENALIDADE_JA_COBRADA";

	String ERRO_NEGOCIO_APURACAO_PERCENTUAL_DESCONTO_SEM_VALOR_DESCONTO = "ERRO_NEGOCIO_APURACAO_PERCENTUAL_DESCONTO_SEM_VALOR_DESCONTO";

	String ERRO_NEGOCIO_APURACAO_VALOR_DESCONTO_SEM_PERCENTUAL_DESCONTO = "ERRO_NEGOCIO_APURACAO_VALOR_DESCONTO_SEM_PERCENTUAL_DESCONTO";

	String ERRO_NEGOCIO_APURACAO_SEM_VOLUME = "ERRO_NEGOCIO_APURACAO_SEM_VOLUME";

	String ERRO_NEGOCIO_APURACAO_SEM_VALOR_CALCULADO = "ERRO_NEGOCIO_APURACAO_SEM_VALOR_CALCULADO";

	String ERRO_NEGOCIO_APURACAO_SEM_VALOR_MEDIA_TARIFA = "ERRO_NEGOCIO_APURACAO_SEM_VALOR_MEDIA_TARIFA";

	String ERRO_NEGOCIO_TIPO_CONSULTA_OBRIGATORIO = "ERRO_NEGOCIO_TIPO_CONSULTA_OBRIGATORIO";

	String ERRO_NEGOCIO_DATA_INICIO_OBRIGATORIO = "ERRO_NEGOCIO_DATA_INICIO_OBRIGATORIO";

	String ERRO_NEGOCIO_DATA_FIM_OBRIGATORIO = "ERRO_NEGOCIO_DATA_FIM_OBRIGATORIO";

	String ERRO_NEGOCIO_INTERVALO_DATAS_OBRIGATORIO = "ERRO_NEGOCIO_INTERVALO_DATAS_OBRIGATORIO";

	String ERRO_NEGOCIO_CONTRATO_MODALIDADE_OBRIGATORIO = "ERRO_NEGOCIO_CONTRATO_MODALIDADE_OBRIGATORIO";

	String ERRO_NEGOCIO_INTERVALO_DATAS_MAIOR_QUE_UM_ANO = "ERRO_NEGOCIO_INTERVALO_DATAS_MAIOR_QUE_UM_ANO";

	String ERRO_NEGOCIO_APURACAO_NAO_ENCONTRADA = "ERRO_NEGOCIO_APURACAO_NAO_ENCONTRADA";

	String ERRO_NEGOCIO_APURACAO_PENALIDADE_SEM_CONTRATO_ATUAL = "ERRO_NEGOCIO_APURACAO_PENALIDADE_SEM_CONTRATO_ATUAL";

	String ERRO_NEGOCIO_APURACAO_PENALIDADE_SEM_PONTO_CONSUMO = "ERRO_NEGOCIO_APURACAO_PENALIDADE_SEM_PONTO_CONSUMO";

	String DISTRIBUICAO_APURACAO = "DISTRIBUICAO_APURACAO";

	String DISTRIBUICAO_FATURAMENTO = "DISTRIBUICAO_FATURAMENTO";

	String DISTRIBUICAO_SIMULACAO = "DISTRIBUICAO_SIMULACAO";

	String PERIODICIDADE_APURACAO_MENSAL = "PERIODICIDADE_APURACAO_MENSAL";

	String PERIODICIDADE_APURACAO_ANUAL = "PERIODICIDADE_APURACAO_ANUAL";

	String ERRO_NEGOCIO_DATA_INICIAL_MAIOR_CONTRATO = "ERRO_NEGOCIO_DATA_INICIAL_MAIOR_CONTRATO";

	/**
	 * Método responsável por tratar as
	 * penalidades existentes no contrato.
	 * Esse método irá atualizar os campos
	 * referentes a valor das penalidades e
	 * compromissos.
	 * 
	 * @param contrato
	 *            Contrato
	 * @param listaPontoConsumo
	 *            Lista de Ponto de Consumo
	 * @param dataInicial
	 *            Data Inicial do período de
	 *            apuração
	 * @param dataFinal
	 *            Data Final do período de
	 *            apuração
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param logProcessamento
	 *            the log processamento
	 * @return the string builder
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */

	StringBuilder tratarPenalidades(Contrato contrato, Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal,
					DadosAuditoria dadosAuditoria, StringBuilder logProcessamento) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por distribuir o consumo
	 * de um ponto de consumo em suas modalidades.
	 * 
	 * @param contrato
	 *            O contrato
	 * @param pontoConsumo
	 *            O ponto de consumo
	 * @param tipoApuracao
	 *            the tipo apuracao
	 * @param idPenalidade
	 *            the id penalidade
	 * @param idPeriodicidade
	 *            the id periodicidade
	 * @param dataInicial
	 *            A data inicial
	 * @param dataFinal
	 *            A data final
	 * @param logProcessamento
	 *            the log processamento
	 * @return ApuracaoPeriodo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	ApuracaoVO obterConsumoDistribuidoPorModalidade(Contrato contrato, PontoConsumo pontoConsumo, String tipoApuracao, Long idPenalidade,
					Long idPeriodicidade, Date dataInicial, Date dataFinal, StringBuilder logProcessamento) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Método responsável por listar pontos de
	 * consumo e seus ultimos contratos.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PontoConsumoVO> pesquisarPontoConsumoPenalidade(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * penalidades.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Penalidade> listarPenalidades() throws NegocioException;

	/**
	 * Método responsável por listar
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return listaApuracaoQuantidadePenalidadePeriodicidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidades(Map<String, Object> filtro)
					throws NegocioException;

	/**
	 * Método responsável por obter
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return apuracaoQuantidadePenalidadePeriodicidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ApuracaoQuantidadePenalidadePeriodicidade obterApuracaoQuantidadePenalidadePeriodicidade(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 * 
	 * @param idContrato
	 *            the id contrato
	 * @param idPenalidade
	 *            the id penalidade
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param periodoApuracao
	 *            the periodo apuracao
	 * @param idModalidade
	 *            the id modalidade
	 * @return the apuracao quantidade penalidade periodicidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ApuracaoQuantidadePenalidadePeriodicidade obterApuracaoQuantidadePenalidadePeriodicidade(Long idContrato, Long idPenalidade,
					Long idPontoConsumo, Integer periodoApuracao, Long idModalidade) throws NegocioException;

	/**
	 * Método responsável por obter uma lista de
	 * QuandidadeContratadaVO.
	 * 
	 * @param apuracao
	 *            the apuracao
	 * @return lista de QuandidadeContratadaVO
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<QuantidadeContratadaVO> obterQuantidadeContratrada(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
					throws NegocioException;

	/**
	 * Método responsável por obter uma lista de
	 * QuantidadeNaoRetiradaVO.
	 * 
	 * @param apuracao
	 *            the apuracao
	 * @param chavePrimariaTipoParada
	 *            the chave primaria tipo parada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<QuantidadeNaoRetiradaVO> obterQuantidadeNaoRetirada(ApuracaoQuantidadePenalidadePeriodicidade apuracao,
					long chavePrimariaTipoParada) throws NegocioException;

	/**
	 * Método responsável por obter uma lista
	 * quantidade paga não recuperada.
	 * 
	 * @param recebimento
	 *            the recebimento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param contrato
	 *            the contrato
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarQuantidadePagaNaoRecuperada(Recebimento recebimento, DadosAuditoria dadosAuditoria, Contrato contrato)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por listar
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 * 
	 * @param periodo
	 *            the periodo
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param idPenalidade
	 *            the id penalidade
	 * @param contrato
	 *            the contrato
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidades(Integer periodo,
					long idPontoConsumo, long idPenalidade, Contrato contrato) throws NegocioException;

	/**
	 * Aplicar percentual desconto sobre valor total.
	 * 
	 * @param valorTotalFormatado
	 *            the valor total formatado
	 * @param percentualDescontoFormatado
	 *            the percentual desconto formatado
	 * @param valorFinalFormatado
	 *            the valor final formatado
	 * @param totalValorFinalFormatado
	 *            the total valor final formatado
	 * @return the string[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	String[] aplicarPercentualDescontoSobreValorTotal(String valorTotalFormatado, String percentualDescontoFormatado,
					String valorFinalFormatado, String totalValorFinalFormatado) throws NegocioException;

	/**
	 * Método responsável por calcular o
	 * percentual de desconto sobre o valor
	 * calculado.
	 * 
	 * @param valorTotalFormatado
	 *            the valor total formatado
	 * @param valorDescontoFormatado
	 *            the valor desconto formatado
	 * @param valorFinalFormatado
	 *            the valor final formatado
	 * @param totalValorFinalFormatado
	 *            the total valor final formatado
	 * @return the string[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	String[] aplicarValorDescontoSobreValorTotal(String valorTotalFormatado, String valorDescontoFormatado, String valorFinalFormatado,
					String totalValorFinalFormatado) throws NegocioException;

	/**
	 * Método para consultar a
	 * ApuracaoQuantidadePenalidadePeriodicidade a
	 * partir dos parametros.
	 * 
	 * @param contrato
	 *            the contrato
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param idPenalidade
	 *            the id penalidade
	 * @param idContratoModalidade
	 *            the id contrato modalidade
	 * @param dataValidadeQNPR
	 *            the data validade qnpr
	 * @return ApuracaoQuantidadePenalidadePeriodicidade
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> consultarApuracaoQtdPenalidadePeriodicidade(Contrato contrato,
					Long idPontoConsumo, Long idPenalidade, Long idContratoModalidade, Date dataValidadeQNPR);

	/**
	 * Apuracao adapter.
	 * 
	 * @param contrato
	 *            the contrato
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param periodoIncial
	 *            the periodo incial
	 * @param periodoFinal
	 *            the periodo final
	 * @return the apuracao periodo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ApuracaoPeriodo apuracaoAdapter(long contrato, long pontoConsumo, Date periodoIncial, Date periodoFinal) throws NegocioException;

	/**
	 * Método responsável por criar uma
	 * ApuracaoQuantidadePenalidadePeriodicidade.
	 * 
	 * @return ApuracaoQuantidadePenalidadePeriodicidade
	 */
	ApuracaoQuantidadePenalidadePeriodicidade criarApuracaoQuantidadePenalidadePeriodicidade();

	/**
	 * Criar apuracao quantidade penalidade.
	 * 
	 * @return ApuracaoQuantidadePenalidade
	 */
	ApuracaoQuantidadePenalidade criarApuracaoQuantidadePenalidade();

	/**
	 * Cria recuperacao.
	 * 
	 * @return Recuperacao
	 */
	Recuperacao criaRecuperacao();

	/**
	 * Criar apuracao quantidade.
	 * 
	 * @return ApuracaoQuantidade
	 */
	ApuracaoQuantidade criarApuracaoQuantidade();

	/**
	 * Criar modalidade data quantidade.
	 * 
	 * @return ModalidadeDataQuantidade
	 */
	ModalidadeDataQuantidade criarModalidadeDataQuantidade();

	/**
	 * Aplicar penalidade.
	 * 
	 * @param apuracoes
	 *            the apuracoes
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return public
	 *         ApuracaoPenalidadePeriodicidade
	 * @throws GGASException
	 *             the GGAS exception
	 */

	/**
	 * Método responsável por aplicar as
	 * penalidades
	 * 
	 * @param apuracoes
	 * @param dadosAuditoria
	 * @return
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	byte[] aplicarPenalidade(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
					throws GGASException;

	/**
	 * Método responsável por validar os
	 * parametros do filtro de pesquisa de
	 * penalidades do extrato de execução do
	 * contrato.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFiltroPesquisaPenalidadesExtratoExecucaoContrato(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método para obter a lista de penalidades
	 * associada ao contrato.
	 * 
	 * @param idContrato
	 *            the id contrato
	 * @return the collection
	 */
	Collection<ContratoPontoConsumoPenalidade> obterListaPenalidasPorContrato(Long idContrato);

	/**
	 * Esse método é responsável por inserir as
	 * apurações no banco.
	 * 
	 * @param apuracaoVO
	 *            Classe auxiliar com dados da
	 *            apuração.
	 * @param dadosAuditoria
	 *            Dados da Auditoria
	 * @param listaPenalidadesNaoCobradas
	 *            Lista Penalidades Nao Cobradas
	 * @param listaPenalidadesCobradas
	 *            Lista Penalidades Cobradas
	 * @param contrato
	 *            Contrato
	 * @param listaPontoConsumo
	 *            Lista Ponto Consumo
	 * @param dataInicial
	 *            Data Inicial
	 * @param dataFinal
	 *            Data Final
	 * @param idPeriodicidade
	 *            Periodicidade Mensal ou Anual
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void inserirApuracao(ApuracaoVO apuracaoVO, DadosAuditoria dadosAuditoria,
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesNaoCobradas,
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidadesCobradas, Contrato contrato,
					Collection<PontoConsumo> listaPontoConsumo, Date dataInicial, Date dataFinal, long idPeriodicidade)
					throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * penalidades que pertencem a um intervalo,
	 * um contrato e/ou ponto consumo.
	 * 
	 * @param contrato
	 *            o contrato do qual será pego o
	 *            contrato pai
	 * @param pontoConsumo
	 *            o ponto de consumo
	 * @param dataInicialMes
	 *            a data de início do período
	 * @param dataFinalMes
	 *            a data de fim do período
	 * @param idPeriodicidade
	 *            Periodicidade
	 * @return as penalidades encontradas
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> consultarPenalidadesPorPeriodo(Contrato contrato, PontoConsumo pontoConsumo,
					Date dataInicialMes, Date dataFinalMes, long idPeriodicidade);

	/**
	 * Método para pesquisar as apurações
	 * penalidades a partir da tela de pesquisa.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Object[]> listarApuracaoQuantidadeVO(Map<String, Object> filtro) throws NegocioException;

	/**
	 * método para retornar uma lista de
	 * ApuracaoQuantidadeVO para exibição na tela.
	 * 
	 * @param listaObjetos
	 *            the lista objetos
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ApuracaoQuantidadeVO> montarListaApuracaoQuantidadeVO(List<Object[]> listaObjetos) throws NegocioException;

	/**
	 * Processa as penalidades associadas ao
	 * contrato informado.
	 * 
	 * @param contrato
	 *            Contrato alvo.
	 * @param pontosAProcessar
	 *            the pontos a processar
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param logProcessamento
	 *            the log processamento
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void processarPenalidadesContrato(Contrato contrato, Collection<PontoConsumo> pontosAProcessar, DadosAuditoria dadosAuditoria,
					StringBuilder logProcessamento) throws GGASException;

	/**
	 * Método para atualizar a apuração após a
	 * inserção da fatura.
	 * 
	 * @param chaveApuracao
	 *            the chave apuracao
	 * @param chaveFatura
	 *            the chave fatura
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarApuracaoQtdPenalidadePeriod(Long chaveApuracao, Long chaveFatura) throws NegocioException, ConcorrenciaException;

	/**
	 * Obter a QDP para cada ponto de consumo.
	 * Caso não exista, usar a QDC.
	 * 
	 * @param listaContratoPontoConsumoModalidade
	 *            lista de modalidades por ponto
	 *            de consumo
	 * @param data
	 *            Data
	 * @return Soma dos valores para o ponto de
	 *         consumo no dia.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal obterQDP(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Date data) throws NegocioException;

	/**
	 * Processar penalidades.
	 * 
	 * @param apuracoes
	 *            the apuracoes
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void processarPenalidades(Collection<ApuracaoQuantidadePenalidadePeriodicidade> apuracoes, DadosAuditoria dadosAuditoria)
					throws NegocioException;

	/**
	 * Método responsável por obter uma lista de
	 * QuantidadeNaoRetiradaVO.
	 * 
	 * @param apuracao
	 *            the apuracao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<QuantidadeRetiradaPeriodoVO> obterQuantidadeRetiradaPeriodo(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
					throws NegocioException;

	/**
	 * Obter apuracao recuperacao quantidade v os.
	 * 
	 * @param idContrato
	 *            the id contrato
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param chavesPontoConsumo
	 *            the chaves ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ApuracaoRecuperacaoQuantidadeVO> obterApuracaoRecuperacaoQuantidadeVOs(Long idContrato, Date dataInicial, Date dataFinal,
					Long[] chavesPontoConsumo) throws NegocioException;

	/**
	 * Listar apuracao quantidade penalidade periodicidade por chave fatura.
	 * 
	 * @param chavesFatura
	 *            the chaves fatura
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidadePorChaveFatura(Long[] chavesFatura)
					throws NegocioException;

	/**
	 * Atualizar qtda recuperada.
	 * 
	 * @param apuracaoQuantPenalidPeri
	 *            the apuracao quant penalid peri
	 * @param qtdaRecuperada
	 *            the qtda recuperada
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             , ConcorrenciaException
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarQtdaRecuperada(ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantPenalidPeri, BigDecimal qtdaRecuperada,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover quant recuperacao.
	 * 
	 * @param recuperacao
	 *            the recuperacao
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             , ConcorrenciaException
	 */
	void removerQuantRecuperacao(Recuperacao recuperacao, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Verificar recuperacao de volumes fatura.
	 * 
	 * @param fatura
	 *            the fatura
	 * @return Collection<Recuperacao>
	 */
	Collection<Recuperacao> verificarRecuperacaoDeVolumesFatura(Fatura fatura);

	/**
	 * Obter qdc contrato ponto consumo modalidade.
	 * 
	 * @param listaContratoPontoConsumoModalidade
	 *            the lista contrato ponto consumo modalidade
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal obterQdcContratoPontoConsumoModalidade(Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade,
					Date dataInicial, Date dataFinal) throws NegocioException;

	/**
	 * Popular qpnr ponto consumo.
	 * 
	 * @param listaPontoConsumoVO
	 *            the lista ponto consumo vo
	 * @param chavePrimaria
	 *            the chave primaria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void popularQPNRPontoConsumo(Collection<PontoConsumoVO> listaPontoConsumoVO, Long chavePrimaria) throws GGASException;

	/**
	 * Transferir saldo qpnr.
	 * 
	 * @param migrarSaldo
	 *            the migrar saldo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void transferirSaldoQPNR(MigrarSaldoQPNRVO migrarSaldo) throws NegocioException, ConcorrenciaException;

	/**
	 * Obter tarifa media.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFaturaGas
	 *            the item fatura gas
	 * @param dataFimApuracao
	 *            the data fim apuracao
	 * @param qtdaPagaNaoRetirada
	 *            the qtda paga nao retirada
	 * @param considerarPrecoGas
	 *            the considerar preco gas
	 * @param considerarImposto
	 *            the considerar imposto
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal obterTarifaMedia(PontoConsumo pontoConsumo, Long itemFaturaGas, Date dataFimApuracao, BigDecimal qtdaPagaNaoRetirada,
					boolean considerarPrecoGas, boolean considerarImposto) throws NegocioException;

	/**
	 * Obter saldo recuperavel.
	 * 
	 * @param dataInicialApuracao
	 *            the data inicial apuracao
	 * @param chavePonto
	 *            the chave ponto
	 * @return the big decimal
	 */
	BigDecimal obterSaldoRecuperavel(Date dataInicialApuracao, Long chavePonto);

	/**
	 * Atualizar penalidade modalidade.
	 * 
	 * @param idModalidade
	 *            the id modalidade
	 * @param idApuracaoPenalidade
	 *            the id apuracao penalidade
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	Collection<ApuracaoQuantidadePenalidadePeriodicidade> atualizarPenalidadeModalidade(Long idModalidade, Long idApuracaoPenalidade)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Atualizar apuracao quantidade penalidade periodicidade.
	 * 
	 * @param apuracao
	 *            the apuracao
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void atualizarApuracaoQuantidadePenalidadePeriodicidade(ApuracaoQuantidadePenalidadePeriodicidade apuracao)
					throws ConcorrenciaException, NegocioException;

	/**
	 * Listar penalidades ret maior menor.
	 * 
	 * @return the collection
	 */
	Collection<Penalidade> listarPenalidadesRetMaiorMenor();

	/**
	 * Aplicar tarifa media momento cobranca.
	 * 
	 * @param listaPenalidades
	 *            the lista penalidades
	 * @param isAplicarTarifa
	 *            the is aplicar tarifa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void aplicarTarifaMediaMomentoCobranca(Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaPenalidades, boolean isAplicarTarifa)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Validar recuperacao penalidade.
	 * 
	 * @param valorQPNR
	 *            the valor qpnr
	 * @param volumeRecuperacao
	 *            the volume recuperacao
	 * @param lista
	 *            the lista
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param rubrica
	 *            the rubrica
	 * @return the big decimal
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal validarRecuperacaoPenalidade(String[] valorQPNR, String[] volumeRecuperacao,
					List<ApuracaoQuantidadePenalidadePeriodicidade> lista, Long idPontoConsumo, Rubrica rubrica)
					throws FormatoInvalidoException, NegocioException;

	/**
	 * Listar apuracao quantidade penalidade periodicidade por chaves.
	 * 
	 * @param chavesApuracaoQuantidadePenaPeri
	 *            the chaves apuracao quantidade pena peri
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<ApuracaoQuantidadePenalidadePeriodicidade> listarApuracaoQuantidadePenalidadePeriodicidadePorChaves(
					Long[] chavesApuracaoQuantidadePenaPeri) throws NegocioException;

	/**
	 * Criar mapa grupo economico ponto consumo.
	 * 
	 * @param listaContratos
	 *            the lista contratos
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void criarMapaGrupoEconomicoPontoConsumo(Collection<Contrato> listaContratos) throws GGASException;

	/**
	 * Obter qnr grupo economico por ponto consumo.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the big decimal
	 */
	BigDecimal obterQNRGrupoEconomicoPorPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Obter consumo diario.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param data
	 *            the data
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal obterConsumoDiario(PontoConsumo pontoConsumo, Date data) throws NegocioException;

	/**
	 * Obter penalidade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the penalidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Penalidade obterPenalidade(Long chavePrimaria) throws NegocioException;
	

	/**
	 * Método responsável por calcular a média do valor de QDP.
	 * 
	 * @param mapaModalidadesPorPontoConsumo - {@link Map}
	 * @param mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 * @param dataPeriodicidade - {@link Date}
	 * @return média do valor QDP - {@link BigDecimal}
	 * @throws NegocioException - {@link NegocioException}
	 */
	BigDecimal calcularQDPMedia(Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo,
			MultiKeyMap mapaValorQDPPorPontoConsumo, Date dataPeriodicidade) throws NegocioException;

	/**
	 * Soma o valor diário de QPD das solicitações de consumo ou o valor diário médio de QDC, caso o QDP seja nulo, de uma coleção de pontos
	 * de consumo.
	 * 
	 * @param mapaModalidadesPorPontoConsumo - {@link Map}
	 * @param mapaValorQDPPorPontoConsumo - {@link MultiKeyMap}
	 * @param data - {@link Date}
	 * @return valorQDP - {@link BigDecimal}
	 * @throws NegocioException - {@link NegocioException}
	 */
	BigDecimal obterValorQDP(Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo,
			MultiKeyMap mapaValorQDPPorPontoConsumo, Date data) throws NegocioException;

	/**
	 * Obtém a quantidade de penalidades associadas a um determinado contrato.
	 * @param idContrato - {@link Long}
	 * @return quantidade de penalidades - {@link Long}
	 */
	Long contarPenalidadesPorContrato(Long idContrato);

}
