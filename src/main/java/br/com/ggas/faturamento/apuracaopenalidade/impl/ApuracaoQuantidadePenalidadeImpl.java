/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.apuracaopenalidade.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidade;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidade;
import br.com.ggas.faturamento.apuracaopenalidade.Penalidade;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class ApuracaoQuantidadePenalidadeImpl extends EntidadeNegocioImpl implements ApuracaoQuantidadePenalidade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal qtdaFaltanteCalculada;

	private BigDecimal qtdaDiariaRetiradaMaior;

	private BigDecimal valorCalculado;

	private BigDecimal valorTarifaMedia;

	private BigDecimal percentualDescontoAplicado;

	private BigDecimal valorDescontoAplicado;

	private BigDecimal valorAcrescido;

	private BigDecimal valorCobrado;

	private String observacoes;

	private ApuracaoQuantidade apuracaoQuantidade;

	private Penalidade penalidade;

	private BigDecimal qtdaDiariaRetirada;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getQtdaFaltanteCalculada()
	 */
	@Override
	public BigDecimal getQtdaFaltanteCalculada() {

		return qtdaFaltanteCalculada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setQtdaFaltanteCalculada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaFaltanteCalculada(BigDecimal qtdaFaltanteCalculada) {

		this.qtdaFaltanteCalculada = qtdaFaltanteCalculada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getQtdaDiariaRetiradaMaior()
	 */
	@Override
	public BigDecimal getQtdaDiariaRetiradaMaior() {

		return qtdaDiariaRetiradaMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setQtdaDiariaRetiradaMaior
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaRetiradaMaior(BigDecimal qtdaDiariaRetiradaMaior) {

		this.qtdaDiariaRetiradaMaior = qtdaDiariaRetiradaMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getValorCalculado()
	 */
	@Override
	public BigDecimal getValorCalculado() {

		return valorCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setValorCalculado(java.math.BigDecimal)
	 */
	@Override
	public void setValorCalculado(BigDecimal valorCalculado) {

		this.valorCalculado = valorCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getValorTarifaMedia()
	 */
	@Override
	public BigDecimal getValorTarifaMedia() {

		return valorTarifaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setValorTarifaMedia(java.math.BigDecimal)
	 */
	@Override
	public void setValorTarifaMedia(BigDecimal valorTarifaMedia) {

		this.valorTarifaMedia = valorTarifaMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getPercentualDescontoAplicado()
	 */
	@Override
	public BigDecimal getPercentualDescontoAplicado() {

		return percentualDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setPercentualDescontoAplicado
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualDescontoAplicado(BigDecimal percentualDescontoAplicado) {

		this.percentualDescontoAplicado = percentualDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getValorDescontoAplicado()
	 */
	@Override
	public BigDecimal getValorDescontoAplicado() {

		return valorDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setValorDescontoAplicado
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorDescontoAplicado(BigDecimal valorDescontoAplicado) {

		this.valorDescontoAplicado = valorDescontoAplicado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getValorAcrescido()
	 */
	@Override
	public BigDecimal getValorAcrescido() {

		return valorAcrescido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setValorAcrescido(java.math.BigDecimal)
	 */
	@Override
	public void setValorAcrescido(BigDecimal valorAcrescido) {

		this.valorAcrescido = valorAcrescido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getValorCobrado()
	 */
	@Override
	public BigDecimal getValorCobrado() {

		return valorCobrado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setValorCobrado(java.math.BigDecimal)
	 */
	@Override
	public void setValorCobrado(BigDecimal valorCobrado) {

		this.valorCobrado = valorCobrado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getObservacoes()
	 */
	@Override
	public String getObservacoes() {

		return observacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setObservacoes(java.lang.String)
	 */
	@Override
	public void setObservacoes(String observacoes) {

		this.observacoes = observacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getApuracaoQuantidade()
	 */
	@Override
	public ApuracaoQuantidade getApuracaoQuantidade() {

		return apuracaoQuantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setApuracaoQuantidade
	 * (br.com.ggas.faturamento
	 * .apuracaopenalidade.ApuracaoQuantidade)
	 */
	@Override
	public void setApuracaoQuantidade(ApuracaoQuantidade apuracaoQuantidade) {

		this.apuracaoQuantidade = apuracaoQuantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getPenalidade()
	 */
	@Override
	public Penalidade getPenalidade() {

		return penalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setPenalidade(
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .Penalidade)
	 */
	@Override
	public void setPenalidade(Penalidade penalidade) {

		this.penalidade = penalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #getQtdaDiariaRetirada()
	 */
	@Override
	public BigDecimal getQtdaDiariaRetirada() {

		return qtdaDiariaRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.apuracaopenalidade
	 * .ApuracaoQuantidadePenalidade
	 * #setQtdaDiariaRetirada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQtdaDiariaRetirada(BigDecimal qtdaDiariaRetirada) {

		this.qtdaDiariaRetirada = qtdaDiariaRetirada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
