/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.creditodebito;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface CreditoDebitoARealizar.
 */
public interface CreditoDebitoARealizar extends EntidadeNegocio {

	/** The bean id credito debito a realizar. */
	String BEAN_ID_CREDITO_DEBITO_A_REALIZAR = "creditoDebitoARealizar";

	/** The credito debito a realizar. */
	String CREDITO_DEBITO_A_REALIZAR = "CREDITO_DEBITO_A_REALIZAR";

	/** The valor. */
	String VALOR = "ROTULO_CREDITO_DEBITO_VALOR";

	/** The valor unitario. */
	String VALOR_UNITARIO = "ROTULO_CREDITO_DEBITO_VALOR_UNITARIO";

	/** The motivo cancelamento. */
	String MOTIVO_CANCELAMENTO = "ROTULO_CREDITO_DEBITO_MOTIVO_CANCELAMENTO";

	/** The cliente. */
	String CLIENTE = "ROTULO_CREDITO_DEBITO_CLIENTE";

	/** The ponto consumo. */
	String PONTO_CONSUMO = "ROTULO_CREDITO_DEBITO_PONTO_CONSUMO";

	/** The rubrica. */
	String RUBRICA = "ROTULO_CREDITO_DEBITO_RUBRICA";

	/** The quantidade. */
	String QUANTIDADE = "ROTULO_CREDITO_DEBITO_QUANTIDADE";

	/** The periodicidade. */
	String PERIODICIDADE = "ROTULO_CREDITO_DEBITO_PERIODICIDADE";

	/** The inicio credito. */
	String INICIO_CREDITO = "ROTULO_CREDITO_DEBITO_INICIO_CREDITO";

	/** The inicio cobranca. */
	String INICIO_COBRANCA = "ROTULO_CREDITO_DEBITO_INICIO_COBRANCA";

	/** The credito origem. */
	String CREDITO_ORIGEM = "ROTULO_CREDITO_DEBITO_CREDITO_ORIGEM";

	/** The periodicidade juros. */
	String PERIODICIDADE_JUROS = "ROTULO_CREDITO_DEBITO_PERIODICIDADE_JUROS";

	/** The parcelas. */
	String PARCELAS = "ROTULO_CREDITO_DEBITO_PARCELAS";

	/** The valor entrada. */
	String VALOR_ENTRADA = "ROTULO_CREDITO_DEBITO_VALOR_ENTRADA";

	/** The taxa juros. */
	String TAXA_JUROS = "ROTULO_CREDITO_DEBITO_TAXA_JUROS";

	/** The melhor dia vencimento. */
	String MELHOR_DIA_VENCIMENTO = "ROTULO_MELHOR_DIA_VENCIMENTO";

	/**
	 * Gets the valor unitario.
	 *
	 * @return the valor unitario
	 */
	BigDecimal getValorUnitario();

	/**
	 * Gets the ano mes cobranca.
	 *
	 * @return the ano mes cobranca
	 */
	Integer getAnoMesCobranca();

	/**
	 * Sets the ano mes cobranca.
	 *
	 * @param anoMesCobranca the new ano mes cobranca
	 */
	void setAnoMesCobranca(Integer anoMesCobranca);

	/**
	 * Gets the credito debito negociado.
	 *
	 * @return the credito debito negociado
	 */
	CreditoDebitoNegociado getCreditoDebitoNegociado();

	/**
	 * Sets the credito debito negociado.
	 *
	 * @param creditoDebitoNegociado the new credito debito negociado
	 */
	void setCreditoDebitoNegociado(CreditoDebitoNegociado creditoDebitoNegociado);

	/**
	 * Gets the credito debito situacao.
	 *
	 * @return the credito debito situacao
	 */
	CreditoDebitoSituacao getCreditoDebitoSituacao();

	/**
	 * Sets the credito debito situacao.
	 *
	 * @param creditoDebitoSituacao the new credito debito situacao
	 */
	void setCreditoDebitoSituacao(CreditoDebitoSituacao creditoDebitoSituacao);

	/**
	 * Gets the numero prestacao.
	 *
	 * @return the numero prestacao
	 */
	Integer getNumeroPrestacao();

	/**
	 * Sets the numero prestacao.
	 *
	 * @param numeroPrestacao the new numero prestacao
	 */
	void setNumeroPrestacao(Integer numeroPrestacao);

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	void setValor(BigDecimal valor);

	/**
	 * Gets the ano mes faturamento.
	 *
	 * @return the ano mes faturamento
	 */
	Integer getAnoMesFaturamento();

	/**
	 * Sets the ano mes faturamento.
	 *
	 * @param anoMesFaturamento the new ano mes faturamento
	 */
	void setAnoMesFaturamento(Integer anoMesFaturamento);

	/**
	 * Gets the numero ciclo.
	 *
	 * @return the numero ciclo
	 */
	Integer getNumeroCiclo();

	/**
	 * Sets the numero ciclo.
	 *
	 * @param numeroCiclo the new numero ciclo
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * Gets the data vencimento.
	 *
	 * @return the data vencimento
	 */
	Date getDataVencimento();

	/**
	 * Sets the data vencimento.
	 *
	 * @param dataVencimento the new data vencimento
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * Checks if is indicador antecipacao.
	 *
	 * @return true, if is indicador antecipacao
	 */
	boolean isIndicadorAntecipacao();

	/**
	 * Sets the indicador antecipacao.
	 *
	 * @param indicadorAntecipacao the new indicador antecipacao
	 */
	void setIndicadorAntecipacao(boolean indicadorAntecipacao);

	/**
	 * Gets the data antecipacao.
	 *
	 * @return the data antecipacao
	 */
	Date getDataAntecipacao();

	/**
	 * Sets the data antecipacao.
	 *
	 * @param dataAntecipacao the new data antecipacao
	 */
	void setDataAntecipacao(Date dataAntecipacao);

	/**
	 * Gets the valor antecipacao.
	 *
	 * @return the valor antecipacao
	 */
	BigDecimal getValorAntecipacao();

	/**
	 * Sets the valor antecipacao.
	 *
	 * @param valorAntecipacao the new valor antecipacao
	 */
	void setValorAntecipacao(BigDecimal valorAntecipacao);

	/**
	 * Gets the devolucao.
	 *
	 * @return the devolucao
	 */
	Devolucao getDevolucao();

	/**
	 * Sets the devolucao.
	 *
	 * @param devolucao the new devolucao
	 */
	void setDevolucao(Devolucao devolucao);

	/**
	 * Gets the origem.
	 *
	 * @return the origem
	 */
	FaturaGeral getOrigem();

	/**
	 * Sets the origem.
	 *
	 * @param origem the new origem
	 */
	void setOrigem(FaturaGeral origem);

	/**
	 * Gets the situacao pagamento.
	 *
	 * @return the situacao pagamento
	 */
	EntidadeConteudo getSituacaoPagamento();

	/**
	 * Sets the situacao pagamento.
	 *
	 * @param situacaoPagamento the new situacao pagamento
	 */
	void setSituacaoPagamento(EntidadeConteudo situacaoPagamento);

	/**
	 * Gets the motivo cancelamento.
	 *
	 * @return the motivo cancelamento
	 */
	EntidadeConteudo getMotivoCancelamento();

	/**
	 * Sets the motivo cancelamento.
	 *
	 * @param motivoCancelamento the new motivo cancelamento
	 */
	void setMotivoCancelamento(EntidadeConteudo motivoCancelamento);

	/**
	 * Gets the quantidade.
	 *
	 * @return the quantidade
	 */
	BigDecimal getQuantidade();

	/**
	 * Sets the quantidade.
	 *
	 * @param quantidade the new quantidade
	 */
	void setQuantidade(BigDecimal quantidade);

	/**
	 * Gets the data vencimento formatada.
	 *
	 * @return the data vencimento formatada
	 */
	String getDataVencimentoFormatada();
}
