/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCreditoDebitoImpl representa uma ControladorCreditoDebitoImpl no sistema.
 *
 * @since 05/04/2010
 *
 */

package br.com.ggas.faturamento.creditodebito.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ValorParcela;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.ControladorDocumentoImpressaoLayout;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.cobranca.parcelamento.DadosParcelas;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.contabil.LancamentoContabilAnaliticoVO;
import br.com.ggas.contabil.LancamentoContabilVO;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.FaturaTributacao;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * The Class ControladorCreditoDebitoImpl.
 */
class ControladorCreditoDebitoImpl extends ControladorNegocioImpl implements ControladorCreditoDebito {

	private static final int CONSTANTE_LIMITE = 999999;

	private static final String AND = " and ";

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int TRINTA_DIAS = 30;

	private static final int ESCALA = 6;

	private static final int LIMITE_VALOR_INTEIRO_ENTRADA = 12;

	/** The Constant SITUACAO_EM_ANDAMENTO. */
	private static final String SITUACAO_EM_ANDAMENTO = "Em Andamento";

	/** The Constant SITUACAO_ENCERRADO. */
	private static final String SITUACAO_ENCERRADO = "Encerrado";

	/** The Constant SITUACAO_CANCELADO. */
	private static final String SITUACAO_CANCELADO = "Cancelado";

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ControladorCreditoDebitoImpl.class);

	/** The controlador constante sistema. */
	private ControladorConstanteSistema controladorConstanteSistema;

	/** The Constant ERRO_NEGOCIO_CONTRATO_SEM_PERIODICIDADE. */
	private static final String ERRO_NEGOCIO_CONTRATO_SEM_PERIODICIDADE = "ERRO_NEGOCIO_CONTRATO_SEM_PERIODICIDADE";

	/** The Constant ERRO_VALOR_MINIMO_ENTRADA_MENOR. */
	private static final String ERRO_VALOR_MINIMO_ENTRADA_MENOR = "ERRO_VALOR_MINIMO_ENTRADA_MENOR";

	/** The Constant ERRO_VALOR_ENTRADA_OBRIGATORIO. */
	private static final String ERRO_VALOR_ENTRADA_OBRIGATORIO = "ERRO_VALOR_ENTRADA_OBRIGATORIO";

	/** The Constant ERRO_TIPO_LANCAMENTO_OBRIGATORIO. */
	private static final String ERRO_TIPO_LANCAMENTO_OBRIGATORIO = "ERRO_TIPO_LANCAMENTO_OBRIGATORIO";

	/** The Constant ERRO_VALOR_MENOR_VALOR_REFERENCIA. */
	private static final String ERRO_VALOR_MENOR_VALOR_REFERENCIA = "ERRO_VALOR_MENOR_VALOR_REFERNCIA";

	/** The Constant ERRO_NEGOCIO_CLIENTE_PONTO_CONSUMO_INVALIDO. */
	private static final String ERRO_NEGOCIO_CLIENTE_PONTO_CONSUMO_INVALIDO = "ERRO_NEGOCIO_CLIENTE_PONTO_CONSUMO_INVALIDO";

	/** The Constant ERRO_TAMANHO_DESCRICAO_INVALIDO. */
	private static final String ERRO_TAMANHO_DESCRICAO_INVALIDO = "ERRO_TAMANHO_DESCRICAO_INVALIDO";

	/** The Constant ERRO_PARCELA_INVALIDA. */
	private static final String ERRO_PARCELA_INVALIDA = "ERRO_PARCELA_INVALIDA";

	/** The Constant ERRO_VALOR_UNITARIO_MAIOR_QUE_VALOR_MAXIMO. */
	private static final String ERRO_VALOR_UNITARIO_MAIOR_QUE_VALOR_MAXIMO = "ERRO_VALOR_UNITARIO_MAIOR_QUE_VALOR_MAXIMO";

	/** The Constant ERRO_VALOR_ENTRADA_MAIOR_QUE_VALOR_MAXIMO. */
	private static final String ERRO_VALOR_ENTRADA_MAIOR_QUE_VALOR_MAXIMO = "ERRO_VALOR_ENTRADA_MAIOR_QUE_VALOR_MAXIMO";

	/** The Constant ERRO_DATA_INICIO_COBRANCA_MENOR. */
	private static final String ERRO_DATA_INICIO_COBRANCA_MENOR = "ERRO_DATA_INICIO_COBRANCA_MENOR";

	/** The Constant ERRO_DATA_INICIO_CREDITO_MENOR. */
	private static final String ERRO_DATA_INICIO_CREDITO_MENOR = "ERRO_DATA_INICIO_CREDITO_MENOR";

	/** The Constant ERRO_VALOR_ENTRADA_MAIOR_VALOR. */
	private static final String ERRO_VALOR_ENTRADA_MAIOR_VALOR = "ERRO_VALOR_ENTRADA_MAIOR_VALOR";

	/** The Constant ERRO_VALOR_ENTRADA_MAIOR_QUE_O_PERMITIDO. */
	private static final String ERRO_VALOR_ENTRADA_MAIOR_QUE_O_PERMITIDO = "ERRO_VALOR_ENTRADA_MAIOR_QUE_O_PERMITIDO";

	/** The Constant ERRO_VALOR_MAIOR_QUE_O_PERMITIDO. */
	private static final String ERRO_VALOR_MAIOR_QUE_O_PERMITIDO = "ERRO_VALOR_MAIOR_QUE_O_PERMITIDO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_PRODUTO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_PRODUTO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_PRODUTO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_SERVICO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_SERVICO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_SERVICO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_PRODUTO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_PRODUTO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_PRODUTO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_SERVICO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_SERVICO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_DEBITO_A_REALIZAR_SERVICO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_PRODUTO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_PRODUTO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_PRODUTO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_SERVICO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_SERVICO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_SERVICO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_PRODUTO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_PRODUTO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_PRODUTO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_SERVICO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_SERVICO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_DEBITO_A_REALIZAR_SERVICO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_DEBITO_PRODUTO_LONGO_PARA_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_DEBITO_PRODUTO_LONGO_PARA_CURTO_PRAZO =
					"EVENTO_COMERCIAL_TRANSFERENCIA_DEBITO_PRODUTO_LONGO_PARA_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_DEBITO_SERVICO_LONGO_PARA_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_DEBITO_SERVICO_LONGO_PARA_CURTO_PRAZO =
					"EVENTO_COMERCIAL_TRANSFERENCIA_DEBITO_SERVICO_LONGO_PARA_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_PRODUTO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_PRODUTO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_PRODUTO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_SERVICO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_SERVICO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_SERVICO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_PRODUTO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_PRODUTO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_PRODUTO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_SERVICO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_SERVICO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_INCLUIR_CREDITO_A_REALIZAR_SERVICO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_PRODUTO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_PRODUTO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_PRODUTO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_SERVICO_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_SERVICO_CURTO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_SERVICO_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_PRODUTO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_PRODUTO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_PRODUTO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_SERVICO_LONGO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_SERVICO_LONGO_PRAZO =
					"EVENTO_COMERCIAL_CANCELAR_CREDITO_A_REALIZAR_SERVICO_LONGO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_CREDITO_PRODUTO_LONGO_PARA_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_CREDITO_PRODUTO_LONGO_PARA_CURTO_PRAZO =
					"EVENTO_COMERCIAL_TRANSFERENCIA_CREDITO_PRODUTO_LONGO_PARA_CURTO_PRAZO";

	/** The Constant CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_CREDITO_SERVICO_LONGO_PARA_CURTO_PRAZO. */
	public static final String CHAVE_EVENTO_COMERCIAL_TRANSFERENCIA_CREDITO_SERVICO_LONGO_PARA_CURTO_PRAZO =
					"EVENTO_COMERCIAL_TRANSFERENCIA_CREDITO_SERVICO_LONGO_PARA_CURTO_PRAZO";

	/** The Constant REFLECTION_GET. */
	private static final String REFLECTION_GET = "get";

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#criarCreditoDebitoDetalhamento()
	 */
	@Override
	public EntidadeNegocio criarCreditoDebitoDetalhamento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						CreditoDebitoDetalhamento.BEAN_ID_CREDITO_DEBITO_DETALHAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#criarCreditoDebitoNegociado()
	 */
	@Override
	public EntidadeNegocio criarCreditoDebitoNegociado() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						CreditoDebitoNegociado.BEAN_ID_CREDITO_DEBITO_NEGOCIADO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#criarCreditoDebitoARealizar()
	 */
	@Override
	public EntidadeNegocio criarCreditoDebitoARealizar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#criarCreditoOrigem()
	 */
	@Override
	public EntidadeNegocio criarCreditoOrigem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CreditoOrigem.BEAN_ID_CREDITO_ORIGEM);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	/**
	 * Gets the classe entidade credito debito detalhamento.
	 *
	 * @return the classe entidade credito debito detalhamento
	 */
	public Class<?> getClasseEntidadeCreditoDebitoDetalhamento() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoDetalhamento.BEAN_ID_CREDITO_DEBITO_DETALHAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#getClasseEntidadeCreditoDebitoNegociado()
	 */
	@Override
	public Class<?> getClasseEntidadeCreditoDebitoNegociado() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoNegociado.BEAN_ID_CREDITO_DEBITO_NEGOCIADO);
	}

	/**
	 * Gets the classe entidade credito origem.
	 *
	 * @return the classe entidade credito origem
	 */
	public Class<?> getClasseEntidadeCreditoOrigem() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoOrigem.BEAN_ID_CREDITO_ORIGEM);
	}

	/**
	 * Gets the classe entidade credito debito situacao.
	 *
	 * @return the classe entidade credito debito situacao
	 */
	public Class<?> getClasseEntidadeCreditoDebitoSituacao() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoSituacao.BEAN_ID_CREDITO_DEBITO_SITUACAO);
	}

	/**
	 * Gets the classe entidade fatura item.
	 *
	 * @return the classe entidade fatura item
	 */
	public Class<?> getClasseEntidadeFaturaItem() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaItem.BEAN_ID_FATURA_ITEM);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditosDebitos(java.lang.Long,
	 * java.lang.Long, java.util.Map,
	 * java.lang.Long[])
	 */
	@Override
	@Cacheable("creditosDebitos")
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitos(Map<String, Object> filtro) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebito ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado creditoDebitoNegociado ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.rubrica rubrica ");
		hql.append(" left outer join fetch creditoDebito.creditoDebitoNegociado.pontoConsumo pontoConsumo ");
		hql.append(" left outer join fetch creditoDebito.creditoDebitoNegociado.pontoConsumo.ramoAtividade ramoAtividade ");
		hql.append(" left outer join fetch creditoDebito.creditoDebitoNegociado.pontoConsumo.instalacaoMedidor instalacaoMedidor ");
		hql.append(" left outer join fetch instalacaoMedidor.medidor medidor ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoSituacao creditoDebitoSituacao ");

		hql.append(" where 1=1 ");

		Long[] chavesCreditoDebito = (Long[]) filtro.get("chavesCreditoDebito");
		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			hql.append(" and creditoDebito.chavePrimaria in (:chavesCreditoDebito) ");
		}

		Long[] chavesCreditoDebitoNegociado = (Long[]) filtro.get("chavesCreditoDebitoNegociado");
		if ((chavesCreditoDebitoNegociado != null) && (chavesCreditoDebitoNegociado.length > 0)) {
			hql.append(" and creditoDebitoNegociado.chavePrimaria in (:chavesCreditoDebitoNegociado) ");
		}

		Long idCliente = (Long) filtro.get("idCliente");
		if ((idCliente != null) && (idCliente > 0)) {
			hql.append(" and creditoDebitoNegociado.cliente.chavePrimaria = :idCliente ");
		}

		Long[] chavesPontoConsumo = (Long[]) filtro.get("chavesPontoConsumo");
		Map<String, List<Long>> mapaPropriedades = new HashMap<>();
		if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
			hql.append(AND);
			mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql, "creditoDebitoNegociado.pontoConsumo.chavePrimaria", "PT_CONS",
					Arrays.asList(chavesPontoConsumo));
		}


		Date dataInicioCobranca = (Date) filtro.get("dataInicioCobranca");
		if (dataInicioCobranca != null) {
			hql.append(" and creditoDebitoNegociado.dataInicioCobranca <= :dataInicioCobranca ");
		}

		Integer referencia = (Integer) filtro.get("referencia");
		Integer ciclo = (Integer) filtro.get("ciclo");
		if (referencia != null) {

			Boolean isReferenciaMenor = (Boolean) filtro.get("isReferenciaMenor");
			if (isReferenciaMenor != null && isReferenciaMenor) {
				if (ciclo != null) {
					hql.append(" and (creditoDebito.anoMesFaturamento < :referencia ");
					hql.append(" or (creditoDebito.anoMesFaturamento = :referencia and creditoDebito.numeroCiclo < :ciclo)) ");
				} else {
					hql.append(" and creditoDebito.anoMesFaturamento < :referencia ");
				}
			} else {
				if (ciclo != null) {
					hql.append(" and (creditoDebito.anoMesFaturamento < :referencia ");
					hql.append(" or (creditoDebito.anoMesFaturamento = :referencia and creditoDebito.numeroCiclo <= :ciclo)) ");
				} else {
					hql.append(" and creditoDebito.anoMesFaturamento <= :referencia ");
				}
			}
		}

		Long[] arraySituacao = (Long[]) filtro.get("arraySituacao");
		if (arraySituacao != null && (arraySituacao.length > 0)) {
			hql.append(" and creditoDebitoSituacao.chavePrimaria in (:arraySituacao) ");
		}

		Long[] arrayPagamento = (Long[]) filtro.get("arrayPagamento");
		if (arrayPagamento != null && (arrayPagamento.length > 0)) {
			hql.append(" and creditoDebito.situacaoPagamento.chavePrimaria in (:arrayPagamento) ");
		}

		// rubrica por id
		Long idRubrica = (Long) filtro.get("idRubrica");
		if (idRubrica != null && (idRubrica > 0)) {
			hql.append(" and rubrica.chavePrimaria = :idRubrica ");
		}

		Boolean tipoCredito = (Boolean) filtro.get("tipoCredito");
		Boolean tipoDebito = (Boolean) filtro.get("tipoDebito");
		if (tipoDebito != null || tipoCredito != null) {
			hql.append(" and rubrica.lancamentoItemContabil.tipoCreditoDebito.chavePrimaria = :tipoCreditoDebito ");
		}

		Boolean indicadorComposicaoNotaFiscal = (Boolean) filtro.get("indicadorComposicaoNotaFiscal");
		if (indicadorComposicaoNotaFiscal != null) {
			hql.append(" and rubrica.indicadorComposicaoNotaFiscal = :indicadorComposicaoNotaFiscal ");
		}

		Long idStatusAutorizacao = (Long) filtro.get("status");
		if (idStatusAutorizacao != null && idStatusAutorizacao > 0) {
			hql.append(" and creditoDebitoNegociado.status.chavePrimaria = :status ");
		}

		hql.append(" and creditoDebito.anoMesCobranca is null ");
		hql.append(" and creditoDebito.devolucao is null ");
		hql.append(" and creditoDebitoSituacao.valido = true ");
		hql.append(" order by creditoDebito.anoMesFaturamento asc, creditoDebito.numeroCiclo asc, creditoDebito.numeroPrestacao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			query.setParameterList("chavesCreditoDebito", chavesCreditoDebito);
		}

		if ((chavesCreditoDebitoNegociado != null) && (chavesCreditoDebitoNegociado.length > 0)) {
			query.setParameterList("chavesCreditoDebitoNegociado", chavesCreditoDebitoNegociado);
		}

		if ((idCliente != null) && (idCliente > 0)) {
			query.setLong("idCliente", idCliente);
		}

		if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		}

		if (dataInicioCobranca != null) {
			query.setDate("dataInicioCobranca", dataInicioCobranca);
		}

		if ((ciclo != null && ciclo > 0) && referencia != null) {
			query.setInteger("ciclo", ciclo);
		}

		if (referencia != null) {
			query.setInteger("referencia", referencia);
		}

		if ((arraySituacao != null) && (arraySituacao.length > 0)) {
			query.setParameterList("arraySituacao", arraySituacao);
		}

		if ((arrayPagamento != null) && (arrayPagamento.length > 0)) {
			query.setParameterList("arrayPagamento", arrayPagamento);
		}

		// rubrica por id
		if ((idRubrica != null) && (idRubrica > 0)) {
			query.setLong("idRubrica", idRubrica);
		}

		if (tipoDebito != null || tipoCredito != null) {
			if (tipoDebito != null && tipoDebito) {
				String chaveTipoDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);

				query.setParameter("tipoCreditoDebito", Long.valueOf(chaveTipoDebito));
			} else if (tipoCredito != null && tipoCredito) {
				String chaveTipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);

				query.setParameter("tipoCreditoDebito", Long.valueOf(chaveTipoCredito));
			}
		}

		if (indicadorComposicaoNotaFiscal != null) {
			query.setBoolean("indicadorComposicaoNotaFiscal", indicadorComposicaoNotaFiscal);
		}

		if (idStatusAutorizacao != null && idStatusAutorizacao > 0) {
			query.setLong("status", idStatusAutorizacao);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditosDebitos(java.lang.Long,
	 * java.lang.Long, java.util.Map,
	 * java.lang.Long[])
	 */
	@Override
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosParaFaturaEncerramento(Map<String, Object> filtro)
					throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebito ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado creditoDebitoNegociado ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.rubrica rubrica ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoSituacao creditoDebitoSituacao ");

		hql.append(" where 1=1 ");

		Long[] chavesCreditoDebito = (Long[]) filtro.get("chavesCreditoDebito");
		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			hql.append(" and creditoDebito.chavePrimaria in (:chavesCreditoDebito) ");
		}

		Long[] chavesPontoConsumo = (Long[]) filtro.get("chavesPontoConsumo");
		Map<String, List<Long>> mapaPropriedades = new HashMap<>();
		if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
			hql.append(AND);
			mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql, "creditoDebitoNegociado.pontoConsumo.chavePrimaria", "PT_CONS",
					Arrays.asList(chavesPontoConsumo));
		}

		Long idCliente = (Long) filtro.get("idCliente");
		if ((idCliente != null) && (idCliente > 0)) {
			hql.append(" and creditoDebitoNegociado.cliente.chavePrimaria = :idCliente ");
		}

		Long[] arraySituacao = (Long[]) filtro.get("arraySituacao");
		if ((arraySituacao != null) && (arraySituacao.length > 0)) {
			hql.append(" and creditoDebitoSituacao.chavePrimaria in (:arraySituacao) ");
		}

		Long[] arrayPagamento = (Long[]) filtro.get("arrayPagamento");
		if ((arrayPagamento != null) && (arrayPagamento.length > 0)) {
			hql.append(" and creditoDebito.situacaoPagamento.chavePrimaria in (:arrayPagamento) ");
		}

		Boolean tipoCredito = (Boolean) filtro.get("tipoCredito");
		Boolean tipoDebito = (Boolean) filtro.get("tipoDebito");
		if (tipoDebito != null || tipoCredito != null) {
			hql.append(" and rubrica.lancamentoItemContabil.tipoCreditoDebito.chavePrimaria = :tipoCreditoDebito ");
		}

		Long idStatusAutorizacao = (Long) filtro.get("status");
		if (idStatusAutorizacao != null && idStatusAutorizacao > 0) {
			hql.append(" and creditoDebitoNegociado.status.chavePrimaria = :status ");
		}

		Boolean isCobrado = (Boolean) filtro.get("isCobrado");
		if (isCobrado != null) {
			if (isCobrado) {
				hql.append(" and creditoDebito.anoMesCobranca is not null ");
			} else {
				hql.append(" and creditoDebito.anoMesCobranca is null ");
			}
		}

		hql.append(" and creditoDebito.devolucao is null ");
		hql.append(" and creditoDebitoSituacao.valido = true ");
		hql.append(" order by creditoDebitoNegociado.chavePrimaria asc, creditoDebito.numeroPrestacao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			query.setParameterList("chavesCreditoDebito", chavesCreditoDebito);
		}

		if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		}

		if ((idCliente != null) && (idCliente > 0)) {
			query.setLong("idCliente", idCliente);
		}

		if ((arraySituacao != null) && (arraySituacao.length > 0)) {
			query.setParameterList("arraySituacao", arraySituacao);
		}

		if ((arrayPagamento != null) && (arrayPagamento.length > 0)) {
			query.setParameterList("arrayPagamento", arrayPagamento);
		}

		if (tipoDebito != null || tipoCredito != null) {
			if (tipoDebito != null && tipoDebito) {
				String chaveTipoDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);

				query.setParameter("tipoCreditoDebito", Long.valueOf(chaveTipoDebito));
			} else if (tipoCredito != null && tipoCredito) {
				String chaveTipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);

				query.setParameter("tipoCreditoDebito", Long.valueOf(chaveTipoCredito));
			}
		}

		if (idStatusAutorizacao != null && idStatusAutorizacao > 0) {
			query.setLong("status", idStatusAutorizacao);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditosDebitosPorChaves(java.lang
	 * .Object[])
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosPorChaves(Object[] chavesCreditoDebito) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		Collection<CreditoDebitoARealizar> listaCreditoDebito = null;

		if ((chavesCreditoDebito != null) && (chavesCreditoDebito.length > 0)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebito ");
			hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado cDNegociado ");
			hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.rubrica rubrica ");
			hql.append(" where ");
			hql.append(" creditoDebito.chavePrimaria in (:chavesCreditoDebito) ");
			hql.append(" and creditoDebito.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesCreditoDebito", chavesCreditoDebito);
			query.setLong("idStatusAutorizacao", status);

			listaCreditoDebito = query.list();
		}

		return listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditoDebitoARealizarPorParcelamento
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoARealizar> consultarCreditoDebitoARealizarPorParcelamento(Long idParcelamento) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebito ");
		hql.append(" where ");
		// TODO : não há mais parcelamento em
		// creditoDebito.
		hql.append(" creditoDebito.creditoDebitoNegociado.parcelamento.chavePrimaria = :idParcelamento ");
		hql.append(" and creditoDebito.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");
		hql.append(" order by creditoDebito.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("idParcelamento", idParcelamento);
		query.setLong("idStatusAutorizacao", status);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditosDebitos(java.lang.Object[]
	 * )
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitos(Object[] chavesPrimarias) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		Collection<CreditoDebitoARealizar> listaCreditoDebito = null;
		if (chavesPrimarias != null) {

			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebitoARealizar ");
			hql.append(" where ");
			hql.append(" creditoDebitoARealizar.chavePrimaria in(:chavesPrimarias) ");
			hql.append(" and creditoDebito.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");

			// TODO remover código comentado após
			// testar.
			/*
			 * hql.append(
			 * " order by creditoDebitoARealizar.rubrica.chavePrimaria, "
			 * );hql.append(
			 * " creditoDebitoARealizar.creditoDebitoPrincipal.chavePrimaria, "
			 * )
			 * hql.append(
			 * " creditoDebitoARealizar.numeroPrestacaoCobrada, "
			 * )
			 * hql.append(
			 * " creditoDebitoARealizar.chavePrimaria "
			 * )
			 */

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesPrimarias", chavesPrimarias);
			query.setLong("idStatusAutorizacao", status);

			listaCreditoDebito = query.list();
		}

		return listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditosDebitosParcelados(java.lang
	 * .Long, java.lang.String[])
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosParcelados(Long chavePrincipal, String[] numerosPrestacoesCobradas)
					throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		Collection<CreditoDebitoARealizar> listaCreditoDebito = null;

		if (chavePrincipal != null) {

			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebitoARealizar ");
			hql.append(" where ");
			hql.append(" ( creditoDebitoARealizar.chavePrimaria =:chavePrincipal ");
			hql.append(" or creditoDebitoARealizar.creditoDebitoNegociado.chavePrimaria =:chavePrincipal ) ");
			hql.append(" and creditoDebitoARealizar.creditoDebitoNegociado.numeroPrestacaoCobrada in (:numerosPrestacoesCobradas) ");
			hql.append(" and creditoDebitoARealizar.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameter("chavePrincipal", chavePrincipal);
			query.setParameterList("numerosPrestacoesCobradas", Util.arrayStringParaArrayInteger(numerosPrestacoesCobradas));
			query.setLong("idStatusAutorizacao", status);

			listaCreditoDebito = query.list();
		}

		return listaCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterClienteSituacao
	 * (long)
	 */
	@Override
	public CreditoDebitoNegociado obterCreditoDebitoNegociado(long chavePrimaria) throws NegocioException {

		return (CreditoDebitoNegociado) super.obter(chavePrimaria, getClasseEntidadeCreditoDebitoNegociado());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#obterCreditoDebitoNegociado(long, java.lang.String[])
	 */
	@Override
	public CreditoDebitoNegociado obterCreditoDebitoNegociado(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (CreditoDebitoNegociado) super.obter(chavePrimaria, getClasseEntidadeCreditoDebitoNegociado(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterCreditoDebitoDetalhamento(long)
	 */
	@Override
	public CreditoDebitoDetalhamento obterCreditoDebitoDetalhamento(long chavePrimaria) throws NegocioException {

		return (CreditoDebitoDetalhamento) super.obter(chavePrimaria, getClasseEntidadeCreditoDebitoDetalhamento());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #montarParcelasDetalhamento(java.lang.Long)
	 */
	@Override
	public DadosGeraisParcelas montarParcelasDetalhamento(Long idCreditoDebitoNegociado) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		DadosGeraisParcelas dadosGeraisParcelas = (DadosGeraisParcelas) controladorParcelamento.criarDadosGeraisParcelas();

		CreditoDebitoNegociado creditoDebitoNegociado = null;
		if (idCreditoDebitoNegociado != null) {
			creditoDebitoNegociado = this.obterCreditoDebitoNegociado(idCreditoDebitoNegociado);
		}

		Map<String, Object> filtro = new HashMap<>();
		if ((idCreditoDebitoNegociado != null) && (idCreditoDebitoNegociado > 0)) {
			filtro.put("idCreditoDebitoNegociado", idCreditoDebitoNegociado);
		}

		Collection<CreditoDebitoARealizar> listaCDARealizar = this.consultarCreditoDebitoARealizar(filtro);

		Long codigoLAICJurosParcelamento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_PARCELAMENTO));

		Long codigoLAICJurosFinanciamento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_FINANCIAMENTO));

		BigDecimal saldoInicial = BigDecimal.ZERO;
		if (creditoDebitoNegociado != null) {
			saldoInicial = creditoDebitoNegociado.getValor();
		}

		if (creditoDebitoNegociado != null && creditoDebitoNegociado.getValor() != null) {
			BigDecimal valorSaldo = creditoDebitoNegociado.getValor();
			valorSaldo = valorSaldo.subtract(creditoDebitoNegociado.getValorEntrada());
			dadosGeraisParcelas.setValorTotalAPagar(valorSaldo);
		}

		for (CreditoDebitoARealizar creditoDebitoARealizar : listaCDARealizar) {
			Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = this
							.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());

			DadosParcelas dadosParcelas = (DadosParcelas) controladorParcelamento.criarDadosParcelas();
			dadosParcelas.setJuros(BigDecimal.ZERO);
			dadosParcelas.setAmortizacao(BigDecimal.ZERO);

			dadosParcelas.setSaldoInicial(saldoInicial);
			dadosParcelas.setNumeroParcela(creditoDebitoARealizar.getNumeroPrestacao());
			for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCDDetalhamento) {

				if (creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() == codigoLAICJurosParcelamento
								|| creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria() == codigoLAICJurosFinanciamento) {
					dadosParcelas.setJuros(creditoDebitoDetalhamento.getValor());
				} else {
					dadosParcelas.setAmortizacao(creditoDebitoDetalhamento.getValor());
					saldoInicial = saldoInicial.subtract(creditoDebitoDetalhamento.getValor());
				}
			}
			dadosParcelas.setSaldoFinal(saldoInicial);
			dadosParcelas.setTotal(dadosParcelas.getJuros().add(dadosParcelas.getAmortizacao()));
			dadosGeraisParcelas.getListaDadosParcelas().add(dadosParcelas);

			if (creditoDebitoARealizar.getMotivoCancelamento() != null && dadosGeraisParcelas.getMotivoCancelamento() == null) {
				dadosGeraisParcelas.setMotivoCancelamento(creditoDebitoARealizar.getMotivoCancelamento().getDescricao());
			}

			if (creditoDebitoARealizar.getQuantidade() != null && dadosGeraisParcelas.getQuantidade() == null) {
				dadosGeraisParcelas.setQuantidade(creditoDebitoARealizar.getQuantidade());
			}

			// calculo da data começa aqui
			if (creditoDebitoARealizar.getAnoMesCobranca() != null) {
				FaturaItem faturaItem = controladorFatura.obterFaturaItemPorCreditoDebitoARealizar(creditoDebitoARealizar
								.getChavePrimaria());
				if (faturaItem != null) {
					dadosParcelas.setDataVencimento(faturaItem.getFatura().getDataVencimento());
				}

				Date dataRecebimento = controladorRecebimento.obterDataPagamentoPelaFatura(creditoDebitoARealizar.getChavePrimaria());

				if (dataRecebimento != null) {
					dadosParcelas.setDataPagamento(dataRecebimento);
				}

			} else {
				Date dataVencimentoDocCobranca = controladorArrecadacao
								.obterDataVencimentoDocumentoCobrancaPeloCreditoDebitoARealizar(creditoDebitoARealizar.getChavePrimaria());
				if (dataVencimentoDocCobranca != null) {
					dadosParcelas.setDataVencimento(dataVencimentoDocCobranca);
				}

				Date dataRecebimento = controladorRecebimento.obterDataPagamentoPeloDocumentoCobranca(creditoDebitoARealizar
								.getChavePrimaria());

				if (dataRecebimento != null) {
					dadosParcelas.setDataPagamento(dataRecebimento);
				}
			}

			// e termina aqui
		}

		return dadosGeraisParcelas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#consultarCreditoDebitoDetalhamentoPeloCDARealizar(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoDetalhamento> consultarCreditoDebitoDetalhamentoPeloCDARealizar(Long idCreditoDebitoARealizar)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCreditoDebitoDetalhamento().getSimpleName()).append(" creditoDebitoDetalhamento ");
		hql.append("left join fetch creditoDebitoDetalhamento.lancamentoItemContabil lancamentoItemContabil");
		hql.append(" where ");
		hql.append(" creditoDebitoDetalhamento.creditoDebitoARealizar = :idCreditoDebitoARealizar ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCreditoDebitoARealizar", idCreditoDebitoARealizar);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.contrato.proposta.
	 * ControladorProposta
	 * #consultarPropostas(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoDetalhamento> consultarCreditoDebitoDetalhamento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = this.createCriteria(getClasseEntidadeCreditoDebitoDetalhamento());

		if (filtro != null) {

			Long chavePrimaria = (Long) filtro.get("chavePrimaria");
			if ((chavePrimaria != null) && (chavePrimaria > 0)) {
				criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
			}

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			Long idCreditoDebitoARealizar = (Long) filtro.get("idCreditoDebitoARealizar");
			if ((idCreditoDebitoARealizar != null) && (idCreditoDebitoARealizar > 0)) {
				criteria.createCriteria("creditoDebitoARealizar").add(
								Restrictions.eq("creditoDebitoARealizar.chavePrimaria", idCreditoDebitoARealizar));
			}

			Long[] chavesPonto = (Long[]) filtro.get("chavesPontoConsumo");
			if ((chavesPonto != null) && (chavesPonto.length > 0)) {
				criteria.createCriteria("creditoDebitoARealizar").createCriteria("creditoDebitoNegociado")
								.add(Restrictions.in("pontoConsumo.chavePrimaria", chavesPonto));
			}

			Long idCliente = (Long) filtro.get("idCliente");
			if ((idCliente != null) && (idCliente > 0)) {
				criteria.createCriteria("creditoDebitoARealizar").createCriteria("creditoDebitoNegociado")
								.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

			criteria.setFetchMode("lancamentoItemContabil", FetchMode.JOIN);
			criteria.setFetchMode("lancamentoItemContabil.tipoCreditoDebito", FetchMode.JOIN);
			criteria.setFetchMode("creditoDebitoARealizar.creditoDebitoNegociado.pontoConsumo", FetchMode.JOIN);
		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditoDebitoNegociado
	 * (java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CreditoDebitoNegociado> consultarCreditoDebitoNegociado(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = this.createCriteria(getClasseEntidadeCreditoDebitoNegociado());

		if (filtro != null) {
			Long chavePrimaria = (Long) filtro.get("chavePrimaria");
			if ((chavePrimaria != null) && (chavePrimaria > 0)) {
				criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
			}

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			Long[] chavesPonto = (Long[]) filtro.get("chavesPontoConsumo");
			if ((chavesPonto != null) && (chavesPonto.length > 0)) {
				criteria.createAlias("pontoConsumo", "pontoConsumo");
				criteria.add(Restrictions.in("pontoConsumo.chavePrimaria", chavesPonto));
			} else {
				criteria.setFetchMode("pontoConsumo", FetchMode.JOIN);
			}

			Long idCliente = (Long) filtro.get("idCliente");
			if ((idCliente != null) && (idCliente > 0)) {
				criteria.createAlias("cliente", "cliente");
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

			Long idParcelamento = (Long) filtro.get("idParcelamento");
			if (idParcelamento != null && idParcelamento > 0) {
				criteria.add(Restrictions.eq("parcelamento.chavePrimaria", idParcelamento));
			}

			Long idStatusAutorizacao = (Long) filtro.get("status");
			if (idStatusAutorizacao != null && idStatusAutorizacao > 0) {
				criteria.add(Restrictions.eq("status.chavePrimaria", idStatusAutorizacao));
			}

			criteria.setFetchMode("financiamentoTipo", FetchMode.JOIN);
			criteria.setFetchMode("rubrica", FetchMode.JOIN);
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #consultarCreditoDebitoARealizar
	 * (java.util.Map)
	 */
	@Override
	public Collection<CreditoDebitoARealizar> consultarCreditoDebitoARealizar(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.createAlias("creditoDebitoNegociado", "creditoDebitoNegociado");

		if (filtro != null) {
			Long chavePrimaria = (Long) filtro.get("chavePrimaria");
			if ((chavePrimaria != null) && (chavePrimaria > 0)) {
				criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
			}

			Long idCreditoDebitoNegociado = (Long) filtro.get("idCreditoDebitoNegociado");
			if ((idCreditoDebitoNegociado != null) && (idCreditoDebitoNegociado > 0)) {
				criteria.add(Restrictions.eq("creditoDebitoNegociado.chavePrimaria", idCreditoDebitoNegociado));
			}

			Long[] chavesCreditoDebitoNegociado = (Long[]) filtro.get("chavesCreditoDebitosNegociado");
			if ((chavesCreditoDebitoNegociado != null) && (chavesCreditoDebitoNegociado.length > 0)) {
				criteria.add(Restrictions.in("creditoDebitoNegociado.chavePrimaria", chavesCreditoDebitoNegociado));
			}

			Long idParcelamento = (Long) filtro.get("idParcelamento");
			if (idParcelamento != null) {
				criteria.add(Restrictions.eq("creditoDebitoNegociado.parcelamento.chavePrimaria", idParcelamento));
			}

			//
			Long[] chavesPonto = (Long[]) filtro.get("chavesPontoConsumo");
			if ((chavesPonto != null) && (chavesPonto.length > 0)) {
				criteria.add(Restrictions.in("creditoDebitoNegociado.pontoConsumo.chavePrimaria", chavesPonto));
			}

			Long idCliente = (Long) filtro.get("idCliente");
			if ((idCliente != null) && (idCliente > 0)) {
				criteria.add(Restrictions.eq("creditoDebitoNegociado.cliente.chavePrimaria", idCliente));
			}

			Long idStatusAutorizacao = (Long) filtro.get("status");
			if (idStatusAutorizacao != null && idStatusAutorizacao > 0) {
				criteria.add(Restrictions.eq("creditoDebitoNegociado.status.chavePrimaria", idStatusAutorizacao));
			}

			String numeroDocumento = (String) filtro.get("numeroDocumento");
			if (numeroDocumento != null && !numeroDocumento.isEmpty()) {
				criteria.createAlias("origem", "origem");
				criteria.add(Restrictions.eq("origem.faturaAtual.chavePrimaria", Long.parseLong(numeroDocumento)));
			}
			//

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

			Boolean anoMesCobrancaIsNull = (Boolean) filtro.get("anoMesCobrancaIsNull");
			if (anoMesCobrancaIsNull != null) {
				criteria.add(Restrictions.isNull("anoMesCobranca"));
			}

			//
			criteria.setFetchMode("creditoDebitoNegociado.rubrica", FetchMode.JOIN);
			//

			criteria.setFetchMode("situacaoPagamento", FetchMode.JOIN);
			criteria.setFetchMode("creditoDebitoSituacao", FetchMode.JOIN);
			criteria.addOrder(Order.asc("numeroPrestacao"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterQtdCreditoDebitoCobrados(java.lang.Long
	 * )
	 */
	@Override
	public Integer obterQtdCreditoDebitoCobrados(Long idCreditoDebitoNegociado) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		Long resultado = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(creditoDebitoARealizar.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebitoARealizar ");
		hql.append(" where ");
		hql.append(" creditoDebitoARealizar.creditoDebitoNegociado.chavePrimaria = :idCreditoDebitoNegociado ");
		hql.append(" and creditoDebitoARealizar.anoMesCobranca is not null ");
		hql.append(" and creditoDebitoARealizar.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCreditoDebitoNegociado", idCreditoDebitoNegociado);
		query.setLong("idStatusAutorizacao", status);

		resultado = (Long) query.uniqueResult();

		return resultado.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterCDDetalhamentoPeloCDARealizar(java.lang
	 * .Long)
	 */
	@Override
	public Collection<CreditoDebitoDetalhamento> obterCDDetalhamentoPeloCDARealizar(Long idCreditoDebitoARealizar) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCreditoDebitoDetalhamento().getSimpleName()).append(" cDDetalhamento ");
		hql.append(" inner join fetch cDDetalhamento.lancamentoItemContabil ");
		hql.append(" inner join fetch cDDetalhamento.lancamentoItemContabil.tipoCreditoDebito ");
		hql.append(" where ");
		hql.append(" cDDetalhamento.creditoDebitoARealizar.chavePrimaria = :idCreditoDebitoARealizar ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCreditoDebitoARealizar", idCreditoDebitoARealizar);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #gerarParcelasCreditoDebito(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public DadosGeraisParcelas gerarParcelasCreditoDebito(Map<String, Object> dados, boolean indicadorDetalhamento)
					throws NegocioException, FormatoInvalidoException {

		if (!indicadorDetalhamento) {
			this.validarCamposObrigatoriosGeracaoParcelas(dados);
		}

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();

		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);

		controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);

		DadosGeraisParcelas dadosGerais = (DadosGeraisParcelas) controladorParcelamento.criarDadosGeraisParcelas();
		Collection<ValorParcela> listaParcelas = new ArrayList<>();

		String indicadorCreditoDebito = (String) dados.get("tipoDocumento");
		boolean tipoCreditoDebito = Boolean.valueOf(indicadorCreditoDebito);

		Long idCreditoOrigem = (Long) dados.get("idCreditoOrigem");

		if (indicadorDetalhamento && idCreditoOrigem != null) {
			tipoCreditoDebito = false;
			// caso
			// credito
		} else if (indicadorDetalhamento) {
			tipoCreditoDebito = true;
			// caso
			// debito
		}

		Long idCliente = (Long) dados.get("idCliente");

		BigDecimal valor = (BigDecimal) dados.get("valorHidden");

		BigDecimal valorUnitario = (BigDecimal) dados.get("valorUnitarioHidden");

		if (valorUnitario != null) {
			dadosGerais.setValorUnitario(valorUnitario);
		}

		Integer parcelas = (Integer) dados.get("parcelas");
		Long idRubrica = (Long) dados.get("idRubrica");
		Rubrica rubrica = null;
		Integer numeroMaximoParcelas = null;
		if (idRubrica != null) {
			dadosGerais.setIdRubrica(idRubrica);
			rubrica = (Rubrica) controladorRubrica.obter(idRubrica, "amortizacao", "valorRegulamentados");
			numeroMaximoParcelas = rubrica.getNumeroMaximoParcela();
		}

		if (parcelas != null && parcelas > 0) {
			dadosGerais.setNumeroPrestacoes(parcelas);

			if ((numeroMaximoParcelas != null) && (parcelas.compareTo(numeroMaximoParcelas) > 0)) {
				throw new NegocioException(ERRO_NEGOCIO_NUMERO_PARCELAS_MAIOR_RUBRICA, true);
			}
		} else {
			throw new NegocioException(ERRO_PARCELA_INVALIDA, true);
		}

		BigDecimal taxaJuros = (BigDecimal) dados.get("taxaJuros");
		Long idPeriodicidadeJuros = (Long) dados.get("idPeriodicidadeJuros");

		Long idPeriodicidadeCobranca = (Long) dados.get("idPeriodicidade");

		String descricao = (String) dados.get("complementoDescricao");

		if (!StringUtils.isEmpty(descricao)) {
			dadosGerais.setDescricao(descricao);
		}

		ParametroSistema parametroTamanhoDescricao = controladorParametroSistema
						.obterParametroPorCodigo(ParametroSistema.TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR);
		Integer tamanhoMaximoDescricao = Integer.parseInt(parametroTamanhoDescricao.getValor());
		if (!StringUtils.isEmpty(descricao) && descricao.length() > tamanhoMaximoDescricao) {
			throw new NegocioException(ERRO_TAMANHO_DESCRICAO_INVALIDO, true);
		}

		BigDecimal valorEntrada = (BigDecimal) dados.get("valorEntrada");
		BigDecimal quantidade = (BigDecimal) dados.get("quantidade");
		String quantidadeDiasCobranca = (String) dados.get("qtdDiasCobranca");

		String melhorDiaVencimento = (String) dados.get("melhorDiaVencimento");
		if (!StringUtils.isEmpty(melhorDiaVencimento)) {
			dadosGerais.setMelhorDiaVencimento(melhorDiaVencimento);
		}
		Long idInicioCobranca = (Long) dados.get("idInicioCobranca");

		Long idPontoConsumo = (Long) dados.get("idPontoConsumo");

		if (idCliente == null && idPontoConsumo == null) {
			throw new NegocioException(ERRO_NEGOCIO_CLIENTE_PONTO_CONSUMO_INVALIDO, true);
		}

		Long idAmortizacao = null;
		if (indicadorDetalhamento) {
			idAmortizacao = (Long) dados.get("tipoAmortizacao");
		} else if (rubrica != null && rubrica.getAmortizacao() != null) {
			idAmortizacao = rubrica.getAmortizacao().getChavePrimaria();
		}

		if (quantidade != null) {
			dadosGerais.setQuantidade(quantidade);
		}

		if (idInicioCobranca != null) {
			dadosGerais.setIdInicioCobranca(idInicioCobranca);
		}

		if (!StringUtils.isEmpty(quantidadeDiasCobranca)) {
			dadosGerais.setQuantidadeDiasCobranca(quantidadeDiasCobranca);
		}

		if (idCreditoOrigem != null) {
			dadosGerais.setIdCreditoOrigem(idCreditoOrigem);
		}

		if (taxaJuros != null) {
			dadosGerais.setTaxaJuros(taxaJuros);
		}

		if (!indicadorDetalhamento && rubrica.getValorReferencia() != null
						&& valor.compareTo(rubrica.getValorReferencia()) < 0) {
			throw new NegocioException(ERRO_VALOR_MENOR_VALOR_REFERENCIA, true);
		}

		if (valorEntrada != null) {
			String valorInteiroEntrada = valorEntrada.toString();
			if (!StringUtils.isEmpty(valorInteiroEntrada)) {
				valorInteiroEntrada = valorInteiroEntrada.substring(0, valorInteiroEntrada.indexOf('.'));
				if (valorInteiroEntrada.length() > LIMITE_VALOR_INTEIRO_ENTRADA) {
					throw new NegocioException(ERRO_VALOR_ENTRADA_MAIOR_QUE_O_PERMITIDO, true);
				}
			}
		}

		if (!indicadorDetalhamento && rubrica.getValorMaximo() != null) {
			if (valorUnitario.compareTo(rubrica.getValorMaximo()) > 0) {
				throw new NegocioException(ERRO_VALOR_UNITARIO_MAIOR_QUE_VALOR_MAXIMO, true);
			}

			if (valorEntrada != null && valorEntrada.compareTo(rubrica.getValorMaximo()) > 0) {
				throw new NegocioException(ERRO_VALOR_ENTRADA_MAIOR_QUE_VALOR_MAXIMO, true);
			}
		}

		Date dataHoje = DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime());
		Date dataInicio = null;
		if (tipoCreditoDebito) {
			// true, caso
			// Débito
			dataInicio = (Date) dados.get("dataInicioCobranca");

			if (dataInicio != null) {
				if (Util.compararDatas(dataInicio, dataHoje) >= 0 || indicadorDetalhamento) {
					DateTime dataApos = new DateTime(dataInicio);
					// um
					// dia após a data
					// determinada.
					dataInicio = dataApos.toDate();
					dadosGerais.setDataInicioCobranca(dataInicio);
				} else {
					throw new NegocioException(ERRO_DATA_INICIO_COBRANCA_MENOR, true);
				}
			}

			String valorInteiro = valor.toString();
			if (!StringUtils.isEmpty(valorInteiro)) {
				valorInteiro = valorInteiro.substring(0, valorInteiro.indexOf('.'));
				if (valorInteiro.length() > LIMITE_VALOR_INTEIRO_ENTRADA) {
					throw new NegocioException(ERRO_VALOR_MAIOR_QUE_O_PERMITIDO, true);
				}
			}

			BigDecimal valorVerificacao = BigDecimal.ZERO;
			if (rubrica.getPercentualMinimoEntrada() != null) {
				valorVerificacao = valor.multiply(Util.converterBigDecimalParaPercentual(rubrica.getPercentualMinimoEntrada()));
			}

			controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			Long tipoPrice = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_AMORTIZACAO_PRICE));

			if (valorEntrada != null) {
				dadosGerais.setValorEntrada(valorEntrada);

				if (valor.compareTo(valorEntrada) <= 0) {
					throw new NegocioException(ERRO_VALOR_ENTRADA_MAIOR_VALOR, true);
				}

				// verifica se o valor de entrada
				// é maior do que a porcentagem
				// minima de entrada (valorEntrada
				// > (valor * porcentagem))
				if (valorEntrada.compareTo(valorVerificacao) >= 0) {
					valor = valor.subtract(valorEntrada);
				} else if (!indicadorDetalhamento) {
					throw new NegocioException(ERRO_VALOR_MINIMO_ENTRADA_MENOR, true);
				}
			} else {
				if (!indicadorDetalhamento && rubrica.getIndicadorEntradaObrigatoria()) {// significa
																							// que
																							// ele
																							// é
																							// nulo
																							// e
																							// obrigatório
					throw new NegocioException(ERRO_VALOR_ENTRADA_OBRIGATORIO, true);
				}
			}

			// calcula a periodicidade da cobrança do calculo de price/sac
			String codigoPeriodicidadeCobranca = "";
			EntidadeConteudo periodicidadeJuros = null;

			if (idPeriodicidadeJuros != null) {
				dadosGerais.setIdPeriodicidadeJuros(idPeriodicidadeJuros);
				periodicidadeJuros = controladorEntidadeConteudo.obter(idPeriodicidadeJuros);
			}

			if (idPeriodicidadeCobranca != null) {
				dadosGerais.setIdPeriodicidade(idPeriodicidadeCobranca);
			}
			codigoPeriodicidadeCobranca = obterPeriodicidadeCobranca(controladorParcelamento, idPeriodicidadeCobranca, idPontoConsumo);

			if (idAmortizacao != null) {
				// TODO[gsantos]: recalcular depois o valor usado no combo de iniciar cobranca após, para que ele pegue a data base válida
				Date dataInicioAmortizacao = dataInicio;
				if (dataInicioAmortizacao == null) {
					DateTime dataApos = new DateTime(Calendar.getInstance().getTime());
					dataApos = dataApos.plusDays(Integer.parseInt(quantidadeDiasCobranca));
					// um dia após a data determinada.
					dataInicioAmortizacao = dataApos.toDate();
					//TODO PROCENGE: possível erro no código, nesse caso a variável dataInicio sempre será nula, não faz sentido a linha
					//abaixo. Favor verificar.
					dadosGerais.setDataInicioCobranca(dataInicio); //NOSONAR Roberto Alencar: a Procenge deverá verificar o TODO acima
				}

				// Price
				if (idAmortizacao.equals(tipoPrice)) {
					listaParcelas = controladorCobranca.calcularPrice(valor, parcelas, taxaJuros, dataInicioAmortizacao,
									Integer.parseInt(codigoPeriodicidadeCobranca), Integer.valueOf(periodicidadeJuros.getCodigo()), true);
					// SAC
				} else {
					listaParcelas = controladorCobranca.calcularSAC(valor, parcelas, taxaJuros, dataInicioAmortizacao,
									Integer.parseInt(codigoPeriodicidadeCobranca), Integer.valueOf(periodicidadeJuros.getCodigo()), true);
				}
			} else {
				// tratar aqui caso não haja
				// calculo de amortizacao

				BigDecimal valorIndividual = valor.divide(new BigDecimal(parcelas), ESCALA, RoundingMode.HALF_UP);
				BigDecimal saldoIndividual = valor;
				BigDecimal parcelasBigDecimal = new BigDecimal(parcelas);

				for (int i = 0; i < parcelas; i++) {

					ValorParcela valorParcela = (ValorParcela) controladorCobranca.criarValorParcela();
					valorParcela.setAmortizacao(valorIndividual);

					if (i + 1 == parcelas) {

						BigDecimal valorInicial = valor.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
						BigDecimal todasAmortizacoes = parcelasBigDecimal.multiply(valorIndividual.setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
						BigDecimal diferenca = valorInicial.subtract(todasAmortizacoes);

						valorParcela.setAmortizacao(valorIndividual.add(diferenca));

					}

					valorParcela.setValorPagar(valorIndividual);
					valorParcela.setJuros(BigDecimal.ZERO);
					valorParcela.setSaldo(saldoIndividual);
					saldoIndividual = saldoIndividual.subtract(valorParcela.getAmortizacao());

					listaParcelas.add(valorParcela);
				}

			}
		} else {
			// false; caso seja crédito
			dataInicio = (Date) dados.get("dataInicioCredito");

			if (dataInicio != null) {
				if (Util.compararDatas(dataInicio, dataHoje) >= 0 || indicadorDetalhamento) {
					DateTime dataApos = new DateTime(dataInicio);
					// um
					// dia após a data
					// determinada.
					dataInicio = dataApos.toDate();
					dadosGerais.setDataInicioCredito(dataInicio);
				} else {
					throw new NegocioException(ERRO_DATA_INICIO_CREDITO_MENOR, true);
				}
			}

			BigDecimal parcelasBigDecimal = new BigDecimal(parcelas);
			BigDecimal valorIndividual = valor.divide(parcelasBigDecimal, ESCALA, RoundingMode.HALF_UP);
			BigDecimal diferencaSaldo = valor.subtract(valorIndividual.multiply(parcelasBigDecimal));
			BigDecimal saldoIndividual = valor;
			for (int i = 0; i < parcelas; i++) {

				ValorParcela valorParcela = (ValorParcela) controladorCobranca.criarValorParcela();
				valorParcela.setAmortizacao(valorIndividual);
				if (i + 1 == parcelas) {
					valorParcela.setAmortizacao(valorParcela.getAmortizacao().add(diferencaSaldo));
				}
				valorParcela.setValorPagar(valorIndividual);
				valorParcela.setJuros(BigDecimal.ZERO);
				valorParcela.setSaldo(saldoIndividual);
				saldoIndividual = saldoIndividual.subtract(valorParcela.getAmortizacao());

				listaParcelas.add(valorParcela);
			}
		}

		if ((listaParcelas != null) && (!listaParcelas.isEmpty())) {

			Collection<DadosParcelas> listaDadosParcelas = new ArrayList<>();
			int qtde = 1;
			BigDecimal valorTotalJuros = BigDecimal.ZERO;
			BigDecimal valorTotalAPagar = BigDecimal.ZERO;
			for (ValorParcela valorParcela : listaParcelas) {
				DadosParcelas dadosParcela = (DadosParcelas) controladorParcelamento.criarDadosParcelas();
				dadosParcela.setNumeroParcela(qtde);
				if (valorParcela.getAmortizacao() != null) {
					dadosParcela.setAmortizacao(valorParcela.getAmortizacao().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
				} else {
					dadosParcela.setAmortizacao(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
				}
				if (valorParcela.getJuros() != null) {
					dadosParcela.setJuros(valorParcela.getJuros().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
					valorTotalJuros = valorTotalJuros.add(dadosParcela.getJuros());
				} else {
					dadosParcela.setJuros(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
				}
				if (valorParcela.getSaldo() != null) {
					dadosParcela.setSaldoInicial(valorParcela.getSaldo().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
				} else {
					dadosParcela.setSaldoInicial(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
				}
				if (valorParcela.getValorPagar() != null) {
					dadosParcela.setTotal(valorParcela.getValorPagar().setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP));
					valorTotalAPagar = valorTotalAPagar.add(dadosParcela.getTotal());
				} else {
					dadosParcela.setTotal(BigDecimal.ZERO.setScale(DUAS_CASAS_DECIMAIS));
				}

				if (valorParcela.getSaldo() != null && valorParcela.getAmortizacao() != null) {
					BigDecimal saldoFinal = valorParcela.getSaldo().subtract(valorParcela.getAmortizacao());
					if (saldoFinal.compareTo(BigDecimal.ZERO) >= 0) {
						dadosParcela.setSaldoFinal(saldoFinal);
					} else {
						dadosParcela.setSaldoFinal(BigDecimal.ZERO);
					}
				} else {
					dadosParcela.setSaldoFinal(BigDecimal.ZERO);
				}

				listaDadosParcelas.add(dadosParcela);
				qtde++;
			}
			dadosGerais.setListaDadosParcelas(listaDadosParcelas);
		}

		if (idPontoConsumo != null) {
			dadosGerais.setIdPontoConsumo(idPontoConsumo);
		}

		if (idCliente != null) {
			dadosGerais.setIdCliente(idCliente);
		}

		dadosGerais.setValorTotalAPagar(valor);

		return dadosGerais;
	}

	/**
	 * Método responsável por obter a
	 * periodicidade de cobrança de um contrato;
	 * caso não haja, a periodicidade mensal(30) é
	 * usada como padrão.
	 *
	 * @param controladorParcelamento
	 *            the controlador parcelamento
	 * @param idPeriodicidadeCobranca
	 *            the id periodicidade cobranca
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String obterPeriodicidadeCobranca(ControladorParcelamento controladorParcelamento, Long idPeriodicidadeCobranca,
					Long idPontoConsumo) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		EntidadeConteudo periodicidadeCobranca;
		String codigoPeriodicidadeCobranca;
		if (idPeriodicidadeCobranca != null) {
			periodicidadeCobranca = controladorEntidadeConteudo.obter(idPeriodicidadeCobranca);
			codigoPeriodicidadeCobranca = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_A_CADA_FATURAMENTO);

			if (codigoPeriodicidadeCobranca.equals(String.valueOf(periodicidadeCobranca.getChavePrimaria()))) {
				// a
				// cada
				// faturamento
				// procurar contrato
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);
				ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

				if (contratoPontoConsumo == null) {
					contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
									.getChavePrimaria());
				}

				if (contratoPontoConsumo != null) {
					ContratoPontoConsumoItemFaturamento cpcItemFaturamento = controladorParcelamento
									.obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);

					if (cpcItemFaturamento != null) {

						// FIXME: URGENTE está misturando periodicidade de cobrança com tamanho da periodicidade
						// TODO: Tratar mês civil
						codigoPeriodicidadeCobranca = String.valueOf(contratoPontoConsumo.getPeriodicidade().getQuantidadeDias());
					} else {
						throw new NegocioException(ControladorParcelamento.ERRO_NEGOCIO_ITEM_FATURA, true);
					}
				}

			} else {
				// FIXME: URGENTE está misturando periodicidade de cobrança com tamanho da periodicidade
				codigoPeriodicidadeCobranca = periodicidadeCobranca.getCodigo();
			}
		} else {
			EntidadeConteudo periocididadeCobrancaMensal = (EntidadeConteudo) controladorEntidadeConteudo.obter(Long
							.parseLong((String) controladorParametroSistema
											.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_PERIODICIDADE_COBRANCA_MENSAL)));

			// FIXME: URGENTE está misturando periodicidade de cobrança com tamanho da periodicidade
			codigoPeriodicidadeCobranca = periocididadeCobrancaMensal.getCodigo();
		}
		return codigoPeriodicidadeCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterPeriodicidadeContratoPontoConsumo
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public String obterPeriodicidadeContratoPontoConsumo(Long idPeriodicidadeCobranca, Long idPontoConsumo) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);
		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		String codigoPeriodicidadeCobranca = null;
		if (idPontoConsumo != null) {

			// procurar contrato
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);
			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

			if (contratoPontoConsumo == null) {
				contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
								.getChavePrimaria());
			}

			if (contratoPontoConsumo != null) {
				ContratoPontoConsumoItemFaturamento cpcItemFaturamento = controladorParcelamento
								.obterContratoPontoConsumoItemFaturamento(contratoPontoConsumo);

				if (cpcItemFaturamento != null) {
					// caso exista periodicidade definada no contrato
					// FIXME: URGENTE está misturando periodicidade de cobrança com tamanho da periodicidade
					// TODO: Tratar mês civil
					codigoPeriodicidadeCobranca = String.valueOf(contratoPontoConsumo.getPeriodicidade().getQuantidadeDias());
				} else {
					throw new NegocioException(ERRO_NEGOCIO_CONTRATO_SEM_PERIODICIDADE, true);
				}
			}

		} else {
			// caso não exista periodicidade definida no contrato
			EntidadeConteudo periocididadeCobrancaMensal = (EntidadeConteudo) controladorEntidadeConteudo.obter(Long
							.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_MENSAL)));

			// FIXME: URGENTE está misturando periodicidade de cobrança com tamanho da periodicidade
			codigoPeriodicidadeCobranca = periocididadeCobrancaMensal.getCodigo();
		}

		return codigoPeriodicidadeCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #salvarCreditoDebitoARealizar
	 * (br.com.ggas.cobranca
	 * .parcelamento.DadosGeraisParcelas)
	 */
	@Override
	public byte[] salvarCreditoDebitoARealizar(Map<String, Object> dados, DadosGeraisParcelas dadosGerais) throws GGASException {

		CreditoDebitoNegociado creditoDebitoNegociado = (CreditoDebitoNegociado) this.criarCreditoDebitoNegociado();
		CreditoDebitoARealizar creditoDebitoARealizar = null;
		CreditoDebitoDetalhamento creditoDebitoDetalhamento = null;
		ControladorParcelamento controladorParcelamento = ServiceLocator.getInstancia().getControladorParcelamento();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String indicadorCreditoDebito = (String) dados.get("tipoDocumento");
		boolean tipoCreditoDebito = false;
		
		Collection<CreditoDebitoARealizar> listaCreditoDebitoRealizar = new HashSet<CreditoDebitoARealizar>();

		if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
			tipoCreditoDebito = Boolean.valueOf(indicadorCreditoDebito);
		} else {
			throw new NegocioException(ERRO_TIPO_LANCAMENTO_OBRIGATORIO, true);
		}

		BigDecimal valorEntrada = dadosGerais.getValorEntrada();
		BigDecimal valor = dadosGerais.getValorTotalAPagar();
		Integer parcelas = dadosGerais.getNumeroPrestacoes();
		BigDecimal taxaJuros = dadosGerais.getTaxaJuros();
		Long idPontoConsumo = dadosGerais.getIdPontoConsumo();
		Date dataInicioCobranca = dadosGerais.getDataInicioCobranca();
		Date dataInicioCredito = dadosGerais.getDataInicioCredito();
		Long idRubrica = dadosGerais.getIdRubrica();
		BigDecimal quantidade = dadosGerais.getQuantidade();
		Long idCreditoOrigem = dadosGerais.getIdCreditoOrigem();
		Long idCliente = dadosGerais.getIdCliente();
		Long idPeriodicidadeJuros = dadosGerais.getIdPeriodicidadeJuros();
		Long idPeriodicidade = dadosGerais.getIdPeriodicidade();
		String quantidadeDiasCobranca = dadosGerais.getQuantidadeDiasCobranca();
		Long idInicioCobranca = dadosGerais.getIdInicioCobranca();
		String descricao = dadosGerais.getDescricao();
		String melhorDiaVencimento = dadosGerais.getMelhorDiaVencimento();

		EntidadeConteudo periodicidade = null;
		EntidadeConteudo periodicidadeJuros = null;
		Rubrica rubrica = null;

		if (idPeriodicidadeJuros != null) {
			periodicidadeJuros = controladorEntidadeConteudo.obter(idPeriodicidadeJuros);
		}

		if (idPeriodicidade != null) {
			periodicidade = controladorEntidadeConteudo.obter(idPeriodicidade);
		}

		if (idRubrica != null) {
			rubrica = (Rubrica) controladorRubrica.obter(idRubrica, "lancamentoItemContabil");
		}

		CreditoOrigem creditoOrigem = null;
		if (idCreditoOrigem != null) {
			creditoOrigem = this.obterCreditoOrigem(idCreditoOrigem);
		}

		Cliente cliente = null;
		if (idCliente != null) {
			cliente = (Cliente) controladorCliente.obter(idCliente);
		}

		PontoConsumo pontoConsumo = null;
		if (idPontoConsumo != null) {
			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);

			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);
			if (contratoPontoConsumo == null) {
				contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo
								.getChavePrimaria());
			}

			cliente = (Cliente) controladorCliente.obter(contratoPontoConsumo.getContrato().getClienteAssinatura().getChavePrimaria());
		}

		// rubrica
		creditoDebitoNegociado.setRubrica(rubrica);

		// descricao
		creditoDebitoNegociado.setDescricao(descricao);

		// quantidade
		creditoDebitoNegociado.setNumeroPrestacaoCobrada(0);
		// valor padrão

		// cliente selecionado
		creditoDebitoNegociado.setCliente(cliente);

		// dadosAuditoria
		creditoDebitoNegociado.setDadosAuditoria(dadosGerais.getDadosAuditoria());

		// parcelas
		creditoDebitoNegociado.setQuantidadeTotalPrestacoes(parcelas);

		// timestamp financiamento
		creditoDebitoNegociado.setDataFinanciamento(Calendar.getInstance().getTime());

		//
		if (rubrica != null && rubrica.getAmortizacao() != null) {
			creditoDebitoNegociado.setAmortizacao(rubrica.getAmortizacao());
		}

		String codigoEntidadeConteudoFatura = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_FATURA);

		EntidadeConteudo entidadeConteudoFatura = (EntidadeConteudo) controladorEntidadeConteudo.obter(Long
						.parseLong(codigoEntidadeConteudoFatura));

		creditoDebitoNegociado.setFormaCobranca(entidadeConteudoFatura);

		// ponto consumo (se) selecionado
		if (pontoConsumo != null) {
			creditoDebitoNegociado.setPontoConsumo(pontoConsumo);
			creditoDebitoNegociado.setSegmento(pontoConsumo.getSegmento());
		}

		// periodicidade do credito/debito
		creditoDebitoNegociado.setPeriodicidadeCobranca(periodicidade);

		if (!StringUtils.isEmpty(melhorDiaVencimento)) {
			creditoDebitoNegociado.setMelhorDiaVencimento(Integer.parseInt(melhorDiaVencimento));
		}

		// ano mes inclusao
		creditoDebitoNegociado.setAnoMesInclusao(Util.obterAnoMesCorrente());
		// valor
		creditoDebitoNegociado.setValor(valor);
		if (rubrica != null) {
			creditoDebitoNegociado.setFinanciamentoTipo(rubrica.getFinanciamentoTipo());
		}

		EntidadeConteudo entidadeConteudoInicioCobranca = null;
		Fatura fatura = null;

		// populando o creditoDebitoNegociado e o Detalhamento
		if (tipoCreditoDebito) {
			// true; caso debito

			// juros
			creditoDebitoNegociado.setPercentualJuros(taxaJuros);
			creditoDebitoNegociado.setPeriodicidadeJuros(periodicidadeJuros);

			if ((valorEntrada != null) && (valorEntrada.compareTo(BigDecimal.ZERO) > 0)) {
				// caso o valor de entrada seja informado, ele é cobrado via 1(uma) fatura, relacionada à rubrica.
				// valor de entrada
				creditoDebitoNegociado.setValorEntrada(valorEntrada);

				// Insere a fatura com suas dependências.
				fatura = this.carregarFatura(idPontoConsumo, valorEntrada, null, cliente, rubrica);
				fatura.setHabilitado(Boolean.TRUE);
				fatura.setUltimaAlteracao(Calendar.getInstance().getTime());
				fatura.setValorConciliado(BigDecimal.ZERO);

				// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
				getHibernateTemplate().getSessionFactory().getCurrentSession().save(fatura);

				FaturaGeral faturaGeral = (FaturaGeral) controladorParcelamento.criarFaturaGeral();
				faturaGeral.setFaturaAtual(fatura);
				faturaGeral.setHabilitado(Boolean.TRUE);
				faturaGeral.setUltimaAlteracao(Calendar.getInstance().getTime());
				// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
				getHibernateTemplate().getSessionFactory().getCurrentSession().save(faturaGeral);

				fatura.setFaturaGeral(faturaGeral);
				fatura.setUltimaAlteracao(Calendar.getInstance().getTime());
				// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
				getHibernateTemplate().getSessionFactory().getCurrentSession().update(fatura);

				fatura.setDadosAuditoria(dadosGerais.getDadosAuditoria());

				// salvar tributos da fatura
				List<FaturaTributacao> listaFaturaTributacao = controladorFatura.montarFaturaTributacaoNotaDebito(fatura);
				for (FaturaTributacao faturaTributacao : listaFaturaTributacao) {
					controladorFatura.inserir(faturaTributacao);
				}

				controladorFatura.registrarLancamentoContabil(fatura, OperacaoContabil.INCLUIR_NOTA,
								controladorFatura.parametrosMontarCodigoChaveEventoComercial(), null, Boolean.TRUE);
				// FIXME: falta gerar integração de títulos

			} else {
				creditoDebitoNegociado.setValorEntrada(BigDecimal.ZERO);
			}

			if (dataInicioCobranca != null) {
				creditoDebitoNegociado.setDataInicioCobranca(dataInicioCobranca);
			} else {
				entidadeConteudoInicioCobranca = controladorEntidadeConteudo.obter(idInicioCobranca);
				creditoDebitoNegociado.setEventoInicioCobranca(entidadeConteudoInicioCobranca);

				creditoDebitoNegociado.setNumeroDiasCarencia(Integer.parseInt(quantidadeDiasCobranca));
			}

		} else {
			// false; caso credito

			// credito origem
			creditoDebitoNegociado.setCreditoOrigem(creditoOrigem);

			creditoDebitoNegociado.setValorEntrada(BigDecimal.ZERO);

			// data de inicio de cobranca
			if (dataInicioCredito != null) {
				creditoDebitoNegociado.setDataInicioCobranca(dataInicioCredito);
			}
		}

		creditoDebitoNegociado.setHabilitado(Boolean.TRUE);
		creditoDebitoNegociado.setUltimaAlteracao(Calendar.getInstance().getTime());

		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		Long idCreditoDebitoNegociado = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(creditoDebitoNegociado);
		creditoDebitoNegociado = this.obterCreditoDebitoNegociado(idCreditoDebitoNegociado);

		validarAlcada(dadosGerais, creditoDebitoNegociado);

		// geracao dos creditosDebitosARealizar
		String codigoLAICJurosFinanciamento = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_LANCAMENTO_ITEM_CONTABIL_JUROS_FINANCIAMENTO);

		LancamentoItemContabil laicJurosParcelamento = controladorParcelamento.obterLancamentoItemContabil(Long
						.valueOf(codigoLAICJurosFinanciamento));
		LancamentoItemContabil laicAmortizacao = null;
		if (rubrica != null) {
			laicAmortizacao = rubrica.getLancamentoItemContabil();
		}

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPendente));

		String valorParametro = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);
		CreditoDebitoSituacao creditoDebitoSituacao = null;
		creditoDebitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(Long.valueOf(valorParametro));

		// regra para definir a periodicidade:
		// 08/07/10
		int ciclo = 1;
		// ciclo atual na contagem
		int cicloMaximo = 0;
		// quantidade máxima de ciclos no mes
		int valorReferenciaFaturamento = 0;
		// ultimo
		// ano
		// mes
		// faturado

		String referenciaFaturamento = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);

		if (dataInicioCobranca != null) {

			String codigoPeriodicidadeContrato = obterPeriodicidadeContratoPontoConsumo(dadosGerais.getIdPeriodicidade(), idPontoConsumo);
			// periodicidade informada no contrato(Se houver)

			valorReferenciaFaturamento = this.obterValorReferenciaFaturamento(dataInicioCobranca, referenciaFaturamento);

			// -----verifica o ciclo -------
			DateTime dataCobranca = new DateTime(dataInicioCobranca);
			// data de inicio de cobrança (informada na tela)

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dataInicioCobranca);
			// FIXME: URGENTE deve ser recuperado da periodicidade
			cicloMaximo = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) / Integer.parseInt(codigoPeriodicidadeContrato);

			int diaBaseCobranca = dataCobranca.getDayOfMonth();
			int basePeriodicidade = Integer.parseInt(codigoPeriodicidadeContrato);

			while (diaBaseCobranca > basePeriodicidade) {
				ciclo = ciclo + 1;
				basePeriodicidade = basePeriodicidade + Integer.parseInt(codigoPeriodicidadeContrato);
			}
		}

		if (dataInicioCredito != null) {
			valorReferenciaFaturamento = this.obterValorReferenciaFaturamento(dataInicioCredito, referenciaFaturamento);
		}

		BigDecimal totalValorJuros = BigDecimal.ZERO;
		for (DadosParcelas dadosParcelas : dadosGerais.getListaDadosParcelas()) {

			totalValorJuros = totalValorJuros.add(dadosParcelas.getJuros());
			creditoDebitoARealizar = (CreditoDebitoARealizar) this.criar();

			if (dataInicioCobranca != null || dataInicioCredito != null) {
				creditoDebitoARealizar.setAnoMesFaturamento(valorReferenciaFaturamento);
				creditoDebitoARealizar.setNumeroCiclo(ciclo);

				Map<String, Integer> referenciaCiclo = null;
				if (controladorParametroSistema.obterValorParametroUtilizacaoMultiplosCiclos()) {
					referenciaCiclo = Util.gerarProximaReferenciaCiclo(valorReferenciaFaturamento, ciclo, TRINTA_DIAS, new DateTime());
				} else {
					referenciaCiclo = Util.rolarReferenciaCiclo(valorReferenciaFaturamento, ciclo, cicloMaximo);
				}

				ciclo = referenciaCiclo.get("ciclo");
				valorReferenciaFaturamento = referenciaCiclo.get("referencia");
			} else {
				creditoDebitoARealizar.setAnoMesFaturamento(CONSTANTE_LIMITE);
			}

			creditoDebitoARealizar.setCreditoDebitoNegociado(creditoDebitoNegociado);
			creditoDebitoARealizar.setCreditoDebitoSituacao(creditoDebitoSituacao);
			creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamento);
			creditoDebitoARealizar.setNumeroPrestacao(dadosParcelas.getNumeroParcela());
			creditoDebitoARealizar.setValor(dadosParcelas.getTotal());
			creditoDebitoARealizar.setQuantidade(quantidade);
			creditoDebitoARealizar.setIndicadorAntecipacao(false);
			// valor
			// padrão
			creditoDebitoARealizar.setHabilitado(Boolean.TRUE);
			creditoDebitoARealizar.setUltimaAlteracao(Calendar.getInstance().getTime());

			creditoDebitoARealizar.setDadosAuditoria(dadosGerais.getDadosAuditoria());

			Long idCreditoDebitoARealizar = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession()
							.save(creditoDebitoARealizar);
			creditoDebitoARealizar = (CreditoDebitoARealizar) this.obter(idCreditoDebitoARealizar);
			
			listaCreditoDebitoRealizar.add(creditoDebitoARealizar);

			creditoDebitoDetalhamento = popularCreditoDebitoDetalhamento(creditoDebitoARealizar, rubrica, dadosGerais.getDadosAuditoria());
			if (laicAmortizacao != null) {
				creditoDebitoDetalhamento.setLancamentoItemContabil(laicAmortizacao);
			}
			if (dadosParcelas.getAmortizacao() != null) {
				creditoDebitoDetalhamento.setValor(dadosParcelas.getAmortizacao());
			} else {
				creditoDebitoDetalhamento.setValor(BigDecimal.ZERO);
			}

			if (creditoDebitoDetalhamento.getValor().compareTo(BigDecimal.ZERO) > 0) {
				// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
				getHibernateTemplate().getSessionFactory().getCurrentSession().save(creditoDebitoDetalhamento);
			}

			if (dadosParcelas.getJuros() != null) {
				creditoDebitoDetalhamento = popularCreditoDebitoDetalhamento(creditoDebitoARealizar, rubrica,
								dadosGerais.getDadosAuditoria());
				creditoDebitoDetalhamento.setLancamentoItemContabil(laicJurosParcelamento);
				if (dadosParcelas.getJuros() != null) {
					creditoDebitoDetalhamento.setValor(dadosParcelas.getJuros());
				} else {
					creditoDebitoDetalhamento.setValor(BigDecimal.ZERO);
				}

				if (creditoDebitoDetalhamento.getValor().compareTo(BigDecimal.ZERO) > 0) {
					// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
					getHibernateTemplate().getSessionFactory().getCurrentSession().save(creditoDebitoDetalhamento);
				}
			}
		}

		creditoDebitoNegociado.setValorJuros(totalValorJuros);
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(creditoDebitoNegociado);

		DocumentoCobranca documentoCobranca = (DocumentoCobranca) controladorCobranca.criarDocumentoCobranca();
		documentoCobranca.setDadosAuditoria(dadosGerais.getDadosAuditoria());
		documentoCobranca.setDataVencimento(Calendar.getInstance().getTime());

		registrarLancamentoContabil(creditoDebitoNegociado, OperacaoContabil.INCLUIR_CREDITO_DEBITO, null);

		return this.relatorioCreditoDebitoRealizar(creditoDebitoNegociado, listaCreditoDebitoRealizar);
	}

	/**
	 * Valida de existe controle de alçada para o
	 * crédito/débito.
	 *
	 * @param dadosGerais
	 *            the dados gerais
	 * @param creditoDebitoNegociado
	 *            the credito debito negociado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarAlcada(DadosGeraisParcelas dadosGerais, CreditoDebitoNegociado creditoDebitoNegociado) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		EntidadeConteudo status = null;

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorAlcada controladorAlcada = (ControladorAlcada) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ParametroSistema parametroTabelaCreditoDebito = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_CREDITO_DEBITO_NEGOCIADO);
		Long chaveTabelaCredDeb = null;
		if (parametroTabelaCreditoDebito != null) {
			chaveTabelaCredDeb = Long.valueOf(parametroTabelaCreditoDebito.getValor());
		}

		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		Collection<Papel> papeis = null;
		if (dadosGerais.getDadosAuditoria() != null && dadosGerais.getDadosAuditoria().getUsuario() != null
						&& dadosGerais.getDadosAuditoria().getUsuario().getFuncionario() != null) {
			papeis = controladorPapel.consultarPapeisPorFuncionario(dadosGerais.getDadosAuditoria().getUsuario().getFuncionario()
							.getChavePrimaria());
		}

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		if (chaveTabelaCredDeb != null && papeis != null && !papeis.isEmpty()) {
			Collection<Alcada> colecaoAlcadas = controladorAlcada.obterListaAlcadasVigentes(chaveTabelaCredDeb, papeis, Calendar
							.getInstance().getTime());
			if (colecaoAlcadas != null && !colecaoAlcadas.isEmpty()) {
				for (Alcada alcada : colecaoAlcadas) {
					try {

						String propriedade = alcada.getColuna().getNomePropriedade();
						propriedade = propriedade.substring(0, 1).toUpperCase() + propriedade.subSequence(1, propriedade.length());
						String nomeMetodo = REFLECTION_GET + propriedade;

						Object valorMetodoObj = creditoDebitoNegociado.getClass().getDeclaredMethod(nomeMetodo, new Class[] {})
										.invoke(creditoDebitoNegociado);
						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());
							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)) {

								status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));

								break;
							}
						}
					} catch (IllegalArgumentException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (SecurityException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (IllegalAccessException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (InvocationTargetException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					} catch (NoSuchMethodException e) {
						// Colocar informações de
						// erro no log.
						LOG.error(e);
					}
				}
			}
		}

		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		if (status != null) {
			creditoDebitoNegociado.setStatus(status);
			if (creditoDebitoNegociado.getUltimoUsuarioAlteracao() == null) {
				creditoDebitoNegociado.setUltimoUsuarioAlteracao(dadosGerais.getDadosAuditoria().getUsuario());
			}

			boolean isUnicoUsuarioAutorizado = controladorAlcada.possuiAlcadaAutorizacaoCreditosDebitos(dadosGerais.getDadosAuditoria()
							.getUsuario(), creditoDebitoNegociado);

			if (isUnicoUsuarioAutorizado) {
				status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusAutorizada));
			} else {
				status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
			}
		} else {
			status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusAutorizada));
		}

		if (creditoDebitoNegociado != null) {
			creditoDebitoNegociado.setStatus(status);
		}

		if (creditoDebitoNegociado != null && creditoDebitoNegociado.getDadosAuditoria() != null
						&& creditoDebitoNegociado.getDadosAuditoria().getUsuario() != null) {
			creditoDebitoNegociado.setUltimoUsuarioAlteracao(creditoDebitoNegociado.getDadosAuditoria().getUsuario());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterValorReferenciaFaturamento
	 * (java.util.Date, java.lang.String)
	 */
	@Override
	public int obterValorReferenciaFaturamento(Date dataInicioCreditoCobranca, String referenciaFaturamento) throws NegocioException {

		int valorReferenciaFaturamento = 0;

		if (Util.compararAnoMes(Util.obterAnoMes(dataInicioCreditoCobranca), Integer.valueOf(referenciaFaturamento)) <= 0) {
			valorReferenciaFaturamento = Integer.parseInt(referenciaFaturamento);
		} else {
			valorReferenciaFaturamento = Util.obterAnoMes(dataInicioCreditoCobranca);
		}

		return valorReferenciaFaturamento;

	}

	/**
	 * Carregar fatura.
	 *
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param valor
	 *            the valor
	 * @param debitoARealizar
	 *            the debito a realizar
	 * @param cliente
	 *            the cliente
	 * @param rubrica
	 *            the rubrica
	 * @return the fatura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Fatura carregarFatura(Long idPontoConsumo, BigDecimal valor, CreditoDebitoARealizar debitoARealizar, Cliente cliente,
					Rubrica rubrica) throws NegocioException {

		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Fatura fatura = (Fatura) controladorParcelamento.criarFatura();
		fatura.setAnoMesReferencia(Util.obterAnoMesCorrente());
		fatura.setNumeroCiclo(1);
		fatura.setValorTotal(valor);
		fatura.setCobrada(Boolean.TRUE);
		fatura.setProvisaoDevedoresDuvidosos(Boolean.FALSE);
		fatura.setDataVencimento(Calendar.getInstance().getTime());

		fatura.setTipoDocumento(controladorParcelamento.obterTipoDocumentoNotasDebito());
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo, "segmento");
			fatura.setPontoConsumo(pontoConsumo);
			fatura.setSegmento(pontoConsumo.getSegmento());
		}
		fatura.setCliente(cliente);
		String codigoSituacaoNormal = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL);
		fatura.setCreditoDebitoSituacao(controladorParcelamento.obterDebitoSituacao(codigoSituacaoNormal));

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPendente));
		fatura.setSituacaoPagamento(situacaoPagamento);

		fatura.setDataEmissao(Util.getDataCorrente(false));

		// Criando e adicionando valores da Fatura Item.
		FaturaItem faturaItem = (FaturaItem) controladorParcelamento.criarFaturaItem();
		faturaItem.setCreditoDebitoARealizar(debitoARealizar);
		faturaItem.setValorUnitario(valor);
		faturaItem.setQuantidade(BigDecimal.ONE);
		faturaItem.setValorTotal(valor);
		faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
		faturaItem.setRubrica(rubrica);
		faturaItem.setHabilitado(Boolean.TRUE);
		faturaItem.setNumeroSequencial(1);

		fatura.getListaFaturaItem().add(faturaItem);
		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #listarCreditoOrigem()
	 */
	@Override
	public Collection<CreditoOrigem> listarCreditoOrigem() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCreditoOrigem());
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #listarCreditoDebitoSituacao()
	 */
	@Override
	public Collection<CreditoDebitoSituacao> listarCreditoDebitoSituacao() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCreditoDebitoSituacao());
		criteria.addOrder(Order.asc("descricao"));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #listarCreditoDebitoSituacao()
	 */
	@Override
	public Collection<CreditoDebitoSituacao> listarCreditoDebitoSituacaoValida() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCreditoDebitoSituacao());
		criteria.add(Restrictions.eq("valido", Boolean.TRUE));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterCreditoOrigem(java.lang.Long)
	 */
	@Override
	public CreditoOrigem obterCreditoOrigem(Long idCreditoOrigem) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCreditoOrigem().getSimpleName()).append(" creditoOrigem ");
		hql.append(" where ");
		hql.append(" creditoOrigem.chavePrimaria = :idCreditoOrigem ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCreditoOrigem", idCreditoOrigem);

		return (CreditoOrigem) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #cancelarLancamentoCreditoDebito
	 * (java.lang.Long[])
	 */
	@Override
	public void cancelarLancamentoCreditoDebito(Long[] chavesCreditoDebito, Long idMotivoCancelamento, DadosAuditoria dadosAuditoria)
					throws NegocioException, ConcorrenciaException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("chavesCreditoDebitosNegociado", chavesCreditoDebito);
		Collection<CreditoDebitoARealizar> listaCreditoDebito = this.consultarCreditoDebitoARealizar(filtro);
		ControladorConstanteSistema controladorConstantesSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		if (idMotivoCancelamento == null || idMotivoCancelamento <= 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, CreditoDebitoARealizar.MOTIVO_CANCELAMENTO);
		}

		Long idRubricaParcelamento = Long.valueOf(controladorConstantesSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_PARCELAMENTO_DEBITO));

		for (CreditoDebitoARealizar creditoDebito : listaCreditoDebito) {
			if (creditoDebito.getCreditoDebitoNegociado().getRubrica().getChavePrimaria() == idRubricaParcelamento) {
				throw new NegocioException(ControladorFatura.ERRO_NEGOCIO_RUBRICA_PARCELAMENTO, true);
			}
		}

		String idCreditoDebitoSituacao = controladorConstantesSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_CANCELADO);

		ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		CreditoDebitoSituacao debitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(Long.valueOf(idCreditoDebitoSituacao));

		EntidadeConteudo motivoCancelamento = controladorEntidadeConteudo.obter(idMotivoCancelamento);

		for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebito) {
			creditoDebitoARealizar.setCreditoDebitoSituacao(debitoSituacao);
			creditoDebitoARealizar.setMotivoCancelamento(motivoCancelamento);
			creditoDebitoARealizar.setDadosAuditoria(dadosAuditoria);

			try {
				this.atualizar(creditoDebitoARealizar);
			} catch (ConcorrenciaException e) {
				throw new NegocioException(e);
			}
		}

		for (Long chave : chavesCreditoDebito) {
			CreditoDebitoNegociado creditoDebitoNegociado = this.obterCreditoDebitoNegociado(chave);
			creditoDebitoNegociado.setDadosAuditoria(dadosAuditoria);
			registrarLancamentoContabil(creditoDebitoNegociado, OperacaoContabil.CANCELAR_CREDITO_DEBITO, null);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #popularCreditoDebitoNegociado
	 * (br.com.ggas.cadastro.imovel.PontoConsumo,
	 * br.com.ggas.cadastro.cliente.Cliente,
	 * br.com.ggas.faturamento.rubrica.Rubrica,
	 * java.math.BigDecimal,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public CreditoDebitoNegociado popularCreditoDebitoNegociado(PontoConsumo pontoConsumo, Cliente cliente, Rubrica rubrica,
					BigDecimal valor, DadosAuditoria dadosAuditoria, String origem) throws NegocioException {

		CreditoDebitoNegociado debitoNegociado = (CreditoDebitoNegociado) this.criarCreditoDebitoNegociado();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ServiceLocator.getInstancia().getControladorContrato();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		debitoNegociado.setAmortizacao(null);
		debitoNegociado.setPeriodicidadeJuros(null);
		debitoNegociado.setDataInicioCobranca(Calendar.getInstance().getTime());
		debitoNegociado.setPercentualJuros(null);
		debitoNegociado.setRubrica(rubrica);
		debitoNegociado.setNumeroPrestacaoCobrada(0);
		debitoNegociado.setPontoConsumo(pontoConsumo);
		debitoNegociado.setFinanciamentoTipo(rubrica.getFinanciamentoTipo());

		Cliente clienteAux = null;

		EntidadeConteudo formaCobranca = controladorEntidadeConteudo
						.obterEntidadeConteudoPadraoPorClasse(EntidadeClasse.CODIGO_FORMA_COBRANCA_PARCELAMENTO);

		if (!StringUtils.isEmpty(origem)) {
			CreditoOrigem creditoOrigem = obterCreditoOrigem(Long.valueOf(origem));
			debitoNegociado.setCreditoOrigem(creditoOrigem);
		}

		if (pontoConsumo != null) {

			PontoConsumo pontoConsumoBD = (PontoConsumo) controladorPontoConsumo.obter(pontoConsumo.getChavePrimaria(), "segmento");
			debitoNegociado.setSegmento(pontoConsumoBD.getSegmento());
			clienteAux = controladorCliente.obterClientePorPontoConsumo(pontoConsumo.getChavePrimaria());

		}

		if (cliente != null) {
			clienteAux = (Cliente) controladorCliente.obter(cliente.getChavePrimaria());
		}
		debitoNegociado.setCliente(clienteAux);
		debitoNegociado.setFormaCobranca(formaCobranca);
		debitoNegociado.setQuantidadeTotalPrestacoes(1);
		debitoNegociado.setDataFinanciamento(Calendar.getInstance().getTime());
		debitoNegociado.setAnoMesInclusao(Util.obterAnoMesCorrente());
		debitoNegociado.setValorJuros(null);
		debitoNegociado.setValor(valor);
		debitoNegociado.setValorEntrada(BigDecimal.ZERO);
		debitoNegociado.setDadosAuditoria(dadosAuditoria);
		debitoNegociado.setHabilitado(Boolean.TRUE);
		debitoNegociado.setUltimaAlteracao(Calendar.getInstance().getTime());
		debitoNegociado.setUltimoUsuarioAlteracao(dadosAuditoria.getUsuario());

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema alcadaAutorizado = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		EntidadeConteudo status = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(alcadaAutorizado.getValor()));
		debitoNegociado.setStatus(status);

		return debitoNegociado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #popularCreditoDebitoARealizar
	 * (br.com.ggas.faturamento.DocumentoCobrancaItem
	 * ,
	 * br.com.ggas.faturamento.FaturaGeral,
	 * java.math.BigDecimal,
	 * br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public CreditoDebitoARealizar popularCreditoDebitoARealizar(DocumentoCobrancaItem documentoCobrancaItem, FaturaGeral fatura,
					BigDecimal valor, CreditoDebitoNegociado debitoNegociado, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		EntidadeConteudo situacaoPagamento = controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPendente));

		String codigoSituacaoNormal = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL);

		CreditoDebitoARealizar creditoDebitoARealizar = (CreditoDebitoARealizar) this.criar();

		creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamento);
		creditoDebitoARealizar.setNumeroPrestacao(1);
		creditoDebitoARealizar.setIndicadorAntecipacao(Boolean.FALSE);
		creditoDebitoARealizar.setCreditoDebitoNegociado(debitoNegociado);
		creditoDebitoARealizar.setAnoMesFaturamento(Util.obterAnoMesCorrente());
		creditoDebitoARealizar.setValor(valor);
		creditoDebitoARealizar.setQuantidade(BigDecimal.ONE);

		CreditoDebitoSituacao debitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(Long.valueOf(codigoSituacaoNormal));
		creditoDebitoARealizar.setCreditoDebitoSituacao(debitoSituacao);
		FaturaGeral faturaGeral = fatura;
		if (faturaGeral == null && documentoCobrancaItem != null && documentoCobrancaItem.getFaturaGeral() != null) {
			faturaGeral = documentoCobrancaItem.getFaturaGeral();
		}

		if (faturaGeral != null) {
			faturaGeral = (FaturaGeral) controladorFatura.obter(faturaGeral.getChavePrimaria(), FaturaGeralImpl.class, "faturaAtual");
			creditoDebitoARealizar.setOrigem(faturaGeral);
			creditoDebitoARealizar.setNumeroCiclo(faturaGeral.getFaturaAtual().getNumeroCiclo());
		}

		// TODO verificar o que setar nesses dois
		// atributos.
		// private Date dataVencimento
		// private Integer anoMesCobranca

		creditoDebitoARealizar.setDadosAuditoria(dadosAuditoria);
		creditoDebitoARealizar.setHabilitado(Boolean.TRUE);
		creditoDebitoARealizar.setUltimaAlteracao(Calendar.getInstance().getTime());

		return creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #popularCreditoDebitoDetalhamento
	 * (br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoARealizar,
	 * br.com.ggas.faturamento.rubrica.Rubrica,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public CreditoDebitoDetalhamento popularCreditoDebitoDetalhamento(CreditoDebitoARealizar creditoDebitoARealizar, Rubrica rubrica,
					DadosAuditoria dadosAuditoria) throws NegocioException {

		CreditoDebitoDetalhamento creditoDebitoDetalhamento = (CreditoDebitoDetalhamento) this.criarCreditoDebitoDetalhamento();
		creditoDebitoDetalhamento.setLancamentoItemContabil(rubrica.getLancamentoItemContabil());
		creditoDebitoDetalhamento.setCreditoDebitoARealizar(creditoDebitoARealizar);
		creditoDebitoDetalhamento.setValor(creditoDebitoARealizar.getValor());
		creditoDebitoDetalhamento.setDadosAuditoria(dadosAuditoria);
		creditoDebitoDetalhamento.setHabilitado(Boolean.TRUE);
		creditoDebitoDetalhamento.setUltimaAlteracao(Calendar.getInstance().getTime());
		return creditoDebitoDetalhamento;
	}

	/**
	 * Validar campos obrigatorios geracao parcelas.
	 *
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarCamposObrigatoriosGeracaoParcelas(Map<String, Object> dados) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;
		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();

		Long idPontoConsumo = (Long) dados.get("idPontoConsumo");

		String indicadorCreditoDebito = (String) dados.get("tipoDocumento");
		Boolean tipoDebito = null;
		if (!StringUtils.isEmpty(indicadorCreditoDebito)) {
			tipoDebito = Boolean.valueOf(indicadorCreditoDebito);
		}

		Long idRubrica = (Long) dados.get("idRubrica");
		Rubrica rubrica = null;
		if ((idRubrica == null) || (idRubrica <= 0)) {
			stringBuilder.append(CreditoDebitoARealizar.RUBRICA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			rubrica = (Rubrica) controladorRubrica.obter(idRubrica);
		}

		if (tipoDebito == null) {
			stringBuilder.append("Tipo de Lançamento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if (tipoDebito) {
				Date dataInicioCobranca = (Date) dados.get("dataInicioCobranca");
				String quantidadeDiasCobranca = (String) dados.get("qtdDiasCobranca");
				Long idInicioCobranca = (Long) dados.get("idInicioCobranca");
				BigDecimal taxaJuros = (BigDecimal) dados.get("taxaJuros");
				Long idPeriodicidadeJuros = (Long) dados.get("idPeriodicidadeJuros");

				BigDecimal quantidade = (BigDecimal) dados.get("quantidade");
				if ((quantidade == null) || (quantidade.compareTo(BigDecimal.ZERO) <= 0)) {
					stringBuilder.append(CreditoDebitoARealizar.QUANTIDADE);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}

				if (rubrica != null) {
					if (rubrica.getIndicadorEntradaObrigatoria() && dados.get("valorEntrada") == null) {
						stringBuilder.append(CreditoDebitoARealizar.VALOR_ENTRADA);
						stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
					}

					if (rubrica.getAmortizacao() != null && taxaJuros == null) {
						stringBuilder.append(CreditoDebitoARealizar.TAXA_JUROS);
						stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
					}

					if (rubrica.getAmortizacao() != null && idPeriodicidadeJuros == null) {
						stringBuilder.append(CreditoDebitoARealizar.PERIODICIDADE_JUROS);
						stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
					}
				}

				if (dataInicioCobranca == null && (idInicioCobranca == null || quantidadeDiasCobranca == null || idInicioCobranca <= 0
									|| StringUtils.isEmpty(quantidadeDiasCobranca))) {
					stringBuilder.append(CreditoDebitoARealizar.INICIO_COBRANCA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}

			} else {
				Long idCreditoOrigem = (Long) dados.get("idCreditoOrigem");

				if ((idCreditoOrigem == null) || (idCreditoOrigem <= 0)) {
					stringBuilder.append(CreditoDebitoARealizar.CREDITO_ORIGEM);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}

				BigDecimal quantidade = (BigDecimal) dados.get("quantidade");
				if ((quantidade == null) || (quantidade.compareTo(BigDecimal.ZERO) <= 0)) {
					stringBuilder.append(CreditoDebitoARealizar.QUANTIDADE);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}

				Date dataInicioCredito = (Date) dados.get("dataInicioCredito");
				if (dataInicioCredito == null) {
					stringBuilder.append(CreditoDebitoARealizar.INICIO_CREDITO);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		// TODO remover comentário quando setar a
		// quantidade no action.
		/*
		 * Integer quantidade = (Integer)
		 * dados.get("quantidade")
		 * if (quantidade == null) Ponto.
		 * stringBuilder.append(CreditoDebitoARealizar
		 * .QUANTIDADE)
		 * stringBuilder.append(Constantes.
		 * STRING_VIRGULA_ESPACO)
		 */

		BigDecimal valor = (BigDecimal) dados.get("valorUnitarioHidden");

		if (valor == null) {
			stringBuilder.append(CreditoDebitoARealizar.VALOR_UNITARIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		Integer parcelas = (Integer) dados.get("parcelas");
		if (parcelas == null) {
			stringBuilder.append(CreditoDebitoARealizar.PARCELAS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		Long idPeriodicidade = (Long) dados.get("idPeriodicidade");
		if ((idPeriodicidade == null) || (idPeriodicidade <= 0)) {
			stringBuilder.append(CreditoDebitoARealizar.PERIODICIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {

			String periodicidadeMensal = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COBRANCA_MENSAL);
			String melhorDiaVencimento = (String) dados.get("melhorDiaVencimento");

			// Se a periodicidade for mensal o
			// campo 'melhorDiaVencimento' é
			// obrigatório.
			if (idPeriodicidade.equals(Long.valueOf(periodicidadeMensal)) && (idPontoConsumo == null || idPontoConsumo <= 0)
							&& (melhorDiaVencimento == null || Long.parseLong(melhorDiaVencimento) <= 0)) {
				stringBuilder.append(CreditoDebitoARealizar.MELHOR_DIA_VENCIMENTO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterSaldoDevedorCreditoDebitoNegociado
	 * (java.lang.Long)
	 */
	@Override
	public BigDecimal obterSaldoDevedorCreditoDebitoNegociado(Long idCreditoDebitoNegociado) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		StringBuilder hql = new StringBuilder();
		hql.append(" select sum(creditoDebito.valor) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebito ");
		hql.append(" where ");
		hql.append(" creditoDebito.creditoDebitoNegociado.chavePrimaria = :idCreditoDebitoNegociado ");
		hql.append(" and creditoDebito.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");
		hql.append(" and creditoDebito.anoMesCobranca is null");
		hql.append(" and creditoDebito.situacaoPagamento.chavePrimaria = ").append(codigoSituacaoPendente);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCreditoDebitoNegociado", idCreditoDebitoNegociado);
		query.setLong("idStatusAutorizacao", status);

		return (BigDecimal) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#consultarPesquisaCreditoDebito(java.util.Map)
	 */
	@Override
	public Collection<Map<String, Object>> consultarPesquisaCreditoDebito(Map<String, Object> filtro) throws NegocioException {

		List<Map<String, Object>> listaExibicaoCreditoDebito = new ArrayList<>();
		Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = this.consultarCreditoDebitoARealizar(filtro);
		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		List<Long> chavesCreditoDebitoNegociado = new ArrayList<>();
		for (CreditoDebitoARealizar creditoDebitoARealizar1 : listaCreditoDebitoARealizar) {
			Map<String, Object> dados = new HashMap<>();
			CreditoDebitoNegociado creditoDebitoNegociado = creditoDebitoARealizar1.getCreditoDebitoNegociado();
			if (!chavesCreditoDebitoNegociado.contains(creditoDebitoNegociado.getChavePrimaria())) {
				chavesCreditoDebitoNegociado.add(creditoDebitoNegociado.getChavePrimaria());

				int countSituacaoInvalida = 0;
				int countSituacaoCobrada = 0;
				int countSituacaoQuitada = 0;
				BigDecimal saldoDevedor = BigDecimal.ZERO;

				dados.put("chavePrimaria", creditoDebitoNegociado.getChavePrimaria());
				dados.put("rubrica", creditoDebitoNegociado.getRubrica().getDescricao());
				if (creditoDebitoNegociado.getStatus() != null) {
					dados.put("status", creditoDebitoNegociado.getStatus().getDescricao());
				}
				dados.put("numeroPrestacaoCobrada", creditoDebitoNegociado.getNumeroPrestacaoCobrada());
				dados.put("quantidadeTotalPrestacoes", creditoDebitoNegociado.getQuantidadeTotalPrestacoes());

				if (creditoDebitoNegociado.getPontoConsumo() != null) {
					dados.put("pontoConsumo", creditoDebitoNegociado.getPontoConsumo().getDescricao());
					dados.put("codigoLegado", creditoDebitoNegociado.getPontoConsumo().getCodigoLegado());
					dados.put("codigoPontoConsumo", creditoDebitoNegociado.getPontoConsumo().getCodigoPontoConsumo());
				}

				if (creditoDebitoNegociado.getCreditoOrigem() != null) {
					dados.put("creditoOrigem", Boolean.TRUE);
				}

				Map<String, Object> filtroCreditoDebitoARealizar = new HashMap<>();
				filtroCreditoDebitoARealizar.put("idCreditoDebitoNegociado", creditoDebitoNegociado.getChavePrimaria());
				Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizarConsulta = this
								.consultarCreditoDebitoARealizar(filtroCreditoDebitoARealizar);

				if (listaCreditoDebitoARealizarConsulta != null) {

					BigDecimal somatorioParcelas = BigDecimal.ZERO;
					for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebitoARealizarConsulta) {

						if (creditoDebitoARealizar.getAnoMesCobranca() == null
										&& creditoDebitoARealizar.getCreditoDebitoSituacao().isValido()) {
							saldoDevedor = saldoDevedor.add(creditoDebitoARealizar.getValor());
							if (creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria() != Long.parseLong(codigoSituacaoPendente)) {
								// parcialmente pago
								BigDecimal valorPago = controladorRecebimento
												.obterValorRecebimentoPelaCreditoDebitoARealizar(creditoDebitoARealizar.getChavePrimaria());
								if (valorPago != null) {
									saldoDevedor = saldoDevedor.subtract(valorPago);
								}
							}
						}

						somatorioParcelas = somatorioParcelas.add(creditoDebitoARealizar.getValor());

						String idSituacaoPagamentoPago = controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

						if (!creditoDebitoARealizar.getCreditoDebitoSituacao().isValido()) {
							countSituacaoInvalida++;
						} else if (creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria() == Long
										.parseLong(idSituacaoPagamentoPago)) {
							countSituacaoQuitada++;
						} else if (creditoDebitoARealizar.getAnoMesCobranca() != null) {
							countSituacaoCobrada++;
						}
					}

					dados.put("valor", somatorioParcelas);

					int countTotalSituacao = 0;
					countTotalSituacao = countSituacaoInvalida + countSituacaoCobrada + countSituacaoQuitada;

					if (creditoDebitoNegociado.getNumeroPrestacaoCobrada().equals(creditoDebitoNegociado.getQuantidadeTotalPrestacoes())) {
						dados.put("situacao", SITUACAO_ENCERRADO);
						dados.put("saldo", BigDecimal.ZERO);
						dados.put("indicadorCancelamentoBloqueado", true);
					} else if (countSituacaoInvalida == creditoDebitoNegociado.getQuantidadeTotalPrestacoes()) {
						dados.put("situacao", SITUACAO_CANCELADO);
						dados.put("saldo", BigDecimal.ZERO);
						dados.put("indicadorCancelamentoBloqueado", true);
					} else if (creditoDebitoNegociado.getQuantidadeTotalPrestacoes() == countTotalSituacao) {
						dados.put("situacao", SITUACAO_ENCERRADO);
						dados.put("saldo", BigDecimal.ZERO);
						dados.put("indicadorCancelamentoBloqueado", true);
					} else {
						dados.put("situacao", SITUACAO_EM_ANDAMENTO);
						dados.put("saldo", saldoDevedor);
						dados.put("indicadorCancelamentoBloqueado", false);
					}
				}

				listaExibicaoCreditoDebito.add(dados);
			}
		}

		return listaExibicaoCreditoDebito;
	}

	@Override
	public Collection<Map<String, Object>> consultarPesquisaCreditoDebitoParcelamento(Map<String, Object> filtro, boolean andamento)
					throws NegocioException {

		Collection<CreditoDebitoNegociado> listaCreditoDebito = this.consultarCreditoDebitoNegociado(filtro);
		List<Map<String, Object>> listaExibicaoCreditoDebito = new ArrayList<>();
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSituacaoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		String codigoSituacaoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

		for (CreditoDebitoNegociado creditoDebitoNegociado : listaCreditoDebito) {
			Map<String, Object> dados = new HashMap<>();

			dados.put("chavePrimaria", creditoDebitoNegociado.getChavePrimaria());
			dados.put("rubrica", creditoDebitoNegociado.getRubrica().getDescricao());
			dados.put("numeroPrestacaoCobrada", creditoDebitoNegociado.getNumeroPrestacaoCobrada());
			dados.put("quantidadeTotalPrestacoes", creditoDebitoNegociado.getQuantidadeTotalPrestacoes());
			creditoDebitoNegociado.setValorEntrada(BigDecimal.ZERO);

			if (creditoDebitoNegociado.getPontoConsumo() != null) {
				dados.put("pontoConsumo", creditoDebitoNegociado.getPontoConsumo().getDescricao());
				dados.put("codigoLegado", creditoDebitoNegociado.getPontoConsumo().getCodigoLegado());
			}

			if (creditoDebitoNegociado.getCreditoOrigem() != null) {
				dados.put("creditoOrigem", Boolean.TRUE);
			}

			Map<String, Object> filtroCreditoDebitoARealizar = new HashMap<>();
			filtroCreditoDebitoARealizar.put("idCreditoDebitoNegociado", creditoDebitoNegociado.getChavePrimaria());
			Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = this
							.consultarCreditoDebitoARealizar(filtroCreditoDebitoARealizar);

			if (listaCreditoDebitoARealizar != null) {
				CreditoDebitoARealizar creditoDebitoARealizarAnterior = null;
				boolean indicadorMudouSituacao = false;
				boolean indicadorCancelado = false;

				BigDecimal somatorioParcelas = BigDecimal.ZERO;
				for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebitoARealizar) {

					Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = this
									.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
					BigDecimal valoresDetalhamento = BigDecimal.ZERO;

					for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCDDetalhamento) {
						if (creditoDebitoDetalhamento.getValor() != null) {
							valoresDetalhamento = valoresDetalhamento.add(creditoDebitoDetalhamento.getValor());
						}
					}

					creditoDebitoARealizar.setValor(valoresDetalhamento);

					if (creditoDebitoARealizarAnterior == null) {

						creditoDebitoARealizarAnterior = creditoDebitoARealizar;
						if (creditoDebitoARealizar.getMotivoCancelamento() != null) {
							indicadorCancelado = true;
						}

					} else if (creditoDebitoARealizar.getMotivoCancelamento() != null) {

						indicadorCancelado = true;

					} else if (creditoDebitoARealizarAnterior.getCreditoDebitoSituacao().getChavePrimaria() != creditoDebitoARealizar
									.getCreditoDebitoSituacao().getChavePrimaria()) {

						indicadorMudouSituacao = true;

					}

					BigDecimal valorSomado = BigDecimal.ZERO;

					if (creditoDebitoARealizar.getAnoMesCobranca() == null && creditoDebitoARealizar.getCreditoDebitoSituacao().isValido()) {
						if (creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria() == Long
										.parseLong(codigoSituacaoParcialmentePago)) {
							// parcialmente pago
							BigDecimal saldoDevedor = controladorRecebimento
											.obterValorRecebimentoPelaCreditoDebitoARealizar(creditoDebitoARealizar.getChavePrimaria());
							if (saldoDevedor != null) {
								valorSomado = creditoDebitoNegociado.getValorEntrada();
								creditoDebitoNegociado.setValorEntrada(valorSomado.add(creditoDebitoARealizar.getValor().subtract(
												saldoDevedor)));
							} else {
								valorSomado = creditoDebitoNegociado.getValorEntrada();
								creditoDebitoNegociado.setValorEntrada(valorSomado.add(creditoDebitoARealizar.getValor()));
							}
						} else if (creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria() == Long
										.parseLong(codigoSituacaoPendente)) {
							// pendente
							valorSomado = creditoDebitoNegociado.getValorEntrada();
							creditoDebitoNegociado.setValorEntrada(valorSomado.add(creditoDebitoARealizar.getValor()));
						}
					}

					somatorioParcelas = somatorioParcelas.add(creditoDebitoARealizar.getValor());
				}

				dados.put("valor", somatorioParcelas);

				String idCreditoDebitoSituacao = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_CANCELADO);
				ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
				CreditoDebitoSituacao debitoSituacao = controladorCobranca
								.obterCreditoDebitoSituacao(Long.valueOf(idCreditoDebitoSituacao));

				if (indicadorCancelado) {

					dados.put("situacao", SITUACAO_CANCELADO);
					dados.put("valorEntrada", BigDecimal.ZERO);
					dados.put("indicadorCancelamentoBloqueado", true);

				} else if (!indicadorMudouSituacao && creditoDebitoARealizarAnterior != null
						&& creditoDebitoARealizarAnterior.getCreditoDebitoSituacao() != null
								&& creditoDebitoARealizarAnterior.getCreditoDebitoSituacao().getChavePrimaria() == debitoSituacao
												.getChavePrimaria()) {

					dados.put("situacao", SITUACAO_ENCERRADO);
					dados.put("valorEntrada", BigDecimal.ZERO);
					dados.put("indicadorCancelamentoBloqueado", true);

				} else if (!indicadorMudouSituacao && creditoDebitoNegociado.getValorEntrada().compareTo(BigDecimal.ZERO) != 0) {

					dados.put("situacao", SITUACAO_EM_ANDAMENTO);
					dados.put("valorEntrada", creditoDebitoNegociado.getValorEntrada());
					dados.put("indicadorCancelamentoBloqueado", false);

				} else if (creditoDebitoNegociado.getValorEntrada().compareTo(BigDecimal.ZERO) == 0) {

					dados.put("situacao", SITUACAO_ENCERRADO);
					dados.put("valorEntrada", BigDecimal.ZERO);
					dados.put("indicadorCancelamentoBloqueado", true);

				}
			}

			if (andamento) {

				if (dados.get("situacao").equals(SITUACAO_EM_ANDAMENTO) && andamento) {
					listaExibicaoCreditoDebito.add(dados);
				}

			} else {

				listaExibicaoCreditoDebito.add(dados);

			}
		}

		return listaExibicaoCreditoDebito;
	}

	/**
	 * Obter data pagamento credito debito a realizar.
	 *
	 * @param idCreditoDebitoNegociado
	 *            the id credito debito negociado
	 * @param numeroPrestacao
	 *            the numero prestacao
	 * @return the credito debito a realizar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterCDDetalhamentoPeloCDARealizar(java.lang
	 * .Long)
	 */
	public CreditoDebitoARealizar obterDataPagamentoCreditoDebitoARealizar(Long idCreditoDebitoNegociado, Integer numeroPrestacao)
					throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long status = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" creditoDebitoARealizar ").append(" where ");
		hql.append(" creditoDebitoARealizar.numeroPrestacao = :numeroPrestacao ").append(AND);
		hql.append(" creditoDebitoARealizar.creditoDebitoNegociado.chavePrimaria = :idCreditoDebitoNegociado ");
		hql.append(" and creditoDebitoARealizar.creditoDebitoNegociado.status.chavePrimaria = :idStatusAutorizacao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setInteger("numeroPrestacao", numeroPrestacao);
		query.setLong("idCreditoDebitoNegociado", idCreditoDebitoNegociado);
		query.setLong("idStatusAutorizacao", status);

		return (CreditoDebitoARealizar) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterMultiplicacaoValorUnitario
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public Map<String, String> obterMultiplicacaoValorUnitario(String valorQuantidade, String valorUnitario)
					throws FormatoInvalidoException, NegocioException {

		Map<String, String> dados = new HashMap<>();
		BigDecimal retorno = BigDecimal.ZERO;

		if (valorQuantidade != null && valorUnitario != null) {
			BigDecimal valorQuantidadeBigDecimal = Util.converterCampoStringParaValorBigDecimal("Valor Quantidade", valorQuantidade,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

			BigDecimal valorUnitarioBigDecimal = Util.converterCampoStringParaValorBigDecimal("Valor Unitário", valorUnitario,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

			retorno = valorQuantidadeBigDecimal.multiply(valorUnitarioBigDecimal);
		}

		dados.put("valorTotal", String.valueOf(retorno));
		return dados;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #adicionarParametrosConsultaSituacao
	 * (java.util.Map)
	 */
	@Override
	public void adicionarParametrosConsultaSituacao(Map<String, Object[]> parametros) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoCreditoDebitoNormal = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);
		String codigoCreditoDebitoRetificada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_RETIFICADO);
		String codigoCreditoDebitoIncluida = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_INCLUIDO);

		Long[] arraySituacao = {Long.valueOf(codigoCreditoDebitoNormal), Long.valueOf(codigoCreditoDebitoRetificada), Long
						.valueOf(codigoCreditoDebitoIncluida)};
		parametros.put("arraySituacao", arraySituacao);

		String codigoSituacaoPagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

		Long[] arrayPagamento = {Long.valueOf(codigoSituacaoPagamentoPendente), Long.valueOf(codigoSituacaoPagamentoParcialmentePago)};
		parametros.put("arrayPagamento", arrayPagamento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterValorAbatidoCredito(java.lang.Long)
	 */
	@Override
	public BigDecimal obterValorCobradoCredito(Long idCredito, Long idFatura) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select sum(faturaItem.valorTotal) from ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" faturaItem ");
		hql.append(" where ");
		hql.append(" faturaItem.creditoDebitoARealizar.chavePrimaria = :idCreditoDebito ");
		hql.append(" and faturaItem.fatura.creditoDebitoSituacao.chavePrimaria != :idSituacaoCancelada ");

		if (idFatura != null && idFatura > 0) {
			hql.append(" and faturaItem.fatura.chavePrimaria = :idFatura ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCreditoDebito", idCredito);

		if (idFatura != null && idFatura > 0) {
			query.setLong("idFatura", idFatura);
		}

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoCancelada = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_CANCELADO));
		query.setLong("idSituacaoCancelada", idSituacaoCancelada);

		return (BigDecimal) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #obterSaldoCreditoDebito
	 * (br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoARealizar)
	 */
	@Override
	public BigDecimal obterSaldoCreditoDebito(CreditoDebitoARealizar creditoDebitoARealizar) throws NegocioException {

		BigDecimal saldo = creditoDebitoARealizar.getValor();
		BigDecimal valorCobrado = this.obterValorCobradoCredito(creditoDebitoARealizar.getChavePrimaria(), null);
		if (valorCobrado != null) {
			saldo = saldo.subtract(valorCobrado);
		}

		return saldo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #inserirCreditoDebitoNegociado
	 * (br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoNegociado)
	 */
	@Override
	public void inserirCreditoDebitoNegociado(CreditoDebitoNegociado creditoDebitoNegociado) throws NegocioException {

		this.inserir(creditoDebitoNegociado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #inserirCreditoDebitoARealizar
	 * (br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoARealizar)
	 */
	@Override
	public void inserirCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) throws NegocioException {

		this.inserir(creditoDebitoARealizar);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #inserirCreditoDebitoDetalhamento
	 * (br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoDetalhamento)
	 */
	@Override
	public void inserirCreditoDebitoDetalhamento(CreditoDebitoDetalhamento creditoDebitoDetalhamento) throws NegocioException {

		this.inserir(creditoDebitoDetalhamento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #registrarLancamentoContabil
	 * (br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoNegociado)
	 */
	@Override
	public void registrarLancamentoContabil(CreditoDebitoNegociado creditoDebitoNegociado, OperacaoContabil operacao, Date dataContabil)
					throws NegocioException, ConcorrenciaException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		Map<String, Object> filtroCreditoDebitoARealizar = new HashMap<>();
		filtroCreditoDebitoARealizar.put("idCreditoDebitoNegociado", creditoDebitoNegociado.getChavePrimaria());
		filtroCreditoDebitoARealizar.put("anoMesCobrancaIsNull", true);
		Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = this.consultarCreditoDebitoARealizar(filtroCreditoDebitoARealizar);

		Collections.sort((List) listaCreditoDebitoARealizar, new Comparator<CreditoDebitoARealizar>(){

			@Override
			public int compare(CreditoDebitoARealizar o1, CreditoDebitoARealizar o2) {

				return Long.valueOf(o1.getChavePrimaria()).compareTo(Long.valueOf(o2.getChavePrimaria()));
			}
		});
		int qtdCreditoDebito = 0;
		Map<Long, BigDecimal> acumularCurtoPrazo = new LinkedHashMap<>();
		Map<Long, BigDecimal> acumularLongoPrazo = new LinkedHashMap<>();
		List<LancamentoItemContabil> listaLancamentoitemContabil = new ArrayList<>();

		int qtdCurtoPrazo = QTD_MES_CONTABILIDADE_CURTO_PRAZO;
		if (listaCreditoDebitoARealizar.size() > QTD_MES_CONTABILIDADE_CURTO_PRAZO && creditoDebitoNegociado.getPontoConsumo() != null) {
			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(creditoDebitoNegociado
							.getPontoConsumo());
			// TKT 3332
			if (contratoPontoConsumo == null) {
				contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(creditoDebitoNegociado
								.getPontoConsumo().getChavePrimaria());
			}
			qtdCurtoPrazo = QTD_MES_CONTABILIDADE_CURTO_PRAZO * contratoPontoConsumo.getPeriodicidade().getQuantidadeCiclo();
		}

		for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebitoARealizar) {
			qtdCreditoDebito++;

			if (qtdCreditoDebito <= qtdCurtoPrazo) {
				Collection<CreditoDebitoDetalhamento> listaCreditoDebitoDetalhamento = this
								.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCreditoDebitoDetalhamento) {
					BigDecimal valorAcumulado = acumularCurtoPrazo.get(creditoDebitoDetalhamento.getLancamentoItemContabil()
									.getChavePrimaria());
					if (valorAcumulado == null) {
						acumularCurtoPrazo.put(creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria(),
										creditoDebitoDetalhamento.getValor());
						listaLancamentoitemContabil.add(creditoDebitoDetalhamento.getLancamentoItemContabil());
					} else {
						valorAcumulado = valorAcumulado.add(creditoDebitoDetalhamento.getValor());
						acumularCurtoPrazo.put(creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria(), valorAcumulado);
					}
				}

			} else {
				Collection<CreditoDebitoDetalhamento> listaCreditoDebitoDetalhamento = this
								.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
				for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCreditoDebitoDetalhamento) {
					BigDecimal valorAcumulado = acumularLongoPrazo.get(creditoDebitoDetalhamento.getLancamentoItemContabil()
									.getChavePrimaria());
					if (valorAcumulado == null) {
						acumularLongoPrazo.put(creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria(),
										creditoDebitoDetalhamento.getValor());
					} else {
						valorAcumulado = valorAcumulado.add(creditoDebitoDetalhamento.getValor());
						acumularLongoPrazo.put(creditoDebitoDetalhamento.getLancamentoItemContabil().getChavePrimaria(), valorAcumulado);
					}
				}
			}

		}

		for (LancamentoItemContabil lancamentoItemContabil : listaLancamentoitemContabil) {
			BigDecimal valorCurtoPrazo = acumularCurtoPrazo.get(lancamentoItemContabil.getChavePrimaria());
			if (valorCurtoPrazo != null) {
				salvarLancamentoContabilVO(creditoDebitoNegociado, valorCurtoPrazo, true, operacao, lancamentoItemContabil, dataContabil);
			}

			BigDecimal valorLongoPrazo = acumularLongoPrazo.get(lancamentoItemContabil.getChavePrimaria());
			if (valorLongoPrazo != null) {
				salvarLancamentoContabilVO(creditoDebitoNegociado, valorLongoPrazo, false, operacao, lancamentoItemContabil, dataContabil);
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #salvarLancamentoContabilVO
	 * (br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoNegociado,
	 * java.math.BigDecimal, boolean,
	 * br.com.ggas.contabil.impl.OperacaoContabil,
	 * br
	 * .com.ggas.faturamento.LancamentoItemContabil
	 * )
	 */
	@Override
	public void salvarLancamentoContabilVO(CreditoDebitoNegociado creditoDebitoNegociado, BigDecimal valor, boolean isCurtoPrazo,
					OperacaoContabil operacao, LancamentoItemContabil lancamentoItemContabil, Date dataContabil) throws NegocioException,
					ConcorrenciaException {

		ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();

		if (dataContabil == null) {
			lancamentoContabilVO.setDataRealizacaoEvento(Calendar.getInstance().getTime());
		} else {
			lancamentoContabilVO.setDataRealizacaoEvento(dataContabil);
		}
		lancamentoContabilVO.setContaBancaria(null);

		lancamentoContabilVO.setSegmento(creditoDebitoNegociado.getSegmento());
		lancamentoContabilVO.setCnpj(creditoDebitoNegociado.getCliente().getCnpj());

		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setLancamentoItemContabil(lancamentoItemContabil);

		String chaveEvento = controladorContabil.obterChaveEventoComercial(creditoDebitoNegociado.getClass(),
						creditoDebitoNegociado.getRubrica(), operacao, isCurtoPrazo);
		lancamentoContabilVO.setEventoComercial(controladorContabil.obterEventoComercial(controladorContabil
						.obterChaveEventoComercial(chaveEvento)));

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVOCurtoPrazo = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVOCurtoPrazo.setCodigoObjeto(creditoDebitoNegociado.getChavePrimaria());
		lancamentoContabilAnaliticoVOCurtoPrazo.setValor(valor);

		lancamentoContabilVO.setListaLancamentosContabeisAnaliticos(new ArrayList<LancamentoContabilAnaliticoVO>());
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVOCurtoPrazo);

		lancamentoContabilVO.setDadosAuditoria(creditoDebitoNegociado.getDadosAuditoria());

		// ------------ popular conta Auxiliar e historico contabil
		List<Object> listaObjetos = new ArrayList<>();
		listaObjetos.add(creditoDebitoNegociado.getCliente());
		listaObjetos.add(creditoDebitoNegociado);
		controladorContabil.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
		// ---------------------------------------------------

		// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
		Map<String, Object> filtro = controladorContabil.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
		EventoComercialLancamento eventoComercialLancamento = controladorContabil.consultarEventoComercialLancamentoPorFiltro(filtro);
		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		// ----------------------------------------------------------------------------------------------------------------------------

		controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #registrarLancamentoContabil
	 * (br.com.ggas.faturamento.creditodebito.
	 * registrarLancamentoContabilTransferenciaLongoCurtoPrazo
	 * )
	 */
	// @SuppressWarnings("unchecked")
	// FIXME: refatorar tratando o ponto de consumo nulo e o contrato ponto consumo nulo sem replicação
	@Override
	public void registrarLancamentoContabilTransferenciaLongoCurtoPrazo(CreditoDebitoARealizar creditoDebitoARealizar,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		if (creditoDebitoARealizar.getCreditoDebitoNegociado().getPontoConsumo() != null) {
			ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(creditoDebitoARealizar
							.getCreditoDebitoNegociado().getPontoConsumo());
			if (contratoPontoConsumo == null) {
				contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(creditoDebitoARealizar
								.getCreditoDebitoNegociado().getPontoConsumo().getChavePrimaria());
			}
			if (contratoPontoConsumo != null) {
				int qtdCurtoPrazo = QTD_MES_CONTABILIDADE_CURTO_PRAZO * contratoPontoConsumo.getPeriodicidade().getQuantidadeCiclo();

				if (creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes() > qtdCurtoPrazo) {
					int proximaPrestacao = creditoDebitoARealizar.getNumeroPrestacao() + qtdCurtoPrazo;
					if (proximaPrestacao <= creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes()) {
						CreditoDebitoARealizar creditoDebitoARealizarProximaPrestacao = this.obterDataPagamentoCreditoDebitoARealizar(
										creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria(), proximaPrestacao);
						if (creditoDebitoARealizarProximaPrestacao != null) {
							Collection<CreditoDebitoDetalhamento> listaCreditoDebitoDetalhamento = this
											.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizarProximaPrestacao
															.getChavePrimaria());

							for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCreditoDebitoDetalhamento) {
								salvarLancamentoContabilTransferenciaVO(creditoDebitoARealizarProximaPrestacao,
												creditoDebitoDetalhamento.getValor(),
												creditoDebitoDetalhamento.getLancamentoItemContabil(), dadosAuditoria);
							}
						}
					}
				}
			} else {
				ContratoPontoConsumo contratoPontoConsumoRecente = controladorContrato
								.consultarContratoPontoConsumoPorPontoConsumoRecente(creditoDebitoARealizar.getCreditoDebitoNegociado()
												.getPontoConsumo().getChavePrimaria());
				int qtdCurtoPrazo = QTD_MES_CONTABILIDADE_CURTO_PRAZO * contratoPontoConsumoRecente.getPeriodicidade().getQuantidadeCiclo();

				if (creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes() > qtdCurtoPrazo) {
					int proximaPrestacao = creditoDebitoARealizar.getNumeroPrestacao() + qtdCurtoPrazo;
					if (proximaPrestacao <= creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes()) {

						for (int i = proximaPrestacao; i <= creditoDebitoARealizar.getCreditoDebitoNegociado()
										.getQuantidadeTotalPrestacoes(); i++) {
							CreditoDebitoARealizar creditoDebitoARealizarProximaPrestacao = this.obterDataPagamentoCreditoDebitoARealizar(
											creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria(), i);
							if (creditoDebitoARealizarProximaPrestacao != null) {

								Collection<CreditoDebitoDetalhamento> listaCreditoDebitoDetalhamento = this
												.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizarProximaPrestacao
																.getChavePrimaria());
								for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCreditoDebitoDetalhamento) {
									salvarLancamentoContabilTransferenciaVO(creditoDebitoARealizarProximaPrestacao,
													creditoDebitoDetalhamento.getValor(),
													creditoDebitoDetalhamento.getLancamentoItemContabil(), dadosAuditoria);
								}

							}
						}
					}
				}
			}
		} else {
			// Crédito/Débito por cliente (sem ponto
			// de consumo)

			int qtdCurtoPrazo = QTD_MES_CONTABILIDADE_CURTO_PRAZO;

			if (creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes() > qtdCurtoPrazo) {
				int proximaPrestacao = creditoDebitoARealizar.getNumeroPrestacao() + qtdCurtoPrazo;
				if (proximaPrestacao <= creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes()) {

					for (int i = proximaPrestacao; i <= creditoDebitoARealizar.getCreditoDebitoNegociado().getQuantidadeTotalPrestacoes(); i++) {
						CreditoDebitoARealizar creditoDebitoARealizarProximaPrestacao = this.obterDataPagamentoCreditoDebitoARealizar(
										creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria(), i);
						if (creditoDebitoARealizarProximaPrestacao != null) {

							Collection<CreditoDebitoDetalhamento> listaCreditoDebitoDetalhamento = this
											.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizarProximaPrestacao
															.getChavePrimaria());
							for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCreditoDebitoDetalhamento) {
								salvarLancamentoContabilTransferenciaVO(creditoDebitoARealizarProximaPrestacao,
												creditoDebitoDetalhamento.getValor(),
												creditoDebitoDetalhamento.getLancamentoItemContabil(), dadosAuditoria);
							}

						}
					}
				}
			}
		}
	}

	/**
	 * Salvar lancamento contabil transferencia vo.
	 *
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param valor
	 *            the valor
	 * @param lancamentoItemContabil
	 *            the lancamento item contabil
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #salvarLancamentoContabilTransferenciaVO
	 * (br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado,
	 * java.math.BigDecimal, boolean,
	 * br.com.ggas.contabil.impl.OperacaoContabil,
	 * br
	 * .com.ggas.faturamento.LancamentoItemContabil
	 * )
	 */
	public void salvarLancamentoContabilTransferenciaVO(CreditoDebitoARealizar creditoDebitoARealizar, BigDecimal valor,
					LancamentoItemContabil lancamentoItemContabil, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		Map<String, Long> parametros = parametrosMontarCodigoChaveEventoComercial();

		ControladorContabil controladorContabil = (ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();

		lancamentoContabilVO.setDataRealizacaoEvento(Calendar.getInstance().getTime());
		lancamentoContabilVO.setContaBancaria(null);

		lancamentoContabilVO.setSegmento(creditoDebitoARealizar.getCreditoDebitoNegociado().getSegmento());
		lancamentoContabilVO.setCnpj(creditoDebitoARealizar.getCreditoDebitoNegociado().getCliente().getCnpj());

		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setLancamentoItemContabil(lancamentoItemContabil);

		String chaveEvento = montarCodigoChaveEventoComercial(creditoDebitoARealizar.getCreditoDebitoNegociado().getRubrica(), parametros);
		lancamentoContabilVO.setEventoComercial(controladorContabil.obterEventoComercial(controladorContabil
						.obterChaveEventoComercial(chaveEvento)));

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVO.setCodigoObjeto(creditoDebitoARealizar.getCreditoDebitoNegociado().getChavePrimaria());
		lancamentoContabilAnaliticoVO.setValor(valor);

		lancamentoContabilVO.setListaLancamentosContabeisAnaliticos(new ArrayList<LancamentoContabilAnaliticoVO>());
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

		lancamentoContabilVO.setDadosAuditoria(dadosAuditoria);

		// ------------ popular conta Auxiliar e historico contabil
		List<Object> listaObjetos = new ArrayList<>();
		listaObjetos.add(creditoDebitoARealizar.getCreditoDebitoNegociado().getCliente());
		listaObjetos.add(creditoDebitoARealizar);
		controladorContabil.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
		// ---------------------------------------------------

		// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
		Map<String, Object> filtro = controladorContabil.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
		EventoComercialLancamento eventoComercialLancamento = controladorContabil.consultarEventoComercialLancamentoPorFiltro(filtro);
		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		// ----------------------------------------------------------------------------------------------------------------------------

		controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);
	}

	/**
	 * Monta, a partir dos parâmetros informados,
	 * o código para recuperar a chave do
	 * evento comercial no mapa de eventos.
	 *
	 * @param rubrica
	 *            the rubrica
	 * @param parametros
	 *            the parametros
	 * @return codigoChaveEvento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String montarCodigoChaveEventoComercial(Rubrica rubrica, Map<String, Long> parametros) {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String chaveEvento = null;

		if (rubrica.getFinanciamentoTipo() != null
						&& rubrica.getFinanciamentoTipo().getChavePrimaria() == parametros.get(Constantes.C_TIPO_RUBRICA_PARCELAMENTO)) {
			chaveEvento = "_PARCELAMENTO_DEBITO";
		}

		if (chaveEvento == null) {
			if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == parametros.get(Constantes.C_CREDITO)) {
				chaveEvento = "_CREDITO";
			} else if (rubrica.getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == parametros.get(Constantes.C_DEBITO)) {
				chaveEvento = "_DEBITO";
			}

			if (rubrica.getFinanciamentoTipo() != null
							&& rubrica.getFinanciamentoTipo().getDescricaoAbreviada()
											.equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_PRODUTO_ABREVIADA)) {
				chaveEvento += "_PRODUTO";
			} else if (rubrica.getFinanciamentoTipo() != null
							&& rubrica.getFinanciamentoTipo().getDescricaoAbreviada()
											.equals(FinanciamentoTipo.FINANCIAMENTO_TIPO_DS_SERVICO_ABREVIADA)) {
				chaveEvento += "_SERVICO";
			}
		}

		return "EVENTO_COMERCIAL_TRANSFERENCIA" + chaveEvento + "_LONGO_PARA_CURTO_PRAZO";

	}

	/**
	 * Monta um mapa com os parâmetros usados para
	 * definir o evento comercial de uma fatura.
	 *
	 * @return mapParametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<String, Long> parametrosMontarCodigoChaveEventoComercial() {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Map<String, Long> parametros = new LinkedHashMap<>();

		ServiceLocator.getInstancia().getControladorParametroSistema();

		parametros.put(Constantes.C_TIPO_RUBRICA_PARCELAMENTO, Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PARCELAMENTO)));
		parametros.put(Constantes.C_DEBITO,
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO)));
		parametros.put(Constantes.C_CREDITO,
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO)));

		return parametros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * ControladorCreditoDebito
	 * #listarCreditoDebitoNegociadoPorRecebimento
	 * (java.lang.Long)
	 */
	@Override
	public Collection<Map<String, Object>> listarCreditoDebitoNegociadoPorRecebimento(Long idRecebimento) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Criteria criteria = this.createCriteria(CreditoDebitoNegociado.class);
		criteria.createAlias("rubrica", "rubrica", Criteria.INNER_JOIN);
		criteria.createAlias("financiamentoTipo", "financiamentoTipo", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("recebimento.chavePrimaria", idRecebimento));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.addOrder(Order.asc("rubrica.descricao"));

		Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado = criteria.list();
		List<Map<String, Object>> listaExibicaoCreditoDebito = new ArrayList<>();

		for (CreditoDebitoNegociado creditoDebitoNegociado : listaCreditoDebitoNegociado) {

			Map<String, Object> dados = new HashMap<>();

			dados.put("chavePrimaria", creditoDebitoNegociado.getChavePrimaria());
			dados.put("rubrica", creditoDebitoNegociado.getRubrica().getDescricao());

			if (creditoDebitoNegociado.getCreditoOrigem() != null) {
				dados.put("creditoOrigem", Boolean.TRUE);
			}

			Map<String, Object> filtroCreditoDebitoARealizar = new HashMap<>();
			filtroCreditoDebitoARealizar.put("idCreditoDebitoNegociado", creditoDebitoNegociado.getChavePrimaria());
			Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = this
							.consultarCreditoDebitoARealizar(filtroCreditoDebitoARealizar);

			if (listaCreditoDebitoARealizar != null) {

				ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
								ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);
				String codigoSituacaoPendente = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
				String codigoSituacaoParcialmentePago = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

				CreditoDebitoARealizar creditoDebitoARealizarAnterior = null;
				boolean indicadorMudouSituacao = false;
				boolean indicadorCancelado = false;

				BigDecimal somatorioParcelas = BigDecimal.ZERO;

				for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebitoARealizar) {

					Collection<CreditoDebitoDetalhamento> listaCDDetalhamento = this
									.consultarCreditoDebitoDetalhamentoPeloCDARealizar(creditoDebitoARealizar.getChavePrimaria());
					BigDecimal valoresDetalhamento = BigDecimal.ZERO;

					for (CreditoDebitoDetalhamento creditoDebitoDetalhamento : listaCDDetalhamento) {

						if (creditoDebitoDetalhamento.getValor() != null) {

							valoresDetalhamento = valoresDetalhamento.add(creditoDebitoDetalhamento.getValor());

						}

					}

					creditoDebitoARealizar.setValor(valoresDetalhamento);

					if (creditoDebitoARealizarAnterior == null) {

						creditoDebitoARealizarAnterior = creditoDebitoARealizar;

						if (creditoDebitoARealizar.getMotivoCancelamento() != null) {

							indicadorCancelado = true;

						}

					} else if (creditoDebitoARealizar.getMotivoCancelamento() != null) {

						indicadorCancelado = true;

					} else if (creditoDebitoARealizarAnterior.getCreditoDebitoSituacao().getChavePrimaria() != creditoDebitoARealizar
									.getCreditoDebitoSituacao().getChavePrimaria()) {

						indicadorMudouSituacao = true;

					}

					BigDecimal valorSomado = BigDecimal.ZERO;

					BigDecimal valorCobrado = null;
					if (creditoDebitoARealizar.getCreditoDebitoNegociado().getCreditoOrigem() != null) {

						valorCobrado = this.obterValorCobradoCredito(creditoDebitoARealizar.getChavePrimaria(), null);

					}

					if (creditoDebitoARealizar.getAnoMesCobranca() == null && creditoDebitoARealizar.getCreditoDebitoSituacao().isValido()) {

						if (creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria() == Long
										.parseLong(codigoSituacaoParcialmentePago)) {
							// parcialmente pago
							BigDecimal saldoDevedor = controladorRecebimento
											.obterValorRecebimentoPelaCreditoDebitoARealizar(creditoDebitoARealizar.getChavePrimaria());

							if (saldoDevedor != null) {

								valorSomado = creditoDebitoNegociado.getValorEntrada();
								creditoDebitoNegociado.setValorEntrada(valorSomado.add(creditoDebitoARealizar.getValor().subtract(
												saldoDevedor)));

							} else {

								valorSomado = creditoDebitoNegociado.getValorEntrada();
								creditoDebitoNegociado.setValorEntrada(valorSomado.add(creditoDebitoARealizar.getValor()));

							}
						} else if (creditoDebitoARealizar.getSituacaoPagamento().getChavePrimaria() == Long
										.parseLong(codigoSituacaoPendente)) {

							// pendente
							valorSomado = creditoDebitoNegociado.getValorEntrada();
							creditoDebitoNegociado.setValorEntrada(valorSomado.add(creditoDebitoARealizar.getValor()));

						}
						if (valorCobrado != null) {

							valorCobrado = creditoDebitoNegociado.getValorEntrada().subtract(valorCobrado);
							creditoDebitoNegociado.setValorEntrada(valorCobrado);

						}
					}

					somatorioParcelas = somatorioParcelas.add(creditoDebitoARealizar.getValor());

				}

				dados.put("valor", somatorioParcelas);

				String idCreditoDebitoSituacao = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_CANCELADO);
				ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
				CreditoDebitoSituacao debitoSituacao = controladorCobranca
								.obterCreditoDebitoSituacao(Long.valueOf(idCreditoDebitoSituacao));

				if (indicadorCancelado) {

					dados.put("situacao", SITUACAO_CANCELADO);

				} else if (!indicadorMudouSituacao
								&& creditoDebitoARealizarAnterior != null
								&& creditoDebitoARealizarAnterior.getCreditoDebitoSituacao().getChavePrimaria() == debitoSituacao
												.getChavePrimaria()) {

					dados.put("situacao", SITUACAO_ENCERRADO);

				} else if (!indicadorMudouSituacao && creditoDebitoNegociado.getValorEntrada().compareTo(BigDecimal.ZERO) != 0) {

					dados.put("situacao", SITUACAO_EM_ANDAMENTO);

				} else if (creditoDebitoNegociado.getValorEntrada().compareTo(BigDecimal.ZERO) == 0) {

					dados.put("situacao", SITUACAO_ENCERRADO);

				}

				listaExibicaoCreditoDebito.add(dados);

			}

		}

		return listaExibicaoCreditoDebito;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#atualizarDataAnoMesFaturamento(java.lang.Long, java.util.Date,
	 * br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void atualizarDataAnoMesFaturamento(Long chavePontoConsumo, Date dataInicioCobranca,
					CreditoDebitoNegociado creditoDebitoNegociado, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		// regra para definir a periodicidade
		int ciclo = 1;
		// ciclo atual na contagem
		int cicloMaximo = 0;
		// quantidade máxima
		// de ciclos no
		// mes
		int valorReferenciaFaturamento = 0;
		// ultimo
		// ano
		// mes
		// faturado
		Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = null;
		String referenciaFaturamento = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);
		int quantidadeDias = 0;
		if (dataInicioCobranca != null) {

			Long idPeriodicidadeCobranca = null;
			if (creditoDebitoNegociado.getPeriodicidadeCobranca() != null) {
				idPeriodicidadeCobranca = creditoDebitoNegociado.getPeriodicidadeCobranca().getChavePrimaria();
			}

			String codigoPeriodicidadeContrato = obterPeriodicidadeContratoPontoConsumo(idPeriodicidadeCobranca, chavePontoConsumo);

			valorReferenciaFaturamento = this.obterValorReferenciaFaturamento(dataInicioCobranca, referenciaFaturamento);

			// -----verifica o ciclo -------
			DateTime dataCobranca = new DateTime(dataInicioCobranca);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dataInicioCobranca);
			// FIXME: URGENTE deve ser recuperado da periodicidade
			cicloMaximo = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) / Integer.parseInt(codigoPeriodicidadeContrato);

			int diaBaseCobranca = dataCobranca.getDayOfMonth();
			int basePeriodicidade = Integer.parseInt(codigoPeriodicidadeContrato);

			while (diaBaseCobranca > basePeriodicidade) {
				ciclo = ciclo + 1;
				basePeriodicidade = basePeriodicidade + Integer.parseInt(codigoPeriodicidadeContrato);
			}
			// ----------

			Map<String, Object> filtro = new HashMap<>();
			filtro.put("idCreditoDebitoNegociado", creditoDebitoNegociado.getChavePrimaria());
			listaCreditoDebitoARealizar = consultarCreditoDebitoARealizar(filtro);

			for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebitoARealizar) {
				creditoDebitoARealizar.setAnoMesFaturamento(valorReferenciaFaturamento);
				creditoDebitoARealizar.setNumeroCiclo(ciclo);
				creditoDebitoARealizar.setDadosAuditoria(dadosAuditoria);
				this.atualizar(creditoDebitoARealizar);

				quantidadeDias += creditoDebitoNegociado.getPeriodicidadeCobranca().getQuantidadeDias();
				Map<String, Integer> referenciaCiclo = null;
				if (controladorParametroSistema.obterValorParametroUtilizacaoMultiplosCiclos()) {
					referenciaCiclo = Util.gerarProximaReferenciaCiclo(valorReferenciaFaturamento, ciclo,
							creditoDebitoNegociado.getPeriodicidadeCobranca().getQuantidadeDias(), new DateTime().plusDays(quantidadeDias));
				} else {
					referenciaCiclo = Util.rolarReferenciaCiclo(valorReferenciaFaturamento, ciclo, cicloMaximo);
				}
				ciclo = referenciaCiclo.get("ciclo");
				valorReferenciaFaturamento = referenciaCiclo.get("referencia");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito#fatorCorrecaoIndiceFinanceiro(long, java.util.Date)
	 */
	@Override
	public BigDecimal fatorCorrecaoIndiceFinanceiro(long chavePrimaria, Date dataVencimento) {

		return null;
	}
	
	@Override
	public byte[] relatorioCreditoDebitoRealizar(CreditoDebitoNegociado creditoDebitoNegociado, Collection<CreditoDebitoARealizar> listaCreditoDebitoRealizar) throws GGASException {
		ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
				ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		
		BigDecimal valorCredito = creditoDebitoNegociado.getValorTotal();

		
		if (valorCredito.compareTo(BigDecimal.ZERO) > 0) {

			// Populando documento de cobrança para ser usado na geração do
			// relatório.
			DocumentoCobranca documentoCobranca = (DocumentoCobranca) controladorCobranca.criarDocumentoCobranca();
			documentoCobranca.setCliente(creditoDebitoNegociado.getCliente());
			//documentoCobranca.setDataVencimento(creditoDebitoNegociado.getDataVencimento());
			documentoCobranca.setDadosAuditoria(creditoDebitoNegociado.getDadosAuditoria());
			documentoCobranca.setDataEmissao(Calendar.getInstance().getTime());
			documentoCobranca.setValorTotal(valorCredito.setScale(DUAS_CASAS_DECIMAIS, BigDecimal.ROUND_HALF_UP));

			for (CreditoDebitoARealizar creditoDebitoRealizar : listaCreditoDebitoRealizar) {
				DocumentoCobrancaItem documentoCobrancaItem = (DocumentoCobrancaItem) controladorCobranca
						.criarDocumentoCobrancaItem();
				if (creditoDebitoNegociado.getPontoConsumo() != null) {
					documentoCobrancaItem.setPontoConsumo(creditoDebitoNegociado.getPontoConsumo());
				}

				documentoCobrancaItem.setCreditoDebitoARealizar(creditoDebitoRealizar);

				documentoCobrancaItem.setValor(creditoDebitoNegociado.getValorTotal());

				documentoCobranca.getItens().add(documentoCobrancaItem);
			}

			ControladorDocumentoImpressaoLayout controladorDocumentoImpressaoLayout = ServiceLocator.getInstancia()
							.getControladorDocumentoImpressaoLayout();

			String nomeRelatorio = controladorDocumentoImpressaoLayout
							.obterDocumentoImpressaoLayoutPorConstante(Constantes.C_RELATORIO_NOTA_CREDITO_REALIZAR);

			if (!StringUtils.isEmpty(nomeRelatorio)) {
				return controladorCobranca.gerarRelatorioCreditoDebitoRealizar(documentoCobranca, nomeRelatorio,
								Boolean.FALSE, Boolean.FALSE, creditoDebitoNegociado);
			} else {
				throw new NegocioException(Constantes.ERRO_DOCUMENTO_LAYOUT_NAO_CADASTRADO, true);
			}

		}
		return null;
	}

}
