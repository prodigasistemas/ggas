/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.creditodebito;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.parcelamento.DadosGeraisParcelas;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Crédito e Débito. 
 *
 */
public interface ControladorCreditoDebito extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CREDITO_DEBITO = "controladorCreditoDebito";

	String ERRO_NEGOCIO_NUMERO_PARCELAS_MAIOR_RUBRICA = "ERRO_NEGOCIO_NUMERO_PARCELAS_MAIOR_RUBRICA";

	static final int QTD_MES_CONTABILIDADE_CURTO_PRAZO = 12;

	/**
	 * Criar credito debito detalhamento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCreditoDebitoDetalhamento();

	/**
	 * Criar credito debito negociado.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCreditoDebitoNegociado();

	/**
	 * Criar credito debito a realizar.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCreditoDebitoARealizar();

	/**
	 * Criar credito origem.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCreditoOrigem();

	/**
	 * Método responsável por consultar os
	 * créditos/débitos a realizar através do
	 * filtro informado.
	 * 
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Coleção de Créditos/Débtos a
	 *         realizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoARealizar> consultarCreditosDebitos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * créditos/débitos a realizar para a fatura
	 * de encerramento.
	 * 
	 * @param filtro
	 *            Mapa com os parâmetros para a
	 *            consulta.
	 * @return Coleção de Créditos/Débtos a
	 *         realizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoARealizar> consultarCreditosDebitosParaFaturaEncerramento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * créditos/débitos a realizar através das
	 * chaves informadas.
	 * 
	 * @param chavesCreditoDebito
	 *            Chaves primárias do crédito
	 *            débito a realizar
	 * @return Coleção de Créditos/Débtos a
	 *         realizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoARealizar> consultarCreditosDebitosPorChaves(Object[] chavesCreditoDebito) throws NegocioException;

	/**
	 * Retorna todas os CreditoDebitoARealizar que
	 * pertencem a um Parcelamento.
	 * 
	 * @param idParcelamento
	 *            chave do Parcelamento
	 * @return Lista dos CreditoDebitoARealizar
	 *         que pertencem ao parcelamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CreditoDebitoARealizar> consultarCreditoDebitoARealizarPorParcelamento(Long idParcelamento) throws NegocioException;

	/**
	 * Método responsável por consultar crédito e
	 * débitos pela chave primária e pela chave
	 * primária do creditoDebitoPrincipal.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos crédito e
	 *            débitos e do
	 *            creditoDebitoPrincipal.
	 * @return Uma coleção de
	 *         CreditoDebitoARealizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoARealizar> consultarCreditosDebitos(Object[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por obter os crédito e
	 * débitos parcelados pela chave primária do
	 * creditoDebitoPrincipal.
	 * 
	 * @param chavePrincipal
	 *            Chave primária do
	 *            creditoDebitoPrincipal.
	 * @param numerosPrestacoesCobradas
	 *            the numeros prestacoes cobradas
	 * @return Uma coleção de
	 *         CreditoDebitoARealizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoARealizar> consultarCreditosDebitosParcelados(Long chavePrincipal, String[] numerosPrestacoesCobradas)
					throws NegocioException;

	/**
	 * Método responsável por obter um Credito
	 * Débito Negociado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the credito debito negociado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CreditoDebitoNegociado obterCreditoDebitoNegociado(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um Credito
	 * Débito Negociado.
	 * 
	 * @param chavePrimaria
	 *            Chave primária do
	 *            CreditoDebitoNegociado.
	 * @param propriedadesLazy
	 *            Propriedades que serão
	 *            carregadas.
	 * @return Um CreditoDebitoNegociado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CreditoDebitoNegociado obterCreditoDebitoNegociado(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter um Credito
	 * Débito Detalhamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the credito debito detalhamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CreditoDebitoDetalhamento obterCreditoDebitoDetalhamento(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter os
	 * creditoDebitoARealizar que ainda não foram
	 * cobrados pela chave primária do
	 * creditoDebitoNegociado.
	 * 
	 * @param idCreditoDebitoNegociado
	 *            Chave primária do
	 *            creditoDebitoNegociado.
	 * @return A quantidade de
	 *         creditoDebitoARealizar não cobrados
	 *         de um parcelamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer obterQtdCreditoDebitoCobrados(Long idCreditoDebitoNegociado) throws NegocioException;

	/**
	 * Método responsável por obter o
	 * creditoDebitoDetalhamento pela chave
	 * primária do creditoDebitoARealizar.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            Chave primária do
	 *            creditoDebitoARealizar.
	 * @return Uma coleção de
	 *         CreditoDebitoDetalhamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoDetalhamento> obterCDDetalhamentoPeloCDARealizar(Long idCreditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por obter uma coleçao de
	 * credito debito detalhamento pelo credito
	 * debito a realizar.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            the id credito debito a realizar
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CreditoDebitoDetalhamento> consultarCreditoDebitoDetalhamentoPeloCDARealizar(Long idCreditoDebitoARealizar)
					throws NegocioException;

	/**
	 * Método responsável por montar as parcelas
	 * do detalhamento do credito debito a
	 * realizar.
	 * 
	 * @param idCreditoDebitoNegociado
	 *            the id credito debito negociado
	 * @return the dados gerais parcelas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DadosGeraisParcelas montarParcelasDetalhamento(Long idCreditoDebitoNegociado) throws NegocioException;

	/**
	 * Método responsável por consultar
	 * CreditoDebitoDetalhamento.
	 * 
	 * @param filtro
	 *            Filtro com os parâmetros da
	 *            pesquisa.
	 * @return Uma coleção de
	 *         CreditoDebitoDetalhamento..
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoDetalhamento> consultarCreditoDebitoDetalhamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as Origens de
	 * Crédito.
	 * 
	 * @return Uma coleção de
	 *         CreditoDebitoARealizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoOrigem> listarCreditoOrigem() throws NegocioException;

	/**
	 * Método responsável por retornar uma origem
	 * de crédito pelo id.
	 * 
	 * @param idCreditoOrigem
	 *            the id credito origem
	 * @return the credito origem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CreditoOrigem obterCreditoOrigem(Long idCreditoOrigem) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * situações de crédito débito.
	 * 
	 * @return Uma coleção de
	 *         CreditoDebitoSituacao.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoSituacao> listarCreditoDebitoSituacao() throws NegocioException;

	/**
	 * Listar credito debito situacao valida.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CreditoDebitoSituacao> listarCreditoDebitoSituacaoValida() throws NegocioException;

	/**
	 * Método responsável por gerar as parcelas de
	 * crédito de débito.
	 * 
	 * @param dados
	 *            Dados da tela para a geração das
	 *            parcelas.
	 * @param indicadorDetalhamento
	 *            the indicador detalhamento
	 * @return DadosGeraisParcelas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	DadosGeraisParcelas gerarParcelasCreditoDebito(Map<String, Object> dados, boolean indicadorDetalhamento) throws NegocioException,
					FormatoInvalidoException;

	/**
	 * Método responsável por persistir as
	 * parcelas de um crédito/débito a realizar.
	 * 
	 * @param dados
	 *            the dados
	 * @param dadosGerais
	 *            the dados gerais
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException 
	 */
	byte[] salvarCreditoDebitoARealizar(Map<String, Object> dados, DadosGeraisParcelas dadosGerais) throws NegocioException,
					FormatoInvalidoException, ConcorrenciaException, GGASException;

	/**
	 * Método responsável por cancelar créditos e
	 * débitos.
	 * 
	 * @param chavesCreditoDebito
	 *            Chave primária dos
	 *            créditos/débitos a serem
	 *            cancelados.
	 * @param idMotivoCancelamento
	 *            Chave primária do motivo de
	 *            cancelamento da fatura.
	 * @param dadosAuditoria
	 *            Dados de auditoria.
	 * @throws NegocioException
	 *             Caso ocorra algume erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void cancelarLancamentoCreditoDebito(Long[] chavesCreditoDebito, Long idMotivoCancelamento, DadosAuditoria dadosAuditoria)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por popular a entidade
	 * de crédito débito negociado.
	 * 
	 * @param pontoConsumo
	 *            Ponto de Consumo
	 * @param cliente
	 *            Cliente
	 * @param rubrica
	 *            Rubrica
	 * @param valor
	 *            Valor
	 * @param dadosAuditoria
	 *            Dados Auditoria
	 * @param creditoOrigem
	 *            Crédito Origem
	 * @return the credito debito negociado
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CreditoDebitoNegociado popularCreditoDebitoNegociado(PontoConsumo pontoConsumo, Cliente cliente, Rubrica rubrica, BigDecimal valor,
					DadosAuditoria dadosAuditoria, String creditoOrigem) throws NegocioException;

	/**
	 * Método responsável por popular a entidade
	 * de crédito débito a realizar.
	 * 
	 * @param documentoCobrancaItem
	 *            the documento cobranca item
	 * @param faturaGeral
	 *            the fatura geral
	 * @param valor
	 *            the valor
	 * @param debitoNegociado
	 *            the debito negociado
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the credito debito a realizar
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CreditoDebitoARealizar popularCreditoDebitoARealizar(DocumentoCobrancaItem documentoCobrancaItem, FaturaGeral faturaGeral,
					BigDecimal valor, CreditoDebitoNegociado debitoNegociado, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por popular a entidade
	 * de crédito débito detalhamento.
	 * 
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param rubrica
	 *            the rubrica
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the credito debito detalhamento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CreditoDebitoDetalhamento popularCreditoDebitoDetalhamento(CreditoDebitoARealizar creditoDebitoARealizar, Rubrica rubrica,
					DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * CreditoDebitoNegociado.
	 * 
	 * @param filtro
	 *            Filtro da pesquisa.
	 * @return Uma coleção de
	 *         CreditoDebitoNegociado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoNegociado> consultarCreditoDebitoNegociado(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * CreditoDebitoARealizar.
	 * 
	 * @param filtro
	 *            Filtro da pesquisa.
	 * @return Uma coleção de
	 *         CreditoDebitoARealizar.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CreditoDebitoARealizar> consultarCreditoDebitoARealizar(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter o o saldo
	 * devedor de um CreditoDebitoNegociado.
	 * 
	 * @param idCreditoDebitoNegociado
	 *            Chave primária do
	 *            CreditoDebitNegociado.
	 * @return O valor do saldo devedor do
	 *         CreditoDebitoNegociado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal obterSaldoDevedorCreditoDebitoNegociado(Long idCreditoDebitoNegociado) throws NegocioException;

	/**
	 * Consultar pesquisa credito debito.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Map<String, Object>> consultarPesquisaCreditoDebito(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar pesquisa credito debito parcelamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param andamento
	 *            the andamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Map<String, Object>> consultarPesquisaCreditoDebitoParcelamento(Map<String, Object> filtro, boolean andamento)
					throws NegocioException;

	/**
	 * Método responsável por multiplicar a
	 * quantidade e o valor unitário exibidos na
	 * tela de incluir credito/debito a realizar.
	 * 
	 * @param valorQuantidade
	 *            the valor quantidade
	 * @param valorUnitario
	 *            the valor unitario
	 * @return the map
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, String> obterMultiplicacaoValorUnitario(String valorQuantidade, String valorUnitario) throws FormatoInvalidoException,
					NegocioException;

	/**
	 * Método responsável por adicionar os
	 * parâmetros de situação para uma consulta.
	 * 
	 * @param parametros
	 *            Mapa dos parâmetros.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void adicionarParametrosConsultaSituacao(Map<String, Object[]> parametros) throws NegocioException;

	/**
	 * Método responsável por obter o valor total
	 * já abadito de um crédito.
	 * 
	 * @param idCredito
	 *            Chave primária do crédito.
	 * @param idFatura
	 *            Chave primária da fatura.
	 * @return Valor total abatido do crédito.
	 * @throws NegocioException
	 *             Caso ocorra algum eror na
	 *             invocação do método.
	 */
	BigDecimal obterValorCobradoCredito(Long idCredito, Long idFatura) throws NegocioException;

	/**
	 * Método responsável por obter o saldo de um
	 * crédito.
	 * 
	 * @param creditoDebitoARealizar
	 *            O crédito.
	 * @return O saldo do crédito.
	 * @throws NegocioException
	 *             Caso ocorra algum eror na
	 *             invocação do método.
	 */
	BigDecimal obterSaldoCreditoDebito(CreditoDebitoARealizar creditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por inserir um
	 * CreditoDebitoNegociado.
	 * 
	 * @param creditoDebitoNegociado
	 *            the credito debito negociado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirCreditoDebitoNegociado(CreditoDebitoNegociado creditoDebitoNegociado) throws NegocioException;

	/**
	 * Método responsável por inserir um
	 * CreditoDebitoARealizar.
	 * 
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por inserir um
	 * CreditoDebitoDetalhamento.
	 * 
	 * @param creditoDebitoDetalhamento
	 *            the credito debito detalhamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirCreditoDebitoDetalhamento(CreditoDebitoDetalhamento creditoDebitoDetalhamento) throws NegocioException;

	/**
	 * Recupera os créditos/débitos do cliente
	 * relacionados à rubrica dada que possuam
	 * uma das situações informadas.
	 * 
	 * @param idPeriodicidadeCobranca
	 *            the id periodicidade cobranca
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */

	/**
	 * Método responsável por trazer a
	 * periodicidade do contrato de um ponto de
	 * consumo; caso não exista, ele traz
	 * mensal(30) como padrão.
	 * 
	 * @param idPeriodicidadeCobranca
	 * @param idPontoConsumo
	 * @return
	 * @throws NegocioException
	 */
	String obterPeriodicidadeContratoPontoConsumo(Long idPeriodicidadeCobranca, Long idPontoConsumo) throws NegocioException;

	/**
	 * Obter valor referencia faturamento.
	 * 
	 * @param dataInicioCreditoCobranca
	 *            the data inicio credito cobranca
	 * @param referenciaFaturamento
	 *            the referencia faturamento
	 * @return the int
	 * @throws NegocioException
	 *             the negocio exception
	 */
	int obterValorReferenciaFaturamento(Date dataInicioCreditoCobranca, String referenciaFaturamento) throws NegocioException;

	/**
	 * Método responsável por registrar lançamento
	 * contabil de CreditoDebito.
	 * 
	 * @param creditoDebitoNegociado
	 *            o Credito Débito Negociado
	 * @param operacao
	 *            A Operação
	 * @param dataContabil
	 *            the data contabil
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 * @throws ConcorrenciaException
	 *             caso ocorra algum erro
	 */
	void registrarLancamentoContabil(CreditoDebitoNegociado creditoDebitoNegociado, OperacaoContabil operacao, Date dataContabil)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por popular criar um
	 * lançamento contábil para crédito/débito e
	 * inseri-lo.
	 * 
	 * @param creditoDebitoNegociado
	 *            o Crédito/Débito
	 * @param valor
	 *            o valor
	 * @param isCurtoPrazo
	 *            indicador de curto/longo prazo
	 * @param operacao
	 *            a operação
	 * @param lancamentoItemContabil
	 *            o Lnaçamento Contábil
	 * @param dataContabil
	 *            the data contabil
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 * @throws ConcorrenciaException
	 *             caso ocorra algum erro
	 */
	void salvarLancamentoContabilVO(CreditoDebitoNegociado creditoDebitoNegociado, BigDecimal valor, boolean isCurtoPrazo,
					OperacaoContabil operacao, LancamentoItemContabil lancamentoItemContabil, Date dataContabil) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Método responsável por registrar
	 * contabilmente a transferecia de long para
	 * curto prazo do creditoDebitoARealizar.
	 * 
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void registrarLancamentoContabilTransferenciaLongoCurtoPrazo(CreditoDebitoARealizar creditoDebitoARealizar,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por listar todos os
	 * creditos debitos negociado por recebimento.
	 * 
	 * @param idRecebimento
	 *            the id recebimento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Map<String, Object>> listarCreditoDebitoNegociadoPorRecebimento(Long idRecebimento) throws NegocioException;

	/**
	 * Método que retorna a classe CreditoDebitoNegociado
	 *  
	 * @return a Classe CreditoDebitoNegociado
	 */
	Class<?> getClasseEntidadeCreditoDebitoNegociado();

	/**
	 * Atualizar data ano mes faturamento.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param dataInicioCobranca
	 *            the data inicio cobranca
	 * @param creditoDebitoNegociado
	 *            the credito debito negociado
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarDataAnoMesFaturamento(Long chavePontoConsumo, Date dataInicioCobranca, CreditoDebitoNegociado creditoDebitoNegociado,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * Fator Correcao Indice Financeiro
	 * 
	 * @param chavePrimaria
	 * 		the Chave Primaria
	 * @param dataVencimento
	 * 			the Data Vencimento
	 * @return  the BigDecimal
	 */
	BigDecimal fatorCorrecaoIndiceFinanceiro(long chavePrimaria, Date dataVencimento);

	byte[] relatorioCreditoDebitoRealizar(CreditoDebitoNegociado creditoDebitoNegociado, Collection<CreditoDebitoARealizar> listaCreditoDebitoRealizar) throws GGASException;
}
