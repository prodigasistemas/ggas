/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.creditodebito;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a negociação de credito e débito
 *
 */
public interface CreditoDebitoNegociado extends EntidadeNegocio {

	String BEAN_ID_CREDITO_DEBITO_NEGOCIADO = "creditoDebitoNegociado";

	String CREDITO_DEBITO = "Crédito/Débito";

	/**
	 * Gets the valor total.
	 *
	 * @return O valor do crédito/débito
	 *         multiplicado pela quantidade total
	 *         de prestações.
	 */
	BigDecimal getValorTotal();

	/**
	 * Gets the valor entrada.
	 *
	 * @return the valor entrada
	 */
	BigDecimal getValorEntrada();

	/**
	 * Sets the valor entrada.
	 *
	 * @param valorEntrada the new valor entrada
	 */
	void setValorEntrada(BigDecimal valorEntrada);

	/**
	 * Gets the ano mes inclusao.
	 *
	 * @return the ano mes inclusao
	 */
	Integer getAnoMesInclusao();

	/**
	 * Sets the ano mes inclusao.
	 *
	 * @param anoMesInclusao the new ano mes inclusao
	 */
	void setAnoMesInclusao(Integer anoMesInclusao);

	/**
	 * Sets the quantidade total prestacoes.
	 *
	 * @param quantidadeTotalPrestacoes the new quantidade total prestacoes
	 */
	void setQuantidadeTotalPrestacoes(Integer quantidadeTotalPrestacoes);

	/**
	 * Gets the amortizacao.
	 *
	 * @return the amortizacao
	 */
	EntidadeConteudo getAmortizacao();

	/**
	 * Sets the amortizacao.
	 *
	 * @param amortizacao the new amortizacao
	 */
	void setAmortizacao(EntidadeConteudo amortizacao);

	/**
	 * Gets the parcelamento.
	 *
	 * @return the parcelamento
	 */
	Parcelamento getParcelamento();

	/**
	 * Sets the parcelamento.
	 *
	 * @param parcelamento the new parcelamento
	 */
	void setParcelamento(Parcelamento parcelamento);

	/**
	 * Gets the financiamento tipo.
	 *
	 * @return the financiamento tipo
	 */
	FinanciamentoTipo getFinanciamentoTipo();

	/**
	 * Sets the financiamento tipo.
	 *
	 * @param financiamentoTipo the new financiamento tipo
	 */
	void setFinanciamentoTipo(FinanciamentoTipo financiamentoTipo);

	/**
	 * Gets the rubrica.
	 *
	 * @return the rubrica
	 */
	Rubrica getRubrica();

	/**
	 * Sets the rubrica.
	 *
	 * @param rubrica the new rubrica
	 */
	void setRubrica(Rubrica rubrica);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	void setSegmento(Segmento segmento);

	/**
	 * Gets the credito origem.
	 *
	 * @return the credito origem
	 */
	CreditoOrigem getCreditoOrigem();

	/**
	 * Sets the credito origem.
	 *
	 * @param creditoOrigem the new credito origem
	 */
	void setCreditoOrigem(CreditoOrigem creditoOrigem);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(Cliente cliente);

	/**
	 * Gets the ponto consumo.
	 *
	 * @return the ponto consumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * Sets the ponto consumo.
	 *
	 * @param pontoConsumo the new ponto consumo
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Gets the data financiamento.
	 *
	 * @return the data financiamento
	 */
	Date getDataFinanciamento();

	/**
	 * Sets the data financiamento.
	 *
	 * @param financiamento the new data financiamento
	 */
	void setDataFinanciamento(Date financiamento);

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	void setValor(BigDecimal valor);

	/**
	 * Gets the quantidade total prestacoes.
	 *
	 * @return the quantidade total prestacoes
	 */
	Integer getQuantidadeTotalPrestacoes();

	/**
	 * Gets the numero prestacao cobrada.
	 *
	 * @return the numero prestacao cobrada
	 */
	Integer getNumeroPrestacaoCobrada();

	/**
	 * Sets the numero prestacao cobrada.
	 *
	 * @param numeroPrestacaoCobrada the new numero prestacao cobrada
	 */
	void setNumeroPrestacaoCobrada(Integer numeroPrestacaoCobrada);

	/**
	 * Gets the periodicidade juros.
	 *
	 * @return the periodicidade juros
	 */
	EntidadeConteudo getPeriodicidadeJuros();

	/**
	 * Sets the periodicidade juros.
	 *
	 * @param periodicidadeJuros the new periodicidade juros
	 */
	void setPeriodicidadeJuros(EntidadeConteudo periodicidadeJuros);

	/**
	 * Gets the percentual juros.
	 *
	 * @return the percentual juros
	 */
	BigDecimal getPercentualJuros();

	/**
	 * Sets the percentual juros.
	 *
	 * @param percentualJuros the new percentual juros
	 */
	void setPercentualJuros(BigDecimal percentualJuros);

	/**
	 * Gets the valor juros.
	 *
	 * @return the valor juros
	 */
	BigDecimal getValorJuros();

	/**
	 * Sets the valor juros.
	 *
	 * @param valorJuros the new valor juros
	 */
	void setValorJuros(BigDecimal valorJuros);

	/**
	 * Gets the periodicidade cobranca.
	 *
	 * @return the periodicidade cobranca
	 */
	EntidadeConteudo getPeriodicidadeCobranca();

	/**
	 * Sets the periodicidade cobranca.
	 *
	 * @param periodicidadeCobranca the new periodicidade cobranca
	 */
	void setPeriodicidadeCobranca(EntidadeConteudo periodicidadeCobranca);

	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao the new descricao
	 */
	void setDescricao(String descricao);

	/**
	 * Gets the forma cobranca.
	 *
	 * @return the forma cobranca
	 */
	EntidadeConteudo getFormaCobranca();

	/**
	 * Sets the forma cobranca.
	 *
	 * @param formaCobranca the new forma cobranca
	 */
	void setFormaCobranca(EntidadeConteudo formaCobranca);

	/**
	 * Gets the data inicio cobranca.
	 *
	 * @return the data inicio cobranca
	 */
	Date getDataInicioCobranca();

	/**
	 * Sets the data inicio cobranca.
	 *
	 * @param dataInicioCobranca the new data inicio cobranca
	 */
	void setDataInicioCobranca(Date dataInicioCobranca);

	/**
	 * Gets the evento inicio cobranca.
	 *
	 * @return the evento inicio cobranca
	 */
	EntidadeConteudo getEventoInicioCobranca();

	/**
	 * Sets the evento inicio cobranca.
	 *
	 * @param eventoInicioCobranca the new evento inicio cobranca
	 */
	void setEventoInicioCobranca(EntidadeConteudo eventoInicioCobranca);

	/**
	 * Gets the numero dias carencia.
	 *
	 * @return the numero dias carencia
	 */
	Integer getNumeroDiasCarencia();

	/**
	 * Sets the numero dias carencia.
	 *
	 * @param numeroDiasCarencia the new numero dias carencia
	 */
	void setNumeroDiasCarencia(Integer numeroDiasCarencia);

	/**
	 * Gets the melhor dia vencimento.
	 *
	 * @return the melhor dia vencimento
	 */
	Integer getMelhorDiaVencimento();

	/**
	 * Sets the melhor dia vencimento.
	 *
	 * @param melhorDiaVencimento the new melhor dia vencimento
	 */
	void setMelhorDiaVencimento(Integer melhorDiaVencimento);

	/**
	 * Gets the data incicio cobranca formatada.
	 *
	 * @return the data incicio cobranca formatada
	 */
	String getDataIncicioCobrancaFormatada();

	/**
	 * Gets the recebimento.
	 *
	 * @return the recebimento
	 */
	Recebimento getRecebimento();

	/**
	 * Sets the recebimento.
	 *
	 * @param recebimento the new recebimento
	 */
	void setRecebimento(Recebimento recebimento);

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	EntidadeConteudo getStatus();

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	void setStatus(EntidadeConteudo status);

	/**
	 * Gets the ultimo usuario alteracao.
	 *
	 * @return the ultimo usuario alteracao
	 */
	Usuario getUltimoUsuarioAlteracao();

	/**
	 * Sets the ultimo usuario alteracao.
	 *
	 * @param ultimoUsuarioAlteracao the new ultimo usuario alteracao
	 */
	void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao);

}
