/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/05/2015 16:03:20
 @author crsilva
 */

package br.com.ggas.faturamento.creditodebito.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * 
 * Classe responsável pela implementação de métodos 
 * relacionados ao histórico de nota débito / crédito
 *
 */
public class HistoricoNotaDebitoCredito extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO_INVALIDO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Fatura fatura;

	private Usuario usuario;

	private Date dataAnterior;

	private Date dataProrrogacao;

	private String observacoes;

	private static final String DATA_PRORROGACAO = "Data Prorrogação";

	public Fatura getFatura() {

		return fatura;
	}

	public Usuario getUsuario() {

		return usuario;
	}

	public Date getDataAnterior() {
		Date data = null;
		if (this.dataAnterior != null) {
			data = (Date) dataAnterior.clone();
		}
		return data;
	}

	public Date getDataProrrogacao() {
		Date data = null;
		if (this.dataProrrogacao != null) {
			data = (Date) dataProrrogacao.clone();
		}
		return data;
	}

	public String getObservacoes() {

		return observacoes;
	}

	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	public void setDataAnterior(Date dataAnterior) {
		if (dataAnterior != null) {
			this.dataAnterior = (Date) dataAnterior.clone();
		} else {
			this.dataAnterior = null;
		}
	}

	public void setDataProrrogacao(Date dataProrrogacao) {
		if(dataProrrogacao != null) {
			this.dataProrrogacao = (Date) dataProrrogacao.clone();
		} else {
			this.dataProrrogacao = null;
		}
	}

	public void setObservacoes(String observacoes) {

		this.observacoes = observacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Date dataAtual = Util.getDataCorrente(false);

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(dataProrrogacao.compareTo(dataAtual) < 0) {
			camposObrigatoriosAcumulados.append(DATA_PRORROGACAO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.NOTA_DEBITO_CREDITO_DATA_PRORROGACAO_MAIOR_IGUAL_DATA_CORRENTE,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - 0));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE,
					camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO_INVALIDO));
		}
		return erros;
	}
}
