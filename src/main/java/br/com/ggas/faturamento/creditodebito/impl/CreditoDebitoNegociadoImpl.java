/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.creditodebito.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

class CreditoDebitoNegociadoImpl extends EntidadeNegocioImpl implements CreditoDebitoNegociado {

	private static final long serialVersionUID = -5893349973107306692L;

	private Rubrica rubrica;

	private Segmento segmento;

	private CreditoOrigem creditoOrigem;

	private Cliente cliente;

	private PontoConsumo pontoConsumo;

	private Date dataFinanciamento;

	private BigDecimal valor;

	private BigDecimal valorEntrada;

	private Integer quantidadeTotalPrestacoes;

	private Integer numeroPrestacaoCobrada;

	private EntidadeConteudo periodicidadeJuros;

	private BigDecimal percentualJuros;

	private BigDecimal valorJuros;

	private EntidadeConteudo periodicidadeCobranca;

	private String descricao;

	private EntidadeConteudo formaCobranca;

	private Date dataInicioCobranca;

	private Integer anoMesInclusao;

	private EntidadeConteudo eventoInicioCobranca;

	private Integer numeroDiasCarencia;

	private EntidadeConteudo amortizacao;

	private Parcelamento parcelamento;

	private FinanciamentoTipo financiamentoTipo;

	private Integer melhorDiaVencimento;

	private Recebimento recebimento;

	private EntidadeConteudo status;

	private Usuario ultimoUsuarioAlteracao;

	/**
	 * @return the melhorDiaVencimento
	 */
	@Override
	public Integer getMelhorDiaVencimento() {

		return melhorDiaVencimento;
	}

	/**
	 * @param melhorDiaVencimento
	 *            the melhorDiaVencimento to set
	 */
	@Override
	public void setMelhorDiaVencimento(Integer melhorDiaVencimento) {

		this.melhorDiaVencimento = melhorDiaVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado#getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		BigDecimal valorTotal = valor;
		if(quantidadeTotalPrestacoes != null) {
			valorTotal = valor.multiply(BigDecimal.valueOf(quantidadeTotalPrestacoes));
		}
		return valorTotal;
	}

	/**
	 * @return the valorEntrada
	 */
	@Override
	public BigDecimal getValorEntrada() {

		return valorEntrada;
	}

	/**
	 * @param valorEntrada
	 *            the valorEntrada to set
	 */
	@Override
	public void setValorEntrada(BigDecimal valorEntrada) {

		this.valorEntrada = valorEntrada;
	}

	/**
	 * @return the anoMesInclusao
	 */
	@Override
	public Integer getAnoMesInclusao() {

		return anoMesInclusao;
	}

	/**
	 * @param anoMesInclusao
	 *            the anoMesInclusao to set
	 */
	@Override
	public void setAnoMesInclusao(Integer anoMesInclusao) {

		this.anoMesInclusao = anoMesInclusao;
	}

	/**
	 * @param quantidadeTotalPrestacoes
	 *            the quantidadeTotalPrestacoes to
	 *            set
	 */
	@Override
	public void setQuantidadeTotalPrestacoes(Integer quantidadeTotalPrestacoes) {

		this.quantidadeTotalPrestacoes = quantidadeTotalPrestacoes;
	}

	/**
	 * @return the amortizacao
	 */
	@Override
	public EntidadeConteudo getAmortizacao() {

		return amortizacao;
	}

	/**
	 * @param amortizacao
	 *            the amortizacao to set
	 */
	@Override
	public void setAmortizacao(EntidadeConteudo amortizacao) {

		this.amortizacao = amortizacao;
	}

	/**
	 * @return the parcelamento
	 */
	@Override
	public Parcelamento getParcelamento() {

		return parcelamento;
	}

	/**
	 * @param parcelamento
	 *            the parcelamento to set
	 */
	@Override
	public void setParcelamento(Parcelamento parcelamento) {

		this.parcelamento = parcelamento;
	}

	/**
	 * @return the financiamentoTipo
	 */
	@Override
	public FinanciamentoTipo getFinanciamentoTipo() {

		return financiamentoTipo;
	}

	/**
	 * @param financiamentoTipo
	 *            the financiamentoTipo to set
	 */
	@Override
	public void setFinanciamentoTipo(FinanciamentoTipo financiamentoTipo) {

		this.financiamentoTipo = financiamentoTipo;
	}

	/**
	 * @return the rubrica
	 */
	@Override
	public Rubrica getRubrica() {

		return rubrica;
	}

	/**
	 * @param rubrica
	 *            the rubrica to set
	 */
	@Override
	public void setRubrica(Rubrica rubrica) {

		this.rubrica = rubrica;
	}

	/**
	 * @return the segmento
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/**
	 * @return the creditoOrigem
	 */
	@Override
	public CreditoOrigem getCreditoOrigem() {

		return creditoOrigem;
	}

	/**
	 * @param creditoOrigem
	 *            the creditoOrigem to set
	 */
	@Override
	public void setCreditoOrigem(CreditoOrigem creditoOrigem) {

		this.creditoOrigem = creditoOrigem;
	}

	/**
	 * @return the cliente
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return the pontoConsumo
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the dataFinanciamento
	 */
	@Override
	public Date getDataFinanciamento() {

		return dataFinanciamento;
	}

	/**
	 * @param dataFinanciamento
	 *            the dataFinanciamento to set
	 */
	@Override
	public void setDataFinanciamento(Date dataFinanciamento) {

		this.dataFinanciamento = dataFinanciamento;
	}

	/**
	 * @return the valor
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * @return the quantidadeTotalPrestacoes
	 */
	@Override
	public Integer getQuantidadeTotalPrestacoes() {

		return quantidadeTotalPrestacoes;
	}

	/**
	 * @return the numeroPrestacaoCobrada
	 */
	@Override
	public Integer getNumeroPrestacaoCobrada() {

		return numeroPrestacaoCobrada;
	}

	/**
	 * @param numeroPrestacaoCobrada
	 *            the numeroPrestacaoCobrada to
	 *            set
	 */
	@Override
	public void setNumeroPrestacaoCobrada(Integer numeroPrestacaoCobrada) {

		this.numeroPrestacaoCobrada = numeroPrestacaoCobrada;
	}

	/**
	 * @return the periodicidadeJuros
	 */
	@Override
	public EntidadeConteudo getPeriodicidadeJuros() {

		return periodicidadeJuros;
	}

	/**
	 * @param periodicidadeJuros
	 *            the periodicidadeJuros to set
	 */
	@Override
	public void setPeriodicidadeJuros(EntidadeConteudo periodicidadeJuros) {

		this.periodicidadeJuros = periodicidadeJuros;
	}

	/**
	 * @return the percentualJuros
	 */
	@Override
	public BigDecimal getPercentualJuros() {

		return percentualJuros;
	}

	/**
	 * @param percentualJuros
	 *            the percentualJuros to set
	 */
	@Override
	public void setPercentualJuros(BigDecimal percentualJuros) {

		this.percentualJuros = percentualJuros;
	}

	/**
	 * @return the valorJuros
	 */
	@Override
	public BigDecimal getValorJuros() {

		return valorJuros;
	}

	/**
	 * @param valorJuros
	 *            the valorJuros to set
	 */
	@Override
	public void setValorJuros(BigDecimal valorJuros) {

		this.valorJuros = valorJuros;
	}

	/**
	 * @return the periodicidadeCobranca
	 */
	@Override
	public EntidadeConteudo getPeriodicidadeCobranca() {

		return periodicidadeCobranca;
	}

	/**
	 * @param periodicidadeCobranca
	 *            the periodicidadeCobranca to set
	 */
	@Override
	public void setPeriodicidadeCobranca(EntidadeConteudo periodicidadeCobranca) {

		this.periodicidadeCobranca = periodicidadeCobranca;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the formaCobranca
	 */
	@Override
	public EntidadeConteudo getFormaCobranca() {

		return formaCobranca;
	}

	/**
	 * @param formaCobranca
	 *            the formaCobranca to set
	 */
	@Override
	public void setFormaCobranca(EntidadeConteudo formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	/**
	 * @return the dataInicioCobranca
	 */
	@Override
	public Date getDataInicioCobranca() {

		return dataInicioCobranca;
	}

	/**
	 * @param dataInicioCobranca
	 *            the dataInicioCobranca to set
	 */
	@Override
	public void setDataInicioCobranca(Date dataInicioCobranca) {

		this.dataInicioCobranca = dataInicioCobranca;
	}

	/**
	 * @return the eventoInicioCobranca
	 */
	@Override
	public EntidadeConteudo getEventoInicioCobranca() {

		return eventoInicioCobranca;
	}

	/**
	 * @param eventoInicioCobranca
	 *            the eventoInicioCobranca to set
	 */
	@Override
	public void setEventoInicioCobranca(EntidadeConteudo eventoInicioCobranca) {

		this.eventoInicioCobranca = eventoInicioCobranca;
	}

	/**
	 * @return the numeroDiasCarencia
	 */
	@Override
	public Integer getNumeroDiasCarencia() {

		return numeroDiasCarencia;
	}

	/**
	 * @param numeroDiasCarencia
	 *            the numeroDiasCarencia to set
	 */
	@Override
	public void setNumeroDiasCarencia(Integer numeroDiasCarencia) {

		this.numeroDiasCarencia = numeroDiasCarencia;
	}

	@Override
	public Recebimento getRecebimento() {

		return recebimento;
	}

	@Override
	public void setRecebimento(Recebimento recebimento) {

		this.recebimento = recebimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado#getStatus()
	 */
	@Override
	public EntidadeConteudo getStatus() {

		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado
	 * #setStatus(br.com.ggas
	 * .geral.EntidadeConteudo)
	 */
	@Override
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado
	 * #getUltimoUsuarioAlteracao()
	 */
	@Override
	public Usuario getUltimoUsuarioAlteracao() {

		return ultimoUsuarioAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.creditodebito.
	 * CreditoDebitoNegociado
	 * #setUltimoUsuarioAlteracao
	 * (br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao) {

		this.ultimoUsuarioAlteracao = ultimoUsuarioAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getDataIncicioCobrancaFormatada() {

		if(dataInicioCobranca != null) {
			return Util.converterDataParaStringSemHora(dataInicioCobranca, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}

	}
}
