/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.creditodebito.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * 
 * Interface responsável pelos métodos relacionados
 * ao crédito / débito à realizar
 *
 */
public class CreditoDebitoARealizarImpl extends EntidadeNegocioImpl implements CreditoDebitoARealizar {

	private static final int ESCALA = 2;

	private static final long serialVersionUID = 7421898138968392316L;

	private CreditoDebitoNegociado creditoDebitoNegociado;

	private CreditoDebitoSituacao creditoDebitoSituacao;

	private Integer numeroPrestacao;

	private BigDecimal valor;

	private Integer anoMesFaturamento;

	private Integer numeroCiclo;

	private Date dataVencimento;

	private boolean indicadorAntecipacao;

	private Date dataAntecipacao;

	private BigDecimal valorAntecipacao;

	private Devolucao devolucao;

	private Integer anoMesCobranca;

	private BigDecimal quantidade;

	private FaturaGeral origem;

	private EntidadeConteudo situacaoPagamento;

	private EntidadeConteudo motivoCancelamento;

	@Override
	public BigDecimal getValorUnitario() {

		BigDecimal valorUnitario = valor;
		if(quantidade != null) {
			valorUnitario = valorUnitario.divide(quantidade, ESCALA, RoundingMode.HALF_UP);
		}
		return valorUnitario;
	}

	/**
	 * @return the anoMesCobranca
	 */
	@Override
	public Integer getAnoMesCobranca() {

		return anoMesCobranca;
	}

	/**
	 * @param anoMesCobranca
	 *            the anoMesCobranca to set
	 */
	@Override
	public void setAnoMesCobranca(Integer anoMesCobranca) {

		this.anoMesCobranca = anoMesCobranca;
	}

	/**
	 * @return the creditoDebitoNegociado
	 */
	@Override
	public CreditoDebitoNegociado getCreditoDebitoNegociado() {

		return creditoDebitoNegociado;
	}

	/**
	 * @param creditoDebitoNegociado
	 *            the creditoDebitoNegociado to
	 *            set
	 */
	@Override
	public void setCreditoDebitoNegociado(CreditoDebitoNegociado creditoDebitoNegociado) {

		this.creditoDebitoNegociado = creditoDebitoNegociado;
	}

	/**
	 * @return the creditoDebitoSituacao
	 */
	@Override
	public CreditoDebitoSituacao getCreditoDebitoSituacao() {

		return creditoDebitoSituacao;
	}

	/**
	 * @param creditoDebitoSituacao
	 *            the creditoDebitoSituacao to set
	 */
	@Override
	public void setCreditoDebitoSituacao(CreditoDebitoSituacao creditoDebitoSituacao) {

		this.creditoDebitoSituacao = creditoDebitoSituacao;
	}

	/**
	 * @return the numeroPrestacao
	 */
	@Override
	public Integer getNumeroPrestacao() {

		return numeroPrestacao;
	}

	/**
	 * @param numeroPrestacao
	 *            the numeroPrestacao to set
	 */
	@Override
	public void setNumeroPrestacao(Integer numeroPrestacao) {

		this.numeroPrestacao = numeroPrestacao;
	}

	/**
	 * @return the valor
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * @return the anoMesFaturamento
	 */
	@Override
	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}

	/**
	 * @param anoMesFaturamento
	 *            the anoMesFaturamento to set
	 */
	@Override
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	/**
	 * @return the numeroCiclo
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/**
	 * @return the dataVencimento
	 */
	@Override
	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {
		if(dataVencimento != null) {
			this.dataVencimento = (Date) dataVencimento.clone();
		} else {
			this.dataVencimento = null;
		}
	}

	/**
	 * @return the indicadorAntecipacao
	 */
	@Override
	public boolean isIndicadorAntecipacao() {

		return indicadorAntecipacao;
	}

	/**
	 * @param indicadorAntecipacao
	 *            the indicadorAntecipacao to set
	 */
	@Override
	public void setIndicadorAntecipacao(boolean indicadorAntecipacao) {

		this.indicadorAntecipacao = indicadorAntecipacao;
	}

	/**
	 * @return the dataAntecipacao
	 */
	@Override
	public Date getDataAntecipacao() {
		Date data = null;
		if (this.dataAntecipacao != null) {
			data = (Date) dataAntecipacao.clone();
		}
		return data;
	}

	/**
	 * @param dataAntecipacao
	 *            the dataAntecipacao to set
	 */
	@Override
	public void setDataAntecipacao(Date dataAntecipacao) {
		if (dataAntecipacao != null) {
			this.dataAntecipacao = (Date) dataAntecipacao.clone();
		} else {
			this.dataAntecipacao = null;
		}
	}

	/**
	 * @return the valorAntecipacao
	 */
	@Override
	public BigDecimal getValorAntecipacao() {

		return valorAntecipacao;
	}

	/**
	 * @param valorAntecipacao
	 *            the valorAntecipacao to set
	 */
	@Override
	public void setValorAntecipacao(BigDecimal valorAntecipacao) {

		this.valorAntecipacao = valorAntecipacao;
	}

	/**
	 * @return the devolucao
	 */
	@Override
	public Devolucao getDevolucao() {

		return devolucao;
	}

	/**
	 * @param devolucao
	 *            the devolucao to set
	 */
	@Override
	public void setDevolucao(Devolucao devolucao) {

		this.devolucao = devolucao;
	}

	/**
	 * @return the origem
	 */
	@Override
	public FaturaGeral getOrigem() {

		return origem;
	}

	/**
	 * @param origem
	 *            the origem to set
	 */
	@Override
	public void setOrigem(FaturaGeral origem) {

		this.origem = origem;
	}

	/**
	 * @return the situacaoPagamento
	 */
	@Override
	public EntidadeConteudo getSituacaoPagamento() {

		return situacaoPagamento;
	}

	/**
	 * @param situacaoPagamento
	 *            the situacaoPagamento to set
	 */
	@Override
	public void setSituacaoPagamento(EntidadeConteudo situacaoPagamento) {

		this.situacaoPagamento = situacaoPagamento;
	}

	/**
	 * @return the motivoCancelamento
	 */
	@Override
	public EntidadeConteudo getMotivoCancelamento() {

		return motivoCancelamento;
	}

	/**
	 * @param motivoCancelamento
	 *            the motivoCancelamento to set
	 */
	@Override
	public void setMotivoCancelamento(EntidadeConteudo motivoCancelamento) {

		this.motivoCancelamento = motivoCancelamento;
	}

	/**
	 * @return the quantidade
	 */
	@Override
	public BigDecimal getQuantidade() {

		return quantidade;
	}

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	@Override
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getDataVencimentoFormatada() {

		if(dataVencimento != null) {
			return Util.converterDataParaStringSemHora(dataVencimento, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}

	}

}
