/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.transportadora.dominio.Transportadora;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.fatura.FaturaMotivoRevisao;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.rota.Rota;

/**
 * The Interface Fatura.
 */
/**
 * @author ewerttonbravo
 *
 */
/**
 * @author ewerttonbravo
 *
 */
public interface Fatura extends EntidadeNegocio, Cloneable {

	/** The bean id fatura. */
	String BEAN_ID_FATURA = "fatura";

	/** The nota debito credito. */
	String NOTA_DEBITO_CREDITO = "ROTULO_NOTA_DEBITO_CREDITO";

	/** The valor total. */
	String VALOR_TOTAL = "ROTULO_FATURA_VALOR_TOTAL";

	/** The rotulo situacao credito debito. */
	String ROTULO_SITUACAO_CREDITO_DEBITO = "ROTULO_SITUACAO_CREDITO_DEBITO";

	/** The rotulo situacao pagamento. */
	String ROTULO_SITUACAO_PAGAMENTO = "ROTULO_SITUACAO_PAGAMENTO";

	/** The rotulo motivo alteracao. */
	String ROTULO_MOTIVO_ALTERACAO = "ROTULO_MOTIVO_ALTERACAO";

	/** The rotulo data vancimento alteracao. */
	String ROTULO_DATA_VANCIMENTO_ALTERACAO = "ROTULO_DATA_VANCIMENTO_ALTERACAO";

	/** The cliente. */
	String CLIENTE = "ROTULO_FATURA_CLIENTE";

	/** The ponto consumo. */
	String PONTO_CONSUMO = "ROTULO_FATURA_PONTO_CONSUMO";

	/** The tipo nota. */
	String TIPO_NOTA = "ROTULO_FATURA_TIPO_NOTA";

	/** The data vencimento. */
	String DATA_VENCIMENTO = "ROTULO_FATURA_DATA_VENCIMENTO";

	/** The valor. */
	String VALOR = "ROTULO_FATURA_VALOR";

	/** The sucesso alteracao vencimento fatura. */
	String SUCESSO_ALTERACAO_VENCIMENTO_FATURA = "SUCESSO_ALTERACAO_VENCIMENTO_FATURA";

	/** The sucesso coloca revisao fatura. */
	String SUCESSO_COLOCA_REVISAO_FATURA = "SUCESSO_COLOCA_REVISAO_FATURA";

	/** The sucesso retira revisao fatura. */
	String SUCESSO_RETIRA_REVISAO_FATURA = "SUCESSO_RETIRA_REVISAO_FATURA";

	/** The qtde maxima motivo alteracao. */
	int QTDE_MAXIMA_MOTIVO_ALTERACAO = 200;

	/** The observacao nota. */
	String OBSERVACAO_NOTA = "ROTULO_FATURA_OBSERVACAO_NOTA";

	/** The notas debito. */
	String NOTAS_DEBITO = "ROTULO_FATURA_NOTAS_DEBITO";

	/** The notas credito. */
	String NOTAS_CREDITO = "ROTULO_FATURA_NOTAS_CREDITO";

	/** The valor conciliado. */
	String VALOR_CONCILIADO = "ROTULO_FATURA_VALOR_CONCILIADO";
	
	/** The sucesso envio fatura. */
	String SUCESSO_ENVIO_FATURA = "SUCESSO_ENVIO_FATURA";

	/** The sucesso cancelamento fatura. */
	String SUCESSO_CANCELAMENTO_FATURA = "SUCESSO_CANCELAMENTO_FATURA";

	/** The sucesso exclusao fatura. */
	String SUCESSO_EXCLUSAO_FATURA = "SUCESSO_EXCLUSAO_FATURA";

	/** The motivo inclusao. */
	String MOTIVO_INCLUSAO = "ROTULO_FATURA_MOTIVO_INCLUSAO";

	/** The motivo refaturamento. */
	String MOTIVO_REFATURAMENTO = "ROTULO_FATURA_MOTIVO_REFATURAMENTO";

	/** The fatura. */
	String FATURA_ROTULO = "ROTULO_FATURA";

	/** The fatura nota devolucao. */
	String FATURA_NOTA_DEVOLUCAO = "ROTULO_FATURA_NOTA_DEVOLUCAO";
	
	/** Constantes para ser utilizadas no filtro. */
	String INDICADOR_AVISO_CORTE_FATURA = "INDICADOR_AVISO_CORTE_FATURA";

	/**
	 * Gets the ciclo referencia formatado.
	 *
	 * @return the ciclo referencia formatado
	 */
	String getCicloReferenciaFormatado();

	/**
	 * Gets the valor total formatado.
	 *
	 * @return the valor total formatado
	 */
	String getValorTotalFormatado();

	/**
	 * Gets the data emissao formatada.
	 *
	 * @return the data emissao formatada
	 */
	String getDataEmissaoFormatada();

	/**
	 * Gets the data vencimento formatada.
	 *
	 * @return the data vencimento formatada
	 */
	String getDataVencimentoFormatada();

	/**
	 * Gets the fatura geral.
	 *
	 * @return the faturaGeral
	 */
	FaturaGeral getFaturaGeral();

	/**
	 * Sets the fatura geral.
	 *
	 * @param faturaGeral            the faturaGeral to set
	 */
	void setFaturaGeral(FaturaGeral faturaGeral);

	/**
	 * Gets the motivo revisao.
	 *
	 * @return the motivoRevisao
	 */
	FaturaMotivoRevisao getMotivoRevisao();

	/**
	 * Sets the motivo revisao.
	 *
	 * @param motivoRevisao            the motivoRevisao to set
	 */
	void setMotivoRevisao(FaturaMotivoRevisao motivoRevisao);

	/**
	 * Gets the motivo inclusao.
	 *
	 * @return the motivo inclusao
	 */
	EntidadeConteudo getMotivoInclusao();

	/**
	 * Sets the motivo inclusao.
	 *
	 * @param motivoInclusao the new motivo inclusao
	 */
	void setMotivoInclusao(EntidadeConteudo motivoInclusao);

	/**
	 * Gets the tipo documento.
	 *
	 * @return the tipoDocumento
	 */
	TipoDocumento getTipoDocumento();

	/**
	 * Sets the tipo documento.
	 *
	 * @param tipoDocumento            the tipoDocumento to set
	 */
	void setTipoDocumento(TipoDocumento tipoDocumento);

	/**
	 * Gets the ponto consumo.
	 *
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * Metodo que obtem a descricao do ponto de consumo se o ponto de consumo 
	 * nao for nulo, caso seja uma string vazia será retornada.
	 *
	 * @return a descricao do ponto de consumo casa o ponto de consumo nao
	 * 			seja nulo, caso seja uma string vazia será retornada
	 */
	String getDescricaoPontoConsumo();

	/**
	 * Sets the ponto consumo.
	 *
	 * @param pontoConsumo            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * Gets the rota.
	 *
	 * @return the rota
	 */
	Rota getRota();

	/**
	 * Sets the rota.
	 *
	 * @param rota            the rota to set
	 */
	void setRota(Rota rota);

	/**
	 * Gets the credito debito situacao.
	 *
	 * @return the creditoDebitoSituacao
	 */
	CreditoDebitoSituacao getCreditoDebitoSituacao();

	/**
	 * Sets the credito debito situacao.
	 *
	 * @param creditoDebitoSituacao            the creditoDebitoSituacao to set
	 */
	void setCreditoDebitoSituacao(CreditoDebitoSituacao creditoDebitoSituacao);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento            the segmento to set
	 */
	void setSegmento(Segmento segmento);

	/**
	 * Gets the historico consumo.
	 *
	 * @return the historicoConsumo
	 */
	HistoricoConsumo getHistoricoConsumo();

	/**
	 * Sets the historico consumo.
	 *
	 * @param historicoConsumo            the historicoConsumo to set
	 */
	void setHistoricoConsumo(HistoricoConsumo historicoConsumo);

	/**
	 * Gets the historico medicao.
	 *
	 * @return the historicoMedicao
	 */
	HistoricoMedicao getHistoricoMedicao();

	/**
	 * Sets the historico medicao.
	 *
	 * @param historicoMedicao            the historicoMedicao to set
	 */
	void setHistoricoMedicao(HistoricoMedicao historicoMedicao);

	/**
	 * Gets the ano mes referencia.
	 *
	 * @return the anoMesReferencia
	 */
	Integer getAnoMesReferencia();

	/**
	 * Sets the ano mes referencia.
	 *
	 * @param anoMesReferencia            the anoMesReferencia to set
	 */
	void setAnoMesReferencia(Integer anoMesReferencia);

	/**
	 * Gets the numero ciclo.
	 *
	 * @return the numeroCiclo
	 */
	Integer getNumeroCiclo();

	/**
	 * Sets the numero ciclo.
	 *
	 * @param numeroCiclo            the numeroCiclo to set
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * Gets the valor total.
	 *
	 * @return the valorTotal
	 */
	BigDecimal getValorTotal();

	/**
	 * Sets the valor total.
	 *
	 * @param valorTotal            the valorTotal to set
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * Gets the situacao pagamento.
	 *
	 * @return the situacaoPagamento
	 */
	EntidadeConteudo getSituacaoPagamento();

	/**
	 * Sets the situacao pagamento.
	 *
	 * @param situacaoPagamento            the situacaoPagamento to set
	 */
	void setSituacaoPagamento(EntidadeConteudo situacaoPagamento);

	/**
	 * Gets the data emissao.
	 *
	 * @return the dataEmissao
	 */
	Date getDataEmissao();

	/**
	 * Sets the data emissao.
	 *
	 * @param dataEmissao            the dataEmissao to set
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * Gets the data vencimento.
	 *
	 * @return the dataVencimento
	 */
	Date getDataVencimento();

	/**
	 * Metodo que informa se a fatura esta vencida.
	 *
	 * @return true, se a fatura estiver vencida, false caso contrario
	 */
	boolean isVencida();

	/**
	 * Sets the data vencimento.
	 *
	 * @param dataVencimento            the dataVencimento to set
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * Gets the motivo alteracao.
	 *
	 * @return the motivoAlteracao
	 */
	String getMotivoAlteracao();

	/**
	 * Sets the motivo alteracao.
	 *
	 * @param motivoAlteracao            the motivoAlteracao to set
	 */
	void setMotivoAlteracao(String motivoAlteracao);

	/**
	 * Gets the lista fatura item.
	 *
	 * @return the listaFaturaItem
	 */
	Collection<FaturaItem> getListaFaturaItem();

	/**
	 * Sets the lista fatura item.
	 *
	 * @param listaFaturaItem            the listaFaturaItem to set
	 */
	void setListaFaturaItem(Collection<FaturaItem> listaFaturaItem);

	/**
	 * Gets the devolucao.
	 *
	 * @return the devolucao
	 */
	Devolucao getDevolucao();

	/**
	 * Sets the devolucao.
	 *
	 * @param devolucao            the devolucao to set
	 */
	void setDevolucao(Devolucao devolucao);

	/**
	 * Gets the parcelamento.
	 *
	 * @return the parcelamento
	 */
	Parcelamento getParcelamento();

	/**
	 * Sets the parcelamento.
	 *
	 * @param parcelamento            the parcelamento to set
	 */
	void setParcelamento(Parcelamento parcelamento);

	/**
	 * Gets the data revisao.
	 *
	 * @return the dataRevisao
	 */
	Date getDataRevisao();

	/**
	 * Sets the data revisao.
	 *
	 * @param dataRevisao            the dataRevisao to set
	 */

	/**
	 * @param dataRevisao
	 */
	void setDataRevisao(Date dataRevisao);

	/**
	 * Gets the valor conciliado.
	 *
	 * @return the valorConciliado
	 */
	BigDecimal getValorConciliado();

	/**
	 * Sets the valor conciliado.
	 *
	 * @param valorConciliado            the valorConciliado to set
	 */
	void setValorConciliado(BigDecimal valorConciliado);

	/**
	 * Gets the observacao nota.
	 *
	 * @return the observacaoNota
	 */
	String getObservacaoNota();

	/**
	 * Sets the observacao nota.
	 *
	 * @param observacaoNota            the observacaoNota to set
	 */
	void setObservacaoNota(String observacaoNota);

	/**
	 * Gets the motivo cancelamento.
	 *
	 * @return the motivoCancelamento
	 */
	EntidadeConteudo getMotivoCancelamento();

	/**
	 * Sets the motivo cancelamento.
	 *
	 * @param motivoCancelamento            the motivoCancelamento to set
	 */
	void setMotivoCancelamento(EntidadeConteudo motivoCancelamento);

	/**
	 * Gets the motivo refaturamento.
	 *
	 * @return the motivo refaturamento
	 */
	EntidadeConteudo getMotivoRefaturamento();

	/**
	 * Sets the motivo refaturamento.
	 *
	 * @param motivoRefaturamento the new motivo refaturamento
	 */
	void setMotivoRefaturamento(EntidadeConteudo motivoRefaturamento);

	/**
	 * Gets the descricao cancelamento.
	 *
	 * @return the descricaoCancelamento
	 */
	String getDescricaoCancelamento();

	/**
	 * Sets the descricao cancelamento.
	 *
	 * @param descricaoCancelamento            the descricaoCancelamento to set
	 */
	void setDescricaoCancelamento(String descricaoCancelamento);

	/**
	 * Gets the fatura agrupada.
	 *
	 * @return the faturaAgrupada
	 */
	Fatura getFaturaAgrupada();

	/**
	 * Sets the fatura agrupada.
	 *
	 * @param faturaAgrupada the new fatura agrupada
	 */
	void setFaturaAgrupada(Fatura faturaAgrupada);

	/**
	 * Gets the contrato.
	 *
	 * @return the contrato
	 */
	Contrato getContrato();

	/**
	 * Sets the contrato.
	 *
	 * @param contrato the new contrato
	 */
	void setContrato(Contrato contrato);

	/**
	 * Checks if is cobrada.
	 *
	 * @return the cobrada
	 */
	boolean isCobrada();

	/**
	 * Sets the cobrada.
	 *
	 * @param cobrada            the cobrada to set
	 */
	void setCobrada(boolean cobrada);

	/**
	 * método responsável por calcular o saldo
	 * levando em consideracao o valor conciliado.
	 *
	 * @return the valor saldo conciliado
	 */
	BigDecimal getValorSaldoConciliado();

	/**
	 * Gets the contrato atual.
	 *
	 * @return the contratoAtual
	 */
	Contrato getContratoAtual();

	/**
	 * Sets the contrato atual.
	 *
	 * @param contratoAtual            the contratoAtual to set
	 */
	void setContratoAtual(Contrato contratoAtual);

	/**
	 * Checks if is encerramento.
	 * 
	 * @return the encerramento
	 */
	Boolean isEncerramento();

	/**
	 * Sets the encerramento.
	 *
	 * @param encerramento            the encerramento to set
	 */
	void setEncerramento(Boolean encerramento);

	/**
	 * Checks if is registro perda.
	 * 
	 * @return the registroPerda
	 */
	Boolean isRegistroPerda();

	/**
	 * Sets the registro perda.
	 *
	 * @param registroPerda            the registroPerda to set
	 */
	void setRegistroPerda(Boolean registroPerda);

	/**
	 * Sets the natureza operacao cfop.
	 *
	 * @param naturezaOperacaoCFOP the new natureza operacao cfop
	 */
	void setNaturezaOperacaoCFOP(NaturezaOperacaoCFOP naturezaOperacaoCFOP);

	/**
	 * Gets the natureza operacao cfop.
	 *
	 * @return the natureza operacao cfop
	 */
	NaturezaOperacaoCFOP getNaturezaOperacaoCFOP();

	/**
	 * Gets the provisao devedores duvidosos.
	 *
	 * @return the provisao devedores duvidosos
	 */
	Boolean getProvisaoDevedoresDuvidosos();

	/**
	 * Sets the provisao devedores duvidosos.
	 *
	 * @param provisaoDevedoresDuvidosos the new provisao devedores duvidosos
	 */
	void setProvisaoDevedoresDuvidosos(Boolean provisaoDevedoresDuvidosos);

	/**
	 * Gets the fatura avulso.
	 *
	 * @return the fatura avulso
	 */
	Boolean getFaturaAvulso();

	/**
	 * Sets the fatura avulso.
	 *
	 * @param faturaAvulso the new fatura avulso
	 */

	void setFaturaAvulso(Boolean faturaAvulso);

	/**
	 * Gets the data cancelamento.
	 *
	 * @return the data cancelamento
	 */
	Date getDataCancelamento();

	/**
	 * Sets the data cancelamento.
	 *
	 * @param dataCancelamento            the dataCancelamento to set
	 */
	void setDataCancelamento(Date dataCancelamento);

	/**
	 * Gets the dias atraso.
	 *
	 * @return the dias atraso
	 */
	Integer getDiasAtraso();

	/**
	 * Sets the dias atraso.
	 *
	 * @param diasAtraso the new dias atraso
	 */
	void setDiasAtraso(Integer diasAtraso);

	/**
	 * Checks if is indicador nota fiscal devolucao.
	 *
	 * @return true, if is indicador nota fiscal devolucao
	 */
	boolean isIndicadorNotaFiscalDevolucao();

	/**
	 * Sets the indicador nota fiscal devolucao.
	 *
	 * @param indicadorNotaFiscalDevolucao the new indicador nota fiscal devolucao
	 */
	void setIndicadorNotaFiscalDevolucao(boolean indicadorNotaFiscalDevolucao);

	/**
	 * Gets the transportadora.
	 *
	 * @return the transportadora
	 */
	Transportadora getTransportadora();

	/**
	 * Sets the transportadora.
	 *
	 * @param transportadora the new transportadora
	 */
	void setTransportadora(Transportadora transportadora);

	/**
	 * Gets the veiculo.
	 *
	 * @return the veiculo
	 */
	Veiculo getVeiculo();

	/**
	 * Sets the veiculo.
	 *
	 * @param veiculo the new veiculo
	 */
	void setVeiculo(Veiculo veiculo);

	/**
	 * Gets the motorista.
	 *
	 * @return the motorista
	 */
	Motorista getMotorista();

	/**
	 * Sets the motorista.
	 *
	 * @param motorista the new motorista
	 */
	void setMotorista(Motorista motorista);

	/**
	 * Gets the responsavel revisao.
	 *
	 * @return the responsavel revisao
	 */
	Funcionario getResponsavelRevisao();

	/**
	 * Sets the responsavel revisao.
	 *
	 * @param responsavelRevisao the new responsavel revisao
	 */
	void setResponsavelRevisao(Funcionario responsavelRevisao);

	/**
	 * Gets the habilitado cobranca.
	 *
	 * @return the habilitado cobranca
	 */
	Boolean getHabilitadoCobranca();

	/**
	 * Sets the habilitado cobranca.
	 *
	 * @param habilitadoCobranca the new habilitado cobranca
	 */
	void setHabilitadoCobranca(Boolean habilitadoCobranca);

	/**
	 * Gets the id municipio cliente.
	 *
	 * @return the id municipio cliente
	 */
	Long getIdMunicipioCliente();

	/**
	 * Gets the log data informada aplicada.
	 *
	 * @return the logDataInformadaAplicada
	 */
	String getLogDataInformadaAplicada();

	/**
	 * Sets the log data informada aplicada.
	 *
	 * @param logDataInformadaAplicada the new log data informada aplicada
	 */
	void setLogDataInformadaAplicada(String logDataInformadaAplicada);

	/**
	 * Gets the fatura principal.
	 *
	 * @return the fatura principal
	 */
	Fatura getFaturaPrincipal();

	/**
	 * Sets the fatura principal.
	 *
	 * @param faturaPrincipal the new fatura principal
	 */
	void setFaturaPrincipal(Fatura faturaPrincipal);

	/**
	 * Gets the observacao complementar.
	 *
	 * @return the observacao complementar
	 */
	String getObservacaoComplementar();

	/**
	 * Sets the observacao complementar.
	 *
	 * @param observacaoComplementar the new observacao complementar
	 */
	void setObservacaoComplementar(String observacaoComplementar);

	/**
	 * Gets the motivo complemento.
	 *
	 * @return the motivo complemento
	 */
	EntidadeConteudo getMotivoComplemento();

	/**
	 * Sets the motivo complemento.
	 *
	 * @param motivoComplemento the new motivo complemento
	 */
	void setMotivoComplemento(EntidadeConteudo motivoComplemento);

	/**
	 * Cria um objeto clone da fatura
	 * @return O objeto clone
	 */
	Fatura clone();
	
	/**
	 * Verifica se a fatura esta em revisao
	 * @return true se estiver em revisao
	 */
	boolean emRevisao();
	
	
	/**
	 * @return Boolean - Retorna se a proxima fatura vai receber  a notificao de corte.
	 */
	public Boolean getIndicadorNotificacaoFatura();

	/**
	 * @param diasAtraso - Set indicador se a proxima fatura vai receber a notificada de corte.
	 */
	void setIndicadorNotificacaoFatura(Boolean indicadorNotificacaoFatura);
	
	
	/**
	 * @return indicadorExibirAvisoCorte - Retorna se a fatura vai exibir o aviso de corte
	 */
	Boolean getIndicadorExibirAvisoCorte();

	/**
	 * @param indicadorExibirAvisoCorte - indicar para determinar se a fatura ira ou nao exibir o aviso de corte
	 */
	void setIndicadorExibirAvisoCorte(Boolean indicadorExibirAvisoCorte);

	BigDecimal getConsumoMedido();

	void setConsumoMedido(BigDecimal consumoMedido);
}
