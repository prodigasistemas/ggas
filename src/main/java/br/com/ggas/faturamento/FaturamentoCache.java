package br.com.ggas.faturamento;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Cache utilizado no faturar grupo.
 */
public class FaturamentoCache {

	public static final String CONTRATO_PONTO_CONSUMO = "CONTRATO_PONTO_CONSUMO";

	public static final String CLIENTES = "CLIENTES";

	public static final String TARIFAS = "TARIFAS";

	public static final String HISTORICO_CONSUMO = "HISTORICO_CONSUMO";

	public static final String RAMO_ATIVIDADE = "RAMO_ATIVIDADE";

	public static final String TIPO_DOCUMENTO_FATURA = "TIPO_DOCUMENTO_FATURA";

	public static final String LISTA_FATURA_NFE_PENDENTE = "LISTA_FATURA_NFE_PENDENTE";

	private static FaturamentoCache instancia = new FaturamentoCache();

	private Map<String, Object> buckets = new HashMap<>();

	public static FaturamentoCache getInstance() {
		return instancia;
	}

	/**
	 * Remove os pares chave/valor, do cache.
	 */
	public void destroy() {
		buckets = new HashMap<>();
	}

	/**
	 * Adiciona um novo par chave/valor ao cache.
	 * 
	 * @param key   the key
	 * @param value the value
	 * @param value
	 */
	public void put(String key, Object value) {
		buckets.put(key, value);
	}

	/**
	 * Obtém um valor do cache, por chave.
	 * 
	 * @param key the key
	 * @return mapa key
	 */
	public Object get(String key) {
		return buckets.get(key);
	}

	/**
	 * Método responsável por adicionar um item uma coleção que é um valor de um par
	 * chave/valor.
	 * 
	 * @param <T>   - Tipo da Coleção
	 * @param key   - {@link String}
	 * @param value - Item adicionado a Coleção
	 * @return Retorna true se o mapa possui a chave, caso contrário, retorna false.
	 */
	public <T> Boolean add(String key, T value) {

		Boolean hasKey = containsKey(key);
		if (hasKey) {
			Collection<T> col = (Collection<T>) this.get(key);

			col.add(value);
		}
		return hasKey;
	}

	private Boolean containsKey(String key) {
		return buckets.containsKey(key);
	}

	/**
	 * Remove um valor do cache, por chave.
	 * 
	 * @param key the key
	 */
	public void remove(String key) {
		buckets.remove(key);
	}

	/**
	 * Substitui um par chave/valor do cache.
	 * 
	 * @param key   the key
	 * @param value the value
	 * @param value
	 */
	public void over(String key, Object value) {
		buckets.remove(key);
		buckets.put(key, value);
	}

}
