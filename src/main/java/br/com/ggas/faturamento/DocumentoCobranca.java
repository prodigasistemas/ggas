/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * ao documento de cobrança
 *
 */
public interface DocumentoCobranca extends EntidadeNegocio {

	String BEAN_ID_DOCUMENTO_COBRANCA = "documentoCobranca";

	String COBRANCA_DEBITO_SITUACAO = "DOCUMENTO_COBRANCA_COBRANCA_DEBITO_SITUACAO";

	String VALOR_TOTAL = "ROTULO_DOCUMENTO_COBRANCA_VALOR_TOTAL";

	/**
	 * @return the nossoNumero
	 */
	Long getNossoNumero();

	/**
	 * @param nossoNumero
	 *            the nossoNumero to set
	 */
	void setNossoNumero(Long nossoNumero);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the tipoDocumento
	 */
	TipoDocumento getTipoDocumento();

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	void setTipoDocumento(TipoDocumento tipoDocumento);

	/**
	 * @return the cobrancaDebitoSituacao
	 */
	CobrancaDebitoSituacao getCobrancaDebitoSituacao();

	/**
	 * @param cobrancaDebitoSituacao
	 *            the cobrancaDebitoSituacao to
	 *            set
	 */
	void setCobrancaDebitoSituacao(CobrancaDebitoSituacao cobrancaDebitoSituacao);

	/**
	 * @return the dataEmissao
	 */
	Date getDataEmissao();

	/**
	 * @param dataEmissao
	 *            the dataEmissao to set
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * @return the sequencial
	 */
	Integer getSequencial();

	/**
	 * @param sequencial
	 *            the sequencial to set
	 */
	void setSequencial(Integer sequencial);

	/**
	 * @return the sequencialImpressao
	 */
	Integer getSequencialImpressao();

	/**
	 * @param sequencialImpressao
	 *            the sequencialImpressao to set
	 */
	void setSequencialImpressao(Integer sequencialImpressao);

	/**
	 * @return the valorTotal
	 */
	BigDecimal getValorTotal();

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * @return the itens
	 */
	Collection<DocumentoCobrancaItem> getItens();

	/**
	 * @param itens
	 *            the itens to set
	 */
	void setItens(Collection<DocumentoCobrancaItem> itens);

	/**
	 * @return the dataVencimento
	 */
	Date getDataVencimento();

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * @return the valorCredito
	 */
	BigDecimal getValorCredito();

	/**
	 * @param valorCredito
	 *            the valorCredito to set
	 */
	void setValorCredito(BigDecimal valorCredito);

	/**
	 * @return the valorDebito
	 */
	BigDecimal getValorDebito();

	/**
	 * @param valorDebito
	 *            the valorDebito to set
	 */
	void setValorDebito(BigDecimal valorDebito);

}
