/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados 
 * a anormalidade de faturamento
 *
 */
public interface FaturamentoAnormalidade extends EntidadeNegocio {

	String BEAN_ID_FATURAMENTO_ANORMALIDADE = "faturamentoAnormalidade";

	String CODIGO_CLIENTE_INAPTO_FATURAMENTO = "CLIENTE_INAPTO_FATURAMENTO";

	String CODIGO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO = "PONTO_CONSUMO_SEM_CONTRATO_ATIVO";

	String CODIGO_PONTO_CONSUMO_NAO_FATURAVEL_COM_CONSUMO = "PONTO_CONSUMO_NAO_FATURAVEL_COM_CONSUMO";

	String CODIGO_PONTO_CONSUMO_NAO_FATURAVEL = "PONTO_CONSUMO_NAO_FATURAVEL";

	String CODIGO_CONTRATO_NAO_FATURAVEL_COM_CONSUMO = "CONTRATO_NAO_FATURAVEL_COM_CONSUMO";

	String CODIGO_CONTRATO_NAO_FATURAVEL = "CONTRATO_NAO_FATURAVEL";

	String CODIGO_DIAS_FORNECIMENTO_SUPERIOR_LEGISLACAO = "DIAS_FORNECIMENTO_SUPERIOR_LEGISLACAO";

	String CODIGO_DIAS_FORNECIMENTO_INFERIOR_LEGISLACAO = "DIAS_FORNECIMENTO_INFERIOR_LEGISLACAO";

	String CODIGO_PONTO_CONSUMO_POSSUI_BLOQUEIO_MEDICAO = "PONTO_CONSUMO_POSSUI_BLOQUEIO_MEDICAO";

	String CODIGO_BASE_APURACAO_INVALIDA = "BASE_APURACAO_INVALIDA";

	String CODIGO_CONSUMO_NAO_INFORMADO = "CONSUMO_NAO_INFORMADO";

	String CODIGO_CONSUMO_NAO_POSITIVO = "CONSUMO_NAO_POSITIVO";

	String CODIGO_DATA_FIM_NAO_INFORMADA = "DATA_FIM_NAO_INFORMADA";

	String CODIGO_DATA_INICIO_NAO_INFORMADA = "DATA_INICIO_NAO_INFORMADA";

	String CODIGO_DATA_INICIO_POSTERIOR_DATA_FIM = "DATA_INICIO_POSTERIOR_DATA_FIM";

	String CODIGO_DATA_LEITURA_ATUAL_INVALIDA = "DATA_LEITURA_ATUAL_INVALIDA";

	String CODIGO_ESCALA_NAO_INFORMADA = "ESCALA_NAO_INFORMADA";

	String CODIGO_ITEM_FATURA_NAO_INFORMADO = "ITEM_FATURA_NAO_INFORMADO";

	String CODIGO_PERIODO_CALCULO_FORNECIMENTO = "PERIODO_CALCULO_FORNECIMENTO";

	String CODIGO_PONTO_CONSUMO_NAO_INFORMADO = "PONTO_CONSUMO_NAO_INFORMADO";

	String CODIGO_PONTO_CONSUMO_SEM_TARIFA_NO_PERIODO = "PONTO_CONSUMO_SEM_TARIFA_NO_PERIODO";

	String CODIGO_PRECISAO_NAO_INFORMADA = "PRECISAO_NAO_INFORMADA";

	String CODIGO_PRECO_GAS_EXCEPTION = "PRECO_GAS_EXCEPTION";

	String CODIGO_SIMULAR_APURACAO_DIARIA = "SIMULAR_APURACAO_DIARIA";

	String CODIGO_SUBSTITUICAO_TRIBUTARIA = "SUBSTITUICAO_TRIBUTARIA";

	String CODIGO_TARIFA_NAO_INFORMADA = "TARIFA_NAO_INFORMADA";

	String CODIGO_TARIFA_VIGENCIA_FAIXA = "TARIFA_VIGENCIA_FAIXA";

	String CODIGO_TARIFA_VIGENCIA_NAO_INFORMADA = "TARIFA_VIGENCIA_NAO_INFORMADA";

	String CODIGO_TIPO_CALCULO_INVALIDO = "TIPO_CALCULO_INVALIDO";

	String CODIGO_TRIBUTO_MAIOR_VALOR = "TRIBUTO_MAIOR_VALOR";

	String CODIGO_TARIFA_SEM_VIGENCIAS_PERIODO = "TARIFA_SEM_VIGENCIAS_PERIODO";

	String CODIGO_TARIFA_COM_VIGENCIA_PENDENTE_APROVACAO = "CODIGO_TARIFA_COM_VIGENCIA_PENDENTE_APROVACAO";

	String CODIGO_TAMANHO_MAXIMO_FATURAMENTO_INFORMADO = "TAMANHO_MAXIMO_FATURAMENTO_INFORMADO";

	String CODIGO_TAMANHO_MAXIMO_FATURAMENTO_MEDIO = "TAMANHO_MAXIMO_FATURAMENTO_MEDIO";

	String CODIGO_CALCULO_FORNECIMENTO = "CALCULO_FORNECIMENTO";

	String CODIGO_ANOMALIA_GERAL = "ANOMALIA_GERAL";

	String CODIGO_PONTO_CONSUMO_POSSUI_CICLO_NAO_FATURADO = "PONTO_CONSUMO_POSSUI_CICLO_NAO_FATURADO";

	String CODIGO_PONTO_CONSUMO_SEM_CONSUMO = "PONTO_CONSUMO_SEM_CONSUMO";

	String CODIGO_ERRO_GERACAO_NOTA_FISCAL_ELETRONICA = "ERRO_GERACAO_NOTA_FISCAL_ELETRONICA";

	String CODIGO_TIPO_MEDICAO = "TIPO_MEDICAO";

	String CODIGO_CONTRATO_PONTOS_CONSUMO_SEGMENTOS_DIFERENTES = "CONTRATO_PONTOS_CONSUMO_SEGMENTOS_DIFERENTES";

	String CODIGO_SERIE_NAO_CADASTRADA = "SERIE_NAO_CADASTRADA";

	String DIAS_CONSUMO_INFERIOR_PRIMEIRA_MEDICAO = "DIAS_CONSUMO_INFERIOR_PRIMEIRA_MEDICAO";

	String CODIGO_RA_TARIFA_DIVERGE_RA_PONTO_CONSUMO = "RA_TARIFA_DIVERGE_RA_PONTO_CONSUMO";

	String CODIGO_CLIENTE_INTEGRACAO_INEXISTENTE = "CLIENTE_INTEGRACAO_INEXISTENTE";
	
	String CODIGO_VARIACAO_CAMBIAL = "VARIACAO_CAMBIAL";
	
	String CODIGO_VALOR_TOTAL_ELEVADO = "VALOR_TOTAL_ELEVADO";


	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @return descrição com letras minusculas
	 */
	String getDescricaoLower();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the indicadorBloqueiaFaturamento
	 */
	Boolean getIndicadorBloqueiaFaturamento();

	/**
	 * @param indicadorBloqueiaFaturamento
	 *            the indicadorBloqueiaFaturamento
	 *            to set
	 */
	void setIndicadorBloqueiaFaturamento(Boolean indicadorBloqueiaFaturamento);

	/**
	 * @return the indicadorResumoImpressao
	 */
	Boolean getIndicadorResumoImpressao();

	/**
	 * @param indicadorResumoImpressao
	 *            the indicadorResumoImpressao to
	 *            set
	 */
	void setIndicadorResumoImpressao(Boolean indicadorResumoImpressao);

	/**
	 * @return Boolean - Retorna indicador impede faturamento.	
	 */
	Boolean getIndicadorImpedeFaturamento();

	/**
	 * @param indicadorImpedeFaturamento - Set indicador impede faturamento.
	 */
	void setIndicadorImpedeFaturamento(Boolean indicadorImpedeFaturamento);

}
