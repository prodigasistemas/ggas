
package br.com.ggas.faturamento.ecarta.negocio.impl;

import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.ecarta.LoteECarta;
import br.com.ggas.faturamento.ecarta.LoteECartaFaturaImpressao;
import br.com.ggas.faturamento.ecarta.impl.LoteECartaFaturaImpressaoImpl;
import br.com.ggas.faturamento.ecarta.impl.LoteECartaImpl;
import br.com.ggas.faturamento.ecarta.negocio.ControladorLoteECarta;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.hibernate.Query;
import org.jdom.input.DOMBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Classe responsável pela implementação dos métodos relacionados ao controlador LoteECartas
 */
public class ControladorLoteECartaImpl extends ControladorNegocioImpl implements ControladorLoteECarta {

	private static final String SEPARADOR_ARQUIVO = " - ";
	public static final int NUMERO_BYTES_PARA_LER = 1024;

	@Override
	public Long inserirLote(LoteECarta loteECarta) throws NegocioException {
		return super.inserir(loteECarta);
	}

	@Override
	public LoteECarta gerarLote() throws NegocioException {
		LoteECarta lote = new LoteECartaImpl();
		lote.setDataEmissao(Calendar.getInstance().getTime());
		Long id = super.inserir(lote);
		return (LoteECarta) super.obter(id);
	}

	@Override
	public void atualizarLote(LoteECarta loteECarta) throws GGASException {
		super.atualizar(loteECarta);
	}

	@Override
	public LoteECarta obterLoteECarta(long chavePrimaria) throws NegocioException {
		return (LoteECarta) super.obter(chavePrimaria, getClasseEntidade());
	}

	@Override
	public void associarFaturaAoLote(LoteECarta loteECarta, FaturaImpressao faturaImpressao)
			throws NegocioException {
		LoteECartaFaturaImpressao loteECartaFaturaImpressao = new LoteECartaFaturaImpressaoImpl();
		loteECartaFaturaImpressao.setFaturaImpressao(faturaImpressao);
		loteECartaFaturaImpressao.setLoteECarta(loteECarta);
		super.inserir(loteECartaFaturaImpressao);
	}

	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LoteECarta.BEAN_ID_LOTE_ECARTA);
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(LoteECarta.BEAN_ID_LOTE_ECARTA);
	}

	@Override
	public File gerarECartas(List<FaturaImpressao> faturas) throws IOException, GGASException, ParserConfigurationException {
		StringBuilder log = new StringBuilder();
		final ControladorParametroSistema controladorParametroSistema = getBeanControladorParametroSistema();
		String caminhoDiretorioFatura = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_FATURA);
		List<Pair<FaturaImpressao, File>> faturasEmitidas = validarFaturas(faturas, caminhoDiretorioFatura, log);
		if (faturasEmitidas.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NENHUMA_FATURA_ENCONTRADA_ECARTAS, true);
		} else {
			return montarArquivoZip(caminhoDiretorioFatura, faturasEmitidas);
		}
	}

	@Override
	public FaturaImpressao obterFaturaImpressaoPorFatura(Long codigoFatura) {

		FaturaImpressao retorno = null;
		String classeFaturaImpressao = ServiceLocator.getInstancia()
				.getClassPorID(FaturaImpressao.BEAN_ID_FATURA_IMPRESSAO).getSimpleName();

		if (codigoFatura != null) {
			StringBuilder hql = new StringBuilder();

			hql.append(" from ");
			hql.append(classeFaturaImpressao);
			hql.append(" faturaImpressao ");
			hql.append(" inner join fetch faturaImpressao.fatura fatura ");
			hql.append(" where ");
			hql.append(" fatura.chavePrimaria = :idFatura ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("idFatura", codigoFatura);
			retorno = (FaturaImpressao) query.uniqueResult();
		}

		return retorno;
	}

	@Override
	public void gerarECartas(Processo processo, StringBuilder  logProcessamento)
			throws IOException, GGASException, ParserConfigurationException {
		logProcessamento.append("\r\n Iniciando Batch de Processamento de Geração E-Cartas... \r\n\r\n");
		logProcessamento.append("Listando Faturas para Geração...\r\n");
		final ControladorFatura controladorFatura = getBeanControladorFatura();
		final ControladorParametroSistema controladorParametroSistema = getBeanControladorParametroSistema();
		final ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia().getControladorProcessoDocumento();
		String caminhoDiretorioFatura = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_FATURA);

		Collection<FaturaImpressao> listaFaturaImpressao = controladorFatura.filtrarListaFaturaImpressao(processo, logProcessamento);
		List<Pair<FaturaImpressao, File>> faturasEmitidas = validarFaturas(listaFaturaImpressao, caminhoDiretorioFatura, logProcessamento);
		if (faturasEmitidas.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NENHUMA_FATURA_ENCONTRADA_ECARTAS, true);
		} else {
			String paramQtdPorLote = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_FATURA_POR_LOTE_ECARTAS);
			int qtdPorLote;
			if (paramQtdPorLote == null) {
				qtdPorLote = faturasEmitidas.size();
			} else {
				qtdPorLote = Integer.parseInt(paramQtdPorLote);
			}
			int qtdLotes = (int) Math.ceil((double)faturasEmitidas.size() / qtdPorLote);
			for (int i = 0; i < qtdLotes; i++) {
				int indiceInicial = i * qtdPorLote;
				int indiceFinal = (i + 1) * qtdPorLote;
				if (indiceFinal > faturasEmitidas.size()) {
					indiceFinal = faturasEmitidas.size();
				}
				List<Pair<FaturaImpressao, File>> listaLote = faturasEmitidas.subList(indiceInicial, indiceFinal);
				File arquivoZip = montarArquivoZip(caminhoDiretorioFatura, listaLote);
				ProcessoDocumento processoDocumento =
						controladorProcessoDocumento.criaEPopulaProcessoDocumento(processo, arquivoZip.getPath(), arquivoZip.getName());
				controladorProcessoDocumento.inserirProcessoDocumento(processoDocumento);
			}
			logProcessamento.append("\r\n Batch de Geração de E-Cartas concluído com sucesso! \r\n\r\n");
		}
	}

	/**
	 * Valida uma lista de FaturaImpreesao verificando se todos os objetos estão nos conformes para entrar no e-Cartas
	 * @param listaFaturaImpressao A lista de FaturaImpressao que será processada
	 * @param caminhoDiretorioFatura O caminho do diretório de geração dos arquivos de faturas emitidas
	 * @param logProcessamento O log de processamento da operação
	 * @return Um mapa contendo as FaturaImpressao válidas e os arquivos de fatura gerados por elas.
	 */
	private List<Pair<FaturaImpressao, File>> validarFaturas(Collection<FaturaImpressao> listaFaturaImpressao,
			String caminhoDiretorioFatura, StringBuilder  logProcessamento) {
		logProcessamento.append("\n");
		List<Pair<FaturaImpressao, File>> faturasEmitidas = new ArrayList<>();
		listaFaturaImpressao.forEach(fatura -> {
			Long idFatura = fatura.getFatura().getChavePrimaria();
			String pontoConsumo = fatura.getFatura().getDescricaoPontoConsumo();
			if (pontoConsumo == null || pontoConsumo.isEmpty()) {
				pontoConsumo = fatura.getFatura().getPontoConsumo().getDescricao();
			}
			String logFatura = String.format("Ponto de consumo %s - Fatura %d incluida no lote e-Cartas\n", pontoConsumo, idFatura);
			if (fatura.getFatura().getContrato().getIndicadorParticipanteECartas() == null ||
					!fatura.getFatura().getContrato().getIndicadorParticipanteECartas()) {
				logFatura = String.format("Fatura %d não incluida pois o contrato não indica um não participante do e-Cartas\n", idFatura);
			} else {
				File arquivoFatura = gerarArquivoFatura(caminhoDiretorioFatura, fatura);
				if (arquivoFatura.exists()) {
					faturasEmitidas.add(new Pair<>(fatura, arquivoFatura));
				} else {
					logFatura = String.format("Fatura %d não incluida pois o arquivo pdf não foi encontrado\n", idFatura);
				}
			}
			logProcessamento.append(logFatura);
		});
		return faturasEmitidas;
	}

	/**
	 * Recupera o arquivo de fatura.
	 * @param caminhoDiretorioFatura  O caminho para o diretório onde a fatura é salva
	 * @param fatura A fatura de referência
	 * @return O arquivo da fatura
	 */
	private File gerarArquivoFatura(String caminhoDiretorioFatura, FaturaImpressao fatura) {
		String nomeFatura = gerarNomeDoArquivoDeImpressao(fatura.getFatura());
		String caminhoDiretorio, diretorioDocumento;
		caminhoDiretorio = caminhoDiretorioFatura + File.separator
				+ Util.converterDataParaStringSemHoraSemBarra(fatura.getDataGeracao(), Constantes.FORMATO_DATA_US) + File.separator;
		diretorioDocumento = caminhoDiretorio + nomeFatura + Constantes.EXTENSAO_ARQUIVO_PDF;
		return Util.getFile(diretorioDocumento);
	}

	/**
	 * Monta um arquivo zip do e-Cartas à partir de um par FaturaImpressao (Informações da fatura)- File(Arquivo pdf da fatura)
	 * @param caminhoDiretorioFatura O diretório de armazenamento das faturas emitidas
	 * @param faturasEmitidas O mapa de FaturaImpressao - File contendo as faturas que estão validadas para entrar no e-Cartas
	 * @return O arquivo Zip do e-Cartas
	 * @throws ParserConfigurationException the ParserConfigurationException
	 * @throws IOException the IOException
	 * @throws GGASException the GGASException representando os erros de negócio que podem ocorrer no processo
	 */
	@SuppressWarnings("squid:S4797")
	private File montarArquivoZip(String caminhoDiretorioFatura, List<Pair<FaturaImpressao, File>> faturasEmitidas)
			throws ParserConfigurationException, IOException, GGASException {
		LoteECarta loteECarta =  this.gerarLote();
		long numLote = loteECarta.getChavePrimaria();
		String nomeArquivoZip = String.format("e-Carta_%d_%d_servico.zip", LoteECarta.CONSTANTE_ECARTAS, numLote);
		String caminhoDiretorio = caminhoDiretorioFatura + File.separator + numLote ;
		String caminhoZip = caminhoDiretorio + File.separator + nomeArquivoZip;
		File zip = Util.getFile(caminhoDiretorio);
		if (!zip.exists()) {
			zip.mkdirs();
		}
		FileOutputStream fileOutputStream = new FileOutputStream(caminhoZip);
		ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
		for (Pair<FaturaImpressao, File> dados : faturasEmitidas) {
			FaturaImpressao fatura = dados.getFirst();
			File arquivo = dados.getSecond();
			FileInputStream fileInputStream = new FileInputStream(arquivo);
			String nomeAnexoPdf = String.format("e-Carta_%d_%d_1_%d_complementar.pdf", LoteECarta.CONSTANTE_ECARTAS, numLote,
					fatura.getFatura().getChavePrimaria());
			ZipEntry zipEntry = new ZipEntry(nomeAnexoPdf);
			zipOutputStream.putNextEntry(zipEntry);

			byte[] bytes = new byte[NUMERO_BYTES_PARA_LER];
			int length;
			while((length = fileInputStream.read(bytes)) >= 0) {
				zipOutputStream.write(bytes, 0, length);
			}
			this.associarFaturaAoLote(loteECarta, fatura);
			fileInputStream.close();
		}

		final ControladorParametroSistema controladorParametroSistema = getBeanControladorParametroSistema();

		String contratoECartas = controladorParametroSistema.
				obterParametroPorCodigo(Constantes.PARAMETRO_NUMERO_CONTRATO_ECARTAS).getValor();
		String cartaoPostalECartas = controladorParametroSistema.
				obterParametroPorCodigo(Constantes.PARAMETRO_NUMERO_CARTAO_POSTAL_ECARTAS).getValor();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		gerarXmlECartas(byteArrayOutputStream, faturasEmitidas, numLote, contratoECartas, cartaoPostalECartas);

		String nomeArquivoXml = String.format("e-Carta_%d_%d_servico.xml", LoteECarta.CONSTANTE_ECARTAS, numLote);
		ZipEntry zipArquivoXml = new ZipEntry(nomeArquivoXml);
		zipArquivoXml.setSize(byteArrayOutputStream.size());
		zipOutputStream.putNextEntry(zipArquivoXml);
		zipOutputStream.write(byteArrayOutputStream.toByteArray());

		zipOutputStream.close();
		fileOutputStream.close();

		loteECarta.setNomeDocumento(nomeArquivoZip);
		this.atualizarLote(loteECarta);
		return new File(caminhoZip);
	}

	/**
	 * Gera o arquivo xml do e-Cartas
	 * @param arquivoXml O OutputStream onde será gravado o arquivo
	 * @param faturasEmitidas O mapa de FaturaImpressao - File contendo as faturas que estão validadas para entrar no e-Cartas
	 * @param numLote O número do lote atual do e-Cartas
	 * @param numeroContrato O número do contrato e-Cartas
	 * @param cartaoPostalECartas O número do cartão postal e-Cartas
	 * @throws ParserConfigurationException the ParserConfigurationException
	 * @throws IOException the IOException
	 */
	private void gerarXmlECartas(OutputStream arquivoXml, List<Pair<FaturaImpressao, File>> faturasEmitidas, long numLote,
			String numeroContrato, String cartaoPostalECartas) throws ParserConfigurationException, IOException {
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();
		// Raiz
		Element root = document.createElement("eCarta");
		document.appendChild(root);
		// TAg lote
		Element lote = document.createElement("Lote");
		lote.appendChild(document.createTextNode(String.valueOf(numLote)));
		root.appendChild(lote);
		// Tag numero de contrato
		Element contrato = document.createElement("ncontr");
		lote.appendChild(document.createTextNode(numeroContrato));
		root.appendChild(contrato);
		// Tag Cartão postal
		Element cartaoPostal = document.createElement("ncartaopost");
		lote.appendChild(document.createTextNode(cartaoPostalECartas));
		root.appendChild(cartaoPostal);
		// Tag de registro de fatura
		for (Pair<FaturaImpressao, File> dados : faturasEmitidas) {
			FaturaImpressao fatura = dados.getFirst();
			Element reg = document.createElement("reg");
			root.appendChild(reg);

			Element idCliente = document.createElement("cdObjCliente");
			idCliente.appendChild(document.createTextNode(String.valueOf(fatura.getFatura().getChavePrimaria())));
			reg.appendChild(idCliente);

			Element descricaoPonto = document.createElement("xNome");
			descricaoPonto.appendChild(document.createTextNode(fatura.getFatura().getPontoConsumo().getDescricao()));
			reg.appendChild(descricaoPonto);

			Element logradouro = document.createElement("xLgr");
			logradouro.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getEnderecoLogradouro()));
			reg.appendChild(logradouro);

			Element municipio = document.createElement("xMun");
			municipio.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getCep().getNomeMunicipio()));
			reg.appendChild(municipio);

			Element estado = document.createElement("UF");
			estado.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getCep().getUf()));
			reg.appendChild(estado);

			Element numero = document.createElement("nro");
			numero.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getNumero()));
			reg.appendChild(numero);

			Element complemento = document.createElement("xCp1");
			complemento.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getComplemento()));
			reg.appendChild(complemento);

			Element bairro = document.createElement("xBairro");
			bairro.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getCep().getBairro()));
			reg.appendChild(bairro);

			Element pais = document.createElement("xPais");
			pais.appendChild(document.createTextNode("BR"));
			reg.appendChild(pais);

			Element cep = document.createElement("CEP");
			cep.appendChild(document.createTextNode(fatura.getFatura().getCliente().getEnderecoPrincipal().getCep().getCep()));
			reg.appendChild(cep);

			Element idArqComplementar = document.createElement("idArqComplementar");
			idArqComplementar.appendChild(document.createTextNode("S"));
			reg.appendChild(idArqComplementar);

			Element complementar = document.createElement("complementar");
			reg.appendChild(complementar);

			Element arqComplementar = document.createElement("arqCpl");
			String nomeAnexoPdf = String.format("e-Carta_%d_%d_1_%d_complementar.pdf", LoteECarta.CONSTANTE_ECARTAS, numLote,
					fatura.getFatura().getChavePrimaria());
			arqComplementar.appendChild(document.createTextNode(nomeAnexoPdf));
			complementar.appendChild(arqComplementar);

			Element servAdic = document.createElement("ServAdic");
			complementar.appendChild(servAdic);
		}
		DOMBuilder builder = new DOMBuilder();
		org.jdom.Document jdomDoc = builder.build(document);
		XMLOutputter outter = new XMLOutputter();
		outter.setFormat(Format.getPrettyFormat());
		outter.output(jdomDoc, arquivoXml);
	}

	/**
	 * Gera o nome do arquivo de impressão à partir de uma fatura
	 * @param fatura A fatura de referencia para gerar o nome do arquivo
	 * @return O nome do arquivo de impressão referente à fatura
	 */
	private String gerarNomeDoArquivoDeImpressao(Fatura fatura) {
		return Util.removerTodosCaracteresEspeciais(fatura.getContrato().getNumeroFormatado()) + SEPARADOR_ARQUIVO
				+ Util.removerTodosCaracteresEspeciais(fatura.getCliente().getNome()) + SEPARADOR_ARQUIVO
				+ Util.removerTodosCaracteresEspeciais(fatura.getPontoConsumo().getDescricao()) + SEPARADOR_ARQUIVO
				+ Util.removerTodosCaracteresEspeciais(fatura.getDataVencimentoFormatada());
	}

	/**
	 * Obtém o bean do controlador de fatura
	 * @return bean do controlador de fatura
	 */
	private ControladorFatura getBeanControladorFatura() {
		return ServiceLocator.getInstancia().getControladorFatura();
	}

	/**
	 * Obtém o bean do controlador de parâmetro de sistema
	 * @return bean do controlador de parâmetro de sistema
	 */
	private ControladorParametroSistema getBeanControladorParametroSistema() {
		return ServiceLocator.getInstancia().getControladorParametroSistema();
	}

}

