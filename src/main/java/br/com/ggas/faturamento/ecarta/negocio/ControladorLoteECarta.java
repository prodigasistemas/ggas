
package br.com.ggas.faturamento.ecarta.negocio;

import br.com.ggas.batch.Processo;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.ecarta.LoteECarta;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Interface responsável pelos métodos do lote e-carta
 */
public interface ControladorLoteECarta extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_LOTE_ECARTA = "controladorLoteECarta";

	/**
	 * Inserir lote ecarta.
	 * 
	 * @param loteECarta o lote que será inserido
	 * @return o id do lote inserido
	 * @throws NegocioException the negocio exception
	 */
	Long inserirLote(LoteECarta loteECarta) throws NegocioException;

	/**
	 * Gera um novo lote ecarta.
	 *
	 * @return o id do lote geado
	 * @throws NegocioException the negocio exception
	 */
	LoteECarta gerarLote() throws NegocioException;

	/**
	 * Atualizar lote e-carta.
	 * 
	 * @param loteECarta o lote que será atualizado
	 * @throws NegocioException the negocio exception
	 * @throws GGASException the GGASException exception
	 */
	void atualizarLote(LoteECarta loteECarta) throws GGASException;

	/**
	 * Obter.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the entidade negocio
	 * @throws NegocioException the negocio exception
	 */
	LoteECarta obterLoteECarta(long chavePrimaria) throws NegocioException;

	/**
	 * Associa uma fatura impressao ao lote e-carta gerando um registro LoteEcartaFaturaImpressao
	 * @param loteECarta o lote
	 * @param faturaImpressao a fatura que será associada
	 * @throws NegocioException Signals that an I/O exception has occurred.
	 */
	void associarFaturaAoLote(LoteECarta loteECarta, FaturaImpressao faturaImpressao) throws NegocioException;

	/**
	 * Gerar zip com faturas no padrão E-Cartas
	 * @param processo o processo que solicitou a geração
	 * @param logProcessamento O log do processamento
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 * @throws ParserConfigurationException the ParserConfigurationException
	 */
	void gerarECartas(Processo processo, StringBuilder logProcessamento)
			throws IOException, GGASException, ParserConfigurationException;

	/**
	 * Gerar zip com faturas no padrão E-Cartas a partir de uma lista de faturas
	 * @param faturas A lista de faturas que farão parte do envelope
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GGASException the GGAS exception
	 * @throws ParserConfigurationException the ParserConfigurationException
	 * @return O arquivo zip
	 */
	File gerarECartas(List<FaturaImpressao> faturas)
			throws IOException, GGASException, ParserConfigurationException;

	/**
	 * Obtem um objeto FaturaImpressao a partir do id da fatura
	 * @param codigoFatura A chave primeira da fatura
	 * @return O objeto FaturaImpressao
	 */
	FaturaImpressao obterFaturaImpressaoPorFatura(Long codigoFatura);
}
