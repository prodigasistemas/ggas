/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados 
 * a configuração da natureza da operação
 *
 */
public interface NaturezaOperacaoConfiguracao extends EntidadeNegocio {

	String BEAN_ID_NATUREZA_OPERACAO_CONFIGURACAO = "naturezaOperacaoConfiguracao";

	/**
	 * @return NaturezaOperacao - Retorna objeto natureza operação.
	 */
	NaturezaOperacao getNaturezaOperacao();

	/**
	 * @param naturezaOperacao - Set Natureza Operação.
	 */
	void setNaturezaOperacao(NaturezaOperacao naturezaOperacao);

	/**
	 * @return Segmeto - Retorna objeto Segmento.
	 */
	Segmento getSegmento();

	/**
	 * @param segmento - Set Segmento.
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return RamoAtividade - Retorna objeto Ramo atividade.
	 */
	RamoAtividade getRamoAtividade();

	/**
	 * @param ramoAtividade
	 */
	void setRamoAtividade(RamoAtividade ramoAtividade);

	/**
	 * @return Rubrica - Retorna objeto Rúbrica.
	 */
	Rubrica getRubrica();

	/**
	 * @param rubrica - Set Rúbrica.
	 */
	void setRubrica(Rubrica rubrica);

}
