/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import org.apache.log4j.Logger;

/**
 * Classe responsável pela representação da entidade DadosFornecimentoGas
 *
 */
 
public class DadosFornecimentoGasVO implements Serializable, Cloneable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4984138436977467198L;
	
	private static final Logger LOG = Logger.getLogger(DadosFornecimentoGasVO.class);

	private BigDecimal valorTotalGas;

	private BigDecimal valorTotalGasSemSubstituicao;

	private BigDecimal valorTotalGasSemTributo;

	private BigDecimal valorFinalMetroCubicoGas;

	private BigDecimal valorFinalMetroCubicoGasSemSubstituicao;

	private BigDecimal valorFinalMetroCubicoGasSemTributo;

	private Collection<DadosTributoVO> colecaoTributos;

	private BigDecimal valorTotalDescontos;

	private Collection<DadosVigenciaVO> dadosVigencia;

	private DadosTributoVO icmsSubstituto;

	private Long idItemFatura;
	
	private BigDecimal valorTotalComDescontoComImposto;

	/**
	 * @return the valorTotalGasSemSubstituicao
	 */
	public BigDecimal getValorTotalGasSemSubstituicao() {

		return valorTotalGasSemSubstituicao;
	}

	/**
	 * @param valorTotalGasSemSubstituicao
	 *            the valorTotalGasSemSubstituicao
	 *            to set
	 */
	public void setValorTotalGasSemSubstituicao(BigDecimal valorTotalGasSemSubstituicao) {

		this.valorTotalGasSemSubstituicao = valorTotalGasSemSubstituicao;
	}

	/**
	 * @return the
	 *         valorFinalMetroCubicoGasSemSubstituicao
	 */
	public BigDecimal getValorFinalMetroCubicoGasSemSubstituicao() {

		return valorFinalMetroCubicoGasSemSubstituicao;
	}

	/**
	 * @param valorFinalMetroCubicoGasSemSubstituicao
	 *            the
	 *            valorFinalMetroCubicoGasSemSubstituicao
	 *            to set
	 */
	public void setValorFinalMetroCubicoGasSemSubstituicao(BigDecimal valorFinalMetroCubicoGasSemSubstituicao) {

		this.valorFinalMetroCubicoGasSemSubstituicao = valorFinalMetroCubicoGasSemSubstituicao;
	}

	/**
	 * @return the icmsSubstituto
	 */
	public DadosTributoVO getIcmsSubstituto() {

		return icmsSubstituto;
	}

	/**
	 * @param icmsSubstituto
	 *            the icmsSubstituto to set
	 */
	public void setIcmsSubstituto(DadosTributoVO icmsSubstituto) {

		this.icmsSubstituto = icmsSubstituto;
	}

	/**
	 * @return the valorTotalGas
	 */
	public BigDecimal getValorTotalGas() {

		return valorTotalGas;
	}

	/**
	 * @param valorTotalGas
	 *            the valorTotalGas to set
	 */
	public void setValorTotalGas(BigDecimal valorTotalGas) {

		this.valorTotalGas = valorTotalGas;
	}

	/**
	 * @return the valorFinalMetroCubicoGas
	 */
	public BigDecimal getValorFinalMetroCubicoGas() {

		return valorFinalMetroCubicoGas;
	}

	/**
	 * @param valorFinalMetroCubicoGas
	 *            the valorFinalMetroCubicoGas to
	 *            set
	 */
	public void setValorFinalMetroCubicoGas(BigDecimal valorFinalMetroCubicoGas) {

		this.valorFinalMetroCubicoGas = valorFinalMetroCubicoGas;
	}

	/**
	 * @return the colecaoTributos
	 */
	public Collection<DadosTributoVO> getColecaoTributos() {

		return colecaoTributos;
	}

	/**
	 * @param colecaoTributos
	 *            the colecaoTributos to set
	 */
	public void setColecaoTributos(Collection<DadosTributoVO> tributoBase) {

		this.colecaoTributos = tributoBase;
	}

	/**
	 * @return the valorTotalDescontos
	 */
	public BigDecimal getValorTotalDescontos() {

		return valorTotalDescontos;
	}

	/**
	 * @param valorTotalDescontos
	 *            the valorTotalDescontos to set
	 */
	public void setValorTotalDescontos(BigDecimal valorTotalDescontos) {

		this.valorTotalDescontos = valorTotalDescontos;
	}

	/**
	 * @return the dadosVigencia
	 */
	public Collection<DadosVigenciaVO> getDadosVigencia() {

		return dadosVigencia;
	}

	/**
	 * @param dadosVigencia
	 *            the dadosVigencia to set
	 */
	public void setDadosVigencia(Collection<DadosVigenciaVO> dadosVigencia) {

		this.dadosVigencia = dadosVigencia;
	}

	/**
	 * @return the idItemFatura
	 */
	public Long getIdItemFatura() {

		return idItemFatura;
	}

	/**
	 * @param idItemFatura
	 *            the idItemFatura to set
	 */
	public void setIdItemFatura(Long idItemFatura) {

		this.idItemFatura = idItemFatura;
	}

	/**
	 * @return
	 */
	public BigDecimal getValorTotalGasSemTributo() {

		return valorTotalGasSemTributo;
	}

	/**
	 * @param valorTotalGasSemTributo
	 */
	public void setValorTotalGasSemTributo(BigDecimal valorTotalGasSemTributo) {

		this.valorTotalGasSemTributo = valorTotalGasSemTributo;
	}

	/**
	 * @return
	 */
	public BigDecimal getValorFinalMetroCubicoGasSemTributo() {

		return valorFinalMetroCubicoGasSemTributo;
	}

	/**
	 * @param valorFinalMetroCubicoGasSemTributo
	 */
	public void setValorFinalMetroCubicoGasSemTributo(BigDecimal valorFinalMetroCubicoGasSemTributo) {

		this.valorFinalMetroCubicoGasSemTributo = valorFinalMetroCubicoGasSemTributo;
	}

	/**
	 * @return the valorTotalComDescontoComImposto
	 */
	public BigDecimal getValorTotalComDescontoComImposto() {

		return valorTotalComDescontoComImposto;
	}

	/**
	 * @param valorTotalComDescontoComImposto the valorTotalComDescontoComImposto to set
	 */
	public void setValorTotalComDescontoComImposto(BigDecimal valorTotalComDescontoComImposto) {

		this.valorTotalComDescontoComImposto = valorTotalComDescontoComImposto;
	}

	@Override
	public DadosFornecimentoGasVO clone() {
		try {
			return (DadosFornecimentoGasVO) super.clone();
		} catch (CloneNotSupportedException e) {
			LOG.error(e);
			return this;
		}
	}
}
