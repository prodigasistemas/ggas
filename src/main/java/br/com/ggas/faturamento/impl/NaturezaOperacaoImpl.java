/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.faturamento.NaturezaOperacao;
import br.com.ggas.faturamento.NaturezaOperacaoCFOP;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class NaturezaOperacaoImpl extends EntidadeNegocioImpl implements NaturezaOperacao {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2815710520308610900L;

	private String descricao;

	private EntidadeConteudo tipoOperacao;

	private String descricaoExplicativa;

	private Collection<NaturezaOperacaoCFOP> listaNaturezaOperacaoCFOP = new HashSet<NaturezaOperacaoCFOP>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.NaturezaOperacao
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.NaturezaOperacao
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.NaturezaOperacao
	 * #getTipoNota()
	 */
	@Override
	public EntidadeConteudo getTipoOperacao() {

		return tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.NaturezaOperacao
	 * #
	 * setTipoNota(br.com.ggas.geral.EntidadeConteudo
	 * )
	 */
	@Override
	public void setTipoOperacao(EntidadeConteudo tipoOperacao) {

		this.tipoOperacao = tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.NaturezaOperacao
	 * #getListaNaturezaOperacaoCFOP()
	 */
	@Override
	public Collection<NaturezaOperacaoCFOP> getListaNaturezaOperacaoCFOP() {

		return listaNaturezaOperacaoCFOP;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.NaturezaOperacao
	 * #setListaNaturezaOperacaoCFOP
	 * (java.util.Collection)
	 */
	@Override
	public void setListaNaturezaOperacaoCFOP(Collection<NaturezaOperacaoCFOP> listaNaturezaOperacaoCFOP) {

		this.listaNaturezaOperacaoCFOP = listaNaturezaOperacaoCFOP;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.NaturezaOperacao
	 * #getDescricaoExplicativa()
	 */
	@Override
	public String getDescricaoExplicativa() {

		return descricaoExplicativa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.NaturezaOperacao
	 * #setDescricaoExplicativa(java.lang.String)
	 */
	@Override
	public void setDescricaoExplicativa(String descricaoExplicativa) {

		this.descricaoExplicativa = descricaoExplicativa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
