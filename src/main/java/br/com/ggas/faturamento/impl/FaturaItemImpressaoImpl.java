/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelos atributos e  implementação dos métodos relacionados a Impressão do item de Fatura
 *
 */
public class FaturaItemImpressaoImpl extends EntidadeNegocioImpl implements FaturaItemImpressao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
     * 
     */
	private static final long serialVersionUID = -5046935864534251979L;

	private FaturaItem faturaItem;

	private Tarifa tarifa;

	private Date dataInicial;

	private Date dataFinal;

	private Integer quantidadeDias;

	private BigDecimal consumo;

	private BigDecimal valorUnitario;

	private BigDecimal valorTotal;

	private String descricaoDesconto;

	private Boolean indicadorDesconto;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao
	 * #getFaturaItem()
	 */
	@Override
	public FaturaItem getFaturaItem() {

		return faturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setFaturaItem(br
	 * .com.procenge.ggas.faturamento.FaturaItem)
	 */
	@Override
	public void setFaturaItem(FaturaItem faturaItem) {

		this.faturaItem = faturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao
	 * #getTarifa()
	 */
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setTarifa(br.com
	 * .procenge.ggas.faturamento.tarifa.Tarifa)
	 */
	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.FaturaItemImpressao# getDataInicial()
	 */
	@Override
	public Date getDataInicial() {
		Date data = null;
		if (this.dataInicial != null) {
			data = (Date) dataInicial.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setDataInicial(java
	 * .util.Date)
	 */
	@Override
	public void setDataInicial(Date dataInicial) {
		if(dataInicial != null) {
			this.dataInicial = (Date) dataInicial.clone();
		} else {
			this.dataInicial = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.FaturaItemImpressao #getDataFinal()
	 */
	@Override
	public Date getDataFinal() {
		Date data = null;
		if (this.dataFinal != null) {
			data = (Date) dataFinal.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.FaturaItemImpressao# setDataFinal(java
	 * .util.Date)
	 */
	@Override
	public void setDataFinal(Date dataFinal) {
		if (dataFinal != null) {
			this.dataFinal = (Date) dataFinal.clone();
		} else {
			this.dataFinal = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * getQuantidadeDias()
	 */
	@Override
	public Integer getQuantidadeDias() {

		return quantidadeDias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setQuantidadeDias
	 * (int)
	 */
	@Override
	public void setQuantidadeDias(Integer quantidadeDias) {

		this.quantidadeDias = quantidadeDias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao
	 * #getConsumo()
	 */
	@Override
	public BigDecimal getConsumo() {

		return consumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setConsumo(java.
	 * math.BigDecimal)
	 */
	@Override
	public void setConsumo(BigDecimal consumo) {

		this.consumo = consumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * getValorUnitario()
	 */
	@Override
	public BigDecimal getValorUnitario() {

		return valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setValorUnitario
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorUnitario(BigDecimal valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao
	 * #getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setValorTotal(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * getDescricaoDesconto
	 * ()
	 */
	@Override
	public String getDescricaoDesconto() {

		return descricaoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao#
	 * setDescricaoDesconto
	 * (java.lang.String)
	 */
	@Override
	public void setDescricaoDesconto(String descricaoDesconto) {

		this.descricaoDesconto = descricaoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao
	 * #getIndicadorDesconto()
	 */
	@Override
	public Boolean getIndicadorDesconto() {

		return indicadorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.FaturaItemImpressao
	 * #setIndicadorDesconto(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorDesconto(Boolean indicadorDesconto) {

		this.indicadorDesconto = indicadorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados
	 * ()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(faturaItem == null) {
			stringBuilder.append(FATURA_ITEM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataInicial == null) {
			stringBuilder.append(DATA_INICIAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataFinal == null) {
			stringBuilder.append(DATA_FINAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(quantidadeDias == null) {
			stringBuilder.append(QUANTIDADE_DIAS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(consumo == null) {
			stringBuilder.append(CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(valorUnitario == null) {
			stringBuilder.append(VALOR_UNITARIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(valorTotal == null) {
			stringBuilder.append(VALOR_TOTAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
