/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.NaturezaOperacaoCFOP;
import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela implementação dos métodos relacionados ao Documento Fiscal
 *
 */
public class DocumentoFiscalImpl extends EntidadeNegocioImpl implements DocumentoFiscal {

	/**
	 * serialVersionUID
	 */

	private static final String TIPO_OPERACAO_ENTRADA = "Entrada";

	private static final long serialVersionUID = -8059590974963492026L;

	private Long numero;

	private String descricaoSerie;

	private Integer codigoANTT;

	private BigDecimal valorTotal;

	private String nomeCliente;

	private String cpfCpnj;

	private String inscricaoEstadual;

	private String rg;

	private String endereco;

	private String complemento;

	private String bairro;

	private String cep;

	private String municipio;

	private String uf;

	private Date apresentacao;

	private Date dataEmissao;

	private String descricaoPontoConsumo;

	private String mensagem;

	private String placaVeiculo;

	private String ufPlaca;

	private String nomeTransportador;

	private String cpfCNPJTransportadora;

	private String municipioTransportadora;

	private String ufTransportadora;

	private String inscEstadualTransportadora;

	private String inscricaoEstadualSubs;

	private Date horaSaida;

	private String enderecoTransportadora;

	private Fatura fatura;

	private Fatura faturaOrigemDevolucao;

	private EntidadeConteudo tipoOperacao;

	private EntidadeConteudo tipoFaturamento;

	private NaturezaOperacaoCFOP naturezaOperacaoCFOP;

	private Serie serie;

	private EntidadeConteudo tipoEmissaoNfe;

	private EntidadeConteudo statusNfe;

	private String chaveAcesso;

	private String numeroProtocolo;

	private Integer indicadorTipoNota;

	private byte[] arquivoXml;

	private Integer emissaoCliente;

	@Override
	public NaturezaOperacaoCFOP getNaturezaOperacaoCFOP() {

		return naturezaOperacaoCFOP;
	}

	@Override
	public void setNaturezaOperacaoCFOP(NaturezaOperacaoCFOP naturezaOperacaoCFOP) {

		this.naturezaOperacaoCFOP = naturezaOperacaoCFOP;
	}

	@Override
	public Fatura getFatura() {

		return fatura;
	}

	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	@Override
	public Fatura getFaturaOrigemDevolucao() {

		return faturaOrigemDevolucao;
	}

	@Override
	public void setFaturaOrigemDevolucao(Fatura faturaOrigemDevolucao) {

		this.faturaOrigemDevolucao = faturaOrigemDevolucao;
	}

	@Override
	public String getInscricaoEstadualSubs() {

		return inscricaoEstadualSubs;
	}

	@Override
	public void setInscricaoEstadualSubs(String inscricaoEstadualSubs) {

		this.inscricaoEstadualSubs = inscricaoEstadualSubs;
	}

	@Override
	public Date getHoraSaida() {
		Date data = null;
		if (this.horaSaida != null) {
			data = (Date) this.horaSaida.clone();
		}
		return data;
	}

	@Override
	public void setHoraSaida(Date horaSaida) {
		if(horaSaida != null) {
			this.horaSaida = (Date) horaSaida.clone();
		} else {
			this.horaSaida = null;
		}
	}

	@Override
	public String getEnderecoTransportadora() {

		return enderecoTransportadora;
	}

	@Override
	public void setEnderecoTransportadora(String enderecoTransportadora) {

		this.enderecoTransportadora = enderecoTransportadora;
	}

	/**
	 * @return the numero
	 */
	@Override
	public Long getNumero() {

		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	@Override
	public void setNumero(Long numero) {

		this.numero = numero;
	}

	@Override
	public String getDescricaoSerie() {

		return descricaoSerie;
	}

	@Override
	public void setDescricaoSerie(String descricaoSerie) {

		this.descricaoSerie = descricaoSerie;
	}

	@Override
	public Integer getCodigoANTT() {

		return codigoANTT;
	}

	@Override
	public void setCodigoANTT(Integer codigoANTT) {

		this.codigoANTT = codigoANTT;
	}

	@Override
	public String getNomeTransportador() {

		return nomeTransportador;
	}

	@Override
	public void setNomeTransportador(String nomeTransportador) {

		this.nomeTransportador = nomeTransportador;
	}

	@Override
	public String getInscEstadualTransportadora() {

		return inscEstadualTransportadora;
	}

	@Override
	public void setInscEstadualTransportadora(String inscEstadualTransportadora) {

		this.inscEstadualTransportadora = inscEstadualTransportadora;
	}

	@Override
	public String getPlacaVeiculo() {

		return placaVeiculo;
	}

	@Override
	public void setPlacaVeiculo(String placaVeiculo) {

		this.placaVeiculo = placaVeiculo;
	}

	@Override
	public String getUfPlaca() {

		return ufPlaca;
	}

	@Override
	public void setUfPlaca(String ufPlaca) {

		this.ufPlaca = ufPlaca;
	}

	@Override
	public String getCpfCNPJTransportadora() {

		return cpfCNPJTransportadora;
	}

	@Override
	public void setCpfCNPJTransportadora(String cpfCNPJTransportadora) {

		this.cpfCNPJTransportadora = cpfCNPJTransportadora;
	}

	@Override
	public String getMunicipioTransportadora() {

		return municipioTransportadora;
	}

	@Override
	public void setMunicipioTransportadora(String municipioTransportadora) {

		this.municipioTransportadora = municipioTransportadora;
	}

	@Override
	public String getUfTransportadora() {

		return ufTransportadora;
	}

	@Override
	public void setUfTransportadora(String ufTransportadora) {

		this.ufTransportadora = ufTransportadora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setValorTotal(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getNomeCliente()
	 */
	@Override
	public String getNomeCliente() {

		return nomeCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setNomeCliente(java.lang.String)
	 */
	@Override
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getCpfCpnj()
	 */
	@Override
	public String getCpfCpnj() {

		return cpfCpnj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setCpfCpnj(java.lang.String)
	 */
	@Override
	public void setCpfCpnj(String cpfCpnj) {

		this.cpfCpnj = cpfCpnj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getInscricaoEstadual()
	 */
	@Override
	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setInscricaoEstadual(java.lang.String)
	 */
	@Override
	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getRg()
	 */
	@Override
	public String getRg() {

		return rg;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setRg(java.lang.String)
	 */
	@Override
	public void setRg(String rg) {

		this.rg = rg;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getEndereco()
	 */
	@Override
	public String getEndereco() {

		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setEndereco(java.lang.String)
	 */
	@Override
	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getComplemento()
	 */
	@Override
	public String getComplemento() {

		return complemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setComplemento(java.lang.String)
	 */
	@Override
	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getBairro()
	 */
	@Override
	public String getBairro() {

		return bairro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setBairro(java.lang.String)
	 */
	@Override
	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getCep()
	 */
	@Override
	public String getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setCep(java.lang.String)
	 */
	@Override
	public void setCep(String cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getMunicipio()
	 */
	@Override
	public String getMunicipio() {

		return municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setMunicipio(java.lang.String)
	 */
	@Override
	public void setMunicipio(String municipio) {

		this.municipio = municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getUf()
	 */
	@Override
	public String getUf() {

		return uf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setUf(java.lang.String)
	 */
	@Override
	public void setUf(String uf) {

		this.uf = uf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.impl.DocumentoFiscal #getApresentacao()
	 */
	@Override
	public Date getApresentacao() {
		Date data = null;
		if (this.apresentacao != null) {
			data = (Date) apresentacao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setApresentacao(java.util.Date)
	 */
	@Override
	public void setApresentacao(Date apresentacao) {
		if (apresentacao != null) {
			this.apresentacao = (Date) apresentacao.clone();
		} else {
			this.apresentacao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.impl.DocumentoFiscal #getDataEmissao()
	 */
	@Override
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setDataEmissao(java.util.Date)
	 */
	@Override
	public void setDataEmissao(Date dataEmissao) {
		if (dataEmissao != null) {
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getDescricaoPontoConsumo()
	 */
	@Override
	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setDescricaoPontoConsumo(java.lang.String)
	 */
	@Override
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getMensagem()
	 */
	@Override
	public String getMensagem() {

		return mensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #setMensagem(java.lang.String)
	 */
	@Override
	public void setMensagem(String mensagem) {

		this.mensagem = mensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getTipoOperacao()
	 */
	@Override
	public EntidadeConteudo getTipoOperacao() {

		return tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #
	 * setTipoOperacao(br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setTipoOperacao(EntidadeConteudo tipoOperacao) {

		this.tipoOperacao = tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #getTipoNota()
	 */
	@Override
	public EntidadeConteudo getTipoFaturamento() {

		return tipoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.DocumentoFiscal
	 * #
	 * setTipoOperacao(br.com.ggas.contrato.contrato
	 * .EntidadeConteudo)
	 */
	@Override
	public void setTipoFaturamento(EntidadeConteudo tipoFaturamento) {

		this.tipoFaturamento = tipoFaturamento;
	}

	@Override
	public Serie getSerie() {

		return serie;
	}

	@Override
	public void setSerie(Serie serie) {

		this.serie = serie;
	}

	@Override
	public EntidadeConteudo getTipoEmissaoNfe() {

		return tipoEmissaoNfe;
	}

	@Override
	public void setTipoEmissaoNfe(EntidadeConteudo tipoEmissaoNfe) {

		this.tipoEmissaoNfe = tipoEmissaoNfe;
	}

	@Override
	public EntidadeConteudo getStatusNfe() {

		return statusNfe;
	}

	@Override
	public void setStatusNfe(EntidadeConteudo statusNfe) {

		this.statusNfe = statusNfe;
	}

	@Override
	public String getChaveAcesso() {

		return chaveAcesso;
	}

	@Override
	public void setChaveAcesso(String chaveAcesso) {

		this.chaveAcesso = chaveAcesso;
	}

	@Override
	public String getNumeroProtocolo() {

		return numeroProtocolo;
	}

	@Override
	public void setNumeroProtocolo(String numeroProtocolo) {

		this.numeroProtocolo = numeroProtocolo;
	}

	@Override
	public Integer getIndicadorTipoNota() {

		return indicadorTipoNota;
	}

	@Override
	public void setIndicadorTipoNota(Integer indicadorTipoNota) {

		this.indicadorTipoNota = indicadorTipoNota;
	}

	@Override
	public byte[] getArquivoXml() {

		return arquivoXml;
	}

	@Override
	public void setArquivoXml(byte[] arquivoXml) {

		this.arquivoXml = arquivoXml;
	}

	@Override
	public boolean isTipoOperacaoEntrada() {

		return getTipoOperacao().getDescricao().equalsIgnoreCase(TIPO_OPERACAO_ENTRADA);
	}

	@Override
	public Integer getEmissaoCliente() {
		return emissaoCliente;
	}

	@Override
	public void setEmissaoCliente(Integer emissaoCliente) {
		this.emissaoCliente = emissaoCliente;
	}
}
