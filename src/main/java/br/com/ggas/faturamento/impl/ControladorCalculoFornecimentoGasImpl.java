/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.SerializationUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.FaturamentoCache;
import br.com.ggas.faturamento.exception.ApuracaoDiariaException;
import br.com.ggas.faturamento.exception.BaseApuracaoInvalidaException;
import br.com.ggas.faturamento.exception.ConsumoNaoInformadoException;
import br.com.ggas.faturamento.exception.ConsumoNaoPositivoException;
import br.com.ggas.faturamento.exception.DataFimNaoInformadoException;
import br.com.ggas.faturamento.exception.DataInicioNaoInformadoException;
import br.com.ggas.faturamento.exception.DataInicioPosteriorDataFimException;
import br.com.ggas.faturamento.exception.DataLeituraAtualNaoInformadaOuInvalidaException;
import br.com.ggas.faturamento.exception.EscalaNaoInformadaException;
import br.com.ggas.faturamento.exception.ItemFaturaNaoInformadoException;
import br.com.ggas.faturamento.exception.PeriodoCalculoFornecimentoGasException;
import br.com.ggas.faturamento.exception.PontoConsumoNaoInformadoException;
import br.com.ggas.faturamento.exception.PrecisaoNaoInformadaException;
import br.com.ggas.faturamento.exception.PrecoGasException;
import br.com.ggas.faturamento.exception.SimularApuracaoDiariaException;
import br.com.ggas.faturamento.exception.SubstituicaoTributariaException;
import br.com.ggas.faturamento.exception.TarifaComVigenciaPendenteDeAprovacaoException;
import br.com.ggas.faturamento.exception.TarifaNaoInformadaException;
import br.com.ggas.faturamento.exception.TarifaNaoPossuiVigenciasNoPeriodoException;
import br.com.ggas.faturamento.exception.TarifaVigenciaFaixaException;
import br.com.ggas.faturamento.exception.TarifaVigenciaNaoInformadaException;
import br.com.ggas.faturamento.exception.TipoCalculoInvalidoException;
import br.com.ggas.faturamento.exception.TipoMedicaoException;
import br.com.ggas.faturamento.exception.TributoMaiorValorException;
import br.com.ggas.faturamento.exception.VariacaoCambialException;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaPontoConsumo;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 * Classe da Controladora de Cálculo de Fornecimento de Gás
 *
 */
class ControladorCalculoFornecimentoGasImpl extends ControladorNegocioImpl implements ControladorCalculoFornecimentoGas {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final int ESCALA = 6;
	
	private static final String ERRO_VARIACAO_CAMBIAL = "ERRO_VARIACAO_CAMBIAL";

	private static final String VALOR_MAXIMO_FAIXA_FINAL = "99999999999999999";

	private static final Logger LOG = Logger.getLogger(ControladorCalculoFornecimentoGasImpl.class);

	private static final String TIPO_SUBSTITUTO = "TIPO_SUBSTITUTO";

	private static final String BASE_APURACAO = "baseApuracao";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String TARIFA_CHAVE_PRIMARIA = "tarifa.chavePrimaria";

	private static final String STATUS_CHAVE_PRIMARIA = "status.chavePrimaria";

	private static final String DATA_VIGENCIA = "dataVigencia";

	private static final String GRUPO_FATURAMENTO = "grupoFaturamento";

	/**
	 * Declaração para uso na própria classe.
	 */
	private StringBuilder deleteme = null;

	private Long valorICMS = null;

	private Long valorTipoSubstituto = null;

	private boolean isDescontoAplicadoValortotal = false;
	
	@Autowired
	@Qualifier(value = "controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeTarifaVigenciaFaixa() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaFaixa.BEAN_ID_TARIFA_VIGENCIA_FAIXA);
	}

	public Class<?> getClasseEntidadeTarifaVigencia() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigencia.BEAN_ID_TARIFA_VIGENCIA);
	}

	public Class<?> getClasseEntidadeTarifa() {

		return ServiceLocator.getInstancia().getClassPorID(Tarifa.BEAN_ID_TARIFA);
	}

	public Class<?> getClasseEntidadeTarifaPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaPontoConsumo.BEAN_ID_TARIFA_PONTOCONSUMO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoItemFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(
						ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContrato() {

		return ServiceLocator.getInstancia().getClassPorID(Contrato.BEAN_ID_CONTRATO);
	}

	public Class<?> getClasseEntidadeTributoAliquota() {

		return ServiceLocator.getInstancia().getClassPorID(TributoAliquota.BEAN_ID_TRIBUTO_ALIQUOTA);
	}

	public Class<?> getClasseEntidadeTarifaTributo() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaTributo.BEAN_ID_TARIFA_VIGENCIA_TRIBUTO);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeTarifaVigenciaDesconto() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaDesconto.BEAN_ID_TARIFA_VIGENCIA_DESCONTO);
	}

	public Class<?> getClasseEntidadeTarifaFaixaDesconto() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaFaixaDesconto.BEAN_ID_TARIFA_FAIXA_DESCONTO);
	}

	public Class<?> getClasseEntidadeIndiceFinanceiroValorNominal() {

		return ServiceLocator.getInstancia().getClassPorID(IndiceFinanceiroValorNominal.BEAN_ID_INDICE_FINANCEIRO_VALOR_NOMINAL);
	}

	/**
	 * Método que mapeia as TarifasVigencia utilizadas num determinado período.
	 *
	 * @param dataInicioPeriodoMedicao data de Início do período
	 * @param periodoMedicaoDataFim data de Término do período
	 * @param itemFatura the item fatura
	 * @param colecaoPontoConsumo the colecao ponto consumo
	 * @param tarifaVigenciaParametro the tarifa vigencia parametro
	 * @param tarifa tarifa que pode ou não ser passada. Caso seja, é a única a ser utilizada. Caso contrário, deve-se obter as tarifas do
	 *            ponto de consumo.
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param idContratoAditadoAlterado the id contrato aditado alterado
	 * @param idContrato the id contrato
	 * @return Mapa com os dias do intervalo e as TarifaVigencia utilizadas em cada dia.
	 * @throws NegocioException the negocio exception
	 */
	private List<PeriodoVigenciaDesconto> mapearTarifasVigenciaPorDia(Date periodoMedicaoDataInicio, Date periodoMedicaoDataFim,
			EntidadeConteudo itemFatura, Collection<PontoConsumo> colecaoPontoConsumo, TarifaVigencia tarifaVigenciaParametro,
			Tarifa tarifa, boolean isConsiderarDescontos, Contrato contratoAditadoAlterado, Contrato contrato) throws NegocioException {

		Collection<Tarifa> colecaoTarifas = null;
		List<TarifaVigencia> colecaoTarifasVigencia = null;
		Date dataInicioPeriodoMedicao = periodoMedicaoDataInicio;
		Date dataFimPeriodoMedicao = periodoMedicaoDataFim;

		if (tarifaVigenciaParametro != null) {

			// Se for simulação deve usar apenas a
			// TarifaVigencia e a Tarifa que foram
			// passadas por parâmetro.
			colecaoTarifas = new ArrayList<Tarifa>();
			colecaoTarifas.add(tarifaVigenciaParametro.getTarifa());

			colecaoTarifasVigencia = new ArrayList<TarifaVigencia>();
			colecaoTarifasVigencia.add(tarifaVigenciaParametro);

		} else if (tarifa != null) {
			colecaoTarifas = new ArrayList<Tarifa>();
			colecaoTarifas.add(tarifa);

			// As Vigencias que pertencem às
			// tarifas de colecaoTarifas
			colecaoTarifasVigencia = listarTarifasVigenciaPorColecaoTarifasIntervaloDatas(colecaoTarifas, dataInicioPeriodoMedicao,
							dataFimPeriodoMedicao);

			if (colecaoTarifasVigencia == null || colecaoTarifasVigencia.isEmpty()) {
				throw new TarifaNaoPossuiVigenciasNoPeriodoException();
			}

		} else {

			// As Tarifas que são utilizadas pelo
			// pontoConsumo entre
			// dataInicioPeriodo e dataFimPeriodo
			PontoConsumo pontoConsumo = colecaoPontoConsumo.iterator().next();
			if (contratoAditadoAlterado == null) {
				colecaoTarifas = listarChavesTarifasPorPontoConsumo(pontoConsumo, contrato, itemFatura);
			} else {
				colecaoTarifas =
						listarTarifasPorPontoConsumoSemContratoAtivo(pontoConsumo, itemFatura, contratoAditadoAlterado.getChavePrimaria());
			}

			// verifica se tem tarifa vigência pendente de autorização
			Long qtdTarifasPendentes =
					contarTarifasPendentesPorPontoConsumo(pontoConsumo, itemFatura, dataInicioPeriodoMedicao, dataFimPeriodoMedicao);	
			if (qtdTarifasPendentes > 0) {
				throw new TarifaComVigenciaPendenteDeAprovacaoException();
			}

			// As Vigencias que pertencem às
			// tarifas de colecaoTarifas
			colecaoTarifasVigencia = listarTarifasVigenciaPorColecaoTarifasIntervaloDatas(colecaoTarifas, dataInicioPeriodoMedicao,
							dataFimPeriodoMedicao);

			if (colecaoTarifasVigencia == null || colecaoTarifasVigencia.isEmpty()) {
				throw new TarifaNaoPossuiVigenciasNoPeriodoException();
			}

		}

		// Ordena as coleções de TarifaVigencia
		// pela dataVigencia
		List<TarifaVigencia> colecaoTarifaVigenciaOrdenada = colecaoTarifasVigencia;

		// Objeto auxiliar para guardar a última
		// TarifaVigencia iterada
		TarifaVigencia tarifaVigenciaAnterior = null;

		// Coleção em que serão mapeados os
		// períodos
		List<PeriodoVigenciaDesconto> colecaoPeriodos = new ArrayList<PeriodoVigenciaDesconto>();

		// DateTime do início do período de
		// medição
		DateTime dateTimeInicioPeriodoMedicao = new DateTime(dataInicioPeriodoMedicao);

		// DateTime do fim do período de medição
		DateTime dateTimeFimPeriodoMedicao = new DateTime(dataFimPeriodoMedicao);

		if (tarifaVigenciaParametro == null && colecaoPontoConsumo != null && !colecaoPontoConsumo.isEmpty()) {

			/* *************************************************** *
			 * Tarifas Temporárias
			 * ************************************
			 * ***************
			 */

			// As Tarifas Temporárias que são
			// utilizadas pelo pontoConsumo entre
			// dataInicioPeriodo e dataFimPeriodo
			Collection<TarifaPontoConsumo> colecaoTarifasTemporarias = listarTarifasTemporariasIntervaloDatas(colecaoPontoConsumo,
							dataInicioPeriodoMedicao, dataFimPeriodoMedicao);

			// As Vigencias que pertencem às
			// tarifas temporárias de
			// colecaoTarifasTemporarias
			Map<TarifaPontoConsumo, List<TarifaVigencia>> colecaoTarifasVigenciaTemporarias =
							listarTarifasVigenciaTemporariasPorTarifasIntervaloDatas(
							colecaoTarifasTemporarias, dataInicioPeriodoMedicao, dataFimPeriodoMedicao);

			for (Map.Entry<TarifaPontoConsumo, List<TarifaVigencia>> entry : colecaoTarifasVigenciaTemporarias.entrySet()) {

				TarifaPontoConsumo tarifaPontoConsumo = entry.getKey();
				// ordena a coleção de
				// TarifaVigencia da tarifa
				// temporária
				List<TarifaVigencia> listaOrdenadaVigenciasTemporarias = ordenarTarifasVigencia(entry.getValue());

				tarifaVigenciaAnterior = null;
				//
				// by
				// vmelo
				// on
				// 16/08/10
				// 14:24

				if (listaOrdenadaVigenciasTemporarias != null && !listaOrdenadaVigenciasTemporarias.isEmpty()) {

					Collection<TarifaVigenciaDesconto> listaDescontos = null;

					for (TarifaVigencia tarifaVigencia : listaOrdenadaVigenciasTemporarias) {

						if (tarifaVigenciaAnterior != null) {

							// Data Inicial: Pega
							// a data de Inicio da
							// Vigencia ou a data
							// de Inicio da Tarifa
							// Temporaria, a que
							// for maior.
							DateTime dateTimeInicioVigenciaTemporaria = null;

							// Datas sem hora
							Date dataInicioTarifaPonto = DataUtil.gerarDataHmsZerados(tarifaPontoConsumo.getDataInicioVigencia());
							Date dataTarifaVigenciaAnterior = DataUtil.gerarDataHmsZerados(tarifaVigenciaAnterior.getDataVigencia());

							if (dataInicioTarifaPonto.compareTo(dataTarifaVigenciaAnterior) > 0) {
								dateTimeInicioVigenciaTemporaria = new DateTime(dataInicioTarifaPonto);
							} else {
								dateTimeInicioVigenciaTemporaria = new DateTime(dataTarifaVigenciaAnterior);
							}
							if (dateTimeInicioPeriodoMedicao.isAfter(dateTimeInicioVigenciaTemporaria)) {
								dateTimeInicioVigenciaTemporaria = dateTimeInicioPeriodoMedicao;
							}

							// Data Final: Pega a
							// data de fim da
							// Vigencia ou a data
							// de Inicio da Tarifa
							// Temporaria, a que
							// for menor.
							DateTime dateTimeFimVigenciaTemporaria = null;

							// Datas sem hora
							Date dataFimTarifaPonto = DataUtil.gerarDataHmsZerados(tarifaPontoConsumo.getDataFimVigencia());
							Date dataTarifaVigencia = DataUtil.gerarDataHmsZerados(tarifaVigencia.getDataVigencia());

							if (dataFimTarifaPonto.compareTo(dataTarifaVigencia) < 0) {
								dateTimeFimVigenciaTemporaria = new DateTime(dataFimTarifaPonto);
							} else {
								dateTimeFimVigenciaTemporaria = new DateTime(dataTarifaVigencia);
								dateTimeFimVigenciaTemporaria = dateTimeFimVigenciaTemporaria.minusDays(1);
							}
							if (dateTimeFimPeriodoMedicao.isBefore(dateTimeFimVigenciaTemporaria)) {
								dateTimeFimVigenciaTemporaria = dateTimeFimPeriodoMedicao;
							}

							// Lista de
							// TarifaVigenciaDesconto
							// utilizados
							if (isConsiderarDescontos) {

								listaDescontos = consultarDescontosPorData(tarifaVigenciaAnterior.getTarifa(),
												dateTimeInicioVigenciaTemporaria.toDate(), dateTimeFimVigenciaTemporaria.toDate());

								if (listaDescontos != null && !listaDescontos.isEmpty()) {
									criarPeriodosVigenciaDesconto(colecaoPeriodos, dateTimeInicioPeriodoMedicao, dateTimeFimPeriodoMedicao,
													tarifaVigenciaAnterior, listaDescontos, dateTimeInicioVigenciaTemporaria);
								}
							}

							criarPeriodosVigencia(tarifaVigenciaAnterior, colecaoPeriodos, dateTimeInicioVigenciaTemporaria,
											dateTimeFimVigenciaTemporaria);
						}

						tarifaVigenciaAnterior = tarifaVigencia;
					}

					/*
					 * Processa a o último
					 * elemento de
					 * colecaoTarifaVigenciaOrdenada
					 * pois ele ainda não foi
					 * processado.
					 * Inclui esse último elemento
					 * no mapa que foi populado
					 * acima.
					 */

					// Data Inicial
					DateTime dateTimeInicioUltimaVigenciaTemporaria = null;

					// Datas sem hora
					Date dataInicioTarifaPonto = DataUtil.gerarDataHmsZerados(tarifaPontoConsumo.getDataInicioVigencia());
					Date dataTarifaVigenciaAnterior = DataUtil.gerarDataHmsZerados(tarifaVigenciaAnterior.getDataVigencia());

					if (dataInicioTarifaPonto.compareTo(dataTarifaVigenciaAnterior) > 0) {
						dateTimeInicioUltimaVigenciaTemporaria = new DateTime(dataInicioTarifaPonto);
					} else {
						dateTimeInicioUltimaVigenciaTemporaria = new DateTime(dataTarifaVigenciaAnterior);

					}
					if (dateTimeInicioPeriodoMedicao.isAfter(dateTimeInicioUltimaVigenciaTemporaria)) {
						dateTimeInicioUltimaVigenciaTemporaria = dateTimeInicioPeriodoMedicao;
					}

					// Pega a data final
					DateTime dateTimeFimUltimaVigenciaTemporaria = null;

					// Datas sem hora
					Date dataFimTarifaPonto = DataUtil.gerarDataHmsZerados(tarifaPontoConsumo.getDataFimVigencia());
					dataFimPeriodoMedicao = DataUtil.gerarDataHmsZerados(dataFimPeriodoMedicao);

					if (dataFimTarifaPonto.compareTo(dataFimPeriodoMedicao) < 0) {
						dateTimeFimUltimaVigenciaTemporaria = new DateTime(dataFimTarifaPonto);
					} else {
						dateTimeFimUltimaVigenciaTemporaria = new DateTime(dataFimPeriodoMedicao);
						dateTimeFimUltimaVigenciaTemporaria.minusDays(1);
					}
					if (dateTimeFimPeriodoMedicao.isBefore(dateTimeFimUltimaVigenciaTemporaria)) {
						dateTimeFimUltimaVigenciaTemporaria = dateTimeFimPeriodoMedicao;
					}

					// Lista de
					// TarifaVigenciaDesconto
					// utilizados
					if (isConsiderarDescontos) {
						listaDescontos = consultarDescontosPorData(tarifaVigenciaAnterior.getTarifa(),
										dateTimeInicioUltimaVigenciaTemporaria.toDate(), dateTimeFimUltimaVigenciaTemporaria.toDate());

						if (listaDescontos != null && !listaDescontos.isEmpty()) {
							criarPeriodosVigenciaDesconto(colecaoPeriodos, dateTimeInicioPeriodoMedicao, dateTimeFimPeriodoMedicao,
											tarifaVigenciaAnterior, listaDescontos, dateTimeInicioUltimaVigenciaTemporaria);
						}
					}

					criarPeriodosVigencia(tarifaVigenciaAnterior, colecaoPeriodos, dateTimeInicioUltimaVigenciaTemporaria,
									dateTimeFimUltimaVigenciaTemporaria);
				}
			}
		}

		/* *************************************************** *
		 * Tarifas Não-Temporárias
		 * ****************************************
		 * ***********
		 */

		tarifaVigenciaAnterior = null;
		// by
		// vmelo
		// on
		// 26/07/10
		// 09:53
		Collection<TarifaVigenciaDesconto> listaDescontos = null;

		// Mapeia a TarifaVigencia que deve ser
		// utilizada em cada dia do intervalo
		// passado por parâmetro
		if (colecaoTarifaVigenciaOrdenada != null && !colecaoTarifaVigenciaOrdenada.isEmpty()) {
			for (TarifaVigencia tarifaVigencia : colecaoTarifaVigenciaOrdenada) {

				if (tarifaVigenciaAnterior != null) {

					// Data Inicial: Pega a data
					// de Inicio da Vigencia ou a
					// data de Inicio da Tarifa
					// Temporaria, a que for
					// maior.
					DateTime dateTimeInicioVigenciaNaoTemporaria = null;

					// Datas sem hora
					dataInicioPeriodoMedicao = DataUtil.gerarDataHmsZerados(dataInicioPeriodoMedicao);
					Date dataTarifaVigenciaAnterior = DataUtil.gerarDataHmsZerados(tarifaVigenciaAnterior.getDataVigencia());

					if (dataInicioPeriodoMedicao.compareTo(dataTarifaVigenciaAnterior) > 0) {
						dateTimeInicioVigenciaNaoTemporaria = new DateTime(dataInicioPeriodoMedicao);
					} else {
						dateTimeInicioVigenciaNaoTemporaria = new DateTime(dataTarifaVigenciaAnterior);
					}

					// Data Final: Pega a data de
					// fim da Vigencia ou a data
					// de Inicio da Tarifa
					// Temporaria, a que for
					// menor.
					DateTime dateTimeFimVigenciaNaoTemporaria = null;

					// Datas sem hora
					dataFimPeriodoMedicao = DataUtil.gerarDataHmsZerados(dataFimPeriodoMedicao);
					Date dataTarifaVigencia = DataUtil.gerarDataHmsZerados(tarifaVigencia.getDataVigencia());

					if (dataFimPeriodoMedicao.compareTo(dataTarifaVigencia) < 0) {
						dateTimeFimVigenciaNaoTemporaria = new DateTime(dataFimPeriodoMedicao);
					} else {
						dateTimeFimVigenciaNaoTemporaria = new DateTime(dataTarifaVigencia);
						dateTimeFimVigenciaNaoTemporaria = dateTimeFimVigenciaNaoTemporaria.minusDays(1);
					}

					// Lista de
					// TarifaVigenciaDesconto
					// utilizados
					if (isConsiderarDescontos) {

						listaDescontos = consultarDescontosPorData(tarifaVigenciaAnterior.getTarifa(),
										dateTimeInicioVigenciaNaoTemporaria.toDate(), dateTimeFimVigenciaNaoTemporaria.toDate());

						if (listaDescontos != null && !listaDescontos.isEmpty()) {
							criarPeriodosVigenciaDesconto(colecaoPeriodos, dateTimeInicioPeriodoMedicao, dateTimeFimPeriodoMedicao,
											tarifaVigenciaAnterior, listaDescontos, dateTimeInicioVigenciaNaoTemporaria);
						}
					}

					criarPeriodosVigencia(tarifaVigenciaAnterior, colecaoPeriodos, dateTimeInicioVigenciaNaoTemporaria,
									dateTimeFimVigenciaNaoTemporaria);
				}

				tarifaVigenciaAnterior = tarifaVigencia;
			}
		}

		/*
		 * Processa a o último elemento de
		 * colecaoTarifaVigenciaOrdenada pois ele
		 * ainda não foi processado.
		 * Inclui esse último elemento no mapa que
		 * foi populado acima.
		 */

		// Data Inicial
		DateTime dateTimeInicioUltimaVigenciaNaoTemporaria = null;

		// Datas sem hora
		dataInicioPeriodoMedicao = DataUtil.gerarDataHmsZerados(dataInicioPeriodoMedicao);
		if (tarifaVigenciaAnterior != null) {
			Date dataTarifaVigenciaAnterior = DataUtil.gerarDataHmsZerados(tarifaVigenciaAnterior.getDataVigencia());
			if (dataInicioPeriodoMedicao.compareTo(dataTarifaVigenciaAnterior) > 0 || tarifaVigenciaParametro != null) {
				dateTimeInicioUltimaVigenciaNaoTemporaria = new DateTime(dataInicioPeriodoMedicao);
			} else {
				dateTimeInicioUltimaVigenciaNaoTemporaria = new DateTime(dataTarifaVigenciaAnterior);
			}
		}

		// Pega a data final
		dataFimPeriodoMedicao = DataUtil.gerarDataHmsZerados(dataFimPeriodoMedicao);
		DateTime dateTimeFimUltimaVigenciaNaoTemporaria = new DateTime(dataFimPeriodoMedicao);

		if (isConsiderarDescontos) {
			// Lista de TarifaVigenciaDesconto
			// utilizados
			listaDescontos = consultarDescontosPorData(tarifaVigenciaAnterior.getTarifa(),
							dateTimeInicioUltimaVigenciaNaoTemporaria.toDate(), dateTimeFimUltimaVigenciaNaoTemporaria.toDate());

			if (listaDescontos != null && !listaDescontos.isEmpty()) {
				criarPeriodosVigenciaDesconto(colecaoPeriodos, dateTimeInicioPeriodoMedicao, dateTimeFimPeriodoMedicao,
								tarifaVigenciaAnterior, listaDescontos, dateTimeInicioUltimaVigenciaNaoTemporaria);
			}
		}

		if (tarifaVigenciaAnterior != null && colecaoPeriodos != null && dateTimeInicioUltimaVigenciaNaoTemporaria != null
						&& dateTimeFimUltimaVigenciaNaoTemporaria != null) {

			colecaoPeriodos = criarPeriodosVigencia(tarifaVigenciaAnterior, colecaoPeriodos, dateTimeInicioUltimaVigenciaNaoTemporaria,
							dateTimeFimUltimaVigenciaNaoTemporaria);
		}

		Collections.sort(colecaoPeriodos);

		return colecaoPeriodos;
	}

	/**
	 * Criar periodos vigencia.
	 *
	 * @param tarifaVigenciaAnterior
	 *            the tarifa vigencia anterior
	 * @param colecaoPeriodos
	 *            the colecao periodos
	 * @param dateTimeInicio
	 *            the date time inicio
	 * @param dateTimeFim
	 *            the date time fim
	 * @return the list
	 */
	private List<PeriodoVigenciaDesconto> criarPeriodosVigencia(TarifaVigencia tarifaVigenciaAnterior,
					List<PeriodoVigenciaDesconto> colecaoPeriodos, DateTime dateTimeInicio, DateTime dateTimeFim) {

		DateTime dateTimeInicialPeriodo = null;
		DateTime dateTimeAnterior = null;

		DateTime dateTimeIteracao = dateTimeInicio;
		while (!dateTimeIteracao.isAfter(dateTimeFim)) {

			if (!verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos) && dateTimeInicialPeriodo == null) {
				dateTimeInicialPeriodo = dateTimeIteracao;
			}

			if (!dateTimeIteracao.isBefore(dateTimeFim)
							|| (verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos) && dateTimeInicialPeriodo != null)) {

				if (dateTimeInicialPeriodo != null) {
					DateTime dateTimeFinalPeriodo = null;
					if (!dateTimeIteracao.isBefore(dateTimeFim) && !verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos)) {
						dateTimeFinalPeriodo = dateTimeIteracao;
					} else {
						dateTimeFinalPeriodo = dateTimeAnterior;
					}
					Periodo periodoVigenciaTemporaria = new Periodo(dateTimeInicialPeriodo, dateTimeFinalPeriodo);
					PeriodoVigenciaDesconto pvd = new PeriodoVigenciaDesconto();
					pvd.periodo = periodoVigenciaTemporaria;
					pvd.tarifaVigencia = tarifaVigenciaAnterior;
					colecaoPeriodos.add(pvd);
					dateTimeInicialPeriodo = null;
					//
					// by
					// vmelo
					// on
					// 26/07/10
					// 09:53
				}
			} else {
				if (!verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos) && dateTimeInicialPeriodo == null) {
					dateTimeInicialPeriodo = dateTimeIteracao;
				}
			}
			dateTimeAnterior = dateTimeIteracao;
			dateTimeIteracao = dateTimeIteracao.plusDays(1);
		}
		return colecaoPeriodos;
	}

	/**
	 * Criar periodos vigencia desconto.
	 *
	 * @param colecaoPeriodos
	 *            the colecao periodos
	 * @param dateTimeInicioPeriodoMedicao
	 *            the date time inicio periodo medicao
	 * @param dateTimeFimPeriodoMedicao
	 *            the date time fim periodo medicao
	 * @param tarifaVigenciaAnterior
	 *            the tarifa vigencia anterior
	 * @param listaDescontos
	 *            the lista descontos
	 * @param dateTimeInicioUltimaVigenciaTemporaria
	 *            the date time inicio ultima vigencia temporaria
	 * @param dateTimeFimUltimaVigenciaTemporaria
	 *            the date time fim ultima vigencia temporaria
	 */
	private void criarPeriodosVigenciaDesconto(List<PeriodoVigenciaDesconto> colecaoPeriodos, DateTime dateTimeInicioPeriodoMedicao,
					DateTime dateTimeFimPeriodoMedicao, TarifaVigencia tarifaVigenciaAnterior,
					Collection<TarifaVigenciaDesconto> listaDescontos, DateTime dateTimeInicioUltimaVigenciaTemporaria
					) {

		for (TarifaVigenciaDesconto tarifaVigenciaDesconto : listaDescontos) {

			DateTime dateTimeInicioVigenciaDesconto = new DateTime(tarifaVigenciaDesconto.getInicioVigencia());

			DateTime dateTimeFimVigenciaDesconto = new DateTime(tarifaVigenciaDesconto.getFimVigencia());

			// Data Inicial: Pega a data de Inicio
			// da Vigencia, a data de Inicio da
			// Tarifa Temporaria, a que for maior.
			DateTime dateTimeInicioPeriodoDesconto = null;
			if (dateTimeInicioUltimaVigenciaTemporaria.isAfter(dateTimeInicioVigenciaDesconto)) {
				dateTimeInicioPeriodoDesconto = dateTimeInicioUltimaVigenciaTemporaria;
			} else {
				dateTimeInicioPeriodoDesconto = dateTimeInicioVigenciaDesconto;
			}
			if (dateTimeInicioPeriodoMedicao.isAfter(dateTimeInicioPeriodoDesconto)) {
				dateTimeInicioPeriodoDesconto = dateTimeInicioPeriodoMedicao;
			}

			// Data Final: Pega a data de fim da
			// Vigencia ou a data de Inicio da
			// Tarifa Temporaria, a que for menor.
			DateTime dateTimeFimPeriodoDesconto = null;
			dateTimeFimPeriodoDesconto = dateTimeFimVigenciaDesconto;
			if (dateTimeFimPeriodoMedicao.isBefore(dateTimeFimPeriodoDesconto)) {
				dateTimeFimPeriodoDesconto = dateTimeFimPeriodoMedicao;
			}

			DateTime dateTimeInicialPeriodo = null;
			DateTime dateTimeAnterior = null;
			DateTime dateTimeIteracao = dateTimeInicioPeriodoDesconto;
			while (!dateTimeIteracao.isAfter(dateTimeFimPeriodoDesconto)) {
				if (!verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos) && dateTimeInicialPeriodo == null) {
					dateTimeInicialPeriodo = dateTimeIteracao;
				}

				if (!dateTimeIteracao.isBefore(dateTimeFimPeriodoDesconto)
								|| (verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos) && dateTimeInicialPeriodo != null)) {

					if (dateTimeInicialPeriodo != null) {
						DateTime dateTimeFinalPeriodo = null;
						if (!dateTimeIteracao.isBefore(dateTimeFimPeriodoDesconto)
										&& !verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos)) {
							dateTimeFinalPeriodo = dateTimeIteracao;
						} else {
							dateTimeFinalPeriodo = dateTimeAnterior;
						}
						Periodo periodoVigenciaTemporaria = new Periodo(dateTimeInicialPeriodo, dateTimeFinalPeriodo);
						PeriodoVigenciaDesconto pvd = new PeriodoVigenciaDesconto();
						pvd.periodo = periodoVigenciaTemporaria;
						pvd.desconto = tarifaVigenciaDesconto;
						pvd.tarifaVigencia = tarifaVigenciaAnterior;
						colecaoPeriodos.add(pvd);
						dateTimeInicialPeriodo = null;
						//
						// by
						// vmelo
						// on
						// 26/07/10
						// 09:53
					}
				} else {
					if (!verificaDataEntrePeriodos(dateTimeIteracao, colecaoPeriodos) && dateTimeInicialPeriodo == null) {
						dateTimeInicialPeriodo = dateTimeIteracao;
					}
				}
				dateTimeAnterior = dateTimeIteracao;
				dateTimeIteracao = dateTimeIteracao.plusDays(1);
			}

		}
	}

	/**
	 * Verifica data entre periodos.
	 *
	 * @param dateTimeIteracao
	 *            the date time iteracao
	 * @param colecaoPeriodos
	 *            the colecao periodos
	 * @return true, if successful
	 */
	private boolean verificaDataEntrePeriodos(DateTime dateTimeIteracao, Collection<PeriodoVigenciaDesconto> colecaoPeriodos) {

		boolean retorno = false;
		if (dateTimeIteracao != null && colecaoPeriodos != null) {
			for (PeriodoVigenciaDesconto pvd : colecaoPeriodos) {
				if (!dateTimeIteracao.isBefore(pvd.periodo.dataInicio) && !dateTimeIteracao.isAfter(pvd.periodo.dataFim)) {
					retorno = true;
					break;
				}
			}
		}
		return retorno;
	}

	/**
	 * Listar tarifas vigencia temporarias por tarifas intervalo datas.
	 *
	 * @param colecaoTarifasPontoConsumo
	 *            the colecao tarifas ponto consumo
	 * @param dataInicioPeriodo
	 *            the data inicio periodo
	 * @param dataFimPeriodo
	 *            the data fim periodo
	 * @return the hash map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<TarifaPontoConsumo, List<TarifaVigencia>> listarTarifasVigenciaTemporariasPorTarifasIntervaloDatas(
					Collection<TarifaPontoConsumo> colecaoTarifasPontoConsumo, Date dataInicioPeriodo, Date dataFimPeriodo)
					throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Map<TarifaPontoConsumo, List<TarifaVigencia>> mapaTarifaVigenciaTarifaPontoConsumo =
						new HashMap<TarifaPontoConsumo, List<TarifaVigencia>>();

		if (colecaoTarifasPontoConsumo != null && !colecaoTarifasPontoConsumo.isEmpty()) {

			for (TarifaPontoConsumo tarifaPontoConsumo : colecaoTarifasPontoConsumo) {

				StringBuilder hql = new StringBuilder();
				hql.append(" from ");
				hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
				hql.append(" tv ");
				hql.append(" inner join fetch tv.baseApuracao ");
				hql.append(" where tv.tarifa.chavePrimaria =  :idTarifa ");
				hql.append(" and tv.status = :status ");
				hql.append(" and ( ");
				hql.append(" ( tv.dataVigencia between :dataInicio and :dataFim ) ");
				hql.append(" or ");
				hql.append(" ( ");
				hql.append(" tv.dataVigencia = ( ");
				hql.append(" select max(tvAux.dataVigencia) from ");
				hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
				hql.append(" tvAux ");
				hql.append(" where tvAux.tarifa.chavePrimaria = :idTarifa ");
				hql.append(" and tvAux.dataVigencia < :dataInicio ");
				hql.append(" and tvAux.status = :status ");
				hql.append(" ) ");
				hql.append(" ) ");
				hql.append(" ) ");

				hql.append(" order by tv.dataVigencia ");

				Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

				// Parametros

				ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

				String paramStatusAutorizada = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
				query.setLong("status", Long.parseLong(paramStatusAutorizada));

				query.setParameter("idTarifa", tarifaPontoConsumo.getTarifa().getChavePrimaria());

				Date dataInicioConsulta = null;
				if (tarifaPontoConsumo.getDataInicioVigencia().compareTo(dataInicioPeriodo) > 0) {
					dataInicioConsulta = tarifaPontoConsumo.getDataInicioVigencia();
				} else {
					dataInicioConsulta = dataInicioPeriodo;
				}
				query.setParameter("dataInicio", dataInicioConsulta);

				Date dataFimConsulta = null;
				if (tarifaPontoConsumo.getDataFimVigencia().compareTo(dataFimPeriodo) < 0) {
					dataFimConsulta = tarifaPontoConsumo.getDataFimVigencia();
				} else {
					dataFimConsulta = dataFimPeriodo;
				}
				query.setParameter("dataFim", dataFimConsulta);

				List<TarifaVigencia> valor = query.list();

				mapaTarifaVigenciaTarifaPontoConsumo.put(tarifaPontoConsumo, valor);
			}
		}

		return mapaTarifaVigenciaTarifaPontoConsumo;
	}

	/**
	 * Ordenar tarifas vigencia.
	 *
	 * @param colecaoTarifasVigencia
	 *            the colecao tarifas vigencia
	 * @return the list
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private List<TarifaVigencia> ordenarTarifasVigencia(List<TarifaVigencia> colecaoTarifasVigencia) {

		BeanComparator comparador = new BeanComparator("dataVigencia");
		Collections.sort((List) colecaoTarifasVigencia, comparador);

		return colecaoTarifasVigencia;
	}

	/**
	 * Listar tarifas temporarias intervalo datas.
	 *
	 * @param colecaoPontoConsumo
	 *            the colecao ponto consumo
	 * @param dataInicioPeriodo
	 *            the data inicio periodo
	 * @param dataFimPeriodo
	 *            the data fim periodo
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<TarifaPontoConsumo> listarTarifasTemporariasIntervaloDatas(Collection<PontoConsumo> colecaoPontoConsumo,
					Date dataInicioPeriodo, Date dataFimPeriodo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaPontoConsumo().getSimpleName());
		hql.append(" tarifaPontoConsumo ");
		hql.append(" where ");
		hql.append(" tarifaPontoConsumo.pontoConsumo.chavePrimaria in ( :chavePontoConsumo ) ");
		hql.append(" and ( ( tarifaPontoConsumo.dataInicioVigencia < :dtInicio and tarifaPontoConsumo.dataFimVigencia >= :dtInicio ) ");
		hql.append(" or ( tarifaPontoConsumo.dataInicioVigencia between :dtInicio and :dtFinal ) ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavePontoConsumo", Util.collectionParaArrayChavesPrimarias(colecaoPontoConsumo));
		query.setDate("dtInicio", dataInicioPeriodo);
		query.setDate("dtFinal", dataFimPeriodo);

		return query.list();
	}

	/**
	 * Listar tarifas vigencia por colecao tarifas intervalo datas.
	 *
	 * @param colecaoTarifas
	 *            the colecao tarifas
	 * @param dataInicioPeriodo
	 *            the data inicio periodo
	 * @param dataFimPeriodo
	 *            the data fim periodo
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<TarifaVigencia> listarTarifasVigenciaPorColecaoTarifasIntervaloDatas(Collection<Tarifa> colecaoTarifas,
					Date dataInicioPeriodo, Date dataFimPeriodo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigencia());
		
		this.listarTarifasVigenciaPorColecaoTarifasIntervaloDatas(colecaoTarifas, dataInicioPeriodo, dataFimPeriodo, criteria);
		
		return criteria.list();
	}
	
	private void listarTarifasVigenciaPorColecaoTarifasIntervaloDatas(Collection<Tarifa> colecaoTarifas,
			Date dataInicioPeriodo, Date dataFimPeriodo, Criteria criteria) {
		
		criteria.createAlias(BASE_APURACAO, BASE_APURACAO, Criteria.INNER_JOIN);

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		String paraSistemaStatusAutorizado =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		criteria.add(Restrictions.eq(STATUS_CHAVE_PRIMARIA, Long.parseLong(paraSistemaStatusAutorizado)));

		Long[] arrayChavesPrimarias = null;
		
		if(colecaoTarifas!=null && !colecaoTarifas.isEmpty()) {
			arrayChavesPrimarias = Util.collectionParaArrayChavesPrimarias(colecaoTarifas);
		}
		
		if(arrayChavesPrimarias!=null){
			criteria.add(Restrictions.in(TARIFA_CHAVE_PRIMARIA, arrayChavesPrimarias));
		}

		if (dataInicioPeriodo != null && dataFimPeriodo != null && arrayChavesPrimarias!=null) {
			DetachedCriteria subconsulta1 = DetachedCriteria.forClass(this.getClasseEntidadeTarifaVigencia());
			subconsulta1.setProjection(Projections.max(DATA_VIGENCIA));
			subconsulta1.add(Restrictions.in(TARIFA_CHAVE_PRIMARIA, arrayChavesPrimarias));
			subconsulta1.add(Restrictions.le(DATA_VIGENCIA, dataInicioPeriodo));

			DetachedCriteria subconsulta2 = DetachedCriteria.forClass(this.getClasseEntidadeTarifaVigencia());
			subconsulta2.setProjection(Projections.id());
			subconsulta2.add(Restrictions.in(TARIFA_CHAVE_PRIMARIA, arrayChavesPrimarias));
			subconsulta2.add(Restrictions.ge(DATA_VIGENCIA, dataInicioPeriodo));
			subconsulta2.add(Restrictions.le(DATA_VIGENCIA, dataFimPeriodo));

			Criterion restrictionOR = Restrictions.or(Subqueries.propertyEq(DATA_VIGENCIA, subconsulta1),
													  Subqueries.propertyIn(CHAVE_PRIMARIA, subconsulta2));
			criteria.add(restrictionOR);
			criteria.addOrder(Order.asc(DATA_VIGENCIA));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#listarTarifasPorPontoConsumo(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.lang.Long, br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public Collection<Tarifa> listarTarifasPorPontoConsumo(PontoConsumo pontoConsumo, Contrato contrato, EntidadeConteudo itemFatura)
					throws NegocioException {


		StringBuilder hql = new StringBuilder();
		hql.append(" select cpcit.tarifa ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" cpcit ");

		hql.append(" inner join cpcit.contratoPontoConsumo cpc");
		hql.append(" inner join  cpc.contrato ctr");

		hql.append(" where cpcit.contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and cpc.chavePrimaria = cpcit.contratoPontoConsumo.chavePrimaria ");
		hql.append(" and cpc.contrato.chavePrimaria = ctr.chavePrimaria ");
		if (contrato != null) {
			hql.append(" and ctr.chavePrimaria = :chaveContrato ");
		}

		hql.append(" and ctr.situacao in ( ");
		hql.append(SituacaoContrato.ATIVO);
		hql.append(" , ");
		hql.append(SituacaoContrato.EM_NEGOCIACAO);
		hql.append(" ) ");

		if (itemFatura != null) {
			hql.append(" and cpcit.itemFatura.chavePrimaria = :itemFatura ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePontoConsumo", pontoConsumo.getChavePrimaria());

		if (itemFatura != null) {
			query.setLong("itemFatura", itemFatura.getChavePrimaria());
		}

		if (contrato != null) {
			query.setLong("chaveContrato", contrato.getChavePrimaria());
		}

		ControladorTarifa controladorTarifa = (ControladorTarifa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);

		return controladorTarifa.verificarListaTarifa(query.list());
	}

	/**
	 * Listar tarifas por ponto consumo sem contrato ativo.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param idContratoAditadoAlterado
	 *            the id contrato aditado alterado
	 * @return the collection
	 */
	@Override
	public Collection<Tarifa> listarTarifasPorPontoConsumoSemContratoAtivo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Long idContratoAditadoAlterado) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select cpcit.tarifa ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" cpcit ");

		hql.append(" inner join cpcit.contratoPontoConsumo cpc");
		hql.append(" inner join  cpc.contrato ctr");

		hql.append(" where cpcit.contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and cpc.chavePrimaria = cpcit.contratoPontoConsumo.chavePrimaria ");
		hql.append(" and cpc.contrato.chavePrimaria = ctr.chavePrimaria ");
		hql.append(" and ctr.chavePrimaria = :chaveContrato");
		hql.append(" and ctr.situacao in ( ");
		hql.append(SituacaoContrato.ADITADO);
		hql.append(" , ");
		hql.append(SituacaoContrato.ALTERADO);
		hql.append(" ) ");

		if (itemFatura != null) {
			hql.append(" and cpcit.itemFatura.chavePrimaria = :itemFatura ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePontoConsumo", pontoConsumo.getChavePrimaria());
		query.setLong("chaveContrato", idContratoAditadoAlterado);

		if (itemFatura != null) {
			query.setLong("itemFatura", itemFatura.getChavePrimaria());
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#calcularValorFornecimentoGas(br.com.ggas.geral.EntidadeConteudo,
	 * java.util.Date,
	 * java.util.Date, java.math.BigDecimal, java.util.Collection, br.com.ggas.faturamento.tarifa.TarifaVigencia, boolean, boolean,
	 * java.lang.Long,
	 * java.lang.Boolean)
	 */
	// calcularValorFornecimentoGasSimulacaoDadosReais
	@Override
	public DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, TarifaVigencia tarifaVigenciaParametro,
					boolean isConsiderarDescontos, boolean isFaturaAvulso, Contrato contrato, Boolean isConsumoDrawback)
					throws NegocioException {

		deleteme = new StringBuilder();
		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\n----------------- CÁLCULO SIMULAÇÃO DADOS REAIS INICIADO -----------------");
		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\nData Realização: ").append(new Date());

		if (itemFatura == null) {
			throw new ItemFaturaNaoInformadoException();
		}

		if (dataInicio == null) {
			throw new DataInicioNaoInformadoException();
		} else if (dataFim == null) {
			throw new DataFimNaoInformadoException();
		} else if (Util.compararDatas(dataInicio, dataFim) > 0) {
			throw new DataInicioPosteriorDataFimException();
		}

		if (consumoApurado == null) {
			throw new ConsumoNaoInformadoException();
		} else if (consumoApurado.compareTo(BigDecimal.ZERO) < 0) {
			throw new ConsumoNaoPositivoException();
		}

		if (colecaoPontoConsumo == null || colecaoPontoConsumo.isEmpty()) {
			throw new PontoConsumoNaoInformadoException();
		}
		if (tarifaVigenciaParametro == null) {
			throw new TarifaVigenciaNaoInformadaException();
		}

		return calcularValorFornecimentoGas(colecaoPontoConsumo, itemFatura, dataInicio, dataFim, consumoApurado, tarifaVigenciaParametro,
						null, isConsiderarDescontos, null, isFaturaAvulso, contrato, isConsumoDrawback, null, null);
	}

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param itemFatura
	 *            the item fatura
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @param consumoApurado
	 *            the consumo apurado
	 * @param colecaoPontoConsumo
	 *            the colecao ponto consumo
	 * @param isConsiderarDescontos
	 *            the is considerar descontos
	 * @param idContratoAditadoAlterado
	 *            the id contrato aditado alterado
	 * @param isFaturaAvulso
	 *            the is fatura avulso
	 * @param idContrato
	 *            the id contrato
	 * @param isConsumoDrawback
	 *            the is consumo drawback
	 * @return the dados fornecimento gas vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, boolean isConsiderarDescontos,
					Contrato contratoAditadoAlterado, Boolean isFaturaAvulso, Contrato contrato, Boolean isConsumoDrawback,
					String valorInformado, Map<String, Object> param) throws NegocioException {

		deleteme = new StringBuilder();
		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\n---------------------- CÁLCULO DADOS REAIS INICIADO ----------------------");
		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\nData Realização: ").append(new Date());

		if (itemFatura == null) {
			throw new ItemFaturaNaoInformadoException();
		}

		if (dataInicio == null) {
			throw new DataInicioNaoInformadoException();
		} else if (dataFim == null) {
			throw new DataLeituraAtualNaoInformadaOuInvalidaException();
		} else if (Util.compararDatas(dataInicio, dataFim) > 0) {
			throw new DataInicioPosteriorDataFimException();
		}

		if (consumoApurado == null) {
			throw new ConsumoNaoInformadoException();
		} else if (consumoApurado.compareTo(BigDecimal.ZERO) < 0) {
			throw new ConsumoNaoPositivoException();
		}

		if (colecaoPontoConsumo == null || colecaoPontoConsumo.isEmpty()) {
			throw new PontoConsumoNaoInformadoException();
		}

		return calcularValorFornecimentoGas(colecaoPontoConsumo, itemFatura, dataInicio, dataFim, consumoApurado, null, null,
						isConsiderarDescontos, contratoAditadoAlterado, isFaturaAvulso, contrato, isConsumoDrawback, valorInformado, param);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#calcularValorFornecimentoGas(br.com.ggas.geral.EntidadeConteudo,
	 * java.util.Date,
	 * java.util.Date, java.math.BigDecimal, br.com.ggas.faturamento.tarifa.Tarifa, boolean, boolean, java.lang.Long, java.lang.Boolean)
	 */
	// calcularValorFornecimentoGasDadosFicticios
	@Override
	public DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Tarifa tarifa, boolean isConsiderarDescontos, boolean isFaturaAvulso, Contrato contrato,
					Boolean isConsumoDrawback) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		deleteme = new StringBuilder();
		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\n--------------- CÁLCULO SIMULAÇÃO DADOS FICTÍCIOS INICIADO ---------------");
		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\nData Realização: ").append(new Date());

		if (itemFatura == null) {
			throw new ItemFaturaNaoInformadoException();
		}

		if (dataInicio == null) {
			throw new DataInicioNaoInformadoException();
		} else if (dataFim == null) {
			throw new DataFimNaoInformadoException();
		} else if (Util.compararDatas(dataInicio, dataFim) > 0) {
			throw new DataInicioPosteriorDataFimException();
		}

		if (consumoApurado == null) {
			throw new ConsumoNaoInformadoException();
		} else if (consumoApurado.compareTo(BigDecimal.ZERO) < 0) {
			throw new ConsumoNaoPositivoException();
		}
		if (tarifa == null) {
			throw new TarifaNaoInformadaException();
		}

		String paramICMS = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS);
		valorICMS = Long.valueOf(paramICMS);

		String valorSubst = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(TIPO_SUBSTITUTO);
		valorTipoSubstituto = Long.valueOf(valorSubst);

		return calcularValorFornecimentoGas(null, itemFatura, dataInicio, dataFim, consumoApurado, null, tarifa, isConsiderarDescontos,
						null, isFaturaAvulso, contrato, isConsumoDrawback, null, null);
	}

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param colecaoPontoConsumo
	 *            O ponto de consumo.
	 * @param itemFatura
	 *            the item fatura
	 * @param dataInicio
	 *            Data inicial do período de
	 *            medição.
	 * @param dataFim
	 *            Data final do período de
	 *            medição.
	 * @param consumoApurado
	 *            Consumo apurado no período de
	 *            medição. (Com as devidas
	 *            correções)
	 * @param tarifaVigenciaParametro
	 *            the tarifa vigencia parametro
	 * @param tarifa
	 *            Tarifa informada.
	 * @param isConsiderarDescontos
	 *            the is considerar descontos
	 * @param idContratoAditadoAlterado
	 *            the id contrato aditado alterado
	 * @param isFaturaAvulso
	 *            the is fatura avulso
	 * @param idContrato
	 *            the id contrato
	 * @param isConsumoDrawback
	 *            the is consumo drawback
	 * @return the dados fornecimento gas vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private DadosFornecimentoGasVO calcularValorFornecimentoGas(Collection<PontoConsumo> colecaoPontoConsumo, EntidadeConteudo itemFatura,
					Date dataInicio, Date dataFim, BigDecimal consumoApurado, TarifaVigencia tarifaVigenciaParametro, Tarifa tarifa,
					boolean isConsiderarDescontos, Contrato contratoAditadoAlterado, boolean isFaturaAvulso, Contrato contrato,
					Boolean isConsumoDrawback, String valorInformado, Map<String, Object> param) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorPrecoGas controladorPrecoGas = ServiceLocator.getInstancia().getControladorPrecoGas();
		ControladorRamoAtividadeSubstituicaoTributaria controladorRamoAtividadeSubstTrib = ServiceLocator.getInstancia()
						.getControladorRamoAtividadeSubstituicaoTributaria();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String pArredondamentoCalculo = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CALCULO);
		String pArredondamentoConsumo = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO);
		String pEscala = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);

		final Integer parametroArredondamentoCalculo;		

		if (pArredondamentoCalculo == null) {
			throw new PrecisaoNaoInformadaException();
		} else {
			parametroArredondamentoCalculo = Integer.valueOf(pArredondamentoCalculo);
		}
		final Integer parametroArredondamentoConsumo;
		if (pArredondamentoConsumo == null) {
			throw new PrecisaoNaoInformadaException();
		} else {
			parametroArredondamentoConsumo = Integer.valueOf(pArredondamentoConsumo);
		}
		final Integer parametroEscala;
		if (pEscala == null) {
			throw new EscalaNaoInformadaException();
		} else {
			parametroEscala = Integer.valueOf(pEscala);
		}

		PontoConsumo pontoConsumo = null;
		BigDecimal precoGas = null;

		if (colecaoPontoConsumo != null && !colecaoPontoConsumo.isEmpty()) {

			pontoConsumo = colecaoPontoConsumo.iterator().next();

			// Obter preço do gás que será usado
			// ao calcular valores com imposto.
			EntidadeConteudo contratoCompra = controladorContrato.obterContratoCompraPorPontoConsumo(pontoConsumo);
			if (contratoCompra != null) {
				precoGas = controladorPrecoGas.consultarPrecoGas(dataInicio, dataFim, itemFatura, contratoCompra);
			}
		}

		// Quando for simulação com dados
		// fictícios o ponto de consumo é null e o
		// período deve ser exatamente o
		// informado.
		DateTime dataInicioAjustada = new DateTime(dataInicio);
		if (dataInicio.compareTo(dataFim) < 0 && colecaoPontoConsumo != null) {
			dataInicioAjustada = dataInicioAjustada.plusDays(1);
		}

		List<PeriodoVigenciaDesconto> listaPeriodoTarifaVigenciaDesconto = mapearTarifasVigenciaPorDia(dataInicioAjustada.toDate(),
						dataFim, itemFatura, colecaoPontoConsumo, tarifaVigenciaParametro, tarifa, isConsiderarDescontos,
						contratoAditadoAlterado, contrato);

		// Essa validação não será necessária para
		// o caso de simulação com dados fictícios
		// pois o ponto de consumo não é
		// informado.
		// Se for fatura avulsa, ignorar essa validação
		if (!isFaturaAvulso && pontoConsumo != null) {
			this.validarTarifaVigenciaUtilizadas(listaPeriodoTarifaVigenciaDesconto, pontoConsumo, contratoAditadoAlterado);
		}

		// Obter todas as tarifas usadas no
		// periodo de leitura
		List<TarifaVigencia> tarifasUtilizadas = this.obterTarifasUtilizadas(listaPeriodoTarifaVigenciaDesconto);

		Map<Tarifa, Collection<TributoAliquota>> mapaTarifaTributos = listarTributosAliquotaPorTarifaPontoConsumo(tarifasUtilizadas,
						pontoConsumo, isConsumoDrawback);

		DadosFornecimentoGasVO dadosFornecimentoGasVO = new DadosFornecimentoGasVO();
		DadosVigenciaVO dadosVigenciaVO = null;

		String codigoMetodoReajusteCascata = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_METODO_DE_REAJUSTE_CASCATA);
		EntidadeConteudo metodoReajuste = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(codigoMetodoReajusteCascata));

		BigDecimal consumoAcumulado = null;
		if (metodoReajuste.getChavePrimaria() == EntidadeConteudo.CHAVE_CASCATA_COMPLEMENTAR) {
			consumoAcumulado = BigDecimal.ZERO;
		} else {
			ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia().getControladorHistoricoConsumo();
			if (pontoConsumo != null) {
				Long[] idsPontoConsumo = new Long[colecaoPontoConsumo.size()];
				int i=0;
				for(PontoConsumo p : colecaoPontoConsumo){
					idsPontoConsumo[i] = p.getChavePrimaria();
					i++;
				}
				consumoAcumulado = controladorHistoricoConsumo.
								consultarTotalConsumoApuradoAnoMesGrupoFaturamentoCorrente(idsPontoConsumo);
			}
		}

		Periodo periodo = new Periodo(dataInicioAjustada.toDate(), dataFim);
		BigDecimal qtdDiasPeriodoMedicao = BigDecimal.valueOf(periodo.getQuantidadeDiasIntervalo());
		BigDecimal consumoMedio = consumoApurado.divide(qtdDiasPeriodoMedicao, parametroEscala, parametroArredondamentoConsumo);

		if (listaPeriodoTarifaVigenciaDesconto != null && !listaPeriodoTarifaVigenciaDesconto.isEmpty()) {

			Iterator<PeriodoVigenciaDesconto> itPvd = listaPeriodoTarifaVigenciaDesconto.iterator();
			Iterator<PeriodoVigenciaDesconto> itPvdTemp = listaPeriodoTarifaVigenciaDesconto.iterator();
			PeriodoVigenciaDesconto pvd = null;
			BigDecimal consumoPeriodo = null;
			BigDecimal somaConsumos = BigDecimal.ZERO;
			Collection<TributoAliquota> colecaoTributoAliquota = new ArrayList<TributoAliquota>();
			BigDecimal periodoTotal = BigDecimal.ZERO;
			while(itPvdTemp.hasNext()) {
				PeriodoVigenciaDesconto pvdAux = itPvdTemp.next();
				BigDecimal periodoLeitura = BigDecimal.valueOf(DataUtil.diferencaDiasEntreDatas(pvdAux.periodo.dataInicio.toDate(),
						pvdAux.periodo.dataFim.toDate()));
				periodoTotal = periodoTotal.add(periodoLeitura).add(BigDecimal.ONE);
			}
			while (itPvd.hasNext()) {
				pvd = itPvd.next();
				
				BigDecimal peridoLeituraDias = BigDecimal.valueOf(DataUtil.diferencaDiasEntreDatas(pvd.periodo.dataInicio.toDate(),
						pvd.periodo.dataFim.toDate())).add(BigDecimal.ONE);	
				
				if(peridoLeituraDias.compareTo(BigDecimal.ZERO) == 0) {
					peridoLeituraDias = BigDecimal.ONE;
				}
	
				deleteme.append("\n\n..................................................................")
						.append("..................................................\n");
				deleteme.append("\n................ Período (")
						.append(Util.converterDataParaStringSemHora(pvd.periodo.dataInicio.toDate(), Constantes.FORMATO_DATA_BR))
						.append(" Até ")
						.append(Util.converterDataParaStringSemHora(pvd.periodo.dataFim.toDate(), Constantes.FORMATO_DATA_BR))
						.append(") Vigência: ").append(pvd.tarifaVigencia.getChavePrimaria());

				TarifaVigencia tarifaVigencia = pvd.tarifaVigencia;

				BigDecimal variacaoCambial = this.obterVariacaoCambial(tarifaVigencia, dataFim);
				if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
					deleteme.append("\n................ Tarifa em dolar. Data: ")
							.append(Util.converterDataParaStringSemHora(dataFim, Constantes.FORMATO_DATA_BR))
							.append(" Valor da taxa de câmbio: ")
							.append(variacaoCambial);
				}

				colecaoTributoAliquota = mapaTarifaTributos.get(tarifaVigencia.getTarifa());

				Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> colecaoDescontosFaixa = null;

				if (pvd.desconto != null) {
					colecaoDescontosFaixa = mapearTarifaFaixaDescontoPorTarifaVigenciaDesconto(pvd.desconto);
				}

				TarifaVigencia pvdTarifaVigencia = pvd.tarifaVigencia;
				Periodo pvdPeriodo = pvd.periodo;
				BigDecimal qtdDiasIntervaloPeriodo = BigDecimal.valueOf(pvdPeriodo.getQuantidadeDiasIntervalo());
				BigDecimal percentualUtilizado = qtdDiasIntervaloPeriodo.divide(qtdDiasPeriodoMedicao, ESCALA, BigDecimal.ROUND_DOWN);

				if (itPvd.hasNext()) {
					consumoPeriodo = consumoMedio.multiply(qtdDiasIntervaloPeriodo);
				} else {
					consumoPeriodo = consumoApurado.subtract(somaConsumos);
				}
				somaConsumos = somaConsumos.add(consumoPeriodo);
				
				BigDecimal periodoTemp = new BigDecimal(0);
				if(periodoTotal.compareTo(BigDecimal.ZERO) !=0 ) {
					periodoTemp = peridoLeituraDias.divide(periodoTotal, 15, RoundingMode.HALF_UP);
				}
				
				if (tarifaVigencia.getBaseApuracao().getChavePrimaria() == EntidadeConteudo.CHAVE_MEDIA_DIARIA
						|| checarTarifaDiariaColetor(tarifaVigencia, pontoConsumo)) {

					if (!isFaturaAvulso) {
						deleteme.append("\n...... Base de Apuração: Média Diária");
						dadosVigenciaVO = calculoMediaDiaria(pontoConsumo, itemFatura, pvdPeriodo, pvdTarifaVigencia,
										colecaoTributoAliquota, consumoMedio, colecaoDescontosFaixa, parametroEscala,
										parametroArredondamentoCalculo, variacaoCambial, precoGas, dataFim, BigDecimal.ONE);
					} else {
						deleteme.append("\n...... Base de Apuração: Periódica fatura Avulsa");
						dadosVigenciaVO = calculoPeriodico(pontoConsumo, itemFatura, consumoPeriodo, pvdPeriodo, pvdTarifaVigencia,
										colecaoTributoAliquota, colecaoDescontosFaixa, consumoAcumulado, parametroEscala,
										parametroArredondamentoCalculo, variacaoCambial, percentualUtilizado, consumoApurado, precoGas,
										valorInformado, dataFim, param,periodoTemp);

					}

				} else if (tarifaVigencia.getBaseApuracao().getChavePrimaria() == EntidadeConteudo.CHAVE_DIARIA) {

					if (!isFaturaAvulso) {
						deleteme.append("\n...... Base de Apuração: Diária");
						dadosVigenciaVO = calculoDiario(colecaoPontoConsumo, itemFatura, pvdPeriodo, pvdTarifaVigencia,
										colecaoTributoAliquota, colecaoDescontosFaixa, parametroEscala, parametroArredondamentoCalculo,
										variacaoCambial, consumoApurado, precoGas, dataFim, periodoTemp);
					} else {
						deleteme.append("\n...... Base de Apuração: Periódica fatura Avulsa");
						dadosVigenciaVO = calculoPeriodico(pontoConsumo, itemFatura, consumoPeriodo, pvdPeriodo, pvdTarifaVigencia,
										colecaoTributoAliquota, colecaoDescontosFaixa, consumoAcumulado, parametroEscala,
										parametroArredondamentoCalculo, variacaoCambial, percentualUtilizado, consumoApurado, precoGas,
										valorInformado, dataFim,param,periodoTemp);

					}

				} else if (tarifaVigencia.getBaseApuracao().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICA) {

					deleteme.append("\n...... Base de Apuração: Periódica");
					dadosVigenciaVO = calculoPeriodico(pontoConsumo, itemFatura, consumoPeriodo, pvdPeriodo, pvdTarifaVigencia,
									colecaoTributoAliquota, colecaoDescontosFaixa, consumoAcumulado, parametroEscala,
									parametroArredondamentoCalculo, variacaoCambial, percentualUtilizado, consumoApurado, precoGas,
									valorInformado, dataFim, param, periodoTemp);


					if (valorInformado != null) {

						BigDecimal valor = BigDecimal.ZERO;
						try {
							valor = Util.converterCampoStringParaValorBigDecimal("valor", valorInformado, Constantes.FORMATO_VALOR_BR,
											Constantes.LOCALE_PADRAO);
						} catch (FormatoInvalidoException e) {
							LOG.info(e.getMessage(), e);
						}

						if (valor.compareTo(BigDecimal.ZERO) > 0) {
							dadosVigenciaVO.setConsumoFaturado(BigDecimal.ZERO);
						}
					}
				} else {

					throw new BaseApuracaoInvalidaException();
				}

				if (dadosFornecimentoGasVO.getDadosVigencia() != null) {

					dadosFornecimentoGasVO.getDadosVigencia().add(dadosVigenciaVO);

					dadosFornecimentoGasVO.setValorTotalGas(dadosFornecimentoGasVO.getValorTotalGas().add(dadosVigenciaVO.getValorTotal()));
					dadosFornecimentoGasVO.setValorTotalGasSemTributo(dadosFornecimentoGasVO.getValorTotalGasSemTributo().add(
									dadosVigenciaVO.getValorTotalSemTributo()));

					dadosFornecimentoGasVO.setValorTotalGasSemSubstituicao(dadosFornecimentoGasVO.getValorTotalGasSemSubstituicao().add(
									dadosVigenciaVO.getValorTotalSemSubstituicao()));

					Collection<DadosTributoVO> colecaoTributos = mesclarColecoesDadosTributoVO(dadosFornecimentoGasVO.getColecaoTributos(),
									dadosVigenciaVO.getColecaoTributos());
					dadosFornecimentoGasVO.setColecaoTributos(colecaoTributos);

					dadosFornecimentoGasVO.setValorTotalDescontos(dadosFornecimentoGasVO.getValorTotalDescontos().add(
									dadosVigenciaVO.getValorDescontos()));

					dadosFornecimentoGasVO.setValorTotalComDescontoComImposto(dadosFornecimentoGasVO.
									getValorTotalComDescontoComImposto().add(dadosVigenciaVO.
													getValorTotalComDescontoComImposto()));

				} else {

					List<DadosVigenciaVO> colecaoDadosVigencia = new ArrayList<DadosVigenciaVO>();
					colecaoDadosVigencia.add(dadosVigenciaVO);
					dadosFornecimentoGasVO.setDadosVigencia(colecaoDadosVigencia);

					Collection<DadosTributoVO> colecaoTributos = dadosVigenciaVO.getColecaoTributos();
					dadosFornecimentoGasVO.setColecaoTributos(colecaoTributos);

					dadosFornecimentoGasVO.setValorTotalGas(dadosVigenciaVO.getValorTotal());
					dadosFornecimentoGasVO.setValorTotalDescontos(dadosVigenciaVO.getValorDescontos());
					dadosFornecimentoGasVO.setValorTotalGasSemSubstituicao(dadosVigenciaVO.getValorTotalSemSubstituicao());
					dadosFornecimentoGasVO.setValorTotalGasSemTributo(dadosVigenciaVO.getValorTotalSemTributo());
					dadosFornecimentoGasVO.setValorTotalComDescontoComImposto(dadosVigenciaVO.getValorTotalComDescontoComImposto());
				}

				BigDecimal somaConsumo = BigDecimal.ZERO;

				for (DadosVigenciaVO vigencia : dadosFornecimentoGasVO.getDadosVigencia()) {
					somaConsumo = somaConsumo.add(vigencia.getConsumoFaturado());
				}
				// TODO validar regra para definir
				// valor final do metro cubico
				// para tarifa minima com volume
				// igual a zero.
				if (somaConsumo.compareTo(BigDecimal.ZERO) == 0) {
					somaConsumo = BigDecimal.ONE;
				}
				dadosFornecimentoGasVO.setValorFinalMetroCubicoGas(dadosFornecimentoGasVO.getValorTotalGas().divide(somaConsumo,
								parametroEscala, parametroArredondamentoCalculo));
				dadosFornecimentoGasVO.setValorFinalMetroCubicoGasSemSubstituicao(dadosFornecimentoGasVO.getValorTotalGasSemSubstituicao());
				dadosFornecimentoGasVO.setValorFinalMetroCubicoGasSemTributo(dadosFornecimentoGasVO.getValorTotalGasSemTributo().divide(
								somaConsumo, parametroEscala, parametroArredondamentoCalculo));

				if (dadosVigenciaVO.getConsumoApurado() != null && consumoAcumulado != null) {
					consumoAcumulado = consumoAcumulado.add(dadosVigenciaVO.getConsumoApurado());
					deleteme.append("\n................ Acumulado: ").append(consumoAcumulado);
				}

			}

			// Aplica o desconto do grupo econômico
			aplicarDescontoGrupoEconomico(controladorFatura, pontoConsumo, dadosFornecimentoGasVO);

			this.calcularSubstituicaoTributaria(dadosFornecimentoGasVO, pontoConsumo, precoGas,
					parametroArredondamentoCalculo, colecaoTributoAliquota, dataFim);

		} else {
			// TODO verificar mensagem.
			throw new PeriodoCalculoFornecimentoGasException();
		}

		deleteme.append("\n........................................................");
		deleteme.append("\n........        DADOS FORNECIMENTO GAS          ........");
		deleteme.append("\n........................................................");

		deleteme.append("\nvalorTotalGas: ").append(dadosFornecimentoGasVO.getValorTotalGas());
		deleteme.append("\nvalorTotalDescontos: ").append(dadosFornecimentoGasVO.getValorTotalDescontos());
		deleteme.append("\nvalorFinalMetroCubico: ").append(dadosFornecimentoGasVO.getValorFinalMetroCubicoGas());

		deleteme.append("\n--------------------------------------------------------------------------");
		deleteme.append("\n--------------------------- CÁLCULO FINALIZADO ---------------------------");
		deleteme.append("\n--------------------------------------------------------------------------");

		// Atualizar a base de calculo do PIS,
		// COFINS quando tiver substituto
		// PETROBRAS e tipo MVA.
		RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstTributaria = null;
		// Verifica se tem substituição tributária
		// com substituto PETROBRAS e MVA, caso
		// seja diferente disso a substituição
		// será feita no final do cálculo.
		Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> mapaRamoAtividadeSubstituicaoTributaria;
		mapaRamoAtividadeSubstituicaoTributaria = (Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>>)
						FaturamentoCache.getInstance().get(FaturamentoCache.RAMO_ATIVIDADE);
		if (pontoConsumo != null) {
			RamoAtividade ramoAtividade = pontoConsumo.getRamoAtividade();
			if(pontoConsumo.getDadosResumoPontoConsumo().getRamoAtividadeSubstituicaoTributaria() != null){
				ramoAtividadeSubstTributaria = pontoConsumo.getDadosResumoPontoConsumo().getRamoAtividadeSubstituicaoTributaria();
			} else if(mapaRamoAtividadeSubstituicaoTributaria != null) {
				Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria;
				listaRamoAtividadeSubstituicaoTributaria = mapaRamoAtividadeSubstituicaoTributaria.get(ramoAtividade);
				ramoAtividadeSubstTributaria = controladorRamoAtividadeSubstTrib
								.escolherRamoAtividadeSubstituicaoTributariaEmVigencia(listaRamoAtividadeSubstituicaoTributaria, dataFim);
				if(ramoAtividadeSubstTributaria != null){
					pontoConsumo.getDadosResumoPontoConsumo().setRamoAtividadeSubstituicaoTributaria(ramoAtividadeSubstTributaria);
					pontoConsumo.getDadosResumoPontoConsumo().setTemSubstituicaoTributaria(Boolean.TRUE);
				}
			} else {
				ramoAtividadeSubstTributaria =
						controladorRamoAtividadeSubstTrib.obterRamoAtividadeSubstTribPorRamoAtividade(ramoAtividade, dataFim);
				if (ramoAtividadeSubstTributaria != null) {
					pontoConsumo.getDadosResumoPontoConsumo().setRamoAtividadeSubstituicaoTributaria(ramoAtividadeSubstTributaria);
					pontoConsumo.getDadosResumoPontoConsumo().setTemSubstituicaoTributaria(Boolean.TRUE);
				}
			}
		}

		String paramPIS = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PIS);
		Long valorPIS = Long.valueOf(paramPIS);
		String paramCOFINS = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COFINS);
		Long valorCOFINS = Long.valueOf(paramCOFINS);

		if (ramoAtividadeSubstTributaria != null
						&& ramoAtividadeSubstTributaria.getTipoSubstituicao().getChavePrimaria() == EntidadeConteudo.CHAVE_MVA
						&& valorTipoSubstituto != null && valorTipoSubstituto == EntidadeConteudo.CHAVE_PETROBRAS) {
			for (DadosTributoVO dadosTributoVO : dadosFornecimentoGasVO.getColecaoTributos()) {
				if (dadosTributoVO.getTributoAliquota().getTributo().getChavePrimaria() == valorPIS
								|| dadosTributoVO.getTributoAliquota().getTributo().getChavePrimaria() == valorCOFINS) {
					dadosTributoVO.setBaseCalculo(dadosFornecimentoGasVO.getIcmsSubstituto().getBaseCalculo());
					//aliquota em valor
					dadosTributoVO.setValor(dadosFornecimentoGasVO.getIcmsSubstituto().getBaseCalculo()
									.multiply(dadosTributoVO.getTributoAliquota().getValorAliquota()));
				}
			}
		}

		if ("S".equals(Constantes.GERAR_LOG_FATURAMENTO)) {
			LOG.info(deleteme);
		}

		return dadosFornecimentoGasVO;

	}

	/**
	 * Aplicar desconto grupo economico.
	 *
	 * @param controladorFatura
	 *            the controlador fatura
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dadosFornecimentoGasVO
	 *            the dados fornecimento gas vo
	 */
	// Pesquisa o mapa com os pontos de consumo e seus descontos associados
	private void aplicarDescontoGrupoEconomico(ControladorFatura controladorFatura, PontoConsumo pontoConsumo,
					DadosFornecimentoGasVO dadosFornecimentoGasVO) {

		BigDecimal valorDesconto = controladorFatura.obterDescontoGrupoEconomicoPorPontoConsumo(pontoConsumo);

		if (valorDesconto != null) {
			BigDecimal valorTotalGas = dadosFornecimentoGasVO.getValorTotalGas().subtract(valorDesconto);

			dadosFornecimentoGasVO.setValorTotalGas(valorTotalGas);
		}
	}

	/**
	 * Validar tarifa vigencia utilizadas.
	 *
	 * @param listaPeriodoTarifaVigenciaDesconto
	 *            the lista periodo tarifa vigencia desconto
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param idContratoAditadoAlterado
	 *            the id contrato aditado alterado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarTarifaVigenciaUtilizadas(List<PeriodoVigenciaDesconto> listaPeriodoTarifaVigenciaDesconto,
					PontoConsumo pontoConsumo, Contrato contratoAditadoAlterado) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// obter tipo de medição do contrato do
		// ponto de consumo
		TipoMedicao tipoMedicaoPontoConsumo = controladorContrato.obterTipoMedicaoContratoPontoConsumo(pontoConsumo,
						contratoAditadoAlterado);

		// Obter parametro que indica tipo de
		// medição diária.
		String tipoMedicaoParametro = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDICAO_DIARIA);

		// Varre a lista de Tarifa Vigência
		// procurando se existe alguma com base de
		// apuração diária.
		if (listaPeriodoTarifaVigenciaDesconto != null && !listaPeriodoTarifaVigenciaDesconto.isEmpty() && pontoConsumo != null) {

			for (PeriodoVigenciaDesconto periodoVigenciaDesconto : listaPeriodoTarifaVigenciaDesconto) {

				if (periodoVigenciaDesconto.tarifaVigencia != null
								&& periodoVigenciaDesconto.tarifaVigencia.getBaseApuracao() != null
								&& periodoVigenciaDesconto.tarifaVigencia.getBaseApuracao().getChavePrimaria() == EntidadeConteudo.CHAVE_DIARIA
								&& tipoMedicaoPontoConsumo.getChavePrimaria() != Long.parseLong(tipoMedicaoParametro)
								&& !checarTarifaDiariaColetor(periodoVigenciaDesconto.tarifaVigencia, pontoConsumo)) {

					// Se existir alguma tarifa
					// com base de apuração
					// diária, verifica se o tipo
					// de medição do ponto de
					// consumo também é diária, se
					// não for lança exceção.
					throw new TipoMedicaoException();
				}
			}
		}
	}

	/**
	 * Calcular substituicao tributaria.
	 *
	 * @param dadosFornecimentoGasVO            the dados fornecimento gas vo
	 * @param pontoConsumo            the ponto consumo
	 * @param itemFatura            the item fatura
	 * @param precoGasParametro            the preco gas parametro
	 * @param parametroEscala            the parametro escala
	 * @param parametroArredondamentoCalculo            the parametro arredondamento calculo
	 * @param colecaoTributoAliquota            the colecao tributo aliquota
	 * @param ultimaLeitura the ultima leitura
	 * @throws NegocioException             the negocio exception
	 */
	private void calcularSubstituicaoTributaria(DadosFornecimentoGasVO dadosFornecimentoGasVO,
			PontoConsumo pontoConsumo,
			BigDecimal precoGasParametro,
			Integer parametroArredondamentoCalculo,
			Collection<TributoAliquota> colecaoTributoAliquota,
			Date ultimaLeitura) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorRamoAtividadeSubstituicaoTributaria controladorRamoAtividadeSubstTrib = ServiceLocator.getInstancia()
						.getControladorRamoAtividadeSubstituicaoTributaria();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		RamoAtividadeSubstituicaoTributaria ramoAtividadeSubsTributaria = null;
		BigDecimal percentualIcms = BigDecimal.ZERO;
		BigDecimal baseCalculoSubstituto = BigDecimal.ZERO;
		BigDecimal valorIcmsSubstituto = BigDecimal.ZERO;

		// Varre a coleção de tributos para obter
		// o percentual do ICMS
		Collection<DadosTributoVO> colecaoTributos = dadosFornecimentoGasVO.getColecaoTributos();
		if (valorICMS == null) {
			String paramICMS = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS);
			valorICMS = Long.valueOf(paramICMS);
		}

		if (colecaoTributos != null && !colecaoTributos.isEmpty()) {
			for (DadosTributoVO dadosTributoVO : colecaoTributos) {
				if (dadosTributoVO.getTributoAliquota() != null && dadosTributoVO.getTributoAliquota().getTributo() != null
								&& valorICMS != null
								// &&
								// !paramICMS.isEmpty()
								&& dadosTributoVO.getTributoAliquota().getTributo().getChavePrimaria() == valorICMS) {
					//aliquota em valor
					percentualIcms = dadosTributoVO.getTributoAliquota().getValorAliquota();
				}
			}

			ramoAtividadeSubsTributaria = definirRamoAtividadeSubsTributaria(
							pontoConsumo, controladorRamoAtividadeSubstTrib, ramoAtividadeSubsTributaria, ultimaLeitura);

			// Só haverá substituição tributária
			// se tiver
			// 'RamoAtividadeSubstituicaoTributaria'
			// e se tiver ICMS
			if (ramoAtividadeSubsTributaria != null && percentualIcms.compareTo(BigDecimal.ZERO) > 0) {

				String substituto = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(TIPO_SUBSTITUTO);
				long idSubstituto;
				if (substituto != null) {
					idSubstituto = Long.parseLong(substituto);
				} else {
					idSubstituto = -1;
				}

				// Calcula o ICMS Total
				BigDecimal icmsTotal = null;
				BigDecimal consumoTotal = this.obterConsumoTotal(dadosFornecimentoGasVO);

				if (ramoAtividadeSubsTributaria.getTipoSubstituicao().getChavePrimaria() == EntidadeConteudo.CHAVE_MVA) {

					BigDecimal precoGas = null;

					/**
					 * Caso o substituto seja
					 * 'PETROBRAS' e
					 * tipoSubstituicao seja 'MVA'
					 * a substituição é realizada
					 * no
					 * momento em que estão sendo
					 * obtidos os valores com
					 * imposto.
					 */
					if (idSubstituto == EntidadeConteudo.CHAVE_CDL) {

						deleteme.append("\n------------------Substituição Tributária MVA - CDL ------------------------");

						// Se for MVA e CDL o
						// preço do gás será a
						// Tarifa Média com
						// impostos.
						precoGas = dadosFornecimentoGasVO.getValorFinalMetroCubicoGas();

						// Multiplicar precoGas
						// por consumo pois esse
						// preço é para cada m³
						BigDecimal precoGasTotal = precoGas.multiply(consumoTotal);

						BigDecimal percentualMVA = ramoAtividadeSubsTributaria.getPercentualSubstituto();
						BigDecimal percentualSubstitutoMaisUm = BigDecimal.ONE.add(percentualMVA);

						// Base de cálculo = valor
						// total * (1 + MVA)
						baseCalculoSubstituto = precoGasTotal.multiply(percentualSubstitutoMaisUm);

						// icmsTotal = (valor
						// total * (1 + MVA)) *
						// icms
						icmsTotal = baseCalculoSubstituto.multiply(percentualIcms);

						deleteme.append("\n Preço do Gás: ").append(precoGas);

						// Valor do ICMS
						// Substituto = ICMS Total
						// - ICMS Normal.
						BigDecimal valorIcmsNormal = dadosFornecimentoGasVO.getValorTotalGas().multiply(percentualIcms);
						valorIcmsSubstituto = icmsTotal.subtract(valorIcmsNormal);

						DadosTributoVO tributoSubstituto = new DadosTributoVO();
						tributoSubstituto.setBaseCalculo(baseCalculoSubstituto);
						tributoSubstituto.setValor(valorIcmsSubstituto);
						dadosFornecimentoGasVO.setIcmsSubstituto(tributoSubstituto);

						deleteme.append("\n Base de Cálculo ICMS Substituto: ").append(baseCalculoSubstituto);
						deleteme.append("\n Valor ICMS Substituto: ").append(valorIcmsSubstituto);

					} else if (idSubstituto == EntidadeConteudo.CHAVE_PETROBRAS) {

						// Esse 'else' é apenas
						// para montar informações
						// do substituto para a
						// tela de simulação.

						BigDecimal percentualSubstitutoMaisUm = BigDecimal.ONE.add(ramoAtividadeSubsTributaria.getPercentualSubstituto());
						BigDecimal baseICMSSubstituto = precoGasParametro.multiply(percentualSubstitutoMaisUm);
						baseCalculoSubstituto = baseICMSSubstituto.multiply(consumoTotal);

						DadosTributoVO tributoSubstituto = new DadosTributoVO();
						tributoSubstituto.setBaseCalculo(baseCalculoSubstituto);

						icmsTotal = baseCalculoSubstituto.multiply(percentualIcms);

						BigDecimal icmsNormal = dadosFornecimentoGasVO.getValorFinalMetroCubicoGasSemSubstituicao();
						icmsNormal = icmsNormal.multiply(percentualIcms);

						// Valor do ICMS
						// Substituto = ICMS Total
						// - ICMS Normal.
						tributoSubstituto.setValor(icmsTotal.subtract(icmsNormal));
						dadosFornecimentoGasVO.setIcmsSubstituto(tributoSubstituto);

					}

				} else if (ramoAtividadeSubsTributaria.getTipoSubstituicao().getChavePrimaria() == EntidadeConteudo.CHAVE_PMPF) {

					deleteme.append("\n------------------Substituição Tributária PMPF ------------------------");

					BigDecimal fator = getControladorFatura().fatorReducaoBaseCalculoTributoIcms();
					BigDecimal pmpf = ramoAtividadeSubsTributaria.getValorSubstituto();
					baseCalculoSubstituto = pmpf.multiply(consumoTotal).multiply(fator);
					icmsTotal = baseCalculoSubstituto.multiply(percentualIcms);

					// Valor do ICMS Substituto =
					// ICMS Total - ICMS Normal.
					BigDecimal valorIcmsNormal = dadosFornecimentoGasVO.getValorTotalGas()
									.multiply(fator).multiply(percentualIcms);
					valorIcmsSubstituto = icmsTotal.subtract(valorIcmsNormal);

					DadosTributoVO tributoSubstituto = new DadosTributoVO();
					tributoSubstituto.setBaseCalculo(baseCalculoSubstituto);
					tributoSubstituto.setValor(valorIcmsSubstituto);
					dadosFornecimentoGasVO.setIcmsSubstituto(tributoSubstituto);

					deleteme.append("\n Base de Cálculo ICMS Substituto: ").append(baseCalculoSubstituto);
					deleteme.append("\n Valor ICMS Substituto: ").append(valorIcmsSubstituto);

				} else {
					throw new SubstituicaoTributariaException();
				}
			}

			for (DadosTributoVO tributo : colecaoTributos) {
				if (NumeroUtil.nulo(tributo.getBaseCalculo())) {
					tributo.setBaseCalculo(dadosFornecimentoGasVO.getValorTotalGas());
				}
			}

		}

	}

	/**
	 * DefinirRamoAtividadedeSubsTributaria
	 * @param pontoConsumo
	 * @param controladorRamoAtividadeSubstTrib
	 * @param atividadeSubsTributariaRamo
	 * @param ultimaLeitura
	 * @return
	 * @throws NegocioException
	 */
	private RamoAtividadeSubstituicaoTributaria definirRamoAtividadeSubsTributaria(PontoConsumo pontoConsumo,
			ControladorRamoAtividadeSubstituicaoTributaria controladorRamoAtividadeSubstTrib,
			RamoAtividadeSubstituicaoTributaria atividadeSubsTributariaRamo, Date ultimaLeitura) throws NegocioException {

		RamoAtividadeSubstituicaoTributaria ramoAtividadeSubsTributaria = atividadeSubsTributariaRamo;
		Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> mapaRamoAtividadeSubstituicaoTributaria;
		mapaRamoAtividadeSubstituicaoTributaria = (Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>>) FaturamentoCache
				.getInstance().get(FaturamentoCache.RAMO_ATIVIDADE);
		if (pontoConsumo != null) {
			RamoAtividade ramoAtividade = pontoConsumo.getRamoAtividade();
			if (pontoConsumo.getDadosResumoPontoConsumo().getRamoAtividadeSubstituicaoTributaria() != null) {
				ramoAtividadeSubsTributaria = pontoConsumo.getDadosResumoPontoConsumo().getRamoAtividadeSubstituicaoTributaria();
			} else if (mapaRamoAtividadeSubstituicaoTributaria != null) {
				Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria;
				listaRamoAtividadeSubstituicaoTributaria = mapaRamoAtividadeSubstituicaoTributaria.get(ramoAtividade);
				ramoAtividadeSubsTributaria = controladorRamoAtividadeSubstTrib
						.escolherRamoAtividadeSubstituicaoTributariaEmVigencia(listaRamoAtividadeSubstituicaoTributaria, ultimaLeitura);
				if (ramoAtividadeSubsTributaria != null) {
					pontoConsumo.getDadosResumoPontoConsumo().setRamoAtividadeSubstituicaoTributaria(ramoAtividadeSubsTributaria);
					pontoConsumo.getDadosResumoPontoConsumo().setTemSubstituicaoTributaria(Boolean.TRUE);
				}
			} else {
				ramoAtividadeSubsTributaria =
						controladorRamoAtividadeSubstTrib.obterRamoAtividadeSubstTribPorRamoAtividade(ramoAtividade, ultimaLeitura);
				if (ramoAtividadeSubsTributaria != null) {
					pontoConsumo.getDadosResumoPontoConsumo().setRamoAtividadeSubstituicaoTributaria(ramoAtividadeSubsTributaria);
					pontoConsumo.getDadosResumoPontoConsumo().setTemSubstituicaoTributaria(Boolean.TRUE);
				}
			}
		}

		return ramoAtividadeSubsTributaria;
	}

	/**
	 * Obter consumo total.
	 *
	 * @param dadosFornecimentoGasVO
	 *            the dados fornecimento gas vo
	 * @return the big decimal
	 */
	private BigDecimal obterConsumoTotal(DadosFornecimentoGasVO dadosFornecimentoGasVO) {

		Collection<DadosVigenciaVO> listaDadosVigencia = dadosFornecimentoGasVO.getDadosVigencia();
		BigDecimal consumoTotal = BigDecimal.ZERO;

		for (DadosVigenciaVO dadosVigencia : listaDadosVigencia) {
			consumoTotal = consumoTotal.add(dadosVigencia.getConsumoApurado());
		}
		return consumoTotal;
	}

	/**
	 * Mesclar colecoes dados tributo vo.
	 *
	 * @param colecao1
	 *            the colecao1
	 * @param colecao2
	 *            the colecao2
	 * @return the collection
	 */
	private Collection<DadosTributoVO> mesclarColecoesDadosTributoVO(Collection<DadosTributoVO> colecao1,
					Collection<DadosTributoVO> colecao2) {

		Collection<DadosTributoVO> retorno = colecao1;
		if (colecao1 != null && !colecao1.isEmpty() && colecao2 != null && !colecao2.isEmpty()) {
			for (DadosTributoVO vo1 : retorno) {
				for (DadosTributoVO vo2 : colecao2) {
					if (vo1.getTributoAliquota().getChavePrimaria() == vo2.getTributoAliquota().getChavePrimaria()) {
						vo1.setBaseCalculo(vo1.getBaseCalculo().add(vo2.getBaseCalculo()));
						vo1.setValor(vo1.getValor().add(vo2.getValor()));
					}
				}
			}

			for (DadosTributoVO vo2 : colecao2) {
				if (!retorno.contains(vo2)) {
					retorno.add(vo2);
				}
			}
		}

		return retorno;
	}

	/**
	 * Obter tarifas utilizadas.
	 *
	 * @param listaTarifasVigenciaDia
	 *            the lista tarifas vigencia dia
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<TarifaVigencia> obterTarifasUtilizadas(List<PeriodoVigenciaDesconto> listaTarifasVigenciaDia) {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		List<TarifaVigencia> retorno = new ArrayList<TarifaVigencia>();
		if (listaTarifasVigenciaDia != null && !listaTarifasVigenciaDia.isEmpty()) {

			TarifaVigencia tarifaVigencia = null;
			String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

			for (PeriodoVigenciaDesconto pvd : listaTarifasVigenciaDia) {
				tarifaVigencia = pvd.tarifaVigencia;
				if (tarifaVigencia.getStatus() != null && tarifaVigencia.getStatus().getChavePrimaria() == Long.parseLong(statusAutorizado)
								&& !retorno.contains(tarifaVigencia)) {
					retorno.add(tarifaVigencia);
				}
			}
		}

		return retorno;
	}

	/**
	 * Listar tributos aliquota por tarifa ponto consumo.
	 *
	 * @param tarifasUtilizadas
	 *            the tarifas utilizadas
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param isConsumoDrawback
	 *            the is consumo drawback
	 * @return the hash map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<Tarifa, Collection<TributoAliquota>> listarTributosAliquotaPorTarifaPontoConsumo(
					Collection<TarifaVigencia> tarifasUtilizadas, PontoConsumo pontoConsumo, Boolean isConsumoDrawback)
					throws NegocioException {

		Collection<PontoConsumoTributoAliquota> colecaoTributoAliquotaPontoConsumo = new HashSet<PontoConsumoTributoAliquota>();
		Collection<TributoAliquota> tributosUtilizados = new HashSet<TributoAliquota>();

		Collection<Long> chavesTributosIsentosPonto = new HashSet<Long>();

		Map<Tarifa, Collection<TributoAliquota>> colecaoTarifaTributoAliquota = new HashMap<Tarifa, Collection<TributoAliquota>>();
		ControladorTributo controladorTributo = (ControladorTributo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);

		// Caso não haja ponto de consumo, utiliza as aliquotas vigentes das rubricas.
		// Caso haja ponto de consumo, verifica se o ponto de consumo é isento para algum tributo e se há aliquota diferenciada.

		
		if (pontoConsumo != null) {

			// obter os tributo aliquota do ponto de consumo (não isentos)
			colecaoTributoAliquotaPontoConsumo = controladorTributo.obterPorcentagemAliquotaPorPontoConsumo(pontoConsumo, Boolean.FALSE,
							Boolean.FALSE);

			chavesTributosIsentosPonto = this.obterChavesTributosIsentos(pontoConsumo, isConsumoDrawback);
			
		}

		for (TarifaVigencia tarifaVigencia : tarifasUtilizadas) {

			Collection<Long> chavesTributos = getChavesTributosTarifaVigencia(tarifaVigencia);
			
			Collection<TributoAliquota> colecaoTributoAliquotaTarifa = controladorTributo
					.listarTributoAliquotaPorTarifa(
							chavesTributos,
							tarifaVigencia.getTarifa().getChavePrimaria(),
							chavesTributosIsentosPonto);
			
			colecaoTributoAliquotaTarifa = filtrarTributoAliquotaPorTributo(tarifaVigencia, colecaoTributoAliquotaTarifa);
			
			// Junta as duas coleções - Tributos do Ponto de Consumo e da tarifa corrente.

			for (TributoAliquota tributoAliquota : colecaoTributoAliquotaTarifa) {
				for (PontoConsumoTributoAliquota pontoConsumoTributoAliquota : colecaoTributoAliquotaPontoConsumo) {
					if (tributoAliquota.getTributo().equals(pontoConsumoTributoAliquota.getTributo())) {
						getControladorTabelaAuxiliar().definirValorAliquotaTributoPorPercentual(tributoAliquota,
								pontoConsumoTributoAliquota.getPorcentagemAliquota());
						break;
					}
				}
			}

			tributosUtilizados.addAll(colecaoTributoAliquotaTarifa);

			if (!tributosUtilizados.isEmpty()) {
				colecaoTarifaTributoAliquota.put(tarifaVigencia.getTarifa(), tributosUtilizados);
			}

		}

		return colecaoTarifaTributoAliquota;
	}
	
	private Collection<Long> getChavesTributosTarifaVigencia(TarifaVigencia tarifaVigencia) {

		Collection<Long> chavesTributos = new HashSet<>();

		if (tarifaVigencia.getTributos() != null && !tarifaVigencia.getTributos().isEmpty()) {

			for (TarifaVigenciaTributo tarifaVigenciaTributo : tarifaVigencia.getTributos()) {

				chavesTributos.add(tarifaVigenciaTributo.getTributo().getChavePrimaria());
			}
		}

		return chavesTributos;
	}

	
	private Collection<TributoAliquota> filtrarTributoAliquotaPorTributo(TarifaVigencia tarifaVigencia,
			Collection<TributoAliquota> colecaoTributoAliquotaTarifa) {

		Map<Long, TributoAliquota> mapaTributoAliquota = new HashMap<Long, TributoAliquota>();

		int cont = 0;
		// trazer apenas os items atuais que forem da tarifa
		for (TributoAliquota tributoAliquota : colecaoTributoAliquotaTarifa) {
			if (cont >= tarifaVigencia.getTributos().size()) {
				break;
			}
			if (!mapaTributoAliquota.containsKey(tributoAliquota.getTributo().getChavePrimaria())) {
				mapaTributoAliquota.put(tributoAliquota.getTributo().getChavePrimaria(), tributoAliquota);
				cont++;
			}
		}

		return mapaTributoAliquota.values();
	}
	
	private Collection<Long> obterChavesTributosIsentos(PontoConsumo pontoConsumo, Boolean isConsumoDrawback) {

		ControladorTributo controladorTributo = (ControladorTributo) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);

		return controladorTributo.obterChavesTributoAliquotaPorPontoConsumo(pontoConsumo, Boolean.TRUE,
				isConsumoDrawback);

	}

	/**
	 * Gets the controlador tabela auxiliar.
	 *
	 * @return the controlador tabela auxiliar
	 */
	private ControladorTabelaAuxiliar getControladorTabelaAuxiliar() {

		return ServiceLocator.getInstancia().getControladorTabelaAuxiliar();
	}

	/**
	 * Gets the controlador fatura.
	 *
	 * @return the controlador fatura
	 */
	private ControladorFatura getControladorFatura() {

		return ServiceLocator.getInstancia().getControladorFatura();
	}


	/**
	 * Listar tarifa vigencia faixa.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<TarifaVigenciaFaixa> listarTarifaVigenciaFaixa(TarifaVigencia tarifaVigencia) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaFaixa().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifaVigencia.chavePrimaria = :idTarifaVigencia");
		hql.append(" order by medidaInicio");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTarifaVigencia", tarifaVigencia.getChavePrimaria());

		return query.list();
	}

	/**
	 * Calculo periodico.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param consumoApuradoPeriodo
	 *            the consumo apurado periodo
	 * @param periodo
	 *            the periodo
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param colecaoTributoAliquota
	 *            the colecao tributo aliquota
	 * @param colecaoDescontosFaixa
	 *            the colecao descontos faixa
	 * @param consumoAcumulado
	 *            the consumo acumulado
	 * @param escala
	 *            the escala
	 * @param tipoArredondamento
	 *            the tipo arredondamento
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param percentualUtilizado
	 *            the percentual utilizado
	 * @param consumoTotal
	 *            the consumo total
	 * @param precoGas
	 *            the preco gas
	 * @param ultimaLeitura
	 * @param param 
	 * @return the dados vigencia vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private DadosVigenciaVO calculoPeriodico(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura, BigDecimal consumoApuradoPeriodo,
					Periodo periodo, TarifaVigencia tarifaVigencia, Collection<TributoAliquota> colecaoTributoAliquota,
					Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> colecaoDescontosFaixa, BigDecimal consumoAcumulado, Integer escala,
					Integer tipoArredondamento, BigDecimal variacaoCambial, BigDecimal percentualUtilizado, BigDecimal consumoTotal,

					BigDecimal precoGas, String valorInformadoComplemento, Date ultimaLeitura, Map<String, Object> param, BigDecimal periodoLeitura) throws NegocioException {

		Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa = listarTarifaVigenciaFaixa(tarifaVigencia);

		if (colecaoTarifaVigenciaFaixa == null || colecaoTarifaVigenciaFaixa.isEmpty()) {
			throw new TarifaVigenciaFaixaException();
		}

		DadosVigenciaVO dadosVigencia = new DadosVigenciaVO();
		Collection<DadosFaixaVO> colecaoFaixas = new ArrayList<DadosFaixaVO>();

		dadosVigencia.setTarifaVigencia(tarifaVigencia);
		dadosVigencia.setDataPeriodoInicial(periodo.dataInicio);
		dadosVigencia.setDataPeriodoFinal(periodo.dataFim);

		if (EntidadeConteudo.CHAVE_CALCULO_CASCATA == tarifaVigencia.getTipoCalculo().getChavePrimaria()) {
			if (param != null) {
				GrupoFaturamento grupoFaturamento = (GrupoFaturamento) param.get(GRUPO_FATURAMENTO);
			
			if(grupoFaturamento.getIndicadorContinuidadeCascataTarifa()){
				DadosFaixaVO dadosFaixaVO = calcularValorConsumoPorCascataContinua(pontoConsumo, itemFatura, consumoApuradoPeriodo, colecaoDescontosFaixa, 
						colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, consumoAcumulado, escala, tipoArredondamento,
						variacaoCambial, percentualUtilizado, precoGas, valorInformadoComplemento, ultimaLeitura);
				colecaoFaixas.add(dadosFaixaVO);
			}
			}else{
				colecaoFaixas = calcularValorConsumoPorCascata(pontoConsumo, itemFatura, consumoTotal, colecaoDescontosFaixa,
						colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, consumoAcumulado, escala, tipoArredondamento,
						variacaoCambial, percentualUtilizado, precoGas, valorInformadoComplemento, ultimaLeitura, periodoLeitura);
			}
		} else if (EntidadeConteudo.CHAVE_CALCULO_FAIXA == tarifaVigencia.getTipoCalculo().getChavePrimaria()) {
			DadosFaixaVO dadosFaixaRetorno = calcularValorConsumoPorFaixa(pontoConsumo, itemFatura, consumoApuradoPeriodo,
							colecaoDescontosFaixa, colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, escala, tipoArredondamento,
							variacaoCambial, percentualUtilizado, consumoTotal, precoGas, ultimaLeitura, periodoLeitura);
			colecaoFaixas.add(dadosFaixaRetorno);
		} else {
			throw new TipoCalculoInvalidoException();
		}

		consolidarValoresFaixas(colecaoTributoAliquota, consumoAcumulado, dadosVigencia, colecaoFaixas,
				tipoArredondamento, escala, itemFatura);

		deleteme.append("\n............ INFORMAÇÕES DADOS VIGÊNCIA");
		deleteme.append("\n............ Base Cálculo Impostos: ").append(dadosVigencia.getBaseCalculoImpostos());
		deleteme.append("\n............ Consumo Apurado no Dia: ").append(dadosVigencia.getConsumoApurado());
		deleteme.append("\n............ Consumo Faturado no Dia: ").append(dadosVigencia.getConsumoFaturado());
		deleteme.append("\n............ Periodo Inicial: ").append(Util.converterDataParaStringSemHora(
				dadosVigencia.getDataPeriodoInicial().toDate(), Constantes.FORMATO_DATA_BR));
		deleteme.append("\n............ Periodo Final: ").append(Util.converterDataParaStringSemHora(
				dadosVigencia.getDataPeriodoFinal().toDate(), Constantes.FORMATO_DATA_BR));
		deleteme.append("\n............ Valor Descontos: ").append(dadosVigencia.getValorDescontos());
		deleteme.append("\n............ Valor Total: ").append(dadosVigencia.getValorTotal());
		if (dadosVigencia.getColecaoTributos() != null) {
			deleteme.append("\n............ Tributos: ").append(dadosVigencia.getValorTotal());
			for (DadosTributoVO dadosTributoVO : dadosVigencia.getColecaoTributos()) {
				deleteme.append("\n..... ").append(dadosTributoVO.getTributoAliquota().getTributo().getDescricao())
						.append(": AliquotaVL=").append(dadosTributoVO.getTributoAliquota().getValorAliquota())
						.append(": AliquotaPR=")
						.append(dadosTributoVO.getTributoAliquota().getValorPercentualAliquota()).append("; Base=")
						.append(dadosTributoVO.getBaseCalculo()).append("; Valor=").append(dadosTributoVO.getValor());
			}
		} else {
			deleteme.append("\n............ Tributos: Nenhum");
		}
		deleteme.append("\n............ Faixas: ");

		for (DadosFaixaVO df : dadosVigencia.getDadosFaixa()) {

			deleteme.append("\n........ FAIXA (").append(df.getTarifaVigenciaFaixa().getMedidaInicio()).append(" - ")
					.append(df.getTarifaVigenciaFaixa().getMedidaFim()).append(") ")
					.append("\n........ Consumo Apurado na Faixa: ").append(df.getConsumoApurado())
					.append("\n........ Consumo Faturado na Faixa: ").append(df.getConsumoFaturado())
					.append("\n........ Valor Desconto Fixo: ").append(df.getValorDescontoFixo());
			LOG.info("........ Valor Desconto Variável: " + df.getValorDescontoVariavel());
			deleteme.append("\n........ Valor Desconto Variável: ").append(df.getValorDescontoVariavel())
					.append("\n........ Valor Fixo Sem Imposto Sem Desconto: ")
					.append(df.getValorFixoSemImpostoSemDesconto())
					.append("\n........ Valor Fixo Sem Imposto Com Desconto: ")
					.append(df.getValorFixoSemImpostoComDesconto())
					.append("\n........ Valor Fixo Com Imposto Com Desconto: ")
					.append(df.getValorFixoComImpostoComDesconto())
					.append("\n........ Valor Fixo Com Imposto Sem Desconto: ")
					.append(df.getValorFixoComImpostoSemDesconto())
					.append("\n........ Valor Variavel Com Imposto Com Desconto: ")
					.append(df.getValorVariavelComImpostoComDesconto())
					.append("\n........ Valor Variavel Com Imposto Sem Desconto: ")
					.append(df.getValorVariavelComImpostoSemDesconto())
					.append("\n........ Valor Variavel Sem Imposto Com Desconto: ")
					.append(df.getValorVariavelSemImpostoComDesconto())
					.append("\n........ Valor Variavel Sem Imposto Sem Desconto: ")
					.append(df.getValorVariavelSemImpostoSemDesconto());

		}

		return dadosVigencia;
	}

	/**
	 * Consolidar valores faixas.
	 *
	 * @param colecaoTributoAliquota
	 *            the colecao tributo aliquota
	 * @param consumoAcumulado
	 *            the consumo acumulado
	 * @param dadosVigencia
	 *            the dados vigencia
	 * @param colecaoFaixas
	 *            the colecao faixas
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param tipoArredondamento
	 *            the tipo arredondamento
	 * @param escala
	 *            the escala
	 * @param itemFatura
	 *            the item fatura
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal consolidarValoresFaixas(Collection<TributoAliquota> colecaoTributoAliquota,
			BigDecimal consumoAcumuladoTmp, DadosVigenciaVO dadosVigencia, Collection<DadosFaixaVO> colecaoFaixas,
			Integer tipoArredondamento, Integer escala, EntidadeConteudo itemFatura) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);
		BigDecimal consumoAcumulado = consumoAcumuladoTmp;
		BigDecimal valorTotalComDescontoComImposto = BigDecimal.ZERO;
		BigDecimal baseCalculo = BigDecimal.ZERO;

		if (dadosVigencia.getBaseCalculoImpostos() != null && dadosVigencia.getBaseCalculoImpostos().compareTo(BigDecimal.ZERO) > 0) {
			baseCalculo = dadosVigencia.getBaseCalculoImpostos();
		}
		BigDecimal valorDesconto = BigDecimal.ZERO;
		if (dadosVigencia.getValorDescontos() != null && dadosVigencia.getValorDescontos().compareTo(BigDecimal.ZERO) > 0) {
			valorDesconto = dadosVigencia.getValorDescontos();
		}
		BigDecimal consumoVigencia = BigDecimal.ZERO;
		if (dadosVigencia.getConsumoApurado() != null && dadosVigencia.getConsumoApurado().compareTo(BigDecimal.ZERO) > 0) {
			consumoVigencia = dadosVigencia.getConsumoApurado();
		}

		BigDecimal consumoVigenciaFaturado = BigDecimal.ZERO;
		if (dadosVigencia.getConsumoFaturado() != null && dadosVigencia.getConsumoFaturado().compareTo(BigDecimal.ZERO) > 0) {
			consumoVigenciaFaturado = dadosVigencia.getConsumoFaturado();
		}

		BigDecimal valorTotal = BigDecimal.ZERO;
		if (dadosVigencia.getValorTotal() != null && dadosVigencia.getValorTotal().compareTo(BigDecimal.ZERO) > 0) {
			valorTotal = dadosVigencia.getValorTotal();
			valorTotalComDescontoComImposto = dadosVigencia.getValorTotal();
		}

		BigDecimal valorTotalSemSubstituicao = BigDecimal.ZERO;
		if (dadosVigencia.getValorTotalSemSubstituicao() != null
						&& dadosVigencia.getValorTotalSemSubstituicao().compareTo(BigDecimal.ZERO) > 0) {
			valorTotalSemSubstituicao = dadosVigencia.getValorTotalSemSubstituicao();
		}

		BigDecimal valorTotalSemTributo = BigDecimal.ZERO;
		if (dadosVigencia.getValorTotalSemTributo() != null && dadosVigencia.getValorTotalSemTributo().compareTo(BigDecimal.ZERO) > 0) {
			valorTotalSemTributo = dadosVigencia.getValorTotalSemTributo();
		}

		if (dadosVigencia.getDadosFaixa() != null) {

			for (DadosFaixaVO dadosFaixaRetorno : colecaoFaixas) {

				if (dadosVigencia.getDadosFaixa().contains(dadosFaixaRetorno)) {

					DadosFaixaVO voSoma = null;

					for (DadosFaixaVO dadosFaixaVO : dadosVigencia.getDadosFaixa()) {

						if (dadosFaixaVO.equals(dadosFaixaRetorno)) {
							voSoma = new DadosFaixaVO(dadosFaixaVO);
							// atualizar valores,
							// agrupar por faixas.
							// se já existe soma
							// valores das faixas
							// iguais.
							voSoma.setConsumoFaturado(voSoma.getConsumoFaturado().add(dadosFaixaRetorno.getConsumoFaturado()));
							voSoma.setConsumoApurado(voSoma.getConsumoApurado().add(dadosFaixaRetorno.getConsumoApurado()));
							voSoma.setValorFixoSemImpostoSemDesconto(voSoma.getValorFixoSemImpostoSemDesconto().add(
											dadosFaixaRetorno.getValorFixoSemImpostoSemDesconto()));
							voSoma.setValorFixoComImpostoSemDesconto(voSoma.getValorFixoComImpostoSemDesconto().add(
											dadosFaixaRetorno.getValorFixoComImpostoSemDesconto()));
							voSoma.setValorFixoSemImpostoComDesconto(voSoma.getValorFixoSemImpostoComDesconto().add(
											dadosFaixaRetorno.getValorFixoSemImpostoComDesconto()));
							voSoma.setValorFixoComImpostoComDesconto(voSoma.getValorFixoComImpostoComDesconto().add(
											dadosFaixaRetorno.getValorFixoComImpostoComDesconto()));
							voSoma.setValorVariavelComImpostoSemDesconto(voSoma.getValorVariavelComImpostoSemDesconto().add(
											dadosFaixaRetorno.getValorVariavelComImpostoSemDesconto()));
							voSoma.setValorVariavelSemImpostoSemDesconto(voSoma.getValorVariavelSemImpostoSemDesconto().add(
											dadosFaixaRetorno.getValorVariavelSemImpostoSemDesconto()));
							voSoma.setValorVariavelComImpostoComDesconto(voSoma.getValorVariavelComImpostoComDesconto().add(
											dadosFaixaRetorno.getValorVariavelComImpostoComDesconto()));
							voSoma.setValorVariavelSemImpostoComDesconto(voSoma.getValorVariavelSemImpostoComDesconto().add(
											dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto()));

							if (dadosFaixaRetorno.getValorDescontoFixo() != null) {
								valorDesconto = valorDesconto.add(dadosFaixaRetorno.getValorDescontoFixo());
							}
							if (dadosFaixaRetorno.getValorDescontoVariavel() != null) {
								valorDesconto = valorDesconto.add(dadosFaixaRetorno.getValorDescontoVariavel().multiply(
												dadosFaixaRetorno.getConsumoApurado()));
							}

							if (consumoAcumulado != null) {
								consumoAcumulado = consumoAcumulado.add(dadosFaixaRetorno.getConsumoApurado());
							}

							if (dadosFaixaRetorno.getValorVariavelComImpostoComDesconto() != null
											&& dadosFaixaRetorno.getValorVariavelComImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
								valorTotal = valorTotal.add(dadosFaixaRetorno.getValorVariavelComImpostoComDesconto());
							}

							if (dadosFaixaRetorno.getValorFixoComImpostoComDesconto() != null
											&& dadosFaixaRetorno.getValorFixoComImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
								valorTotal = valorTotal.add(dadosFaixaRetorno.getValorFixoComImpostoComDesconto());
							}

							if (dadosFaixaRetorno.getValorTotalComImpostoSemSubstituicao() != null
											&& dadosFaixaRetorno.getValorTotalComImpostoSemSubstituicao().compareTo(BigDecimal.ZERO) > 0) {
								valorTotalSemSubstituicao = valorTotalSemSubstituicao.add(dadosFaixaRetorno
												.getValorTotalComImpostoSemSubstituicao());
							}

							if (dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto() != null
											&& dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
								valorTotalSemTributo = valorTotalSemTributo.add(dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto());
							}

							if (dadosFaixaRetorno.getValorFixoSemImpostoComDesconto() != null
											&& dadosFaixaRetorno.getValorFixoSemImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
								valorTotalSemTributo = valorTotalSemTributo.add(dadosFaixaRetorno.getValorFixoSemImpostoComDesconto());
							}

							baseCalculo = baseCalculo.add(dadosFaixaRetorno.getValorTotalComImpostoSemSubstituicao());

							if (dadosFaixaRetorno.getConsumoApurado() != null) {
								consumoVigencia = consumoVigencia.add(dadosFaixaRetorno.getConsumoApurado());
							}
							if (dadosFaixaRetorno.getConsumoFaturado() != null) {
								consumoVigenciaFaturado = consumoVigenciaFaturado.add(dadosFaixaRetorno.getConsumoFaturado());
							}

							break;
						}
					}

					if (dadosVigencia.getDadosFaixa().contains(voSoma)) {
						int index = dadosVigencia.getDadosFaixa().indexOf(voSoma);
						dadosVigencia.getDadosFaixa().remove(index);
						dadosVigencia.getDadosFaixa().add(index, voSoma);
					}
				} else {

					if (dadosFaixaRetorno.getValorDescontoFixo() != null) {
						valorDesconto = valorDesconto.add(dadosFaixaRetorno.getValorDescontoFixo());
					}
					if (dadosFaixaRetorno.getValorDescontoVariavel() != null) {
						valorDesconto = valorDesconto.add(dadosFaixaRetorno.getValorDescontoVariavel());
					}

					if (consumoAcumulado != null) {
						consumoAcumulado = consumoAcumulado.add(dadosFaixaRetorno.getConsumoApurado());
					}

					if (dadosFaixaRetorno.getValorVariavelComImpostoComDesconto() != null
									&& dadosFaixaRetorno.getValorVariavelComImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotal = valorTotal.add(dadosFaixaRetorno.getValorVariavelComImpostoComDesconto());
					}

					if (dadosFaixaRetorno.getValorFixoComImpostoComDesconto() != null
									&& dadosFaixaRetorno.getValorFixoComImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotal = valorTotal.add(dadosFaixaRetorno.getValorFixoComImpostoComDesconto());
					}

					if (dadosFaixaRetorno.getValorTotalComImpostoSemSubstituicao() != null
									&& dadosFaixaRetorno.getValorTotalComImpostoSemSubstituicao().compareTo(BigDecimal.ZERO) > 0) {
						valorTotalSemSubstituicao = valorTotalSemSubstituicao.add(dadosFaixaRetorno
										.getValorTotalComImpostoSemSubstituicao());
					}

					if (dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto() != null
									&& dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotalSemTributo = valorTotalSemTributo.add(dadosFaixaRetorno.getValorVariavelSemImpostoComDesconto());
					}

					if (dadosFaixaRetorno.getValorFixoSemImpostoComDesconto() != null
									&& dadosFaixaRetorno.getValorFixoSemImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotalSemTributo = valorTotalSemTributo.add(dadosFaixaRetorno.getValorFixoSemImpostoComDesconto());
					}

					baseCalculo = baseCalculo.add(dadosFaixaRetorno.getValorTotalComImpostoSemSubstituicao());

					if (dadosFaixaRetorno.getConsumoApurado() != null) {
						consumoVigencia = consumoVigencia.add(dadosFaixaRetorno.getConsumoApurado());
					}

					if (dadosFaixaRetorno.getConsumoFaturado() != null) {
						consumoVigenciaFaturado = consumoVigenciaFaturado.add(dadosFaixaRetorno.getConsumoFaturado());
					}

					dadosVigencia.getDadosFaixa().add(dadosFaixaRetorno);
				}
			}

		} else {

			for (DadosFaixaVO dadosFaixaIteracao : colecaoFaixas) {

				if (dadosFaixaIteracao != null) {
					BigDecimal baseIteracao = dadosFaixaIteracao.getValorTotalComImpostoSemSubstituicao();
					baseCalculo = baseCalculo.add(baseIteracao);

					if (dadosFaixaIteracao.getValorVariavelComImpostoComDesconto() != null
									&& dadosFaixaIteracao.getValorVariavelComImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotal = valorTotal.add(dadosFaixaIteracao.getValorVariavelComImpostoComDesconto());
						valorTotalComDescontoComImposto = dadosFaixaIteracao.getValorVariavelComImpostoComDesconto();
					}

					if (dadosFaixaIteracao.getValorFixoComImpostoComDesconto() != null
									&& dadosFaixaIteracao.getValorFixoComImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotal = valorTotal.add(dadosFaixaIteracao.getValorFixoComImpostoComDesconto());
					}

					if (dadosFaixaIteracao.getValorTotalComImpostoSemSubstituicao() != null
									&& dadosFaixaIteracao.getValorTotalComImpostoSemSubstituicao().compareTo(BigDecimal.ZERO) > 0) {
						valorTotalSemSubstituicao = valorTotalSemSubstituicao.add(dadosFaixaIteracao
										.getValorTotalComImpostoSemSubstituicao());
					}

					if (dadosFaixaIteracao.getValorVariavelSemImpostoComDesconto() != null
									&& dadosFaixaIteracao.getValorVariavelSemImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotalSemTributo = valorTotalSemTributo.add(dadosFaixaIteracao.getValorVariavelSemImpostoComDesconto());
					}

					if (dadosFaixaIteracao.getValorFixoSemImpostoComDesconto() != null
									&& dadosFaixaIteracao.getValorFixoSemImpostoComDesconto().compareTo(BigDecimal.ZERO) > 0) {
						valorTotalSemTributo = valorTotalSemTributo.add(dadosFaixaIteracao.getValorFixoSemImpostoComDesconto());
					}

					if (dadosFaixaIteracao.getValorDescontoFixo() != null
									&& dadosFaixaIteracao.getTarifaVigenciaFaixa().getValorFixo() != null
									&& dadosFaixaIteracao.getTarifaVigenciaFaixa().getValorFixo().compareTo(BigDecimal.ZERO) > 0) {

						valorDesconto = valorDesconto.add(dadosFaixaIteracao.getValorDescontoFixo());
					}

					if (dadosFaixaIteracao.getValorDescontoVariavel() != null
									&& dadosFaixaIteracao.getTarifaVigenciaFaixa().getValorVariavel() != null
									&& dadosFaixaIteracao.getTarifaVigenciaFaixa().getValorVariavel().compareTo(BigDecimal.ZERO) > 0) {

						valorDesconto = valorDesconto.add(dadosFaixaIteracao.getConsumoApurado().multiply(
										dadosFaixaIteracao.getValorDescontoVariavel()));
					}

					if (dadosFaixaIteracao.getConsumoApurado() != null) {
						consumoVigencia = consumoVigencia.add(dadosFaixaIteracao.getConsumoApurado());
						if (consumoAcumulado != null) {
							consumoAcumulado = consumoAcumulado.add(dadosFaixaIteracao.getConsumoApurado());
						}
					}

					if (dadosFaixaIteracao.getConsumoFaturado() != null) {
						consumoVigenciaFaturado = consumoVigenciaFaturado.add(dadosFaixaIteracao.getConsumoFaturado());
					}

				}

			}
			dadosVigencia.setDadosFaixa(new ArrayList<DadosFaixaVO>(colecaoFaixas));
		}

		dadosVigencia.setBaseCalculoImpostos(baseCalculo);
		dadosVigencia.setConsumoApurado(consumoVigencia);
		dadosVigencia.setConsumoFaturado(consumoVigenciaFaturado);
		dadosVigencia.setValorDescontos(valorDesconto);
		dadosVigencia.setValorTotalComDescontoComImposto(valorTotalComDescontoComImposto);

		dadosVigencia.setValorTotal(valorTotal);
		dadosVigencia.setValorTotalSemSubstituicao(valorTotalSemSubstituicao);
		dadosVigencia.setValorTotalSemTributo(valorTotalSemTributo);
		isDescontoAplicadoValortotal = Boolean.FALSE;
		if (colecaoTributoAliquota != null) {

			Collection<DadosTributoVO> listaTributos = new ArrayList<DadosTributoVO>();
			DadosTributoVO dadosTributoVO = null;

			for (TributoAliquota tributoAliquota : colecaoTributoAliquota) {

				dadosTributoVO = new DadosTributoVO();
				dadosTributoVO.setTributoAliquota(tributoAliquota);
				BigDecimal aliquota = tributoAliquota.getValorAliquota();

				if ( tributoAliquota.getTributo() != null
								&& valorICMS != null
								&& tributoAliquota.getTributo().getChavePrimaria() == valorICMS) {
					aliquota = getControladorFatura().aplicarReducaoBaseCalculo(aliquota, tributoAliquota);

					BigDecimal baseCalculoTributoAliquota =
									getControladorFatura().aplicarReducaoBaseCalculo(
													dadosVigencia.getBaseCalculoImpostos(), tributoAliquota);
					dadosTributoVO.setBaseCalculo(baseCalculoTributoAliquota);
				} else {
					dadosTributoVO.setBaseCalculo(dadosVigencia.getBaseCalculoImpostos());
				}

				dadosTributoVO.setValor(dadosVigencia.getBaseCalculoImpostos().multiply(aliquota));

				listaTributos.add(dadosTributoVO);

			}
			dadosVigencia.setColecaoTributos(listaTributos);
		}

		return consumoAcumulado;
	}

	/**
	 * Calculo media diaria.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param periodo
	 *            the periodo
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param colecaoTributoAliquota
	 *            the colecao tributo aliquota
	 * @param consumoPeriodo
	 *            the consumo periodo
	 * @param colecaoDescontosFaixa
	 *            the colecao descontos faixa
	 * @param escala
	 *            the escala
	 * @param tipoArredondamento
	 *            the tipo arredondamento
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param precoGas
	 *            the preco gas
	 * @param ultimaLeitura
	 * @return the dados vigencia vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private DadosVigenciaVO calculoMediaDiaria(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura, Periodo periodo,
					TarifaVigencia tarifaVigencia, Collection<TributoAliquota> colecaoTributoAliquota, BigDecimal consumoPeriodo,
					Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> colecaoDescontosFaixa, Integer escala, Integer tipoArredondamento,
					BigDecimal variacaoCambial, BigDecimal precoGas, Date ultimaLeitura, BigDecimal periodoLeitura) throws NegocioException {

		Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa = listarTarifaVigenciaFaixa(tarifaVigencia);

		if (colecaoTarifaVigenciaFaixa == null || colecaoTarifaVigenciaFaixa.isEmpty()) {
			throw new TarifaVigenciaFaixaException();
		}

		DadosVigenciaVO dadosVigencia = new DadosVigenciaVO();
		Collection<DadosFaixaVO> colecaoFaixas = new ArrayList<DadosFaixaVO>();

		dadosVigencia.setDataPeriodoInicial(periodo.dataInicio);
		dadosVigencia.setDataPeriodoFinal(periodo.dataFim);
		dadosVigencia.setTarifaVigencia(tarifaVigencia);
		BigDecimal percentualUtilizado = BigDecimal.ONE;

		DateTime dateTimeIteracao = periodo.dataInicio;
		while (!dateTimeIteracao.isAfter(periodo.dataFim)) {
			deleteme.append("\n\n........................................................................ Processando o Dia: ")
					.append(Util.converterDataParaStringSemHora(dateTimeIteracao.toDate(), Constantes.FORMATO_DATA_BR));

			if (EntidadeConteudo.CHAVE_CALCULO_CASCATA == tarifaVigencia.getTipoCalculo().getChavePrimaria()) {
				colecaoFaixas = this.calcularValorConsumoPorCascata(pontoConsumo, itemFatura, consumoPeriodo, colecaoDescontosFaixa,
								colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, null, escala, tipoArredondamento, variacaoCambial,
								percentualUtilizado, precoGas, null, ultimaLeitura, periodoLeitura);
			} else if (EntidadeConteudo.CHAVE_CALCULO_FAIXA == tarifaVigencia.getTipoCalculo().getChavePrimaria()) {
				DadosFaixaVO dadosFaixaRetorno = this.calcularValorConsumoPorFaixa(pontoConsumo, itemFatura, consumoPeriodo,
								colecaoDescontosFaixa, colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, escala, tipoArredondamento,
								variacaoCambial, percentualUtilizado, null, precoGas, ultimaLeitura, periodoLeitura);
				colecaoFaixas.add(dadosFaixaRetorno);
			} else {
				throw new TipoCalculoInvalidoException();
			}
			this.consolidarValoresFaixas(colecaoTributoAliquota, null, dadosVigencia, colecaoFaixas, tipoArredondamento,
							escala, itemFatura);
			colecaoFaixas.clear();
			dateTimeIteracao = dateTimeIteracao.plusDays(1);
		}

		deleteme.append("\n\n\n............ INFORMAÇÕES DADOS VIGÊNCIA")
				.append("\n............ Base Cálculo Impostos: ").append(dadosVigencia.getBaseCalculoImpostos())
				.append("\n............ Consumo Apurado no Dia: ").append(dadosVigencia.getConsumoApurado())
				.append("\n............ Consumo Faturado no Dia: ").append(dadosVigencia.getConsumoFaturado())
				.append("\n............ Periodo Inicial: ")
				.append(Util.converterDataParaStringSemHora(dadosVigencia.getDataPeriodoInicial().toDate(), Constantes.FORMATO_DATA_BR))
				.append("\n............ Periodo Final: ")
				.append(Util.converterDataParaStringSemHora(dadosVigencia.getDataPeriodoFinal().toDate(), Constantes.FORMATO_DATA_BR))
				.append("\n............ Valor Descontos: ").append(dadosVigencia.getValorDescontos()).append("\n............ Valor Total: ")
				.append(dadosVigencia.getValorTotal());
		if (dadosVigencia.getColecaoTributos() != null) {
			deleteme.append("\n............ Tributos: ").append(dadosVigencia.getBaseCalculoImpostos());
			for (DadosTributoVO dadosTributoVO : dadosVigencia.getColecaoTributos()) {
				deleteme.append("\n..... ").append(dadosTributoVO.getTributoAliquota().getTributo().getDescricao()).append(": AliquotaVL=")
						.append(dadosTributoVO.getTributoAliquota().getValorAliquota()).append(": AliquotaPR=")
						.append(dadosTributoVO.getTributoAliquota().getValorPercentualAliquota()).append("; Base=")
						.append(dadosTributoVO.getBaseCalculo()).append("; Valor=").append(dadosTributoVO.getValor());
			}
		} else {
			deleteme.append("\n............ Tributos: Nenhum");
		}
		deleteme.append("\n............ Faixas: ");

		for (DadosFaixaVO df : dadosVigencia.getDadosFaixa()) {

			deleteme.append("\n\n........ FAIXA (").append(df.getTarifaVigenciaFaixa().getMedidaInicio())
					.append(" - ")
					.append(df.getTarifaVigenciaFaixa().getMedidaFim()).append(") ").append("\n........ Consumo Apurado na Faixa: ")
					.append(df.getConsumoApurado()).append("\n........ Consumo Faturado na Faixa: ").append(df.getConsumoFaturado())
					.append("\n........ Valor Desconto Fixo: ").append(df.getValorDescontoFixo())
					.append("\n........ Valor Desconto Variável: ").append(df.getValorDescontoVariavel())
					.append("\n........ Valor Fixo Sem Imposto Sem Desconto: ").append(df.getValorFixoSemImpostoSemDesconto())
					.append("\n........ Valor Fixo Sem Imposto Com Desconto: ").append(df.getValorFixoSemImpostoComDesconto())
					.append("\n........ Valor Fixo Com Imposto Com Desconto: ").append(df.getValorFixoComImpostoComDesconto())
					.append("\n........ Valor Fixo Com Imposto Sem Desconto: ").append(df.getValorFixoComImpostoSemDesconto())
					.append("\n........ Valor Variavel Com Imposto Com Desconto: ").append(df.getValorVariavelComImpostoComDesconto())
					.append("\n........ Valor Variavel Com Imposto Sem Desconto: ").append(df.getValorVariavelComImpostoSemDesconto())
					.append("\n........ Valor Variavel Sem Imposto Com Desconto: ").append(df.getValorVariavelSemImpostoComDesconto())
					.append("\n........ Valor Variavel Sem Imposto Sem Desconto: ").append(df.getValorVariavelSemImpostoSemDesconto());

		}

		return dadosVigencia;
	}

	/**
	 * Calculo diario.
	 *
	 * @param colecaoPontoConsumo the colecao ponto consumo
	 * @param itemFatura the item fatura
	 * @param periodo the periodo
	 * @param tarifaVigencia the tarifa vigencia
	 * @param colecaoTributoAliquota the colecao tributo aliquota
	 * @param colecaoDescontosFaixa the colecao descontos faixa
	 * @param escala the escala
	 * @param tipoArredondamento the tipo arredondamento
	 * @param variacaoCambial the variacao cambial
	 * @param consumoFicticio the consumo ficticio
	 * @param precoGas the preco gas
	 * @param ultimaLeitura
	 * @return the dados vigencia vo
	 * @throws NegocioException the negocio exception
	 */
	private DadosVigenciaVO calculoDiario(Collection<PontoConsumo> colecaoPontoConsumo, EntidadeConteudo itemFatura, Periodo periodo,
			TarifaVigencia tarifaVigencia, Collection<TributoAliquota> colecaoTributoAliquota,
			Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> colecaoDescontosFaixa, Integer escala, Integer tipoArredondamento,
			BigDecimal variacaoCambial, BigDecimal consumoFicticio, BigDecimal precoGas, Date ultimaLeitura, BigDecimal periodoLeitura) throws NegocioException {

		// O consumo acumulado precisa realmente ser mantido

		PontoConsumo pontoConsumo = null;

		Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa = listarTarifaVigenciaFaixa(tarifaVigencia);

		if (colecaoTarifaVigenciaFaixa == null || colecaoTarifaVigenciaFaixa.isEmpty()) {
			throw new TarifaVigenciaFaixaException();
		}

		// Consultar consumoApurado para cada dia
		// DESSE periodo da vigencia.
		Map<Date, BigDecimal> mapaDataConsumo = new HashMap<Date, BigDecimal>();

		if (colecaoPontoConsumo != null && !colecaoPontoConsumo.isEmpty()) {

			pontoConsumo = colecaoPontoConsumo.iterator().next();
			mapaDataConsumo = this.consultarConsumoApuradoIntervalo(colecaoPontoConsumo, periodo);

		} else {

			// se pontoConsumo é null significa
			// que é uma simulação com dados
			// ficticios.
			if (periodo.getQuantidadeDiasIntervalo() > 1) {
				// para simular com dados
				// ficticios e base de apuração
				// diária, é preciso limitar o
				// intervalo a 1 dia.
				throw new SimularApuracaoDiariaException();
			} else {
				mapaDataConsumo.put(periodo.dataInicio.toDate(), consumoFicticio);
			}
		}

		DadosVigenciaVO dadosVigencia = new DadosVigenciaVO();
		dadosVigencia.setDataPeriodoInicial(periodo.dataInicio);
		dadosVigencia.setDataPeriodoFinal(periodo.dataFim);
		dadosVigencia.setTarifaVigencia(tarifaVigencia);

		Collection<DadosFaixaVO> colecaoFaixas = new ArrayList<DadosFaixaVO>();

		if (mapaDataConsumo.size() != periodo.getQuantidadeDiasIntervalo()) {
			throw new ApuracaoDiariaException(ControladorCalculoFornecimentoGas.ERRO_QUANTIDADE_CONSUMO_DIARIO, true);
		}
		DateTime dateTimeIteracao = periodo.dataInicio;
		while (!dateTimeIteracao.isAfter(periodo.dataFim)) {

			deleteme.append("\n\n........................................................................ Processando o Dia: ")
					.append(Util.converterDataParaStringSemHora(dateTimeIteracao.toDate(), Constantes.FORMATO_DATA_BR));

			Date data = dateTimeIteracao.toDate();
			BigDecimal consumoApurado = mapaDataConsumo.get(data);
			BigDecimal percentualUtilizado = BigDecimal.ONE;

			BigDecimal consumoApuroadoArredondado = consumoApurado.setScale(0, RoundingMode.HALF_EVEN);
			
			if (EntidadeConteudo.CHAVE_CALCULO_CASCATA == tarifaVigencia.getTipoCalculo().getChavePrimaria()) {
				colecaoFaixas = calcularValorConsumoPorCascata(pontoConsumo, itemFatura, consumoApuroadoArredondado, colecaoDescontosFaixa,
								colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, null, escala, tipoArredondamento, variacaoCambial,
								percentualUtilizado, precoGas, null, ultimaLeitura, BigDecimal.ONE);
			} else if (EntidadeConteudo.CHAVE_CALCULO_FAIXA == tarifaVigencia.getTipoCalculo().getChavePrimaria()) {
				DadosFaixaVO dadosFaixaRetorno = calcularValorConsumoPorFaixa(pontoConsumo, itemFatura, consumoApuroadoArredondado,
								colecaoDescontosFaixa, colecaoTarifaVigenciaFaixa, colecaoTributoAliquota, escala, tipoArredondamento,
								variacaoCambial, percentualUtilizado, null, precoGas, ultimaLeitura, BigDecimal.ONE);
				colecaoFaixas.add(dadosFaixaRetorno);
			} else {
				throw new TipoCalculoInvalidoException();
			}

			consolidarValoresFaixas(colecaoTributoAliquota, null, dadosVigencia, colecaoFaixas, tipoArredondamento,
					escala, itemFatura);
			colecaoFaixas.clear();
			dateTimeIteracao = dateTimeIteracao.plusDays(1);
		}

		Collections.sort(dadosVigencia.getDadosFaixa());

		deleteme.append("\n............ INFORMAÇÕES DADOS VIGÊNCIA")
				.append("\n............ Base Cálculo Impostos: ").append(dadosVigencia.getBaseCalculoImpostos())
				.append("\n............ Consumo Apurado no Dia: ").append(dadosVigencia.getConsumoApurado())
				.append("\n............ Consumo Faturado no Dia: ").append(dadosVigencia.getConsumoFaturado())
				.append("\n............ Periodo Inicial: ")
				.append(Util.converterDataParaStringSemHora(dadosVigencia.getDataPeriodoInicial().toDate(), Constantes.FORMATO_DATA_BR))
				.append("\n............ Periodo Final: ")
				.append(Util.converterDataParaStringSemHora(dadosVigencia.getDataPeriodoFinal().toDate(), Constantes.FORMATO_DATA_BR))
				.append("\n............ Valor Descontos: ").append(dadosVigencia.getValorDescontos())
				.append("\n............ Valor Total: ").append(dadosVigencia.getValorTotal());
		if (dadosVigencia.getColecaoTributos() != null) {
			deleteme.append("\n............ Tributos: ").append(dadosVigencia.getBaseCalculoImpostos());
			for (DadosTributoVO dadosTributoVO : dadosVigencia.getColecaoTributos()) {
				deleteme.append("\n..... ").append(dadosTributoVO.getTributoAliquota().getTributo().getDescricao()).append(": AliquotaVL=")
						.append(dadosTributoVO.getTributoAliquota().getValorAliquota()).append(": AliquotaPR=")
						.append(dadosTributoVO.getTributoAliquota().getValorPercentualAliquota()).append("; Base=")
						.append(dadosTributoVO.getBaseCalculo()).append("; Valor=").append(dadosTributoVO.getValor());
			}
		} else {
			deleteme.append("\n............ Tributos: Nenhum");

		}
		deleteme.append("\n............ Faixas: ");

		for (DadosFaixaVO df : dadosVigencia.getDadosFaixa()) {

			deleteme.append("\n........ FAIXA (").append(df.getTarifaVigenciaFaixa().getMedidaInicio()).append(" - ")
					.append(df.getTarifaVigenciaFaixa().getMedidaFim()).append(") ").append("\n........ Consumo Apurado na Faixa: ")
					.append(df.getConsumoApurado()).append("\n........ Consumo Faturado na Faixa: ").append(df.getConsumoFaturado())
					.append("\n........ Valor Desconto Fixo: ").append(df.getValorDescontoFixo())
					.append("\n........ Valor Desconto Variável: ").append(df.getValorDescontoVariavel())
					.append("\n........ Valor Fixo Sem Imposto Sem Desconto: ").append(df.getValorFixoSemImpostoSemDesconto())
					.append("\n........ Valor Fixo Sem Imposto Com Desconto: ").append(df.getValorFixoSemImpostoComDesconto())
					.append("\n........ Valor Fixo Com Imposto Com Desconto: ").append(df.getValorFixoComImpostoComDesconto())
					.append("\n........ Valor Fixo Com Imposto Sem Desconto: ").append(df.getValorFixoComImpostoSemDesconto())
					.append("\n........ Valor Variavel Com Imposto Com Desconto: ").append(df.getValorVariavelComImpostoComDesconto())
					.append("\n........ Valor Variavel Com Imposto Sem Desconto: ").append(df.getValorVariavelComImpostoSemDesconto())
					.append("\n........ Valor Variavel Sem Imposto Com Desconto: ").append(df.getValorVariavelSemImpostoComDesconto())
					.append("\n........ Valor Variavel Sem Imposto Sem Desconto: ").append(df.getValorVariavelSemImpostoSemDesconto());

		}

		return dadosVigencia;
	}

	/**
	 * Obter variacao cambial.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param dataReferencia
	 *            the data referencia
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterVariacaoCambial(TarifaVigencia tarifaVigencia, Date dataReferencia) throws NegocioException {

		/**
		 * select *
		 * from indice_financeiro_val_nominal
		 * indice
		 * where indice.infi_cd = 3
		 * and indice.infv_dt_referencia =
		 * (select max(indiceFinanceiro.
		 * infv_dt_referencia)
		 * from indice_financeiro_val_nominal
		 * indiceFinanceiro
		 * where indiceFinanceiro.infi_cd = 3
		 * and indiceFinanceiro.infv_dt_referencia
		 * <= to_date('09/01/09','dd/MM/yy'));
		 */

		BigDecimal variacaoCambial = BigDecimal.ZERO;
		ServiceLocator.getInstancia().getControladorParametroSistema();

		if (tarifaVigencia != null
						&& tarifaVigencia.getUnidadeMonetaria().getChavePrimaria() == EntidadeConteudo.CHAVE_UNIDADE_MONETARIA_DOLAR) {

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			String chaveIndiceFinanceiroDolar = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DOLAR);

			StringBuilder hql = new StringBuilder();

			hql.append(" select indiceFinanceiroNominal.valorNominal ");
			hql.append(" from ");
			hql.append(getClasseEntidadeIndiceFinanceiroValorNominal().getSimpleName());
			hql.append(" indiceFinanceiroNominal ");
			hql.append(" inner join  indiceFinanceiroNominal.indiceFinanceiro ");
			hql.append(" where ");
			hql.append(" indiceFinanceiroNominal.indiceFinanceiro.chavePrimaria = :indiceFinanceiro ");

			hql.append(" and indiceFinanceiroNominal.dataReferencia = ");
			hql.append("  (select max(indiceFinanceiro.dataReferencia) ");
			hql.append("   from  ");
			hql.append(getClasseEntidadeIndiceFinanceiroValorNominal().getSimpleName());
			hql.append("   indiceFinanceiro ");
			hql.append("   where indiceFinanceiro.indiceFinanceiro.chavePrimaria = :indiceFinanceiro ");
			hql.append("   and  indiceFinanceiro.dataReferencia <= :dataReferencia )");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setDate("dataReferencia", dataReferencia);
			query.setLong("indiceFinanceiro", Long.parseLong(chaveIndiceFinanceiroDolar));

			variacaoCambial = (BigDecimal) query.uniqueResult();
			if (variacaoCambial == null) {
				throw new VariacaoCambialException();
			}

		}

		return variacaoCambial;
	}

	/**
	 * Mapear tarifa faixa desconto por tarifa vigencia desconto.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @return the map
	 */
	@SuppressWarnings("unchecked")
	private Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> mapearTarifaFaixaDescontoPorTarifaVigenciaDesconto(
					TarifaVigenciaDesconto tarifaVigenciaDesconto) {

		Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> retorno = new HashMap<TarifaVigenciaFaixa, TarifaFaixaDesconto>();

		if (tarifaVigenciaDesconto != null) {
			StringBuilder hql = new StringBuilder();

			hql.append(" from ");
			hql.append(getClasseEntidadeTarifaFaixaDesconto().getSimpleName());
			hql.append(" tfd ");
			hql.append(" where ");
			hql.append(" tfd.tarifaVigenciaDesconto.chavePrimaria = :idVigenciaDesconto");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("idVigenciaDesconto", tarifaVigenciaDesconto.getChavePrimaria());

			Collection<TarifaFaixaDesconto> colecaoRetorno = query.list();
			if (colecaoRetorno != null && !colecaoRetorno.isEmpty()) {
				for (TarifaFaixaDesconto tfd : colecaoRetorno) {
					tfd.setTarifaVigenciaDesconto(tarifaVigenciaDesconto);
					retorno.put(tfd.getTarifaVigenciaFaixa(), tfd);
				}
			}
		}

		return retorno;
	}

	/**
	 * Consultar consumo apurado intervalo.
	 *
	 * @param colecaoPontoConsumo
	 *            the colecao ponto consumo
	 * @param periodo
	 *            the periodo
	 * @return the hash map
	 */
	private Map<Date, BigDecimal> consultarConsumoApuradoIntervalo(Collection<PontoConsumo> colecaoPontoConsumo, Periodo periodo) {

		Collection<Object> retornoConsulta = null;
		Map<Date, BigDecimal> datasConsumo = new HashMap<Date, BigDecimal>();

		StringBuilder hql = new StringBuilder();
		hql.append(" select historicoMedicao.dataLeituraInformada, historicoConsumo.consumoApurado from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
		hql.append(" where ");
		hql.append(" historicoConsumo.pontoConsumo.chavePrimaria in ( :idPontoConsumo ) ");
		hql.append(" and historicoMedicao.dataLeituraInformada between :dataInicio and :dataFim ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = :isConsumoCiclo ");
		hql.append(" and historicoConsumo.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("idPontoConsumo", Util.collectionParaArrayChavesPrimarias(colecaoPontoConsumo));

		Util.adicionarRestricaoDataSemHora(query, periodo.dataInicio.toDate(), "dataInicio", Boolean.TRUE);
		Util.adicionarRestricaoDataSemHora(query, periodo.dataFim.toDate(), "dataFim", Boolean.FALSE);
		query.setBoolean("isConsumoCiclo", Boolean.FALSE);

		retornoConsulta = query.list();

		if (retornoConsulta != null && !retornoConsulta.isEmpty()) {
			for (Object object : retornoConsulta) {
				Object[] resultadoArray = (Object[]) object;
				Date data = (Date) resultadoArray[0];
				BigDecimal consumo = (BigDecimal) resultadoArray[1];
				if (datasConsumo.containsKey(data)) {
					BigDecimal consumoAnterior = datasConsumo.get(data);
					consumo = consumo.add(consumoAnterior);
				}
				datasConsumo.put(data, consumo);
			}
		}
		return datasConsumo;
	}

	/**
	 * Calcular valor consumo por faixa.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param consumoApurado
	 *            the consumo apurado
	 * @param mapaDescontosFaixa
	 *            the mapa descontos faixa
	 * @param colecaoTarifaVigenciaFaixa
	 *            the colecao tarifa vigencia faixa
	 * @param colecaoTributos
	 *            the colecao tributos
	 * @param escala
	 *            the escala
	 * @param tipoArredondamento
	 *            the tipo arredondamento
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param percentualUtilizado
	 *            the percentual utilizado
	 * @param consumoTotal
	 *            the consumo total
	 * @param precoGas
	 *            the preco gas
	 * @param ultimaLeitura
	 * @return the dados faixa vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private DadosFaixaVO calcularValorConsumoPorFaixa(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura, BigDecimal consumoApurado,
					Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> mapaDescontosFaixa,
					Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa, Collection<TributoAliquota> colecaoTributos,
					Integer escala, Integer tipoArredondamento, BigDecimal variacaoCambial, BigDecimal percentualUtilizado,
					BigDecimal consumoTotal, BigDecimal precoGas, Date ultimaLeitura, BigDecimal periodoLeitura) throws NegocioException {

		deleteme.append("\n...... Tipo de Cálculo: Na Faixa");

		TarifaFaixaDesconto tarifaFaixaDesconto = null;

		// Verifica se deve usar o 'consumo total'
		// para obter a faixa ou o 'consumo do
		// período'.
		BigDecimal consumo = null;
		if (consumoTotal != null) {
			consumo = consumoTotal;
		} else {
			consumo = consumoApurado;
		}

		TarifaVigenciaFaixa tarifaVigenciaFaixa = obterTarifaVigenciaFaixaPorConsumo(colecaoTarifaVigenciaFaixa, consumo);

		if (tarifaVigenciaFaixa == null) {
			throw new TarifaVigenciaFaixaException();
		}

		if (mapaDescontosFaixa != null && !mapaDescontosFaixa.isEmpty()) {
			tarifaFaixaDesconto = mapaDescontosFaixa.get(tarifaVigenciaFaixa);
		}

		return preencherDadosFaixaVO(pontoConsumo, itemFatura, consumoApurado, tarifaFaixaDesconto, tarifaVigenciaFaixa, colecaoTributos,
						escala, tipoArredondamento, variacaoCambial, percentualUtilizado, precoGas, BigDecimal.ZERO, null, ultimaLeitura, periodoLeitura);
	}

	/**
	 * Preencher dados faixa vo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param itemFatura the item fatura
	 * @param consumoApurado the consumo apurado
	 * @param desconto the desconto
	 * @param tarifaVigenciaFaixa the tarifa vigencia faixa
	 * @param colecaoTributos the colecao tributos
	 * @param escala the escala
	 * @param tipoArredondamento the tipo arredondamento
	 * @param variacaoCambial the variacao cambial
	 * @param percentualUtilizado the percentual utilizado
	 * @param precoGas the preco gas
	 * @param consumoRestante the consumo restante
	 * @param ultimaLeitura
	 * @return the dados faixa vo
	 * @throws NegocioException the negocio exception
	 */
	private DadosFaixaVO preencherDadosFaixaVO(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura, BigDecimal consumoApurado,
			TarifaFaixaDesconto desconto, TarifaVigenciaFaixa tarifaVigenciaFaixa, Collection<TributoAliquota> colecaoTributos,
			Integer escala, Integer tipoArredondamento, BigDecimal variacaoCambial, BigDecimal percentualUtilizado, BigDecimal precoGas,
			BigDecimal consumoRestante, String valorComplementarInformado, Date ultimaLeitura, BigDecimal periodoLeitura) throws NegocioException {
			
		deleteme.append("\n... Consumo Apurado: ").append(consumoApurado);
		deleteme.append("\n... Tarifa Vigencia Faixa: ").append(tarifaVigenciaFaixa.getChavePrimaria());
		if (colecaoTributos != null) {
			deleteme.append("\n... Tributos: ").append(colecaoTributos.size());
		} else {
			deleteme.append("\n... Tributos: 0");
		}

		if (colecaoTributos != null && !colecaoTributos.isEmpty()) {
			for (TributoAliquota tributoAliquota : colecaoTributos) {
				deleteme.append("\n..").append(tributoAliquota.getTributo().getDescricao()).append(": ")
						.append(tributoAliquota.getValorPercentualAliquota());
			}
		}

		if (desconto != null) {
			deleteme.append("\n... Tarifa Faixa Desconto: ").append(desconto.getChavePrimaria());
		}

		DadosFaixaVO dadosFaixaVO = new DadosFaixaVO();
		dadosFaixaVO.setTarifaVigenciaFaixa(tarifaVigenciaFaixa);
		BigDecimal valorInformado = null;
		if (valorComplementarInformado != null) {
			BigDecimal valor = BigDecimal.ZERO;
			try {
				valor = Util.converterCampoStringParaValorBigDecimal("Valor", valorComplementarInformado, Constantes.FORMATO_VALOR_BR,
								Constantes.LOCALE_PADRAO);
			} catch (FormatoInvalidoException e) {
				LOG.info(e.getMessage(), e);
			}
			if (valor.compareTo(BigDecimal.ZERO) > 0) {
				valorInformado = valor;
			}
		}

		// se é a primeira faixa e existe valor
		// fixo e não existe valor variável, usar
		// consumo mínimo. #4131
		if ((tarifaVigenciaFaixa.getMedidaInicio().compareTo(BigDecimal.ZERO) == 0)
						&& (tarifaVigenciaFaixa.getValorFixo() != null && tarifaVigenciaFaixa.getValorFixo().compareTo(BigDecimal.ZERO) > 0)
						&& (tarifaVigenciaFaixa.getValorVariavel() == null || tarifaVigenciaFaixa.getValorVariavel().compareTo(
										BigDecimal.ZERO) == 0) && (consumoRestante.compareTo(BigDecimal.ZERO) <= 0)) {
			dadosFaixaVO.setConsumoFaturado(tarifaVigenciaFaixa.getMedidaFim().multiply(periodoLeitura));
		} else {
			dadosFaixaVO.setConsumoFaturado(consumoApurado.multiply(periodoLeitura));
		}

		dadosFaixaVO.setConsumoApurado(consumoApurado.multiply(periodoLeitura));

		BigDecimal descontoNoValorFixo = BigDecimal.ZERO;
		BigDecimal descontoNoValorVariavel = BigDecimal.ZERO;
		
		if (desconto != null) {

			BigDecimal valorTotalDescontos = BigDecimal.ZERO;

			if (desconto.getValorDescontoFixo() != null) {
				descontoNoValorFixo = desconto.getValorDescontoFixo();
				dadosFaixaVO.setTarifaFaixaDescontoFixo(desconto);
			} else if (desconto.getPercentualDescontoFixo() != null) {
				descontoNoValorFixo = tarifaVigenciaFaixa.getValorFixo().multiply(desconto.getPercentualDescontoFixo());
				dadosFaixaVO.setTarifaFaixaDescontoFixo(desconto);
			}

			if (desconto.getValorDescontoVariavel() != null) {
				descontoNoValorVariavel = desconto.getValorDescontoVariavel();
				dadosFaixaVO.setTarifaFaixaDescontoVariavel(desconto);
			} else if (desconto.getPercentualDescontoVariavel() != null) {
				descontoNoValorVariavel = tarifaVigenciaFaixa.getValorVariavel()
						.multiply(desconto.getPercentualDescontoVariavel());
				dadosFaixaVO.setTarifaFaixaDescontoVariavel(desconto);
			}

			valorTotalDescontos = valorTotalDescontos.add(descontoNoValorFixo).add(descontoNoValorVariavel);

			dadosFaixaVO.setValorDescontoFixo(descontoNoValorFixo);
			dadosFaixaVO.setValorDescontoVariavel(descontoNoValorVariavel);

		} else {
			dadosFaixaVO.setValorDescontoFixo(BigDecimal.ZERO);
			dadosFaixaVO.setValorDescontoVariavel(BigDecimal.ZERO);
		}

		deleteme.append("\n..Desconto fixo: ").append(descontoNoValorFixo);
		deleteme.append("\n..Desconto variável: ").append(descontoNoValorVariavel);

			
		// calcula valor variavel sem imposto sem
		// desconto
		BigDecimal valorVariavelSemImpostoSemDesconto = calcularValorVariavelSemImpostoSemDesconto(tarifaVigenciaFaixa, consumoApurado,
						variacaoCambial);
		dadosFaixaVO.setValorVariavelSemImpostoSemDesconto(valorVariavelSemImpostoSemDesconto);

		// calcula valor variavel com imposto sem
		// desconto
		BigDecimal valorVariavelComImpostoSemDesconto = calcularValorVariavelComImpostoSemDesconto(pontoConsumo, itemFatura,
						colecaoTributos, tarifaVigenciaFaixa, consumoApurado, escala, tipoArredondamento, variacaoCambial, precoGas,
						valorInformado, ultimaLeitura);
		dadosFaixaVO.setValorVariavelComImpostoSemDesconto(valorVariavelComImpostoSemDesconto.multiply(periodoLeitura)
				.setScale(15, RoundingMode.HALF_UP).stripTrailingZeros());

		// calcula valor fixo sem imposto sem
		// desconto
		BigDecimal valorFixoSemImpostoSemDesconto = calcularValorFixoSemImpostoSemDesconto(tarifaVigenciaFaixa, variacaoCambial,
						percentualUtilizado, valorInformado, colecaoTributos);
		dadosFaixaVO.setValorFixoSemImpostoSemDesconto(valorFixoSemImpostoSemDesconto);

		// calcula valor fixo com imposto sem
		// desconto
		BigDecimal valorFixoComImpostoSemDesconto = calcularValorFixoComImpostoSemDesconto(pontoConsumo, itemFatura, colecaoTributos,
				tarifaVigenciaFaixa, consumoApurado, escala, tipoArredondamento, variacaoCambial, percentualUtilizado, precoGas,
						valorInformado, ultimaLeitura);
		dadosFaixaVO.setValorFixoComImpostoSemDesconto(valorFixoComImpostoSemDesconto.multiply(periodoLeitura)
				.setScale(15, RoundingMode.HALF_UP).stripTrailingZeros());

		if (desconto != null) {

			// DIMINUIR OS DESCONTOS GERADOS A PARTIR DO GRUPO ECONOMICO
			// calcula valor variavel sem imposto
			// com desconto
			BigDecimal valorVariavelSemImpostoComDesconto = calcularValorVariavelSemImpostoComDesconto(tarifaVigenciaFaixa, consumoApurado,
							descontoNoValorVariavel, variacaoCambial);
			dadosFaixaVO.setValorVariavelSemImpostoComDesconto(valorVariavelSemImpostoComDesconto);

			// calcula valor variavel com imposto
			// com desconto
			BigDecimal valorVariavelComImpostoComDesconto = calcularValorVariavelComImpostoComDesconto(pontoConsumo, itemFatura,
							colecaoTributos, tarifaVigenciaFaixa, consumoApurado, descontoNoValorVariavel, escala, tipoArredondamento,
							variacaoCambial, precoGas, ultimaLeitura);
			
			if(desconto.getValorDescontoVariavel().compareTo(BigDecimal.ZERO) <= 0) {
				dadosFaixaVO.setValorVariavelComImpostoComDesconto(dadosFaixaVO.getValorVariavelComImpostoSemDesconto());
			} else {
			dadosFaixaVO.setValorVariavelComImpostoComDesconto(valorVariavelComImpostoComDesconto
					.multiply(periodoLeitura).setScale(15, RoundingMode.HALF_UP).stripTrailingZeros());
			}

			// calcula valor fixo sem imposto com
			// desconto
			BigDecimal valorFixoSemImpostoComDesconto = calcularValorFixoSemImpostoComDesconto(tarifaVigenciaFaixa, consumoApurado,
							descontoNoValorFixo, variacaoCambial, percentualUtilizado);
			dadosFaixaVO.setValorFixoSemImpostoComDesconto(valorFixoSemImpostoComDesconto);

			// calcula valor fixo com imposto com
			// desconto
			BigDecimal valorFixoComImpostoComDesconto = calcularValorFixoComImpostoComDesconto(pontoConsumo, itemFatura, colecaoTributos,
					tarifaVigenciaFaixa, consumoApurado, descontoNoValorFixo, escala, tipoArredondamento, variacaoCambial,
							percentualUtilizado, precoGas, ultimaLeitura);

			if(desconto.getValorDescontoFixo().compareTo(BigDecimal.ZERO) <= 0) {
				dadosFaixaVO.setValorFixoComImpostoComDesconto(dadosFaixaVO.getValorFixoComImpostoSemDesconto());
			} else {
				dadosFaixaVO.setValorFixoComImpostoComDesconto(valorFixoComImpostoComDesconto.multiply(periodoLeitura)
						.setScale(15, RoundingMode.HALF_UP).stripTrailingZeros());
			}

		} else {

			dadosFaixaVO.setValorVariavelSemImpostoComDesconto(valorVariavelSemImpostoSemDesconto);
			dadosFaixaVO.setValorVariavelComImpostoComDesconto(dadosFaixaVO.getValorVariavelComImpostoSemDesconto());
			dadosFaixaVO.setValorFixoSemImpostoComDesconto(valorFixoSemImpostoSemDesconto);
			dadosFaixaVO.setValorFixoComImpostoComDesconto(dadosFaixaVO.getValorFixoComImpostoSemDesconto());

		}

		// Sem substituição

		BigDecimal valorTotalComImposto = null;
		if (valorInformado != null && valorInformado.compareTo(BigDecimal.ZERO) > 0) {
			valorTotalComImposto = valorInformado;
		} else {
			valorTotalComImposto =
					dadosFaixaVO.getValorFixoComImpostoComDesconto().add(dadosFaixaVO.getValorVariavelComImpostoComDesconto());
			valorTotalComImposto = aplicarDesconto(pontoConsumo, valorTotalComImposto);
		}
		dadosFaixaVO.setValorTotalComImpostoSemSubstituicao(valorTotalComImposto);

		dadosFaixaVO.setTipoArredondamento(tipoArredondamento);

		return dadosFaixaVO;
	}

	private BigDecimal aplicarDesconto(PontoConsumo pontoConsumo, BigDecimal valor) {

		BigDecimal valorTotal = valor;
		if (!isDescontoAplicadoValortotal) {

			BigDecimal desc = ServiceLocator.getInstancia().getControladorFatura()
							.obterDescontoGrupoEconomicoPorPontoConsumo(pontoConsumo);

			if (NumeroUtil.naoNulo(desc)
							&& NumeroUtil.maiorQue(valorTotal, desc)) {
				valorTotal = valorTotal.subtract(desc);
				isDescontoAplicadoValortotal = true;
			}
		}
		return valorTotal;
	}

	/**
	 * Calcular valor fixo com imposto com desconto.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param itemFatura the item fatura
	 * @param colecaoTributos the colecao tributos
	 * @param tarifaVigenciaFaixa the tarifa vigencia faixa
	 * @param consumoApurado the consumo apurado
	 * @param descontoNoValorFixo the desconto no valor fixo
	 * @param escala the escala
	 * @param tipoArredondamento the tipo arredondamento
	 * @param variacaoCambial the variacao cambial
	 * @param percentualUtilizado the percentual utilizado
	 * @param precoGas the preco gas
	 * @param ultimaLeitura
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal calcularValorFixoComImpostoComDesconto(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
			Collection<TributoAliquota> colecaoTributos, TarifaVigenciaFaixa tarifaVigenciaFaixa, BigDecimal consumoApurado,
			BigDecimal descontoNoValorFixo, Integer escala, Integer tipoArredondamento, BigDecimal variacaoCambial,
			BigDecimal percentualUtilizado, BigDecimal precoGas, Date ultimaLeitura) throws NegocioException {

		/**
		 * Obs. percentualUtilizado - Essa
		 * variável deve ser o percentual que será
		 * utilizado do valor da faixa.
		 * Será a qtd de dias do período dividido
		 * pela qtd de dias de medicao. Se for
		 * diário ou média diária deve ser igual a
		 * 1.
		 */
		BigDecimal valorFixoComImposto = BigDecimal.ZERO;

		if (tarifaVigenciaFaixa != null && consumoApurado != null) {

			// Proporcional ao percentual
			// utilizado.
			valorFixoComImposto = tarifaVigenciaFaixa.getValorFixo().multiply(percentualUtilizado);

			deleteme.append("\n *valor fixo = ").append(tarifaVigenciaFaixa.getValorFixo());

			deleteme.append("\n *percentualUtilizado = ").append(percentualUtilizado);

			deleteme.append("\n *valor fixo proporcional = ").append(tarifaVigenciaFaixa.getValorFixo().multiply(percentualUtilizado));

			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorFixoComImposto = valorFixoComImposto.multiply(variacaoCambial);
			}

			if (valorFixoComImposto.compareTo(BigDecimal.ZERO) > 0) {
				if (descontoNoValorFixo != null) {
					valorFixoComImposto = valorFixoComImposto.subtract(descontoNoValorFixo);
				}
				
				if (colecaoTributos != null) {
					Collection<DadosTributoVO> retornoTributos = calcularTributos(pontoConsumo, itemFatura, colecaoTributos, escala,
									tipoArredondamento, valorFixoComImposto, precoGas, null, ultimaLeitura);

					valorFixoComImposto = adicionaValorVariavelImposto(valorFixoComImposto, retornoTributos).setScale(4, RoundingMode.HALF_UP);
					
				}	
				
				TarifaVigencia tarifaVigencia = tarifaVigenciaFaixa.getTarifaVigencia();
				
				if(tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
					BigDecimal qtdM3 = tarifaVigenciaFaixa.getMedidaFim().subtract(tarifaVigenciaFaixa.getMedidaInicio());
					BigDecimal valorIcms = tarifaVigencia.getIcmsSubstituidoMetroCubico().multiply(qtdM3);
					valorFixoComImposto = valorFixoComImposto.add(valorIcms);
				}

			}
		}

		deleteme.append("\n... ValorFixoComImpostoComDesconto = ").append(valorFixoComImposto);

		return valorFixoComImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#calcularTributos(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * br.com.ggas.geral.EntidadeConteudo, java.util.Collection, java.lang.Integer, java.lang.Integer, java.math.BigDecimal,
	 * java.math.BigDecimal)
	 */
	@Override
	public Collection<DadosTributoVO> calcularTributos(PontoConsumo pontoConsumo,
					EntidadeConteudo itemFatura, Collection<TributoAliquota> colecaoTributos,
					Integer escala, Integer tipoArredondamento, BigDecimal valorTarifa,
					BigDecimal precoGas, BigDecimal valorComplementarInformado, Date ultimaLeitura)
									throws NegocioException {

		ControladorParametroSistema controladorParametroSistema =
						ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorRamoAtividadeSubstituicaoTributaria controladorRamoAtividadeSubstTrib =
						ServiceLocator.getInstancia().getControladorRamoAtividadeSubstituicaoTributaria();

		ControladorConstanteSistema controladorConstanteSistema =
						ServiceLocator.getInstancia().getControladorConstanteSistema();

		Collection<DadosTributoVO> retorno = new ArrayList<DadosTributoVO>();
		BigDecimal valorComImposto = null;
		BigDecimal somaTributos = BigDecimal.ZERO;
		BigDecimal baseCalculoICMSSubstituto = null;
		BigDecimal baseCalculoICMSNormal = null;

		if (valorICMS == null) {
			String paramICMS = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS);
			valorICMS = Long.valueOf(paramICMS);
		}

		// Verifica se tem substituição tributária com substituto PETROBRAS e MVA, caso
		// seja diferente disso a substituição será feita no final do cálculo.
		RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstTributaria = null;
		Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> mapaRamoAtividadeSubstituicaoTributaria =
				(Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>>) FaturamentoCache.getInstance()
						.get(FaturamentoCache.RAMO_ATIVIDADE);
		if (pontoConsumo != null) {
			RamoAtividade ramoAtividade = pontoConsumo.getRamoAtividade();
			if (pontoConsumo.getDadosResumoPontoConsumo().getRamoAtividadeSubstituicaoTributaria() != null) {
				ramoAtividadeSubstTributaria = pontoConsumo.getDadosResumoPontoConsumo().getRamoAtividadeSubstituicaoTributaria();
			} else if (mapaRamoAtividadeSubstituicaoTributaria != null) {
				Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
						mapaRamoAtividadeSubstituicaoTributaria.get(ramoAtividade);
				ramoAtividadeSubstTributaria = controladorRamoAtividadeSubstTrib
						.escolherRamoAtividadeSubstituicaoTributariaEmVigencia(listaRamoAtividadeSubstituicaoTributaria, ultimaLeitura);
				if (ramoAtividadeSubstTributaria != null) {
					pontoConsumo.getDadosResumoPontoConsumo().setRamoAtividadeSubstituicaoTributaria(ramoAtividadeSubstTributaria);
					pontoConsumo.getDadosResumoPontoConsumo().setTemSubstituicaoTributaria(Boolean.TRUE);
				}
			} else {
				ramoAtividadeSubstTributaria =
						controladorRamoAtividadeSubstTrib.obterRamoAtividadeSubstTribPorRamoAtividade(ramoAtividade, ultimaLeitura);
				if (ramoAtividadeSubstTributaria != null) {
					pontoConsumo.getDadosResumoPontoConsumo().setRamoAtividadeSubstituicaoTributaria(ramoAtividadeSubstTributaria);
					pontoConsumo.getDadosResumoPontoConsumo().setTemSubstituicaoTributaria(Boolean.TRUE);
				}
			}
		}

		// Obtém o tipo de substituto.
		if (valorTipoSubstituto == null) {
			String valorSubst = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(TIPO_SUBSTITUTO);
			valorTipoSubstituto = Long.valueOf(valorSubst);
		}

		// Soma todos os tributos, exceto o ICMS
		BigDecimal icms = null;
		for (TributoAliquota tributoAliquota : colecaoTributos) {
			BigDecimal aliquota = tributoAliquota.getValorAliquota();
			if (tributoAliquota.getTributo() != null && valorICMS != null && tributoAliquota.getTributo().getChavePrimaria() != valorICMS) {
				somaTributos = somaTributos.add(aliquota);
			} else {
				icms = getControladorFatura().aplicarReducaoBaseCalculo(aliquota, tributoAliquota);
			}
		}

		// Caso tenha substituição com substituto PETROBRAS e tipo MVA.
		if (ramoAtividadeSubstTributaria != null && icms != null
						&& ramoAtividadeSubstTributaria.getTipoSubstituicao().getChavePrimaria() == EntidadeConteudo.CHAVE_MVA
						&& valorTipoSubstituto == EntidadeConteudo.CHAVE_PETROBRAS) {

			if (precoGas == null || precoGas.compareTo(BigDecimal.ZERO) == 0) {
				throw new PrecoGasException();
			}

			// Divisor = 1 - (somaTributos)
			BigDecimal divisor = null;
			if (BigDecimal.ONE.compareTo(somaTributos) > 0) {
				divisor = BigDecimal.ONE.subtract(somaTributos);
			} else {
				throw new TributoMaiorValorException();
			}

			/**
			 * 1. Custo do Gás com tributos = {(C.
			 * Gás ex tributos)/[1- (1,65% + 7,6%
			 * + 17%)]} 2. ICMS Total = Custo do
			 * Gás com tributos x (1+MVA) x 17% 3.
			 * {(Tarifa ex-tributos + ICMS
			 * Total)/[1- (1,65% + 7,6%)]}
			 */

			BigDecimal percentualSubstitutoMaisUm = BigDecimal.ONE.add(ramoAtividadeSubstTributaria.getPercentualSubstituto());

			BigDecimal icmsTotal = precoGas.multiply(percentualSubstitutoMaisUm).multiply(icms);

			valorComImposto = (valorTarifa.add(icmsTotal)).divide(divisor, escala, tipoArredondamento);

			// Base cálculo ICMS
			baseCalculoICMSSubstituto = precoGas.multiply(percentualSubstitutoMaisUm);
			// vsm
			baseCalculoICMSNormal = valorTarifa.divide(BigDecimal.ONE.subtract(somaTributos.add(icms)), escala, tipoArredondamento);

		} else {

			if (icms != null) {
				somaTributos = somaTributos.add(icms);
			}

			BigDecimal divisor = null;
			if (BigDecimal.ONE.compareTo(somaTributos) > 0) {
				divisor = BigDecimal.ONE.subtract(somaTributos);
			} else {
				throw new TributoMaiorValorException();
			}

			if (valorComplementarInformado != null && valorComplementarInformado.compareTo(BigDecimal.ZERO) > 0) {
				valorComImposto = valorComplementarInformado;
				baseCalculoICMSNormal = valorComImposto;
			} else {
				valorComImposto = valorTarifa.divide(divisor, escala, tipoArredondamento);
				baseCalculoICMSNormal = valorComImposto;
			}

		}

		DadosTributoVO dadosTributoVO = null;

		for (TributoAliquota tributoAliquota : colecaoTributos) {

			dadosTributoVO = new DadosTributoVO();
			dadosTributoVO.setTributoAliquota(tributoAliquota);
			BigDecimal aliquota = tributoAliquota.getValorAliquota();
			if ( tributoAliquota.getTributo() != null
							&& valorICMS != null
							&& tributoAliquota.getTributo().getChavePrimaria() == valorICMS) {
				aliquota = getControladorFatura().aplicarReducaoBaseCalculo(aliquota, tributoAliquota);
			}

			if (tributoAliquota.getTributo() != null
							&& valorICMS != null
							&& tributoAliquota.getTributo().getChavePrimaria() == valorICMS
							&& baseCalculoICMSSubstituto != null) {

				dadosTributoVO.setBaseCalculo(baseCalculoICMSSubstituto);
				dadosTributoVO.setValor(baseCalculoICMSSubstituto.multiply(aliquota));

			} else {

				dadosTributoVO.setBaseCalculo(valorComImposto);
				dadosTributoVO.setValor(valorComImposto.multiply(aliquota));

			}

			dadosTributoVO.setBaseCalculoNormal(baseCalculoICMSNormal);
			dadosTributoVO.setValorNormal(
							baseCalculoICMSNormal.multiply(aliquota));

			retorno.add(dadosTributoVO);
		}

		return retorno;
	}

	/**
	 * Calcular valor fixo sem imposto com desconto.
	 *
	 * @param tarifaVigenciaFaixa
	 *            the tarifa vigencia faixa
	 * @param consumoApurado
	 *            the consumo apurado
	 * @param descontoNoValorFixo
	 *            the desconto no valor fixo
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param percentualUtilizado
	 *            the percentual utilizado
	 * @return the big decimal
	 */
	private BigDecimal calcularValorFixoSemImpostoComDesconto(TarifaVigenciaFaixa tarifaVigenciaFaixa, BigDecimal consumoApurado,
					BigDecimal descontoNoValorFixo, BigDecimal variacaoCambial, BigDecimal percentualUtilizado) {

		/**
		 * Obs. percentualUtilizado - Essa variável deve ser o percentual que será utilizado do valor da faixa.
		 * Será a qtd de dias do período dividido pela qtd de dias de medicao. Se for
		 * diário ou média diária deve ser igual a
		 * 1.
		 */

		BigDecimal retorno = BigDecimal.ZERO;
		if (tarifaVigenciaFaixa != null && consumoApurado != null) {

			// Proporcional ao percentual
			// utilizado.
			BigDecimal valorFixoSemImpostoComDesconto = tarifaVigenciaFaixa.getValorFixo().multiply(percentualUtilizado);

			deleteme.append("\n *valor fixo = ").append(tarifaVigenciaFaixa.getValorFixo());
			deleteme.append("\n *percentualUtilizado = ").append(percentualUtilizado);
			deleteme.append("\n *valor fixo proporcional = ").append(tarifaVigenciaFaixa.getValorFixo().multiply(percentualUtilizado));

			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorFixoSemImpostoComDesconto = valorFixoSemImpostoComDesconto.multiply(variacaoCambial);
			}

			if (valorFixoSemImpostoComDesconto.compareTo(BigDecimal.ZERO) > 0 && descontoNoValorFixo != null) {
				valorFixoSemImpostoComDesconto = valorFixoSemImpostoComDesconto.subtract(descontoNoValorFixo);
				retorno = valorFixoSemImpostoComDesconto;
			}
		}
		deleteme.append("\n... ValorFixoSemImpostoComDesconto = ").append(retorno);
		return retorno;
	}

	/**
	 * Calcular valor variavel com imposto com desconto.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param colecaoTributos
	 *            the colecao tributos
	 * @param tarifaVigenciaFaixa
	 *            the tarifa vigencia faixa
	 * @param consumoApurado
	 *            the consumo apurado
	 * @param descontoNoValorVariavel
	 *            the desconto no valor variavel
	 * @param escala
	 *            the escala
	 * @param tipoArredondamento
	 *            the tipo arredondamento
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param precoGas
	 *            the preco gas
	 * @param ultimaLeitura
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal calcularValorVariavelComImpostoComDesconto(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Collection<TributoAliquota> colecaoTributos, TarifaVigenciaFaixa tarifaVigenciaFaixa, BigDecimal consumoApurado,
					BigDecimal descontoNoValorVariavel, Integer escala, Integer tipoArredondamento, BigDecimal variacaoCambial,
					BigDecimal precoGas, Date ultimaLeitura) throws NegocioException {

		BigDecimal valorVariavelComImposto = BigDecimal.ZERO;

		if (tarifaVigenciaFaixa != null && consumoApurado != null) {

			valorVariavelComImposto = tarifaVigenciaFaixa.getValorVariavel();

			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorVariavelComImposto = valorVariavelComImposto.multiply(variacaoCambial);
			}

			if (valorVariavelComImposto.compareTo(BigDecimal.ZERO) > 0) {

				if (descontoNoValorVariavel != null) {
					valorVariavelComImposto = valorVariavelComImposto.subtract(descontoNoValorVariavel);
				}
				
				if (colecaoTributos != null) {
					Collection<DadosTributoVO> retornoTributos = calcularTributos(pontoConsumo, itemFatura, colecaoTributos, escala,
									tipoArredondamento, valorVariavelComImposto, precoGas, null, ultimaLeitura);

					valorVariavelComImposto = adicionaValorVariavelImposto(valorVariavelComImposto, retornoTributos).setScale(4, RoundingMode.HALF_UP);
					
				}
				
				TarifaVigencia tarifaVigencia = tarifaVigenciaFaixa.getTarifaVigencia();
				
				if(tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
					valorVariavelComImposto = valorVariavelComImposto.add(tarifaVigencia.getIcmsSubstituidoMetroCubico());
				}

				valorVariavelComImposto = consumoApurado.multiply(valorVariavelComImposto);
				
			}
		}

		deleteme.append("\n... ValorVariavelComImpostoComDesconto = ").append(valorVariavelComImposto);
		return valorVariavelComImposto;
	}

	/**
	 * Calcular valor variavel sem imposto com desconto.
	 *
	 * @param tarifaVigenciaFaixa
	 *            the tarifa vigencia faixa
	 * @param consumoApurado
	 *            the consumo apurado
	 * @param descontoNoValorVariavel
	 *            the desconto no valor variavel
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @return the big decimal
	 */
	private BigDecimal calcularValorVariavelSemImpostoComDesconto(TarifaVigenciaFaixa tarifaVigenciaFaixa, BigDecimal consumoApurado,
					BigDecimal descontoNoValorVariavel, BigDecimal variacaoCambial) {

		BigDecimal retorno = BigDecimal.ZERO;

		if (tarifaVigenciaFaixa != null && consumoApurado != null) {

			BigDecimal valorVariavelSemImpostoComDesconto = tarifaVigenciaFaixa.getValorVariavel();

			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorVariavelSemImpostoComDesconto = valorVariavelSemImpostoComDesconto.multiply(variacaoCambial);
			}

			if (valorVariavelSemImpostoComDesconto.compareTo(BigDecimal.ZERO) > 0) {

				if (descontoNoValorVariavel != null) {
					valorVariavelSemImpostoComDesconto = valorVariavelSemImpostoComDesconto.subtract(descontoNoValorVariavel);
				}

				retorno = consumoApurado.multiply(valorVariavelSemImpostoComDesconto);
			}
		}

		deleteme.append("\n... ValorVariavelSemImpostoComDesconto = ").append(retorno);

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#obterTarifaVigenciaFaixaPorConsumo(java.util.Collection,
	 * java.math.BigDecimal)
	 */
	@Override
	public TarifaVigenciaFaixa obterTarifaVigenciaFaixaPorConsumo(Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa,
					BigDecimal consumoApurado) {

		TarifaVigenciaFaixa tarifaVigenciaFaixa = null;
		if (colecaoTarifaVigenciaFaixa != null && !colecaoTarifaVigenciaFaixa.isEmpty()) {
			for (TarifaVigenciaFaixa vigenciaFaixa : colecaoTarifaVigenciaFaixa) {
				if ((vigenciaFaixa.getMedidaInicio().compareTo(consumoApurado) <= 0)
								&& (vigenciaFaixa.getMedidaFim().compareTo(consumoApurado) >= 0)) {
					tarifaVigenciaFaixa = vigenciaFaixa;
					break;
				}
			}
		}
		return tarifaVigenciaFaixa;

	}

	/**
	 * Calcular valor consumo por cascata.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param itemFatura the item fatura
	 * @param consumoApurado the consumo apurado
	 * @param mapaDescontosFaixa the mapa descontos faixa
	 * @param colecaoTarifaVigenciaFaixa the colecao tarifa vigencia faixa
	 * @param colecaoTributos the colecao tributos
	 * @param consumoAcumulado the consumo acumulado
	 * @param escala the escala
	 * @param tipoArredondamento the tipo arredondamento
	 * @param variacaoCambial the variacao cambial
	 * @param percentualInicial the percentual inicial
	 * @param precoGas the preco gas
	 * @param ultimaLeitura
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	private Collection<DadosFaixaVO> calcularValorConsumoPorCascata(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
			BigDecimal consumoApurado, Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> mapaDescontosFaixa,
			Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa, Collection<TributoAliquota> colecaoTributos,
			BigDecimal consumoAcumulado, Integer escala, Integer tipoArredondamento, BigDecimal variacaoCambial,
			BigDecimal percentualInicial, BigDecimal precoGas, String valorComplementarInformado, Date ultimaLeitura, BigDecimal periodoLeitura)
			throws NegocioException {

		deleteme.append("\n...... Tipo de Cálculo: Cascata");

		Collection<DadosFaixaVO> colecaoFaixas = new ArrayList<DadosFaixaVO>();

		BigDecimal consumoAcumuladoRestante = consumoAcumulado;
		BigDecimal consumoRestante = null;
		if (consumoApurado != null) {
			consumoRestante = consumoApurado;
		} else {
			consumoRestante = BigDecimal.ZERO;
		}

		DadosFaixaVO dadosFaixaVO = null;
		BigDecimal valorTotalDescontos = BigDecimal.ZERO;
		BigDecimal valorTotalFaturado = BigDecimal.ZERO;

		// Valor final da faixa anterior.
		BigDecimal medidaFimAnterior = BigDecimal.ZERO;

		if (colecaoTarifaVigenciaFaixa != null) {
			deleteme.append("\n................................. Acumulado Atual: ").append(consumoAcumulado);
			for (TarifaVigenciaFaixa faixa : colecaoTarifaVigenciaFaixa) {
				TarifaFaixaDesconto desconto = null;

				BigDecimal intervaloFaixa = faixa.getMedidaFim().subtract(medidaFimAnterior);
				medidaFimAnterior = faixa.getMedidaFim();
				BigDecimal percentualUtilizado = BigDecimal.ONE;

				boolean isAcumuladoProcessado = false;
				if (consumoAcumuladoRestante != null && consumoAcumuladoRestante.compareTo(BigDecimal.ZERO) > 0) {
					if (intervaloFaixa.compareTo(consumoAcumuladoRestante) > 0) {

						percentualUtilizado = percentualInicial;

						intervaloFaixa = intervaloFaixa.subtract(consumoAcumuladoRestante);
						isAcumuladoProcessado = true;
						deleteme.append("\n................................. Faixa ").append(faixa.getChavePrimaria())
								.append(" Parcialmente Já Processado: ").append(consumoAcumuladoRestante);
						consumoAcumuladoRestante = BigDecimal.ZERO;
					} else {
						consumoAcumuladoRestante = consumoAcumuladoRestante.subtract(intervaloFaixa);
						deleteme.append("\n................................. Faixa ").append(faixa.getChavePrimaria())
								.append(" Já Processada! [").append(intervaloFaixa).append("]");
					}
				} else {
					isAcumuladoProcessado = true;
				}

				if (isAcumuladoProcessado) {

					// Calcula o Consumo Apurado
					// na Faixa
					BigDecimal consumoApuradoNaFaixa = null;
					if (intervaloFaixa.compareTo(consumoRestante) > 0) {
						consumoApuradoNaFaixa = consumoRestante;
					} else {
						consumoApuradoNaFaixa = intervaloFaixa;
					}
					deleteme.append("\n.................................  Faixa ").append(faixa.getChavePrimaria())
							.append(" Processada Agora: ").append(consumoApuradoNaFaixa);

					if (mapaDescontosFaixa != null && mapaDescontosFaixa.containsKey(faixa)) {
						desconto = mapaDescontosFaixa.get(faixa);
					}
					// Subtrai o consumo de acordo com a tabela da tarifa
					consumoRestante = consumoRestante.subtract(intervaloFaixa);

					// Calcula o o valor do consumo de volume de acordo com as tarifas
					dadosFaixaVO = preencherDadosFaixaVO(pontoConsumo, itemFatura, consumoApuradoNaFaixa, desconto, faixa, colecaoTributos,
									escala, tipoArredondamento, variacaoCambial, percentualUtilizado, precoGas, consumoRestante,
									valorComplementarInformado, ultimaLeitura, periodoLeitura);

					valorTotalFaturado = valorTotalFaturado.add(dadosFaixaVO.getValorFixoComImpostoSemDesconto());
					valorTotalFaturado = valorTotalFaturado.add(dadosFaixaVO.getValorVariavelComImpostoSemDesconto());
					if (dadosFaixaVO.getValorDescontoFixo() != null) {
						valorTotalDescontos = valorTotalDescontos.add(dadosFaixaVO.getValorDescontoFixo());
					}
					if (dadosFaixaVO.getValorDescontoVariavel() != null) {
						valorTotalDescontos = valorTotalDescontos.add(dadosFaixaVO.getValorDescontoVariavel());
					}
					colecaoFaixas.add(dadosFaixaVO);

					if (consumoRestante.compareTo(BigDecimal.ZERO) <= 0) {
						break;
					}
				}
			}
		}

		return colecaoFaixas;
	}

	/**
	 * Método responsável por calcular o valor
	 * fixo sem imposto e sem desconto.
	 *
	 * @param faixa
	 *            the faixa
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param percentualUtilizado
	 *            the percentual utilizado
	 * @return the big decimal
	 */
	private BigDecimal calcularValorFixoSemImpostoSemDesconto(TarifaVigenciaFaixa faixa, BigDecimal variacaoCambial,
					BigDecimal percentualUtilizado, BigDecimal valorComplementarInformado, Collection<TributoAliquota> colecaoTributos) {

		/**
		 * Obs. percentualUtilizado - Essa
		 * variável deve ser o percentual que será
		 * utilizado do valor da faixa.
		 * Será a qtd de dias do período dividido
		 * pela qtd de dias de medicao. Se for
		 * diário ou média diária deve ser igual a
		 * 1.
		 */

		BigDecimal valorFixoSemImposto = BigDecimal.ZERO;

		if (valorComplementarInformado != null && valorComplementarInformado.compareTo(BigDecimal.ZERO) > 0) {
			BigDecimal tributos = BigDecimal.ZERO;
			if (colecaoTributos != null && !colecaoTributos.isEmpty()) {
				for (TributoAliquota tributo : colecaoTributos) {
					//aliquota em valor
					tributos = tributos.add(tributo.getValorAliquota());
				}
			}
			return valorComplementarInformado.subtract(valorComplementarInformado.multiply(tributos));
		}

		if (faixa != null) {

			// Proporcional ao percentual
			// utilizado.
			if (faixa.getValorFixo() != null) {
				valorFixoSemImposto = faixa.getValorFixo().multiply(percentualUtilizado);
			}
			deleteme.append("\n *valor fixo = ").append(faixa.getValorFixo()).append("\n *percentualUtilizado = ")
					.append(percentualUtilizado).append("\n *valor fixo proporcional = ").append(valorFixoSemImposto);

			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorFixoSemImposto = valorFixoSemImposto.multiply(variacaoCambial);
			}
		}
		deleteme.append("\n... ValorFixoSemImpostoSemDesconto = ").append(valorFixoSemImposto);

		return valorFixoSemImposto;
	}

	/**
	 * Calcular valor fixo com imposto sem desconto.
	 *
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param colecaoTributos
	 *            the colecao tributos
	 * @param faixa
	 *            the faixa
	 * @param consumoApurado
	 *            the consumo apurado
	 * @param escala
	 *            the escala
	 * @param tipoArredondamento
	 *            the tipo arredondamento
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @param percentualUtilizado
	 *            the percentual utilizado
	 * @param precoGas
	 *            the preco gas
	 * @param ultimaLeitura
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal calcularValorFixoComImpostoSemDesconto(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Collection<TributoAliquota> colecaoTributos, TarifaVigenciaFaixa faixa, BigDecimal consumoApurado, Integer escala,
					Integer tipoArredondamento, BigDecimal variacaoCambial, BigDecimal percentualUtilizado, BigDecimal precoGas,
					BigDecimal valorComplementarInformado, Date ultimaLeitura) throws NegocioException {

		/**
		 * Obs. percentualUtilizado - Essa
		 * variável deve ser o percentual que será
		 * utilizado do valor da faixa.
		 * Será a qtd de dias do período dividido
		 * pela qtd de dias de medicao. Se for
		 * diário ou média diária deve ser igual a
		 * 1.
		 */
		BigDecimal valorFixoComImposto = BigDecimal.ZERO;

		if (faixa != null && consumoApurado != null) {

			// Proporcional ao percentual
			// utilizado.
			if (valorComplementarInformado != null && valorComplementarInformado.compareTo(BigDecimal.ZERO) > 0) {
				return valorComplementarInformado;
			} else if (faixa.getValorFixo() != null) {
				valorFixoComImposto = faixa.getValorFixo().multiply(percentualUtilizado);
			}
			deleteme.append("\n *valor fixo = ").append(faixa.getValorFixo()).append("\n *percentualUtilizado = ")
					.append(percentualUtilizado).append("\n *valor fixo proporcional = ").append(valorFixoComImposto);

			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorFixoComImposto = valorFixoComImposto.multiply(variacaoCambial);
			}

			if (valorFixoComImposto.compareTo(BigDecimal.ZERO) > 0 && colecaoTributos != null) {
				Collection<DadosTributoVO> retornoTributos = calcularTributos(pontoConsumo, itemFatura, colecaoTributos, escala,
								tipoArredondamento, valorFixoComImposto, precoGas, valorComplementarInformado, ultimaLeitura);
				valorFixoComImposto = adicionaValorVariavelImposto(valorFixoComImposto, retornoTributos);
			}
			
			if (valorFixoComImposto.compareTo(BigDecimal.ZERO) > 0) {
				TarifaVigencia tarifaVigencia = faixa.getTarifaVigencia();

				if (tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
					BigDecimal qtdM3 = faixa.getMedidaFim().subtract(faixa.getMedidaInicio());
					BigDecimal valorIcmsPorM3 = tarifaVigencia.getIcmsSubstituidoMetroCubico().multiply(qtdM3);
					valorFixoComImposto = valorFixoComImposto.add(valorIcmsPorM3);
				}
			}
		}
		deleteme.append("\n... ValorFixoComImpostoSemDesconto = ").append(valorFixoComImposto);

		return valorFixoComImposto;
	}

	/**
	 * Calcula o Valor Variável da Tarifa com Tributos e Sem Descontos.
	 *
	 * @param pontoConsumo Ponto de consumo.
	 * @param itemFatura Item de Fatura.
	 * @param colecaoTributos Os tributos que devem ser aplicados ao cálculo
	 * @param faixa A faixa em que o cálculo enquadrou o consumo
	 * @param consumoApurado the consumo apurado
	 * @param escala the escala
	 * @param tipoArredondamento the tipo arredondamento
	 * @param variacaoCambial the variacao cambial
	 * @param precoGas preço do gás médio para o período de medição.
	 * @param ultimaLeitura
	 * @return Valor Variável da Tarifa Com Tributos e Sem Descontos
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal calcularValorVariavelComImpostoSemDesconto(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
			Collection<TributoAliquota> colecaoTributos, TarifaVigenciaFaixa faixa, BigDecimal consumoApurado, Integer escala,
			Integer tipoArredondamento, BigDecimal variacaoCambial, BigDecimal precoGas, BigDecimal valorComplementarInformado,
			Date ultimaLeitura) throws NegocioException {

		BigDecimal valorVariavelSemImposto = BigDecimal.ZERO;
		BigDecimal valorVariavelComImposto = BigDecimal.ZERO;

		BigDecimal valorPercentual = BigDecimal.ZERO;

		BigDecimal valorTotal = BigDecimal.ZERO;

		if (faixa != null && consumoApurado != null) {

			if(NumeroUtil.naoNulo(valorComplementarInformado)){
				valorVariavelComImposto = valorComplementarInformado;
			} else {
				valorVariavelComImposto = faixa.getValorVariavel();
			}

			if (NumeroUtil.naoNulo(variacaoCambial)) {
				valorVariavelComImposto = valorVariavelComImposto.multiply(variacaoCambial);
			}

			if (NumeroUtil.maiorQueZero(valorVariavelComImposto)) {
				if (colecaoTributos != null) {

					Collection<DadosTributoVO> retornoTributos =
									calcularTributos(pontoConsumo, itemFatura, colecaoTributos, escala,
									tipoArredondamento, valorVariavelComImposto, precoGas, null, ultimaLeitura);

					valorVariavelComImposto = adicionaValorVariavelImposto(valorVariavelComImposto, retornoTributos);
					
					TarifaVigencia tarifaVigencia = faixa.getTarifaVigencia();
					
					if(tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
						valorVariavelComImposto = valorVariavelComImposto.add(tarifaVigencia.getIcmsSubstituidoMetroCubico());
					}

					valorPercentual = defineValorPercentual(faixa, valorVariavelComImposto, valorPercentual);
				} else {
					TarifaVigencia tarifaVigencia = faixa.getTarifaVigencia();
					
					if(tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
						valorVariavelComImposto = valorVariavelComImposto.add(tarifaVigencia.getIcmsSubstituidoMetroCubico());
					}
					
					valorPercentual = defineValorPercentual(faixa, valorVariavelComImposto, valorPercentual);
				}

				valorVariavelSemImposto = consumoApurado.multiply(valorVariavelComImposto.setScale(4, RoundingMode.HALF_UP));

			}
		}

		valorTotal = valorVariavelSemImposto;
		deleteme.append("\n... ValorVariavelComImpostoSemDesconto = ").append(valorVariavelComImposto);

		return valorTotal;
	}

	private BigDecimal defineValorPercentual(TarifaVigenciaFaixa faixa, BigDecimal valorVariavelComImposto, BigDecimal valorPercentual) {
		if(NumeroUtil.maiorQueZero(faixa.getValorVariavel())){
			valorPercentual = valorVariavelComImposto.divide(
							faixa.getValorVariavel(), RoundingMode.HALF_UP);
		}
		return valorPercentual;
	}

	private BigDecimal adicionaValorVariavelImposto(BigDecimal valorVariavelComImposto, Collection<DadosTributoVO> retornoTributos) {
		for (DadosTributoVO dadosTributoVO : retornoTributos) {
			valorVariavelComImposto = valorVariavelComImposto.add(
							dadosTributoVO.getValor());
		}
		return valorVariavelComImposto;
	}

	/**
	 * Calcula o Valor Variável da Tarifa sem
	 * Tributos e Sem Descontos.
	 *
	 * @param faixa
	 *            A faixa em que o cálculo
	 *            enquadrou o consumo
	 * @param consumoApuradoNaFaixa
	 *            O consumo apurado dentro dos
	 *            limites da faixa
	 * @param variacaoCambial
	 *            the variacao cambial
	 * @return Valor Variável da Tarifa Sem
	 *         Tributos e Sem Descontos
	 */

	// Calcula o valor do consumo apurado de acordo com a coleção das tarifas
	private BigDecimal calcularValorVariavelSemImpostoSemDesconto(TarifaVigenciaFaixa faixa, BigDecimal consumoApuradoNaFaixa,
					BigDecimal variacaoCambial) {

		BigDecimal valorVariavelSemImposto = BigDecimal.ZERO;

		if (faixa != null && consumoApuradoNaFaixa != null) {

			if (faixa.getValorVariavel() != null) {
				valorVariavelSemImposto = faixa.getValorVariavel();
			}
			if (variacaoCambial != null && variacaoCambial.compareTo(BigDecimal.ZERO) > 0) {
				valorVariavelSemImposto = valorVariavelSemImposto.multiply(variacaoCambial);
			}

			if (valorVariavelSemImposto.compareTo(BigDecimal.ZERO) > 0) {
				valorVariavelSemImposto = consumoApuradoNaFaixa.multiply(valorVariavelSemImposto);
			}
		}

		deleteme.append("\n... ValorVariavelSemImpostoSemDesconto = ").append(valorVariavelSemImposto);

		return valorVariavelSemImposto;
	}

	/**
	 * Consultar descontos por data.
	 *
	 * @param tarifa
	 *            the tarifa
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @return the collection
	 */
	private Collection<TarifaVigenciaDesconto> consultarDescontosPorData(Tarifa tarifa, Date dataInicio, Date dataFim) {


		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaDesconto().getSimpleName());
		hql.append(" tvd ");
		hql.append(" where ");
		hql.append(" tvd.tarifa.chavePrimaria = :idTarifa ");
		hql.append(" and ( ");
		hql.append("	     ( ");
		hql.append(" 		tvd.inicioVigencia between :dataInicio and :dataFim ");
		hql.append(" 		or tvd.fimVigencia between :dataInicio and :dataFim ");
		hql.append(" 	     ) ");
		hql.append(" 	     or ");
		hql.append(" 	     ( ");
		hql.append(" 		:dataInicio between tvd.inicioVigencia and tvd.fimVigencia ");
		hql.append(" 		or :dataFim between tvd.inicioVigencia and tvd.fimVigencia ");
		hql.append(" 	     ) ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTarifa", tarifa.getChavePrimaria());
		query.setDate("dataInicio", dataInicio);
		query.setDate("dataFim", dataFim);

		return query.list();
	}

	private class PeriodoVigenciaDesconto implements Comparable<PeriodoVigenciaDesconto>, Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2021450144047782782L;

		private Periodo periodo;

		private TarifaVigencia tarifaVigencia;

		private TarifaVigenciaDesconto desconto;

		/*
		 * (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(PeriodoVigenciaDesconto pvd) {

			int retorno = 0;
			if (this.periodo.dataInicio.isBefore(pvd.periodo.dataInicio)) {
				retorno = -1;
			} else if (this.periodo.dataInicio.isAfter(pvd.periodo.dataInicio)) {
				retorno = 1;
			}

			return retorno;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {

			boolean retorno = false;
			if (obj instanceof PeriodoVigenciaDesconto) {
				PeriodoVigenciaDesconto periodoVigenciaDesconto = (PeriodoVigenciaDesconto) obj;
				if (periodoVigenciaDesconto.periodo.equals(this.periodo)) {
					retorno = true;
				}
			} else {
				retorno = false;
			}
			return retorno;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {

			if (periodo != null && tarifaVigencia != null) {
				return new HashCodeBuilder(3, 5).append(periodo).append(tarifaVigencia).toHashCode();
			} else {
				return super.hashCode();
			}
		}

	}

	private class Periodo implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2720369369266217712L;

		private DateTime dataInicio;

		private DateTime dataFim;

		/**
		 * Instantiates a new periodo.
		 *
		 * @param inicio
		 *            the inicio
		 * @param fim
		 *            the fim
		 */
		public Periodo(DateTime inicio, DateTime fim) {

			this.dataInicio = inicio;
			this.dataFim = fim;
		}

		/**
		 * Instantiates a new periodo.
		 *
		 * @param inicio
		 *            the inicio
		 * @param fim
		 *            the fim
		 */
		public Periodo(Date inicio, Date fim) {

			this.dataInicio = new DateTime(inicio);
			this.dataFim = new DateTime(fim);
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {

			boolean retorno = false;
			if (obj instanceof Periodo) {
				Periodo periodo = (Periodo) obj;
				if ((periodo.dataInicio.equals(this.dataInicio)) && (periodo.dataFim.equals(this.dataFim))) {
					retorno = true;
				}
			} else {
				retorno = false;
			}
			return retorno;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {

			if (dataInicio != null && dataFim != null) {
				return new HashCodeBuilder(3, 5).append(dataInicio).append(dataFim).toHashCode();
			} else {
				return super.hashCode();
			}
		}

		public int getQuantidadeDiasIntervalo() {

			int retorno = 0;
			DateTime dateTimeIteracao = dataInicio;
			while (!dateTimeIteracao.isAfter(dataFim)) {
				retorno++;
				dateTimeIteracao = dateTimeIteracao.plusDays(1);
			}
			return retorno;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#validaDadosCalcularValorFornecimentoGas(java.lang.String,
	 * java.lang.Long,
	 * java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public void validaDadosCalcularValorFornecimentoGas(String idPontoConsumo, Long idTarifaReal, Long idTarifaSimulada,
					Long idHistoricoConsumo, Long idVigencia) throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(idPontoConsumo)) {
			stringBuilder.append("PONTO_DE_CONSUMO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (idTarifaReal == null || idTarifaReal <= 0L) {
			stringBuilder.append("TARIFA_REAL");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (idTarifaSimulada == null || idTarifaSimulada <= 0L) {
			stringBuilder.append("TARIFA_SIMULADA");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (idHistoricoConsumo == null || idHistoricoConsumo <= 0L) {
			stringBuilder.append("HISTORICO_CONSUMO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (idVigencia == null || idVigencia <= 0L) {
			stringBuilder.append("TARIFA_VIGENCIA");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}

	}

	@Override
	public String getLog() {

		return deleteme.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#validaDadosCalcularFornecimentoFicticio(java.lang.Long,
	 * java.util.Date,
	 * java.util.Date, java.lang.String)
	 */
	@Override
	public void validaDadosCalcularFornecimentoFicticio(Long idTarifa, Date dataInicio, Date dataFim, String totalConsumido)
					throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (idTarifa == null || idTarifa <= 0L) {
			stringBuilder.append("TARIFA_SIMULADA_FICTICIO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataInicio == null) {
			stringBuilder.append("DATA_INICIO_CALCULO_FICTICIO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataFim == null) {
			stringBuilder.append("DATA_FIM_CALCULO_FICTICIO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(totalConsumido)) {
			stringBuilder.append("TOTAL_CONSUMO_FICTICIO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#calcularTarifaMediaNoPeriodo(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * br.com.ggas.geral.EntidadeConteudo, java.util.Date, java.util.Date, boolean)
	 */
	@Override
	public Collection<TarifaVigenciaFaixa> calcularTarifaMediaNoPeriodo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Date dataInicioPeriodoMedicao, Date dataFimPeriodoMedicao, boolean isConsiderarDescontos) throws NegocioException {

		final BigDecimal maxValue = new BigDecimal(VALOR_MAXIMO_FAIXA_FINAL);

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Collection<TarifaVigenciaFaixa> retorno = new ArrayList<TarifaVigenciaFaixa>();

		Collection<PontoConsumo> colecaoPontoConsumo = new ArrayList<PontoConsumo>();
		colecaoPontoConsumo.add(pontoConsumo);
		Collection<PeriodoVigenciaDesconto> colecaoPeriodoVigenciaDesconto = mapearTarifasVigenciaPorDia(dataInicioPeriodoMedicao,
						dataFimPeriodoMedicao, itemFatura, colecaoPontoConsumo, null, null, isConsiderarDescontos, null, null);

		Integer totalDiasIntervalo = Util.quantidadeDiasIntervalo(dataInicioPeriodoMedicao, dataFimPeriodoMedicao);
		String pArredondamentoCalculo = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CALCULO);
		String pEscala = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);
		final Integer arredondamento;
		if (pArredondamentoCalculo == null) {
			throw new PrecisaoNaoInformadaException();
		} else {
			arredondamento = Integer.valueOf(pArredondamentoCalculo);
		}
		final Integer escala;
		if (pEscala == null) {
			throw new EscalaNaoInformadaException();
		} else {
			escala = Integer.valueOf(pEscala);
		}

		/*
		 * Monta uma lista ordenada com todos os
		 * Inícios e Fins das faixas
		 */
		MultiKeyMap mapaFaixas = new MultiKeyMap();
		List<BigDecimal> listaFaixas = new ArrayList<BigDecimal>();
		for (PeriodoVigenciaDesconto periodoVigenciaDesconto : colecaoPeriodoVigenciaDesconto) {
			for (TarifaVigenciaFaixa faixa : periodoVigenciaDesconto.tarifaVigencia.getTarifaVigenciaFaixas()) {
				if (!listaFaixas.contains(faixa.getMedidaInicio())) {
					listaFaixas.add(faixa.getMedidaInicio());
				}
				if (!listaFaixas.contains(faixa.getMedidaFim())) {
					listaFaixas.add(faixa.getMedidaFim());
				}
			}
		}
		Collections.sort(listaFaixas);

		/*
		 * Monta um Mapa com a intersecção das
		 * faixas
		 */
		ListIterator<BigDecimal> itFaixa = listaFaixas.listIterator();
		while (itFaixa.hasNext()) {
			BigDecimal limiteInferior = itFaixa.next();
			BigDecimal limiteSuperior = null;
			if (itFaixa.hasNext()) {
				limiteSuperior = itFaixa.next();
				itFaixa.previous();
			} else {
				limiteSuperior = maxValue;
			}
			if (limiteInferior.compareTo(limiteSuperior) != 0) {
				mapaFaixas.put(limiteInferior, limiteSuperior, new HashMap<TarifaVigenciaFaixa, Integer>());
			}
		}

		/*
		 * Popula o mapa com todas as
		 * TarifaVigenciaFaixa que serão
		 * utilizadas em cada faixa
		 */
		for (PeriodoVigenciaDesconto periodoVigenciaDesconto : colecaoPeriodoVigenciaDesconto) {
			Integer periodoIntervalo = periodoVigenciaDesconto.periodo.getQuantidadeDiasIntervalo();
			for (TarifaVigenciaFaixa faixa : periodoVigenciaDesconto.tarifaVigencia.getTarifaVigenciaFaixas()) {

				BigDecimal limiteInferior = null;
				BigDecimal limiteSuperior = null;
				Map<TarifaVigenciaFaixa, Integer> valor = null;

				for (Iterator<?> it = mapaFaixas.entrySet().iterator(); it.hasNext();) {
					Entry entry = (Entry<?, ?>) it.next();
					Object[] chave = ((MultiKey) entry.getKey()).getKeys();
					limiteInferior = (BigDecimal) chave[0];
					limiteSuperior = (BigDecimal) chave[1];
					valor = (HashMap<TarifaVigenciaFaixa, Integer>) entry.getValue();

					if ((limiteInferior.compareTo(faixa.getMedidaInicio()) >= 0) && (limiteSuperior.compareTo(faixa.getMedidaFim()) <= 0)) {
						valor.put(faixa, periodoIntervalo);
					}
				}
			}
		}

		/*
		 * calcula os valores médios
		 */
		ControladorTarifa controladorTarifa = (ControladorTarifa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);
		for (Iterator<?> it = mapaFaixas.entrySet().iterator(); it.hasNext();) {
			Entry entry = (Entry<?, ?>) it.next();
			Object[] chave = ((MultiKey) entry.getKey()).getKeys();

			TarifaVigenciaFaixa faixa = (TarifaVigenciaFaixa) controladorTarifa.criarTarifaVigenciaFaixa();

			BigDecimal limiteInferior = (BigDecimal) chave[0];
			faixa.setMedidaInicio(limiteInferior);

			BigDecimal limiteSuperior = (BigDecimal) chave[1];
			faixa.setMedidaFim(limiteSuperior);

			Map<TarifaVigenciaFaixa, Integer> colecaoFaixas = (HashMap<TarifaVigenciaFaixa, Integer>) mapaFaixas.get(chave[0], chave[1]);

			BigDecimal valorFixo = BigDecimal.ZERO;
			BigDecimal valorVariavel = BigDecimal.ZERO;
			for (Map.Entry<TarifaVigenciaFaixa, Integer> e : colecaoFaixas.entrySet()) {
				TarifaVigenciaFaixa tarifaVigenciaFaixa = e.getKey();
				BigDecimal proporcao = BigDecimal.valueOf(e.getValue()).divide(BigDecimal.valueOf(totalDiasIntervalo), escala,
								arredondamento);
				valorFixo = valorFixo.add(tarifaVigenciaFaixa.getValorFixo().multiply(proporcao));
				valorVariavel = valorVariavel.add(tarifaVigenciaFaixa.getValorVariavel().multiply(proporcao));
			}

			faixa.setValorFixo(valorFixo);
			faixa.setValorVariavel(valorVariavel);
			retorno.add(faixa);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorCalculoFornecimentoGas#obterTributoAliquota(java.lang.Long, java.util.Date)
	 */
	@Override
	public TributoAliquota obterTributoAliquota(Long identificacaoTributo, Date dataVigencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" tributoAliquota ");
		hql.append(" where ");
		hql.append(" tributoAliquota.tributo.chavePrimaria = :idTributo ");
		hql.append(" and ");
		hql.append(" tributoAliquota.dataVigencia = ( select max(tribAliquota.dataVigencia) from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName()).append(" tribAliquota ");
		hql.append(" where tribAliquota.dataVigencia <= :dataVigencia");
		hql.append(" and tribAliquota.tributo.chavePrimaria = :idTributo)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTributo", identificacaoTributo);
		query.setDate("dataVigencia", dataVigencia);

		return (TributoAliquota) query.uniqueResult();
	}

	private Collection<Tarifa> listarChavesTarifasPorPontoConsumo(PontoConsumo pontoConsumo, Contrato contrato,
			EntidadeConteudo itemFatura) {
		String selectQuery = " select tarifa.chavePrimaria as chavePrimaria ";

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();

		Long statusTarifaAutorizada = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO));

		Query query = criarConsultaTarifasPorPontoConsumo(selectQuery, pontoConsumo, itemFatura, statusTarifaAutorizada,
				null, null, contrato);

		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeTarifa()));

		return query.list();
	}

	private Long contarTarifasPendentesPorPontoConsumo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
			Date dataInicioPeriodo, Date dataFimPeriodo) throws NegocioException {

		String selectQuery = " select count(cpcit.tarifa.chavePrimaria) ";

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();

		Long statusTarifaPendente = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE));

		Query query = criarConsultaTarifasPorPontoConsumo(selectQuery, pontoConsumo, itemFatura, statusTarifaPendente,
				dataInicioPeriodo, dataFimPeriodo, null);

		return (Long) query.uniqueResult();
	}

	private Query criarConsultaTarifasPorPontoConsumo(String selectQuery, PontoConsumo pontoConsumo,
			EntidadeConteudo itemFatura, Long statusTarifa, Date dataInicioPeriodo, Date dataFimPeriodo,
			Contrato contrato) {

		ControladorTarifa controladorTarifa = (ControladorTarifa) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);

		StringBuilder hql = new StringBuilder();
		hql.append(selectQuery);
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" cpcit ");

		hql.append(" inner join cpcit.contratoPontoConsumo cpc");
		hql.append(" inner join  cpc.contrato ctr");
		hql.append(" inner join cpcit.tarifa tarifa ");
		hql.append(" where cpcit.contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		hql.append(" and cpc.chavePrimaria = cpcit.contratoPontoConsumo.chavePrimaria ");
		hql.append(" and cpc.contrato.chavePrimaria = ctr.chavePrimaria ");
		hql.append(" and tarifa.chavePrimaria in (:listaChavesTarifa) ");

		if (contrato != null) {
			hql.append(" and ctr.chavePrimaria = :chaveContrato ");
		}

		hql.append(" and ctr.situacao in ( ");
		hql.append(SituacaoContrato.ATIVO);
		hql.append(" , ");
		hql.append(SituacaoContrato.EM_NEGOCIACAO);
		hql.append(" ) ");

		if (itemFatura != null) {
			hql.append(" and cpcit.itemFatura.chavePrimaria = :itemFatura ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePontoConsumo", pontoConsumo.getChavePrimaria());

		Collection<Long> listaChavesTarifa = controladorTarifa.obterListaIdsTarifas(statusTarifa, dataInicioPeriodo,
				dataFimPeriodo);

		if (!listaChavesTarifa.isEmpty()) {
			query.setParameterList("listaChavesTarifa", listaChavesTarifa);
		} else {
			query.setParameter("listaChavesTarifa", null);
		}

		if (itemFatura != null) {
			query.setLong("itemFatura", itemFatura.getChavePrimaria());
		}

		if (contrato != null) {
			query.setLong("chaveContrato", contrato.getChavePrimaria());
		}

		return query;
	}

	/**
	 * Agrupar dados faixa para exibicao.
	 *
	 * @param dadosFornecimentoGasVO - {@link DadosFornecimentoGasVO}
	 * @return lista de DadosFaixaVO - {@link List}
	 */
	@Override
	public List<DadosFaixaVO> agruparDadosFaixaParaExibicao(DadosFornecimentoGasVO dadosFornecimentoGasVO) {

		List<DadosFaixaVO> listaRetorno = new ArrayList<DadosFaixaVO>();
		Map<BigDecimal, HashMap<BigDecimal, DadosFaixaVO>> mapaMedidaInicialMapaFaixas =
				new HashMap<BigDecimal, HashMap<BigDecimal, DadosFaixaVO>>();
		boolean isEstruturaTabelasTarifaDiferente = false;

		if (dadosFornecimentoGasVO != null && dadosFornecimentoGasVO.getDadosVigencia() != null
				&& !dadosFornecimentoGasVO.getDadosVigencia().isEmpty()) {
			for (DadosVigenciaVO dadosVigenciaVO : dadosFornecimentoGasVO.getDadosVigencia()) {
				for (DadosFaixaVO faixaNova : dadosVigenciaVO.getDadosFaixa()) {
					if (mapaMedidaInicialMapaFaixas.containsKey(faixaNova.getTarifaVigenciaFaixa().getMedidaInicio())) {
						Map<BigDecimal, DadosFaixaVO> mapaMedidaFinalFaixa =
								mapaMedidaInicialMapaFaixas.get(faixaNova.getTarifaVigenciaFaixa().getMedidaInicio());
						if (mapaMedidaFinalFaixa.containsKey(faixaNova.getTarifaVigenciaFaixa().getMedidaFim())) {
							DadosFaixaVO dadosFaixa =
									new DadosFaixaVO(mapaMedidaFinalFaixa.get(faixaNova.getTarifaVigenciaFaixa().getMedidaFim()));

							dadosFaixa.setConsumoApurado(dadosFaixa.getConsumoApurado().add(faixaNova.getConsumoApurado()));

							dadosFaixa.setValorDescontoFixo(dadosFaixa.getValorDescontoFixo().add(faixaNova.getValorDescontoFixo()));

							dadosFaixa.setValorDescontoVariavel(
									dadosFaixa.getValorDescontoVariavel().add(faixaNova.getValorDescontoVariavel()));

							dadosFaixa.setValorFixoSemImpostoSemDesconto(
									dadosFaixa.getValorFixoSemImpostoSemDesconto().add(faixaNova.getValorFixoSemImpostoSemDesconto()));

							dadosFaixa.setValorFixoComImpostoSemDesconto(
									dadosFaixa.getValorFixoComImpostoSemDesconto().add(faixaNova.getValorFixoComImpostoSemDesconto()));

							dadosFaixa.setValorFixoSemImpostoComDesconto(
									dadosFaixa.getValorFixoSemImpostoComDesconto().add(faixaNova.getValorFixoSemImpostoComDesconto()));

							dadosFaixa.setValorFixoComImpostoComDesconto(
									dadosFaixa.getValorFixoComImpostoComDesconto().add(faixaNova.getValorFixoComImpostoComDesconto()));

							dadosFaixa.setValorVariavelComImpostoSemDesconto(dadosFaixa.getValorVariavelComImpostoSemDesconto()
									.add(faixaNova.getValorVariavelComImpostoSemDesconto()));

							dadosFaixa.setValorVariavelSemImpostoSemDesconto(dadosFaixa.getValorVariavelSemImpostoSemDesconto()
									.add(faixaNova.getValorVariavelSemImpostoSemDesconto()));

							dadosFaixa.setValorVariavelComImpostoComDesconto(dadosFaixa.getValorVariavelComImpostoComDesconto()
									.add(faixaNova.getValorVariavelComImpostoComDesconto()));

							dadosFaixa.setValorVariavelSemImpostoComDesconto(dadosFaixa.getValorVariavelSemImpostoComDesconto()
									.add(faixaNova.getValorVariavelSemImpostoComDesconto()));

							Map<BigDecimal, DadosFaixaVO> mapaMedidaFimFaixa = new HashMap<BigDecimal, DadosFaixaVO>();
							mapaMedidaFimFaixa.put(dadosFaixa.getTarifaVigenciaFaixa().getMedidaFim(), dadosFaixa);
							mapaMedidaInicialMapaFaixas.put(dadosFaixa.getTarifaVigenciaFaixa().getMedidaInicio(),
									(HashMap<BigDecimal, DadosFaixaVO>) mapaMedidaFimFaixa);

						} else {
							// se contém a data de
							// início mas não
							// contém a data final
							// é porque a faixa é
							// diferente.
							isEstruturaTabelasTarifaDiferente = true;
							break;
						}
					} else {
						// se não contém a data de
						// início é porque ou
						// ainda não foi
						// cadastrado ou a faixa é
						// diferente
						if (isFaixaEntreIntervalos(faixaNova.getTarifaVigenciaFaixa(), mapaMedidaInicialMapaFaixas)) {
							// Está entre algum intervalo já cadastrado!"
							// Se a faixa for
							// diferente
							isEstruturaTabelasTarifaDiferente = true;
							break;
						} else {
							Map<BigDecimal, DadosFaixaVO> mapaMedidaFimFaixa = new HashMap<BigDecimal, DadosFaixaVO>();
							mapaMedidaFimFaixa.put(faixaNova.getTarifaVigenciaFaixa().getMedidaFim(), faixaNova);
							mapaMedidaInicialMapaFaixas.put(faixaNova.getTarifaVigenciaFaixa().getMedidaInicio(),
									(HashMap<BigDecimal, DadosFaixaVO>) mapaMedidaFimFaixa);
						}
					}
				}

				if (isEstruturaTabelasTarifaDiferente) {
					break;
				}
			}
		}

		// Monta Lista de Retorno
		for (Entry<BigDecimal, HashMap<BigDecimal, DadosFaixaVO>> entry : mapaMedidaInicialMapaFaixas.entrySet()) {

			Map<BigDecimal, DadosFaixaVO> mapaFaixas = entry.getValue();
			if (mapaFaixas.size() > 1) {
				isEstruturaTabelasTarifaDiferente = true;
				break;
			} else {
				for (Map.Entry<BigDecimal, DadosFaixaVO> e : mapaFaixas.entrySet()) {
					listaRetorno.add(e.getValue());
				}
			}
		}

		if (isEstruturaTabelasTarifaDiferente) {
			listaRetorno = null;
		} else {
			Collections.sort(listaRetorno);
		}

		return listaRetorno;
	}

	/**
	 * Verificar se existe faixa entre intervalos.
	 * @param tvf - {@link TarifaVigenciaFaixa}
	 * @param mapaMedidaInicialMapaFaixas - {@link Map}
	 * @return indicador se existe faixa entre intervalos - boolean
	 *
	 */
	private boolean isFaixaEntreIntervalos(TarifaVigenciaFaixa tvf,
			Map<BigDecimal, HashMap<BigDecimal, DadosFaixaVO>> mapaMedidaInicialMapaFaixas) {

		boolean retorno = false;

		if (mapaMedidaInicialMapaFaixas != null) {
			for (Map.Entry<BigDecimal, HashMap<BigDecimal, DadosFaixaVO>> entry : mapaMedidaInicialMapaFaixas
					.entrySet()) {
				BigDecimal medidaInicio = entry.getKey();
				Map<BigDecimal, DadosFaixaVO> mapaFinal = entry.getValue();
				for (Map.Entry<BigDecimal, DadosFaixaVO> e : mapaFinal.entrySet()) {
					if (!(tvf.getMedidaInicio().compareTo(e.getKey()) >= 0
							|| tvf.getMedidaFim().compareTo(medidaInicio) <= 0)) {
						retorno = true;
						break;
					}
				}
				if (retorno) {
					break;
				}
			}
		}

		return retorno;
	}

	/**
	 * Agrupar dados vigencia para exibicao.
	 * @param dadosFornecimentoGasReal - {@link DadosFornecimentoGasVO}
	 * @return coleção de DadosFaixaVO - {@link Collection}
	 */
	@Override
	public Collection<DadosFaixaVO> agruparDadosVigenciaParaExibicao(DadosFornecimentoGasVO dadosFornecimentoGasReal) {

		List<DadosFaixaVO> listaRetorno = new ArrayList<DadosFaixaVO>();

		BigDecimal consumoApuradoTotal = BigDecimal.ZERO;
		BigDecimal valorDescontoFixoTotal = BigDecimal.ZERO;
		BigDecimal valorDescontoVariavelTotal = BigDecimal.ZERO;
		BigDecimal valorFixoSemImpostoSemDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorFixoComImpostoSemDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorFixoSemImpostoComDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorFixoComImpostoComDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorVariavelComImpostoSemDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorVariavelSemImpostoSemDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorVariavelComImpostoComDescontoTotal = BigDecimal.ZERO;
		BigDecimal valorVariavelSemImpostoComDescontoTotal = BigDecimal.ZERO;
		TarifaVigenciaFaixa tarifaVigenciaFaixa = null;

		if (dadosFornecimentoGasReal != null && dadosFornecimentoGasReal.getDadosVigencia() != null
				&& !dadosFornecimentoGasReal.getDadosVigencia().isEmpty()) {

			for (DadosVigenciaVO dadosVigencia : dadosFornecimentoGasReal.getDadosVigencia()) {

				Collection<DadosFaixaVO> listaDadosVigencia = dadosVigencia.getDadosFaixa();

				for (DadosFaixaVO dadosFaixaVO : listaDadosVigencia) {
					consumoApuradoTotal = consumoApuradoTotal.add(dadosFaixaVO.getConsumoApurado());
					valorDescontoFixoTotal = valorDescontoFixoTotal.add(dadosFaixaVO.getValorDescontoFixo());
					valorDescontoVariavelTotal = valorDescontoVariavelTotal
							.add(dadosFaixaVO.getValorDescontoVariavel());
					valorFixoSemImpostoSemDescontoTotal = valorFixoSemImpostoSemDescontoTotal
							.add(dadosFaixaVO.getValorFixoSemImpostoSemDesconto());
					valorFixoComImpostoSemDescontoTotal = valorFixoComImpostoSemDescontoTotal
							.add(dadosFaixaVO.getValorFixoComImpostoSemDesconto());

					valorFixoSemImpostoComDescontoTotal = valorFixoSemImpostoComDescontoTotal
							.add(dadosFaixaVO.getValorFixoSemImpostoComDesconto());
					valorFixoComImpostoComDescontoTotal = valorFixoComImpostoComDescontoTotal
							.add(dadosFaixaVO.getValorFixoComImpostoComDesconto());

					valorVariavelComImpostoSemDescontoTotal = valorVariavelComImpostoSemDescontoTotal
							.add(dadosFaixaVO.getValorVariavelComImpostoSemDesconto());
					valorVariavelSemImpostoSemDescontoTotal = valorVariavelSemImpostoSemDescontoTotal
							.add(dadosFaixaVO.getValorVariavelSemImpostoSemDesconto());
					valorVariavelComImpostoComDescontoTotal = valorVariavelComImpostoComDescontoTotal
							.add(dadosFaixaVO.getValorVariavelComImpostoComDesconto());
					valorVariavelSemImpostoComDescontoTotal = valorVariavelSemImpostoComDescontoTotal
							.add(dadosFaixaVO.getValorVariavelSemImpostoComDesconto());
					if (tarifaVigenciaFaixa == null) {
						tarifaVigenciaFaixa = dadosFaixaVO.getTarifaVigenciaFaixa();
					}
				}

			}

			DadosFaixaVO dadosFaixaTotal = new DadosFaixaVO();
			dadosFaixaTotal.setConsumoApurado(consumoApuradoTotal);
			dadosFaixaTotal.setValorDescontoFixo(valorDescontoFixoTotal);
			dadosFaixaTotal.setValorDescontoVariavel(valorDescontoVariavelTotal);
			dadosFaixaTotal.setValorFixoSemImpostoSemDesconto(valorFixoSemImpostoSemDescontoTotal);
			dadosFaixaTotal.setValorFixoComImpostoSemDesconto(valorFixoComImpostoSemDescontoTotal);
			dadosFaixaTotal.setValorFixoSemImpostoComDesconto(valorFixoSemImpostoComDescontoTotal);
			dadosFaixaTotal.setValorFixoComImpostoComDesconto(valorFixoComImpostoComDescontoTotal);

			dadosFaixaTotal.setValorVariavelComImpostoSemDesconto(valorVariavelComImpostoSemDescontoTotal);
			dadosFaixaTotal.setValorVariavelSemImpostoSemDesconto(valorVariavelSemImpostoSemDescontoTotal);
			dadosFaixaTotal.setValorVariavelComImpostoComDesconto(valorVariavelComImpostoComDescontoTotal);
			dadosFaixaTotal.setValorVariavelSemImpostoComDesconto(valorVariavelSemImpostoComDescontoTotal);

			listaRetorno.add(dadosFaixaTotal);

		}

		return listaRetorno;
	}

	/**
	 * Preencher valores total liquido.
	 * @param dadosFaixa - {@link Collection}
	 *
	 */
	@Override
	public void preencherValoresTotalLiquido(Collection<DadosFaixaVO> dadosFaixa) {

		if (dadosFaixa != null && !dadosFaixa.isEmpty()) {
			for (DadosFaixaVO dadosFaixaVO : dadosFaixa) {

				BigDecimal valorTotalComImposto = BigDecimal.ZERO;
				if (dadosFaixaVO.getValorVariavelComImpostoComDesconto() != null
						&& dadosFaixaVO.getValorFixoComImpostoComDesconto() != null) {
					valorTotalComImposto = dadosFaixaVO.getValorVariavelComImpostoComDesconto()
							.add(dadosFaixaVO.getValorFixoComImpostoComDesconto());
				} else if (dadosFaixaVO.getValorVariavelComImpostoComDesconto() != null
						&& dadosFaixaVO.getValorFixoComImpostoComDesconto() == null) {
					valorTotalComImposto = dadosFaixaVO.getValorVariavelComImpostoComDesconto();
				} else if (dadosFaixaVO.getValorFixoComImpostoComDesconto() != null
						&& dadosFaixaVO.getValorVariavelComImpostoComDesconto() == null) {
					valorTotalComImposto = dadosFaixaVO.getValorFixoComImpostoComDesconto();
				}

				BigDecimal valorTotalSemImposto = BigDecimal.ZERO;
				if (dadosFaixaVO.getValorVariavelSemImpostoComDesconto() != null
						&& dadosFaixaVO.getValorFixoSemImpostoComDesconto() != null) {
					valorTotalSemImposto = dadosFaixaVO.getValorVariavelSemImpostoComDesconto()
							.add(dadosFaixaVO.getValorFixoSemImpostoComDesconto());
				} else if (dadosFaixaVO.getValorVariavelSemImpostoComDesconto() != null
						&& dadosFaixaVO.getValorFixoSemImpostoComDesconto() == null) {
					valorTotalSemImposto = dadosFaixaVO.getValorVariavelSemImpostoComDesconto();
				} else if (dadosFaixaVO.getValorFixoSemImpostoComDesconto() != null
						&& dadosFaixaVO.getValorVariavelSemImpostoComDesconto() == null) {
					valorTotalSemImposto = dadosFaixaVO.getValorFixoSemImpostoComDesconto();
				}

				dadosFaixaVO.setValorTotalComImposto(valorTotalComImposto);
				dadosFaixaVO.setValorTotalSemImposto(valorTotalSemImposto);
			}
		}
	}
	
	private DadosFaixaVO calcularValorConsumoPorCascataContinua(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
			BigDecimal consumoApurado, Map<TarifaVigenciaFaixa, TarifaFaixaDesconto> mapaDescontosFaixa,
			Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa, Collection<TributoAliquota> colecaoTributos,
			BigDecimal consumoAcumulado, Integer escala, Integer tipoArredondamento, BigDecimal variacaoCambial,
			BigDecimal percentualInicial, BigDecimal precoGas, String valorComplementarInformado, Date ultimaLeitura)
			throws NegocioException {

		deleteme.append("\n...... Tipo de Cálculo: **************** Cascata Contínua ****************");

		BigDecimal consumoAcumuladoRestante = consumoAcumulado;
		BigDecimal consumoRestante = null;
		if (consumoApurado != null) {
			consumoRestante = consumoApurado;
		} else {
			consumoRestante = BigDecimal.ZERO;
		}

		DadosFaixaVO dadosFaixaVO = null;
		BigDecimal valorTotalDescontos = BigDecimal.ZERO;
		BigDecimal valorTotalFaturado = BigDecimal.ZERO;

		// Valor final da faixa anterior.
		BigDecimal medidaFimAnterior = BigDecimal.ZERO;

		if (colecaoTarifaVigenciaFaixa != null) {
			deleteme.append("\n................................. Acumulado Atual: ").append(consumoAcumulado);
			for (TarifaVigenciaFaixa faixa : colecaoTarifaVigenciaFaixa) {
				TarifaFaixaDesconto desconto = null;

				BigDecimal intervaloFaixa = faixa.getMedidaFim().subtract(medidaFimAnterior);
				medidaFimAnterior = faixa.getMedidaFim();
				BigDecimal percentualUtilizado = BigDecimal.ONE;

				boolean isAcumuladoProcessado = false;
				if (consumoAcumuladoRestante != null && consumoAcumuladoRestante.compareTo(BigDecimal.ZERO) > 0) {
					if (intervaloFaixa.compareTo(consumoAcumuladoRestante) > 0) {

						percentualUtilizado = percentualInicial;

						intervaloFaixa = intervaloFaixa.subtract(consumoAcumuladoRestante);
						isAcumuladoProcessado = true;
						deleteme.append("\n................................. Faixa ").append(faixa.getChavePrimaria())
								.append(" Parcialmente Já Processado: ").append(consumoAcumuladoRestante);
						consumoAcumuladoRestante = BigDecimal.ZERO;
					} else {
						consumoAcumuladoRestante = consumoAcumuladoRestante.subtract(intervaloFaixa);
						deleteme.append("\n................................. Faixa ").append(faixa.getChavePrimaria())
								.append(" Já Processada! [").append(intervaloFaixa).append("]");
					}
				} else {
					isAcumuladoProcessado = true;
					if (intervaloFaixa.compareTo(consumoRestante) > 0) {
						percentualUtilizado = percentualInicial;
					}

				}

				if (isAcumuladoProcessado) {

					// Calcula o Consumo 
					// Apurado na Faixa
					BigDecimal consumoApuradoNaFaixa = null;
					if (intervaloFaixa.compareTo(consumoRestante) > 0) {
						consumoApuradoNaFaixa = consumoRestante;
					} else {
						consumoApuradoNaFaixa = intervaloFaixa;
					}
					deleteme.append("\n.................................  Faixa ").append(faixa.getChavePrimaria())
							.append(" Processada Agora: ").append(consumoApuradoNaFaixa);

					if (mapaDescontosFaixa != null && mapaDescontosFaixa.containsKey(faixa)) {
						desconto = mapaDescontosFaixa.get(faixa);
					}
					// Subtrai o consumo de acordo com a tabela da tarifa
					consumoRestante = consumoRestante.subtract(intervaloFaixa);

					// Calcula o o valor do consumo de volume de acordo com as tarifas
					dadosFaixaVO = preencherDadosFaixaVO(pontoConsumo, itemFatura, consumoApuradoNaFaixa, desconto, faixa, colecaoTributos,
									escala, tipoArredondamento, variacaoCambial, percentualUtilizado, precoGas, consumoRestante,
									valorComplementarInformado, ultimaLeitura,BigDecimal.ONE);

					valorTotalFaturado = valorTotalFaturado.add(dadosFaixaVO.getValorFixoComImpostoSemDesconto());
					valorTotalFaturado = valorTotalFaturado.add(dadosFaixaVO.getValorVariavelComImpostoSemDesconto());
					if (dadosFaixaVO.getValorDescontoFixo() != null) {
						valorTotalDescontos = valorTotalDescontos.add(dadosFaixaVO.getValorDescontoFixo());
					}
					if (dadosFaixaVO.getValorDescontoVariavel() != null) {
						valorTotalDescontos = valorTotalDescontos.add(dadosFaixaVO.getValorDescontoVariavel());
					}
					
					if (consumoRestante.compareTo(BigDecimal.ZERO) <= 0) {
						break;
					}
				}
			}
		}

		return dadosFaixaVO;
	}
	
	/**
	 * Calcular periodoTotal da tarifa
	 * @param itPvdTemp - {@link Iterator}
	 * @return BigDecimal periodoTotal
	 */
	public BigDecimal calcularPeriodoTotal(Iterator<PeriodoVigenciaDesconto> itPvdTemp) {
		BigDecimal periodoTotal = BigDecimal.ZERO;
		while (itPvdTemp.hasNext()) {
			PeriodoVigenciaDesconto pvdAux = itPvdTemp.next();
			BigDecimal periodoLeitura = BigDecimal.valueOf(DataUtil
					.diferencaDiasEntreDatas(pvdAux.periodo.dataInicio.toDate(), pvdAux.periodo.dataFim.toDate()));
			periodoTotal = periodoTotal.add(periodoLeitura).add(BigDecimal.ONE);
		}
		return periodoTotal;
	}
	
	@Override
	public Map<TarifaVigencia, BigDecimal> montarPeriodoDeLeitura(Date periodoMedicaoDataInicio,
			Date periodoMedicaoDataFim, EntidadeConteudo itemFatura, PontoConsumo pontoConsumo) throws NegocioException {
		Map<TarifaVigencia, BigDecimal> retorno = new HashMap<>();
		Collection<PontoConsumo> colecaoPontoConsumo = new HashSet<>();
		colecaoPontoConsumo.add(pontoConsumo);
		PeriodoVigenciaDesconto pvd = null;
		
		List<PeriodoVigenciaDesconto> listaPeriodoTarifaVigenciaDesconto = mapearTarifasVigenciaPorDia(periodoMedicaoDataInicio,
				periodoMedicaoDataFim, itemFatura, colecaoPontoConsumo, null, null, Boolean.TRUE,
				null, null);
		
		if (!listaPeriodoTarifaVigenciaDesconto.isEmpty()) {
			BigDecimal periodoTotal = calcularPeriodoTotal(listaPeriodoTarifaVigenciaDesconto.iterator());
			Iterator<PeriodoVigenciaDesconto> itPvd = listaPeriodoTarifaVigenciaDesconto.iterator();

			while (itPvd.hasNext()) {
				pvd = itPvd.next();

				BigDecimal peridoLeituraDias = BigDecimal.valueOf(
						DataUtil.diferencaDiasEntreDatas(pvd.periodo.dataInicio.toDate(), pvd.periodo.dataFim.toDate()))
						.add(BigDecimal.ONE);
				
				if(retorno.containsKey(pvd.tarifaVigencia)) {
					retorno.put(pvd.tarifaVigencia, retorno.get(pvd.tarifaVigencia)
							.add(peridoLeituraDias.divide(periodoTotal, 15, RoundingMode.HALF_UP)));
				} else {
					retorno.put(pvd.tarifaVigencia, peridoLeituraDias.divide(periodoTotal, 15, RoundingMode.HALF_UP));

				}
			}
		}
		
		return retorno;
	}
	
	
	private Boolean checarTarifaDiariaColetor(TarifaVigencia tarifaVigencia, PontoConsumo pontoConsumo) throws NegocioException {String calculaMediaDiariaColetor = (String) controladorParametroSistema
            .obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CALCULAR_COLETOR_TARIFA_DIARIA);

    ControladorConstanteSistema controladorConstanteSistema =
            (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
                            ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

    
    Long chaveBaseApuracaoDiaria =
            Long.valueOf(controladorConstanteSistema
                            .obterValorConstanteSistemaPorCodigo(Constantes.C_TARIFA_BASE_APURACAO_DIARIA));
    
    if("1".equals(calculaMediaDiariaColetor) 
            && tarifaVigencia.getBaseApuracao().getChavePrimaria() == chaveBaseApuracaoDiaria.longValue()
            && pontoConsumo != null) {
        GrupoFaturamento grupo = pontoConsumo.getRota().getGrupoFaturamento(); 
        if("COLETOR DE DADOS".equals(grupo.getTipoLeitura().getDescricao())) {
            return Boolean.TRUE;
        }
    }
    
    return Boolean.FALSE;
}
	
	
}
