
package br.com.ggas.faturamento.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.FaturamentoGrupoRotaImpressao;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.Rota;

/**
 * Faturamento Grupo Rota Impressao.
 *
 */
public class FaturamentoGrupoRotaImpressaoImpl extends EntidadeNegocioImpl implements FaturamentoGrupoRotaImpressao {

	private static final long serialVersionUID = 237552950396804621L;

	private GrupoFaturamento grupoFaturamento;

	private Rota rota;

	private Integer anoMesReferencia;

	private Integer seqImpressao;

	private Integer ciclo;

	private Date dtUltimaImpressao;

	private Date dtImpressao;

	private Usuario usuario;

	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	@Override
	public GrupoFaturamento getGrupoFaturamento() {

		return grupoFaturamento;
	}

	@Override
	public Integer getCiclo() {

		return ciclo;
	}

	@Override
	public void setCiclo(Integer ciclo) {

		this.ciclo = ciclo;
	}

	@Override
	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	@Override
	public Rota getRota() {

		return rota;
	}

	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	@Override
	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	@Override
	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	@Override
	public Integer getSeqImpressao() {

		return seqImpressao;
	}

	@Override
	public void setSeqImpressao(Integer seqImpressao) {

		this.seqImpressao = seqImpressao;
	}

	@Override
	public Date getDtUltimaImpressao() {
		Date data = null;
		if(this.dtUltimaImpressao != null) {
			data = (Date) dtUltimaImpressao.clone();
		}
		return data;
	}

	@Override
	public void setDtUltimaImpressao(Date dtUltimaImpressao) {
		if (dtUltimaImpressao != null) {
			this.dtUltimaImpressao = (Date) dtUltimaImpressao.clone();
		} else {
			this.dtUltimaImpressao = null;
		}
	}

	@Override
	public Date getDtImpressao() {
		Date data = null;
		if (this.dtImpressao != null) {
			data = (Date) dtImpressao.clone();
		}
		return data;
	}

	@Override
	public void setDtImpressao(Date dtImpressao) {
		if (dtImpressao != null) {
			this.dtImpressao = (Date) dtImpressao.clone();
		} else {
			this.dtImpressao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
