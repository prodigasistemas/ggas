/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela implementação dos métodos relacionados ao Item de Documento de Cobrança 
 *
 */
public class DocumentoCobrancaItemImpl extends EntidadeNegocioImpl implements DocumentoCobrancaItem {

	private static final int NUMERO_DECIMAIS = 2;

	private static final long serialVersionUID = -711856015345774018L;

	private DocumentoCobranca documentoCobranca;

	private FaturaGeral faturaGeral;

	private CreditoDebitoARealizar creditoDebitoARealizar;

	private CobrancaDebitoSituacao cobrancaDebitoSituacao;

	private BigDecimal valor;

	private BigDecimal valorAcrecimos;

	private BigDecimal valorDescontos;

	private Integer numeroPrestacao;

	private Integer numeroTotalPrestacao;

	private PontoConsumo pontoConsumo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #getDocumentoCobranca()
	 */
	@Override
	public DocumentoCobranca getDocumentoCobranca() {

		return documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setDocumentoCobranca(br
	 * .com.ggas.arrecadacao.DocumentoCobranca)
	 */
	@Override
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca) {

		this.documentoCobranca = documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.DocumentoCobrancaItem
	 * #getFaturaGeral()
	 */
	@Override
	public FaturaGeral getFaturaGeral() {

		return faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.DocumentoCobrancaItem
	 * #setFaturaGeral(br.com.ggas.faturamento.
	 * FaturaGeral)
	 */
	@Override
	public void setFaturaGeral(FaturaGeral faturaGeral) {

		this.faturaGeral = faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #getCreditoDebitoARealizar()
	 */
	@Override
	public CreditoDebitoARealizar getCreditoDebitoARealizar() {

		return creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setCreditoDebitoARealizar
	 * (br.com.ggas.faturamento
	 * .CreditoDebitoARealizar)
	 */
	@Override
	public void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) {

		this.creditoDebitoARealizar = creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #getCobrancaDebitoSituacao()
	 */
	@Override
	public CobrancaDebitoSituacao getCobrancaDebitoSituacao() {

		return cobrancaDebitoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setCobrancaDebitoSituacao
	 * (br.com.ggas.arrecadacao
	 * .CobrancaDebitoSituacao)
	 */
	@Override
	public void setCobrancaDebitoSituacao(CobrancaDebitoSituacao cobrancaDebitoSituacao) {

		this.cobrancaDebitoSituacao = cobrancaDebitoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem#getValor()
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setValor(java.math.BigDecimal)
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	@Override
	public String getValorFormatado() {

		return Util.converterCampoValorDecimalParaString("", this.valor, Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem#getValorAcrecimos()
	 */
	@Override
	public BigDecimal getValorAcrecimos() {

		return valorAcrecimos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setValorAcrecimos(java.math.BigDecimal)
	 */
	@Override
	public void setValorAcrecimos(BigDecimal valorAcrecimos) {

		this.valorAcrecimos = valorAcrecimos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem#getValorDescontos()
	 */
	@Override
	public BigDecimal getValorDescontos() {

		return valorDescontos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setValorDescontos(java.math.BigDecimal)
	 */
	@Override
	public void setValorDescontos(BigDecimal valorDescontos) {

		this.valorDescontos = valorDescontos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem#getNumeroPrestacao()
	 */
	@Override
	public Integer getNumeroPrestacao() {

		return numeroPrestacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setNumeroPrestacao(java.lang.Integer)
	 */
	@Override
	public void setNumeroPrestacao(Integer numeroPrestacao) {

		this.numeroPrestacao = numeroPrestacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #getNumeroTotalPrestacao()
	 */
	@Override
	public Integer getNumeroTotalPrestacao() {

		return numeroTotalPrestacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * DocumentoCobrancaItem
	 * #setNumeroTotalPrestacao(java.lang.Integer)
	 */
	@Override
	public void setNumeroTotalPrestacao(Integer numeroTotalPrestacao) {

		this.numeroTotalPrestacao = numeroTotalPrestacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.DocumentoCobrancaItem
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.DocumentoCobrancaItem
	 * #
	 * setPontoConsumo(br.com.ggas.cadastro.imovel
	 * .PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}
}
