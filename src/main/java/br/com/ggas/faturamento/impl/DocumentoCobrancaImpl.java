/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela implementação dos métodos relacionados a Cobrança de Documento. 
 *
 */
public class DocumentoCobrancaImpl extends EntidadeNegocioImpl implements DocumentoCobranca {

	private static final long serialVersionUID = -3633844196745929141L;

	private Cliente cliente;

	private TipoDocumento tipoDocumento;

	private CobrancaDebitoSituacao cobrancaDebitoSituacao;

	private Date dataEmissao;

	private Date dataVencimento;

	private Integer sequencial;

	private Integer sequencialImpressao;

	private Long nossoNumero;

	private BigDecimal valorTotal;

	private BigDecimal valorCredito;

	private BigDecimal valorDebito;

	private Collection<DocumentoCobrancaItem> itens = new HashSet<DocumentoCobrancaItem>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.DocumentoCobranca #getDataVencimento()
	 */
	@Override
	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.DocumentoCobranca
	 * #setDataVencimento(java.util.Date)
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {
		if (dataVencimento != null) {
			this.dataVencimento = (Date) dataVencimento.clone();
		} else {
			this.dataVencimento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.DocumentoCobranca
	 * #getNossoNumero()
	 */
	@Override
	public Long getNossoNumero() {

		return nossoNumero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.DocumentoCobranca
	 * #setNossoNumero(java.lang.Integer)
	 */
	@Override
	public void setNossoNumero(Long nossoNumero) {

		this.nossoNumero = nossoNumero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DocumentoCobranca
	 * #getItens()
	 */
	@Override
	public Collection<DocumentoCobrancaItem> getItens() {

		return itens;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DocumentoCobranca
	 * #setItens(java.util.Collection)
	 */
	@Override
	public void setItens(Collection<DocumentoCobrancaItem> itens) {

		this.itens = itens;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #
	 * setCliente(br.com.ggas.cadastro.cliente.Cliente
	 * )
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #getTipoDocumento()
	 */
	@Override
	public TipoDocumento getTipoDocumento() {

		return tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #setTipoDocumento(br.com.ggas.cobranca.
	 * TipoDocumento)
	 */
	@Override
	public void setTipoDocumento(TipoDocumento tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #getCobrancaDebitoSituacao()
	 */
	@Override
	public CobrancaDebitoSituacao getCobrancaDebitoSituacao() {

		return cobrancaDebitoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #
	 * setCobrancaDebitoSituacao(br.com.ggas.cobranca
	 * .impl.CobrancaDebitoSituacaoImpl)
	 */
	@Override
	public void setCobrancaDebitoSituacao(CobrancaDebitoSituacao cobrancaDebitoSituacao) {

		this.cobrancaDebitoSituacao = cobrancaDebitoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cobranca.impl.DocumentoCobranca #getDataEmissao()
	 */
	@Override
	public Date getDataEmissao() {
		Date data = null;
		if (this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.DocumentoCobranca
	 * #setDataEmissao(java.util.Date)
	 */
	@Override
	public void setDataEmissao(Date dataEmissao) {
		if (dataEmissao != null) {
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #getSequencial()
	 */
	@Override
	public Integer getSequencial() {

		return sequencial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #setSequencial(int)
	 */
	@Override
	public void setSequencial(Integer sequencial) {

		this.sequencial = sequencial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #getSequencialImpressao()
	 */
	@Override
	public Integer getSequencialImpressao() {

		return sequencialImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #setSequencialImpressao(int)
	 */
	@Override
	public void setSequencialImpressao(Integer sequencialImpressao) {

		this.sequencialImpressao = sequencialImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DocumentoCobranca
	 * #setValorTotal(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public BigDecimal getValorCredito() {

		return valorCredito;
	}

	@Override
	public void setValorCredito(BigDecimal valorCredito) {

		this.valorCredito = valorCredito;
	}

	@Override
	public BigDecimal getValorDebito() {

		return valorDebito;
	}

	@Override
	public void setValorDebito(BigDecimal valorDebito) {

		this.valorDebito = valorDebito;
	}

}
