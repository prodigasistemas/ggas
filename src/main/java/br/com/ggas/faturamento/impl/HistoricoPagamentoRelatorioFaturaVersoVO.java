
package br.com.ggas.faturamento.impl;
/**
 * Classe rsponsável pela representação da entidade HistoricoPagamentoRelatorioFaturaVerso 
 *
 */
public class HistoricoPagamentoRelatorioFaturaVersoVO {

	private String mesAno;

	private String vencimento;

	private String valor;

	private String pagamento;

	public String getMesAno() {

		return mesAno;
	}

	public void setMesAno(String mesAno) {

		this.mesAno = mesAno;
	}

	public String getVencimento() {

		return vencimento;
	}

	public void setVencimento(String vencimento) {

		this.vencimento = vencimento;
	}

	public String getValor() {

		return valor;
	}

	public void setValor(String valor) {

		this.valor = valor;
	}

	public String getPagamento() {

		return pagamento;
	}

	public void setPagamento(String pagamento) {

		this.pagamento = pagamento;
	}

}
