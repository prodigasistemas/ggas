/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
/**
 * Classe responsável pela representação da entidade DadosFaixa. 
 *
 */
public class DadosFaixaVO implements Serializable, Comparable<DadosFaixaVO> {

	private static final int QUATRO_CASAS_DECIMAIS = 4;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 448931329411468518L;

	private TarifaVigenciaFaixa tarifaVigenciaFaixa;

	private BigDecimal consumoApurado;

	private BigDecimal valorDescontoFixo;

	private BigDecimal valorDescontoVariavel;

	private BigDecimal valorFixoSemImpostoSemDesconto;

	private BigDecimal valorFixoComImpostoSemDesconto;

	private BigDecimal valorFixoSemImpostoComDesconto;

	private BigDecimal valorFixoComImpostoComDesconto;

	private BigDecimal valorVariavelComImpostoSemDesconto;

	private BigDecimal valorVariavelSemImpostoSemDesconto;

	private BigDecimal valorVariavelComImpostoComDesconto;

	private BigDecimal valorVariavelSemImpostoComDesconto;

	private TarifaFaixaDesconto tarifaFaixaDescontoFixo;

	private TarifaFaixaDesconto tarifaFaixaDescontoVariavel;

	private BigDecimal valorTotalComImposto;

	private BigDecimal valorTotalSemImposto;

	private BigDecimal valorTotalComImpostoSemSubstituicao;

	private int tipoArredondamento;

	private BigDecimal consumoFaturado;

	/**
	 * Instantiates a new dados faixa vo.
	 * 
	 * @param dadosFaixaNovo
	 *            the dados faixa novo
	 */
	public DadosFaixaVO(DadosFaixaVO dadosFaixaNovo) {

		tarifaVigenciaFaixa = dadosFaixaNovo.tarifaVigenciaFaixa;
		consumoApurado = dadosFaixaNovo.consumoApurado;
		valorDescontoFixo = dadosFaixaNovo.valorDescontoFixo;
		valorDescontoVariavel = dadosFaixaNovo.valorDescontoVariavel;
		valorFixoSemImpostoSemDesconto = dadosFaixaNovo.valorFixoSemImpostoSemDesconto;
		valorFixoComImpostoSemDesconto = dadosFaixaNovo.valorFixoComImpostoSemDesconto;
		valorFixoSemImpostoComDesconto = dadosFaixaNovo.valorFixoSemImpostoComDesconto;
		valorFixoComImpostoComDesconto = dadosFaixaNovo.valorFixoComImpostoComDesconto;
		valorVariavelComImpostoSemDesconto = dadosFaixaNovo.valorVariavelComImpostoSemDesconto;
		valorVariavelSemImpostoSemDesconto = dadosFaixaNovo.valorVariavelSemImpostoSemDesconto;
		valorVariavelComImpostoComDesconto = dadosFaixaNovo.valorVariavelComImpostoComDesconto;
		valorVariavelSemImpostoComDesconto = dadosFaixaNovo.valorVariavelSemImpostoComDesconto;
		tarifaFaixaDescontoFixo = dadosFaixaNovo.tarifaFaixaDescontoFixo;
		tarifaFaixaDescontoVariavel = dadosFaixaNovo.tarifaFaixaDescontoVariavel;
		tipoArredondamento = dadosFaixaNovo.tipoArredondamento;
		consumoFaturado = dadosFaixaNovo.consumoFaturado;
	}

	/**
	 * Construtor de Dados Faixa
	 */
	public DadosFaixaVO() {

		super();
	}

	/**
	 * @return the
	 *         valorTotalComImpostoSemSubstituicao
	 */
	public BigDecimal getValorTotalComImpostoSemSubstituicao() {

		return valorTotalComImpostoSemSubstituicao;
	}

	/**
	 * @param valorTotalComImpostoSemSubstituicao
	 *            the
	 *            valorTotalComImpostoSemSubstituicao
	 *            to set
	 */
	public void setValorTotalComImpostoSemSubstituicao(BigDecimal valorTotalComImpostoSemSubstituicao) {

		this.valorTotalComImpostoSemSubstituicao = valorTotalComImpostoSemSubstituicao;
	}

	public int getTipoArredondamento() {

		return tipoArredondamento;
	}

	/**
	 * 
	 * @param tipoArredondamento
	 */
	public void setTipoArredondamento(int tipoArredondamento) {

		this.tipoArredondamento = tipoArredondamento;
	}

	/**
	 * @return the tarifaVigenciaFaixa
	 */
	public TarifaVigenciaFaixa getTarifaVigenciaFaixa() {

		return tarifaVigenciaFaixa;
	}

	/**
	 * @param tarifaVigenciaFaixa
	 *            the tarifaVigenciaFaixa to set
	 */
	public void setTarifaVigenciaFaixa(TarifaVigenciaFaixa tarifaVigenciaFaixa) {

		this.tarifaVigenciaFaixa = tarifaVigenciaFaixa;
	}

	/**
	 * @return the consumoApurado
	 */
	public BigDecimal getConsumoApurado() {

		return consumoApurado;
	}

	/**
	 * @param consumoApurado
	 *            the consumoApurado to set
	 */
	public void setConsumoApurado(BigDecimal consumoApurado) {

		this.consumoApurado = consumoApurado;
	}

	/**
	 * @return the valorDescontoFixo
	 */
	public BigDecimal getValorDescontoFixo() {

		return valorDescontoFixo;
	}

	/**
	 * @param valorDescontoFixo
	 *            the valorDescontoFixo to set
	 */
	public void setValorDescontoFixo(BigDecimal valorDescontoFixo) {

		this.valorDescontoFixo = valorDescontoFixo;
	}

	/**
	 * @return the valorDescontoVariavel
	 */
	public BigDecimal getValorDescontoVariavel() {

		return valorDescontoVariavel;
	}

	/**
	 * @param valorDescontoVariavel
	 *            the valorDescontoVariavel to set
	 */
	public void setValorDescontoVariavel(BigDecimal valorDescontoVariavel) {

		this.valorDescontoVariavel = valorDescontoVariavel;
	}

	/**
	 * @return the valorFixoSemImpostoSemDesconto
	 */
	public BigDecimal getValorFixoSemImpostoSemDesconto() {

		return valorFixoSemImpostoSemDesconto;
	}

	/**
	 * @param valorFixoSemImpostoSemDesconto
	 *            the
	 *            valorFixoSemImpostoSemDesconto
	 *            to set
	 */
	public void setValorFixoSemImpostoSemDesconto(BigDecimal valorFixoSemImpostoSemDesconto) {

		this.valorFixoSemImpostoSemDesconto = valorFixoSemImpostoSemDesconto;
	}

	/**
	 * @return the valorFixoComImpostoSemDesconto
	 */
	public BigDecimal getValorFixoComImpostoSemDesconto() {

		return valorFixoComImpostoSemDesconto;
	}

	/**
	 * @param valorFixoComImpostoSemDesconto
	 *            the
	 *            valorFixoComImpostoSemDesconto
	 *            to set
	 */
	public void setValorFixoComImpostoSemDesconto(BigDecimal valorFixoComImpostoSemDesconto) {

		this.valorFixoComImpostoSemDesconto = valorFixoComImpostoSemDesconto;
	}

	/**
	 * @return the valorFixoSemImpostoComDesconto
	 */
	public BigDecimal getValorFixoSemImpostoComDesconto() {

		return valorFixoSemImpostoComDesconto;
	}

	/**
	 * @param valorFixoSemImpostoComDesconto
	 *            the
	 *            valorFixoSemImpostoComDesconto
	 *            to set
	 */
	public void setValorFixoSemImpostoComDesconto(BigDecimal valorFixoSemImpostoComDesconto) {

		this.valorFixoSemImpostoComDesconto = valorFixoSemImpostoComDesconto;
	}

	/**
	 * @return the valorFixoComImpostoComDesconto
	 */
	public BigDecimal getValorFixoComImpostoComDesconto() {

		return valorFixoComImpostoComDesconto;
	}

	/**
	 * @param valorFixoComImpostoComDesconto
	 *            the
	 *            valorFixoComImpostoComDesconto
	 *            to set
	 */
	public void setValorFixoComImpostoComDesconto(BigDecimal valorFixoComImpostoComDesconto) {

		this.valorFixoComImpostoComDesconto = valorFixoComImpostoComDesconto;
	}

	/**
	 * @return the
	 *         valorVariavelComImpostoSemDesconto
	 */
	public BigDecimal getValorVariavelComImpostoSemDesconto() {

		return valorVariavelComImpostoSemDesconto;
	}

	/**
	 * @param valorVariavelComImpostoSemDesconto
	 *            the
	 *            valorVariavelComImpostoSemDesconto
	 *            to set
	 */
	public void setValorVariavelComImpostoSemDesconto(BigDecimal valorVariavelComImpostoSemDesconto) {

		this.valorVariavelComImpostoSemDesconto = valorVariavelComImpostoSemDesconto;
	}

	/**
	 * @return the
	 *         valorVariavelSemImpostoSemDesconto
	 */
	public BigDecimal getValorVariavelSemImpostoSemDesconto() {

		return valorVariavelSemImpostoSemDesconto;
	}

	/**
	 * @param valorVariavelSemImpostoSemDesconto
	 *            the
	 *            valorVariavelSemImpostoSemDesconto
	 *            to set
	 */
	public void setValorVariavelSemImpostoSemDesconto(BigDecimal valorVariavelSemImpostoSemDesconto) {

		this.valorVariavelSemImpostoSemDesconto = valorVariavelSemImpostoSemDesconto;
	}

	/**
	 * @return the
	 *         valorVariavelComImpostoComDesconto
	 */
	public BigDecimal getValorVariavelComImpostoComDesconto() {

		return valorVariavelComImpostoComDesconto;
	}

	/**
	 * @param valorVariavelComImpostoComDesconto
	 *            the
	 *            valorVariavelComImpostoComDesconto
	 *            to set
	 */
	public void setValorVariavelComImpostoComDesconto(BigDecimal valorVariavelComImpostoComDesconto) {

		this.valorVariavelComImpostoComDesconto = valorVariavelComImpostoComDesconto;
	}

	/**
	 * @return the
	 *         valorVariavelSemImpostoComDesconto
	 */
	public BigDecimal getValorVariavelSemImpostoComDesconto() {

		return valorVariavelSemImpostoComDesconto;
	}

	/**
	 * @param valorVariavelSemImpostoComDesconto
	 *            the
	 *            valorVariavelSemImpostoComDesconto
	 *            to set
	 */
	public void setValorVariavelSemImpostoComDesconto(BigDecimal valorVariavelSemImpostoComDesconto) {

		this.valorVariavelSemImpostoComDesconto = valorVariavelSemImpostoComDesconto;
	}

	/**
	 * @return the tarifaFaixaDescontoFixo
	 */
	public TarifaFaixaDesconto getTarifaFaixaDescontoFixo() {

		return tarifaFaixaDescontoFixo;
	}

	/**
	 * @param tarifaFaixaDescontoFixo
	 *            the tarifaFaixaDescontoFixo to
	 *            set
	 */
	public void setTarifaFaixaDescontoFixo(TarifaFaixaDesconto tarifaFaixaDescontoFixo) {

		this.tarifaFaixaDescontoFixo = tarifaFaixaDescontoFixo;
	}

	/**
	 * @return the tarifaFaixaDescontoVariavel
	 */
	public TarifaFaixaDesconto getTarifaFaixaDescontoVariavel() {

		return tarifaFaixaDescontoVariavel;
	}

	public BigDecimal getValorDescontoVariavelTotal() {

		BigDecimal retorno = BigDecimal.ZERO;
		if(valorDescontoVariavel != null && consumoApurado != null) {
			retorno = valorDescontoVariavel.multiply(consumoApurado);
		}
		return retorno;
	}

	public BigDecimal getValorDescontoFixoTotal() {

		BigDecimal retorno = BigDecimal.ZERO;
		if(valorDescontoFixo != null) {
			retorno = valorDescontoFixo;
		}
		return retorno;
	}

	/**
	 * @param tarifaFaixaDescontoVariavel
	 *            the tarifaFaixaDescontoVariavel
	 *            to set
	 */
	public void setTarifaFaixaDescontoVariavel(TarifaFaixaDesconto tarifaFaixaDescontoVariavel) {

		this.tarifaFaixaDescontoVariavel = tarifaFaixaDescontoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(DadosFaixaVO dadosfaixa) {

		int retorno = 0;
		if(tarifaVigenciaFaixa != null && dadosfaixa != null && 
						dadosfaixa.tarifaVigenciaFaixa != null && tarifaVigenciaFaixa.getMedidaInicio() != null && 
						dadosfaixa.tarifaVigenciaFaixa.getMedidaInicio() != null) {
			retorno = tarifaVigenciaFaixa.getMedidaInicio().compareTo(dadosfaixa.tarifaVigenciaFaixa.getMedidaInicio());
		}
		return retorno;
	}

	/**
	 * @return the totalSemImposto
	 */
	public BigDecimal getValorTotalSemImposto() {

		return valorTotalSemImposto;
	}

	/**
	 * @return the totalComImposto
	 */
	public BigDecimal getValorTotalComImposto() {

		return valorTotalComImposto;
	}

	/**
	 * 
	 * @param valorTotalComImposto
	 */
	public void setValorTotalComImposto(BigDecimal valorTotalComImposto) {

		this.valorTotalComImposto = valorTotalComImposto;
	}
	
	/**
	 * 
	 * @param valorTotalSemImposto
	 */
	public void setValorTotalSemImposto(BigDecimal valorTotalSemImposto) {

		this.valorTotalSemImposto = valorTotalSemImposto;
	}

	public BigDecimal getValorTotalSemImpostoFormatado() {

		return valorTotalSemImposto.setScale(DUAS_CASAS_DECIMAIS, tipoArredondamento);
	}

	public BigDecimal getValorTotalComImpostoFormatado() {

		return valorTotalComImposto.setScale(DUAS_CASAS_DECIMAIS, tipoArredondamento);
	}

	public BigDecimal getValorFixoSemImpostoSemDescontoFormatado() {

		return valorFixoSemImpostoSemDesconto.setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorVariavelSemImpostoSemDescontoFormatado() {

		return valorVariavelSemImpostoSemDesconto.setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorDescontoFixoTotalFormatado() {

		return getValorDescontoFixoTotal().setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorFixoSemImpostoComDescontoFormatado() {

		return valorFixoSemImpostoComDesconto.setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorFixoComImpostoComDescontoFormatado() {

		return valorFixoComImpostoComDesconto.setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorDescontoVariavelTotalFormatado() {

		return getValorDescontoVariavelTotal().setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorVariavelSemImpostoComDescontoFormatado() {

		return valorVariavelSemImpostoComDesconto.setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	public BigDecimal getValorVariavelComImpostoComDescontoFormatado() {

		return valorVariavelComImpostoComDesconto.setScale(getQtdCasasDecimais(), tipoArredondamento);
	}

	private int getQtdCasasDecimais() {

		BigDecimal quantidadeCasasDecimais = BigDecimal.ZERO;
		if(tarifaVigenciaFaixa != null) {
			quantidadeCasasDecimais = tarifaVigenciaFaixa.getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal();
		} else {
			quantidadeCasasDecimais = BigDecimal.valueOf(QUATRO_CASAS_DECIMAIS);
		}

		return quantidadeCasasDecimais.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		boolean retorno = false;
		if(obj != null && obj instanceof DadosFaixaVO) {
			DadosFaixaVO dadosFaixaParametro = (DadosFaixaVO) obj;
			if(tarifaVigenciaFaixa != null && dadosFaixaParametro.tarifaVigenciaFaixa != null
							&& tarifaVigenciaFaixa.getChavePrimaria() == dadosFaixaParametro.tarifaVigenciaFaixa.getChavePrimaria()) {
				retorno = true;
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if(tarifaVigenciaFaixa != null && tarifaVigenciaFaixa.getChavePrimaria() > 0) {
			return new HashCodeBuilder(3, 5).append(tarifaVigenciaFaixa.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}
	}

	public void setConsumoFaturado(BigDecimal consumoFaturado) {

		this.consumoFaturado = consumoFaturado;
	}

	public BigDecimal getConsumoFaturado() {

		return consumoFaturado;
	}

}

