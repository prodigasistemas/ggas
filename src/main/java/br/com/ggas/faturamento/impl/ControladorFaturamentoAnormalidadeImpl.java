/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;

import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
/**
 * Classe responsável pela implementação dos métodos do Controlador de Faturamento para Anormalidade
 *
 */
public class ControladorFaturamentoAnormalidadeImpl extends ControladorNegocioImpl implements ControladorFaturamentoAnormalidade {

	private Map<String, FaturamentoAnormalidade> cacheFaturamentoAnormalidade = new HashMap<String, FaturamentoAnormalidade>();
	
	private static final String ARQUIVO_PROPRIEDADES_FATURAMENTO_ANORMALIDADE = "faturamentoAnormalidade.properties";

	private static Map<String, String> mapaCodigoFaturamentoAnormalidade;

	static {
		inicializarPropriedades();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FaturamentoAnormalidade.BEAN_ID_FATURAMENTO_ANORMALIDADE);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(FaturamentoAnormalidade.BEAN_ID_FATURAMENTO_ANORMALIDADE);
	}

	/**
	 * Método responsável por inicializar o mapa
	 * de código do FaturamentoAnormalidade.
	 */
	private static void inicializarPropriedades() {

		try {
			Properties propriedades = new Properties();
			InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(ARQUIVO_PROPRIEDADES_FATURAMENTO_ANORMALIDADE);
			propriedades.load(stream);

			mapaCodigoFaturamentoAnormalidade = new HashMap<String, String>();
			Iterator<Entry<Object, Object>> iterator = propriedades.entrySet().iterator();
			while(iterator.hasNext()) {
				Entry<Object, Object> entry = iterator.next();
				mapaCodigoFaturamentoAnormalidade.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
			}
			stream.close();
		} catch(Exception ex) {
			Logger.getLogger("ERRO").debug(ex.getMessage(), ex);
		}
	}

	/**
	 * Método responsável por obter o código de
	 * uma anormalidade de acordo com sua chave.
	 * 
	 * @param chaveAnormalidade
	 *            chave da anormalidade
	 * @return the codigo anormalidade
	 * @returno o código da anormalidade
	 */
	private long getCodigoAnormalidade(String chaveAnormalidade) {

		long retorno = -1;

		if(!StringUtils.isEmpty(chaveAnormalidade) && mapaCodigoFaturamentoAnormalidade.containsKey(chaveAnormalidade)) {
			retorno = Long.parseLong(mapaCodigoFaturamentoAnormalidade.get(chaveAnormalidade).trim());
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.
	 * ControladorFaturamentoAnormalidade
	 * #obterAnormalidadePorClasse
	 * (java.lang.String)
	 */
	@Override
	public FaturamentoAnormalidade obterAnormalidadePorClasse(String chaveAnormalidade) throws NegocioException {

		FaturamentoAnormalidade retorno = null;
		
		if (chaveAnormalidade != null) {
			if(getCacheFaturamentoAnormalidade().containsKey(chaveAnormalidade)) {
				
				retorno = getCacheFaturamentoAnormalidade().get(chaveAnormalidade);
			}else{
				
				StringBuilder hql = new StringBuilder();
				hql.append(" from ");
				hql.append(getClasseEntidade().getSimpleName()).append(" anormalidade ");
				hql.append(" where ");
				hql.append(" anormalidade.chavePrimaria = :idAnormalidade ");

				Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

				query.setLong("idAnormalidade", getCodigoAnormalidade(chaveAnormalidade));
				
				retorno = (FaturamentoAnormalidade) query.uniqueResult();
							
				getCacheFaturamentoAnormalidade().put(chaveAnormalidade, retorno);
			}
		}				

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * ControladorFatura#consultarFaturas
	 * (java.lang.Long, java.lang.Long,
	 * java.util.Map)
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Collection<FaturamentoAnormalidade> listaAnormalidadeFaturamento() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	
	public Map<String, FaturamentoAnormalidade> getCacheFaturamentoAnormalidade() {

		return cacheFaturamentoAnormalidade;
	}

	public void setCacheFaturamentoAnormalidade(Map<String, FaturamentoAnormalidade> cacheFaturamentoAnormalidade) {

		this.cacheFaturamentoAnormalidade = cacheFaturamentoAnormalidade;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<FaturamentoAnormalidade> listaAnormalidadeFaturamento(String descricao, Boolean habilitado,
			Boolean indicadorImpedeFaturamento) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where 1 = 1 ");
		final boolean filtrarPorDescricao = descricao != null && descricao.trim().length() > 0;
		if (filtrarPorDescricao) {
			hql.append(" and lower(descricao) like :descricao ");
		}
		if (habilitado != null) {
			hql.append(" and habilitado = :habilitado ");
		}

		if (indicadorImpedeFaturamento != null) {
			hql.append(" and indicadorImpedeFaturamento = :indicadorImpedeFaturamento ");
		}
		hql.append(" order by descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtrarPorDescricao) {
			query.setParameter("descricao", "%"+descricao.trim().toLowerCase()+"%");
		}
		if (habilitado != null) {
			query.setParameter("habilitado", habilitado);
		}
		if (indicadorImpedeFaturamento != null) {
			query.setParameter("indicadorImpedeFaturamento", indicadorImpedeFaturamento);
		}

		return query.list();
	}
}
