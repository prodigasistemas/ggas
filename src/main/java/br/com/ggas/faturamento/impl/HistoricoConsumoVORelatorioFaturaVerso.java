
package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;

/**
 * Historico Consumo VO Relatorio Fatura Verso.
 *
 */
public class HistoricoConsumoVORelatorioFaturaVerso {

	private String data;

	private BigDecimal valorFaturado;

	private BigDecimal valorMedido;

	public String getData() {

		return data;
	}

	public void setData(String data) {

		this.data = data;
	}

	public BigDecimal getValorFaturado() {

		return valorFaturado;
	}

	public void setValorFaturado(BigDecimal valorFaturado) {

		this.valorFaturado = valorFaturado;
	}

	public BigDecimal getValorMedido() {

		return valorMedido;
	}

	public void setValorMedido(BigDecimal valorMedido) {

		this.valorMedido = valorMedido;
	}

}
