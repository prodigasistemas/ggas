/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaHistoricoRevisao;
import br.com.ggas.faturamento.fatura.FaturaMotivoRevisao;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 *
 */
class FaturaHistoricoRevisaoImpl extends EntidadeNegocioImpl implements FaturaHistoricoRevisao {

	private static final long serialVersionUID = 4802107878471150272L;

	private Fatura fatura;

	private FaturaMotivoRevisao motivoRevisao;

	private Date dataEntradaRevisao;

	private Date dataSaidaRevisao;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao#getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao
	 * #setFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao#getMotivoRevisao()
	 */
	@Override
	public FaturaMotivoRevisao getMotivoRevisao() {

		return motivoRevisao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao
	 * #setmotivoRevisao(br.com
	 * .ggas.faturamento.fatura
	 * .FaturaMotivoRevisao)
	 */
	@Override
	public void setMotivoRevisao(FaturaMotivoRevisao motivoRevisao) {

		this.motivoRevisao = motivoRevisao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao
	 * #getDataEntradaRevisao()
	 */
	@Override
	public Date getDataEntradaRevisao() {

		return dataEntradaRevisao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao
	 * #setDataEntradaRevisao(java.util.Date)
	 */
	@Override
	public void setDataEntradaRevisao(Date dataEntradaRevisao) {

		this.dataEntradaRevisao = dataEntradaRevisao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao
	 * #getDataSaidaRevisao()
	 */
	@Override
	public Date getDataSaidaRevisao() {

		return dataSaidaRevisao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao
	 * #setDataSaidaRevisao(java.util.Date)
	 */
	@Override
	public void setDataSaidaRevisao(Date dataSaidaRevisao) {

		this.dataSaidaRevisao = dataSaidaRevisao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * FaturaHistoricoRevisao#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}
}
