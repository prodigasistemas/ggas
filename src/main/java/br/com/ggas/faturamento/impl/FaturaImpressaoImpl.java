/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.Rota;

/**
 * 
 *
 */
class FaturaImpressaoImpl extends EntidadeNegocioImpl implements FaturaImpressao {

	private static final long serialVersionUID = 4802107878471150272L;

	private GrupoFaturamento grupoFaturamento;

	private Fatura fatura;

	private FaturamentoAnormalidade faturamentoAnormalidade;

	private Usuario usuario;

	private EntidadeConteudo formaCobranca;

	private Date dataGeracao;

	private Date dataImpressao;

	private Integer anoMesReferencia;

	private BigDecimal valor;

	private Integer numeroLote;

	private Integer sequenciaImpressao;

	private Boolean indicadorImpressao;

	private Rota rota;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getGrupoFaturamento
	 * ()
	 */
	@Override
	public GrupoFaturamento getGrupoFaturamento() {

		return grupoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setGrupoFaturamento
	 * (
	 * br.com.ggas.cadastro.imovel.GrupoFaturamento
	 * )
	 */
	@Override
	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.k#getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setFatura(br.
	 * com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/**
	 * @return the faturamentoAnormalidade
	 */
	@Override
	public FaturamentoAnormalidade getFaturamentoAnormalidade() {

		return faturamentoAnormalidade;
	}

	/**
	 * @param faturamentoAnormalidade
	 *            the faturamentoAnormalidade to
	 *            set
	 */
	@Override
	public void setFaturamentoAnormalidade(FaturamentoAnormalidade faturamentoAnormalidade) {

		this.faturamentoAnormalidade = faturamentoAnormalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.k#getUsuario()
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setUsuario(br
	 * .com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getDataGeracao()
	 */
	@Override
	public Date getDataGeracao() {

		return dataGeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setDataGeracao
	 * (java.util.Date)
	 */
	@Override
	public void setDataGeracao(Date dataGeracao) {

		this.dataGeracao = dataGeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getDataImpressao
	 * ()
	 */
	@Override
	public Date getDataImpressao() {

		return dataImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setDataImpressao
	 * (java.util.Date)
	 */
	@Override
	public void setDataImpressao(Date dataImpressao) {

		this.dataImpressao = dataImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getAnoMesReferencia
	 * ()
	 */
	@Override
	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setAnoMesReferencia
	 * (java.lang.Integer)
	 */
	@Override
	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.k#getValor()
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setValor(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getNumeroLote()
	 */
	@Override
	public Integer getNumeroLote() {

		return numeroLote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setNumeroLote
	 * (java.lang.Integer)
	 */
	@Override
	public void setNumeroLote(Integer numeroLote) {

		this.numeroLote = numeroLote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getSequenciaImpressao
	 * ()
	 */
	@Override
	public Integer getSequenciaImpressao() {

		return sequenciaImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setSequenciaImpressao
	 * (java.lang.Integer)
	 */
	@Override
	public void setSequenciaImpressao(Integer sequenciaImpressao) {

		this.sequenciaImpressao = sequenciaImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#getIndicadorImpressao
	 * ()
	 */
	@Override
	public Boolean getIndicadorImpressao() {

		return indicadorImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.k#setIndicadorImpressao
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorImpressao(Boolean indicadorImpressao) {

		this.indicadorImpressao = indicadorImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/**
	 * @return the formaCobranca
	 */
	@Override
	public EntidadeConteudo getFormaCobranca() {

		return formaCobranca;
	}

	/**
	 * @param formaCobranca
	 *            the formaCobranca to set
	 */
	@Override
	public void setFormaCobranca(EntidadeConteudo formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	/**
	 * @return the rota
	 */
	@Override
	public Rota getRota() {

		return rota;
	}

	/**
	 * @param rota
	 *            the rota to set
	 */
	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}
}
