
package br.com.ggas.faturamento.impl;

/**
 * Classe responsável pela representação da entidade AnoMesReferencia 
 *
 */
public class AnoMesReferenciaVO {

	private String anoMesFormatado;

	private Integer anoMes;

	private Integer ciclo;

	public String getAnoMesFormatado() {

		return anoMesFormatado;
	}

	public void setAnoMesFormatado(String anoMesFormatado) {

		this.anoMesFormatado = anoMesFormatado;
	}

	public Integer getAnoMes() {

		return anoMes;
	}

	public void setAnoMes(Integer anoMes) {

		this.anoMes = anoMes;
	}

	public Integer getCiclo() {

		return ciclo;
	}

	public void setCiclo(Integer ciclo) {

		this.ciclo = ciclo;
	}
}
