/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.RefaturamentoHistorico;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

class RefaturamentoHistoricoImpl extends EntidadeNegocioImpl implements RefaturamentoHistorico {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PontoConsumo pontoConsumo;

	private Cliente cliente;

	private Fatura fatura;

	private Contrato contrato;

	private Date dataRefaturamento;

	private boolean indicadorProcessoConcluido;

	private Date dataConclusaoRefaturamento;

	private byte[] dados;

	private DadosResumoFatura dadosResumoFatura;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #
	 * setPontoConsumo(br.com.ggas.cadastro.imovel
	 * .PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #
	 * setCliente(br.com.ggas.cadastro.cliente.Cliente
	 * )
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #setFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #setContrato(br.com.ggas.contrato.contrato.
	 * Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getDataRefaturamento()
	 */
	@Override
	public Date getDataRefaturamento() {

		return dataRefaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #setDataRefaturamento(java.util.Date)
	 */
	@Override
	public void setDataRefaturamento(Date dataRefaturamento) {

		this.dataRefaturamento = dataRefaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #isIndicadorProcessoConcluido()
	 */
	@Override
	public boolean isIndicadorProcessoConcluido() {

		return indicadorProcessoConcluido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #setIndicadorProcessoConcluido(boolean)
	 */
	@Override
	public void setIndicadorProcessoConcluido(boolean indicadorProcessoConcluido) {

		this.indicadorProcessoConcluido = indicadorProcessoConcluido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getDataConclusaoFaturamento()
	 */
	@Override
	public Date getDataConclusaoRefaturamento() {

		return dataConclusaoRefaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #
	 * setDataConclusaoFaturamento(java.util.Date)
	 */
	@Override
	public void setDataConclusaoRefaturamento(Date dataConclusaoRefaturamento) {

		this.dataConclusaoRefaturamento = dataConclusaoRefaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getDados()
	 */
	@Override
	public byte[] getDados() {

		return dados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #setDados(byte[])
	 */
	@Override
	public void setDados(byte[] dados) {

		this.dados = dados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.RefaturamentoHistorico
	 * #getDadosResumoFatura()
	 */
	@Override
	public DadosResumoFatura getDadosResumoFatura() throws NegocioException {

		if (dados != null) {
			try {
				dadosResumoFatura = (DadosResumoFatura) Util.transformarBytesParaObjeto(dados);
			} catch (Exception e) {
				throw new NegocioException(e.getMessage(), e);
			}
		}

		return dadosResumoFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(dataRefaturamento == null) {
			stringBuilder.append(REFATURAMENTO_HISTORICO_DATA_REFATURAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dados == null) {
			stringBuilder.append(REFATURAMENTO_HISTORICO_DADOS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(pontoConsumo == null) {
			stringBuilder.append(REFATURAMENTO_HISTORICO_PONTO_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;

	}

}
