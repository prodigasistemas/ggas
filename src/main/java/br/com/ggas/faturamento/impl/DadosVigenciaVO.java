/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;

import br.com.ggas.faturamento.tarifa.TarifaVigencia;

/**
 * Classe responsável pela representação da entidade DadosVigencia 
 *
 */
public class DadosVigenciaVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7533409013708594440L;

	private BigDecimal consumoApurado;

	private BigDecimal consumoFaturado;

	private BigDecimal baseCalculoImpostos;

	private BigDecimal valorTotal;

	private BigDecimal valorTotalSemSubstituicao;

	private BigDecimal valorTotalSemTributo;

	private Collection<DadosTributoVO> colecaoTributos;

	private BigDecimal valorDescontos;

	private List<DadosFaixaVO> dadosFaixa;

	private TarifaVigencia tarifaVigencia;

	private DateTime dataPeriodoInicial;

	private DateTime dataPeriodoFinal;
	
	private BigDecimal valorTotalComDescontoComImposto;

	/**
	 * @return the valorTotalSemSubstituicao
	 */
	public BigDecimal getValorTotalSemSubstituicao() {

		return valorTotalSemSubstituicao;
	}

	/**
	 * @param valorTotalSemSubstituicao
	 *            the valorTotalSemSubstituicao to
	 *            set
	 */
	public void setValorTotalSemSubstituicao(BigDecimal valorTotalSemSubstituicao) {

		this.valorTotalSemSubstituicao = valorTotalSemSubstituicao;
	}

	/**
	 * @return the tarifaVigencia
	 */
	public TarifaVigencia getTarifaVigencia() {

		return tarifaVigencia;
	}

	/**
	 * @param tarifaVigencia
	 *            the tarifaVigencia to set
	 */
	public void setTarifaVigencia(TarifaVigencia tarifaVigencia) {

		this.tarifaVigencia = tarifaVigencia;
	}

	/**
	 * @return the dataPeriodoInicial
	 */
	public DateTime getDataPeriodoInicial() {

		return dataPeriodoInicial;
	}

	/**
	 * @param dataPeriodoInicial
	 *            the dataPeriodoInicial to set
	 */
	public void setDataPeriodoInicial(DateTime dataPeriodoInicial) {

		this.dataPeriodoInicial = dataPeriodoInicial;
	}

	/**
	 * @return the dataPeriodoFinal
	 */
	public DateTime getDataPeriodoFinal() {

		return dataPeriodoFinal;
	}

	/**
	 * @param dataPeriodoFinal
	 *            the dataPeriodoFinal to set
	 */
	public void setDataPeriodoFinal(DateTime dataPeriodoFinal) {

		this.dataPeriodoFinal = dataPeriodoFinal;
	}

	/**
	 * @return the consumoApurado
	 */
	public BigDecimal getConsumoApurado() {

		return consumoApurado;
	}

	/**
	 * @param consumoApurado
	 *            the consumoApurado to set
	 */
	public void setConsumoApurado(BigDecimal consumoApurado) {

		this.consumoApurado = consumoApurado;
	}

	/**
	 * @return the baseCalculoImpostos
	 */
	public BigDecimal getBaseCalculoImpostos() {

		return baseCalculoImpostos;
	}

	/**
	 * @param valorFornecimento
	 *            the baseCalculoImpostos to set
	 */
	public void setBaseCalculoImpostos(BigDecimal baseCalculoImpostos) {

		this.baseCalculoImpostos = baseCalculoImpostos;
	}

	/**
	 * @return the coleção de tributos
	 */
	public Collection<DadosTributoVO> getColecaoTributos() {

		return colecaoTributos;
	}

	/**
	 * @param tributos
	 *            the coleção de tributos to set
	 */
	public void setColecaoTributos(Collection<DadosTributoVO> tributos) {

		this.colecaoTributos = tributos;
	}

	/**
	 * @return the valorDescontos
	 */
	public BigDecimal getValorDescontos() {

		return valorDescontos;
	}

	/**
	 * @param valorDescontos
	 *            the valorDescontos to set
	 */
	public void setValorDescontos(BigDecimal valorDescontos) {

		this.valorDescontos = valorDescontos;
	}

	/**
	 * @return the dadosFaixa
	 */
	public List<DadosFaixaVO> getDadosFaixa() {

		return dadosFaixa;
	}

	/**
	 * @param dadosFaixa
	 *            the dadosFaixa to set
	 */
	public void setDadosFaixa(List<DadosFaixaVO> dadosFaixa) {

		this.dadosFaixa = dadosFaixa;
	}

	/**
	 * @return the valorTotal
	 */
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/**
	 * @return the valorTotalSemTributo
	 */
	public BigDecimal getValorTotalSemTributo() {

		return valorTotalSemTributo;
	}

	/**
	 * @param valorTotalSemTributo
	 *            the valorTotalSemTributo to set
	 */
	public void setValorTotalSemTributo(BigDecimal valorTotalSemTributo) {

		this.valorTotalSemTributo = valorTotalSemTributo;
	}

	/**
	 * método responsável por calcular e retornar
	 * o valor com imposto e sem descontos
	 * 
	 * @return valor com imposto e sem desconto
	 */
	public BigDecimal getValorTotalSemDescontos() {

		BigDecimal retorno = BigDecimal.ZERO;
		if(dadosFaixa != null) {
			for (DadosFaixaVO faixa : dadosFaixa) {
				retorno = retorno.add(faixa.getValorFixoComImpostoSemDesconto());
				// TODO verificar se precisa
				// multiplicar o valor variavel
				// pelo consumo
				retorno = retorno.add(faixa.getValorVariavelComImpostoSemDesconto());
			}
		}
		return retorno;
	}

	public void setConsumoFaturado(BigDecimal consumoFaturado) {

		this.consumoFaturado = consumoFaturado;
	}

	public BigDecimal getConsumoFaturado() {

		return consumoFaturado;
	}

	/**
	 * @return the valorTotalComDescontoComImposto
	 */
	public BigDecimal getValorTotalComDescontoComImposto() {

		return valorTotalComDescontoComImposto;
	}

	/**
	 * @param valorTotalComDescontoComImposto the valorTotalComDescontoComImposto to set
	 */
	public void setValorTotalComDescontoComImposto(BigDecimal valorTotalComDescontoComImposto) {

		this.valorTotalComDescontoComImposto = valorTotalComDescontoComImposto;
	}

}
