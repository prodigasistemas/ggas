/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Collection;

import org.hibernate.Query;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.ServiceLocator;

/**
 *
 *
 */
class ControladorDocumentoCobrancaImpl extends ControladorNegocioImpl implements ControladorDocumentoCobranca {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoCobranca.BEAN_ID_DOCUMENTO_COBRANCA);
	}

	public Class<?> getClasseEntidadeDocumentoCobrancaItem() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoCobrancaItem.BEAN_ID_DOCUMENTO_COBRANCA_ITEM);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorDocumentoCobranca#buscarDocumentoCobranca(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DocumentoCobranca> buscarDocumentoCobranca(Long chavePrimaria) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" documentoCobranca ");
		hql.append(" where ");
		hql.append(" documentoCobranca.chavePrimaria = :chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimaria);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorDocumentoCobranca#buscarDocumentoCobrancaNossoNumero(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DocumentoCobranca> buscarDocumentoCobrancaNossoNumero(Long nossoNumero) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" documentoCobranca ");
		hql.append(" where ");
		hql.append(" documentoCobranca.nossoNumero = :nossoNumero ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("nossoNumero", nossoNumero);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorDocumentoCobranca#consultarDocumentoCobrancaPelaFatura(java.lang.Long,
	 * br.com.ggas.arrecadacao.TipoDocumento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DocumentoCobranca> consultarDocumentoCobrancaPelaFatura(Long chaveFatura, TipoDocumento documentoTipo)
					throws NegocioException {

		/*
		 * select dc.* from FATURA f
		 * left join FATURA_GERAL fg
		 * on fg.fatu_cd_atual = f.fatu_cd
		 * left join DOCUMENTO_COBRANCA dc
		 * on dc.doco_cd in (select dci.doco_cd
		 * from DOCUMENTO_COBRANCA_ITEM dci where
		 * dci.fage_cd = fg.fage_cd)
		 * and dc.doti_cd = 1
		 * where f.fatu_cd = 40281
		 */
		TipoDocumento tipoDocumento = documentoTipo;
		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		if(tipoDocumento == null) {
			String parametroTpDoc = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
			tipoDocumento = controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc));
		}

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" documentoCobranca ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" documentoCobranca ");
		hql.append(" inner join fetch documentoCobranca.cliente cliente ");
		hql.append(" inner join fetch documentoCobranca.tipoDocumento tipoDocumento ");
		hql.append(" inner join fetch documentoCobranca.cobrancaDebitoSituacao cobrancaDebitoSituacao ");
		hql.append(" inner join fetch documentoCobranca.itens documentoCobrancaItem ");
		hql.append(" left join fetch documentoCobrancaItem.faturaGeral faturaGeral ");
		hql.append(" inner join fetch faturaGeral.faturaAtual faturaAtual ");
		hql.append(" inner join fetch faturaAtual.listaFaturaItem faturaItem ");
		hql.append(" inner join fetch faturaItem.rubrica rubrica ");
		hql.append(" inner join fetch rubrica.financiamentoTipo financiamentoTipo ");
		hql.append(" where ");
		hql.append(" faturaAtual.chavePrimaria = :chaveFatura");
		hql.append(" and documentoCobranca.tipoDocumento.chavePrimaria = :tipoFatura");
		hql.append(" order by documentoCobranca.dataEmissao desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveFatura", chaveFatura);
		query.setParameter("tipoFatura", tipoDocumento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.
	 * ControladorDocumentoCobranca
	 * #obterUltimoDocumentoCobrancaFatura
	 * (java.lang.Long)
	 */
	@Override
	public DocumentoCobranca obterUltimoDocumentoCobrancaFatura(Long chaveFatura, Boolean isConsiderarTipoDocumento) throws NegocioException {

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroTpDoc = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		TipoDocumento tipoFatura = controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc));

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" documentoCobrancaItem.documentoCobranca ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual.chavePrimaria = :chaveFatura");
		
		if(isConsiderarTipoDocumento) {
			hql.append(" and documentoCobrancaItem.documentoCobranca.tipoDocumento.chavePrimaria = :tipoFatura");
		}
		hql.append(" and documentoCobrancaItem.documentoCobranca.dataVencimento = (");
		hql.append(" select ");
		hql.append(" max(documentoCobrancaItem.documentoCobranca.dataVencimento) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual.chavePrimaria = :chaveFatura");
		if(isConsiderarTipoDocumento) {
			hql.append(" and documentoCobrancaItem.documentoCobranca.tipoDocumento.chavePrimaria = :tipoFatura");
		}
		hql.append(") ");
		hql.append(" order by documentoCobrancaItem.documentoCobranca.dataEmissao desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveFatura", chaveFatura);
		
		if(isConsiderarTipoDocumento) {
			query.setParameter("tipoFatura", tipoFatura.getChavePrimaria());
		}
		query.setMaxResults(1);

		return (DocumentoCobranca) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.
	 * ControladorDocumentoCobranca
	 * #consultarFaturasPorDocumentoCobranca
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fatura> consultarFaturasPorDocumentoCobranca(Long chaveFatura, Long chaveDocumentoCobranca) {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		/*
		 * select f.* from DOCUMENTO_COBRANCA_ITEM
		 * dci
		 * inner join FATURA_GERAL fg
		 * on fg.fage_cd = dci.fage_cd
		 * inner join FATURA f
		 * on f.fatu_cd = fg.fatu_cd_atual
		 * and f.fatu_cd <> 40281 -- recebe esse
		 * cod por parametro
		 * where dci.doco_cd = 40281 --recebe esse
		 * cod por parametro
		 */

		hql.append(" select ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where ");
		hql.append("documentoCobrancaItem.faturaGeral.faturaAtual.chavePrimaria <> :chaveFatura");
		hql.append(" and documentoCobrancaItem.documentoCobranca.chavePrimaria = :chaveDocumentoCobranca");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveFatura", chaveFatura);
		query.setParameter("chaveDocumentoCobranca", chaveDocumentoCobranca);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorDocumentoCobranca#consultarDocumentoCobrancaItemPelaFatura(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DocumentoCobrancaItem> consultarDocumentoCobrancaItemPelaFatura(Long chaveFatura) throws 
		NegocioException {

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroTpDoc = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		TipoDocumento tipoFatura = controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc));

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" documentoCobrancaItem ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" inner join fetch documentoCobrancaItem.faturaGeral  faturaGeral ");
		hql.append(" inner join fetch faturaGeral.faturaAtual ");
		hql.append(" where ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual.chavePrimaria = :chaveFatura");
		hql.append(" and documentoCobrancaItem.documentoCobranca.tipoDocumento.chavePrimaria = :tipoFatura");
		hql.append(" order by documentoCobrancaItem.documentoCobranca.dataEmissao desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveFatura", chaveFatura);
		query.setParameter("tipoFatura", tipoFatura.getChavePrimaria());

		return query.list();
	}
	
	@Override
	public BigDecimal somarValorDocumentoCobrancaItemPelaFatura(Long chaveFatura)
			throws NegocioException {

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroTpDoc = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		TipoDocumento tipoFatura = controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc));

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(documentoCobrancaItem.valor) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" inner join documentoCobrancaItem.faturaGeral  faturaGeral ");
		hql.append(" inner join faturaGeral.faturaAtual faturaAtual ");
		hql.append(" where ");
		hql.append(" faturaAtual.chavePrimaria = :chaveFatura");
		hql.append(" and documentoCobrancaItem.documentoCobranca.tipoDocumento.chavePrimaria = :tipoFatura");
		hql.append(" order by documentoCobrancaItem.documentoCobranca.dataEmissao desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveFatura", chaveFatura);
		query.setParameter("tipoFatura", tipoFatura.getChavePrimaria());

		return (BigDecimal) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.
	 * ControladorDocumentoCobranca
	 * #consultarFaturaPorDocumentoCobrancaItem
	 * (java.lang.Long)
	 */
	@Override
	public Fatura consultarFaturaPorDocumentoCobrancaItem(Long chaveDocumentoCobrancaItem) {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where ");
		hql.append("documentoCobrancaItem.chavePrimaria = :chaveDocumentoCobrancaItem");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveDocumentoCobrancaItem", chaveDocumentoCobrancaItem);

		return (Fatura) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.ControladorDocumentoCobranca#obterUltimoDocumentoCobranca(java.lang.Long)
	 */
	@Override
	public DocumentoCobranca obterUltimoDocumentoCobranca(Long chaveFatura) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" documentoCobrancaItem.documentoCobranca ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual.chavePrimaria = :chaveFatura");
		hql.append(" and documentoCobrancaItem.documentoCobranca.dataVencimento = (");
		hql.append(" select ");
		hql.append(" max(documentoCobrancaItem.documentoCobranca.dataVencimento) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where ");
		hql.append(" documentoCobrancaItem.faturaGeral.faturaAtual.chavePrimaria = :chaveFatura)");
		hql.append(" order by documentoCobrancaItem.documentoCobranca.dataEmissao desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveFatura", chaveFatura);
		query.setMaxResults(1);

		return (DocumentoCobranca) query.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DocumentoCobranca> consultarDadosDocumentoCobrancaPelaFatura(Long chaveFatura, TipoDocumento documentoTipo)
					throws NegocioException {

		TipoDocumento tipoDocumento = documentoTipo;
		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		if(tipoDocumento == null) {
			String parametroTpDoc = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
			tipoDocumento = controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc));
		}

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" documentoCobranca.chavePrimaria as chavePrimaria, ");
		hql.append(" documentoCobranca.dataEmissao as dataEmissao ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" documentoCobranca ");
		hql.append(" inner join documentoCobranca.cliente cliente ");
		hql.append(" inner join documentoCobranca.tipoDocumento tipoDocumento ");
		hql.append(" inner join documentoCobranca.cobrancaDebitoSituacao cobrancaDebitoSituacao ");
		hql.append(" inner join documentoCobranca.itens documentoCobrancaItem ");
		hql.append(" left join documentoCobrancaItem.faturaGeral faturaGeral ");
		hql.append(" inner join faturaGeral.faturaAtual faturaAtual ");
		hql.append(" inner join faturaAtual.listaFaturaItem faturaItem ");
		hql.append(" inner join faturaItem.rubrica rubrica ");
		hql.append(" inner join rubrica.financiamentoTipo financiamentoTipo ");
		hql.append(" where ");
		hql.append(" faturaAtual.chavePrimaria = :chaveFatura");
		hql.append(" and documentoCobranca.tipoDocumento.chavePrimaria = :tipoFatura");
		hql.append(" order by documentoCobranca.dataEmissao desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidade(), super.getSessionFactory().getAllClassMetadata()));
		query.setParameter("chaveFatura", chaveFatura);
		query.setParameter("tipoFatura", tipoDocumento.getChavePrimaria());

		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.
	 * ControladorDocumentoCobranca
	 * #consultarFaturasPorDocumentoCobranca
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fatura> consultarDadosFaturasPorDocumentoCobranca(Long chaveFatura, Long chaveDocumentoCobranca) {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" faturaAtual.valorTotal as  valorTotal, ");
		hql.append(" faturaAtual.anoMesReferencia as anoMesReferencia, ");
		hql.append(" faturaAtual.numeroCiclo as numeroCiclo ");
		hql.append(" from ");
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" inner join documentoCobrancaItem.faturaGeral faturaGeral ");
		hql.append(" inner join faturaGeral.faturaAtual faturaAtual ");
		hql.append(" inner join documentoCobrancaItem.documentoCobranca documentoCobranca ");
		hql.append(" where ");
		hql.append(" faturaAtual.chavePrimaria <> :chaveFatura ");
		hql.append(" and documentoCobranca.chavePrimaria = :chaveDocumentoCobranca ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(
				new GGASTransformer(FaturaImpl.class, super.getSessionFactory().getAllClassMetadata()));
		query.setParameter("chaveFatura", chaveFatura);
		query.setParameter("chaveDocumentoCobranca", chaveDocumentoCobranca);

		return query.list();
	}

}
