/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.transportadora.dominio.Transportadora;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.*;
import br.com.ggas.faturamento.fatura.FaturaMotivoRevisao;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

/**
 * Classe responsável pela implementação dos métodos relacionados a Fatura 
 *
 */
public class FaturaImpl extends EntidadeNegocioImpl implements Fatura {

	private static final int NUMERO_DECIMAIS = 2;

	private static final int INICIO_CAMPO_MES = 4;

	private static final int LIMITE_CAMPO_ANO = 4;

	private static final long serialVersionUID = 4802107878471150272L;

	private static final Logger LOG = Logger.getLogger(FaturaImpl.class);

	private FaturaGeral faturaGeral;

	private Fatura faturaAgrupada;

	private TipoDocumento tipoDocumento;

	private PontoConsumo pontoConsumo;

	private Cliente cliente;

	private Contrato contrato;

	private Contrato contratoAtual;

	private Rota rota;

	private CreditoDebitoSituacao creditoDebitoSituacao;

	private Segmento segmento;

	private HistoricoConsumo historicoConsumo;

	private HistoricoMedicao historicoMedicao;

	private Integer anoMesReferencia;

	private Integer numeroCiclo;

	private BigDecimal valorTotal;

	private EntidadeConteudo situacaoPagamento;

	private Date dataEmissao;

	private Date dataVencimento;

	private Date dataCancelamento;

	private String motivoAlteracao;

	private FaturaMotivoRevisao motivoRevisao;

	private Devolucao devolucao;

	private Parcelamento parcelamento;

	private Date dataRevisao;

	private BigDecimal valorConciliado;

	private String observacaoNota;

	private EntidadeConteudo motivoCancelamento;

	private EntidadeConteudo motivoRefaturamento;

	private EntidadeConteudo motivoInclusao;

	private String descricaoCancelamento;

	private Collection<FaturaItem> listaFaturaItem = new HashSet<FaturaItem>();

	private boolean cobrada;

	private Boolean encerramento;

	private Boolean registroPerda;

	private NaturezaOperacaoCFOP naturezaOperacaoCFOP;

	private Boolean provisaoDevedoresDuvidosos;

	private Boolean faturaAvulso;

	private Integer diasAtraso;

	private boolean indicadorNotaFiscalDevolucao;

	private Transportadora transportadora;

	private Veiculo veiculo;

	private Motorista motorista;

	private Funcionario responsavelRevisao;

	private String logDataInformadaAplicada;

	private Fatura faturaPrincipal;

	private String observacaoComplementar;

	private EntidadeConteudo motivoComplemento;

	// transient
	private Boolean habilitadoCobranca = Boolean.TRUE;

	private Boolean indicadorNotificacaoFatura;
	
	private Boolean indicadorExibirAvisoCorte;
	
	private BigDecimal consumoMedido;

	@Override
	public Transportadora getTransportadora() {

		return transportadora;
	}

	@Override
	public void setTransportadora(Transportadora transportadora) {

		this.transportadora = transportadora;
	}

	@Override
	public Veiculo getVeiculo() {

		return veiculo;
	}

	@Override
	public void setVeiculo(Veiculo veiculo) {

		this.veiculo = veiculo;
	}

	@Override
	public Motorista getMotorista() {

		return motorista;
	}

	@Override
	public void setMotorista(Motorista motorista) {

		this.motorista = motorista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#isEncerramento()
	 */
	@Override
	public Boolean isEncerramento() {

		return encerramento;
	}

	@Override
	public Integer getDiasAtraso() {

		return diasAtraso;
	}

	@Override
	public void setDiasAtraso(Integer diasAtraso) {

		this.diasAtraso = diasAtraso;
	}

	@Override
	public void setEncerramento(Boolean encerramento) {

		this.encerramento = encerramento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#getContratoAtual
	 * ()
	 */
	@Override
	public Contrato getContratoAtual() {

		return contratoAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setContratoAtual
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void setContratoAtual(Contrato contratoAtual) {

		this.contratoAtual = contratoAtual;
	}

	@Override
	public String getCicloReferenciaFormatado() {

		String referencia = null;
		String ano = String.valueOf(anoMesReferencia).substring(0, LIMITE_CAMPO_ANO);
		String mes = String.valueOf(anoMesReferencia).substring(INICIO_CAMPO_MES, String.valueOf(anoMesReferencia).length());
		if (numeroCiclo == null) {
			referencia = "";
		} else {
			referencia = mes + "/" + ano + "-" + numeroCiclo;
		}
		return referencia;
	}

	@Override
	public String getValorTotalFormatado() {

		return Util.converterCampoValorDecimalParaString("", this.valorTotal, Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
	}

	@Override
	public String getDataEmissaoFormatada() {

		return Util.converterDataParaStringSemHora(this.dataEmissao, Constantes.FORMATO_DATA_BR);
	}

	@Override
	public String getDataVencimentoFormatada() {

		return Util.converterDataParaStringSemHora(this.dataVencimento, Constantes.FORMATO_DATA_BR);
	}

	/**
	 * @return the motivoRevisao
	 */
	@Override
	public FaturaMotivoRevisao getMotivoRevisao() {

		return motivoRevisao;
	}

	/**
	 * @param motivoRevisao
	 *            the motivoRevisao to set
	 */
	@Override
	public void setMotivoRevisao(FaturaMotivoRevisao motivoRevisao) {

		this.motivoRevisao = motivoRevisao;
	}

	@Override
	public EntidadeConteudo getMotivoInclusao() {

		return motivoInclusao;
	}

	@Override
	public void setMotivoInclusao(EntidadeConteudo motivoInclusao) {

		this.motivoInclusao = motivoInclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getFaturaGeral()
	 */
	@Override
	public FaturaGeral getFaturaGeral() {

		return faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setFaturaGeral
	 * (br.com.ggas.faturamento.FaturaGeral)
	 */
	@Override
	public void setFaturaGeral(FaturaGeral faturaGeral) {

		this.faturaGeral = faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getTipoDocumento()
	 */
	@Override
	public TipoDocumento getTipoDocumento() {

		return tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setTipoDocumento
	 * (br.com.ggas.arrecadacao.TipoDocumento)
	 */
	@Override
	public void setTipoDocumento(TipoDocumento tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}
	
	@Override
	public String getDescricaoPontoConsumo() {
		if (getPontoConsumo() != null) {
			return getPontoConsumo().getDescricao();
		}
		return StringUtils.EMPTY;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Fatura#getCliente
	 * ()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Fatura#setCliente
	 * (br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Fatura#getRota
	 * ()
	 */
	@Override
	public Rota getRota() {

		return rota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Fatura#setRota
	 * (br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getCreditoDebitoSituacao()
	 */
	@Override
	public CreditoDebitoSituacao getCreditoDebitoSituacao() {

		return creditoDebitoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setCreditoDebitoSituacao
	 * (br.com.ggas.faturamento
	 * .CreditoDebitoSituacao)
	 */
	@Override
	public void setCreditoDebitoSituacao(CreditoDebitoSituacao creditoDebitoSituacao) {

		this.creditoDebitoSituacao = creditoDebitoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Fatura#getSegmento
	 * ()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Fatura#setSegmento
	 * (br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getHistoricoConsumo()
	 */
	@Override
	public HistoricoConsumo getHistoricoConsumo() {

		return historicoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setHistoricoConsumo
	 * (br.com.ggas.medicao.consumo
	 * .HistoricoConsumo)
	 */
	@Override
	public void setHistoricoConsumo(HistoricoConsumo historicoConsumo) {

		this.historicoConsumo = historicoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getHistoricoMedicao()
	 */
	@Override
	public HistoricoMedicao getHistoricoMedicao() {

		return historicoMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setHistoricoMedicao
	 * (br.com.ggas.medicao.leitura
	 * .HistoricoMedicao)
	 */
	@Override
	public void setHistoricoMedicao(HistoricoMedicao historicoMedicao) {

		this.historicoMedicao = historicoMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getAnoMesReferencia()
	 */
	@Override
	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setAnoMesReferencia(java.lang.Integer)
	 */
	@Override
	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getNumeroCiclo()
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setNumeroCiclo(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Fatura#
	 * setValorTotal(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getSituacaoPagamento()
	 */
	@Override
	public EntidadeConteudo getSituacaoPagamento() {

		return situacaoPagamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setSituacaoPagamento
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setSituacaoPagamento(EntidadeConteudo situacaoPagamento) {

		this.situacaoPagamento = situacaoPagamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#getDataEmissao
	 * ()
	 */
	@Override
	public Date getDataEmissao() {
		Date data = null;
		if(this.dataEmissao != null) {
			data = (Date) dataEmissao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.Fatura#setDataEmissao (java.util.Date)
	 */
	@Override
	public void setDataEmissao(Date dataEmissao) {
		if (dataEmissao != null) {
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.Fatura# getDataVencimento()
	 */
	@Override
	public Date getDataVencimento() {
		Date data = null;
		if (this.dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}
	
	@Override
	public boolean isVencida() {
		return DataUtil.antesHoje(getDataVencimento());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.Fatura# setDataVencimento(java.util.Date)
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {
		if (dataVencimento != null) {
			this.dataVencimento = (Date) dataVencimento.clone();
		} else {
			this.dataVencimento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getMotivoAlteracao()
	 */
	@Override
	public String getMotivoAlteracao() {

		return motivoAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setMotivoAlteracao(java.lang.String)
	 */
	@Override
	public void setMotivoAlteracao(String motivoAlteracao) {

		this.motivoAlteracao = motivoAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getListaFaturaItem()
	 */
	@Override
	public Collection<FaturaItem> getListaFaturaItem() {

		return listaFaturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setListaFaturaItem(java.util.Collection)
	 */
	@Override
	public void setListaFaturaItem(Collection<FaturaItem> listaFaturaItem) {

		this.listaFaturaItem = listaFaturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#getDevolucao
	 * ()
	 */
	@Override
	public Devolucao getDevolucao() {

		return devolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setDevolucao
	 * (
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * )
	 */
	@Override
	public void setDevolucao(Devolucao devolucao) {

		this.devolucao = devolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#getParcelamento
	 * ()
	 */
	@Override
	public Parcelamento getParcelamento() {

		return parcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setParcelamento
	 * (
	 * br.com.ggas.cobranca.parcelamento.Parcelamento
	 * )
	 */
	@Override
	public void setParcelamento(Parcelamento parcelamento) {

		this.parcelamento = parcelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.Fatura#getDataRevisao ()
	 */
	@Override
	public Date getDataRevisao() {
		Date data = null;
		if (this.dataRevisao != null) {
			data = (Date) dataRevisao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setDataRevisao
	 * (java.util.Date)
	 */
	@Override
	public void setDataRevisao(Date dataRevisao) {
		if(dataRevisao != null){
			this.dataRevisao = (Date) dataRevisao.clone();
		} else {
			this.dataRevisao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getValorConciliado()
	 */
	@Override
	public BigDecimal getValorConciliado() {

		return valorConciliado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setValorConciliado(java.math.BigDecimal)
	 */
	@Override
	public void setValorConciliado(BigDecimal valorConciliado) {

		this.valorConciliado = valorConciliado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getObservacaoNota()
	 */
	@Override
	public String getObservacaoNota() {

		return observacaoNota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getMotivoCancelamento()
	 */
	@Override
	public EntidadeConteudo getMotivoCancelamento() {

		return motivoCancelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setMotivoCancelamento
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setMotivoCancelamento(EntidadeConteudo motivoCancelamento) {

		this.motivoCancelamento = motivoCancelamento;
	}

	@Override
	public EntidadeConteudo getMotivoRefaturamento() {

		return motivoRefaturamento;
	}

	@Override
	public void setMotivoRefaturamento(EntidadeConteudo motivoRefaturamento) {

		this.motivoRefaturamento = motivoRefaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setObservacaoNota(java.lang.String)
	 */
	@Override
	public void setObservacaoNota(String observacaoNota) {

		this.observacaoNota = observacaoNota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getDescricaoCancelamento()
	 */
	@Override
	public String getDescricaoCancelamento() {

		return descricaoCancelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setDescricaoCancelamento(java.lang.String)
	 */
	@Override
	public void setDescricaoCancelamento(String descricaoCancelamento) {

		this.descricaoCancelamento = descricaoCancelamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getFaturaAgrupada()
	 */
	@Override
	public Fatura getFaturaAgrupada() {

		return faturaAgrupada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * setFaturaAgrupada
	 * (br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFaturaAgrupada(Fatura faturaAgrupada) {

		this.faturaAgrupada = faturaAgrupada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#getContrato
	 * ()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setContrato
	 * (br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#isFaturaEmitida
	 * ()
	 */
	@Override
	public boolean isCobrada() {

		return cobrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setFaturaEmitida
	 * (boolean)
	 */
	@Override
	public void setCobrada(boolean cobrada) {

		this.cobrada = cobrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#isRegistroPerda
	 * ()
	 */
	@Override
	public Boolean isRegistroPerda() {

		return registroPerda;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.Fatura#setRegistroPerda
	 * (java.lang.Boolean)
	 */
	@Override
	public void setRegistroPerda(Boolean registroPerda) {

		this.registroPerda = registroPerda;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#
	 * getValorSaldoConciliado()
	 */
	@Override
	public BigDecimal getValorSaldoConciliado() {

		BigDecimal valorRetorno = BigDecimal.ZERO;

		if ((valorTotal != null) && (valorConciliado != null)) {
			valorRetorno = valorTotal.subtract(valorConciliado);
		}

		return valorRetorno;
	}

	@Override
	public void setNaturezaOperacaoCFOP(NaturezaOperacaoCFOP naturezaOperacaoCFOP) {

		this.naturezaOperacaoCFOP = naturezaOperacaoCFOP;
	}

	@Override
	public NaturezaOperacaoCFOP getNaturezaOperacaoCFOP() {

		return naturezaOperacaoCFOP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#getProvisaoDevedoresDuvidosos()
	 */
	@Override
	public Boolean getProvisaoDevedoresDuvidosos() {

		return provisaoDevedoresDuvidosos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.Fatura#setProvisaoDevedoresDuvidosos(java.lang.Boolean)
	 */
	@Override
	public void setProvisaoDevedoresDuvidosos(Boolean provisaoDevedoresDuvidosos) {

		this.provisaoDevedoresDuvidosos = provisaoDevedoresDuvidosos;
	}

	/**
	 * @return the dataCancelamento
	 */
	@Override
	public Date getDataCancelamento() {
		Date data = null;
		if(this.dataCancelamento != null) {
			data = (Date) dataCancelamento.clone();
		}
		return data;
	}

	/**
	 * @param dataCancelamento
	 *            the dataCancelamento to set
	 */
	@Override
	public void setDataCancelamento(Date dataCancelamento) {
		if (dataCancelamento != null) {
			this.dataCancelamento = (Date) dataCancelamento.clone();
		} else {
			this.dataCancelamento = null;
		}
	}

	@Override
	public boolean isIndicadorNotaFiscalDevolucao() {

		return indicadorNotaFiscalDevolucao;
	}

	@Override
	public void setIndicadorNotaFiscalDevolucao(boolean indicadorNotaFiscalDevolucao) {

		this.indicadorNotaFiscalDevolucao = indicadorNotaFiscalDevolucao;
	}

	@Override
	public Boolean getFaturaAvulso() {

		return faturaAvulso;
	}

	@Override
	public void setFaturaAvulso(Boolean faturaAvulso) {

		this.faturaAvulso = faturaAvulso;

	}

	@Override
	public Funcionario getResponsavelRevisao() {

		return responsavelRevisao;
	}

	@Override
	public void setResponsavelRevisao(Funcionario responsavelRevisao) {

		this.responsavelRevisao = responsavelRevisao;
	}

	@Override
	public Boolean getHabilitadoCobranca() {

		return habilitadoCobranca;
	}

	@Override
	public void setHabilitadoCobranca(Boolean habilitadoCobranca) {

		this.habilitadoCobranca = habilitadoCobranca;
	}

	@Override
	public Long getIdMunicipioCliente() {

		if (getCliente().getEnderecoPrincipal() != null && getCliente().getEnderecoPrincipal().getMunicipio() != null) {
			return getCliente().getEnderecoPrincipal().getMunicipio().getChavePrimaria();
		}
		return 0L;
	}

	@Override
	public Fatura getFaturaPrincipal() {

		return faturaPrincipal;
	}

	@Override
	public void setFaturaPrincipal(Fatura faturaPrincipal) {

		this.faturaPrincipal = faturaPrincipal;
	}

	@Override
	public String getObservacaoComplementar() {

		return observacaoComplementar;
	}

	@Override
	public void setObservacaoComplementar(String observacaoComplementar) {

		this.observacaoComplementar = observacaoComplementar;
	}

	@Override
	public String getLogDataInformadaAplicada() {

		return logDataInformadaAplicada;
	}

	@Override
	public void setLogDataInformadaAplicada(String logDataInformadaAplicada) {

		this.logDataInformadaAplicada = logDataInformadaAplicada;
	}
	@Override
	public EntidadeConteudo getMotivoComplemento() {

		return motivoComplemento;
	}
	@Override
	public void setMotivoComplemento(EntidadeConteudo motivoComplemento) {

		this.motivoComplemento = motivoComplemento;
	}

	@Override
	public Fatura clone() {
		try {
			return (Fatura) super.clone();
		} catch (CloneNotSupportedException e) {
			LOG.error(e);
			return this;
		}
	}
	
	public boolean emRevisao() {
		return dataRevisao != null && motivoRevisao != null;
	}

	@Override
	public Boolean getIndicadorNotificacaoFatura() {
		return indicadorNotificacaoFatura;
	}

	@Override
	public void setIndicadorNotificacaoFatura(Boolean indicadorNotificacaoFatura) {
		this.indicadorNotificacaoFatura = indicadorNotificacaoFatura;
		
	}

	@Override
	public Boolean getIndicadorExibirAvisoCorte() {
		return indicadorExibirAvisoCorte;
	}

	@Override
	public void setIndicadorExibirAvisoCorte(Boolean indicadorExibirAvisoCorte) {
		this.indicadorExibirAvisoCorte = indicadorExibirAvisoCorte;
	}

	@Override
	public BigDecimal getConsumoMedido() {
		return consumoMedido;
	}

	@Override
	public void setConsumoMedido(BigDecimal consumoMedido) {
		this.consumoMedido = consumoMedido;
	}
}
