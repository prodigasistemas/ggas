/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.impl;

import java.util.Map;

import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 *
 */
class LancamentoItemContabilImpl extends EntidadeNegocioImpl implements LancamentoItemContabil {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2503365293834257249L;

	private String descricao;

	private String descricaoAbreviada;

	private EntidadeConteudo tipoCreditoDebito;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.LancamentoItemContabil
	 * #getTipoCreditoDebito()
	 */
	@Override
	public EntidadeConteudo getTipoCreditoDebito() {

		return tipoCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.LancamentoItemContabil
	 * #setTipoCreditoDebito(br.com.ggas.geral.
	 * EntidadeConteudo)
	 */
	@Override
	public void setTipoCreditoDebito(EntidadeConteudo tipoCreditoDebito) {

		this.tipoCreditoDebito = tipoCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * LancamentoItemContabil#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * LancamentoItemContabil
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * LancamentoItemContabil
	 * #getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * LancamentoItemContabil
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
