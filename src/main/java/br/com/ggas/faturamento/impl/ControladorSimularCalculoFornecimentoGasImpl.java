/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.ControladorSimularCalculoFornecimentoGas;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável pela implementação dos métodos relacionados ao Controlador da Simulação do Calculo de Fornecimento de Gás.
 *
 */
class ControladorSimularCalculoFornecimentoGasImpl extends ControladorNegocioImpl implements ControladorSimularCalculoFornecimentoGas {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeTarifaVigenciaFaixa() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaFaixa.BEAN_ID_TARIFA_VIGENCIA_FAIXA);
	}

	public Class<?> getClasseEntidadeTarifa() {

		return ServiceLocator.getInstancia().getClassPorID(Tarifa.BEAN_ID_TARIFA);
	}

	public Class<?> getClasseEntidadeTarifaVigencia() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigencia.BEAN_ID_TARIFA_VIGENCIA);
	}

	public Class<?> getClasseEntidadeTarifaTributo() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaTributo.BEAN_ID_TARIFA_VIGENCIA_TRIBUTO);
	}

	public Class<?> getClasseEntidadeTributo() {

		return ServiceLocator.getInstancia().getClassPorID(Tributo.BEAN_ID_TRIBUTO);
	}

	public Class<?> getClasseEntidadeTributoAliquota() {

		return ServiceLocator.getInstancia().getClassPorID(TributoAliquota.BEAN_ID_TRIBUTO_ALIQUOTA);
	}

	public Class<?> getClasseEntidadeTarifaFaixaDesconto() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaFaixaDesconto.BEAN_ID_TARIFA_FAIXA_DESCONTO);
	}

	/**
	 * Calcula valor.
	 *
	 * @param consumo
	 *            the consumo
	 * @param colecaoFaixasVigencia
	 *            the colecao faixas vigencia
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception

	private BigDecimal calculaValor(BigDecimal consumo, Collection<TarifaVigenciaFaixa> colecaoFaixasVigencia, TarifaVigencia tarifaVigencia)
					throws NegocioException {

		BigDecimal valorCalculado = BigDecimal.ZERO;

		if (tarifaVigencia != null && tarifaVigencia.getTipoCalculo() != null) {

			if (TIPO_CALCULO_FAIXA != null && tarifaVigencia.getTipoCalculo().getChavePrimaria() == TIPO_CALCULO_FAIXA) {
				valorCalculado = calculoFaixa(consumo, colecaoFaixasVigencia);
			} else if (TIPO_CALCULO_CASCATA != null && tarifaVigencia.getTipoCalculo().getChavePrimaria() == TIPO_CALCULO_CASCATA) {
				valorCalculado = calculoCascata(consumo, colecaoFaixasVigencia);
			} else {
				// TODO alterar mensagem
				throw new NegocioException("Tipo de Cálculo inexistente.");
			}
		} else {
			throw new NegocioException("Tipo de Cálculo inexistente.");
		}

		return valorCalculado;
	}
	 */

	/**
	 * Método responsável para calcular o valor do
	 * consumo em faixa.
	 *
	 * @param consumoDoPeriodo
	 *            the consumo do periodo
	 * @param colecaoTarifaVigenciaFaixa
	 *            the colecao tarifa vigencia faixa
	 * @return ValorCalculoTarifa
	 */

	// virginia validar consulta e testar

	/**
	 * Consultar tarifa.
	 *
	 * @param chavePrimaria - {@link Long}
	 * @return tarifa - {@link Tarifa}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public Tarifa consultarTarifa(Long chavePrimaria) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifa());

		criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));

		return (Tarifa) criteria.uniqueResult();

	}

	// ************************
	// consultas
	// ************************

	/**
	 * Consultar tarifa vigencia.
	 *
	 * @param tarifa
	 *            the tarifa
	 * @param dataVigenciaInicial
	 *            the data vigencia inicial
	 * @param dataVigenciaFinal
	 *            the data vigencia final
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<TarifaVigencia> consultarTarifaVigencia(Tarifa tarifa, Date dataVigenciaInicial, Date dataVigenciaFinal)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifa.chavePrimaira = :tarifa");
		hql.append(" and ");
		hql.append(" dataVigencia between :dataInicial and :dataFinal");
		hql.append(" or dataVigencia (");
		hql.append(" select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" where dataVigencia <= :dataInicial");
		hql.append(" and status = :statusAutorizado");
		hql.append(")");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String paramStatusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		query.setLong("tarifa", tarifa.getChavePrimaria());
		query.setDate("dataInicial", dataVigenciaInicial);
		query.setDate("dataFinal", dataVigenciaFinal);
		query.setDate("dataInicial", dataVigenciaInicial);
		query.setLong("statusAutorizado", Long.parseLong(paramStatusAutorizado));

		return query.list();

	}

	/**
	 * Consultar tarifa vigencia faixa.
	 *
	 * @param identificacaoTarifaVigencia
	 *            the identificacao tarifa vigencia
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<TarifaVigenciaFaixa> consultarTarifaVigenciaFaixa(Long identificacaoTarifaVigencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaFaixa().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifaVigencia.chavePrimaria = :idTarifaVigencia");
		hql.append(" order by medidaFim");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTarifaVigencia", identificacaoTarifaVigencia);

		return query.list();
	}

	/**
	 * Obter tarifa vigencia faixa.
	 *
	 * @param identificacaoTarifaVigencia
	 *            the identificacao tarifa vigencia
	 * @param consumo
	 *            the consumo
	 * @param dataVigencia
	 *            the data vigencia
	 * @return the tarifa vigencia faixa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public TarifaVigenciaFaixa obterTarifaVigenciaFaixa(Long identificacaoTarifaVigencia, Long consumo, Date dataVigencia)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaFaixa().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifaVigencia.chavePrimaria = :idTarifaVigencia");
		hql.append(" and ");
		hql.append(" tarifaVigencia.dataVigencia max(:dataVigencia)");
		hql.append(" and ");
		hql.append(" medidaFim max(:consumo)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTarifaVigencia", identificacaoTarifaVigencia);
		query.setDate("dataVigencia", dataVigencia);
		query.setLong("consumo", consumo);

		return (TarifaVigenciaFaixa) query.uniqueResult();
	}

	/**
	 * Obter tarifa vigencia faixa.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param consumoApurado
	 *            the consumo apurado
	 * @return the tarifa vigencia faixa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	// com sql.
	public TarifaVigenciaFaixa obterTarifaVigenciaFaixa(TarifaVigencia tarifaVigencia, BigDecimal consumoApurado) throws NegocioException {

		/**
		 * select * from tarifa_vigencia_faixa
		 * where tavi_cd = 2
		 * and tavf_md_fim =
		 * (select min(tavf_md_fim)
		 * from tarifa_vigencia_faixa
		 * where tavi_cd = 2
		 * and tavf_md_fim >= &parametro)
		 */

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaFaixa().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifaVigencia.chavePrimaria = :chaveTarifaVigencia ");
		hql.append(" and :consumoApurado between medidaInicio and medidaFim ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveTarifaVigencia", tarifaVigencia.getChavePrimaria());
		query.setBigDecimal("consumoApurado", consumoApurado);

		return (TarifaVigenciaFaixa) query.uniqueResult();
	}

	/**
	 * Consultar tarifa tributo.
	 *
	 * @param identificacaoTarifa
	 *            the identificacao tarifa
	 * @param dataVigencia
	 *            the data vigencia
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<TarifaVigenciaTributo> consultarTarifaTributo(Long identificacaoTarifa, Date dataVigencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaTributo().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifa.chavePrimaria = :idTarifa");
		hql.append(" and ");
		hql.append(" dataVigencia = ( select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTarifaTributo().getSimpleName());
		hql.append(" where dataVigencia <= :dataVigencia");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTarifa", identificacaoTarifa);
		query.setDate("dataVigencia", dataVigencia);

		return query.list();
	}

	/**
	 * Consultar tributo.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<Tributo> consultarTributo(Long[] chavesPrimarias) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributo().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria in (:idTributo)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("idTributo", chavesPrimarias);

		return query.list();
	}

	/**
	 * Obter tributo aliquota.
	 *
	 * @param identificacaoTributo
	 *            the identificacao tributo
	 * @param dataVigencia
	 *            the data vigencia
	 * @return the tributo aliquota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public TributoAliquota obterTributoAliquota(Long identificacaoTributo, Date dataVigencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria =:idTributo");
		hql.append(" and ");
		hql.append(" dataVigencia = ( select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" where dataVigencia <= :dataVigencia");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idTributo", identificacaoTributo);
		query.setDate("dataVigencia", dataVigencia);

		return (TributoAliquota) query.uniqueResult();
	}

	/**
	 * Consultar tributo aliquota.
	 *
	 * @param identificacaoTributo
	 *            the identificacao tributo
	 * @param dataVigencia
	 *            the data vigencia
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<TributoAliquota> consultarTributoAliquota(Long[] identificacaoTributo, Date dataVigencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria in (:idsTributos)");
		hql.append(" and ");
		hql.append(" dataVigencia = ( select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" where dataVigencia <= :dataVigencia");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("idsTributos", identificacaoTributo);
		query.setDate("dataVigencia", dataVigencia);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.ControladorSimularCalculoFornecimentoGas#calcularValorFornecimentoGas(br.com.ggas.cadastro.imovel.PontoConsumo
	 * , br.com.ggas.faturamento.tarifa.Tarifa, java.util.Date, java.util.Date, java.math.BigDecimal)
	 */
	@Override
	public void calcularValorFornecimentoGas(PontoConsumo pontoConsumo, Tarifa tarifa, Date dataInicio, Date dataFim,
					BigDecimal valorConsumo) throws GGASException {

		// Verifica quantas vigencias a tarifa
		// possui dentro do intervalo [dtInicio,
		// dtFim]
		Collection<TarifaVigencia> colecaoTarifasVigencia = consultarTarifaVigenciaValidas(tarifa, dataInicio, dataFim);

		// Se a Tarifa não houver Vigências lança
		// Exceção
		if (colecaoTarifasVigencia == null || colecaoTarifasVigencia.isEmpty()) {
			throw new NegocioException("Não existem tarifas vigentes nesse prazo.");
		}

		// TODO verifica se possui tarifa
		// temporária

		// Para cada Vigencia
		for (TarifaVigencia tarifaVigencia : colecaoTarifasVigencia) {

			/*
			 * Se o Cálculo da Tarifa for do tipo
			 * Cascata
			 */
			if (tarifaVigencia.getTipoCalculo() != null && tarifaVigencia.getTipoCalculo().getChavePrimaria() == 1) {

				/*
				 * Se o Cálculo da Tarifa for do
				 * tipo Faixa
				 */
			} else {
				throw new NegocioException("Não foi possível definir o tipo de Cálculo a ser utilizado.");
			}
		}

		// Pega os Tributos associados à Tarifa
		Collection<Tributo> colecaoTributos = consultarTributosPorTarifa(tarifa);

		// TODO verifica isenção de tributos de
		// acordo com o ponto de consumo

		// Verifica qual o tipo de cálculo será
		// utilizado para os tributos (MVA, MPF ou
		// Convencional)
		String tipoCalculo = "";

		// Para cada Tributo
		for (Tributo tributo : colecaoTributos) {

			// Verifica a aliquota vigente
			consultarTributoAliquota(dataFim, tributo.getChavePrimaria());

			// verifica o tipo de calculo a ser
			// usado
			if (tipoCalculo.equals(TIPO_TRIBUTO_CONVENCIONAL)) {
				// Se for convencional

				// Calcula o valor do
				// fornecimento de gás para a
				// aliquota

			} else if (tipoCalculo.equals(TIPO_TRIBUTO_SUBSTITUICAO_MVA)) {
				// se for MVA
				// Calcula o valor do
				// fornecimento de gás para a
				// aliquota
			} else if (tipoCalculo.equals(TIPO_TRIBUTO_SUBSTITUICAO_PMPF)) {
				// se for MPF
				// Calcula o valor do
				// fornecimento de gás para a
				// aliquota
			}
		}

		// Calcula o valor final do m³ do
		// fornecimento de gas

	}

	/**
	 * Este Caso de Uso é responsável pelo cálculo
	 * do valor de Fornecimento de gás a partir do
	 * recebimento dos parâmetros de entrada.
	 *
	 * @param tarifa
	 *            Tarifa
	 * @param dataVigenciaInicial
	 *            the data vigencia inicial
	 * @param dataVigenciaFinal
	 *            the data vigencia final
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Collection<TarifaVigencia> consultarTarifaVigenciaValidas(Tarifa tarifa, Date dataVigenciaInicial, Date dataVigenciaFinal)
					throws GGASException {

		/**
		 * select *
		 * from tarifa_vigencia
		 * where tari_cd = 4
		 * and tavi_dt_vigencia <=
		 * '&data_final_informada'
		 * and tavi_dt_vigencia >= (select
		 * max(tavi_dt_vigencia)
		 * from tarifa_vigencia
		 * where tavi_dt_vigencia <=
		 * '&data_inicio_informada'
		 * and tari_cd = 4)
		 * order by tavi_dt_vigencia desc
		 */
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tarifaVigencia ");
		hql.append(" where ");
		hql.append(" tarifaVigencia.tarifa.chavePrimaria = :tarifa");
		hql.append(" and ");
		hql.append(" (tarifaVigencia.dataVigencia between :dataInicial and :dataFinal");
		hql.append(" or tarifaVigencia.dataVigencia = (");
		hql.append(" select max(tarifaVigenciaAux.dataVigencia) from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tarifaVigenciaAux ");
		hql.append(" where tarifaVigenciaAux.dataVigencia <= :dataInicial");
		hql.append(" ))");
		hql.append(" order by ");
		hql.append(" tarifaVigencia.dataVigencia ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("tarifa", tarifa.getChavePrimaria());
		query.setDate("dataInicial", dataVigenciaInicial);
		query.setDate("dataFinal", dataVigenciaFinal);
		query.setDate("dataInicial", dataVigenciaInicial);

		return query.list();
	}

	/**
	 * Método responáavel para retornar o valor de
	 * consumo de um determinado intervalo, em
	 * comparação com o total de dias
	 * entre a medição inicial e a final.
	 *
	 * @param tarifa
	 *            the tarifa
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */

	/**
	 * Método responsável para calcular o valor do
	 * consumo em cascata
	 *
	 * @param consumoDoPeriodo
	 * @param tarifa
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public Collection<Tributo> consultarTributosPorTarifa(Tarifa tarifa) throws GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributo().getSimpleName());
		hql.append(" tributo ");
		hql.append(" join ");
		hql.append(getClasseEntidadeTarifaTributo().getSimpleName());
		hql.append(" tarifaTributo ");
		hql.append(" inner join tarifaTributo.tarifaVigencia tarifaVigencia ");
		hql.append(" inner join tarifaVigencia.tarifa tarifa ");
		hql.append(" where ");
		hql.append(" tarifa.chavePrimaria = :tarifa");
		hql.append(" and ");
		hql.append(" tarifaTributo.tributo.chavePrimaria = tributo.chavePrimaria ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("tarifa", tarifa.getChavePrimaria());

		return query.list();
	}

	/**
	 * Consultar tributo aliquota.
	 *
	 * @param dataVigencia
	 *            the data vigencia
	 * @param idTributo
	 *            the id tributo
	 * @return the tributo aliquota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public TributoAliquota consultarTributoAliquota(Date dataVigencia, Long idTributo) throws NegocioException {

		Long[] colecaoChaves = {idTributo};
		Collection<TributoAliquota> tributosAliquota = consultarTributoAliquota(colecaoChaves, dataVigencia);
		TributoAliquota retorno = null;
		if (tributosAliquota != null && !tributosAliquota.isEmpty()) {
			retorno = (TributoAliquota) tributosAliquota.toArray()[0];
		}

		return retorno;
	}

}
