/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.NaturezaOperacaoCFOP;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela implementação dos métodos relacionados ao Item de Fatura
 *
 */
public class FaturaItemImpl extends EntidadeNegocioImpl implements FaturaItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6678816862343245113L;

	private Fatura fatura;

	private Tarifa tarifa;

	private Rubrica rubrica;

	private CreditoDebitoARealizar creditoDebitoARealizar;

	private BigDecimal quantidade;

	private BigDecimal valorTotal;

	private BigDecimal valorUnitario;

	private Integer quantidadeTotalPrestacao;

	private BigDecimal medidaConsumo;

	private NaturezaOperacaoCFOP naturezaOperacaoCFOP;

	private Integer numeroSequencial;

	private Segmento segmento;

	private BigDecimal valorFixoFaixaTarifa;

	private BigDecimal valorVariavelFaixaTarifa;

	private BigDecimal volumeInicialFaixaTarifa;

	private BigDecimal volumeFinalFaixaTarifa;

	@Override
	public NaturezaOperacaoCFOP getNaturezaOperacaoCFOP() {

		return naturezaOperacaoCFOP;
	}

	@Override
	public void setNaturezaOperacaoCFOP(NaturezaOperacaoCFOP naturezaOperacaoCFOP) {

		this.naturezaOperacaoCFOP = naturezaOperacaoCFOP;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getTarifa()
	 */
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setTarifa
	 * (br.com.ggas.contrato.proposta.Tarifa)
	 */
	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getRubrica()
	 */
	@Override
	public Rubrica getRubrica() {

		return rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setRubrica
	 * (br.com.ggas.faturamento.Rubrica)
	 */
	@Override
	public void setRubrica(Rubrica rubrica) {

		this.rubrica = rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getCreditoDebitoARealizar()
	 */
	@Override
	public CreditoDebitoARealizar getCreditoDebitoARealizar() {

		return creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setCreditoDebitoARealizar
	 * (br.com.ggas.faturamento
	 * .CreditoDebitoARealizar)
	 */
	@Override
	public void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) {

		this.creditoDebitoARealizar = creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getQuantidade()
	 */
	@Override
	public BigDecimal getQuantidade() {

		return quantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setQuantidade(java.lang.Integer)
	 */
	@Override
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setValorTotal(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getValorUnitario()
	 */
	@Override
	public BigDecimal getValorUnitario() {

		return valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setValorUnitario(java.math.BigDecimal)
	 */
	@Override
	public void setValorUnitario(BigDecimal valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getQuantidadeTotalPrestacao()
	 */
	@Override
	public Integer getQuantidadeTotalPrestacao() {

		return quantidadeTotalPrestacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setQuantidadeTotalPrestacao
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeTotalPrestacao(Integer quantidadeTotalPrestacao) {

		this.quantidadeTotalPrestacao = quantidadeTotalPrestacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #getMedidaConsumo()
	 */
	@Override
	public BigDecimal getMedidaConsumo() {

		return medidaConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.FaturaItem
	 * #setMedidaConsumo(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaConsumo(BigDecimal medidaConsumo) {

		this.medidaConsumo = medidaConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public void setNumeroSequencial(Integer numeroSequencial) {

		this.numeroSequencial = numeroSequencial;
	}

	@Override
	public Integer getNumeroSequencial() {

		return numeroSequencial;
	}

	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	@Override
	public BigDecimal getValorFixoFaixaTarifa() {
		return valorFixoFaixaTarifa;
	}

	@Override
	public void setValorFixoFaixaTarifa(BigDecimal valorFixoFaixaTarifa) {
		this.valorFixoFaixaTarifa = valorFixoFaixaTarifa;
	}

	@Override
	public BigDecimal getValorVariavelFaixaTarifa() {
		return valorVariavelFaixaTarifa;
	}

	@Override
	public void setValorVariavelFaixaTarifa(BigDecimal valorVariavelFaixaTarifa) {
		this.valorVariavelFaixaTarifa = valorVariavelFaixaTarifa;
	}

	@Override
	public BigDecimal getVolumeInicialFaixaTarifa() {
		return volumeInicialFaixaTarifa;
	}

	@Override
	public void setVolumeInicialFaixaTarifa(BigDecimal volumeInicialFaixaTarifa) {
		this.volumeInicialFaixaTarifa = volumeInicialFaixaTarifa;
	}

	@Override
	public BigDecimal getVolumeFinalFaixaTarifa() {
		return volumeFinalFaixaTarifa;
	}

	@Override
	public void setVolumeFinalFaixaTarifa(BigDecimal volumeFinalFaixaTarifa) {
		this.volumeFinalFaixaTarifa = volumeFinalFaixaTarifa;
	}

	@Override
	public FaturaItem clone() {
		try {
			return (FaturaItem) super.clone();
		} catch (CloneNotSupportedException e) {
			return this;
		}
	}

}
