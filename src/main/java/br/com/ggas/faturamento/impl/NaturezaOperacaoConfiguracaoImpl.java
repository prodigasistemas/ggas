/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.NaturezaOperacao;
import br.com.ggas.faturamento.NaturezaOperacaoConfiguracao;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Natureza Operacao Configuracao.
 *  
 */
public class NaturezaOperacaoConfiguracaoImpl extends EntidadeNegocioImpl implements NaturezaOperacaoConfiguracao {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1832309788666424250L;

	private NaturezaOperacao naturezaOperacao;

	private Segmento segmento;

	private RamoAtividade ramoAtividade;

	private Rubrica rubrica;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao
	 * #getNaturezaOperacao()
	 */
	@Override
	public NaturezaOperacao getNaturezaOperacao() {

		return naturezaOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao
	 * #setNaturezaOperacao
	 * (br.com.ggas.faturamento.NaturezaOperacao)
	 */
	@Override
	public void setNaturezaOperacao(NaturezaOperacao naturezaOperacao) {

		this.naturezaOperacao = naturezaOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao#getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao
	 * #setSegmento(br.
	 * com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao
	 * #getRamoAtividade()
	 */
	@Override
	public RamoAtividade getRamoAtividade() {

		return ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao
	 * #setRamoAtividade
	 * (br.com.ggas.cadastro.imovel.RamoAtividade)
	 */
	@Override
	public void setRamoAtividade(RamoAtividade ramoAtividade) {

		this.ramoAtividade = ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao#getRubrica()
	 */
	@Override
	public Rubrica getRubrica() {

		return rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.
	 * NaturezaOperacaoConfiguracao
	 * #setRubrica(br.com
	 * .ggas.faturamento.rubrica.Rubrica)
	 */
	@Override
	public void setRubrica(Rubrica rubrica) {

		this.rubrica = rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
