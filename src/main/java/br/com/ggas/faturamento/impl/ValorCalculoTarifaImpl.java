/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.impl;

import java.math.BigDecimal;

import br.com.ggas.faturamento.ValorCalculoTarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;

class ValorCalculoTarifaImpl implements ValorCalculoTarifa {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5182312789141421015L;

	private TarifaVigenciaFaixa faixaConsumo;

	private BigDecimal valorFixoSemImposto;

	private BigDecimal valorFixoComImposto;

	private BigDecimal valorFaixaSemImposto;

	private BigDecimal valorFaixaComImposto;

	private BigDecimal consumo;

	private BigDecimal valorConsumo;

	private BigDecimal desconto;

	/**
	 * @return the faixaConsumo
	 */
	@Override
	public TarifaVigenciaFaixa getFaixaConsumo() {

		return faixaConsumo;
	}

	/**
	 * @param faixaConsumo
	 *            the faixaConsumo to set
	 */
	@Override
	public void setFaixaConsumo(TarifaVigenciaFaixa faixaConsumo) {

		this.faixaConsumo = faixaConsumo;
	}

	/**
	 * @return the valorFixoSemImposto
	 */
	@Override
	public BigDecimal getValorFixoSemImposto() {

		return valorFixoSemImposto;
	}

	/**
	 * @param valorFixoSemImposto
	 *            the valorFixoSemImposto to set
	 */
	@Override
	public void setValorFixoSemImposto(BigDecimal valorFixoSemImposto) {

		this.valorFixoSemImposto = valorFixoSemImposto;
	}

	/**
	 * @return the valorFixoComImposto
	 */
	@Override
	public BigDecimal getValorFixoComImposto() {

		return valorFixoComImposto;
	}

	/**
	 * @param valorFixoComImposto
	 *            the valorFixoComImposto to set
	 */
	@Override
	public void setValorFixoComImposto(BigDecimal valorFixoComImposto) {

		this.valorFixoComImposto = valorFixoComImposto;
	}

	/**
	 * @return the valorFaixaSemImposto
	 */
	@Override
	public BigDecimal getValorFaixaSemImposto() {

		return valorFaixaSemImposto;
	}

	/**
	 * @param valorFaixaSemImposto
	 *            the valorFaixaSemImposto to set
	 */
	@Override
	public void setValorFaixaSemImposto(BigDecimal valorFaixaSemImposto) {

		this.valorFaixaSemImposto = valorFaixaSemImposto;
	}

	/**
	 * @return the valorFaixaComImposto
	 */
	@Override
	public BigDecimal getValorFaixaComImposto() {

		return valorFaixaComImposto;
	}

	/**
	 * @param valorFaixaComImposto
	 *            the valorFaixaComImposto to set
	 */
	@Override
	public void setValorFaixaComImposto(BigDecimal valorFaixaComImposto) {

		this.valorFaixaComImposto = valorFaixaComImposto;
	}

	/**
	 * @return the consumo
	 */
	@Override
	public BigDecimal getConsumo() {

		return consumo;
	}

	/**
	 * @param consumo
	 *            the consumo to set
	 */
	@Override
	public void setConsumo(BigDecimal consumo) {

		this.consumo = consumo;
	}

	/**
	 * @return the valorConsumo
	 */
	@Override
	public BigDecimal getValorConsumo() {

		return valorConsumo;
	}

	/**
	 * @param valorConsumo
	 *            the valorConsumo to set
	 */
	@Override
	public void setValorConsumo(BigDecimal valorConsumo) {

		this.valorConsumo = valorConsumo;
	}

	/**
	 * @return the desconto
	 */
	@Override
	public BigDecimal getDesconto() {

		return desconto;
	}

	/**
	 * @param desconto
	 *            the desconto to set
	 */
	@Override
	public void setDesconto(BigDecimal desconto) {

		this.desconto = desconto;
	}

}
