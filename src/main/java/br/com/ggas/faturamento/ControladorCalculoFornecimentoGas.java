/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.impl.DadosFaixaVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos 
 * relacionados ao controlador de calculos de fornecimento de gás
 * 
 */
public interface ControladorCalculoFornecimentoGas extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS = "controladorCalculoFornecimentoGas";

	String ERRO_QUANTIDADE_CONSUMO_DIARIO = "ERRO_QUANTIDADE_CONSUMO_DIARIO";

	String ERRO_SIMULAR_DADOS_FICTICIOS_APURACAO_DIARIA = "ERRO_SIMULAR_DADOS_FICTICIOS_APURACAO_DIARIA";

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param itemFatura the item fatura
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param consumoApurado the consumo apurado
	 * @param colecaoPontoConsumo the colecao ponto consumo
	 * @param tarifaVigenciaParametro the tarifa vigencia parametro
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param isFaturaAvulso the is fatura avulso
	 * @param contrato the contrato
	 * @param isConsumoDrawback the is consumo drawback
	 * @return the dados fornecimento gas vo
	 * @throws NegocioException the negocio exception
	 */
	DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, TarifaVigencia tarifaVigenciaParametro,
					boolean isConsiderarDescontos, boolean isFaturaAvulso, Contrato contrato, Boolean isConsumoDrawback)
					throws NegocioException;

	/**
	 * Calcular valor fornecimento gas.
	 * 
	 * @param itemFatura                the item fatura
	 * @param dataInicio                the data inicio
	 * @param dataFim                   the data fim
	 * @param consumoApurado            the consumo apurado
	 * @param colecaoPontoConsumo       the colecao ponto consumo
	 * @param isConsiderarDescontos     the is considerar descontos
	 * @param idContratoAditadoAlterado the id contrato aditado alterado
	 * @param isFaturaAvulso            the is fatura avulso
	 * @param idContrato                the id contrato
	 * @param isConsumoDrawback         the is consumo drawback
	 * 
	 * @param contratoAditadoAlterado   the contrato aditado alterado
	 * @param contrato                  the contrato
	 * @param valorInformado            the valor informado
	 * @param param                     - {@link Map}
	 * @return the dados fornecimento gas vo
	 * @throws NegocioException the negocio exception
	 */
	DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
			BigDecimal consumoApurado, Collection<PontoConsumo> colecaoPontoConsumo, boolean isConsiderarDescontos,
			Contrato contratoAditadoAlterado, Boolean isFaturaAvulso, Contrato contrato, Boolean isConsumoDrawback,
			String valorInformado, Map<String, Object> param) throws NegocioException;

	/**
	 * Calcular valor fornecimento gas.
	 *
	 * @param itemFatura the item fatura
	 * @param dataInicio the data inicio
	 * @param dataFim the data fim
	 * @param consumoApurado the consumo apurado
	 * @param tarifa the tarifa
	 * @param isConsiderarDescontos the is considerar descontos
	 * @param isFaturaAvulso the is fatura avulso
	 * @param contrato the contrato
	 * @param isConsumoDrawback the is consumo drawback
	 * @return the dados fornecimento gas vo
	 * @throws NegocioException the negocio exception
	 */
	DadosFornecimentoGasVO calcularValorFornecimentoGas(EntidadeConteudo itemFatura, Date dataInicio, Date dataFim,
					BigDecimal consumoApurado, Tarifa tarifa, boolean isConsiderarDescontos, boolean isFaturaAvulso, Contrato contrato,
					Boolean isConsumoDrawback) throws NegocioException;

	/**
	 * Valida dados calcular valor fornecimento gas.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param idTarifaReal
	 *            the id tarifa real
	 * @param idTarifaSimulada
	 *            the id tarifa simulada
	 * @param idHistoricoConsumo
	 *            the id historico consumo
	 * @param idVigencia
	 *            the id vigencia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validaDadosCalcularValorFornecimentoGas(String idPontoConsumo, Long idTarifaReal, Long idTarifaSimulada, Long idHistoricoConsumo,
					Long idVigencia) throws NegocioException;

	/**
	 * Listar tarifas por ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param contrato the contrato
	 * @param itemFatura the item fatura
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Tarifa> listarTarifasPorPontoConsumo(PontoConsumo pontoConsumo, Contrato contrato, EntidadeConteudo itemFatura)
					throws NegocioException;

	/**
	 * Valida dados calcular fornecimento ficticio.
	 * 
	 * @param idTarifa
	 *            the id tarifa
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @param totalConsumido
	 *            the total consumido
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validaDadosCalcularFornecimentoFicticio(Long idTarifa, Date dataInicio, Date dataFim, String totalConsumido)
					throws NegocioException;
	/**
	 * 
	 * @return
	 */
	String getLog();

	/**
	 * Método que calcula a os valores médios para
	 * um item de fatura, de uma determinada
	 * Tarifa, utilizada num período.
	 * 
	 * @param pontoConsumo
	 *            o Ponto de Consumo
	 * @param itemFatura
	 *            o Item de Fatura
	 * @param dataInicioPeriodoMedicao
	 *            a data de início do período
	 * @param dataFimPeriodoMedicao
	 *            a data de fim do período
	 * @param isConsiderarDescontos
	 *            se deve considerar descontos no
	 *            cálculo
	 * @return Coleção de tarifaVigenciaFaixa com
	 *         o valor médio
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<TarifaVigenciaFaixa> calcularTarifaMediaNoPeriodo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Date dataInicioPeriodoMedicao, Date dataFimPeriodoMedicao, boolean isConsiderarDescontos) throws NegocioException;

	/**
	 * Método responsável por calcular os
	 * tributos. Caso o substituto seja PETROBRAS
	 * e o tipo seja MVA, a substituição será
	 * feita nesta etapa.
	 *
	 * @param pontoConsumo            Ponto de Consumo.
	 * @param itemFatura            Item de Fatura.
	 * @param colecaoTributos            Coleção de Tributos.
	 * @param escala            Escala que será usada no
	 *            cálculo.
	 * @param tipoArredondamento            Tipo de Arredondamento que será
	 *            usado no cálculo.
	 * @param valorTarifa            Valor da Tarifa.
	 * @param precoGas            Preço médio do gás para o
	 *            período de medição.
	 * @param valorComplementarInformado the valor complementar informado
	 * @param ultimaLeitura the ultima leitura
	 * @return Coleção com objeto DadosTributosVO.
	 * @throws NegocioException             Caso ocorra algum erro.
	 */
	Collection<DadosTributoVO> calcularTributos(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Collection<TributoAliquota> colecaoTributos, Integer escala, Integer tipoArredondamento, BigDecimal valorTarifa,
					BigDecimal precoGas, BigDecimal valorComplementarInformado, Date ultimaLeitura) throws NegocioException;

	/**
	 * Método responsável por obter o tributo
	 * aliquota.
	 * 
	 * @param identificacaoTributo
	 *            identificação do tributo a ser
	 *            obtido.
	 * @param dataVigencia
	 *            Data vigência
	 * @return Tributo Alíquita
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	TributoAliquota obterTributoAliquota(Long identificacaoTributo, Date dataVigencia) throws NegocioException;

	/**
	 * Listar tarifas por ponto consumo sem contrato ativo.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param itemFatura
	 *            the item fatura
	 * @param idContratoAditadoAlterado
	 *            the id contrato aditado alterado
	 * @return the collection
	 */
	Collection<Tarifa> listarTarifasPorPontoConsumoSemContratoAtivo(PontoConsumo pontoConsumo, EntidadeConteudo itemFatura,
					Long idContratoAditadoAlterado);

	/**
	 * Obter tarifa vigencia faixa por consumo.
	 * 
	 * @param colecaoTarifaVigenciaFaixa
	 *            the colecao tarifa vigencia faixa
	 * @param consumoApurado
	 *            the consumo apurado
	 * @return the tarifa vigencia faixa
	 */
	TarifaVigenciaFaixa obterTarifaVigenciaFaixaPorConsumo(Collection<TarifaVigenciaFaixa> colecaoTarifaVigenciaFaixa,
					BigDecimal consumoApurado);

	/**
	 * Agrupar dados faixa para exibicao.
	 * @param dadosFornecimentoGasVO - {@link DadosFornecimentoGasVO}
	 * @return lista de DadosFaixaVO - {@link List}
	 */
	List<DadosFaixaVO> agruparDadosFaixaParaExibicao(DadosFornecimentoGasVO dadosFornecimentoGasVO);

	/**
	 * Agrupar dados vigencia para exibicao.
	 * @param dadosFornecimentoGasReal - {@link DadosFornecimentoGasVO}
	 * @return coleção de DadosFaixaVO - {@link Collection}
	 */
	Collection<DadosFaixaVO> agruparDadosVigenciaParaExibicao(DadosFornecimentoGasVO dadosFornecimentoGasReal);

	/**
	 * Preencher valores total liquido.
	 * @param dadosFaixa - {@link Collection}
	 * 
	 */
	void preencherValoresTotalLiquido(Collection<DadosFaixaVO> dadosFaixa);

	Map<TarifaVigencia, BigDecimal> montarPeriodoDeLeitura(Date periodoMedicaoDataInicio, Date periodoMedicaoDataFim,
			EntidadeConteudo itemFatura, PontoConsumo pontoConsumo) throws NegocioException;
	
}
