/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura de métodos relacionados
 * ao histórico de refaturamento
 *
 */
public interface RefaturamentoHistorico extends EntidadeNegocio {

	String BEAN_ID_REFATURAMENTO_HISTORICO = "refaturamentoHistorico";

	String REFATURAMENTO_HISTORICO_DATA_REFATURAMENTO = "REFATURAMENTO_HISTORICO_DATA_REFATURAMENTO";

	String REFATURAMENTO_HISTORICO_INDICADOR_PROCESSO_CONCLUIDO = "REFATURAMENTO_HISTORICO_INDICADOR_PROCESSO_CONCLUIDO";

	String REFATURAMENTO_HISTORICO_DADOS = "REFATURAMENTO_HISTORICO_DADOS";

	String REFATURAMENTO_HISTORICO_PONTO_CONSUMO = "REFATURAMENTO_HISTORICO_PONTO_CONSUMO";

	/**
	 * @return
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return
	 */
	Contrato getContrato();

	/**
	 * @param contrato
	 */
	void setContrato(Contrato contrato);

	/**
	 * @return
	 */
	Date getDataRefaturamento();

	/**
	 * @param dataRefaturamento
	 */
	void setDataRefaturamento(Date dataRefaturamento);

	/**
	 * @return
	 */
	boolean isIndicadorProcessoConcluido();

	/**
	 * @param indicadorProcessoConcluido
	 */
	void setIndicadorProcessoConcluido(boolean indicadorProcessoConcluido);

	/**
	 * @return
	 */
	Date getDataConclusaoRefaturamento();

	/**
	 * @param dataConclusaoRefaturamento
	 */
	void setDataConclusaoRefaturamento(Date dataConclusaoRefaturamento);

	/**
	 * @return
	 */
	byte[] getDados();

	/**
	 * @param dados
	 */
	void setDados(byte[] dados);

	/**
	 * @return
	 * @throws NegocioException
	 */
	DadosResumoFatura getDadosResumoFatura() throws NegocioException;

}
