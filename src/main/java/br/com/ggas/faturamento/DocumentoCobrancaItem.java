/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;

import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos
 * relacionados ao item do documento de cobrança
 */
public interface DocumentoCobrancaItem extends EntidadeNegocio {

	String BEAN_ID_DOCUMENTO_COBRANCA_ITEM = "documentoCobrancaItem";

	/**
	 * @return the documentoCobranca
	 */
	DocumentoCobranca getDocumentoCobranca();

	/**
	 * @param documentoCobranca
	 *            the documentoCobranca to set
	 */
	void setDocumentoCobranca(DocumentoCobranca documentoCobranca);

	/**
	 * @return the faturaGeral
	 */
	FaturaGeral getFaturaGeral();

	/**
	 * @param faturaGeral
	 *            the faturaGeral to set
	 */
	void setFaturaGeral(FaturaGeral faturaGeral);

	/**
	 * @return the creditoDebitoARealizar
	 */
	CreditoDebitoARealizar getCreditoDebitoARealizar();

	/**
	 * @param creditoDebitoARealizar
	 *            the creditoDebitoARealizar to
	 *            set
	 */
	void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar);

	/**
	 * @return the cobrancaDebitoSituacao
	 */
	CobrancaDebitoSituacao getCobrancaDebitoSituacao();

	/**
	 * @param cobrancaDebitoSituacao
	 *            the cobrancaDebitoSituacao to
	 *            set
	 */
	void setCobrancaDebitoSituacao(CobrancaDebitoSituacao cobrancaDebitoSituacao);

	/**
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	void setValor(BigDecimal valor);

	/**
	 * @return String - Retorna valor formatado.
	 */
	String getValorFormatado();

	/**
	 * @return the valorAcrecimos
	 */
	BigDecimal getValorAcrecimos();

	/**
	 * @param valorAcrecimos
	 *            the valorAcrecimos to set
	 */
	void setValorAcrecimos(BigDecimal valorAcrecimos);

	/**
	 * @return the valorDescontos
	 */
	BigDecimal getValorDescontos();

	/**
	 * @param valorDescontos
	 *            the valorDescontos to set
	 */
	void setValorDescontos(BigDecimal valorDescontos);

	/**
	 * @return the numeroPrestacao
	 */
	Integer getNumeroPrestacao();

	/**
	 * @param numeroPrestacao
	 *            the numeroPrestacao to set
	 */
	void setNumeroPrestacao(Integer numeroPrestacao);

	/**
	 * @return the numeroTotalPrestacao
	 */
	Integer getNumeroTotalPrestacao();

	/**
	 * @param numeroTotalPrestacao
	 *            the numeroTotalPrestacao to set
	 */
	void setNumeroTotalPrestacao(Integer numeroTotalPrestacao);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);
}
