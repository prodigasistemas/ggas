/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tributo.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Ramo Atividade Substituicao TributariaImpl
 * @author hsilva
 * 
 * Classe responsável pela implementação dos métodos relacionados a Substituicao Tributaria do Ramo de Atividade
 */
public class RamoAtividadeSubstituicaoTributariaImpl extends EntidadeNegocioImpl implements RamoAtividadeSubstituicaoTributaria {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -5304857976332674451L;

	private RamoAtividade ramoAtividade;

	private EntidadeConteudo tipoSubstituicao;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataInicioVigencia;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataFimVigencia;

	private BigDecimal valorSubstituto;

	private BigDecimal percentualSubstituto;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #getRamoAtividade()
	 */
	@Override
	public RamoAtividade getRamoAtividade() {

		return ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #setRamoAtividade
	 * (br.com.ggas.cadastro.imovel.RamoAtividade)
	 */
	@Override
	public void setRamoAtividade(RamoAtividade ramoAtividade) {

		this.ramoAtividade = ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #getTipoSubstituicao()
	 */
	@Override
	public EntidadeConteudo getTipoSubstituicao() {

		return tipoSubstituicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #setTipoSubstituicao
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoSubstituicao(EntidadeConteudo tipoSubstituicao) {

		this.tipoSubstituicao = tipoSubstituicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #getDataInicioVigencia()
	 */
	@Override
	public Date getDataInicioVigencia() {
		Date dataInicio = null;
		if(this.dataInicioVigencia != null){
			dataInicio = new Date(this.dataInicioVigencia.getTime());
		}
		return dataInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #setDataInicioVigencia(java.util.Date)
	 */
	@Override
	public void setDataInicioVigencia(Date dataInicioVigencia) {
		if(dataInicioVigencia != null) {
			this.dataInicioVigencia = new Date(dataInicioVigencia.getTime());
		} else {
			this.dataInicioVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #getDataFimVigencia()
	 */
	@Override
	public Date getDataFimVigencia() {
		Date dataFim = null;
		if(this.dataFimVigencia!=null) {
			dataFim = new Date(this.dataFimVigencia.getTime());
		}
		return dataFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #setDataFimVigencia(java.util.Date)
	 */
	@Override
	public void setDataFimVigencia(Date dataFimVigencia) {
		if(dataFimVigencia != null) {
			this.dataFimVigencia = new Date(dataFimVigencia.getTime());
		} else {
			this.dataFimVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #getValorSubstituto()
	 */
	@Override
	public BigDecimal getValorSubstituto() {

		return valorSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #setValorSubstituto(java.math.BigDecimal)
	 */
	@Override
	public void setValorSubstituto(BigDecimal valorSubstituto) {

		this.valorSubstituto = valorSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #getPercentualSubstituto()
	 */
	@Override
	public BigDecimal getPercentualSubstituto() {

		return percentualSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * RamoAtividadeSubstituicaoTributaria
	 * #setPercentualSubstituto
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualSubstituto(BigDecimal percentualSubstituto) {

		this.percentualSubstituto = percentualSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(ramoAtividade != null) {
			stringBuilder.append(RAMO_ATIVIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoSubstituicao != null) {
			stringBuilder.append(TIPO_SUBSTITUICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		
		if(dataFimVigencia == null){
			result = prime * result + 0;
		} else {
			result = prime * result + dataFimVigencia.hashCode();
		}
		
		if(dataInicioVigencia == null){
			result = prime * result + 0;
		} else {
			result = prime * result + dataInicioVigencia.hashCode();
		}
		
		if(percentualSubstituto == null){
			result = prime * result + 0;
		} else {
			result = prime * result + percentualSubstituto.hashCode();
		}
		
		if(tipoSubstituicao == null){
			result = prime * result + 0;
		} else {
			result = prime * result + tipoSubstituicao.hashCode();
		}
		
		
		if(valorSubstituto == null){
			result = prime * result + 0;
		} else {
			result = prime * result + valorSubstituto.hashCode();
		}
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RamoAtividadeSubstituicaoTributariaImpl other = (RamoAtividadeSubstituicaoTributariaImpl) obj;
		if (dataFimVigencia == null) {
			if (other.dataFimVigencia != null) {
				return false;
			}
		} else if (!dataFimVigencia.equals(other.dataFimVigencia)) {
			return false;
		}
		if (dataInicioVigencia == null) {
			if (other.dataInicioVigencia != null) {
				return false;
			}
		} else if (!dataInicioVigencia.equals(other.dataInicioVigencia)) {
			return false;
		}
		if (percentualSubstituto == null) {
			if (other.percentualSubstituto != null) {
				return false;
			}
		} else if (!(percentualSubstituto.compareTo(other.percentualSubstituto)==0)) {
			return false;
		}
		if (tipoSubstituicao == null) {
			if (other.tipoSubstituicao != null) {
				return false;
			}
		} else if (!tipoSubstituicao.equals(other.tipoSubstituicao)) {
			return false;
		}
		if (valorSubstituto == null) {
			if (other.valorSubstituto != null) {
				return false;
			}
		} else if (!(valorSubstituto.compareTo(other.valorSubstituto)==0)) {
			return false;
		}
		return true;
	}
	
}
