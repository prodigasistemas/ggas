/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCobrancaImpl representa uma ControladorCobrancaImpl no sistema.
 *
 * @since 22/02/2010
 * 
 */

package br.com.ggas.faturamento.tributo.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
class ControladorTributoImpl extends ControladorNegocioImpl implements ControladorTributo {

	private static final String TRIBUTO = "tributo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	/*
	 * **************** Métodos de Criação ****************
	 */
	@Override
	public EntidadeNegocio criar() {

		return (Tributo) ServiceLocator.getInstancia().getBeanPorID(Tributo.BEAN_ID_TRIBUTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.ControladorTributo#criarTributo()
	 */
	@Override
	public EntidadeNegocio criarTributo() {

		return (Tributo) ServiceLocator.getInstancia().getBeanPorID(Tributo.BEAN_ID_TRIBUTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.ControladorTributo#criarTributoAliquota()
	 */
	@Override
	public EntidadeNegocio criarTributoAliquota() {

		return (TributoAliquota) ServiceLocator.getInstancia().getBeanPorID(TributoAliquota.BEAN_ID_TRIBUTO_ALIQUOTA);
	}

	/*
	 * **************** Métodos get Das Classes de Entidade ****************
	 */

	public Class<?> getClasseEntidadeTributo() {

		return ServiceLocator.getInstancia().getClassPorID(Tributo.BEAN_ID_TRIBUTO);
	}

	public Class<?> getClasseEntidadeTributoAliquota() {

		return ServiceLocator.getInstancia().getClassPorID(TributoAliquota.BEAN_ID_TRIBUTO_ALIQUOTA);
	}

	public Class<?> getClasseEntidadePontoConsumoTributoAliquota() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoTributoAliquota.BEAN_ID_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
	}

	public Class<?> getClasseEntidadeRubricaTributo() {

		return ServiceLocator.getInstancia().getClassPorID(RubricaTributo.BEAN_ID_RUBRICA_TRIBUTO);
	}

	public Class<?> getClasseEntidadeTarifaTributo() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaTributo.BEAN_ID_TARIFA_VIGENCIA_TRIBUTO);
	}

	/**
	 * Método responsável por obter o controlador
	 * de Rubrica.
	 * 
	 * @return O controlador de rubrica.
	 */
	private ControladorRubrica getControladorRubrica() {

		return (ControladorRubrica) ServiceLocator.getInstancia().getControladorNegocio(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);
	}

	/*
	 * **************** Métodos "PRE" ****************
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#preInsercao
	 * (br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if(entidadeNegocio instanceof Tributo) {
			Tributo tributo = (Tributo) entidadeNegocio;
			// consultar o Tributo
			Collection<Tributo> listaTributosExistente = this.consultarTributosExistentes(tributo.getDescricao(),
							tributo.getChavePrimaria());
			if(listaTributosExistente != null && !listaTributosExistente.isEmpty()) {
				throw new NegocioException("Já existe um Tributo com a mesma descrição");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#preAtualizacao
	 * (br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if(entidadeNegocio instanceof Tributo) {
			Tributo tributo = (Tributo) entidadeNegocio;
			Collection<Tributo> listaTributosExistente = this.consultarTributosExistentes(tributo.getDescricao(),
							tributo.getChavePrimaria());
			if(listaTributosExistente != null && !listaTributosExistente.isEmpty()) {
				throw new NegocioException("Já existe um Tributo com a mesma descrição");
			}
		}
	}

	/* ****************************************************************
	 * Métodos de Tributo ********************************************
	 * ********************
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.
	 * ControladorTributo#consultarTributos (java.util.Map)
	 */
	@Override
	public Collection<Tributo> consultarTributos(Map<String, Object> filtro) throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeTributo());

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			String descricaoAbreviada = (String) filtro.get("descricaoAbreviada");
			if(!StringUtils.isEmpty(descricaoAbreviada)) {
				criteria.add(Restrictions.ilike("descricaoAbreviada", Util.formatarTextoConsulta(descricaoAbreviada)));
			}

			EntidadeConteudo tipoAtributo = (EntidadeConteudo) filtro.get("tipoAtributo");
			if(tipoAtributo != null) {
				criteria.add(Restrictions.eq("tipoAtributo.chavePrimaria", tipoAtributo.getChavePrimaria()));
			}

			Boolean indicadorServico = (Boolean) filtro.get("indicadorServico");
			if(indicadorServico != null && indicadorServico) {
				criteria.add(Restrictions.eq("indicadorServico", indicadorServico));
			}
			Boolean indicadorProduto = (Boolean) filtro.get("indicadorProduto");
			if(indicadorProduto != null && indicadorProduto) {
				criteria.add(Restrictions.eq("indicadorProduto", indicadorProduto));
			}

		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo#listarTributos
	 * ()
	 */
	@Override
	public Collection<Tributo> listarTributos() throws GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributo().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo#obterTributo
	 * (java.lang.Long)
	 */
	@Override
	public Tributo obterTributo(Long chavePrimaria) throws GGASException {

		return (Tributo) this.obter(chavePrimaria, getClasseEntidadeTributo());
	}

	@SuppressWarnings("unchecked")
	private Collection<Tributo> consultarTributosExistentes(String nomeTributo, Long idTributo) {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, nomeTributo));
		criteria.add(Restrictions.ne(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idTributo));
		return criteria.list();
	}

	/* ****************************************************************
	 * Métodos de Tributo Aliquota ********************************************
	 * ********************
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.ControladorTributo#consultarTributosAliquota(java.util.Map)
	 */
	@Override
	public Collection<TributoAliquota> consultarTributosAliquota(
					Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTributoAliquota());

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long[] chavesTributos = (Long[]) filtro.get("chavesTributos");
			if((chavesTributos != null) && (chavesTributos.length > 0)) {
				criteria.createAlias(TRIBUTO, TRIBUTO);
				criteria.add(Restrictions.in("tributo.chavePrimaria", chavesTributos));
			} else {
				criteria.setFetchMode(TRIBUTO, FetchMode.JOIN);
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Tributo tributo = (Tributo) filtro.get(TRIBUTO);
			if(tributo != null) {
				criteria.add(Restrictions.eq("tributo.chavePrimaria", tributo.getChavePrimaria()));
			}

			Date dataVigencia = (Date) filtro.get("dataVigencia");
			if(dataVigencia != null) {
				criteria.add(Restrictions.eq("dataVigencia", dataVigencia));
			}

			BigDecimal valorAliquota = (BigDecimal) filtro.get("valorAliquota");
			if(valorAliquota != null) {
				criteria.add(Restrictions.eq("valorAliquota", valorAliquota));
			}

			BigDecimal percentualAliquota = (BigDecimal) filtro.get("percentualAliquota");
			if(percentualAliquota != null) {
				criteria.add(Restrictions.eq("percentualAliquota", percentualAliquota));
			}

			Boolean isVigente = (Boolean) filtro.get("isVigente");
			if((isVigente != null) && (isVigente)) {
				criteria.add(Restrictions.le("dataVigencia", Calendar.getInstance().getTime()));
			}

			Long idMunicipio = (Long) filtro.get("idMunicipio");
			if(idMunicipio != null && idMunicipio > 0) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}

			criteria.setFetchMode("tipoAplicacaoTributoAliquota", FetchMode.JOIN);
			
			criteria.setFetchMode("municipio", FetchMode.JOIN);
			
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.ControladorTributo#listarTributosAliquota()
	 */
	@Override
	public Collection<TributoAliquota> listarTributosAliquota() throws GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by dataVigencia ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo#obterTributo
	 * (java.lang.Long)
	 */
	@Override
	public TributoAliquota obterTributoAliquota(Long chavePrimaria) throws GGASException {

		return (TributoAliquota) this.obter(chavePrimaria, getClasseEntidadeTributoAliquota());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #consultarTributosAliquotaPorTributos (java.util.Collection)
	 */
	@Override
	public List<TributoAliquota> consultarTributosAliquotaPorTarifaTributos(Collection<TarifaVigenciaTributo> colecaoTributo)
					throws GGASException {

		/*
		 * select * from tributo_aliquota ta where ta.trib_cd in (1,2,3,5) and
		 * ta.tral_dt_vigencia = ( select max(tral_dt_vigencia) from
		 * tributo_aliquota tral where tral.trib_cd in (1,2,3,5) and
		 * tral.tral_dt_vigencia < to_date('08-07-2010','dd-mm-yyyy') )
		 */

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" ta ");
		hql.append(" where ");
		hql.append(" ta.tributo.chavePrimaria in ( :listaCodigosTributo ) ");
		hql.append(" and ta.dataVigencia = ( ");
		hql.append(" 	select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append("  	tral ");
		hql.append(" 	where tral.tributo.chavePrimaria in ( :listaCodigosTributo ) ");
		hql.append(" 	and tral.dataVigencia < :dataAtual ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		Collection<Tributo> colecaoTribs = new ArrayList<Tributo>();
		if(colecaoTributo != null) {
			for (TarifaVigenciaTributo tt : colecaoTributo) {
				colecaoTribs.add(tt.getTributo());
			}
		}
		query.setParameterList("listaCodigosTributo", Util.collectionParaArrayChavesPrimarias(colecaoTribs));
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #obterAliquotaVigente(java.lang .Long)
	 */
	@Override
	public TributoAliquota obterAliquotaVigente(Long idTributo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" aliquota ");
		hql.append(" inner join fetch aliquota.tributo ");
		hql.append(" where ");
		hql.append(" aliquota.tributo.chavePrimaria = :idTributo ");
		hql.append(" and aliquota.dataVigencia <= :dataAtual ");
		hql.append(" order by aliquota.dataVigencia desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idTributo", idTributo);
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		query.setMaxResults(1);

		return (TributoAliquota) query.uniqueResult();
	}

	/*
	 * **************** Métodos não implementados ****************
	 */

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #listarTributoAliquotaPorPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo, boolean)
	 */
	@Override
	public Collection<PontoConsumoTributoAliquota> listarTributoAliquotaPorPontoConsumo(PontoConsumo pontoConsumo, boolean isIsento,
					boolean isDrawback) {

		StringBuilder hql = new StringBuilder();

		hql.append("    Select pontoConsumoTributoAliquota from ");
		hql.append(getClasseEntidadePontoConsumoTributoAliquota().getSimpleName());
		hql.append("    pontoConsumoTributoAliquota  ");
		hql.append("    inner join pontoConsumoTributoAliquota.pontoConsumo pontoConsumo");
		hql.append("    inner join pontoConsumoTributoAliquota.tributo trib ");
		hql.append("    where pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorIsencao = :isIsento ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorDrawback = :isDrawback ");
		hql.append("    and  pontoConsumoTributoAliquota.dataInicioVigencia <= :dataAtual ");
		hql.append("    and  (");
		hql.append(" 		pontoConsumoTributoAliquota.dataFimVigencia is null ");
		hql.append("     	or pontoConsumoTributoAliquota.dataFimVigencia >= :dataAtual ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setDate("dataAtual", Calendar.getInstance().getTime());
		query.setBoolean("isIsento", isIsento);
		query.setBoolean("isDrawback", isDrawback);

		return query.list();

	}

	@Override
	public Collection<PontoConsumoTributoAliquota> obterPorcentagemAliquotaPorPontoConsumo(PontoConsumo pontoConsumo,
			boolean isIsento, boolean isDrawback) {

		StringBuilder selectQuery = new StringBuilder();
		
		selectQuery.append("select trib.chavePrimaria as tributo_chavePrimaria, ");
		selectQuery.append(" pontoConsumoTributoAliquota.porcentagemAliquota as porcentagemAliquota ");

		Query query = this.criarConsultaTributoAliquotaPorPontoConsumo(selectQuery.toString(), pontoConsumo, isIsento, isDrawback);
		query.setResultTransformer(new GGASTransformer(getClasseEntidadePontoConsumoTributoAliquota(),
				super.getSessionFactory().getAllClassMetadata()));

		return query.list();

	}

	@Override
	public Collection<Long> obterChavesTributoAliquotaPorPontoConsumo(PontoConsumo pontoConsumo, boolean isIsento,
			boolean isDrawback) {

		String selectQuery = "select trib.chavePrimaria";

		Query query = criarConsultaTributoAliquotaPorPontoConsumo(selectQuery, pontoConsumo, isIsento, isDrawback);

		return query.list();

	}

	private Query criarConsultaTributoAliquotaPorPontoConsumo(String selectQuery, PontoConsumo pontoConsumo,
			boolean isIsento, boolean isDrawback) {
		StringBuilder hql = new StringBuilder();

		hql.append(selectQuery);
		hql.append(" from ");
		hql.append(getClasseEntidadePontoConsumoTributoAliquota().getSimpleName());
		hql.append("    pontoConsumoTributoAliquota  ");
		hql.append("    inner join pontoConsumoTributoAliquota.pontoConsumo pontoConsumo");
		hql.append("    inner join pontoConsumoTributoAliquota.tributo trib ");
		hql.append("    where pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorIsencao = :isIsento ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorDrawback = :isDrawback ");
		hql.append("    and  pontoConsumoTributoAliquota.dataInicioVigencia <= :dataAtual ");
		hql.append("    and  (");
		hql.append(" 		pontoConsumoTributoAliquota.dataFimVigencia is null ");
		hql.append("     	or pontoConsumoTributoAliquota.dataFimVigencia >= :dataAtual ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setDate("dataAtual", Calendar.getInstance().getTime());
		query.setBoolean("isIsento", isIsento);
		query.setBoolean("isDrawback", isDrawback);

		return query;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #listarTributoAliquotaPorPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo, boolean)
	 */
	@Override
	public Collection<Boolean> verificarDestaqueIndicadorNotaDeTributoAliquotaPorPontoConsumo(PontoConsumo pontoConsumo, boolean isIsento,
					boolean isDrawback) {

		StringBuilder hql = new StringBuilder();

		hql.append("    Select pontoConsumoTributoAliquota.destaqueIndicadorNota from ");
		hql.append(getClasseEntidadePontoConsumoTributoAliquota().getSimpleName());
		hql.append("    pontoConsumoTributoAliquota  ");
		hql.append("    inner join pontoConsumoTributoAliquota.pontoConsumo pontoConsumo");
		hql.append("    inner join pontoConsumoTributoAliquota.tributo trib ");
		hql.append("    where pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorIsencao = :isIsento ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorDrawback = :isDrawback ");
		hql.append("    and  pontoConsumoTributoAliquota.dataInicioVigencia <= :dataAtual ");
		hql.append("    and  (");
		hql.append(" 		pontoConsumoTributoAliquota.dataFimVigencia is null ");
		hql.append("     	or pontoConsumoTributoAliquota.dataFimVigencia >= :dataAtual ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setDate("dataAtual", Calendar.getInstance().getTime());
		query.setBoolean("isIsento", isIsento);
		query.setBoolean("isDrawback", isDrawback);

		return query.list();

	}
	/**
	 * Consulta as entidades PontoConsumoTributoAliquota por pontos de consumo
	 * onde a data de inicio da vigência é menor ou igual que a data atual e
	 * com data de fim da vigência com valor nulo ou maior ou igual que a data atual.
	 * Para a entidade PontoConsumoTributoAliquota, obtém apenas os seguintes atributos:
	 * {@code chavePrimaria}, {@code valorDrawback}. Para a entidade PontoConsumo,
	 * obtém os seguintes atributos: {@code chavePrimaria}.
	 * @param chavesPontosConsumo
	 * @param isIsento
	 * @param isDrawback
	 * @return mapa de entidades PontoConsumoTributoAliquota por PontoConsumo
	 */
	@Override
	public Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> listarTributoAliquotaPorPontoConsumo(Long[] chavesPontosConsumo,
					boolean isIsento, boolean isDrawback) {

		StringBuilder hql = new StringBuilder();

		hql.append(" Select pontoConsumo.chavePrimaria, pontoConsumoTributoAliquota.chavePrimaria, ");
		hql.append(" pontoConsumoTributoAliquota.valorDrawback ");
		hql.append(" from ");
		hql.append(getClasseEntidadePontoConsumoTributoAliquota().getSimpleName());
		hql.append("    pontoConsumoTributoAliquota  ");
		hql.append("    inner join pontoConsumoTributoAliquota.pontoConsumo pontoConsumo");
		hql.append("    inner join pontoConsumoTributoAliquota.tributo trib ");
		hql.append("    where  ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontosConsumo));
		hql.append("    and  pontoConsumoTributoAliquota.indicadorIsencao = :isIsento ");
		hql.append("    and  pontoConsumoTributoAliquota.indicadorDrawback = :isDrawback ");
		hql.append("    and  pontoConsumoTributoAliquota.dataInicioVigencia <= :dataAtual ");
		hql.append("    and  (");
		hql.append(" 		pontoConsumoTributoAliquota.dataFimVigencia is null ");
		hql.append("     	or pontoConsumoTributoAliquota.dataFimVigencia >= :dataAtual ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		query.setDate("dataAtual", Calendar.getInstance().getTime());
		query.setBoolean("isIsento", isIsento);
		query.setBoolean("isDrawback", isDrawback);

		return this.construirMapaPontoConsumoTributoAliquotaPorPontoConsumo( query.list() );
	}
	
	/**
	 * Constrói um mapa de coleção de entidades PontoConsumoTributoAliquota
	 * por entidade de PontoConsumo.
	 * @param listaAtributos
	 * @return mapa de entidades PontoConsumoTributoAliquota por PontoConsumo
	 */
	private Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> construirMapaPontoConsumoTributoAliquotaPorPontoConsumo(
					List<Object[]> listaAtributos) {
		
		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();
		
		Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> mapaPontoConsumoTributoAliquota = new HashMap<>();	
		Collection<PontoConsumoTributoAliquota> pontoConsumoTributoAliquotas;
		
		for (Object[] atributos : listaAtributos ) {
			PontoConsumo pontoConsumo = (PontoConsumo) controladorImovel.criarPontoConsumo();
			pontoConsumo.setChavePrimaria((long) atributos[0]);
			PontoConsumoTributoAliquota pontoConsumoTributoAliquota = (PontoConsumoTributoAliquota) controladorImovel
							.criarPontoConsumoTributoAliquota();
			pontoConsumoTributoAliquota.setChavePrimaria((long) atributos[1]);
			pontoConsumoTributoAliquota.setValorDrawback((BigDecimal) atributos[2]);
			if ( !mapaPontoConsumoTributoAliquota.containsKey(pontoConsumo) ) {
				pontoConsumoTributoAliquotas = new ArrayList<>();
				mapaPontoConsumoTributoAliquota.put(pontoConsumo, pontoConsumoTributoAliquotas);
			} else {
				pontoConsumoTributoAliquotas = mapaPontoConsumoTributoAliquota.get(pontoConsumo);
			}
			pontoConsumoTributoAliquotas.add(pontoConsumoTributoAliquota);
		}
		return mapaPontoConsumoTributoAliquota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #listarTributoAliquotaPorRubrica
	 * (br.com.ggas.faturamento.rubrica.Rubrica, java.lang.Long[])
	 */
	@Override
	public Collection<TributoAliquota> listarTributoAliquotaPorRubrica(Rubrica rubrica, Long[] chavesTributosIsentosPontoConsumo) {

		// FIXME ver como não usar o current date do banco, que utiliza a data do banco
		StringBuilder hql = new StringBuilder();
		hql.append(" select  ");
		hql.append(" tributoAliquota ");
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" tributoAliquota ");
		hql.append(" inner join fetch tributoAliquota.tributo tributo ");
		hql.append(" where tributo.chavePrimaria in ( ");
		hql.append("    select rubricaTributo.tributo.chavePrimaria from ");
		hql.append(getClasseEntidadeRubricaTributo().getSimpleName());
		hql.append("    rubricaTributo  ");
		hql.append("    inner join rubricaTributo.rubrica rubrica ");
		hql.append("    where rubrica.chavePrimaria = :idRubrica ");
		hql.append("    and  rubricaTributo.dataInicioVigencia <= :currentDate ");
		hql.append("    and  (rubricaTributo.dataFimVigencia is null or rubricaTributo.dataFimVigencia >= :currentDate ) ");
		hql.append(" ) ");
		if(chavesTributosIsentosPontoConsumo != null && chavesTributosIsentosPontoConsumo.length > 0) {
			hql.append(" and tributo.chavePrimaria not in (:tributosIsentos) ");
		}
		hql.append(" and tributoAliquota.dataVigencia = ( ");
		hql.append("    select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append("    where dataVigencia <= :currentDate ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idRubrica", rubrica.getChavePrimaria());
		if(chavesTributosIsentosPontoConsumo != null && chavesTributosIsentosPontoConsumo.length > 0) {
			query.setParameterList("tributosIsentos", chavesTributosIsentosPontoConsumo);
		}

		query.setDate("currentDate", Calendar.getInstance().getTime());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #listaTributoAliquotaVigente(java .lang.Long[])
	 */
	@Override
	public Collection<TributoAliquota> listaTributoAliquotaVigente(Long[] chavesTributos) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" tributoAliquota ");
		hql.append(" inner join fetch tributoAliquota.tributo tributo ");
		hql.append(" where ");
		hql.append(" tributoAliquota.tributo.chavePrimaria in (:chavesTributos) ");
		hql.append(" and tributoAliquota.dataVigencia = ( ");
		hql.append(" select max(dataVigencia) from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" where ");
		hql.append(" dataVigencia < :dataAtual ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesTributos", chavesTributos);
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #obterTributoAliquotaPorTributo (java.lang.Long)
	 */
	@Override
	public TributoAliquota obterTributoAliquotaPorTributo(Map<String, Object> filtro) throws NegocioException {

		Long idTributo = (Long) filtro.get("idTributo");
		Long idMunicipio = (Long) filtro.get("idMunicipio");

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" aliquota ");
		hql.append(" inner join fetch aliquota.tributo tributo ");
		hql.append(" where ");
		hql.append(" tributo.chavePrimaria = :idTributo ");
		if(idMunicipio != null) {
			hql.append(" and aliquota.municipio.chavePrimaria = :idMunicipio ");
		}
		hql.append(" order by aliquota.chavePrimaria desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idTributo", idTributo);
		if(idMunicipio != null) {
			query.setLong("idMunicipio", idMunicipio);
		}

		query.setMaxResults(1);

		return (TributoAliquota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo. ControladorTributo
	 * #obterAliquotaVigente(java.lang .Long)
	 */
	@Override
	public TributoAliquota obterAliquotaVigentePorData(Long idTributo, Date data) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" aliquota ");
		hql.append(" inner join fetch aliquota.tributo ");
		hql.append(" where ");
		hql.append(" aliquota.tributo.chavePrimaria = :idTributo ");
		hql.append(" and aliquota.dataVigencia <= :dataAtual ");
		hql.append(" order by aliquota.dataVigencia desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idTributo", idTributo);
		query.setDate("dataAtual", data);

		query.setMaxResults(1);

		return (TributoAliquota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.ControladorTributo#verificaTabelasTributo(java.lang.Long, java.lang.String)
	 */
	@Override
	public Collection verificaTabelasTributo(Long chaveTributo, String classe) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(classe);
		hql.append(" where ");
		hql.append("tributo.chavePrimaria =:chaveTributo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveTributo", chaveTributo);

		return query.list();
	}

	/**
	 * Método que valida se a rubrica possui aliquota cadastrada, desde que tenha tributos associados.
	 * Caso não tenha tributos associados, ignora a busca por aliquotas.
	 * 
	 * @param chaveRubrica
	 *            the chave rubrica
	 * @return true, if successful
	 * @throws NegocioException
	 */
	@Override
	public boolean rubricaPossuiTributoAliquota(Long chaveRubrica) throws NegocioException {

		Rubrica rubrica = (Rubrica) ServiceLocator.getInstancia().getBeanPorID(Rubrica.BEAN_ID_RUBRICA);
		rubrica.setChavePrimaria(chaveRubrica);
		Collection<RubricaTributo> listaTributo = getControladorRubrica().obterListaRubricaTributo(chaveRubrica);
		if (!listaTributo.isEmpty()) {
			Collection<TributoAliquota> listaTributoAliquota = this.listarTributoAliquotaPorRubrica(rubrica, null);
			return !listaTributoAliquota.isEmpty();
		}
		return true;

	}
	
	/**
	 * Listar tributo aliquota por tarifa.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param chavesTributosIsentosPontoConsumo
	 *            the chaves tributos isentos ponto consumo
	 * @return the collection
	 */
	@Override
	@Cacheable(value="listaTributosAliquota")
	public Collection<TributoAliquota> listarTributoAliquotaPorTarifa(Collection<Long> chavesTributos, Long chaveTarifa,
					Collection<Long> chavesTributosIsentosPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select tributoAliquota from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" tributoAliquota ");
		hql.append(" inner join tributoAliquota.tributo tributo ");
		hql.append(" where tributo.chavePrimaria in ( ");
		hql.append("    select tributoTarifa.tributo.chavePrimaria from ");
		hql.append(getClasseEntidadeTarifaTributo().getSimpleName());
		hql.append("    tributoTarifa  ");
		hql.append("    inner join tributoTarifa.tarifaVigencia tarifaVigencia ");
		hql.append("    inner join tarifaVigencia.tarifa tarifa ");
		hql.append("    where tarifa.chavePrimaria = :idTarifa ");
		hql.append("    and  tributoTarifa.dataVigenciaInicial <= :dataAtual ");
		hql.append("    and  (tributoTarifa.dataVigenciaFinal is null or tributoTarifa.dataVigenciaFinal >= :dataAtual ) ");
		hql.append(" ) ");

		if(chavesTributos != null
						&& !chavesTributos.isEmpty()){
			hql.append(" and tributo.chavePrimaria in (:tributos) ");
		}

		if (chavesTributosIsentosPontoConsumo != null && chavesTributosIsentosPontoConsumo.size() > 0) {
			hql.append(" and tributo.chavePrimaria not in (:tributosIsentos) ");
		}

		hql.append(" and tributoAliquota.dataVigencia <= :dataAtual order by tributoAliquota.dataVigencia desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idTarifa", chaveTarifa);
		query.setDate("dataAtual", Calendar.getInstance().getTime());
		if (chavesTributosIsentosPontoConsumo != null && chavesTributosIsentosPontoConsumo.size() > 0) {
			query.setParameterList("tributosIsentos", chavesTributosIsentosPontoConsumo);
		}
		if(chavesTributos != null
						&& !chavesTributos.isEmpty()){
			query.setParameterList("tributos", chavesTributos);
		}

		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tributo.ControladorTributo#listarTributosAliquota()
	 */
	@Override
	public TributoAliquota obterTributoAliquotaPorDescricaoTributo(String descricao) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTributoAliquota().getSimpleName());
		hql.append(" tributoAliquota ");
		hql.append(" inner join fetch tributoAliquota.tributo tributo ");
		hql.append(" where tributo.habilitado = true ");
		hql.append(" and tributo.descricao = :descricao");
		hql.append(" order by tributoAliquota.dataVigencia desc ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setString("descricao", descricao);

		query.setMaxResults(1);

		return (TributoAliquota) query.uniqueResult();
	}
	
	public Tributo obterTributo(String descricao) throws GGASException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTributo().getSimpleName());
		hql.append(" t where t.descricao = 'ICMS' ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Tributo) query.uniqueResult();
	}


}

