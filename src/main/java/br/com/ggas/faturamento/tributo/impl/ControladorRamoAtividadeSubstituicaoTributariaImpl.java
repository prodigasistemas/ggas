/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCobrancaImpl representa uma ControladorCobrancaImpl no sistema.
 *
 * @since 22/02/2010
 *
 */
package br.com.ggas.faturamento.tributo.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorRamoAtividadeSubstituicaoTributariaImpl extends ControladorNegocioImpl
		implements ControladorRamoAtividadeSubstituicaoTributaria {

	/*
	 * **************** Métodos de Criação ****************
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (RamoAtividadeSubstituicaoTributaria) ServiceLocator.getInstancia()
				.getBeanPorID(RamoAtividadeSubstituicaoTributaria.BEAN_ID_SUBST_TRIBUTARIA);
	}

	/*
	 * **************** Métodos get Das Classes de Entidade ****************
	 */

	public Class<?> getClasseEntidadeRamoAtividadeSubstituicaoTributaria() {

		return ServiceLocator.getInstancia().getClassPorID(RamoAtividadeSubstituicaoTributaria.BEAN_ID_SUBST_TRIBUTARIA);
	}

	/*
	 * **************************************************************** Métodos de Tributo ********************************************
	 * ********************
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel. ControladorRamoAtividadeSubstituicaoTributaria # consultarRamoAtividadeSubstituicaoTributaria(
	 * java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RamoAtividadeSubstituicaoTributaria> consultarRamoAtividadeSubstituicaoTributaria(Map<String, Object> filtro)
			throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeRamoAtividadeSubstituicaoTributaria());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Date dataBase = (Date) filtro.get("dataBase");
			if (dataBase != null) {
				criteria.add(Restrictions.le("dataInicioVigencia", dataBase));
				criteria.add(Restrictions.ge("dataFimVigencia", dataBase));
			}

			EntidadeConteudo tipoSubstituicao = (EntidadeConteudo) filtro.get("tipoSubstituicao");
			if (tipoSubstituicao != null) {
				criteria.add(Restrictions.eq("tipoSubstituicao.chavePrimaria", tipoSubstituicao.getChavePrimaria()));
			}

			RamoAtividade ramoAtividade = (RamoAtividade) filtro.get("ramoAtividade");
			if (ramoAtividade != null) {
				criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", ramoAtividade.getChavePrimaria()));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel. ControladorRamoAtividadeSubstituicaoTributaria # obterRamoAtividadeSubstituicaoTributaria(java
	 * .lang.Long)
	 */
	@Override
	public RamoAtividadeSubstituicaoTributaria obterRamoAtividadeSubstituicaoTributaria(Long chavePrimaria) throws GGASException {

		return (RamoAtividadeSubstituicaoTributaria) this.obter(chavePrimaria, getClasseEntidade());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria#
	 * obterRamoAtividadeSubstTribPorRamoAtividade(br.com.ggas.cadastro.imovel.RamoAtividade, java.util.Date)
	 */
	@Override
	public RamoAtividadeSubstituicaoTributaria obterRamoAtividadeSubstTribPorRamoAtividade(RamoAtividade ramoAtividade, Date ultimaLeitura)
			throws NegocioException {

		RamoAtividadeSubstituicaoTributaria retorno = null;

		if (ramoAtividade != null) {
			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidadeRamoAtividadeSubstituicaoTributaria().getSimpleName());
			hql.append(" rast ");
			hql.append(" where ");
			hql.append(" rast.ramoAtividade.chavePrimaria = ? ");
			hql.append(" and ? between rast.dataInicioVigencia and rast.dataFimVigencia ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong(0, ramoAtividade.getChavePrimaria());

			Date dateQuery = Calendar.getInstance().getTime();

			if (ultimaLeitura != null) {

				Long chaveVigenciaSubstituicaoTributaria =
						getControladorParametroSistema().obterParametroVigenciaSubstituicaoTributaria().getValorLong();

				EntidadeConteudo entidadeConteudoParametro =
						getControladorEntidadeConteudo().obterEntidadeConteudo(chaveVigenciaSubstituicaoTributaria);

				Long valorLong = getControladorConstanteSistema().obterConstanteTipoVigenciaSubstituicaoTributariaDataUltimoDiaConsumo()
						.getValorLong();

				if (entidadeConteudoParametro.getChavePrimaria() == valorLong) {
					dateQuery = ultimaLeitura;
				}
			}

			query.setDate(1, dateQuery);

			query.setCacheable(true);
			query.setCacheMode(CacheMode.NORMAL);

			retorno = (RamoAtividadeSubstituicaoTributaria) query.uniqueResult();

		}

		return retorno;
	}

	/**
	 * Seleciona o RamoAtividadeSubstituicaoTributaria com data de inicio da vigência menor ou igual que a data leitura e data fim de
	 * vigência maior ou igual que a data de leitura.
	 * 
	 * @param listaRamoAtividadeSubstituicaoTributaria
	 * @param dataLeitura
	 * @return RamoAtividadeSubstituicaoTributaria em vigencia
	 */
	@Override
	public RamoAtividadeSubstituicaoTributaria escolherRamoAtividadeSubstituicaoTributariaEmVigencia(
			Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria, Date dataLeitura)
			throws NegocioException {

		RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoEmVigencia = null;
		Date dataVigencia = definirDataLeituraEntreInicioEfimVigencia(dataLeitura);

		if (listaRamoAtividadeSubstituicaoTributaria != null && !listaRamoAtividadeSubstituicaoTributaria.isEmpty()) {
			for (RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria : listaRamoAtividadeSubstituicaoTributaria) {
				Date dataInicio = ramoAtividadeSubstituicaoTributaria.getDataInicioVigencia();
				Date dataFim = ramoAtividadeSubstituicaoTributaria.getDataFimVigencia();

				Boolean maiorOuIgualQueDataInicio = DataUtil.menorOuIgualQue(dataInicio, dataVigencia);
				Boolean menorOuIgualQueDataFim = DataUtil.menorOuIgualQue(dataVigencia, dataFim);

				if (maiorOuIgualQueDataInicio && menorOuIgualQueDataFim) {
					ramoAtividadeSubstituicaoEmVigencia = ramoAtividadeSubstituicaoTributaria;
					break;
				}
			}
		}

		return ramoAtividadeSubstituicaoEmVigencia;
	}

	/**
	 * Define a data de leitura entre inicio e fim de vigencia de um RamoAtividadeSubstituicaoTributaria.
	 * 
	 * @param dataLeitura
	 * @return data entre inicio e fim de vigencia
	 * @throws NegocioException
	 */
	private Date definirDataLeituraEntreInicioEfimVigencia(Date dataLeitura) throws NegocioException {
		Date dataEntreInicioFim = Calendar.getInstance().getTime();

		if (dataEntreInicioFim != null) {

			Long chaveVigenciaSubstituicaoTributaria =
					getControladorParametroSistema().obterParametroVigenciaSubstituicaoTributaria().getValorLong();

			EntidadeConteudo entidadeConteudoParametro =
					getControladorEntidadeConteudo().obterEntidadeConteudo(chaveVigenciaSubstituicaoTributaria);

			Long valorLong =
					getControladorConstanteSistema().obterConstanteTipoVigenciaSubstituicaoTributariaDataUltimoDiaConsumo().getValorLong();

			if (entidadeConteudoParametro.getChavePrimaria() == valorLong) {
				dataEntreInicioFim = dataLeitura;
			}
		}
		return dataEntreInicioFim;
	}

	/**
	 * Obtém uma coleção de RamoAtividadeSubstituicaoTributaria por RamoAtividade.
	 * 
	 * @param ramosDeAtividade
	 * @return mapa de RamoAtividadeSubstituicaoTributaria por RamoAtividade
	 */
	@Override
	public Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> obterRamoAtividadeSubstituicaoTributariaPorRamoAtividade(
			Collection<RamoAtividade> ramosDeAtividade) {

		Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> mapaRamoAtividadeSubstituicaoTributaria = new HashMap<>();
		if (ramosDeAtividade != null && !ramosDeAtividade.isEmpty()) {
			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(ramosDeAtividade);

			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" select ramoAtividade, rast from ");
			hql.append(getClasseEntidadeRamoAtividadeSubstituicaoTributaria().getSimpleName());
			hql.append(" as rast ");
			hql.append(" left join rast.ramoAtividade ramoAtividade ");
			hql.append(" where ");
			hql.append(" ramoAtividade.chavePrimaria IN (:chavesPrimarias) ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameterList("chavesPrimarias", chavesPrimarias);

			mapaRamoAtividadeSubstituicaoTributaria.putAll(this.construirMapaRamoAtividadeSubstTributaria(query.list()));
		}

		return mapaRamoAtividadeSubstituicaoTributaria;
	}

	/**
	 * Constrói um mapa de RamoAtividadeSubstituicaoTributaria por RamoAtividade.
	 * 
	 * @param listaAtributos
	 * @return mapa de RamoAtividadeSubstituicaoTributaria por RamoAtividade
	 */
	private Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> construirMapaRamoAtividadeSubstTributaria(
			List<Object[]> listaAtributos) {

		Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> mapaRamoAtividadeSubstituicaoTributaria = new HashMap<>();
		Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria = null;
		for (Object[] atributos : listaAtributos) {
			RamoAtividade ramoAtividade = (RamoAtividade) atributos[0];
			RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria = (RamoAtividadeSubstituicaoTributaria) atributos[1];
			if (!mapaRamoAtividadeSubstituicaoTributaria.containsKey(ramoAtividade)) {
				listaRamoAtividadeSubstituicaoTributaria = new ArrayList<>();
				mapaRamoAtividadeSubstituicaoTributaria.put(ramoAtividade, listaRamoAtividadeSubstituicaoTributaria);
			} else {
				listaRamoAtividadeSubstituicaoTributaria = mapaRamoAtividadeSubstituicaoTributaria.get(ramoAtividade);
			}
			listaRamoAtividadeSubstituicaoTributaria.add(ramoAtividadeSubstituicaoTributaria);
		}
		return mapaRamoAtividadeSubstituicaoTributaria;
	}

	/**
	 * Gets the controlador entidade conteudo.
	 *
	 * @return the controlador entidade conteudo
	 */
	private ControladorEntidadeConteudo getControladorEntidadeConteudo() {

		return ServiceLocator.getInstancia().getControladorEntidadeConteudo();
	}

	/**
	 * Gets the controlador constante sistema.
	 *
	 * @return the controlador constante sistema
	 */
	private ControladorConstanteSistema getControladorConstanteSistema() {

		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	/**
	 * Gets the controlador parametro sistema.
	 *
	 * @return the controlador parametro sistema
	 */
	private ControladorParametroSistema getControladorParametroSistema() {

		return ServiceLocator.getInstancia().getControladorParametroSistema();
	}

	/*
	 * **************** Métodos não implementados ****************
	 */

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel. ControladorRamoAtividadeSubstituicaoTributaria #
	 * listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade ()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Map<RamoAtividade, RamoAtividadeSubstituicaoTributaria> listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade() {

		Map<RamoAtividade, RamoAtividadeSubstituicaoTributaria> retorno = new HashMap<RamoAtividade, RamoAtividadeSubstituicaoTributaria>();

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeRamoAtividadeSubstituicaoTributaria().getSimpleName());
		hql.append(" rast ");
		hql.append(" where ");
		hql.append(" ? between rast.dataInicioVigencia and rast.dataFimVigencia ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate(0, Calendar.getInstance().getTime());

		Collection<RamoAtividadeSubstituicaoTributaria> listaRamos = query.list();

		if (listaRamos != null) {
			for (RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria : listaRamos) {
				retorno.put(ramoAtividadeSubstituicaoTributaria.getRamoAtividade(), ramoAtividadeSubstituicaoTributaria);
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria#listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade
	 * (br.com.ggas.cadastro.imovel.RamoAtividade)
	 */
	@Override
	public Collection<RamoAtividadeSubstituicaoTributaria> listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(
			RamoAtividade ramoAtividade) throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeRamoAtividadeSubstituicaoTributaria());

		Long idRamoAtividade = ramoAtividade.getChavePrimaria();
		criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
		criteria.createCriteria("tipoSubstituicao", "tipoSubstituicao", Criteria.INNER_JOIN);

		return criteria.list();
	}
}
