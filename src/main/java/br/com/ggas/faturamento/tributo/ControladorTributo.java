/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tributo;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados
 * ao controlador de tributos
 * 
 * 
 */
public interface ControladorTributo extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_TRIBUTO = "controladorTributo";

	String ERRO_NEGOCIO_REMOVER_TRIBUTO = "ERRO_NEGOCIO_REMOVER_TRIBUTO";

	String ERRO_NEGOCIO_REMOVER_TRIBUTO_ALIQUOTA = "ERRO_NEGOCIO_REMOVER_TRIBUTO_ALIQUOTA";

	/**
	 * Criar tributo aliquota.
	 * 
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeNegocio criarTributoAliquota() throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * Tributos de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de Tributos.
	 * @return Uma coleção de Tributos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Tributo> consultarTributos(Map<String, Object> filtro) throws GGASException;

	/**
	 * Método responsável por listar as Tributos.
	 * 
	 * @return Uma coleção de Tributos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Tributo> listarTributos() throws GGASException;

	/**
	 * Método responsável por obter um Tributo
	 * pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Um Tributo
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Tributo obterTributo(Long chavePrimaria) throws GGASException;

	/**
	 * Método responsável por consultar as
	 * Aliquotas de Tributos de acordo com o
	 * filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de Aliquotas de
	 *            Tributos.
	 * @return Uma coleção de Aliquotas de
	 *         Tributos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TributoAliquota> consultarTributosAliquota(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as Aliquotas
	 * de Tributos.
	 * 
	 * @return Uma coleção de Aliquotas de
	 *         Tributos.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TributoAliquota> listarTributosAliquota() throws GGASException;

	/**
	 * Método responsável por obter uma Aliquota
	 * de Tributo pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma Aliquota de Tributo
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TributoAliquota obterTributoAliquota(Long chavePrimaria) throws GGASException;

	/**
	 * Método responsável por consultar os
	 * TributoAliquota vigenentes de uma coleção
	 * de TarifaTributos.
	 * 
	 * @param colecaoTributo
	 *            a coleção de TarifaTributos
	 * @return coleção de TributoAliquota dos
	 *         TarifaTributos passados
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<TributoAliquota> consultarTributosAliquotaPorTarifaTributos(Collection<TarifaVigenciaTributo> colecaoTributo) throws GGASException;

	/**
	 * Método responsável por obter a aliquota
	 * vigente do tributo.
	 * 
	 * @param idTributo
	 *            Chave primária do tributo.
	 * @return Um TributoAliquota
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TributoAliquota obterAliquotaVigente(Long idTributo) throws NegocioException;

	/**
	 * Método responsável por listar as aliquotas
	 * de um ponto de consumo.
	 * 
	 * @param pontoConsumo
	 *            Ponto de consumo.
	 * @param isIsento
	 *            Indicador para buscar aliquotas
	 *            vigentes ou não.
	 * @param isDrawback
	 *            the is drawback
	 * @return Uma coleção de TributoAliquota.
	 */
	Collection<PontoConsumoTributoAliquota> listarTributoAliquotaPorPontoConsumo(PontoConsumo pontoConsumo, boolean isIsento,
					boolean isDrawback);

	/**
	 * Método responsável por listar as aliquotas
	 * de uma rubrica.
	 * 
	 * @param rubrica
	 *            the rubrica
	 * @param chavesTributosIsentosPontoConsumo
	 *            the chaves tributos isentos ponto consumo
	 * @return the collection
	 */
	Collection<TributoAliquota> listarTributoAliquotaPorRubrica(Rubrica rubrica, Long[] chavesTributosIsentosPontoConsumo);

	/**
	 * Método responsável por listar as alíquotas
	 * vigentes dos tributos.
	 * 
	 * @param chavesTributos
	 *            Chaves primárias dos tributos.
	 * @return Uma coleção de TributoAliquota.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TributoAliquota> listaTributoAliquotaVigente(Long[] chavesTributos) throws NegocioException;

	/**
	 * Método para obter a TributoAliquota a
	 * partir do tributo.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the tributo aliquota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	TributoAliquota obterTributoAliquotaPorTributo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter a aliquota
	 * vigente do tributo por data.
	 * 
	 * @param idTributo
	 *            Chave primária do tributo.
	 * @param data  the data.
	 * @return Um TributoAliquota
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TributoAliquota obterAliquotaVigentePorData(Long idTributo, Date data) throws NegocioException;

	/**
	 * Verifica tabelas tributo.
	 * 
	 * @param chaveTributo
	 *            the chave tributo
	 * @param classe
	 *            the classe
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("rawtypes")
	Collection verificaTabelasTributo(Long chaveTributo, String classe) throws NegocioException;

	/**
	 * Criar tributo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTributo();

	/**
	 * Rubrica possui tributo aliquota.
	 * 
	 * @param chaveRubrica
	 *            the chave rubrica
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean rubricaPossuiTributoAliquota(Long chaveRubrica) throws NegocioException;

	/**
	 * Consulta as entidades PontoConsumoTributoAliquota por pontos de consumo
	 * onde a data de inicio da vigência é menor ou igual que a data atual e
	 * com data de fim da vigência com valor nulo ou maior ou igual que a data atual.
	 * Para a entidade PontoConsumoTributoAliquota, obtém apenas os seguintes atributos:
	 * {@code chavePrimaria}, {@code valorDrawback}. Para a entidade PontoConsumo,
	 * obtém os seguintes atributos: {@code chavePrimaria}.
	 * @param chavesPontosConsumo the chaves Pontos Consumo
	 * @param isIsento the is Isento
	 * @param isDrawback the is Drawback
	 * @return mapa de entidades PontoConsumoTributoAliquota por PontoConsumo
	 */
	Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> listarTributoAliquotaPorPontoConsumo(Long[] chavesPontosConsumo,
					boolean isIsento, boolean isDrawback);
	
	/**
	 * Consulta um destaqueIndicadorNota a partir de uma PontoConsumoTributoAliquota
	 * 
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param isIsento {@link boolean}
	 * @param isDrawback {@link boolean}
	 * @return {@link Collection}
	 */
	Collection<Boolean> verificarDestaqueIndicadorNotaDeTributoAliquotaPorPontoConsumo(PontoConsumo pontoConsumo,
			boolean isIsento, boolean isDrawback);

	/**
	 * Obtém as chaves primárisa de {@link TributoAliquota} por ponto de consumo.
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @param isIsento - boolean
	 * @param isDrawback - boolean
	 * @return coleção de chaves - {@link Collection}
	 */
	Collection<Long> obterChavesTributoAliquotaPorPontoConsumo(PontoConsumo pontoConsumo, boolean isIsento,
			boolean isDrawback);

	/**
	 * Obtém a porcentagem da aliquota por ponto de consumo.
	 * 
	 * @param pontoConsumo - {@link PontoConsumo}
	 * @param isIsento - boolean
	 * @param isDrawback - boolean
	 * @return coleção de ponto de consumo
	 */
	Collection<PontoConsumoTributoAliquota> obterPorcentagemAliquotaPorPontoConsumo(PontoConsumo pontoConsumo,
			boolean isIsento, boolean isDrawback);

	/**
	 * Obtém uma lista de {@link TributoAliquota} por tarifa.
	 * @param chavesTributos - {@link Collection}
	 * @param chaveTarifa - {@link Long}
	 * @param chavesTributosIsentosPontoConsumo - {@link Long[]}
	 * @return coleção de {@link TributoAliquota}
	 */
	Collection<TributoAliquota> listarTributoAliquotaPorTarifa(Collection<Long> chavesTributos, Long chaveTarifa,
			Collection<Long> chavesTributosIsentosPontoConsumo);

	TributoAliquota obterTributoAliquotaPorDescricaoTributo(String descricao);

	/**
	 * Método responsável por obter um Tributo
	 * pela Chave Primária.
	 * 
	 * @param descricao
	 *            nome do tributo
	 * @return Um Tributo
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Tributo obterTributo(String descricao) throws GGASException;
}
