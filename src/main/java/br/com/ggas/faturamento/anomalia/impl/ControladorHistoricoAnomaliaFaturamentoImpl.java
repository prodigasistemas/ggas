/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.anomalia.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import br.com.ggas.util.*;
import br.com.ggas.web.faturamento.leitura.AnaliseExcecaoFaturaAction;
import org.apache.commons.lang.StringUtils;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.anomalia.ControladorHistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;

/**
 * Classe responsável pela implementação dos métodos relacionados ao Controlador do Histórico de Anomalia de Faturamento.
 *
 */
public class ControladorHistoricoAnomaliaFaturamentoImpl extends ControladorNegocioImpl implements ControladorHistoricoAnomaliaFaturamento {

	private static final String CONSTANTE_NAO = "Não";

	private static final String CONSTANTE_SIM = "Sim";

	private static final int ESCALA = 4;

	private static final String DATA_GERACAO_FINAL = "dataGeracaoFinal";

	private static final String DATA_GERACAO_INICIAL = "dataGeracaoInicial";

	private static final String REFERENCIA = "referencia";

	private static final String ERRO_PESQUISA_DATA_FINAL_MAIOR_GERACAO = "ERRO_PESQUISA_DATA_FINAL_MAIOR_GERACAO";

	private static final String ERRO_PESQUISA_DATA_INICIAL_MAIOR_ATUAL_GERACAO = "ERRO_PESQUISA_DATA_INICIAL_MAIOR_ATUAL_GERACAO";

	private static final String ERRO_PESQUISA_DATA_FINAL_MAIOR_ATUAL_GERACAO = "ERRO_PESQUISA_DATA_FINAL_MAIOR_ATUAL_GERACAO";

	private static final String ERRO_ANOMALIA_JA_ANALISADA = "ERRO_ANOMALIA_JA_ANALISADA";

	private static final String ERRO_ANOMALIA_ANALISADA = "ERRO_ANOMALIA_ANALISADA";

	private static final String ERRO_ANO_MES_REFERENCIA_SUPERIOR_AO_SISTEMA = "ERRO_ANO_MES_REFERENCIA_SUPERIOR_AO_SISTEMA";

	private static final String ATRIBUTO_HABILITADO = "habilitado";
	public static final int DIVISOR_PORCENTAGEM = 100;

	private ControladorParametroSistema controladorParametroSistema;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (HistoricoAnomaliaFaturamento) ServiceLocator.getInstancia().getBeanPorID(
						HistoricoAnomaliaFaturamento.BEAN_ID_HISTORICO_ANOMALIA_FATURAMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoAnomaliaFaturamento.BEAN_ID_HISTORICO_ANOMALIA_FATURAMENTO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	public Class<?> getClasseEntidadePeriodicidade() {

		return ServiceLocator.getInstancia().getClassPorID(Periodicidade.BEAN_ID_PERIODICIDADE);
	}

	@Override
	public Pair<List<HistoricoAnomaliaFaturamento>, Long> consultarHistoricoAnomaliaFaturamentoPaginada(Map<String, Object> filtro) {
		Query query = montarConsultaHistoricoAnormalidade(filtro);
		int maxResults = (int) filtro.get(AnaliseExcecaoFaturaAction.MAX_RESULTS);
		int offset = (int) filtro.get(AnaliseExcecaoFaturaAction.OFFSET);

		List<HistoricoAnomaliaFaturamento> resultados = new ArrayList<>();
		ScrollableResults rs = query.scroll(ScrollMode.SCROLL_INSENSITIVE);
		rs.beforeFirst();
		if (offset > 0) {
			rs.scroll( offset );
		}
		for (int i = 0; i < maxResults && rs.next(); i++) {
			Object[] results = rs.get();
			if (results.length == 1) {
				HistoricoAnomaliaFaturamento item = (HistoricoAnomaliaFaturamento) results[0];
				resultados.add(item);
			}
		}
		rs.last();
		final long total = rs.getRowNumber();
		return new Pair<>(resultados, total);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.medicao.consumo.
	 * ControladorHistoricoConsumo#
	 * consultarHistoricoConsumoAnaliseExcecoesLeitura
	 * (java.util.Map)
	 */
	@Override
	public Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaliaFaturamento(Map<String, Object> filtro) {
		Query query = montarConsultaHistoricoAnormalidade(filtro);
		return query.list();
	}

	@SuppressWarnings("squid:S1192")
	private Query montarConsultaHistoricoAnormalidade(Map<String, Object> filtro) {
		Long idTipoConsumo = (Long) filtro.get("idTipoConsumo");
		BigDecimal consumoLido = (BigDecimal) filtro.get("consumoLido");
		BigDecimal percentualVariacaoConsumo = (BigDecimal) filtro.get("percentualVariacaoConsumo");

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct(historico) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		
			hql.append(", ");
			hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
			hql.append(" historicoConsumo ");
			
		hql.append(" inner join fetch historico.pontoConsumo ponto");
		hql.append(" inner join fetch ponto.imovel imovel");
		hql.append(" left join fetch historico.analisador analisador");
		hql.append(" left join fetch analisador.funcionario funcionario");
		hql.append(" inner join fetch historico.faturamentoAnormalidade anorFatura ");
		hql.append(" left join fetch imovel.imovelCondominio imovelCondominio ");
		hql.append(" left join fetch historico.fatura fatura ");
		hql.append(" left join fetch fatura.listaFaturaItem listaFaturaItem ");
		hql.append(" left join fetch fatura.historicoConsumo historicoConsumoFatura ");
		//hql.append(" left join fetch historicoConsumoFatura.historicoAtual historicoAtual ");
		hql.append(" where ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and ponto = historicoConsumo.pontoConsumo ");
		
		if (((idTipoConsumo != null) && (idTipoConsumo > 0)) || (consumoLido != null) || (percentualVariacaoConsumo != null)) {
			//hql.append(" and ponto = historicoConsumo.pontoConsumo ");
			hql.append(" and historico.anoMesFaturamento = historicoConsumo.anoMesFaturamento ");
			hql.append(" and historico.numeroCiclo = historicoConsumo.numeroCiclo ");
			hql.append(" and historicoConsumo.indicadorConsumoCiclo = true ");
		}

		Long idCliente = (Long) filtro.get("idCliente");
		if ((idCliente != null) && (idCliente > 0)) {
			hql.append(" and historico.cliente.chavePrimaria= :idCliente ");
		}

		String cpfCliente = (String) filtro.get(AnaliseExcecaoFaturaAction.CPF_CLIENTE);
		if (!StringUtils.isEmpty(cpfCliente)) {
			hql.append(" and historico.cliente.cpf = :cpfCliente ");
		}
		String nomeCliente = (String) filtro.get(AnaliseExcecaoFaturaAction.NOME_CLIENTE);
		if (!StringUtils.isEmpty(nomeCliente)) {
			hql.append(" and historico.cliente.nome like :nomeCliente ");
		}
		String rgCliente = (String) filtro.get(AnaliseExcecaoFaturaAction.RG_CLIENTE);
		if (!StringUtils.isEmpty(rgCliente)) {
			hql.append(" and historico.cliente.rg = :rgCliente ");
		}
		String passaporteCliente = (String) filtro.get(AnaliseExcecaoFaturaAction.NUMERO_PASSAPORTE);
		if (!StringUtils.isEmpty(passaporteCliente)) {
			hql.append(" and historico.cliente.numeroPassaporte = :passaporteCliente ");
		}
		String cnpjCliente = (String) filtro.get(AnaliseExcecaoFaturaAction.CNPJ_CLIENTE);
		if (!StringUtils.isEmpty(cnpjCliente)) {
			hql.append(" and historico.cliente.cnpj = :cnpjCliente ");
		}
		String nomeFantasiaCliente = (String) filtro.get(AnaliseExcecaoFaturaAction.NOME_FANTASIA_CLIENTE);
		if (!StringUtils.isEmpty(nomeFantasiaCliente)) {
			hql.append(" and historico.cliente.nomeFantasia like :nomeFantasiaCliente ");
		}

		String cepImovel = (String) filtro.get("cepImovel");
		if (!StringUtils.isEmpty(cepImovel)) {
			hql.append(" and ponto.cep.cep = :cepImovel ");
		}

		Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
		if (indicadorCondominio != null) {
			hql.append(" and imovel.condominio = :indicadorCondominio ");
		}

		Long matriculaImovel = (Long) filtro.get("matriculaImovel");
		if ((matriculaImovel != null) && (matriculaImovel > 0)) {
			hql.append(" and imovel.chavePrimaria = :matriculaImovel ");
		}

		Long matriculaCondominio = (Long) filtro.get("matriculaCondominio");
		if ((matriculaCondominio != null) && (matriculaCondominio > 0)) {
			hql.append(" and imovelCondominio.chavePrimaria = :matriculaCondominio ");
		}

		String complementoImovel = (String) filtro.get("complementoImovel");
		if (!StringUtils.isEmpty(complementoImovel)) {
			hql.append(" and upper(imovel.descricaoComplemento) like upper(:complementoImovel) ");
		}

		String nomeImovel = (String) filtro.get("nomeImovel");
		if (!StringUtils.isEmpty(nomeImovel)) {
			hql.append(" and upper(imovel.nome) like upper(:nomeImovel) ");
		}

		String numeroImovel = (String) filtro.get("numeroImovel");
		if (!StringUtils.isEmpty(numeroImovel)) {
			hql.append(" and imovel.numeroImovel like :numeroImovel ");
		}

		Long idLocalidade = (Long) filtro.get("idLocalidade");
		if (idLocalidade != null && idLocalidade > 0) {
			hql.append(" and imovel.quadraFace.quadra.setorComercial.localidade.chavePrimaria = :idLocalidade ");
		}

		Long idSetorComercial = (Long) filtro.get("idSetorComercial");
		if (idSetorComercial != null && idSetorComercial > 0) {
			hql.append(" and imovel.quadraFace.quadra.setorComercial.chavePrimaria = :idSetorComercial ");
		}

		Long idGrupoFaturamento = (Long) filtro.get("idGrupoFaturamento");
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			hql.append(" and ponto.rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
		}

		Long idSegmento = (Long) filtro.get("idSegmento");
		if (idSegmento != null && idSegmento > 0) {
			hql.append(" and ponto.segmento.chavePrimaria = :idSegmento ");
		}

		Long idSituacaoPontoConsumo = (Long) filtro.get("idSituacaoPontoConsumo");
		if (idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > 0) {
			hql.append(" and ponto.situacaoConsumo.chavePrimaria = :idSituacaoPontoConsumo ");
		}

		Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			hql.append(" and ponto.chavePrimaria = :idPontoConsumo ");
		}

		// anormalidade faturamento
		Long idAnormalidadeFaturamento = (Long) filtro.get("idAnormalidadeFaturamento");
		if (idAnormalidadeFaturamento != null && idAnormalidadeFaturamento > 0) {
			hql.append(" and anorFatura.chavePrimaria in(:idAnormalidadeFaturamento) ");
		}

		Long[] idsRota = (Long[]) filtro.get("idsRota");
		if (idsRota != null && idsRota.length > 0) {
			hql.append(" and ponto.rota.chavePrimaria in (:idsRota) ");
		}

		if ((idTipoConsumo != null) && (idTipoConsumo > 0)) {
			hql.append(" and historicoConsumo.tipoConsumo.chavePrimaria = :idTipoConsumo ");
		}

		if (consumoLido != null) {
			hql.append(" and historicoConsumo.consumo >= :consumoLido ");
		}

		if (percentualVariacaoConsumo != null) {
			hql.append(" and historicoConsumo.consumoApuradoMedio is not null ");
			hql.append(" and historicoConsumo.habilitado = true ");
			hql.append(" and historicoConsumo.consumoApuradoMedio > 0 ");
			hql.append(" and (historicoConsumo.consumo - historicoConsumo.consumoApuradoMedio)*100/historicoConsumo.consumoApuradoMedio ");
			hql.append("<= :percentualVariacaoConsumo ");
		}

		Boolean analisada = (Boolean) filtro.get("analisada");
		if (analisada != null) {
			hql.append(" and historico.analisada = :analisada ");
			
			if(!analisada) {
				hql.append(" and historicoConsumo.habilitado = true ");
				hql.append(" and historicoConsumo.indicadorFaturamento = false ");
			}
		}

		Boolean indicadorImpedeFaturamento = (Boolean) filtro.get("indicadorImpedeFaturamento");
		if (indicadorImpedeFaturamento != null) {
			hql.append(" and anorFatura.indicadorImpedeFaturamento = :indicadorImpedeFaturamento ");
		}

		BigDecimal percentualVariacaoValorFatura = (BigDecimal) filtro.get("percentualVariacaoValorFatura");
		if (percentualVariacaoValorFatura != null) {
			hql.append(" and historico.fatura is not null ");
			hql.append(" and ");
			hql.append(" ((fatura.valorTotal / historicoConsumoFatura.diasConsumo) <= ");
			hql.append(" ((SELECT sum(fatu2.valorTotal) / sum(cohi2.diasConsumo) ");
			hql.append("	FROM ");
			hql.append(getClasseEntidadeFatura().getSimpleName());
			hql.append(" fatu2 ");
			hql.append("	INNER JOIN fatu2.historicoConsumo cohi2 ");
			hql.append("	WHERE fatura.chavePrimaria <> fatu2.chavePrimaria ");
			hql.append("	AND fatura.pontoConsumo.chavePrimaria = fatu2.pontoConsumo.chavePrimaria ");
			hql.append("	AND fatura.cliente.chavePrimaria = fatu2.cliente.chavePrimaria ");
			hql.append("	AND (fatura.dataEmissao - ");
			hql.append("		(SELECT segm.numeroCiclos * peri.quantidadeDias ");
			hql.append("			FROM ");
			hql.append(getClasseEntidadeSegmento().getSimpleName());
			hql.append(" segm ");
			hql.append("			INNER JOIN segm.periodicidade peri ");
			hql.append("			WHERE fatura.segmento.chavePrimaria = segm.chavePrimaria) ");
			hql.append("	< fatu2.dataEmissao) ");
			hql.append(" ) * (1-:percentualVariacaoValorFatura)) ");
			hql.append(" or ((fatura.valorTotal / historicoConsumoFatura.diasConsumo) >=  ");
			hql.append(" ((SELECT sum(fatu2.valorTotal) / sum(cohi2.diasConsumo) ");
			hql.append("	FROM ");
			hql.append(getClasseEntidadeFatura().getSimpleName());
			hql.append(" fatu2 ");
			hql.append("	INNER JOIN fatu2.historicoConsumo cohi2 ");
			hql.append("	WHERE fatura.chavePrimaria <> fatu2.chavePrimaria ");
			hql.append("	AND fatura.pontoConsumo.chavePrimaria = fatu2.pontoConsumo.chavePrimaria ");
			hql.append("	AND fatura.cliente.chavePrimaria = fatu2.cliente.chavePrimaria ");
			hql.append("	AND (fatura.dataEmissao - ");
			hql.append("		(SELECT segm.numeroCiclos * peri.quantidadeDias ");
			hql.append("			FROM ");
			hql.append(getClasseEntidadeSegmento().getSimpleName());
			hql.append(" segm ");
			hql.append("			INNER JOIN segm.periodicidade peri ");
			hql.append("			WHERE fatura.segmento.chavePrimaria = segm.chavePrimaria) ");
			hql.append("	< fatu2.dataEmissao) ");
			hql.append(" ) * (1+:percentualVariacaoValorFatura)))) ");

		}

		Date dataGeracaoInicial = (Date) filtro.get(DATA_GERACAO_INICIAL);
		Date dataGeracaoFinal = (Date) filtro.get(DATA_GERACAO_FINAL);
		if (dataGeracaoInicial != null) {
			hql.append("and historico.dataGeracao >= :dataGeracaoInicial ");
		}
		if (dataGeracaoFinal != null) {
			hql.append("and historico.dataGeracao <= :dataGeracaoFinal ");
		}

		Date dataMedicaoInicial = (Date) filtro.get("dataMedicaoInicial");
		Date dataMedicaoFinal = (Date) filtro.get("dataMedicaoFinal");		
		
		if (dataMedicaoInicial != null) {
			hql.append("and historicoConsumo.historicoAtual.dataLeituraAnterior >=  :dataMedicaoInicial ");
		}
		if (dataMedicaoFinal != null) {
			hql.append("and historicoConsumo.historicoAtual.dataLeituraInformada <=  :dataMedicaoFinal ");
		}

		String referencia = (String) filtro.get(REFERENCIA);
		if (!StringUtils.isEmpty(referencia)) {
			hql.append(" and historico.anoMesFaturamento = :referencia ");
		}

		String ciclo = (String) filtro.get("ciclo");
		if (!StringUtils.isEmpty(ciclo)) {
			hql.append(" and historico.numeroCiclo = :ciclo ");
		}

		String pontoConsumoLegado = (String) filtro.get("pontoConsumoLegado");
		if (!StringUtils.isEmpty(pontoConsumoLegado)) {
			hql.append(" and (ponto.codigoLegado like :codigoLegado ");
			hql.append(" or ponto.chavePrimaria like :codigoLegado) ");
		}

		Boolean ordenadaDecrescente = (Boolean) filtro.get("ordenadaDecrescente");
		
		if(ordenadaDecrescente != null && ordenadaDecrescente) {
			hql.append(" order by historico.chavePrimaria desc");
		} else {
			hql.append(" order by historico.chavePrimaria, historico.dataGeracao ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((idCliente != null) && (idCliente > 0)) {
			query.setLong("idCliente", idCliente);
		}
		if (!StringUtils.isEmpty(cpfCliente)) {
			query.setString("cpfCliente", cpfCliente);
		}
		if (!StringUtils.isEmpty(nomeCliente)) {
			query.setString("nomeCliente", "%"+nomeCliente+"%");
		}
		if (!StringUtils.isEmpty(rgCliente)) {
			query.setString("rgCliente", rgCliente);
		}
		if (!StringUtils.isEmpty(passaporteCliente)) {
			query.setString("passaporteCliente", passaporteCliente);
		}
		if (!StringUtils.isEmpty(cnpjCliente)) {
			query.setString("cnpjCliente", cnpjCliente);
		}
		if (!StringUtils.isEmpty(nomeFantasiaCliente)) {
			query.setString("nomeFantasiaCliente","%"+ nomeFantasiaCliente+"%");
		}

		if (!StringUtils.isEmpty(cepImovel)) {
			query.setString("cepImovel", cepImovel);
		}
		if (indicadorCondominio != null) {
			query.setBoolean("indicadorCondominio", indicadorCondominio);
		}
		if ((matriculaImovel != null) && (matriculaImovel > 0)) {
			query.setLong("matriculaImovel", matriculaImovel);
		}
		if ((matriculaCondominio != null) && (matriculaCondominio > 0)) {
			query.setLong("matriculaCondominio", matriculaCondominio);
		}
		if (!StringUtils.isEmpty(complementoImovel)) {
			query.setString("complementoImovel", Util.formatarTextoConsulta(complementoImovel));
		}
		if (!StringUtils.isEmpty(nomeImovel)) {
			query.setString("nomeImovel", Util.formatarTextoConsulta(nomeImovel));
		}
		if (!StringUtils.isEmpty(numeroImovel)) {
			query.setString("numeroImovel", numeroImovel);
		}
		if (idSetorComercial != null && idSetorComercial > 0) {
			query.setLong("idSetorComercial", idSetorComercial);
		}
		if (idLocalidade != null && idLocalidade > 0) {
			query.setLong("idLocalidade", idLocalidade);
		}
		if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
			query.setLong("idGrupoFaturamento", idGrupoFaturamento);
		}
		if (idSegmento != null && idSegmento > 0) {
			query.setLong("idSegmento", idSegmento);
		}
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}
		if (idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > 0) {
			query.setLong("idSituacaoPontoConsumo", idSituacaoPontoConsumo);
		}

		if (idAnormalidadeFaturamento != null && idAnormalidadeFaturamento > 0) {
			query.setLong("idAnormalidadeFaturamento", idAnormalidadeFaturamento);
		}
		if (idsRota != null && idsRota.length > 0) {
			query.setParameterList("idsRota", idsRota);
		}
		if (consumoLido != null) {
			query.setBigDecimal("consumoLido", consumoLido);
		}
		if ((idTipoConsumo != null) && (idTipoConsumo > 0)) {
			query.setLong("idTipoConsumo", idTipoConsumo);
		}
		if (percentualVariacaoConsumo != null) {
			query.setBigDecimal("percentualVariacaoConsumo", percentualVariacaoConsumo);
		}

		if (analisada != null) {
			query.setBoolean("analisada", analisada);
		}

		if (percentualVariacaoValorFatura != null) {
			query.setBigDecimal("percentualVariacaoValorFatura",
							percentualVariacaoValorFatura.divide(new BigDecimal(DIVISOR_PORCENTAGEM), ESCALA, RoundingMode.HALF_UP));
		}

		if (dataGeracaoInicial != null) {
			query.setParameter("dataGeracaoInicial", dataGeracaoInicial);
		}
		if (dataGeracaoFinal != null) {
			query.setParameter("dataGeracaoFinal", dataGeracaoFinal);
		}
		if (dataMedicaoInicial != null) {
			query.setParameter("dataMedicaoInicial", dataMedicaoInicial);
		}
		if (dataMedicaoFinal != null) {
			query.setParameter("dataMedicaoFinal", dataMedicaoFinal);
		}

		if (analisada != null) {
			query.setBoolean("analisada", analisada);
		}

		if (!StringUtils.isEmpty(referencia)) {
			query.setInteger(REFERENCIA, Integer.parseInt(referencia));
		}

		if (!StringUtils.isEmpty(ciclo)) {
			query.setInteger("ciclo", Integer.parseInt(ciclo));
		}

		if (!StringUtils.isEmpty(pontoConsumoLegado)) {
			query.setString("codigoLegado", pontoConsumoLegado);
		}

		if (indicadorImpedeFaturamento != null) {
			query.setBoolean("indicadorImpedeFaturamento", indicadorImpedeFaturamento);
		}
		return query;
	}

	/**
	 * @return the controladorParametroSistema
	 */
	public ControladorParametroSistema getControladorParametroSistema() {

		return controladorParametroSistema;
	}

	/**
	 * @param controladorParametroSistema
	 *            the controladorParametroSistema
	 *            to set
	 */

	public void setControladorParametroSistema(ControladorParametroSistema controladorParametroSistema) {

		this.controladorParametroSistema = controladorParametroSistema;
	}

	/**
	 * Método responsavel por verificar os
	 * parametros obrigatorios passados na
	 * pesquisa de recebimentos.
	 * 
	 * @param filtro
	 *            O filtro da tela de pesquisa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public void validarFiltroPesquisaHistoricoAnomaliaFaturamento(Map<String, Object> filtro) throws NegocioException {

		if (filtro != null) {

			Date dataGeracaoInicial = (Date) filtro.get(DATA_GERACAO_INICIAL);
			Date dataGeracaoFinal = (Date) filtro.get(DATA_GERACAO_FINAL);
			String anoMesRerencia = (String) filtro.get(REFERENCIA);

			if (dataGeracaoInicial != null && dataGeracaoFinal != null && dataGeracaoFinal.before(dataGeracaoInicial)) {

				throw new NegocioException(ERRO_PESQUISA_DATA_FINAL_MAIOR_GERACAO, true);
			}
			if (dataGeracaoInicial != null && dataGeracaoInicial.after(Calendar.getInstance().getTime())) {

				throw new NegocioException(ERRO_PESQUISA_DATA_INICIAL_MAIOR_ATUAL_GERACAO, true);
			}
			if (dataGeracaoFinal != null && dataGeracaoFinal.after(Calendar.getInstance().getTime())) {

				throw new NegocioException(ERRO_PESQUISA_DATA_FINAL_MAIOR_ATUAL_GERACAO, true);
			}

			if (!StringUtils.isEmpty(anoMesRerencia)) {
				ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
				Integer ultimoAnoMes = controladorImovel.obterUltimoAnoMesFaturamento();
				Integer anoMesFiltro = Integer.valueOf(anoMesRerencia);
				if (anoMesFiltro.compareTo(ultimoAnoMes) > 0) {
					throw new NegocioException(ERRO_ANO_MES_REFERENCIA_SUPERIOR_AO_SISTEMA, true);
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.ControladorMedidor
	 * #removerMedidores(java.lang.Long[])
	 */

	/**
	 * Atualizar analisada.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void atualizarAnalisada(Long[] chavesPrimarias) throws NegocioException, ConcorrenciaException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesPrimarias);
			Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamentos = this.consultarHistoricoAnomFaturamento(filtro);
			if ((listaHistoricoAnomaliaFaturamentos != null) && (!listaHistoricoAnomaliaFaturamentos.isEmpty())) {
				for (HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento : listaHistoricoAnomaliaFaturamentos) {
					historicoAnomaliaFaturamento.setAnalisada(true);
					historicoAnomaliaFaturamento.setDataAnalise(Calendar.getInstance().getTime());
					super.atualizar(historicoAnomaliaFaturamento);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.ControladorMedidor
	 * #consultarMedidor(java.util.Map)
	 */
	@Override
	@SuppressWarnings({"unchecked", "squid:S1192"})
	public Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomFaturamento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Boolean habilitado = (Boolean) filtro.get(ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(ATRIBUTO_HABILITADO, habilitado));
			}

			Integer numeroCiclo = (Integer) filtro.get("numeroCiclo");
			if (numeroCiclo != null) {
				criteria.add(Restrictions.eq("numeroCiclo", numeroCiclo));
			}

			Integer anoMesFaturamento = (Integer) filtro.get("anoMesFaturamento");
			if (anoMesFaturamento != null) {
				criteria.add(Restrictions.eq("anoMesFaturamento", anoMesFaturamento));
			}

			Long chaveFatura = (Long) filtro.get("chaveFatura");
			if (chaveFatura != null && chaveFatura > 0) {
				criteria.setFetchMode("fatura", FetchMode.JOIN);
				criteria.add(Restrictions.eq("fatura.chavePrimaria", chaveFatura));
			}

			Long chaveRota = (Long) filtro.get("chaveRota");
			if (chaveRota != null && chaveRota > 0) {
				criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.INNER_JOIN);
				criteria.createAlias("pontoConsumo.rota", "rota", Criteria.INNER_JOIN);
				criteria.add(Restrictions.eq("rota.chavePrimaria", chaveRota));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #
	 * consultarModulosPorChaves(java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaliaFaturamentoPorChaves(Long[] chavesPrimarias)
					throws NegocioException {

		Criteria criteria = getCriteria();

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
		}

		criteria.createAlias("faturamentoAnormalidade", "faturamento").addOrder(Order.asc("faturamento.descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.ControladorHistoricoAnomaliaFaturamento#obterHistoricoAnomaliaFaturamento(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public HistoricoAnomaliaFaturamento obterHistoricoAnomaliaFaturamento(Long idHistoricoAnomalia) {

		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true and historico.chavePrimaria = :idHistoricoAnomalia ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idHistoricoAnomalia", idHistoricoAnomalia);

		return (HistoricoAnomaliaFaturamento) query.uniqueResult();
	}

	/**
	 * Checks if is quebra relatorio anamolia.
	 * 
	 * @param historico
	 *            the historico
	 * @param analiseFaturaVO
	 *            the analise fatura vo
	 * @return true, if is quebra relatorio anamolia
	 */
	private boolean isQuebraRelatorioAnamolia(HistoricoAnomaliaFaturamento historico, AnaliseFaturaVO analiseFaturaVO) {

		Boolean retorno = Boolean.FALSE;

		if (historico != null && analiseFaturaVO != null) {

			if (historico.getFaturamentoAnormalidade().getDescricao().equals(analiseFaturaVO.getDescricaoAnormalidade()) && 
							(historico.getDescricaoComplementar() != null || analiseFaturaVO.getDescricaoComplementar() != null)) {

				if (historico.getDescricaoComplementar() != null && analiseFaturaVO.getDescricaoComplementar() != null && 
								historico.getDescricaoComplementar().equals(analiseFaturaVO.getDescricaoComplementar())) {
					
					retorno = Boolean.TRUE;

				}

			} else {
				retorno = Boolean.TRUE;
			}

		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.ControladorHistoricoAnomaliaFaturamento#gerarRelatorioAnomaliaFaturamento(java.lang.Long[],
	 * java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioAnomaliaFaturamento(Long[] chavesPrimarias, Map<String, Object> filtro) throws NegocioException {

		Collection<HistoricoAnomaliaFaturamento> listaAnomaliaFaturamento = this
						.consultarHistoricoAnomaliaFaturamentoPorChaves(chavesPrimarias);
		Collection<AnaliseFaturaHistoricoVO> listaAnaliseFaturaHistoricoVO = new ArrayList<AnaliseFaturaHistoricoVO>();
		Collection<AnaliseFaturaHistoricoVO> listaAnaliseFaturaHistoricoVO2 = new ArrayList<AnaliseFaturaHistoricoVO>();
		Collection<AnaliseFaturaVO> listaAnaliseFaturaVO = new ArrayList<AnaliseFaturaVO>();

		AnaliseFaturaVO analiseFaturaVO = null;
		AnaliseFaturaHistoricoVO analiseFaturaHistoricoVO = null;
		int contador = 0;
		int qtdAnomalias = 0;
		int qtdFaturasGeradas = 0;
		BigDecimal valorFaturasGeradas = BigDecimal.ZERO;
		BigDecimal valorTotalFaturasGeradas = BigDecimal.ZERO;
		int totalFaturasGeradas = 0;
		int totalAnormalidade = 0;

		boolean adiciona = false;

		for (HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento : listaAnomaliaFaturamento) {
			if (contador == 0) {

				analiseFaturaVO = new AnaliseFaturaVO();
				analiseFaturaVO.setDescricaoAnormalidade(historicoAnomaliaFaturamento.getFaturamentoAnormalidade().getDescricao());
				analiseFaturaVO.setDescricaoComplementar(historicoAnomaliaFaturamento.getDescricaoComplementar());

				for (HistoricoAnomaliaFaturamento historico : listaAnomaliaFaturamento) {

					if (this.isQuebraRelatorioAnamolia(historico, analiseFaturaVO)) {

						analiseFaturaHistoricoVO = new AnaliseFaturaHistoricoVO();
						qtdAnomalias++;
						totalAnormalidade++;

						if (historico.getFaturamentoAnormalidade() != null) {
							analiseFaturaHistoricoVO.setDescricaoAnomalia(historico.getFaturamentoAnormalidade().getDescricao());
						}

						if (historico.getPontoConsumo() != null) {
							analiseFaturaHistoricoVO.setImovel(historico.getPontoConsumo().getDescricao());
							if (historico.getPontoConsumo().getSegmento() != null) {
								analiseFaturaHistoricoVO.setSegmentoFatura(historico.getPontoConsumo().getSegmento().getDescricao());
							}
						}

						if (historico.getCliente() != null) {
							analiseFaturaHistoricoVO.setCodigoCliente(String.valueOf(historico.getCliente().getChavePrimaria()));
						}

						if (historico.isAnalisada()) {
							analiseFaturaHistoricoVO.setIndicadorAnomalia(CONSTANTE_SIM);
						} else {
							analiseFaturaHistoricoVO.setIndicadorAnomalia(CONSTANTE_NAO);
						}

						if (historico.getFatura() != null) {
							analiseFaturaHistoricoVO.setNumeroFatura(String.valueOf(historico.getFatura().getChavePrimaria()));
							analiseFaturaHistoricoVO.setValorFatura(String.valueOf(historico.getFatura().getValorTotal()));
							qtdFaturasGeradas++;
							totalFaturasGeradas++;
							valorFaturasGeradas = valorFaturasGeradas.add(historico.getFatura().getValorTotal());
							valorTotalFaturasGeradas = valorTotalFaturasGeradas.add(historico.getFatura().getValorTotal());
						}

						if (historico.getDataGeracao() != null) {
							analiseFaturaHistoricoVO.setDataGeracao(String.valueOf(Util.converterDataParaStringSemHora(
											historico.getDataGeracao(), Constantes.FORMATO_DATA_BR)));
						}

						if (historico.getDataAnalise() != null) {
							analiseFaturaHistoricoVO.setDataResolucao(String.valueOf(Util.converterDataParaStringSemHora(
											historico.getDataAnalise(), Constantes.FORMATO_DATA_BR)));
						}

						if (historico.getAnalisador() != null) {
							analiseFaturaHistoricoVO.setNomeUsuario(historico.getAnalisador().getLogin());
						}

						listaAnaliseFaturaHistoricoVO.add(analiseFaturaHistoricoVO);
						analiseFaturaVO.setQtdAnomalia(String.valueOf(qtdAnomalias));
						analiseFaturaVO.setAnaliseFaturaHistoricoVO(listaAnaliseFaturaHistoricoVO);
						analiseFaturaVO.setQtdFaturasGeradas(String.valueOf(qtdFaturasGeradas));
						analiseFaturaVO.setValorFaturasGeradas(String.valueOf(valorFaturasGeradas));
						analiseFaturaVO.setTotalAnormalidade(String.valueOf(totalAnormalidade));
						analiseFaturaVO.setTotalfaturas(String.valueOf(totalFaturasGeradas));
						analiseFaturaVO.setValorTotalFaturas(String.valueOf(valorTotalFaturasGeradas));
					}
				}

				listaAnaliseFaturaVO.add(analiseFaturaVO);

			} else {

				qtdAnomalias = 0;
				qtdFaturasGeradas = 0;
				valorFaturasGeradas = BigDecimal.ZERO;

				for (AnaliseFaturaVO analiseFaturaVO2 : listaAnaliseFaturaVO) {
					if (!this.isQuebraRelatorioAnamolia(historicoAnomaliaFaturamento, analiseFaturaVO2)) {

						adiciona = true;

					} else {

						adiciona = false;

					}

				}

				if (adiciona) {

					analiseFaturaVO = new AnaliseFaturaVO();
					
					if (historicoAnomaliaFaturamento != null) {
						analiseFaturaVO.setDescricaoAnormalidade(
								historicoAnomaliaFaturamento.getFaturamentoAnormalidade().getDescricao());
						analiseFaturaVO
								.setDescricaoComplementar(historicoAnomaliaFaturamento.getDescricaoComplementar());
					}

					for (HistoricoAnomaliaFaturamento historico2 : listaAnomaliaFaturamento) {

						if (this.isQuebraRelatorioAnamolia(historico2, analiseFaturaVO)) {

							qtdAnomalias++;
							totalAnormalidade++;
							analiseFaturaHistoricoVO = new AnaliseFaturaHistoricoVO();

							if (historico2.getFaturamentoAnormalidade() != null) {
								analiseFaturaHistoricoVO.setDescricaoAnomalia(historico2.getFaturamentoAnormalidade().getDescricao());
							}

							if (historico2.getPontoConsumo() != null) {
								analiseFaturaHistoricoVO.setImovel(historico2.getPontoConsumo().getDescricao());
								if (historico2.getPontoConsumo().getSegmento() != null) {
									analiseFaturaHistoricoVO.setSegmentoFatura(historico2.getPontoConsumo().getSegmento().getDescricao());
								}
							}

							if (historico2.getCliente() != null) {
								analiseFaturaHistoricoVO.setCodigoCliente(String.valueOf(historico2.getCliente().getChavePrimaria()));
							}

							if (historico2.isAnalisada()) {
								analiseFaturaHistoricoVO.setIndicadorAnomalia(CONSTANTE_SIM);
							} else {
								analiseFaturaHistoricoVO.setIndicadorAnomalia(CONSTANTE_NAO);
							}

							if (historico2.getFatura() != null) {
								qtdFaturasGeradas++;
								totalFaturasGeradas++;
								valorFaturasGeradas = valorFaturasGeradas.add(historico2.getFatura().getValorTotal());
								analiseFaturaHistoricoVO.setNumeroFatura(String.valueOf(historico2.getFatura().getChavePrimaria()));
								analiseFaturaHistoricoVO.setValorFatura(String.valueOf(historico2.getFatura().getValorTotal()));
								valorTotalFaturasGeradas = valorTotalFaturasGeradas.add(historico2.getFatura().getValorTotal());
							}

							if (historico2.getDataGeracao() != null) {
								analiseFaturaHistoricoVO.setDataGeracao(String.valueOf(Util.converterDataParaStringSemHora(
												historico2.getDataGeracao(), Constantes.FORMATO_DATA_BR)));
							}

							if (historico2.getDataAnalise() != null) {
								analiseFaturaHistoricoVO.setDataResolucao(String.valueOf(Util.converterDataParaStringSemHora(
												historico2.getDataAnalise(), Constantes.FORMATO_DATA_BR)));
							}

							if (historico2.getAnalisador() != null) {
								analiseFaturaHistoricoVO.setNomeUsuario(historico2.getAnalisador().getLogin());
							}

							listaAnaliseFaturaHistoricoVO2.add(analiseFaturaHistoricoVO);
							analiseFaturaVO.setAnaliseFaturaHistoricoVO(listaAnaliseFaturaHistoricoVO2);
							analiseFaturaVO.setQtdAnomalia(String.valueOf(qtdAnomalias));
							analiseFaturaVO.setQtdFaturasGeradas(String.valueOf(qtdFaturasGeradas));
							analiseFaturaVO.setValorFaturasGeradas(String.valueOf(valorFaturasGeradas));
							analiseFaturaVO.setTotalAnormalidade(String.valueOf(totalAnormalidade));
							analiseFaturaVO.setTotalfaturas(String.valueOf(totalFaturasGeradas));
							analiseFaturaVO.setValorTotalFaturas(String.valueOf(valorTotalFaturasGeradas));

						}
					}
					listaAnaliseFaturaVO.add(analiseFaturaVO);
				}

			}
			contador++;
		}

		Map<String, Object> mapaParametros = prepararParametroRelatorio(filtro);
		return RelatorioUtil.gerarRelatorioPDF(listaAnaliseFaturaVO, mapaParametros, "relatorioAnomaliaHistorico.jasper");
	}

	/**
	 * Preparar parametro relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<String, Object> prepararParametroRelatorio(Map<String, Object> filtro) throws NegocioException {

		Map<String, Object> parametros = new HashMap<String, Object>();

		String cepImovel = (String) filtro.get("cepImovel");
		if (!StringUtils.isEmpty(cepImovel)) {
			parametros.put("CEP", cepImovel);
		}

		Long matriculaImovel = (Long) filtro.get("matriculaImovel");
		if ((matriculaImovel != null) && (matriculaImovel > 0)) {
			parametros.put("matriculaImovel", String.valueOf(matriculaImovel));
		}

		Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
		if (indicadorCondominio != null) {
			if (Boolean.TRUE.equals(indicadorCondominio)) {
				parametros.put("ehImovelCondominio", CONSTANTE_SIM);
			} else {
				parametros.put("ehImovelCondominio", CONSTANTE_NAO);
			}
		}

		String nomeImovel = (String) filtro.get("nomeImovel");
		if (!StringUtils.isEmpty(nomeImovel)) {
			parametros.put("nomeFantasia", nomeImovel);
		}

		String complemento = (String) filtro.get("complementoImovel");
		if (!StringUtils.isEmpty(complemento)) {
			complemento = (complemento.replace('%', ' ')).trim();
			parametros.put("complemento", complemento);
		}

		String numeroImovel = (String) filtro.get("numeroImovel");
		if (!StringUtils.isEmpty(numeroImovel)) {
			parametros.put("numeroImovel", numeroImovel);
		}

		Long[] idsRota = (Long[]) filtro.get("idsRota");
		if (idsRota != null && idsRota.length > 0) {
			StringBuilder stringBuilder = new StringBuilder();
			String rotas = null;
			for (int i = 0; i < idsRota.length; i++) {
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			rotas = stringBuilder.toString();
			if (rotas.length() > 0) {
				parametros.put("rota", rotas.substring(0, stringBuilder.toString().length() - 2));
			}
		}

		Long[] idsAnormalidadeFatura = (Long[]) filtro.get("idsAnormalidadeFatura");
		if (idsAnormalidadeFatura != null && idsAnormalidadeFatura.length > 0) {

			ControladorFaturamentoAnormalidade controladorFaturamentoAnormalidade = (ControladorFaturamentoAnormalidade) ServiceLocator
							.getInstancia().getControladorNegocio(
											ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE);

			StringBuilder stringBuilder = new StringBuilder();
			String anormalidades = null;
			for (int i = 0; i < idsAnormalidadeFatura.length; i++) {
				FaturamentoAnormalidade faturamentoAnormalidade = (FaturamentoAnormalidade) controladorFaturamentoAnormalidade
								.obter(idsAnormalidadeFatura[i]);
				stringBuilder.append(faturamentoAnormalidade.getDescricao());
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			anormalidades = stringBuilder.toString();
			if (anormalidades.length() > 0) {
				parametros.put("anormalidade", anormalidades.substring(0, stringBuilder.toString().length() - 2));
			}
		}

		String consumoLido = (String) filtro.get("consumoLido");
		if (!StringUtils.isEmpty(consumoLido)) {
			parametros.put("consumoLido", consumoLido);
		}

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> filtroEmpresa = new HashMap<String, Object>();
		filtroEmpresa.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtroEmpresa);
		for (Empresa empresa : listaEmpresas) {
			if (empresa.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}
		}
		return parametros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.
	 * ControladorHistoricoAnomaliaFaturamento
	 * #validarInclusaoHistoricoAnomaliaFaturamento
	 * (java.util.Collection)
	 */
	@Override
	public void validarInclusaoFaturaHistoricoAnomaliaFaturamento(Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento)
					throws NegocioException {

		for (HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento : listaHistoricoAnomaliaFaturamento) {
			if (historicoAnomaliaFaturamento.isAnalisada()) {
				throw new NegocioException(ERRO_ANOMALIA_JA_ANALISADA, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.
	 * ControladorHistoricoAnomaliaFaturamento
	 * #validarInclusaoHistoricoAnomaliaFaturamento
	 * (java.util.Collection)
	 */
	@Override
	public void validarAlteracaoFaturaHistoricoAnomaliaFaturamento(
					Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento) throws NegocioException {

		for (HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento : listaHistoricoAnomaliaFaturamento) {
			if (historicoAnomaliaFaturamento.isAnalisada()) {
				throw new NegocioException(ERRO_ANOMALIA_ANALISADA, true);
			}
		}
	}

	/**
	 * Atualiza para analisada os historicos de consumo do processamento anterior
	 */
	@Override
	public void atualizarAnalisaHistoricoAnomaliaFaturamentoProcessamentoAnterior(Rota rota, GrupoFaturamento grupoFaturamento)
					throws NegocioException, ConcorrenciaException {

		StringBuilder hql = new StringBuilder();

		hql.append(" UPDATE ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" SET ");
		hql.append(" historico.analisada = true, ");
		hql.append(" historico.dataAnalise = :dataAtual");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true and historico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = :numeroCiclo  ");
		hql.append(" and historico.pontoConsumo.chavePrimaria in (");
		hql.append(" select pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.rota.chavePrimaria = :chaveRota) ");		

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMesFaturamento", grupoFaturamento.getAnoMesReferencia());
		query.setInteger("numeroCiclo", grupoFaturamento.getNumeroCiclo());		
		query.setLong("chaveRota", rota.getChavePrimaria());
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		query.executeUpdate();
	}
	
	
	/**
	 * Atualiza para analisado os históricos de consumo do processamento anterior, para as rotas especificadas no parâmetro.
	 * 
	 * @param chavesPrimariasRotas
	 * @param grupoFaturamento
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	@Override
	public void atualizarAnalisaHistoricoAnomaliaFaturamentoProcessamentoAnterior(Long[] chavesPrimariasRotas,
			GrupoFaturamento grupoFaturamento) throws NegocioException, ConcorrenciaException {

		StringBuilder hql = new StringBuilder();

		hql.append(" UPDATE ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" SET ");
		hql.append(" historico.analisada = true, ");
		hql.append(" historico.dataAnalise = :dataAtual");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true and historico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = :numeroCiclo  ");
		hql.append(" and historico.pontoConsumo.chavePrimaria in (");
		hql.append(" select pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.rota.chavePrimaria IN (:chavesPrimariasRotas)) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMesFaturamento", grupoFaturamento.getAnoMesReferencia());
		query.setInteger("numeroCiclo", grupoFaturamento.getNumeroCiclo());
		query.setParameterList("chavesPrimariasRotas", chavesPrimariasRotas);
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		query.executeUpdate();
	}
	
	/**
	 * Retorna a lista de historico de anomalia para a referencia e ciclo de uma determinada rota
	 * 
	 * @param rota {@link Rota}}
	 * @param anoMesFaturamento
	 * @param ciclo
	 * @return a lista de historico de anomalias {@link HistoricoAnomaliaFaturamento}
	 */
	@Override
	public List<HistoricoAnomaliaFaturamento> obterHistoricoAnomaliaFaturamentoaPorRota(Rota rota, Integer anoMesFaturamento, Integer ciclo) {
		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo ponto");
		hql.append(" inner join fetch ponto.rota rota");
		hql.append(" inner join fetch historico.faturamentoAnormalidade anormalidade ");
		hql.append(" where ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and rota.chavePrimaria = :idRota ");
		hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = :numeroCiclo ");
		hql.append(" and historico.analisada = :anormalidadeAnalisada");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMesFaturamento", anoMesFaturamento);
		query.setInteger("numeroCiclo", ciclo);
		query.setLong("idRota", rota.getChavePrimaria());
		query.setBoolean("anormalidadeAnalisada", Boolean.FALSE);

		return query.list();
	}
}

