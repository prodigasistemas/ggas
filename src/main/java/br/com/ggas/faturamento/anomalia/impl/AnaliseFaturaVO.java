/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.anomalia.impl;

import java.io.Serializable;
import java.util.Collection;

/**
 * Classe responsável pela representação da análise de fatura
 *
 */
public class AnaliseFaturaVO implements Serializable {

	private static final long serialVersionUID = -2841726497639990301L;

	private String descricaoAnormalidade;

	private Collection<AnaliseFaturaHistoricoVO> analiseFaturaHistoricoVO;

	private String qtdFaturasGeradas;

	private String valorFaturasGeradas;

	private String totalAnormalidade;

	private String totalfaturas;

	private String valorTotalFaturas;

	private String qtdAnomalia;

	private String descricaoComplementar;

	/**
	 * @return the qtdAnomalia
	 */
	public String getQtdAnomalia() {

		return qtdAnomalia;
	}

	/**
	 * @param qtdAnomalia
	 *            the qtdAnomalia to set
	 */
	public void setQtdAnomalia(String qtdAnomalia) {

		this.qtdAnomalia = qtdAnomalia;
	}

	/**
	 * @return the qtdFaturasGeradas
	 */
	public String getQtdFaturasGeradas() {

		return qtdFaturasGeradas;
	}

	/**
	 * @param qtdFaturasGeradas
	 *            the qtdFaturasGeradas to set
	 */
	public void setQtdFaturasGeradas(String qtdFaturasGeradas) {

		this.qtdFaturasGeradas = qtdFaturasGeradas;
	}

	/**
	 * @return the valorFaturasGeradas
	 */
	public String getValorFaturasGeradas() {

		return valorFaturasGeradas;
	}

	/**
	 * @param valorFaturasGeradas
	 *            the valorFaturasGeradas to set
	 */
	public void setValorFaturasGeradas(String valorFaturasGeradas) {

		this.valorFaturasGeradas = valorFaturasGeradas;
	}

	/**
	 * @return the totalAnormalidade
	 */
	public String getTotalAnormalidade() {

		return totalAnormalidade;
	}

	/**
	 * @param totalAnormalidade
	 *            the totalAnormalidade to set
	 */
	public void setTotalAnormalidade(String totalAnormalidade) {

		this.totalAnormalidade = totalAnormalidade;
	}

	/**
	 * @return the totalfaturas
	 */
	public String getTotalfaturas() {

		return totalfaturas;
	}

	/**
	 * @param totalfaturas
	 *            the totalfaturas to set
	 */
	public void setTotalfaturas(String totalfaturas) {

		this.totalfaturas = totalfaturas;
	}

	/**
	 * @return the valorTotalFaturas
	 */
	public String getValorTotalFaturas() {

		return valorTotalFaturas;
	}

	/**
	 * @param valorTotalFaturas
	 *            the valorTotalFaturas to set
	 */
	public void setValorTotalFaturas(String valorTotalFaturas) {

		this.valorTotalFaturas = valorTotalFaturas;
	}

	/**
	 * @return the descricaoAnormalidade
	 */
	public String getDescricaoAnormalidade() {

		return descricaoAnormalidade;
	}

	/**
	 * @param descricaoAnormalidade
	 *            the descricaoAnormalidade to set
	 */
	public void setDescricaoAnormalidade(String descricaoAnormalidade) {

		this.descricaoAnormalidade = descricaoAnormalidade;
	}

	/**
	 * @return the analiseFaturaHistoricoVO
	 */
	public Collection<AnaliseFaturaHistoricoVO> getAnaliseFaturaHistoricoVO() {

		return analiseFaturaHistoricoVO;
	}

	/**
	 * @param analiseFaturaHistoricoVO
	 *            the analiseFaturaHistoricoVO to
	 *            set
	 */
	public void setAnaliseFaturaHistoricoVO(Collection<AnaliseFaturaHistoricoVO> analiseFaturaHistoricoVO) {

		this.analiseFaturaHistoricoVO = analiseFaturaHistoricoVO;
	}

	/**
	 * @return descricaoComplementar
	 */
	public String getDescricaoComplementar() {

		return descricaoComplementar;
	}

	/**
	 * @param descricaoComplementar
	 */
	public void setDescricaoComplementar(String descricaoComplementar) {

		this.descricaoComplementar = descricaoComplementar;
	}

}
