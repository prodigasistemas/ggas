/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.anomalia.impl;

import java.io.Serializable;

/**
 * Classe responsável pela representação de Analise do Historico de Fatura
 *
 */
public class AnaliseFaturaHistoricoVO implements Serializable {

	private static final long serialVersionUID = -2841726497639990301L;

	private String descricaoAnormalidade;

	private String imovel;

	private String codigoCliente;

	private String segmentoFatura;

	private String descricaoAnomalia;

	private String indicadorAnomalia;

	private String numeroFatura;

	private String valorFatura;

	private String dataGeracao;

	private String dataResolucao;

	private String nomeUsuario;

	/**
	 * @return the descricaoAnormalidade
	 */
	public String getDescricaoAnormalidade() {

		return descricaoAnormalidade;
	}

	/**
	 * @param descricaoAnormalidade
	 *            the descricaoAnormalidade to set
	 */
	public void setDescricaoAnormalidade(String descricaoAnormalidade) {

		this.descricaoAnormalidade = descricaoAnormalidade;
	}

	/**
	 * @return the imovel
	 */
	public String getImovel() {

		return imovel;
	}

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	public void setImovel(String imovel) {

		this.imovel = imovel;
	}

	/**
	 * @return the codigoCliente
	 */
	public String getCodigoCliente() {

		return codigoCliente;
	}

	/**
	 * @param codigoCliente
	 *            the codigoCliente to set
	 */
	public void setCodigoCliente(String codigoCliente) {

		this.codigoCliente = codigoCliente;
	}

	/**
	 * @return the segmentoFatura
	 */
	public String getSegmentoFatura() {

		return segmentoFatura;
	}

	/**
	 * @param segmentoFatura
	 *            the segmentoFatura to set
	 */
	public void setSegmentoFatura(String segmentoFatura) {

		this.segmentoFatura = segmentoFatura;
	}

	/**
	 * @return the descricaoAnomalia
	 */
	public String getDescricaoAnomalia() {

		return descricaoAnomalia;
	}

	/**
	 * @param descricaoAnomalia
	 *            the descricaoAnomalia to set
	 */
	public void setDescricaoAnomalia(String descricaoAnomalia) {

		this.descricaoAnomalia = descricaoAnomalia;
	}

	/**
	 * @return the indicadorAnomalia
	 */
	public String getIndicadorAnomalia() {

		return indicadorAnomalia;
	}

	/**
	 * @param indicadorAnomalia
	 *            the indicadorAnomalia to set
	 */
	public void setIndicadorAnomalia(String indicadorAnomalia) {

		this.indicadorAnomalia = indicadorAnomalia;
	}

	/**
	 * @return the numeroFatura
	 */
	public String getNumeroFatura() {

		return numeroFatura;
	}

	/**
	 * @param numeroFatura
	 *            the numeroFatura to set
	 */
	public void setNumeroFatura(String numeroFatura) {

		this.numeroFatura = numeroFatura;
	}

	/**
	 * @return the valorFatura
	 */
	public String getValorFatura() {

		return valorFatura;
	}

	/**
	 * @param valorFatura
	 *            the valorFatura to set
	 */
	public void setValorFatura(String valorFatura) {

		this.valorFatura = valorFatura;
	}

	/**
	 * @return the dataGeracao
	 */
	public String getDataGeracao() {

		return dataGeracao;
	}

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	public void setDataGeracao(String dataGeracao) {

		this.dataGeracao = dataGeracao;
	}

	/**
	 * @return the dataResolucao
	 */
	public String getDataResolucao() {

		return dataResolucao;
	}

	/**
	 * @param dataResolucao
	 *            the dataResolucao to set
	 */
	public void setDataResolucao(String dataResolucao) {

		this.dataResolucao = dataResolucao;
	}

	/**
	 * @return the nomeUsuario
	 */
	public String getNomeUsuario() {

		return nomeUsuario;
	}

	/**
	 * @param nomeUsuario
	 *            the nomeUsuario to set
	 */
	public void setNomeUsuario(String nomeUsuario) {

		this.nomeUsuario = nomeUsuario;
	}

}
