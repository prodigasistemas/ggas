/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.anomalia;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Pair;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador
 * de Histórico e Anomalia de Faturamento. 
 *
 */
public interface ControladorHistoricoAnomaliaFaturamento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_HISTORICO_ANOMALIA_FATURAMENTO = "controladorHistoricoAnomaliaFaturamento";

	/**
	 * Consultar historico anomalia faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaliaFaturamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar historico anomalia faturamento de maneira paginada.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Pair<List<HistoricoAnomaliaFaturamento>, Long> consultarHistoricoAnomaliaFaturamentoPaginada(Map<String, Object> filtro)
			throws NegocioException;

	/**
	 * Método responsável por listar anormalidades
	 * de faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	void validarFiltroPesquisaHistoricoAnomaliaFaturamento(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException;
	
	/**
	 * Método responsável por listar anormalidades
	 * de faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomFaturamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar anormalidades
	 * de faturamento.
	 * 
	 * @param idHistoricoAnomalia
	 *            the id historico anomalia
	 * @return the historico anomalia faturamento
	 */
	HistoricoAnomaliaFaturamento obterHistoricoAnomaliaFaturamento(Long idHistoricoAnomalia);

	/**
	 * Método responsável por listar anormalidades
	 * de faturamento.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return the collection
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<HistoricoAnomaliaFaturamento> consultarHistoricoAnomaliaFaturamentoPorChaves(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar anormalidades
	 * de faturamento.
	 * 
	 * @param chavesPrimariasAux
	 *            the chaves primarias aux
	 * @param filtro
	 *            the filtro
	 * @return the byte[]
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	byte[] gerarRelatorioAnomaliaFaturamento(Long[] chavesPrimariasAux, Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por validar o Historico
	 * Anomalia para inclusão da fatura.
	 * 
	 * @param listaHistoricoAnomaliaFaturamento
	 *            the lista historico anomalia faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarInclusaoFaturaHistoricoAnomaliaFaturamento(Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento)
					throws NegocioException;

	/**
	 * Método responsável por validar o Historico
	 * Anomalia para inclusão da fatura.
	 * 
	 * @param listaHistoricoAnomaliaFaturamento
	 *            the lista historico anomalia faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAlteracaoFaturaHistoricoAnomaliaFaturamento(Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento)
					throws NegocioException;

	/**
	 * Atualiza para analisada os historicos de consumo do processamento anterior
	 * @param rota 
	 * 		the rota
	 * @param grupoFaturamento 
	 * 			the grupo faturamento
	 * @throws NegocioException
	 * 			the negocio exception
	 * @throws ConcorrenciaException
	 * 			the concorrencia Exception
	 */
	void atualizarAnalisaHistoricoAnomaliaFaturamentoProcessamentoAnterior(Rota rota, GrupoFaturamento grupoFaturamento)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Atualiza para analisado os históricos de consumo do processamento anterior, para as
	 * rotas especificadas no parâmetro.
	 * @param chavesPrimariasRotas
	 * 			the Chaves Primarias Rotas
	 * @param grupoFaturamento
	 * 			the Grupo Faturamento
	 * @throws NegocioException
	 * 			the negocio exception
	 * @throws ConcorrenciaException
	 * 			the concorrencia Exception
	 */
	void atualizarAnalisaHistoricoAnomaliaFaturamentoProcessamentoAnterior(Long[] chavesPrimariasRotas, GrupoFaturamento grupoFaturamento)
					throws NegocioException, ConcorrenciaException;
	
	/**
	 * Retorna a lista de historico de anomalia para a referencia e ciclo de uma determinada rota
	 *
	 * @param rota {@link Rota}}
	 * @param anoMesFaturamento
	 * @param ciclo
	 * @return a lista de historico de anomalias {@link HistoricoAnomaliaFaturamento}
	 */
	List<HistoricoAnomaliaFaturamento> obterHistoricoAnomaliaFaturamentoaPorRota(Rota rota, Integer anoMesFaturamento, Integer ciclo);

}
