/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.anomalia.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelos atributos e implementação dos métodos relacionados ao Histórico de Anomalia do Faturamento. 
 *
 */
public class HistoricoAnomaliaFaturamentoImpl extends EntidadeNegocioImpl implements HistoricoAnomaliaFaturamento {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7284788787410467214L;

	private Integer anoMesFaturamento;

	private Integer numeroCiclo;

	private Date dataGeracao;

	private Date dataAnalise;

	private Boolean indicadorCorrecao;

	private PontoConsumo pontoConsumo;

	private Fatura fatura;

	private FaturamentoAnormalidade faturamentoAnormalidade;

	private Usuario analisador;

	private Cliente cliente;

	private boolean analisada;

	private String descricaoComplementar;

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getDataInicioVigenciaFormatada()
	 */
	public String getDataGeracaoFormatada() {

		return Util.converterDataParaStringSemHora(dataGeracao, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getDataFimVigenciaFormatada()
	 */
	public String getDataAnaliseFormatada() {

		String retorno = "";
		if(dataAnalise != null) {
			retorno = Util.converterDataParaStringSemHora(dataAnalise, Constantes.FORMATO_DATA_BR);
		}
		return retorno;
	}

	/**
	 * @return the analisada
	 */
	@Override
	public boolean isAnalisada() {

		return analisada;
	}

	/**
	 * @param analisada
	 *            the analisada to set
	 */
	@Override
	public void setAnalisada(boolean analisada) {

		this.analisada = analisada;
	}

	/**
	 * @return the analisador
	 */
	@Override
	public Usuario getAnalisador() {

		return analisador;
	}

	/**
	 * @param analisador
	 *            the analisador to set
	 */
	@Override
	public void setAnalisador(Usuario analisador) {

		this.analisador = analisador;
	}

	/**
	 * @return the cliente
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/**
	 * @return the dataAnalise
	 */
	@Override
	public Date getDataAnalise() {
		Date data = null;
		if (this.dataAnalise != null) {
			data = (Date) this.dataAnalise.clone();
		}
		return data;
	}

	/**
	 * @param dataAnalise
	 *            the dataAnalise to set
	 */
	@Override
	public void setDataAnalise(Date dataAnalise) {
		if(dataAnalise != null) {
			this.dataAnalise = (Date) dataAnalise.clone();
		} else {
			this.dataAnalise = null;
		}
		
	}

	/**
	 * @return the dataGeracao
	 */
	@Override
	public Date getDataGeracao() {
		Date data = null;
		if (this.dataGeracao != null) {
			data = (Date) dataGeracao.clone();
		}
		return data;
	}

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	@Override
	public void setDataGeracao(Date dataGeracao) {
		if (dataGeracao != null) {
			this.dataGeracao = (Date) dataGeracao.clone();
		} else {
			this.dataGeracao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #getAnoMesFaturamento()
	 */
	@Override
	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #setAnoMesFaturamento(java.lang.Integer)
	 */
	@Override
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #getNumeroCiclo()
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #setNumeroCiclo(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #getIndicadorCorrecao()
	 */
	@Override
	public Boolean getIndicadorCorrecao() {

		return indicadorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #setIndicadorCorrecao(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorCorrecao(Boolean indicadorCorrecao) {

		this.indicadorCorrecao = indicadorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #setPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento#getFatura()
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #setFatura(br.com.ggas.faturamento.Fatura)
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #getFaturamentoAnormalidade()
	 */
	@Override
	public FaturamentoAnormalidade getFaturamentoAnormalidade() {

		return faturamentoAnormalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.impl.
	 * HistoricoAnomaliaFaturamento
	 * #setFaturamentoAnormalidade
	 * (br.com.ggas.faturamento
	 * .FaturamentoAnormalidade)
	 */
	@Override
	public void setFaturamentoAnormalidade(FaturamentoAnormalidade faturamentoAnormalidade) {

		this.faturamentoAnormalidade = faturamentoAnormalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		String camposObrigatorios = null;

		if(anoMesFaturamento == null || anoMesFaturamento < 0) {
			camposObrigatoriosAcumulados.append(ANO_MES_FATURAMENTO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(numeroCiclo == null || numeroCiclo < 0) {
			camposObrigatoriosAcumulados.append(CICLO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(dataGeracao == null) {
			camposObrigatoriosAcumulados.append(DATA_GERACAO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(pontoConsumo == null) {
			camposObrigatoriosAcumulados.append(PONTO_CONSUMO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(faturamentoAnormalidade == null) {
			camposObrigatoriosAcumulados.append(FATURAMENTO_ANORMALIDADE);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0,
							camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.
	 * HistoricoAnomaliaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getAnoMesFaturamentoFormatado() {

		return Util.formatarAnoMes(anoMesFaturamento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.
	 * HistoricoAnomaliaFaturamento
	 * #getDescricaoComplementar()
	 */
	@Override
	public String getDescricaoComplementar() {

		return descricaoComplementar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.anomalia.
	 * HistoricoAnomaliaFaturamento
	 * #setDescricaoComplementar(java.lang.String)
	 */
	@Override
	public void setDescricaoComplementar(String descricaoComplementar) {

		this.descricaoComplementar = descricaoComplementar;
	}

}
