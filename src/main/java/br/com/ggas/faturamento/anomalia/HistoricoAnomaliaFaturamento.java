/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.anomalia;

import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * ao Histórico de Anomalia de Fauramento.
 *
 */
public interface HistoricoAnomaliaFaturamento extends EntidadeNegocio {

	String BEAN_ID_HISTORICO_ANOMALIA_FATURAMENTO = "historicoAnomaliaFaturamento";

	String ANO_MES_FATURAMENTO = "ANO_MES_FATURAMENTO";

	String CICLO = "CICLO";

	String DATA_GERACAO = "DATA_GERACAO";

	String PONTO_CONSUMO = "ROTULO_PONTO_CONSUMO";

	String FATURAMENTO_ANORMALIDADE = "FATURAMENTO_ANORMALIDADE";

	/**
	 * @return the analisador
	 */
	Usuario getAnalisador();

	/**
	 * @param analisador
	 *            the analisador to set
	 */
	void setAnalisador(Usuario analisador);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return Boolean - Retorna boolean Analisada.
	 */
	boolean isAnalisada();

	/**
	 * @param analisada
	 *            the analisada to set
	 */
	void setAnalisada(boolean analisada);

	/**
	 * @return the dataAnalise
	 */
	Date getDataAnalise();

	/**
	 * @param dataAnalise
	 *            the dataAnalise to set
	 */
	void setDataAnalise(Date dataAnalise);

	/**
	 * @return the dataGeracao
	 */
	Date getDataGeracao();

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	void setDataGeracao(Date dataGeracao);

	/**
	 * @return the anoMesFaturamento
	 */
	Integer getAnoMesFaturamento();

	/**
	 * @param anoMesFaturamento
	 *            the anoMesFaturamento to set
	 */
	void setAnoMesFaturamento(Integer anoMesFaturamento);

	/**
	 * @return the numeroCiclo
	 */
	Integer getNumeroCiclo();

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * @return the indicadorCorrecao
	 */
	Boolean getIndicadorCorrecao();

	/**
	 * @param indicadorCorrecao
	 *            the indicadorCorrecao to set
	 */
	void setIndicadorCorrecao(Boolean indicadorCorrecao);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 *            the fatura to set
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return the faturamentoAnormalidade
	 */
	FaturamentoAnormalidade getFaturamentoAnormalidade();

	/**
	 * @param faturamentoAnormalidade
	 *            the faturamentoAnormalidade to
	 *            set
	 */
	void setFaturamentoAnormalidade(FaturamentoAnormalidade faturamentoAnormalidade);

	/**
	 * @return the anoMesFaturamento formatado
	 */
	String getAnoMesFaturamentoFormatado();

	/**
	 * @return descricaoComplementar
	 */
	String getDescricaoComplementar();

	/**
	 * @param descricaoComplementar
	 */
	void setDescricaoComplementar(String descricaoComplementar);

}
