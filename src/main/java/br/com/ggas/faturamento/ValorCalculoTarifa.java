/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * ao calculo do valor da tarifa
 *
 */
public interface ValorCalculoTarifa extends Serializable {

	String BEAN_ID_VALOR_CALCULO_TARIFA = "valorCalculoTarifa";

	/**
	 * @return the faixaConsumo
	 */
	TarifaVigenciaFaixa getFaixaConsumo();

	/**
	 * @param faixaConsumo
	 *            the faixaConsumo to set
	 */
	void setFaixaConsumo(TarifaVigenciaFaixa faixaConsumo);

	/**
	 * @return the valorFixoSemImposto
	 */
	BigDecimal getValorFixoSemImposto();

	/**
	 * @param valorFixoSemImposto
	 *            the valorFixoSemImposto to set
	 */
	void setValorFixoSemImposto(BigDecimal valorFixoSemImposto);

	/**
	 * @return the valorFixoComImposto
	 */
	BigDecimal getValorFixoComImposto();

	/**
	 * @param valorFixoComImposto
	 *            the valorFixoComImposto to set
	 */
	void setValorFixoComImposto(BigDecimal valorFixoComImposto);

	/**
	 * @return the valorFaixaSemImposto
	 */
	BigDecimal getValorFaixaSemImposto();

	/**
	 * @param valorFaixaSemImposto
	 *            the valorFaixaSemImposto to set
	 */
	void setValorFaixaSemImposto(BigDecimal valorFaixaSemImposto);

	/**
	 * @return the valorFaixaComImposto
	 */
	BigDecimal getValorFaixaComImposto();

	/**
	 * @param valorFaixaComImposto
	 *            the valorFaixaComImposto to set
	 */
	void setValorFaixaComImposto(BigDecimal valorFaixaComImposto);

	/**
	 * @return the consumo
	 */
	BigDecimal getConsumo();

	/**
	 * @param consumo
	 *            the consumo to set
	 */
	void setConsumo(BigDecimal consumo);

	/**
	 * @return the valorConsumo
	 */
	BigDecimal getValorConsumo();

	/**
	 * @param valorConsumo
	 *            the valorConsumo to set
	 */
	void setValorConsumo(BigDecimal valorConsumo);

	/**
	 * @return the desconto
	 */
	BigDecimal getDesconto();

	/**
	 * @param desconto
	 *            the desconto to set
	 */
	void setDesconto(BigDecimal desconto);

}
