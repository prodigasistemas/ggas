/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

import java.util.Collection;
/**
 * 
 * Interface responsável pelos métodos relacionados
 * ao controlador de anormalidade de faturamento
 *
 */
public interface ControladorFaturamentoAnormalidade extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE = "controladorFaturamentoAnormalidade";

	/**
	 * Método responsável por obter um
	 * FaturamentoAnormalidade de acordo com a
	 * classe passada.
	 * 
	 * @param chaveAnormalidade
	 *            chave da anormalidade
	 * @return FaturamentoAnormalidade respectivo
	 *         à classe passada
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	FaturamentoAnormalidade obterAnormalidadePorClasse(String chaveAnormalidade) throws NegocioException;

	/**
	 * Método responsável por listar anormalidades
	 * de faturamento.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<FaturamentoAnormalidade> listaAnormalidadeFaturamento() throws NegocioException;

	/**
	 * Método que lista as anormalidades de faturamento filtrando opcionalmente por descrição ou por habilitado
	 * @param descricao parte de descrição para realizar busca
	 * @param habilitado booleano para filtrar se está habilitado
	 * @param indicadorImpedeFaturamento indica se a anormalidade impede ou não o faturamento
	 * @return lista de faturamento
	 * @throws NegocioException lançado se ocorrer erro ao buscar
	 */
	Collection<FaturamentoAnormalidade> listaAnormalidadeFaturamento(String descricao, Boolean habilitado, Boolean indicadorImpedeFaturamento)
			throws NegocioException;

}
