/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.rubrica.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.rubrica.NomeclaturaComumMercosul;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.rubrica.RubricaValorRegulamentado;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos relacionados
 * a Rubrica
 * 
 */
public class RubricaImpl extends EntidadeNegocioImpl implements Rubrica {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6343609065386838116L;

	private FinanciamentoTipo financiamentoTipo;

	private LancamentoItemContabil lancamentoItemContabil;

	private EntidadeConteudo itemFatura;

	private String descricao;

	private String descricaoImpressao;

	private BigDecimal valorReferencia;

	private BigDecimal valorMaximo;

	private Integer numeroMaximoParcela;

	private BigDecimal percentualMinimoEntrada;

	private boolean indicadorEntradaObrigatoria;

	private boolean permiteDescricaoComplementar;

	private boolean indicadorCobrancaMulta;

	private boolean indicadorCobrancaJuros;

	private Boolean indicadorUsoExclusivoSistema;

	private Boolean indicadorRegulamentada;

	private Boolean indicadorComposicaoNotaFiscal;

	private EntidadeConteudo amortizacao;

	private Unidade unidade;

	private NomeclaturaComumMercosul nomeclaturaComumMercosul;

	private Collection<RubricaTributo> tributos = new HashSet<RubricaTributo>();

	private Collection<RubricaValorRegulamentado> valorRegulamentados = new HashSet<RubricaValorRegulamentado>();
	
	private String codigoCEST;
	
	private Boolean indicadorItemFaturamento;
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica
	 * #getAmortizacao()
	 */
	@Override
	public EntidadeConteudo getAmortizacao() {

		return amortizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica#
	 * setAmortizacao(br.com
	 * .procenge.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setAmortizacao(EntidadeConteudo amortizacao) {

		this.amortizacao = amortizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica
	 * #getUnidade()
	 */
	@Override
	public Unidade getUnidade() {

		return unidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica#
	 * setUnidade(br.com.procenge
	 * .ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setUnidade(Unidade unidade) {

		this.unidade = unidade;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.rubrica.Rubrica#
	 * getIndicadorUsoExclusivoSistema()
	 */
	@Override
	public Boolean getIndicadorUsoExclusivoSistema() {

		return indicadorUsoExclusivoSistema;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.rubrica.Rubrica#
	 * setIndicadorUsoExclusivoSistema(java.lang.
	 * Boolean)
	 */
	@Override
	public void setIndicadorUsoExclusivoSistema(Boolean indicadorUsoExclusivoSistema) {

		this.indicadorUsoExclusivoSistema = indicadorUsoExclusivoSistema;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica#
	 * getIndicadorRegulamentada
	 * ()
	 */
	@Override
	public boolean getPermiteDescricaoComplementar() {

		return permiteDescricaoComplementar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica#
	 * getIndicadorRegulamentada
	 * ()
	 */
	@Override
	public void setPermiteDescricaoComplementar(boolean permiteDescricaoComplementar) {

		this.permiteDescricaoComplementar = permiteDescricaoComplementar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica#
	 * getIndicadorRegulamentada
	 * ()
	 */
	@Override
	public Boolean getIndicadorRegulamentada() {

		return indicadorRegulamentada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.Rubrica#
	 * setIndicadorRegulamentada
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorRegulamentada(Boolean indicadorRegulamentada) {

		this.indicadorRegulamentada = indicadorRegulamentada;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.rubrica.Rubrica#
	 * getIndicadorComposicaoNotaFiscal()
	 */
	@Override
	public Boolean getIndicadorComposicaoNotaFiscal() {

		return indicadorComposicaoNotaFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.rubrica.Rubrica#
	 * setIndicadorComposicaoNotaFiscal(java.lang.
	 * Boolean)
	 */
	@Override
	public void setIndicadorComposicaoNotaFiscal(Boolean indicadorComposicaoNotaFiscal) {

		this.indicadorComposicaoNotaFiscal = indicadorComposicaoNotaFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Rubrica#
	 * getFinanciamentoTipo()
	 */
	@Override
	public FinanciamentoTipo getFinanciamentoTipo() {

		return financiamentoTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setFinanciamentoTipo(br
	 * .com.procenge.ggas.faturamento.
	 * FinanciamentoTipo)
	 */
	@Override
	public void setFinanciamentoTipo(FinanciamentoTipo financiamentoTipo) {

		this.financiamentoTipo = financiamentoTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getLancamentoItemContabil()
	 */
	@Override
	public LancamentoItemContabil getLancamentoItemContabil() {

		return lancamentoItemContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setLancamentoItemContabil
	 * (br.com.ggas.faturamento.LancamentoItemContabil
	 * )
	 */
	@Override
	public void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil) {

		this.lancamentoItemContabil = lancamentoItemContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Rubrica#
	 * getItemFatura()
	 */
	@Override
	public EntidadeConteudo getItemFatura() {

		return itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setItemFatura(br.com.procenge
	 * .ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setItemFatura(EntidadeConteudo itemFatura) {

		this.itemFatura = itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Rubrica#
	 * getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setDescricao(java.lang.
	 * String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoImpressao() {

		return descricaoImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setDescricaoAbreviada(java
	 * .lang.String)
	 */
	@Override
	public void setDescricaoImpressao(String descricaoImpressao) {

		this.descricaoImpressao = descricaoImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Rubrica#
	 * getValorReferencia()
	 */
	@Override
	public BigDecimal getValorReferencia() {

		return valorReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setValorReferencia(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setValorReferencia(BigDecimal valorReferencia) {

		this.valorReferencia = valorReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.impl.Rubrica#
	 * getValorMaximo()
	 */
	@Override
	public BigDecimal getValorMaximo() {

		return valorMaximo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setValorMaximo(java.math
	 * .BigDecimal)
	 */
	@Override
	public void setValorMaximo(BigDecimal valorMaximo) {

		this.valorMaximo = valorMaximo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getNumeroMaximoParcela()
	 */
	@Override
	public Integer getNumeroMaximoParcela() {

		return numeroMaximoParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setNumeroMaximoParcela(
	 * java.lang.Integer)
	 */
	@Override
	public void setNumeroMaximoParcela(Integer numeroMaximoParcela) {

		this.numeroMaximoParcela = numeroMaximoParcela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getPercentualMinimoEntrada
	 * ()
	 */
	@Override
	public BigDecimal getPercentualMinimoEntrada() {

		return percentualMinimoEntrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setPercentualMinimoEntrada
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualMinimoEntrada(BigDecimal percentualMinimoEntrada) {

		this.percentualMinimoEntrada = percentualMinimoEntrada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getIndicadorEntradaObrigatoria
	 * ()
	 */
	@Override
	public boolean getIndicadorEntradaObrigatoria() {

		return indicadorEntradaObrigatoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setIndicadorEntradaObrigatoria
	 * (boolean)
	 */
	@Override
	public void setIndicadorEntradaObrigatoria(boolean indicadorEntradaObrigatoria) {

		this.indicadorEntradaObrigatoria = indicadorEntradaObrigatoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getIndicadorCobrancaMulta()
	 */
	@Override
	public boolean getIndicadorCobrancaMulta() {

		return indicadorCobrancaMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setIndicadorCobrancaMulta
	 * (boolean)
	 */
	@Override
	public void setIndicadorCobrancaMulta(boolean indicadorCobrancaMulta) {

		this.indicadorCobrancaMulta = indicadorCobrancaMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getIndicadorCobrancaJuros()
	 */
	@Override
	public boolean getIndicadorCobrancaJuros() {

		return indicadorCobrancaJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setIndicadorCobrancaJuros
	 * (boolean)
	 */
	@Override
	public void setIndicadorCobrancaJuros(boolean indicadorCobrancaJuros) {

		this.indicadorCobrancaJuros = indicadorCobrancaJuros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setTributos
	 * (Collection<RubricaTributo>)
	 */
	@Override
	public void setTributos(Collection<RubricaTributo> tributos) {

		this.tributos = tributos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getTributos()
	 */
	@Override
	public Collection<RubricaTributo> getTributos() {

		return tributos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * setTributos
	 * (Collection<RubricaTributo>)
	 */
	@Override
	public void setValorRegulamentados(Collection<RubricaValorRegulamentado> valorRegulamentados) {

		this.valorRegulamentados = valorRegulamentados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.impl.Rubrica#
	 * getTributos()
	 */
	@Override
	public Collection<RubricaValorRegulamentado> getValorRegulamentados() {

		return valorRegulamentados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados
	 * ()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(RUBRICA_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricaoImpressao)) {
			stringBuilder.append(RUBRICA_DESCRICAO_IMPRESSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorRegulamentada == null) {
			stringBuilder.append(RUBRICA_REGULAMENTADA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(indicadorRegulamentada && valorReferencia == null) {
				stringBuilder.append(RUBRICA_VALOR_REFERENCIA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if(unidade == null) {
			stringBuilder.append(RUBRICA_UNIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(lancamentoItemContabil == null) {
			stringBuilder.append(RUBRICA_LANCAMENTO_ITEM_CONTABIL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorUsoExclusivoSistema == null) {
			stringBuilder.append(RUBRICA_EXCLUSIVO_SISTEMA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorComposicaoNotaFiscal == null) {
			stringBuilder.append(RUBRICA_COMPOSICAO_NF);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(financiamentoTipo == null && itemFatura == null) {
			stringBuilder.append(RUBRICA_ITEM_FATURA_OU_TIPO_RUBRICA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;

	}

	@Override
	public void setNomeclaturaComumMercosul(NomeclaturaComumMercosul nomeclaturaComumMercosul) {

		this.nomeclaturaComumMercosul = nomeclaturaComumMercosul;
	}

	@Override
	public NomeclaturaComumMercosul getNomeclaturaComumMercosul() {

		return nomeclaturaComumMercosul;
	}
	
	/**
	 * Get o codigo cest.
	 *
	 * @return o codigo cest
	 */
	@Override
	public String getCodigoCEST() {

		return codigoCEST;
	}

	/**
	 * Set o codigo cest.
	 *
	 * @param codigoCEST o novo codigo cest
	 */
	@Override
	public void setCodigoCEST(String codigoCEST) {

		this.codigoCEST = codigoCEST;
	}

	/**
	 * @return the indicadorItemFaturamento
	 */
	public Boolean getIndicadorItemFaturamento() {

		return indicadorItemFaturamento;
	}

	/**
	 * @param indicadorItemFaturamento the indicadorItemFaturamento to set
	 */
	public void setIndicadorItemFaturamento(Boolean indicadorItemFaturamento) {

		this.indicadorItemFaturamento = indicadorItemFaturamento;
	}

}
