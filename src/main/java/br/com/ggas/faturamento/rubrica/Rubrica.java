/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.rubrica;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Interface responsável pela assinatura dos métodos relacionados a Rubrica
 *
 */
public interface Rubrica extends EntidadeNegocio {

	String RUBRICA_UNIDADE = "RUBRICA_UNIDADE";

	String BEAN_ID_RUBRICA = "rubrica";

	String RUBRICA_CAMPO_STRING = "RUBRICA";

	String RUBRICA_DESCRICAO = "RUBRICA_DESCRICAO";

	String RUBRICA_DESCRICAO_IMPRESSAO = "RUBRICA_DESCRICAO_IMPRESSAO";

	String RUBRICA_REGULAMENTADA = "RUBRICA_REGULAMENTADA";

	String RUBRICA_LANCAMENTO_ITEM_CONTABIL = "RUBRICA_LANCAMENTO_ITEM_CONTABIL";

	String RUBRICA_EXCLUSIVO_SISTEMA = "RUBRICA_EXCLUSIVO_SISTEMA";

	String RUBRICA_COMPOSICAO_NF = "RUBRICA_COMPOSICAO_NF";

	String RUBRICA_ITEM_FATURA = "RUBRICA_ITEM_FATURA";

	String RUBRICA_VALOR_REFERENCIA = "RUBRICA_VALOR_REFERENCIA";

	String RUBRICA_ITEM_FATURA_OU_TIPO_RUBRICA = "RUBRICA_ITEM_FATURA_OU_TIPO_RUBRICA";

	/**
	 * @return the financiamentoTipo
	 */
	FinanciamentoTipo getFinanciamentoTipo();

	/**
	 * @param financiamentoTipo
	 *            the financiamentoTipo to set
	 */
	void setFinanciamentoTipo(FinanciamentoTipo financiamentoTipo);

	/**
	 * @return the lancamentoItemContabil
	 */
	LancamentoItemContabil getLancamentoItemContabil();

	/**
	 * @param lancamentoItemContabil
	 *            the lancamentoItemContabil to
	 *            set
	 */
	void setLancamentoItemContabil(LancamentoItemContabil lancamentoItemContabil);

	/**
	 * @return the itemFatura
	 */
	EntidadeConteudo getItemFatura();

	/**
	 * @param itemFatura
	 *            the itemFatura to set
	 */
	void setItemFatura(EntidadeConteudo itemFatura);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the descricaoImpressao
	 */
	String getDescricaoImpressao();

	/**
	 * @param descricaoImpressao
	 *            the descricaoImpressao to set
	 */
	void setDescricaoImpressao(String descricaoImpressao);

	/**
	 * @return the valorReferencia
	 */
	BigDecimal getValorReferencia();

	/**
	 * @param valorReferencia
	 *            the valorReferencia to set
	 */
	void setValorReferencia(BigDecimal valorReferencia);

	/**
	 * @return the valorMaximo
	 */
	BigDecimal getValorMaximo();

	/**
	 * @param valorMaximo
	 *            the valorMaximo to set
	 */
	void setValorMaximo(BigDecimal valorMaximo);

	/**
	 * @return the numeroMaximoParcela
	 */
	Integer getNumeroMaximoParcela();

	/**
	 * @param numeroMaximoParcela
	 *            the numeroMaximoParcela to set
	 */
	void setNumeroMaximoParcela(Integer numeroMaximoParcela);

	/**
	 * @return the percentualMinimoEntrada
	 */
	BigDecimal getPercentualMinimoEntrada();

	/**
	 * @param percentualMinimoEntrada
	 *            the percentualMinimoEntrada to
	 *            set
	 */
	void setPercentualMinimoEntrada(BigDecimal percentualMinimoEntrada);

	/**
	 * @return the indicadorEntradaObrigatoria
	 */
	boolean getIndicadorEntradaObrigatoria();

	/**
	 * @param indicadorEntradaObrigatoria
	 *            the indicadorEntradaObrigatoria
	 *            to set
	 */
	void setIndicadorEntradaObrigatoria(boolean indicadorEntradaObrigatoria);

	/**
	 * @return the indicadorCobrancaMulta
	 */
	boolean getIndicadorCobrancaMulta();

	/**
	 * @param indicadorCobrancaMulta
	 *            the indicadorCobrancaMulta to
	 *            set
	 */
	void setIndicadorCobrancaMulta(boolean indicadorCobrancaMulta);

	/**
	 * @return the indicadorCobrancaJuros
	 */
	boolean getIndicadorCobrancaJuros();

	/**
	 * @param indicadorCobrancaJuros
	 *            the indicadorCobrancaJuros to
	 *            set
	 */
	void setIndicadorCobrancaJuros(boolean indicadorCobrancaJuros);

	/**
	 * @return the indicadorUsoExclusivoSistema
	 */
	Boolean getIndicadorUsoExclusivoSistema();

	/**
	 * @param indicadorUsoExclusivoSistema
	 *            the indicadorUsoExclusivoSistema
	 *            to set
	 */
	void setIndicadorUsoExclusivoSistema(Boolean indicadorUsoExclusivoSistema);

	/**
	 * @return the indicadorRegulamentada
	 */
	Boolean getIndicadorRegulamentada();

	/**
	 * @param indicadorRegulamentada
	 *            the indicadorRegulamentada to
	 *            set
	 */
	void setIndicadorRegulamentada(Boolean indicadorRegulamentada);

	/**
	 * @return the indicadorComposicaoNotaFiscal
	 */
	Boolean getIndicadorComposicaoNotaFiscal();

	/**
	 * @param permiteDescricaoComplementar - Set permite descrição complementar.
	 */
	void setPermiteDescricaoComplementar(boolean permiteDescricaoComplementar);

	/**
	 * @return
	 */
	boolean getPermiteDescricaoComplementar();

	/**
	 * @param indicadorComposicaoNotaFiscal
	 *            the
	 *            indicadorComposicaoNotaFiscal to
	 *            set
	 */
	void setIndicadorComposicaoNotaFiscal(Boolean indicadorComposicaoNotaFiscal);

	/**
	 * @return the amortizacao
	 */
	EntidadeConteudo getAmortizacao();

	/**
	 * @param amortizacao
	 *            the amortizacao to set
	 */
	void setAmortizacao(EntidadeConteudo amortizacao);

	/**
	 * @return the unidade
	 */
	Unidade getUnidade();

	/**
	 * @param unidade
	 *            the unidade to set
	 */
	void setUnidade(Unidade unidade);

	/**
	 * @param tributos - Set tributos.
	 */
	void setTributos(Collection<RubricaTributo> tributos);

	/**
	 * @return the unidade
	 */
	Collection<RubricaTributo> getTributos();

	/**
	 * @param valorRegulamentados - Set valor regulamentados.
	 */
	void setValorRegulamentados(Collection<RubricaValorRegulamentado> valorRegulamentados);

	/**
	 * @return the Valor Regulamentados
	 */
	Collection<RubricaValorRegulamentado> getValorRegulamentados();

	/**
	 * @param nomeclaturaComumMercosul - Set nomeclatura comum merco sul.
	 */
	void setNomeclaturaComumMercosul(NomeclaturaComumMercosul nomeclaturaComumMercosul);

	/**
	 * @return NomeclaraturaComumMercoSul - Retorna nomeclatura comum merco sul.
	 */
	NomeclaturaComumMercosul getNomeclaturaComumMercosul();
	
	/**
	 * @return o codigo cest
	 */
	String getCodigoCEST() ;
	
	 /**
	 * @param codigoCEST o novo codigo cest
	 */
	void setCodigoCEST(String codigoCEST) ;

	/**
	 * @return the indicadorItemFaturamento
	 */
	public Boolean getIndicadorItemFaturamento();

	/**
	 * @param indicadorItemFaturamento the indicadorItemFaturamento to set
	 */
	public void setIndicadorItemFaturamento(Boolean indicadorItemFaturamento);
	
}
