/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.rubrica;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao controlador
 * de rubrica
 * 
 *
 */
public interface ControladorRubrica extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_RUBRICA = "controladorRubrica";

	String ERRO_PERCENTUAL_MININO_ENTRADA_MAXIMO_100 = "ERRO_PERCENTUAL_MININO_ENTRADA_MAXIMO_100";

	String ERRO_DATA_DE_VIGENCIA_NAO_INFORMADA = "ERRO_DATA_DE_VIGENCIA_NAO_INFORMADA";

	String ERRO_NEGOCIO_RUBRICA_VINCULADA = "ERRO_NEGOCIO_RUBRICA_VINCULADA";

	String ERRO_VALOR_DE_REFERENCIA_NAO_INFORMADO = "ERRO_VALOR_DE_REFERENCIA_NAO_INFORMADO";

	String ERRO_RUBRICA_DESCRICAO_EXISTENTE = "ERRO_RUBRICA_DESCRICAO_EXISTENTE";

	String ERRO_VALOR_DE_REGULAMENTADO_INVALIDO = "ERRO_VALOR_DE_REGULAMENTADO_INVALIDO";

	String ERRO_DATA_VIGENCIA_MENOR_QUE_ULTIMA_DATA_INFORMADA = "ERRO_DATA_VIGENCIA_MENOR_QUE_ULTIMA_DATA_INFORMADA";

	String ERRO_DATA_VIGENCIA_INVALIDA = "ERRO_DATA_VIGENCIA_INVALIDA";

	String ERRO_DATA_INICIO_VIGENCIA_MENOR_ATUAL = "ERRO_DATA_INICIO_VIGENCIA_MENOR_ATUAL";

	String ERRO_VALOR_MAXIMO_MENOR_VALOR_REFERENCIA = "ERRO_VALOR_MAXIMO_MENOR_VALOR_REFERENCIA";

	String ERRO_RUBRICA_JA_VINCULADA_ITEM_FATURA = "ERRO_RUBRICA_JA_VINCULADA_ITEM_FATURA";

	String VALOR_REGULAMENTADO_ATIVO = "valorRegulamentadoAtivo";

	String ID_RUBRICA = "idRubrica";

	String CHAVES_PRIMARIAS = "chavesPrimarias";

	String TRUE = "true";

	String TRIBUTO_ATIVO = "tributoAtivo";

	String CHAVE_PRIMARIA = "chavePrimaria";

	String CHAVE_PRIMARIA_INVERSA = "chavePrimariaInversa";

	String HABILITADO = "habilitado";

	String INDICADOR_REGULAMENTADA = "indicadorRegulamentada";

	String ERROR_EXCEPTION = "ERROR";

	/**
	 * Consultar rubricas valor regulamentado.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<RubricaValorRegulamentado> consultarRubricasValorRegulamentado(Map<String, Object> filtro)
			throws NegocioException;

	/**
	 * Método responsável por verificar se as chaves foram selecionadas para a
	 * remoção de rubricas.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */

	Collection<RubricaTributo> consultarRubricasTributo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se as chaves foram selecionadas para a
	 * remoção de rubricas.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */

	Collection<Rubrica> consultarRubrica(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se as chaves foram selecionadas para a
	 * remoção de rubricas.
	 * 
	 * @param chavesPrimarias As chaves primárias das rubricas.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void validarRemoverRubrica(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * metodo responsavel por listar todos as entidades financiamento tipo do
	 * sistema.
	 * 
	 * @return uma colecao de financiamento tipo do sistema
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<FinanciamentoTipo> listarFinanciamentoTipo() throws NegocioException;

	/**
	 * metodo responsavel por listar todas as entidades lancamento item contabil do
	 * sistema.
	 * 
	 * @return uma coleção de item contabil do sistema
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<LancamentoItemContabil> listarLancamentoItemContabil() throws NegocioException;

	/**
	 * Método responsável por obter o tipo de financiamento.
	 * 
	 * @param chavePrimaria Chave Primaria
	 * @return FinanciamentoTipo Tipo do Financiamento
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	FinanciamentoTipo obterTipoFinanciamento(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o item de lançamento contábil.
	 * 
	 * @param chavePrimaria Chave Primaria
	 * @return LancamentoItemContabil o item de lançamento contábil.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	LancamentoItemContabil obterLancamentoItemContabil(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o item de lançamento contábil.
	 * 
	 * @return LancamentoItemContabil o item de lançamento contábil.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */

	RubricaValorRegulamentado criarRubricaValorRegulamentado() throws NegocioException;

	/**
	 * Método responsável por obter o item de lançamento contábil.
	 * 
	 * @return LancamentoItemContabil o item de lançamento contábil.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */

	RubricaTributo criarRubricaTributo() throws NegocioException;

	/**
	 * Atualizar rubrica.
	 * 
	 * @param rubrica the rubrica
	 * @throws NegocioException the negocio exception
	 */
	void atualizarRubrica(Rubrica rubrica) throws NegocioException;

	/**
	 * Inserir rubrica.
	 * 
	 * @param rubrica the rubrica
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long inserirRubrica(Rubrica rubrica) throws NegocioException;

	/**
	 * Método responsável por listar Rubricas na tela de Incluir Créditos Débitos a
	 * Realizar.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Rubrica> listarRubricas() throws NegocioException;

	/**
	 * Listar rubricas em uso sistema.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Rubrica> listarRubricasEmUsoSistema() throws NegocioException;

	/**
	 * Inserir rubrica valor regulamentado.
	 * 
	 * @param rubricaValorRegulamentado the rubrica valor regulamentado
	 * @throws NegocioException the negocio exception
	 */
	void inserirRubricaValorRegulamentado(RubricaValorRegulamentado rubricaValorRegulamentado) throws NegocioException;

	/**
	 * Atualizar rubrica valor regulamentado.
	 * 
	 * @param rubricaValorRegulamentado the rubrica valor regulamentado
	 * @throws NegocioException the negocio exception
	 */
	void atualizarRubricaValorRegulamentado(RubricaValorRegulamentado rubricaValorRegulamentado)
			throws NegocioException;

	/**
	 * Inserir rubrica tributo.
	 * 
	 * @param rubricaTributo the rubrica tributo
	 * @throws NegocioException the negocio exception
	 */
	void inserirRubricaTributo(RubricaTributo rubricaTributo) throws NegocioException;

	/**
	 * Atualizar rubrica tributo.
	 * 
	 * @param rubricaTributo the rubrica tributo
	 * @throws NegocioException the negocio exception
	 */
	void atualizarRubricaTributo(RubricaTributo rubricaTributo) throws NegocioException;

	/**
	 * Remover rubrica.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria  the dados auditoria
	 * @throws NegocioException the negocio exception
	 */
	void removerRubrica(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por listar rubricas levando em consideração o tipo de
	 * Crédito/Débito.
	 * 
	 * @param isTipoDebito the is tipo debito
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Rubrica> processarRubricasPortipoCreditoDebito(boolean isTipoDebito) throws NegocioException;

	/**
	 * Método responsável por obter a Rubrica de um FaturaItem.
	 * 
	 * @param idFaturaItem Chave primaria da FaturaItem.
	 * @return Uma Rubrica.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Rubrica obterRubricaPorFaturaItem(Long idFaturaItem) throws NegocioException;

	/**
	 * Método responsável por obter uma Rubrica por uma entidade conteúdo referente
	 * ao Item Fatura.
	 * 
	 * @param idItemFatura A chave da entidade conteúdo.
	 * @return Rubrica Uma Rubrica referente a entidade conteúdo.
	 * @throws NegocioException the negocio exception
	 */
	Rubrica obterRubricaPorItemFatura(Long idItemFatura) throws NegocioException;

	/**
	 * Método responsável por validar para i Item de fatura ter apenas uma rubrica.
	 * 
	 * @param rubrica the rubrica
	 * @return Rubrica Uma Rubrica referente a entidade conteúdo.
	 * @throws NegocioException the negocio exception
	 */
	void validaItemFarturaRubrica(Rubrica rubrica) throws NegocioException;

	/**
	 * método para obter a lista de RubricaTributo.
	 * 
	 * @param idRubrica the id rubrica
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<RubricaTributo> obterListaRubricaTributo(Long idRubrica) throws NegocioException;

	/**
	 * Método responsável por listar todas as NomeclaturaComumMercosul.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<NomeclaturaComumMercosul> listarNomeclaturaComumMercosul() throws NegocioException;

	/**
	 * Método responsável por obter NomeclaturaComumMercosul.
	 * 
	 * @param idNomeclaturaComumMercosul the id nomeclatura comum mercosul
	 * @return the nomeclatura comum mercosul
	 * @throws NegocioException the negocio exception
	 */
	NomeclaturaComumMercosul obterNomeclaturaComumMercosul(Long idNomeclaturaComumMercosul) throws NegocioException;

	/**
	 * Listar rubricas por tipo credito debito.
	 * 
	 * @param isTipoDebito the is tipo debito
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<Rubrica> listarRubricasPorTipoCreditoDebito(boolean isTipoDebito) throws NegocioException;

	/**
	 * Obtém o lançamento do item contábil e o tipo de financiamento da rubrica.
	 * @param chavePrimaria - {@link Long}
	 * @return {@link Rubrica}
	 */
	Rubrica obterLancamentoContabilEfinancimentoDeRubrica(Long chavePrimaria);

	/**
	 * Obtém a {@link Rubrica} por uma constante de sistema.
	 * @param constanteSistema - {@link ConstanteSistema}
	 * @return {@link Rubrica}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Rubrica obterRubricaSubstituicaoTributaria(ConstanteSistema constanteSistema)
			throws NegocioException;

	/**
	 * Obtém a {@link Rubrica} por chave primária.
	 * @param chavePrimaria - {@link Long}
	 * @return {@link Rubrica}
	 */
	Rubrica obterRubrica(Long chavePrimaria);
}
