/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe <nome> representa uma <nome> no sistema.
 *
 * @since data atual
 *
 */

package br.com.ggas.faturamento.rubrica.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FinanciamentoTipo;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.NomeclaturaComumMercosul;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.rubrica.RubricaValorRegulamentado;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 *
 */
class ControladorRubricaImpl extends ControladorNegocioImpl implements ControladorRubrica {

	private static final int UM_DIA_EM_MILISEGUNDOS = 86400000;
	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Rubrica.BEAN_ID_RUBRICA);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public RubricaValorRegulamentado criarRubricaValorRegulamentado() {

		return (RubricaValorRegulamentado) ServiceLocator.getInstancia().getBeanPorID(
						RubricaValorRegulamentado.BEAN_ID_RUBRICA_VALOR_REGULAMENTADO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public RubricaTributo criarRubricaTributo() {

		return (RubricaTributo) ServiceLocator.getInstancia().getBeanPorID(RubricaTributo.BEAN_ID_RUBRICA_TRIBUTO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Rubrica.BEAN_ID_RUBRICA);
	}

	public Class<?> getClasseEntidadeFaturaItem() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaItem.BEAN_ID_FATURA_ITEM);
	}

	public Class<?> getClasseEntidadeFinanciamentoTipo() {

		return ServiceLocator.getInstancia().getClassPorID(FinanciamentoTipo.BEAN_ID_FINANCIAMENTO_TIPO);
	}

	public Class<?> getClasseEntidadeLancamentoItemContabil() {

		return ServiceLocator.getInstancia().getClassPorID(LancamentoItemContabil.BEAN_ID_LANCAMENTO_ITEM_CONTABIL);
	}

	public Class<?> getClasseEntidadeRubricaValorRegulamentado() {

		return ServiceLocator.getInstancia().getClassPorID(RubricaValorRegulamentado.BEAN_ID_RUBRICA_VALOR_REGULAMENTADO);
	}

	public Class<?> getClasseEntidadeRubricaTributo() {

		return ServiceLocator.getInstancia().getClassPorID(RubricaTributo.BEAN_ID_RUBRICA_TRIBUTO);
	}

	public Class<?> getClasseEntidadeCreditoDebitoNegociado() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoNegociado.BEAN_ID_CREDITO_DEBITO_NEGOCIADO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica#consultarRubrica
	 * (java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Rubrica> consultarRubrica(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long chavePrimariaInversa = (Long) filtro.get("chavePrimariaInversa");
			if (chavePrimariaInversa != null && chavePrimariaInversa > 0) {
				criteria.add(Restrictions.ne(CHAVE_PRIMARIA, chavePrimariaInversa));
			}

			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			String descricaoImpressao = (String) filtro.get("descricaoImpressao");
			if (!StringUtils.isEmpty(descricaoImpressao)) {
				criteria.add(Restrictions.ilike("descricaoImpressao", Util.formatarTextoConsulta(descricaoImpressao)));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

			Boolean indicadorUsoExclusivoSistema = (Boolean) filtro.get("indicadoUsoExclusivoSistema");
			if (indicadorUsoExclusivoSistema != null) {
				criteria.add(Restrictions.eq("indicadorUsoExclusivoSistema", indicadorUsoExclusivoSistema));
			}

			Boolean indicadorRegulamentada = (Boolean) filtro.get(INDICADOR_REGULAMENTADA);
			if (indicadorRegulamentada != null) {
				criteria.add(Restrictions.eq(INDICADOR_REGULAMENTADA, indicadorRegulamentada));
			}

			criteria.setFetchMode("financiamentoTipo", FetchMode.JOIN);
		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica#consultarRubrica
	 * (java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RubricaTributo> consultarRubricasTributo(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeRubricaTributo());
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long[] chavesPrimariasRubrica = (Long[]) filtro.get("chavesPrimariasRubrica");
			if (chavesPrimariasRubrica != null && chavesPrimariasRubrica.length > 0) {
				criteria.add(Restrictions.in("rubrica.chavePrimaria", chavesPrimariasRubrica));
			}

			Rubrica rubrica = (Rubrica) filtro.get("idRubrica");
			if (rubrica != null) {
				criteria.add(Restrictions.eq("rubrica.chavePrimaria", rubrica.getChavePrimaria()));
				criteria.setFetchMode("tributo", FetchMode.JOIN);
			}

			Tributo tributo = (Tributo) filtro.get("tributo");
			if (tributo != null) {
				criteria.add(Restrictions.eq("tributo.chavePrimaria", tributo.getChavePrimaria()));
			}

			Boolean bAtivo = (Boolean) filtro.get(TRIBUTO_ATIVO);
			if (bAtivo != null && bAtivo.equals(true)) {
				criteria.add(Restrictions.or(Restrictions.isNull("dataFimVigencia"),
								Restrictions.ge("dataFimVigencia", Calendar.getInstance().getTime())));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #validarRemoverRubrica(java.lang.Long[])
	 */
	@Override
	public void validarRemoverRubrica(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Rubrica rubrica = (Rubrica) entidadeNegocio;
		Integer quantidade = consultarQuantidadeFaturaItemPorRubrica(rubrica.getChavePrimaria());
		if (quantidade > 0) {
			throw new NegocioException(ERRO_NEGOCIO_RUBRICA_VINCULADA, true);
		}
	}

	/**
	 * Consultar quantidade fatura item por rubrica.
	 *
	 * @param chaveRubrica
	 *            the chave rubrica
	 * @return the integer
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Integer consultarQuantidadeFaturaItemPorRubrica(Long chaveRubrica) throws NegocioException {

		Query query = null;
		Long resultado = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(" count(faturaItem.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" faturaItem ");
		hql.append(" where ");
		hql.append(" faturaItem.rubrica.chavePrimaria = :chaveRubrica ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveRubrica", chaveRubrica);
		resultado = (Long) query.uniqueResult();
		return resultado.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.rubrica.
	 * ControladorRubrica#
	 * listarFinanciamentoTipo()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<FinanciamentoTipo> listarFinanciamentoTipo() throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select financiamentoTipo ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFinanciamentoTipo().getSimpleName());
		hql.append(" financiamentoTipo ");
		hql.append(" order by descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.rubrica.
	 * ControladorRubrica#
	 * listarLancamentoItemContabil()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LancamentoItemContabil> listarLancamentoItemContabil() throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select itemContabil ");
		hql.append(" from ");
		hql.append(getClasseEntidadeLancamentoItemContabil().getSimpleName());
		hql.append(" itemContabil ");

		hql.append(" order by descricao asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #obterTipoFinanciamento(long)
	 */
	@Override
	public FinanciamentoTipo obterTipoFinanciamento(long chavePrimaria) throws NegocioException {

		return (FinanciamentoTipo) super.obter(chavePrimaria, getClasseEntidadeFinanciamentoTipo());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #obterLancamentoItemContabil(long)
	 */
	@Override
	public LancamentoItemContabil obterLancamentoItemContabil(long chavePrimaria) throws NegocioException {

		return (LancamentoItemContabil) super.obter(chavePrimaria, getClasseEntidadeLancamentoItemContabil());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.ControladorRubrica#validaItemFarturaRubrica(br.com.ggas.faturamento.rubrica.Rubrica)
	 */
	@Override
	public void validaItemFarturaRubrica(Rubrica rubrica) throws NegocioException {

		if (rubrica.getItemFatura() != null) {
			Integer rubricaLocalizada = obterQuantidadeRubricaPorItemFatura(rubrica.getItemFatura().getChavePrimaria(), rubrica.getChavePrimaria());
			if (rubricaLocalizada > 0) {
				throw new NegocioException(ERRO_RUBRICA_JA_VINCULADA_ITEM_FATURA, true);
			}
		}
	}

	/**
	 * Validar dados.
	 *
	 * @param rubrica
	 *            the rubrica
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDados(Rubrica rubrica) throws NegocioException {

		Map<String, Object> validarDadosRubrica = null;
		if (rubrica != null) {
			validarDadosRubrica = rubrica.validarDados();
		} else {
			throw new NegocioException(Constantes.RUBRICA_TRIBUTO);
		}
		StringBuilder stringBuilder = new StringBuilder();

		if (validarDadosRubrica != null && !validarDadosRubrica.isEmpty()) {
			stringBuilder.append(validarDadosRubrica.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS));
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (rubrica.getIndicadorRegulamentada().equals(true) && rubrica.getValorRegulamentados() != null) {
			for (RubricaValorRegulamentado rubricaValorRegulamentado : rubrica.getValorRegulamentados()) {
				Map<String, Object> validarDadosRubricaValorRegulamentado = rubricaValorRegulamentado.validarDados();
				if ((validarDadosRubricaValorRegulamentado != null) && (!validarDadosRubricaValorRegulamentado.isEmpty())) {
					stringBuilder.append(validarDadosRubricaValorRegulamentado.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS));
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		if (rubrica.getTributos() != null) {
			for (RubricaTributo rubricaTributo : rubrica.getTributos()) {
				Map<String, Object> validarDadosRubricaTributo = rubricaTributo.validarDados();
				if ((validarDadosRubricaTributo != null) && (!validarDadosRubricaTributo.isEmpty())) {
					stringBuilder.append(validarDadosRubricaTributo.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS));
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		if (stringBuilder.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, stringBuilder.toString().substring(0,
							stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if (rubrica.getIndicadorRegulamentada().equals(true) && rubrica.getValorRegulamentados() != null) {

			int qtdLinhasRubricaValorRegulamentado = rubrica.getValorRegulamentados().size();
			if (qtdLinhasRubricaValorRegulamentado > 0) {
				RubricaValorRegulamentado[] rubricasValorRegulamentado = new RubricaValorRegulamentado[qtdLinhasRubricaValorRegulamentado];
				rubrica.getValorRegulamentados().toArray(rubricasValorRegulamentado);

				boolean novaRubrica = rubrica.getChavePrimaria() < 1;
				if (rubrica.getValorRegulamentados().size() != 1) {
					throw new NegocioException(ERRO_VALOR_DE_REGULAMENTADO_INVALIDO, true);
				} else if(novaRubrica) {
					this.validarDataDeInicioVigencia(rubrica);
				}

			} else {
				throw new NegocioException(ERRO_DATA_DE_VIGENCIA_NAO_INFORMADA, true);
			}

		} else if (rubrica.getIndicadorRegulamentada().equals(true) && rubrica.getValorRegulamentados() == null) {
			throw new NegocioException(ERRO_DATA_DE_VIGENCIA_NAO_INFORMADA, true);
		}

		if (rubrica.getFinanciamentoTipo() != null && rubrica.getPercentualMinimoEntrada() != null &&
						rubrica.getPercentualMinimoEntrada().doubleValue() > 1) {

			throw new NegocioException(ERRO_PERCENTUAL_MININO_ENTRADA_MAXIMO_100, true);

		}

		if (rubrica.getDescricao() != null && !rubrica.getDescricao().isEmpty()) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			if (rubrica.getChavePrimaria() != 0) {
				filtro.put(CHAVE_PRIMARIA_INVERSA, rubrica.getChavePrimaria());
			}
			filtro.put(HABILITADO, Boolean.valueOf(TRUE));

			Collection<Rubrica> rubricasBanco = consultarRubrica(filtro);
			if (!rubricasBanco.isEmpty()) {
				this.validarRubricaBancoExistente(rubricasBanco, rubrica);
			}
		}

		if (rubrica.getIndicadorRegulamentada() != null && !rubrica.getIndicadorRegulamentada() &&
						rubrica.getValorReferencia() != null && rubrica.getValorMaximo() != null &&
						rubrica.getValorMaximo().compareTo(rubrica.getValorReferencia()) < 0) {

			throw new NegocioException(ERRO_VALOR_MAXIMO_MENOR_VALOR_REFERENCIA, true);
		}
	}

	private void validarRubricaBancoExistente(Collection<Rubrica> rubricasBanco, Rubrica rubrica) throws NegocioException {
		for (Rubrica rubricaBanco : rubricasBanco) {
			if (rubricaBanco.getDescricao().trim().equalsIgnoreCase(rubrica.getDescricao().trim())) {
				throw new NegocioException(ERRO_RUBRICA_DESCRICAO_EXISTENTE, true);
			}
		}
	}

	private void validarDataDeInicioVigencia(Rubrica rubrica) throws NegocioException {
		for (RubricaValorRegulamentado rubricaValorRegulamentado : rubrica.getValorRegulamentados()) {
			if (rubricaValorRegulamentado.getDataInicioVigencia() != null
					&& Util.compararDatas(rubricaValorRegulamentado.getDataInicioVigencia(), Calendar.getInstance().getTime()) < 0) {
				throw new NegocioException(ERRO_DATA_INICIO_VIGENCIA_MENOR_ATUAL, true);
			}
		}
	}


	/**
	 * Inserir rubrica tributo.
	 *
	 * @param rubricaTributo
	 *            the rubrica tributo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void inserirRubricaTributo(RubricaTributo rubricaTributo) throws NegocioException {

		this.inserir(rubricaTributo);
	}

	/**
	 * Atualizar rubrica tributo.
	 *
	 * @param rubricaTributo
	 *            the rubrica tributo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void atualizarRubricaTributo(RubricaTributo rubricaTributo) throws NegocioException {

		try {
			atualizar(rubricaTributo, this.getClasseEntidadeRubricaTributo());
		} catch (ConcorrenciaException e) {
			LOG.error(e.getStackTrace(), e);
			Logger.getLogger(ERROR_EXCEPTION).debug(e.getStackTrace());
		}
	}

	/**
	 * Atualizar rubricas tributo.
	 *
	 * @param rubrica
	 *            the rubrica
	 * @param rubricasTributo
	 *            the rubricas tributo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarRubricasTributo(Rubrica rubrica, List<RubricaTributo> rubricasTributo) throws NegocioException {

		if (rubricasTributo != null && rubrica != null) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_RUBRICA, rubrica);
			filtro.put(TRIBUTO_ATIVO, Boolean.valueOf(TRUE));

			Collection<RubricaTributo> rubricasTributoBanco = this.consultarRubricasTributo(filtro);

			// Inserir os novos no banco
			Boolean bflagTributo;
			for (RubricaTributo rubricaTributo : rubricasTributo) {
				bflagTributo = false;

				for (RubricaTributo rubricaTributoBanco : rubricasTributoBanco) {
					if (rubricaTributo.getTributo().getChavePrimaria() == rubricaTributoBanco.getTributo().getChavePrimaria()) {
						bflagTributo = true;
					}
				}

				if (!bflagTributo) {
					inserirRubricaTributo(rubricaTributo);
				}
			}

			// desabilitar os existente
			for (RubricaTributo rubricaTributoBanco : rubricasTributoBanco) {
				bflagTributo = false;

				for (RubricaTributo rubricaTributo : rubricasTributo) {
					if (rubricaTributo.getTributo().getChavePrimaria() == rubricaTributoBanco.getTributo().getChavePrimaria()) {
						bflagTributo = true;
					}
				}

				if (!bflagTributo) {
					rubricaTributoBanco.setDadosAuditoria(rubrica.getDadosAuditoria());
					rubricaTributoBanco.setDataFimVigencia(DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime()));

					atualizarRubricaTributo(rubricaTributoBanco);
				}
			}
		}
	}

	/**
	 * Atualizar rubrica valor regulamentado.
	 *
	 * @param rubricaValorRegulamentado
	 *            the rubrica valor regulamentado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void atualizarRubricaValorRegulamentado(RubricaValorRegulamentado rubricaValorRegulamentado) throws NegocioException {

		try {
			atualizar(rubricaValorRegulamentado, this.getClasseEntidadeRubricaValorRegulamentado());
		} catch (ConcorrenciaException e) {
			LOG.error(e.getStackTrace(), e);
			Logger.getLogger(ERROR_EXCEPTION).debug(e.getStackTrace());
		}
	}

	/**
	 * Inserir rubrica valor regulamentado.
	 *
	 * @param rubricaValorRegulamentado
	 *            the rubrica valor regulamentado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void inserirRubricaValorRegulamentado(RubricaValorRegulamentado rubricaValorRegulamentado) throws NegocioException {

		this.inserir(rubricaValorRegulamentado);
	}

	/**
	 * Atualizar rubricas valor regulamentado.
	 *
	 * @param rubrica
	 *            the rubrica
	 * @param rubricasValorRegulamentado
	 *            the rubricas valor regulamentado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarRubricasValorRegulamentado(Rubrica rubrica, List<RubricaValorRegulamentado> rubricasValorRegulamentado)
					throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(ID_RUBRICA, rubrica.getChavePrimaria());

		// Peguntar se pode ser inválido
		// habilitado

		Collection<RubricaValorRegulamentado> rubricasValorRegulamentadoBanco = this.consultarRubricasValorRegulamentado(filtro);
		if (rubricasValorRegulamentadoBanco != null && !rubricasValorRegulamentadoBanco.isEmpty()) {
			if (rubricasValorRegulamentado != null && !rubricasValorRegulamentado.isEmpty()) {
				RubricaValorRegulamentado rubricaValorRegulamentadoAtual = null;

				RubricaValorRegulamentado rubricaValorRegulamentadoBancoAtual =
						this.atualizarRubricaValorRegulamentadoBancoAtual(rubricasValorRegulamentadoBanco);

				for (RubricaValorRegulamentado rubricaValorRegulamentado : rubricasValorRegulamentado) {
					rubricaValorRegulamentadoAtual = rubricaValorRegulamentado;
				}

				if (rubricaValorRegulamentadoAtual != null && rubricaValorRegulamentadoBancoAtual != null) {
					if (rubricaValorRegulamentadoBancoAtual.getDataFimVigencia() == null) {
						// Validando com
						// referencia na data de
						// inicio de vigência
						if (!Util.converterDataParaStringSemHora(rubricaValorRegulamentadoBancoAtual.getDataInicioVigencia(),
										Constantes.FORMATO_DATA_BR).equals(
										Util.converterDataParaStringSemHora(rubricaValorRegulamentadoAtual.getDataInicioVigencia(),
														Constantes.FORMATO_DATA_BR))) {

							if (rubricaValorRegulamentadoBancoAtual.getDataInicioVigencia().before(
											rubricaValorRegulamentadoAtual.getDataInicioVigencia())) {
								rubricaValorRegulamentadoBancoAtual.setDadosAuditoria(rubrica.getDadosAuditoria());

								rubricaValorRegulamentadoBancoAtual.setDataFimVigencia(new Date(rubricaValorRegulamentadoAtual
												.getDataInicioVigencia().getTime() - UM_DIA_EM_MILISEGUNDOS));

								atualizarRubricaValorRegulamentado(rubricaValorRegulamentadoBancoAtual);

								inserirRubricaValorRegulamentado(rubricaValorRegulamentadoAtual);

							} else {
								throw new NegocioException(ERRO_DATA_VIGENCIA_MENOR_QUE_ULTIMA_DATA_INFORMADA, true);
							}
						} else {
							rubricaValorRegulamentadoBancoAtual.setDadosAuditoria(rubrica.getDadosAuditoria());
							rubricaValorRegulamentadoBancoAtual.setValorReferencia(rubricaValorRegulamentadoAtual.getValorReferencia());
							rubricaValorRegulamentadoBancoAtual.setObservacao(rubricaValorRegulamentadoAtual.getObservacao());

							atualizarRubricaValorRegulamentado(rubricaValorRegulamentadoBancoAtual);
						}
					} else {

						// Validando com
						// referencia na data de
						// fim de vigência
						if (rubricaValorRegulamentadoBancoAtual.getDataFimVigencia().before(
										rubricaValorRegulamentadoAtual.getDataInicioVigencia())
										&& rubricaValorRegulamentadoBancoAtual.getDataFimVigencia().compareTo(
														rubricaValorRegulamentadoAtual.getDataInicioVigencia()) != 0) {
							inserirRubricaValorRegulamentado(rubricaValorRegulamentadoAtual);
						} else {
							throw new NegocioException(ERRO_DATA_VIGENCIA_MENOR_QUE_ULTIMA_DATA_INFORMADA, true);
						}
					}
				}
			} else {
				this.atualizarDataFimVigenciaDeRubricaValorRegulamentadoBanco(rubricasValorRegulamentadoBanco, rubrica);
			}
		} else {
			if (rubricasValorRegulamentado != null && !rubricasValorRegulamentado.isEmpty()) {
				for (RubricaValorRegulamentado rubricaValorRegulamentado : rubricasValorRegulamentado) {
					inserirRubricaValorRegulamentado(rubricaValorRegulamentado);
				}
			}
		}
	}

	private void atualizarDataFimVigenciaDeRubricaValorRegulamentadoBanco(
			Collection<RubricaValorRegulamentado> rubricasValorRegulamentadoBanco, Rubrica rubrica) throws NegocioException {
		for (RubricaValorRegulamentado rubricaValorRegulamentadoBanco : rubricasValorRegulamentadoBanco) {
			if (rubricaValorRegulamentadoBanco.getDataFimVigencia() == null) {
				rubricaValorRegulamentadoBanco.setDadosAuditoria(rubrica.getDadosAuditoria());

				if (rubricaValorRegulamentadoBanco.getDataInicioVigencia().before(DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime()))) {
					rubricaValorRegulamentadoBanco.setDataFimVigencia(DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime()));
				} else {
					rubricaValorRegulamentadoBanco.setDataFimVigencia(rubricaValorRegulamentadoBanco.getDataInicioVigencia());
				}

				atualizarRubricaValorRegulamentado(rubricaValorRegulamentadoBanco);
			}
		}
	}

	private RubricaValorRegulamentado atualizarRubricaValorRegulamentadoBancoAtual(
			Collection<RubricaValorRegulamentado> rubricasValorRegulamentadoBanco) {
		RubricaValorRegulamentado rubricaValorRegulamentadoBancoAtual = null;

		for (RubricaValorRegulamentado rubricaValorRegulamentadoBanco : rubricasValorRegulamentadoBanco) {
			if (rubricaValorRegulamentadoBancoAtual == null) {
				rubricaValorRegulamentadoBancoAtual = rubricaValorRegulamentadoBanco;
			} else {
				if (rubricaValorRegulamentadoBancoAtual.getDataInicioVigencia().before(
						rubricaValorRegulamentadoBanco.getDataInicioVigencia())) {
					rubricaValorRegulamentadoBancoAtual = rubricaValorRegulamentadoBanco;
				}
			}
		}
		return rubricaValorRegulamentadoBancoAtual;
	}

	/**
	 * Remover rubrica.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void removerRubrica(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<Rubrica> listasRubrica = this.consultarRubrica(filtro);
			if ((listasRubrica != null) && (!listasRubrica.isEmpty())) {
				for (Rubrica rubrica : listasRubrica) {
					rubrica.setDadosAuditoria(dadosAuditoria);
					super.remover(rubrica, false);
				}
			}
		}
	}

	/**
	 * Atualizar rubrica.
	 *
	 * @param rubrica
	 *            the rubrica
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void atualizarRubrica(Rubrica rubrica) throws NegocioException {

		validarDados(rubrica);

		List<RubricaValorRegulamentado> rubricasValorRegulamentado = new ArrayList<RubricaValorRegulamentado>();
		if (rubrica.getValorRegulamentados() != null) {
			rubricasValorRegulamentado.addAll(rubrica.getValorRegulamentados());
			rubrica.getValorRegulamentados().clear();
		}

		List<RubricaTributo> rubricasTributo = new ArrayList<RubricaTributo>();
		if (rubrica.getTributos() != null) {
			rubricasTributo.addAll(rubrica.getTributos());
			rubrica.getTributos().clear();
		}

		try {
			atualizar(rubrica);
			atualizarRubricasValorRegulamentado(rubrica, rubricasValorRegulamentado);
			atualizarRubricasTributo(rubrica, rubricasTributo);
		} catch (ConcorrenciaException e) {
			LOG.error(e.getStackTrace(), e);
			Logger.getLogger(ERROR_EXCEPTION).debug(e.getStackTrace());
		}

	}

	/**
	 * Inserir rubrica.
	 *
	 * @param rubrica
	 *            the rubrica
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Long inserirRubrica(Rubrica rubrica) throws NegocioException {

		validarDados(rubrica);

		getHibernateTemplate().getSessionFactory().getCurrentSession().clear();
		for (Rubrica rubricaElem : listarRubricas()) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().evict(rubricaElem.getChavePrimaria());
		}
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		Long chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(rubrica);

		if (rubrica.getValorRegulamentados() != null) {
			for (RubricaValorRegulamentado rubricaValorRegulamentado : rubrica.getValorRegulamentados()) {
				inserirRubricaValorRegulamentado(rubricaValorRegulamentado);
			}
		}

		if (rubrica.getTributos() != null) {
			for (RubricaTributo rubricaTributo : rubrica.getTributos()) {
				inserirRubricaTributo(rubricaTributo);
			}
		}

		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.ControladorRubrica#listarRubricas()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Rubrica> listarRubricas() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" habilitado = true ");
		hql.append(" and ");
		hql.append(" indicadorUsoExclusivoSistema = false ");
		hql.append(" and ");
		hql.append(" financiamentoTipo is not null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.ControladorRubrica#listarRubricasEmUsoSistema()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Rubrica> listarRubricasEmUsoSistema() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" habilitado = true ");
		hql.append(" and ");
		hql.append(" indicadorUsoExclusivoSistema = true ");
		hql.append(" order by descricao asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.ControladorRubrica#consultarRubricasValorRegulamentado(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RubricaValorRegulamentado> consultarRubricasValorRegulamentado(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeRubricaValorRegulamentado());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idRubrica = (Long) filtro.get("idRubrica");
			if (idRubrica != null && idRubrica > 0) {
				criteria.add(Restrictions.eq("rubrica.chavePrimaria", idRubrica));
			}

			Long[] chavesPrimariasRubrica = (Long[]) filtro.get("chavesPrimariasRubrica");
			if (chavesPrimariasRubrica != null && chavesPrimariasRubrica.length > 0) {
				criteria.add(Restrictions.in("rubrica.chavePrimaria", chavesPrimariasRubrica));
			}

			Boolean bAtivo = (Boolean) filtro.get("valorRegulamentadoAtivo");
			if (bAtivo != null && bAtivo.equals(true)) {
				criteria.add(Restrictions.isNull("dataFimVigencia"));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
		}

		criteria.addOrder(Order.asc("dataInicioVigencia"));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.ControladorRubrica#processarRubricasPortipoCreditoDebito(boolean)
	 */
	@Override
	public Collection<Rubrica> processarRubricasPortipoCreditoDebito(boolean isTipoDebito) throws NegocioException {

		Collection<Rubrica> listaRubrica = this.listarRubricasPorTipoCreditoDebito(isTipoDebito);
		Collection<Rubrica> listaRubricaRetorno = new ArrayList<Rubrica>();

		if (listaRubrica != null) {
			Date dataAtual = DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime());

			for (Rubrica rubrica : listaRubrica) {
				if (!rubrica.getIndicadorRegulamentada()) {
					listaRubricaRetorno.add(rubrica);
				} else {

					for (RubricaValorRegulamentado rubricaValorRegulamentado : rubrica.getValorRegulamentados()) {
						if (Util.compararDatas(dataAtual, rubricaValorRegulamentado.getDataInicioVigencia()) >= 0
										&& rubricaValorRegulamentado.getDataFimVigencia() == null) {
							listaRubricaRetorno.add(rubrica);
							break;
						} else if (Util.compararDatas(dataAtual, rubricaValorRegulamentado.getDataInicioVigencia()) >= 0
										&& Util.compararDatas(dataAtual, rubricaValorRegulamentado.getDataFimVigencia()) <= 0) {
							listaRubricaRetorno.add(rubrica);
							break;
						}
					}
				}
			}
		}

		return listaRubricaRetorno;
	}

	/**
	 * Método responsável pela parte da consulta
	 * do calculo de listar rubricas por tipo
	 * credito ou debito.
	 *
	 * @param isTipoDebito
	 *            the is tipo debito
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Rubrica> listarRubricasPorTipoCreditoDebito(boolean isTipoDebito) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rubrica ");
		hql.append(" where ");
		hql.append(" habilitado = true ");
		hql.append(" and ");
		hql.append(" indicadorUsoExclusivoSistema = false ");
		hql.append(" and ");
		hql.append(" lancamentoItemContabil.tipoCreditoDebito.chavePrimaria = :tipoCreditoDebito ");
		hql.append(" order by rubrica.descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (isTipoDebito) {
			String chaveTipoDebito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO);

			query.setParameter("tipoCreditoDebito", Long.valueOf(chaveTipoDebito));
		} else {
			String chaveTipoCredito = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO);

			query.setParameter("tipoCreditoDebito", Long.valueOf(chaveTipoCredito));
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #obterRubricaPorFaturaItem(java.lang.Long)
	 */
	@Override
	public Rubrica obterRubricaPorFaturaItem(Long idFaturaItem) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria = ( ");
		hql.append(" select faturaItem.rubrica.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName()).append(" faturaItem ");
		hql.append(" where ");
		hql.append(" faturaItem.chavePrimaria = :idFaturaItem ");
		hql.append(" ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idFaturaItem", idFaturaItem);

		return (Rubrica) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #obterRubricaPorItemFatura(java.lang.Long)
	 */
	@Override
	@Cacheable("rubricaPorItemFatura")
	public Rubrica obterRubricaPorItemFatura(Long idItemFatura) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rubrica ");
		hql.append(" left join fetch rubrica.financiamentoTipo ");
		hql.append(" left join fetch rubrica.tributos ");
		hql.append(" where ");
		hql.append(" rubrica.itemFatura.chavePrimaria = :ITEM_FATURA ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("ITEM_FATURA", idItemFatura);
		return (Rubrica) query.uniqueResult();
	}

	/**
	 * Obter quantidade rubrica por item fatura.
	 *
	 * @param idItemFatura
	 *            the id item fatura
	 * @param chavePrimaria the chave Primaria
	 * @return the integer
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #obterRubricaPorItemFatura(java.lang.Long)
	 */
	public Integer obterQuantidadeRubricaPorItemFatura(Long idItemFatura, Long chavePrimaria) throws NegocioException {

		Long resultado = 0L;
		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select count(rubrica.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rubrica ");
		hql.append(" where ");
		hql.append(" rubrica.itemFatura.chavePrimaria = :ITEM_FATURA ");
		if (chavePrimaria>0) {
			hql.append(" and rubrica.chavePrimaria <> :CHAVE_PRIMARIA ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("ITEM_FATURA", idItemFatura);
		if (chavePrimaria>0) {
			query.setLong("CHAVE_PRIMARIA", chavePrimaria);
		}
		resultado = (Long) query.uniqueResult();
		return resultado.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.ControladorRubrica#obterListaRubricaTributo(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RubricaTributo> obterListaRubricaTributo(Long idRubrica) throws NegocioException {

		Date dataAtual = Calendar.getInstance().getTime();

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeRubricaTributo().getSimpleName());
		hql.append(" rubricaTributo ");
		hql.append(" inner join fetch rubricaTributo.rubrica rubrica ");
		hql.append(" inner join fetch rubricaTributo.tributo tributo ");
		hql.append(" inner join fetch tributo.esferaPoder esferaPoder ");
		hql.append(" where ");
		hql.append(" rubrica.chavePrimaria = :idRubrica ");
		hql.append(" and rubricaTributo.dataInicioVigencia <= :dataAtual ");
		hql.append(" and (rubricaTributo.dataFimVigencia is null ");
		hql.append(" or rubricaTributo.dataFimVigencia >= :dataAtual)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idRubrica", idRubrica);
		query.setDate("dataAtual", dataAtual);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #listarNomeclaturaComumMercosul()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<NomeclaturaComumMercosul> listarNomeclaturaComumMercosul() throws NegocioException {

		Criteria criteria = this.createCriteria(NomeclaturaComumMercosul.class);
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.rubrica.
	 * ControladorRubrica
	 * #obterNomeclaturaComumMercosul
	 * (java.lang.Long)
	 */
	@Override
	public NomeclaturaComumMercosul obterNomeclaturaComumMercosul(Long idNomeclaturaComumMercosul) throws NegocioException {

		Criteria criteria = this.createCriteria(NomeclaturaComumMercosul.class);
		criteria.add(Restrictions.idEq(idNomeclaturaComumMercosul));

		return (NomeclaturaComumMercosul) criteria.uniqueResult();

	}
	
	@Override
	@Cacheable("atributosRubrica")
	public Rubrica obterLancamentoContabilEfinancimentoDeRubrica(Long chavePrimaria) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" select rubrica.chavePrimaria as chavePrimaria, ");
		hql.append(" rubrica.lancamentoItemContabil as lancamentoItemContabil, ");
		hql.append(" rubrica.financiamentoTipo as financiamentoTipo");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rubrica ");
		hql.append(" where ");
		hql.append(" rubrica.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (Rubrica) query.uniqueResult();
	}
	
	@Override
	@Cacheable("rubricaPorChavePrimaria")
	public Rubrica obterRubrica(Long chavePrimaria) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rubrica ");
		hql.append(" inner join fetch rubrica.lancamentoItemContabil ");
		hql.append(" inner join fetch rubrica.financiamentoTipo  ");
		hql.append(" where ");
		hql.append(" rubrica.chavePrimaria = :chavePrimaria ");
		

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimaria);
		
		return (Rubrica) query.uniqueResult();
	}
	
	@Override
	@Cacheable("rubricaSubstituicaoTributaria")
	public Rubrica obterRubricaSubstituicaoTributaria(ConstanteSistema constanteSistema) throws NegocioException {
		return (Rubrica) this.obter(Long.parseLong(constanteSistema.getValor()));
	}
	
}
