package br.com.ggas.faturamento.batch.cenarios;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.batch.ConsistirLeituraConsumoBatch;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * Classe abstrata que executa o processamento do Consistir Leitura e Faturar
 * Grupo, permitindo que subclasses especifiquem o cenário através da definição
 * de Rotas e Grupos de Faturamento.
 *
 */
@Component("cenarioFaturamento")
public class CenarioFaturamento {

	private static final long CHAVE_ATIVIDADE_FATURAMENTO = 8L;

	private static final String ID_ROTA = "idRota";

	private static final String EXTENSAO_LOG = "log";

	private static final String EXTENSAO_CSV = "csv";

	private static final String EXTENSAO_XLS = "XLS";

	private static final String HOME_DIR = "user.home";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String PREFIXO_LOG = "CENARIO_";

	private static final String FORMATO_DATA_LOG = "yyyy_MM_dd_hh@mm#";

	private static final String MENSAGEM_GRUPO_NAO_IDENTIFICADO = "Grupo de Faturamento não identificado.";

	private static final String MENSAGEM_ROTA_NAO_IDENTIFICADA = "Rota não identificada.";

	private static final String MENSAGEM_LOG_FATURAMENTO = "\n Faturar Grupo \n";

	private static final String MENSAGEM_LOG_CONSISTIR = "\n Consistir Leitura \n";

	private static final Logger LOG = Logger.getLogger(CenarioFaturamento.class);

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("controladorRota")
	private ControladorRota controladorRota;

	@Autowired
	@Qualifier("controladorCronogramaFaturamento")
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;

	@Autowired
	@Qualifier("controladorParametroSistema")
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ConsistirLeituraConsumoBatch consistirLeituraConsumoBatch;

	private Rota rota;

	private GrupoFaturamento grupoFaturamento;

	private String cdl;

	/**
	 * Executa o processamento de Consistir Leitura e Faturar Grupo, permitindo
	 * realizar a instrumentação através da contagem de consultas SQL realizas.
	 * 
	 * @param instrumentar   - {@link Boolean}
	 * @param descricaoGrupo - {@link String}
	 * @param numeroRota     - {@link String}
	 * @param cdl            - {@link String}
	 * 
	 */
	public void executar(Boolean instrumentar, String descricaoGrupo, String numeroRota, String cdl) {

		Boolean executarCenario = carregarCenario(descricaoGrupo, numeroRota);
		this.cdl = cdl;
		if (executarCenario) {
			executar(instrumentar);
		}
	}

	private void executar(Boolean instrumentar) {
		StringBuilder logProcessamento = new StringBuilder();

		Statistics statistics = null;

		if (instrumentar) {
			statistics = habilitarInstrumentacao();
		}

		try {

			alterarDiretorioArquivosFaturamento();
			logProcessamento.append(this.executarConsistirCenario());
			logProcessamento.append(criarLogInstrumentacao(statistics));
			logProcessamento.append(this.executarFaturarCenario());
			logProcessamento.append(criarLogInstrumentacao(statistics));

			this.salvarDadosFaturamento();

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		this.salvarLog(logProcessamento.toString());

	}

	private void alterarDiretorioArquivosFaturamento() throws GGASException {
		ParametroSistema parametroSistema = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_ARQUIVOS_FATURA);
		parametroSistema.setValor(System.getProperty(HOME_DIR));
		controladorParametroSistema.atualizar(parametroSistema);
	}

	private Boolean carregarCenario(String descricaoGrupo, String numeroRota) {
		Boolean executarCenario = Boolean.FALSE;

		this.grupoFaturamento = this.carregarGrupoFaturamento(descricaoGrupo);

		if (grupoFaturamento != null) {
			executarCenario = Boolean.TRUE;
			this.rota = carregarRota(numeroRota, grupoFaturamento);
		}

		return executarCenario;
	}

	private GrupoFaturamento carregarGrupoFaturamento(String descricaoGrupo) {
		GrupoFaturamento grupo = null;
		try {
			grupo = controladorRota.obterGrupoPorDescricao(descricaoGrupo);
			if (grupo == null) {
				throw new RuntimeException(MENSAGEM_GRUPO_NAO_IDENTIFICADO);
			}
		} catch (HibernateException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return grupo;
	}

	private Rota carregarRota(String numeroRota, GrupoFaturamento grupoFaturamento) {
		Rota rotaPorGrupo = null;
		try {
			rotaPorGrupo = controladorRota.obterRotaDeGrupoFaturamentoPorNumero(numeroRota, grupoFaturamento);
			if (rotaPorGrupo == null) {
				LOG.info(MENSAGEM_ROTA_NAO_IDENTIFICADA);
			}
		} catch (HibernateException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return rotaPorGrupo;
	}

	private String executarConsistirCenario() throws GGASException {
		StringBuilder logConsistir = new StringBuilder();

		logConsistir.append(criarLogInicioFimProcessamento());

		logConsistir.append(MENSAGEM_LOG_CONSISTIR);
		logConsistir.append(consistirCenario());

		logConsistir.append(criarLogInicioFimProcessamento());

		return logConsistir.toString();

	}

	private String executarFaturarCenario() throws GGASException {
		StringBuilder logFaturarGrupo = new StringBuilder();

		logFaturarGrupo.append(criarLogInicioFimProcessamento());

		logFaturarGrupo.append(MENSAGEM_LOG_FATURAMENTO);
		logFaturarGrupo.append(faturarCenario());

		logFaturarGrupo.append(criarLogInicioFimProcessamento());

		return logFaturarGrupo.toString();
	}

	private String faturarCenario() throws GGASException {

		AtividadeSistema atividadeSistema = controladorCronogramaFaturamento
				.obterAtividadeSistema(CHAVE_ATIVIDADE_FATURAMENTO);

		CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = controladorCronogramaFaturamento
				.consultarCronogramaAtividadeFaturamento(atividadeSistema, grupoFaturamento);

		StringBuilder logProcessamento = new StringBuilder();

		controladorFatura.faturarGrupo(grupoFaturamento, rota, criarDadosAuditoria(), logProcessamento,
				cronogramaAtividadeFaturamento, Boolean.FALSE, null, null);

		return logProcessamento.toString();
	}

	private DadosAuditoria criarDadosAuditoria() throws NegocioException {

		ControladorUsuario controladorUsuario = ServiceLocator.getInstancia().getControladorUsuario();

		Usuario usuario = controladorUsuario.buscar(Constantes.USUARIO_AUDITORIA);

		DadosAuditoria dadosAuditoria = (DadosAuditoria) ServiceLocator.getInstancia()
				.getBeanPorID(DadosAuditoria.BEAN_ID_DADOS_AUDITORIA);

		dadosAuditoria.setUsuario(usuario);

		return dadosAuditoria;
	}

	private String consistirCenario() throws GGASException {

		Map<String, Object> parametros = new HashMap<>();

		if (this.rota != null) {
			parametros.put(ID_ROTA, String.valueOf(rota.getChavePrimaria()));
		}
		if (this.grupoFaturamento != null) {
			parametros.put(ID_GRUPO_FATURAMENTO, String.valueOf(grupoFaturamento.getChavePrimaria()));
		}

		return consistirLeituraConsumoBatch.processar(parametros);
	}

	private Statistics habilitarInstrumentacao() {
		Statistics statistics = sessionFactory.getStatistics();
		statistics.setStatisticsEnabled(Boolean.TRUE);
		return statistics;
	}

	private String criarLogInstrumentacao(Statistics statistics) {

		StringBuilder logInstrumentacao = new StringBuilder();

		if (statistics != null) {
			logInstrumentacao.append("\nConsultas: ");
			logInstrumentacao.append(statistics.getPrepareStatementCount());
			logInstrumentacao.append("\n");
			statistics.clear();
		}

		return logInstrumentacao.toString();
	}

	private String criarLogInicioFimProcessamento() {

		StringBuilder logInicio = new StringBuilder();

		logInicio.append("\n\n");
		logInicio.append(DataUtil.converterDataParaString(new Date(), Boolean.TRUE));
		logInicio.append("\n\n");

		return logInicio.toString();
	}

	private void salvarDadosFaturamento() throws NegocioException {
		Map<String, List<Object>> dadosFaturamento = this.consultarDadosFaturamento();

		this.salvarCSV(this.gerarCSV(dadosFaturamento));
		this.salvarXLS(this.gerarXLS(dadosFaturamento));
	}

	private HSSFWorkbook gerarXLS(Map<String, List<Object>> dadosFaturamento) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet xls = workbook.createSheet();

		int linhas = 0;
		int colunas = 0;
		for (Entry<String, List<Object>> entry : dadosFaturamento.entrySet()) {

			String cabecalho = entry.getKey();
			List<Object> valores = entry.getValue();

			this.adicionarDadosXLS(xls, cabecalho, linhas, colunas);
			linhas += 1;

			for (Object valor : valores) {
				this.adicionarDadosXLS(xls, valor, linhas, colunas);
				linhas += 1;
			}

			linhas = 0;
			colunas += 1;

		}

		return workbook;
	}

	private void adicionarDadosXLS(HSSFSheet xls, Object valor, int l, int c) {

		Row linha = xls.getRow(l);
		if (linha == null) {
			linha = xls.createRow(l);
		}

		Cell celula = linha.createCell(c);

		if (valor instanceof String) {
			celula.setCellValue((String) valor);
		} else if (valor instanceof Double) {
			celula.setCellValue((Double) valor);
		} else if (valor instanceof Date) {
			celula.setCellValue(DataUtil.converterDataParaString((Date) valor, Boolean.TRUE));
		} else if (valor instanceof Long) {
			celula.setCellValue((Long) valor);
		} else if (valor instanceof Integer) {
			celula.setCellValue((Integer) valor);
		} else if (valor instanceof BigDecimal) {
			celula.setCellValue(((BigDecimal) valor).doubleValue());
		} else if (valor == null) {
			celula.setCellValue("null");
		}
	}

	private void salvarCSV(String csv) {

		this.salvarArquivoTexto(csv, EXTENSAO_CSV);
	}

	private void salvarLog(String log) {

		this.salvarArquivoTexto(log, EXTENSAO_LOG);
	}

	private void salvarXLS(HSSFWorkbook workbook) {

		try {
			FileOutputStream outputStream = new FileOutputStream(Util.getFile(criarCaminhoArquivo(EXTENSAO_XLS)));
			workbook.write(outputStream);
			workbook.close();
			outputStream.close();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private void salvarArquivoTexto(String conteudo, String extensao) {

		try {
			FileUtils.writeStringToFile(Util.getFile(criarCaminhoArquivo(extensao)), conteudo);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private String criarCaminhoArquivo(String extensao) {

		String home = System.getProperty(HOME_DIR);
		SimpleDateFormat formato = new SimpleDateFormat(FORMATO_DATA_LOG);
		String data = formato.format(new Date());

		StringBuilder caminhoArquivo = new StringBuilder();

		caminhoArquivo.append(home);
		caminhoArquivo.append(File.separator);
		caminhoArquivo.append(getNomeArquivoLog().toUpperCase());
		caminhoArquivo.append(" - ");
		caminhoArquivo.append(data.replace("@", "h").replace("#", "min"));
		caminhoArquivo.append(".");
		caminhoArquivo.append(extensao);

		return caminhoArquivo.toString();
	}

	private String getNomeArquivoLog() {
		StringBuilder nomeArquivo = new StringBuilder();

		nomeArquivo.append(PREFIXO_LOG);

		nomeArquivo.append(this.getIdentificadorGrupo());

		if (this.rota != null) {
			nomeArquivo.append(" - ");
			nomeArquivo.append(this.getIdentificadorRota());
		}

		if (StringUtils.isNotBlank(this.cdl)) {
			nomeArquivo.append(" - ");
			nomeArquivo.append(this.cdl);
		}

		return nomeArquivo.toString();
	}

	private String getIdentificadorRota() {
		String identificador = "";
		if (this.rota != null) {
			identificador = rota.getNumeroRota();
		}
		return identificador;
	}

	private String getIdentificadorGrupo() {
		String identificador = "";
		if (this.grupoFaturamento != null) {
			identificador = this.grupoFaturamento.getDescricao();
		}
		return identificador;
	}

	private String gerarCSV(Map<String, List<Object>> dadosFaturamento) {
		StringBuilder csv = new StringBuilder();

		for (Entry<String, List<Object>> entry : dadosFaturamento.entrySet()) {

			csv.append(entry.getKey());

			for (Object object : entry.getValue()) {
				csv.append(",");
				csv.append(object);
			}

			csv.append("\n");
		}

		return csv.toString();
	}

	private Map<String, List<Object>> consultarDadosFaturamento() throws NegocioException {

		Map<String, List<Object>> valorPorColuna = new HashMap<>();

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Collection<Rota> rotas = this.getRotasParaConsulta();

		this.agruparResultadoConsultaPorColuna(this.consultarPorRota(criarConsultaHistoricoConsumo(), session, rotas),
				valorPorColuna);
		this.agruparResultadoConsultaPorColuna(this.consultarPorRota(criarConsultaHistoricoMedicao(), session, rotas),
				valorPorColuna);
		this.agruparResultadoConsultaPorColuna(this.consultarPorRota(criarConsultaFaturas(), session, rotas),
				valorPorColuna);
		this.agruparResultadoConsultaPorColuna(this.consultarPorRota(criarConsultaTributos(), session, rotas),
				valorPorColuna);

		this.ordenarValoresColuna(valorPorColuna);

		session.close();

		return valorPorColuna;
	}

	private Collection<Rota> getRotasParaConsulta() throws NegocioException {
		Collection<Rota> rotas = null;
		if (this.rota == null) {
			rotas = controladorRota.consultarRotaPorGrupoFaturamento(grupoFaturamento);
		} else {
			rotas = Arrays.asList(rota);
		}
		return rotas;
	}

	private void agruparResultadoConsultaPorColuna(List<Map<String, Object>> consulta,
			Map<String, List<Object>> valorPorColuna) {

		List<Object> valores = null;

		for (Map<String, Object> linha : consulta) {
			for (Entry<String, Object> colunaValor : linha.entrySet()) {

				String coluna = colunaValor.getKey();
				Object valor = colunaValor.getValue();

				if (!valorPorColuna.containsKey(coluna)) {
					valores = new ArrayList<>();
					valorPorColuna.put(coluna, valores);
				} else {
					valores = valorPorColuna.get(coluna);
				}

				valores.add(valor);
			}
		}
	}

	private void ordenarValoresColuna(Map<String, List<Object>> valorPorColuna) {
		for (Entry<String, List<Object>> coluna : valorPorColuna.entrySet()) {
			ordenarValoresColuna(coluna.getValue());
		}
	}

	private void ordenarValoresColuna(List<Object> valores) {

		Collections.sort(valores, new Comparator<Object>() {

			@Override
			public int compare(Object a, Object b) {
				int ordem = 0;
				if (a == null && b == null) {
					ordem = 0;
				} else if (a == null) {
					ordem = 1;
				} else if (b == null) {
					ordem = -1;
				} else if (a instanceof Comparable && b instanceof Comparable) {
					Comparable<Object> comparableA = (Comparable<Object>) a;
					Comparable<Object> comparableB = (Comparable<Object>) b;
					ordem = comparableA.compareTo(comparableB);
				}
				return ordem;
			}
		});
	}

	private String criarConsultaHistoricoConsumo() {

		StringBuilder hql = new StringBuilder();

		hql.append(" select consumo.diasConsumo as diasConsumo, ");
		hql.append(" consumo.consumo as consumo,  ");
		hql.append(" consumo.consumoApurado as consumoApurado, ");
		hql.append(" consumo.consumoMedido as consumoMedido ");
		hql.append(" from HistoricoConsumoImpl as consumo ");
		hql.append(" inner join consumo.pontoConsumo as pontoConsumo ");
		hql.append(this.criarHqlJoinPontoConsumoRota());

		return hql.toString();
	}

	private String criarConsultaHistoricoMedicao() {

		StringBuilder hql = new StringBuilder();

		hql.append(" select medicao.numeroLeituraInformada as numeroLeituraInformada, ");
		hql.append(" medicao.dataLeituraInformada as dataLeituraInformada, ");
		hql.append(" medicao.numeroLeituraAnterior as numeroLeituraAnterior, ");
		hql.append(" medicao.dataLeituraAnterior as dataLeituraAnterior ");
		hql.append(" from HistoricoMedicaoImpl as medicao ");
		hql.append(" inner join medicao.pontoConsumo as pontoConsumo ");
		hql.append(this.criarHqlJoinPontoConsumoRota());

		return hql.toString();
	}

	private String criarConsultaFaturas() {

		StringBuilder hql = new StringBuilder();

		hql.append(" select fatura.valorTotal as faturaValorTotal, ");
		hql.append(" item.valorTotal as faturaItemValorTotal ");
		hql.append(" from FaturaImpl as fatura  ");
		hql.append(" left join fatura.listaFaturaItem item ");
		hql.append(" inner join fatura.pontoConsumo pontoConsumo ");
		hql.append(this.criarHqlJoinPontoConsumoRota());

		return hql.toString();
	}

	private String criarConsultaTributos() {
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" tributacao.valorImposto as valorImposto, ");
		hql.append(" tributacao.valorBaseCalculo as valorBaseCalculo, ");
		hql.append(" tributacao.valorSubstituicao as valorSubstituicao, ");
		hql.append(" tributacao.valorBaseSubstituicao as valorBaseSubstituicao ");
		hql.append(" from FaturaItemTributacaoImpl as tributacao ");
		hql.append(" inner join tributacao.faturaItem item ");
		hql.append(" inner join item.fatura fatura ");
		hql.append(" where fatura.chavePrimaria in ( ");
		hql.append(" select f.chavePrimaria ");
		hql.append(" from ");
		hql.append(" FaturaImpl as f ");
		hql.append(" inner join f.pontoConsumo pontoConsumo ");
		hql.append(this.criarHqlJoinPontoConsumoRota());
		hql.append(" ) ");

		return hql.toString();
	}

	private String criarHqlJoinPontoConsumoRota() {
		StringBuilder hql = new StringBuilder();
		hql.append(" inner join pontoConsumo.rota as rota ");
		hql.append(" where rota.chavePrimaria IN (:chavesPrimarias) ");
		return hql.toString();
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> consultarPorRota(String hql, Session session, Collection<Rota> rotas) {

		Query query = session.createQuery(hql);

		query.setParameterList(TabelaAuxiliar.ATRIBUTO_CHAVES_PRIMARIAS,
				Util.collectionParaArrayChavesPrimarias(rotas));

		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		return query.list();
	}

}
