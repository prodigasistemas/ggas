/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.batch;

import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável pelo processo de Encerramento de Ciclo de Faturamento. 
 *
 */
@Component
public class EncerrarCicloFaturamentoBatch implements Batch {

	private final ControladorFatura controladorFatura;

	private final ControladorRota controladorRota;
	
	private static final String PROCESSO = "processo";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	/**
	 * Instantiates a new encerrar ciclo faturamento batch.
	 */
	public EncerrarCicloFaturamentoBatch() {

		controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		
		controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		String idGrupoFaturamento = (String) parametros.get(ID_GRUPO_FATURAMENTO);
		
		StringBuilder logProcessamento = new StringBuilder();
		GrupoFaturamento grupo = controladorRota.obterGrupoFaturamento(Long.valueOf(idGrupoFaturamento));

		controladorFatura.processarEncerrarCicloFaturamento(processo, logProcessamento, grupo);

		return logProcessamento.toString();
	}
	
}
