/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.batch;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;

/**
 * Classe responsável pelo processo batch que apura a aplicação de penalidade. 
 *
 */
@Component
public class ApurarPenalidadeBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		try {

			ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(
							ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

			ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
							.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

			ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(processo.getUsuario(), processo.getOperacao(),
							(String) controladorParametrosSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));

			logProcessamento.append("\r\n ...Pesquisando Contratos que possuem penalidades associadas. \r\n\r\n");

			Collection<Contrato> listaContratos = controladorContrato.obterListaContratoComPenalidades();

			// Agrupar os pontos de consumo por grupo economico
			controladorApuracaoPenalidade.criarMapaGrupoEconomicoPontoConsumo(listaContratos);

			for (Contrato contrato : listaContratos) {
				controladorApuracaoPenalidade.processarPenalidadesContrato(contrato, null, dadosAuditoria, logProcessamento);
			}
		} catch(HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());			
			throw new HibernateException(e);
		} catch(NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());			
			throw new NegocioException(e);
		} catch(GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());			
			throw new GGASException(e);
		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());			
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

}
