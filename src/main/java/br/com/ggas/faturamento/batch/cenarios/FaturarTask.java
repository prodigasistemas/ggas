package br.com.ggas.faturamento.batch.cenarios;

import org.apache.log4j.Logger;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import br.com.ggas.geral.exception.NegocioException;

/**
 * 
 * Classe implementada para realizar a execução parametrizada de cenários de
 * Consistir Leitura e Faturar Grupo
 *
 */
public class FaturarTask {

	private static ApplicationContext context;

	private static final Logger LOG = Logger.getLogger(FaturarTask.class);

	private static final String MENSAGEM_ARGUMENTO_INVALIDO = "Argumentos inválidos para a execução do faturamento";

	private static final String BEAN_CENARIO_FATURAMENTO = "cenarioFaturamento";

	private static final int QUANTIDADE_PARAMETROS = 3;
	
	private static final int PARAMETRO_CDL = 0;

	private static final int PARAMETRO_GRUPO = 1;
	
	private static final int PARAMETRO_ROTA = 2;

	private FaturarTask() {
	}

	/**
	 * Main utilizada pela "Task" do Gradle: faturar
	 * 
	 * @param args
	 * 		the args
	 * @throws NegocioException
	 */
	public static void main(String[] args) {

		context = new ClassPathXmlApplicationContext("file:src/main/webapp/WEB-INF/spring-config-test.xml");

		String rota = "";
		String grupoFaturamento = "";
		String cdl = "";

		if (args.length > 0 && args.length <= QUANTIDADE_PARAMETROS) {
			cdl = getParametro(PARAMETRO_CDL, args);
			grupoFaturamento = getParametro(PARAMETRO_GRUPO, args);
			rota = getParametro(PARAMETRO_ROTA, args);
		} else {
			LOG.info(MENSAGEM_ARGUMENTO_INVALIDO);
			System.exit(1);
		}
	
		try {
			executarCenario(grupoFaturamento, rota, cdl);
		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
			System.exit(1);
		}
		System.exit(0);
	}

	private static String getParametro(int index, String[] args) {
		String parametro = "";
		try {
			parametro = args[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.debug(e.getMessage(), e);
		}
		return parametro;
	}

	private static void executarCenario(String grupoFaturamento, String rota, String cdl) {

		CenarioFaturamento cenarioFaturamento = (CenarioFaturamento) context.getBean(BEAN_CENARIO_FATURAMENTO);

		cenarioFaturamento.executar(Boolean.FALSE, grupoFaturamento, rota, cdl);

	}

}
