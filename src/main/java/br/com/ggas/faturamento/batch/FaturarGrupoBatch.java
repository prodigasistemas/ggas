/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.batch;

import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.impl.TipoDataRetroativaInvalida;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelo processo de Faturar Grupo. 
 *
 */
@Component
public class FaturarGrupoBatch implements Batch {

	private static final String PROCESSO = "processo";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_ROTA = "idRota";

	private static final String ATIVIDADE_SISTEMA = "atividadeSistema";

	private static final String PERIODO_EMISSAO = "periodoEmissao";

	private static final String TIPO_DATA_RETROATIVA_INVALIDA = "tipoDataRetroativaInvalida";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		String idGrupoFaturamento = (String) parametros.get(ID_GRUPO_FATURAMENTO);
		String idRota = (String) parametros.get(ID_ROTA);
		String periodoEmissao = (String) parametros.get(PERIODO_EMISSAO);
		TipoDataRetroativaInvalida tipoDataRetroativaInvalida = null;
		if(parametros.containsKey(TIPO_DATA_RETROATIVA_INVALIDA)) {
			tipoDataRetroativaInvalida = TipoDataRetroativaInvalida.toTipoDataRetroativaInvalida(Integer.valueOf((String) parametros
							.get(TIPO_DATA_RETROATIVA_INVALIDA)));
		}

		AtividadeSistema atividadeSistema = (AtividadeSistema) parametros.get(ATIVIDADE_SISTEMA);

		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(processo.getUsuario(), processo.getOperacao(),
						(String) controladorParametrosSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf(idGrupoFaturamento));
		Rota rota = null;

		if(idRota != null && !idRota.isEmpty()) {
			rota = (Rota) controladorRota.obter(Long.parseLong(idRota));
		}

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);
		
		ControladorProcesso controladorProcesso = ServiceLocator.getInstancia().getControladorProcesso();


		CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = controladorCronogramaFaturamento
						.consultarCronogramaAtividadeFaturamento(atividadeSistema, grupoFaturamento);

		
		CronogramaRota cronogramaRota = controladorRota.obterCronogramaRotaAtual(rota,
				AtividadeSistema.CODIGO_ATIVIDADE_SIMULAR_FATURAMENTO, true, false);
		
		AtividadeSistema atividadeSimular = controladorProcesso.obterAtividadeSistema(AtividadeSistema.CODIGO_ATIVIDADE_SIMULAR_FATURAMENTO);
		
		
		if (!atividadeSimular.isObrigatoria() || cronogramaRota != null) {

			if (cronogramaAtividadeFaturamento != null) {
				controladorFatura.faturarGrupo(grupoFaturamento, rota, dadosAuditoria, logProcessamento,
						cronogramaAtividadeFaturamento, Boolean.FALSE, periodoEmissao, tipoDataRetroativaInvalida,
						processo);

			} else {
				logProcessamento
						.append(" Não foi encontrado Cronograma de Atividade do Faturamento para a atividade \"")
						.append(atividadeSistema.getDescricao()).append("\" no Grupo de Faturamento \"")
						.append(grupoFaturamento.getDescricao()).append("\"");
			}
		} else {
			logProcessamento
			.append(" É necessário executar o processo Simular Faturamento Grupo dessa Rota antes de executar esse processo.");
		}

		return logProcessamento.toString();
	}

}
