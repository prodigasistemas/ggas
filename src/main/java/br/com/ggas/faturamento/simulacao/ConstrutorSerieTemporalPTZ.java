package br.com.ggas.faturamento.simulacao;

import java.math.BigDecimal;

import com.google.common.base.Function;

import br.com.ggas.medicao.consumo.HistoricoConsumo;

/**
 * The Class ConstrutorSerieTemporalPTZ.
 */
public class ConstrutorSerieTemporalPTZ extends ConstrutorSerieTemporal {

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.simulacao.ConstrutorSerieTemporal#getFuncaoCriarValores()
	 */
	@Override
	protected Function<HistoricoConsumo, BigDecimal> getFuncaoCriarValores() {
		return new Function<HistoricoConsumo,BigDecimal>(){

			@Override
			public BigDecimal apply(HistoricoConsumo input) {
				return input.getFatorPTZ();
			}
			
		};
	}

}
