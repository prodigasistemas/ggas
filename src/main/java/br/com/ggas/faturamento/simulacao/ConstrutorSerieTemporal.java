package br.com.ggas.faturamento.simulacao;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Maps;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.util.Fachada;

/**
 * The Class ConstrutorSerieTemporal.
 */
public abstract class ConstrutorSerieTemporal {
	
	/** The historico. */
	private Set<HistoricoConsumo> historico;
	
	/**
	 * Instantiates a new construtor serie temporal.
	 */
	public ConstrutorSerieTemporal() {
		historico = new HashSet<>();
	}
	
	/**
	 * Novo construtor serie temporal PCS.
	 *
	 * @return the construtor serie temporal
	 */
	public static ConstrutorSerieTemporal novoConstrutorSerieTemporalPCS() {
		return new ConstrutorSerieTemporalPCS();
	}
	
	/**
	 * Novo construtor serie temporal PTZ.
	 *
	 * @return the construtor serie temporal
	 */
	public static ConstrutorSerieTemporal novoConstrutorSerieTemporalPTZ() {
		return new ConstrutorSerieTemporalPTZ();
	}
	
	/**
	 * Gets the funcao criar valores.
	 *
	 * @return the funcao criar valores
	 */
	protected abstract Function<HistoricoConsumo,BigDecimal> getFuncaoCriarValores();
	
	/**
	 * Gets the funcao criar chaves.
	 *
	 * @return the funcao criar chaves
	 */
	private Function<HistoricoConsumo,Long> getFuncaoCriarChaves() {
		return new Function<HistoricoConsumo,Long>(){

			@Override
			public Long apply(HistoricoConsumo input) {
				return input.getHistoricoAtual().getDataLeituraInformada().getTime();
			}
			
		};
	}
	
	/**
	 * Consultar historico.
	 *
	 * @param idHistoricoConsumo the id historico consumo
	 * @return the construtor serie temporal
	 * @throws GGASException the GGAS exception
	 */
	public ConstrutorSerieTemporal consultarHistorico(Long idHistoricoConsumo) throws GGASException {
		historico.clear();
		Fachada fachada = Fachada.getInstancia();
		HistoricoConsumo historicoSintetizado = fachada.obterHistoricoConsumo(idHistoricoConsumo);
		if (historicoSintetizado != null) {
			if (historicoSintetizado.getFatorPCS() == null) {
				historico.addAll(fachada.consultarHistoricoConsumoPorHistoricoConsumoSintetizador(historicoSintetizado));
			} else {
				historico.add(historicoSintetizado);
			} 
		}
		return this;
	}
	
	/**
	 * Criar mapa.
	 *
	 * @return the map
	 */
	public Map<Long, BigDecimal> criarMapa() {
		return Maps.transformValues(
				ImmutableSortedMap.copyOf(
						Maps.uniqueIndex(historico, getFuncaoCriarChaves())), getFuncaoCriarValores());
	}
	
}
