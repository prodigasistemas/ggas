/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.simulacao;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.tarifa.Tarifa;
/**
 * 
 * Simulacao
 *
 */
public interface Simulacao {

	String BEAN_ID_SIMULACAO = "simulacao";

	String SIMULACAO_CAMPO_STRING = "SIMULAÇÃO";

	String SIMULACOES = "SIMULAÇÕES";

	/**
	 * @param imovel - Set imovel.
	 */
	void setImovel(Imovel imovel);

	/**
	 * @return Imovel - Retorna um objeto imovel.
	 */
	Imovel getImovel();

	/**
	 * @param consumo - Set Consumo.
	 */
	void setConsumo(BigDecimal consumo);

	/**
	 * @return BigDecimal - Retorna Consumo.
	 */
	BigDecimal getConsumo();

	/**
	 * @param dataLeituraInicial - Set Data leitura Inicial
	 */
	void setDataLeituraInicial(Date dataLeituraInicial);

	/**
	 * @return Date - Retorna data leitura inicial.
	 */
	Date getDataLeituraInicial();

	/**
	 * @param dataLeituraFinal - Set Data de leitura final.
	 */
	void setDataLeituraFinal(Date dataLeituraFinal);

	/**
	 * @return Date - Retorna Data Leitura Final.
	 */
	Date getDataLeituraFinal();

	/**
	 * @param faturaItem - Set objeto Fatura Item.
	 */
	void setFaturaItem(FaturaItem faturaItem);

	/**
	 * @return FaturaItem - Retorna um objeto FaturaItem.
	 */
	FaturaItem getFaturaItem();

	/**
	 * @param tarifa - Set um objeto Tarifa.
	 */
	void setTarifa(Tarifa tarifa);

	/**
	 * @return Tarifa - Retorna um objeto tarifa.
	 */
	Tarifa getTarifa();

}
