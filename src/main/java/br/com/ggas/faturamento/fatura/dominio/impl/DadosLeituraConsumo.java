package br.com.ggas.faturamento.fatura.dominio.impl;

import java.math.BigDecimal;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.fatura.DadosLeituraConsumoFatura;

/**
 * 
 * Classe resposável pelos dados de leitura do consumo
 *
 */
public class DadosLeituraConsumo {

	/**
	 * Verifica consumo apurado pelo numero fator correcao
	 * 
	 * @param contratoPontoConsumo - {@link ContratoPontoConsumo}
	 * @param dadosLeituraConsumoFatura - {@link DadosResumoFatura}
	 * @return BigDecimal - {@link BigDecimal}
	 */
	public BigDecimal verificarConsumoApuradoPeloNumeroFatorCorrecao(ContratoPontoConsumo contratoPontoConsumo,
			DadosLeituraConsumoFatura dadosLeituraConsumoFatura) {
		if (contratoPontoConsumo.getNumeroFatorCorrecao() != null) {
			double consumoApuradoCalculado =
					(dadosLeituraConsumoFatura.getLeituraAtual().longValue() - dadosLeituraConsumoFatura.getLeituraAnterior()
							.longValue()) * contratoPontoConsumo.getNumeroFatorCorrecao().doubleValue();
			
			return BigDecimal.valueOf(consumoApuradoCalculado);
		} else {
			return dadosLeituraConsumoFatura.getConsumoFaturado();
		}
	}
}
