/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.atendimento.chamado.dominio.DocumentoFiscalVO;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.NaturezaOperacaoCFOP;
import br.com.ggas.faturamento.NaturezaOperacaoConfiguracao;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoDetalhamentoVO;
import br.com.ggas.faturamento.creditodebito.impl.HistoricoNotaDebitoCredito;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.exception.ValorTotalElevadoException;
import br.com.ggas.faturamento.fatura.impl.TipoDataRetroativaInvalida;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Pair;
import br.com.ggas.web.faturamento.fatura.FaturaVO;
import br.com.ggas.web.faturamento.fatura.TitulosGSAVO;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioDescontosConcedidosFaturaVO;
import br.com.ggas.webservice.NfeTO;

/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados 
 * ao controlador de fatura
 *
 */
public interface ControladorFatura extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_FATURA = "controladorFatura";

	String ERRO_DATA_VENCIMENTO_OBRIGATORIA = "ERRO_DATA_VENCIMENTO_OBRIGATORIA";

	String ERRO_NEGOCIO_ALTERAR_VENCIMENTO_FATURA = "ERRO_NEGOCIO_ALTERAR_VENCIMENTO_FATURA";

	String ERRO_NEGOCIO_QUANTIDADE_DIAS_MAXIMO_PRORROGACAO = "ERRO_NEGOCIO_QUANTIDADE_DIAS_MAXIMO_PRORROGACAO";

	String ERRO_NEGOCIO_DATA_VENCIMENTO_INVALIDA = "ERRO_NEGOCIO_DATA_VENCIMENTO_INVALIDA";

	String ERRO_NEGOCIO_DATA_VENCIMENTO_ANTERIOR_DATA_ATUAL = "ERRO_NEGOCIO_DATA_VENCIMENTO_ANTERIOR_DATA_ATUAL";

	String ERRO_NEGOCIO_QTDE_MAXIMA_MOTIVO_ALTERACAO = "ERRO_NEGOCIO_QTDE_MAXIMA_MOTIVO_ALTERACAO";

	String ERRO_NEGOCIO_FATURAS_ESTADOS_DIFERENTES = "ERRO_NEGOCIO_FATURAS_ESTADOS_DIFERENTES";

	String ERRO_NEGOCIO_MOTIVO_REVISAO_NAO_SELECIONADO = "ERRO_NEGOCIO_MOTIVO_REVISAO_NAO_SELECIONADO";

	String ERRO_NEGOCIO_FATURAS_NAO_SELECIONADAS = "ERRO_NEGOCIO_FATURAS_NAO_SELECIONADAS";

	String ERRO_NEGOCIO_SITUACAO_INVALIDA = "ERRO_NEGOCIO_SITUACAO_INVALIDA";

	String ERRO_NEGOCIO_CANCELAMENTO_NOTA = "ERRO_NEGOCIO_CANCELAMENTO_NOTA";

	String ERRO_NEGOCIO_PONTO_CONSUMO_CONTRATO_ATIVO = "ERRO_NEGOCIO_PONTO_CONSUMO_CONTRATO_ATIVO";

	String ERRO_NEGOCIO_SITUACAO_NOTAS_PAGAS = "ERRO_NEGOCIO_SITUACAO_NOTAS_PAGAS";

	String ERRO_NEGOCIO_SELECAO_NOTAS = "ERRO_NEGOCIO_SELECAO_NOTAS";
	
	String ERRO_NEGOCIO_SELECAO_NOTAS_CANCELADAS = "ERRO_NEGOCIO_SELECAO_NOTAS_CANCELADAS";

	String ERRO_NEGOCIO_VENCIMENTO_NOTA_MENOR_QUE_DATA_ATUAL = "ERRO_NEGOCIO_VENCIMENTO_NOTA_MENOR_QUE_DATA_ATUAL";

	String ERRO_NEGOCIO_NOTA_CANCELADA = "ERRO_NEGOCIO_NOTA_CANCELADA";

	String ERRO_NEGOCIO_NOTA_QUITADA = "ERRO_NEGOCIO_NOTA_QUITADA";

	String ERRO_NEGOCIO_SELECAO_NOTA_OBRIGATORIO = "ERRO_NEGOCIO_SELECAO_NOTA_OBRIGATORIO";

	String ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL = "ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL";

	String ERRO_NEGOCIO_VALOR_TOTAL_MENOR_VALOR_REFERENCIA = "ERRO_NEGOCIO_VALOR_TOTAL_MENOR_VALOR_REFERENCIA";

	String ERRO_FILTRO_PESQUISA_FATURA_VAZIO = "ERRO_FILTRO_PESQUISA_FATURA_VAZIO";

	String ERRO_NEGOCIO_VALOR_TOTAL_MAIOR_VALOR_MAXIMO = "ERRO_NEGOCIO_VALOR_TOTAL_MAIOR_VALOR_MAXIMO";

	String ERRO_NEGOCIO_VENCIMENTO_FATURA_DIFERENTE = "ERRO_NEGOCIO_VENCIMENTO_FATURA_DIFERENTE";

	String ERRO_NEGOCIO_SITUACAO_PAGAMENTO_CANCELAMENTO_NOTA = "ERRO_NEGOCIO_SITUACAO_PAGAMENTO_CANCELAMENTO_NOTA";

	String ERRO_NEGOCIO_SITUACAO_PAGAMENTO_CANCELAMENTO = "ERRO_NEGOCIO_SITUACAO_PAGAMENTO_CANCELAMENTO";

	String ERRO_NEGOCIO_RUBRICA_PARCELAMENTO = "ERRO_NEGOCIO_RUBRICA_PARCELAMENTO";

	String ERRO_NEGOCIO_FATURA_ITEM_RUBRICA = "ERRO_NEGOCIO_FATURA_ITEM_RUBRICA";

	String ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONSUMO = "ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONSUMO";

	String ERRO_NEGOCIO_CONTRATO_SITUACAO_NAO_FATURAVEL = "ERRO_NEGOCIO_CONTRATO_SITUACAO_NAO_FATURAVEL";

	String ERRO_NEGOCIO_CONTRATO_SITUACAO_NAO_FATURAVEL_COM_CONSUMO = "ERRO_NEGOCIO_CONTRATO_SITUACAO_NAO_FATURAVEL_COM_CONSUMO";

	String ERRO_NEGOCIO_PONTO_CONSUMO_SITUACAO_NAO_FATURAVEL = "ERRO_NEGOCIO_PONTO_CONSUMO_SITUACAO_NAO_FATURAVEL";

	String ERRO_NEGOCIO_PONTO_CONSUMO_SITUACAO_NAO_FATURAVEL_COM_CONSUMO = "ERRO_NEGOCIO_PONTO_CONSUMO_SITUACAO_NAO_FATURAVEL_COM_CONSUMO";

	String ERRO_NEGOCIO_CONSUMO_APURADO_NAO_INFORMADO = "ERRO_NEGOCIO_CONSUMO_APURADO_NAO_INFORMADO";

	String ERRO_NEGOCIO_SELECAO_DEBITO_OBRIGATORIO = "ERRO_NEGOCIO_SELECAO_DEBITO_OBRIGATORIO";

	String ERRO_NEGOCIO_AUSENCIA_DEBITO = "ERRO_NEGOCIO_AUSENCIA_DEBITO";

	String ERRO_NEGOCIO_CREDITO_MAIOR_DEBITO = "ERRO_NEGOCIO_CREDITO_MAIOR_DEBITO";

	String ERRO_NEGOCIO_REFATURAR_FATURAS_QUITADAS = "ERRO_NEGOCIO_REFATURAR_FATURAS_QUITADAS";

	String ERRO_NEGOCIO_FATURA_DIFERENTE_REFERENCIA_CONTABIL = "ERRO_NEGOCIO_FATURA_DIFERENTE_REFERENCIA_CONTABIL";

	String ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO = "ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO";

	String INDICADOR_IMOVEL_SEM_PONTO_CONSUMO = "indicadorImovelSemPontoConsumo";

	String ERRO_NEGOCIO_BASE_CALCULO_TRIBUTOS_NEGATIVA = "ERRO_NEGOCIO_BASE_CALCULO_TRIBUTOS_NEGATIVA";

	String ERRO_NEGOCIO_FATURA_VALOR_NEGATIVO = "ERRO_NEGOCIO_FATURA_VALOR_NEGATIVO";

	String ERRO_NEGOCIO_PONTOS_AGRUPADOS_DATA_LEITURA_DIFERENTE = "ERRO_NEGOCIO_PONTOS_AGRUPADOS_DATA_LEITURA_DIFERENTE";

	String ERRO_NEGOCIO_CREDITO_REMANESCENTE = "ERRO_NEGOCIO_CREDITO_REMANESCENTE";

	String ERRO_CONCORRENCIA = "ERRO_CONCORRENCIA";

	String ERRO_NEGOCIO_PARAMETRO_VALOR_MINIMO_GERACAO_DOC_COBRANCA_NAO_ENCONTRADO = 
					"ERRO_NEGOCIO_PARAMETRO_VALOR_MINIMO_GERACAO_DOC_COBRANCA_NAO_ENCONTRADO";

	String ERRO_NEGOCIO_REMOVER_GRUPO_FATURAMENTO = "ERRO_NEGOCIO_REMOVER_GRUPO_FATURAMENTO";

	String ERRO_NEGOCIO_REMOVER_PERIODICIDADE = "ERRO_NEGOCIO_REMOVER_PERIODICIDADE";

	String ERRO_NEGOCIO_CONTRATO_PONTOS_CONSUMO_SEGMENTOS_DIFERENTES = "ERRO_NEGOCIO_CONTRATO_PONTOS_CONSUMO_SEGMENTOS_DIFERENTES";

	String ERRO_NEGOCIO_FATURA_SERIE_NAO_CADASTRADA = "ERRO_NEGOCIO_FATURA_SERIE_NAO_CADASTRADA";

	public static final String ORDENACAO_NUMERO_FATURA_DESC = "ordenacaoNumeroFatura";

	/**
	 * Método responsável por consultar as faturas
	 * de acordo com os parametros informados.
	 * 
	 * @param idCliente
	 *            Chave primária do cliente
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @param parametros
	 *            Parametros para obter as
	 *            situações.
	 * @param chavesFatura
	 *            the chaves fatura
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @return Coleção de Faturas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturas(Long idCliente, Long idPontoConsumo, Map<String, Object[]> parametros, Long[] chavesFatura,
					Date campoDataReferencia, Boolean dataCancelamento) throws NegocioException;

	/**
	 * Método responsável por consultar as faturas
	 * pelas chaves primárias
	 * 
	 * @param chavesFatura
	 *            Chaves primárias das faturas
	 * @return Coleção de Faturas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturasPorChaves(Object[] chavesFatura) throws NegocioException;

	/**
	 * Retorna todas as Faturas que pertencem a um
	 * Parcelamento.
	 * 
	 * @param idParcelamento
	 *            chave do Parcelamento
	 * @return Lista das Faturas que pertencem ao
	 *         parcelamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasPorParcelamento(Long idParcelamento) throws NegocioException;

	/**
	 * Método responsável por consultar as faturas
	 * pelo número do contrato na qual elas estão
	 * vinculadas e pelo ano da data de vencimento
	 * da fatura.
	 * 
	 * @param numeroContrato
	 *            Número do contrato das fatura.
	 * @param anoFaturas
	 *            Ano da data de vencimento das
	 *            faturas.
	 * @param anoAtual
	 *            Indicador para pesquisar faturas
	 *            do ano atual ou dos anos
	 *            anteriores.
	 * @param parametros
	 *            Parametros para as situações das
	 *            faturas.
	 * @return Uma coleção de Fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Fatura> consultarFaturasPorContrato(String numeroContrato, String anoFaturas, boolean anoAtual,
					Map<String, Object[]> parametros) throws NegocioException;

	/**
	 * Método responsável por obter uma FaturaItem
	 * pela chave primária do
	 * CreditoDebitoARealizar.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            Chave primária do
	 *            CreditoDebitoARealizar.
	 * @return Uma FaturaItem.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	FaturaItem obterFaturaItemPorCreditoDebitoARealizar(Long idCreditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por instanciar uma
	 * Fatura Item.
	 * 
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeNegocio criarFaturaItem() throws NegocioException;

	/**
	 * Método responsável por instanciar uma
	 * Fatura Impressão.
	 * 
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeNegocio criarFaturaImpressao() throws NegocioException;

	/**
	 * Método responsável por instanciar uma
	 * Historico de Revisao da Fatura.
	 * 
	 * @return EntidadeNegocio
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	EntidadeNegocio criarFaturaHistoricoRevisao() throws NegocioException;

	/**
	 * Método responsável por instanciar um
	 * DadosResumoFatura.
	 * 
	 * @return DadosResumoFatura
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	DadosResumoFatura criarDadosResumoFatura() throws NegocioException;

	/**
	 * Método responsável por instanciar um
	 * DadosInclusaoFatura.
	 * 
	 * @return DadosLeituraConsumoFatura
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	DadosLeituraConsumoFatura criarDadosLeituraConsumoFatura() throws NegocioException;

	/**
	 * Método responsável por instanciar um
	 * DadosItensFatura.
	 * 
	 * @return DadosItensFatura
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	DadosItensFatura criarDadosItensFatura() throws NegocioException;

	/**
	 * Método responsável por instanciar um
	 * DadosGeraisItensFatura.
	 * 
	 * @return DadosGeraisItensFatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DadosGeraisItensFatura criarDadosGeraisItensFatura() throws NegocioException;

	/**
	 * Método responsável por instanciar uma
	 * Fatura.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFatura(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por Consultar faturas para relatório.
	 *
	 * @param filtro {@link Map}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<Fatura> consultarFaturaRelatorio(Map<String, Object> filtro) throws NegocioException;
	/**
	 * Método responsável por verificar se o ponto
	 * de consumo ou o tipo de nota foi
	 * selecionado.
	 * 
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @param indicadorCreditoDebito
	 *            Indicador para saber se é nota
	 *            de crédito ou nota de débito.
	 * @param idCliente {@link Long}
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarFiltroExibicaoNotasDebitoCredito(Long idPontoConsumo, String indicadorCreditoDebito, Long idCliente)
					throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * faturas selecionadas poderão ter suas datas
	 * de
	 * vencimento alteradas.
	 * 
	 * @param chavesPrimarias
	 *            As chaves das faturas
	 *            selecionadas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarFaturasAlteracaoVencimento(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por alterar a(s) data(s)
	 * de vencimento da(s) Fatura(s).
	 * 
	 * @param parametros
	 *            Mapa com os parametros para
	 *            alterar a(s) data(s) de
	 *            venciemnto da(s) Fatura(s).
	 * @param dadosAuditoria
	 *            Dados da auditoria.
	 * @return faturas para exibição(pdf).
	 * @throws GGASException
	 *             Caso ocorra algum erro no método.
	 */
	byte[] atualizarVencimentoFatura(Map<String, Object> parametros, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por verificar se as
	 * faturas a serem revisadas tem o mesmo
	 * motivo.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return true, caso exista revisao nas
	 *         faturas selecionadas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean validarRevisaoFatura(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar os Motivos de
	 * revisão de fatura.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<FaturaMotivoRevisao> listarMotivoRevisaoFatura() throws NegocioException;

	/**
	 * Método responsável por revisar ou tirar de
	 * revisão um grupo de faturas.
	 * 
	 * @param chavesFaturas
	 *            the chaves faturas
	 * @param idMotivoRevisao
	 *            the id motivo revisao
	 * @param existeMotivoRevisao
	 *            the existe motivo revisao
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param idFuncionario
	 *            the id funcionario
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void revisarFatura(Long[] chavesFaturas, Long idMotivoRevisao, boolean existeMotivoRevisao, DadosAuditoria dadosAuditoria,
					Long idFuncionario) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por executar o Processo
	 * de Revisão de Faturas.
	 * 
	 * @param processo
	 *            the processo
	 * @param logProcessamento
	 *            the log processamento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void processarRevisaoFatura(Processo processo, StringBuilder logProcessamento) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por listar as Faturas em
	 * Revisão.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> listarFaturasEmRevisao() throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * obrigatórios para a inclusão de nota de
	 * débito e
	 * crédito.
	 * 
	 * @param fatura
	 *            Nota de débito e crédito.
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrencia.
	 */
	void validarDadosNotaDebito(Fatura fatura) throws NegocioException;

	/**
	 * Método responsável por conciliar notas de
	 * débito e notas de crédito.
	 * 
	 * @param chavesNotasDebito
	 *            Chaves primárias das notas de
	 *            débito.
	 * @param chavesNotasCredito
	 *            Chaves primárias das notas de
	 *            crédito.
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void conciliarNotasDebitoCredito(Long[] chavesNotasDebito, Long[] chavesNotasCredito, DadosAuditoria dadosAuditoria)
					throws GGASException;

	/**
	 * Método responsável por inserir uma nota de
	 * débito ou crédito.
	 * 
	 * @param notaDebitoCredito
	 *            Nota de débito ou de crédito a
	 *            ser inserida.
	 * @param chavesPrimarias
	 *            Chaves primárias das notas de
	 *            débito ou crédito.
	 * @param chaveApuracaoPenalidade
	 *            the chave apuracao penalidade
	 * @return Array com o relatório da nota
	 *         inserida, caso seja nota de débito
	 *         e a chave primária
	 *         da nota.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Pair<byte[], Fatura> inserirNotaDebitoCredito(Fatura notaDebitoCredito, Long[] chavesPrimarias, Long chaveApuracaoPenalidade)
					throws GGASException;

	/**
	 * Método responsável por consultar as
	 * FaturaConciliacao pela chave primária do
	 * débito ou do
	 * crédito.
	 * 
	 * @param idDebitoCredito
	 *            Chave primária do débito ou do
	 *            crédito.
	 * @return Uma lista de FaturaConciliacao.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<FaturaConciliacao> consultarFaturaConciliacao(Long idDebitoCredito) throws NegocioException;

	/**
	 * Método responsável por cancelar notas de
	 * débito e notas de crédito.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das notas.
	 * @param idMotivoCancelamento
	 *            Chave primária do motivo de
	 *            cancelamento.
	 * @param descricaoCancelamento
	 *            Complemento do motivo de
	 *            cancelamento.
	 * @param dadosAuditoria
	 *            Dados de auditoria.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void cancelarNotaDebitoCredito(Long[] chavesPrimarias, Long idMotivoCancelamento, String descricaoCancelamento,
					DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por validar se o ponto
	 * de consumo possui um contrato ativo.
	 * 
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarPontoConsumoSelecionado(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * notas de débito / crédito selecionadas
	 * possuem
	 * situação de pagamento
	 * 'Paga'.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das notas.
	 * @throws NegocioException
	 *             Caso algum nota possua situação
	 *             de pagamento 'Paga'.
	 */
	void validarSituacaoNotasConciliacao(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por validar a seleção
	 * das notas de débito e crédito.
	 * 
	 * @param chavesNotasDebitos
	 *            Chaves primárias das notas de
	 *            débito.
	 * @param chavesNotasCredito
	 *            Chaves primárias das notas de
	 *            crédito.
	 * @throws NegocioException
	 *             Caso nenhuma nota de débito ou
	 *             crédito tenha sido selecionada
	 *             ou só um tipo de
	 *             nota tenha sido
	 *             selecionada.
	 */
	void validarNotasSelecionadas(Long[] chavesNotasDebitos, Long[] chavesNotasCredito) throws NegocioException;

	/**
	 * Método responsável por calcular o saldo da
	 * fatura informada, levando em consideração o
	 * recebimento e o valor concilidado.
	 * 
	 * @param fatura {@link Fatura}
	 * @return BigDecimal {@link Bigdecimal}
	 * @throws NegocioException {@link NegocioException}
	 */
	BigDecimal calcularSaldoFatura(Fatura fatura) throws NegocioException;
	
	/**
	 * Método responsável por calcular o saldo da
	 * fatura informada, levando em consideração o
	 * recebimento e o valor concilidado.
	 * @param operacaoGgas {@link boolean}
	 * @param fatura  {@link Fatura}
	 * @param corrigirSaldo		{@link boolean}
	 * @param adicionarImpontualidade	{@link boolean}
	 * @return  BigDecimal {@link Bigdecimal}
	 * @throws NegocioException		{@link NegocioException}
	 */
	Pair<BigDecimal, BigDecimal> calcularSaldoFatura(boolean operacaoGgas, Fatura fatura, boolean corrigirSaldo,
					boolean adicionarImpontualidade) throws NegocioException;
	/**
	 * Método responsável por calcular o saldo da
	 * fatura (first) e o saldo corrigido (second), levando em consideração o
	 * recebimento, o valor concilidado e a correcao monetaria.
	 * 
	 * @param fatura	{@link Fatura}
	 * @param corrigirSaldo	{@link boolean}
	 * @param adicionarImpontualidade	{@link boolean}
	 * @return BigDecimal {@link Bigdecimal}
	 * @throws NegocioException		{@link NegocioException}
	 */
	Pair<BigDecimal, BigDecimal> calcularSaldoFatura(Fatura fatura, boolean corrigirSaldo, boolean adicionarImpontualidade)
					throws NegocioException;
	/**
	 * Método responsável por calcular o saldo da
	 * fatura (first)
	 * 
	 * @param fatura {@link Fatura}
	 * @param dataFinalCalculo {@link Date}
	 * @param corrigirSaldo {@link boolean}
	 * @param adicionarImpontualidade {@link boolean}
	 * @return Pair {@link Pair}
	 * @throws NegocioException {@link NegocioException}
	 */
	Pair<BigDecimal, BigDecimal> calcularSaldoFatura(Fatura fatura, Date dataFinalCalculo, boolean corrigirSaldo,
					boolean adicionarImpontualidade) throws NegocioException;

	/**
	 * * Método responsável por calcular o saldo da
	 * fatura (first) e o saldo corrigido (second), levando em consideração o
	 * recebimento, o valor concilidado e a correcao monetaria ate uma data
	 * final definida.
	 * 
	 * @param operacaoGgas {@link boolean}
	 * @param fatura {@link Fatura}
	 * @param dataFinalCalculo {@link Date}
	 * @param corrigirSaldo {@link boolean}
	 * @param adicionarImpontualidade {@link boolean}
	 * @return Pair {@link Pair}
	 * @throws NegocioException {@link NegocioException}
	 */
	Pair<BigDecimal, BigDecimal> calcularSaldoFatura(boolean operacaoGgas, Fatura fatura, Date dataFinalCalculo, boolean corrigirSaldo,
					boolean adicionarImpontualidade) throws NegocioException;

	/**
	 * Método responsável por validar a situação
	 * das notas para cancelamento.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das notas.
	 * @throws NegocioException
	 *             Caso alguma nota esteja com
	 *             CreditoDebitoSituacao
	 *             cancelada.
	 */
	void validarSituacaoNotasCancelamento(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Validar filtro pesquisa fatura.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFiltroPesquisaFatura(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Validar situacao alteracao vencimento.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarSituacaoAlteracaoVencimento(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Obter fatura por historico medicao.
	 * 
	 * @param historicoMedicao
	 *            the historico medicao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> obterFaturaPorHistoricoMedicao(HistoricoMedicao historicoMedicao) throws NegocioException;

	/**
	 * Método responsável por gerar um relatório
	 * de nota crédito ou nota de débito.
	 * 
	 * @param notaInserida
	 *            Chave primária da nota que será
	 *            gerado o relatório.
	 * @param isSegundaVia
	 *            indica se o relatório a ser
	 *            gerado é segunda via.
	 * @return Um array de bytes com o relatório.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorioNotaDebitoCredito(Fatura notaInserida, boolean isSegundaVia) throws GGASException;

	/**
	 * Método responsável por consultar todos os
	 * Itens de Fatura de uma Tarifa.
	 * 
	 * @param idTarifa
	 *            chave Primária da Tarifa
	 * @return Coleção de Itens de Fatura
	 *         encontrados
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<FaturaItem> listarFaturaItemPorTarifa(Long idTarifa) throws NegocioException;

	/**
	 * Método responsável por montar os dados para
	 * exibição da lista de ítens de fatura de um
	 * contrato.
	 * 
	 * @param listaItemFaturamento
	 *            Lista de de
	 *            ContratoPontoConsumoItemFaturamento
	 *            .
	 * @param dataEmissaoInformada
	 *            the data emissao informada
	 * @return Um lista com as propriedades a
	 *         serem exibidas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<DadosGeraisItensFatura> montarDadosGeraisItensFatura(Collection<ContratoPontoConsumoItemFaturamento> listaItemFaturamento,
					Date dataEmissaoInformada) throws NegocioException;

	/**
	 * Método responsável por processar a
	 * catalogação das faturas em lote.
	 * 
	 * @param processo
	 *            the processo
	 * @param logProcessamento
	 *            the log processamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	Collection<FaturaImpressao> processarLotesFatura(Processo processo, StringBuilder logProcessamento) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Obter dados inclusao fatura.
	 *
	 * @param idCliente the id cliente
	 * @param pontoConsumo the ponto consumo
	 * @param periodoEmissao the periodo emissao
	 * @return the dados resumo fatura
	 * @throws GGASException the GGAS exception
	 */
	DadosResumoFatura obterDadosInclusaoFatura(Long idCliente, PontoConsumo pontoConsumo, String periodoEmissao) throws GGASException;

	/**
	 * Obter dados inclusao fatura.
	 *
	 * @param idCliente the id cliente
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param referencia the referencia
	 * @param ciclo the ciclo
	 * @param isFaturaEncerramento the is fatura encerramento
	 * @param periodoEmissao the periodo emissao
	 * @return the dados resumo fatura
	 * @throws GGASException the GGAS exception
	 */
	DadosResumoFatura obterDadosInclusaoFatura(Long idCliente, Collection<PontoConsumo> listaPontoConsumo, Integer referencia, Integer ciclo,
					Boolean isFaturaEncerramento, String periodoEmissao) throws GGASException;

	/**
	 * Processar emissao fatura.
	 * 
	 * @param processo
	 *            the processo
	 * @param logProcessamento
	 *            the log processamento
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void processarEmissaoFatura(Processo processo, StringBuilder logProcessamento) throws IOException, GGASException;

	/**
	 * Gera uma lista de Fatura Impressao livre de itens que não poderão ser processados
	 * @param processo o processo que executou a chamada
	 * @param logProcessamento o log processamento
	 * @return Collection A lista filtrada
	 * @throws NegocioException {@link NegocioException}
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 */
	Collection<FaturaImpressao> filtrarListaFaturaImpressao(Processo processo, StringBuilder logProcessamento)
			throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por consultar uma
	 * FaturaImpressao através de uma Fatura.
	 * 
	 * @param codigoFatura
	 *            código da Fatura
	 * @return FaturaImpressao que possui o código
	 *         Fatura
	 */
	FaturaImpressao obterFaturaImpressaoPorFatura(Long codigoFatura);

	/**
	 * Obter fatura impressao.
	 * 
	 * @param codigoFaturaImpressao
	 *            the codigo fatura impressao
	 * @return the fatura impressao
	 */
	FaturaImpressao obterFaturaImpressao(Long codigoFaturaImpressao);

	/**
	 * Método responsável por consultar os
	 * Documentos Fiscais através de uma Fatura.
	 * 
	 * @param codigoFatura
	 *            código da Fatura
	 * @return Collection<DocumentoFiscal>
	 */
	Collection<DocumentoFiscal> obterDocumentosFiscaisPorFatura(Long codigoFatura);

	/**
	 * Método responsável por consultar os
	 * Documentos Fiscais através do Id de uma fatura.
	 *
	 * @param codigoFatura
	 *            código da Fatura
	 * @return Collection<DocumentoFiscal>
	 */
	Collection<DocumentoFiscal> obterDocumentosFiscaisPorIdFatura(Long codigoFatura);

	/**
	 * Método responsável por consultar o
	 * Documento Fiscal mais recente através de
	 * uma Fatura.
	 * 
	 * @param codigoFatura
	 *            código da Fatura
	 * @return Collection<DocumentoFiscal>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DocumentoFiscal obterDocumentoFiscalPorFaturaMaisRecente(Long codigoFatura) throws NegocioException;

	/**
	 * Método responsável por consultar todas as
	 * Faturas que pertecem a uma Fatura agrupada.
	 * 
	 * @param chaveFaturaAgrupada
	 *            chave da Fatura agrupada
	 * @return Coleção de Tarifas consultadas
	 */
	Collection<Fatura> listarFaturaPorFaturaAgrupada(Long chaveFaturaAgrupada);

	/**
	 * Método responsável por consultar um
	 * DocumentoFiscal através do pontoConsumo e
	 * tipoFaturamento.
	 * 
	 * @param chavePontoConsumo
	 *            chave do ponto de consumo
	 * @param chaveContrato
	 *            chave do tipo de faturamento
	 * @param chaveFatura
	 *            a chave da Fatura que está sendo
	 *            montada
	 * @param chaveTipo
	 *            chave do tipo de faturamento
	 * @param anoMesReferencia
	 *            ano mês referência
	 * @param ciclo
	 *            ciclo
	 * @return DocumentoFiscal pesquisado
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<DocumentoFiscal> consultarDocumentoFiscalPorPontoConsumo(Long chavePontoConsumo, Long chaveContrato, Long chaveFatura,
					Long chaveTipo, Integer anoMesReferencia, Integer ciclo) throws NegocioException;

	/**
	 * Método responsável por consultar um
	 * FaturaItem pela Fatura e ItemFatura da
	 * Rubrica.
	 * 
	 * @param chaveFatura
	 *            chave da Fatura
	 * @param chaveItemFatura
	 *            chave do ItemFatura da Rubrica
	 * @return FaturaItem pesquisado
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	FaturaItem consultarFaturaItemPorCodigoFatura(Long chaveFatura, Long chaveItemFatura) throws NegocioException;

	/**
	 * Método responsável por consultar a data de
	 * pagamento mais recente de uma fatura
	 * através de
	 * FaturaConciliacao pela FaturaGeral.
	 * 
	 * @param chaveFaturaGeral
	 *            chave primária da Fatura Geral
	 * @return A data de pagamento consultada
	 */
	Date consultarDataPagamentoFaturaConciliacaoPorCodigoFaturaGeral(Long chaveFaturaGeral);

	/**
	 * Método responsável por consultar a tarifa
	 * utiliza numa fatura.
	 * 
	 * @param chaveFatura
	 *            chave da Fatura
	 * @param chaveItemFatura
	 *            chave do Item de Fatura
	 * @return A Tarifa utilizada na fatura
	 * @throws NumberFormatException
	 *             caso ocorra algum erro
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Tarifa consultarTarifaUtilizadaNaFatura(Long chaveFatura, Long chaveItemFatura) throws NegocioException;

	/**
	 * Método responsável por consultar as faturas
	 * vencidas de uma determinada fatura.
	 * 
	 * @param chavePontoConumo
	 *            chave primária do ponto de
	 *            consumo
	 * @param chaveContrato
	 *            chave primária do contrato
	 * @param chaveCliente
	 *            chave primária do cliente
	 * @param anoMesReferencia
	 *            ano/mês de referência
	 * @param numeroCiclo
	 *            ciclo
	 * @return Coleção de Faturas em atraso
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<Fatura> consultarFaturasVencidas(Long chavePontoConumo, Long chaveContrato, Long chaveCliente, Integer anoMesReferencia,
					Integer numeroCiclo) throws NegocioException;

	/**
	 * Método responsável por obter as Faturas que
	 * possuam a mesma FaturaAgrupamento.
	 * 
	 * @param idFaturaAgrupamento
	 *            the id fatura agrupamento
	 * @return the collection
	 */
	Collection<Fatura> consultarFaturaPorFaturaAgrupamento(Long idFaturaAgrupamento);


	/**
	 * Método responsável por consultar histórico de consumo por Fatura Agrupamento
	 *
	 * @param idFaturaAgrupamento - {@link Long}
	 * @return coleçao historico de consumo - {@link HistoricoConsumo}
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumoPorFaturaAgrupamento(Long idFaturaAgrupamento);

	/**
	 * Método responsável por instanciar uma
	 * Fatura Tributação.
	 * 
	 * @return FaturaTributacao Criada
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeNegocio criarFaturaTributacao() throws NegocioException;

	/**
	 * Método responsável por consultar uma
	 * FAturaTributacao pelo código da Fatura.
	 * 
	 * @param chaveFatura
	 *            chave primária da Fatura
	 * @return Coleção de FaturaTributação
	 *         encontradas
	 */
	Collection<FaturaTributacao> consultarFaturaTributacaoPorFatura(long chaveFatura);

	/**
	 * Método responsável por gerar o relatório de
	 * emissão Nota Fiscal / Fatura Gás.
	 * 
	 * @param fatura
	 *            a fatura que será gerada
	 * @param faturasFilhas
	 *            the faturas filhas
	 * @param sequencial
	 *            o número sequencial da
	 *            FaturaImpressao
	 * @param lote
	 *            o Lote da FaturaImpressao
	 * @param dadosAuditoria
	 *            o objeto de Auditoria dos Dados
	 * @param ultimaFatura
	 *            the ultima fatura
	 * @param acumuladoFaturas
	 *            the acumulado faturas
	 * @param segundaVia Se a emissão é para uma segunda via de fatura
	 * @param documentoFiscal
	 * 			the documento fiscal
	 * @return a fatura
	 * @throws GGASException
	 *             caso ocorra algum erro
	 */
	byte[] gerarEmissaoFatura(Fatura fatura, Collection<Fatura> faturasFilhas, Integer sequencial, Integer lote,
					DadosAuditoria dadosAuditoria, Boolean ultimaFatura, BigDecimal acumuladoFaturas, DocumentoFiscal documentoFiscal,
					boolean segundaVia)
					throws GGASException;

	/**
	 * Método responsável por capturar uma fatura
	 * já impressa e gerar uma segunda via para
	 * ela.
	 * 
	 * @param fatura
	 *            fatura a ser gerada a segunda
	 *            via.
	 * @param documentoFiscal {@link DocumentoFiscal}
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarImpressaoFaturaSegundaVia(Fatura fatura, DocumentoFiscal documentoFiscal) throws GGASException;

	/**
	 * Método responsável por gerar um DANFE a partir de um xml da NFe
	 *
	 * @param documentoFiscal {@link DocumentoFiscal}
	 * @return the byte[] {@link byte}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarImpressaoDanfe(DocumentoFiscal documentoFiscal) throws GGASException;

	/***
	 * Método responsável por listas as faturas
	 * que serão canceladas.
	 * 
	 * @param listaFatura
	 *            lista de faturas pesquisadas
	 * @param chavesPrimarias
	 *            chaves primárias de todas as
	 *            faturas a serem canceladas
	 * @return lista de faturas a serem canceladas
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Fatura> listaFaturaCancelar(Collection<Fatura> listaFatura, Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por cancelar as fatura
	 * selecionadas.
	 * 
	 * @param chavesPrimarias
	 *            Chave primária das faturas a
	 *            serem canceladas.
	 * @param idMotivoCancelamento
	 *            Chave do motivo de cancelamento.
	 * @param dadosAuditoria
	 *            Dados de auditoria.
	 * @param isRefaturar
	 *            Indicador de refaturamento.
	 * @param constMotivoBaixaPdd
	 * 			the constMotivoBaixaPdd
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro.
	 */
	void cancelarFaturas(Long[] chavesPrimarias, Long idMotivoCancelamento, DadosAuditoria dadosAuditoria, boolean isRefaturar,
					String constMotivoBaixaPdd) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por solicitar o
	 * cancelamento de faturas.
	 * O método irá atualizar dados do documento
	 * fiscal e o sistema aguarda
	 * retorno do sistema de nfe para de fato
	 * realizar o cancelamento.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param idMotivoCancelamento
	 *            the id motivo cancelamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param isRefaturar
	 *            the is refaturar
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void solicitarCancelamentoFaturas(Long[] chavesPrimarias, Long idMotivoCancelamento, DadosAuditoria dadosAuditoria, boolean isRefaturar)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Remover faturas.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void removerFaturas(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * *
	 * Valida se foi escolhido um motivo de
	 * cancelamento da fatura.
	 * 
	 * @param idMotivoCancelamento
	 *            chave primária do motivo de
	 *            cancelamento
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	void validarMotivoCancelamento(Long idMotivoCancelamento) throws NegocioException;

	/**
	 * Método responsável por criar um Documento
	 * Fiscal.
	 * 
	 * @return EntidadeNegocio que representa um
	 *         Documento Fiscal.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	EntidadeNegocio criarDocumentoFiscal() throws NegocioException;

	/**
	 * Método responsável por criar uma Serie.
	 * 
	 * @return EntidadeNegocio que representa uma
	 *         Serie.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarSerie() throws NegocioException;

	/**
	 * Método responsável por consistir os dados
	 * de leitura na inclusão de fatura.
	 * 
	 * @param listaHistoricoMedicao
	 *            Lista de histórico de medição.
	 * @param isFaturaAvulso
	 *            the is fatura avulso
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws FormatoInvalidoException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void consistirLeituraFaturamento(Collection<HistoricoMedicao> listaHistoricoMedicao, Boolean isFaturaAvulso) throws GGASException;

	/**
	 * Método responsável por obter os dados para exibição da tela de resumo da
	 * fatura.
	 * 
	 * @param dadosResumoFatura   the Dados Resumo Fatura
	 * @param listaCreditoDebito  the Lista Credito Debito
	 * @param isInclusao          the isInclusao
	 * @param tipoApuracao        the Tipo Apuracao
	 * @param isSimulacao         the isSimulacao
	 * @param valorInformado      the Valor Informado
	 * @param idMotivoComplemento the idMotivoComplemento
	 * @return Um Vo de DadosResumoFatura.
	 * @throws NegocioException      the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	DadosResumoFatura obterDadosResumoFatura(DadosResumoFatura dadosResumoFatura,
			Collection<CreditoDebitoARealizar> listaCreditoDebito, boolean isInclusao, String tipoApuracao,
			boolean isSimulacao, String valorInformado, Long idMotivoComplemento)
			throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por obter os Pontos de
	 * Consumo de uma Fatura.
	 * 
	 * @param idFatura
	 *            Chave primária da Fatura
	 * @return Ima lista de Pontos de Consumo da
	 *         Fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Long[] obterChavesPontosConsumoFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por processar a inclusão
	 * de fatura.
	 * 
	 * @param dadosResumoFatura
	 *            DadosResumoFatura.
	 * @param isIncluirDocumentoCobranca
	 *            indicador se deve incluir
	 *            documentos de cobrança.
	 * @param isBatch
	 *            indicador de processamento batch
	 *            ou por tela.
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 * 			the GGASException
	 */
	Collection<Fatura> processarInclusaoFatura(DadosResumoFatura dadosResumoFatura, boolean isIncluirDocumentoCobranca, boolean isBatch,
					DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por obter o tipo de
	 * documento fatura.
	 * 
	 * @return TipoDocumento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoDocumento obterTipoDocumentoFatura() throws NegocioException;

	/**
	 * Método responsável por validar o saldo
	 * remanescente da fatura de um cliente.
	 * 
	 * @param fatura
	 *            the fatura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarSaldoRemanescenteCredito(Fatura fatura) throws NegocioException;

	/**
	 * Método responsável por validar a quantidade
	 * maxima consecutiva de um tipo de consumo de
	 * faturamento.
	 * 
	 * @param historicoConsumo
	 *            historico consumo usado como
	 *            base
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarQuantidadeMaximaConsecutivaFaturamento(HistoricoConsumo historicoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter a serie atual.
	 * 
	 * @param idTipoOperacao
	 *            the id tipo operacao
	 * @param idTipoFaturamento
	 *            the id tipo faturamento
	 * @param isContingenciaScan
	 *            the is contingencia scan
	 * @param isSerieEletronica
	 *            the is serie eletronica
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @return Serie.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Serie obterSerieAtual(Long idTipoOperacao, Long idTipoFaturamento, Boolean isContingenciaScan, Boolean isSerieEletronica,
					Collection<PontoConsumo> listaPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter a natura de
	 * operação CFOP.
	 * 
	 * @param idTipoNota
	 *            Chave primária do tipo da nota
	 *            fiscal.
	 * @param ramoAtividade
	 *            Ramo de atividade do ponto de
	 *            consumo.
	 * @param segmento
	 *            Segmento do ponto de consumo.
	 * @param rubrica
	 *            Rubrica.
	 * @return Uma NaturezaOperacaoCFOP
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	NaturezaOperacaoCFOP obterNaturezaOperacao(Long idTipoNota, RamoAtividade ramoAtividade, Segmento segmento, Rubrica rubrica)
					throws NegocioException;

	/**
	 * Método responsável por verficar
	 * inadimplencia do ponto de consumo.
	 * 
	 * @param idPontoConsumo
	 *            Chave do ponto de consumo.
	 * @return True caso seja inadimplente.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarInadimplenciaPontoConsumo(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por retornar os dados da
	 * fatura para detalhamento.
	 * 
	 * @param idFatura
	 *            Chave da Fatura.
	 * @param idDocumentoFiscal
	 *            the id documento fiscal
	 * @return DadosResumoFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	DadosResumoFatura obterDadosDetalhamentoFatura(Long idFatura, Long idDocumentoFiscal) throws NegocioException;

	/**
	 * Método responsável por obter o número da
	 * série usada no mês atual e atualiza-lo.
	 * 
	 * @param tipoOperacao
	 *            Indica se a série é de saída ou
	 *            entrada.
	 * @param tipoFaturamento
	 *            Indica se a série é produto ou
	 *            serviço.
	 * @param dadosAuditoria
	 *            Dados de auditoria.
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @param fatura
	 *            the fatura
	 * @return Um mapa com o número da série e o
	 *         sequencial de numeração.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Map<String, Object> obterNumeroSerie(EntidadeConteudo tipoOperacao, EntidadeConteudo tipoFaturamento, DadosAuditoria dadosAuditoria,
					Collection<PontoConsumo> listaPontoConsumo, Fatura fatura) throws NegocioException;

	/**
	 * Método responsável por validar as faturas
	 * que irão ser refaturadas.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias das faturas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarFaturasParaRefaturar(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por Calcular a QDP
	 * Média.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dataPeriodicidade
	 *            the data periodicidade
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal calcularQDPMedia(PontoConsumo pontoConsumo, Date dataPeriodicidade) throws NegocioException;

	/**
	 * Método responsável por obter os dados da
	 * fatura para exibir a tela de refaturar
	 * Fatura.
	 * 
	 * @param chavePrimaria
	 *            A chave da fatura
	 * @return DadosResumoFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DadosResumoFatura obterDadosRefaturarFatura(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por refaturar uma
	 * fatura.
	 * Este método irá cancelar a fatura e gerar
	 * uma nova com os novos valores informados.
	 * 
	 * @param dadosResumoFatura
	 *            Dados Resumo Fatura
	 * @param dadosAuditoria
	 *            Dados Auditoria.
	 * @return Faturas Retificada
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<Fatura> refaturarFatura(DadosResumoFatura dadosResumoFatura, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por validar o filtro de
	 * pesquisa da tela de Guia de Pagamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFiltroPesquisaNotaDebito(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por receber uma colecao
	 * de faturas e gerar o documento de impressão
	 * delas.
	 * 
	 * @param dadosAuditoria
	 *            parametro necessário para
	 *            atualização das faturas.
	 * @param listaFatura
	 *            lista das faturas que terao os
	 *            documentos gerados.
	 * @return array de byte representando os
	 *         documentos de fatura.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] processarDocumentoRetornoFatura(DadosAuditoria dadosAuditoria, Collection<Fatura> listaFatura) throws GGASException;

	/**
	 * Método responsável por criar um VO
	 * DadosItemFaturamento.
	 * 
	 * @return DadosItemFaturamento criado
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	DadosItemFaturamento criarDadosItemFaturamento() throws NegocioException;

	/**
	 * Método responsável por criar um
	 * FaturaItemImpressao.
	 * 
	 * @return FaturaItemImpressao criado
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	FaturaItemImpressao criarFaturaItemImpressao() throws NegocioException;

	/**
	 * Método responsável por validar a leitura e
	 * consumo ao avançar para a tela de resumo
	 * inclusão
	 * fatura.
	 * 
	 * @param listaHistoricoMedicao
	 *            the lista historico medicao
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	void validarAvancarResumoInclusaoFatura(Collection<HistoricoMedicao> listaHistoricoMedicao) throws NegocioException;

	/**
	 * Método responsável por realizar o
	 * faturamento de um grupo.
	 * 
	 * @param grupoFaturamento
	 * 			the Grupo Faturamento
	 * @param rota
	 * 			the Rota
	 * @param dadosAuditoria
	 * 			the Dados Auditoria
	 * @param logProcessamento
	 * 			the Log Processamento
	 * @param cronogramaAtividadeFaturamento
	 * 			the Cronograma Atividade Faturamento
	 * @param isSimulacao
	 * 			the isSimulacao
	 * @param periodoEmissao
	 * 			the Periodo Emissao
	 * @param processo
	 * 			the Processo
	 * @throws GGASException
	 * 			the GGASException
	 */
	void faturarGrupo(GrupoFaturamento grupoFaturamento, Rota rota, DadosAuditoria dadosAuditoria, StringBuilder logProcessamento,
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, boolean isSimulacao, String periodoEmissao, Processo processo)
					throws GGASException;

	/**
	 * Método responsável por realizar o
	 * faturamento de um grupo.
	 * 
	 * @param grupoFaturamento
	 * 		the grupoFaturamento
	 * @param rota
	 * 			the Rota
	 * @param dadosAuditoria
	 * 			the Dados Auditoria
	 * @param logProcessamento
	 * 			the Log Processamento
	 * @param cronogramaAtividadeFaturamento
	 * 			the Cronograma Atividade Faturamento
	 * @param isSimulacao
	 * 		the isSimulacao
	 * @param periodoEmissao
	 * 			the Periodo Emissao
	 * @param tipoDataRetroativaInvalida
	 * 			the Tipo Data Retroativa Invalida
	 * @param processo
	 * 			the Processo
	 * @throws GGASException
	 * 			the GGASException
	 */
	void faturarGrupo(GrupoFaturamento grupoFaturamento, Rota rota, DadosAuditoria dadosAuditoria, StringBuilder logProcessamento,
					CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, boolean isSimulacao, String periodoEmissao,
					TipoDataRetroativaInvalida tipoDataRetroativaInvalida, Processo processo) throws GGASException;

	/**
	 * Método responsável por validar a leitura e
	 * consumo ao avançar para a tela de resumo
	 * inclusão
	 * fatura.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @return the collection
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<FaturaItem> listarFaturaItemPorChaveFatura(Long idFatura) throws NegocioException;

	/**
	 * Método responsável por inserir uma
	 * anormalidade na base de dados.
	 * 
	 * @param codigoAnormalidade
	 *            o código de identificação do
	 *            tipo de anormalidade
	 * @param pontoConsumo
	 *            o ponto de consumo que lançou a
	 *            anormalidade
	 * @param referencia
	 *            a referencia que estava sendo
	 *            faturada
	 * @param ciclo
	 *            o ciclo que estava sendo
	 *            faturado
	 * @param dadosAuditoria
	 *            o DadosAuditoria
	 * @param descricaoComplementar
	 * 			the Descricao Complementar
	 * @return A anormalidade que foi inserida
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	FaturamentoAnormalidade inserirAnormalidade(String codigoAnormalidade, PontoConsumo pontoConsumo, Integer referencia, Integer ciclo,
					DadosAuditoria dadosAuditoria, String descricaoComplementar) throws NegocioException;

	/**
	 * Método responsável por validar a leitura e
	 * consumo ao avançar para a tela de resumo
	 * inclusão
	 * fatura.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param parametros
	 *            the parametros
	 * @param chavesFatura
	 *            the chaves fatura
	 * @return the collection
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<Fatura> consultarFaturasParaParcelamento(Long idCliente, Long idPontoConsumo, Map<String, Object[]> parametros,
					Long[] chavesFatura) throws NegocioException;

	/**
	 * Método responsável por emitir o relatório
	 * de faturas por rubrica.
	 * 
	 * @param parametroAnoMesFechamento
	 *            the parametro ano mes fechamento
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] gerarRelatorioResumoFaturaRubrica(Integer parametroAnoMesFechamento, Integer numeroCiclo) throws NegocioException;

	/**
	 * Método responsável por consultar faturas
	 * usando o ano mes referência como parametro.
	 * 
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasPorAnoMesReferencia(Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Método responsável por consultar faturas
	 * usando o ano mes referência como parametro
	 * e
	 * ordenado de acordo com o campoOrdenacao.
	 * 
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param camposOrdenacao
	 *            the campos ordenacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasPorDataLeituraInformada(Integer anoMesReferencia, Integer numeroCiclo, String camposOrdenacao)
					throws NegocioException;

	/**
	 * Método responsável por realizar a inclusão
	 * da fatura após a análise da anomalia.
	 * 
	 * @param listaHistoricoAnomaliaFaturamento
	 *            the lista historico anomalia faturamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return indicador se a fatura foi inserida
	 *         com sucesso
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	boolean processarInclusaoFaturaAnomaliaCorrigida(Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento,
					DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Método responsável por faturar a lista de
	 * clientes passada por parâmetro.
	 * 
	 * @param listaClientes
	 * 		the Lista Clientes
	 * @param dadosAuditoria
	 * 			the Dados Auditoria
	 * @param logProcessamento
	 * 		the Log Processamento
	 * @param processo
	 * 			the Processo
	 * @throws NegocioException
	 * 			the NegocioException
	 */
	void faturarClientes(Collection<Cliente> listaClientes, DadosAuditoria dadosAuditoria, StringBuilder logProcessamento,
			Processo processo) throws NegocioException;

	/**
	 * Método Responsável por verificar se todos
	 * os pontos de consumo da rota foram
	 * faturados.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarFaturamentoPorRota(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Método Responsável por retornar os grupos
	 * de faturamento que tenham ano mes e ciclo
	 * diferente
	 * de 0.
	 * 
	 * @return Coleção de GrupoFaturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<GrupoFaturamento> consultarGrupoFaturamentoAnoMesReferencia() throws NegocioException;

	/**
	 * Cancela uma nota crédito débito do cliente
	 * referente à rubrica informada.
	 * 
	 * @param contrato
	 *            the contrato
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param rubrica
	 *            Tipo de rubrica do débito
	 *            desejado.
	 * @param dadosAuditoria
	 * 		the Dados Auditoria
	 *            .
	 * @param isRefaturamento
	 * 			the isRefaturamento
	 *            .
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void cancelarNotaCreditoDebitoAnteriorPorClienteRubrica(Contrato contrato, PontoConsumo pontoConsumo, Rubrica rubrica,
					DadosAuditoria dadosAuditoria, boolean isRefaturamento) throws GGASException;

	/**
	 * Insere uma fatura no sistema gerando os
	 * devidos lançamentos contábeis
	 * correspondentes.
	 * 
	 * @param fatura
	 *            Fatura nova a inserir
	 * @return identificadorFatura
	 * @throws GGASException
	 *             the GGAS exception
	 */
	long inserir(Fatura fatura) throws GGASException;

	/**
	 * Ponto consumo possui nota debito recisao.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param contrato
	 *            the contrato
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long pontoConsumoPossuiNotaDebitoRecisao(Long idPontoConsumo, Contrato contrato) throws NegocioException;

	/**
	 * Método responsável por validar seleção de
	 * notas de débito / crédito.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das notas,
	 *            idCliente e idPontoConsumo.
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 *             
	 *  @throws GGASException
	 *  		the GGAS exception           
	 */
	void validarSelecaoNotas(Long[] chavesPrimarias, Long idCliente, Long idPontoConsumo) throws GGASException;

	/**
	 * Método responsável por verficar
	 * Encerramento da Fatura.
	 * 
	 * @param idPontoConsumo
	 *            e contrato
	 * @param contrato
	 *            the contrato
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long verificarEncerramento(Long idPontoConsumo, Contrato contrato) throws NegocioException;

	/**
	 * Gerar Demonstrativo Consumo Mensal
	 * 
	 * @param faturaParam - {@link Fatura}
	 * @return byte - {@link byte}
	 * @throws GGASException the GGAS Exception
	 */
	byte[] gerarDemonstrativoConsumoMensal(Fatura faturaParam) throws GGASException;

	/**
	 * Método responsável por consultar todos os
	 * FaturaItemImpressao de um
	 * determinado faturaItem.
	 * 
	 * @param itemFatura
	 *            o faturaItem
	 * @return os FaturaItemImpressao do
	 *         FaturaItem
	 */
	Collection<FaturaItemImpressao> consultarFaturaItemImpressaoPorFaturaItem(FaturaItem itemFatura);

	/**
	 * Cria um débito de multa recisória com o
	 * valor informado para um dado contrato.
	 * Caso já exista um débito de multa recisória
	 * para esse contrato ele será cancelado antes
	 * da
	 * inclusão do novo débito.
	 * 
	 * @param contrato
	 *            Contrato alvo.
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param valorMulta
	 *            Valor total do débito a
	 *            cadastrar.
	 * @param dataVencimento
	 *            the data vencimento
	 * @return the object[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Pair<byte[], Fatura> registrarNotaDebitoMultaRecisoria(Contrato contrato, PontoConsumo pontoConsumo, BigDecimal valorMulta,
					String dataVencimento) throws GGASException;

	/**
	 * Método para verificar se a fatura está com
	 * situação pendente.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @return boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarFaturaSituacaoPendente(Long idFatura) throws NegocioException;

	/**
	 * Método para pesquisar as faturas que possam
	 * ser reclassificadas Contabilmente.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasAReclassificar() throws NegocioException;

	/**
	 * Consultar documento fiscal por tipo operacao status nfe.
	 * 
	 * @param codigoTipoOperacao
	 *            the codigo tipo operacao
	 * @param codigoStatusNfe
	 *            the codigo status nfe
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<DocumentoFiscal> consultarDocumentoFiscalPorTipoOperacaoStatusNfe(Long codigoTipoOperacao, Long codigoStatusNfe)
					throws NegocioException;

	/**
	 * Método responsável por cancelar uma fatura.
	 * Chamado pelo Batch
	 * VerificarAtutorizacaoNfe.
	 * 
	 * @param fatura
	 *            fatura
	 * @param motivoCancelamento
	 *            motivo do cancelamento
	 * @param dadosAuditoria
	 *            dados da Auditoria
	 * @param isRefaturar
	 *            indicador refaturamento
	 * @param constMotivoBaixaPdd
	 *            the const motivo baixa pdd
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void cancelarFatura(Fatura fatura, EntidadeConteudo motivoCancelamento, DadosAuditoria dadosAuditoria, boolean isRefaturar,
					String constMotivoBaixaPdd) throws NegocioException, ConcorrenciaException;

	/**
	 * Desmembra um fatura item de acordo com as faixas de tarifa
	 * @param dadosItensFatura Os dados da faturaItem para ser analiado
	 * @return A lista referente a cascata para os dados informados
	 * @throws NegocioException O NegocioException
	 */
	List<DadosItensFatura> cascatearFaturaItem(DadosItensFatura dadosItensFatura) throws NegocioException;

	/**
	 * Método responsável por inserir informação
	 * de debito automatico cobrança bancaria.
	 * 
	 * @param documentosCobranca
	 *            the documentos cobranca
	 * @param dadosAuditoria
	 *            dados da Auditoria
	 * @param fatura
	 *            fatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void inserirInfoDebitoAutoCobrancaBancaria(List<DocumentoCobranca> documentosCobranca, DadosAuditoria dadosAuditoria, Fatura fatura)
					throws NegocioException;

	/**
	 * Método responsável por atualizar o
	 * documento fiscal.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarDocumentoFiscal(DocumentoFiscal documentoFiscal) throws NegocioException, ConcorrenciaException;

	/**
	 * Monta um mapa com os parâmetros usados para
	 * definir o evento comercial de uma fatura.
	 * 
	 * @return mapParametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Long> parametrosMontarCodigoChaveEventoComercial() throws NegocioException;

	/**
	 * Prepara os dados necessários e chama a
	 * rotina de registro dos lançamentos
	 * contábeis
	 * sintéticos e analíticos de faturas.
	 * Usado para registrar lancamento depois da
	 * autorização da nota fiscal
	 * 
	 * @param fatura
	 *            the fatura
	 * @param operacao
	 *            the operacao
	 * @param parametros
	 *            the parametros
	 * @param indicadorProduto
	 *            the indicador produto
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void registrarLancamentoContabilAposAutorizacaoFiscal(Fatura fatura, OperacaoContabil operacao, Map<String, Long> parametros,
					Boolean indicadorProduto) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por cancelar fatura a
	 * partir do botão não faturar da tela de
	 * analise de
	 * anomalias.
	 * 
	 * @param historicoAnomaliaFaturamento
	 *            the historico anomalia faturamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void cancelarFaturaAnomalia(HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento, DadosAuditoria dadosAuditoria)
					throws NegocioException;

	/**
	 * Método para obter o documento fiscal de
	 * saída.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param tipoOperacao
	 *            the tipo operacao
	 * @return the documento fiscal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public DocumentoFiscal obterDocumentoFiscalPorFatura(Fatura fatura, EntidadeConteudo tipoOperacao) throws NegocioException;

	/**
	 * Método responsável por agrupar uma coleção
	 * de faturas de acordo com o seus Históricos
	 * de
	 * Consumo.
	 * 
	 * @param colecaoFaturasDuplicadas
	 *            coleção de faturas que serão
	 *            agrupadas
	 * @return Mapa de Historico de Consumo por
	 *         coleção de Faturas
	 * @throws NegocioException
	 *             Caso ocorra algum erro
	 */
	Map<HistoricoConsumo, Collection<Fatura>> agruparFaturasPorHistoricoConsumo(Collection<Fatura> colecaoFaturasDuplicadas)
					throws NegocioException;

	/**
	 * Prepara os dados necessários e chama a
	 * rotina de registro dos lançamentos
	 * contábeis
	 * sintéticos e analíticos de faturas que não
	 * tenham segmento.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param operacao
	 *            the operacao
	 * @param parametros
	 *            the parametros
	 * @param dadosResumoFatura
	 *            the dados resumo fatura
	 * @param indicadorProduto
	 *            the indicador produto
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void registrarLancamentoContabilComDadosResumoFatura(Fatura fatura, OperacaoContabil operacao, Map<String, Long> parametros,
					DadosResumoFatura dadosResumoFatura, Boolean indicadorProduto) throws NegocioException, ConcorrenciaException;

	/**
	 * Obter NaturazaOperacaoCFOP.
	 * 
	 * @param idTipoOperacao
	 *            the id tipo operacao
	 * @param ramoAtividade
	 *            the ramo atividade
	 * @param segmento
	 *            the segmento
	 * @param rubrica
	 *            the rubrica
	 * @return the natureza operacao cfop
	 * @throws NegocioException
	 *             the negocio exception
	 */
	NaturezaOperacaoCFOP obterNaturezaOperacaoCFOP(Long idTipoOperacao, RamoAtividade ramoAtividade, Segmento segmento, Rubrica rubrica)
					throws NegocioException;

	/**
	 * Prepara os dados necessários e chama a
	 * rotina de registro dos lançamentos
	 * contábeis
	 * sintéticos e analíticos.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param operacao
	 *            the operacao
	 * @param parametros
	 *            the parametros
	 * @param constMotivoBaixaPdd
	 *            the const motivo baixa pdd
	 * @param regimeContabilCompetencia
	 *            the regime contabil competencia
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void registrarLancamentoContabil(Fatura fatura, OperacaoContabil operacao, Map<String, Long> parametros, String constMotivoBaixaPdd,
					Boolean regimeContabilCompetencia) throws NegocioException, ConcorrenciaException;

	/**
	 * Monta os tributos na fatura.
	 * 
	 * @param notaDebitoCredito
	 *            the nota debito credito
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<FaturaTributacao> montarFaturaTributacaoNotaDebito(Fatura notaDebitoCredito) throws NegocioException;

	/**
	 * Obter documento fiscal por chave.
	 * 
	 * @param idDocumentoFiscal
	 *            the id documento fiscal
	 * @return the documento fiscal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DocumentoFiscal obterDocumentoFiscalPorChave(Long idDocumentoFiscal) throws NegocioException;

	/**
	 * Recupera os consumos em aberto.
	 * 
	 * @param dadosResumoFatura
	 *            the dados resumo fatura
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoConsumo> mapearConsumoEmAberto(DadosResumoFatura dadosResumoFatura) throws NegocioException;

	/**
	 * Monta o VO utilizado para a inclusão de
	 * faturas relativo aos dados informados.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param referencia
	 *            the referencia
	 * @param ciclo
	 *            the ciclo
	 * @param isSimulacao
	 *            the is simulacao
	 * @param periodoEmissao
	 *            the periodo emissao
	 * @param param - {@link Map}
	 * @return the dados resumo fatura
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DadosResumoFatura obterDadosFatura(PontoConsumo pontoConsumo, Integer referencia, Integer ciclo, boolean isSimulacao,
					String periodoEmissao, Map<String, Object> param) throws GGASException;

	/**
	 * Inclui no mapa de anomalias o ponto de
	 * consumo para a anomalia informada.
	 * 
	 * @param mapaAnomalias
	 *            the mapa anomalias
	 * @param anormalidade
	 *            the anormalidade
	 * @param pontoConsumo
	 *            the ponto consumo
	 */
	void popularMapaAnomalias(Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, FaturamentoAnormalidade anormalidade,
					PontoConsumo pontoConsumo);

	/**
	 * Consultar natureza operacao configuracao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<NaturezaOperacaoConfiguracao> consultarNaturezaOperacaoConfiguracao(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar fatura impressao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<FaturaImpressao> consultarFaturaImpressao(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar grupo faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<GrupoFaturamento> consultarGrupoFaturamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Obter documento fiscal por codigo fatura.
	 * 
	 * @param codigoFatura
	 *            the codigo fatura
	 * @param isValido
	 *            the is valido
	 * @return the documento fiscal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DocumentoFiscal obterDocumentoFiscalPorCodigoFatura(Long codigoFatura, boolean isValido) throws NegocioException;

	/**
	 * Obter dados incluir nfd fatura.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the dados resumo fatura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DadosResumoFatura obterDadosIncluirNFDFatura(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir nota fiscal devolucao.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosResumoFatura
	 *            the dados resumo fatura
	 * @param idFatura
	 *            the id fatura
	 * @param idMotivoCancelamento
	 *            the id motivo cancelamento
	 * @param consumoItem
	 * 			  the consumo item
	 * @param vlrUnitarioItem
	 * 			  the valor unitario item
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void inserirNotaFiscalDevolucao(DocumentoFiscal documentoFiscal, DadosResumoFatura dadosResumoFatura, Long idFatura,
			Long idMotivoCancelamento, Float[] consumoItem, Float[] vlrUnitarioItem, DadosAuditoria dadosAuditoria)
			throws GGASException;

	/**
	 * Listar fatura item tributacao.
	 * 
	 * @param faturaItem
	 *            the fatura item
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<FaturaItemTributacao> listarFaturaItemTributacao(FaturaItem faturaItem) throws NegocioException;

	/**
	 * Inserir nota.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param chavesFaturas
	 *            the chaves faturas
	 * @param chaveApuracaoPenalidade
	 *            the chave apuracao penalidade
	 * @return the fatura
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Fatura inserirNota(Fatura fatura, Long[] chavesFaturas, Long chaveApuracaoPenalidade) throws GGASException;

	/**
	 * Método responsável por finalizar um ano mes
	 * de faturamento, verificando as rotas dos
	 * grupos de faturamento e gerando relatório
	 * para as faturas.
	 * 
	 * @param processo {@link Processo}
	 * @param logProcessamento {@link StringBuilder}
	 * @param grupoFaturamento {@link GrupoFaturamento}
	 * @throws GGASException {@link GGASException}
	 *             
	 */
	void processarEncerrarCicloFaturamento(Processo processo, StringBuilder logProcessamento,
			GrupoFaturamento grupoFaturamento) throws GGASException;

	/**
	 * Consultar faturas vencidas ponto consumo.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param acaoComando
	 *            the acao comando
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasVencidasPontoConsumo(Map<String, Object> filtro, AcaoComando acaoComando) throws NegocioException;

	/**
	 * Agrupar faturas por data vencimento.
	 * 
	 * @param colecaoFaturasDuplicadas
	 *            the colecao faturas duplicadas
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Collection<Fatura>> agruparFaturasPorDataVencimento(Collection<Fatura> colecaoFaturasDuplicadas) throws NegocioException;

	/**
	 * Agrupar faturas por imovel ou cliente.
	 * 
	 * @param colecaoFaturasDuplicadas
	 *            the colecao faturas duplicadas
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Collection<Fatura>> agruparFaturasPorImovelOuCliente(Collection<Fatura> colecaoFaturasDuplicadas) throws NegocioException;

	/**
	 * Consultar faturas por rota.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasPorRota(Map<String, Object> parametros) throws NegocioException;

	/**
	 * Listar fatura por ponto cosumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<FaturaVO> listarFaturaPorPontoCosumo(long idPontoConsumo) throws GGASException;

	/**
	 * Consulta documento fiscal.
	 * 
	 * @param nfeTO
	 *            the nfe to
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws HibernateException
	 *             the hibernate exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	List<DocumentoFiscal> consultaDocumentoFiscal(NfeTO nfeTO, List<Long> chavePontoConsumo) throws HibernateException,
					GGASException;

	/**
	 * Obter documento fiscal por numeroe serie.
	 * 
	 * @param numeroDocumentoFiscal
	 *            the numero documento fiscal
	 * @param numeroSerie
	 *            the numero serie
	 * @return the documento fiscal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DocumentoFiscal obterDocumentoFiscalPorNumeroeSerie(long numeroDocumentoFiscal, String numeroSerie) throws NegocioException;

	/** 
	 * Gerar boleto bancario padrão.
	 * 
	 * @param faturaParam
	 * 			the faturaParam
	 * @param ultimaFatura
	 * 				the ultima fatura
	 * @param tipoDocumento
	 * 				the tipo documento
	 * @param dataVencimento
	 * 				the data vencimento
	 * @return byte[]
	 * @throws GGASException
	 * 				the GGAS exception
	 */
	byte[] gerarBoletoBancarioPadrao(Fatura faturaParam, Boolean ultimaFatura,
					String tipoDocumento, String dataVencimento) throws GGASException;

	/**
	 * Gerar boleto bancaria padrão
	 * 
	 * @param faturaParam
	 * 			the faturaParam
	 * @param ultimaFatura
	 * 			the ultima Fatura
	 * @param constanteTipoDocumento
	 * 			the constante tipo documento
	 * @param dataVencimento
	 * 			the data vencimento
	 * @param subtrairValorRecebido
	 * 				the subtrair valor recebido
	 * @return byte[]
	 * @throws GGASException
	 * 			the GGAS exception
	 */
	byte[] gerarBoletoBancarioPadrao(Fatura faturaParam, Boolean ultimaFatura,
					String constanteTipoDocumento, String dataVencimento, boolean subtrairValorRecebido) throws GGASException;

	/**
	 * Atualizar dias atraso.
	 * 
	 * @param listaFatura
	 *            the lista fatura
	 * @param diasAtraso
	 *            the dias atraso
	 * @return the collection
	 */
	Collection<Fatura> atualizarDiasAtraso(Collection<Fatura> listaFatura, Integer diasAtraso);

	/**
	 * Busca os detalhamentos de crédito/debito para uma fatura em específico.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public List<CreditoDebitoDetalhamentoVO> carregarCreditoDebitoDetalhamentoPorFatura(Long idFatura) throws NegocioException;

	/**
	 * Verificar encerrar ciclo faturamento.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarEncerrarCicloFaturamento(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Popular filtro credito debitos.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Object> popularFiltroCreditoDebitos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Validar fatura avulsa transportadora.
	 * 
	 * @param idTransportadora
	 *            the id transportadora
	 * @param idVeiculo
	 *            the id veiculo
	 * @param idMotorista
	 *            the id motorista
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFaturaAvulsaTransportadora(Long idTransportadora, Long idVeiculo, Long idMotorista) throws NegocioException;

	/**
	 * Consultar fatura remover transportadora.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<Fatura> consultarFaturaRemoverTransportadora(Map<String, Object> filtro);

	/**
	 * Obter desconto grupo economico por ponto consumo.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the big decimal
	 */
	BigDecimal obterDescontoGrupoEconomicoPorPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Gerar emissao fatura com layout.
	 * 
	 * @param faturaParam
	 *            the fatura param
	 * @param faturasFilhas
	 *            the faturas filhas
	 * @param sequencial
	 *            the sequencial
	 * @param lote
	 *            the lote
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param ultimaFatura
	 *            the ultima fatura
	 * @param acumuladoFaturas
	 *            the acumulado faturas
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarEmissaoFaturaComLayout(Fatura faturaParam, Collection<Fatura> faturasFilhas, Integer sequencial, Integer lote,
					DadosAuditoria dadosAuditoria, Boolean ultimaFatura, BigDecimal acumuladoFaturas, DocumentoFiscal documentoFiscal)
					throws GGASException;

	/**
	 * Remover fatura por dias atraso.
	 * 
	 * @param listaFatura
	 *            the lista fatura
	 * @param dias
	 *            the dias
	 * @param dataReferencia
	 *            the data referencia
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> removerFaturaPorDiasAtraso(Collection<Fatura> listaFatura, Integer dias, Date dataReferencia)
					throws NegocioException;

	/**
	 * Gerar relatorio descontos concedidos fatura.
	 * 
	 * @param pesquisaRelatorioDescontosConcedidosFaturaVO
	 *            the pesquisa relatorio descontos concedidos fatura vo
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] gerarRelatorioDescontosConcedidosFatura(PesquisaRelatorioDescontosConcedidosFaturaVO pesquisaRelatorioDescontosConcedidosFaturaVO)
					throws NegocioException;

	/**
	 * Listar faturas por ponto consumo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<Fatura> listarFaturasPorPontoConsumo(Long chavePrimaria);

	/**
	 * Inserir historico nota debito credito.
	 * 
	 * @param historicoNotaDebitoCredito
	 *            the historico nota debito credito
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirHistoricoNotaDebitoCredito(HistoricoNotaDebitoCredito historicoNotaDebitoCredito) throws NegocioException;

	/**
	 * Gets the lista historico nota debito credito.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @return the lista historico nota debito credito
	 */
	Collection<HistoricoNotaDebitoCredito> getListaHistoricoNotaDebitoCredito(Long idFatura);

	/**
	 * Validar situacao pagamento fatura.
	 * 
	 * @param faturas
	 *            the faturas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarSituacaoPagamentoFatura(List<Fatura> faturas) throws NegocioException;

	/**
	 * Obter quantidade fatura historico revisao por fatura.
	 * 
	 * @param chaveFatura
	 *            the chave fatura
	 * @return the long
	 */
	Long obterQuantidadeFaturaHistoricoRevisaoPorFatura(Long chaveFatura);

	/**
	 * Este metodo busca fatura itens do ponto de consumo e ano/mes referencia
	 * e soma o consumo.
	 * 
	 * @param idPontoConsumo
	 *            chave primaria do ponto de consumo
	 * @param anoMesReferenciaFatura
	 *            ano mes da fatura do ponto de consumo
	 * @return soma do consumo do ponto de consumo no ano/mes de referencia
	 */
	BigDecimal somaMedidaConsumoPorPontoConsumoAnoMesReferencia(Long idPontoConsumo, int anoMesReferenciaFatura);

	/**
	 * Consulta faturas do tipo nota de débito.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Coleção de faturas do tipo nota de débito.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<Fatura> consultarNotaDebito(Map<String, Object> filtro) throws GGASException;

	/**
	 *  Incluir faturar complementar
	 * 
	 * @param idMotivoComplemento
	 * 			the idMotivoComplemento
	 * @param idFatura
	 * 			the idFatura
	 * @param volumeApurado
	 * 			the volumeApurado
	 * @param valor
	 * 			the valor
	 * @param observacoesComplemento
	 * 			the observacoes Complemento
	 * @param dadosResumoFatura
	 * 			the dados Resumo Fatura
	 * @param pontoConsumo
	 * 			the pontoConsumo
	 * @param contratoPontoConsumo
	 * 			the contratoPontoConsumo
	 * @param historico
	 * 			the historico
	 * @param dadosAuditoria
	 * 			the dadosAuditoria
	 * @throws GGASException
	 * 			the GGAS exception
	 */
	void incluirFaturaComplementar(Long idMotivoComplemento, Long idFatura, String[] volumeApurado, String valor,
					String observacoesComplemento, DadosResumoFatura dadosResumoFatura, PontoConsumo pontoConsumo,
					ContratoPontoConsumo contratoPontoConsumo, HistoricoMedicao historico, DadosAuditoria dadosAuditoria)
					throws GGASException;

	/**
	 * Método responsável aplicar reducao baseCalculo.
	 * 
	 * @param baseCalculo
	 * 				the baseCalculo
	 * @param tributoAliquota
	 * 				the tributoAliquota
	 * @return the BigDecimal
	 * @throws NegocioException
	 * 				the negocio exception
	 */
	BigDecimal aplicarReducaoBaseCalculo(BigDecimal baseCalculo, 
					TributoAliquota tributoAliquota) throws NegocioException;

	
	/**
	 * Fator reducao base calculo tributo icms.
	 *
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	BigDecimal fatorReducaoBaseCalculoTributoIcms() throws NegocioException;

	/**
	 * Efetua as validações necessárias e depois calcula o nosso número
	 *
	 * @param arrecadadorContratoConvenio o convênio
	 * @param dadosAuditoria dados para auditoria
	 * @return o nosso número
	 * @throws NegocioException em caso de erro na consulta
	 */
	Long calcularNossoNumero(ArrecadadorContratoConvenio arrecadadorContratoConvenio, DadosAuditoria dadosAuditoria) throws NegocioException;
	
	/**
	 * Método responsável por identificar se a fatura é agrupadora ou normal.
	 * 
	 * @param fatura
	 * 			the fatura
	 * @return true para fatura agrupadora e false para fatura normal.
	 */
	boolean isFaturaAgrupadora(Fatura fatura);

	/**
	 * Método responsável por identificar se a fatura é agrupada ou normal.
	 * 
	 * @param fatura
	 * 			the fatura
	 * @return true para fatura agrupada e false para fatura normal.
	 */
	boolean isFaturaAgrupada(Fatura fatura);

	/**
	 * Consultar Documentos Fiscais por Status da NFE e data de Emissão
	 * @param statusNfe {@link Long}
	 * @param dataEmissao {@link Date}
	 * @param numeroCiclo {@link Integer}
	 * @param anoMesFaturamento {@link Integer}
	 * @return Collection {@link Collection}
	 */
	Collection<DocumentoFiscal> consultarDocumentosFiscaisPorStatusNfeDataEmissao(
			Long statusNfe, Date dataEmissao, Integer anoMesFaturamento, Integer numeroCiclo);

	/**
	 * Obter Doc Fiscal por fatura
	 * @param idFatura {@link Long}
	 * @return DocumentoFiscal {@link DocumentoFiscal}
	 *  @throws NegocioException {@link NegocioException}
	 */
	public DocumentoFiscal obterDocumentoFiscalporNumeroFatura(Long idFatura) throws NegocioException;

	/**
	 * Obter Número do Documento Fiscal por fatura mais recente.
	 * @param idFatura {@link Long}
	 * @return numero do Documento Fiscal {@link Long}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Long obterNumeroDocumentoFiscalPorFaturaMaisRecente(Long idFatura) throws NegocioException;

	/**
	 * Obtem valor de imposto da tributação de fatura por fatura.
	 * @param chaveFatura {@link Long}
	 * @param chaveTributo  {@link Long}
	 * @return Tributação de Fatura {@link BigDecimal}
	 */
	BigDecimal consultaValorDeImpostoDeFaturaTributacaoPorFatura(long chaveFatura, long chaveTributo);

	/**
	 * Obtem valor unitário de item de fatura.
	 *
	 * @param chaveFatura {@link Long}
	 * @param chaveRubrica {@link Long}
	 * @return Valor Unitário {@link BigDecimal}
	 */
	BigDecimal obterValorUnitarioDeFaturaItem(Long chaveFatura, Long chaveRubrica);

	/**
	 * Método responsável por pesquisar faturas
	 *
	 * @param filtro {@link Map}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<Fatura> consultarFaturaPesquisa(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter Documentos Fiscais por chaves primarias de faturas
	 *
	 * @param chaveFatura {@link Long}
	 * @return Map {@link Map}
	 */
	public Map<Long, Collection<DocumentoFiscal>> obterDocumentosFiscaisPorFaturas(Long[] chaveFatura);

	/**
	 * Consultar quantidade pontos consumo fatura
	 *
	 * @param grupoFaturamento - {@link GrupoFaturamento}
	 * @param anoMesFaturamento - {@link Integer}
	 * @param ciclo - {@link Integer}
	 * @return quantidade de pontos de consumo - {@link Long}
	 */
	Long consultarQuantidadePontosConsumoFatura(Long grupoFaturamento, Integer anoMesFaturamento,Integer ciclo);


	/**
	 * Método responsável por obter Documentos Fiscais por lista de faturas
	 *
	 * @param listaFatura {@link List}
	 * @return {@link Map}
	 */
	public Map<Long, Collection<DocumentoFiscal>> obterDocumentosFiscaisPorListaFatura(List<Fatura> listaFatura);
	
	/**
	 * Cria um novo movimento de cobranca ao prorrogar uma nota de debito
	 * 
	 * @param faturas - Lista de faturas
	 * @throws GGASException Caso ocorra algum erro 
	 */
	public void atualizarVencimentoNotaDebito(List<Fatura> faturas) throws GGASException;

	/**
	 * Método responsável por listar documento fiscal por cliente.
	 *
	 * @param cliente 
	 * 				the cliente
	 * @return the collection
	 */
	List<DocumentoFiscalVO> listarDocumentoFiscalPorCliente(Cliente cliente);

	/**
	 * Método responsável por obter a data de pagamento da fatura
	 * 
	 * @param faturaID - {@link Long}
	 * @return data de pagamento - {@link Date}
	 */
	Date obterDataPagamentoFatura(Long faturaID);

	/**
	 * Método responsável por Consultar os seguintes dados de faturas 
	 * chavePrimaria
	 * dataEmissao 
	 * valorTotal 
	 * contratoAtual 
	 * valorConciliado 
	 * faturarAvulso
	 * pontoConsumo 
	 * situacaoPagamento
	 * 
	 * @param filtro {@link Map}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<Fatura> consultarDadosFatura(Map<String, Object> filtro) throws NegocioException;
	
	/**
	 * Método responsável por retornar um mapa com uma listas de faturaItens por chavePrimaria de fatura.
	 * 
	 * @param chavesFaturas {@link Long[]}
	 * @return Map {@link Map}
	 */
	public Map<Long, Collection<FaturaItem>> consultarFaturaItemPorChaveFatura(Long[] chavesFaturas);
	
	/**
	 * Método responsável por retornar a Data de Geracao
	 * 
	 * @param codigoFatura {@link Long}
	 * @return DataGeracao {@link Date}
	 */
	public FaturaImpressao obterDataGeracaoFaturaImpressaoPorFatura(Long codigoFatura);

	/**
	 * Métodos responsável por trazer descrição do tipo de operação
	 * o número do documento fiscal, a fatura
	 *  relacionados ao Documento Fiscal.
	 * 
	 * @param chavePontoConsumo {@link Long}
	 * @param chaveContrato {@link Long}
	 * @param chaveFatura {@link Long}
	 * @param chaveTipo {@link Long}
	 * @param anoMesReferencia {@link Integer}
	 * @param ciclo {@link Integer}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<DocumentoFiscal> consultarDadosDocumentoFiscalPorPontoConsumo(Long chavePontoConsumo,
			Long chaveContrato, Long chaveFatura, Long chaveTipo, Integer anoMesReferencia, Integer ciclo)
			throws NegocioException;
	
	/**
	 * Método responsável por consultar 
	 * 
	 * @param itemFatura {@link FaturaItem}
	 * @return Collection {@link Colletion}
	 */
	public Map<Long, Collection<FaturaItemImpressao>> consultarMapaFaturaItemImpressaoPorFaturaItem(Collection<FaturaItem> itemFatura);
	
	/**
	 * Método responsável por consultar a descricao e a chavePrimaria do statusNFE
	 * 
	 * @param fatura {@link Fatura}
	 * @param tipoOperacao {@link EntidadeConteudo}
	 * @return DocumentoFiscal {@link DocumentoFiscal}
	 * @throws NegocioException {@link NegocioException}
	 */
	public DocumentoFiscal obterNumeroSerieDocumentoFiscalPorFatura(Fatura fatura, EntidadeConteudo tipoOperacao) throws NegocioException;
	
	/**
	 * Método responsável por retornar um mapa com 
	 * 
	 * @param faturas {@link Collection}
	 * @param tipoOperacao {@link EntidadeConteudo}
	 * @return Map {@link Map}
	 */
	public Map<Long, Collection<DocumentoFiscal>> obterMapaDocumentosFiscaisPorFatura(Collection<Fatura> faturas,
			EntidadeConteudo tipoOperacao);
	
	/**
	 * Método responsável por consultar um valorImposto e percentualAliquota de uma entidade FaturaTributacao.
	 * 
	 * @param chaveFatura {@link Long}
	 * @return Collection {@link Collection}
	 */
	public Collection<FaturaTributacao> consultarValoresFaturaTributacaoPorFatura(long chaveFatura);

	/**
	 * Obtém uma coleção de {@link Serie} por tipo de operação.
	 * @param tipoOperacao - {@link EntidadeConteudo}
	 * @return coleção de {@link Serie}
	 */
	Collection<Serie> consultarSeriePorTipoOperacao(EntidadeConteudo tipoOperacao);
	
    /**
	 * Consulta faturas vencidas por ponto de consumo
	 * @param chavePontoConsumo - {@link Long}
	 * @return colecao faturas vencidas por ponto de consumo
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<Fatura> consultarFaturasVencidasPontoConsumo(Long chavePontoConsumo) throws NegocioException;

	/**
	 * Valida se tem pontos de consumos com valor 0 e consumo 0
	 * @param listaDadosResumo - {@link DadosResumoFatura}
	 * @param cronograma - {@link Cronograma}
	 * @param dadosAuditoria - {@link DadosAuditorias}
	 * @param listaPontoConsumo - {@link PontoConsumo}
	 */
	void validarPontosConsumoSemConsumo(List<DadosResumoFatura> listaDadosResumo,
			CronogramaAtividadeFaturamento cronograma, DadosAuditoria dadosAuditoria,
			Collection<PontoConsumo> listaPontoConsumo);

	BigDecimal[] consultarValorVolumeTotalPeriodo(Date dataInicial, Date dataFinal);

	Collection<Fatura> consultarFaturasRelatorioVolume(Map<String, Object> filtro) throws NegocioException;

	Collection<Fatura> consultarFaturasAgrupadasVencidas(Long chaveContrato, Long chaveCliente,
			Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;
	/**
	 * Método responsável pelo processamento e emissão de faturas.
	 * @param processo - {@link Processo}
	 * @param contratoPorPontoConsumo - {@link ContratoPontoConsumo}
	 * @param chavesPontosPorContrato - {@link Map}
	 * @param listaImpressao - {@link List}
	 * @param dadosAuditoria - {@link DadosAuditoria}
	 * @param controladorAtividadeConogramaFaturamento - {@link ControladorCronogramaAtividadeFaturamento}
	 * @param lote - {@link Integer}
	 * @param faturaImpressao - {@link FaturaImpressao}
	 * @return dataEmissao - {@link Date}
	 * @throws NegocioException - {@link NegocioException}
	 * @throws GGASException - {@link GGASException}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 */
	Date emitirFaturas(Processo processo, Map<Long, Collection<Contrato>> contratoPorPontoConsumo,
			Map<Long, Collection<Long>> chavesPontosPorContrato, List<FaturaImpressao> listaImpressao,
			DadosAuditoria dadosAuditoria,
			ControladorCronogramaAtividadeFaturamento controladorAtividadeConogramaFaturamento, Integer lote,
			FaturaImpressao faturaImpressao, Collection<byte[]> listaFaturasCanceladas) throws NegocioException, GGASException, ConcorrenciaException;


	/**
	 * Consultar Faturas Vencidas por Cliente
	 * @param chaveCliente - {@link Long}
	 * @return lista faturas por cliente - {@link Fatura}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<Fatura> consultarFaturasVencidasCliente(Long chaveCliente) throws NegocioException;

	Collection<Fatura> consultarFaturasEmAbertoPorPontoConsumo(Long chavePontoConsumo);

	Date obterDataEmissaoMaxima();

	BigDecimal definirVolumeAFaturarSemProgramacao(PontoConsumo pontoConsumo, HistoricoConsumo historicoAFaturar,
			Boolean isContratoAditadoAlterado) throws NegocioException;

	Collection<TitulosGSAVO> obterTitulosGSAPorPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException;

	List<Object> listarFaturasAbertoGSA(String codigoUnico);

	List<Object> listarFaturasAbertoPorClienteGSA(String identificacaoCliente);

	Collection<TitulosGSAVO> obterTitulosAbertoCliente(String identificacaoCliente) throws NegocioException;

	Collection<TitulosGSAVO> obterTitulosPorNumero(String[] numerosTitulos) throws NegocioException;

}
