/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Classe responsável pela representação da entidade RelatorioFatura
 *
 */
public class RelatorioFaturaVO implements Serializable {

	private static final long serialVersionUID = -4593979344281413040L;

	private String dtEmissao;

	private String dtVencimento;

	private String mesAno;

	private String numeroNotaFiscal;

	private String numeroSerie;

	private String referenciaCiclo;

	private String nomeCDL;

	private String enderecoCDL;

	private String bairroCidadeEstadoCEPCDL;

	private String cnpjCDL;

	private String inscricaoEstadualCDL;

	private String telefone;

	private String url;

	private String dataPostagem;

	private String nomeCliente;

	private String docIdentificador;

	private String enderecoCliente;

	private String bairroCliente;

	private String municipioCliente;

	private String valorAPagar;

	private String codigoCliente;

	private String inscricaoEstadualCliente;

	private String contrato;

	private String ufCliente;

	private String cepCliente;

	private List<SubRelatorioPontoConsumoVO> colecaoPontoConsumo;

	private List<SubRelatorioFaturasVencidasVO> colecaoFaturasVencidas;

	private List<SubRelatorioFornecimentoVO> colecaoFornecimentos;

	private List<SubRelatorioHistoricoConsumoVO> colecaoHistoricoConsumo;

	private List<SubRelatorioProdutosVO> colecaoProdutos;

	private List<SubRelatorioTributosIncluidosVO> colecaoTributosIncluidos;

	public String getDtEmissao() {

		return dtEmissao;
	}

	public void setDtEmissao(String dtEmissao) {

		this.dtEmissao = dtEmissao;
	}

	public String getDtVencimento() {

		return dtVencimento;
	}

	public void setDtVencimento(String dtVencimento) {

		this.dtVencimento = dtVencimento;
	}

	public String getMesAno() {

		return mesAno;
	}

	public void setMesAno(String mesAno) {

		this.mesAno = mesAno;
	}

	public String getNumeroNotaFiscal() {

		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {

		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	public String getNumeroSerie() {

		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	public String getReferenciaCiclo() {

		return referenciaCiclo;
	}

	public void setReferenciaCiclo(String referenciaCiclo) {

		this.referenciaCiclo = referenciaCiclo;
	}

	public String getNomeCDL() {

		return nomeCDL;
	}

	public void setNomeCDL(String nomeCDL) {

		this.nomeCDL = nomeCDL;
	}

	public String getEnderecoCDL() {

		return enderecoCDL;
	}

	public void setEnderecoCDL(String enderecoCDL) {

		this.enderecoCDL = enderecoCDL;
	}

	public String getBairroCidadeEstadoCEPCDL() {

		return bairroCidadeEstadoCEPCDL;
	}

	public void setBairroCidadeEstadoCEPCDL(String bairroCidadeEstadoCEPCDL) {

		this.bairroCidadeEstadoCEPCDL = bairroCidadeEstadoCEPCDL;
	}

	public String getCnpjCDL() {

		return cnpjCDL;
	}

	public void setCnpjCDL(String cnpjCDL) {

		this.cnpjCDL = cnpjCDL;
	}

	public String getInscricaoEstadualCDL() {

		return inscricaoEstadualCDL;
	}

	public void setInscricaoEstadualCDL(String inscricaoEstadualCDL) {

		this.inscricaoEstadualCDL = inscricaoEstadualCDL;
	}

	public String getTelefone() {

		return telefone;
	}

	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	public String getUrl() {

		return url;
	}

	public void setUrl(String url) {

		this.url = url;
	}

	public String getDataPostagem() {

		return dataPostagem;
	}

	public void setDataPostagem(String dataPostagem) {

		this.dataPostagem = dataPostagem;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public String getDocIdentificador() {

		return docIdentificador;
	}

	public void setDocIdentificador(String docIdentificador) {

		this.docIdentificador = docIdentificador;
	}

	public String getEnderecoCliente() {

		return enderecoCliente;
	}

	public void setEnderecoCliente(String enderecoCliente) {

		this.enderecoCliente = enderecoCliente;
	}

	public String getBairroCliente() {

		return bairroCliente;
	}

	public void setBairroCliente(String bairroCliente) {

		this.bairroCliente = bairroCliente;
	}

	public String getMunicipioCliente() {

		return municipioCliente;
	}

	public void setMunicipioCliente(String municipioCliente) {

		this.municipioCliente = municipioCliente;
	}

	public String getValorAPagar() {

		return valorAPagar;
	}

	public void setValorAPagar(String valorAPagar) {

		this.valorAPagar = valorAPagar;
	}

	public String getCodigoCliente() {

		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {

		this.codigoCliente = codigoCliente;
	}

	public String getInscricaoEstadualCliente() {

		return inscricaoEstadualCliente;
	}

	public void setInscricaoEstadualCliente(String inscricaoEstadualCliente) {

		this.inscricaoEstadualCliente = inscricaoEstadualCliente;
	}

	public String getContrato() {

		return contrato;
	}

	public void setContrato(String contrato) {

		this.contrato = contrato;
	}

	public String getUfCliente() {

		return ufCliente;
	}

	public void setUfCliente(String ufCliente) {

		this.ufCliente = ufCliente;
	}

	public String getCepCliente() {

		return cepCliente;
	}

	public void setCepCliente(String cepCliente) {

		this.cepCliente = cepCliente;
	}

	public List<SubRelatorioPontoConsumoVO> getColecaoPontoConsumo() {

		return colecaoPontoConsumo;
	}

	public void setColecaoPontoConsumo(List<SubRelatorioPontoConsumoVO> colecaoPontoConsumo) {

		this.colecaoPontoConsumo = colecaoPontoConsumo;
	}

	public List<SubRelatorioFaturasVencidasVO> getColecaoFaturasVencidas() {

		return colecaoFaturasVencidas;
	}

	public void setColecaoFaturasVencidas(List<SubRelatorioFaturasVencidasVO> colecaoFaturasVencidas) {

		this.colecaoFaturasVencidas = colecaoFaturasVencidas;
	}

	public List<SubRelatorioFornecimentoVO> getColecaoFornecimentos() {

		return colecaoFornecimentos;
	}

	public void setColecaoFornecimentos(List<SubRelatorioFornecimentoVO> colecaoFornecimentos) {

		this.colecaoFornecimentos = colecaoFornecimentos;
	}

	public List<SubRelatorioProdutosVO> getColecaoProdutos() {

		return colecaoProdutos;
	}

	public void setColecaoProdutos(List<SubRelatorioProdutosVO> colecaoProdutos) {

		this.colecaoProdutos = colecaoProdutos;
	}

	public List<SubRelatorioTributosIncluidosVO> getColecaoTributosIncluidos() {

		return colecaoTributosIncluidos;
	}

	public void setColecaoTributosIncluidos(List<SubRelatorioTributosIncluidosVO> colecaoTributosIncluidos) {

		this.colecaoTributosIncluidos = colecaoTributosIncluidos;
	}

	public List<SubRelatorioHistoricoConsumoVO> getColecaoHistoricoConsumo() {

		return colecaoHistoricoConsumo;
	}

	public void setColecaoHistoricoConsumo(List<SubRelatorioHistoricoConsumoVO> colecaoHistoricoConsumo) {

		this.colecaoHistoricoConsumo = colecaoHistoricoConsumo;
	}

}
