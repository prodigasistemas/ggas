/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.fatura.impl.TipoDataRetroativaInvalida;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * aos dados de resumo da fatura
 *
 */
public interface DadosResumoFatura extends Serializable {

	String BEAN_ID_DADOS_RESUMO_FATURA = "dadosResumoFatura";

	/**
	 * @return the ciclo
	 */
	Integer getCiclo();

	/**
	 * @param ciclo
	 *            the ciclo to set
	 */
	void setCiclo(Integer ciclo);

	/**
	 * @return the referencia
	 */
	Integer getReferencia();

	/**
	 * @param referencia
	 *            the referencia to set
	 */
	void setReferencia(Integer referencia);

	/**
	 * @return the quantidadeCiclo
	 */
	Integer getQuantidadeCiclo();

	/**
	 * @param quantidadeCiclo
	 *            the quantidadeCiclo to set
	 */
	void setQuantidadeCiclo(Integer quantidadeCiclo);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the cobrancaAbaixoMinimo
	 */
	boolean isCobrancaAbaixoMinimo();

	/**
	 * @param cobrancaAbaixoMinimo
	 *            the cobrancaAbaixoMinimo to set
	 */
	void setCobrancaAbaixoMinimo(boolean cobrancaAbaixoMinimo);

	/**
	 * @return the faturamentoAgrupado
	 */
	boolean isFaturamentoAgrupado();

	/**
	 * @param faturamentoAgrupado
	 *            the faturamentoAgrupado to set
	 */
	void setFaturamentoAgrupado(boolean faturamentoAgrupado);

	/**
	 * @return the listaDadosGeraisItens
	 */
	Collection<DadosGeraisItensFatura> getListaDadosGeraisItens();

	/**
	 * @param listaDadosGeraisItens
	 *            the listaDadosGeraisItens to set
	 */
	void setListaDadosGeraisItens(Collection<DadosGeraisItensFatura> listaDadosGeraisItens);

	/**
	 * @return the listaPontoConsumo
	 */
	List<PontoConsumo> getListaPontoConsumo();

	/**
	 * @param listaPontoConsumo
	 *            the listaPontoConsumo to set
	 */
	void setListaPontoConsumo(List<PontoConsumo> listaPontoConsumo);

	/**
	 * @return the listaHistoricoConsumo
	 */
	List<HistoricoConsumo> getListaHistoricoConsumo();

	/**
	 * @param listaHistoricoConsumo
	 *            the listaHistoricoConsumo to set
	 */
	void setListaHistoricoConsumo(List<HistoricoConsumo> listaHistoricoConsumo);

	/**
	 * @return the listaDadosLeituraConsumoFatura
	 */
	Collection<DadosLeituraConsumoFatura> getListaDadosLeituraConsumoFatura();

	/**
	 * @param listaDadosLeituraConsumoFatura
	 *            the
	 *            listaDadosLeituraConsumoFatura
	 *            to set
	 */
	void setListaDadosLeituraConsumoFatura(Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura);

	/**
	 * @return the listaFatura
	 */
	Collection<Fatura> getListaFaturasPendentes();

	/**
	 * @param listaFaturasPendentes - Set Fista fatuas pendentes
	 */
	void setListaFaturasPendentes(Collection<Fatura> listaFaturasPendentes);

	/**
	 * @return the listaDadosTributoVO
	 */
	Collection<DadosTributoVO> getListaDadosTributoVO();

	/**
	 * @param listaDadosTributoVO
	 *            the listaDadosTributoVO to set
	 */
	void setListaDadosTributoVO(Collection<DadosTributoVO> listaDadosTributoVO);

	/**
	 * @return the medicaoDiaria
	 */
	boolean isMedicaoDiaria();

	/**
	 * @param medicaoDiaria
	 *            the medicaoDiaria to set
	 */
	void setMedicaoDiaria(boolean medicaoDiaria);

	/**
	 * @return the idMotivo
	 */
	long getIdMotivo();

	/**
	 * @param idMotivo
	 *            the idMotivo to set
	 */
	void setIdMotivo(long idMotivo);

	/**
	 * @return the contrato
	 */
	Contrato getContrato();

	/**
	 * @param contrato
	 *            the contrato to set
	 */
	void setContrato(Contrato contrato);

	/**
	 * Recupera Data de emissão informada pelo usuário no momento de execução de batch para grupo de faturamento.
	 * 
	 * @return dataEmissaoInformada
	 */
	public Date getDataEmissaoInformada();

	/**
	 * Seta Data de emissão informada pelo usuário no momento de execução de batch para grupo de faturamento.
	 * 
	 * @param dataEmissaoInformada
	 */
	public void setDataEmissaoInformada(Date dataEmissaoInformada);

	/**
	 * @return Date - Data Emissão
	 */
	Date getDataEmissao();

	/**
	 * @param dataEmissao - Set Data Emissão
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * @return String - Motivo
	 */
	String getMotivo();

	/**
	 * @param motivo - Set motivo
	 */
	void setMotivo(String motivo);

	/**
	 * @return String - Numero Contrato
	 */
	String getNumeroContrato();

	/**
	 * @param numeroContrato - Set Numero Contrato
	 */
	void setNumeroContrato(String numeroContrato);

	/**
	 * @return the dadosAuditoria
	 */
	DadosAuditoria getDadosAuditoria();

	/**
	 * @param dadosAuditoria
	 *            the dadosAuditoria to set
	 */
	void setDadosAuditoria(DadosAuditoria dadosAuditoria);

	/**
	 * @return the chavesFaturasPendentes
	 */
	Long[] getChavesFaturasPendentes();

	/**
	 * @param chavesFaturasPendentes
	 *            the chavesFaturasPendentes to
	 *            set
	 */
	void setChavesFaturasPendentes(Long[] chavesFaturasPendentes);

	/**
	 * @return the exibirAlerta
	 */
	Boolean getExibirAlerta();

	/**
	 * @param exibirAlerta
	 *            the exibirAlerta to set
	 */
	void setExibirAlerta(Boolean exibirAlerta);

	/**
	 * @return the listaCreditoDebitoARealizar
	 */
	Collection<CreditoDebitoARealizar> getListaCreditoDebitoARealizar();

	/**
	 * @param listaCreditoDebitoARealizar
	 *            the listaCreditoDebitoARealizar
	 *            to set
	 */
	void setListaCreditoDebitoARealizar(Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar);

	/**
	 * @return
	 */
	Collection<FaturaItem> getListaFaturaItem();

	/**
	 * @param listaFaturaItem
	 */
	void setListaFaturaItem(Collection<FaturaItem> listaFaturaItem);

	/**
	 * @return the listaDebitosCreditoApuracao
	 */
	Collection<CreditoDebitoDetalhamento> getListaDebitosCreditoApuracao();

	/**
	 * @param listaDebitosCreditoApuracao
	 *            the listaDebitosCreditoApuracao
	 *            to set
	 */
	void setListaDebitosCreditoApuracao(Collection<CreditoDebitoDetalhamento> listaDebitosCreditoApuracao);

	/**
	 * @return the
	 *         listaCreditoDebitoARealizarTotal
	 */
	Collection<CreditoDebitoARealizar> getListaCreditoDebitoARealizarTotal();

	/**
	 * @param listaCreditoDebitoARealizarTotal
	 *            the
	 *            listaCreditoDebitoARealizarTotal
	 *            to set
	 */
	void setListaCreditoDebitoARealizarTotal(Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizarTotal);

	/**
	 * @return the idFatura
	 */
	long getIdFatura();

	/**
	 * @param idFatura
	 *            the idFatura to set
	 */
	void setIdFatura(long idFatura);

	/**
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 *            the fatura to set
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return lista de FaturaItemImpressao
	 */
	Collection<FaturaItemImpressao> getListaFaturaItemImpressao();

	/**
	 * @param listaFaturaItemImpressao
	 *            nova lista de
	 *            FaturaItemImpressao
	 */
	void setListaFaturaItemImpressao(Collection<FaturaItemImpressao> listaFaturaItemImpressao);

	/**
	 * @param encerramento
	 */
	void setEncerramento(Boolean encerramento);

	/**
	 * @return
	 */
	Boolean getEncerramento();

	/**
	 * @return Boolean - Contrato Aditado Alterado
	 */
	boolean isContratoAditadoAlterado();

	/**
	 * @param isContratoAditadoAlterado - Set Contrato Aditado e Alterado
	 */
	void setContratoAditadoAlterado(boolean isContratoAditadoAlterado);

	/**
	 * @return
	 */
	EntidadeConteudo getStatusNfe();

	/**
	 * @param statusNfe - Set Status nfe.
	 */
	void setStatusNfe(EntidadeConteudo statusNfe);

	/**
	 * @return
	 */
	String getNumeroSerieDocFiscal();

	/**
	 * @param numeroSerieDocFiscal
	 */
	void setNumeroSerieDocFiscal(String numeroSerieDocFiscal);

	/**
	 * Checks if is fatura avulso.
	 * 
	 * @return the boolean
	 */
	Boolean isFaturaAvulso();

	/**
	 * @param isFaturaAvulso - Set Fatura Avulso
	 */
	void setFaturaAvulso(Boolean isFaturaAvulso);

	/**
	 * @param idTransportadora - Set id Transportadora
	 */
	void setIdTransportadora(Long idTransportadora);

	/**
	 * @return Long - Id Transportadora
	 */
	Long getIdTransportadora();

	/**
	 * @return Long - Id Veiculo
	 */
	Long getIdVeiculo();

	/**
	 * @param idVeiculo - Set Id Veiculo
	 */
	void setIdVeiculo(Long idVeiculo);

	/**
	 * @return Long - Id Motorista
	 */
	Long getIdMotorista();

	/**
	 * @param idMotorista - Seta id Motorista
	 */
	void setIdMotorista(Long idMotorista);

	/**
	 * @return Boolean - Indicador Contrato Complementar
	 */
	Boolean getIndicadorContratoComplementar();

	/**
	 * @param indicadorContratoComplementar - Indicador Contrato Complementar
	 */
	void setIndicadorContratoComplementar(Boolean indicadorContratoComplementar);

	/**
	 * @return the tipoDataRetroativaInvalida
	 */
	TipoDataRetroativaInvalida getTipoDataRetroativaInvalida();

	/**
	 * @param tipoDataRetroativaInvalida
	 *            the tipoDataRetroativaInvalida to set
	 */
	void setTipoDataRetroativaInvalida(TipoDataRetroativaInvalida tipoDataRetroativaInvalida);

	/**
	 * Recupera indicadorDataInformada.
	 * 
	 * @return indicadorDataInformada
	 */
	Boolean getIndicadorDataInformadaAplicada();

	/**
	 * Atribui valor para indicadorDataInformada
	 * 
	 * @param indicadorDataInformada
	 */
	void setIndicadorDataInformadaAplicada(Boolean indicadorDataInformada);

	/**
	 * Soma o valor do consumo apurdo de todos os historicos de consumo
	 * 
	 * @return totalConsumoApurado
	 */
	BigDecimal getTotalConsumoApurado();

	PontoConsumo getPontoConsumo();

	BigDecimal getValorTotal();
}
