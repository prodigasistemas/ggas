
package br.com.ggas.faturamento.fatura.impl;

import java.util.Map;
/**
 * Enum responsável pela forma de Ordenação da Data de Emissão de Fatura. 
 *
 */
public enum OrdenacaoDataEmissaoFatura {
	ASCENDENTE, DESCENDENTE;

	public static final String KEY_ORDENACAO_DATA_EMISSAO_FATURA = "OrdenacaoDataEmissaoFatura";

	/**
	 * Sets the ordenacao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacaoDataEmissaoFatura
	 *            the ordenacao data emissao fatura
	 * @return the ordenacao data emissao fatura
	 */
	public static OrdenacaoDataEmissaoFatura setOrdenacao(Map<String, Object> filtro, OrdenacaoDataEmissaoFatura ordenacaoDataEmissaoFatura) {

		return (OrdenacaoDataEmissaoFatura) filtro.put(KEY_ORDENACAO_DATA_EMISSAO_FATURA, ordenacaoDataEmissaoFatura);
	}

	/**
	 * Gets the ordenacao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the ordenacao
	 */
	public static OrdenacaoDataEmissaoFatura getOrdenacao(Map<String, Object> filtro) {

		return (OrdenacaoDataEmissaoFatura) filtro.get(KEY_ORDENACAO_DATA_EMISSAO_FATURA);
	}

}
