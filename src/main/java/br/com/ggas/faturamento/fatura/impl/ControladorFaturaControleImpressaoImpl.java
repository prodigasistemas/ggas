/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorFaturaImpl representa uma ControladorFaturaImpl no sistema.
 *
 * @since 05/04/2010
 * 
 */

package br.com.ggas.faturamento.fatura.impl;

import java.util.Collection;
import java.util.Map;

import org.hibernate.Query;

import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.FaturamentoGrupoRotaImpressao;
import br.com.ggas.faturamento.fatura.ControladorFaturaControleImpressao;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pela implementação dos métodos relacionados ao Controlador do Controle de Impressão de Fatura. 
 *
 */
public class ControladorFaturaControleImpressaoImpl extends ControladorNegocioImpl implements ControladorFaturaControleImpressao {

	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";

	private static final String CICLO = "ciclo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						FaturamentoGrupoRotaImpressao.BEAN_ID_FATURAMENTO_GRUPO_ROTA_IMPRESSAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(FaturamentoGrupoRotaImpressao.BEAN_ID_FATURAMENTO_GRUPO_ROTA_IMPRESSAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.ControladorFaturaControleImpressao#consultarFaturaControleImpressao(java.util.HashMap)
	 */
	@Override
	public Collection<FaturamentoGrupoRotaImpressao> consultarFaturaControleImpressao(Map<String, Object> filtro) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(" FaturamentoGrupoRotaImpressaoImpl fgri ");
		hql.append(" inner join fetch fgri.grupoFaturamento as grupoFaturamento ");
		hql.append(" inner join fetch fgri.rota as rota ");
		hql.append(" where 0=0 ");

		if(filtro.containsKey(GRUPO_FATURAMENTO)) {
			hql.append(" and grupoFaturamento.chavePrimaria = :grupoFaturamento ");
		}

		if(filtro.containsKey(ROTA)) {
			hql.append(" and rota.chavePrimaria = :rota ");
		}

		if(filtro.containsKey(ANO_MES_REFERENCIA)) {
			hql.append(" and fgri.anoMesReferencia = :anoMesReferencia ");
		}
		if(filtro.containsKey(CICLO)) {
			hql.append(" and fgri.ciclo = :ciclo ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(filtro.containsKey(GRUPO_FATURAMENTO)) {
			query.setParameter("grupoFaturamento", filtro.get(GRUPO_FATURAMENTO));
		}

		if(filtro.containsKey(ROTA)) {
			query.setParameter("rota", filtro.get(ROTA));
		}

		if(filtro.containsKey(ANO_MES_REFERENCIA)) {
			query.setParameter("anoMesReferencia", filtro.get(ANO_MES_REFERENCIA));
		}

		if(filtro.containsKey(CICLO)) {
			query.setParameter("ciclo", filtro.get(CICLO));
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.ControladorFaturaControleImpressao#obterOperacaoEmitirFatura()
	 */
	@Override
	public Operacao obterOperacaoEmitirFatura() {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(" OperacaoImpl ");
		hql.append(" where upper(descricao) = :descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("descricao", Constantes.EMITIR_FATURAS );

		return (Operacao) query.uniqueResult();
	}

}
