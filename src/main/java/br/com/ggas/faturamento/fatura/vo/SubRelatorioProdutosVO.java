/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;

/**
 * Classe responsável pela representação da entidade SubRelatorioProdutos
 *
 */
public class SubRelatorioProdutosVO implements Serializable {

	private static final long serialVersionUID = 1368239045186791222L;

	private String descricao;

	private String volumeFaturado;

	private String valorUnitario;

	private String valorTotal;

	private String unidade;

	private String cfop;

	private String dataInicio;

	private String dataFinal;

	private String quantidadeDias;

	private boolean indicadorCredito;

	private String descricaoTarifa;

	private String codigoProduto;
	
	private String desconto;

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public String getVolumeFaturado() {

		return volumeFaturado;
	}

	public void setVolumeFaturado(String volumeFaturado) {

		this.volumeFaturado = volumeFaturado;
	}

	public String getValorUnitario() {

		return valorUnitario;
	}

	public void setValorUnitario(String valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	public String getValorTotal() {

		return valorTotal;
	}

	public void setValorTotal(String valorTotal) {

		this.valorTotal = valorTotal;
	}

	public String getCfop() {

		return cfop;
	}

	public void setCfop(String cfop) {

		this.cfop = cfop;
	}

	public String getUnidade() {

		return unidade;
	}

	public void setUnidade(String unidade) {

		this.unidade = unidade;
	}

	public boolean isIndicadorCredito() {

		return indicadorCredito;
	}

	public void setIndicadorCredito(boolean indicadorCredito) {

		this.indicadorCredito = indicadorCredito;
	}

	public String getDataInicio() {

		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {

		this.dataInicio = dataInicio;
	}

	public String getDataFinal() {

		return dataFinal;
	}

	public void setDataFinal(String dataFinal) {

		this.dataFinal = dataFinal;
	}

	public String getQuantidadeDias() {

		return quantidadeDias;
	}

	public void setQuantidadeDias(String quantidadeDias) {

		this.quantidadeDias = quantidadeDias;
	}

	public String getDescricaoTarifa() {

		return descricaoTarifa;
	}

	public void setDescricaoTarifa(String descricaoTarifa) {

		this.descricaoTarifa = descricaoTarifa;
	}

	public String getCodigoProduto() {

		return codigoProduto;
	}

	public void setCodigoProduto(String codigoProduto) {

		this.codigoProduto = codigoProduto;
	}

	public String getDesconto() {

		return desconto;
	}

	public void setDesconto(String desconto) {

		this.desconto = desconto;
	}

}
