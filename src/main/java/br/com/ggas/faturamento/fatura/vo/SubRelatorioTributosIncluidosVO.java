/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;

/**
 * Classe responsável pela representação da entidade SubRelatorioTributosIncluidos 
 *
 */
public class SubRelatorioTributosIncluidosVO implements Serializable {

	private static final long serialVersionUID = -6977971285689473512L;

	private String valorBaseCalculoICMS;

	private String valorAliquotaICMS;

	private String valorICMS;

	private String valorBaseCalculoPIS;

	private String valorAliquotaPIS;

	private String valorPIS;

	private String baseCalculoFiscoICMS;

	private String valorRetidoFiscoICMS;

	private String informacoesComplementares;

	private String valorBaseCalculoIPI;

	private String valorAliquotaIPI;

	private String valorIPI;

	public String getValorBaseCalculoICMS() {

		return valorBaseCalculoICMS;
	}

	public void setValorBaseCalculoICMS(String valorBaseCalculoICMS) {

		this.valorBaseCalculoICMS = valorBaseCalculoICMS;
	}

	public String getValorAliquotaICMS() {

		return valorAliquotaICMS;
	}

	public void setValorAliquotaICMS(String valorAliquotaICMS) {

		this.valorAliquotaICMS = valorAliquotaICMS;
	}

	public String getValorICMS() {

		return valorICMS;
	}

	public void setValorICMS(String valorICMS) {

		this.valorICMS = valorICMS;
	}

	public String getValorBaseCalculoPIS() {

		return valorBaseCalculoPIS;
	}

	public void setValorBaseCalculoPIS(String valorBaseCalculoPIS) {

		this.valorBaseCalculoPIS = valorBaseCalculoPIS;
	}

	public String getValorAliquotaPIS() {

		return valorAliquotaPIS;
	}

	public void setValorAliquotaPIS(String valorAliquotaPIS) {

		this.valorAliquotaPIS = valorAliquotaPIS;
	}

	public String getValorPIS() {

		return valorPIS;
	}

	public void setValorPIS(String valorPIS) {

		this.valorPIS = valorPIS;
	}

	public String getBaseCalculoFiscoICMS() {

		return baseCalculoFiscoICMS;
	}

	public void setBaseCalculoFiscoICMS(String baseCalculoFiscoICMS) {

		this.baseCalculoFiscoICMS = baseCalculoFiscoICMS;
	}

	public String getValorRetidoFiscoICMS() {

		return valorRetidoFiscoICMS;
	}

	public void setValorRetidoFiscoICMS(String valorRetidoFiscoICMS) {

		this.valorRetidoFiscoICMS = valorRetidoFiscoICMS;
	}

	public String getInformacoesComplementares() {

		return informacoesComplementares;
	}

	public void setInformacoesComplementares(String informacoesComplementares) {

		this.informacoesComplementares = informacoesComplementares;
	}

	public String getValorBaseCalculoIPI() {

		return valorBaseCalculoIPI;
	}

	public void setValorBaseCalculoIPI(String valorBaseCalculoIPI) {

		this.valorBaseCalculoIPI = valorBaseCalculoIPI;
	}

	public String getValorAliquotaIPI() {

		return valorAliquotaIPI;
	}

	public void setValorAliquotaIPI(String valorAliquotaIPI) {

		this.valorAliquotaIPI = valorAliquotaIPI;
	}

	public String getValorIPI() {

		return valorIPI;
	}

	public void setValorIPI(String valorIPI) {

		this.valorIPI = valorIPI;
	}

}
