/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a série
 *
 */
public interface Serie extends EntidadeNegocio {

	String BEAN_ID_SERIE = "serie";

	String SERIE_CAMPO_STRING = "Série";

	/**
	 * @return the numero
	 */
	String getNumero();

	/**
	 * @param numero
	 *            the numero to set
	 */
	void setNumero(String numero);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the sequenciaNumeracao
	 */
	Long getSequenciaNumeracao();

	/**
	 * @param sequenciaNumeracao
	 *            the sequenciaNumeracao to set
	 */
	void setSequenciaNumeracao(Long sequenciaNumeracao);

	/**
	 * @return the anoMes
	 */
	Integer getAnoMes();

	/**
	 * @param anoMes
	 *            the anoMes to set
	 */
	void setAnoMes(Integer anoMes);

	/**
	 * @return the tipoOperacao
	 */
	EntidadeConteudo getTipoOperacao();

	/**
	 * @param tipoOperacao
	 *            the tipoOperacao to set
	 */
	void setTipoOperacao(EntidadeConteudo tipoOperacao);

	/**
	 * @return the tipoFaturamento
	 */
	EntidadeConteudo getTipoFaturamento();

	/**
	 * @param tipoFaturamento
	 *            the tipoFaturamento to set
	 */
	void setTipoFaturamento(EntidadeConteudo tipoFaturamento);

	/**
	 * @return Retorna segmento
	 */
	Segmento getSegmento();

	/**
	 * @param segmento  - Set segmento.
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return Retorna AnoMesFormatado
	 */
	String getAnoMesFormatado();

	/**
	 * @return Retorna IndicadorControleMensalFormatado
	 */
	String getIndicadorControleMensalFormatado();

	/**
	 * @return Retorna IndicadorSerieEletronicaFormatado
	 */
	String getIndicadorSerieEletronicaFormatado();

	/**
	 * @return Retorna IndicadorContingenciaScanFormatado
	 */
	String getIndicadorContingenciaScanFormatado();

	/**
	 * @return Retorna HabilitadoFormatado
	 */
	String getHabilitadoFormatado();

	/**
	 * @return Boolean -Retorna isIndicadorControleMensal
	 */
	boolean isIndicadorControleMensal();

	/**
	 * @param indicadorControleMensal - Set indicador Controlador mensal.
	 */
	void setIndicadorControleMensal(boolean indicadorControleMensal);

	/**
	 * @return Boolean - Retorna isIndicadorSerieEletronica.
	 */
	boolean isIndicadorSerieEletronica();

	/**
	 * @param indicadorSerieEletronica - Set indicador serie eletrônica.
	 */
	void setIndicadorSerieEletronica(boolean indicadorSerieEletronica);

	/**
	 * @return Boolean -  Retorna isIndicadorContingenciaScan.
	 */
	boolean isIndicadorContingenciaScan();

	/**
	 * @param indicadorContingenciaScan - Set indicador contigência Scan.
	 */
	void setIndicadorContingenciaScan(boolean indicadorContingenciaScan);
}
