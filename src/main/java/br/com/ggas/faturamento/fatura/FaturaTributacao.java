/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.math.BigDecimal;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a tributação de fatura
 *
 */
public interface FaturaTributacao extends EntidadeNegocio {

	String BEAN_ID_FATURA_TRIBUTACAO = "faturaTributacao";

	/**
	 * @return BigDecimal - Retorna o percentual de aliquota.
	 */
	BigDecimal getPercentualAliquota();

	/**
	 * @param percentualAliquota - Set percentual de aliquota.
	 */
	void setPercentualAliquota(BigDecimal percentualAliquota);

	/**
	 * @return BigDecimal - Retorna valor base do Cálculo.
	 */
	BigDecimal getValorBaseCalculo();

	/**
	 * @param valorBaseCalculo - Set valor basé do cálculo.
	 */
	void setValorBaseCalculo(BigDecimal valorBaseCalculo);

	/**
	 * @return BigDecimal - Retorna valor do imposto.
	 */
	BigDecimal getValorImposto();

	/**
	 * @param valorImposto - Set valor do imposto.
	 */
	void setValorImposto(BigDecimal valorImposto);

	/**
	 * @return BigDecimal - Retorna o valor base da substituição.
	 */
	BigDecimal getValorBaseSubstituicao();

	/**
	 * @param valorBaseSubstituicao - Set valor base da substituição.
	 */
	void setValorBaseSubstituicao(BigDecimal valorBaseSubstituicao);

	/**
	 * @return BigDecimal - Retorna o valor da subistituição.
	 */
	BigDecimal getValorSubstituicao();

	/**
	 * @param valorSubstituicao - set valor da substituição.
	 */
	void setValorSubstituicao(BigDecimal valorSubstituicao);

	/**
	 * @return Tributi - Retorna objeto tributo.
	 */
	Tributo getTributo();

	/**
	 * @param tributo - Set objeto tributo.
	 */
	void setTributo(Tributo tributo);

	/**
	 * @return Fatura - Retorna objeto fatura.
	 */
	Fatura getFatura();

	/**
	 * @param fatura - Set objeto fatura.
	 */
	void setFatura(Fatura fatura);

}
