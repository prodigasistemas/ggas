
package br.com.ggas.faturamento.fatura.impl;
/**
 * Enum responsável pela verificação do
 *  Tipo de Data Retroativa Invalida.  
 *
 */
public enum TipoDataRetroativaInvalida {

	DATA_ATUAL, PROXIMA_DATA_VALIDA;

	/**
	 * To tipo data retroativa invalida.
	 * 
	 * @param valor
	 *            the valor
	 * @return the tipo data retroativa invalida
	 */
	public static TipoDataRetroativaInvalida toTipoDataRetroativaInvalida(Integer valor) {

		for (TipoDataRetroativaInvalida tipoDataRetroativaInvalida : values()) {
			if(valor == tipoDataRetroativaInvalida.ordinal()) {
				return tipoDataRetroativaInvalida;
			}
		}
		return null;
	}

	public boolean isDataAtual() {

		return this.ordinal() == DATA_ATUAL.ordinal();
	}

	public boolean isProximaDataValida() {

		return this.ordinal() == PROXIMA_DATA_VALIDA.ordinal();
	}

}
