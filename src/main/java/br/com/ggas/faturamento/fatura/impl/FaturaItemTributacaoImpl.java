/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;

import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.fatura.FaturaItemTributacao;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class FaturaItemTributacaoImpl extends EntidadeNegocioImpl implements FaturaItemTributacao {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7292258685875292280L;
	
	private static final Logger LOG = Logger.getLogger(FaturaItemTributacaoImpl.class);

	private BigDecimal percentualAliquota;

	private BigDecimal valorBaseCalculo;

	private BigDecimal valorImposto;

	private BigDecimal valorBaseSubstituicao;

	private BigDecimal valorSubstituicao;

	private Tributo tributo;

	private FaturaItem faturaItem;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao#getPercentualAliquota()
	 */
	@Override
	public BigDecimal getPercentualAliquota() {

		return percentualAliquota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao
	 * #setPercentualAliquota(java.
	 * math.BigDecimal)
	 */
	@Override
	public void setPercentualAliquota(BigDecimal percentualAliquota) {

		this.percentualAliquota = percentualAliquota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao#getValorBaseCalculo()
	 */
	@Override
	public BigDecimal getValorBaseCalculo() {

		return valorBaseCalculo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao
	 * #setValorBaseCalculo(java.math.BigDecimal)
	 */
	@Override
	public void setValorBaseCalculo(BigDecimal valorBaseCalculo) {

		this.valorBaseCalculo = valorBaseCalculo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao#getValorImposto()
	 */
	@Override
	public BigDecimal getValorImposto() {

		return valorImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao
	 * #setValorImposto(java.math.BigDecimal)
	 */
	@Override
	public void setValorImposto(BigDecimal valorImposto) {

		this.valorImposto = valorImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao#getValorBaseSubstituicao()
	 */
	@Override
	public BigDecimal getValorBaseSubstituicao() {

		return valorBaseSubstituicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao
	 * #setValorBaseSubstituicao(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setValorBaseSubstituicao(BigDecimal valorBaseSubstituicao) {

		this.valorBaseSubstituicao = valorBaseSubstituicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao#getValorSubstituicao()
	 */
	@Override
	public BigDecimal getValorSubstituicao() {

		return valorSubstituicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao
	 * #setValorSubstituicao(java.math.BigDecimal)
	 */
	@Override
	public void setValorSubstituicao(BigDecimal valorSubstituicao) {

		this.valorSubstituicao = valorSubstituicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao#getTributo()
	 */
	@Override
	public Tributo getTributo() {

		return tributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * FaturaTributacao
	 * #setTributo(br.com.ggas.faturamento
	 * .tributo.Tributo)
	 */
	@Override
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public FaturaItem getFaturaItem() {

		return faturaItem;
	}

	@Override
	public void setFaturaItem(FaturaItem faturaItem) {

		this.faturaItem = faturaItem;
	}

	@Override
	public FaturaItemTributacao clone() {
		try {
			return (FaturaItemTributacao) super.clone();
		} catch (CloneNotSupportedException e) {
			LOG.error(e);
			return this;
		}
	}
}
