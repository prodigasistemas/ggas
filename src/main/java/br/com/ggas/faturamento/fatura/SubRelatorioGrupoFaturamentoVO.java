/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pela representação do SubRelatorioGrupoFaturamento
 *
 */
public class SubRelatorioGrupoFaturamentoVO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2012818218923680730L;

	private BigDecimal valorResidencial;

	private BigDecimal valorComercial;

	private BigDecimal valorIndustrial;

	private BigDecimal valorOutros;

	private String descricaoRubrica;

	/**
	 * Instantiates a new sub relatorio grupo faturamento vo.
	 */
	public SubRelatorioGrupoFaturamentoVO() {

		this.valorResidencial = BigDecimal.ZERO;
		this.valorComercial = BigDecimal.ZERO;
		this.valorIndustrial = BigDecimal.ZERO;
		this.valorOutros = BigDecimal.ZERO;
		this.descricaoRubrica = "";
	}

	/**
	 * @return the valorResidencial
	 */
	public BigDecimal getValorResidencial() {

		return valorResidencial;
	}

	/**
	 * @param valorResidencial the valorResidencial to set
	 */
	public void setValorResidencial(BigDecimal valorResidencial) {

		this.valorResidencial = valorResidencial;
	}

	public String getValorResidencialFormatado() {

		return Util.converterCampoCurrencyParaString(valorResidencial, Constantes.LOCALE_PADRAO);
	}

	/**
	 * @return the valorComercial
	 */
	public BigDecimal getValorComercial() {

		return valorComercial;
	}

	/**
	 * @param valorComercial the valorComercial to set
	 */
	public void setValorComercial(BigDecimal valorComercial) {

		this.valorComercial = valorComercial;
	}

	public String getValorComercialFormatado() {

		return Util.converterCampoCurrencyParaString(valorComercial, Constantes.LOCALE_PADRAO);
	}

	/**
	 * @return the valorIndustrial
	 */
	public BigDecimal getValorIndustrial() {

		return valorIndustrial;
	}

	/**
	 * @param valorIndustrial the valorIndustrial to set
	 */
	public void setValorIndustrial(BigDecimal valorIndustrial) {

		this.valorIndustrial = valorIndustrial;
	}

	public String getValorIndustrialFormatado() {

		return Util.converterCampoCurrencyParaString(valorIndustrial, Constantes.LOCALE_PADRAO);
	}

	/**
	 * @return the valorOutros
	 */
	public BigDecimal getValorOutros() {

		return valorOutros;
	}

	/**
	 * @param valorOutros the valorOutros to set
	 */
	public void setValorOutros(BigDecimal valorOutros) {

		this.valorOutros = valorOutros;
	}

	public String getValorOutrosFormatado() {

		return Util.converterCampoCurrencyParaString(valorOutros, Constantes.LOCALE_PADRAO);
	}

	/**
	 * @return the descricaoRubrica
	 */
	public String getDescricaoRubrica() {

		return descricaoRubrica;
	}

	/**
	 * @param descricaoRubrica the descricaoRubrica to set
	 */
	public void setDescricaoRubrica(String descricaoRubrica) {

		this.descricaoRubrica = descricaoRubrica;
	}
}
