/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Classe responsável pela representação da entidade RelatorioFaturamentoGrupo
 *
 */

public class RelatorioFaturamentoGrupoVO implements Serializable {

	private static final long serialVersionUID = -4593979344281413040L;

	private List<SubRelatorioPontoConsumoVO> colecaoAnomalia;

	private List<SubRelatorioFaturasVencidasVO> colecaoRubrica;

	private List<SubRelatorioFornecimentoVO> colecaoTipoCobranca;

	public List<SubRelatorioPontoConsumoVO> getColecaoAnomalia() {

		return colecaoAnomalia;
	}

	public void setColecaoAnomalia(List<SubRelatorioPontoConsumoVO> colecaoAnomalia) {

		this.colecaoAnomalia = colecaoAnomalia;
	}

	public List<SubRelatorioFaturasVencidasVO> getColecaoRubrica() {

		return colecaoRubrica;
	}

	public void setColecaoRubrica(List<SubRelatorioFaturasVencidasVO> colecaoRubrica) {

		this.colecaoRubrica = colecaoRubrica;
	}

	public List<SubRelatorioFornecimentoVO> getColecaoTipoCobranca() {

		return colecaoTipoCobranca;
	}

	public void setColecaoTipoCobranca(List<SubRelatorioFornecimentoVO> colecaoTipoCobranca) {

		this.colecaoTipoCobranca = colecaoTipoCobranca;
	}

}
