/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos atributos e implementação dos métodos relacionados a Série.
 *
 */
public class SerieImpl extends EntidadeNegocioImpl implements Serie {

	private static final int TAMANHO_STRING_MES_ANO = 6;

	private static final int INDICE_INICIO_MES = 4;

	private static final int INDICE_FINAL_MES = 6;

	private static final int INDICE_FINAL_ANO = 4;

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -9193643424807595569L;

	private String numero;

	private String descricao;

	private Long sequenciaNumeracao;

	private Integer anoMes;

	private boolean indicadorControleMensal;

	private boolean indicadorSerieEletronica;

	private EntidadeConteudo tipoOperacao;

	private EntidadeConteudo tipoFaturamento;

	private Segmento segmento;

	private boolean indicadorContingenciaScan;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.Serie#getNumero
	 * ()
	 */
	@Override
	public String getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.Serie#setNumero
	 * (java.lang.String)
	 */
	@Override
	public void setNumero(String numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #getSequenciaNumeracao()
	 */
	@Override
	public Long getSequenciaNumeracao() {

		return sequenciaNumeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #setSequenciaNumeracao(java.lang.Long)
	 */
	@Override
	public void setSequenciaNumeracao(Long sequenciaNumeracao) {

		this.sequenciaNumeracao = sequenciaNumeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #getAnoMes()
	 */
	@Override
	public Integer getAnoMes() {

		return anoMes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #setAnoMes(java.lang.Integer)
	 */
	@Override
	public void setAnoMes(Integer anoMes) {

		this.anoMes = anoMes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #getTipoOperacao()
	 */
	@Override
	public EntidadeConteudo getTipoOperacao() {

		return tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #setTipoOperacao
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoOperacao(EntidadeConteudo tipoOperacao) {

		this.tipoOperacao = tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #getTipoFaturamento()
	 */
	@Override
	public EntidadeConteudo getTipoFaturamento() {

		return tipoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.impl.Serie
	 * #setTipoFaturamento
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoFaturamento(EntidadeConteudo tipoFaturamento) {

		this.tipoFaturamento = tipoFaturamento;
	}

	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	@Override
	public String getAnoMesFormatado() {

		String anoMesTemp = null;
		if(anoMes != null) {
			anoMesTemp = anoMes.toString();
			if(anoMesTemp.length() == TAMANHO_STRING_MES_ANO) {
				anoMesTemp = anoMesTemp.substring(INDICE_INICIO_MES, INDICE_FINAL_MES) + "/" + anoMesTemp.substring(0, INDICE_FINAL_ANO);
			} else {
				return "";
			}
		}
		return anoMesTemp;

	}

	@Override
	public String getIndicadorControleMensalFormatado() {

		if(isIndicadorControleMensal()) {
			return "Sim";
		}
		return "Não";
	}

	@Override
	public String getIndicadorSerieEletronicaFormatado() {

		if(isIndicadorSerieEletronica()) {
			return "Sim";
		}
		return "Não";
	}

	@Override
	public String getIndicadorContingenciaScanFormatado() {

		if(isIndicadorContingenciaScan()) {
			return "Sim";
		}
		return "Não";
	}

	@Override
	public String getHabilitadoFormatado() {

		if(isHabilitado()) {
			return "Sim";
		}
		return "Não";
	}

	@Override
	public boolean isIndicadorControleMensal() {

		return indicadorControleMensal;
	}

	@Override
	public void setIndicadorControleMensal(boolean indicadorControleMensal) {

		this.indicadorControleMensal = indicadorControleMensal;
	}

	@Override
	public boolean isIndicadorSerieEletronica() {

		return indicadorSerieEletronica;
	}

	@Override
	public void setIndicadorSerieEletronica(boolean indicadorSerieEletronica) {

		this.indicadorSerieEletronica = indicadorSerieEletronica;
	}

	@Override
	public boolean isIndicadorContingenciaScan() {

		return indicadorContingenciaScan;
	}

	@Override
	public void setIndicadorContingenciaScan(boolean indicadorContingenciaScan) {

		this.indicadorContingenciaScan = indicadorContingenciaScan;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(numero)) {
			stringBuilder.append("Número");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(sequenciaNumeracao == null) {
			stringBuilder.append("Sequencial");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoOperacao == null) {
			stringBuilder.append("Operação");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoFaturamento == null) {
			stringBuilder.append("Tipo faturamento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (indicadorControleMensal && anoMes == null) {
			stringBuilder.append("Mês/Ano");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
