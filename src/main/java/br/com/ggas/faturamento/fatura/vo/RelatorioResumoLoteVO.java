/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;

/**
 * Classe responsável pela representação da entidade RelatorioResumoLote
 *
 */
public class RelatorioResumoLoteVO implements Serializable {

	private static final long serialVersionUID = 1368239045186791222L;

	private String chaveFatura;

	private String valorFatura;

	private String sequencial;

	private String numeroLote;

	private String formaCobranca;

	/**
	 * @return the chaveFatura
	 */
	public String getChaveFatura() {

		return chaveFatura;
	}

	/**
	 * @param chaveFatura
	 *            the chaveFatura to set
	 */
	public void setChaveFatura(String chaveFatura) {

		this.chaveFatura = chaveFatura;
	}

	/**
	 * @return the valorFatura
	 */
	public String getValorFatura() {

		return valorFatura;
	}

	/**
	 * @param valorFatura
	 *            the valorFatura to set
	 */
	public void setValorFatura(String valorFatura) {

		this.valorFatura = valorFatura;
	}

	/**
	 * @return the sequencial
	 */
	public String getSequencial() {

		return sequencial;
	}

	/**
	 * @param sequencial
	 *            the sequencial to set
	 */
	public void setSequencial(String sequencial) {

		this.sequencial = sequencial;
	}

	/**
	 * @return the numeroLote
	 */
	public String getNumeroLote() {

		return numeroLote;
	}

	/**
	 * @param numeroLote
	 *            the numeroLote to set
	 */
	public void setNumeroLote(String numeroLote) {

		this.numeroLote = numeroLote;
	}

	/**
	 * @return the formaCobranca
	 */
	public String getFormaCobranca() {

		return formaCobranca;
	}

	/**
	 * @param formaCobranca
	 *            the formaCobranca to set
	 */
	public void setFormaCobranca(String formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

}
