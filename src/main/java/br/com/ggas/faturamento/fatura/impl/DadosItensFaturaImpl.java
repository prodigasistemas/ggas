/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.fatura.DadosItensFatura;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;

/**
 * @author pburlamaqui
 */
class DadosItensFaturaImpl implements DadosItensFatura {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6502691339112182476L;
	
	private static final Logger LOG = Logger.getLogger(DadosItensFaturaImpl.class);

	private BigDecimal valor;

	private CreditoDebitoARealizar creditoDebitoARealizar;

	private EntidadeConteudo itemFatura;

	private Rubrica rubrica;

	private FaturaItem faturaItem;

	private boolean indicadorCredito;

	private PontoConsumo pontoConsumo;

	private Boolean creditoAbatido;

	// Abaixo estão os atributos que serão
	// utilizados na exibição de
	// FaturaItemImpressao na tela de
	// ResumoInclusaoFatura
	private Tarifa tarifa;

	private Date dataInicial;

	private Date dataFinal;

	private Integer quantidadeDias;

	private BigDecimal consumo;

	private BigDecimal valorUnitario;

	private BigDecimal valorTotal;

	private BigDecimal valorTotalDisponivel;

	private String descricaoDesconto;

	private DadosFornecimentoGasVO dadosFornecimentoGasVO;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosItensFatura#getValor()
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosItensFatura
	 * #setValor(java.math.BigDecimal)
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosItensFatura
	 * #getCreditoDebitoARealizar()
	 */
	@Override
	public CreditoDebitoARealizar getCreditoDebitoARealizar() {

		return creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosItensFatura
	 * #setCreditoDebitoARealizar(br
	 * .com.ggas.faturamento
	 * .creditodebito.CreditoDebitoARealizar)
	 */
	@Override
	public void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) {

		this.creditoDebitoARealizar = creditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosItensFatura#getItemFatura()
	 */
	@Override
	public EntidadeConteudo getItemFatura() {

		return itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosItensFatura
	 * #setItemFatura(br.com.ggas.geral
	 * .EntidadeConteudo)
	 */
	@Override
	public void setItemFatura(EntidadeConteudo itemFatura) {

		this.itemFatura = itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getRubrica()
	 */
	@Override
	public Rubrica getRubrica() {

		return rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #
	 * setRubrica(br.com.ggas.faturamento.rubrica.
	 * Rubrica)
	 */
	@Override
	public void setRubrica(Rubrica rubrica) {

		this.rubrica = rubrica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getFaturaItem()
	 */
	@Override
	public FaturaItem getFaturaItem() {

		return faturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setFaturaItem(br.com.ggas.faturamento.
	 * FaturaItem)
	 */
	@Override
	public void setFaturaItem(FaturaItem faturaItem) {

		this.faturaItem = faturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #isIndicadorCredito()
	 */
	@Override
	public boolean isIndicadorCredito() {

		return indicadorCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setIndicadorCredito(boolean)
	 */
	@Override
	public void setIndicadorCredito(boolean indicadorCredito) {

		this.indicadorCredito = indicadorCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #
	 * setPontoConsumo(br.com.ggas.cadastro.imovel
	 * .PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #isCreditoAbatido()
	 */
	@Override
	public Boolean isCreditoAbatido() {

		return creditoAbatido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setCreditoAbatido(java.lang.Boolean)
	 */
	@Override
	public void setCreditoAbatido(Boolean creditoAbatido) {

		this.creditoAbatido = creditoAbatido;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getTarifa()
	 */
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #
	 * setTarifa(br.com.ggas.faturamento.tarifa.Tarifa
	 * )
	 */
	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getDataInicial()
	 */
	@Override
	public Date getDataInicial() {

		return dataInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setDataInicial(java.util.Date)
	 */
	@Override
	public void setDataInicial(Date dataInicial) {

		this.dataInicial = dataInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getDataFinal()
	 */
	@Override
	public Date getDataFinal() {

		return dataFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setDataFinal(java.util.Date)
	 */
	@Override
	public void setDataFinal(Date dataFinal) {

		this.dataFinal = dataFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getQuantidadeDias()
	 */
	@Override
	public Integer getQuantidadeDias() {

		return quantidadeDias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setQuantidadeDias(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeDias(Integer quantidadeDias) {

		this.quantidadeDias = quantidadeDias;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getConsumo()
	 */
	@Override
	public BigDecimal getConsumo() {

		return consumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setConsumo(java.math.BigDecimal)
	 */
	@Override
	public void setConsumo(BigDecimal consumo) {

		this.consumo = consumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getValorUnitario()
	 */
	@Override
	public BigDecimal getValorUnitario() {

		return valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setValorUnitario(java.math.BigDecimal)
	 */
	@Override
	public void setValorUnitario(BigDecimal valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getValorTotal()
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setValorTotal(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosItensFatura#getValorTotalDisponivel()
	 */
	@Override
	public BigDecimal getValorTotalDisponivel() {

		return valorTotalDisponivel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosItensFatura#setValorTotalDisponivel(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalDisponivel(BigDecimal valorTotalDisponivel) {

		this.valorTotalDisponivel = valorTotalDisponivel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #getDescricaoDesconto()
	 */
	@Override
	public String getDescricaoDesconto() {

		return descricaoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.fatura.DadosItensFatura
	 * #setDescricaoDesconto(java.lang.String)
	 */
	@Override
	public void setDescricaoDesconto(String descricaoDesconto) {

		this.descricaoDesconto = descricaoDesconto;
	}

	@Override
	public DadosFornecimentoGasVO getDadosFornecimentoGasVO() {

		return dadosFornecimentoGasVO;
	}

	@Override
	public void setDadosFornecimentoGasVO(DadosFornecimentoGasVO dadosFornecimentoGasVO) {

		this.dadosFornecimentoGasVO = dadosFornecimentoGasVO;
	}

	@Override
	public DadosItensFatura clone() {
		try {
			return (DadosItensFatura) super.clone();
		} catch (CloneNotSupportedException e) {
			LOG.error(e);
			return this;
		}
	}

}
