/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.fatura.DadosGeraisItensFatura;
import br.com.ggas.faturamento.fatura.DadosLeituraConsumoFatura;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;

class DadosResumoFaturaImpl implements DadosResumoFatura {

	private static final long serialVersionUID = -1494039100749771933L;

	private Integer ciclo;

	private Integer referencia;

	private Integer quantidadeCiclo;

	private Cliente cliente;

	private boolean cobrancaAbaixoMinimo;

	private boolean faturamentoAgrupado;

	private boolean medicaoDiaria;

	private long idMotivo;

	private Contrato contrato;

	private Date dataEmissao;

	private Date dataEmissaoInformada;

	private TipoDataRetroativaInvalida tipoDataRetroativaInvalida;

	private Boolean indicadorDataInformadaAplicada;

	private String motivo;

	private String numeroContrato;

	private DadosAuditoria dadosAuditoria;

	private Long[] chavesFaturasPendentes;

	private Boolean exibirAlerta;

	private EntidadeConteudo statusNfe;

	private String numeroSerieDocFiscal;

	private Collection<DadosGeraisItensFatura> listaDadosGeraisItens = new ArrayList<DadosGeraisItensFatura>();

	private List<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

	private List<HistoricoConsumo> listaHistoricoConsumo = new ArrayList<HistoricoConsumo>();

	private Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura = new ArrayList<DadosLeituraConsumoFatura>();

	private Collection<Fatura> listaFaturasPendentes = new ArrayList<Fatura>();

	private Collection<DadosTributoVO> listaDadosTributoVO = new ArrayList<DadosTributoVO>();

	private Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = new ArrayList<CreditoDebitoARealizar>();

	private Collection<FaturaItem> listaFaturaItem = new ArrayList<FaturaItem>();

	private Collection<CreditoDebitoDetalhamento> listaDebitosCreditoApuracao = new ArrayList<CreditoDebitoDetalhamento>();

	private Collection<FaturaItemImpressao> listaFaturaItemImpressao = new ArrayList<FaturaItemImpressao>();

	// Usado no refaturar
	private Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizarTotal = new ArrayList<CreditoDebitoARealizar>();

	private long idFatura;

	private Fatura fatura;

	private Boolean encerramento;

	private boolean isContratoAditadoAlterado;

	private Boolean faturaAvulso;

	private Long idTransportadora;

	private Long idVeiculo;

	private Long idMotorista;

	private Boolean indicadorContratoComplementar;

	/**
	 * @return the fatura
	 */
	@Override
	public Fatura getFatura() {

		return fatura;
	}

	/**
	 * @param fatura
	 *            the fatura to set
	 */
	@Override
	public void setFatura(Fatura fatura) {

		this.fatura = fatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getIdFatura()
	 */
	@Override
	public long getIdFatura() {

		return idFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#setIdFatura(long)
	 */
	@Override
	public void setIdFatura(long idFatura) {

		this.idFatura = idFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaCreditoDebitoARealizarTotal()
	 */
	@Override
	public Collection<CreditoDebitoARealizar> getListaCreditoDebitoARealizarTotal() {

		return listaCreditoDebitoARealizarTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaCreditoDebitoARealizarTotal
	 * (java.util.Collection)
	 */
	@Override
	public void setListaCreditoDebitoARealizarTotal(Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizarTotal) {

		this.listaCreditoDebitoARealizarTotal = listaCreditoDebitoARealizarTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getCiclo()
	 */
	@Override
	public Integer getCiclo() {

		return ciclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setCiclo(java.lang.Integer)
	 */
	@Override
	public void setCiclo(Integer ciclo) {

		this.ciclo = ciclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getReferencia()
	 */
	@Override
	public Integer getReferencia() {

		return referencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setReferencia(java.lang.Integer)
	 */
	@Override
	public void setReferencia(Integer referencia) {

		this.referencia = referencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getQuantidadeCiclo()
	 */
	@Override
	public Integer getQuantidadeCiclo() {

		return quantidadeCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setQuantidadeCiclo(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeCiclo(Integer quantidadeCiclo) {

		this.quantidadeCiclo = quantidadeCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setCliente(br.com.ggas.cadastro
	 * .cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#isCobrancaAbaixoMinimo()
	 */
	@Override
	public boolean isCobrancaAbaixoMinimo() {

		return cobrancaAbaixoMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setCobrancaAbaixoMinimo(boolean)
	 */
	@Override
	public void setCobrancaAbaixoMinimo(boolean cobrancaAbaixoMinimo) {

		this.cobrancaAbaixoMinimo = cobrancaAbaixoMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #isIndicadorFaturamentoAgrupado()
	 */
	@Override
	public boolean isFaturamentoAgrupado() {

		return faturamentoAgrupado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setIndicadorFaturamentoAgrupado(boolean)
	 */
	@Override
	public void setFaturamentoAgrupado(boolean faturamentoAgrupado) {

		this.faturamentoAgrupado = faturamentoAgrupado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getListaPontoConsumo()
	 */
	@Override
	public List<PontoConsumo> getListaPontoConsumo() {

		return listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaPontoConsumo(java.util.List)
	 */
	@Override
	public void setListaPontoConsumo(List<PontoConsumo> listaPontoConsumo) {

		this.listaPontoConsumo = listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaHistoricoConsumo()
	 */
	@Override
	public List<HistoricoConsumo> getListaHistoricoConsumo() {

		return listaHistoricoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaHistoricoConsumo(java.util.List)
	 */
	@Override
	public void setListaHistoricoConsumo(List<HistoricoConsumo> listaHistoricoConsumo) {

		this.listaHistoricoConsumo = listaHistoricoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaDadosLeituraConsumoFatura()
	 */
	@Override
	public Collection<DadosLeituraConsumoFatura> getListaDadosLeituraConsumoFatura() {

		return listaDadosLeituraConsumoFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaDadosLeituraConsumoFatura
	 * (java.util.Collection)
	 */
	@Override
	public void setListaDadosLeituraConsumoFatura(Collection<DadosLeituraConsumoFatura> listaDadosLeituraConsumoFatura) {

		this.listaDadosLeituraConsumoFatura = listaDadosLeituraConsumoFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getListaFatura()
	 */
	@Override
	public Collection<Fatura> getListaFaturasPendentes() {

		return listaFaturasPendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaFatura(java.util.Collection)
	 */
	@Override
	public void setListaFaturasPendentes(Collection<Fatura> listaFaturasPendentes) {

		this.listaFaturasPendentes = listaFaturasPendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getListaDadosTributoVO()
	 */
	@Override
	public Collection<DadosTributoVO> getListaDadosTributoVO() {

		return listaDadosTributoVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaDadosTributoVO(java
	 * .util.Collection)
	 */
	@Override
	public void setListaDadosTributoVO(Collection<DadosTributoVO> listaDadosTributoVO) {

		this.listaDadosTributoVO = listaDadosTributoVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#isMedicaoDiaria()
	 */
	@Override
	public boolean isMedicaoDiaria() {

		return medicaoDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#setMedicaoDiaria(boolean)
	 */
	@Override
	public void setMedicaoDiaria(boolean medicaoDiaria) {

		this.medicaoDiaria = medicaoDiaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getMotivoInclucao()
	 */
	@Override
	public long getIdMotivo() {

		return idMotivo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setMotivoInclucao(br.com.ggas
	 * .geral.EntidadeConteudo)
	 */
	@Override
	public void setIdMotivo(long idMotivo) {

		this.idMotivo = idMotivo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#setContrato(long)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	@Override
	public Date getDataEmissaoInformada() {

		return dataEmissaoInformada;
	}

	@Override
	public void setDataEmissaoInformada(Date dataEmissaoInformada) {

		this.dataEmissaoInformada = dataEmissaoInformada;
	}

	@Override
	public TipoDataRetroativaInvalida getTipoDataRetroativaInvalida() {

		return tipoDataRetroativaInvalida;
	}

	@Override
	public void setTipoDataRetroativaInvalida(TipoDataRetroativaInvalida tipoDataRetroativaInvalida) {

		this.tipoDataRetroativaInvalida = tipoDataRetroativaInvalida;
	}

	@Override
	public Boolean getIndicadorDataInformadaAplicada() {

		return indicadorDataInformadaAplicada;
	}

	@Override
	public void setIndicadorDataInformadaAplicada(Boolean indicadorDataInformada) {

		this.indicadorDataInformadaAplicada = indicadorDataInformada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getDataEmissao()
	 */
	@Override
	public Date getDataEmissao() {

		return dataEmissao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setDataEmissao(java.util.Date)
	 */
	@Override
	public void setDataEmissao(Date dataEmissao) {

		this.dataEmissao = dataEmissao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getMotivo()
	 */
	@Override
	public String getMotivo() {

		return motivo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setMotivo(java.lang.String)
	 */
	@Override
	public void setMotivo(String motivo) {

		this.motivo = motivo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getNumeroContrato()
	 */
	@Override
	public String getNumeroContrato() {

		return numeroContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setNumeroContrato(java.lang.String)
	 */
	@Override
	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getDadosAuditoria()
	 */
	@Override
	public DadosAuditoria getDadosAuditoria() {

		return dadosAuditoria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setDadosAuditoria(br.com.ggas
	 * .auditoria.DadosAuditoria)
	 */
	@Override
	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {

		this.dadosAuditoria = dadosAuditoria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getFaturasPendentes()
	 */
	@Override
	public Long[] getChavesFaturasPendentes() {

		return chavesFaturasPendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setFaturasPendentes(java.lang.Long[])
	 */
	@Override
	public void setChavesFaturasPendentes(Long[] chavesFaturasPendentes) {

		this.chavesFaturasPendentes = chavesFaturasPendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getExibirAlerta()
	 */
	@Override
	public Boolean getExibirAlerta() {

		return exibirAlerta;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setExibirAlerta(java.lang.Boolean)
	 */
	@Override
	public void setExibirAlerta(Boolean exibirAlerta) {

		this.exibirAlerta = exibirAlerta;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaCreditoDebitoARealizar()
	 */
	@Override
	public Collection<CreditoDebitoARealizar> getListaCreditoDebitoARealizar() {

		return listaCreditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaCreditoDebitoARealizar
	 * (java.util.Collection)
	 */
	@Override
	public void setListaCreditoDebitoARealizar(Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar) {

		this.listaCreditoDebitoARealizar = listaCreditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaDadosGeraisItens()
	 */
	@Override
	public Collection<DadosGeraisItensFatura> getListaDadosGeraisItens() {

		return listaDadosGeraisItens;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaDadosGeraisItens(java
	 * .util.Collection)
	 */
	@Override
	public void setListaDadosGeraisItens(Collection<DadosGeraisItensFatura> listaDadosGeraisItens) {

		this.listaDadosGeraisItens = listaDadosGeraisItens;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getListaFaturaItem()
	 */
	@Override
	public Collection<FaturaItem> getListaFaturaItem() {

		return listaFaturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaFaturaItem(java.util.Collection)
	 */
	@Override
	public void setListaFaturaItem(Collection<FaturaItem> listaFaturaItem) {

		this.listaFaturaItem = listaFaturaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaDebitosCreditoApuracao()
	 */
	@Override
	public Collection<CreditoDebitoDetalhamento> getListaDebitosCreditoApuracao() {

		return listaDebitosCreditoApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaDebitosCreditoApuracao
	 * (java.util.Collection)
	 */
	@Override
	public void setListaDebitosCreditoApuracao(Collection<CreditoDebitoDetalhamento> listaDebitosCreditoApuracao) {

		this.listaDebitosCreditoApuracao = listaDebitosCreditoApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #getListaFaturaItemImpressao()
	 */
	@Override
	public Collection<FaturaItemImpressao> getListaFaturaItemImpressao() {

		return listaFaturaItemImpressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setListaFaturaItemImpressao
	 * (java.util.Collection)
	 */
	@Override
	public void setListaFaturaItemImpressao(Collection<FaturaItemImpressao> listaFaturaItemImpressao) {

		this.listaFaturaItemImpressao = listaFaturaItemImpressao;
	}

	/**
	 * @param encerramento
	 */
	@Override
	public void setEncerramento(Boolean encerramento) {

		this.encerramento = encerramento;
	}

	/**
	 * @return
	 */
	@Override
	public Boolean getEncerramento() {

		return encerramento;
	}

	@Override
	public boolean isContratoAditadoAlterado() {

		return isContratoAditadoAlterado;
	}

	@Override
	public void setContratoAditadoAlterado(boolean isContratoAditadoAlterado) {

		this.isContratoAditadoAlterado = isContratoAditadoAlterado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getStatusNfe()
	 */
	@Override
	public EntidadeConteudo getStatusNfe() {

		return statusNfe;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setStatusNfe(br.com.ggas.geral
	 * .EntidadeConteudo)
	 */
	@Override
	public void setStatusNfe(EntidadeConteudo statusNfe) {

		this.statusNfe = statusNfe;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura#getNumeroSerieDocFiscal()
	 */
	@Override
	public String getNumeroSerieDocFiscal() {

		return numeroSerieDocFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura
	 * #setNumeroSerieDocFiscal(java.lang.String)
	 */
	@Override
	public void setNumeroSerieDocFiscal(String numeroSerieDocFiscal) {

		this.numeroSerieDocFiscal = numeroSerieDocFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoFatura#isFaturaAvulso()
	 */
	@Override
	public Boolean isFaturaAvulso() {

		return faturaAvulso;
	}

	@Override
	public void setFaturaAvulso(Boolean isFaturaAvulso) {

		this.faturaAvulso = isFaturaAvulso;

	}

	@Override
	public void setIdTransportadora(Long idTransportadora) {

		this.idTransportadora = idTransportadora;

	}

	@Override
	public Long getIdTransportadora() {

		return idTransportadora;
	}

	@Override
	public Long getIdVeiculo() {

		return idVeiculo;
	}

	@Override
	public void setIdVeiculo(Long idVeiculo) {

		this.idVeiculo = idVeiculo;
	}

	@Override
	public Long getIdMotorista() {

		return idMotorista;
	}

	@Override
	public void setIdMotorista(Long idMotorista) {

		this.idMotorista = idMotorista;
	}

	@Override
	public Boolean getIndicadorContratoComplementar() {

		return indicadorContratoComplementar;
	}

	@Override
	public void setIndicadorContratoComplementar(Boolean indicadorContratoComplementar) {

		this.indicadorContratoComplementar = indicadorContratoComplementar;
	}
	
	@Override
	public BigDecimal getTotalConsumoApurado() {
		BigDecimal totalCOnsumoApurado = null;
		
		if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {
			totalCOnsumoApurado = BigDecimal.ZERO;
			for (HistoricoConsumo historico : listaHistoricoConsumo) {
				
				if(historico.getConsumoApurado() != null) {
					totalCOnsumoApurado = totalCOnsumoApurado.add(historico.getConsumoApurado());
				}
			}
			
		}
		
		return totalCOnsumoApurado;
	}
	
	@Override
	public PontoConsumo getPontoConsumo() {
		PontoConsumo pontoConsumo = null;
		
		if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {
			for (HistoricoConsumo historico : listaHistoricoConsumo) {
				pontoConsumo = historico.getPontoConsumo();
				break;
			}
			
		}
		
		return pontoConsumo;
	}
	
	
	@Override
	public BigDecimal getValorTotal() {
		BigDecimal valorTotal = null;
		
		if (listaFaturaItemImpressao != null && !listaFaturaItemImpressao.isEmpty()) {
			valorTotal = BigDecimal.ZERO;
			for (FaturaItemImpressao faturaItem : listaFaturaItemImpressao) {
				
				if(!faturaItem.getIndicadorDesconto()) {
					valorTotal = valorTotal.add(faturaItem.getValorTotal());
				}
				
			}
			
		}
		
		return valorTotal;
	}

}
