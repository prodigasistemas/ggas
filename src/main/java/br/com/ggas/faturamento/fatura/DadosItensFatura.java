/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * aos dados de item de fatura
 *
 */
public interface DadosItensFatura extends Serializable, Cloneable {

	String BEAN_ID_DADOS_ITENS_FATURA = "dadosItensFatura";

	/**
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	void setValor(BigDecimal valor);

	/**
	 * @return the creditoDebitoARealizar
	 */
	CreditoDebitoARealizar getCreditoDebitoARealizar();

	/**
	 * @param creditoDebitoARealizar
	 *            the creditoDebitoARealizar to
	 *            set
	 */
	void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar);

	/**
	 * @return the itemFatura
	 */
	EntidadeConteudo getItemFatura();

	/**
	 * @param itemFatura
	 *            the itemFatura to set
	 */
	void setItemFatura(EntidadeConteudo itemFatura);

	/**
	 * @return the rubrica
	 */
	Rubrica getRubrica();

	/**
	 * @param rubrica
	 *            the rubrica to set
	 */
	void setRubrica(Rubrica rubrica);

	/**
	 * @return the FaturaItem
	 */
	FaturaItem getFaturaItem();

	/**
	 * @param faturaItem
	 *            the FaturaItem to set
	 */
	void setFaturaItem(FaturaItem faturaItem);

	/**
	 * @return the indicadorCredito
	 */
	boolean isIndicadorCredito();

	/**
	 * @param indicadorCredito
	 *            the indicadorCredito to set
	 */
	void setIndicadorCredito(boolean indicadorCredito);

	/**
	 * @return the PontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the PontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Checks if is credito abatido.
	 * 
	 * @return the creditoAbatido
	 */
	Boolean isCreditoAbatido();

	/**
	 * @param creditoAbatido
	 *            the creditoAbatido to set
	 */
	void setCreditoAbatido(Boolean creditoAbatido);

	/**
	 * @return the Tarifa
	 */
	public Tarifa getTarifa();

	/**
	 * @param tarifa
	 *            the Tarifa to set
	 */
	public void setTarifa(Tarifa tarifa);

	/**
	 * @return the DataInicial
	 */
	public Date getDataInicial();

	/**
	 * @param dataInicial
	 *            the DataInicial to set
	 */
	public void setDataInicial(Date dataInicial);

	/**
	 * @return DataFinal
	 */
	public Date getDataFinal();

	/**
	 * @param dataFinal
	 *            the DataFinal to set
	 */
	public void setDataFinal(Date dataFinal);

	/**
	 * @return the QuantidadeDias
	 */
	public Integer getQuantidadeDias();

	/**
	 * @param quantidadeDias
	 *            the QuantidadeDias to set
	 */
	public void setQuantidadeDias(Integer quantidadeDias);

	/**
	 * @return Consumo
	 */
	public BigDecimal getConsumo();

	/**
	 * @param consumo
	 *            the Consumo to set
	 */
	public void setConsumo(BigDecimal consumo);

	/**
	 * @return the ValorUnitario
	 */
	public BigDecimal getValorUnitario();

	/**
	 * @param valorUnitario
	 *            the ValorUnitario to set
	 */
	public void setValorUnitario(BigDecimal valorUnitario);

	/**
	 * @return ValorTotal
	 */
	public BigDecimal getValorTotal();

	/**
	 * @param valorTotal
	 *            the ValorTotal to set
	 */
	public void setValorTotal(BigDecimal valorTotal);

	/**
	 * @return the valorTotalDisponivel
	 */
	public BigDecimal getValorTotalDisponivel();

	/**
	 * @param valorTotalDisponivel
	 *            the valorTotalDisponivel to set
	 */
	public void setValorTotalDisponivel(BigDecimal valorTotalDisponivel);

	/**
	 * @return DescricaoDesconto
	 */
	public String getDescricaoDesconto();

	/**
	 * @param descricaoDesconto
	 *            the DescricaoDesconto to set
	 */
	public void setDescricaoDesconto(String descricaoDesconto);

	/**
	 * @return DadosFornecimentoGasVO - Retorna objeto Dados fornecimento gás vo.
	 */
	public DadosFornecimentoGasVO getDadosFornecimentoGasVO();

	/**
	 * @param dadosFornecimentoGasVO - Set dados fornecimento gás VO.
	 */
	public void setDadosFornecimentoGasVO(DadosFornecimentoGasVO dadosFornecimentoGasVO);

	/**
	 * Clona o objeto DadosItensFatura
	 * @return o clone do DadodsItensFatura
	 */
	DadosItensFatura clone();
}
