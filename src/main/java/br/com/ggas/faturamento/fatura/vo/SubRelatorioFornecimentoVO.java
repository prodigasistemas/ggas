/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;

/**
 * Classe responsável pela representação da entidade SubRelatorioFornecimento 
 *
 */
public class SubRelatorioFornecimentoVO implements Serializable {

	private static final long serialVersionUID = 4321246999383000363L;

	private String medidor;

	private String dataAtual;

	private String leituraAtual;

	private String dataAnterior;

	private String leituraAnterior;

	private String consumoNaoCorrigido;

	private String fatorPTZ;

	private String fatorCorrecao;

	private String fatorPCS;

	private String consumoCorrigido;

	private String totalFornecimento;

	private String quantidadeDias;

	private String tipoConsumo;
	
	private String consumoFaturado;

	public String getMedidor() {

		return medidor;
	}

	public void setMedidor(String medidor) {

		this.medidor = medidor;
	}

	public String getDataAtual() {

		return dataAtual;
	}

	public void setDataAtual(String dataAtual) {

		this.dataAtual = dataAtual;
	}

	public String getLeituraAtual() {

		return leituraAtual;
	}

	public void setLeituraAtual(String leituraAtual) {

		this.leituraAtual = leituraAtual;
	}

	public String getDataAnterior() {

		return dataAnterior;
	}

	public void setDataAnterior(String dataAnterior) {

		this.dataAnterior = dataAnterior;
	}

	public String getLeituraAnterior() {

		return leituraAnterior;
	}

	public void setLeituraAnterior(String leituraAnterior) {

		this.leituraAnterior = leituraAnterior;
	}

	public String getConsumoNaoCorrigido() {

		return consumoNaoCorrigido;
	}

	public void setConsumoNaoCorrigido(String consumoNaoCorrigido) {

		this.consumoNaoCorrigido = consumoNaoCorrigido;
	}

	public String getFatorPTZ() {

		return fatorPTZ;
	}

	public void setFatorPTZ(String fatorPTZ) {

		this.fatorPTZ = fatorPTZ;
	}

	public String getFatorCorrecao() {

		return fatorCorrecao;
	}

	public void setFatorCorrecao(String fatorCorrecao) {

		this.fatorCorrecao = fatorCorrecao;
	}

	public String getFatorPCS() {

		return fatorPCS;
	}

	public void setFatorPCS(String fatorPCS) {

		this.fatorPCS = fatorPCS;
	}

	public String getConsumoCorrigido() {

		return consumoCorrigido;
	}

	public void setConsumoCorrigido(String consumoCorrigido) {

		this.consumoCorrigido = consumoCorrigido;
	}

	public String getTotalFornecimento() {

		return totalFornecimento;
	}

	public void setTotalFornecimento(String totalFornecimento) {

		this.totalFornecimento = totalFornecimento;
	}

	public String getQuantidadeDias() {

		return quantidadeDias;
	}

	public void setQuantidadeDias(String quantidadeDias) {

		this.quantidadeDias = quantidadeDias;
	}

	public String getTipoConsumo() {

		return tipoConsumo;
	}

	public void setTipoConsumo(String tipoConsumo) {

		this.tipoConsumo = tipoConsumo;
	}

	public String getConsumoFaturado() {
		return consumoFaturado;
	}

	public void setConsumoFaturado(String consumoFaturado) {
		this.consumoFaturado = consumoFaturado;
	}

}
