/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.fatura.DadosLeituraConsumoFatura;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.leitura.Leiturista;

class DadosLeituraConsumoFaturaImpl implements DadosLeituraConsumoFatura {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3770581141703810345L;

	// Dados de leitura e consumo atual.
	private long idPontoConsumo;

	private long idHistoricoMedicao;

	private String descricaoPontoConsumo;

	private BigDecimal leituraAtual;

	private boolean informadoCliente;

	private Date dataLeituraAtual;

	private Leiturista leiturista;

	private BigDecimal consumoApurado;

	private int qtdDigitosMedidor;

	// Dados de leitura e consumo anterior.
	private BigDecimal leituraAnterior;

	private Date dataLeituraAnterior;

	private AnormalidadeLeitura anormalidadeLeitura;

	private BigDecimal volumeAnteriorApurado;

	private BigDecimal consumoFaturado;

	private Integer diasConsumo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosLeituraConsumoFatura
	 * #getIdPontoConsumo()
	 */
	@Override
	public long getIdPontoConsumo() {

		return idPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosLeituraConsumoFatura
	 * #setIdPontoConsumo(long)
	 */
	@Override
	public void setIdPontoConsumo(long idPontoConsumo) {

		this.idPontoConsumo = idPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosLeituraConsumoFatura
	 * #getIdHistoricoMedicao()
	 */
	@Override
	public long getIdHistoricoMedicao() {

		return idHistoricoMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosLeituraConsumoFatura
	 * #setIdHistoricoMedicao(long)
	 */
	@Override
	public void setIdHistoricoMedicao(long idHistoricoMedicao) {

		this.idHistoricoMedicao = idHistoricoMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getDescricaoPontoConsumo()
	 */
	@Override
	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setDescricaoPontoConsumo(java.lang.String)
	 */
	@Override
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura#getLeituraAtual()
	 */
	@Override
	public BigDecimal getLeituraAtual() {

		return leituraAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setLeituraAtual(java.math.BigDecimal)
	 */
	@Override
	public void setLeituraAtual(BigDecimal leituraAtual) {

		this.leituraAtual = leituraAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #isInformadoCliente()
	 */
	@Override
	public boolean isInformadoCliente() {

		return informadoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setInformadoCliente(boolean)
	 */
	@Override
	public void setInformadoCliente(boolean informadoCliente) {

		this.informadoCliente = informadoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getDataLeituraAtual()
	 */
	@Override
	public Date getDataLeituraAtual() {

		return dataLeituraAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setDataLeituraAtual(java.util.Date)
	 */
	@Override
	public void setDataLeituraAtual(Date dataLeituraAtual) {

		this.dataLeituraAtual = dataLeituraAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura#getLeiturista()
	 */
	@Override
	public Leiturista getLeiturista() {

		return leiturista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setLeiturista(br.com
	 * .ggas.medicao.leitura.Leiturista)
	 */
	@Override
	public void setLeiturista(Leiturista leiturista) {

		this.leiturista = leiturista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getConsumoApurado()
	 */
	@Override
	public BigDecimal getConsumoApurado() {

		return consumoApurado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setConsumoApurado(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoApurado(BigDecimal consumoApurado) {

		this.consumoApurado = consumoApurado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosLeituraConsumoFatura
	 * #getQtdDigitosMedidor()
	 */
	@Override
	public int getQtdDigitosMedidor() {

		return qtdDigitosMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosLeituraConsumoFatura
	 * #setQtdDigitosMedidor(int)
	 */
	@Override
	public void setQtdDigitosMedidor(int qtdDigitosMedidor) {

		this.qtdDigitosMedidor = qtdDigitosMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getLeituraAnterior()
	 */
	@Override
	public BigDecimal getLeituraAnterior() {

		return leituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setLeituraAnterior(java.math.BigDecimal)
	 */
	@Override
	public void setLeituraAnterior(BigDecimal leituraAnterior) {

		this.leituraAnterior = leituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getDataLeituraAnterior()
	 */
	@Override
	public Date getDataLeituraAnterior() {

		return dataLeituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setDataLeituraAnterior(java.util.Date)
	 */
	@Override
	public void setDataLeituraAnterior(Date dataLeituraAnterior) {

		this.dataLeituraAnterior = dataLeituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getAnormalidadeLeitura()
	 */
	@Override
	public AnormalidadeLeitura getAnormalidadeLeitura() {

		return anormalidadeLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setAnormalidadeLeitura
	 * (br.com.ggas.medicao.
	 * anormalidade.AnormalidadeLeitura)
	 */
	@Override
	public void setAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura) {

		this.anormalidadeLeitura = anormalidadeLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getVolumeAnteriorApurado()
	 */
	@Override
	public BigDecimal getVolumeAnteriorApurado() {

		return volumeAnteriorApurado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setVolumeAnteriorApurado
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setVolumeAnteriorApurado(BigDecimal volumeAnteriorApurado) {

		this.volumeAnteriorApurado = volumeAnteriorApurado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #getConsumoFaturado()
	 */
	@Override
	public BigDecimal getConsumoFaturado() {

		return consumoFaturado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setConsumoFaturado(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoFaturado(BigDecimal consumoFaturado) {

		this.consumoFaturado = consumoFaturado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura#getDiasConsumo()
	 */
	@Override
	public Integer getDiasConsumo() {

		return diasConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosLeituraConsumoFatura
	 * #setDiasConsumo(java.lang.Integer)
	 */
	@Override
	public void setDiasConsumo(Integer diasConsumo) {

		this.diasConsumo = diasConsumo;
	}
}
