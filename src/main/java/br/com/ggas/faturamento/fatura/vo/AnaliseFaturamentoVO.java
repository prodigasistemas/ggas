/*
 Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

 Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/***
 * Classe de VO para geração de Relatório de Análise de Faturamento 
 * e tratamento dos dados
 */
public class AnaliseFaturamentoVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private Long cdPontoConsumo;
	
	private String dsPontoConsumo;
	
	private String dsRota;
	
	private String dsGrupoFaturamento;
	
	private String dsSegmento;
	
	private String dataInicial;
	 
	private Date dataFinal;
	
	private Integer diasConsumo;
	 
	private BigDecimal consumo;
	
	private BigDecimal consumoGas;
	
	private BigDecimal icms;
	
	private BigDecimal icmsSubstituto;
	
	private BigDecimal valorTotal;
	

	public Long getCdPontoConsumo() {
		return cdPontoConsumo;
	}

	public void setCdPontoConsumo(Long cdPontoConsumo) {
		this.cdPontoConsumo = cdPontoConsumo;
	}

	public String getDsPontoConsumo() {
		return dsPontoConsumo;
	}

	public void setDsPontoConsumo(String dsPontoConsumo) {
		this.dsPontoConsumo = dsPontoConsumo;
	}

	public String getDsRota() {
		return dsRota;
	}

	public void setDsRota(String dsRota) {
		this.dsRota = dsRota;
	}

	public String getDsGrupoFaturamento() {
		return dsGrupoFaturamento;
	}

	public void setDsGrupoFaturamento(String dsGrupoFaturamento) {
		this.dsGrupoFaturamento = dsGrupoFaturamento;
	}

	public String getDsSegmento() {
		return dsSegmento;
	}

	public void setDsSegmento(String dsSegmento) {
		this.dsSegmento = dsSegmento;
	}


	/**
	 * Método para calcular a Data Inicial
	 * @return
	 */
	public String getDataInicial() {
		Calendar calendarData = Calendar.getInstance();
		calendarData.setTime(dataFinal);
		calendarData.add(Calendar.DATE,-(diasConsumo-1));
		Date data = calendarData.getTime();
		SimpleDateFormat dataF = new SimpleDateFormat("dd/MM/yyyy");
		dataInicial = dataF.format(data);
		return dataInicial;
	}

	public void setDataInicial(String data) {
		dataInicial = data;
	}

	public String getDataFinal() {
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");	 
		return data.format(dataFinal);
	}

	public void setDataFinal(Date pDataFinal) {
		dataFinal = pDataFinal;
	}


	public BigDecimal getConsumo() {
		if(consumo != null) {
			return consumo.setScale(0, RoundingMode.HALF_EVEN);
		}else {
			return consumo;
		}
	}

	public void setConsumo(BigDecimal consumo) {
		this.consumo = consumo;
	}


	public BigDecimal getValorTotal() {	
		setValorTotal(); 
		return valorTotal;
	}

	/**
	 * Método para somar o valor total: 
	 * consumoGas com Icms Substituto
	 */
	public void setValorTotal() {
		if (icmsSubstituto != null ) {
			this.valorTotal = consumoGas.add(icmsSubstituto);
		}else {
			this.valorTotal = consumoGas;
		}
	}

	public Integer getDiasConsumo() {
		return diasConsumo;
	}

	public void setDiasConsumo(Integer diasConsumo) {
		this.diasConsumo = diasConsumo;
	}

	public BigDecimal getConsumoGas() {
		return consumoGas;
	}

	public void setConsumoGas(BigDecimal consumoGas) {
		this.consumoGas = consumoGas;
	}

	public BigDecimal getIcmsSubstituto() {
		if(icmsSubstituto!= null) {
			return icmsSubstituto;
		}else {
			return BigDecimal.ZERO;
		}
	}

	public void setIcmsSubstituto(BigDecimal icmsSubstituto) {
		this.icmsSubstituto = icmsSubstituto;
	}
	
	public void setIcms(BigDecimal icms) {
		this.icms = icms;
	}

	public BigDecimal getIcms() {
		if(icms!= null) {
			return icms;
		}else {
			return BigDecimal.ZERO;
		}
	}
	

}
