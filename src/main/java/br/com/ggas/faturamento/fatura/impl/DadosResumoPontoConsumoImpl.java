/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.util.Collection;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;

/**
 * The Class DadosResumoPontoConsumoImpl.
 */
public class DadosResumoPontoConsumoImpl implements DadosResumoPontoConsumo {

	private static final long serialVersionUID = -1494039100749771933L;

	private Contrato contrato;

	private ContratoPontoConsumo contratoPontoConsumo;

	private ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamentoGas;

	private HistoricoConsumo historicoConsumo;

	private boolean temSubstituicaoTributaria;

	private RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria;	
	
	private Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaMedidor;

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#setContrato(br.com.ggas.contrato.contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#getContratoPontoConsumo()
	 */
	@Override
	public ContratoPontoConsumo getContratoPontoConsumo() {

		return contratoPontoConsumo;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#setContratoPontoConsumo(br.com.ggas.contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) {

		this.contratoPontoConsumo = contratoPontoConsumo;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#getContratoPontoConsumoItemFaturamentoGas()
	 */
	@Override
	public ContratoPontoConsumoItemFaturamento getContratoPontoConsumoItemFaturamentoGas() {

		return contratoPontoConsumoItemFaturamentoGas;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura
	 * .DadosResumoPontoConsumo
	 * #setContratoPontoConsumoItemFaturamento(
	 * br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento)
	 */
	@Override
	public void setContratoPontoConsumoItemFaturamentoGas(ContratoPontoConsumoItemFaturamento contratoPontoConsumoItemFaturamentoGas) {

		this.contratoPontoConsumoItemFaturamentoGas = contratoPontoConsumoItemFaturamentoGas;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#getHistoricoConsumo()
	 */
	@Override
	public HistoricoConsumo getHistoricoConsumo() {

		return historicoConsumo;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#setHistoricoConsumo(br.com.ggas.medicao.consumo.HistoricoConsumo)
	 */
	@Override
	public void setHistoricoConsumo(HistoricoConsumo historicoConsumo) {

		this.historicoConsumo = historicoConsumo;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#isTemSubstituicaoTributaria()
	 */
	@Override
	public boolean isTemSubstituicaoTributaria() {

		return temSubstituicaoTributaria;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#setTemSubstituicaoTributaria(boolean)
	 */
	@Override
	public void setTemSubstituicaoTributaria(boolean temSubstituicaoTributaria) {

		this.temSubstituicaoTributaria = temSubstituicaoTributaria;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#getRamoAtividadeSubstituicaoTributaria()
	 */
	@Override
	public RamoAtividadeSubstituicaoTributaria getRamoAtividadeSubstituicaoTributaria() {

		return ramoAtividadeSubstituicaoTributaria;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura
	 * .DadosResumoPontoConsumo
	 * #setRamoAtividadeSubstituicaoTributaria(
	 * br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria)
	 */
	@Override
	public void setRamoAtividadeSubstituicaoTributaria(RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria) {

		this.ramoAtividadeSubstituicaoTributaria = ramoAtividadeSubstituicaoTributaria;
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#getListaHistoricoOperacaMedidor()
	 */
	@Override
	public Collection<HistoricoOperacaoMedidor> getListaHistoricoOperacaMedidor() {

		return listaHistoricoOperacaMedidor;
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo#setListaHistoricoOperacaMedidor(java.util.Collection)
	 */
	@Override
	public void setListaHistoricoOperacaMedidor(Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaMedidor) {

		this.listaHistoricoOperacaMedidor = listaHistoricoOperacaMedidor;
	}

	
}
