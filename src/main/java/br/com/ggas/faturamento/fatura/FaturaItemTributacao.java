/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.math.BigDecimal;

import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a tributação de item de fatura
 *
 */
public interface FaturaItemTributacao extends EntidadeNegocio, Cloneable {

	String BEAN_ID_FATURA_ITEM_TRIBUTACAO = "faturaItemTributacao";

	/**
	 * @return BigDecimal - Percentual de aliquota.
	 */
	BigDecimal getPercentualAliquota();

	/**
	 * @param percentualAliquota - Set Percentual de aliquota.
	 */
	void setPercentualAliquota(BigDecimal percentualAliquota);

	/**
	 * @return BigDecimal - Valor base do cálculo
	 */
	BigDecimal getValorBaseCalculo();

	/**
	 * @param valorBaseCalculo - Set valor base do cálculo.
	 */
	void setValorBaseCalculo(BigDecimal valorBaseCalculo);

	/**
	 * @return BigDecimal - Valor do imposto.
	 */
	BigDecimal getValorImposto();

	/**
	 * @param valorImposto - Set Valor do imposto.
	 */
	void setValorImposto(BigDecimal valorImposto);

	/**
	 * @return BigDecimal - Valor base da substituiçao.
	 */
	BigDecimal getValorBaseSubstituicao();

	/**
	 * @param valorBaseSubstituicao - Set Valor base da Substituição
	 */
	void setValorBaseSubstituicao(BigDecimal valorBaseSubstituicao);

	/**
	 * @return BigDecimal - Valor da Substituição.
	 */
	BigDecimal getValorSubstituicao();

	/**
	 * @param valorSubstituicao - Set Valor da Substituiçao.
	 */
	void setValorSubstituicao(BigDecimal valorSubstituicao);

	/**
	 * @return Tributo - Tributo
	 */
	Tributo getTributo();

	/**
	 * @param tributo - Set tributo.
	 */
	void setTributo(Tributo tributo);

	/**
	 * @return FaturaItem - Faturar item.
	 */
	FaturaItem getFaturaItem();

	/**
	 * @param faturaItem - Set Faturar item.
	 */
	void setFaturaItem(FaturaItem faturaItem);

	/**
	 * Clona o objeto FaturaItemTributacao
	 * @return O objeto clone
	 */
	FaturaItemTributacao clone();
}
