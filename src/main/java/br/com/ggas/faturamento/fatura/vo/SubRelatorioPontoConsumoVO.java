/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;

/**
 * Classe responsável pela representação da entidade SubRelatorioPontoConsumo
 *
 */
public class SubRelatorioPontoConsumoVO implements Serializable {

	private static final long serialVersionUID = 623828927223976597L;

	private String segmento;

	private String tarifa;

	private String tipoConsumo;

	private String dataProximaLeitura;

	private String descricaoPontoConsumo;

	private String endereco;

	private String numeroDiasPeriodo;

	private String rotaLeitura;

	private String medidor;

	public String getTipoConsumo() {

		return tipoConsumo;
	}

	public void setTipoConsumo(String tipoConsumo) {

		this.tipoConsumo = tipoConsumo;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getRotaLeitura() {

		return rotaLeitura;
	}

	public void setRotaLeitura(String rotaLeitura) {

		this.rotaLeitura = rotaLeitura;
	}

	public String getMedidor() {

		return medidor;
	}

	public void setMedidor(String medidor) {

		this.medidor = medidor;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public String getTarifa() {

		return tarifa;
	}

	public void setTarifa(String tarifa) {

		this.tarifa = tarifa;
	}

	public String getDataProximaLeitura() {

		return dataProximaLeitura;
	}

	public void setDataProximaLeitura(String dataProximaLeitura) {

		this.dataProximaLeitura = dataProximaLeitura;
	}

	public String getNumeroDiasPeriodo() {

		return numeroDiasPeriodo;
	}

	public void setNumeroDiasPeriodo(String numeroDiasPeriodo) {

		this.numeroDiasPeriodo = numeroDiasPeriodo;
	}

}
