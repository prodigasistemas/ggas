package br.com.ggas.faturamento.fatura.impl;

/**
 * Classe responsável pela transferência de dados entre as telas relacionadas a
 * Série Nota Fiscal
 *
 */
public class SerieVO {

	private Boolean indicadorControleMensal;
	private Boolean indicadorSerieEletronica;
	private Boolean indicadorContingenciaScan;
	private Boolean habilitado;
	private String anoMes;
	private String sequenciaNumeracao;
	
	public Boolean getIndicadorControleMensal() {
		return indicadorControleMensal;
	}
	public void setIndicadorControleMensal(Boolean indicadorControleMensal) {
		this.indicadorControleMensal = indicadorControleMensal;
	}
	public Boolean getIndicadorSerieEletronica() {
		return indicadorSerieEletronica;
	}
	public void setIndicadorSerieEletronica(Boolean indicadorSerieEletronica) {
		this.indicadorSerieEletronica = indicadorSerieEletronica;
	}
	public Boolean getIndicadorContingenciaScan() {
		return indicadorContingenciaScan;
	}
	public void setIndicadorContingenciaScan(Boolean indicadorContingenciaSca) {
		this.indicadorContingenciaScan = indicadorContingenciaSca;
	}
	public Boolean getHabilitado() {
		return habilitado;
	}
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	public String getAnoMes() {
		return anoMes;
	}
	public void setAnoMes(String anoMes) {
		this.anoMes = anoMes;
	}
	public String getSequenciaNumeracao() {
		return sequenciaNumeracao;
	}
	public void setSequenciaNumeracao(String sequenciaNumeracao) {
		this.sequenciaNumeracao = sequenciaNumeracao;
	}
}

