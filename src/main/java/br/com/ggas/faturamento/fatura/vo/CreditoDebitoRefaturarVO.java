/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.geral.EntidadeConteudo;
/**
 * Classe responsável pela representação de uma entidade CreditoDebitoRefaturar
 *
 */
public class CreditoDebitoRefaturarVO implements Serializable {

	private static final int ESCALA = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3706331601585335939L;

	private CreditoDebitoARealizar creditoDebitoARealizar;

	private BigDecimal saldoCredito;

	private BigDecimal valorLancado;

	private boolean isIncluidoNaFatura;

	/**
	 * @return the creditoDebitoARealizar
	 */
	public CreditoDebitoARealizar getCreditoDebitoARealizar() {

		return creditoDebitoARealizar;
	}

	/**
	 * @param creditoDebitoARealizar
	 *            the creditoDebitoARealizar to
	 *            set
	 */
	public void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar) {

		this.creditoDebitoARealizar = creditoDebitoARealizar;
	}

	/**
	 * @return the isIncluidoNaFatura
	 */
	public boolean getIsIncluidoNaFatura() {

		return isIncluidoNaFatura;
	}

	/**
	 * @param isIncluidoNaFatura
	 *            the isIncluidoNaFatura to set
	 */
	public void setIncluidoNaFatura(boolean isIncluidoNaFatura) {

		this.isIncluidoNaFatura = isIncluidoNaFatura;
	}

	public BigDecimal getValorUnitario() {

		BigDecimal valorUnitario = creditoDebitoARealizar.getValor();
		if(creditoDebitoARealizar.getQuantidade() != null) {
			valorUnitario = valorUnitario.divide(creditoDebitoARealizar.getQuantidade(), ESCALA, RoundingMode.HALF_UP);
		}
		return valorUnitario;
	}

	/**
	 * @return the valorLancado
	 */
	public BigDecimal getValorLancado() {

		return valorLancado;
	}

	/**
	 * @param valorLancado
	 *            the valorLancado to set
	 */
	public void setValorLancado(BigDecimal valorLancado) {

		this.valorLancado = valorLancado;
	}

	/**
	 * @return the anoMesCobranca
	 */
	public Integer getAnoMesCobranca() {

		return creditoDebitoARealizar.getAnoMesCobranca();
	}

	/**
	 * @return the creditoDebitoNegociado
	 */
	public CreditoDebitoNegociado getCreditoDebitoNegociado() {

		return creditoDebitoARealizar.getCreditoDebitoNegociado();
	}

	/**
	 * @return the creditoDebitoSituacao
	 */
	public CreditoDebitoSituacao getCreditoDebitoSituacao() {

		return creditoDebitoARealizar.getCreditoDebitoSituacao();
	}

	/**
	 * @return the numeroPrestacao
	 */
	public Integer getNumeroPrestacao() {

		return creditoDebitoARealizar.getNumeroPrestacao();
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {

		return creditoDebitoARealizar.getValor();
	}

	/**
	 * @return the anoMesFaturamento
	 */
	public Integer getAnoMesFaturamento() {

		return creditoDebitoARealizar.getAnoMesFaturamento();
	}

	/**
	 * @return the numeroCiclo
	 */
	public Integer getNumeroCiclo() {

		return creditoDebitoARealizar.getNumeroCiclo();
	}

	/**
	 * @return the dataVencimento
	 */
	public Date getDataVencimento() {

		return creditoDebitoARealizar.getDataVencimento();
	}

	/**
	 * @return the indicadorAntecipacao
	 */
	public boolean isIndicadorAntecipacao() {

		return creditoDebitoARealizar.isIndicadorAntecipacao();
	}

	/**
	 * @return the dataAntecipacao
	 */
	public Date getDataAntecipacao() {

		return creditoDebitoARealizar.getDataAntecipacao();
	}

	/**
	 * @return the valorAntecipacao
	 */
	public BigDecimal getValorAntecipacao() {

		return creditoDebitoARealizar.getValorAntecipacao();
	}

	/**
	 * @return the devolucao
	 */
	public Devolucao getDevolucao() {

		return creditoDebitoARealizar.getDevolucao();
	}

	/**
	 * @return the origem
	 */
	public FaturaGeral getOrigem() {

		return creditoDebitoARealizar.getOrigem();
	}

	/**
	 * @return the situacaoPagamento
	 */
	public EntidadeConteudo getSituacaoPagamento() {

		return creditoDebitoARealizar.getSituacaoPagamento();
	}

	/**
	 * @return the motivoCancelamento
	 */
	public EntidadeConteudo getMotivoCancelamento() {

		return creditoDebitoARealizar.getMotivoCancelamento();
	}

	/**
	 * @return the quantidade
	 */
	public BigDecimal getQuantidade() {

		return creditoDebitoARealizar.getQuantidade();
	}

	/**
	 * getChavePrimaria
	 * 
	 * @return
	 */
	public long getChavePrimaria() {

		return creditoDebitoARealizar.getChavePrimaria();
	}

	/**
	 * @return the saldoCredito
	 */
	public BigDecimal getSaldoCredito() {

		return saldoCredito;
	}

	/**
	 * @param saldoCredito
	 *            the saldoCredito to set
	 */
	public void setSaldoCredito(BigDecimal saldoCredito) {

		this.saldoCredito = saldoCredito;
	}
}
