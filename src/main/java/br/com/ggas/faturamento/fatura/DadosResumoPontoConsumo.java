/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.Serializable;
import java.util.Collection;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;

/**
 * The Interface DadosResumoPontoConsumo.
 */
public interface DadosResumoPontoConsumo extends Serializable {

	/**
	 * @return
	 */
	Contrato getContrato();
	
	/**
	 * @param contrato
	 */
	void setContrato(Contrato contrato);
	
	/**
	 * @return
	 */
	ContratoPontoConsumo getContratoPontoConsumo();
	
	/**
	 * @param contratoPontoConsumo
	 */
	void setContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo);
	
	/**
	 * @return
	 */
	ContratoPontoConsumoItemFaturamento getContratoPontoConsumoItemFaturamentoGas();
	
	/**
	 * Sets the contrato ponto consumo item faturamento gas.
	 *
	 * @param contratoPontoConsumoItemFaturamentoGas the new contrato ponto consumo item faturamento gas
	 */
	void setContratoPontoConsumoItemFaturamentoGas(ContratoPontoConsumoItemFaturamento 
					contratoPontoConsumoItemFaturamentoGas);
	
	/**
	 * @return
	 */
	HistoricoConsumo getHistoricoConsumo();
	
	/**
	 * @param historicoConsumo
	 */
	void setHistoricoConsumo(HistoricoConsumo historicoConsumo);
	
	/**
	 * @return
	 */
	boolean isTemSubstituicaoTributaria();
	
	/**
	 * @param temSubstituicaoTributaria
	 */
	void setTemSubstituicaoTributaria(boolean temSubstituicaoTributaria);
	
	/**
	 * @return
	 */
	RamoAtividadeSubstituicaoTributaria getRamoAtividadeSubstituicaoTributaria();
	
	/**
	 * @param ramoAtividadeSubstituicaoTributaria
	 */
	void setRamoAtividadeSubstituicaoTributaria(RamoAtividadeSubstituicaoTributaria 
					ramoAtividadeSubstituicaoTributaria);
		
	/**
	 * @return
	 */
	Collection<HistoricoOperacaoMedidor> getListaHistoricoOperacaMedidor();
	
	/**
	 * @param listaHistoricoOperacaMedidor
	 */
	void setListaHistoricoOperacaMedidor(Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaMedidor);
	
}
