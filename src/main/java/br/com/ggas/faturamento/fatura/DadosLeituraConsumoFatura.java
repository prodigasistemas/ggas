/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.leitura.Leiturista;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * aos dados de leitura do consumo da fatura 
 *
 */
public interface DadosLeituraConsumoFatura extends Serializable {

	String BEAN_ID_DADOS_LEITURA_CONSUMO_FATURA = "dadosLeituraConsumoFatura";

	/**
	 * @return the idPontoConsumo
	 */
	long getIdPontoConsumo();

	/**
	 * @param idPontoConsumo
	 *            the idPontoConsumo to set
	 */
	void setIdPontoConsumo(long idPontoConsumo);

	/**
	 * @return the idHistoricoMedicao
	 */
	long getIdHistoricoMedicao();

	/**
	 * @param idHistoricoMedicao
	 *            the idHistoricoMedicao to set
	 */
	void setIdHistoricoMedicao(long idHistoricoMedicao);

	/**
	 * @return the descricaoPontoConsumo
	 */
	String getDescricaoPontoConsumo();

	/**
	 * @param descricaoPontoConsumo
	 *            the descricaoPontoConsumo to set
	 */
	void setDescricaoPontoConsumo(String descricaoPontoConsumo);

	/**
	 * @return the leituraAtual
	 */
	BigDecimal getLeituraAtual();

	/**
	 * @param leituraAtual
	 *            the leituraAtual to set
	 */
	void setLeituraAtual(BigDecimal leituraAtual);

	/**
	 * @return the informadoCliente
	 */
	boolean isInformadoCliente();

	/**
	 * @param informadoCliente
	 *            the informadoCliente to set
	 */
	void setInformadoCliente(boolean informadoCliente);

	/**
	 * @return the dataLeituraAtual
	 */
	Date getDataLeituraAtual();

	/**
	 * @param dataLeituraAtual
	 *            the dataLeituraAtual to set
	 */
	void setDataLeituraAtual(Date dataLeituraAtual);

	/**
	 * @return the leiturista
	 */
	Leiturista getLeiturista();

	/**
	 * @param leiturista
	 *            the leiturista to set
	 */
	void setLeiturista(Leiturista leiturista);

	/**
	 * @return the consumoApurado
	 */
	BigDecimal getConsumoApurado();

	/**
	 * @param consumoApurado
	 *            the consumoApurado to set
	 */
	void setConsumoApurado(BigDecimal consumoApurado);

	/**
	 * @return the qtdDigitosMedidor
	 */
	int getQtdDigitosMedidor();

	/**
	 * @param qtdDigitosMedidor
	 *            the qtdDigitosMedidor to set
	 */
	void setQtdDigitosMedidor(int qtdDigitosMedidor);

	/**
	 * @return the leituraAnterior
	 */
	BigDecimal getLeituraAnterior();

	/**
	 * @param leituraAnterior
	 *            the leituraAnterior to set
	 */
	void setLeituraAnterior(BigDecimal leituraAnterior);

	/**
	 * @return the dataLeituraAnterior
	 */
	Date getDataLeituraAnterior();

	/**
	 * @param dataLeituraAnterior
	 *            the dataLeituraAnterior to set
	 */
	void setDataLeituraAnterior(Date dataLeituraAnterior);

	/**
	 * @return the anormalidadeLeitura
	 */
	AnormalidadeLeitura getAnormalidadeLeitura();

	/**
	 * @param anormalidadeLeitura
	 *            the anormalidadeLeitura to set
	 */
	void setAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura);

	/**
	 * @return the volumeAnteriorApurado
	 */
	BigDecimal getVolumeAnteriorApurado();

	/**
	 * @param volumeAnteriorApurado
	 *            the volumeAnteriorApurado to set
	 */
	void setVolumeAnteriorApurado(BigDecimal volumeAnteriorApurado);

	/**
	 * @return the consumoFaturado
	 */
	BigDecimal getConsumoFaturado();

	/**
	 * @param consumoFaturado
	 *            the consumoFaturado to set
	 */
	void setConsumoFaturado(BigDecimal consumoFaturado);

	/**
	 * @return the diasConsumo
	 */
	Integer getDiasConsumo();

	/**
	 * @param diasConsumo
	 *            the diasConsumo to set
	 */
	void setDiasConsumo(Integer diasConsumo);

}
