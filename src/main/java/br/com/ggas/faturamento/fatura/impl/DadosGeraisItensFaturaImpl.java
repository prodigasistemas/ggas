/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoVO;
import br.com.ggas.faturamento.fatura.DadosGeraisItensFatura;
import br.com.ggas.faturamento.fatura.DadosItensFatura;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;

class DadosGeraisItensFaturaImpl implements DadosGeraisItensFatura {

	private static final long serialVersionUID = -1569804884758310849L;

	private String descricao;

	private Date dataVencimento;

	private Collection<DadosItensFatura> listaDadosItensFatura = new ArrayList<DadosItensFatura>();

	private boolean indicadorProduto;

	private Collection<DadosFornecimentoGasVO> listaDadosFornecimentoGasVO = new ArrayList<DadosFornecimentoGasVO>();

	private Collection<DadosTributoVO> listaDadosTributoVO = new ArrayList<DadosTributoVO>();

	private BigDecimal valorTotalFatura;

	private BigDecimal valorTotalDebitos;

	private Collection<DadosItensFatura> listaDadosItensFaturaExibicao = new ArrayList<DadosItensFatura>();

	private ApuracaoVO apuracao;

	private Date dataVencimentoParaDataEmissaoInformada;

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosGeraisItensFatura#getDataVencimento()
	 */
	@Override
	public Date getDataVencimento() {

		return dataVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosGeraisItensFatura
	 * #setDataVencimento(java.util.Date)
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosGeraisItensFatura
	 * #getListaDadosItensFatura()
	 */
	@Override
	public Collection<DadosItensFatura> getListaDadosItensFatura() {

		return listaDadosItensFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.impl.
	 * DadosGeraisItensFatura
	 * #setListaDadosItensFatura
	 * (java.util.Collection)
	 */
	@Override
	public void setListaDadosItensFatura(Collection<DadosItensFatura> listaDadosItensFatura) {

		this.listaDadosItensFatura = listaDadosItensFatura;
	}

	/**
	 * @return the indicadorProduto
	 */
	@Override
	public boolean isIndicadorProduto() {

		return indicadorProduto;
	}

	/**
	 * @param indicadorProduto
	 *            the indicadorProduto to set
	 */
	@Override
	public void setIndicadorProduto(boolean indicadorProduto) {

		this.indicadorProduto = indicadorProduto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #getListaDadosFornecimentoGasVO()
	 */
	@Override
	public Collection<DadosFornecimentoGasVO> getListaDadosFornecimentoGasVO() {

		return listaDadosFornecimentoGasVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #setListaDadosFornecimentoGasVO
	 * (java.util.Collection)
	 */
	@Override
	public void setListaDadosFornecimentoGasVO(Collection<DadosFornecimentoGasVO> listaDadosFornecimentoGasVO) {

		this.listaDadosFornecimentoGasVO = listaDadosFornecimentoGasVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #getListaDadosTributoVO()
	 */
	@Override
	public Collection<DadosTributoVO> getListaDadosTributoVO() {

		return listaDadosTributoVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #setListaDadosTributoVO
	 * (java.util.Collection)
	 */
	@Override
	public void setListaDadosTributoVO(Collection<DadosTributoVO> listaDadosTributoVO) {

		this.listaDadosTributoVO = listaDadosTributoVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #getValorTotalFatura()
	 */
	@Override
	public BigDecimal getValorTotalFatura() {

		return valorTotalFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #setValorTotalFatura(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalFatura(BigDecimal valorTotalFatura) {

		this.valorTotalFatura = valorTotalFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #getValorTotalDebitos()
	 */
	@Override
	public BigDecimal getValorTotalDebitos() {

		return valorTotalDebitos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #setValorTotalDebitos(java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalDebitos(BigDecimal valorTotalDebitos) {

		this.valorTotalDebitos = valorTotalDebitos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #getListaDadosItensFaturaExibicao()
	 */
	@Override
	public Collection<DadosItensFatura> getListaDadosItensFaturaExibicao() {

		return listaDadosItensFaturaExibicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #setListaDadosItensFaturaExibicao
	 * (java.util.Collection)
	 */
	@Override
	public void setListaDadosItensFaturaExibicao(Collection<DadosItensFatura> listaDadosItensFaturaExibicao) {

		this.listaDadosItensFaturaExibicao = listaDadosItensFaturaExibicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura#getApuracao()
	 */
	@Override
	public ApuracaoVO getApuracao() {

		return apuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.fatura.
	 * DadosGeraisItensFatura
	 * #setApuracao(br.com.ggas
	 * .faturamento.apuracaopenalidade.ApuracaoVO)
	 */
	@Override
	public void setApuracao(ApuracaoVO apuracao) {

		this.apuracao = apuracao;
	}

	@Override
	public Date getDataVencimentoParaDataEmissaoInformada() {

		return dataVencimentoParaDataEmissaoInformada;
	}

	@Override
	public void setDataVencimentoParaDataEmissaoInformada(Date dataVencimentoParaDataEmissaoInformada) {

		this.dataVencimentoParaDataEmissaoInformada = dataVencimentoParaDataEmissaoInformada;
	}
}
