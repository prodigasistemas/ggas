/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.fatura;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosTributoVO;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * aos dados gerais dos itens da fatura
 *
 */
public interface DadosGeraisItensFatura extends Serializable {

	String BEAN_ID_DADOS_GERAIS_ITENS_FATURA = "dadosGeraisItensFatura";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the dataVencimento
	 */
	Date getDataVencimento();

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * @return the listaDadosItensFatura
	 */
	Collection<DadosItensFatura> getListaDadosItensFatura();

	/**
	 * @param listaDadosItensFatura
	 *            the listaDadosItensFatura to set
	 */
	void setListaDadosItensFatura(Collection<DadosItensFatura> listaDadosItensFatura);

	/**
	 * @return the indicadorProduto
	 */
	boolean isIndicadorProduto();

	/**
	 * @param indicadorProduto
	 *            the indicadorProduto to set
	 */
	void setIndicadorProduto(boolean indicadorProduto);

	/**
	 * @return the listaDadosFornecimentoGasVO
	 */
	Collection<DadosFornecimentoGasVO> getListaDadosFornecimentoGasVO();

	/**
	 * @param listaDadosFornecimentoGasVO
	 *            the listaDadosFornecimentoGasVO
	 *            to set
	 */
	void setListaDadosFornecimentoGasVO(Collection<DadosFornecimentoGasVO> listaDadosFornecimentoGasVO);

	/**
	 * @return the listaDadosTributoVO
	 */
	Collection<DadosTributoVO> getListaDadosTributoVO();

	/**
	 * @param listaDadosTributoVO
	 *            the listaDadosTributoVO to set
	 */
	void setListaDadosTributoVO(Collection<DadosTributoVO> listaDadosTributoVO);

	/**
	 * @return the valorTotalFatura
	 */
	BigDecimal getValorTotalFatura();

	/**
	 * @param valorTotalFatura
	 *            the valorTotalFatura to set
	 */
	void setValorTotalFatura(BigDecimal valorTotalFatura);

	/**
	 * @return the valorTotalDebitos
	 */
	BigDecimal getValorTotalDebitos();

	/**
	 * @param valorTotalDebitos
	 *            the valorTotalDebitos to set
	 */
	void setValorTotalDebitos(BigDecimal valorTotalDebitos);

	/**
	 * @return the dadosItensFatura
	 */
	public Collection<DadosItensFatura> getListaDadosItensFaturaExibicao();

	/**
	 * @param listaDadosItensFaturaExibicao
	 *            the
	 *            listaDadosItensFaturaExibicao to
	 *            set
	 */
	public void setListaDadosItensFaturaExibicao(Collection<DadosItensFatura> listaDadosItensFaturaExibicao);

	/**
	 * @return the apuracao
	 */
	public ApuracaoVO getApuracao();

	/**
	 * @param apuracao
	 *            the apuracao to set
	 */
	public void setApuracao(ApuracaoVO apuracao);

	/**
	 * Recupera DataVencimentoParaDataEmissaoInformada
	 * 
	 * @return DataVencimentoParaDataEmissaoInformada
	 */
	Date getDataVencimentoParaDataEmissaoInformada();

	/**
	 * Insere DataVencimentoParaDataEmissaoInformada
	 * 
	 * @param dataVencimentoParaDataEmissaoInformada
	 */
	void setDataVencimentoParaDataEmissaoInformada(Date dataVencimentoParaDataEmissaoInformada);

}
