/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.mensagem;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Mensagem de Faturamento
 *
 */
public interface ControladorMensagemFaturamento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_MENSAGEM_FATURAMENTO = "controladorMensagemFaturamento";

	String HABILITADO = "habilitado";

	/**
	 * @return a Classe da Entidade Mensagem
	 *         Faturamento
	 */
	Class<?> getClasseEntidadeMensagemFaturamento();

	/**
	 * @return a Classe da Entidade Mensagem
	 *         Faturamento
	 */
	Class<?> getClasseEntidadeMensagemCobranca();

	/**
	 * @return a Classe da Entidade Mensagem
	 *         Faturamento
	 */
	Class<?> getClasseEntidadeMensagemSegmento();

	/**
	 * @return a Classe da Entidade Mensagem
	 *         Faturamento
	 */
	Class<?> getClasseEntidadeMensagemVencimento();

	/**
	 * Método responsável por consultar as
	 * Mensagens de Faturamento de acordo
	 * com o filtro passado.
	 * 
	 * @param filtro
	 *            filtro a ser utilizado na
	 *            consulta
	 * @return coleção de Mensagens de Faturamento
	 * @throws GGASException
	 *             Caso ocorra algum erro durante
	 *             a execução do método
	 */
	Collection<MensagemFaturamento> consultarMensagemFaturamento(Map<String, Object> filtro) throws GGASException;

	/**
	 * Método responsável por criar uma instância
	 * da MensagemFaturamento.
	 * 
	 * @return a MensagemFaturamento criada
	 */
	MensagemFaturamento criarMensagemFaturamento();

	/**
	 * Método responsável por criar uma instância
	 * da MensagemSegmento.
	 * 
	 * @return a MensagemSegmento criada
	 */
	MensagemSegmento criarMensagemSegmento();

	/**
	 * Método responsável por criar uma instância
	 * da MensagemCobranca.
	 * 
	 * @return a MensagemCobranca criada
	 */
	MensagemCobranca criarMensagemCobranca();

	/**
	 * Método responsável por criar uma instância
	 * da MensagemVencimento.
	 * 
	 * @return a MensagemVencimento criada
	 */
	MensagemVencimento criarMensagemVencimento();

	/**
	 * Método responsável por inserir uma nova
	 * mensagemFaturamento.
	 * 
	 * @param mensagemFaturamento
	 *            a MensagemFaturamento a inserir
	 * @return chavePrimaria da
	 *         MensagemFaturamento que foi
	 *         inserida
	 * @throws NegocioException
	 *             Caso ocorra algum erro durante
	 *             a execução do método
	 */
	Long inserirMensagemFaturamento(MensagemFaturamento mensagemFaturamento) throws NegocioException;

	/**
	 * método responsável por remover uma Mensagem
	 * de Faturamento.
	 * 
	 * @param chavesPrimarias
	 *            chaves primárias das mensagens
	 *            de faturamento que serão
	 *            removidas
	 * @param dadosAuditoria
	 *            dados de Auditoria
	 * @throws NegocioException
	 *             Caso ocorra alguma erro durante
	 *             a invocação do método
	 */
	void removerMensagemFaturamento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por alterar uma Mensagem
	 * de Faturamento.
	 * 
	 * @param mensagemFaturamento
	 *            mensagem de faturamento para
	 *            alterar
	 * @throws ConcorrenciaException
	 *             caso ocorra algum erro durante
	 *             a invocação do método
	 * @throws NegocioException
	 *             caso ocorra algum erro durante
	 *             a invocação do método
	 */
	void alterarMensagemFaturamento(MensagemFaturamento mensagemFaturamento) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por consultar a mensagem
	 * vigente pertencente a um tipo e um
	 * vencimento.
	 * 
	 * @param dtVencimento
	 *            data de vencimento
	 * @param chaveTipoMensagem
	 *            identificador do tipo de
	 *            mensagem
	 * @return MensagemFaturamento encontrada
	 */
	MensagemFaturamento consultarMensagemFaturamentoVigentePorVencimentos(Date dtVencimento, long chaveTipoMensagem);

	/**
	 * Método responsável por consultar a mensagem vigente pertencente a tipos de mensagem e um segmento.
	 * 
	 * @param chavesTipoMensagem - {@link Long[]}
	 * @param chaveSegmento - {@link Long}
	 * @return mensagem por tipo - {@link Map}
	 */
	Map<Long, Collection<MensagemFaturamento>> consultarMensagemFaturamentoVigenteDeSegmentoPorTipo(
			Long[] chavesTipoMensagem, Long chaveSegmento);

	/**
	 * Consultar mensagem segmento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the list
	 */
	List<MensagemSegmento> consultarMensagemSegmento(Map<String, Object> filtro);

	/**
	 * Método responsável por consultar a mensagem vigente pertencente a tipos de mensagem e um imóvel.
	 * 
	 * @param chavesTipoMensagem - {@link Long[]}
	 * @param chaveImovel - {@link Long}
	 * @return mensagem por tipo - {@link Map}
	 */
	Map<Long, Collection<MensagemFaturamento>> consultarMensagemFaturamentoVigenteDeImovelPorTipo(
			Long[] chavesTipoMensagem, Long chaveImovel);

	/**
	 * Método responsável por consultar a mensagem vigente pertencente a tipos de mensagem.
	 * 
	 * @param dtVencimento - {@link Date}
	 * @param chavesTipoMensagem - {@link Long[]}
	 * @return mensagem por tipo - {@link Map}
	 */
	Map<Long, Collection<MensagemFaturamento>> consultarMensagemFaturamentoVigentePorVencimentos(Date dtVencimento,
			Long[] chavesTipoMensagem);

}
