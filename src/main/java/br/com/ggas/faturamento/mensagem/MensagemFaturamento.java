/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.mensagem;

import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * MensagemFaturamento
 * 
 */
public interface MensagemFaturamento extends EntidadeNegocio {

	String BEAN_ID_MENSAGEM_FATURAMENTO = "mensagemFaturamento";

	String MENSAGEM_FATURAMENTO = "MENSAGEM_FATURAMENTO";

	String MENSAGENS_FATURAMENTO = "MENSAGENS_FATURAMENTO";

	String TIPO_MENSAGEM = "MENSAGEM_FATURAMENTO_TIPO_MENSAGEM";

	String IMOVEL = "MENSAGEM_FATURAMENTO_IMOVEL";

	String DESCRICAO = "MENSAGEM_FATURAMENTO_DESCRICAO";

	String INICIO_VIGENCIA = "MENSAGEM_FATURAMENTO_INICIO_VIGENCIA";

	String FIM_VIGENCIA = "MENSAGEM_FATURAMENTO_FIM_VIGENCIA";

	/**
	 * @return TipoMensagem
	 */
	EntidadeConteudo getTipoMensagem();

	/**
	 * @param tipoMensagem
	 *            novo TipoMensagem
	 */
	void setTipoMensagem(EntidadeConteudo tipoMensagem);

	/**
	 * @return Imovel
	 */
	Imovel getImovel();

	/**
	 * @param imovel
	 *            novo Imovel
	 */
	void setImovel(Imovel imovel);

	/**
	 * @return descrição
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            nova descrição
	 */
	void setDescricao(String descricao);

	/**
	 * @return início vigência
	 */
	Date getDataInicioVigencia();

	/**
	 * @param dataInicioVigencia
	 *            novo início de vigência
	 */
	void setDataInicioVigencia(Date dataInicioVigencia);

	/**
	 * @return fim vigência
	 */
	Date getDataFimVigencia();

	/**
	 * @param dataFimVigencia
	 *            novo fim de vigência
	 */
	void setDataFimVigencia(Date dataFimVigencia);

	/**
	 * @return lista Segmentos
	 */
	Set<MensagemSegmento> getListaSegmentos();

	/**
	 * @param listaSegmentos
	 *            nova ista Segmentos
	 */
	void setListaSegmentos(Set<MensagemSegmento> listaSegmentos);

	/**
	 * @return lista cobranças
	 */
	Set<MensagemCobranca> getListaFormasCobranca();

	/**
	 * @param listaFormasCobranca
	 *            nova lista cobranças
	 */
	void setListaFormasCobranca(Set<MensagemCobranca> listaFormasCobranca);

	/**
	 * @return nova lista vencimentos
	 */
	Set<MensagemVencimento> getListaVencimentos();

	/**
	 * @param listaVencimentos
	 *            lista vencimentos
	 */
	void setListaVencimentos(Set<MensagemVencimento> listaVencimentos);

	/**
	 * @return data de início da vigência
	 *         formatada para exibição
	 */
	String getDataInicioVigenciaFormatada();

	/**
	 * @return data de fim da vigência formatada
	 *         para exibição
	 */
	String getDataFimVigenciaFormatada();

	/**
	 * @return retorna todos os segmentos da
	 *         mensagem faturamento
	 */
	Set<Segmento> getListaNaoRepetidaSegmentos();

	/**
	 * @return retorna todos os ramos de atividade
	 *         da mensagem faturamento
	 */
	Set<RamoAtividade> getListaNaoRepetidaRamosAtividade();

	/**
	 * @return retorna todos as formas de cobrança
	 *         da mensagem faturamento
	 */
	Set<EntidadeConteudo> getListaNaoRepetidaFormasCobranca();

	/**
	 * @return retorna todos as datas de
	 *         vencimento da mensagem faturamento
	 */
	Set<Integer> getListaNaoRepetidaDatasVencimento();

	/**
	 * @return Lista ListMensagemVencimentoOrderDiaVencimento
	 */
	List<MensagemVencimento> getListMensagemVencimentoOrderDiaVencimento();

	/**
	 * @return retorna lista ListaSegmentosOrderPorRamoAtividadeDescricao
	 */
	List<MensagemSegmento> getListaSegmentosOrderPorRamoAtivdadeDescricao();

	/**
	 * @return retorna uma lista de SegmentosOrderDescricao
	 */
	List<Segmento> getListaSegmentosOrderDescricao();

	/**
	 * @return Retorna uma lista de ListaRamosAtividadeOrderDescricao
	 */
	List<RamoAtividade> getListaRamosAtividadeOrderDescricao();
}
