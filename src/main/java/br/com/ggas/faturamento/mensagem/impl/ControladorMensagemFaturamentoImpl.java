/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe MensagemFaturamento representa uma Mensagem a ser usada nas faturas no sistema.
 *
 * @since 10/07/2010
 * @author hsilva
 */

package br.com.ggas.faturamento.mensagem.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.mensagem.ControladorMensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemCobranca;
import br.com.ggas.faturamento.mensagem.MensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemSegmento;
import br.com.ggas.faturamento.mensagem.MensagemVencimento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 *
 */
class ControladorMensagemFaturamentoImpl extends ControladorNegocioImpl implements ControladorMensagemFaturamento {
	
	private static final Logger LOG = Logger.getLogger(ControladorMensagemFaturamentoImpl.class);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(MensagemFaturamento.BEAN_ID_MENSAGEM_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public MensagemFaturamento criarMensagemFaturamento() {

		return (MensagemFaturamento) ServiceLocator.getInstancia().getBeanPorID(MensagemFaturamento.BEAN_ID_MENSAGEM_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #criarMensagemSegmento()
	 */
	@Override
	public MensagemSegmento criarMensagemSegmento() {

		return (MensagemSegmento) ServiceLocator.getInstancia().getBeanPorID(MensagemSegmento.BEAN_ID_MENSAGEM_SEGMENTO_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #criarMensagemCobranca()
	 */
	@Override
	public MensagemCobranca criarMensagemCobranca() {

		return (MensagemCobranca) ServiceLocator.getInstancia().getBeanPorID(MensagemCobranca.BEAN_ID_MENSAGEM_COBRANCA_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #criarMensagemVencimento()
	 */
	@Override
	public MensagemVencimento criarMensagemVencimento() {

		return (MensagemVencimento) ServiceLocator.getInstancia().getBeanPorID(MensagemVencimento.BEAN_ID_MENSAGEM_VENCIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(MensagemFaturamento.BEAN_ID_MENSAGEM_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #getClasseEntidadeMensagemFaturamento()
	 */
	@Override
	public Class<?> getClasseEntidadeMensagemFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(MensagemFaturamento.BEAN_ID_MENSAGEM_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #getClasseEntidadeMensagemCobranca()
	 */
	@Override
	public Class<?> getClasseEntidadeMensagemCobranca() {

		return ServiceLocator.getInstancia().getClassPorID(MensagemCobranca.BEAN_ID_MENSAGEM_COBRANCA_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #getClasseEntidadeMensagemSegmento()
	 */
	@Override
	public Class<?> getClasseEntidadeMensagemSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(MensagemSegmento.BEAN_ID_MENSAGEM_SEGMENTO_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #getClasseEntidadeMensagemVencimento()
	 */
	@Override
	public Class<?> getClasseEntidadeMensagemVencimento() {

		return ServiceLocator.getInstancia().getClassPorID(MensagemVencimento.BEAN_ID_MENSAGEM_VENCIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #consultarMensagemFaturamento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<MensagemFaturamento> consultarMensagemFaturamento(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			Long tipoMensagem = (Long) filtro.get("tipoMensagem");
			if(tipoMensagem != null && tipoMensagem > 0) {
				criteria.createAlias("tipoMensagem", "tipoMensagem");
				criteria.add(Restrictions.eq("tipoMensagem.chavePrimaria", tipoMensagem));
			} else {
				criteria.setFetchMode("tipoMensagem", FetchMode.JOIN);
			}

			Date dtInicio = (Date) filtro.get("dataInicioVigencia");
			if(dtInicio != null) {
				criteria.add(Restrictions.ge("dataInicioVigencia", dtInicio));
			}

			Date dtFim = (Date) filtro.get("dataFinalVigencia");
			if(dtFim != null) {
				criteria.add(Restrictions.le("dataFimVigencia", dtFim));
			}

			criteria.createAlias("listaSegmentos", "listaSegmentos", CriteriaSpecification.LEFT_JOIN);
			criteria.createAlias("listaSegmentos.segmento", "segmento", CriteriaSpecification.LEFT_JOIN);

			Long segmento = (Long) filtro.get("segmento");
			if(segmento != null && segmento > 0) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", segmento));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
		}

		criteria.addOrder(Order.desc("dataFimVigencia"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.ControladorMensagemFaturamento#consultarMensagemSegmento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<MensagemSegmento> consultarMensagemSegmento(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeMensagemSegmento());

		if(filtro != null) {

			Long idRamoAtividade = (Long) filtro.get("ramoAtividade");
			if(idRamoAtividade != null && idRamoAtividade > 0) {
				criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #inserirMensagemFaturamento
	 * (br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento)
	 */
	@Override
	public Long inserirMensagemFaturamento(MensagemFaturamento mensagemFaturamento) throws NegocioException {

		if(mensagemFaturamento != null) {
			validarDatas(mensagemFaturamento);

			mensagemFaturamento.setHabilitado(true);
			mensagemFaturamento.setUltimaAlteracao(Calendar.getInstance().getTime());

			if(mensagemFaturamento.getListaFormasCobranca() != null && !mensagemFaturamento.getListaFormasCobranca().isEmpty()) {
				for (MensagemCobranca mensagemCobranca : mensagemFaturamento.getListaFormasCobranca()) {
					mensagemCobranca.setDadosAuditoria(mensagemFaturamento.getDadosAuditoria());
					mensagemCobranca.setHabilitado(true);
					mensagemCobranca.setUltimaAlteracao(Calendar.getInstance().getTime());
				}
			}

			if(mensagemFaturamento.getListaSegmentos() != null && !mensagemFaturamento.getListaSegmentos().isEmpty()) {
				for (MensagemSegmento mensagemSegmento : mensagemFaturamento.getListaSegmentos()) {
					mensagemSegmento.setDadosAuditoria(mensagemFaturamento.getDadosAuditoria());
					mensagemSegmento.setHabilitado(true);
					mensagemSegmento.setUltimaAlteracao(Calendar.getInstance().getTime());
				}
			}

			if(mensagemFaturamento.getListaVencimentos() != null) {
				for (MensagemVencimento mensagemVencimento : mensagemFaturamento.getListaVencimentos()) {
					mensagemVencimento.setDadosAuditoria(mensagemFaturamento.getDadosAuditoria());
					mensagemVencimento.setHabilitado(true);
					mensagemVencimento.setUltimaAlteracao(Calendar.getInstance().getTime());
				}
			}
		}
		if(mensagemFaturamento != null) {
			return this.inserir(mensagemFaturamento);
		}
		return null;

	}

	/**
	 * @param mensagemFaturamento
	 * @throws NegocioException
	 */
	private void validarDatas(MensagemFaturamento mensagemFaturamento) throws NegocioException {
		if(mensagemFaturamento.getDataInicioVigencia() != null && mensagemFaturamento.getDataFimVigencia() != null) {
			try {
				Util.validarIntervaloDatas(mensagemFaturamento.getDataInicioVigencia(), mensagemFaturamento.getDataFimVigencia());
			} catch(GGASException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
			}
			Date dataInicioVigencia = mensagemFaturamento.getDataInicioVigencia();
			if(DataUtil.antesHoje(dataInicioVigencia)) {
				if(mensagemFaturamento.getChavePrimaria() > 0) {
					MensagemFaturamento mensagemFaturamentoBanco = (MensagemFaturamento) this.obter(mensagemFaturamento.getChavePrimaria());
					Date dataInicioVigenciaBanco = mensagemFaturamentoBanco.getDataInicioVigencia();
					if(dataInicioVigencia.compareTo(dataInicioVigenciaBanco) != 0) {
						throw criarExcecaoDataInicioVigencia(dataInicioVigencia);
					}
				} else {
					throw criarExcecaoDataInicioVigencia(dataInicioVigencia);
				}
			}
			Date dataFimVigencia = mensagemFaturamento.getDataFimVigencia();
			if(DataUtil.antesHoje(dataFimVigencia)) {
				throw criarExcecaoDataInicioVigencia(dataFimVigencia);
			}
		}
	}

	/**
	 * @param dataInicioVigencia
	 * @return excecao data inicio vigencia
	 */
	private NegocioException criarExcecaoDataInicioVigencia(Date dataInicioVigencia) {
		Map<String, Object> erros = new HashMap<String, Object>();
		String dataInicioVigenciaStr = DataUtil.converterDataParaString(dataInicioVigencia);
		erros.put(Constantes.ERRO_NEGOCIO_DATA_ANTERIOR_HOJE_MSG_INCLUSIVA, dataInicioVigenciaStr);
		return new NegocioException(erros);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #removerMensagemFaturamento(java.lang.Long[]
	 * ,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerMensagemFaturamento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put("chavesPrimarias", chavesPrimarias);

			Collection<MensagemFaturamento> listaMensagemFaturamentos = this.consultarMensagemFaturamento(filtro);

			if((listaMensagemFaturamentos != null) && (!listaMensagemFaturamentos.isEmpty())) {

				for (MensagemFaturamento mensagemFaturamento : listaMensagemFaturamentos) {
					mensagemFaturamento.setDadosAuditoria(dadosAuditoria);

					boolean isExclusaoFisica = false;

					if(Util.compararDatas(mensagemFaturamento.getDataInicioVigencia(), Calendar.getInstance().getTime()) > 0) {
						isExclusaoFisica = true;
					}
					super.remover(mensagemFaturamento, isExclusaoFisica);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento
	 * #alterarMensagemFaturamento
	 * (br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento)
	 */
	@Override
	public void alterarMensagemFaturamento(MensagemFaturamento mensagemFaturamento) throws ConcorrenciaException, NegocioException {

		if(mensagemFaturamento != null) {
			validarDatas(mensagemFaturamento);

			if(mensagemFaturamento.getDataInicioVigencia() != null
							&& Util.compararDatas(mensagemFaturamento.getDataInicioVigencia(), Calendar.getInstance().getTime()) > 0) {
				mensagemFaturamento.setHabilitado(Boolean.TRUE);
			}

			this.atualizar(mensagemFaturamento);
		}

	}

	/**
	 * Método responsável por consultar a mensagem vigente pertencente a tipos de mensagem e um imóvel.
	 * 
	 * @param listaTipoMensagem - {@link Collection}
	 * @param chaveImovel - {@link Long}
	 * @return mensagem por tipo - {@link Map}
	 */
	@Override
	public Map<Long, Collection<MensagemFaturamento>> consultarMensagemFaturamentoVigenteDeImovelPorTipo(
			Long[] chavesTipoMensagem, Long chaveImovel) {

		Map<Long, Collection<MensagemFaturamento>> mensagemImovelPorTipo = null;

		if (chavesTipoMensagem != null && chavesTipoMensagem.length > 0) {
			StringBuilder hql = new StringBuilder();

			hql.append(" select tipoMensagem.chavePrimaria as tipoMensagem_chavePrimaria, ");
			hql.append(" mensagem.descricao as descricao ");
			hql.append(" from ");
			hql.append(getClasseEntidadeMensagemFaturamento().getSimpleName());
			hql.append(" mensagem ");
			hql.append(" inner join mensagem.imovel as imovel ");
			hql.append(" inner join mensagem.tipoMensagem as tipoMensagem ");
			hql.append(" where ");
			hql.append(" tipoMensagem.chavePrimaria IN (:chavesPrimarias) ");
			hql.append(" and imovel.chavePrimaria = :chaveImovel ");
			hql.append(" and :dataAtual between mensagem.dataInicioVigencia and mensagem.dataFimVigencia ");
			hql.append(" and mensagem.habilitado is true ");

			Query query = super.getSession().createQuery(hql.toString());
			query.setResultTransformer(new GGASTransformer(getClasseEntidadeMensagemFaturamento(),
					super.getSessionFactory().getAllClassMetadata(), "tipoMensagem_chavePrimaria"));
			query.setLong("chaveImovel", chaveImovel);
			query.setParameterList(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesTipoMensagem);
			query.setDate("dataAtual", Calendar.getInstance().getTime());

			mensagemImovelPorTipo = (Map<Long, Collection<MensagemFaturamento>>) query.uniqueResult();
		}

		return mensagemImovelPorTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento#
	 * consultarMensagemFaturamentoVigentePorVencimentos
	 * (java.util.Date, long)
	 */
	@Override
	public MensagemFaturamento consultarMensagemFaturamentoVigentePorVencimentos(Date dtVencimento, long chaveTipoMensagem) {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" select mensagem.descricao as descricao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeMensagemFaturamento().getSimpleName());
		hql.append(" mensagem ");
		hql.append(" inner join mensagem.listaVencimentos listaVencimentos ");
		hql.append(" inner join mensagem.tipoMensagem tipoMensagem ");
		hql.append(" where ");
		hql.append(" listaVencimentos.diaVencimento = :dtVencimento ");
		hql.append(" and tipoMensagem.chavePrimaria = :chaveTipoMensagem ");
		hql.append(" and :dataAtual between mensagem.dataInicioVigencia and mensagem.dataFimVigencia ");
		hql.append(" and mensagem.habilitado is true ");
		hql.append(" and mensagem.ultimaAlteracao = (select max(msg.ultimaAlteracao)");
		hql.append(" from ");
		hql.append(getClasseEntidadeMensagemFaturamento().getSimpleName());
		hql.append(" msg ");
		hql.append(" inner join msg.listaVencimentos listaVencimentosMsg ");
		hql.append(" inner join msg.tipoMensagem tipoMensagemMsg ");
		hql.append(" where ");
		hql.append(" listaVencimentosMsg.diaVencimento = :dtVencimento ");
		hql.append(" and tipoMensagemMsg.chavePrimaria = :chaveTipoMensagem ");
		hql.append(" and :dataAtual between msg.dataInicioVigencia and msg.dataFimVigencia ");
		hql.append(" and msg.habilitado is true ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		query.setInteger("dtVencimento", new DateTime(dtVencimento).getDayOfMonth());
		query.setLong("chaveTipoMensagem", chaveTipoMensagem);
		query.setDate("dataAtual", Calendar.getInstance().getTime());
		return (MensagemFaturamento) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.
	 * ControladorMensagemFaturamento#
	 * consultarMensagemFaturamentoVigentePorVencimentos
	 * (java.util.Date, long)
	 */
	@Override
	public Map<Long, Collection<MensagemFaturamento>> consultarMensagemFaturamentoVigentePorVencimentos(
			Date dtVencimento, Long[] chavesTipoMensagem) {

		Map<Long, Collection<MensagemFaturamento>> mensagemVigentePorTipo = null;

		
		if (chavesTipoMensagem != null && chavesTipoMensagem.length > 0) { 
			StringBuilder hql = new StringBuilder();

			hql.append(" select tipoMensagem.chavePrimaria as tipoMensagem_chavePrimaria, ");
			hql.append(" mensagem.descricao as descricao ");
			hql.append(" from ");
			hql.append(getClasseEntidadeMensagemFaturamento().getSimpleName());
			hql.append(" mensagem ");
			hql.append(" inner join mensagem.listaVencimentos listaVencimentos ");
			hql.append(" inner join mensagem.tipoMensagem tipoMensagem ");
			hql.append(" where ");
			hql.append(" listaVencimentos.diaVencimento = :dtVencimento ");
			hql.append(" and tipoMensagem.chavePrimaria in (:chavesPrimarias) ");
			hql.append(" and :dataAtual between mensagem.dataInicioVigencia and mensagem.dataFimVigencia ");
			hql.append(" and mensagem.habilitado is true ");
			hql.append(" and mensagem.ultimaAlteracao = (select max(msg.ultimaAlteracao)");
			hql.append(" from ");
			hql.append(getClasseEntidadeMensagemFaturamento().getSimpleName());
			hql.append(" msg ");
			hql.append(" inner join msg.listaVencimentos listaVencimentosMsg ");
			hql.append(" inner join msg.tipoMensagem tipoMensagemMsg ");
			hql.append(" where ");
			hql.append(" listaVencimentosMsg.diaVencimento = :dtVencimento ");
			hql.append(" and tipoMensagemMsg.chavePrimaria in (:chavesPrimarias) ");
			hql.append(" and :dataAtual between msg.dataInicioVigencia and msg.dataFimVigencia ");
			hql.append(" and msg.habilitado is true ");
			hql.append(" ) ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setResultTransformer(new GGASTransformer(getClasseEntidadeMensagemFaturamento(),
					super.getSessionFactory().getAllClassMetadata(), "tipoMensagem_chavePrimaria"));
			query.setInteger("dtVencimento", new DateTime(dtVencimento).getDayOfMonth());
			query.setParameterList(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesTipoMensagem);
			query.setDate("dataAtual", Calendar.getInstance().getTime());
			mensagemVigentePorTipo = (Map<Long, Collection<MensagemFaturamento>>) query.setMaxResults(1).uniqueResult();
		}
		
		
		return mensagemVigentePorTipo;
	}

	
	/**
	 * Método responsável por consultar a mensagem vigente pertencente a tipos de mensagem e um segmento.
	 * 
	 * @param chavesTipoMensagem - {@link Long[]}
	 * @param chaveSegmento - {@link Long}
	 * @return mensagem por tipo - {@link Map}
	 */
	@Override
	public Map<Long, Collection<MensagemFaturamento>> consultarMensagemFaturamentoVigenteDeSegmentoPorTipo(
			Long[] chavesTipoMensagem, Long chaveSegmento) {

		Map<Long, Collection<MensagemFaturamento>> mensagemSegmentoPorTipo = null;

		if (chavesTipoMensagem != null && chavesTipoMensagem.length > 0) {
			StringBuilder hql = new StringBuilder();

			hql.append(" select tipoMensagem.chavePrimaria as tipoMensagem_chavePrimaria, ");
			hql.append(" mensagem.descricao as descricao ");
			hql.append(" from ");
			hql.append(getClasseEntidadeMensagemFaturamento().getSimpleName());
			hql.append(" mensagem ");
			hql.append(" inner join mensagem.listaSegmentos listaSegmentos ");
			hql.append(" inner join mensagem.tipoMensagem tipoMensagem ");
			hql.append(" where ");
			hql.append(" listaSegmentos.segmento.chavePrimaria = :chaveSegmento ");
			hql.append(" and tipoMensagem.chavePrimaria IN (:chavesPrimarias) ");
			hql.append(" and :dataAtual between mensagem.dataInicioVigencia and mensagem.dataFimVigencia ");
			hql.append(" and mensagem.habilitado is true ");

			Query query = super.getSession().createQuery(hql.toString());
			query.setResultTransformer(new GGASTransformer(getClasseEntidadeMensagemFaturamento(),
					super.getSessionFactory().getAllClassMetadata(), "tipoMensagem_chavePrimaria"));
			query.setLong("chaveSegmento", chaveSegmento);
			query.setParameterList(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesTipoMensagem);
			query.setDate("dataAtual", Calendar.getInstance().getTime());
			mensagemSegmentoPorTipo = (Map<Long, Collection<MensagemFaturamento>>) query.uniqueResult();
		}

		return mensagemSegmentoPorTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#obter(long,
	 * java.lang.String[])
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return obter(chavePrimaria, getClasseEntidade(), Boolean.TRUE, propriedadesLazy);
	}

}
