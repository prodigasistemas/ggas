/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.faturamento.mensagem.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.mensagem.MensagemCobranca;
import br.com.ggas.faturamento.mensagem.MensagemFaturamento;
import br.com.ggas.faturamento.mensagem.MensagemSegmento;
import br.com.ggas.faturamento.mensagem.MensagemVencimento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

class MensagemFaturamentoImpl extends EntidadeNegocioImpl implements MensagemFaturamento {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -853539663893071203L;

	private EntidadeConteudo tipoMensagem;

	private Imovel imovel;

	private String descricao;

	private Date dataInicioVigencia;

	private Date dataFimVigencia;

	private Set<MensagemSegmento> listaSegmentos;

	private Set<MensagemCobranca> listaFormasCobranca;

	private Set<MensagemVencimento> listaVencimentos;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#getTipoMensagem
	 * ()
	 */
	@Override
	public EntidadeConteudo getTipoMensagem() {

		return tipoMensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#setTipoMensagem
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoMensagem(EntidadeConteudo tipoMensagem) {

		this.tipoMensagem = tipoMensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#setImovel
	 * (br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#getDescricao
	 * ()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#setDescricao
	 * (java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getDataInicioVigencia()
	 */
	@Override
	public Date getDataInicioVigencia() {

		return dataInicioVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * setDataInicioVigencia(java.util.Date)
	 */
	@Override
	public void setDataInicioVigencia(Date dataInicioVigencia) {

		this.dataInicioVigencia = dataInicioVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getDataFimVigencia()
	 */
	@Override
	public Date getDataFimVigencia() {

		return dataFimVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * setDataFimVigencia(java.util.Date)
	 */
	@Override
	public void setDataFimVigencia(Date dataFimVigencia) {

		this.dataFimVigencia = dataFimVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaSegmentos()
	 */
	@Override
	public Set<MensagemSegmento> getListaSegmentos() {

		return listaSegmentos;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * setListaSegmentos(java.util.Collection)
	 */
	@Override
	public void setListaSegmentos(Set<MensagemSegmento> listaSegmentos) {

		this.listaSegmentos = listaSegmentos;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaFormasCobranca()
	 */
	@Override
	public Set<MensagemCobranca> getListaFormasCobranca() {

		return listaFormasCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * setListaFormasCobranca(java.util.Collection)
	 */
	@Override
	public void setListaFormasCobranca(Set<MensagemCobranca> listaFormasCobranca) {

		this.listaFormasCobranca = listaFormasCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaVencimentos()
	 */
	@Override
	public Set<MensagemVencimento> getListaVencimentos() {

		return listaVencimentos;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * setListaVencimentos(java.util.Collection)
	 */
	@Override
	public void setListaVencimentos(Set<MensagemVencimento> listaVencimentos) {

		this.listaVencimentos = listaVencimentos;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getDataInicioVigenciaFormatada()
	 */
	@Override
	public String getDataInicioVigenciaFormatada() {

		return Util.converterDataParaStringSemHora(dataInicioVigencia, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getDataFimVigenciaFormatada()
	 */
	@Override
	public String getDataFimVigenciaFormatada() {

		return Util.converterDataParaStringSemHora(dataFimVigencia, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaNaoRepetidaSegmentos()
	 */
	@Override
	public Set<Segmento> getListaNaoRepetidaSegmentos() {

		Set<Segmento> retorno = new HashSet<Segmento>();
		for (MensagemSegmento mensagemSegmento : listaSegmentos) {
			retorno.add(mensagemSegmento.getSegmento());
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaNaoRepetidaRamosAtividade()
	 */
	@Override
	public Set<RamoAtividade> getListaNaoRepetidaRamosAtividade() {

		Set<RamoAtividade> retorno = new HashSet<RamoAtividade>();
		for (MensagemSegmento mensagemSegmento : listaSegmentos) {
			if(mensagemSegmento.getRamoAtividade() != null) {
				retorno.add(mensagemSegmento.getRamoAtividade());
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaNaoRepetidaFormasCobranca()
	 */
	@Override
	public Set<EntidadeConteudo> getListaNaoRepetidaFormasCobranca() {

		Set<EntidadeConteudo> retorno = new HashSet<EntidadeConteudo>();
		for (MensagemCobranca mensagemCobranca : listaFormasCobranca) {
			retorno.add(mensagemCobranca.getFormaCobranca());
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.mensagem.
	 * MensagemFaturamento#
	 * getListaNaoRepetidaDatasVencimento()
	 */
	@Override
	public Set<Integer> getListaNaoRepetidaDatasVencimento() {

		Set<Integer> retorno = new HashSet<Integer>();
		for (MensagemVencimento mensagemVencimento : listaVencimentos) {
			retorno.add(mensagemVencimento.getDiaVencimento());
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados
	 * ()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		String camposObrigatorios = null;

		if(tipoMensagem == null) {
			sb.append(TIPO_MENSAGEM);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricao)) {
			sb.append(DESCRICAO);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataInicioVigencia == null) {
			sb.append(INICIO_VIGENCIA);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataFimVigencia == null) {
			sb.append(FIM_VIGENCIA);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = sb.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, sb.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;

	}

	@Override
	public List<MensagemVencimento> getListMensagemVencimentoOrderDiaVencimento() {

		if(this.getListaVencimentos() != null) {
			List<MensagemVencimento> listaMensagens = new ArrayList<MensagemVencimento>();

			listaMensagens.addAll(this.getListaVencimentos());

			Collections.sort(listaMensagens, new Comparator<MensagemVencimento>(){

				@Override
				public int compare(MensagemVencimento o1, MensagemVencimento o2) {

					return o1.getDiaVencimento().compareTo(o2.getDiaVencimento());
				}
			});
			return listaMensagens;
		}
		return null;
	}

	@Override
	public List<MensagemSegmento> getListaSegmentosOrderPorRamoAtivdadeDescricao() {

		if(this.getListaSegmentos() != null) {
			List<MensagemSegmento> listaMsg = new ArrayList<MensagemSegmento>();

			for (MensagemSegmento mensagemSegmento : this.getListaSegmentos()) {
				if(mensagemSegmento.getRamoAtividade() != null && !mensagemSegmento.getRamoAtividade().getDescricao().isEmpty()) {
					listaMsg.add(mensagemSegmento);
				}
			}

			if(listaMsg.size() > 1) {
				Collections.sort(listaMsg, new Comparator<MensagemSegmento>(){

					@Override
					public int compare(MensagemSegmento o1, MensagemSegmento o2) {

						return o1.getRamoAtividade().getDescricao().compareToIgnoreCase(o2.getRamoAtividade().getDescricao());
					}
				});
			}
			return listaMsg;
		}
		return null;
	}

	@Override
	public List<Segmento> getListaSegmentosOrderDescricao() {

		List<Segmento> listaSegmento = new ArrayList<Segmento>();

		for (Segmento segmento : this.getListaNaoRepetidaSegmentos()) {
			if (!segmento.getDescricao().isEmpty()) {
				listaSegmento.add(segmento);
			}
		}

		if (listaSegmento.size() > 1) {
			Collections.sort(listaSegmento, new Comparator<Segmento>() {

				@Override
				public int compare(Segmento o1, Segmento o2) {

					return o1.getDescricao().compareToIgnoreCase(o2.getDescricao());
				}
			});
		}
		return listaSegmento;
	}

	@Override
	public List<RamoAtividade> getListaRamosAtividadeOrderDescricao() {

		List<MensagemSegmento> listaMsgSegmento = this.getListaSegmentosOrderPorRamoAtivdadeDescricao();

		List<RamoAtividade> listaRetorno = new ArrayList<RamoAtividade>();
		for (MensagemSegmento msgSegmento : listaMsgSegmento) {
			listaRetorno.add(msgSegmento.getRamoAtividade());
		}
		return listaRetorno;
	}

}
