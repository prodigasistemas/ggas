/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.mensagem.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.mensagem.MensagemCobranca;
import br.com.ggas.faturamento.mensagem.MensagemFaturamento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 * 
 */
class MensagemCobrancaImpl extends EntidadeNegocioImpl implements MensagemCobranca {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2430726046146222456L;

	private MensagemFaturamento mensagemFaturamento;

	private EntidadeConteudo formaCobranca;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.
	 * MensagemCobrancaFaturamento
	 * #getMensagemFaturamento()
	 */
	@Override
	public MensagemFaturamento getMensagemFaturamento() {

		return mensagemFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.
	 * MensagemCobrancaFaturamento
	 * #setMensagemFaturamento
	 * (br.com.ggas.faturamento
	 * .mensagem.MensagemFaturamento)
	 */
	@Override
	public void setMensagemFaturamento(MensagemFaturamento mensagemFaturamento) {

		this.mensagemFaturamento = mensagemFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.
	 * MensagemCobrancaFaturamento
	 * #getFormaCobranca()
	 */
	@Override
	public EntidadeConteudo getFormaCobranca() {

		return formaCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.mensagem.
	 * MensagemCobrancaFaturamento
	 * #setFormaCobranca
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setFormaCobranca(EntidadeConteudo formaCobranca) {

		this.formaCobranca = formaCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados
	 * ()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		String camposObrigatorios = null;

		if(mensagemFaturamento == null) {
			sb.append(MENSAGEM_FATURAMENTO);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(formaCobranca == null) {
			sb.append(FORMA_COBRANCA);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		camposObrigatorios = sb.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, sb.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
