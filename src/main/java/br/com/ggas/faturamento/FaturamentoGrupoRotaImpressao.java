
package br.com.ggas.faturamento;

import java.util.Date;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.rota.Rota;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a impressão do faturamento de grupo em rota especifica
 *
 */
public interface FaturamentoGrupoRotaImpressao extends EntidadeNegocio {

	String BEAN_ID_FATURAMENTO_GRUPO_ROTA_IMPRESSAO = "faturamentoGrupoRotaImpressao";

	/**
	 * @return GrupoFaturamento - Set grupo faturamento
	 */
	GrupoFaturamento getGrupoFaturamento();

	/**
	 * @param grupoFaturamento - Set grupo faturamento.
	 */
	void setGrupoFaturamento(GrupoFaturamento grupoFaturamento);

	/**
	 * @return Rota - Retorna rota.
	 */
	Rota getRota();

	/**
	 * @param rota - Set rota.
	 */
	void setRota(Rota rota);

	/**
	 * @return Integer - Retorna Ano com Mês da referência.
	 */
	Integer getAnoMesReferencia();

	/**
	 * @param anoMesReferencia - set anos com mês da referência.
	 */
	void setAnoMesReferencia(Integer anoMesReferencia);

	/**
	 * @return Integer - retorna sequência de impressão.
	 */
	Integer getSeqImpressao();

	/**
	 * @param seqImpressao - Set sequência de immpressão.
	 */
	void setSeqImpressao(Integer seqImpressao);

	/**
	 * @return Date - Retorna última data de impressão.
	 */
	Date getDtUltimaImpressao();

	/**
	 * @param dtUltimaImpressao - Set última data de impressão.
	 */
	void setDtUltimaImpressao(Date dtUltimaImpressao);

	/**
	 * @return Date - Retorna data impressão.
	 */
	Date getDtImpressao();

	/**
	 * @param dtImpressao - Set data impress'ao.
	 */
	void setDtImpressao(Date dtImpressao);

	/**
	 * @return Integer - Retorna um ciclo.
	 */
	Integer getCiclo();

	/**
	 * @param ciclo - Set ciclo.
	 */
	void setCiclo(Integer ciclo);

	/**
	 * @return Usuario - Retorna Usuário.
	 */
	Usuario getUsuario();

	/**
	 * @param usuario - Set usuário. 
	 */
	void setUsuario(Usuario usuario);

}
