/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 27/06/2013 15:45:30
 @author asoares
 */

package br.com.ggas.faturamento.serie.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.faturamento.serie.ControladorSerie;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * 
 * ControladorSerieImpl.
 *
 */
public class ControladorSerieImpl extends ControladorNegocioImpl implements ControladorSerie {

	private static final String TIPO_FATURAMENTO = "tipoFaturamento";
	
	private static final String SEGMENTO = "segmento";
	
	private static final String NUMERO = "numero";
	
	private static final String DESCRICAO = "descricao";
	
	private static final String IND_CONTINGENCIA_SCAN = "indicadorContingenciaScan";
	
	private static final String IND_CONTROLE_MENSAL = "indicadorControleMensal";
	
	private static final String IND_SERIE_ELETRONICA = "indicadorSerieEletronica";
	
	private static final String HABILITADO = "habilitado"; 

	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.serie.ControladorSerie#listarSerie(java.util.Map)
	 */
	@Override
	public Collection<Serie> listarSerie(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long operacao = (Long) filtro.get("operacao");
			Long tipoFaturamento = (Long) filtro.get(TIPO_FATURAMENTO);
			Long segmento = (Long) filtro.get(SEGMENTO);
			String numero = (String) filtro.get(NUMERO);
			String descricao = (String) filtro.get(DESCRICAO);
			Boolean indicadorContingenciaScan = null;
			if(filtro.get(IND_CONTINGENCIA_SCAN) != null) {
				indicadorContingenciaScan = (Boolean) filtro.get(IND_CONTINGENCIA_SCAN);
			}
			Boolean indicadorControleMensal = null;
			if(filtro.get(IND_CONTROLE_MENSAL) != null) {
				indicadorControleMensal = (Boolean) filtro.get(IND_CONTROLE_MENSAL);
			}
			Boolean indicadorSerieEletronica = null;
			if(filtro.get(IND_SERIE_ELETRONICA) != null) {
				indicadorSerieEletronica = (Boolean) filtro.get(IND_SERIE_ELETRONICA);
			}
			Boolean habilitado = null;
			if(filtro.get(HABILITADO) != null) {
				habilitado = (Boolean) filtro.get(HABILITADO);
			}

			if(operacao != null && operacao > 0) {
				criteria.add(Restrictions.eq("tipoOperacao.chavePrimaria", operacao));
			}
			if(tipoFaturamento != null && tipoFaturamento > 0) {
				criteria.add(Restrictions.eq("tipoFaturamento.chavePrimaria", tipoFaturamento));
			}
			if(segmento != null && segmento > 0) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", segmento));
			}
			if(numero != null && !"".equals(numero.trim())) {
				criteria.add(Restrictions.ilike(NUMERO, Util.formatarTextoConsulta(numero)));
			}
			if(descricao != null && !"".equals(descricao.trim())) {
				criteria.add(Restrictions.ilike(DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			if(habilitado != null) {
				if(habilitado.equals(true)) {
					criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
				} else if(habilitado.equals(false)) {
					criteria.add(Restrictions.eq(HABILITADO, Boolean.FALSE));
				}
			}
			if(indicadorContingenciaScan != null) {
				if(indicadorContingenciaScan.equals(true)) {
					criteria.add(Restrictions.eq(IND_CONTINGENCIA_SCAN, Boolean.TRUE));
				} else if(indicadorContingenciaScan.equals(false)) {
					criteria.add(Restrictions.eq(IND_CONTINGENCIA_SCAN, Boolean.FALSE));
				}
			}
			if(indicadorSerieEletronica != null) {
				if(indicadorSerieEletronica.equals(true)) {
					criteria.add(Restrictions.eq(IND_SERIE_ELETRONICA, Boolean.TRUE));
				} else if(indicadorSerieEletronica.equals(false)) {
					criteria.add(Restrictions.eq(IND_SERIE_ELETRONICA, Boolean.FALSE));
				}
			}
			if(indicadorControleMensal != null) {
				if(indicadorControleMensal.equals(true)) {
					criteria.add(Restrictions.eq(IND_CONTROLE_MENSAL, Boolean.TRUE));
				} else if(indicadorControleMensal.equals(false)) {
					criteria.add(Restrictions.eq(IND_CONTROLE_MENSAL, Boolean.FALSE));
				}
			}

		}

		criteria.createAlias("tipoOperacao", "tipoOperacao");
		criteria.createAlias(TIPO_FATURAMENTO, TIPO_FATURAMENTO, CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias(SEGMENTO, SEGMENTO, CriteriaSpecification.LEFT_JOIN);

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if(filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if(StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if(paginacaoPadrao) {
			criteria.addOrder(Order.asc(NUMERO));
			criteria.addOrder(Order.asc(DESCRICAO));
		}

		return criteria.list();

	}
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Serie.BEAN_ID_SERIE);
	}

	public Class<?> getClasseSerie() {

		return ServiceLocator.getInstancia().getClassPorID(Serie.BEAN_ID_SERIE);
	}

	/**
	 * Listar entidade conteudo.
	 * 
	 * @param entidadeConteudo
	 *            the entidade conteudo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<EntidadeConteudo> listarEntidadeConteudo(Long entidadeConteudo) throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeConteudo.class);
		criteria.add(Restrictions.eq("entidadeConteudo.entidadeClasse", entidadeConteudo));
		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}

	/**
	 * Listar segmento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Segmento> listarSegmento(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(Segmento.class);
		criteria.add(Restrictions.eq("entidadeConteudo.segmento", chavePrimaria));
		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseEntidadeClasse() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeClasse.BEAN_ID_ENTIDADE_CLASSE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		this.validarDadosSerie((Serie) (entidadeNegocio));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		this.validarDadosSerie((Serie) (entidadeNegocio));
	}

	/**
	 * Validar dados serie.
	 * 
	 * @param serie
	 *            the serie
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosSerie(Serie serie) throws NegocioException {

		if(StringUtils.isNotEmpty(serie.getNumero())) {
			this.verificarSerieExistente(serie);
			this.verificarSerieDuplicada(serie);
		}

		if(serie.isIndicadorControleMensal() && serie.getAnoMes() != null 
				&& !Util.validarAnoMes(serie.getAnoMes())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERIE_NUMERO_MES_ANO_INVALIDO, false);
		}

	}

	/**
	 * Verificar serie existente.
	 * 
	 * @param serie
	 *            the serie
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarSerieExistente(Serie serie) throws NegocioException {

		Integer quantidadeSeries = null;
		Query query = null;
		Long resultado = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(serie.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" serie ");
		hql.append(" where ");
		hql.append(" serie.numero = ? ");
		if(serie.getChavePrimaria() > 0) {
			hql.append(" and serie.chavePrimaria != ?");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, serie.getNumero());
		if(serie.getChavePrimaria() > 0) {
			query.setLong(1, serie.getChavePrimaria());
		}

		resultado = (Long) query.uniqueResult();
		quantidadeSeries = resultado.intValue();

		if(quantidadeSeries != null && quantidadeSeries > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERIE_NUMERO_DUPLICADO, false);
		}
	}

	/**
	 * Verificar serie duplicada.
	 * 
	 * @param serie
	 *            the serie
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarSerieDuplicada(Serie serie) throws NegocioException {

		Integer quantidadeSeries = null;
		Query query = null;
		Long resultado = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(serie.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" serie ");
		hql.append(" where ");
		hql.append(" serie.tipoOperacao.chavePrimaria = :chavePrimariaTipoOperacao ");
		hql.append(" and serie.tipoFaturamento.chavePrimaria = :chavePrimariaTipoFaturamento ");
		hql.append(" and serie.indicadorContingenciaScan = :indicadorContingenciaScan ");
		hql.append(" and serie.indicadorSerieEletronica = :indicadorSerieEletronica ");
		hql.append(" and serie.indicadorControleMensal = :indicadorControleMensal ");

		if(serie.getSegmento() != null && serie.getSegmento().getChavePrimaria() > 0) {
			hql.append(" and serie.segmento.chavePrimaria = :chavePrimariaSegmento ");
		} else {
			hql.append(" and serie.segmento is null ");
		}
		if(serie.isIndicadorControleMensal()) {
			hql.append(" and serie.anoMes = :anoMes ");
		}

		if(serie.getChavePrimaria() > 0) {
			hql.append(" and serie.chavePrimaria != :chavePrimaria ");
		}
		hql.append(" and serie.habilitado = :habilitado ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaTipoOperacao", serie.getTipoOperacao().getChavePrimaria());
		query.setLong("chavePrimariaTipoFaturamento", serie.getTipoFaturamento().getChavePrimaria());
		query.setBoolean("indicadorContingenciaScan", serie.isIndicadorContingenciaScan());
		query.setBoolean("indicadorSerieEletronica", serie.isIndicadorSerieEletronica());
		query.setBoolean("indicadorControleMensal", serie.isIndicadorControleMensal());

		if(serie.getSegmento() != null && serie.getSegmento().getChavePrimaria() > 0) {
			query.setLong("chavePrimariaSegmento", serie.getSegmento().getChavePrimaria());
		}
		if(serie.isIndicadorControleMensal()) {
			query.setInteger("anoMes", serie.getAnoMes());
		}
		if(serie.getChavePrimaria() > 0) {
			query.setLong("chavePrimaria", serie.getChavePrimaria());
		}
		query.setBoolean(HABILITADO, Boolean.TRUE);

		resultado = (Long) query.uniqueResult();
		quantidadeSeries = resultado.intValue();

		if(quantidadeSeries != null && quantidadeSeries > 0) {

			StringBuilder stringBuilder = new StringBuilder();
			String erros = null;

			stringBuilder.append("Tipo de operação ");
			stringBuilder.append(serie.getTipoOperacao().getDescricao());
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			stringBuilder.append("Tipo de faturamento ");
			stringBuilder.append(serie.getTipoFaturamento().getDescricao());
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);

			if(serie.isIndicadorSerieEletronica()) {
				stringBuilder.append("e Série Eletrônica ");
			} else {
				stringBuilder.append("sem Série Eletrônica ");
			}
			if(serie.isIndicadorContingenciaScan()) {
				stringBuilder.append("em Contingência ");
			} else {
				stringBuilder.append("sem Contingência ");
			}
			if(serie.isIndicadorControleMensal()) {
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				stringBuilder.append("Mês/Ano ");
			}
			if(serie.getSegmento() != null && serie.getSegmento().getChavePrimaria() > 0) {
				stringBuilder.append("e Segmento ");
				stringBuilder.append(serie.getSegmento().getDescricao());
			} else {
				stringBuilder.append(" e sem Segmento");
			}
			erros = stringBuilder.toString();

			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERIE_VALORES_DUPLICADO, erros);

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.serie.ControladorSerie#verificarSerieEmUso(java.lang.Long[])
	 */
	@Override
	public void verificarSerieEmUso(Long[] chavesPrimarias) throws NegocioException {

		Criteria criteria = this.createCriteria(DocumentoFiscal.class);
		criteria.add(Restrictions.in("serie.chavePrimaria", chavesPrimarias));

		Collection<DocumentoFiscal> lista = criteria.list();
		if(lista != null && !lista.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERIE_EM_USO, false);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.serie.ControladorSerie#existeSerieEmUso(java.lang.Long)
	 */
	@Override
	public boolean existeSerieEmUso(Long idSerie) {

		Criteria criteria = this.createCriteria(DocumentoFiscal.class);
		criteria.add(Restrictions.eq("serie.chavePrimaria", idSerie));

		Collection<DocumentoFiscal> lista = criteria.list();
		if(lista != null && !lista.isEmpty()) {
			return true;
		}

		return false;

	}
	
	@Cacheable(value = "serieAtual")
	public Collection<Serie> obterSerieAtual(Long idTipoOperacao, Long idTipoFaturamento, Boolean isContingenciaScan,
			Boolean isSerieEletronica) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" serie ");
		hql.append(" where ");
		hql.append(" serie.tipoOperacao.chavePrimaria = :idTipoOperacao ");
		if (idTipoFaturamento != null) {
			hql.append(" and serie.tipoFaturamento.chavePrimaria = :idTipoFaturamento ");
		}
		hql.append(" and serie.habilitado = true ");
		hql.append(" and serie.indicadorContingenciaScan = :isContingenciaScan ");
		hql.append(" and serie.indicadorSerieEletronica = :indicadorSerieEletronica ");
		hql.append(" order by serie.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idTipoOperacao", idTipoOperacao);
		if (idTipoFaturamento != null) {
			query.setLong("idTipoFaturamento", idTipoFaturamento);
		}
		query.setBoolean("isContingenciaScan", isContingenciaScan);

		if (isSerieEletronica == null) {
			query.setBoolean("indicadorSerieEletronica", Boolean.FALSE);
		} else {
			query.setBoolean("indicadorSerieEletronica", isSerieEletronica);
		}

		return query.list();
	}

	
	/**
	 * Obter serie por segmento.
	 *
	 * @param segmento the segmento
	 * @return the serie
	 */
	@Override
	public Serie obterSeriePorSegmentoID(Segmento segmento)  {
		Criteria criteria = this.createCriteria(Serie.class,"serie");
		if(segmento == null){
			criteria.add(Restrictions.isNull("serie.segmento.chavePrimaria"));
		} else {
			criteria.add(Restrictions.eq("serie.segmento.chavePrimaria", segmento.getChavePrimaria()));
		}
		criteria.add(Restrictions.eq("serie.habilitado", Boolean.TRUE));
		criteria.setMaxResults(1);
		
		return (Serie) criteria.uniqueResult();
	}

	
}
