/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 27/06/2013 10:16:53
 @author asoares
 */

package br.com.ggas.faturamento.serie;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * 
 * ControladorSerie
 *
 */
public interface ControladorSerie extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_SERIE = "controladorSerie";
    
	String IND_CONTINGENCIA_SCAN = "indicadorContingenciaScan";
	
	String IND_CONTROLE_MENSAL = "indicadorControleMensal";
	
	String IND_SERIE_ELETRONICA = "indicadorSerieEletronica";
	
	String HABILITADO = "habilitado"; 

	/**
	 * Listar serie.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<Serie> listarSerie(Map<String, Object> filtro);

	/**
	 * Verificar serie em uso.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarSerieEmUso(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Existe serie em uso.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return true, if successful
	 */
	boolean existeSerieEmUso(Long chavePrimaria);

	/**
	 * Obtém uma {@link Serie} por tipo de operação e faturamento, contingência e indicador de série eletrônica.
	 * @param idTipoOperacao - {@link Long}
	 * @param idTipoFaturamento - {@link Long}
	 * @param isContingenciaScan - {@link Boolean}
	 * @param isSerieEletronica - {@link Boolean}
	 * @return {@link Serie}
	 */
	Collection<Serie> obterSerieAtual(Long idTipoOperacao, Long idTipoFaturamento, Boolean isContingenciaScan,
			Boolean isSerieEletronica);
	/**
	 * Obter serie por segmento.
	 *
	 * @param segmento the segmento
	 * @return the serie
	 */
	public Serie obterSeriePorSegmentoID(Segmento segmento);

}
