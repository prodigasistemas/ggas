/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCobrancaImpl representa uma ControladorCobrancaImpl no sistema.
 *
 * @since 22/02/2010
 *
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Objects;
import java.util.TreeMap;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.Cacheable;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.arrecadacao.impl.IndiceFinanceiroValorNominalImpl;
import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoItemFaturamento;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturamentoCache;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.DadosTributoVO;
import br.com.ggas.faturamento.tarifa.AcaoSincroniaDesconto;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.DadosFaixasTarifa;
import br.com.ggas.faturamento.tarifa.DadosTarifa;
import br.com.ggas.faturamento.tarifa.FaixasTarifaDesconto;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaPontoConsumo;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaPesquisar;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.BigDecimalUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorTarifaImpl extends ControladorNegocioImpl implements ControladorTarifa {

	private static final int CONSTANTE_NUMERO_DOIS = 2;

	private static final long CONSTANTE_NUMERO_OITO = 100L;

	private static final int ESCALA = 6;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final int LIMITE_COMENTARIO_TARIFA_VIGENCIA = 199;
	
	private static final int CONSTANTE_NUMERO_VINTE_TRES = 23;
	
	private static final int CONSTANTE_NUMERO_TRINTA= 30;
	
	private static final int CONSTANTE_NUMERO_CINQUENTA_NOVE = 59;
	
	private static final int CONSTANTE_NOVENTA_NOVE = 99;
	
	private static final int CONSTANTE_NUMERO_DOZE = 12;
	
	private static final int CONSTANTE_NUMERO_NOVENTA = 90;
	
	private static final int CONSTANTE_DUZENTOS_CIQUENTA_CINCO = 255;

	private static final Logger LOG = Logger.getLogger(ControladorTarifaImpl.class);

	private static final String KEY_IS_ORDENADO_POR_MEDIDA_INICIO = "isOrdenadoPorMedidaInicio";

	private static final Logger LOG_TARIFA_VIGENCIA = Logger.getLogger(TarifaVigenciaImpl.class);

	private static final String ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO = "ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO";

	private static final String ERRO_TARIFA_IMPOSSIVEL_CADASTRAR_DESCONTO_SEM_FAIXAS_VIGENCIAS = "ERRO_TARIFA_IMPOSSIVEL_CADASTRAR_"
					+ "DESCONTO_SEM_FAIXAS_VIGENCIAS";

	private static final String ERRO_O_VALOR_DA_FAIXA_DESCONTO_NAO_PODE_SER_MAIOR_QUE_99 = "ERRO_O_VALOR_DA_FAIXA_"
					+ "DESCONTO_NAO_PODE_SER_MAIOR_QUE_99";

	private static final String ERRO_NAO_EXISTE_FAIXA_DE_VIGENCIA_PREENCHIDA = "ERRO_NAO_EXISTE_FAIXA_DE_VIGENCIA_PREENCHIDA";

	private static final String ERRO_TARIFA_DESCONTO_FAVOR_PREENCHER_VIGENCIA = "ERRO_TARIFA_DESCONTO_FAVOR_PREENCHER_VIGENCIA";

	private static final String ERRO_TARIFA_FAIXA_DESCONTO_FAVOR_PREENCHER_TARIFA_DESCONTO = "ERRO_TARIFA_FAIXA_DESCONTO_"
					+ "FAVOR_PREENCHER_TARIFA_DESCONTO";

	private static final String ERRO_NEGOCIO_VIGENCIA_DATA_IGUAL = "ERRO_NEGOCIO_VIGENCIA_DATA_IGUAL";

	private static final String ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS = "ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS";

	private static final String ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE_VALOR_MAIOR_DO_QUE_FAIXA_VIGENCIA = "ERRO_TARIFA_"
					+ "FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE_VALOR_MAIOR_DO_QUE_FAIXA_VIGENCIA";

	private static final String ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE = "ERRO_TARIFA_FAIXAS_DESCONTO_"
					+ "NAO_PREENCHIDAS_CORRETAMENTE";

	private static final String ERRO_TARIFA_DATA_INICIO_VIGENCIA_MAIOR_DATA_INICIO_DESCONTO =
					"ERRO_TARIFA_DATA_INICIO_VIGENCIA_MAIOR_DATA_INICIO_DESCONTO";

	private static final String ERRO_TARIFA_DATA_INICIO_DESCONTO_MAIOR_DATA_FIM_DESCONTO =
					"ERRO_TARIFA_DATA_INICIO_DESCONTO_MAIOR_DATA_FIM_DESCONTO";

	private static final String ERRO_DATA_INICIAL_TARIFA_CONSUMO_MENOR_DATA_VIGENCIA_TARIFA =
					"ERRO_DATA_INICIAL_TARIFA_CONSUMO_MENOR_DATA_VIGENCIA_TARIFA";

	private static final String ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO = "ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO";

	private static final String ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO = "ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO";

	private static final String ERRO_TARIFA_SENDO_USADA_FATURA = "ERRO_TARIFA_SENDO_USADA_FATURA";

	private static final String TIPO_ARREDONDAMENTO_CALCULO = "TIPO_ARREDONDAMENTO_CALCULO";

	private static final String ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO_ATIVO = "ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO_ATIVO";

	private static final String RELATORIO_TARIFAS_CADASTRADAS = "relatorioTarifasCadastradas.jasper";

	private static final String RELATORIO_TARIFA = "relatorioTarifa.jasper";

	private static final String DATA_INICIO = "dataInicio";

	private static final String DATA_FIM = "dataFim";

	private static final String DATA_VIGENCIA = "dataVigencia";

	private static final String TARIFA_CHAVE_PRIMARIA = "tarifa.chavePrimaria";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarTarifaVigencia()
	 */
	@Override
	public EntidadeNegocio criarTarifaVigencia() {

		return (TarifaVigencia) ServiceLocator.getInstancia().getBeanPorID(TarifaVigencia.BEAN_ID_TARIFA_VIGENCIA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarTarifaVigenciaFaixa()
	 */
	@Override
	public EntidadeNegocio criarTarifaVigenciaFaixa() {

		return (TarifaVigenciaFaixa) ServiceLocator.getInstancia().getBeanPorID(TarifaVigenciaFaixa.BEAN_ID_TARIFA_VIGENCIA_FAIXA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarTarifaVigenciaDesconto()
	 */
	@Override
	public EntidadeNegocio criarTarifaVigenciaDesconto() {

		return (TarifaVigenciaDesconto) ServiceLocator.getInstancia().getBeanPorID(TarifaVigenciaDesconto.BEAN_ID_TARIFA_VIGENCIA_DESCONTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarTarifaFaixaDesconto()
	 */
	@Override
	public EntidadeNegocio criarTarifaFaixaDesconto() {

		return (TarifaFaixaDesconto) ServiceLocator.getInstancia().getBeanPorID(TarifaFaixaDesconto.BEAN_ID_TARIFA_FAIXA_DESCONTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarTarifaTributo()
	 */
	@Override
	public EntidadeNegocio criarTarifaTributo() {

		return (TarifaVigenciaTributo) ServiceLocator.getInstancia().getBeanPorID(TarifaVigenciaTributo.BEAN_ID_TARIFA_VIGENCIA_TRIBUTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarDadosFaixasTarifa()
	 */
	@Override
	public DadosFaixasTarifa criarDadosFaixasTarifa() {

		return (DadosFaixasTarifa) ServiceLocator.getInstancia().getBeanPorID(DadosFaixasTarifa.BEAN_ID_DADOS_FAIXAS_TARIFAS);
	}

	/**
	 * Criar faixas tarifa desconto.
	 *
	 * @return the faixas tarifa desconto
	 */
	public FaixasTarifaDesconto criarFaixasTarifaDesconto() {

		return (FaixasTarifaDesconto) ServiceLocator.getInstancia().getBeanPorID(FaixasTarifaDesconto.BEAN_ID_FAIXAS_TARIFA_DESCONTO);
	}

	/**
	 * Criar dados tarifa.
	 *
	 * @return the dados tarifa
	 */
	public DadosTarifa criarDadosTarifa() {

		return (DadosTarifa) ServiceLocator.getInstancia().getBeanPorID(DadosTarifa.BEAN_ID_DADOS_TARIFA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarIndiceFinanceiroValorNominal()
	 */
	@Override
	public EntidadeNegocio criarIndiceFinanceiroValorNominal() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						IndiceFinanceiroValorNominal.BEAN_ID_INDICE_FINANCEIRO_VALOR_NOMINAL);
	}

	public Class<?> getClasseEntidadeTarifa() {

		return ServiceLocator.getInstancia().getClassPorID(Tarifa.BEAN_ID_TARIFA);
	}

	public Class<?> getClasseEntidadeTarifaPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaPontoConsumo.BEAN_ID_TARIFA_PONTOCONSUMO);
	}

	public Class<?> getClasseEntidadeTarifaVigencia() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigencia.BEAN_ID_TARIFA_VIGENCIA);
	}

	public Class<?> getClasseEntidadeTarifaVigenciaFaixa() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaFaixa.BEAN_ID_TARIFA_VIGENCIA_FAIXA);
	}

	public Class<?> getClasseEntidadeTarifaVigenciaDesconto() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaDesconto.BEAN_ID_TARIFA_VIGENCIA_DESCONTO);
	}

	public Class<?> getClasseEntidadeTarifaFaixaDesconto() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaFaixaDesconto.BEAN_ID_TARIFA_FAIXA_DESCONTO);
	}

	public Class<?> getClasseEntidadeTarifaTributo() {

		return ServiceLocator.getInstancia().getClassPorID(TarifaVigenciaTributo.BEAN_ID_TARIFA_VIGENCIA_TRIBUTO);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	public Class<?> getClasseEntidadeIndiceFinanceiroValorNominal() {

		return ServiceLocator.getInstancia().getClassPorID(IndiceFinanceiroValorNominal.BEAN_ID_INDICE_FINANCEIRO_VALOR_NOMINAL);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoItemFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(
						ContratoPontoConsumoItemFaturamento.BEAN_ID_CONTRATO_PONTO_CONSUMO_ITEM_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseEntidadeIndiceFinanceiro() {

		return ServiceLocator.getInstancia().getClassPorID(IndiceFinanceiro.BEAN_ID_INDICE_FINANCEIRO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#preInsercao
	 * (br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio instanceof Tarifa) {
			Tarifa tarifa = (Tarifa) entidadeNegocio;
			// consultar a ação de cobrança
			Collection<Tarifa> listaTarifasExistente = this.consultarTarifaExistente(tarifa.getDescricao(), tarifa.getChavePrimaria());
			if (listaTarifasExistente != null && !listaTarifasExistente.isEmpty()) {
				throw new NegocioException(Constantes.TARIFA_EXISTENTE);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#preAtualizacao
	 * (br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio instanceof Tarifa) {
			Tarifa tarifa = (Tarifa) entidadeNegocio;
			Collection<Tarifa> listaTarifasExistentes = this.consultarTarifaExistente(tarifa.getDescricao(), tarifa.getChavePrimaria());
			if (listaTarifasExistentes != null && !listaTarifasExistentes.isEmpty()) {
				throw new NegocioException("Já existe uma Tarifa com a mesma descrição");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifas(java.util.Map)
	 */
	@Override
	public Collection<Tarifa> consultarTarifas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifa());
		Boolean isVerificarTVAutorizada = Boolean.FALSE;
		Collection<Tarifa> listaTarifas = null;

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			EntidadeConteudo itemFatura = (EntidadeConteudo) filtro.get(Tarifa.ATRIBUTO_ITEM_FATURA);
			if (itemFatura != null) {
				criteria.add(Restrictions.eq("itemFatura.chavePrimaria", itemFatura.getChavePrimaria()));
			}

			String descricao = (String) filtro.get(Tarifa.ATRIBUTO_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(Tarifa.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			String descricaoAbreviada = (String) filtro.get(Tarifa.ATRIBUTO_DESCRICAO_ABREVIADA);
			if (!StringUtils.isEmpty(descricaoAbreviada)) {
				criteria.add(Restrictions.ilike(Tarifa.ATRIBUTO_DESCRICAO_ABREVIADA, Util.formatarTextoConsulta(descricaoAbreviada)));
			}

			Long idSegmento = (Long) filtro.get(Tarifa.ATRIBUTO_ID_SEGMENTO);
			if (idSegmento != null && idSegmento > 0) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			Boolean verificarTVAutorizada = (Boolean) filtro.get("verificarTarifaVigenciaAutorizada");
			if (verificarTVAutorizada != null && verificarTVAutorizada) {
				isVerificarTVAutorizada = Boolean.TRUE;
			}

			criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));
		}

		if (isVerificarTVAutorizada) {
			listaTarifas = verificarListaTarifa(criteria.list());
		} else {
			listaTarifas = criteria.list();
		}

		return listaTarifas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifas()
	 */
	@Override
	public Collection<Tarifa> listarTarifas() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifa().getSimpleName());
		hql.append(" tarifa ");
		hql.append(" left join fetch tarifa.segmento segmento");
		hql.append(" where tarifa.habilitado = true ");
		hql.append(" order by tarifa.descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return verificarListaTarifa(query.list());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#verificarListaTarifa(java.util.Collection)
	 */
	@Override
	public Collection<Tarifa> verificarListaTarifa(Collection<Tarifa> listaTarifas) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<Tarifa> listarTarifasAux = new ArrayList<Tarifa>();
		Map<String, Object> filtro = new HashMap<String, Object>();
		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

		FaturamentoCache cache = FaturamentoCache.getInstance();
		Map< String , Tarifa> mapa = (Map< String , Tarifa>)cache.get(FaturamentoCache.TARIFAS);
		if( mapa == null ){
			Map< String , Tarifa> m = new HashMap<>();
			cache.over(FaturamentoCache.TARIFAS, m);
			mapa = m;
		}


		for (Tarifa tarifa : listaTarifas) {
			filtro.put(Tarifa.ATRIBUTO_ID_TARIFA, tarifa.getChavePrimaria());
			filtro.put(Tarifa.ATRIBUTO_STATUS, Long.parseLong(paramStatusAutorizada));
			String key = tarifa.getChavePrimaria() + ";" + paramStatusAutorizada;

			if( mapa.containsKey( key )){
				listarTarifasAux.add( mapa.get(key));
			}else{
				Long quantidadeTarifaVigencia = contarTarifasVigencia(filtro);
				if (quantidadeTarifaVigencia != null && quantidadeTarifaVigencia > 0) {
					mapa.put(key, tarifa);
					listarTarifasAux.add(tarifa);
				}
			}
		}
		cache.over(FaturamentoCache.TARIFAS, mapa);

		return listarTarifasAux;
	}

	private Criteria getCriteriaTarifa() {

		return createCriteria(getClasseEntidadeTarifa());
	}

	/**
	 * Consultar tarifa existente.
	 *
	 * @param nomeTarifa
	 *            the nome tarifa
	 * @param idTarifa
	 *            the id tarifa
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<Tarifa> consultarTarifaExistente(String nomeTarifa, Long idTarifa) {

		Criteria criteria = getCriteriaTarifa();
		criteria.add(Restrictions.eq(EntidadeConteudo.ATRIBUTO_DESCRICAO, nomeTarifa));
		criteria.add(Restrictions.ne(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idTarifa));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasVigencia(java.util.Map)
	 */
	@Override
	public  List<TarifaVigencia> consultarTarifasVigencia(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigencia());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idTarifa = (Long) filtro.get(Tarifa.ATRIBUTO_ID_TARIFA);
			if (idTarifa != null && idTarifa > 0) {
				criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, idTarifa));
			}

			Date dataVigencia = (Date) filtro.get(Tarifa.ATRIBUTO_DATA_VIGENCIA);
			if (dataVigencia != null) {
				criteria.add(Restrictions.eq(Tarifa.ATRIBUTO_DATA_VIGENCIA, dataVigencia));
			}

			EntidadeConteudo unidadeMonetaria = (EntidadeConteudo) filtro.get("unidadeMonetaria");
			if (unidadeMonetaria != null) {
				criteria.add(Restrictions.eq("unidadeMonetaria.chavePrimaria", unidadeMonetaria.getChavePrimaria()));
			}

			EntidadeConteudo tipoCalculo = (EntidadeConteudo) filtro.get("tipoCalculo");
			if (tipoCalculo != null) {
				criteria.add(Restrictions.eq("tipoCalculo.chavePrimaria", tipoCalculo.getChavePrimaria()));
			}

			Long chavePrimariaTarifa = (Long) filtro.get("chavePrimariaTarifa");
			if (chavePrimariaTarifa != null && chavePrimariaTarifa > 0) {
				criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, chavePrimariaTarifa));
			}

			Long[] chavesTarifa = (Long[]) filtro.get("chavesTarifa");
			if (chavesTarifa != null && chavesTarifa.length > 0) {
				criteria.add(Restrictions.in(TARIFA_CHAVE_PRIMARIA, chavesTarifa));
			}

			Long status = (Long) filtro.get("status");
			if (status != null) {
				criteria.add(Restrictions.eq("status.chavePrimaria", status));
			}

			criteria.addOrder(Order.desc(Tarifa.ATRIBUTO_DATA_VIGENCIA));

		}

		return criteria.list();
	}



	/**
	 * Conta a quantidade de entidades do tipo TarifaVigencia de acordo
	 * com o filtro especificado.
	 * @param filtro
	 * @return quantidade de entidades de TarifaVigencia
	 *
	 */
	@Override
	public Long contarTarifasVigencia(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigencia());
		criteria.setProjection(Projections.rowCount());

		
		this.filtrarConsultaTarifaVigencia(criteria, filtro);
		
		return (Long) criteria.uniqueResult();
	}
	
	private List<Long> obterIdsTarifaPorTarifaVigencia(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigencia());

		criteria.setProjection(Projections.projectionList().add(Projections.property(TARIFA_CHAVE_PRIMARIA),
				Tarifa.ATRIBUTO_ID_TARIFA));

		this.filtrarConsultaTarifaVigencia(criteria, filtro);

		return criteria.list();
	}
	
	private void filtrarConsultaTarifaVigencia(Criteria criteria, Map<String, Object> filtro) {
		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idTarifa = (Long) filtro.get(Tarifa.ATRIBUTO_ID_TARIFA);
			if (idTarifa != null && idTarifa > 0) {
				criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, idTarifa));
			}

			Date dataVigencia = (Date) filtro.get(Tarifa.ATRIBUTO_DATA_VIGENCIA);
			if (dataVigencia != null) {
				criteria.add(Restrictions.eq(Tarifa.ATRIBUTO_DATA_VIGENCIA, dataVigencia));
			}

			EntidadeConteudo unidadeMonetaria = (EntidadeConteudo) filtro.get("unidadeMonetaria");
			if (unidadeMonetaria != null) {
				criteria.add(Restrictions.eq("unidadeMonetaria.chavePrimaria", unidadeMonetaria.getChavePrimaria()));
			}

			EntidadeConteudo tipoCalculo = (EntidadeConteudo) filtro.get("tipoCalculo");
			if (tipoCalculo != null) {
				criteria.add(Restrictions.eq("tipoCalculo.chavePrimaria", tipoCalculo.getChavePrimaria()));
			}

			Long chavePrimariaTarifa = (Long) filtro.get("chavePrimariaTarifa");
			if (chavePrimariaTarifa != null && chavePrimariaTarifa > 0) {
				criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, chavePrimariaTarifa));
			}

			Long[] chavesTarifa = (Long[]) filtro.get("chavesTarifa");
			if (chavesTarifa != null && chavesTarifa.length > 0) {
				criteria.add(Restrictions.in(TARIFA_CHAVE_PRIMARIA, chavesTarifa));
			}

			Long status = (Long) filtro.get("status");
			if (status != null) {
				criteria.add(Restrictions.eq("status.chavePrimaria", status));
			}

			Date dataInicio = (Date) filtro.get(DATA_INICIO);
			Date dataFim = (Date) filtro.get(DATA_FIM);

			if (dataInicio != null && dataFim != null && idTarifa!=null) {
				DetachedCriteria subconsulta1 = DetachedCriteria.forClass(this.getClasseEntidadeTarifaVigencia());
				subconsulta1.setProjection(Projections.max(CHAVE_PRIMARIA));
				subconsulta1.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, idTarifa));
				subconsulta1.add(Restrictions.le(DATA_VIGENCIA, dataInicio));

				DetachedCriteria subconsulta2 = DetachedCriteria.forClass(this.getClasseEntidadeTarifaVigencia());
				subconsulta2.setProjection(Projections.id());
				subconsulta2.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, idTarifa));
				subconsulta2.add(Restrictions.ge(DATA_VIGENCIA, dataInicio));
				subconsulta2.add(Restrictions.le(DATA_VIGENCIA, dataFim));


				Criterion restrictionOR = Restrictions.or(Subqueries.propertyEq(CHAVE_PRIMARIA, subconsulta1),
														  Subqueries.propertyIn(CHAVE_PRIMARIA, subconsulta2));

				criteria.add(restrictionOR);

			}

			criteria.addOrder(Order.desc(Tarifa.ATRIBUTO_DATA_VIGENCIA));

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifasVigencia()
	 */
	@Override
	public Collection<TarifaVigencia> listarTarifasVigencia() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by dataVigencia ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigencia(java.lang.Long)
	 */
	@Override
	public TarifaVigencia obterTarifaVigencia(Long chavePrimaria) throws NegocioException {

		return (TarifaVigencia) this.obter(chavePrimaria, getClasseEntidadeTarifaVigencia());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifa(java.lang.Long)
	 */
	@Override
	public Tarifa obterTarifa(Long chavePrimaria) throws NegocioException {

		return (Tarifa) this.obter(chavePrimaria, getClasseEntidadeTarifa());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterIndiceFinanceiro(java.lang.Long)
	 */
	@Override
	public IndiceFinanceiro obterIndiceFinanceiro(Long chavePrimaria) throws NegocioException {

		return (IndiceFinanceiro) this.obter(chavePrimaria, getClasseEntidadeIndiceFinanceiro());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigencia(java.lang.Long, java.lang.String[])
	 */
	@Override
	public TarifaVigencia obterTarifaVigencia(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (TarifaVigencia) this.obter(chavePrimaria, getClasseEntidadeTarifaVigencia(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#verificarUnidadeMonetariaTarifaVigencia(long)
	 */
	@Override
	public boolean verificarUnidadeMonetariaTarifaVigencia(long chavePrimariaTarifa) throws NegocioException {

		boolean retorno = false;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" where ");
		hql.append(" tarifa.chavePrimaria = :chavePrimariaTarifa");
		hql.append(" and unidadeMonetaria.chavePrimaria = :chaveTarifaVigencia");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaTarifa", chavePrimariaTarifa);
		query.setLong("chaveTarifaVigencia", EntidadeConteudo.CHAVE_UNIDADE_MONETARIA_DOLAR);

		Collection<TarifaVigencia> listaTarifaVigencia = query.list();

		if (!listaTarifaVigencia.isEmpty()) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasVigenciaFaixa(java.util.Map)
	 */
	@Override
	public Collection<TarifaVigenciaFaixa> consultarTarifasVigenciaFaixa(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigenciaFaixa());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			TarifaVigencia tarifaVigencia = (TarifaVigencia) filtro.get("tarifaVigencia");
			if (tarifaVigencia != null) {
				criteria.add(Restrictions.eq("tarifaVigencia.chavePrimaria", tarifaVigencia.getChavePrimaria()));
			} else {
				criteria.setFetchMode("tarifaVigencia", FetchMode.JOIN);
			}

			Long chaveTarifaVigencia = (Long) filtro.get("chaveTarifaVigencia");
			if (chaveTarifaVigencia != null && chaveTarifaVigencia > 0) {
				criteria.add(Restrictions.eq("tarifaVigencia.chavePrimaria", chaveTarifaVigencia));
			}

			BigDecimal medidaInicio = (BigDecimal) filtro.get("medidaInicio");
			if (medidaInicio != null) {
				criteria.add(Restrictions.eq("medidaInicio", medidaInicio));
			}

			BigDecimal medidaFim = (BigDecimal) filtro.get("medidaFim");
			if (medidaFim != null) {
				criteria.add(Restrictions.eq("medidaFim", medidaFim));
			}

			BigDecimal valorFixo = (BigDecimal) filtro.get("valorFixo");
			if (valorFixo != null) {
				criteria.add(Restrictions.eq("valorFixo", valorFixo));
			}

			BigDecimal valorVariavel = (BigDecimal) filtro.get("valorVariavel");
			if (valorVariavel != null) {
				criteria.add(Restrictions.eq("valorVariavel", valorVariavel));
			}

			String ordenadoPor = (String) filtro.get("ordenadoPor");
			if (ordenadoPor != null) {
				criteria.addOrder(Order.asc(ordenadoPor));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifasVigenciaFaixa()
	 */
	@Override
	public Collection<TarifaVigenciaFaixa> listarTarifasVigenciaFaixa() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaFaixa().getSimpleName());
		hql.append(" where habilitado = true ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigenciaFaixa(java.lang.Long)
	 */
	@Override
	public TarifaVigenciaFaixa obterTarifaVigenciaFaixa(Long chavePrimaria) throws NegocioException {

		return (TarifaVigenciaFaixa) this.obter(chavePrimaria, getClasseEntidadeTarifaVigenciaFaixa());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasFaixaDesconto(java.util.Map)
	 */
	@Override
	public Collection<TarifaFaixaDesconto> consultarTarifasFaixaDesconto(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaFaixaDesconto());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			TarifaVigenciaFaixa tarifaVigenciaFaixa = (TarifaVigenciaFaixa) filtro.get("tarifaVigenciaFaixa");
			if (tarifaVigenciaFaixa != null) {
				criteria.add(Restrictions.eq("tarifaVigenciaFaixa.chavePrimaria", tarifaVigenciaFaixa.getChavePrimaria()));
			}

			Date dataInicioVigencia = (Date) filtro.get("dataInicioVigencia");
			if (dataInicioVigencia != null) {
				criteria.add(Restrictions.eq("dataInicioVigencia", dataInicioVigencia));
			}

			Date dataFimVigencia = (Date) filtro.get("dataFimVigencia");
			if (dataFimVigencia != null) {
				criteria.add(Restrictions.eq("dataFimVigencia", dataFimVigencia));
			}

			String observacao = (String) filtro.get("observacao");
			if (!StringUtils.isEmpty(observacao)) {
				criteria.add(Restrictions.ilike("observacao", Util.formatarTextoConsulta(observacao)));
			}

			BigDecimal valorDesconto = (BigDecimal) filtro.get("valorDesconto");
			if (valorDesconto != null) {
				criteria.add(Restrictions.eq("valorDesconto", valorDesconto));
			}

			BigDecimal percentualDesconto = (BigDecimal) filtro.get("percentualDesconto");
			if (percentualDesconto != null) {
				criteria.add(Restrictions.eq("percentualDesconto", percentualDesconto));
			}
			Long idTarifaVigencia = (Long) filtro.get("idTarifaVigencia");
			criteria.createAlias("tarifaVigenciaFaixa", "tarifaVigenciaFaixa");
			if (idTarifaVigencia != null && idTarifaVigencia > 0) {
				criteria.add(Restrictions.eq("tarifaVigenciaFaixa.tarifaVigencia.chavePrimaria", idTarifaVigencia));
			} else {
				criteria.setFetchMode("tarifaVigenciaFaixa", FetchMode.JOIN);
			}

			Long idTarifaVigenciaDesconto = (Long) filtro.get("idTarifaVigenciaDesconto");
			if (idTarifaVigenciaDesconto != null && idTarifaVigenciaDesconto > 0) {
				criteria.add(Restrictions.eq("tarifaVigenciaDesconto.chavePrimaria", idTarifaVigenciaDesconto));
			}

			Long[] chavesTarifaVigenciaDesconto = (Long[]) filtro.get("chavesTarifaVigenciaDesconto");
			if (chavesTarifaVigenciaDesconto != null && chavesTarifaVigenciaDesconto.length > 0) {
				criteria.add(Restrictions.in("tarifaVigenciaDesconto.chavePrimaria", chavesTarifaVigenciaDesconto));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null && habilitado) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			if (idTarifaVigencia != null && idTarifaVigencia > 0) {
				criteria.addOrder(Order.asc("tarifaVigenciaFaixa.medidaInicio"));
			}

			Boolean isOrdenadoPorMedidaInicio = (Boolean) filtro.get(KEY_IS_ORDENADO_POR_MEDIDA_INICIO);
			if (isOrdenadoPorMedidaInicio != null && isOrdenadoPorMedidaInicio) {
				criteria.addOrder(Order.asc("tarifaVigenciaFaixa.medidaInicio"));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifasFaixaDesconto()
	 */
	@Override
	public Collection<TarifaFaixaDesconto> listarTarifasFaixaDesconto() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaFaixaDesconto().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by dataFimVigencia ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifasFaixaDesconto(java.lang.Long)
	 */
	@Override
	public Collection<TarifaFaixaDesconto> listarTarifasFaixaDesconto(Long idTarifaVigenciaDesconto) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idTarifaVigenciaDesconto", idTarifaVigenciaDesconto);
		filtro.put(KEY_IS_ORDENADO_POR_MEDIDA_INICIO, true);
		return this.consultarTarifasFaixaDesconto(filtro);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaFaixaDesconto(java.lang.Long)
	 */
	@Override
	public TarifaFaixaDesconto obterTarifaFaixaDesconto(Long chavePrimaria) throws NegocioException {

		return (TarifaFaixaDesconto) this.obter(chavePrimaria, getClasseEntidadeTarifaFaixaDesconto());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasTributo(java.util.Map)
	 */
	@Override
	public Collection<TarifaVigenciaTributo> consultarTarifasTributo(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaTributo());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idTarifa = (Long) filtro.get("idTarifa");
			if (idTarifa != null) {
				criteria.setFetchMode("tarifaVigencia", FetchMode.JOIN);
				criteria.setFetchMode("tarifaVigencia.tarifa", FetchMode.JOIN);
				criteria.setFetchMode("tributo", FetchMode.JOIN);
				criteria.createAlias("tarifaVigencia.tarifa", "tarifa");
				criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, idTarifa));
			}

			Long idTarifaVigencia = (Long) filtro.get("idTarifaVigencia");
			if (idTarifaVigencia != null && idTarifaVigencia > 0) {
				criteria.setFetchMode("tarifaVigencia", FetchMode.JOIN);
				criteria.add(Restrictions.eq("tarifaVigencia.chavePrimaria", idTarifaVigencia));
			}

			Tributo tributo = (Tributo) filtro.get("tributo");
			if (tributo != null) {
				criteria.add(Restrictions.eq("tributo.chavePrimaria", tributo.getChavePrimaria()));
			}

			Date dataVigencia = (Date) filtro.get(DATA_VIGENCIA);
			if (dataVigencia != null) {
				criteria.add(Restrictions.eq(DATA_VIGENCIA, dataVigencia));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifasTributo()
	 */
	@Override
	public Collection<TarifaVigenciaTributo> listarTarifasTributo() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaTributo().getSimpleName());
		hql.append(" where habilitado = true ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaTributo(java.lang.Long)
	 */
	@Override
	public TarifaVigenciaTributo obterTarifaTributo(Long chavePrimaria) throws NegocioException {

		return (TarifaVigenciaTributo) this.obter(chavePrimaria, getClasseEntidadeTarifaTributo());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasVigenciaDesconto(java.util.Map)
	 */
	@Override
	public Collection<TarifaVigenciaDesconto> consultarTarifasVigenciaDesconto(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigenciaDesconto());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idTarifa = (Long) filtro.get("idTarifa");
			if (idTarifa != null && idTarifa > 0) {
				criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, idTarifa));
			}

			Long[] chavesTarifa = (Long[]) filtro.get("chavesTarifa");
			if (chavesTarifa != null && chavesTarifa.length > 0) {
				criteria.add(Restrictions.in(TARIFA_CHAVE_PRIMARIA, chavesTarifa));
			}

			String descricao = (String) filtro.get(EntidadeConteudo.ATRIBUTO_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(EntidadeConteudo.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Long idTarifaVigencia = (Long) filtro.get("idTarifaVigencia");
			if (idTarifaVigencia != null && idTarifaVigencia > 0) {
				criteria.add(Restrictions.eq("tarifaVigencia.chavePrimaria", idTarifaVigencia));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null && habilitado) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			criteria.addOrder(Order.desc("inicioVigencia"));

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasVigenciaDescontoPorTarifaPeriodo(java.lang.Long,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<TarifaVigenciaDesconto> consultarTarifasVigenciaDescontoPorTarifaPeriodo(Long idTarifa, Date dataInicioPeriodo,
					Date dataFimPeriodo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaDesconto().getSimpleName());
		hql.append(" tvd ");
		hql.append(" where tvd.tarifa.chavePrimaria = :idTarifa ");
		if (dataFimPeriodo != null) {
			hql.append(" and (tvd.inicioVigencia between :dataInicioPeriodo and :dataFimPeriodo ");
			hql.append(" or tvd.fimVigencia between :dataInicioPeriodo and :dataFimPeriodo ");
			hql.append(" or ( tvd.inicioVigencia <= :dataInicioPeriodo and tvd.fimVigencia >= :dataFimPeriodo )) ");
		} else {
			hql.append(" and ( tvd.inicioVigencia >= :dataInicioPeriodo ");
			hql.append(" or tvd.fimVigencia >= :dataInicioPeriodo ) ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idTarifa != null && idTarifa > 0) {
			query.setParameter("idTarifa", idTarifa);
		}

		if (dataInicioPeriodo != null) {
			query.setParameter("dataInicioPeriodo", dataInicioPeriodo);
		}
		if (dataFimPeriodo != null) {
			query.setParameter("dataFimPeriodo", dataFimPeriodo);
		}

		return query.list();
	}

	/**
	 * Obter tarifas pela vigencia.
	 *
	 * @param idTarifa
	 *            the id tarifa
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Map<String, Object> obterTarifasPelaVigencia(Long idTarifa) throws NegocioException {

		Map<String, Object> tarifas = new HashMap<String, Object>();

		TarifaVigencia tvVigente = null;
		TarifaVigencia tvPosterior = null;

		Collection<TarifaVigencia> listaTarifaVigencia = this.consultarVigenciaTarifaMaior(idTarifa);
		if (listaTarifaVigencia != null && !listaTarifaVigencia.isEmpty()) {
			Date dataAtual = Calendar.getInstance().getTime();
			TarifaVigencia tvAnterior = null;
			for (TarifaVigencia tv : listaTarifaVigencia) {
				if (tvAnterior != null) {
					// obter tarifa vigente
					if ((dataAtual.compareTo(tvAnterior.getDataVigencia()) >= 0) && (dataAtual.compareTo(tv.getDataVigencia()) <= 0)) {

						tvVigente = tvAnterior;
						tvPosterior = tv;
						break;
					}

				} else {
					tvAnterior = tv;
				}
			}
			if (tvVigente == null && tvAnterior != null) {
				tvVigente = tvAnterior;
			} else {
				throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_TARIFA_VEGENTE, true);
			}
		}

		tarifas.put("tarifaVigente", tvVigente);
		tarifas.put("tarifaPosterior", tvPosterior);
		return tarifas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifasVigenciaDesconto()
	 */
	@Override
	public Collection<TarifaVigenciaDesconto> listarTarifasVigenciaDesconto() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaTributo().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigenciaDesconto(java.lang.Long)
	 */
	@Override
	public TarifaVigenciaDesconto obterTarifaVigenciaDesconto(Long chavePrimaria) throws NegocioException {

		return (TarifaVigenciaDesconto) this.obter(chavePrimaria, getClasseEntidadeTarifaVigenciaDesconto());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigenciaDesconto(java.lang.Long, java.lang.String[])
	 */
	@Override
	public TarifaVigenciaDesconto obterTarifaVigenciaDesconto(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (TarifaVigenciaDesconto) this.obter(chavePrimaria, getClasseEntidadeTarifaTributo(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (Tarifa) ServiceLocator.getInstancia().getBeanPorID(Tarifa.BEAN_ID_TARIFA);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Tarifa.BEAN_ID_TARIFA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#removerTarifa(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerTarifa(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {

			ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

			Collection<ContratoPontoConsumoItemFaturamento> lista = controladorContrato
							.listarContratoItemFaturamentoPorTarifas(chavesPrimarias);

			if (lista != null && !lista.isEmpty()) {
				throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_EXCLUSAO_TARIFA, true);
			} else {
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("chavesTarifa", chavesPrimarias);
				Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = this.consultarTarifasVigenciaDesconto(filtro);
				Long[] chavesTarifaVigenciaDesconto = Util.collectionParaArrayChavesPrimarias(listaTarifaVigenciaDesconto);

				this.removerTarifasFaixaDesconto(chavesTarifaVigenciaDesconto, auditoria);
				this.removerTarifaVigenciaDesconto(listaTarifaVigenciaDesconto, auditoria);
				this.removerTarifaVigencia(chavesPrimarias, auditoria);
				this.removerTarifas(chavesPrimarias, auditoria);

			}
		}
	}

	/**
	 * Remover tarifas.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerTarifas(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavesPrimarias", chavesPrimarias);
		Collection<Tarifa> listaTarifa = this.consultarTarifas(filtro);
		if ((listaTarifa != null) && (!listaTarifa.isEmpty())) {
			for (Tarifa tarifa : listaTarifa) {
				tarifa.setDadosAuditoria(auditoria);
				super.remover(tarifa);
			}
		}
	}

	/**
	 * Remover tarifa vigencia.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerTarifaVigencia(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavesTarifa", chavesPrimarias);
		Collection<TarifaVigencia> listaTarifaVigencia = this.consultarTarifasVigencia(filtro);
		if ((listaTarifaVigencia != null) && (!listaTarifaVigencia.isEmpty())) {
			for (TarifaVigencia tarifaVigencia : listaTarifaVigencia) {
				tarifaVigencia.setDadosAuditoria(auditoria);
				super.remover(tarifaVigencia);
			}
		}
	}

	/**
	 * Remover tarifa vigencia desconto.
	 *
	 * @param listaTarifaVigenciaDesconto
	 *            the lista tarifa vigencia desconto
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerTarifaVigenciaDesconto(Collection<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto, DadosAuditoria auditoria)
					throws NegocioException {

		if ((listaTarifaVigenciaDesconto != null) && (!listaTarifaVigenciaDesconto.isEmpty())) {
			for (TarifaVigenciaDesconto tarifaVigenciaDesconto : listaTarifaVigenciaDesconto) {
				tarifaVigenciaDesconto.setDadosAuditoria(auditoria);
				super.remover(tarifaVigenciaDesconto);
			}
		}
	}

	/**
	 * Remover tarifas faixa desconto.
	 *
	 * @param chavesTarifaVigenciaDesconto
	 *            the chaves tarifa vigencia desconto
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerTarifasFaixaDesconto(Long[] chavesTarifaVigenciaDesconto, DadosAuditoria auditoria) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavesTarifaVigenciaDesconto", chavesTarifaVigenciaDesconto);
		Collection<TarifaFaixaDesconto> listaTarifaFaixaDesconto = this.consultarTarifasFaixaDesconto(filtro);
		if ((listaTarifaFaixaDesconto != null) && (!listaTarifaFaixaDesconto.isEmpty())) {
			for (TarifaFaixaDesconto tarifaFaixaDesconto : listaTarifaFaixaDesconto) {
				tarifaFaixaDesconto.setDadosAuditoria(auditoria);
				super.remover(tarifaFaixaDesconto);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#inserirTarifa(br.com.ggas.faturamento.tarifa.Tarifa,
	 * br.com.ggas.faturamento.tarifa.TarifaVigencia, br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto, java.util.List)
	 */
	@Override
	public Long inserirTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		Map<String, Object> mapaErros = tarifa.validarDados();
		StringBuilder erros = new StringBuilder();
		String errosTarifa = (String) mapaErros.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS);
		if (!StringUtils.isEmpty(errosTarifa)) {
			erros.append(errosTarifa);
			erros.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (tarifaVigencia != null) {
			mapaErros = tarifaVigencia.validarDados();
		}
		String errosTarifaVigencia = (String) mapaErros.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS);
		if (!StringUtils.isEmpty(errosTarifaVigencia)) {
			erros.append(errosTarifaVigencia);
		} else if (erros.length() > 0) {
			erros.delete(erros.length() - CONSTANTE_NUMERO_DOIS, erros.length());
		}

		if (erros.length() > 0) {
			mapaErros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, erros.toString());
			throw new NegocioException(mapaErros);
		}

		Long chavePrimaria = this.inserir(tarifa);

		if (tarifaVigencia != null && validaSeVaiInserirTarifaVigencia(tarifaVigencia)) {
			this.inserirComplementoTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
		} else {
			if (validaSeFoiPreenchidoTarifaVigenciaDesconto(tarifaVigenciaDesconto)) {
				throw new NegocioException(ERRO_TARIFA_DESCONTO_FAVOR_PREENCHER_VIGENCIA, true);
			}
			if (validaSeNaoTemFaixaDescontoPreenchida(listaDadosFaixa)) {
				throw new NegocioException(ERRO_TARIFA_FAIXA_DESCONTO_FAVOR_PREENCHER_TARIFA_DESCONTO, true);
			}
		}

		return chavePrimaria;
	}

	/**
	 * Insere uma vigência de uma tarifa e seus
	 * respectivos descontos e faixas.
	 *
	 * @param tarifa
	 *            a tarifa da vigência a ser
	 *            inserida
	 * @param tarifaVigencia
	 *            a vigência a ser isnerida
	 * @param tarifaVigenciaDesconto
	 *            os descontos da vigência a ser
	 *            inserida
	 * @param listaDadosFaixa
	 *            os dados de cada faixa da
	 *            vigência a ser inserida
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void inserirComplementoTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		this.inserirTarifaVigencia(tarifaVigencia);

		if (tarifaVigenciaDesconto != null && validaSeVaiInserirTarifaVigenciaDesconto(tarifaVigenciaDesconto)) {

			this.validarDataTarifaDesconto(tarifaVigencia, tarifaVigenciaDesconto);

			this.validarTarifaVigenciaDesconto(tarifaVigenciaDesconto, listaDadosFaixa);
			tarifaVigenciaDesconto.setTarifaVigencia(tarifaVigencia);
			this.inserir(tarifaVigenciaDesconto);

			try {
				this.validaDadosInserirTarifa(tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
				Collection<TarifaFaixaDesconto> tarifaFaixaDescontos = this.popularTarifaFaixaDesconto(tarifaVigenciaDesconto,
								tarifaVigencia, listaDadosFaixa);

				for (TarifaFaixaDesconto tarifaFaixaDescontoLoop : tarifaFaixaDescontos) {
					tarifaFaixaDescontoLoop.setDadosAuditoria(tarifa.getDadosAuditoria());
					this.inserir(tarifaFaixaDescontoLoop);
				}

			} catch (FormatoInvalidoException ex) {
				LOG.error(ex.getMessage(), ex);
				throw new NegocioException(ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE, true);
			}

		} else {
			/*
			 * Caso não tenha nenhum Desconto
			 * cadastrado, validar para ver se
			 * existe algum valor digitado na
			 * faixa desconto.
			 */
			if (validaSeNaoTemFaixaDescontoPreenchida(listaDadosFaixa)) {
				throw new NegocioException(ERRO_TARIFA_FAIXA_DESCONTO_FAVOR_PREENCHER_TARIFA_DESCONTO, true);
			}
		}
	}

	/**
	 * Possui alcada autorizacao tarifa.
	 *
	 * @param usuario
	 *            the usuario
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @return true, if successful
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public boolean possuiAlcadaAutorizacaoTarifa(Usuario usuario, TarifaVigencia tarifaVigencia) throws GGASException {

		Collection<TarifaVigencia> tarifas = new ArrayList<TarifaVigencia>();

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		String paramStatusPendente = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_STATUS_PENDENTE);

		if (tarifaVigencia != null && tarifaVigencia.getStatus() != null
						&& tarifaVigencia.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
			tarifas.add(tarifaVigencia);
			obterTarifaPendentesPorUsuario(usuario, tarifas);
		}

		return !tarifas.isEmpty();
	}

	/**
	 * Obter tarifa pendentes por usuario.
	 *
	 * @param usuario
	 *            the usuario
	 * @param pendentes
	 *            the pendentes
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<TarifaVigencia> obterTarifaPendentesPorUsuario(Usuario usuario, Collection<TarifaVigencia> pendentes)
					throws GGASException {

		ControladorAlcada controladorAlcada = (ControladorAlcada) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);

		ControladorAuditoria controladorAuditoria = (ControladorAuditoria) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAuditoria.BEAN_ID_CONTROLADOR_AUDITORIA);

		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Collection<Papel> papeisUsuario = controladorPapel.consultarPapeisPorUsuario(usuario.getChavePrimaria());

		Long chaveTabelaTarifa = controladorAuditoria.consultarTabelaPorNomeDaClasse(TarifaImpl.class.getName()).getChavePrimaria();

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idTabela", chaveTabelaTarifa);

		Class<?> interfacePendente = Util.buscarInterface(Tarifa.class);

		for (Iterator<TarifaVigencia> iterator = pendentes.iterator(); iterator.hasNext();) {
			TarifaVigencia pendente = iterator.next();
			Collection<Papel> papeisAutorizados = new ArrayList<Papel>();

			for (Papel papel : papeisUsuario) {
				filtro.put("idPapel", papel.getChavePrimaria());
				Collection<Alcada> alcadasTarifa = controladorAlcada.consultarAlcada(filtro);

				for (Alcada alcada : alcadasTarifa) {
					try {

						String nomeMetodo = Util.montarNomeGet(alcada.getColuna().getNomePropriedade());
						Object valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(pendente);

						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());

							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)
											&& (pendente.getUltimaAlteracao().after(alcada.getDataInicial()) && pendente
															.getUltimaAlteracao().before(alcada.getDataFinal()))) {

								papeisAutorizados.add(papel);
							}
						}

					} catch (Exception e) {
						LOG_TARIFA_VIGENCIA.error(e);
					}
				}
			}

			if (!papeisAutorizados.isEmpty()) {
				// Se esse CreditoDebitoNegociado
				// pendente tem usuário da última
				// alteração.
				if (pendente.getUltimoUsuarioAlteracao() != null && pendente.getUltimoUsuarioAlteracao().equals(usuario)) {
					Collection<Usuario> usuarios = controladorAcesso.consultarUsuarioDosPapeis(papeisAutorizados);
					// Se existe outro usuário que
					// possa aprovar essa
					// pendência, não deixar o
					// usuário que a alterou
					// também autorizá-la.
					if (usuarios.size() > 1) {
						iterator.remove();
					}
				}
			} else {
				// Caso contrário, remove a pendência
				// da lista de retorno.
				iterator.remove();
			}
		}

		return pendentes;
	}

	/**
	 * Inserir tarifa vigencia.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void inserirTarifaVigencia(TarifaVigencia tarifaVigencia) throws GGASException {

		if (tarifaVigencia != null) {

			if ((!StringUtils.isEmpty(tarifaVigencia.getComentario()))
					&& (tarifaVigencia.getComentario().length() > LIMITE_COMENTARIO_TARIFA_VIGENCIA)) {
				throw new NegocioException(Constantes.ERRO_TAMANHO_LIMITE, new String[] {TarifaVigencia.OBSERVACAO});
			}

			ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

			if (tarifaVigencia.getDadosAuditoria() != null && tarifaVigencia.getDadosAuditoria().getUsuario() != null) {
				tarifaVigencia.setUltimoUsuarioAlteracao(tarifaVigencia.getDadosAuditoria().getUsuario());

				ServiceLocator.getInstancia().getControladorNegocio(ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

				// minha implementacao
				ControladorAlcada controladorAlcada = (ControladorAlcada) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorAlcada.BEAN_ID_CONTROLADOR_ALCADA);

				ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);

				ControladorAuditoria controladorAuditoria = (ControladorAuditoria) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorAuditoria.BEAN_ID_CONTROLADOR_AUDITORIA);

				String paramStatusAutorizada = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);

				Collection<Papel> colecaoPapeis = null;
				Collection<Alcada> colecaoAlcadas = null;
				Tabela tabela = null;
				tabela = controladorAuditoria.consultarTabelaPorNomeDaClasse(TarifaVigencia.class.getName());

				colecaoPapeis = controladorPapel.consultarPapeisPorFuncionario(tarifaVigencia.getDadosAuditoria().getUsuario()
								.getFuncionario().getChavePrimaria());

				colecaoAlcadas = controladorAlcada.obterListaAlcadasVigentes(tabela.getChavePrimaria(), colecaoPapeis, Calendar
								.getInstance().getTime());

				boolean isUnicoUsuarioAutorizado = controladorAlcada.possuiAlcadaAutorizacaoTarifaVigencia(tarifaVigencia
								.getDadosAuditoria().getUsuario(), tarifaVigencia);

				if (colecaoAlcadas != null && !colecaoAlcadas.isEmpty()) {
					if (isUnicoUsuarioAutorizado) {

						EntidadeConteudo statusAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(Long
										.valueOf(paramStatusAutorizada));
						tarifaVigencia.setStatus(statusAutorizada);
					} else {
						EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long
										.valueOf(paramStatusPendente));
						tarifaVigencia.setStatus(statusPendente);
					}
				}
			} else {
				EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
				tarifaVigencia.setStatus(statusPendente);

			}

			tarifaVigencia.validarDados();

			// Verifica se houve alteração de data
			Tarifa tarifa = tarifaVigencia.getTarifa();
			if (tarifa != null) {
				Collection<AcaoSincroniaDesconto> acoes = acoesParaSincronizacao(tarifaVigencia);
				for (AcaoSincroniaDesconto acaoSincroniaDesconto : acoes) {
					this.atualizar(modificarDesconto(acaoSincroniaDesconto), getClasseEntidadeTarifaVigenciaDesconto());
				}
				this.inserir(tarifaVigencia);
			}
		}
	}

	private TarifaVigenciaDesconto modificarDesconto(AcaoSincroniaDesconto acaoSincroniaDesconto) {
		TarifaVigenciaDesconto desconto = acaoSincroniaDesconto.getDesconto();
		switch (acaoSincroniaDesconto.getAcaoSincronia()) {
			case PASSAR:
			default:
				break;
			case DESABILITAR:
				desconto.setHabilitado(false);
				break;
			case MUDAR_FIM_VIGENCIA:
				desconto.setFimVigencia(acaoSincroniaDesconto.getData());
				break;
			case MUDAR_INICIO_VIGENCIA:
				desconto.setInicioVigencia(acaoSincroniaDesconto.getData());
				break;
		}
		return desconto;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#acoesParaSincronizacao(br.com.ggas.faturamento.tarifa.TarifaVigencia)
	 */
	@Override
	public Collection<AcaoSincroniaDesconto> acoesParaSincronizacao(TarifaVigencia tarifaVigenciaModificada) throws NegocioException {
		Map<String, Object> filtro = criarFiltroConsultas(tarifaVigenciaModificada);
		final NavigableMap<Date, TarifaVigencia> mapaDataVigencia = criarMapaVigenciaPorData(filtro);
		final NavigableMap<Date, TarifaVigencia> mapaDataVigenciaModificado =
				criarMapaVigenciaModificado(tarifaVigenciaModificada, mapaDataVigencia);
		Collection<TarifaVigenciaDesconto> descontos = consultarTarifasVigenciaDesconto(filtro);

		return CollectionUtils.collect(descontos, new Transformer() {

			@Override
			public Object transform(Object input) {
				TarifaVigenciaDesconto desconto = (TarifaVigenciaDesconto) input;
				TarifaVigencia paiInicioModificado =
						buscarMudancaPai(desconto.getInicioVigencia(), mapaDataVigencia, mapaDataVigenciaModificado);
				TarifaVigencia paiFimModificado = buscarMudancaPai(desconto.getFimVigencia(), mapaDataVigencia, mapaDataVigenciaModificado);
				return criarAcaoSincronia(desconto, paiInicioModificado, paiFimModificado);
			}

		});
	}

	private AcaoSincroniaDesconto criarAcaoSincronia(TarifaVigenciaDesconto desconto,
			TarifaVigencia paiInicioModificado, TarifaVigencia paiFimModificado) {
		boolean mudancaInicio = paiInicioModificado != null;
		boolean mudancaFim = paiFimModificado != null;

		AcaoSincronia acao = AcaoSincronia.PASSAR;
		Date novaData = null;
		if(mudancaInicio && mudancaFim) {
			acao = AcaoSincronia.DESABILITAR;
		} else if(mudancaInicio) {
			acao = AcaoSincronia.MUDAR_INICIO_VIGENCIA;
			novaData = paiInicioModificado.getDataVigencia();
		} else if(mudancaFim) {
			acao = AcaoSincronia.MUDAR_FIM_VIGENCIA;
			novaData = DateUtils.addDays(paiFimModificado.getDataVigencia(),-1);
		}
		return FabricaAcaoSincroniaDesconto.criar(novaData, acao, desconto);
	}

	private TarifaVigencia buscarMudancaPai(Date dataDesconto,
			NavigableMap<Date, TarifaVigencia> mapaDataVigencia,
			NavigableMap<Date, TarifaVigencia> mapaDataVigenciaModificado) {
		TarifaVigencia pai = buscarPaiDesconto(dataDesconto, mapaDataVigencia);
		TarifaVigencia paiModificado = buscarPaiDesconto(dataDesconto, mapaDataVigenciaModificado);
		if(Objects.equals(pai,paiModificado)) {
			return null;
		} else if(paiModificado == null) {
			return mapaDataVigenciaModificado.get(mapaDataVigenciaModificado.firstKey());
		} else {
			return paiModificado;
		}
	}

	private TarifaVigencia buscarPaiDesconto(Date dataDesconto, NavigableMap<Date, TarifaVigencia> mapaDataVigencia) {
		NavigableMap<Date, TarifaVigencia> headMapa = mapaDataVigencia.headMap(dataDesconto, true);
		if(headMapa.isEmpty()) {
			return null;
		} else {
			return headMapa.get(headMapa.lastKey());
		}
	}

	private NavigableMap<Date, TarifaVigencia> criarMapaVigenciaModificado(TarifaVigencia tarifaVigenciaModificada,
			NavigableMap<Date, TarifaVigencia> mapaDataVigencia) {
		NavigableMap<Date, TarifaVigencia> mapaDataVigenciaModificado = modificarMapaVigencias(tarifaVigenciaModificada,
				mapaDataVigencia);
		mapaDataVigenciaModificado.put(tarifaVigenciaModificada.getDataVigencia(), tarifaVigenciaModificada);
		return mapaDataVigenciaModificado;
	}

	private NavigableMap<Date, TarifaVigencia> modificarMapaVigencias(TarifaVigencia tarifaVigenciaNova,
			NavigableMap<Date, TarifaVigencia> mapaDataVigencia) {
		NavigableMap<Date, TarifaVigencia> mapaDataVigenciaModificado = new TreeMap<Date, TarifaVigencia>(mapaDataVigencia);
		Iterator<Entry<Date, TarifaVigencia>> entradas = mapaDataVigenciaModificado.entrySet().iterator();
		while(entradas.hasNext()) {
			TarifaVigencia vigencia = entradas.next().getValue();
			if(vigencia.getChavePrimaria() == tarifaVigenciaNova.getChavePrimaria()) {
				entradas.remove();
			}
		}
		return mapaDataVigenciaModificado;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private NavigableMap<Date, TarifaVigencia> criarMapaVigenciaPorData(Map<String, Object> filtro)
			throws NegocioException {
		Collection<TarifaVigencia> vigencias = consultarTarifasVigencia(filtro);
		Map<Date, TarifaVigencia> mapaVigenciaPorData = Maps.uniqueIndex(vigencias, new Function(){

			@Override
			public Object apply(Object input) {
				TarifaVigencia vig = (TarifaVigencia) input;
				return vig.getDataVigencia();
			}

		});
		return new TreeMap<Date, TarifaVigencia>(mapaVigenciaPorData);
	}

	private Map<String, Object> criarFiltroConsultas(TarifaVigencia tarifaVigenciaModificada) {
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idTarifa", tarifaVigenciaModificada.getTarifa().getChavePrimaria());
		filtro.put("habilitado", true);
		return filtro;
	}



	@Override
	public List<TarifaVigenciaDesconto> montarListaTarifaVigenciaDescontoNaInsercao(TarifaVigencia tarifaVigencia)
					throws ConcorrenciaException, NegocioException {

		List<TarifaVigenciaDesconto> retorno = new ArrayList<TarifaVigenciaDesconto>();
		Tarifa tarifa = tarifaVigencia.getTarifa();
		TarifaVigencia tarifaVigenciaAnterior = consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(),
						tarifaVigencia.getDataVigencia());
		Date dataVigenciaNova = tarifaVigencia.getDataVigencia();
		// Verifica se existem descontos
		// no intervalo para inativar
		if (tarifaVigenciaAnterior != null) {
			List<TarifaVigenciaDesconto> listaTVD = consultarTarifasVigenciaDescontoPorTarifaVigencia(tarifaVigenciaAnterior
							.getChavePrimaria());
			if (listaTVD != null) {
				for (TarifaVigenciaDesconto tvd : listaTVD) {
					if (tvd.getInicioVigencia().compareTo(dataVigenciaNova) >= 0) {
						tvd.setHabilitado(Boolean.FALSE);
						tvd.setDadosAuditoria(tarifa.getDadosAuditoria());
						retorno.add(tvd);
					} else if (tvd.getInicioVigencia().compareTo(dataVigenciaNova) < 0
									&& tvd.getFimVigencia().compareTo(dataVigenciaNova) >= 0) {
						DateTime fimVigencia = new DateTime(dataVigenciaNova);
						fimVigencia = fimVigencia.minusDays(1);
						tvd.setFimVigencia(fimVigencia.toDate());
						tvd.setDadosAuditoria(tarifa.getDadosAuditoria());
						retorno.add(tvd);
					}
				}

			}
		}
		return retorno;
	}
	/**
	 * Atualizar tarifa vigencia.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private Collection<TarifaVigenciaDesconto> atualizarTarifaVigencia(TarifaVigencia tarifaVigencia) throws NegocioException,
					ConcorrenciaException {

		Collection<TarifaVigenciaDesconto> retorno = new ArrayList<TarifaVigenciaDesconto>();

		if (tarifaVigencia != null) {

			if (tarifaVigencia.getDadosAuditoria() != null && tarifaVigencia.getDadosAuditoria().getUsuario() != null) {
				tarifaVigencia.setUltimoUsuarioAlteracao(tarifaVigencia.getDadosAuditoria().getUsuario());
			}

			ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

			if (tarifaVigencia.getDadosAuditoria() != null && tarifaVigencia.getDadosAuditoria().getUsuario() != null) {
				tarifaVigencia.setUltimoUsuarioAlteracao(tarifaVigencia.getDadosAuditoria().getUsuario());
				ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

				boolean isUnicoUsuarioAutorizado = controladorAcesso.verificarUnicoUsuarioAutorizado(tarifaVigencia
								.getUltimoUsuarioAlteracao());

				if (isUnicoUsuarioAutorizado) {
					String paramStatusAutorizada = controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
					EntidadeConteudo statusAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(Long
									.valueOf(paramStatusAutorizada));
					tarifaVigencia.setStatus(statusAutorizada);
				} else {
					EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
					tarifaVigencia.setStatus(statusPendente);
				}
			} else {
				EntidadeConteudo statusPendente = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(paramStatusPendente));
				tarifaVigencia.setStatus(statusPendente);
			}

			if ((!StringUtils.isEmpty(tarifaVigencia.getComentario()))
					&& (tarifaVigencia.getComentario().length() > LIMITE_COMENTARIO_TARIFA_VIGENCIA)) {
				throw new NegocioException(Constantes.ERRO_TAMANHO_LIMITE, new String[] { TarifaVigencia.OBSERVACAO });
			}

			tarifaVigencia.validarDados();

			// Verifica se houve alteração de data
			Tarifa tarifa = tarifaVigencia.getTarifa();
			if (tarifa != null) {
				Collection<AcaoSincroniaDesconto> acoes = acoesParaSincronizacao(tarifaVigencia);
				for (AcaoSincroniaDesconto acaoSincroniaDesconto : acoes) {
					this.atualizar(modificarDesconto(acaoSincroniaDesconto), getClasseEntidadeTarifaVigenciaDesconto());
				}

				this.atualizar(tarifaVigencia, this.getClasseEntidadeTarifaVigencia());
				tarifaVigencia.getTarifaVigenciaFaixas().clear();
				tarifaVigencia.getTarifaVigenciaFaixas().addAll(this.consultarTarifasVigenciaFaixa(tarifaVigencia.getChavePrimaria()));
			}
		}
		return retorno;
	}

	/**
	 * Método responsável por montar uma lista de {@link TarifaVigenciaDesconto} para atualização.
	 * @param tarifaVigencia - {@link TarifaVigencia}
	 * @return lista de {@link TarifaVigenciaDesconto} - {@link List}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public List<TarifaVigenciaDesconto> montarListaTarifaVigenciaDescontoNaAtualizacao(TarifaVigencia tarifaVigencia)
					throws ConcorrenciaException, NegocioException {

		Tarifa tarifa = tarifaVigencia.getTarifa();
		TarifaVigencia tarifaVigenciaOriginal = obterTarifaVigencia(tarifaVigencia.getChavePrimaria());
		TarifaVigencia tarifaVigenciaAnterior = consultarTarifaVigenciaAnteriorMaisRecente(tarifa.getChavePrimaria(),
						tarifaVigencia.getDataVigencia());
		Date dataVigenciaOriginal = tarifaVigenciaOriginal.getDataVigencia();
		Date dataVigenciaNova = tarifaVigencia.getDataVigencia();
		List<TarifaVigenciaDesconto> retorno = new ArrayList<TarifaVigenciaDesconto>();
		if (dataVigenciaNova.compareTo(dataVigenciaOriginal) != 0) {
			List<TarifaVigenciaDesconto> listaTVD = null;
			if (dataVigenciaNova.compareTo(dataVigenciaOriginal) > 0) {
				if (tarifaVigenciaOriginal != null) {
					listaTVD = consultarTarifasVigenciaDescontoPorTarifaVigencia(tarifaVigenciaOriginal.getChavePrimaria());
				}
				if (listaTVD != null) {
					for (TarifaVigenciaDesconto tvd : listaTVD) {
						if (tvd.getFimVigencia().compareTo(dataVigenciaNova) < 0) {
							tvd.setHabilitado(Boolean.FALSE);
							tvd.setDadosAuditoria(tarifa.getDadosAuditoria());
							retorno.add(tvd);
						} else if (tvd.getInicioVigencia().compareTo(dataVigenciaNova) < 0
										&& tvd.getFimVigencia().compareTo(dataVigenciaNova) >= 0) {
							tvd.setInicioVigencia(dataVigenciaNova);
							tvd.setDadosAuditoria(tarifa.getDadosAuditoria());
							retorno.add(tvd);
						} else {
							this.getHibernateTemplate().getSessionFactory().getCurrentSession().evict(tvd);
						}
					}
				}
			} else if (dataVigenciaNova.compareTo(dataVigenciaOriginal) < 0) {
				if (tarifaVigenciaAnterior != null) {
					listaTVD = consultarTarifasVigenciaDescontoPorTarifaVigencia(tarifaVigenciaAnterior.getChavePrimaria());
				}
				if (listaTVD != null) {
					for (TarifaVigenciaDesconto tvd : listaTVD) {
						if (tvd.getInicioVigencia().compareTo(dataVigenciaNova) >= 0) {
							tvd.setHabilitado(Boolean.FALSE);
							tvd.setDadosAuditoria(tarifa.getDadosAuditoria());
							retorno.add(tvd);
						} else if (tvd.getInicioVigencia().compareTo(dataVigenciaNova) < 0
										&& tvd.getFimVigencia().compareTo(dataVigenciaNova) >= 0) {
							DateTime fimVigencia = new DateTime(dataVigenciaNova);
							fimVigencia = fimVigencia.minusDays(1);
							tvd.setFimVigencia(fimVigencia.toDate());
							tvd.setDadosAuditoria(tarifa.getDadosAuditoria());
							retorno.add(tvd);
						} else {
							this.getHibernateTemplate().getSessionFactory().getCurrentSession().evict(tvd);
						}
					}
				}
			}
		}

		this.getHibernateTemplate().getSessionFactory().getCurrentSession().evict(tarifaVigenciaOriginal);
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasVigenciaDescontoPorTarifaVigencia(long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaVigenciaDesconto> consultarTarifasVigenciaDescontoPorTarifaVigencia(long chaveTarifaVigencia) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaDesconto().getSimpleName());
		hql.append(" tvd ");
		hql.append(" where tvd.habilitado = true ");
		hql.append(" and tvd.tarifaVigencia.chavePrimaria = :chaveTarifaVigencia ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveTarifaVigencia", chaveTarifaVigencia);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifaVigenciaAnteriorMaisRecente(long, java.util.Date)
	 */
	@Override
	public TarifaVigencia consultarTarifaVigenciaAnteriorMaisRecente(long chaveTarifa, Date dataVigencia) {

		Date dataVigenciaParametro = dataVigencia;
		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tv ");
		hql.append(" where tv.habilitado = true ");
		hql.append(" and tv.tarifa.chavePrimaria = :chaveTarifa ");
		hql.append(" and tv.dataVigencia = ( ");
		hql.append(" select ");
		hql.append(" max(tavi.dataVigencia) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tavi ");
		hql.append(" where tavi.habilitado = true ");
		hql.append(" and tavi.tarifa.chavePrimaria = :chaveTarifa");
		hql.append(" and tavi.dataVigencia < :dataVigencia");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveTarifa", chaveTarifa);
		if (dataVigenciaParametro == null) {
			dataVigenciaParametro = Calendar.getInstance().getTime();
		}

		query.setParameter(DATA_VIGENCIA, dataVigenciaParametro);

		return (TarifaVigencia) query.uniqueResult();
	}

	/**
	 * Validar tarifa vigencia desconto.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto, List<DadosFaixasTarifa> listaDadosFaixa)
					throws NegocioException {

		Map<String, Object> erros = tarifaVigenciaDesconto.validarDados();

		StringBuilder errosDesconto = new StringBuilder();
		if (!erros.isEmpty()) {
			errosDesconto.append((String) erros.get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS));
		}
		for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixa) {
			if (dadosFaixasTarifa.getTipoDesconto() == null) {
				if (errosDesconto.length() > 0) {
					errosDesconto.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				errosDesconto.append(Tarifa.TARIFA_TIPO_DESCONTO);
				break;
			}
		}

		if (errosDesconto.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, errosDesconto.toString());
			throw new NegocioException(erros);
		}

		if ((!StringUtils.isEmpty(tarifaVigenciaDesconto.getDescricaoDescontoVigencia()))
				&& (tarifaVigenciaDesconto.getDescricaoDescontoVigencia().length() > CONSTANTE_DUZENTOS_CIQUENTA_CINCO)) {
			throw new NegocioException(Constantes.ERRO_TAMANHO_LIMITE, new String[] { TarifaVigenciaDesconto.DESCRICAO });
		}
	}

	/**
	 * Valida dados inserir tarifa.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private void validaDadosInserirTarifa(TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws NegocioException, FormatoInvalidoException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		if (tarifaVigenciaDesconto != null && tarifaVigenciaDesconto.getInicioVigencia() != null
						&& tarifaVigenciaDesconto.getFimVigencia() != null) {

			if (!listaDadosFaixa.isEmpty()) {
				Long idTipoPercentual = Long.valueOf(controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL));

				int qtdLinhasVigenciaFaixa = 0;
				if (tarifaVigencia != null && tarifaVigencia.getTarifaVigenciaFaixas() != null) {
					qtdLinhasVigenciaFaixa = tarifaVigencia.getTarifaVigenciaFaixas().size();
				}

				if (qtdLinhasVigenciaFaixa == 0) {
					throw new NegocioException(ERRO_NAO_EXISTE_FAIXA_DE_VIGENCIA_PREENCHIDA, true);
				}

				for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixa) {
					String valorFixo = dadosFaixasTarifa.getDescontoFixoSemImposto();

					// Validando se o valor é
					// diferente, maior que zero
					if (StringUtils.isEmpty(valorFixo)
									|| Util.converterCampoStringParaValorBigDecimal("Valor Fixo", valorFixo, Constantes.FORMATO_VALOR_BR,
													Constantes.LOCALE_PADRAO).compareTo(BigDecimal.ZERO) < 0) {

						valorFixo = String.valueOf(BigDecimal.ZERO);
						dadosFaixasTarifa.setDescontoFixoSemImposto(String.valueOf(BigDecimal.ZERO));
					}

					// Validando se foi escolhido
					// alguma opção
					if (dadosFaixasTarifa.getTipoDesconto() == null || dadosFaixasTarifa.getTipoDesconto() == 0) {
						throw new NegocioException(ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE, true);
					}

					// Validando se for do tipo
					// VALOR o valor não pode ser
					// maior que o valor da
					// vigencia
					if (dadosFaixasTarifa.getTipoDesconto().compareTo(idTipoPercentual) != 0) {
						BigDecimal valorFixoSemImp = Util.converterCampoStringParaValorBigDecimal("Valor Fixo Sem Imposto",
										dadosFaixasTarifa.getValorFixoSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
						BigDecimal descontoFixoSemImp = Util.converterCampoStringParaValorBigDecimal("Desconto Fixo Sem Imposto",
										dadosFaixasTarifa.getDescontoFixoSemImposto(), Constantes.FORMATO_VALOR_BR,
										Constantes.LOCALE_PADRAO);

						if (valorFixoSemImp.compareTo(descontoFixoSemImp) < 0) {
							throw new NegocioException(
											ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE_VALOR_MAIOR_DO_QUE_FAIXA_VIGENCIA,
											true);
						}
					}

					// Validando se for do tipo
					// PERCENTUAL o valor não pode
					// ser
					// maior que 99
					if (dadosFaixasTarifa.getTipoDesconto().compareTo(idTipoPercentual) == 0) {
						BigDecimal valorFixoDesconto = Util.converterCampoStringParaValorBigDecimal(
										TarifaFaixaDesconto.TARIFA_FAIXA_DESCONTO_VALOR_FIXO,
										dadosFaixasTarifa.getDescontoFixoSemImposto(), Constantes.FORMATO_VALOR_NUMERO,
										Constantes.LOCALE_PADRAO);

						if (valorFixoDesconto.compareTo(new BigDecimal(CONSTANTE_NOVENTA_NOVE)) > 0) {
							throw new NegocioException(ERRO_O_VALOR_DA_FAIXA_DESCONTO_NAO_PODE_SER_MAIOR_QUE_99, true);
						}
					}

					// Validando se o valor é
					// diferente e maior que zero
					// e menor
					// do que 0
					String valorVariavel = dadosFaixasTarifa.getDescontoVariavelSemImposto();
					if (StringUtils.isEmpty(valorVariavel)
									|| Util.converterCampoStringParaValorBigDecimal("Valor Variável", valorVariavel,
													Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO).compareTo(BigDecimal.ZERO) < 0) {

						valorVariavel = String.valueOf(BigDecimal.ZERO);
						dadosFaixasTarifa.setDescontoVariavelSemImposto(String.valueOf(BigDecimal.ZERO));
					}


					this.validarTipoValor(idTipoPercentual, dadosFaixasTarifa);

					this.validarTipoPercentual(dadosFaixasTarifa, idTipoPercentual);
				}
			} else {
				throw new NegocioException(ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS, true);
			}

		}

	}

	private void validarTipoValor(Long idTipoPercentual, DadosFaixasTarifa dadosFaixasTarifa) throws FormatoInvalidoException,
			NegocioException {
		// Validando se for do tipo
		// VALOR o valor não pode ser
		// maior
		// que o valor da vigencia

		if (dadosFaixasTarifa.getTipoDesconto().compareTo(idTipoPercentual) != 0) {

			BigDecimal valorVarSemImp =
					Util.converterCampoStringParaValorBigDecimal("Valor Variável Sem Imposto",
							dadosFaixasTarifa.getValorVariavelSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

			BigDecimal descontoVarSemImp =
					Util.converterCampoStringParaValorBigDecimal("Desconto Variável Sem Imposto",
							dadosFaixasTarifa.getDescontoVariavelSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);

			if (valorVarSemImp.compareTo(descontoVarSemImp) < 0) {
				throw new NegocioException(ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE_VALOR_MAIOR_DO_QUE_FAIXA_VIGENCIA, true);
			}
		}
	}

	private void validarTipoPercentual(DadosFaixasTarifa dadosFaixasTarifa, Long idTipoPercentual) throws NegocioException,
			FormatoInvalidoException {
		// Validando se for do tipo
		// percentual o valor não pode
		// ser
		// maior que 99
		if (dadosFaixasTarifa.getTipoDesconto().compareTo(idTipoPercentual) == 0) {
			BigDecimal valorVariavelDesconto =
					Util.converterCampoStringParaValorBigDecimal(TarifaFaixaDesconto.TARIFA_FAIXA_DESCONTO_VALOR_FIXO,
							dadosFaixasTarifa.getDescontoVariavelSemImposto(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

			if (valorVariavelDesconto.compareTo(new BigDecimal(CONSTANTE_NOVENTA_NOVE)) > 0) {
				throw new NegocioException(ERRO_O_VALOR_DA_FAIXA_DESCONTO_NAO_PODE_SER_MAIOR_QUE_99, true);
			}
		}
	}

	/**
	 * Valida se nao tem faixa desconto preenchida.
	 *
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @return true, if successful
	 */
	private boolean validaSeNaoTemFaixaDescontoPreenchida(List<DadosFaixasTarifa> listaDadosFaixa) {

		boolean retorno = false;
		if (listaDadosFaixa != null && !listaDadosFaixa.isEmpty()) {
			for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixa) {
				if (!StringUtils.isEmpty(dadosFaixasTarifa.getDescontoFixoSemImposto())
								|| !StringUtils.isEmpty(dadosFaixasTarifa.getDescontoVariavelSemImposto())) {
					retorno = true;
					break;
				}
			}
		}
		return retorno;
	}

	/**
	 * Validar data tarifa desconto.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDataTarifaDesconto(TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto)
					throws NegocioException {

		if (tarifaVigencia.getDataVigencia() != null && tarifaVigenciaDesconto.getInicioVigencia() != null
						&& tarifaVigenciaDesconto.getFimVigencia() != null) {

			if (tarifaVigenciaDesconto.getInicioVigencia().compareTo(tarifaVigenciaDesconto.getFimVigencia()) > 0) {
				throw new NegocioException(ERRO_TARIFA_DATA_INICIO_DESCONTO_MAIOR_DATA_FIM_DESCONTO, true);
			}

			if (tarifaVigencia.getDataVigencia().compareTo(tarifaVigenciaDesconto.getInicioVigencia()) > 0) {
				throw new NegocioException(ERRO_TARIFA_DATA_INICIO_VIGENCIA_MAIOR_DATA_INICIO_DESCONTO, true);
			}

			if (tarifaVigencia.getTarifaVigenciaFaixas() != null && tarifaVigencia.getTarifaVigenciaFaixas().isEmpty()) {
				throw new NegocioException(ERRO_TARIFA_IMPOSSIVEL_CADASTRAR_DESCONTO_SEM_FAIXAS_VIGENCIAS, true);
			}
		}
	}

	/**
	 * Valida se vai inserir tarifa vigencia.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @return true, if successful
	 */
	private boolean validaSeVaiInserirTarifaVigencia(TarifaVigencia tarifaVigencia) {

		boolean retorno = true;
		if ((tarifaVigencia.getDataVigencia() == null) && (tarifaVigencia.getTipoCalculo() == null)
						&& (tarifaVigencia.getBaseApuracao() == null)
						&& (tarifaVigencia.getUnidadeMonetaria() == null && tarifaVigencia.getComentario() == null)) {

			retorno = false;
		}
		return retorno;
	}

	/**
	 * Valida se vai inserir tarifa vigencia desconto.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @return true, if successful
	 */
	private boolean validaSeVaiInserirTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto) {

		boolean retorno = true;
		if ((tarifaVigenciaDesconto.getInicioVigencia() == null) && (tarifaVigenciaDesconto.getFimVigencia() == null)
						&& (StringUtils.isBlank(tarifaVigenciaDesconto.getDescricaoDescontoVigencia()))) {

			retorno = false;
		}
		return retorno;
	}

	/**
	 * Valida se foi preenchido tarifa vigencia desconto.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @return true, if successful
	 */
	private boolean validaSeFoiPreenchidoTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto) {

		boolean retorno = false;
		if ((tarifaVigenciaDesconto.getInicioVigencia() != null) || (tarifaVigenciaDesconto.getFimVigencia() != null)
						|| (tarifaVigenciaDesconto.getDescricaoDescontoVigencia() != null)) {

			retorno = true;
		}
		return retorno;
	}

	/**
	 * Popular tarifa faixa desconto.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private Collection<TarifaFaixaDesconto> popularTarifaFaixaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto,
					TarifaVigencia tarifaVigencia, List<DadosFaixasTarifa> listaDadosFaixa) throws NegocioException,
					FormatoInvalidoException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<TarifaFaixaDesconto> lstTarifaFaixaDesconto = new HashSet<TarifaFaixaDesconto>();
		Collection<TarifaVigenciaFaixa> lstTarifaVigenciaFaixa = null;

		if (!listaDadosFaixa.isEmpty()) {

			Long idTipoPercentual = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL));

			lstTarifaVigenciaFaixa = this.consultarTarifasVigenciaFaixa(tarifaVigencia.getChavePrimaria());

			int i = 0;

			for (TarifaVigenciaFaixa tarifaVigenciaFaixa : lstTarifaVigenciaFaixa) {

				DadosFaixasTarifa dadosFaixasTarifa = listaDadosFaixa.get(i);

				if (!StringUtils.isEmpty(dadosFaixasTarifa.getDescontoFixoSemImposto()) && dadosFaixasTarifa.getTipoDesconto() != null
								&& !StringUtils.isEmpty(dadosFaixasTarifa.getDescontoVariavelSemImposto())) {

					BigDecimal valorPercentualFixo = Util.converterCampoStringParaValorBigDecimal("Valor Percentual Fixo",
									dadosFaixasTarifa.getDescontoFixoSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
					BigDecimal valorPercentualVariavel = Util.converterCampoStringParaValorBigDecimal("Valor Percentual Variável",
									dadosFaixasTarifa.getDescontoVariavelSemImposto(), Constantes.FORMATO_VALOR_BR,
									Constantes.LOCALE_PADRAO);

					TarifaFaixaDesconto tarifaFaixaDesconto = (TarifaFaixaDesconto) this.criarTarifaFaixaDesconto();

					Long tipoDesconto = dadosFaixasTarifa.getTipoDesconto();

					this.adicionarValoresDesconto(tarifaFaixaDesconto, tipoDesconto, idTipoPercentual, valorPercentualFixo, valorPercentualVariavel);

					tarifaFaixaDesconto.setTarifaVigenciaDesconto(tarifaVigenciaDesconto);
					tarifaFaixaDesconto.setTarifaVigenciaFaixa(tarifaVigenciaFaixa);
					tarifaFaixaDesconto.setHabilitado(true);
					tarifaFaixaDesconto.setUltimaAlteracao(Calendar.getInstance().getTime());

					lstTarifaFaixaDesconto.add(tarifaFaixaDesconto);
				}
				i++;
			}
		}
		return lstTarifaFaixaDesconto;
	}

	private void adicionarValoresDesconto(TarifaFaixaDesconto tarifaFaixaDesconto, Long tipoDesconto, Long idTipoPercentual,
			BigDecimal valorPercentualFixo, BigDecimal valorPercentualVariavel) {

		if ((tipoDesconto != null) && (tipoDesconto.equals(idTipoPercentual))) {
			tarifaFaixaDesconto.setPercentualDescontoFixo(valorPercentualFixo.divide(BigDecimal.valueOf(CONSTANTE_NUMERO_OITO)));
			tarifaFaixaDesconto.setPercentualDescontoVariavel(valorPercentualVariavel.divide(BigDecimal.valueOf(CONSTANTE_NUMERO_OITO)));

			tarifaFaixaDesconto.setValorDescontoFixo(null);
			tarifaFaixaDesconto.setValorDescontoVariavel(null);
		} else {
			tarifaFaixaDesconto.setValorDescontoFixo(valorPercentualFixo);
			tarifaFaixaDesconto.setValorDescontoVariavel(valorPercentualVariavel);

			tarifaFaixaDesconto.setPercentualDescontoFixo(null);
			tarifaFaixaDesconto.setPercentualDescontoVariavel(null);
		}
	}

	/**
	 * Popular tarifa faixa desconto novo cadastro.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private Collection<TarifaFaixaDesconto> popularTarifaFaixaDescontoNovoCadastro(TarifaVigenciaDesconto tarifaVigenciaDesconto,
					TarifaVigencia tarifaVigencia, List<DadosFaixasTarifa> listaDadosFaixa) throws FormatoInvalidoException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<TarifaFaixaDesconto> lstTarifaFaixaDesconto = new HashSet<TarifaFaixaDesconto>();

		Long idTipoPercentual = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL));

		this.popularListaFaixaDesconto(tarifaVigenciaDesconto, tarifaVigencia, listaDadosFaixa, lstTarifaFaixaDesconto, idTipoPercentual);

		return lstTarifaFaixaDesconto;
	}

	/**
	 * Popular tarifa faixa desconto alteracao.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private Map<String, Collection<TarifaFaixaDesconto>> popularTarifaFaixaDescontoAlteracao(TarifaVigenciaDesconto tarifaVigenciaDesconto,
					TarifaVigencia tarifaVigencia, List<DadosFaixasTarifa> listaDadosFaixa) throws NegocioException,
					FormatoInvalidoException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Map<String, Collection<TarifaFaixaDesconto>> mapa = new HashMap<String, Collection<TarifaFaixaDesconto>>();

		Collection<TarifaFaixaDesconto> lstTarifaFaixaDesconto = new HashSet<TarifaFaixaDesconto>();
		List<TarifaFaixaDesconto> lstTarifaFaixaDescontoBD = this.consultarTarifasFaixaDesconto(tarifaVigenciaDesconto.getChavePrimaria());
		mapa.put("lstTarifaFaixaDescontoBD", lstTarifaFaixaDescontoBD);

		if (lstTarifaFaixaDescontoBD != null && !lstTarifaFaixaDescontoBD.isEmpty()) {
			Long idTipoPercentual = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL));

			this.popularListaFaixaDesconto(tarifaVigenciaDesconto, tarifaVigencia, listaDadosFaixa, lstTarifaFaixaDesconto,
							idTipoPercentual);
		}
		mapa.put("lstTarifaFaixaDesconto", lstTarifaFaixaDesconto);
		return mapa;
	}

	/**
	 * Popular lista faixa desconto.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @param lstTarifaFaixaDesconto
	 *            the lst tarifa faixa desconto
	 * @param idTipoPercentual
	 *            the id tipo percentual
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private void popularListaFaixaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto, TarifaVigencia tarifaVigencia,
					List<DadosFaixasTarifa> listaDadosFaixa, Collection<TarifaFaixaDesconto> lstTarifaFaixaDesconto, Long idTipoPercentual)
					throws FormatoInvalidoException {

		List<TarifaVigenciaFaixa> listaTVF = new ArrayList<TarifaVigenciaFaixa>(tarifaVigencia.getTarifaVigenciaFaixas());

		// Ordenação da lista de
		// tarifaVigenciaFaixa
		Collections.sort(listaTVF, new Comparator<TarifaVigenciaFaixa>(){

			@Override
			public int compare(TarifaVigenciaFaixa o1, TarifaVigenciaFaixa o2) {

				return o1.getMedidaInicio().compareTo(o2.getMedidaInicio());
			}
		});

		int count = 0;
		for (DadosFaixasTarifa dados : listaDadosFaixa) {
			if ((count < listaDadosFaixa.size()) && (count < listaTVF.size())) {
				TarifaVigenciaFaixa tvf = listaTVF.get(count);
				this.popularListaFaixaDescontoParaAlteracao(tarifaVigenciaDesconto, lstTarifaFaixaDesconto, idTipoPercentual, tvf, dados);
				count++;
			}
		}
	}

	/**
	 * Popular lista faixa desconto para alteracao.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param lstTarifaFaixaDesconto
	 *            the lst tarifa faixa desconto
	 * @param idTipoPercentual
	 *            the id tipo percentual
	 * @param tvf
	 *            the tvf
	 * @param dados
	 *            the dados
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private void popularListaFaixaDescontoParaAlteracao(TarifaVigenciaDesconto tarifaVigenciaDesconto,
					Collection<TarifaFaixaDesconto> lstTarifaFaixaDesconto, Long idTipoPercentual, TarifaVigenciaFaixa tvf,
					DadosFaixasTarifa dados) throws FormatoInvalidoException {

		TarifaFaixaDesconto tfDesconto = (TarifaFaixaDesconto) this.criarTarifaFaixaDesconto();
		Long tipoDesconto = dados.getTipoDesconto();
		if ((tipoDesconto != null) && (tipoDesconto.equals(idTipoPercentual))) {

			BigDecimal valorPercentualFixo = Util.converterCampoStringParaValorBigDecimal("Valor Percentual Fixo",
							dados.getDescontoFixoSemImposto(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			tfDesconto.setPercentualDescontoFixo(Util.converterBigDecimalParaPercentual(valorPercentualFixo));

			BigDecimal valorPercentualVariavel = Util.converterCampoStringParaValorBigDecimal("Valor Percentual Variável",
							dados.getDescontoVariavelSemImposto(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			tfDesconto.setPercentualDescontoVariavel(Util.converterBigDecimalParaPercentual(valorPercentualVariavel));

			tfDesconto.setValorDescontoFixo(null);
			tfDesconto.setValorDescontoVariavel(null);
		} else {

			BigDecimal valorDescontoFixo = Util.converterCampoStringParaValorBigDecimal("Valor Percentual Fixo",
							dados.getDescontoFixoSemImposto(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			tfDesconto.setValorDescontoFixo(valorDescontoFixo);

			BigDecimal valorDescontoVariavel = Util.converterCampoStringParaValorBigDecimal("Valor Percentual Variável",
							dados.getDescontoVariavelSemImposto(), Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			tfDesconto.setValorDescontoVariavel(valorDescontoVariavel);

			tfDesconto.setPercentualDescontoFixo(null);
			tfDesconto.setPercentualDescontoVariavel(null);
		}

		tfDesconto.setTarifaVigenciaFaixa(tvf);
		tfDesconto.setTarifaVigenciaDesconto(tarifaVigenciaDesconto);
		tfDesconto.setHabilitado(true);
		tfDesconto.setUltimaAlteracao(Calendar.getInstance().getTime());

		lstTarifaFaixaDesconto.add(tfDesconto);
	}

	/**
	 * obter Tarifa Vigencia Pesquisar.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public Collection<TarifaVigenciaPesquisar> obterTarifaVigenciaPesquisar(Map<String, Object> filtro)
			throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT new br.com.ggas.faturamento.tarifa.impl.TarifaVigenciaPesquisarImpl( ");

		// Indicador de vigência
		hql.append(" CASE WHEN (SELECT min(tarVig.dataVigencia) FROM  ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append("         tarVig WHERE tarifa.chavePrimaria = tarVig.tarifa.chavePrimaria AND tarVig.dataVigencia > "
				+ "tarifaVigencia.dataVigencia) < :currentDate THEN 'False' ELSE 'True' END AS VIGENTE,  ");

		// Chave primária, descrição, descrição
		// abreviada e indicador de habilitado
		hql.append("    tarifa.chavePrimaria, tarifa.descricao, tarifa.descricaoAbreviada, tarifa.habilitado,  ");

		// Segmento
		hql.append("    segmento.descricao, ");

		// Data de início da vigência
		hql.append("    TO_CHAR(tarifaVigencia.dataVigencia, 'dd/mm/yyyy') As dataVigencia, ");

		// Data de fim da vigência
		hql.append("    (SELECT TO_CHAR(min(tarVig.dataVigencia), 'dd/mm/yyyy') FROM  ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tarVig WHERE tarifa.chavePrimaria = tarVig.tarifa.chavePrimaria AND tarVig.dataVigencia >"
				+ " tarifaVigencia.dataVigencia) as DATA_FIM ) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append("    tarifaVigencia, ");
		hql.append(getClasseEntidadeTarifa().getSimpleName());
		hql.append("    tarifa, ");
		hql.append(getClasseEntidadeSegmento().getSimpleName());
		hql.append("    segmento, ");
		hql.append(getClasseEntidadeConteudo().getSimpleName());
		hql.append("    entidadeConteudo ");
		hql.append(" WHERE tarifa.chavePrimaria = tarifaVigencia.tarifa.chavePrimaria ");
		hql.append(" AND tarifa.segmento.chavePrimaria = segmento.chavePrimaria ");

		if (filtro != null) {
			String descricaoTarifa = (String) filtro.get("descricaoTarifa");
			if (!StringUtils.isEmpty(descricaoTarifa)) {
				hql.append(" AND LOWER(tarifa.descricao) like (:DESCRICAO)");
			}

			String descricaoAbreviadaTarifa = (String) filtro.get("descricaoAbreviadaTarifa");
			if (!StringUtils.isEmpty(descricaoAbreviadaTarifa)) {
				hql.append(" AND LOWER(tarifa.descricaoAbreviada) like (:DESCRICAO_ABREVIADA)");
			}

			Date dataVigencia = (Date) filtro.get(DATA_VIGENCIA);
			if (dataVigencia != null) {
				hql.append(" AND tarifaVigencia.dataVigencia <= :DATA_VIGENCIA ");
			}

			Long idSegmento = (Long) filtro.get("idSegmento");
			if (idSegmento != null) {
				hql.append(" AND segmento.chavePrimaria = :ID_SEGMENTO ");
			}

			Long idItemFatura = (Long) filtro.get("idItemFatura");
			if (idItemFatura != null) {
				hql.append(" AND tarifa.itemFatura.chavePrimaria = :ID_ITEM_FATURA ");
			}

			Long tipoContrato = (Long) filtro.get("tipoContrato");
			if (tipoContrato != null) {
				hql.append(" AND tarifa.tipoContrato.chavePrimaria = entidadeConteudo.chavePrimaria ");
				hql.append(" AND tarifa.tipoContrato.chavePrimaria = :ID_TIPO_CONTRATO ");
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				hql.append(" AND tarifa.habilitado = :HABILITADO ");
			}

			Long status = (Long) filtro.get("status");
			if (status != null) {
				hql.append(" AND tarifaVigencia.status.chavePrimaria = :STATUS ");
			}

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				hql.append(" AND tarifa.chavePrimaria in (:CHAVES_PRIMARIAS) ");
			}
		}

		hql.append(" ORDER BY tarifa.segmento.descricao, tarifa.descricao, tarifaVigencia.dataVigencia ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setDate("currentDate", Calendar.getInstance().getTime());

		if (filtro != null) {
			String descricaoTarifa = (String) filtro.get("descricaoTarifa");
			if (!StringUtils.isEmpty(descricaoTarifa)) {
				query.setParameter("DESCRICAO", Util.formatarTextoConsulta(descricaoTarifa.toLowerCase()));
			}
			String descricaoAbreviadaTarifa = (String) filtro.get("descricaoAbreviadaTarifa");
			if (!StringUtils.isEmpty(descricaoAbreviadaTarifa)) {
				query.setParameter("DESCRICAO_ABREVIADA",
						Util.formatarTextoConsulta(descricaoAbreviadaTarifa.toLowerCase()));
			}
			Date dataVigencia = (Date) filtro.get(DATA_VIGENCIA);
			if (dataVigencia != null) {
				query.setDate("DATA_VIGENCIA", dataVigencia);
			}
			Long idSegmento = (Long) filtro.get("idSegmento");
			if (idSegmento != null) {
				query.setParameter("ID_SEGMENTO", idSegmento);
			}
			Long idItemFatura = (Long) filtro.get("idItemFatura");
			if (idItemFatura != null) {
				query.setParameter("ID_ITEM_FATURA", idItemFatura);
			}
			Long tipoContrato = (Long) filtro.get("tipoContrato");
			if (tipoContrato != null) {
				query.setParameter("ID_TIPO_CONTRATO", tipoContrato);
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				query.setParameter("HABILITADO", habilitado);
			}
			Long status = (Long) filtro.get("status");
			if (status != null) {
				query.setParameter("STATUS", status);
			}
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				query.setParameterList("CHAVES_PRIMARIAS", chavesPrimarias);
			}
		}

		return obterTarifasVigenciaPesquisarVigentes(query.list(),filtro);
	}

	/**
	 * Obter tarifas vigencia pesquisar vigentes.
	 *
	 * @param listaTarifas
	 *            the lista tarifas
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<TarifaVigenciaPesquisar> obterTarifasVigenciaPesquisarVigentes(Collection<TarifaVigenciaPesquisar> listaTarifas,
			Map<String, Object> filtro) throws NegocioException {

		List<TarifaVigenciaPesquisar> listaTarifaVigenciaPesquisar = new ArrayList<TarifaVigenciaPesquisar>();

		if ((listaTarifas != null) && (!listaTarifas.isEmpty())) {
			Date dataAtual = Calendar.getInstance().getTime();
			Date dataInicioVigenciaAux = null;
			Date dataFimVigenciaAux = null;
			for (TarifaVigenciaPesquisar tarifaVigenciaPesquisar : listaTarifas) {
				Date dataVigenciaAux = null;
				try {
					if(!StringUtils.isEmpty(tarifaVigenciaPesquisar.getDataInicioVigencia())) {
						dataInicioVigenciaAux = Util.converterCampoStringParaData(TarifaVigencia.DATA_VIGENCIA,
								tarifaVigenciaPesquisar.getDataInicioVigencia(), Constantes.FORMATO_DATA_BR);
					}
					if(!StringUtils.isEmpty(tarifaVigenciaPesquisar.getDataFimVigencia())) {
						dataVigenciaAux = Util.converterCampoStringParaData(TarifaVigencia.DATA_VIGENCIA,
									tarifaVigenciaPesquisar.getDataFimVigencia(), Constantes.FORMATO_DATA_BR);

						dataFimVigenciaAux = Util.converterCampoStringParaData(TarifaVigencia.DATA_VIGENCIA,
								tarifaVigenciaPesquisar.getDataFimVigencia(), Constantes.FORMATO_DATA_BR);

					}
				} catch (GGASException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, new String[] {TarifaVigencia.DATA_VIGENCIA});
				}

				if (listaTarifaVigenciaPesquisar.contains(tarifaVigenciaPesquisar)) {

					int indiceTarifa = listaTarifaVigenciaPesquisar.indexOf(tarifaVigenciaPesquisar);

					Date dataInicioVigencia = this.formatarData(tarifaVigenciaPesquisar);

					int retornoDatas = Util.compararDatas(dataInicioVigencia, dataAtual);

					// Caso o início da vigência
					// seja igual a data atual.
					if (retornoDatas == 0) {
						listaTarifaVigenciaPesquisar.set(indiceTarifa, tarifaVigenciaPesquisar);

						// Caso o início da
						// vigência seja menor que
						// a data atual.
					} else if (retornoDatas < 0) {
						if (StringUtils.isEmpty(tarifaVigenciaPesquisar.getDataFimVigencia())) {
							listaTarifaVigenciaPesquisar.set(indiceTarifa, tarifaVigenciaPesquisar);
						} else {
							Date dataFimVigencia = null;

							try {
								dataFimVigencia = Util.converterCampoStringParaData(TarifaVigencia.DATA_VIGENCIA,
												tarifaVigenciaPesquisar.getDataFimVigencia(), Constantes.FORMATO_DATA_BR);

								if(dataFimVigencia != null && Util.compararDatas(dataInicioVigenciaAux,dataFimVigencia) >=0 &&
										Util.compararDatas(dataFimVigenciaAux, dataFimVigencia) <= 0) {
									listaTarifaVigenciaPesquisar.add(tarifaVigenciaPesquisar);
								}
							} catch (GGASException e) {
								LOG.error(e.getMessage(), e);
								throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, new String[] {TarifaVigencia.DATA_VIGENCIA});
							}

							if (Util.compararDatas(dataFimVigencia, dataAtual) > 0) {
								listaTarifaVigenciaPesquisar.set(indiceTarifa, tarifaVigenciaPesquisar);
							}
						}
					}
				} else {
					if(filtro != null && ((Date) filtro.get(DATA_VIGENCIA) != null)) {
						if ((Util.compararDatas(dataInicioVigenciaAux, (Date) filtro.get(DATA_VIGENCIA)) <= 0
								&& Util.compararDatas(dataVigenciaAux, (Date) filtro.get(DATA_VIGENCIA)) >= 0)
								|| Util.compararDatas(dataVigenciaAux, dataAtual) >= 0) {
							if(!listaTarifaVigenciaPesquisar.contains(tarifaVigenciaPesquisar)) {
								listaTarifaVigenciaPesquisar.add(tarifaVigenciaPesquisar);
							}
						}
					} else {
						listaTarifaVigenciaPesquisar.add(tarifaVigenciaPesquisar);
					}
				}
			}
		}

		return listaTarifaVigenciaPesquisar;
	}

	private Date formatarData(TarifaVigenciaPesquisar tarifaVigenciaPesquisar) throws NegocioException {
		Date dataInicioVigencia = null;
		try {
			dataInicioVigencia =
					Util.converterCampoStringParaData(TarifaVigencia.DATA_VIGENCIA, tarifaVigenciaPesquisar.getDataInicioVigencia(),
							Constantes.FORMATO_DATA_BR);
		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, new String[] { TarifaVigencia.DATA_VIGENCIA });
		}
		return dataInicioVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #atualizarTarifa(br.com.ggas
	 * .faturamento.tarifa.Tarifa,
	 * br.com.ggas.faturamento
	 * .tarifa.TarifaVigencia,
	 * br.com.ggas.faturamento
	 * .tarifa.TarifaVigenciaDesconto,
	 * java.util.List, boolean)
	 */
	@Override
	public void atualizarTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa, boolean isNovaVigencia) throws GGASException {

		if (tarifaVigencia != null && tarifaVigencia.getDataVigencia() == null) {
			throw new NegocioException(ERRO_TARIFA_DESCONTO_FAVOR_PREENCHER_VIGENCIA, true);
		}

		ControladorMenu controladorMenu = (ControladorMenu) ServiceLocator.getInstancia().getBeanPorID(
						ControladorMenu.BEAN_ID_CONTROLADOR_MENU);
		tarifa.getDadosAuditoria().getOperacao()
						.setMenu((Menu) controladorMenu.obter(tarifa.getDadosAuditoria().getOperacao().getMenu().getChavePrimaria()));

		this.atualizar(tarifa);

		if (tarifaVigencia != null && validaSeVaiInserirTarifaVigencia(tarifaVigencia)) {

			if (isNovaVigencia) {
				this.verificarCriacaoNovaVigencia(tarifa.getChavePrimaria(), tarifaVigencia.getDataVigencia());
				this.inserirComplementoTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
			} else {
				this.atualizarComplementoTarifa(tarifa, tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
			}

		} else {

			if (this.validaSeFoiPreenchidoTarifaVigenciaDesconto(tarifaVigenciaDesconto)) {
				throw new NegocioException(ERRO_TARIFA_DESCONTO_FAVOR_PREENCHER_VIGENCIA, true);
			}
			if (this.validaSeNaoTemFaixaDescontoPreenchida(listaDadosFaixa)) {
				throw new NegocioException(ERRO_TARIFA_FAIXA_DESCONTO_FAVOR_PREENCHER_TARIFA_DESCONTO, true);
			}

		}
	}

	/**
	 * Atualiza uma vigência de uma tarifa e seus
	 * respectivos descontos e faixas.
	 *
	 * @param tarifa
	 *            a tarifa da vigência a ser
	 *            atualizada
	 * @param tarifaVigencia
	 *            a vigência a ser atualizada
	 * @param tarifaVigenciaDesconto
	 *            os descontos da vigência a ser
	 *            atualizada
	 * @param listaDadosFaixa
	 *            os dados de cada faixa da
	 *            vigência a ser atualizada
	 * @throws ConcorrenciaException
	 *             caso ocorra algum erro durante
	 *             a execução do método
	 * @throws NegocioException
	 *             caso ocorra algum erro durante
	 *             a execução do método
	 */
	private void atualizarComplementoTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws ConcorrenciaException, NegocioException {

		this.validarTarifaVigencia(tarifaVigencia);
		Collection<TarifaVigenciaDesconto> descontosAtualizadosOuRemovidos = this.atualizarTarifaVigencia(tarifaVigencia);

		if (tarifaVigenciaDesconto != null && validaSeVaiInserirTarifaVigenciaDesconto(tarifaVigenciaDesconto)
						&& !descontosAtualizadosOuRemovidos.contains(tarifaVigenciaDesconto)) {

			this.validarDataTarifaDesconto(tarifaVigencia, tarifaVigenciaDesconto);
			this.validarTarifaVigenciaDesconto(tarifaVigenciaDesconto, listaDadosFaixa);
			this.atualizar(tarifaVigenciaDesconto, getClasseEntidadeTarifaVigenciaDesconto());

			try {
				this.validaDadosInserirTarifa(tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
				Map<String, Collection<TarifaFaixaDesconto>> mapa = this.popularTarifaFaixaDescontoAlteracao(tarifaVigenciaDesconto,
								tarifaVigencia, listaDadosFaixa);

				Collection<TarifaFaixaDesconto> lstTarifaFaixaDescontoBD = mapa.get("lstTarifaFaixaDescontoBD");
				for (TarifaFaixaDesconto tarifaFaixaDesconto : lstTarifaFaixaDescontoBD) {
					tarifaFaixaDesconto.setDadosAuditoria(tarifa.getDadosAuditoria());
					this.remover(tarifaFaixaDesconto, Boolean.TRUE);
				}

				Collection<TarifaFaixaDesconto> tarifaFaixaDescontos = mapa.get("lstTarifaFaixaDesconto");
				for (TarifaFaixaDesconto tarifaFaixaDescontoLoop : tarifaFaixaDescontos) {
					tarifaFaixaDescontoLoop.setDadosAuditoria(tarifa.getDadosAuditoria());
					this.inserir(tarifaFaixaDescontoLoop);
				}
			} catch (FormatoInvalidoException ex) {
				LOG.error(ex.getMessage(), ex);
				throw new NegocioException(ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE, true);
			}

		} else {
			// Caso não tenha nenhum Desconto
			// cadastrado, validar para ser
			// se existe algum valor digitado na
			// faixa desconto.
			if (validaSeNaoTemFaixaDescontoPreenchida(listaDadosFaixa)) {
				throw new NegocioException(ERRO_TARIFA_FAIXA_DESCONTO_FAVOR_PREENCHER_TARIFA_DESCONTO, true);
			}
		}
	}

	/**
	 * Validar tarifa vigencia.
	 *
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarTarifaVigencia(TarifaVigencia tarifaVigencia) throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idTarifa", tarifaVigencia.getTarifa().getChavePrimaria());
		Collection<TarifaVigencia> lista = this.consultarTarifasVigencia(filtro);
		for (TarifaVigencia tv : lista) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().evict(tv);
			if ((tv.getChavePrimaria() != tarifaVigencia.getChavePrimaria())
							&& (tv.getDataVigencia().compareTo(tarifaVigencia.getDataVigencia()) == 0)) {

				throw new NegocioException(ERRO_NEGOCIO_VIGENCIA_DATA_IGUAL, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#verificarCriacaoNovaVigencia(java.lang.Long, java.util.Date)
	 */
	@Override
	public boolean verificarCriacaoNovaVigencia(Long idTarifa, Date novaDataVigencia) throws NegocioException {

		boolean retorno = true;

		if (novaDataVigencia != null) {
			// Consulta as vigências da tarifa
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idTarifa", idTarifa);
			Collection<TarifaVigencia> lista = this.consultarTarifasVigencia(filtro);

			Date dataAtual = Calendar.getInstance().getTime();
			if (novaDataVigencia.compareTo(dataAtual) == 0) {
				retorno = false;
			}
			if (lista != null && !lista.isEmpty()) {
				/*
				 * Verifica se a data da nova
				 * vigência é igual à data atual
				 * ou à data de alguma vigência já
				 * existente.
				 * Se verdadeiro, então a nova
				 * vigência não poderá ser criada.
				 */
				this.verificarDataVigencia(novaDataVigencia, lista);
			}

		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, TarifaVigencia.DATA_VIGENCIA);
		}

		return retorno;
	}

	private void verificarDataVigencia(Date novaDataVigencia, Collection<TarifaVigencia> lista) throws NegocioException {
		for (TarifaVigencia tarifaVigenciaBD : lista) {
			if (tarifaVigenciaBD.getDataVigencia().compareTo(novaDataVigencia) == 0) {
				// TODO ver com analista
				// depois como vai ficar
				// essa validacao!!!

				throw new NegocioException(ERRO_NEGOCIO_VIGENCIA_DATA_IGUAL, true);
			}
		}
	}
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#gerarIndicesFinanceiros(java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public Collection<IndiceFinanceiroValorNominal> gerarIndicesFinanceiros(Date dataInicio, Date dataFim, Long idIndice)
					throws NegocioException {

		IndiceFinanceiro indiceFinanceiro = null;
		List<IndiceFinanceiroValorNominal> indicesFinanceirosGerados = new ArrayList<IndiceFinanceiroValorNominal>();

		if ((idIndice != null) && (idIndice > 0)) {
			indiceFinanceiro = this.obterIndiceFinanceiro(idIndice);
			this.validarDatasIndiceFinanceiro(dataInicio, dataFim, indiceFinanceiro);



			// Quando o índice for mensal
			// trabalhará com o primeiro dia do
			// mês.
			Collection<IndiceFinanceiroValorNominal> listaIndiceFinanceiro = this.listarIndiceFinanceiroEntreData(dataInicio, dataFim,
							idIndice);

			if (indiceFinanceiro.isIndicadorMensal()) {
				int qtdMeses = Util.intervaloMeses(dataInicio, dataFim) + 1;

				this.gerarListaDeIndicesMeses(qtdMeses, dataInicio, indicesFinanceirosGerados);

			} else {
				int qtdDias = Util.intervaloDatas(dataInicio, dataFim) + 1;

				DateTime dataReferencia = new DateTime(dataInicio);
				// Gerando lista de índices entre
				// as datas selecionadas.
				for (int i = 0; i < qtdDias; i++) {
					IndiceFinanceiroValorNominal indiceFinanceiroValor = (IndiceFinanceiroValorNominal) this
									.criarIndiceFinanceiroValorNominal();
					indiceFinanceiroValor.setDataReferencia(dataReferencia.toDate());
					indicesFinanceirosGerados.add(indiceFinanceiroValor);
					dataReferencia = dataReferencia.plusDays(1);
				}
			}

			if ((listaIndiceFinanceiro != null) && (!listaIndiceFinanceiro.isEmpty())) {
				// Populando valor dos índices que
				// já existem e serão mostrados na
				// tela.
				for (IndiceFinanceiroValorNominal indiceFinanceiroGerado : indicesFinanceirosGerados) {
					IndiceFinanceiroValorNominal indiceFinanceiroEncontrado = this.contemIndiceFinanceiroData(indiceFinanceiroGerado,
									listaIndiceFinanceiro);
					if (indiceFinanceiroEncontrado != null) {
						BigDecimal valorNominal = indiceFinanceiroEncontrado.getValorNominal();
						if (indiceFinanceiro.isIndicadorValorPercentual()) {
							try {
								valorNominal = BigDecimalUtil.converterPercentualParaBigDecimal(valorNominal);
							} catch (FormatoInvalidoException e) {
								LOG.error(e.getMessage(), e);
								throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, IndiceFinanceiroValorNominal.VALOR_NOMINAL);
							}
						}
						indiceFinanceiroGerado.setValorNominal(valorNominal);
						indiceFinanceiroGerado.setIndicadorUtilizado(indiceFinanceiroEncontrado.isIndicadorUtilizado());
					}
				}
			}

		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					new String[] { IndiceFinanceiro.INDICE_FINANCEIRO });
		}

		return indicesFinanceirosGerados;
	}

	private void gerarListaDeIndicesMeses(int qtdMeses, Date dataInicio, List<IndiceFinanceiroValorNominal> indicesFinanceirosGerados) {
		Calendar calendar = Calendar.getInstance();

		int mesAtual = calendar.get(Calendar.MONTH);
		int anoAtual = calendar.get(Calendar.YEAR);

		calendar.setTime(dataInicio);
		Integer mesInicio = calendar.get(Calendar.MONTH);
		int anoInicio = calendar.get(Calendar.YEAR);

		for (int i = 0; i < qtdMeses; i++) {
			IndiceFinanceiroValorNominal indiceFinanceiroValor = (IndiceFinanceiroValorNominal) this.criarIndiceFinanceiroValorNominal();
			calendar = Calendar.getInstance();

			int mes = mesInicio;
			if (mesInicio >= CONSTANTE_NUMERO_DOZE) {
				mes = mesInicio % CONSTANTE_NUMERO_DOZE;
				if (mes == 0) {
					anoInicio++;
				}
			}

			calendar.set(anoInicio, mes, 1);
			indiceFinanceiroValor.setDataReferencia(calendar.getTime());

			// Desabilitar os índices
			// gerados para os meses
			// maiores que o atual setando
			// eles como utilizados.
			if ((mes > mesAtual) && (anoInicio >= anoAtual)) {
				indiceFinanceiroValor.setIndicadorUtilizado(Boolean.TRUE);
			}

			indicesFinanceirosGerados.add(indiceFinanceiroValor);
			mesInicio++;

		}
	}

	/**
	 * Contem indice financeiro data.
	 *
	 * @param indiceFinanceiro
	 *            the indice financeiro
	 * @param listaIndiceFinanceiro
	 *            the lista indice financeiro
	 * @return the indice financeiro valor nominal
	 */
	private IndiceFinanceiroValorNominal contemIndiceFinanceiroData(IndiceFinanceiroValorNominal indiceFinanceiro,
					Collection<IndiceFinanceiroValorNominal> listaIndiceFinanceiro) {

		IndiceFinanceiroValorNominal indiceFinanceiroEncontrado = null;

		for (IndiceFinanceiroValorNominal indiceFinanceiroValorNominal : listaIndiceFinanceiro) {
			if ((indiceFinanceiro.getDataReferencia() != null)
							&& (indiceFinanceiroValorNominal.getDataReferencia() != null)
							&& (Util.compararDatas(indiceFinanceiro.getDataReferencia(), indiceFinanceiroValorNominal.getDataReferencia()) == 0)) {
				indiceFinanceiroEncontrado = indiceFinanceiroValorNominal;
				break;
			}
		}

		return indiceFinanceiroEncontrado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarIndiceFinanceiroEntreData(java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public Collection<IndiceFinanceiroValorNominal> listarIndiceFinanceiroEntreData(Date dataInicio, Date dataFim, Long idIndice)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeIndiceFinanceiroValorNominal().getSimpleName()).append(" indice ");
		hql.append(" where ");
		hql.append(" indice.indiceFinanceiro.chavePrimaria = :idIndice ");
		if ((dataInicio != null) && (dataFim != null)) {
			hql.append(" and indice.dataReferencia >= :dataInicio ");
			hql.append(" and indice.dataReferencia <= :dataFim ");
		}

		hql.append(" order by indice.dataReferencia ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idIndice", idIndice);
		if ((dataInicio != null) && (dataFim != null)) {
			query.setDate(DATA_INICIO, dataInicio);
			query.setDate(DATA_FIM, dataFim);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #atualizarIndicesFinanceiros
	 * (java.lang.String[],
	 * java.lang.String[],
	 * br.com.ggas.arrecadacao.
	 * IndiceFinanceiroValorNominal)
	 */
	@Override
	public void atualizarIndicesFinanceiros(String[] datasReferencia, String[] mesDatasReferencia,
			String[] valoresNominais, IndiceFinanceiroValorNominal indicePopulado, String[] indicadorUtilizado)
			throws GGASException {

		Map<String, Object> erros = indicePopulado.validarDados();

		if ((erros != null) && (!erros.isEmpty())) {
			throw new NegocioException(erros);
		}

		Date primeiraDataReferencia = null;
		Date ultimaDataReferencia = null;

		Collection<IndiceFinanceiroValorNominal> listaIndiceFinanceiro = null;
		List<IndiceFinanceiroValorNominal> listaIndicesInclusao = new ArrayList<IndiceFinanceiroValorNominal>();
		String[] arrayDatas = null;

		// Caso o índice selecionado seja mensal.
		if ((indicePopulado.getIndiceFinanceiro() != null)
				&& (indicePopulado.getIndiceFinanceiro().isIndicadorMensal())) {

			if ((mesDatasReferencia != null) && (mesDatasReferencia.length > 0) && (valoresNominais != null)
					&& (valoresNominais.length > 0)) {
				listaIndiceFinanceiro = this.listarIndiceFinanceiroEntreData(primeiraDataReferencia,
						ultimaDataReferencia, indicePopulado.getIndiceFinanceiro().getChavePrimaria());
				arrayDatas = mesDatasReferencia;
			}

			// Caso o índices selecionado seja
			// diário.
		} else if (indicePopulado.getIndiceFinanceiro() != null
				&& !indicePopulado.getIndiceFinanceiro().isIndicadorMensal() && datasReferencia != null
				&& datasReferencia.length > 0 && valoresNominais != null && valoresNominais.length > 0) {

			String primeiraData = datasReferencia[0];
			String ultimaData = datasReferencia[datasReferencia.length - 1];

			try {
				primeiraDataReferencia = Util.converterCampoStringParaData("Início", primeiraData,
						Constantes.FORMATO_DATA_BR);
				ultimaDataReferencia = Util.converterCampoStringParaData("Fim", ultimaData, Constantes.FORMATO_DATA_BR);
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, IndiceFinanceiroValorNominal.DATA);
			}

			listaIndiceFinanceiro = this.listarIndiceFinanceiroEntreData(primeiraDataReferencia, ultimaDataReferencia,
					indicePopulado.getIndiceFinanceiro().getChavePrimaria());
			arrayDatas = datasReferencia;

		}

		// Populando índices.
		Boolean passou;
		if (arrayDatas != null) {
			for (int i = 0; i < arrayDatas.length; i++) {
				passou = false;

				String valorNominal = valoresNominais[i];
				String utilizado = indicadorUtilizado[i];

				Date dataReferencia = null;
				try {
					dataReferencia = Util.converterCampoStringParaData("Data", arrayDatas[i],
							Constantes.FORMATO_DATA_BR);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, IndiceFinanceiroValorNominal.DATA);
				}

				// se for um campo vazio, apague o
				// campo no BD
				if (StringUtils.isEmpty(valorNominal)) {

					this.removerIndiceFinanceiroValorNominal(listaIndiceFinanceiro, dataReferencia);

				} else {
					IndiceFinanceiroValorNominal indiceFinanceiro = (IndiceFinanceiroValorNominal) this.criarIndiceFinanceiroValorNominal();

					indiceFinanceiro.setDataReferencia(dataReferencia);

					this.adicionarValorEmIndicadorUtilizado(indiceFinanceiro, utilizado);

					indiceFinanceiro.setIndiceFinanceiro(indicePopulado.getIndiceFinanceiro());
					indiceFinanceiro.setUltimaAlteracao(Calendar.getInstance().getTime());
					indiceFinanceiro.setDadosAuditoria(indicePopulado.getDadosAuditoria());


					BigDecimal valorNominalIndice =
							this.adicionarValorNominalEmIndiceFinanceiro(indicePopulado, indiceFinanceiro, valorNominal);

					if ((listaIndiceFinanceiro != null) && (!listaIndiceFinanceiro.isEmpty())) {
						for (IndiceFinanceiroValorNominal indiceFinanceiroValorNominal : listaIndiceFinanceiro) {
							if (indiceFinanceiroValorNominal.getDataReferencia().compareTo(dataReferencia) == 0) {

								if (valorNominalIndice != null) {
									indiceFinanceiroValorNominal.setValorNominal(valorNominalIndice);
								}
								indiceFinanceiroValorNominal
										.setIndiceFinanceiro(indiceFinanceiro.getIndiceFinanceiro());
								indiceFinanceiroValorNominal.setUltimaAlteracao(indiceFinanceiro.getUltimaAlteracao());
								indiceFinanceiroValorNominal
										.setIndicadorUtilizado(indiceFinanceiro.isIndicadorUtilizado());
								indiceFinanceiroValorNominal.setDadosAuditoria(indiceFinanceiro.getDadosAuditoria());

								this.atualizar(indiceFinanceiroValorNominal, IndiceFinanceiroValorNominalImpl.class);
								passou = true;
								break;
							}
						}
					}

					this.adicionarIndiceFinanceiro(passou, listaIndicesInclusao, indiceFinanceiro);
				}
			}
		}

		// insira todos os campos que nao existiam
		for (IndiceFinanceiroValorNominal indiceFinanceiroValorNominal : listaIndicesInclusao) {
			this.inserir(indiceFinanceiroValorNominal);
		}
	}

	private void removerIndiceFinanceiroValorNominal(Collection<IndiceFinanceiroValorNominal> listaIndiceFinanceiro, Date dataReferencia)
			throws NegocioException {
		if ((listaIndiceFinanceiro != null) && (!listaIndiceFinanceiro.isEmpty())) {
			for (IndiceFinanceiroValorNominal indiceFinanceiroValorNominal : listaIndiceFinanceiro) {
				if (indiceFinanceiroValorNominal.getDataReferencia().compareTo(dataReferencia) == 0) {
					this.remover(indiceFinanceiroValorNominal, IndiceFinanceiroValorNominalImpl.class);
					break;
				}
			}
		}
	}


	private void adicionarValorEmIndicadorUtilizado(IndiceFinanceiroValorNominal indiceFinanceiro, String utilizado) {
		if (StringUtils.isEmpty(utilizado)) {
			indiceFinanceiro.setIndicadorUtilizado(Boolean.FALSE);
		} else {
			indiceFinanceiro.setIndicadorUtilizado(Boolean.valueOf(utilizado));
		}
	}

	private BigDecimal adicionarValorNominalEmIndiceFinanceiro(IndiceFinanceiroValorNominal indicePopulado,
			IndiceFinanceiroValorNominal indiceFinanceiro, String valorNominal) throws NegocioException {
		BigDecimal valorNominalIndice = null;
		try {
			valorNominalIndice =
					Util.converterCampoStringParaValorBigDecimal("Valor Nominal Índice", valorNominal, Constantes.FORMATO_VALOR_BR,
							Constantes.LOCALE_PADRAO);

			if (indicePopulado.getIndiceFinanceiro().isIndicadorValorPercentual()) {
				valorNominalIndice = Util.converterBigDecimalParaPercentual(valorNominalIndice);
			}

			indiceFinanceiro.setValorNominal(valorNominalIndice);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO, IndiceFinanceiroValorNominal.VALOR_NOMINAL);
		}
		return valorNominalIndice;
	}

	private void adicionarIndiceFinanceiro(Boolean passou, List<IndiceFinanceiroValorNominal> listaIndicesInclusao,
			IndiceFinanceiroValorNominal indiceFinanceiro) {

		// se nao passou é porque nao
		// existia antes. Insira no BD
		if (!passou) {
			listaIndicesInclusao.add(indiceFinanceiro);
		}
	}

	/**
	 * Validar datas indice financeiro.
	 *
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @param indiceFinanceiro
	 *            the indice financeiro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDatasIndiceFinanceiro(Date dataInicio, Date dataFim, IndiceFinanceiro indiceFinanceiro) throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (dataInicio == null) {
			stringBuilder.append("Início");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataFim == null) {
			stringBuilder.append("Fim");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
			throw new NegocioException(erros);
		} else {
			if (Util.compararDatas(dataInicio, dataFim) > 0) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_MAIOR_QUE_DATA_FIM, true);
			}

			if ((indiceFinanceiro != null) && (!indiceFinanceiro.isIndicadorMensal())
					&& (Util.intervaloDatas(dataInicio, dataFim) > CONSTANTE_NUMERO_NOVENTA)) {
				throw new NegocioException(ERRO_NEGOCIO_PERIODO_MAIOR_QUE_LIMITE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#exibirErroTarifa(java.lang.String)
	 */
	@Override
	public void exibirErroTarifa(String erro) throws NegocioException {

		throw new NegocioException(erro, true);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TarifaVigenciaFaixa> consultarTarifasVigenciaFaixa(Long chaveTarifaVigencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaFaixa().getSimpleName()).append(" tvf ");
		hql.append(" where ");
		hql.append(" tvf.tarifaVigencia.chavePrimaria = :CHAVE_TARIFA_VIGENCIA ");
		hql.append(" order by tvf.medidaInicio asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_TARIFA_VIGENCIA", chaveTarifaVigencia);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#popularDadosTarifaVigenciaFaixa(br.com.ggas.faturamento.tarifa.TarifaVigencia)
	 */
	@Override
	public List<DadosFaixasTarifa> popularDadosTarifaVigenciaFaixa(TarifaVigencia tarifaVigencia) throws GGASException {

		List<DadosFaixasTarifa> listaDadosFaixasTarifa = new ArrayList<DadosFaixasTarifa>();

		int qtdCasasDecimal = CONSTANTE_NUMERO_DOIS;
		if (tarifaVigencia != null) {

			if ((tarifaVigencia.getTarifa() != null) && (tarifaVigencia.getTarifa().getQuantidadeCasaDecimal() != null)) {
				qtdCasasDecimal = tarifaVigencia.getTarifa().getQuantidadeCasaDecimal().intValue();
			}

			Collection<TarifaVigenciaFaixa> listaTVF = this.consultarTarifasVigenciaFaixa(tarifaVigencia.getChavePrimaria());
			if (listaTVF != null && !listaTVF.isEmpty()) {
				Integer count = 0;
				for (TarifaVigenciaFaixa tarifaVigenciaFaixa : listaTVF) {
					DadosFaixasTarifa dados = this.criarDadosFaixasTarifa();

					dados.setIdTarifaVigenciaFaixa(tarifaVigenciaFaixa.getChavePrimaria());

					dados.setFaixaInicial(tarifaVigenciaFaixa.getMedidaInicio().toBigInteger().toString());
					dados.setFaixaFinal(tarifaVigenciaFaixa.getMedidaFim().toBigInteger().toString());

					this.adicionarValorFixoSemImpostoEmDadosFaixasTarifa(tarifaVigenciaFaixa, dados, qtdCasasDecimal);

					dados.setValorCompra(formatarValorDadosFaixas(tarifaVigenciaFaixa.getValorCompra(), "Valor Compra", qtdCasasDecimal));

					dados.setMargemValorAgregado(formatarValorDadosFaixas(tarifaVigenciaFaixa.getMargemValorAgregado(),
									"Margem Valor Agregado", qtdCasasDecimal));

					this.adicionarValorVariavelSemImpostoEmDadosFaixasTarifa(tarifaVigenciaFaixa, dados, qtdCasasDecimal);

					dados.setNumeroColunaFinal(count);
					listaDadosFaixasTarifa.add(dados);

					count++;
				}
			}
		}

		// Ordenação da lista de dados faixa
		Collections.sort(listaDadosFaixasTarifa, new Comparator<DadosFaixasTarifa>(){

			@Override
			public int compare(DadosFaixasTarifa o1, DadosFaixasTarifa o2) {

				String pattern = "[.,]";
				return Long.valueOf(o1.getFaixaInicial().replaceAll(pattern, "")).compareTo(
								Long.valueOf(o2.getFaixaInicial().replaceAll(pattern, "")));
			}
		});

		return listaDadosFaixasTarifa;
	}

	private void adicionarValorFixoSemImpostoEmDadosFaixasTarifa(TarifaVigenciaFaixa tarifaVigenciaFaixa, DadosFaixasTarifa dados,
			int qtdCasasDecimal) {
		if (tarifaVigenciaFaixa.getValorFixo() != null) {
			dados.setValorFixoSemImposto(Util.converterCampoValorDecimalParaString("Valor Fixo Sem Imposto", tarifaVigenciaFaixa
					.getValorFixo().setScale(qtdCasasDecimal, RoundingMode.HALF_UP), Constantes.LOCALE_PADRAO, qtdCasasDecimal));
		}
	}

	private void adicionarValorVariavelSemImpostoEmDadosFaixasTarifa(TarifaVigenciaFaixa tarifaVigenciaFaixa, DadosFaixasTarifa dados,
			int qtdCasasDecimal) {

		if (tarifaVigenciaFaixa.getValorVariavel() != null) {
			dados.setValorVariavelSemImposto(Util.converterCampoValorDecimalParaString("Valor Variável Sem Imposto", tarifaVigenciaFaixa
					.getValorVariavel().setScale(qtdCasasDecimal, RoundingMode.HALF_UP), Constantes.LOCALE_PADRAO, qtdCasasDecimal));
		}
	}

	/**
	 * Formatar valor dados faixas.
	 *
	 * @param valorCompra
	 *            the valor compra
	 * @param rotulo
	 *            the rotulo
	 * @param qtdCasasDecimal
	 *            the qtd casas decimal
	 * @return the string
	 */
	private String formatarValorDadosFaixas(BigDecimal valorCompra, String rotulo, int qtdCasasDecimal) {

		if (valorCompra != null) {
			return Util.converterCampoValorDecimalParaString(rotulo, valorCompra.setScale(qtdCasasDecimal, RoundingMode.HALF_UP),
							Constantes.LOCALE_PADRAO, qtdCasasDecimal);
		}

		return null;
	}

	/**
	 * Consultar tarifas faixa desconto.
	 *
	 * @param chaveTarifaVigenciaDesconto
	 *            the chave tarifa vigencia desconto
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<TarifaFaixaDesconto> consultarTarifasFaixaDesconto(Long chaveTarifaVigenciaDesconto) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaFaixaDesconto().getSimpleName()).append(" tvd ");
		hql.append(" left join fetch tvd.tarifaVigenciaFaixa");
		hql.append(" where ");
		hql.append(" tvd.tarifaVigenciaDesconto.chavePrimaria = :CHAVE_TARIFA_VIGENCIA_DESCONTO");
		hql.append(" order by tvd.tarifaVigenciaFaixa.medidaInicio ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_TARIFA_VIGENCIA_DESCONTO", chaveTarifaVigenciaDesconto);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#validarCamposReajuste(java.lang.String, java.lang.Long)
	 */
	@Override
	public void validarCamposReajuste(String valorReajuste, Long idTipoReajuste) throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(valorReajuste)) {
			stringBuilder.append(Tarifa.TARIFA_VALOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (idTipoReajuste == null || idTipoReajuste == 0) {
			stringBuilder.append(Tarifa.TARIFA_TIPO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(ControladorTarifa.ERRO_NEGOCIO_REAJUSTE,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
			throw new NegocioException(erros);
		}

	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.tarifa.
	 * ControladorTarifa#
	 * reajustarFaixaTarifa(java.lang.String,
	 * java.lang.Long,java.util.Collection,
	 * java.util.Collection)
	 */
	@Override
	public void reajustarFaixaTarifa(String valorReajusteParam, Long idTipoReajusteParam, Collection<DadosFaixasTarifa> listaDadosFaixa,
					Collection<DadosFaixasTarifa> listaDescontosFaixa, Integer qtdCasasDecimais) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idTipoPercentual = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL));

		boolean isPercentual = false;
		BigDecimal valor = BigDecimal.ZERO;
		if (idTipoReajusteParam.compareTo(idTipoPercentual) == 0) {
			BigDecimal valorReajuste = Util.converterCampoStringParaValorBigDecimal("Valor Reajuste", valorReajusteParam,
							Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
			valor = Util.converterBigDecimalParaPercentual(valorReajuste);
			isPercentual = true;
		} else {
			valor = Util.converterCampoStringParaValorBigDecimal("Valor Reajuste", valorReajusteParam, Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO);
		}

		this.reajustarListaDadosFaixaTarifa(valor, listaDadosFaixa, isPercentual, qtdCasasDecimais);
		this.reajustarListaDadosFaixaTarifa(valor, listaDescontosFaixa, isPercentual, qtdCasasDecimais);
	}

	/**
	 * Reajustar lista dados faixa tarifa.
	 *
	 * @param valor
	 *            the valor
	 * @param lista
	 *            the lista
	 * @param isPercentual
	 *            the is percentual
	 * @param qtdCasasDecimais
	 *            the qtd casas decimais
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void reajustarListaDadosFaixaTarifa(BigDecimal valor, Collection<DadosFaixasTarifa> lista, boolean isPercentual,
					Integer qtdCasasDecimais) throws GGASException {

		if (lista != null && !lista.isEmpty()) {

			for (DadosFaixasTarifa dados : lista) {
				BigDecimal valorFixo = Util.converterCampoStringParaValorBigDecimal("Valor Fixo", dados.getValorFixoSemImposto(),
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

				valorFixo = this.adicionarValorFixo(valorFixo, isPercentual, valor);

				dados.setValorFixoSemImposto(Util.converterCampoValorDecimalParaString("Valor Fixo", valorFixo, Constantes.LOCALE_PADRAO,
								qtdCasasDecimais));

				dados.setValorCompra(reajustarValor("Valor de Compra", dados.getValorCompra(), isPercentual, valor, qtdCasasDecimais));

				dados.setMargemValorAgregado(reajustarValor("Margem de Valor Agregado", dados.getMargemValorAgregado(), isPercentual,
								valor, qtdCasasDecimais));

				BigDecimal valorVariavel;

				if (!isPercentual && (dados.getValorCompra() != null || dados.getMargemValorAgregado() != null)) {

					BigDecimal valorCompra = NumeroUtil.converterCampoStringParaValorBigDecimal("Valor de Compra", dados.getValorCompra());
					BigDecimal margemValorAgregado = NumeroUtil.converterCampoStringParaValorBigDecimal("Margem de Valor Agregado",
									dados.getMargemValorAgregado());

					valorVariavel = NumeroUtil.soma(valorCompra, margemValorAgregado);

				} else {
					valorVariavel = Util.converterCampoStringParaValorBigDecimal("Valor Variável", dados.getValorVariavelSemImposto(),
									Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

					valorVariavel = this.adicionarValorVariavel(valorVariavel, isPercentual, valor);

				}

				dados.setValorVariavelSemImposto(Util.converterCampoValorDecimalParaString("Valor Variável", valorVariavel,
								Constantes.LOCALE_PADRAO, qtdCasasDecimais));
			}
		}
	}

	private BigDecimal adicionarValorFixo(BigDecimal valorFixoAux, boolean isPercentual, BigDecimal valor) {
		BigDecimal valorFixo = valorFixoAux;
		if (valorFixo.compareTo(BigDecimal.ZERO) > 0) {
			if (isPercentual) {
				valorFixo = valorFixo.add(valorFixo.multiply(valor));
			} else {
				valorFixo = valorFixo.add(valor);
			}

			if (valorFixo.compareTo(BigDecimal.ZERO) < 0) {
				valorFixo = BigDecimal.ZERO;
			}
		}
		return valorFixo;
	}

	private BigDecimal adicionarValorVariavel(BigDecimal valorVariavelAux, boolean isPercentual, BigDecimal valor) {

		BigDecimal valorVariavel = valorVariavelAux;
		if (valorVariavel.compareTo(BigDecimal.ZERO) > 0) {
			if (isPercentual) {
				valorVariavel = valorVariavel.add(valorVariavel.multiply(valor));
			} else {
				valorVariavel = valorVariavel.add(valor);
			}

			if (valorVariavel.compareTo(BigDecimal.ZERO) < 0) {
				valorVariavel = BigDecimal.ZERO;
			}
		}
		return valorVariavel;
	}



	/**
	 * Reajustar valor.
	 *
	 * @param rotulo
	 *            the rotulo
	 * @param valorString
	 *            the valor string
	 * @param isPercentual
	 *            the is percentual
	 * @param valor
	 *            the valor
	 * @param qtdCasasDecimais
	 *            the qtd casas decimais
	 * @return the string
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private String reajustarValor(String rotulo, String valorString, boolean isPercentual, BigDecimal valor, int qtdCasasDecimais)
					throws FormatoInvalidoException {

		if (StringUtils.isNotBlank(valorString)) {

			BigDecimal valorCompra = Util.converterCampoStringParaValorBigDecimal(rotulo, valorString, Constantes.FORMATO_VALOR_NUMERO,
							Constantes.LOCALE_PADRAO);

			if (valorCompra.compareTo(BigDecimal.ZERO) > 0) {
				if (isPercentual) {
					valorCompra = valorCompra.add(valorCompra.multiply(valor));
				} else {
					valorCompra = valorCompra.add(valor);
				}

				if (valorCompra.compareTo(BigDecimal.ZERO) < 0) {
					valorCompra = BigDecimal.ZERO;
				}
			}

			return Util.converterCampoValorDecimalParaString(rotulo, valorCompra, Constantes.LOCALE_PADRAO, qtdCasasDecimais);

		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#popularDadosTarifaFaixaDesconto(java.util.List,
	 * br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto)
	 */
	@Override
	public List<DadosFaixasTarifa> popularDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa,
					TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		List<DadosFaixasTarifa> listaDadosFaixasDesconto = new ArrayList<DadosFaixasTarifa>();
		if (tarifaVigenciaDesconto != null && (listaDadosFaixasTarifa != null && !listaDadosFaixasTarifa.isEmpty())) {
			Long idTipoPercentual = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL));

			Collection<TarifaFaixaDesconto> listaTFD = this.consultarTarifasFaixaDesconto(tarifaVigenciaDesconto.getChavePrimaria());
			this.popularDadosTFD(listaDadosFaixasDesconto, listaTFD, listaDadosFaixasTarifa, idTipoPercentual);

		}
		return listaDadosFaixasDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#popularDadosTarifaFaixaDesconto(java.util.List)
	 */
	@Override
	public List<DadosFaixasTarifa> popularDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa) throws GGASException {

		List<DadosFaixasTarifa> listaDadosFaixasDesconto = new ArrayList<DadosFaixasTarifa>();
		if (listaDadosFaixasTarifa != null && !listaDadosFaixasTarifa.isEmpty()) {
			for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixasTarifa) {
				DadosFaixasTarifa dados = this.criarDadosFaixasTarifa();
				dados.setFaixaInicial(dadosFaixasTarifa.getFaixaInicial());
				dados.setFaixaFinal(dadosFaixasTarifa.getFaixaFinal());
				dados.setValorFixoSemImposto(dadosFaixasTarifa.getValorFixoSemImposto());
				dados.setValorVariavelSemImposto(dadosFaixasTarifa.getValorVariavelSemImposto());
				listaDadosFaixasDesconto.add(dados);
			}
		}

		Collections.sort(listaDadosFaixasDesconto, new Comparator<DadosFaixasTarifa>(){

			@Override
			public int compare(DadosFaixasTarifa o1, DadosFaixasTarifa o2) {

				return (Long.valueOf(o1.getFaixaInicial())).compareTo(Long.valueOf(o2.getFaixaInicial()));
			}
		});
		return listaDadosFaixasDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#carregarDadosTarifaFaixaDesconto(java.util.List, java.lang.Long)
	 */
	@Override
	public DadosTarifa carregarDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa, Long idTarifaVigencia)
					throws GGASException {

		DadosTarifa dadosTarifa = this.criarDadosTarifa();
		List<TarifaVigenciaDesconto> lista = new ArrayList<TarifaVigenciaDesconto>(
						this.obterTarifaVigenciaDescontoPorTarifaVigencia(idTarifaVigencia));
		TarifaVigenciaDesconto tvdAtual = null;
		if (!lista.isEmpty()) {
			tvdAtual = lista.get(0);
			dadosTarifa.setTarifaVigenciaDesconto(tvdAtual);
			List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa = this.popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa, tvdAtual);
			dadosTarifa.getFaixasDesconto().addAll(listaDadosFaixasDescontoTarifa);
		}
		return dadosTarifa;
	}

	/**
	 * Popular dados tfd.
	 *
	 * @param listaDadosFaixasDesconto
	 *            the lista dados faixas desconto
	 * @param listaTFD
	 *            the lista tfd
	 * @param listaDadosFaixasTarifa
	 *            the lista dados faixas tarifa
	 * @param idTipoPercentual
	 *            the id tipo percentual
	 */
	/*
	 * TFD - Tarifa Faixa Desconto
	 */
	private void popularDadosTFD(List<DadosFaixasTarifa> listaDadosFaixasDesconto, Collection<TarifaFaixaDesconto> listaTFD,
					List<DadosFaixasTarifa> listaDadosFaixasTarifa, Long idTipoPercentual){

		int count = 0;
		for (TarifaFaixaDesconto tarifaFaixaDesconto : listaTFD) {
			if (count < listaDadosFaixasTarifa.size()) {
				DadosFaixasTarifa dados = this.criarEPopularVODadosFaixa(listaDadosFaixasTarifa, idTipoPercentual, count,
								tarifaFaixaDesconto);
				listaDadosFaixasDesconto.add(dados);
			}
			count++;

		}
	}

	/**
	 * Criar e popular vo dados faixa.
	 *
	 * @param listaDadosFaixasTarifa
	 *            the lista dados faixas tarifa
	 * @param idTipoPercentual
	 *            the id tipo percentual
	 * @param count
	 *            the count
	 * @param tarifaFaixaDesconto
	 *            the tarifa faixa desconto
	 * @return the dados faixas tarifa
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private DadosFaixasTarifa criarEPopularVODadosFaixa(List<DadosFaixasTarifa> listaDadosFaixasTarifa, Long idTipoPercentual, int count,
					TarifaFaixaDesconto tarifaFaixaDesconto) {

		int qtdCasasDecimal = CONSTANTE_NUMERO_DOIS;
		if ((tarifaFaixaDesconto.getTarifaVigenciaDesconto() != null)
						&& (tarifaFaixaDesconto.getTarifaVigenciaDesconto().getTarifa() != null)
						&& (tarifaFaixaDesconto.getTarifaVigenciaDesconto().getTarifa().getQuantidadeCasaDecimal() != null)) {
			qtdCasasDecimal = tarifaFaixaDesconto.getTarifaVigenciaDesconto().getTarifa().getQuantidadeCasaDecimal().intValue();
		}

		DadosFaixasTarifa dados = this.criarDadosFaixasTarifa();

		dados.setFaixaInicial(listaDadosFaixasTarifa.get(count).getFaixaInicial());
		dados.setFaixaFinal(listaDadosFaixasTarifa.get(count).getFaixaFinal());
		dados.setValorFixoSemImposto(listaDadosFaixasTarifa.get(count).getValorFixoSemImposto());
		dados.setValorVariavelSemImposto(listaDadosFaixasTarifa.get(count).getValorVariavelSemImposto());
		dados.setDescontoFixoSemImposto(Util.converterCampoValorDecimalParaString("Desconto Fixo Sem Imposto", tarifaFaixaDesconto
						.getValorFixo().setScale(qtdCasasDecimal, RoundingMode.DOWN), Constantes.LOCALE_PADRAO, qtdCasasDecimal));
		dados.setDescontoVariavelSemImposto(Util.converterCampoValorDecimalParaString("Desconto Variável Sem Imposto", tarifaFaixaDesconto
						.getValorVariavel().setScale(qtdCasasDecimal, RoundingMode.DOWN), Constantes.LOCALE_PADRAO, qtdCasasDecimal));
		if (tarifaFaixaDesconto.getPercentualDescontoFixo() != null) {
			dados.setTipoDesconto(idTipoPercentual);
		} else {
			dados.setTipoDesconto(136L);
			// TODO -
			// Referente
			// a valor
			// - criar
			// um
			// parametro
			// de
			// sistema
			// para
			// obter
			// essa
			// informação.
		}
		return dados;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#ordenarFaixasPorMatriz(java.util.List)
	 */
	@Override
	public List<DadosFaixasTarifa> ordenarFaixasPorMatriz(List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException {

		final String zero = "0";

		// Ordena as faixas por ordem crescente de
		// valor inicial
		Collections.sort(listaDadosFaixa, new Comparator<DadosFaixasTarifa>(){

			@Override
			public int compare(DadosFaixasTarifa o1, DadosFaixasTarifa o2) {

				return (Long.valueOf(o1.getFaixaInicial())).compareTo(Long.valueOf(o2.getFaixaInicial()));
			}
		});

		// Valida se os dados de cada faixa foram
		// devidamente preenchidos
		Iterator<DadosFaixasTarifa> iteratorDadosFaixa = listaDadosFaixa.iterator();
		DadosFaixasTarifa dados = null;
		while (iteratorDadosFaixa.hasNext()) {

			dados = iteratorDadosFaixa.next();

			if (StringUtils.isEmpty(dados.getValorFixoSemImposto()) && StringUtils.isEmpty(dados.getValorVariavelSemImposto())) {
				throw new NegocioException(Tarifa.ERRO_TARIFA_VALORES_FIXO_VARIAVEL_NAO_INFORMADOS, dados.getFaixaInicial());
			} else if (StringUtils.isEmpty(dados.getValorFixoSemImposto())) {
				dados.setValorFixoSemImposto(zero);
			} else if (StringUtils.isEmpty(dados.getValorVariavelSemImposto())) {
				dados.setValorVariavelSemImposto(zero);
			}

			if (iteratorDadosFaixa.hasNext() && StringUtils.isEmpty(dados.getFaixaFinal())) {
				throw new NegocioException(Tarifa.ERRO_TARIFA_FAIXA_FINAL_NAO_INFORMADA, dados.getFaixaInicial());
			} else if (!iteratorDadosFaixa.hasNext()) {
				dados.setFaixaFinal(VALOR_MAXIMO_FAIXA_FINAL);
			}

		}

		// Valida se não existem lacunas de
		// valores entre as faixas
		String ultimaFaixaFinal = zero;
		for (DadosFaixasTarifa dadosFaixa : listaDadosFaixa) {
			if (!dadosFaixa.getFaixaInicial().equals(ultimaFaixaFinal)) {
				throw new NegocioException(Tarifa.ERRO_TARIFA_LACUNA_NAS_FAIXAS,
								new Object[] {ultimaFaixaFinal, dadosFaixa.getFaixaInicial()});
			}

			validarValoresFaixas(dadosFaixa);

			ultimaFaixaFinal = dadosFaixa.getFaixaFinal();
		}

		return listaDadosFaixa;
	}

	/**
	 * Validar valores faixas.
	 *
	 * @param dadosFaixa
	 *            the dados faixa
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarValoresFaixas(DadosFaixasTarifa dadosFaixa) throws FormatoInvalidoException, NegocioException {

		int countValores = 0;
		if (!StringUtils.isEmpty(dadosFaixa.getValorFixoSemImposto())) {
			BigDecimal valorFixo = Util.converterCampoStringParaValorBigDecimal(TarifaVigenciaFaixa.VALOR_FIXO,
							dadosFaixa.getValorFixoSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			if (valorFixo.compareTo(BigDecimal.ZERO) <= 0) {
				countValores++;
			}
		} else {
			countValores++;
		}

		if (!StringUtils.isEmpty(dadosFaixa.getValorVariavelSemImposto())) {
			BigDecimal valorVariavel = Util.converterCampoStringParaValorBigDecimal(TarifaVigenciaFaixa.VALOR_FIXO,
							dadosFaixa.getValorVariavelSemImposto(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
			if (valorVariavel.compareTo(BigDecimal.ZERO) <= 0) {
				countValores++;
			}
		} else {
			countValores++;
		}

		if (countValores == CONSTANTE_NUMERO_DOIS) {
			throw new NegocioException(Tarifa.ERRO_TARIFA_VALORES_FIXO_VARIAVEL_NAO_INFORMADOS, new String[] {dadosFaixa.getFaixaInicial()});
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #validarFaixasTarifa(java.util.Collection)
	 */
	@Override
	public void validarFaixasTarifa(Collection<DadosFaixasTarifa> listaDadosFaixa, boolean validarValoresFaixas) throws NegocioException,
					FormatoInvalidoException {

		DadosFaixasTarifa dadosFaixasTarifaAnterior = null;
		for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixa) {
			if (dadosFaixasTarifaAnterior != null) {

				Long faixaFinalAnterior = Util.converterCampoStringParaValorLong("faixa final anterior",
								Util.removerVirgulasPontos(dadosFaixasTarifaAnterior.getFaixaFinal()));
				Long faixaInicialAtual = Util.converterCampoStringParaValorLong("faixa inicial atual",
								Util.removerVirgulasPontos(dadosFaixasTarifa.getFaixaInicial()));
				if (faixaInicialAtual.longValue() != faixaFinalAnterior.longValue()+1) {
					throw new NegocioException(ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO, true);
				}
			}
			if (validarValoresFaixas) {
				this.validarValoresFaixas(dadosFaixasTarifa);
			}
			dadosFaixasTarifaAnterior = dadosFaixasTarifa;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #validarFaixasZeradasTarifa(java.util.Collection)
	 */
	@Override
	public void validarFaixasZeradasTarifa(List<DadosFaixasTarifa> listaDadosFaixa) throws NegocioException, FormatoInvalidoException {

		int countValores = 0;
		for (DadosFaixasTarifa dadosFaixasTarifa : listaDadosFaixa) {
			if (dadosFaixasTarifa.getFaixaFinal() == null || "0".equals(dadosFaixasTarifa.getFaixaFinal())) {
				countValores++;
			}
		}
		if (countValores > 1) {
			throw new NegocioException(Tarifa.ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#ordenarFaixasPorMatriz(java.util.Map, java.util.List)
	 */
	@Override
	public List<DadosFaixasTarifa> ordenarFaixasPorMatriz(Map<String, String[]> vetores, List<DadosFaixasTarifa> listaDadosFaixa)
					throws GGASException {

		Map<String, String[]> matrizOrdenada = new TreeMap<String, String[]>();

		String[] mdFim = vetores.get("0");
		String[] vlFixo = vetores.get("1");
		String[] vlVariavel = vetores.get("2");
		String[] vlUltimaColuna = vetores.get("3");

		int index = 0;

		String valorUltimaColuna = "";

		if (vlUltimaColuna.length > 0) {
			valorUltimaColuna = vlUltimaColuna[0];
		}

		for (String valor : mdFim) {

			if (StringUtils.isEmpty(valor) || StringUtils.isEmpty(vlFixo[index]) || StringUtils.isEmpty(vlVariavel[index])) {

				throw new NegocioException(Tarifa.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
			}

			String[] valores = {vlFixo[index], vlVariavel[index]};
			matrizOrdenada.put(valor, valores);
			index++;
		}

		listaDadosFaixa.clear();

		boolean primeiraVez = true;
		String valorAnterior = "0";
		Integer qtdAtual = 0;
		boolean sair = false;
		for (Map.Entry<String, String[]> entry : matrizOrdenada.entrySet()) {
			String[] valores;
			String indice = entry.getKey();
			valores = entry.getValue();
			DadosFaixasTarifa listaFaixa = this.criarDadosFaixasTarifa();
			if (primeiraVez) {
				primeiraVez = false;
				listaFaixa.setFaixaInicial("0");

				BigDecimal indiceAux = Util.converterCampoStringParaValorBigDecimal("Índice", indice, Constantes.FORMATO_VALOR_NUMERO,
								Constantes.LOCALE_PADRAO);
				valorAnterior = indiceAux.toString();
			} else {
				listaFaixa.setFaixaInicial(valorAnterior);
				BigDecimal indiceAux = Util.converterCampoStringParaValorBigDecimal("Índice", indice, Constantes.FORMATO_VALOR_NUMERO,
								Constantes.LOCALE_PADRAO);
				valorAnterior = indiceAux.toString();
			}

			if (!StringUtils.isEmpty(valorUltimaColuna) && Long.parseLong(valorUltimaColuna) >= 0
							&& Long.parseLong(valorUltimaColuna) == qtdAtual) {

				BigDecimal valorMaximo = Util.converterCampoStringParaValorBigDecimal("Valor Máximo", VALOR_MAXIMO_FAIXA_FINAL,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				listaFaixa.setFaixaFinal(valorMaximo.toString());
				sair = true;
			} else {
				listaFaixa.setFaixaFinal(String.valueOf(indice));
			}

			listaFaixa.setValorFixoSemImposto(valores[0]);
			listaFaixa.setValorVariavelSemImposto(valores[1]);
			listaFaixa.setNumeroColunaFinal(qtdAtual);
			listaDadosFaixa.add(listaFaixa);
			qtdAtual++;
			if (sair) {
				break;
			}
		}

		return listaDadosFaixa;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#ordenarFaixasPorVO(java.util.List, java.util.List)
	 */
	@Override
	public List<DadosFaixasTarifa> ordenarFaixasPorVO(List<DadosFaixasTarifa> listaDadosFaixaTemp,
					List<DadosFaixasTarifa> listaDadosFaixaTela) throws GGASException {

		listaDadosFaixaTela.clear();

		for (DadosFaixasTarifa dados : listaDadosFaixaTemp) {
			DadosFaixasTarifa dadosFaixa = criarDadosFaixasTarifa();
			dadosFaixa.setFaixaFinal(dados.getFaixaFinal());
			dadosFaixa.setFaixaInicial(dados.getFaixaInicial());
			dadosFaixa.setValorFixoSemImposto(dados.getValorFixoSemImposto());
			dadosFaixa.setValorCompra(dados.getValorCompra());
			dadosFaixa.setMargemValorAgregado(dados.getMargemValorAgregado());
			dadosFaixa.setValorVariavelSemImposto(dados.getValorVariavelSemImposto());
			dadosFaixa.setIdTarifaVigenciaFaixa(dados.getIdTarifaVigenciaFaixa());

			listaDadosFaixaTela.add(dadosFaixa);
		}

		Collections.sort(listaDadosFaixaTela, new Comparator<DadosFaixasTarifa>(){

			@Override
			public int compare(DadosFaixasTarifa o1, DadosFaixasTarifa o2) {

				return Long.valueOf(o1.getFaixaInicial()).compareTo(Long.valueOf(o2.getFaixaInicial()));
			}

		});

		return listaDadosFaixaTela;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarDescontosCadastrados(java.lang.Long)
	 */
	@Override
	public List<TarifaFaixaDesconto> consultarDescontosCadastrados(Long chaveTarifa) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select new br.com.ggas.faturamento.tarifa.impl.VigenciaDescontosCadastradoImpl("
						+ "tvd.chavePrimaria, tvd.inicioVigencia, tvd.fimVigencia) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigenciaDesconto().getSimpleName()).append(" tvd ");
		hql.append(" where ");
		hql.append(" tvd.tarifa.chavePrimaria = :CHAVE_TARIFA ");
		hql.append(" and tvd.habilitado = :HABILITADO ");
		hql.append(" order by tvd.inicioVigencia desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_TARIFA", chaveTarifa);
		query.setParameter("HABILITADO", Boolean.TRUE);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarVigenciasTarifa(java.lang.Long)
	 */
	@Override
	public List<TarifaVigencia> consultarVigenciasTarifa(Long chaveTarifa) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName()).append(" tv ");
		hql.append(" where ");
		hql.append(" tv.tarifa.chavePrimaria = :CHAVE_TARIFA ");
		hql.append(" and tv.habilitado = :HABILITADO ");
		hql.append(" order by tv.dataVigencia desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_TARIFA", chaveTarifa);
		query.setParameter("HABILITADO", Boolean.TRUE);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #cadastrarDescontos(br.com.
	 * ggas.faturamento.tarifa.TarifaVigencia,
	 * br.com
	 * .ggas.faturamento.tarifa.TarifaVigenciaDesconto
	 * , java.util.List)
	 */
	@Override
	public void cadastrarDescontos(TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws NegocioException {

		this.validarDataTarifaDesconto(tarifaVigencia, tarifaVigenciaDesconto);

		this.validarTarifaVigenciaDesconto(tarifaVigenciaDesconto, listaDadosFaixa);
		this.validarVigenciaDescontoCadastrado(tarifaVigenciaDesconto);

		this.inserir(tarifaVigenciaDesconto);
		try {
			this.validaDadosInserirTarifa(tarifaVigencia, tarifaVigenciaDesconto, listaDadosFaixa);
			Collection<TarifaFaixaDesconto> tarifaFaixaDescontos = this.popularTarifaFaixaDescontoNovoCadastro(tarifaVigenciaDesconto,
							tarifaVigencia, listaDadosFaixa);

			for (TarifaFaixaDesconto tarifaFaixaDescontoLoop : tarifaFaixaDescontos) {
				tarifaFaixaDescontoLoop.setDadosAuditoria(tarifaVigenciaDesconto.getDadosAuditoria());
				this.inserir(tarifaFaixaDescontoLoop);
			}
		} catch (FormatoInvalidoException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new NegocioException(ERRO_TARIFA_FAIXAS_DESCONTO_NAO_PREENCHIDAS_CORRETAMENTE, true);
		}
	}

	/**
	 * Validar vigencia desconto cadastrado.
	 *
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarVigenciaDescontoCadastrado(TarifaVigenciaDesconto tarifaVigenciaDesconto) throws NegocioException {

		List<TarifaVigenciaDesconto> lista = consultarTarifasVigenciaDescontoPorTarifaVigencia(tarifaVigenciaDesconto.getTarifaVigencia()
						.getChavePrimaria());

		if (lista != null && !lista.isEmpty()) {
			for (TarifaVigenciaDesconto tvd : lista) {

				Date dataInicio = tvd.getInicioVigencia();
				Date dataFim = tvd.getFimVigencia();
				if ((dataInicio.compareTo(tarifaVigenciaDesconto.getInicioVigencia()) <= 0)
								&& (dataFim.compareTo(tarifaVigenciaDesconto.getInicioVigencia()) >= 0)) {

					throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DESCONTO_SOBREPOSTO, true);
				}
				if ((dataInicio.compareTo(tarifaVigenciaDesconto.getFimVigencia()) <= 0)
								&& (dataFim.compareTo(tarifaVigenciaDesconto.getFimVigencia()) >= 0)) {

					throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DESCONTO_SOBREPOSTO, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #removerDescontoCadastrado(java.lang.Long,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerDescontoCadastrado(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws NegocioException {

		if (chavePrimaria != null && chavePrimaria > 0) {
			TarifaVigenciaDesconto tarifaVigenciaDesconto = (TarifaVigenciaDesconto) super.obter(chavePrimaria,
							getClasseEntidadeTarifaVigenciaDesconto());
			tarifaVigenciaDesconto.setDadosAuditoria(dadosAuditoria);
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idTarifaVigenciaDesconto", tarifaVigenciaDesconto.getChavePrimaria());
			Collection<TarifaFaixaDesconto> listaTarifaFaixaDesconto = this.consultarTarifasFaixaDesconto(filtro);
			for (TarifaFaixaDesconto tarifaFaixaDesconto : listaTarifaFaixaDesconto) {
				tarifaFaixaDesconto.setDadosAuditoria(dadosAuditoria);
				super.remover(tarifaFaixaDesconto);
			}
			super.remover(tarifaVigenciaDesconto);
		} else {
			throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_EXCLUSAO_DESCONTO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#removerVigenciaCadastrada(java.lang.Long, br.com.ggas.auditoria.DadosAuditoria)
	 */
	/*
	 * tvd - Tarifa Vigência Desconto
	 * tfd - Tarifa Faixa Desconto
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #removerVigenciaCadastrada(java.lang.Long,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerVigenciaCadastrada(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws NegocioException {

		if (chavePrimaria != null && chavePrimaria > 0) {
			TarifaVigencia tarifaVigencia = (TarifaVigencia) super.obter(chavePrimaria, getClasseEntidadeTarifaVigencia(), "tarifa");

			Collection<TarifaVigencia> listaVigencias = this.consultarVigenciasTarifa(tarifaVigencia.getTarifa().getChavePrimaria());

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idTarifaVigencia", tarifaVigencia.getChavePrimaria());
			Collection<TarifaVigenciaDesconto> listaVigenciaDesconto = this.consultarTarifasVigenciaDesconto(filtro);
			for (TarifaVigenciaDesconto tvd : listaVigenciaDesconto) {
				Map<String, Object> filtroDesconto = new HashMap<String, Object>();
				filtroDesconto.put("idTarifaVigenciaDesconto", tvd.getChavePrimaria());
				Collection<TarifaFaixaDesconto> listaTarifaFaixaDesconto = this.consultarTarifasFaixaDesconto(filtroDesconto);
				for (TarifaFaixaDesconto tfd : listaTarifaFaixaDesconto) {
					tfd.setDadosAuditoria(dadosAuditoria);
					super.remover(tfd);
				}
				tvd.setDadosAuditoria(dadosAuditoria);
				super.remover(tvd);
			}
			tarifaVigencia.setDadosAuditoria(dadosAuditoria);
			super.remover(tarifaVigencia);

			// Se a tarifa só tiver apenas uma
			// vigência, não será permitida a
			// exclusão.
			if ((listaVigencias != null) && (!listaVigencias.isEmpty()) && (listaVigencias.size() == 1)) {
				throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_EXCLUSAO_VIGENCIA_COM_TARIFA, true);
			}

		} else {
			throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_EXCLUSAO_VIGENCIA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#adicionarFaixaFinalDesconto(java.util.List, java.lang.Long)
	 */
	@Override
	public List<DadosFaixasTarifa> adicionarFaixaFinalDesconto(List<DadosFaixasTarifa> listaDadosFaixa, Long indexUltimaFaixa) {

		Collection<DadosFaixasTarifa> dadosExluidos = new ArrayList<DadosFaixasTarifa>();

		boolean excluir = false;
		for (DadosFaixasTarifa dados : listaDadosFaixa) {

			if (excluir) {
				dadosExluidos.add(dados);
			}
			if (dados.getFaixaInicial().equals(String.valueOf(indexUltimaFaixa))) {
				dados.setFaixaFinal(this.VALOR_MAXIMO_FAIXA_FINAL);
				excluir = true;
			}
		}

		listaDadosFaixa.removeAll(dadosExluidos);
		return listaDadosFaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#validarDadosAdicionarAssocTempTarifaPontoConsumo(java.util.Date,
	 * java.util.Date, java.lang.Long, java.lang.Long)
	 */
	@Override
	public void validarDadosAdicionarAssocTempTarifaPontoConsumo(Date dataInicial, Date dataFinal, Long idItemFatura, Long idTarifa)
					throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (idTarifa == null || idTarifa < 1) {
			stringBuilder.append(TarifaPontoConsumo.TARIFA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (idItemFatura == null || idItemFatura < 1) {
			stringBuilder.append(Tarifa.TARIFA_ITEM_FATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataInicial == null) {
			stringBuilder.append(TarifaPontoConsumo.DATA_INICIO_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataFinal == null) {
			stringBuilder.append(TarifaPontoConsumo.DATA_FIM_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
			throw new NegocioException(erros);
		} else {
			if (dataInicial != null && dataFinal != null && Util.compararDatas(dataInicial, dataFinal) > 0) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_MAIOR_QUE_DATA_FIM, true);
			}
		}
	}

	/**
	 * Validar data tarifa ponto consumo.
	 *
	 * @param tarifaPontoConsumo
	 *            the tarifa ponto consumo
	 * @param dataVigencia
	 *            the data vigencia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarDataTarifaPontoConsumo(TarifaPontoConsumo tarifaPontoConsumo, Date dataVigencia) throws NegocioException {

		if (tarifaPontoConsumo.getDataInicioVigencia() != null && dataVigencia != null
						&& Util.compararDatas(tarifaPontoConsumo.getDataInicioVigencia(), dataVigencia) < 0) {
			throw new NegocioException(ERRO_DATA_INICIAL_TARIFA_CONSUMO_MENOR_DATA_VIGENCIA_TARIFA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#criarTarifaPontoConsumo()
	 */
	@Override
	public EntidadeNegocio criarTarifaPontoConsumo() {

		return (TarifaPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(TarifaPontoConsumo.BEAN_ID_TARIFA_PONTOCONSUMO);
	}

	/**
	 * Inserir tarifa ponto consumo.
	 *
	 * @param itemTarifaPontoConsumo
	 *            the item tarifa ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */

	public void inserirTarifaPontoConsumo(TarifaPontoConsumo itemTarifaPontoConsumo) throws NegocioException {

		this.inserir(itemTarifaPontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#validarTarifaFaturamento(br.com.ggas.faturamento.tarifa.TarifaPontoConsumo)
	 */
	@Override
	public void validarTarifaFaturamento(TarifaPontoConsumo tarifaPontoConsumo) throws NegocioException {

		ControladorFatura cFatura = ServiceLocator.getInstancia().getControladorFatura();

		Collection<FaturaItem> listaFaturaItem = cFatura.listarFaturaItemPorTarifa(tarifaPontoConsumo.getTarifa().getChavePrimaria());
		for (FaturaItem faturaItem : listaFaturaItem) {

			// se existir algum item de fatura com
			// o ponto de consumo da tarifa
			if (tarifaPontoConsumo.getPontoConsumo().getChavePrimaria() == faturaItem.getFatura().getPontoConsumo().getChavePrimaria()) {
				throw new NegocioException(ERRO_TARIFA_SENDO_USADA_FATURA, true);
			}

		}

	}

	/**
	 * Atualizar tarifa ponto consumo.
	 *
	 * @param tarifaPontoConsumo
	 *            the tarifa ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void atualizarTarifaPontoConsumo(TarifaPontoConsumo tarifaPontoConsumo) throws NegocioException, ConcorrenciaException {

		this.atualizar(tarifaPontoConsumo, getClasseEntidadeTarifaPontoConsumo());
	}

	/**
	 * Validar contrato ponto consumo.
	 *
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarContratoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		if (contratoPontoConsumo == null) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO_ATIVO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.ControladorTarifa#listarItemFaturaPontoConsumo(br.com.ggas.contrato.contrato.ContratoPontoConsumo)
	 */
	@Override
	public Collection<EntidadeConteudo> listarItemFaturaPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		validarContratoPontoConsumo(contratoPontoConsumo);

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeConteudo().getSimpleName());
		hql.append(" itemFatura ");
		hql.append(" where ");
		hql.append(" itemFatura.chavePrimaria in ( ");
		hql.append(" select contratoPontoConsumoItemFaturamento.itemFatura.chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumoItemFaturamento().getSimpleName());
		hql.append(" contratoPontoConsumoItemFaturamento ");
		hql.append(" where ");
		hql.append(" contratoPontoConsumoItemFaturamento.contratoPontoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and");
		hql.append(" contratoPontoConsumoItemFaturamento.contratoPontoConsumo.contrato.chavePrimaria = :idContrato");
		hql.append(" ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", contratoPontoConsumo.getPontoConsumo().getChavePrimaria());
		query.setLong("idContrato", contratoPontoConsumo.getContrato().getChavePrimaria());

		Collection<EntidadeConteudo> entidades = query.list();
		if (entidades != null && entidades.isEmpty()) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO, true);
		}

		return entidades;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifas(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Tarifa> obterTarifas(Long chavePontoConsumo, Long chaveItemFatura) throws NegocioException {

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias("segmento", "segmento");
		criteria.createAlias("ramoAtividade", "ramoAtividade");
		criteria.add(Restrictions.idEq(chavePontoConsumo));

		PontoConsumo pontoConsumo = (PontoConsumo) criteria.uniqueResult();

		criteria = this.createCriteria(Tarifa.class);
		criteria.add(Restrictions.eq("itemFatura.chavePrimaria", chaveItemFatura));
		criteria.add(Restrictions.eq("segmento.chavePrimaria", pontoConsumo.getSegmento().getChavePrimaria()));

		RamoAtividade ramoAtividade = pontoConsumo.getRamoAtividade();
		Criterion restricaoRamoAtividade = Restrictions.eq("ramoAtividade.chavePrimaria", ramoAtividade.getChavePrimaria());
		Criterion restricaoRamoAtividadeNulo = Restrictions.isNull("ramoAtividade");
		Criterion restricaoOR = Restrictions.or(restricaoRamoAtividade, restricaoRamoAtividadeNulo);
		criteria.add(restricaoOR);

		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));

		Collection<Tarifa> listaTarifa = verificarListaTarifa(criteria.list());
		return (List<Tarifa>) listaTarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifasPorRamoAtividade(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Tarifa> obterTarifasPorRamoAtividade(Long chavePontoConsumo, Long chaveItemFatura) throws NegocioException {

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias("ramoAtividade", "ramoAtividade");
		criteria.add(Restrictions.idEq(chavePontoConsumo));

		PontoConsumo pontoConsumo = (PontoConsumo) criteria.uniqueResult();

		criteria = this.createCriteria(Tarifa.class);
		criteria.add(Restrictions.eq("itemFatura.chavePrimaria", chaveItemFatura));
		criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", pontoConsumo.getRamoAtividade().getChavePrimaria()));

		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));

		Collection<Tarifa> listaTarifa = verificarListaTarifa(criteria.list());
		return (List<Tarifa>) listaTarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifaVigenciaItemFatura(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Collection<TarifaVigencia> listarTarifaVigenciaItemFatura(Long idItemFatura, Long idSegmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tarifaVigencia ");
		hql.append(" inner join fetch tarifaVigencia.tarifa tarifa ");
		hql.append(" inner join fetch tarifa.itemFatura itemFatura ");
		hql.append(" inner join fetch tarifa.segmento segmento ");
		hql.append(" where ");
		hql.append(" itemFatura.chavePrimaria = :idItemFatura");
		hql.append(" and segmento.chavePrimaria = :idSegmento");

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// Restrição de tarifa autorizada (Alçada)
		String statusAutorizado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_AUTORIZADO);
		if (statusAutorizado != null) {
			hql.append(" and tarifaVigencia.status.chavePrimaria = :idStatus");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idItemFatura", idItemFatura);
		query.setLong("idSegmento", idSegmento);
		if (statusAutorizado != null) {
			query.setLong("idStatus", Long.parseLong(statusAutorizado));
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterDataVigenciaTarifa(java.lang.Long)
	 */
	@Override
	public Date obterDataVigenciaTarifa(Long idTarifa) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tarifaVigencia ");
		hql.append(" inner join fetch tarifaVigencia.tarifa tarifa ");

		hql.append(" where ");
		hql.append(" tarifa.chavePrimaria = :idTarifa ");
		hql.append(" order by tarifaVigencia.dataVigencia asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idTarifa", idTarifa);
		query.setMaxResults(1);

		TarifaVigencia tarifaVigencia = (TarifaVigencia) query.uniqueResult();
		Date menorVigencia = null;

		if (tarifaVigencia != null) {
			menorVigencia = tarifaVigencia.getDataVigencia();
		}

		return menorVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#validarItemFaturaRepetidoTarifaPontoConsumo(java.util.Collection,
	 * br.com.ggas.faturamento.tarifa.TarifaPontoConsumo, java.lang.Integer)
	 */
	@Override
	public void validarItemFaturaRepetidoTarifaPontoConsumo(Collection<TarifaPontoConsumo> listaTarifasPontosConsumo,
					TarifaPontoConsumo tarifaPontoConsumo, Integer index) throws NegocioException {

		List<TarifaPontoConsumo> listaTarifaPontoConsumo = new ArrayList<TarifaPontoConsumo>();

		if (listaTarifasPontosConsumo != null) {
			listaTarifaPontoConsumo.addAll(listaTarifasPontosConsumo);
		}

		if (listaTarifasPontosConsumo != null && !listaTarifasPontosConsumo.isEmpty() && index > -1) {
			listaTarifaPontoConsumo.remove(index.intValue());
		}

		if (tarifaPontoConsumo != null) {
			for (TarifaPontoConsumo tarifaPontoConsumoAtual : listaTarifaPontoConsumo) {

				this.validarItemFaturaRepetido(tarifaPontoConsumo, tarifaPontoConsumoAtual);

			}
		}
	}

	private void validarItemFaturaRepetido(TarifaPontoConsumo tarifaPontoConsumo, TarifaPontoConsumo tarifaPontoConsumoAtual)
			throws NegocioException {

		if (tarifaPontoConsumo.getDataInicioVigencia().after(tarifaPontoConsumoAtual.getDataInicioVigencia())
				|| tarifaPontoConsumo.getDataInicioVigencia().equals(tarifaPontoConsumoAtual.getDataInicioVigencia())) {
			if (tarifaPontoConsumoAtual.getDataFimVigencia() == null) {
				throw new NegocioException(ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO, true);
			} else {
				if (tarifaPontoConsumo.getDataInicioVigencia().before(tarifaPontoConsumoAtual.getDataFimVigencia())
						|| tarifaPontoConsumo.getDataInicioVigencia().equals(tarifaPontoConsumoAtual.getDataFimVigencia())) {
					throw new NegocioException(ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO, true);
				}
			}
		} else {
			if (tarifaPontoConsumo.getDataInicioVigencia().before(tarifaPontoConsumoAtual.getDataInicioVigencia())) {
				if (tarifaPontoConsumo.getDataFimVigencia() == null) {
					throw new NegocioException(ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO, true);
				} else {

					if (tarifaPontoConsumoAtual.getDataFimVigencia() != null) {
						if ((tarifaPontoConsumo.getDataInicioVigencia().after(tarifaPontoConsumoAtual.getDataFimVigencia()) || tarifaPontoConsumo
								.getDataInicioVigencia().equals(tarifaPontoConsumoAtual.getDataFimVigencia()))
								&& tarifaPontoConsumoAtual.getDataFimVigencia() != null) {
							throw new NegocioException(ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO, true);
						} else {
							if ((tarifaPontoConsumo.getDataFimVigencia().after(tarifaPontoConsumoAtual.getDataInicioVigencia()) || tarifaPontoConsumo
									.getDataFimVigencia().equals(tarifaPontoConsumoAtual.getDataInicioVigencia()))
									&& tarifaPontoConsumoAtual.getDataFimVigencia() != null) {
								throw new NegocioException(ERRO_ITEM_FATURA_REPETIDO_TARIFA_PONTO_CONSUMO, true);
							}
						}
					}
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifaPontoConsumo(java.lang.Long)
	 */
	@Override
	public Collection<TarifaPontoConsumo> listarTarifaPontoConsumo(Long idPontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaPontoConsumo().getSimpleName());
		hql.append(" tarifaPontoConsumo ");
		hql.append(" inner join fetch tarifaPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch tarifaPontoConsumo.tarifa tarifa ");
		hql.append(" inner join fetch tarifa.itemFatura itemFatura ");
		hql.append(" where ");
		hql.append(" pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" order by tarifaPontoConsumo.dataInicioVigencia desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", idPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#validarPontoConsumoImovelContrato(java.util.Collection)
	 */
	@Override
	public void validarPontoConsumoImovelContrato(Collection<EntidadeConteudo> itensFatura) throws NegocioException {

		if (itensFatura.isEmpty()) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_NAO_ASSOCIADO_CONTRATO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#atualizarTarifasPontoConsumo(java.lang.Long, java.util.Collection,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void atualizarTarifasPontoConsumo(Long idPontoConsumo, Collection<TarifaPontoConsumo> itensTarifaPontoConsumo,
					DadosAuditoria dadosAuditoria) throws NegocioException {

		if (itensTarifaPontoConsumo != null && idPontoConsumo != null) {

			Collection<TarifaPontoConsumo> itensTarifaPontoConsumoBanco = this.listarTarifaPontoConsumo(idPontoConsumo);

			// Inserir os novos no banco
			Boolean bflagTributo;
			for (TarifaPontoConsumo tarifaPontoConsumo : itensTarifaPontoConsumo) {
				bflagTributo = false;

				for (TarifaPontoConsumo tarifaPontoConsumoBanco : itensTarifaPontoConsumoBanco) {
					if (tarifaPontoConsumo.getChavePrimaria() == tarifaPontoConsumoBanco.getChavePrimaria()) {
						bflagTributo = true;
					}
				}

				if (!bflagTributo) {
					this.inserirTarifaPontoConsumo(tarifaPontoConsumo);
				}
			}

			// desabilitar os existente
			for (TarifaPontoConsumo tarifaPontoConsumoBanco : itensTarifaPontoConsumoBanco) {
				bflagTributo = false;

				for (TarifaPontoConsumo tarifaPontoConsumo : itensTarifaPontoConsumo) {

					if (tarifaPontoConsumo.getChavePrimaria() == tarifaPontoConsumoBanco.getChavePrimaria()) {

						tarifaPontoConsumoBanco.setPontoConsumo(tarifaPontoConsumo.getPontoConsumo());
						tarifaPontoConsumoBanco.setDataInicioVigencia(tarifaPontoConsumo.getDataInicioVigencia());
						tarifaPontoConsumoBanco.setDataFimVigencia(tarifaPontoConsumo.getDataFimVigencia());
						tarifaPontoConsumoBanco.setTarifa(tarifaPontoConsumo.getTarifa());
						tarifaPontoConsumoBanco.setDadosAuditoria(dadosAuditoria);

						bflagTributo = true;

					}
				}

				if (!bflagTributo) {

					tarifaPontoConsumoBanco.setDadosAuditoria(dadosAuditoria);
					this.remover(tarifaPontoConsumoBanco);

				} else {

					try {

						this.atualizarTarifaPontoConsumo(tarifaPontoConsumoBanco);

					} catch (ConcorrenciaException e) {
						LOG.error(e.getMessage(), e);
						throw new NegocioException(ERRO_CONCORRENCIA, true);

					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#adicionarFaixaDesconto(br.com.ggas.faturamento.tarifa.DadosFaixasTarifa,
	 * java.util.List)
	 */
	@Override
	public List<DadosFaixasTarifa> adicionarFaixaDesconto(DadosFaixasTarifa novaFaixa, List<DadosFaixasTarifa> listaDadosFaixa)
					throws NegocioException {

		if ("".equals(novaFaixa.getFaixaInicial().trim()) || "0".equals(novaFaixa.getFaixaInicial().trim())) {
			throw new NegocioException(Tarifa.ERRO_FAIXAS_VAZIAS, true);
		}

		for (DadosFaixasTarifa dados : listaDadosFaixa) {

			if (dados.getFaixaInicial().equals(novaFaixa.getFaixaInicial())) {
				throw new NegocioException(Tarifa.ERRO_FAIXA_JA_EXISTE, true);
			}

			if (novaFaixa.getFaixaFinal() != null && !novaFaixa.getFaixaFinal().equals(VALOR_MAXIMO_FAIXA_FINAL)
							&& Integer.parseInt(dados.getFaixaInicial()) <= Integer.parseInt(novaFaixa.getFaixaInicial())
							&& Integer.parseInt(novaFaixa.getFaixaInicial()) <= Integer.parseInt(dados.getFaixaFinal())) {
				throw new NegocioException(Tarifa.ERRO_FAIXAS_NAO_PERMITIDAS, true);
			}
		}
		listaDadosFaixa.add(novaFaixa);

		return listaDadosFaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#montarListaFaixasTarifaDesconto(br.com.ggas.faturamento.tarifa.TarifaVigencia,
	 * br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto)
	 */
	@Override
	public Collection<FaixasTarifaDesconto> montarListaFaixasTarifaDesconto(TarifaVigencia tarifaVigencia,
					TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException {

		Collection<FaixasTarifaDesconto> listaFaixasTarifaDesconto = new ArrayList<FaixasTarifaDesconto>();

		Map<Long, TarifaFaixaDesconto> mapaFaixasDesconto = new HashMap<Long, TarifaFaixaDesconto>();
		if (tarifaVigenciaDesconto != null) {
			Collection<TarifaFaixaDesconto> listaFaixasDesconto = consultarTarifasFaixaDesconto(tarifaVigenciaDesconto.getChavePrimaria());
			if (listaFaixasDesconto != null && !listaFaixasDesconto.isEmpty()) {
				for (TarifaFaixaDesconto tarifaFaixaDesconto : listaFaixasDesconto) {
					mapaFaixasDesconto.put(tarifaFaixaDesconto.getTarifaVigenciaFaixa().getChavePrimaria(), tarifaFaixaDesconto);
				}
			}
		}

		Collection<TarifaVigenciaFaixa> listaFaixasVigencia = null;
		if (tarifaVigencia != null) {
			listaFaixasVigencia = tarifaVigencia.getTarifaVigenciaFaixas();

			for (TarifaVigenciaFaixa tarifaVigenciaFaixa : listaFaixasVigencia) {
				FaixasTarifaDesconto faixasTarifaDesconto = this.montarDadosFaixasTarifaDesconto(tarifaVigenciaFaixa,
								mapaFaixasDesconto.get(tarifaVigenciaFaixa.getChavePrimaria()));
				listaFaixasTarifaDesconto.add(faixasTarifaDesconto);
			}
		}

		BeanComparator comparador = new BeanComparator("medidaInicio");
		Collections.sort((List) listaFaixasTarifaDesconto, comparador);

		return listaFaixasTarifaDesconto;
	}

	/**
	 * Validar adicao faixa desconto.
	 *
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @param validarValoresFaixas
	 *            the validar valores faixas
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Override
	public void validarAdicaoFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixa, boolean validarValoresFaixas) throws GGASException {

		Long inicio;
		Long fim;
		int i = 0;
		for (DadosFaixasTarifa dadosFaixaDesconto : listaDadosFaixa) {
			fim = null;
			inicio = Util.converterCampoStringParaValorLong("Faixa Início", dadosFaixaDesconto.getFaixaInicial());

			if (!"".equals(dadosFaixaDesconto.getFaixaFinal().trim())) {
				fim = Util.converterCampoStringParaValorLong("Faixa Final", dadosFaixaDesconto.getFaixaFinal());
			}

			if (i != listaDadosFaixa.size() - 1 && fim == null) {
				throw new NegocioException(Tarifa.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
			}
			if (fim != null && inicio >= fim) {
				throw new NegocioException(DadosFaixasTarifa.ERRO_FAIXAS_INVALIDAS, true);
			}

			String valorFixoSemImposto = dadosFaixaDesconto.getValorFixoSemImposto();
			String valorVariavelSemImposto = dadosFaixaDesconto.getValorVariavelSemImposto();

			if (valorFixoSemImposto == null || "".equals(valorFixoSemImposto)) {
				throw new NegocioException(Tarifa.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
			}

			if (valorVariavelSemImposto == null || "".equals(valorVariavelSemImposto)) {
				throw new NegocioException(Tarifa.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
			}

			if (validarValoresFaixas) {
				BigDecimal valorFixo = Util.converterCampoStringParaValorBigDecimal("Valor Fixo Sem Imposto", valorFixoSemImposto,
								Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);
				BigDecimal valorVariavel = Util.converterCampoStringParaValorBigDecimal("Valor Variável Sem Imposto",
								valorVariavelSemImposto, Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

				if ((valorFixo.compareTo(BigDecimal.ZERO) == 0) && (valorVariavel.compareTo(BigDecimal.ZERO) == 0)) {
					throw new NegocioException(Tarifa.ERRO_VALOR_FAIXA_TARIFA_INVALIDO, true);
				}
			}

			i++;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#validarFaixaDesconto(br.com.ggas.faturamento.tarifa.DadosFaixasTarifa)
	 */
	@Override
	public void validarFaixaDesconto(DadosFaixasTarifa dadosFaixaDesconto) throws NegocioException {

		// Validação: ver se foi preenchido os
		// campos.
		if (dadosFaixaDesconto.getValorFixoSemImposto().isEmpty() || dadosFaixaDesconto.getValorVariavelSemImposto().isEmpty()
						|| dadosFaixaDesconto.getFaixaFinal().isEmpty()) {

			throw new NegocioException(Tarifa.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #validarFaixaDesconto(java.util.List)
	 */
	@Override
	public void validarFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixaDesconto) throws GGASException {

		// Validação: ver se foi preenchido os
		// campos.

		for (DadosFaixasTarifa dadosFaixaDesconto : listaDadosFaixaDesconto) {
			if (StringUtils.isEmpty(dadosFaixaDesconto.getValorFixoSemImposto())
							|| StringUtils.isEmpty(dadosFaixaDesconto.getValorVariavelSemImposto())
							|| StringUtils.isEmpty(dadosFaixaDesconto.getFaixaFinal())) {
				throw new NegocioException(Tarifa.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
			} else {

				Long inicio = Util.converterCampoStringParaValorLong("Faixa Início", dadosFaixaDesconto.getFaixaInicial());
				Long fim = Util.converterCampoStringParaValorLong("Faixa Final", dadosFaixaDesconto.getFaixaFinal());

				if (inicio >= fim) {
					throw new NegocioException(DadosFaixasTarifa.ERRO_FAIXAS_INVALIDAS, true);
				}

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #validarVigenciaEDescontoParaExibicaoFaixas
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public void validarVigenciaEDescontoParaExibicaoFaixas(Long idTarifaVigencia) throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (idTarifaVigencia == null || idTarifaVigencia <= 0) {
			stringBuilder.append(Tarifa.VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(ControladorTarifa.ERRO_NEGOCIO_EXIBICAO_FAIXAS_TARIFA_DESCONTO,
							camposObrigatorios.substring(0, stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
			throw new NegocioException(erros);
		}
	}

	/**
	 * Montar dados faixas tarifa desconto.
	 *
	 * @param tarifaVigenciaFaixa
	 *            the tarifa vigencia faixa
	 * @param tarifaFaixaDesconto
	 *            the tarifa faixa desconto
	 * @return the faixas tarifa desconto
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private FaixasTarifaDesconto montarDadosFaixasTarifaDesconto(TarifaVigenciaFaixa tarifaVigenciaFaixa,
					TarifaFaixaDesconto tarifaFaixaDesconto) throws GGASException {

		ControladorCalculoFornecimentoGas controladorCalculoFornecimentoGas = ServiceLocator.getInstancia()
						.getControladorCalculoFornecimentoGas();
		ControladorTributo controladorTributos = ServiceLocator.getInstancia().getControladorTributo();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String parametroTipoArredondamento = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(TIPO_ARREDONDAMENTO_CALCULO);
		Integer tipoArredondamento = Integer.valueOf(parametroTipoArredondamento);

		FaixasTarifaDesconto faixa = this.criarFaixasTarifaDesconto();

		TarifaVigencia tarifaVigencia = obterTarifaVigencia(tarifaVigenciaFaixa.getTarifaVigencia().getChavePrimaria());
		Tarifa tarifa = obterTarifa(tarifaVigenciaFaixa.getTarifaVigencia().getTarifa().getChavePrimaria());

		faixa.setQtdeCasasDecimais(tarifa.getQuantidadeCasaDecimal().intValue());
		faixa.setMedidaInicio(tarifaVigenciaFaixa.getMedidaInicio());
		faixa.setMedidaFim(tarifaVigenciaFaixa.getMedidaFim());
		faixa.setValorFixo(tarifaVigenciaFaixa.getValorFixo());
		faixa.setValorCompra(tarifaVigenciaFaixa.getValorCompra());
		faixa.setMargemValorAgregado(tarifaVigenciaFaixa.getMargemValorAgregado());
		faixa.setValorVariavel(tarifaVigenciaFaixa.getValorVariavel());
		BigDecimal valorLiquidoComImpostoFixo = null;
		BigDecimal valorLiquidoComImpostoVariavel = null;
		if (tarifaFaixaDesconto != null) {
			if (tarifaFaixaDesconto.getValorDescontoFixo() != null) {
				faixa.setValorDescontoFixo(tarifaFaixaDesconto.getValorDescontoFixo());
			} else if (tarifaFaixaDesconto.getPercentualDescontoFixo() != null) {
				BigDecimal valorDescontoFixo = faixa.getValorFixo().multiply(tarifaFaixaDesconto.getPercentualDescontoFixo());
				faixa.setValorDescontoFixo(valorDescontoFixo);
			}

			if (tarifaFaixaDesconto.getValorDescontoVariavel() != null) {
				faixa.setValorDescontoVariavel(tarifaFaixaDesconto.getValorDescontoVariavel());
			} else if (tarifaFaixaDesconto.getPercentualDescontoVariavel() != null) {
				BigDecimal valorDescontoVariavel = faixa.getValorVariavel().multiply(tarifaFaixaDesconto.getPercentualDescontoVariavel());
				faixa.setValorDescontoVariavel(valorDescontoVariavel);
			}

			valorLiquidoComImpostoFixo = tarifaFaixaDesconto.getValorLiquidoSemImpostoFixo();
			valorLiquidoComImpostoVariavel = tarifaFaixaDesconto.getValorLiquidoSemImpostoVariavel();

			faixa.setValorLiquidoSemImpostoFixo(valorLiquidoComImpostoFixo);
			faixa.setValorLiquidoSemImpostoVariavel(valorLiquidoComImpostoVariavel);

		} else {

			faixa.setValorDescontoFixo(BigDecimal.ZERO);
			faixa.setValorDescontoVariavel(BigDecimal.ZERO);

			valorLiquidoComImpostoFixo = faixa.getValorFixo();
			valorLiquidoComImpostoVariavel = faixa.getValorVariavel();

			faixa.setValorLiquidoSemImpostoFixo(valorLiquidoComImpostoFixo);
			faixa.setValorLiquidoSemImpostoVariavel(valorLiquidoComImpostoVariavel);
		}

		if (tarifaVigencia.getTributos() != null && !tarifaVigencia.getTributos().isEmpty()) {

			Integer qtdCasasDecimais = tarifa.getQuantidadeCasaDecimal().intValue();
			BigDecimal icmsSubstituidoMetroCubico = BigDecimal.ZERO;
			
			if(tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
				icmsSubstituidoMetroCubico = tarifaVigencia.getIcmsSubstituidoMetroCubico();
			}
			
			Collection<TributoAliquota> colecaoTributoAliquota = controladorTributos
							.consultarTributosAliquotaPorTarifaTributos(tarifaVigencia.getTributos());

			Collection<DadosTributoVO> colecaoTributosCalculadosFixo = controladorCalculoFornecimentoGas.calcularTributos(null,
							tarifa.getItemFatura(), colecaoTributoAliquota, qtdCasasDecimais, tipoArredondamento,
							valorLiquidoComImpostoFixo, null, null, null);

			for (DadosTributoVO dtVO : colecaoTributosCalculadosFixo) {
				valorLiquidoComImpostoFixo = valorLiquidoComImpostoFixo.add(dtVO.getValor());
			}

			if(valorLiquidoComImpostoFixo.compareTo(BigDecimal.ZERO) != 0) {
				BigDecimal qtdM3 = tarifaVigenciaFaixa.getMedidaFim().subtract(tarifaVigenciaFaixa.getMedidaInicio());
				BigDecimal valorIcmsM3 = tarifaVigencia.getIcmsSubstituidoMetroCubico().multiply(qtdM3);
				valorLiquidoComImpostoFixo = valorLiquidoComImpostoFixo.add(valorIcmsM3);
			}
			
			faixa.setValorLiquidoComImpostoFixo(valorLiquidoComImpostoFixo);
			
			Collection<DadosTributoVO> colecaoTributosCalculadosVariavel = controladorCalculoFornecimentoGas.calcularTributos(null,
							tarifa.getItemFatura(), colecaoTributoAliquota, qtdCasasDecimais, tipoArredondamento,
							valorLiquidoComImpostoVariavel, null, null, null);
			for (DadosTributoVO dtVO : colecaoTributosCalculadosVariavel) {
				valorLiquidoComImpostoVariavel = valorLiquidoComImpostoVariavel.add(dtVO.getValor());
			}
			
			if(valorLiquidoComImpostoVariavel.compareTo(BigDecimal.ZERO) != 0) {
				valorLiquidoComImpostoVariavel = valorLiquidoComImpostoVariavel.add(icmsSubstituidoMetroCubico);
			}
			
			faixa.setValorLiquidoComImpostoVariavel(valorLiquidoComImpostoVariavel);

		} else {
			
			BigDecimal icmsSubstituidoMetroCubico = BigDecimal.ZERO;
			
			if(tarifaVigencia.getIcmsSubstituidoMetroCubico() != null) {
				icmsSubstituidoMetroCubico = tarifaVigencia.getIcmsSubstituidoMetroCubico();
			}
			
			if(valorLiquidoComImpostoFixo.compareTo(BigDecimal.ZERO) != 0) {
				BigDecimal qtdM3 = tarifaVigenciaFaixa.getMedidaFim().subtract(tarifaVigenciaFaixa.getMedidaInicio());
				BigDecimal valorIcmsM3 = tarifaVigencia.getIcmsSubstituidoMetroCubico().multiply(qtdM3);
				valorLiquidoComImpostoFixo = valorLiquidoComImpostoFixo.add(valorIcmsM3);
			}
			
			if(valorLiquidoComImpostoVariavel.compareTo(BigDecimal.ZERO) != 0) {
				valorLiquidoComImpostoVariavel = valorLiquidoComImpostoVariavel.add(icmsSubstituidoMetroCubico);
			}
			
			faixa.setValorLiquidoComImpostoFixo(valorLiquidoComImpostoFixo);
			faixa.setValorLiquidoComImpostoVariavel(valorLiquidoComImpostoVariavel);
		}

		if (faixa.getValorLiquidoSemImpostoFixo() != null) {
			faixa.setValorTotalSemImposto(faixa.getValorLiquidoSemImpostoFixo().add(faixa.getValorLiquidoSemImpostoVariavel()));
		}

		if ((faixa.getValorLiquidoComImpostoFixo() != null) && (faixa.getValorLiquidoComImpostoVariavel() != null)) {
			faixa.setValorTotalComImposto(faixa.getValorLiquidoComImpostoFixo().add(faixa.getValorLiquidoComImpostoVariavel()));
		}

		return faixa;
	}

	public Class<?> getClasseEntidadeTributoAliquota() {

		return ServiceLocator.getInstancia().getClassPorID(TributoAliquota.BEAN_ID_TRIBUTO_ALIQUOTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listaIndiceFinanceiro()
	 */
	@Override
	public Collection<IndiceFinanceiro> listaIndiceFinanceiro() throws NegocioException {

		Criteria criteria = this.createCriteria(getClasseEntidadeIndiceFinanceiro());
		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#montarDadosTarifa(java.lang.Long)
	 */
	@Override
	public DadosTarifa montarDadosTarifa(Long idTarifa) throws GGASException {

		DadosTarifa dadosTarifa = this.criarDadosTarifa();

		Tarifa tarifa = (Tarifa) super.obter(idTarifa, "segmento", "itemFatura", "ramoAtividade");
		dadosTarifa.setTarifa(tarifa);

		Map<String, Object> tarifas = this.obterTarifasPelaVigencia(tarifa.getChavePrimaria());

		TarifaVigencia tarifaVigente = (TarifaVigencia) tarifas.get("tarifaVigente");
		TarifaVigencia tarifaPosterior = (TarifaVigencia) tarifas.get("tarifaPosterior");

		dadosTarifa.setTarifaVigencia(tarifaVigente);

		List<DadosFaixasTarifa> listaDadosFaixasTarifa = this.popularDadosTarifaVigenciaFaixa(tarifaVigente);
		dadosTarifa.getFaixasTarifa().addAll(listaDadosFaixasTarifa);

		Date dataInicioPeriodo = tarifaVigente.getDataVigencia();

		Date dataFimPeriodo = null;
		if (tarifaPosterior != null) {
			dataFimPeriodo = tarifaPosterior.getDataVigencia();
		}

		List<TarifaVigenciaDesconto> listaTarifaVigenciaDesconto = this.consultarTarifasVigenciaDescontoPorTarifaPeriodo(
						tarifa.getChavePrimaria(), dataInicioPeriodo, dataFimPeriodo);

		if (listaTarifaVigenciaDesconto != null && !listaTarifaVigenciaDesconto.isEmpty()) {
			dadosTarifa.getListaTarifaVigenciaDesconto().addAll(listaTarifaVigenciaDesconto);
			for (TarifaVigenciaDesconto tvd : listaTarifaVigenciaDesconto) {
				if (tvd.getTarifaVigencia().getChavePrimaria() == tarifaVigente.getChavePrimaria()) {
					dadosTarifa.setTarifaVigenciaDesconto(tvd);
					List<DadosFaixasTarifa> listaDadosFaixasDescontoTarifa = this.popularDadosTarifaFaixaDesconto(listaDadosFaixasTarifa,
									tvd);
					dadosTarifa.getFaixasDesconto().addAll(listaDadosFaixasDescontoTarifa);
					break;
				}
			}
		}

		return dadosTarifa;
	}

	/**
	 * Consultar vigencia tarifa maior.
	 *
	 * @param idTarifa
	 *            the id tarifa
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<TarifaVigencia> consultarVigenciaTarifaMaior(Long idTarifa) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName()).append(" tv ");
		hql.append(" where ");
		hql.append(" tv.tarifa.chavePrimaria = :ID_TARIFA ");
		hql.append(" order by tv.dataVigencia DESC");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("ID_TARIFA", idTarifa);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.tarifa.
	 * ControladorTarifa#
	 * validarNovaVigenciaDesconto
	 * (br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaDesconto,
	 * java.lang.Long)
	 */
	@Override
	public void validarNovaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto, Long idDescontoCadastrado)
					throws NegocioException {

		if (tarifaVigenciaDesconto.getInicioVigencia() != null && (idDescontoCadastrado == null || idDescontoCadastrado <= 0)) {
			throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_VIGENCIA_DESCONTO_NAO_CADASTRADA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorTarifa
	 * #validarSelecaoVigencia(java.lang.Long)
	 */
	@Override
	public void validarSelecaoVigencia(Long idVigencia) throws NegocioException {

		if ((idVigencia == null) || (idVigencia <= 0)) {
			throw new NegocioException(ERRO_NEGOCIO_SELECAO_VIGENCIA_CLONAR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigenciaDescontoPorTarifaVigencia(long)
	 */
	@Override
	public Collection<TarifaVigenciaDesconto> obterTarifaVigenciaDescontoPorTarifaVigencia(long idTarifaVigencia) throws NegocioException {

		return obterTarifaVigenciaDescontoPorTarifaVigencia(idTarifaVigencia, false);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#obterTarifaVigenciaDescontoPorTarifaVigencia(long, boolean)
	 */
	@Override
	public Collection<TarifaVigenciaDesconto> obterTarifaVigenciaDescontoPorTarifaVigencia(long idTarifaVigencia,
					boolean ordemTarifaVigenciaDescontoInicioVigenciaAsc) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append("select distinct tarifaVigenciaDesconto");
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaFaixaDesconto().getSimpleName());
		hql.append(" tarifaFaixaDesconto");
		hql.append(" inner join tarifaFaixaDesconto.tarifaVigenciaDesconto tarifaVigenciaDesconto");
		hql.append(" where ");
		hql.append(" tarifaFaixaDesconto.tarifaVigenciaFaixa.tarifaVigencia.chavePrimaria = :idTarifaVigencia");
		hql.append(" and tarifaVigenciaDesconto.habilitado = true ");
		if (ordemTarifaVigenciaDescontoInicioVigenciaAsc) {
			hql.append(" order by tarifaVigenciaDesconto.inicioVigencia asc");
		} else {
			hql.append(" order by tarifaVigenciaDesconto.inicioVigencia desc");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idTarifaVigencia", idTarifaVigencia);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#verificaSeTarifaEstaSendoUtilizada(java.lang.Long)
	 */
	@Override
	public boolean verificaSeTarifaEstaSendoUtilizada(Long idTarifa) throws NegocioException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		boolean retorno = false;

		Collection<ContratoPontoConsumoItemFaturamento> listaContratoPontoConsumoItemFaturamento = controladorContrato
						.listarContratoItemFaturamentoPorTarifas(new Long[] {idTarifa});

		if (listaContratoPontoConsumoItemFaturamento != null && !listaContratoPontoConsumoItemFaturamento.isEmpty()) {
			retorno = true;
		}

		Collection<FaturaItem> listaFaturaItem = null;
		if (!retorno) {
			ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			listaFaturaItem = controladorFatura.listarFaturaItemPorTarifa(idTarifa);
			if (listaFaturaItem != null && !listaFaturaItem.isEmpty()) {
				retorno = true;
			}
		}

		Collection<TarifaPontoConsumo> listaTarifaPontoConsumo = null;
		if (!retorno) {
			listaTarifaPontoConsumo = this.listarTarifaPontoConsumoPorTarifa(idTarifa);
			if (listaTarifaPontoConsumo != null && !listaTarifaPontoConsumo.isEmpty()) {
				retorno = true;
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifaPontoConsumoPorTarifa(java.lang.Long)
	 */
	@Override
	public Collection<TarifaPontoConsumo> listarTarifaPontoConsumoPorTarifa(Long idTarifa) {

		Collection<TarifaPontoConsumo> retorno = null;

		if (idTarifa != null) {
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidadeTarifaPontoConsumo().getSimpleName());
			hql.append(" tpc ");
			hql.append(" where ");
			hql.append(" tpc.tarifa.chavePrimaria = :idTarifa ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("idTarifa", idTarifa);

			retorno = query.list();
		}
		if (retorno == null) {
			retorno = new ArrayList<TarifaPontoConsumo>();
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifaVO()
	 */
	@Override
	public Collection<TarifaVO> listarTarifaVO() throws NegocioException {

		Collection<TarifaVO> collVO = new ArrayList<TarifaVO>();

		Criteria criteria = this.createCriteria(Tarifa.class);
		criteria.createAlias("segmento", "segmento", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));

		Collection<Tarifa> tarifas = criteria.list();

		for (Tarifa tarifa : tarifas) {

			criteria = this.createCriteria(TarifaVigencia.class);
			criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, tarifa.getChavePrimaria()));
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
			criteria.addOrder(Order.asc(DATA_VIGENCIA));

			Object[] objs = criteria.list().toArray();

			for (int i = 0; i < objs.length; i++) {

				int posicaoDataInicial = i;
				int posicaoDataFinal = i + 1;

				TarifaVO tarifaVO = new TarifaVO();
				tarifaVO.setTarifa(tarifa);
				tarifaVO.setDataInicioVigencia(((TarifaVigencia) objs[posicaoDataInicial]).getDataVigencia());

				if (posicaoDataFinal < objs.length) {
					tarifaVO.setDataFinalVigencia(((TarifaVigencia) objs[posicaoDataFinal]).getDataVigencia());
				}

				collVO.add(tarifaVO);
			}

		}

		return collVO;
	}

	/**
	 * Listar tarifa vigencia desconto vo.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<RelatorioTarifaVO> listarTarifaVigenciaDescontoVO(FiltroTarifaVigenciaDescontos filtro) throws NegocioException {

		Collection<RelatorioTarifaVO> collVO = new ArrayList<RelatorioTarifaVO>();

		Tarifa tarifa = (Tarifa) obter(filtro.getChavePrimariaTarifa(), "segmento", "ramoAtividade", "tipoContrato");

		Collection<RelatorioTarifaVigenciaVO> tarifaVigenciaVOs = new ArrayList<RelatorioTarifaVigenciaVO>();
		TarifaVigencia tarifaVigencia = obterTarifaVigencia(filtro.getChavePrimariaTarifaVigencia(), "tarifaVigenciaFaixas", "tipoCalculo",
						"unidadeMonetaria", "status", "tributos");
		Collection<RelatorioTarifaVigenciaDescontoVO> tarifaVigenciaDescontos = new ArrayList<RelatorioTarifaVigenciaDescontoVO>();

		String tipoDesconto = filtro.getTipoDesconto();
		if (tipoDesconto.contains("%")) {
			tipoDesconto = "(%)";
		} else if (tipoDesconto.contains("$")) {
			tipoDesconto = "(R$)";
		}

		if (filtro.getChavePrimariaTarifaVigenciaDesconto() != null) {
			TarifaVigenciaDesconto tarifaVigenciaDesconto = obterTarifaVigenciaDesconto(filtro.getChavePrimariaTarifaVigenciaDesconto());

			Collection<TarifaFaixaDesconto> tarifaFaixaDescontos = listarTarifasFaixaDesconto(filtro
							.getChavePrimariaTarifaVigenciaDesconto());

			RelatorioTarifaVigenciaDescontoVO relatorioTarifaVigenciaDescontoVO = RelatorioTarifaVigenciaDescontoVO
							.montarRelatorioTarifaVigenciaDescontoVO(tarifaVigenciaDesconto, tarifaFaixaDescontos, tipoDesconto);

			tarifaVigenciaDescontos.add(relatorioTarifaVigenciaDescontoVO);
		} else {
			Collection<TarifaVigenciaDesconto> tarifaVigenciaDescontosPorTarifaVigencia = obterTarifaVigenciaDescontoPorTarifaVigencia(
							filtro.getChavePrimariaTarifaVigencia(), true);

			for (TarifaVigenciaDesconto tarifaVigenciaDesconto : tarifaVigenciaDescontosPorTarifaVigencia) {

				Collection<TarifaFaixaDesconto> tarifaFaixaDescontos = listarTarifasFaixaDesconto(tarifaVigenciaDesconto.getChavePrimaria());

				RelatorioTarifaVigenciaDescontoVO relatorioTarifaVigenciaDescontoVO = RelatorioTarifaVigenciaDescontoVO
								.montarRelatorioTarifaVigenciaDescontoVO(tarifaVigenciaDesconto, tarifaFaixaDescontos, tipoDesconto);

				tarifaVigenciaDescontos.add(relatorioTarifaVigenciaDescontoVO);
			}

		}

		tarifaVigenciaVOs.add(RelatorioTarifaVigenciaVO.montarRelatorioTarifaVigenciaVO(tarifaVigencia, tarifaVigenciaDescontos));

		RelatorioTarifaVO tarifaVO = new RelatorioTarifaVO();
		tarifaVO.setTarifa(tarifa);
		tarifaVO.setTarifaVigencias(tarifaVigenciaVOs);
		collVO.add(tarifaVO);

		return collVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#exportarRelatorioTarifasCadastradas(br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] exportarRelatorioTarifasCadastradas(FormatoImpressao formatoImpressao) throws NegocioException {

		Map<String, Object> parametros = new HashMap<String, Object>();
		if (this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		Collection<TarifaVO> collTarifaVO = this.listarTarifaVO();

		return RelatorioUtil.gerarRelatorio(collTarifaVO, parametros, RELATORIO_TARIFAS_CADASTRADAS, formatoImpressao);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#exportarRelatorioTarifa(br.com.ggas.faturamento.tarifa.impl.
	 * FiltroTarifaVigenciaDescontos)
	 */
	@Override
	public byte[] exportarRelatorioTarifa(FiltroTarifaVigenciaDescontos filtro) throws NegocioException {

		Map<String, Object> parametros = new HashMap<String, Object>();

		if (this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		Collection<RelatorioTarifaVO> collTarifaVO = this.listarTarifaVigenciaDescontoVO(filtro);

		return RelatorioUtil.gerarRelatorio(collTarifaVO, parametros, RELATORIO_TARIFA, filtro.getFormatoImpressao());
	}

	/**
	 * Obter empresa principal.
	 *
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifaVigenciaVO(java.util.Map)
	 */
	@Override
	public Collection<TarifaVO> listarTarifaVigenciaVO(Map<String, Object> filtro) throws NegocioException {

		this.validarDadosGerarRelatorioTarifasCadastradas(filtro);

		Collection<TarifaVO> collVO = new ArrayList<TarifaVO>();

		Long idSegmento = (Long) filtro.get("idSegmento");
		Date dataInicio = (Date) filtro.get("dataInicialTarifas");
		Date dataFim = (Date) filtro.get("dataFinalTarifas");

		Calendar calDataInicio = Calendar.getInstance();
		calDataInicio.setTime(dataInicio);
		calDataInicio.set(Calendar.HOUR, 0);
		calDataInicio.set(Calendar.MINUTE, 0);
		calDataInicio.set(Calendar.SECOND, 0);

		Calendar calDataFim = Calendar.getInstance();
		calDataFim.setTime(dataFim);
		calDataFim.set(Calendar.HOUR, CONSTANTE_NUMERO_VINTE_TRES);
		calDataFim.set(Calendar.MINUTE, CONSTANTE_NUMERO_CINQUENTA_NOVE);
		calDataFim.set(Calendar.SECOND, CONSTANTE_NUMERO_CINQUENTA_NOVE);

		Criteria criteria = this.createCriteria(Tarifa.class);
		criteria.createAlias("segmento", "segmento", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));

		if (idSegmento != null) {

			criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));

		}

		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));

		Collection<Tarifa> tarifas = criteria.list();

		for (Tarifa tarifa : tarifas) {

			criteria = this.createCriteria(TarifaVigencia.class);
			criteria.add(Restrictions.eq(TARIFA_CHAVE_PRIMARIA, tarifa.getChavePrimaria()));
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
			criteria.add(Restrictions.between(DATA_VIGENCIA, calDataInicio.getTime(), calDataFim.getTime()));
			criteria.addOrder(Order.asc(DATA_VIGENCIA));

			Object[] objs = criteria.list().toArray();

			for (int i = 0; i < objs.length; i++) {

				int posicaoDataInicial = i;
				int posicaoDataFinal = i + 1;

				TarifaVO tarifaVO = new TarifaVO();
				tarifaVO.setTarifa(tarifa);
				tarifaVO.setDataInicioVigencia(((TarifaVigencia) objs[posicaoDataInicial]).getDataVigencia());

				if (posicaoDataFinal < objs.length) {
					tarifaVO.setDataFinalVigencia(((TarifaVigencia) objs[posicaoDataFinal]).getDataVigencia());
				}

				collVO.add(tarifaVO);
			}

		}

		if (collVO.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return collVO;
	}

	/**
	 * Validar dados gerar relatorio tarifas cadastradas.
	 *
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioTarifasCadastradas(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("dataInicialTarifas").toString().isEmpty()) {
			throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DATA_INICIAL_TARIFA_CAD, parametros.get("dataInicialTarifas")
							.toString());
		}

		if (parametros.get("dataFinalTarifas").toString().isEmpty()) {
			throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DATA_FINAL_TARIFA_CAD, parametros.get("dataFinalTarifas").toString());
		}

		Date dataInicial = (Date) parametros.get("dataInicialTarifas");
		Date dataFinal = (Date) parametros.get("dataFinalTarifas");

		if (Util.compararDatas(dataInicial, dataFinal) > 0) {
			throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DATA_INICIAL_MAIOR_TARIFA_CAD, dataFinal);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#verificarListaTarifaPendente(java.util.Collection, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public Collection<Tarifa> verificarListaTarifaPendente(Collection<Tarifa> listaTarifas, Date dataInicioPeriodo, Date dataFimPeriodo)
			throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<Tarifa> listarTarifasAux = new ArrayList<Tarifa>();
		Map<String, Object> filtro = new HashMap<String, Object>();
		String paramStatusAutorizada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		for (Tarifa tarifa : listaTarifas) {
			filtro.put("idTarifa", tarifa.getChavePrimaria());
			filtro.put("status", Long.parseLong(paramStatusAutorizada));
			filtro.put(DATA_INICIO, dataInicioPeriodo);
			filtro.put(DATA_FIM, dataFimPeriodo);
			Long countTarifasVigencia = contarTarifasVigencia(filtro);
			if (countTarifasVigencia != null && countTarifasVigencia > 0) {
				listarTarifasAux.add(tarifa);
			}
		}

		return listarTarifasAux;
	}
	
	@Override
	@Cacheable("listaChavesTarifa")
	public List<Long> obterListaIdsTarifas(Long status, Date dataInicioPeriodo, Date dataFimPeriodo) {

		Map<String, Object> filtro = new HashMap<String, Object>();
		
		filtro.put("status", status);
		
		if(dataInicioPeriodo != null) {
			filtro.put(DATA_INICIO, dataInicioPeriodo);
		}
		
		if(dataFimPeriodo != null) {
			filtro.put(DATA_FIM, dataFimPeriodo);
		}
		
		
		return obterIdsTarifaPorTarifaVigencia(filtro);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#calcularJurosEntreDatasIndiceFinanceiro(java.util.Date, java.util.Date,
	 * java.lang.Long, java.math.BigDecimal)
	 */
	@Override
	public BigDecimal calcularJurosEntreDatasIndiceFinanceiro(Date dataInicio, Date dataFim, Long idIndice, BigDecimal valor)
					throws NegocioException {

		IndiceFinanceiro indiceFinanceiro = this.obterIndiceFinanceiro(idIndice);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataInicio);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		Collection<IndiceFinanceiroValorNominal> listaIndiceValorNominal = null;
		if (indiceFinanceiro.isIndicadorMensal()) {
			listaIndiceValorNominal = this.listarIndiceFinanceiroEntreData(calendar.getTime(), dataFim, idIndice);
		} else {
			listaIndiceValorNominal = this.listarIndiceFinanceiroEntreData(dataInicio, dataFim, idIndice);
		}

		Calendar calendarInicio = Calendar.getInstance();
		calendarInicio.setTime(dataInicio);
		Calendar calendarFim = Calendar.getInstance();
		calendarFim.setTime(dataFim);
		BigDecimal valorCalculado = valor;
		BigDecimal valorTempoTaxa = BigDecimal.ONE;
		BigDecimal valorTempoTaxaSemPercentual = BigDecimal.ZERO;

		// valorFinal = valorInicial * (1 + i) ^ n
		for (IndiceFinanceiroValorNominal indiceFinanceiroValorNominal : listaIndiceValorNominal) {

			Date dataValorNominal = indiceFinanceiroValorNominal.getDataReferencia();
			BigDecimal valorNominal = indiceFinanceiroValorNominal.getValorNominal();

			Calendar calendarValorNominal = Calendar.getInstance();
			calendarValorNominal.setTime(dataValorNominal);

			int intervaloDias = 1;
			if (indiceFinanceiro.isIndicadorMensal()) {

				boolean isMesNormal = false;
				if (calendarInicio.get(Calendar.MONTH) == calendarValorNominal.get(Calendar.MONTH)) {
					int ultimoDiaMes = calendarValorNominal.getActualMaximum(Calendar.DAY_OF_MONTH);
					intervaloDias = ultimoDiaMes - calendarInicio.get(Calendar.DAY_OF_MONTH);
				} else if (calendarFim.get(Calendar.MONTH) == calendarValorNominal.get(Calendar.MONTH)) {
					intervaloDias = calendarFim.get(Calendar.DAY_OF_MONTH);
				} else {
					isMesNormal = true;
				}

				if (!isMesNormal) {
					if (indiceFinanceiro.isIndicadorValorPercentual()) {
						// realiza o cálculo da equivalência de taxas, que determina a taxa para N dias equivalênte a i a.m.
						// ((1 + i) ^ n/30) - 1
						BigDecimal expoente = new BigDecimal(intervaloDias).divide(new BigDecimal(CONSTANTE_NUMERO_TRINTA), 6, RoundingMode.HALF_UP);
						valorNominal = BigDecimal.valueOf(Math.pow(valorNominal.add(BigDecimal.ONE).doubleValue(), expoente.doubleValue()))
										.subtract(BigDecimal.ONE);
					} else {
						valorNominal = valorNominal.divide(new BigDecimal(CONSTANTE_NUMERO_TRINTA), ESCALA, RoundingMode.HALF_UP).multiply(
										new BigDecimal(intervaloDias));
					}
				}
			}

			if (indiceFinanceiro.isIndicadorValorPercentual()) {
				valorNominal = valorNominal.add(BigDecimal.ONE);
				valorTempoTaxa = valorTempoTaxa.multiply(valorNominal);
			} else {
				valorTempoTaxaSemPercentual = valorTempoTaxaSemPercentual.add(valorNominal);
			}

		}

		if (indiceFinanceiro.isIndicadorValorPercentual()) {
			valorCalculado = valorCalculado.multiply(valorTempoTaxa).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.UP);
		} else {
			valorCalculado = valorCalculado.add(valorTempoTaxaSemPercentual).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.UP);
		}

		return valorCalculado;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#listarTarifaVigenciaPorTarifa(java.lang.Long)
	 */
	@Override
	public Collection<TarifaVigencia> listarTarifaVigenciaPorTarifa(Long idTarifa) {

		Collection<TarifaVigencia> retorno = null;

		if (idTarifa != null) {
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
			hql.append(" tavi ");
			hql.append(" inner join fetch tavi.tarifa tarifa");
			hql.append(" inner join fetch tavi.baseApuracao baseApuracao");
			hql.append(" inner join fetch tavi.tipoCalculo tipoCalculo");
			hql.append(" where ");
			hql.append(" tavi.tarifa.chavePrimaria = :idTarifa ");
			hql.append(" order by tavi.dataVigencia desc ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("idTarifa", idTarifa);

			retorno = query.list();
		}
		if (retorno == null) {
			retorno = new ArrayList<TarifaVigencia>();
		}

		return retorno;
	}

	@Override
	public List<TarifaVigenciaFaixa> listarTarifaVigenciaFaixaPorPontoConsumo(Long idPontoConsumo) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT(tvf.TAVF_CD) ,tvf.tavf_md_inicio, tvf.tavf_md_fim, tvf.tavf_vl_variavel, tvf.tavf_vl_fixo FROM TARIFA t ");
		sql.append("INNER JOIN (select * from TARIFA_VIGENCIA t1 where tavi_dt_vigencia >= all( ");
		sql.append("select tavi_dt_vigencia from TARIFA_VIGENCIA t2 where t1.TAVI_CD <> t2.TAVI_CD and t1.TARI_CD = t2.TARI_CD) ");
		sql.append(" ) tv ON t.TARI_CD = tv.TARI_CD ");
		sql.append(" INNER JOIN TARIFA_VIGENCIA_FAIXA tvf ON tvf.TAVI_CD = tv.TAVI_CD ");
		sql.append(" INNER JOIN CONTRATO_PONTO_CONS_ITEM_FATUR cif ON cif.TARI_CD = t.TARI_CD ");
		sql.append(" INNER JOIN CONTRATO_PONTO_CONSUMO cpc ON cpc.COPC_CD = cif.COPC_CD ");
		sql.append(" WHERE cpc.POCN_CD = :pontoConsumo AND t.tari_in_uso = 1 ");
		sql.append("order by tavf_md_inicio");

		SQLQuery query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString());
		query.setParameter("pontoConsumo", idPontoConsumo);
		List<Objects[]> results = query.list();

		List<TarifaVigenciaFaixa> faixas = new ArrayList<>();
		for (Object[] row: results) {
			final TarifaVigenciaFaixa faixa = new TarifaVigenciaFaixaImpl();
			Long id = Long.valueOf(row[0].toString());
			faixa.setChavePrimaria(id);
			faixa.setMedidaInicio((BigDecimal) row[1]);
			faixa.setMedidaFim((BigDecimal) row[CONSTANTE_NUMERO_DOIS]);
			faixa.setValorVariavel((BigDecimal) row[3]);
			faixa.setValorFixo((BigDecimal) row[4]);
			faixas.add(faixa);
		}
		return faixas;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifasVigenciaFaixa(java.util.Map)
	 */
	@Override
	public Collection<TarifaVigenciaFaixa> consultarDadosTarifasVigenciaFaixa(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTarifaVigenciaFaixa());
		criteria.setProjection(Projections.projectionList()
				.add(Projections.distinct(Projections.property(CHAVE_PRIMARIA)), CHAVE_PRIMARIA)
				.add(Projections.property("medidaFim"), "medidaFim")
				.add(Projections.property("valorFixo"), "valorFixo"));
		
		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			TarifaVigencia tarifaVigencia = (TarifaVigencia) filtro.get("tarifaVigencia");
			if (tarifaVigencia != null) {
				criteria.add(Restrictions.eq("tarifaVigencia.chavePrimaria", tarifaVigencia.getChavePrimaria()));
			} else {
				criteria.setFetchMode("tarifaVigencia", FetchMode.JOIN);
			}

			Long chaveTarifaVigencia = (Long) filtro.get("chaveTarifaVigencia");
			if (chaveTarifaVigencia != null && chaveTarifaVigencia > 0) {
				criteria.add(Restrictions.eq("tarifaVigencia.chavePrimaria", chaveTarifaVigencia));
			}

			BigDecimal medidaInicio = (BigDecimal) filtro.get("medidaInicio");
			if (medidaInicio != null) {
				criteria.add(Restrictions.eq("medidaInicio", medidaInicio));
			}

			BigDecimal medidaFim = (BigDecimal) filtro.get("medidaFim");
			if (medidaFim != null) {
				criteria.add(Restrictions.eq("medidaFim", medidaFim));
			}

			BigDecimal valorFixo = (BigDecimal) filtro.get("valorFixo");
			if (valorFixo != null) {
				criteria.add(Restrictions.eq("valorFixo", valorFixo));
			}

			BigDecimal valorVariavel = (BigDecimal) filtro.get("valorVariavel");
			if (valorVariavel != null) {
				criteria.add(Restrictions.eq("valorVariavel", valorVariavel));
			}

			String ordenadoPor = (String) filtro.get("ordenadoPor");
			if (ordenadoPor != null) {
				criteria.addOrder(Order.asc(ordenadoPor));
			}

		}

		criteria.setResultTransformer(
				new GGASTransformer(getClasseEntidadeTarifaVigenciaFaixa(), getSessionFactory().getAllClassMetadata()));
		
		return criteria.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorTarifa#consultarTarifaVigenciaAnteriorMaisRecente(long, java.util.Date)
	 */
	@Override
	public TarifaVigencia consultarDadosTarifaVigenciaAnteriorMaisRecente(long chaveTarifa, Date dataVigencia) {

		Date dataVigenciaParametro = dataVigencia;
		StringBuilder hql = new StringBuilder();

		hql.append(" select  ");
		hql.append(" tv.informarMsgIcmsSubstituido as informarMsgIcmsSubstituido, ");
		hql.append(" tv.baseCalculoIcmsSubstituido as baseCalculoIcmsSubstituido, ");
		hql.append(" tv.icmsSubstituidoMetroCubico as icmsSubstituidoMetroCubico, ");
		hql.append(" tv.mensagemIcmsSubstituto as mensagemIcmsSubstituto ");
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tv ");
		hql.append(" where tv.habilitado = true ");
		hql.append(" and tv.tarifa.chavePrimaria = :chaveTarifa ");
		hql.append(" and tv.dataVigencia = ( ");
		hql.append(" select ");
		hql.append(" max(tavi.dataVigencia) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeTarifaVigencia().getSimpleName());
		hql.append(" tavi ");
		hql.append(" where tavi.habilitado = true ");
		hql.append(" and tavi.tarifa.chavePrimaria = :chaveTarifa");
		hql.append(" and tavi.dataVigencia < :dataVigencia");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeTarifaVigencia(), super.getSessionFactory().getAllClassMetadata()));
		query.setParameter("chaveTarifa", chaveTarifa);
		if (dataVigenciaParametro == null) {
			dataVigenciaParametro = Calendar.getInstance().getTime();
		}

		query.setParameter(DATA_VIGENCIA, dataVigenciaParametro);

		return (TarifaVigencia) query.uniqueResult();
	}
}
