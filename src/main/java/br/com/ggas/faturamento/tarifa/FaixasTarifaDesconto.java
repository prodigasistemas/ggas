/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * Faixas Tarifa Desconto
 *
 */
public interface FaixasTarifaDesconto extends Serializable {

	String BEAN_ID_FAIXAS_TARIFA_DESCONTO = "faixasTarifaDesconto";

	/**
	 * @return the tipo
	 */
	boolean isValor();

	/**
	 * @return the qtdeCasasDecimais
	 */
	Integer getQtdeCasasDecimais();

	/**
	 * @param qtdeCasasDecimais
	 *            the qtdeCasasDecimais to set
	 */
	void setQtdeCasasDecimais(Integer qtdeCasasDecimais);

	/**
	 * @return the medidaInicio
	 */
	BigDecimal getMedidaInicio();

	/**
	 * @param medidaInicio
	 *            the medidaInicio to set
	 */
	void setMedidaInicio(BigDecimal medidaInicio);

	/**
	 * @return the medidaFim
	 */
	BigDecimal getMedidaFim();

	/**
	 * @param medidaFim
	 *            the medidaFim to set
	 */
	void setMedidaFim(BigDecimal medidaFim);

	/**
	 * @return the valorFixo
	 */
	BigDecimal getValorFixo();

	/**
	 * @param valorFixo
	 *            the valorFixo to set
	 */
	void setValorFixo(BigDecimal valorFixo);

	/**
	 * @return the valorDescontoFixo
	 */
	BigDecimal getValorDescontoFixo();

	/**
	 * @param valorDescontoFixo
	 *            the valorDescontoFixo to set
	 */
	void setValorDescontoFixo(BigDecimal valorDescontoFixo);

	/**
	 * @return the valorLiquidoSemImpostoFixo
	 */
	BigDecimal getValorLiquidoSemImpostoFixo();

	/**
	 * @param valorLiquidoSemImpostoFixo
	 *            the valorLiquidoSemImpostoFixo
	 *            to set
	 */
	void setValorLiquidoSemImpostoFixo(BigDecimal valorLiquidoSemImpostoFixo);

	/**
	 * @return the valorLiquidoComImpostoFixo
	 */
	BigDecimal getValorLiquidoComImpostoFixo();

	/**
	 * @param valorLiquidoComImpostoFixo
	 *            the valorLiquidoComImpostoFixo
	 *            to set
	 */
	void setValorLiquidoComImpostoFixo(BigDecimal valorLiquidoComImpostoFixo);

	/**
	 * @return the valorCompra
	 */
	BigDecimal getValorCompra();

	/**
	 * @param valorCompra
	 *            the valorCompra to set
	 */
	void setValorCompra(BigDecimal valorCompra);

	/**
	 * @return the margemValorAgregado
	 */
	BigDecimal getMargemValorAgregado();

	/**
	 * @param margemValorAgregado
	 *            the margemValorAgregado to set
	 */
	void setMargemValorAgregado(BigDecimal margemValorAgregado);

	/**
	 * @return the valorVariavel
	 */
	BigDecimal getValorVariavel();

	/**
	 * @param valorVariavel
	 *            the valorVariavel to set
	 */
	void setValorVariavel(BigDecimal valorVariavel);

	/**
	 * @return the valorDescontoVariavel
	 */
	BigDecimal getValorDescontoVariavel();

	/**
	 * @param valorDescontoVariavel
	 *            the valorDescontoVariavel to set
	 */
	void setValorDescontoVariavel(BigDecimal valorDescontoVariavel);

	/**
	 * @return the valorLiquidoSemImpostoVariavel
	 */
	BigDecimal getValorLiquidoSemImpostoVariavel();

	/**
	 * @param valorLiquidoSemImpostoVariavel
	 *            the
	 *            valorLiquidoSemImpostoVariavel
	 *            to set
	 */
	void setValorLiquidoSemImpostoVariavel(BigDecimal valorLiquidoSemImpostoVariavel);

	/**
	 * @return the valorLiquidoComImpostoVariavel
	 */
	BigDecimal getValorLiquidoComImpostoVariavel();

	/**
	 * @param valorLiquidoComImpostoVariavel
	 *            the
	 *            valorLiquidoComImpostoVariavel
	 *            to set
	 */
	void setValorLiquidoComImpostoVariavel(BigDecimal valorLiquidoComImpostoVariavel);

	/**
	 * @return the valorTotalSemImposto
	 */
	BigDecimal getValorTotalSemImposto();

	/**
	 * @param valorTotalSemImposto
	 *            the valorTotalSemImposto to set
	 */
	void setValorTotalSemImposto(BigDecimal valorTotalSemImposto);

	/**
	 * @return the valorTotalComImposto
	 */
	BigDecimal getValorTotalComImposto();

	/**
	 * @param valorTotalComImposto
	 *            the valorTotalComImposto to set
	 */
	void setValorTotalComImposto(BigDecimal valorTotalComImposto);

}
