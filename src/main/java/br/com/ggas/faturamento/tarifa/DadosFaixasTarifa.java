/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.io.Serializable;
/**
 * 
 * Dados Faixas Tarifa
 *
 */
public interface DadosFaixasTarifa extends Serializable {

	String BEAN_ID_DADOS_FAIXAS_TARIFAS = "dadosFaixasTarifa";

	String ERRO_FAIXAS_INVALIDAS = "ERRO_FAIXAS_INVALIDAS";

	/**
	 * @return the idTarifaVigenciaFaixa
	 */
	Long getIdTarifaVigenciaFaixa();

	/**
	 * @param idTarifaVigenciaFaixa
	 *            the idTarifaVigenciaFaixa to set
	 */
	void setIdTarifaVigenciaFaixa(Long idTarifaVigenciaFaixa);

	/**
	 * @return the tipoDesconto
	 */
	Long getTipoDesconto();

	/**
	 * @param tipoDesconto
	 *            the tipoDesconto to set
	 */
	void setTipoDesconto(Long tipoDesconto);

	/**
	 * @return the numeroColunaFinal
	 */
	Integer getNumeroColunaFinal();

	/**
	 * @param numeroColunaFinal
	 *            the numeroColunaFinal to set
	 */
	void setNumeroColunaFinal(Integer numeroColunaFinal);

	/**
	 * @return the descontoFixoSemImposto
	 */
	String getDescontoFixoSemImposto();

	/**
	 * @param descontoFixoSemImposto
	 *            the descontoFixoSemImposto to
	 *            set
	 */
	void setDescontoFixoSemImposto(String descontoFixoSemImposto);

	/**
	 * @return the descontoVariavelSemImposto
	 */
	String getDescontoVariavelSemImposto();

	/**
	 * @param descontoVariavelSemImposto
	 *            the descontoVariavelSemImposto
	 *            to set
	 */
	void setDescontoVariavelSemImposto(String descontoVariavelSemImposto);

	/**
	 * @return the faixaInicial
	 */
	String getFaixaInicial();

	/**
	 * @param faixaInicial
	 *            the faixaInicial to set
	 */
	void setFaixaInicial(String faixaInicial);

	/**
	 * @return the faixaFinal
	 */
	String getFaixaFinal();

	/**
	 * @param faixaFinal
	 *            the faixaFinal to set
	 */
	void setFaixaFinal(String faixaFinal);

	/**
	 * @return the valorFixoSemImposto
	 */
	String getValorFixoSemImposto();

	/**
	 * @param valorFixoSemImposto
	 *            the valorFixoSemImposto to set
	 */
	void setValorFixoSemImposto(String valorFixoSemImposto);

	/**
	 * @return the valorCompra
	 */
	String getValorCompra();

	/**
	 * @param valorCompra
	 *            the valorCompra to set
	 */
	void setValorCompra(String valorCompra);

	/**
	 * @return the margemValorAgregado
	 */
	String getMargemValorAgregado();

	/**
	 * @param margemValorAgregado
	 *            the margemValorAgregado to set
	 */
	void setMargemValorAgregado(String margemValorAgregado);

	/**
	 * @return the valorVariavelSemImposto
	 */
	String getValorVariavelSemImposto();

	/**
	 * @param valorVariavelSemImposto
	 *            the valorVariavelSemImposto to
	 *            set
	 */
	void setValorVariavelSemImposto(String valorVariavelSemImposto);

	/**
	 * @return
	 */
	Boolean getUltimaFaixa();

	/**
	 * @param ultimaFaixa
	 */
	void setUltimaFaixa(Boolean ultimaFaixa);

	/**
	 * @return
	 */
	Boolean getValorFixoSemImpostoPreenchido();

	/**
	 * @return
	 */
	Boolean getValorVariavelSemImpostoPreenchido();

}
