/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorCobrancaImpl representa uma ControladorCobrancaImpl no sistema.
 *
 * 
 * @since 22/02/2010
 *
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.tarifa.ControladorPrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 *
 */
class ControladorPrecoGasImpl extends ControladorNegocioImpl implements ControladorPrecoGas {

	private static final int LIMITE_TAMANHO_OBSERVACAO = 200;

	private static final String TIPO_ARREDONDAMENTO_CONSUMO = "TIPO_ARREDONDAMENTO_CONSUMO";

	private static final String TIPO_ARREDONDAMENTO_CALCULO = "TIPO_ARREDONDAMENTO_CALCULO";

	private static final String ERRO_PRECO_GAS_CONTRATO_COMPRA = "ERRO_PRECO_GAS_CONTRATO_COMPRA";

	private static final String ERRO_NEGOCIO_ESCALA_NAO_INFORMADA = "ERRO_NEGOCIO_ESCALA_NAO_INFORMADA";

	private static final String QUANTIDADE_ESCALA_CALCULO = "QUANTIDADE_ESCALA_CALCULO";
	
	private static final Logger LOG = Logger.getLogger(ControladorPrecoGasImpl.class);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorPrecoGas#listarPrecoGas()
	 */
	@Override
	public Collection<PrecoGas> listarPrecoGas() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<PrecoGas>) query.list();
	}

	/**
	 * Método responsável por Validar as
	 * informações do Itém do Preço do Gas.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @param precosGasItem
	 *            the precos gas item
	 * @param precoGasItem
	 *            the preco gas item
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void validarPrecoGasItem(PrecoGas precoGas, Collection<PrecoGasItem> precosGasItem, PrecoGasItem precoGasItem)
					throws NegocioException {

		if(precoGasItem != null) {
			Map<String, Object> validarPrecoGasItem = precoGasItem.validarDados();
			if(validarPrecoGasItem != null && !validarPrecoGasItem.isEmpty()) {
				throw new NegocioException(validarPrecoGasItem);
			}
		}
		// se preço gás for nulo ou volume for zero
		if((precoGas == null || precoGasItem == null) || (precoGasItem.getVolume().compareTo(BigDecimal.ZERO)) <= 0) {
			throw new NegocioException(ERRO_PRECO_GAS_ITEM_VOLUME, true);
		}

		if(precoGas.getIndicadorComplementar().equals(false)) {
			if((precoGasItem.getValor().compareTo(BigDecimal.ZERO)) <= 0) {
				throw new NegocioException(ERRO_PRECO_GAS_ITEM, true);
			}

			if(precoGas.getChavePrimaria() > 0) {
				Boolean bFlagEncontrado = false;
				Map<String, Object> filtro = new HashMap<String, Object>();

				// Valida todos os itens de fatura
				filtro.clear();
				filtro.put("idPrecoGasComplementar", precoGas.getChavePrimaria());
				Collection<PrecoGasItem> precoGasItens = consultarPrecoGasItem(filtro);

				if(precoGasItens != null && !precoGasItens.isEmpty()) {
					
					bFlagEncontrado = this.buscarPrecoGasItem(precoGasItens, precoGasItem);
					
					this.validarPrecoPaiDePrecoGas(bFlagEncontrado, precoGasItem);
					
				}
			}
		} else {
			Map<String, Object> validarPrecoGas = precoGas.validarDados();
			if(validarPrecoGas != null && !validarPrecoGas.isEmpty()) {
				throw new NegocioException(validarPrecoGas);
			}

			if(precoGas.getChavePrimaria() > 0) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("idPrecoGasComplementar", precoGas.getChavePrimaria());
				Collection<PrecoGas> precosGas = consultarPrecoGas(filtro);
				if(precosGas != null && !precosGas.isEmpty()) {
					throw new NegocioException(ERRO_PRECO_GAS_COMPLEMENTAR_SIM, true);
				}
			}

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idPrecoGas", precoGas.getPrecoGas().getChavePrimaria());
			filtro.put("idItemFatura", precoGasItem.getItemFatura().getChavePrimaria());

			Collection<PrecoGasItem> precoGasItens = consultarPrecoGasItem(filtro);
			if(precoGasItens == null || precoGasItens.isEmpty()) {
				throw new NegocioException(ERRO_PRECO_GAS_ITEM_PRECO_NAO_ENCONTRADO_COMPLEMENTAR, true);
			} else {
				
				this.validarVolumePrecoGasItem(precoGasItens, precoGasItem);
				
				if(precoGasItem.getValor().compareTo(BigDecimal.ZERO) < 0) {
					validarValorPrecoGasItem(precoGas, precoGasItem);
				}
			}
		}

		if (precoGas.getDataFornecimentoFim() != null
				&& ((precoGasItem.getDataVencimento() != null) && (precoGas.getDataEmissaoNotaFiscal() != null))
				&& (precoGasItem.getDataVencimento().before(precoGas.getDataEmissaoNotaFiscal()))) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MENOR_DATA_EMISAO, true);

		}

		if(precosGasItem != null) {
			for (PrecoGasItem precoGasItemAtual : precosGasItem) {
				if (precoGas.getDataFornecimentoFim() != null
						&& ((precoGasItemAtual.getDataVencimento() != null) && (precoGas.getDataEmissaoNotaFiscal() != null))) {

					throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MENOR_DATA_EMISAO, true);

				}

				if(precoGasItemAtual.getItemFatura().getChavePrimaria() == precoGasItem.getItemFatura().getChavePrimaria()) {
					throw new NegocioException(ERRO_PRECO_GAS_ITEM_ITEM_FATURA_ADICIONADO, true);
				}
			}
		}
	}
	
	private Boolean buscarPrecoGasItem(Collection<PrecoGasItem> precoGasItens, PrecoGasItem precoGasItem) throws NegocioException {

		Boolean bFlagEncontrado = false;
		for (PrecoGasItem precoGasItemAtual : precoGasItens) {
			if (precoGasItemAtual.getItemFatura().getChavePrimaria() == precoGasItem.getItemFatura().getChavePrimaria()) {
				bFlagEncontrado = true;
			}
			
			if (!(precoGasItemAtual.getVolume().compareTo(precoGasItem.getVolume()) == 0)
					&& precoGasItemAtual.getChavePrimaria() == precoGasItem.getChavePrimaria()) {
				throw new NegocioException(ERRO_PRECO_ITEM_GAS_VOLUME_DIFERENTE_PAI, true);
			}
		}
		return bFlagEncontrado;
	}

	private void validarPrecoPaiDePrecoGas(Boolean bFlagEncontrado, PrecoGasItem precoGasItem) throws NegocioException {
		if (!bFlagEncontrado && precoGasItem.getChavePrimaria() > 0) {
			throw new NegocioException(ERRO_PRECO_GAS_ITEM_PRECO_PAI_ENCONTRADO_COMPLEMENTAR, true);
		}
	}
	
	private void validarVolumePrecoGasItem(Collection<PrecoGasItem> precoGasItens, PrecoGasItem precoGasItem) throws NegocioException {
		for (PrecoGasItem precoGasItemAtual : precoGasItens) {
			if (!(precoGasItemAtual.getVolume().compareTo(precoGasItem.getVolume()) == 0)) {
				throw new NegocioException(ERRO_PRECO_ITEM_GAS_VOLUME_DIFERENTE, true);
			}

		}
	}
	
	/**
	 * Validar valor preco gas item.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @param precoGasItem
	 *            the preco gas item
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarValorPrecoGasItem(PrecoGas precoGas, PrecoGasItem precoGasItem) throws NegocioException {

		BigDecimal valorTotal = BigDecimal.valueOf(0);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idPrecoGas", precoGas.getPrecoGas().getChavePrimaria());
		Collection<PrecoGasItem> precoGasItens = consultarPrecoGasItem(filtro);
		for (PrecoGasItem precoGasItemAtual : precoGasItens) {
			if(precoGasItemAtual.getItemFatura().getChavePrimaria() == precoGasItem.getItemFatura().getChavePrimaria()) {
				valorTotal = valorTotal.add(precoGasItemAtual.getVolume().multiply(precoGasItemAtual.getValor()));
			}
		}

		filtro.clear();
		filtro.put("idPrecoGasComplementar", precoGas.getPrecoGas().getChavePrimaria());
		filtro.put("idPrecoGasReverso", precoGas.getChavePrimaria());
		precoGasItens = consultarPrecoGasItem(filtro);
		for (PrecoGasItem precoGasItemAtual : precoGasItens) {
			if(precoGasItemAtual.getItemFatura().getChavePrimaria() == precoGasItem.getItemFatura().getChavePrimaria()) {
				valorTotal = valorTotal.add(precoGasItemAtual.getVolume().multiply(precoGasItemAtual.getValor()));
			}
		}

		if(valorTotal.add(precoGasItem.getValor().multiply(precoGasItem.getVolume())).compareTo(BigDecimal.valueOf(0)) <= 0) {
			throw new NegocioException(ERRO_SOMATORIO_PRECO_GAS_MENOR_ZERO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorPrecoGas#validarRemoverPrecoGasItem(br.com.ggas.faturamento.tarifa.PrecoGasItem)
	 */
	@Override
	public void validarRemoverPrecoGasItem(PrecoGasItem precoGasItem) throws NegocioException {

		if(precoGasItem.getPrecoGas() != null && precoGasItem.getPrecoGas().getChavePrimaria() > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("idPrecoGasComplementar", precoGasItem.getPrecoGas().getChavePrimaria());
			Collection<PrecoGasItem> precoGasItens = consultarPrecoGasItem(filtro);

			Boolean bFlagEncontrado = false;
			for (PrecoGasItem precoGasItemAtual : precoGasItens) {
				if(precoGasItemAtual.getItemFatura().getChavePrimaria() == precoGasItem.getItemFatura().getChavePrimaria()) {
					bFlagEncontrado = true;
				}
			}

			if(bFlagEncontrado) {
				throw new NegocioException(ERRO_PRECO_GAS_ITEM_PRECO_PAI_ENCONTRADO_COMPLEMENTAR, true);
			}
		}
	}

	/**
	 * Método responsável por Validar as
	 * informações do Preço do Gas.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void validarPrecoGas(PrecoGas precoGas) throws NegocioException {

		if(precoGas != null) {
			Map<String, Object> validarPrecoGas = precoGas.validarDados();
			if(!validarPrecoGas.isEmpty()) {
				throw new NegocioException(validarPrecoGas);
			}
		} else {
			throw new NegocioException(ERRO_PRECO_GAS_ITEM, true);
		}

		if(precoGas.getDataFornecimentoInicio() != null && precoGas.getDataFornecimentoInicio().after(Calendar.getInstance().getTime())) {
			throw new NegocioException(ERRO_DATA_INICIO_FORNECIMENTO_MENOR_ATUAL, true);
		}

		if(precoGas.getDataFornecimentoFim() != null && precoGas.getDataFornecimentoFim().after(Calendar.getInstance().getTime())) {
			throw new NegocioException(ERRO_DATA_FIM_FORNECIMENTO_MENOR_ATUAL, true);
		}

		if(precoGas.getDataFornecimentoInicio() != null && precoGas.getDataFornecimentoFim() != null && 
						precoGas.getDataFornecimentoInicio().after(precoGas.getDataFornecimentoFim())) {

			throw new NegocioException(ERRO_PRECO_GAS_DATA_FORNECIMENTO_INICIO_MAIOR_FIM, true);
		}

		if(precoGas.getDataEmissaoNotaFiscal().after(Calendar.getInstance().getTime())) {
			throw new NegocioException(ERRO_DATA_EMISSAO_NOTA_FISCAL_MENOR_ATUAL, true);
		}

		if((precoGas.getDataEmissaoNotaFiscal() != null) && (precoGas.getDataFornecimentoFim() != null) && 
						(precoGas.getDataEmissaoNotaFiscal().before(precoGas.getDataFornecimentoFim()))) {

			throw new NegocioException(ERRO_NEGOCIO_DATA_EMISSAO_NOTA_FISCAL_MENOR_DATA_FIM, true);
		}

		if(precoGas.getIndicadorComplementar() && precoGas.getPrecoGas() != null) {
			PrecoGas precoGasPAI = (PrecoGas) obter(precoGas.getPrecoGas().getChavePrimaria());
			if(precoGasPAI.getIndicadorComplementar()) {
				throw new NegocioException(ERRO_PRECO_GAS_COMPLEMENTAR_PAI, true);
			}
			if(precoGas.getDataEmissaoNotaFiscal().before(precoGasPAI.getDataEmissaoNotaFiscal())) {
				throw new NegocioException(ERRO_PRECO_GAS_DATA_EMISSAO_MAIOR_IGUAL_PAI, true);
			}
		}

		if(precoGas.getNumeroNotaFiscal() != null) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			if(precoGas.getChavePrimaria() != 0) {
				filtro.put(CHAVE_PRIMARIA_INVERSA, precoGas.getChavePrimaria());
			}
			filtro.put(HABILITADO, Boolean.valueOf(TRUE));
			Collection<PrecoGas> precosGasBanco = consultarPrecoGas(filtro);
			if(!precosGasBanco.isEmpty()) {
				for (PrecoGas precoGasBanco : precosGasBanco) {
					getHibernateTemplate().getSessionFactory().getCurrentSession().evict(precoGasBanco);
					if((precoGas.getChavePrimaria() != precoGasBanco.getChavePrimaria())
									&& (precoGasBanco.getNumeroNotaFiscal().equals(precoGas.getNumeroNotaFiscal()))) {
						throw new NegocioException(ERRO_PRECO_GAS_NOTA_FISCAL_REGISTRADA, true);
					}
				}
			}
		}

		if (precoGas.getObservacao() != null && !precoGas.getObservacao().isEmpty()
				&& precoGas.getObservacao().length() > LIMITE_TAMANHO_OBSERVACAO) {
			throw new NegocioException(ERRO_PRECO_GAS_OBSERVACAO_MAIOR_200, true);
		}

		if(precoGas.getPrecoGasItens() != null && precoGas.getPrecoGasItens().isEmpty()) {

			throw new NegocioException(ERRO_PRECO_GAS_SEM_ITENS, true);
		}

		if (precoGas.getChavePrimaria() > 0) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			// valida o preco gas
			filtro.put("idPrecoGasComplementar", precoGas.getChavePrimaria());
			Collection<PrecoGas> precosGas = consultarPrecoGas(filtro);
			if (!precosGas.isEmpty()) {

				this.verificarExistenciaContratoCompraDePrecoGas(precoGas, precosGas);
			}
		}

		if(precoGas.getPrecoGasItens() != null) {
			for (PrecoGasItem precoGasItemAtual : precoGas.getPrecoGasItens()) {
				validarPrecoGasItem(precoGas, null, precoGasItemAtual);
			}
		}
	}

	private void verificarExistenciaContratoCompraDePrecoGas(PrecoGas precoGas, Collection<PrecoGas> precosGas) throws NegocioException {

		Boolean bFlagEncontrado = false;

		for (PrecoGas precoGasAtual : precosGas) {
			if (precoGasAtual.getContratoCompra().getChavePrimaria() == precoGas.getContratoCompra().getChavePrimaria()) {
				bFlagEncontrado = true;
			}

			this.validarDataFornecimentoPrecoGas(precoGasAtual, precoGas);

			this.validarDataEmissaoNotaFiscalPrecoGas(precoGasAtual, precoGas);

			if (!bFlagEncontrado) {
				throw new NegocioException(ERRO_PRECO_GAS_SITUACAO_COMPRA_POSSUI_COMPLEMENTO, true);
			}
		}
	
	}
	
	private void validarDataFornecimentoPrecoGas(PrecoGas precoGasAtual, PrecoGas precoGas) throws NegocioException {
		if (!(precoGas.getDataFornecimentoFim().equals(precoGasAtual.getDataFornecimentoFim()))
				|| !(precoGas.getDataFornecimentoInicio().equals(precoGasAtual.getDataFornecimentoInicio()))) {
			throw new NegocioException(ERRO_PRECO_GAS_DATA_FORA_COMPLEMENTO, true);
		}
	}

	private void validarDataEmissaoNotaFiscalPrecoGas(PrecoGas precoGasAtual, PrecoGas precoGas) throws NegocioException {
		if (precoGas.getDataEmissaoNotaFiscal().after(precoGasAtual.getDataEmissaoNotaFiscal())) {
			throw new NegocioException(ERRO_PRECO_GAS_DATA_EMISSAO_PAI_MENOR, true);
		}
	}

	/**
	 * Método responsável por Inserir o Preço do
	 * Gas.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Long inserirPrecoGas(PrecoGas precoGas) throws NegocioException {

		this.validarPrecoGas(precoGas);

		return this.inserir(precoGas);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorPrecoGas#atualizarPrecoGas(br.com.ggas.faturamento.tarifa.PrecoGas)
	 */
	@Override
	public void atualizarPrecoGas(PrecoGas precoGas) throws NegocioException {

		this.validarPrecoGas(precoGas);

		try {
			this.atualizar(precoGas);
		} catch(ConcorrenciaException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_CONCORRENCIA, true);

		}
	}

	/**
	 * Método responsável por consultar o Preço do
	 * Gás de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de Precos de Gas.
	 * @return Uma coleção de Preco de Gás.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	@Override
	public Collection<PrecoGas> consultarPrecoGas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String numeroNotaFiscal = (String) filtro.get("numeroNotaFiscal");
			if(numeroNotaFiscal != null && !StringUtils.isEmpty(numeroNotaFiscal)) {
				criteria.add(Restrictions.eq("numeroNotaFiscal", Long.valueOf(numeroNotaFiscal)));
			}

			Long chavePrimariaContratoCompra = (Long) filtro.get("idContratoCompra");
			if(chavePrimariaContratoCompra != null && chavePrimariaContratoCompra > 0) {
				criteria.add(Restrictions.eq("contratoCompra.chavePrimaria", chavePrimariaContratoCompra));
			}

			Date dataInicioEmissaoNotaFiscal = (Date) filtro.get("dataInicioEmissao");
			Date dataFimEmissaoNotaFiscal = (Date) filtro.get("dataFimEmissao");

			if(dataInicioEmissaoNotaFiscal != null && dataFimEmissaoNotaFiscal == null) {
				criteria.add(Restrictions.ge("dataEmissaoNotaFiscal", dataInicioEmissaoNotaFiscal));
			} else if(dataInicioEmissaoNotaFiscal == null && dataFimEmissaoNotaFiscal != null) {
				criteria.add(Restrictions.le("dataEmissaoNotaFiscal", dataFimEmissaoNotaFiscal));
			} else if(dataInicioEmissaoNotaFiscal != null && dataFimEmissaoNotaFiscal != null) {

				if(Util.compararDatas(dataInicioEmissaoNotaFiscal, dataFimEmissaoNotaFiscal) > 0) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_INICIO_MAIOR_DATA_FIM, true);
				}

				criteria.add(Restrictions.ge("dataEmissaoNotaFiscal", dataInicioEmissaoNotaFiscal));
				criteria.add(Restrictions.le("dataEmissaoNotaFiscal", dataFimEmissaoNotaFiscal));
			}

			Date dataInicioFornecimento = (Date) filtro.get("dataInicioFornecimento");
			Date dataFimFornecimento = (Date) filtro.get("dataFimFornecimento");
			if(dataInicioFornecimento != null && dataFimFornecimento != null) {
				criteria.add(Restrictions.eq("dataFornecimentoInicio", dataInicioFornecimento));
				criteria.add(Restrictions.eq("dataFornecimentoFim", dataFimFornecimento));

			}

			Boolean indicadorComplementar = (Boolean) filtro.get("indicadorComplementar");
			if(indicadorComplementar != null) {
				criteria.add(Restrictions.eq("indicadorComplementar", indicadorComplementar));
			}

			Long idPrecoGasComplementar = (Long) filtro.get("idPrecoGasComplementar");
			if(idPrecoGasComplementar != null) {
				criteria.add(Restrictions.eq("precoGas.chavePrimaria", idPrecoGasComplementar));
			}

			Long idPrecoGasReverso = (Long) filtro.get("idPrecoGasReverso");
			if(idPrecoGasReverso != null) {
				criteria.add(Restrictions.ne(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idPrecoGasReverso));
			}
		}

		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.addOrder(Order.asc("numeroNotaFiscal"));

		return criteria.list();

	}

	/**
	 * Método responsável por consultar o itém do
	 * Preço do Gás de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa ddo item do Precos de
	 *            Gas.
	 * @return Uma coleção de Preco de Gás.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	@Override
	public Collection<PrecoGasItem> consultarPrecoGasItem(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadePrecoGasItem());

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idPrecoGas = (Long) filtro.get("idPrecoGas");
			if(idPrecoGas != null) {
				criteria.add(Restrictions.eq("precoGas.chavePrimaria", idPrecoGas));
				criteria.setFetchMode("itemFatura", FetchMode.JOIN);
			}

			Long idPrecoGasComplementar = (Long) filtro.get("idPrecoGasComplementar");
			if(idPrecoGasComplementar != null) {
				criteria.createAlias("precoGas", "precoGasPai");
				criteria.add(Restrictions.eq("precoGasPai.precoGas.chavePrimaria", idPrecoGasComplementar));
			}

			Long idPrecoGasReverso = (Long) filtro.get("idPrecoGasReverso");
			if(idPrecoGasReverso != null) {
				criteria.add(Restrictions.ne("precoGas.chavePrimaria", idPrecoGasReverso));
			}

			Long idItemFatura = (Long) filtro.get("idItemFatura");
			if(idItemFatura != null) {
				criteria.add(Restrictions.eq("itemFatura.chavePrimaria", idItemFatura));
				criteria.setFetchMode("itemFatura", FetchMode.JOIN);
			}
		}

		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorPrecoGas#consultarPrecoGas(long, java.util.Date)
	 */
	@Override
	public PrecoGasItem consultarPrecoGas(long cdItemFatura, Date dataExecucao) throws NegocioException {

		/*
		 * select * from PRECO_GAS pg
		 * join PRECO_GAS_ITEM pgi on pgi.prga_cd
		 * = pg.prga_cd and
		 * pgi.enco_cd_item_fatura = 23
		 * where
		 * to_date('01/03/2010','mm/dd/yyyy')
		 * between pg.PRGA_DT_FORNECIMENTO_INICIO
		 * and pg.PRGA_DT_FORNECIMENTO_FIM
		 */
		if(dataExecucao != null) {
			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" pg ");
			hql.append(" join ");
			hql.append(getClasseEntidadePrecoGasItem().getSimpleName());
			hql.append(" pgi ");
			hql.append(" on ");
			hql.append(" pgi.precoGas.chavePrimaria = pg.chavePrimaria nad pgi.itemFatura.chavePrimaria = ? ");
			hql.append(" where ");
			hql.append(" ? between pg.dataFornecimentoInicio and pg.dataFornecimentoFim ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong(0, cdItemFatura);

			query.setDate(1, dataExecucao);

			return (PrecoGasItem) query.uniqueResult();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * ControladorPrecoGas
	 * #consultarPrecoGas(java.util.Date,
	 * java.util.Date,
	 * br.com.ggas.geral.EntidadeConteudo,
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public BigDecimal consultarPrecoGas(Date dataMedicaoInicial, Date dataMedicaoFinal, EntidadeConteudo itemFatura,
					EntidadeConteudo contratoCompra) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		BigDecimal precoGas = BigDecimal.ZERO;
		BigDecimal somaValorReferencia = BigDecimal.ZERO;
		BigDecimal somaVolumeReferencia = BigDecimal.ZERO;

		String arredondamentoConsumo = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(TIPO_ARREDONDAMENTO_CONSUMO);
		String arredondamentoCalculo = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(TIPO_ARREDONDAMENTO_CALCULO);
		String pEscala = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(QUANTIDADE_ESCALA_CALCULO);

		final Integer parametroEscala;
		if(pEscala == null) {
			throw new NegocioException(ERRO_NEGOCIO_ESCALA_NAO_INFORMADA, true);
		} else {
			parametroEscala = Integer.valueOf(pEscala);
		}

		if(contratoCompra == null) {
			throw new NegocioException(ERRO_PRECO_GAS_CONTRATO_COMPRA, true);
		}

		// Consulta PrecoGasItem, caso existam
		// notas complementares esse método já
		// retorna seus valores agrupados.
		Collection<PrecoGasItem> colecaoPrecoGasItem = consultarPrecoGasItem(dataMedicaoInicial, dataMedicaoFinal, itemFatura,
						contratoCompra);

		if(colecaoPrecoGasItem != null && !colecaoPrecoGasItem.isEmpty()) {
			for (PrecoGasItem precoGasItem : colecaoPrecoGasItem) {

				// Verifica o volume médio diário
				// para poder fazer a
				// proporcionalidade de acordo com
				// a qnt de dias utilizados.
				Date dataFornecimentoInicial = precoGasItem.getPrecoGas().getDataFornecimentoInicio();
				Date dataFornecimentoFinal = precoGasItem.getPrecoGas().getDataFornecimentoFim();
				int intervaloFornecimento = Util.quantidadeDiasIntervalo(dataFornecimentoInicial, dataFornecimentoFinal);

				BigDecimal volumeReferencia = (precoGasItem.getVolume()).divide(new BigDecimal(intervaloFornecimento), parametroEscala,
								Integer.parseInt(arredondamentoConsumo));

				/*
				 * Verificando por quantos dias
				 * foi utilizada esta nota.
				 * Se a data de fornecimento
				 * inicial for menor que data de
				 * medicao inicial, usar a data de
				 * medicao inicial.
				 * Se a data de fornecimento final
				 * for maior que a data de medicao
				 * final, usar a data de medicao
				 * final.
				 */
				Date dataUtilizacaoInicial = dataFornecimentoInicial;
				Date dataUtilizacaoFinal = dataFornecimentoFinal;

				if(dataFornecimentoInicial.compareTo(dataMedicaoInicial) < 0) {
					dataUtilizacaoInicial = dataMedicaoInicial;
				}

				if(dataFornecimentoFinal.compareTo(dataMedicaoFinal) > 0) {
					dataUtilizacaoFinal = dataMedicaoFinal;
				}

				// Verifica o intervalo de
				// utilizacao.
				int qtdDiasUtilizacao = Util.quantidadeDiasIntervalo(dataUtilizacaoInicial, dataUtilizacaoFinal);

				// Faz a proporcionalidade, média
				// diária multiplicado pela
				// quantidade de dias de
				// utilização.
				volumeReferencia = volumeReferencia.multiply(new BigDecimal(qtdDiasUtilizacao));

				// O valor de referência será o
				// volume médio utilizado
				// multiplicado pelo valor.
				BigDecimal valorReferencia = volumeReferencia.multiply(precoGasItem.getValor());

				// O volume e valor serão somados
				// a cada iteração.
				somaValorReferencia = somaValorReferencia.add(valorReferencia);
				somaVolumeReferencia = somaVolumeReferencia.add(volumeReferencia);
			}

			// O preço do gás será a soma dos
			// valores divido pela soma dos
			// volumes
			precoGas = somaValorReferencia.divide(somaVolumeReferencia, parametroEscala, Integer.parseInt(arredondamentoCalculo));

		}

		return precoGas;

	}

	/**
	 * Este método irá retornar uma coleção da
	 * entidade PrecoGasItem para um
	 * período de medição, item de fatura e
	 * contrato de compra.
	 * 
	 * @param dataInicial
	 *            Data inicial do período de
	 *            medição.
	 * @param dataFinal
	 *            Data final do período de
	 *            medição.
	 * @param itemFatura
	 *            Item de Fatura.
	 * @param contratoCompra
	 *            Contrato de Compra.
	 * @return Coleção da entidade PrecoGasItem.
	 * @throws NegocioException
	 */
	@SuppressWarnings("unchecked")
	private Collection<PrecoGasItem> consultarPrecoGasItem(Date dataInicial, Date dataFinal, EntidadeConteudo itemFatura,
					EntidadeConteudo contratoCompra) {

		// Essa consulta trás todos os registros
		// independentemente de ser 'nota
		// complementar' ou não que estejam
		// habilitados.
		Collection<PrecoGasItem> colecaoPrecoGasItem = null;

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadePrecoGasItem().getSimpleName());
		hql.append(" precoGasItem ");
		hql.append(" inner join fetch precoGasItem.precoGas precoGas ");
		hql.append(" where precoGasItem.itemFatura.chavePrimaria = :itemFatura ");
		hql.append(" and  precoGas.habilitado = true ");
		hql.append(" and  precoGas.contratoCompra.chavePrimaria = :contratoCompra ");
		hql.append(" and  (( ");
		hql.append(" 	precoGas.dataFornecimentoInicio < :dataInicial ");
		hql.append(" 	and precoGas.dataFornecimentoFim >= :dataFinal ");
		hql.append("       ) ");
		hql.append(" or (precoGas.dataFornecimentoInicio between :dataInicial and :dataFinal) ");
		hql.append(" or (precoGas.dataFornecimentoFim between :dataInicial and :dataFinal)) ");
		hql.append(" order by precoGas.indicadorComplementar asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("itemFatura", itemFatura.getChavePrimaria());
		query.setLong("contratoCompra", contratoCompra.getChavePrimaria());
		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);

		colecaoPrecoGasItem = (Collection<PrecoGasItem>) query.list();

		// Trata o resultado da consulta para
		// agrupar valores das notas
		// complementares
		Map<Long, PrecoGasItem> mapaPrecoGasItem = new HashMap<Long, PrecoGasItem>();

		if(colecaoPrecoGasItem != null && !colecaoPrecoGasItem.isEmpty()) {
			for (PrecoGasItem precoGasItem : colecaoPrecoGasItem) {
				if(!precoGasItem.getPrecoGas().getIndicadorComplementar()) {

					// Se não é complementar, vai
					// adicionando no mapa.
					mapaPrecoGasItem.put(precoGasItem.getPrecoGas().getChavePrimaria(), precoGasItem);

				} else {

					// Se for um item
					// complementar, obter do mapa
					// o elemento que será
					// complementado.
					if(precoGasItem.getPrecoGas() != null && precoGasItem.getPrecoGas().getPrecoGas() != null) {

						PrecoGas precoGasPrincipal = precoGasItem.getPrecoGas().getPrecoGas();

						// Obter do mapa o
						// elemento que tenha o
						// 'preço do gás' igual ao
						// principal.
						PrecoGasItem precoGasItemComplementavel = mapaPrecoGasItem.get(precoGasPrincipal.getChavePrimaria());

						// Verifica se encontrou o
						// elemento no mapa e soma
						// seu valor com o valor
						// do item complementar.
						if(precoGasItemComplementavel != null) {
							precoGasItemComplementavel.setValor(precoGasItemComplementavel.getValor().add(precoGasItem.getValor()));
							mapaPrecoGasItem.put(precoGasPrincipal.getChavePrimaria(), precoGasItemComplementavel);
						}
					}
				}
			}
		}
		return mapaPrecoGasItem.values();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PrecoGas.BEAN_ID_PRECO_GAS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.ControladorPrecoGas#criarPrecoGasItem()
	 */
	@Override
	public PrecoGasItem criarPrecoGasItem() {

		return (PrecoGasItem) ServiceLocator.getInstancia().getBeanPorID(PrecoGasItem.BEAN_ID_PRECO_GAS_ITEM);
	}

	/**
	 *
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(PrecoGas.BEAN_ID_PRECO_GAS);
	}

	/**
	 * @return
	 */
	public Class<?> getClasseEntidadePrecoGasItem() {

		return ServiceLocator.getInstancia().getClassPorID(PrecoGasItem.BEAN_ID_PRECO_GAS_ITEM);
	}

	/**
	 * Método responsável por remover os Precos do
	 * Gas selecionados.
	 * 
	 * @param chavesPrimarias
	 *            As chaves dos Precos do gas a
	 *            serem excluídos.
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	@Override
	public void removerPrecoGas(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();

			filtro.put(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<PrecoGas> listaPrecoGas = this.consultarPrecoGas(filtro);

			if((listaPrecoGas != null) && (!listaPrecoGas.isEmpty())) {
				for (PrecoGas precoGas : listaPrecoGas) {

					filtro.clear();
					filtro.put("idPrecoGasComplementar", precoGas.getChavePrimaria());
					filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, true);
					Collection<PrecoGas> precosGas = consultarPrecoGas(filtro);
					this.validarListaPrecosGasVazia(precosGas);

					this.removerPrecoGasItem(precoGas, auditoria);

					precoGas.setDadosAuditoria(auditoria);
					super.remover(precoGas, false);
				}
			}
		}
	}
	
	private void validarListaPrecosGasVazia(Collection<PrecoGas> precosGas) throws NegocioException {
		if (!precosGas.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_EXCLUIR_PRECO_GAS_COM_COMPLEMENTO, true);
		}
	}

	private void removerPrecoGasItem(PrecoGas precoGas,DadosAuditoria auditoria) throws NegocioException {
		for (PrecoGasItem precoGasItem : precoGas.getPrecoGasItens()) {
			precoGasItem.setDadosAuditoria(auditoria);
			super.remover(precoGasItem, false);
		}
	}
}
