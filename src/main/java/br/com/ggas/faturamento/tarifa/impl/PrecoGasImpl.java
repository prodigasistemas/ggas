/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos relacionados ao preço do gás
 * 
 */
public class PrecoGasImpl extends EntidadeNegocioImpl implements PrecoGas {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6164188612075665001L;

	private Long numeroNotaFiscal;

	private Date dataEmissaoNotaFiscal;

	private Date dataFornecimentoInicio;

	private Date dataFornecimentoFim;

	private EntidadeConteudo contratoCompra;

	private PrecoGas precoGas;

	private String observacao;

	private Boolean indicadorComplementar;

	private Collection<PrecoGasItem> precoGasItens = new HashSet<PrecoGasItem>();

	@Override
	public Boolean getIndicadorComplementar() {

		return indicadorComplementar;
	}

	@Override
	public void setIndicadorComplementar(Boolean indicadorComplementar) {

		this.indicadorComplementar = indicadorComplementar;
	}

	@Override
	public PrecoGas getPrecoGas() {

		return precoGas;
	}

	@Override
	public void setPrecoGas(PrecoGas precoGas) {

		this.precoGas = precoGas;
	}

	@Override
	public EntidadeConteudo getContratoCompra() {

		return contratoCompra;
	}

	@Override
	public void setContratoCompra(EntidadeConteudo contratoCompra) {

		this.contratoCompra = contratoCompra;
	}

	@Override
	public String getObservacao() {

		return observacao;
	}

	@Override
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getNumeroNotaFiscal()
	 */
	@Override
	public Long getNumeroNotaFiscal() {

		return numeroNotaFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setNumeroNotaFiscal(java.math.BigDecimal)
	 */
	@Override
	public void setNumeroNotaFiscal(Long numeroNotaFiscal) {

		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getDataEmissaoNotaFiscal()
	 */
	@Override
	public Date getDataEmissaoNotaFiscal() {

		return dataEmissaoNotaFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setDataEmissaoNotaFiscal(java.util.Date)
	 */
	@Override
	public void setDataEmissaoNotaFiscal(Date dataEmissaoNotaFiscal) {
		if(dataEmissaoNotaFiscal != null) {
			this.dataEmissaoNotaFiscal = dataEmissaoNotaFiscal;
		} else {
			this.dataEmissaoNotaFiscal = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getDataFornecimentoInicio()
	 */
	@Override
	public Date getDataFornecimentoInicio() {

		return dataFornecimentoInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setDataFornecimentoInicio(java.util.Date)
	 */
	@Override
	public void setDataFornecimentoInicio(Date dataFornecimentoInicio) {
		if(dataFornecimentoInicio != null) {
			this.dataFornecimentoInicio = dataFornecimentoInicio;
		} else {
			this.dataFornecimentoInicio = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getDataFornecimentoFim()
	 */
	@Override
	public Date getDataFornecimentoFim() {

		return dataFornecimentoFim;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setDataFornecimentoFim(java.util.Date)
	 */
	@Override
	public void setDataFornecimentoFim(Date dataFornecimentoFim) {
		if(dataFornecimentoFim != null) {
			this.dataFornecimentoFim = dataFornecimentoFim;
		} else {
			this.dataFornecimentoFim = null;
		}
	}

	/**
	 * 
	 */
	@Override
	public void setPrecoGasItens(Collection<PrecoGasItem> precoGasItens) {

		this.precoGasItens = precoGasItens;
	}

	/**
	 * @return
	 */
	@Override
	public Collection<PrecoGasItem> getPrecoGasItens() {

		return precoGasItens;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(numeroNotaFiscal == null) {
			stringBuilder.append(NUMERO_NOTA_FISCAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(contratoCompra == null) {
			stringBuilder.append(CONTRATO_COMPRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataFornecimentoInicio == null) {
			stringBuilder.append(DATA_FORNECIMENTO_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataFornecimentoFim == null) {
			stringBuilder.append(DATA_FORNECIMENTO_FIM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indicadorComplementar == null) {
			stringBuilder.append(INDICADOR_COMPLEMENTAR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if (indicadorComplementar && precoGas == null) {
				stringBuilder.append(PRECO_GAS_A_COMPLEMENTAR);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if(dataEmissaoNotaFiscal == null) {
			stringBuilder.append(DATA_EMISSAO_NOTA_FISCAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
