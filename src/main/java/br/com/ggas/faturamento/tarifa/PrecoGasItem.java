/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Preco Gas Item
 *
 */
public interface PrecoGasItem extends EntidadeNegocio {

	String BEAN_ID_PRECO_GAS_ITEM = "precoGasItem";

	String PRECO_GAS_ITEM = "PRECO_GAS_ITEM";

	String PRECOS_GAS_ITEM = "PRECOS_GAS_ITEM";

	String PRECOS_GAS_ITEM_ITEM_FATURA = "PRECOS_GAS_ITEM_ITEM_FATURA";

	String PRECOS_GAS_ITEM_VALOR = "PRECOS_GAS_ITEM_VALOR";

	String PRECOS_GAS_ITEM_DATA_VENCIMENTO = "PRECOS_GAS_ITEM_DATA_VENCIMENTO";

	String PRECOS_GAS_VOLUME = "PRECOS_GAS_VOLUME";

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getItemFatura()
	 */
	/**
	 * @return Retorna EntidadeConteudo - Retorna objetido entidade conteudo
	 * com informações sobre item fatura.
	 */
	EntidadeConteudo getItemFatura();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setItemFatura
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	/**
	 * @param itemFatura - Set Item fatura.
	 */
	void setItemFatura(EntidadeConteudo itemFatura);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setCompraContrato
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	/**
	 * @return Volume - Retorna Volume.
	 */
	BigDecimal getVolume();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setCompraContrato
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	/**
	 * @param volume - Set volume.
	 */
	void setVolume(BigDecimal volume);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getDescricao()
	 */
	/**
	 * @return BigDecimal - Retorna Valor.
	 */
	BigDecimal getValor();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setDescricao(java.lang.String)
	 */
	/**
	 * @param valor - Set Valor.
	 */
	void setValor(BigDecimal valor);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getDescricaoAbreviada()
	 */
	/**
	 * @return Date - Retorna Data de vencimento.
	 */
	Date getDataVencimento();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	/**
	 * @param dataVencimento - Set Data vencimento.
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * @return
	 */
	PrecoGas getPrecoGas();

	/**
	 * @param precoGas
	 */
	void setPrecoGas(PrecoGas precoGas);

}
