package br.com.ggas.faturamento.tarifa.impl;

import java.util.Date;

import br.com.ggas.faturamento.tarifa.AcaoSincroniaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;

/**
 * The Class AcaoSincroniaDescontoImpl.
 */
public class AcaoSincroniaDescontoImpl implements AcaoSincroniaDesconto {
	
	private Date data;
	private AcaoSincronia acao;
	private TarifaVigenciaDesconto desconto;
	
	/**
	 * Instantiates a new acao sincronia desconto impl.
	 *
	 * @param data the data
	 * @param acao the acao
	 * @param desconto the desconto
	 */
	public AcaoSincroniaDescontoImpl(Date data, AcaoSincronia acao, TarifaVigenciaDesconto desconto) {
		super();
		if(data != null) {
			this.data = (Date) data.clone();
		}
		this.acao = acao;
		this.desconto = desconto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDescontoI#getData()
	 */
	@Override
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) this.data.clone();
		}
		return dataTmp;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDescontoI#setData(java.util.Date)
	 */
	@Override
	public void setData(Date data) {
		if(data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDescontoI#getAcao()
	 */
	@Override
	public AcaoSincronia getAcaoSincronia() {
		return acao;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDescontoI#setAcao(br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDesconto.Acao)
	 */
	@Override
	public void setAcaoSincronia(AcaoSincronia acao) {
		this.acao = acao;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDescontoI#getDesconto()
	 */
	@Override
	public TarifaVigenciaDesconto getDesconto() {
		return desconto;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.AcaoSincroniaDescontoI#setDesconto(br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto)
	 */
	@Override
	public void setDesconto(TarifaVigenciaDesconto desconto) {
		this.desconto = desconto;
	}
	
}
