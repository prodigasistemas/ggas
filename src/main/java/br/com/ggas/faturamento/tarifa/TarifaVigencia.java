/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a TarifaVigencia
 *
 */
public interface TarifaVigencia extends EntidadeNegocio {

	String BEAN_ID_TARIFA_VIGENCIA = "tarifaVigencia";

	String TARIFA_VIGENCIA = "TARIFA_VIGENCIA";

	String TARIFAS_VIGENCIA = "TARIFAS_VIGENCIA";

	String TARIFA = "TARIFA";

	String DATA_VIGENCIA = "TARIFA_VIGENCIA_DATA_VIGENCIA";

	String UNIDADE_MONETARIA = "TARIFA_VIGENCIA_UNIDADE_MONETARIA";

	String TIPO_CALCULO = "TARIFA_VIGENCIA_TIPO_CALCULO";

	String BASE_APURACAO = "TARIFA_VIGENCIA_BASE_APURACAO";

	String OBSERVACAO = "TARIFA_VIGENCIA_OBSERVACAO";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getTarifaVigencia()
	 */
	/**
	 * @return Collection TarifaVigenciaFaixa -  Tarifas vigências Faixas.
	 */
	Collection<TarifaVigenciaFaixa> getTarifaVigenciaFaixas();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getTarifaVigencia()
	 */
	/**
	 * @param tarifaVigenciaFaixas - Set Tarifas vigências Faixas.
	 */
	void setTarifaVigenciaFaixas(Collection<TarifaVigenciaFaixa> tarifaVigenciaFaixas);

	/**
	 * @return the baseApuracao
	 */
	EntidadeConteudo getBaseApuracao();

	/**
	 * @param baseApuracao
	 *            the baseApuracao to set
	 */
	void setBaseApuracao(EntidadeConteudo baseApuracao);

	/**
	 * @return the unidadeMonetaria
	 */
	EntidadeConteudo getUnidadeMonetaria();

	/**
	 * @param unidadeMonetaria
	 *            the unidadeMonetaria to set
	 */
	void setUnidadeMonetaria(EntidadeConteudo unidadeMonetaria);

	/**
	 * @return the tipoCalculo
	 */
	EntidadeConteudo getTipoCalculo();

	/**
	 * @param tipoCalculo
	 *            the tipoCalculo to set
	 */
	void setTipoCalculo(EntidadeConteudo tipoCalculo);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaVigencia
	 * #getTarifa()
	 */
	/**
	 * @return Tarifa - Tarifa.
	 */
	Tarifa getTarifa();

	/**
	 * @return String - Comentário.
	 */
	String getComentario();

	/**
	 * @param comentario
	 *            the comentario to set
	 */
	void setComentario(String comentario);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaVigencia
	 * #
	 * setTarifa(br.com.ggas.faturamento.tarifa.Tarifa
	 * )
	 */
	/**
	 * @param tarifa - Set Tarifa.
	 */
	void setTarifa(Tarifa tarifa);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaVigencia
	 * #getDataVigencia()
	 */
	/**
	 * @return Date - Data vigência.
	 */
	Date getDataVigencia();

	/**
	 * @return String - Retorna Data viagência formatada.
	 */
	String getDataVigenciaFormatada();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaVigencia
	 * #setDataVigencia(java.util.Date)
	 */
	/**
	 * @param dataVigencia - Set Data vigência.
	 */
	void setDataVigencia(Date dataVigencia);

	/**
	 * @return the status
	 */
	EntidadeConteudo getStatus();

	/**
	 * @param status
	 *            the status to set
	 */
	void setStatus(EntidadeConteudo status);

	/**
	 * @return the ultimoUsuarioAlteracao
	 */
	Usuario getUltimoUsuarioAlteracao();

	/**
	 * @param ultimoUsuarioAlteracao
	 *            the ultimoUsuarioAlteracao to
	 *            set
	 */
	void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao);

	/**
	 * @return retorna InformaMsgIcmsSubstituido
	 */
	Boolean getInformarMsgIcmsSubstituido();

	/**
	 * @param informarMsgIcmsSubstituido - Set Informar Mensagem Icms Substituido.
	 */
	void setInformarMsgIcmsSubstituido(Boolean informarMsgIcmsSubstituido);

	/**
	 * @return retorna BaseCalculoIcmsSubtituido
	 */
	BigDecimal getBaseCalculoIcmsSubstituido();

	/**
	 * @param baseCalculoIcmsSubstituido - Set Base cálculo icms substituido.
	 */
	void setBaseCalculoIcmsSubstituido(BigDecimal baseCalculoIcmsSubstituido);

	/**
	 * @return retorna IcmsSubstituidoMetroCubico
	 */
	BigDecimal getIcmsSubstituidoMetroCubico();

	/**
	 * @param icmsSubstituidoMetroCubico - Set Icms Substituido Métro Cúbico.
	 */
	void setIcmsSubstituidoMetroCubico(BigDecimal icmsSubstituidoMetroCubico);

	/**
	 * @return retorna MensagemIcmsSubstituto
	 */
	String getMensagemIcmsSubstituto();

	/**
	 * @param mensagemIcmsSubstituto - Set Mensagem Icms do Substituto
	 */
	void setMensagemIcmsSubstituto(String mensagemIcmsSubstituto);

	/**
	 * @return Retorna Tributos
	 */
	Collection<TarifaVigenciaTributo> getTributos();

	/**
	 * @param tributos - Set Tributos.
	 */
	void setTributos(Collection<TarifaVigenciaTributo> tributos);

	/**
	 * @return Retorna DescricaoTipoCalculo
	 */
	String getDescricaoTipoCalculo();

	/**
	 * @return Retorna DescricaoBaseApuracao
	 */
	String getDescricaoBaseApuracao();

	/**
	 * @return DescricaoComentario
	 */
	String getDescricaoComentario();

	/**
	 * @return DescricaoUnidadeMonetaria
	 */
	String getDescricaoUnidadeMonetaria();

	/**
	 * @return DescricaoStatus
	 */
	String getDescricaoStatus();

}
