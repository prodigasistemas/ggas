package br.com.ggas.faturamento.tarifa.impl;

import java.util.Date;

import br.com.ggas.faturamento.tarifa.AcaoSincroniaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;

/**
 * The Class FabricaAcaoSincroniaDesconto.
 */
public class FabricaAcaoSincroniaDesconto {

	/**
	 * Criar.
	 *
	 * @param novaData the nova data
	 * @param acao the acao
	 * @param desconto the desconto
	 * @return the acao sincronia desconto
	 */
	public static AcaoSincroniaDesconto criar(Date novaData, AcaoSincronia acao, TarifaVigenciaDesconto desconto) {
		return new AcaoSincroniaDescontoImpl(novaData, acao, desconto);
	}

}
