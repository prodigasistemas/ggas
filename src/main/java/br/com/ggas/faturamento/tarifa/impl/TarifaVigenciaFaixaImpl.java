/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;

/**
 * 
 * 
 */
class TarifaVigenciaFaixaImpl extends EntidadeNegocioImpl implements TarifaVigenciaFaixa {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6532384064291486637L;

	private TarifaVigencia tarifaVigencia;

	private BigDecimal medidaInicio;

	private BigDecimal medidaFim;

	private BigDecimal valorFixo = BigDecimal.ZERO;

	private BigDecimal valorCompra;

	private BigDecimal margemValorAgregado;

	private BigDecimal valorVariavel = BigDecimal.ZERO;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getTarifaVigencia()
	 */
	@Override
	public TarifaVigencia getTarifaVigencia() {

		return tarifaVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa
	 * #setTarifaVigencia(br.com
	 * .ggas.faturamento.tarifa.TarifaVigencia)
	 */
	@Override
	public void setTarifaVigencia(TarifaVigencia tarifaVigencia) {

		this.tarifaVigencia = tarifaVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMedidaInicio()
	 */
	@Override
	public BigDecimal getMedidaInicio() {

		return medidaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMedidaInicioFormatada()
	 */
	@Override
	public String getMedidaInicioFormatada() {

		return medidaInicio.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa
	 * #setMedidaInicio(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaInicio(BigDecimal medidaInicio) {

		this.medidaInicio = medidaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMedidaFim()
	 */
	@Override
	public BigDecimal getMedidaFim() {

		return medidaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMedidaFimFormatada()
	 */
	@Override
	public String getMedidaFimFormatada() {

		return medidaFim.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa
	 * #setMedidaFim(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaFim(BigDecimal medidaFim) {

		this.medidaFim = medidaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorFixo()
	 */
	@Override
	public BigDecimal getValorFixo() {

		return valorFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorFixoFormatado()
	 */
	@Override
	public String getValorFixoFormatado() {

		Integer numeroCasasDecimais = this.getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal().intValue();
		return NumeroUtil.converterCampoValorDecimalParaString(valorFixo, numeroCasasDecimais);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa
	 * #setValorFixo(java.math.BigDecimal)
	 */
	@Override
	public void setValorFixo(BigDecimal valorFixo) {

		this.valorFixo = valorFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorCompra()
	 */
	@Override
	public BigDecimal getValorCompra() {

		return valorCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorCompraFormatado()
	 */
	@Override
	public String getValorCompraFormatado() {

		Integer numeroCasasDecimais = this.getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal().intValue();
		if(valorCompra != null) {
			return NumeroUtil.converterCampoValorDecimalParaString(valorCompra, numeroCasasDecimais);
		}
		return "-";
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorCompraFormatado(
	 * java.lang.Integer)
	 */
	@Override
	public String getValorCompraFormatado(Integer numeroCasasDecimais) {

		if(valorCompra != null) {
			return NumeroUtil.converterCampoValorDecimalParaString(valorCompra, numeroCasasDecimais);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#setValorCompra()
	 */
	@Override
	public void setValorCompra(BigDecimal valorCompra) {

		this.valorCompra = valorCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMargemValorAgregado()
	 */
	@Override
	public BigDecimal getMargemValorAgregado() {

		return margemValorAgregado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMargemValorAgregadoFormatado()
	 */
	@Override
	public String getMargemValorAgregadoFormatado() {

		Integer numeroCasasDecimais = this.getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal().intValue();
		if(margemValorAgregado != null) {
			return NumeroUtil.converterCampoValorDecimalParaString(margemValorAgregado, numeroCasasDecimais);
		}
		return "-";
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getMargemValorAgregadoFormatado(
	 * java.lang.Integer)
	 */
	@Override
	public String getMargemValorAgregadoFormatado(Integer numeroCasasDecimais) {

		if(margemValorAgregado != null) {
			return NumeroUtil.converterCampoValorDecimalParaString(margemValorAgregado, numeroCasasDecimais);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#setMargemValorAgregado()
	 */
	@Override
	public void setMargemValorAgregado(BigDecimal margemValorAgregado) {

		this.margemValorAgregado = margemValorAgregado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorVariavel()
	 */
	@Override
	public BigDecimal getValorVariavel() {

		return valorVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorVariavelFormatado()
	 */
	@Override
	public String getValorVariavelFormatado() {

		Integer numeroCasasDecimais = this.getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal().intValue();
		return NumeroUtil.converterCampoValorDecimalParaString(valorVariavel, numeroCasasDecimais);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa
	 * #setValorVariavel(java.math.BigDecimal)
	 */
	@Override
	public void setValorVariavel(BigDecimal valorVariavel) {

		this.valorVariavel = valorVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tarifaVigencia == null) {
			stringBuilder.append(TARIFA_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(medidaInicio == null) {
			stringBuilder.append(MEDIDA_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(medidaFim == null) {
			stringBuilder.append(MEDIDA_FIM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
