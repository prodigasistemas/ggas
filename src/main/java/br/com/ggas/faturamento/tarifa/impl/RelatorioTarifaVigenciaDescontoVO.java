/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Collection;

import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;

/**
 * 
 * Relatorio Tarifa Vigencia DescontoVO
 *
 */
public class RelatorioTarifaVigenciaDescontoVO {

	private TarifaVigenciaDesconto tarifaVigenciaDesconto;

	private Collection<TarifaFaixaDesconto> tarifaFaixaDescontos;

	private String tipoDesconto;

	/**
	 * Montar relatorio tarifa vigencia desconto vo.
	 * 
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param tarifaFaixaDescontos
	 *            the tarifa faixa descontos
	 * @param tipoDesconto
	 *            the tipo desconto
	 * @return the relatorio tarifa vigencia desconto vo
	 */
	public static RelatorioTarifaVigenciaDescontoVO montarRelatorioTarifaVigenciaDescontoVO(TarifaVigenciaDesconto tarifaVigenciaDesconto,
					Collection<TarifaFaixaDesconto> tarifaFaixaDescontos, String tipoDesconto) {

		RelatorioTarifaVigenciaDescontoVO relatorioTarifaVigenciaDescontoVO = new RelatorioTarifaVigenciaDescontoVO();

		relatorioTarifaVigenciaDescontoVO.setTipoDesconto(tipoDesconto);
		relatorioTarifaVigenciaDescontoVO.setTarifaVigenciaDesconto(tarifaVigenciaDesconto);
		relatorioTarifaVigenciaDescontoVO.setTarifaFaixaDescontos(tarifaFaixaDescontos);

		return relatorioTarifaVigenciaDescontoVO;
	}

	public String getDataInicioDescontoVigenciaFormatada() {

		return tarifaVigenciaDesconto.getInicioVigenciaFormatado();
	}

	public String getDataFimDescontoVigenciaFormatada() {

		return tarifaVigenciaDesconto.getFimVigenciaFormatado();
	}

	public String getDescricaoDescontoVigencia() {

		return tarifaVigenciaDesconto.getDescricaoDescontoVigencia();
	}

	public String getTipoDesconto() {

		return tipoDesconto;
	}

	public void setTipoDesconto(String tipoDesconto) {

		this.tipoDesconto = tipoDesconto;
	}

	/**
	 * @return the tarifaVigenciaDesconto
	 */
	public TarifaVigenciaDesconto getTarifaVigenciaDesconto() {

		return tarifaVigenciaDesconto;
	}

	/**
	 * @param tarifaVigenciaDesconto
	 *            the tarifaVigenciaDesconto to set
	 */
	public void setTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto) {

		this.tarifaVigenciaDesconto = tarifaVigenciaDesconto;
	}

	/**
	 * @return the tarifaFaixaDescontos
	 */
	public Collection<TarifaFaixaDesconto> getTarifaFaixaDescontos() {

		return tarifaFaixaDescontos;
	}

	/**
	 * @param tarifaFaixaDescontos
	 *            the tarifaFaixaDescontos to set
	 */
	public void setTarifaFaixaDescontos(Collection<TarifaFaixaDesconto> tarifaFaixaDescontos) {

		this.tarifaFaixaDescontos = tarifaFaixaDescontos;
	}

}
