/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.util.Date;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * aos Descontos da Tarifa de Vigência 
 *
 */
public interface TarifaVigenciaDesconto extends EntidadeNegocio {

	String BEAN_ID_TARIFA_VIGENCIA_DESCONTO = "tarifaVigenciaDesconto";

	String TARIFA_VIGENCIA_DESCONTO = "TARIFA_VIGENCIA_DESCONTO";

	String TARIFAS_VIGENCIA_DESCONTO = "TARIFAS_VIGENCIA_DESCONTO";

	String TARIFA = "TARIFA";

	String TARIFA_VIGENCIA_INICIO = "TARIFA_VIGENCIA_INICIO";

	String TARIFA_VIGENCIA_FIM = "TARIFA_VIGENCIA_FIM";

	String IMPRESSAO_DESCONTO = "TARIFA_VIGENCIA_IMPRESSAO_DESCONTO";

	String IMPRESSAO_DESCRICAO = "TARIFA_VIGENCIA_IMPRESSAO_DESCRICAO";

	String DESCRICAO = "TARIFA_DESCONTRO_DESCRICAO";

	String TARIFA_DESCONTO = "TARIFA_DESCONTO";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto#getTarifa()
	 */
	/**
	 * @return Retorna Tarifa
	 */
	Tarifa getTarifa();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #setTarifa(br.com.ggas
	 * .faturamento.tarifa.Tarifa)
	 */
	/**
	 * @param tarifa - Set tarifa.
	 */
	void setTarifa(Tarifa tarifa);

	/**
	 * @return a vigência da tarifa
	 */
	TarifaVigencia getTarifaVigencia();

	/**
	 * @param tarifaVigencia
	 *            nova vigência da tarifa
	 */
	void setTarifaVigencia(TarifaVigencia tarifaVigencia);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto#getIniciovigencia()
	 */
	/**
	 * @return Date - Retorna Inicio Vigência.
	 */
	Date getInicioVigencia();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto#getInicioVigenciaFormatado()
	 */
	/**
	 * @return String - Retorno Inicio Vigência Formatado.
	 */
	String getInicioVigenciaFormatado();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #setIniciovigencia(java.util.Date)
	 */
	/**
	 * @param inicioVigencia - Set Inicio vigência.
	 */
	void setInicioVigencia(Date inicioVigencia);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto#getFimVigencia()
	 */
	/**
	 * @return Date - Retorna Fim Vigência.
	 * 
	 */
	Date getFimVigencia();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto#getFimVigenciaFormatado()
	 */
	/**
	 * @return String - Retorna Fim vingência Formatado.
	 */
	String getFimVigenciaFormatado();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #setFimvigencia(java.util.Date)
	 */
	/**
	 * @param fimVigencia - Set Fim vigência.
	 */
	void setFimVigencia(Date fimVigencia);

	/**
	 * @return String - Retorna Vigências Formatadas.
	 */
	String getVigenciasFormatadas();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto#getDescricao()
	 */
	/**
	 * @return String - Retorna Descrição.
	 */
	String getDescricaoDescontoVigencia();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #setDescricao(java.lang.String)
	 */
	/**
	 * @param descricao - Set descrição.
	 */
	void setDescricaoDescontoVigencia(String descricao);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #getImpressaodescricao()
	 */
	/**
	 * @return Boolean - Retorna Impressão Descrição.
	 */
	boolean getImpressaoDescricao();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #setImpressaodescricao
	 * (java.math.BigDecimal)
	 */
	/**
	 * @param impressaoDescricao - Set impressão descrição.
	 */
	void setImpressaoDescricao(boolean impressaoDescricao);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #getImpressaodesconto()
	 */
	/**
	 * @return Boolean - Retorna impressão Desconto.
	 */
	boolean getImpressaoDesconto();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVirgenciaDesconto
	 * #setImpressaodesconto(java.math.BigDecimal)
	 */
	/**
	 * @param impressaoDesconto - Set Immpressão desconto.
	 */
	void setImpressaoDesconto(boolean impressaoDesconto);

}
