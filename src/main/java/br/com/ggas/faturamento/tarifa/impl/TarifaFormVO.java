package br.com.ggas.faturamento.tarifa.impl;

/**
 * Classe responsável por representar campos do formulário de Tarifa.
 * 
 * @author esantana
 *
 */
public class TarifaFormVO {

	private String[] faixaFinal;
	private String[] valorFixoSemImposto;
	private String[] valorCompraEscondido;
	private String[] margemValorAgregadoEscondido;
	private String[] valorVariavelSemImpostoEscondido;
	private String[] descontoFixoSemImposto;
	private String[] descontoVariavelSemImposto;
	private Long idTipoDesconto;
	private Long idCloneFaixa;
	private Long idDescontoCadastrado;
	private Long idVigenciaTarifa;
	private Long idVigenciasTarifa;
	private String vigenciaBaseCalculoIcms;
	private String vigenciaIcmsSubstituidoMetroCubico;
	private Long[] tributosAssociados;
	private String valorReajuste;
	private String faixaInicialExclusao;
	private Long idTipoReajuste;
	private Long chavePrimariaVigencia;
	private Long chavePrimariaVigenciaDesconto;
	private Long ultimaFaixa;
	private String chavesPrimarias;
	private Long status;
	private Boolean isAutorizacao;
	private String dataVigencia;
	private Boolean habilitado;
 
	public Long getIdDescontoCadastrado() {
		return idDescontoCadastrado;
	}

	public void setIdDescontoCadastrado(Long idDescontoCadastrado) {
		this.idDescontoCadastrado = idDescontoCadastrado;
	}

	public Long getIdVigenciaTarifa() {
		return idVigenciaTarifa;
	}

	public void setIdVigenciaTarifa(Long idVigenciaTarifa) {
		this.idVigenciaTarifa = idVigenciaTarifa;
	}

	public Long[] getTributosAssociados() {
		Long[] retorno = null;
		if(this.tributosAssociados != null) {
			retorno = this.tributosAssociados.clone();
		}
		return retorno;
	}

	public void setTributosAssociados(Long[] idTributosAssociados) {
		if(idTributosAssociados != null) {
			this.tributosAssociados = idTributosAssociados.clone();
		}
	}

	public String[] getFaixaFinal() {
		String[] retorno = null;
		if(this.faixaFinal != null) {
			retorno = this.faixaFinal.clone();
		}
		return retorno;
	}

	public void setFaixaFinal(String[] faixaFinal) {
		if(faixaFinal != null) {
			this.faixaFinal = faixaFinal.clone();
		}
	}

	public String[] getValorFixoSemImposto() {
		String[] retorno = null;
		if(this.valorFixoSemImposto != null) {
			retorno = this.valorFixoSemImposto.clone();
		}
		return retorno;
	}

	public void setValorFixoSemImposto(String[] valorFixoSemImposto) {
		if(valorFixoSemImposto != null) {
			this.valorFixoSemImposto = valorFixoSemImposto.clone();
		}
	}

	public String[] getValorCompraEscondido() {
		String[] retorno = null;
		if(this.valorCompraEscondido != null) {
			retorno = valorCompraEscondido.clone();
		}
		return retorno;
	}

	public void setValorCompraEscondido(String[] valorCompraEscondido) {
		if(valorCompraEscondido != null) {
			this.valorCompraEscondido = valorCompraEscondido.clone();
		}
		
	}

	public String[] getMargemValorAgregadoEscondido() {
		String[] retorno = null;
		if(this.margemValorAgregadoEscondido != null) {
			retorno = this.margemValorAgregadoEscondido.clone();
		}
		return retorno;
	}

	public void setMargemValorAgregadoEscondido(String[] margemValorAgregadoEscondido) {
		if(margemValorAgregadoEscondido != null) {
			this.margemValorAgregadoEscondido = margemValorAgregadoEscondido.clone();
		}
		
	}

	public String[] getValorVariavelSemImpostoEscondido() {
		String[] retorno = null;
		if(this.valorVariavelSemImpostoEscondido != null) {
			retorno = this.valorVariavelSemImpostoEscondido.clone();
		}
		return retorno;
	}

	public void setValorVariavelSemImpostoEscondido(String[] valorVariavelSemImpostoEscondido) {
		if(valorVariavelSemImpostoEscondido != null) {
			this.valorVariavelSemImpostoEscondido = valorVariavelSemImpostoEscondido.clone();
		}
	}

	public String[] getDescontoFixoSemImposto() {
		String[] retorno = null;
		if(this.descontoFixoSemImposto != null) {
			retorno = descontoFixoSemImposto.clone();
		}
		return retorno;
	}

	public void setDescontoFixoSemImposto(String[] descontoFixoSemImposto) {
		if(descontoFixoSemImposto != null) {
			this.descontoFixoSemImposto = descontoFixoSemImposto.clone();
		}
	}

	public String[] getDescontoVariavelSemImposto() {
		String[] retorno = null;
		if(this.descontoVariavelSemImposto != null) {
			retorno = this.descontoVariavelSemImposto.clone();
		}
		return retorno;
	}

	public void setDescontoVariavelSemImposto(String[] descontoVariavelSemImposto) {
		if(descontoVariavelSemImposto != null) {
			this.descontoVariavelSemImposto = descontoVariavelSemImposto.clone();
		}
	}

	public Long getIdTipoDesconto() {
		return idTipoDesconto;
	}

	public void setIdTipoDesconto(Long idTipoDesconto) {
		this.idTipoDesconto = idTipoDesconto;
	}

	public Long getIdCloneFaixa() {
		return idCloneFaixa;
	}

	public void setIdCloneFaixa(Long idCloneFaixa) {
		this.idCloneFaixa = idCloneFaixa;
	}

	public Long getIdVigenciasTarifa() {
		return idVigenciasTarifa;
	}

	public void setIdVigenciasTarifa(Long idVigenciasTarifa) {
		this.idVigenciasTarifa = idVigenciasTarifa;
	}

	public String getVigenciaBaseCalculoIcms() {
		return vigenciaBaseCalculoIcms;
	}

	public void setVigenciaBaseCalculoIcms(String vigenciaBaseCalculoIcms) {
		this.vigenciaBaseCalculoIcms = vigenciaBaseCalculoIcms;
	}

	public String getVigenciaIcmsSubstituidoMetroCubico() {
		return vigenciaIcmsSubstituidoMetroCubico;
	}

	public void setVigenciaIcmsSubstituidoMetroCubico(String vigenciaIcmsSubstituidoMetroCubico) {
		this.vigenciaIcmsSubstituidoMetroCubico = vigenciaIcmsSubstituidoMetroCubico;
	}

	public String getValorReajuste() {
		return valorReajuste;
	}

	public void setValorReajuste(String valorReajuste) {
		this.valorReajuste = valorReajuste;
	}

	public Long getIdTipoReajuste() {
		return idTipoReajuste;
	}

	public void setIdTipoReajuste(Long idTipoReajuste) {
		this.idTipoReajuste = idTipoReajuste;
	}

	public Long getChavePrimariaVigencia() {
		return chavePrimariaVigencia;
	}

	public void setChavePrimariaVigencia(Long chavePrimariaVigencia) {
		this.chavePrimariaVigencia = chavePrimariaVigencia;
	}

	public Long getChavePrimariaVigenciaDesconto() {
		return chavePrimariaVigenciaDesconto;
	}

	public void setChavePrimariaVigenciaDesconto(Long chavePrimariaVigenciaDesconto) {
		this.chavePrimariaVigenciaDesconto = chavePrimariaVigenciaDesconto;
	}

	public String getFaixaInicialExclusao() {
		return faixaInicialExclusao;
	}

	public void setFaixaInicialExclusao(String faixaInicialExclusao) {
		this.faixaInicialExclusao = faixaInicialExclusao;
	}

	public Long getUltimaFaixa() {
		return ultimaFaixa;
	}

	public void setUltimaFaixa(Long ultimaFaixa) {
		this.ultimaFaixa = ultimaFaixa;
	}

	public String getChavesPrimarias() {
		return chavesPrimarias;
	}

	public void setChavesPrimarias(String chavesPrimarias) {
		this.chavesPrimarias = chavesPrimarias;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Boolean getIsAutorizacao() {
		return isAutorizacao;
	}

	public void setIsAutorizacao(Boolean isAutorizacao) {
		this.isAutorizacao = isAutorizacao;
	}

	public String getDataVigencia() {
		return dataVigencia;
	}

	public void setDataVigencia(String dataVigencia) {
		this.dataVigencia = dataVigencia;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	
}
