/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface relacionada ao preço dos gás
 *
 */
public interface PrecoGas extends EntidadeNegocio {

	String BEAN_ID_PRECO_GAS = "precoGas";

	String PRECO_GAS = "PRECO_GAS";

	String PRECOS_GAS = "PRECOS_GAS";

	String NUMERO_NOTA_FISCAL = "NUMERO_NOTA_FISCAL";

	String DATA_EMISSAO_NOTA_FISCAL = "DATA_EMISSAO_NOTA_FISCAL";

	String DATA_FORNECIMENTO_INICIO = "DATA_FORNECIMENTO_INICIO";

	String DATA_FORNECIMENTO_FIM = "DATA_FORNECIMENTO_FIM";

	String PRECO_GAS_QUANTIDADE_CASA_DECIMAL = "PRECO_GAS_QUANTIDADE_CASA_DECIMAL";

	String CONTRATO_COMPRA = "CONTRATO_COMPRA";

	String INDICADOR_COMPLEMENTAR = "INDICADOR_COMPLEMENTAR";

	String PRECO_GAS_A_COMPLEMENTAR = "PRECO_GAS_A_COMPLEMENTAR";

	/**
	 * @return Indicador complementar
	 */
	Boolean getIndicadorComplementar();

	/**
	 * @param indicadorComplementar - Set indicador complementar.
	 */
	void setIndicadorComplementar(Boolean indicadorComplementar);

	/**
	 * @return EntidadeConteudo - Retorna objeto entidade conteudo com informações 
	 * sobre contrato da compra.
	 */
	EntidadeConteudo getContratoCompra();

	/**
	 * @param contratoCompra - Set contrato compra.
	 */
	void setContratoCompra(EntidadeConteudo contratoCompra);

	/**
	 * @return PrecoGas - Retorna o objeto preço gás.
	 */
	PrecoGas getPrecoGas();

	/**
	 * @param contratoCompra - Set Contrato da compra.
	 */
	void setPrecoGas(PrecoGas contratoCompra);

	/**
	 * @return String - Retorna Observação.
	 */
	String getObservacao();

	/**
	 * @param observacao - Set observação.
	 */
	void setObservacao(String observacao);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getNumeroNotaFiscal()
	 */
	/**
	 * @return Long - Retorna Numero da Nota Fiscal.
	 */
	Long getNumeroNotaFiscal();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setNumeroNotaFiscal(java.math.BigDecimal)
	 */
	/**
	 * @param numeroNotaFiscal - Set número da nota fiscal.
	 */
	void setNumeroNotaFiscal(Long numeroNotaFiscal);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getDataEmissaoNotaFiscal()
	 */
	/**
	 * @return Date - Retorna data Emissao da nota Fiscal.
	 */
	Date getDataEmissaoNotaFiscal();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setDataEmissaoNotaFiscal(java.util.Date)
	 */

	/**
	 * @param dataEmissaoNotaFiscal - Set Data Emissão de nota fiscal.
	 */
	void setDataEmissaoNotaFiscal(Date dataEmissaoNotaFiscal);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getDataFornecimentoInicio()
	 */
	/**
	 * @return Date - Retorna data de fornecimento inicial.
	 */
	Date getDataFornecimentoInicio();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setDataFornecimentoInicio(java.util.Date)
	 */
	/**
	 * @param dataFornecimentoInicio - Set data de forneicmento do inicio. 
	 */
	void setDataFornecimentoInicio(Date dataFornecimentoInicio);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #getDataFornecimentoFim()
	 */
	/**
	 * @return Date - Retorna Data Fornecimento Fim.
	 */
	Date getDataFornecimentoFim();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #setDataFornecimentoFim(java.util.Date)
	 */
	/**
	 * @param dataFornecimentoFim - Set Data fornecimeno fim.
	 */
	void setDataFornecimentoFim(Date dataFornecimentoFim);

	/**
	 * @param precoGasItens
	 */
	void setPrecoGasItens(Collection<PrecoGasItem> precoGasItens);

	/**
	 * @return
	 */
	Collection<PrecoGasItem> getPrecoGasItens();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.PrecoGas
	 * #validarDados()
	 */
	@Override
	Map<String, Object> validarDados();

}
