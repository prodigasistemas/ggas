/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.tarifa.PrecoGas;
import br.com.ggas.faturamento.tarifa.PrecoGasItem;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos relacionados a entidade item de preço de gás 
 * 
 */
public class PrecoGasItemImpl extends EntidadeNegocioImpl implements PrecoGasItem {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */

	private static final long serialVersionUID = 7003849470820720007L;

	private EntidadeConteudo itemFatura;

	private BigDecimal volume;

	private BigDecimal valor;

	private Date dataVencimento;

	private PrecoGas precoGas;

	/**
	 * @return the valor
	 */
	@Override
	public BigDecimal getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	@Override
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}

	/**
	 * @return the dataVencimento
	 */
	@Override
	public Date getDataVencimento() {

		return dataVencimento;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	/**
	 * @return the precoGas
	 */
	@Override
	public PrecoGas getPrecoGas() {

		return precoGas;
	}

	/**
	 * @param precoGas
	 *            the precoGas to set
	 */
	@Override
	public void setPrecoGas(PrecoGas precoGas) {

		this.precoGas = precoGas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * PrecoGasItem#getItemFatura()
	 */
	@Override
	public EntidadeConteudo getItemFatura() {

		return itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * PrecoGasItem
	 * #setItemFatura(br.com.ggas.geral
	 * .EntidadeConteudo)
	 */
	@Override
	public void setItemFatura(EntidadeConteudo itemFatura) {

		this.itemFatura = itemFatura;
	}

	@Override
	public BigDecimal getVolume() {

		return volume;
	}

	@Override
	public void setVolume(BigDecimal volume) {

		this.volume = volume;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * PrecoGasItem#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(itemFatura == null) {
			stringBuilder.append(PRECOS_GAS_ITEM_ITEM_FATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataVencimento == null) {
			stringBuilder.append(PRECOS_GAS_ITEM_DATA_VENCIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(volume == null) {
			stringBuilder.append(PRECOS_GAS_VOLUME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(valor == null) {
			stringBuilder.append(PRECOS_GAS_ITEM_VALOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}
}
