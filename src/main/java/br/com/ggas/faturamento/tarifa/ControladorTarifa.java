/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.tarifa.impl.FiltroTarifaVigenciaDescontos;
import br.com.ggas.faturamento.tarifa.impl.TarifaVO;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.FormatoImpressao;

/**
 * 
 * ControladorTarifa
 *
 */
public interface ControladorTarifa extends ControladorNegocio {

	String VALOR_MAXIMO_FAIXA_FINAL = "99999999999999999";

	String BEAN_ID_CONTROLADOR_TARIFA = "controladorTarifa";

	String ERRO_NEGOCIO_DATA_INICIO_MAIOR_QUE_DATA_FIM = "ERRO_NEGOCIO_DATA_INICIO_MAIOR_QUE_DATA_FIM";

	String ERRO_NEGOCIO_MES_INICIO_MAIOR_QUE_MES_FIM = "ERRO_NEGOCIO_MES_INICIO_MAIOR_QUE_MES_FIM";

	String ERRO_NEGOCIO_PERIODO_MAIOR_QUE_LIMITE = "ERRO_NEGOCIO_PERIODO_MAIOR_QUE_LIMITE";

	String ERRO_NEGOCIO_INDICE_FINANCEIRO_ALTERADO = "ERRO_NEGOCIO_INDICE_FINANCEIRO_ALTERADO";

	String ERRO_NEGOCIO_DATA_INICIAL_MAIOR_QUE_DATA_FINAL = "ERRO_NEGOCIO_DATA_INICIAL_MAIOR_QUE_DATA_FINAL";

	String ERRO_NEGOCIO_REAJUSTE = "ERRO_NEGOCIO_REAJUSTE";

	String ERRO_NEGOCIO_EXIBICAO_FAIXAS_TARIFA_DESCONTO = "ERRO_NEGOCIO_EXIBICAO_FAIXAS_TARIFA_DESCONTO";

	String ERRO_NEGOCIO_DESCONTO_SOBREPOSTO = "ERRO_NEGOCIO_DESCONTO_SOBREPOSTO";

	String ERRO_NEGOCIO_EXCLUSAO_TARIFA = "ERRO_NEGOCIO_EXCLUSAO_TARIFA";

	String ERRO_NEGOCIO_EXCLUSAO_DESCONTO = "ERRO_NEGOCIO_EXCLUSAO_DESCONTO";

	String ERRO_NEGOCIO_ALTERACAO_DATA_VIGENCIA = "ERRO_NEGOCIO_ALTERACAO_DATA_VIGENCIA";

	String ERRO_NEGOCIO_TARIFA_VEGENTE = "ERRO_NEGOCIO_TARIFA_VEGENTE";

	String ERRO_NEGOCIO_VIGENCIA_DESCONTO_NAO_CADASTRADA = "ERRO_NEGOCIO_VIGENCIA_DESCONTO_NAO_CADASTRADA";

	String ERRO_NEGOCIO_SELECAO_VIGENCIA_CLONAR = "ERRO_NEGOCIO_SELECAO_VIGENCIA_CLONAR";

	String ERRO_NEGOCIO_EXCLUSAO_VIGENCIA = "ERRO_NEGOCIO_EXCLUSAO_VIGENCIA";

	String ERRO_NEGOCIO_EXCLUSAO_VIGENCIA_COM_TARIFA = "ERRO_NEGOCIO_EXCLUSAO_VIGENCIA_COM_TARIFA";

	String ERRO_CONCORRENCIA = "ERRO_CONCORRENCIA";

	String ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_PERIODO_OBRIGATORIO = 
					"ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_PERIODO_OBRIGATORIO";

	String ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_DATA_INICIAL_OBRIGATORIO = 
					"ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_DATA_INICIAL_OBRIGATORIO";

	String ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_DATA_FINAL_OBRIGATORIO = 
					"ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_DATA_FINAL_OBRIGATORIO";

	String ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_DATA_INICIAL_MAIOR_DATA_FINAL = 
					"ERRO_NEGOCIO_RELATORIO_TARIFAS_CADASTRADAS_CAMPO_DATA_INICIAL_MAIOR_DATA_FINAL";

	String ERRO_NEGOCIO_DATA_INICIAL_TARIFA_CAD = "ERRO_NEGOCIO_DATA_INICIAL_TARIFA_CAD";

	String ERRO_NEGOCIO_DATA_FINAL_TARIFA_CAD = "ERRO_NEGOCIO_DATA_FINAL_TARIFA_CAD";

	String ERRO_NEGOCIO_DATA_INICIAL_MAIOR_TARIFA_CAD = "ERRO_NEGOCIO_DATA_INICIAL_MAIOR_TARIFA_CAD";

	String ERRO_NEGOCIO_DATA_INICIAL_INV_TARIFA_CAD = "ERRO_NEGOCIO_DATA_INICIAL_INV_TARIFA_CAD";

	String ERRO_NEGOCIO_DATA_FINAL_INV_TARIFA_CAD = "ERRO_NEGOCIO_DATA_FINAL_INV_TARIFA_CAD";

	/**
	 * Obter tarifa.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the tarifa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Tarifa obterTarifa(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um
	 * IndiceFinanceiro.
	 * 
	 * @param chavePrimaria
	 *            Chave primária do índice
	 *            financeiro.
	 * @return Um IndiceFinanceiro.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	IndiceFinanceiro obterIndiceFinanceiro(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por consultar as Tarifas
	 * de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de ATarifas.
	 * @return Uma coleção de Tarifas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Tarifa> consultarTarifas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as Tarifas.
	 * 
	 * @return Uma coleção de Tarifas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Tarifa> listarTarifas() throws NegocioException;

	/* ****************************************************************
	 * Métodos de TarifaVigencia
	 * ********************************************
	 * ********************
	 */

	/**
	 * Método responsável por consultar as
	 * TarifasVigencia de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de TarifasVigencia.
	 * @return Uma coleção de TarifasVigencia.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<TarifaVigencia> consultarTarifasVigencia(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * TarifasVigencia.
	 * 
	 * @return Uma coleção de Tarifas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigencia> listarTarifasVigencia() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifasVigencia pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma TarifasVigencia
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaVigencia obterTarifaVigencia(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifasVigencia pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Uma TarifasVigencia
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaVigencia obterTarifaVigencia(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por verificar se uma
	 * determinada tarifa esta associada ao Dolar.
	 * 
	 * @param chavePrimariaTarifa
	 *            A chave primaria da tarifa para
	 *            verificar se está relacionada ao
	 *            Dolar
	 * @return true, if successful
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean verificarUnidadeMonetariaTarifaVigencia(long chavePrimariaTarifa) throws NegocioException;

	/* ****************************************************************
	 * Métodos de TarifaVigenciaFaixa
	 * ********************************************
	 * ********************
	 */

	/**
	 * Método responsável por consultar as
	 * TarifaVigenciaFaixa de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de
	 *            TarifaVigenciaFaixa.
	 * @return Uma coleção de TarifaVigenciaFaixa.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaFaixa> consultarTarifasVigenciaFaixa(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * TarifaVigenciaFaixa.
	 * 
	 * @return Uma coleção de TarifaVigenciaFaixa.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaFaixa> listarTarifasVigenciaFaixa() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaVigenciaFaixa pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma TarifaVigenciaFaixa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaVigenciaFaixa obterTarifaVigenciaFaixa(Long chavePrimaria) throws NegocioException;

	/* ****************************************************************
	 * Métodos de TarifaFaixaDesconto
	 * ********************************************
	 * ********************
	 */

	/**
	 * Método responsável por consultar as
	 * TarifaFaixaDesconto de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de
	 *            TarifaFaixaDesconto.
	 * @return Uma coleção de TarifaFaixaDesconto.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaFaixaDesconto> consultarTarifasFaixaDesconto(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * TarifaFaixaDesconto.
	 * 
	 * @return Uma coleção de TarifaFaixaDesconto.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaFaixaDesconto> listarTarifasFaixaDesconto() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaFaixaDesconto pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma TarifaFaixaDesconto
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaFaixaDesconto obterTarifaFaixaDesconto(Long chavePrimaria) throws NegocioException;

	/* ****************************************************************
	 * Métodos de TarifaTributo
	 * ********************************************
	 * ********************
	 */

	/**
	 * Método responsável por consultar as
	 * TarifaTributo de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de TarifaTributo.
	 * @return Uma coleção de TarifaTributo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaTributo> consultarTarifasTributo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * TarifaTributo.
	 * 
	 * @return Uma coleção de TarifaTributo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaTributo> listarTarifasTributo() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaTributo pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma TarifaTributo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaVigenciaTributo obterTarifaTributo(Long chavePrimaria) throws NegocioException;

	/* ****************************************************************
	 * Métodos de TarifaVigenciaDesconto
	 * ********************************************
	 * ********************
	 */

	/**
	 * Método responsável por consultar as
	 * TarifaVigenciaDesconto de acordo com o
	 * filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de
	 *            TarifaVigenciaDesconto.
	 * @return Uma coleção de
	 *         TarifaVigenciaDesconto.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaDesconto> consultarTarifasVigenciaDesconto(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * descontos de uma tarifa num dado período.
	 * Se a data final do período não for passada
	 * será desconsiderada na consulta.
	 * 
	 * @param idTarifa
	 *            chavePrimaria da tarifa
	 * @param dataInicioPeriodo
	 *            data de início do período
	 * @param dataFimPeriodo
	 *            data final do período
	 * @return coleção de TarifaVigenciaDesconto
	 *         para a tarifa no período
	 * @throws NegocioException
	 *             caso ocorra alguma exceção
	 */
	List<TarifaVigenciaDesconto> consultarTarifasVigenciaDescontoPorTarifaPeriodo(Long idTarifa, Date dataInicioPeriodo, Date dataFimPeriodo)
					throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * descontos de uma tarifa vigencia.
	 * 
	 * @param chaveTarifaVigencia
	 *            chavePrimaria da tarifa vigencia
	 * @return coleção de TarifaVigenciaDesconto
	 *         para a tarifa vigencia
	 */
	List<TarifaVigenciaDesconto> consultarTarifasVigenciaDescontoPorTarifaVigencia(long chaveTarifaVigencia);

	/**
	 * Método responsável por listar as
	 * TarifaVigenciaDesconto.
	 * 
	 * @return Uma coleção de
	 *         TarifaVigenciaDesconto.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaDesconto> listarTarifasVigenciaDesconto() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaVigenciaDesconto pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Uma TarifaVigenciaDesconto
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaVigenciaDesconto obterTarifaVigenciaDesconto(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaVigenciaDesconto pela Chave Primária.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Uma TarifaVigenciaDesconto
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TarifaVigenciaDesconto obterTarifaVigenciaDesconto(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaVigencia.
	 * 
	 * @return Uma TarifaVigencia
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarTarifaVigencia() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaVigenciaFaixa.
	 * 
	 * @return Uma TarifaVigenciaFaixa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarTarifaVigenciaFaixa() throws NegocioException;

	/**
	 * Método responsável por obter uma
	 * TarifaTributo.
	 * 
	 * @return Uma TarifaTributo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarTarifaTributo() throws NegocioException;

	/**
	 * Método responsável por remover as tarifas
	 * selecionadas.
	 * 
	 * @param chavesPrimarias
	 *            As chaves dos perfis a serem
	 *            excluídos.
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerTarifa(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException;

	/**
	 * Método responsável por remover as tarifas
	 * selecionadas.
	 * 
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarTarifaVigenciaDesconto() throws NegocioException;

	/**
	 * Método responsável por criar uma
	 * TarifaFaixaDesconto.
	 * 
	 * @return the entidade negocio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarTarifaFaixaDesconto() throws NegocioException;

	/**
	 * Método responsável por criar uma
	 * DadosFaixasTarifa.
	 * 
	 * @return the dados faixas tarifa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DadosFaixasTarifa criarDadosFaixasTarifa() throws NegocioException;

	/**
	 * Método responsável por criar um
	 * IndiceFinanceiroValorNominal.
	 * 
	 * @return IndiceFinanceiroValorNominal.
	 */
	EntidadeNegocio criarIndiceFinanceiroValorNominal();

	/**
	 * Método responsável pela inserção de Tarifa.
	 * @param tarifa - {@link Tarifa}
	 * @param tarifaVigencia - {@link TarifaVigencia}
	 * @param tarifaVigenciaDesconto - {@link TarifaVigenciaDesconto}
	 * @param listaDadosFaixa - {@link List}
	 * @return chave de tarifa - {@link Long}
	 * @throws GGASException - {@link GGASException}
	 */
	Long inserirTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException;

	/**
	 * Método responsável por Consultar as tarifas
	 * selecionadas com as vigencias.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaPesquisar> obterTarifaVigenciaPesquisar(Map<String, Object> filtro) throws GGASException;

	/**
	 * Método responsável por gerar os índices
	 * financeiros para o periodo de datas
	 * desejado ou retornar os já
	 * existentes, caso exista algum no período.
	 * 
	 * @param dataInicio
	 *            Data de ínicio do período.
	 * @param dataFim
	 *            Data de fim do período.
	 * @param idIndice
	 *            Chave primária do Índice
	 *            Financeiro.
	 * @return Uma coleção de
	 *         IndicaFinanceiroValorNominal.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IndiceFinanceiroValorNominal> gerarIndicesFinanceiros(Date dataInicio, Date dataFim, Long idIndice) throws NegocioException;

	/**
	 * Método responsável por listar os índices
	 * financeiros existentes em um período de
	 * datas.
	 * 
	 * @param dataInicio
	 *            Data de início do período.
	 * @param dataFim
	 *            Data de fim do período.
	 * @param idIndice
	 *            Chave primária do Índice
	 *            Financeiro.
	 * @return Uma coleção de
	 *         IndiceFinanceiroValorNominal.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IndiceFinanceiroValorNominal> listarIndiceFinanceiroEntreData(Date dataInicio, Date dataFim, Long idIndice)
					throws NegocioException;

	/**
	 * Método responsável por inserir novos
	 * índices financeiro e remover e atualizar os
	 * já existentes.
	 * 
	 * @param datasReferencia
	 *            Array com as datas dos índices
	 *            financeiros.
	 * @param mesDatasReferencia
	 *            Array com os meses dos índices
	 *            financeiros.
	 * @param valoresNominais
	 *            Array com valores dos índices
	 *            financeiros.
	 * @param indicePopulado
	 *            IndiceFinanceiroValorNominal já
	 *            populado que será usado para
	 *            popular os demais.
	 * @param indicadorUtilizado
	 *            Array com indicador de o índice
	 *            está sendo usado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarIndicesFinanceiros(String[] datasReferencia, String[] mesDatasReferencia, String[] valoresNominais,
					IndiceFinanceiroValorNominal indicePopulado, String[] indicadorUtilizado) throws GGASException;

	/**
	 * Método que levanta os erros.
	 * 
	 * @param erro
	 *            the erro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void exibirErroTarifa(String erro) throws NegocioException;

	/**
	 * Método responsável por popular os dados do
	 * VO das faixas da vigencia da tarifa.
	 * 
	 * @param tarifaVigencia
	 *            A tarifa vigência
	 * @return Uma coleção de dados das faixas da
	 *         tarifa.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<DadosFaixasTarifa> popularDadosTarifaVigenciaFaixa(TarifaVigencia tarifaVigencia) throws GGASException;

	/**
	 * Método responsável por obter uma
	 * TarifaPontoConsumo.
	 * 
	 * @return Uma TarifaPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeNegocio criarTarifaPontoConsumo() throws NegocioException;

	/***
	 * Método responsável por inserir, atualizar e
	 * excluir uma TarifaPontoConsumo.
	 * 
	 * @param idPontoConsumo
	 *            Chave primaria do ponto de
	 *            consumo
	 * @param itensTarifaPontoConsumo
	 *            Coleção de TarifaPontoConsumo
	 *            para ser alterado ou inserido ou
	 *            excluido
	 * @param dadosAuditoria
	 *            Os dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void atualizarTarifasPontoConsumo(Long idPontoConsumo, Collection<TarifaPontoConsumo> itensTarifaPontoConsumo,
					DadosAuditoria dadosAuditoria) throws NegocioException;

	/***
	 * Método responsável por verificar se a
	 * tarifa esta sendo usada em alguma fatura.
	 * 
	 * @param tarifaPontoConsumo
	 *            A tarifaPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarTarifaFaturamento(TarifaPontoConsumo tarifaPontoConsumo) throws NegocioException;

	/**
	 * *
	 * Método responsável por lista todos os item
	 * fatura de um determinado ponto de consumo
	 * associados a um contrato ativo.
	 * 
	 * @param contratoPontoConsumo
	 *            Um contratoPontoConsumo onde vai
	 *            ser utilizado informações a
	 *            respeito do contrato e do ponto
	 *            de consumo
	 * @return Uma coleção de EntidadeConteudo de
	 *         itemFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarItemFaturaPontoConsumo(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;

	/**
	 * *
	 * Método responsável por listar as
	 * TarifaVigencia de um determinado itemFatura
	 * onde o segmento e o mesmo do ponto de
	 * consumo.
	 * 
	 * @param idItemFatura
	 *            A chave primária do itemFatura
	 * @param idSegmento
	 *            A chave primária do segmento
	 * @return Uma coleção de TarifaVigencia
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigencia> listarTarifaVigenciaItemFatura(Long idItemFatura, Long idSegmento) throws NegocioException;

	/***
	 * Método responsável por validar dados da
	 * tela de associação temporária de tarifa a
	 * um ponto de consumo.
	 * 
	 * @param dataInicial
	 *            a dataInicial da vigência de uma
	 *            tarifaPontoConsumo
	 * @param dataFinal
	 *            a dataFinal da vigência de uma
	 *            tarifaPontoConsumo
	 * @param idItemFatura
	 *            a chave primária de um
	 *            itemFatura
	 * @param idTarifa
	 *            a chave primária de uma tarifa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDadosAdicionarAssocTempTarifaPontoConsumo(Date dataInicial, Date dataFinal, Long idItemFatura, Long idTarifa)
					throws NegocioException;

	/**
	 * *
	 * Método responsável pela menor data de
	 * vigencia de uma tarifa.
	 * 
	 * @param idTarifa
	 *            Chave primária de uma tarifa
	 * @return A menos data de vigência de uma
	 *         tarifa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date obterDataVigenciaTarifa(Long idTarifa) throws NegocioException;

	/***
	 * Método responsável por validar se já existe
	 * uma associação para o período de vigência
	 * informado.
	 * 
	 * @param listaTarifaPontoConsumo
	 *            Coleção de TarifaPontoConsumo
	 * @param itemTarifaPontoConsumoAtual
	 *            Um objeto TarifaPontoConsumo que
	 *            será verificado se está no
	 *            período de vigência
	 * @param index
	 *            O index do objeto
	 *            itemTarifaPontoConsumoAtual para
	 *            ser usado na verifiação dentro
	 *            da coleção
	 *            listaTarifaPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarItemFaturaRepetidoTarifaPontoConsumo(Collection<TarifaPontoConsumo> listaTarifaPontoConsumo,
					TarifaPontoConsumo itemTarifaPontoConsumoAtual, Integer index) throws NegocioException;

	/**
	 * *
	 * Método que lista as TarifaPontoConsumo.
	 * 
	 * @param idPontoConsumo
	 *            Chave primária de um ponto de
	 *            consumo
	 * @return Uma coleção de TarifaPontoConsumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaPontoConsumo> listarTarifaPontoConsumo(Long idPontoConsumo) throws NegocioException;

	/***
	 * Método responsável por validar se um ponto
	 * de consumo está associado a um contrato.
	 * 
	 * @param itensFatura
	 *            Uma coleção de EntidadeConteudo
	 *            do tipo itemFatura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarPontoConsumoImovelContrato(Collection<EntidadeConteudo> itensFatura) throws NegocioException;

	/**
	 * Método responsável por popular os dados do
	 * VO das faixas dos descontos da tarifa.
	 * 
	 * @param listaDadosFaixasTarifa
	 *            A lista de dados das faixas da
	 *            tarifa.
	 * @param tarifaVigenciaDesconto
	 *            A tarifa vigência desconto.
	 * @return Uma coleção de dados das faixas de
	 *         desconto das tarifas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<DadosFaixasTarifa> popularDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa,
					TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException;

	/**
	 * Método responsável por popular os dados do
	 * VO das faixas dos descontos da tarifa sem
	 * descontos.
	 * 
	 * @param listaDadosFaixasTarifa
	 *            A lista de dados das faixas da
	 *            tarifa.
	 * @return Uma coleção de dados das faixas de
	 *         desconto das tarifas.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<DadosFaixasTarifa> popularDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa) throws GGASException;

	/**
	 * Método reponsável por ordenar as faixas por
	 * matriz.
	 * 
	 * @param vetores
	 *            O mapa com os valores para
	 *            ordenação.
	 * @param listaDadosFaixa
	 *            lista de dados das faixas.
	 * @return the list
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<DadosFaixasTarifa> ordenarFaixasPorMatriz(Map<String, String[]> vetores, List<DadosFaixasTarifa> listaDadosFaixa)
					throws GGASException;

	/**
	 * Ordenar faixas por matriz.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @return the list
	 * @throws GGASException
	 *             the GGAS exception
	 */
	List<DadosFaixasTarifa> ordenarFaixasPorMatriz(List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException;

	/**
	 * Método responsável por ordenar as faixas
	 * para o VO.
	 * 
	 * @param listaDadosFaixaTemp
	 *            Lista temporária dos dados da
	 *            faixa
	 * @param listaDadosFaixaTela
	 *            Lista dos dados da faixa
	 * @return the list
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<DadosFaixasTarifa> ordenarFaixasPorVO(List<DadosFaixasTarifa> listaDadosFaixaTemp, List<DadosFaixasTarifa> listaDadosFaixaTela)
					throws GGASException;

	/**
	 * Método responsável por reajustar os valores
	 * das faixas da tarifa de acordo com o valor
	 * informado.
	 * 
	 * @param valorReajusteParam
	 *            O valor informado
	 * @param idTipoReajusteParam
	 *            a chave do tipo do reajuste
	 * @param listaDadosFaixa
	 *            a lista de dados faixa
	 * @param listaDescontosFaixa
	 *            e a lista de dados dos descontos
	 * @param qtdCasasDecimais
	 *            the qtd casas decimais
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void reajustarFaixaTarifa(String valorReajusteParam, Long idTipoReajusteParam, Collection<DadosFaixasTarifa> listaDadosFaixa,
					Collection<DadosFaixasTarifa> listaDescontosFaixa, Integer qtdCasasDecimais) throws GGASException;

	/**
	 * Método responsável por validar os campos do
	 * reajuste.
	 * 
	 * @param valorReajuste
	 *            O valor informado
	 * @param idTipoReajuste
	 *            O tipo do reajuste
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarCamposReajuste(String valorReajuste, Long idTipoReajuste) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * descontos cadastrados.
	 * 
	 * @param chaveTarifa
	 *            A chave da tarifa
	 * @return the list
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<TarifaFaixaDesconto> consultarDescontosCadastrados(Long chaveTarifa) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * vigencias da tarifa.
	 * 
	 * @param chaveTarifa
	 *            A chave primária da tarifa.
	 * @return lista de tarifaVigencia
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<TarifaVigencia> consultarVigenciasTarifa(Long chaveTarifa) throws NegocioException;

	/**
	 * Método responsável por cadastrar um novo
	 * desconto para a tarifa.
	 * 
	 * @param tarifaVigencia
	 *            A tarifa vigência
	 * @param tarifaVigenciaDesconto
	 *            A tarifa vigência desconto
	 * @param listaDadosFaixa
	 *            A lista de dados da faixa da
	 *            tarifa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void cadastrarDescontos(TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa) throws NegocioException;

	/**
	 * Método reponsável por remover o desconto
	 * cadastrado para a tarifa.
	 * 
	 * @param chavePrimaria
	 *            a chave da tarifa
	 * @param dadosAuditoria
	 *            os dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerDescontoCadastrado(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método reponsável por remover uma vigencia
	 * cadastrada da tarifa.
	 * 
	 * @param chavePrimaria
	 *            a chave da tarifa vigencia
	 * @param dadosAuditoria
	 *            os dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerVigenciaCadastrada(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Validar faixa desconto.
	 * 
	 * @param dadosFaixaTarifa
	 *            the dados faixa tarifa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFaixaDesconto(DadosFaixasTarifa dadosFaixaTarifa) throws NegocioException;

	/**
	 * Validar faixa desconto.
	 * 
	 * @param listaDadosFaixaTarifa
	 *            the lista dados faixa tarifa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixaTarifa) throws GGASException;

	/**
	 * Método responsável por validar a seleção da
	 * vigência e do desconto para exibição da
	 * tabela da faixas de tarifa e descontos.
	 * 
	 * @param idTarifaVigencia
	 *            Chave primária da tarifa
	 *            vigência
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarVigenciaEDescontoParaExibicaoFaixas(Long idTarifaVigencia) throws NegocioException;

	/**
	 * Método responsável por montar a lista de
	 * faixas de tarifa e desconto da tela de
	 * detalhamento.
	 * 
	 * @param tarifaVigencia
	 *            a tarifa vigencia selecionada
	 *            pelo usuário
	 * @param tarifaVigenciaDesconto
	 *            o desconto selecionado pelo
	 *            usuário
	 * @return lista de faixas de tarifa e
	 *         desconto
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<FaixasTarifaDesconto> montarListaFaixasTarifaDesconto(TarifaVigencia tarifaVigencia,
					TarifaVigenciaDesconto tarifaVigenciaDesconto) throws GGASException;

	/**
	 * Validar adicao faixa desconto.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @param validarValoresFaixas
	 *            the validar valores faixas
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarAdicaoFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixa, boolean validarValoresFaixas) throws GGASException;

	/**
	 * Adicionar faixa desconto.
	 * 
	 * @param novaFaixa
	 *            the nova faixa
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<DadosFaixasTarifa> adicionarFaixaDesconto(DadosFaixasTarifa novaFaixa, List<DadosFaixasTarifa> listaDadosFaixa)
					throws NegocioException;

	/**
	 * Adicionar faixa final desconto.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @param indexUltimaFaixa
	 *            the index ultima faixa
	 * @return the list
	 */
	List<DadosFaixasTarifa> adicionarFaixaFinalDesconto(List<DadosFaixasTarifa> listaDadosFaixa, Long indexUltimaFaixa);

	/**
	 * Método responsável por verificar se é para
	 * criar uma nova vigência para a tarifa a ser
	 * alterada.
	 * 
	 * @param idTarifa
	 *            the id tarifa
	 * @param novaDataVigencia
	 *            A nova data da vigência
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarCriacaoNovaVigencia(Long idTarifa, Date novaDataVigencia) throws NegocioException;

	/**
	 * Método responsável por atualizar uma tarifa.
	 * 
	 * @param tarifa
	 *            tarifa a tarifa a ser atualizada
	 * @param tarifaVigencia
	 *            a vigência que vai ser
	 *            incluída/atualizada
	 * @param tarifaVigenciaDesconto
	 *            tarifaVigenciaDesconto a
	 *            vigência de desconto que vai ser
	 *            incluída/atualizada
	 * @param listaDadosFaixa
	 *            dados de cada faixa da vigência
	 * @param isNovaVigencia
	 *            indicador se a vigência é nova
	 *            ou não
	 * @throws GGASException
	 *             caso ocora algum erro durante a
	 *             execução do método
	 */
	void atualizarTarifa(Tarifa tarifa, TarifaVigencia tarifaVigencia, TarifaVigenciaDesconto tarifaVigenciaDesconto,
					List<DadosFaixasTarifa> listaDadosFaixa, boolean isNovaVigencia) throws GGASException;

	/**
	 * Método responsável por listar os índices
	 * financeiros.
	 * 
	 * @return Uma coleção de IndiceFinanceiro.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IndiceFinanceiro> listaIndiceFinanceiro() throws NegocioException;

	/**
	 * Método responsável por montar os dados da
	 * exibição da tela da tarifa.
	 * 
	 * @param idTarifa
	 *            Chave da tarifa
	 * @return Um VO com os dados da tarifa.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DadosTarifa montarDadosTarifa(Long idTarifa) throws GGASException;

	/**
	 * Método responsável por verificar se a nova
	 * vigencia está cadastrada.
	 * 
	 * @param tarifaVigenciaDesconto
	 *            the tarifa vigencia desconto
	 * @param idDescontoCadastrado
	 *            the id desconto cadastrado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarNovaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto, Long idDescontoCadastrado) throws NegocioException;

	/**
	 * Consulta todas as TarifaPontoConsumo de uma
	 * determinada tarifa.
	 * 
	 * @param idTarifa
	 *            chavePrimaria da tarifa
	 * @return coleção de TarifaPontoConsumo
	 *         encontradas
	 */
	Collection<TarifaPontoConsumo> listarTarifaPontoConsumoPorTarifa(Long idTarifa);

	/**
	 * Verifica se uma tarifa já está sendo
	 * utilizada em algum lugar do sistema.
	 * 
	 * @param idTarifa
	 *            chave primária da tarifa
	 * @return true = a tarifa está sendo
	 *         utilizada; false = ele não está
	 *         sendo utilizada.
	 * @throws NegocioException
	 *             caso ocorra algum erro.
	 */
	boolean verificaSeTarifaEstaSendoUtilizada(Long idTarifa) throws NegocioException;

	/**
	 * Método responsável por validar se uma
	 * vigência foi selecionada para clonar as
	 * faixas.
	 * 
	 * @param idVigencia
	 *            Chave primária da vigência.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarSelecaoVigencia(Long idVigencia) throws NegocioException;

	/**
	 * Método responsável por obter todas as
	 * tarifaVigenciaDesconto de uma determinada
	 * tarifaVigencia.
	 * 
	 * @param idTarifaVigencia
	 *            the id tarifa vigencia
	 * @return Uma TarifaVigenciaDesconto.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaDesconto> obterTarifaVigenciaDescontoPorTarifaVigencia(long idTarifaVigencia) throws GGASException;

	/**
	 * Método responsável por obter todas as
	 * tarifaVigenciaDesconto de uma determinada
	 * tarifaVigencia com a ordenacao definida.
	 * 
	 * @param idTarifaVigencia
	 *            id da vigencia para buscar os descontos
	 * @param ordemTarifaVigenciaDescontoInicioVigenciaAsc
	 *            ordenacao da lista ascendente (true) ou descendente (false)
	 * @return a lista tarifa desconto da vigencia indicada pelo id
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TarifaVigenciaDesconto> obterTarifaVigenciaDescontoPorTarifaVigencia(long idTarifaVigencia,
					boolean ordemTarifaVigenciaDescontoInicioVigenciaAsc) throws NegocioException;

	/**
	 * Carregar dados tarifa faixa desconto.
	 * 
	 * @param listaDadosFaixasTarifa
	 *            the lista dados faixas tarifa
	 * @param idTarifaVigencia
	 *            the id tarifa vigencia
	 * @return the dados tarifa
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DadosTarifa carregarDadosTarifaFaixaDesconto(List<DadosFaixasTarifa> listaDadosFaixasTarifa, Long idTarifaVigencia)
					throws GGASException;

	/**
	 * Método responsável por validar se os
	 * intervalos entre as faixas estão corretos.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @param validarValoresFaixas
	 *            the validar valores faixas
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	void validarFaixasTarifa(Collection<DadosFaixasTarifa> listaDadosFaixa, boolean validarValoresFaixas) throws NegocioException,
					FormatoInvalidoException;

	/**
	 * Método responsável por validar se existe
	 * mais de uma faixa com valor final zerado ou
	 * nulo.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarFaixasZeradasTarifa(List<DadosFaixasTarifa> listaDadosFaixa) throws GGASException;

	/**
	 * Método reponsável por listar todas as
	 * tarifas cadastradas.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TarifaVO> listarTarifaVO() throws NegocioException;

	/**
	 * Método responsável por gerar o relatorio
	 * das tarifas cadastradas.
	 * 
	 * @param formatoImpressao
	 *            the formato impressao
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] exportarRelatorioTarifasCadastradas(FormatoImpressao formatoImpressao) throws NegocioException;

	/**
	 * Método responsável por gerar o relatorio
	 * da tarifa.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] exportarRelatorioTarifa(FiltroTarifaVigenciaDescontos filtro) throws NegocioException;

	/**
	 * Método responsável por listar tarifas
	 * cadastradas.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TarifaVO> listarTarifaVigenciaVO(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as tarifas
	 * relacionadas a um ponto de consumo e a um
	 * item de fatura.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param chaveItemFatura
	 *            the chave item fatura
	 * @return List<Tarifa>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Tarifa> obterTarifas(Long chavePontoConsumo, Long chaveItemFatura) throws NegocioException;

	/**
	 * Verificar lista tarifa.
	 * 
	 * @param listaTarifas
	 *            the lista tarifas
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Tarifa> verificarListaTarifa(Collection<Tarifa> listaTarifas) throws NegocioException;

	/**
	 * Método responsável por listar as tarifas
	 * relacionadas a um ponto de consumo por ramo de atividade e a um
	 * item de fatura.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param chaveItemFatura
	 *            the chave item fatura
	 * @return List<Tarifa>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Tarifa> obterTarifasPorRamoAtividade(Long chavePontoConsumo, Long chaveItemFatura) throws NegocioException;

	/**
	 * Consultar tarifa vigencia anterior mais recente.
	 * 
	 * @param chaveTarifa
	 *            the chave tarifa
	 * @param dataVigencia
	 *            the data vigencia
	 * @return the tarifa vigencia
	 */
	TarifaVigencia consultarTarifaVigenciaAnteriorMaisRecente(long chaveTarifa, Date dataVigencia);

	/**
	 * 
	 * Verificar lista tarifa pendente.
	 * 
	 * @param listaTarifas the lista Tarifas
	 * @param dataInicioPeriodo Data Inicio Periodo
	 * @param dataFimPeriodo Data Fim Periodo
	 * 
	 * @return Coleção de Tarifas
	 * 
	 * @throws NegocioException the negocio exception
	 */
	Collection<Tarifa> verificarListaTarifaPendente(Collection<Tarifa> listaTarifas, Date dataInicioPeriodo, Date dataFimPeriodo)
			throws NegocioException;

	/**
	 * Calcular juros entre datas indice financeiro.
	 * 
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @param idIndice
	 *            the id indice
	 * @param valor
	 *            the valor
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal calcularJurosEntreDatasIndiceFinanceiro(Date dataInicio, Date dataFim, Long idIndice, BigDecimal valor)
					throws NegocioException;

	/**
	 * Listar tarifa vigencia por tarifa.
	 * 
	 * @param idTarifa
	 *            the id tarifa
	 * @return the collection
	 */
	public Collection<TarifaVigencia> listarTarifaVigenciaPorTarifa(Long idTarifa);

	/**
	 * Listar tarifas faixa desconto.
	 * 
	 * @param idTarifaVigenciaDesconto
	 *            the id tarifa vigencia desconto
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TarifaFaixaDesconto> listarTarifasFaixaDesconto(Long idTarifaVigenciaDesconto) throws NegocioException;

	/**
	 * Método responsável por obter {@link TarifaVigencia} por id de tarifa.
	 * @param idTarifa - {@link Long}
	 * @return Retorna uma Map de Tarifas buscado pelo idTarifa - {@link Map}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Map<String, Object> obterTarifasPelaVigencia(Long idTarifa) throws NegocioException;

	/**
	 * Método responsável por montar uma lista de {@link TarifaVigenciaDesconto} para inserção. 
	 * 
	 * @param tarifaVigencia - {@link TarifaVigencia}
	 * @return lista de {@link TarifaVigenciaDesconto} - {@link List}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	List<TarifaVigenciaDesconto> montarListaTarifaVigenciaDescontoNaInsercao(TarifaVigencia tarifaVigencia) throws ConcorrenciaException,
					NegocioException;
	
	/**
	 * Método responsável por montar uma lista de {@link TarifaVigenciaDesconto} para atualização.
	 * @param tarifaVigencia - {@link TarifaVigencia}
	 * @return lista de {@link TarifaVigenciaDesconto} - {@link List}
	 * @throws ConcorrenciaException - {@link ConcorrenciaException}
	 * @throws NegocioException - {@link NegocioException}
	 */
	List<TarifaVigenciaDesconto> montarListaTarifaVigenciaDescontoNaAtualizacao(TarifaVigencia tarifaVigencia)
					throws ConcorrenciaException, NegocioException;

	/**
	 * Conta a quantidade de entidades do tipo TarifaVigencia de acordo 
	 * com o filtro especificado.
	 * @param filtro the filtro
	 * @return quantidade de entidades de TarifaVigencia
	 * @throws NegocioException the Negocio Exception
	 */
	Long contarTarifasVigencia(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por converter uma lista de {@link TarifaVigenciaDesconto} 
	 * em uma coleção de {@link AcaoSincroniaDesconto}.
	 * 
	 * @param tarifaVigenciaModificada - {@link TarifaVigencia}
	 * @return coleção de {@link AcaoSincroniaDesconto} - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<AcaoSincroniaDesconto> acoesParaSincronizacao(TarifaVigencia tarifaVigenciaModificada) throws NegocioException;

	/**
	 * Consultar tarifas vigencia faixa.
	 *
	 * @param chaveTarifaVigencia the chave tarifa vigencia
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<TarifaVigenciaFaixa> consultarTarifasVigenciaFaixa(Long chaveTarifaVigencia) throws NegocioException;

	/**
	 * Consulta as faixas de tarifa vigentes a partir do ponto de consumo
	 * @param idPontoConsumo A chave primária do ponto de consumo
	 * @return A lista de TarifaVigenciaFaixa
	 */
	List<TarifaVigenciaFaixa> listarTarifaVigenciaFaixaPorPontoConsumo(Long idPontoConsumo);

	/**
	 * Obtém uma lista de chaves primárias de {@link Tarifa} por status e período.
	 * @param status - {@link Long}
	 * @param dataInicioPeriodo - {@link Date}
	 * @param dataFimPeriodo - {@link Date}
	 * @return lista de chaves - {@link List}
	 */
	List<Long> obterListaIdsTarifas(Long status, Date dataInicioPeriodo, Date dataFimPeriodo);
	
	/**
	 * Método responsável por trazer medida fim e o valor fixo de uma faixa de tarifa vigência
	 * 
	 * @param filtro {@link Map}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<TarifaVigenciaFaixa> consultarDadosTarifasVigenciaFaixa(Map<String, Object> filtro) throws NegocioException;
	
	/**
	 * Método responsável por trazer informarMsgIcmsSubstituido,
	 * baseCalculoIcmsSubstituido, icmsSubstituidoMetroCubico,
	 * mensagemIcmsSubstituto de uma Tarifa Vigência.
	 * 
	 * @param chaveTarifa  {@link Long}
	 * @param dataVigencia {@link Date}
	 * @return {@link TarifaVigencia}
	 */
	public TarifaVigencia consultarDadosTarifaVigenciaAnteriorMaisRecente(long chaveTarifa, Date dataVigencia);
}
