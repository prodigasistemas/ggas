/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Collection;

import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;

/**
 * RelatorioTarifaVigenciaVO para Relatorio Tarifa VigenciaVO
 *
 */
public class RelatorioTarifaVigenciaVO {

	private TarifaVigencia tarifaVigencia;

	private Collection<RelatorioTarifaVigenciaDescontoVO> tarifaVigenciaDescontos;

	/**
	 * Montar relatorio tarifa vigencia vo.
	 * 
	 * @param tarifaVigencia
	 *            the tarifa vigencia
	 * @param tarifaVigenciaDescontos
	 *            the tarifa vigencia descontos
	 * @return the relatorio tarifa vigencia vo
	 */
	public static RelatorioTarifaVigenciaVO montarRelatorioTarifaVigenciaVO(TarifaVigencia tarifaVigencia,
					Collection<RelatorioTarifaVigenciaDescontoVO> tarifaVigenciaDescontos) {

		RelatorioTarifaVigenciaVO relatorioTarifaVigenciaVO = new RelatorioTarifaVigenciaVO();
		relatorioTarifaVigenciaVO.setTarifaVigencia(tarifaVigencia);
		relatorioTarifaVigenciaVO.setTarifaVigenciaDescontos(tarifaVigenciaDescontos);
		return relatorioTarifaVigenciaVO;
	}

	public String getDataInicioVigenciaFormatada() {

		return tarifaVigencia.getDataVigenciaFormatada();
	}

	public String getDescricaoTipoCalculo() {

		return tarifaVigencia.getDescricaoTipoCalculo();
	}

	public String getDescricaoVolumeBaseCalculo() {

		return tarifaVigencia.getDescricaoBaseApuracao();
	}

	public String getDescricaoUnidadeMonetaria() {

		return tarifaVigencia.getDescricaoUnidadeMonetaria();
	}

	public String getDescricaoComentario() {

		return tarifaVigencia.getDescricaoComentario();
	}

	public String getDescricaoStatus() {

		return tarifaVigencia.getDescricaoStatus();
	}

	public Collection<TarifaVigenciaFaixa> getTarifaVigenciaFaixas() {

		return tarifaVigencia.getTarifaVigenciaFaixas();
	}

	/**
	 * @return the tarifaVigencia
	 */
	public TarifaVigencia getTarifaVigencia() {

		return tarifaVigencia;
	}

	/**
	 * @param tarifaVigencia
	 *            the tarifaVigencia to set
	 */
	public void setTarifaVigencia(TarifaVigencia tarifaVigencia) {

		this.tarifaVigencia = tarifaVigencia;
	}

	/**
	 * @return the tarifaVigenciaDescontos
	 */
	public Collection<RelatorioTarifaVigenciaDescontoVO> getTarifaVigenciaDescontos() {

		return tarifaVigenciaDescontos;
	}

	/**
	 * @param tarifaVigenciaDescontos
	 *            the tarifaVigenciaDescontos to set
	 */
	public void setTarifaVigenciaDescontos(Collection<RelatorioTarifaVigenciaDescontoVO> tarifaVigenciaDescontos) {

		this.tarifaVigenciaDescontos = tarifaVigenciaDescontos;
	}

}
