package br.com.ggas.faturamento.tarifa.impl;

/**
 * Classe responsável pela representação do filtro na tela de índice financeiro.
 * @author esantana
 *
 */
public class IndiceFinanceiroFormVO {

	
	private Integer mesInicio;
	private Integer mesFim;
	private Integer anoInicio;
	private Integer anoFim;
	private String dataInicio;
	private String dataFim;
	private String[] dataIndiceFinanceiro = new String[] {};
	private String[] valorNominal;
	private String[] indicadorUtilizado;
	private String[] valorIndice;
	private String[] mesIndiceFinanceiro;
	
	/**
	 * Obtém o Mês inicial
	 * 
	 * @return mês inicial - {@link Integer}
	 */
	public Integer getMesInicio() {
		return mesInicio;
	}
	
	/**
	 * Adiciona o Mês inicial
	 * @param mesInicio - {@link Integer}
	 */
	public void setMesInicio(Integer mesInicio) {
		this.mesInicio = mesInicio;
	}
	
	/**
	 * Obtém o Mês final
	 * @return mês final - {@link Integer}
	 */
	public Integer getMesFim() {
		return mesFim;
	}
	
	/**
	 * Adiciona o Mês final
	 * 
	 * @param mesFim - {@link Integer}
	 */
	public void setMesFim(Integer mesFim) {
		this.mesFim = mesFim;
	}
	
	/**
	 * Obtém o Ano inicial
	 * @return ano inicial - {@link Integer}
	 */
	public Integer getAnoInicio() {
		return anoInicio;
	}
	
	/**
	 * Adiciona o Ano inicial
	 * @param anoInicio - {@link Integer}
	 */
	public void setAnoInicio(Integer anoInicio) {
		this.anoInicio = anoInicio;
	}

	/**
	 * Obtém o Ano final
	 * @return ano final - {@link Integer}
	 */
	public Integer getAnoFim() {
		return anoFim;
	}
	
	/**
	 * Adiciona o Ano final
	 * 
	 * @param anoFim - {@link Integer}
	 */
	public void setAnoFim(Integer anoFim) {
		this.anoFim = anoFim;
	}
	
	/**
	 * Obtém a data inicial
	 * @return data inicial - {@link String}
	 */
	public String getDataInicio() {
		return dataInicio;
	}
	
	/**
	 * Adiciona a data inicial
	 * @param dataInicio - {@link String}
	 */
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	/**
	 * Obtém a data fim
	 * @return data final - {@link String}
	 */
	public String getDataFim() {
		return dataFim;
	}
	
	/**
	 * Adiciona a data final
	 * @param dataFim - {@link String}
	 */
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
	/**
	 * Obtém um array de datas do índice financeiro
	 * 
	 * @return array de datas do índice financeiro
	 */
	public String[] getDataIndiceFinanceiro() {
		String[] retorno = null;
		if(this.dataIndiceFinanceiro != null) {
			retorno = this.dataIndiceFinanceiro.clone();
		}
		return retorno;
	}
	
	/**
	 * Adiciona um array de datas do índice financeiro
	 * 
	 * @param dataIndiceFinanceiro
	 */
	public void setDataIndiceFinanceiro(String[] dataIndiceFinanceiro) {
		if(dataIndiceFinanceiro != null) {
			this.dataIndiceFinanceiro = dataIndiceFinanceiro.clone();
		}
	}
	
	/**
	 * Obtém um array de valores nominais
	 * 
	 * @return um array de valores nominais
	 */
	public String[] getValorNominal() {
		String[] retorno = null;
		if(this.valorNominal != null) {
			retorno = this.valorNominal.clone();
		}
		return retorno;
	}
	
	/**
	 * Adiciona um array de valores nominais
	 * 
	 * @param valorNominal
	 */
	public void setValorNominal(String[] valorNominal) {
		if(valorNominal != null) {
			this.valorNominal = valorNominal.clone();
		}
	}
	
	/**
	 * Obtém um array de indicadores utilizados.
	 * 
	 * @return array de indicadores utilizados
	 */
	public String[] getIndicadorUtilizado() {
		String[] retorno = null;
		if(this.indicadorUtilizado != null) {
			retorno = this.indicadorUtilizado.clone();
		}
		return retorno;
	}
	
	/**
	 * Adiciona um array de indicadores utilizados.
	 * 
	 * @param indicadorUtilizado
	 */
	public void setIndicadorUtilizado(String[] indicadorUtilizado) {
		if(indicadorUtilizado != null) {
			this.indicadorUtilizado = indicadorUtilizado.clone();
		}
	}
	
	/**
	 * Obtém um array de valores do índice financeiro.
	 * 
	 * @return array de valores do índice financeiro
	 */
	public String[] getValorIndice() {
		String[] retorno = null;
		if(this.valorIndice != null) {
			retorno = this.valorIndice.clone();
		}
		return retorno;
	}
	
	/**
	 * Adiciona um array de valores do índice de financeiro.
	 * 
	 * @param valorIndice
	 */
	public void setValorIndice(String[] valorIndice) {
		if(valorIndice != null) {
			this.valorIndice = valorIndice.clone();
		}
	}
	
	/**
	 * Obtém os meses do índice financeiro.
	 * 
	 * @return array de meses do índice financeiro 
	 */
	public String[] getMesIndiceFinanceiro() {
		String[] retorno = null;
		if(this.mesIndiceFinanceiro != null) {
			retorno = this.mesIndiceFinanceiro.clone();
		}
		return retorno;
	}
	
	/**
	 * Adiciona um array de meses do índice financeiro.
	 * 
	 * @param mesIndiceFinanceiro
	 */
	public void setMesIndiceFinanceiro(String[] mesIndiceFinanceiro) {
		if(mesIndiceFinanceiro != null) {
			this.mesIndiceFinanceiro = mesIndiceFinanceiro.clone();
		}
	}
}
