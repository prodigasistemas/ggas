package br.com.ggas.faturamento.tarifa;

import java.util.Date;

import br.com.ggas.faturamento.tarifa.impl.AcaoSincronia;

/**
 * The Interface AcaoSincroniaDesconto.
 */
public interface AcaoSincroniaDesconto {

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	Date getData();

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	void setData(Date data);

	/**
	 * Gets the acao sincronia.
	 *
	 * @return the acao sincronia
	 */
	AcaoSincronia getAcaoSincronia();

	/**
	 * Sets the acao sincronia.
	 *
	 * @param acao the new acao sincronia
	 */
	void setAcaoSincronia(AcaoSincronia acao);

	/**
	 * Gets the desconto.
	 *
	 * @return the desconto
	 */
	TarifaVigenciaDesconto getDesconto();

	/**
	 * Sets the desconto.
	 *
	 * @param desconto the new desconto
	 */
	void setDesconto(TarifaVigenciaDesconto desconto);

}
