/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos relacionados a Tarifa de Vigência
 * 
 */
public class TarifaVigenciaImpl extends EntidadeNegocioImpl implements TarifaVigencia {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6532384064291486637L;

	private Tarifa tarifa;

	@DateTimeFormat(pattern = Constantes.FORMATO_DATA_BR)
	private Date dataVigencia;

	private String comentario;

	private EntidadeConteudo unidadeMonetaria;

	private EntidadeConteudo tipoCalculo;

	private EntidadeConteudo baseApuracao;

	private Collection<TarifaVigenciaFaixa> tarifaVigenciaFaixas = new HashSet<TarifaVigenciaFaixa>();

	private EntidadeConteudo status;

	private Usuario ultimoUsuarioAlteracao;

	private Boolean informarMsgIcmsSubstituido;

	private BigDecimal baseCalculoIcmsSubstituido;

	private BigDecimal icmsSubstituidoMetroCubico;

	private String mensagemIcmsSubstituto;

	private Collection<TarifaVigenciaTributo> tributos = new HashSet<TarifaVigenciaTributo>();

	/**
	 * @return the tarifaVigenciaFaixas
	 */
	@Override
	public Collection<TarifaVigenciaFaixa> getTarifaVigenciaFaixas() {

		return tarifaVigenciaFaixas;
	}

	/**
	 * @param tarifaVigenciaFaixas
	 *            the tarifaVigenciaFaixas to set
	 */
	@Override
	public void setTarifaVigenciaFaixas(Collection<TarifaVigenciaFaixa> tarifaVigenciaFaixas) {

		this.tarifaVigenciaFaixas = tarifaVigenciaFaixas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getBaseApuracao()
	 */
	@Override
	public EntidadeConteudo getBaseApuracao() {

		return baseApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #getDescricaoBaseApuracao()
	 */
	@Override
	public String getDescricaoBaseApuracao() {

		if(baseApuracao != null) {
			return baseApuracao.getDescricao();
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #setBaseApuracao(br.com.ggas.geral
	 * .EntidadeConteudo)
	 */
	@Override
	public void setBaseApuracao(EntidadeConteudo baseApuracao) {

		this.baseApuracao = baseApuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getUnidadeMonetaria()
	 */
	@Override
	public EntidadeConteudo getUnidadeMonetaria() {

		return unidadeMonetaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #getDescricaoUnidadeMonetaria()
	 */
	@Override
	public String getDescricaoUnidadeMonetaria() {

		if(unidadeMonetaria != null) {
			return unidadeMonetaria.getDescricao();
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #setUnidadeMonetaria(br.com.ggas
	 * .geral.EntidadeConteudo)
	 */
	@Override
	public void setUnidadeMonetaria(EntidadeConteudo unidadeMonetaria) {

		this.unidadeMonetaria = unidadeMonetaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getTipoCalculo()
	 */
	@Override
	public EntidadeConteudo getTipoCalculo() {

		return tipoCalculo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #getDescricaoTipoCalculo()
	 */
	@Override
	public String getDescricaoTipoCalculo() {

		if(tipoCalculo != null) {
			return tipoCalculo.getDescricao();
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #setTipoCalculo(br.com.ggas.geral
	 * .EntidadeConteudo)
	 */
	@Override
	public void setTipoCalculo(EntidadeConteudo tipoCalculo) {

		this.tipoCalculo = tipoCalculo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getTarifa()
	 */
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getComentario()
	 */
	@Override
	public String getComentario() {

		return comentario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getDescricaoComentario()
	 */
	@Override
	public String getDescricaoComentario() {

		if(comentario != null) {
			return comentario;
		}
		return "";
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #setComentario(java.lang.String)
	 */
	@Override
	public void setComentario(String comentario) {

		this.comentario = comentario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #setTarifa(br.com.ggas.faturamento
	 * .tarifa.Tarifa)
	 */
	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia#getDataVigencia()
	 */
	@Override
	public Date getDataVigencia() {
		Date data = null;
		if (this.dataVigencia != null) {
			data = (Date) dataVigencia.clone();
		}
		return data;
	}

	@Override
	public String getDataVigenciaFormatada() {

		StringBuilder vigencias = new StringBuilder();
		if(dataVigencia != null) {
			vigencias.append(Util.converterDataParaStringSemHora(dataVigencia, Constantes.FORMATO_DATA_BR));
		}
		return vigencias.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.tarifa.impl. TarifaVigencia
	 * #setDataVigencia(java.util.Date)
	 */
	@Override
	public void setDataVigencia(Date dataVigencia) {
		if (dataVigencia != null) {
			this.dataVigencia = (Date) dataVigencia.clone();
		} else {
			this.dataVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.Tarifa#getStatus
	 * ()
	 */
	@Override
	public EntidadeConteudo getStatus() {

		return status;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigencia
	 * #getDescricaoStatus()
	 */
	@Override
	public String getDescricaoStatus() {

		if(status != null) {
			return status.getDescricao();
		} else {
			return "";
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.Tarifa#setStatus
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	@Override
	public Usuario getUltimoUsuarioAlteracao() {

		return ultimoUsuarioAlteracao;
	}

	@Override
	public void setUltimoUsuarioAlteracao(Usuario ultimoUsuarioAlteracao) {

		this.ultimoUsuarioAlteracao = ultimoUsuarioAlteracao;
	}

	@Override
	public Boolean getInformarMsgIcmsSubstituido() {

		return informarMsgIcmsSubstituido;
	}

	@Override
	public void setInformarMsgIcmsSubstituido(Boolean informarMsgIcmsSubstituido) {

		this.informarMsgIcmsSubstituido = informarMsgIcmsSubstituido;
	}

	@Override
	public BigDecimal getBaseCalculoIcmsSubstituido() {

		return baseCalculoIcmsSubstituido;
	}

	@Override
	public void setBaseCalculoIcmsSubstituido(BigDecimal baseCalculoIcmsSubstituido) {

		this.baseCalculoIcmsSubstituido = baseCalculoIcmsSubstituido;
	}

	@Override
	public BigDecimal getIcmsSubstituidoMetroCubico() {

		return icmsSubstituidoMetroCubico;
	}

	@Override
	public void setIcmsSubstituidoMetroCubico(BigDecimal icmsSubstituidoMetroCubico) {

		this.icmsSubstituidoMetroCubico = icmsSubstituidoMetroCubico;
	}

	@Override
	public String getMensagemIcmsSubstituto() {

		return mensagemIcmsSubstituto;
	}

	@Override
	public void setMensagemIcmsSubstituto(String mensagemIcmsSubstituto) {

		this.mensagemIcmsSubstituto = mensagemIcmsSubstituto;
	}

	/**
	 * @return the tributos
	 */
	@Override
	public Collection<TarifaVigenciaTributo> getTributos() {

		return tributos;
	}

	/**
	 * @param tributos
	 *            the tributos to set
	 */
	@Override
	public void setTributos(Collection<TarifaVigenciaTributo> tributos) {

		this.tributos = tributos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tarifa == null) {
			stringBuilder.append(TARIFA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataVigencia == null) {
			stringBuilder.append(DATA_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoCalculo == null) {
			stringBuilder.append(TIPO_CALCULO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(baseApuracao == null) {
			stringBuilder.append(BASE_APURACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(unidadeMonetaria == null) {
			stringBuilder.append(UNIDADE_MONETARIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(comentario)) {
			stringBuilder.append(OBSERVACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
