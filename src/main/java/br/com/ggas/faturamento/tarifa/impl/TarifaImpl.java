/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por implementar os métodos relacionados a Tarifa 
 * 
 */
public class TarifaImpl extends EntidadeNegocioImpl implements Tarifa {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6532384064291486637L;

	private EntidadeConteudo itemFatura;

	private String descricao;

	private String descricaoAbreviada;

	private Segmento segmento;

	private RamoAtividade ramoAtividade;

	private BigDecimal quantidadeCasaDecimal;

	private EntidadeConteudo tipoContrato;

	/**
	 * @return the ramoAtividade
	 */
	@Override
	public RamoAtividade getRamoAtividade() {

		return ramoAtividade;
	}

	/**
	 * @param ramoAtividade
	 *            the ramoAtividade to set
	 */
	@Override
	public void setRamoAtividade(RamoAtividade ramoAtividade) {

		this.ramoAtividade = ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getItemFatura()
	 */
	@Override
	public EntidadeConteudo getItemFatura() {

		return itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setItemFatura
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setItemFatura(EntidadeConteudo itemFatura) {

		this.itemFatura = itemFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setSegmento
	 * (br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getQuantidadeCasaDecimal()
	 */
	@Override
	public BigDecimal getQuantidadeCasaDecimal() {

		return quantidadeCasaDecimal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setQuantidadeCasaDecimal
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setQuantidadeCasaDecimal(BigDecimal quantidadeCasaDecimal) {

		this.quantidadeCasaDecimal = quantidadeCasaDecimal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(TARIFA_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricaoAbreviada)) {
			stringBuilder.append(TARIFA_DESCRICAO_ABREVIADA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoContrato == null) {
			stringBuilder.append(TARIFA_TIPO_CONTRATO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(itemFatura == null) {
			stringBuilder.append(TARIFA_ITEM_FATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(segmento == null) {
			stringBuilder.append(TARIFA_SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(quantidadeCasaDecimal == null) {
			stringBuilder.append(TARIFA_QUANTIDADE_CASA_DECIMAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	@Override
	public EntidadeConteudo getTipoContrato() {

		return tipoContrato;
	}

	@Override
	public void setTipoContrato(EntidadeConteudo tipoContrato) {

		this.tipoContrato = tipoContrato;
	}

}
