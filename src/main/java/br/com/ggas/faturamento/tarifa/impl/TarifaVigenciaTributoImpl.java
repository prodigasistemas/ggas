/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por implementar os métodos relacionados 
 * ao Tributo da Tarifa de Vigencia
 * 
 */
public class TarifaVigenciaTributoImpl extends EntidadeNegocioImpl implements TarifaVigenciaTributo {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6532384064291486637L;

	private TarifaVigencia tarifaVigencia;

	private Tributo tributo;

	private Date dataVigenciaInicial;

	private Date dataVigenciaFinal;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #getDataVigenciaInicial()
	 */
	@Override
	public Date getDataVigenciaInicial() {
		Date data = null;
		if (this.dataVigenciaInicial != null) {
			data = (Date) dataVigenciaInicial.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #setDataVigenciaInicial(java.util.Date)
	 */
	@Override
	public void setDataVigenciaInicial(Date dataVigenciaInicial) {
		if (dataVigenciaInicial != null) {
			this.dataVigenciaInicial = (Date) dataVigenciaInicial.clone();
		} else {
			this.dataVigenciaInicial = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.tarifa.TarifaTributo #getDataVigenciaFinal()
	 */
	@Override
	public Date getDataVigenciaFinal() {
		Date data = null;
		if (dataVigenciaFinal != null) {
			data = (Date) dataVigenciaFinal.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #setDataVigenciaFinal(java.util.Date)
	 */
	@Override
	public void setDataVigenciaFinal(Date dataVigenciaFinal) {
		if(dataVigenciaFinal != null) {
			this.dataVigenciaFinal = (Date) dataVigenciaFinal.clone();
		} else {
			this.dataVigenciaFinal = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #getTarifa()
	 */
	@Override
	public TarifaVigencia getTarifaVigencia() {

		return tarifaVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #
	 * setTarifa(br.com.ggas.faturamento.tarifa.Tarifa
	 * )
	 */
	@Override
	public void setTarifaVigencia(TarifaVigencia tarifaVigencia) {

		this.tarifaVigencia = tarifaVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #getTributo()
	 */
	@Override
	public Tributo getTributo() {

		return tributo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.TarifaTributo
	 * #
	 * setTributo(br.com.ggas.faturamento.tributo.
	 * Tributo)
	 */
	@Override
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tarifaVigencia == null) {
			stringBuilder.append(TARIFA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tributo == null) {
			stringBuilder.append(TRIBUTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataVigenciaInicial == null) {
			stringBuilder.append(DATA_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
