/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.tarifa.TarifaFaixaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaFaixa;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;

/**
 * TarifaFaixaDescontoImpl
 *
 */
class TarifaFaixaDescontoImpl extends EntidadeNegocioImpl implements TarifaFaixaDesconto {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final int PERCENTUAL = 100;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -6532384064291486637L;

	private TarifaVigenciaFaixa tarifaVigenciaFaixa;

	private TarifaVigenciaDesconto tarifaVigenciaDesconto;

	private BigDecimal valorDescontoFixo;

	private BigDecimal percentualDescontoFixo;

	private BigDecimal valorDescontoVariavel;

	private BigDecimal percentualDescontoVariavel;

	@Override
	public String getValoresFaixaInicioFim() {

		String faixaInicio = this.getTarifaVigenciaFaixa().getMedidaInicio().toString();
		String faixaFim = this.getTarifaVigenciaFaixa().getMedidaFim().toString();

		StringBuilder builder = new StringBuilder();
		builder.append(faixaInicio);
		builder.append(" - ");
		builder.append(faixaFim);
		return builder.toString();
	}

	@Override
	public String getValorFixoVigenciaFaixaFormatado() {

		return this.getTarifaVigenciaFaixa().getValorFixoFormatado();
	}

	@Override
	public String getValorVariavelVigenciaFaixaFormatado() {

		return this.getTarifaVigenciaFaixa().getValorVariavelFormatado();
	}

	@Override
	public BigDecimal getValorFixo() {

		BigDecimal valorFixo = BigDecimal.ZERO;
		if(this.valorDescontoFixo != null) {
			valorFixo = this.valorDescontoFixo;
		} else if(this.percentualDescontoFixo != null) {
			valorFixo = this.percentualDescontoFixo.multiply(BigDecimal.valueOf(PERCENTUAL));
		}

		return valorFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorFixoFormatado()
	 */
	@Override
	public String getValorFixoFormatado() {

		Integer quantidadeCasasDecimais = this.getTarifaVigenciaFaixa().getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal()
						.intValue();
		return NumeroUtil.converterCampoValorDecimalParaString(this.getValorFixo(), quantidadeCasasDecimais);
	}

	@Override
	public BigDecimal getValorVariavel() {

		BigDecimal valorVariavel = BigDecimal.ZERO;
		if(this.valorDescontoVariavel != null) {
			valorVariavel = this.valorDescontoVariavel;
		} else if(this.percentualDescontoVariavel != null) {
			valorVariavel = this.percentualDescontoVariavel.multiply(BigDecimal.valueOf(PERCENTUAL));
		}

		return valorVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaFaixa#getValorVariavelFormatado()
	 */
	@Override
	public String getValorVariavelFormatado() {

		Integer quantidadeCasasDecimais = this.getTarifaVigenciaFaixa().getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal()
						.intValue();
		return NumeroUtil.converterCampoValorDecimalParaString(this.getValorVariavel(), quantidadeCasasDecimais);
	}

	/**
	 * @return the tarifaVigenciaFaixa
	 */
	@Override
	public TarifaVigenciaFaixa getTarifaVigenciaFaixa() {

		return tarifaVigenciaFaixa;
	}

	/**
	 * @param tarifaVigenciaFaixa
	 *            the tarifaVigenciaFaixa to set
	 */
	@Override
	public void setTarifaVigenciaFaixa(TarifaVigenciaFaixa tarifaVigenciaFaixa) {

		this.tarifaVigenciaFaixa = tarifaVigenciaFaixa;
	}

	/**
	 * @return the tarifaVigenciaDesconto
	 */
	@Override
	public TarifaVigenciaDesconto getTarifaVigenciaDesconto() {

		return tarifaVigenciaDesconto;
	}

	/**
	 * @param tarifaVigenciaDesconto
	 *            the tarifaVigenciaDesconto to
	 *            set
	 */
	@Override
	public void setTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto) {

		this.tarifaVigenciaDesconto = tarifaVigenciaDesconto;
	}

	/**
	 * @return the valorDescontoFixo
	 */
	@Override
	public BigDecimal getValorDescontoFixo() {

		return valorDescontoFixo;
	}

	/**
	 * @param valorDescontoFixo
	 *            the valorDescontoFixo to set
	 */
	@Override
	public void setValorDescontoFixo(BigDecimal valorDescontoFixo) {

		this.valorDescontoFixo = valorDescontoFixo;
	}

	/**
	 * @return the percentualDescontoFixo
	 */
	@Override
	public BigDecimal getPercentualDescontoFixo() {

		return percentualDescontoFixo;
	}

	/**
	 * @param percentualDescontoFixo
	 *            the percentualDescontoFixo to
	 *            set
	 */
	@Override
	public void setPercentualDescontoFixo(BigDecimal percentualDescontoFixo) {

		this.percentualDescontoFixo = percentualDescontoFixo;
	}

	/**
	 * @return the valorDescontoVariavel
	 */
	@Override
	public BigDecimal getValorDescontoVariavel() {

		return valorDescontoVariavel;
	}

	/**
	 * @param valorDescontoVariavel
	 *            the valorDescontoVariavel to set
	 */
	@Override
	public void setValorDescontoVariavel(BigDecimal valorDescontoVariavel) {

		this.valorDescontoVariavel = valorDescontoVariavel;
	}

	/**
	 * @return the percentualDescontoVariavel
	 */
	@Override
	public BigDecimal getPercentualDescontoVariavel() {

		return percentualDescontoVariavel;
	}

	/**
	 * @param percentualDescontoVariavel
	 *            the percentualDescontoVariavel
	 *            to set
	 */
	@Override
	public void setPercentualDescontoVariavel(BigDecimal percentualDescontoVariavel) {

		this.percentualDescontoVariavel = percentualDescontoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaFaixaDesconto
	 * #getValorLiquidoSemImposto()
	 */
	@Override
	public BigDecimal getValorLiquidoSemImpostoFixo() {

		BigDecimal retorno = BigDecimal.ZERO;
		if(tarifaVigenciaFaixa != null && tarifaVigenciaFaixa.getValorFixo() != null) {
			if(valorDescontoFixo != null) {
				retorno = tarifaVigenciaFaixa.getValorFixo().subtract(valorDescontoFixo);
			} else if(percentualDescontoFixo != null) {
				retorno = tarifaVigenciaFaixa.getValorFixo().subtract(tarifaVigenciaFaixa.getValorFixo().multiply(percentualDescontoFixo));
			} else {
				retorno = tarifaVigenciaFaixa.getValorFixo();
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaFaixaDesconto
	 * #getValorLiquidoSemImpostoFixoFormatado()
	 */
	@Override
	public String getValorLiquidoSemImpostoFixoFormatado() {

		Integer quantidadeCasasDecimais = this.getTarifaVigenciaFaixa().getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal()
						.intValue();
		return NumeroUtil.converterCampoValorDecimalParaString(this.getValorLiquidoSemImpostoFixo(), quantidadeCasasDecimais);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaFaixaDesconto
	 * #getValorLiquidoSemImpostoVariavel()
	 */
	@Override
	public BigDecimal getValorLiquidoSemImpostoVariavel() {

		BigDecimal retorno = BigDecimal.ZERO;
		if(tarifaVigenciaFaixa != null && tarifaVigenciaFaixa.getValorVariavel() != null) {
			if(valorDescontoVariavel != null) {
				retorno = tarifaVigenciaFaixa.getValorVariavel().subtract(valorDescontoVariavel);
			} else if(percentualDescontoVariavel != null) {
				retorno = tarifaVigenciaFaixa.getValorVariavel().subtract(
								tarifaVigenciaFaixa.getValorVariavel().multiply(percentualDescontoVariavel));
			} else {
				retorno = tarifaVigenciaFaixa.getValorVariavel();
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaFaixaDesconto
	 * #getValorLiquidoSemImpostoVariavelFormatado()
	 */
	@Override
	public String getValorLiquidoSemImpostoVariavelFormatado() {

		Integer quantidadeCasasDecimais = this.getTarifaVigenciaFaixa().getTarifaVigencia().getTarifa().getQuantidadeCasaDecimal()
						.intValue();
		return NumeroUtil.converterCampoValorDecimalParaString(this.getValorLiquidoSemImpostoVariavel(), quantidadeCasasDecimais);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tarifaVigenciaFaixa == null) {
			stringBuilder.append(TARIFA_FAIXA_DESCONTO_TARIFA_VIGENCIA_FAIXA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tarifaVigenciaDesconto == null) {
			stringBuilder.append(TARIFA_FAIXA_DESCONTO_TARIFA_VIGENCIA_DESCONTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
