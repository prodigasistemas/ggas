/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Indice Financeiro
 *
 */
public interface IndiceFinanceiro extends EntidadeNegocio {

	String BEAN_ID_INDICE_FINANCEIRO = "indiceFinanceiro";

	String INDICE_FINANCEIRO = "ROTULO_INDICE_FINANCEIRO";

	String PERIODICIDADE_MENSAL = "ROTULO_INDICE_PERIODICIDADE_MENSAL";

	String PERIODICIDADE_DIARIA = "ROTULO_INDICE_PERIODICIDADE_DIARIA";

	String TIPO_VALOR = "ROTULO_INDICE_TIPO_VALOR";

	String TIPO_PERCENTUAL = "ROTULO_INDICE_TIPO_PERCENTUAL";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * @param descricaoAbreviada
	 *            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * @return the indicadorPermiteNegativo
	 */
	boolean isIndicadorPermiteNegativo();

	/**
	 * @param indicadorPermiteNegativo
	 *            the indicadorPermiteNegativo to
	 *            set
	 */
	void setIndicadorPermiteNegativo(boolean indicadorPermiteNegativo);

	/**
	 * @return the indicadorMensal
	 */
	boolean isIndicadorMensal();

	/**
	 * @param indicadorMensal
	 *            the indicadorMensal to set
	 */
	void setIndicadorMensal(boolean indicadorMensal);

	/**
	 * @return the indicadorValorPercentual
	 */
	boolean isIndicadorValorPercentual();

	/**
	 * @param indicadorValorPercentual
	 *            the indicadorValorPercentual to
	 *            set
	 */
	void setIndicadorValorPercentual(boolean indicadorValorPercentual);
}
