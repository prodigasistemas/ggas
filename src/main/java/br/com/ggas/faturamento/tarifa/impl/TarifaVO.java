/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelas tranferências de dados relacionados a tarifa.
 *
 */
public class TarifaVO {

	private Tarifa tarifa;

	private Date dataInicioVigencia;

	private Date dataFinalVigencia;

	public Tarifa getTarifa() {

		return tarifa;
	}

	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	public Date getDataInicioVigencia() {
		Date data = null;
		if (this.dataInicioVigencia != null) {
			data = (Date) dataInicioVigencia.clone();
		}
		return data;
	}

	public void setDataInicioVigencia(Date dataInicioVigencia) {
		if (dataInicioVigencia != null) {
			this.dataInicioVigencia = (Date) dataInicioVigencia.clone();
		} else {
			this.dataInicioVigencia = null;
		}
	}

	public Date getDataFinalVigencia() {
		Date data = null;
		if (this.dataFinalVigencia != null) {
			data = (Date) dataFinalVigencia.clone();
		}
		return data;
	}

	public void setDataFinalVigencia(Date dataFinalVigencia) {
		if (dataFinalVigencia != null) {
			this.dataFinalVigencia = (Date) dataFinalVigencia.clone();
		} else {
			this.dataFinalVigencia = null;
		}
	}

	public String getDescricaoTarifa() {

		if(tarifa != null) {
			return tarifa.getDescricao();
		} else {
			return "";
		}

	}

	public String getDescricaoSegmentoTarifa() {

		if(tarifa != null && tarifa.getSegmento() != null) {
			return tarifa.getSegmento().getDescricao();
		} else {
			return "";
		}

	}

	public String getDataInicioVigenciaFormatada() {

		if(dataInicioVigencia != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			return df.format(dataInicioVigencia);
		} else {
			return "";
		}

	}

	public String getDataFinalVigenciaFormatada() {

		if(dataFinalVigencia != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			return df.format(dataFinalVigencia);
		} else {
			return "";
		}

	}

}
