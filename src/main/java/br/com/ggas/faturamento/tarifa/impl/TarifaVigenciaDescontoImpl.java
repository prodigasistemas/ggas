/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pela representação de um desconto aplicado a uma tarifa.
 * 
 */
public class TarifaVigenciaDescontoImpl extends EntidadeNegocioImpl implements TarifaVigenciaDesconto {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -1271519948961972772L;
	
	@DateTimeFormat(pattern = Constantes.FORMATO_DATA_BR)
	private Date inicioVigencia;
	
	@DateTimeFormat(pattern = Constantes.FORMATO_DATA_BR)
	private Date fimVigencia;

	private String descricaoDescontoVigencia;

	private boolean impressaoDescricao;

	private boolean impressaoDesconto;

	private Tarifa tarifa;

	private TarifaVigencia tarifaVigencia;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto#getTarifa()
	 */
	@Override
	public Tarifa getTarifa() {

		return tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #setTarifa(br.com.ggas
	 * .faturamento.tarifa.Tarifa)
	 */
	@Override
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaDesconto#getTarifaVigencia()
	 */
	@Override
	public TarifaVigencia getTarifaVigencia() {

		return tarifaVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaDesconto
	 * #setTarifaVigencia(br.
	 * com.ggas.faturamento.tarifa.TarifaVigencia)
	 */
	@Override
	public void setTarifaVigencia(TarifaVigencia tarifaVigencia) {

		this.tarifaVigencia = tarifaVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto#getIniciovigencia()
	 */
	@Override
	public Date getInicioVigencia() {
		Date retorno = null;
		if(this.inicioVigencia != null) {
			retorno = (Date) this.inicioVigencia.clone();
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto#getIniciovigencia()
	 */
	@Override
	public String getInicioVigenciaFormatado() {

		return Util.converterDataParaStringSemHora(inicioVigencia, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #setIniciovigencia(java.util.Date)
	 */
	@Override
	public void setInicioVigencia(Date inicioVigencia) {
		if(inicioVigencia != null) {
			this.inicioVigencia = (Date) inicioVigencia.clone();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto#getFimvigencia()
	 */
	@Override
	public Date getFimVigencia() {
		Date retorno = null;
		if(this.fimVigencia != null) {
			retorno = (Date) this.fimVigencia.clone();
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto#getFimvigencia()
	 */
	@Override
	public String getFimVigenciaFormatado() {

		return Util.converterDataParaStringSemHora(fimVigencia, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #setFimvigencia(java.util.Date)
	 */
	@Override
	public void setFimVigencia(Date fimVigencia) {
		if(fimVigencia != null) {
			this.fimVigencia = (Date) fimVigencia.clone();
		}
	}

	@Override
	public String getVigenciasFormatadas() {

		StringBuilder vigencias = new StringBuilder();
		if((inicioVigencia != null) && (fimVigencia != null)) {
			vigencias.append(Util.converterDataParaStringSemHora(inicioVigencia, Constantes.FORMATO_DATA_BR));
			vigencias.append(" - ");
			vigencias.append(Util.converterDataParaStringSemHora(fimVigencia, Constantes.FORMATO_DATA_BR));
		}
		return vigencias.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto#getDescricao()
	 */
	@Override
	public String getDescricaoDescontoVigencia() {

		return descricaoDescontoVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricaoDescontoVigencia(String descricao) {

		this.descricaoDescontoVigencia = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #getImpressaodescricao()
	 */
	@Override
	public boolean getImpressaoDescricao() {

		return impressaoDescricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #setImpressaodescricao
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setImpressaoDescricao(boolean impressaoDescricao) {

		this.impressaoDescricao = impressaoDescricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #getImpressaodesconto()
	 */
	@Override
	public boolean getImpressaoDesconto() {

		return impressaoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * TarifaVigenciaDesconto
	 * #setImpressaodesconto(java.math.BigDecimal)
	 */
	@Override
	public void setImpressaoDesconto(boolean impressaoDesconto) {

		this.impressaoDesconto = impressaoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(inicioVigencia == null) {
			stringBuilder.append(TARIFA_VIGENCIA_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(fimVigencia == null) {
			stringBuilder.append(TARIFA_VIGENCIA_FIM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricaoDescontoVigencia)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tarifa == null) {
			stringBuilder.append(TARIFA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
