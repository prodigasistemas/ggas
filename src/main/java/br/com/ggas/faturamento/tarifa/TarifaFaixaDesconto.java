/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface relacionada as faixas das tarifas de descontos 
 *
 */
public interface TarifaFaixaDesconto extends EntidadeNegocio {

	String BEAN_ID_TARIFA_FAIXA_DESCONTO = "tarifaFaixaDesconto";

	String TARIFA_FAIXA_DESCONTO = "TARIFA_FAIXA_DESCONTO";

	String TARIFAS_FAIXA_DESCONTO = "TARIFAS_FAIXA_DESCONTO";

	String TARIFA_FAIXA_DESCONTO_TARIFA_VIGENCIA_DESCONTO = "TARIFA_FAIXA_DESCONTO_TARIFA_VIGENCIA_DESCONTO";

	String TARIFA_FAIXA_DESCONTO_TARIFA_VIGENCIA_FAIXA = "TARIFA_FAIXA_DESCONTO_TARIFA_VIGENCIA_FAIXA";

	String TARIFA_FAIXA_DESCONTO_VALOR_FIXO = "TARIFA_FAIXA_DESCONTO_VALOR_FIXO";

	String TARIFA_FAIXA_DESCONTO_VALOR_VARIAVEL = "TARIFA_FAIXA_DESCONTO_VALOR_VARIAVEL";

	/**
	 * @return O valor das faixas inicio e fim da
	 *         vigencia concatenados
	 */
	String getValoresFaixaInicioFim();

	/**
	 * @return O valor fixo da faixa da vigencia
	 *         formatado
	 */
	String getValorFixoVigenciaFaixaFormatado();

	/**
	 * @return O valor variável da faixa da vigencia
	 *         formatado
	 */
	String getValorVariavelVigenciaFaixaFormatado();

	/**
	 * @return o valor ou o percentual de desconto
	 *         fixo
	 */
	BigDecimal getValorFixo();

	/**
	 * @return o valor ou o percentual de desconto
	 *         fixo formatado
	 */
	String getValorFixoFormatado();

	/**
	 * @return o valor ou o percentual de desconto
	 *         variável
	 */
	BigDecimal getValorVariavel();

	/**
	 * @return o valor ou o percentual de desconto
	 *         variável formatado
	 */
	String getValorVariavelFormatado();

	/**
	 * @return the tarifaVigenciaFaixa
	 */
	TarifaVigenciaFaixa getTarifaVigenciaFaixa();

	/**
	 * @param tarifaVigenciaFaixa
	 *            the tarifaVigenciaFaixa to set
	 */
	void setTarifaVigenciaFaixa(TarifaVigenciaFaixa tarifaVigenciaFaixa);

	/**
	 * @return the tarifaVigenciaDesconto
	 */
	TarifaVigenciaDesconto getTarifaVigenciaDesconto();

	/**
	 * @param tarifaVigenciaDesconto
	 *            the tarifaVigenciaDesconto to
	 *            set
	 */
	void setTarifaVigenciaDesconto(TarifaVigenciaDesconto tarifaVigenciaDesconto);

	/**
	 * @return the valorDescontoFixo
	 */
	BigDecimal getValorDescontoFixo();

	/**
	 * @param valorDescontoFixo
	 *            the valorDescontoFixo to set
	 */
	void setValorDescontoFixo(BigDecimal valorDescontoFixo);

	/**
	 * @return the percentualDescontoFixo
	 */
	BigDecimal getPercentualDescontoFixo();

	/**
	 * @param percentualDescontoFixo
	 *            the percentualDescontoFixo to
	 *            set
	 */
	void setPercentualDescontoFixo(BigDecimal percentualDescontoFixo);

	/**
	 * @return the valorDescontoVariavel
	 */
	BigDecimal getValorDescontoVariavel();

	/**
	 * @param valorDescontoVariavel
	 *            the valorDescontoVariavel to set
	 */
	void setValorDescontoVariavel(BigDecimal valorDescontoVariavel);

	/**
	 * @return the percentualDescontoVariavel
	 */
	BigDecimal getPercentualDescontoVariavel();

	/**
	 * @param percentualDescontoVariavel
	 *            the percentualDescontoVariavel
	 *            to set
	 */
	void setPercentualDescontoVariavel(BigDecimal percentualDescontoVariavel);

	/**
	 * @return O valor Líquido sem imposto fixo.
	 */
	BigDecimal getValorLiquidoSemImpostoFixo();

	/**
	 * @return O valor Líquido sem imposto fixo formatado.
	 */
	String getValorLiquidoSemImpostoFixoFormatado();

	/**
	 * @return O valor líquido sem imposto
	 *         variável.
	 */
	BigDecimal getValorLiquidoSemImpostoVariavel();

	/**
	 * @return O valor líquido sem imposto
	 *         variável formatado.
	 */
	String getValorLiquidoSemImpostoVariavelFormatado();

}
