/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao controlador de Preço de gás
 * 
 * 
 */
public interface ControladorPrecoGas extends ControladorNegocio {

	String ERRO_PRECO_GAS_ITEM_ITEM_FATURA_ADICIONADO = "ERRO_PRECO_GAS_ITEM_ITEM_FATURA_ADICIONADO";

	String ERRO_PRECO_GAS_ITEM = "ERRO_PRECO_GAS_ITEM";

	String ERRO_DATA_PERIODO_EMISSAO = "ERRO_DATA_PERIODO_EMISSAO";

	String ERRO_PRECO_GAS_DATA_FORNECIMENTO_INICIO_MAIOR_FIM = "ERRO_PRECO_GAS_DATA_FORNECIMENTO_INICIO_MAIOR_FIM";

	String ERRO_PRECO_GAS_NOTA_FISCAL_REGISTRADA = "ERRO_PRECO_GAS_NOTA_FISCAL_REGISTRADA";

	String ERRO_DATA_EMISSAO_NOTA_FISCAL_MENOR_ATUAL = "ERRO_DATA_EMISSAO_NOTA_FISCAL_MENOR_ATUAL";

	String CHAVE_PRIMARIA_INVERSA = "chavePrimariaInversa";

	String HABILITADO = "habilitado";

	String TRUE = "true";

	String BEAN_ID_CONTROLADOR_PRECO_GAS = "controladorPrecoGas";

	String PERIODO_EMISSAO_INICIAL = "PERIODO_EMISSAO_INICIAL_PRECO_GAS";

	String PERIODO_EMISSAO_FINAL = "PERIODO_EMISSAO_FINAL_PRECO_GAS";

	String ERRO_NEGOCIO_DATA_INICIO_MAIOR_DATA_FIM = "ERRO_NEGOCIO_DATA_INICIO_MAIOR_DATA_FIM";

	String ERRO_NEGOCIO_DATA_EMISSAO_NOTA_FISCAL_MAIOR_DATA_FIM = "ERRO_NEGOCIO_DATA_EMISSAO_NOTA_FISCAL_MAIOR_DATA_FIM";

	String ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MAIOR_DATA_FIM = "ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MAIOR_DATA_FIM";

	String ERRO_NEGOCIO_DATA_EMISSAO_NOTA_FISCAL_MENOR_DATA_FIM = "ERRO_NEGOCIO_DATA_EMISSAO_NOTA_FISCAL_MENOR_DATA_FIM";

	String ERRO_PRECO_GAS_OBSERVACAO_MAIOR_200 = "ERRO_PRECO_GAS_OBSERVACAO_MAIOR_200";

	String ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MENOR_DATA_FIM = "ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MENOR_DATA_FIM";

	String ERRO_PRECO_GAS_COMPLEMENTAR_PAI = "ERRO_PRECO_GAS_COMPLEMENTAR_PAI";

	String ERRO_PRECO_GAS_ITEM_PRECO_NAO_ENCONTRADO_COMPLEMENTAR = "ERRO_PRECO_GAS_ITEM_PRECO_NAO_ENCONTRADO_COMPLEMENTAR";

	String ERRO_PRECO_GAS_ITEM_PRECO_GAS_COMPLEMENTAR = "ERRO_PRECO_GAS_ITEM_PRECO_GAS_COMPLEMENTAR";

	String ERRO_PRECO_GAS_ITEM_VOLUME = "ERRO_PRECO_GAS_ITEM_VOLUME";

	String ERRO_PRECO_ITEM_GAS_VOLUME_DIFERENTE = "ERRO_PRECO_ITEM_GAS_VOLUME_DIFERENTE";

	String ERRO_PRECO_GAS_SEM_ITENS = "ERRO_PRECO_GAS_SEM_ITENS";

	String ERRO_SOMATORIO_PRECO_GAS_MENOR_ZERO = "ERRO_SOMATORIO_PRECO_GAS_MENOR_ZERO";

	String ERRO_DATA_FIM_FORNECIMENTO_MENOR_ATUAL = "ERRO_DATA_FIM_FORNECIMENTO_MENOR_ATUAL";

	String ERRO_DATA_INICIO_FORNECIMENTO_MENOR_ATUAL = "ERRO_DATA_INICIO_FORNECIMENTO_MENOR_ATUAL";

	String ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MENOR_DATA_EMISAO = "ERRO_NEGOCIO_DATA_VENCIMENTO_ITEM_MENOR_DATA_EMISAO";

	String ERRO_PRECO_GAS_ITEM_PRECO_PAI_ENCONTRADO_COMPLEMENTAR = "ERRO_PRECO_GAS_ITEM_PRECO_PAI_ENCONTRADO_COMPLEMENTAR";

	String ERRO_PRECO_ITEM_GAS_VOLUME_DIFERENTE_PAI = "ERRO_PRECO_ITEM_GAS_VOLUME_DIFERENTE_PAI";

	String ERRO_NEGOCIO_EXCLUIR_PRECO_GAS_COM_COMPLEMENTO = "ERRO_NEGOCIO_EXCLUIR_PRECO_GAS_COM_COMPLEMENTO";

	String ERRO_PRECO_GAS_DATA_FORA_COMPLEMENTO = "ERRO_PRECO_GAS_DATA_FORA_COMPLEMENTO";

	String ERRO_PRECO_GAS_SITUACAO_COMPRA_POSSUI_COMPLEMENTO = "ERRO_PRECO_GAS_SITUACAO_COMPRA_POSSUI_COMPLEMENTO";

	String ERRO_PRECO_GAS_COMPLEMENTAR_SIM = "ERRO_PRECO_GAS_COMPLEMENTAR_SIM";

	String ERRO_PRECO_GAS_DATA_EMISSAO_MAIOR_IGUAL_PAI = "ERRO_PRECO_GAS_DATA_EMISSAO_MAIOR_IGUAL_PAI";

	String ERRO_PRECO_GAS_DATA_EMISSAO_PAI_MENOR = "ERRO_PRECO_GAS_DATA_EMISSAO_PAI_MENOR";

	String ERRO_CONCORRENCIA = "ERRO_CONCORRENCIA";

	/**
	 * Método responsável por consultar o Preço do
	 * Gás de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa de Precos de Gas.
	 * @return Uma coleção de Preco de Gás.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<PrecoGas> consultarPrecoGas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar as Precos
	 * Gas.
	 * 
	 * @return Uma coleção de Precos de Gas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<PrecoGas> listarPrecoGas() throws NegocioException;

	/**
	 * Método responsável por Criar um itém do
	 * Preço do Gas.
	 * 
	 * @return the preco gas item
	 */
	PrecoGasItem criarPrecoGasItem();

	/**
	 * Método responsável por Validar as
	 * informações do Itém do Preço do Gas.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @param precosGasItem
	 *            the precos gas item
	 * @param precoGasItem
	 *            the preco gas item
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarPrecoGasItem(PrecoGas precoGas, Collection<PrecoGasItem> precosGasItem, PrecoGasItem precoGasItem) throws NegocioException;

	/**
	 * Método responsável por Validar as
	 * informações do Preço do Gas.
	 * 
	 * @param precosGas
	 *            the precos gas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarPrecoGas(PrecoGas precosGas) throws NegocioException;

	/**
	 * Método responsável por Inserir o Preço do
	 * Gas.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserirPrecoGas(PrecoGas precoGas) throws NegocioException;

	/**
	 * Método responsável por remover os Precos do
	 * Gas selecionados.
	 * 
	 * @param chavesPrimarias
	 *            As chaves dos Precos do gas a
	 *            serem excluídos.
	 * @param auditoria
	 *            the auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerPrecoGas(Long[] chavesPrimarias, DadosAuditoria auditoria) throws NegocioException;

	/**
	 * Método responsável por consultar o itém do
	 * Preço do Gás de acordo com o filtro.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa ddo item do Precos de
	 *            Gas.
	 * @return Uma coleção de Preco de Gás.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<PrecoGasItem> consultarPrecoGasItem(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar o preço de
	 * gás num dia, para um determinado item de
	 * fatura.
	 * 
	 * @param cdItemFatura
	 *            Item Fatura
	 * @param dataExecucao
	 *            Data do Preço
	 * @return O preço do gás
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PrecoGasItem consultarPrecoGas(long cdItemFatura, Date dataExecucao) throws NegocioException;

	/**
	 * Método responsável por consultar o preço do
	 * gás num intervalo de data, para um
	 * determinado item de fatura e contrato de
	 * compra.
	 * 
	 * @param dataInicial
	 *            Data Inicial do Período de
	 *            Fornecimento.
	 * @param dataFinal
	 *            Data Final do Período de
	 *            Fornecimento.
	 * @param itemFatura
	 *            the item fatura
	 * @param contratoCompra
	 *            the contrato compra
	 * @return O preço do gás.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal consultarPrecoGas(Date dataInicial, Date dataFinal, EntidadeConteudo itemFatura, EntidadeConteudo contratoCompra)
					throws NegocioException;

	/**
	 * Atualizar preco gas.
	 * 
	 * @param precoGas
	 *            the preco gas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void atualizarPrecoGas(PrecoGas precoGas) throws NegocioException;

	/**
	 * Validar remover preco gas item.
	 * 
	 * @param precoGasItem
	 *            the preco gas item
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemoverPrecoGasItem(PrecoGasItem precoGasItem) throws NegocioException;
}
