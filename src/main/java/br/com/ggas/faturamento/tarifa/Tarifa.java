/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface resonsável pela assinatura dos métodos relacionados a entidade Tarifa
 *
 */
public interface Tarifa extends EntidadeNegocio {

	String BEAN_ID_TARIFA = "tarifa";

	String TARIFA_CAMPO_STRING = "TARIFA";

	String TARIFAS = "TARIFAS";

	String TARIFA_ITEM_FATURA = "TARIFA_ITEM_FATURA";

	String TARIFA_DESCRICAO = "TARIFA_DESCRICAO";

	String TARIFA_DESCRICAO_ABREVIADA = "TARIFA_DESCRICAO_ABREVIADA";

	String TARIFA_TIPO_CONTRATO = "TARIFA_TIPO_CONTRATO";

	String TARIFA_SEGMENTO = "TARIFA_SEGMENTO";

	String TARIFA_QUANTIDADE_CASA_DECIMAL = "TARIFA_QN_CASA_DECIMAL";

	String TARIFA_TIPO_DESCONTO = "TARIFA_TIPO_DESCONTO";

	String ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA = "ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA";

	String TARIFA_TIPO = "TARIFA_TIPO";

	String TARIFA_VALOR = "TARIFA_VALOR";

	String VIGENCIA = "TARIFA_VIGENCIA";

	String DESCONTO = "TARIFA_DESCONTO";

	String ERRO_FAIXAS_VAZIAS = "ERRO_FAIXAS_VAZIAS";

	String ERRO_FAIXAS_NAO_PERMITIDAS = "ERRO_FAIXAS_NAO_PERMITIDAS";

	String ERRO_FAIXA_JA_EXISTE = "ERRO_FAIXA_JA_EXISTE";

	String ERRO_TARIFA_VALORES_FIXO_VARIAVEL_NAO_INFORMADOS = "ERRO_TARIFA_VALORES_FIXO_VARIAVEL_NAO_INFORMADOS";

	String ERRO_TARIFA_FAIXA_FINAL_NAO_INFORMADA = "ERRO_TARIFA_FAIXA_FINAL_NAO_INFORMADA";

	String ERRO_TARIFA_LACUNA_NAS_FAIXAS = "ERRO_TARIFA_LACUNA_NAS_FAIXAS";

	String ERRO_TARIFA_VALOR_MAXIMO_ALCANCADO = "ERRO_TARIFA_VALOR_MAXIMO_ALCANCADO";

	String ERRO_VALOR_FAIXA_TARIFA_INVALIDO = "ERRO_VALOR_FAIXA_TARIFA_INVALIDO";

	String ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO = "ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO";

	String ATRIBUTO_ITEM_FATURA = "itemFatura";
	
	String ATRIBUTO_DESCRICAO = "descricao";
	
	String ATRIBUTO_DESCRICAO_ABREVIADA = "descricaoAbreviada";
	
	String ATRIBUTO_ID_SEGMENTO = "idSegmento";
	
	String ATRIBUTO_ID_TARIFA = "idTarifa";
	
	String ATRIBUTO_STATUS = "status";	
	
	String ATRIBUTO_DATA_VIGENCIA = "dataVigencia";
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getRamoAtividade()
	 */
	/**
	 * @return RamoAtividade - Retorna ramo atividade.
	 */
	RamoAtividade getRamoAtividade();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setRamoAtividade
	 * (br.com.ggas.cadastro.imovel.RamoAtividade)
	 */
	/**
	 * @param ramoAtividade - Set ramo de atividade.
	 */
	void setRamoAtividade(RamoAtividade ramoAtividade);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getItemFatura()
	 */
	/**
	 * @return EntidadeConteudo - Retorna Item Fatura.
	 */
	EntidadeConteudo getItemFatura();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setItemFatura
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	/**
	 * @param itemFatura - Set Item fatura.
	 */
	void setItemFatura(EntidadeConteudo itemFatura);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getDescricao()
	 */
	/**
	 * @return String - Retorna Descrição.
	 */
	String getDescricao();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setDescricao(java.lang.String)
	 */
	/**
	 * @param descricao - Set descrição
	 */
	void setDescricao(String descricao);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getDescricaoAbreviada()
	 */
	/**
	 * @return String - Retorna Descrição Abreviada.
	 */
	String getDescricaoAbreviada();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	/**
	 * @param descricaoAbreviada - Set descrição abreviada.
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getSegmento()
	 */
	/**
	 * @return Segmento - Retorna um objeto segmento.
	 */
	Segmento getSegmento();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setSegmento
	 * (br.com.ggas.cadastro.imovel.Segmento)
	 */
	/**
	 * @param segmento - Set objeto segmento.
	 */
	void setSegmento(Segmento segmento);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #getQuantidadeCasaDecimal()
	 */
	/**
	 * @return BigDecimal - Retorna Quantidade de casa decimal.
	 */
	BigDecimal getQuantidadeCasaDecimal();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Tarifa
	 * #setQuantidadeCasaDecimal
	 * (java.math.BigDecimal)
	 */
	/**
	 * @param quantidadeCasaDecimal - Set quantidade de casa decimal.
	 */
	void setQuantidadeCasaDecimal(BigDecimal quantidadeCasaDecimal);

	/**
	 * @return EntidadeConteudo - Retorna Tipo do contrato.
	 */
	EntidadeConteudo getTipoContrato();

	/**
	 * @param tipoContrato - Set Tipo do contrato.
	 */
	void setTipoContrato(EntidadeConteudo tipoContrato);

}
