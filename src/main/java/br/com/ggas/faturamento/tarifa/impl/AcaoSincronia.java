package br.com.ggas.faturamento.tarifa.impl;

/**
 * AcaoSincronia para Acao sincronia
 *
 */
public enum AcaoSincronia {
	PASSAR,DESABILITAR,MUDAR_INICIO_VIGENCIA,MUDAR_FIM_VIGENCIA
}
