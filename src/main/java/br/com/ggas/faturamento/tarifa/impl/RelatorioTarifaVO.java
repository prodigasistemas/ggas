/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Collection;

import br.com.ggas.faturamento.tarifa.Tarifa;

/**
 * Classe de VO para o relatório de tarifa
 *
 */
public class RelatorioTarifaVO {

	private Tarifa tarifa;

	private Collection<RelatorioTarifaVigenciaVO> tarifaVigencias;

	public String getDescricaoTarifa() {

		if(tarifa != null) {
			return tarifa.getDescricao();
		} else {
			return "";
		}

	}

	public String getDescricaoAbreviadaTarifa() {

		if(tarifa != null) {
			return tarifa.getDescricaoAbreviada();
		} else {
			return "";
		}

	}

	public String getDescricaoTipoContrato() {

		if(tarifa != null && tarifa.getTipoContrato() != null) {
			return tarifa.getTipoContrato().getDescricao();
		} else {
			return "";
		}

	}

	public String getDescricaoItemFatura() {

		if(tarifa != null && tarifa.getItemFatura() != null) {
			return tarifa.getItemFatura().getDescricao();
		} else {
			return "";
		}

	}

	public String getDescricaoSegmento() {

		if(tarifa != null && tarifa.getSegmento() != null) {
			return tarifa.getSegmento().getDescricao();
		} else {
			return "";
		}

	}

	public String getDescricaoRamoAtividade() {

		if(tarifa != null && tarifa.getRamoAtividade() != null) {
			return tarifa.getRamoAtividade().getDescricao();
		} else {
			return "";
		}

	}

	public String getQuantidadeCasaDecimalTarifa() {

		if(tarifa != null) {
			return tarifa.getQuantidadeCasaDecimal().toString();
		} else {
			return "";
		}

	}

	/**
	 * @return the tarifa
	 */
	public Tarifa getTarifa() {

		return tarifa;
	}

	/**
	 * @param tarifa
	 *            the tarifa to set
	 */
	public void setTarifa(Tarifa tarifa) {

		this.tarifa = tarifa;
	}

	/**
	 * @return the tarifaVigencias
	 */
	public Collection<RelatorioTarifaVigenciaVO> getTarifaVigencias() {

		return tarifaVigencias;
	}

	/**
	 * @param tarifaVigencias
	 *            the tarifaVigencias to set
	 */
	public void setTarifaVigencias(Collection<RelatorioTarifaVigenciaVO> tarifaVigencias) {

		this.tarifaVigencias = tarifaVigencias;
	}

}
