/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import br.com.ggas.faturamento.tarifa.DadosFaixasTarifa;

class DadosFaixasTarifaImpl implements DadosFaixasTarifa {

	private static final long serialVersionUID = 4397746932867374476L;

	private String faixaInicial;

	private String faixaFinal;

	private String valorFixoSemImposto;

	private String valorCompra;

	private String margemValorAgregado;

	private String valorVariavelSemImposto;

	private String descontoFixoSemImposto;

	private String descontoVariavelSemImposto;

	private Long tipoDesconto;

	private Integer numeroColunaFinal;

	private Boolean ultimaFaixa;

	private Boolean valorVariavelSemImpostoPreenchido;

	private Long idTarifaVigenciaFaixa;
	
	private Boolean valorFixoSemImpostoPreenchido;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #getIdTarifaVigenciaFaixa()
	 */
	@Override
	public Long getIdTarifaVigenciaFaixa() {

		return idTarifaVigenciaFaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #setIdTarifaVigenciaFaixa(java.lang.Long)
	 */
	@Override
	public void setIdTarifaVigenciaFaixa(Long idTarifaVigenciaFaixa) {

		this.idTarifaVigenciaFaixa = idTarifaVigenciaFaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa#getTipoDesconto()
	 */
	@Override
	public Long getTipoDesconto() {

		return tipoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #setTipoDesconto(java.lang.String)
	 */
	@Override
	public void setTipoDesconto(Long tipoDesconto) {

		this.tipoDesconto = tipoDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa#getNumeroColunaFinal()
	 */
	@Override
	public Integer getNumeroColunaFinal() {

		return numeroColunaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #setNumeroColunaFinal(java.lang.Long)
	 */
	@Override
	public void setNumeroColunaFinal(Integer numeroColunaFinal) {

		this.numeroColunaFinal = numeroColunaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #getDescontoFixoSemImposto()
	 */
	@Override
	public String getDescontoFixoSemImposto() {

		return descontoFixoSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #setDescontoFixoSemImposto(
	 * java.lang.String)
	 */
	@Override
	public void setDescontoFixoSemImposto(String descontoFixoSemImposto) {

		this.descontoFixoSemImposto = descontoFixoSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #getDescontoVariavelSemImposto()
	 */
	@Override
	public String getDescontoVariavelSemImposto() {

		return descontoVariavelSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * DadosFaixasTarifa
	 * #setDescontoVariavelSemImposto
	 * (java.lang.String)
	 */
	@Override
	public void setDescontoVariavelSemImposto(String descontoVariavelSemImposto) {

		this.descontoVariavelSemImposto = descontoVariavelSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#getFaixaInicial()
	 */
	@Override
	public String getFaixaInicial() {

		return faixaInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas
	 * #setFaixaInicial(java.lang.String)
	 */
	@Override
	public void setFaixaInicial(String faixaInicial) {

		this.faixaInicial = faixaInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#getFaixaFinal()
	 */
	@Override
	public String getFaixaFinal() {

		return faixaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas
	 * #setFaixaFinal(java.lang.String)
	 */
	@Override
	public void setFaixaFinal(String faixaFinal) {

		this.faixaFinal = faixaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#getValorFixoSemImposto()
	 */
	@Override
	public String getValorFixoSemImposto() {

		return valorFixoSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#getValorCompra()
	 */
	@Override
	public String getValorCompra() {

		return valorCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#setValorCompra()
	 */
	@Override
	public void setValorCompra(String valorCompra) {

		this.valorCompra = valorCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#getMargemValorAgregado()
	 */
	@Override
	public String getMargemValorAgregado() {

		return margemValorAgregado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas#setMargemValorAgregado()
	 */
	@Override
	public void setMargemValorAgregado(String margemValorAgregado) {

		this.margemValorAgregado = margemValorAgregado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas
	 * #setValorFixoSemImposto(java.lang.String)
	 */
	@Override
	public void setValorFixoSemImposto(String valorFixoSemImposto) {

		this.valorFixoSemImposto = valorFixoSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas
	 * #getValorVariavelSemImposto()
	 */
	@Override
	public String getValorVariavelSemImposto() {

		return valorVariavelSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DadosFaixasTarifas
	 * #setValorVariavelSemImposto
	 * (java.lang.String)
	 */
	@Override
	public void setValorVariavelSemImposto(String valorVariavelSemImposto) {

		this.valorVariavelSemImposto = valorVariavelSemImposto;
	}

	@Override
	public Boolean getUltimaFaixa() {

		return ultimaFaixa;
	}

	@Override
	public void setUltimaFaixa(Boolean ultimaFaixa) {

		this.ultimaFaixa = ultimaFaixa;
	}

	@Override
	public Boolean getValorFixoSemImpostoPreenchido() {

		Boolean retorno = false;
		if((this.valorFixoSemImposto != null) && (!"".equals(this.valorFixoSemImposto))
						&& (Double.valueOf(this.valorFixoSemImposto.replace(".", "").replace(",", ".")) > 0)) {
			retorno = true;
		}
		this.valorFixoSemImpostoPreenchido = retorno;
		return valorFixoSemImpostoPreenchido;
	}

	@Override
	public Boolean getValorVariavelSemImpostoPreenchido() {

		Boolean retorno = false;
		if((this.valorVariavelSemImposto != null) && (!"".equals(this.valorVariavelSemImposto))
						&& (Double.valueOf(this.valorVariavelSemImposto.replace(".", "").replace(",", ".")) > 0)) {
			retorno = true;
		}
		this.valorVariavelSemImpostoPreenchido = retorno;
		return valorVariavelSemImpostoPreenchido;
	}

}
