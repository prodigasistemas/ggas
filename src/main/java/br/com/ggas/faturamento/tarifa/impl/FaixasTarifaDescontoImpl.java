/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.math.BigDecimal;

import br.com.ggas.faturamento.tarifa.FaixasTarifaDesconto;

class FaixasTarifaDescontoImpl implements FaixasTarifaDesconto {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8339065484854753635L;

	private Integer qtdeCasasDecimais;

	private BigDecimal medidaInicio;

	private BigDecimal medidaFim;

	private BigDecimal valorFixo;

	private BigDecimal valorDescontoFixo;

	private BigDecimal valorLiquidoSemImpostoFixo;

	private BigDecimal valorLiquidoComImpostoFixo;

	private BigDecimal valorCompra;

	private BigDecimal margemValorAgregado;

	private BigDecimal valorVariavel;

	private BigDecimal valorDescontoVariavel;

	private BigDecimal valorLiquidoSemImpostoVariavel;

	private BigDecimal valorLiquidoComImpostoVariavel;

	private BigDecimal valorTotalSemImposto;

	private BigDecimal valorTotalComImposto;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * FaixasTarifaDesconto#isValor()
	 */
	@Override
	public boolean isValor() {

		return getValorDescontoFixo() != null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * FaixasTarifaDesconto#getQtdeCasasDecimais()
	 */
	@Override
	public Integer getQtdeCasasDecimais() {

		return qtdeCasasDecimais;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * FaixasTarifaDesconto
	 * #setQtdeCasasDecimais(java.lang.Integer)
	 */
	@Override
	public void setQtdeCasasDecimais(Integer qtdeCasasDecimais) {

		this.qtdeCasasDecimais = qtdeCasasDecimais;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getMedidaInicio()
	 */
	@Override
	public BigDecimal getMedidaInicio() {

		return medidaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setMedidaInicio(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaInicio(BigDecimal medidaInicio) {

		this.medidaInicio = medidaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getMedidaFim()
	 */
	@Override
	public BigDecimal getMedidaFim() {

		return medidaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setMedidaFim(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaFim(BigDecimal medidaFim) {

		this.medidaFim = medidaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getValorFixo()
	 */
	@Override
	public BigDecimal getValorFixo() {

		return valorFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorFixo(java.math.BigDecimal)
	 */
	@Override
	public void setValorFixo(BigDecimal valorFixo) {

		this.valorFixo = valorFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getValorDescontoFixo()
	 */
	@Override
	public BigDecimal getValorDescontoFixo() {

		return valorDescontoFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorDescontoFixo(java.math.BigDecimal)
	 */
	@Override
	public void setValorDescontoFixo(BigDecimal valorDescontoFixo) {

		this.valorDescontoFixo = valorDescontoFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorLiquidoSemImpostoFixo()
	 */
	@Override
	public BigDecimal getValorLiquidoSemImpostoFixo() {

		return valorLiquidoSemImpostoFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorLiquidoSemImpostoFixo
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorLiquidoSemImpostoFixo(BigDecimal valorLiquidoSemImpostoFixo) {

		this.valorLiquidoSemImpostoFixo = valorLiquidoSemImpostoFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorLiquidoComImpostoFixo()
	 */
	@Override
	public BigDecimal getValorLiquidoComImpostoFixo() {

		return valorLiquidoComImpostoFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorLiquidoComImpostoFixo
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorLiquidoComImpostoFixo(BigDecimal valorLiquidoComImpostoFixo) {

		this.valorLiquidoComImpostoFixo = valorLiquidoComImpostoFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getValorCompra()
	 */
	@Override
	public BigDecimal getValorCompra() {

		return valorCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorCompra(java.math.BigDecimal)
	 */
	@Override
	public void setValorCompra(BigDecimal valorCompra) {

		this.valorCompra = valorCompra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getMargemValorAgregado()
	 */
	@Override
	public BigDecimal getMargemValorAgregado() {

		return margemValorAgregado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setMargemValorAgregado(java.math.BigDecimal)
	 */
	@Override
	public void setMargemValorAgregado(BigDecimal margemValorAgregado) {

		this.margemValorAgregado = margemValorAgregado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto#getValorVariavel()
	 */
	@Override
	public BigDecimal getValorVariavel() {

		return valorVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorVariavel(java.math.BigDecimal)
	 */
	@Override
	public void setValorVariavel(BigDecimal valorVariavel) {

		this.valorVariavel = valorVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorDescontoVariavel()
	 */
	@Override
	public BigDecimal getValorDescontoVariavel() {

		return valorDescontoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorDescontoVariavel
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorDescontoVariavel(BigDecimal valorDescontoVariavel) {

		this.valorDescontoVariavel = valorDescontoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorLiquidoSemImpostoVariavel()
	 */
	@Override
	public BigDecimal getValorLiquidoSemImpostoVariavel() {

		return valorLiquidoSemImpostoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorLiquidoSemImpostoVariavel
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorLiquidoSemImpostoVariavel(BigDecimal valorLiquidoSemImpostoVariavel) {

		this.valorLiquidoSemImpostoVariavel = valorLiquidoSemImpostoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorLiquidoComImpostoVariavel()
	 */
	@Override
	public BigDecimal getValorLiquidoComImpostoVariavel() {

		return valorLiquidoComImpostoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorLiquidoComImpostoVariavel
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorLiquidoComImpostoVariavel(BigDecimal valorLiquidoComImpostoVariavel) {

		this.valorLiquidoComImpostoVariavel = valorLiquidoComImpostoVariavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorTotalSemImposto()
	 */
	@Override
	public BigDecimal getValorTotalSemImposto() {

		return valorTotalSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorTotalSemImposto
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalSemImposto(BigDecimal valorTotalSemImposto) {

		this.valorTotalSemImposto = valorTotalSemImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #getValorTotalComImposto()
	 */
	@Override
	public BigDecimal getValorTotalComImposto() {

		return valorTotalComImposto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * FaixasTarifaDesconto
	 * #setValorTotalComImposto
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setValorTotalComImposto(BigDecimal valorTotalComImposto) {

		this.valorTotalComImposto = valorTotalComImposto;
	}

}
