/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Date;

import br.com.ggas.faturamento.tarifa.VigenciaDescontosCadastrado;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * @author euchoa
 */
class VigenciaDescontosCadastradoImpl implements VigenciaDescontosCadastrado {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4909157290253097337L;

	private Long idTarifaVigenciaDesconto;

	private Date dataInicial;

	private Date dataFinal;

	/**
	 * Instantiates a new vigencia descontos cadastrado impl.
	 * 
	 * @param idTarifaVigenciaDesconto
	 *            the id tarifa vigencia desconto
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 */
	public VigenciaDescontosCadastradoImpl(Long idTarifaVigenciaDesconto, Date dataInicial, Date dataFinal) {

		this.idTarifaVigenciaDesconto = idTarifaVigenciaDesconto;
		this.dataInicial = dataInicial;
		this.dataFinal = dataFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * VingeciaDescontosCadastrado
	 * #getIdTarifaVigenciaDesconto()
	 */
	@Override
	public Long getIdTarifaVigenciaDesconto() {

		return idTarifaVigenciaDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * VingeciaDescontosCadastrado
	 * #setIdTarifaVigenciaDesconto
	 * (java.lang.Long)
	 */
	@Override
	public void setIdTarifaVigenciaDesconto(Long idTarifaVigenciaDesconto) {

		this.idTarifaVigenciaDesconto = idTarifaVigenciaDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DatasDescontosCadastrado#getDataInicial()
	 */
	@Override
	public Date getDataInicial() {

		return dataInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DatasDescontosCadastrado
	 * #setDataInicial(java.util.Date)
	 */
	@Override
	public void setDataInicial(Date dataInicial) {

		this.dataInicial = dataInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DatasDescontosCadastrado#getDataFinal()
	 */
	@Override
	public Date getDataFinal() {

		return dataFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.impl.
	 * DatasDescontosCadastrado
	 * #setDataFinal(java.util.Date)
	 */
	@Override
	public void setDataFinal(Date dataFinal) {

		this.dataFinal = dataFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * VigenciaDescontosCadastrado
	 * #getVigenciasFormatadas()
	 */
	@Override
	public String getVigenciasFormatadas() {

		StringBuilder vigencias = new StringBuilder();
		if((dataInicial != null) && (dataFinal != null)) {
			vigencias.append(Util.converterDataParaStringSemHora(dataInicial, Constantes.FORMATO_DATA_BR));
			vigencias.append(" - ");
			vigencias.append(Util.converterDataParaStringSemHora(dataFinal, Constantes.FORMATO_DATA_BR));
		}
		return vigencias.toString();
	}

}
