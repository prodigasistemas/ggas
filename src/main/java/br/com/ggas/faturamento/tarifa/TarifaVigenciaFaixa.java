/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa;

import java.math.BigDecimal;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados a TarifaVigenciaFaixa
 *
 */
public interface TarifaVigenciaFaixa extends EntidadeNegocio {

	String BEAN_ID_TARIFA_VIGENCIA_FAIXA = "tarifaVigenciaFaixa";

	String TARIFA_VIGENCIA_FAIXA = "TARIFA_VIGENCIA_FAIXA";

	String TARIFAS_VIGENCIA_FAIXA = "TARIFAS_VIGENCIA_FAIXA";

	String TARIFA_VIGENCIA = "TARIFA_VIGENCIA";

	String MEDIDA_INICIO = "TARIFA_VIGENCIA_FAIXA_MEDIDA_INICIO";

	String MEDIDA_FIM = "TARIFA_VIGENCIA_FAIXA_MEDIDA_FIM";

	String VALOR_FIXO = "TARIFA_VIGENCIA_FAIXA_VALOR_FIXO";

	String VALOR_VARIAVEL = "TARIFA_VIGENCIA_FAIXA_VALOR_VARIAVEL";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getTarifaVigencia()
	 */
	/**
	 * @return Retorna Tarifa Vigencia
	 */
	TarifaVigencia getTarifaVigencia();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa
	 * #setTarifaVigencia(br.com
	 * .ggas.faturamento.tarifa.TarifaVigencia)
	 */
	/**
	 * @param tarifaVigencia - Tarifa vigência.
	 */
	void setTarifaVigencia(TarifaVigencia tarifaVigencia);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getMedidaInicio()
	 */
	/**
	 * @return Retorna Medida Inicio
	 */
	BigDecimal getMedidaInicio();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getMedidaInicioFormatada()
	 */
	/**
	 * @return Retorna Medida Inicio Formatada
	 */
	String getMedidaInicioFormatada();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa
	 * #setMedidaInicio(java.math.BigDecimal)
	 */
	/**
	 * @param medidaInicio - Set médida inicio. 
	 */
	void setMedidaInicio(BigDecimal medidaInicio);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getMedidaFim()
	 */
	/**
	 * @return Retorna Medida Fim
	 */
	BigDecimal getMedidaFim();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getMedidaFimFormatada()
	 */
	/**
	 * @return Retorna Medida Fim Formatada
	 */
	String getMedidaFimFormatada();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa
	 * #setMedidaFim(java.math.BigDecimal)
	 */
	/**
	 * @param medidaFim - Set médida fim.
	 */
	void setMedidaFim(BigDecimal medidaFim);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getValorFixo()
	 */
	/**
	 * @return Retorna Valor Fixo
	 */
	BigDecimal getValorFixo();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getValorFixoFormatado()
	 */
	/**
	 * @return Retornar Valor Fixo Formatado
	 */
	String getValorFixoFormatado();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa
	 * #setValorFixo(java.math.BigDecimal)
	 */
	/**
	 * @param valorFixo - Set valor fixo.
	 */
	void setValorFixo(BigDecimal valorFixo);

	/**
	 * @return the valorCompra
	 */
	BigDecimal getValorCompra();

	/**
	 * Gets the valor compra formatado.
	 * 
	 * @param numeroCasasDecimais
	 *            numero de casas decimais
	 * @return the valorCompra formatado
	 */
	String getValorCompraFormatado(Integer numeroCasasDecimais);

	/**
	 * @return the valorCompra formatado
	 */
	String getValorCompraFormatado();

	/**
	 * @param valorCompra
	 *            the valorCompra to set
	 */
	void setValorCompra(BigDecimal valorCompra);

	/**
	 * @return the margemValorAgregado
	 */
	BigDecimal getMargemValorAgregado();

	/**
	 * @return the margemValorAgregado formatado
	 */
	String getMargemValorAgregadoFormatado();

	/**
	 * Gets the margem valor agregado formatado.
	 * 
	 * @param numeroCasasDecimais
	 *            numero de casas decimais
	 * @return the margemValorAgregado formatado
	 */
	String getMargemValorAgregadoFormatado(Integer numeroCasasDecimais);

	/**
	 * @param margemValorAgregado - Margem valor agregado.
	 */
	void setMargemValorAgregado(BigDecimal margemValorAgregado);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getValorVariavel()
	 */
	/**
	 * @return Retorna Valor Variavel
	 */
	BigDecimal getValorVariavel();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa#getValorVariavelFormatado()
	 */
	/**
	 * @return Retorna Valor Variavel Formatado
	 */
	String getValorVariavelFormatado();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaFaixa
	 * #setValorVariavel(java.math.BigDecimal)
	 */
	/**
	 * @param valorVariavel - Set valor variável.
	 */
	void setValorVariavel(BigDecimal valorVariavel);

}
