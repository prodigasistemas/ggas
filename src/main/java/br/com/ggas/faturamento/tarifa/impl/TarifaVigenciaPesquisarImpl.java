/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import java.util.Map;

import br.com.ggas.faturamento.tarifa.TarifaVigenciaPesquisar;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 * 
 */

class TarifaVigenciaPesquisarImpl extends EntidadeNegocioImpl implements TarifaVigenciaPesquisar {

	/**
	 * serial version
	 */
	private static final long serialVersionUID = -4614036370742625819L;

	private String vigente;

	private String descricaoTarifa;

	private String descricaoAbreviadaTarifa;

	private String descricaoSegmento;

	private String dataInicioVigencia;

	private String dataFimVigencia;


	/**
	 * Instantiates a new tarifa vigencia pesquisar impl.
	 * 
	 * @param vigente
	 *            the vigente
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param descricaoTarifa
	 *            the descricao tarifa
	 * @param descricaoAbreviadaTarifa
	 *            the descricao abreviada tarifa
	 * @param habilitado
	 *            the habilitado
	 * @param descricaoSegmento
	 *            the descricao segmento
	 * @param dataInicioVigencia
	 *            the data inicio vigencia
	 * @param dataFimVigencia
	 *            the data fim vigencia
	 */
	public TarifaVigenciaPesquisarImpl(String vigente, long chavePrimaria, String descricaoTarifa, String descricaoAbreviadaTarifa,
										Boolean habilitado, String descricaoSegmento, String dataInicioVigencia, String dataFimVigencia) {

		super();
		this.vigente = vigente;
		super.setChavePrimaria(chavePrimaria);
		this.descricaoTarifa = descricaoTarifa;
		this.descricaoAbreviadaTarifa = descricaoAbreviadaTarifa;
		super.setHabilitado(habilitado);
		this.descricaoSegmento = descricaoSegmento;
		this.dataInicioVigencia = dataInicioVigencia;
		this.dataFimVigencia = dataFimVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #getVigente()
	 */
	@Override
	public String getVigente() {

		return vigente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #setVigente(java.lang.Boolean)
	 */
	@Override
	public void setVigente(String vigente) {

		this.vigente = vigente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #getDescricaoTarifa()
	 */
	@Override
	public String getDescricaoTarifa() {

		return descricaoTarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #setDescricaoTarifa(java.lang.String)
	 */
	@Override
	public void setDescricaoTarifa(String descricaoTarifa) {

		this.descricaoTarifa = descricaoTarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #getDescricaoAbreviadaTarifa()
	 */
	@Override
	public String getDescricaoAbreviadaTarifa() {

		return descricaoAbreviadaTarifa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #setDescricaoAbreviadaTariva
	 * (java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviadaTariva(String descricaoAbreviadaTarifa) {

		this.descricaoAbreviadaTarifa = descricaoAbreviadaTarifa;
	}

	@Override
	public Boolean getHabilitado() {

		return super.isHabilitado();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #getDescricaoSegmento()
	 */
	@Override
	public String getDescricaoSegmento() {

		return descricaoSegmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #setDescricaoSegmento(java.lang.String)
	 */
	@Override
	public void setDescricaoSegmento(String descricaoSegmento) {

		this.descricaoSegmento = descricaoSegmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #getDataInicioVigencia()
	 */
	@Override
	public String getDataInicioVigencia() {

		return dataInicioVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #setDataInicioVigencia(java.util.Date)
	 */
	@Override
	public void setDataInicioVigencia(String dataInicioVigencia) {

		this.dataInicioVigencia = dataInicioVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #getDataFimVigencia()
	 */
	@Override
	public String getDataFimVigencia() {

		return dataFimVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #setDataFimVigencia(java.util.Date)
	 */
	@Override
	public void setDataFimVigencia(String dataFimVigencia) {

		this.dataFimVigencia = dataFimVigencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.tarifa.impl.Teste
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaPesquisar#getStatus()
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.tarifa.
	 * TarifaVigenciaPesquisar
	 * #setStatus(java.lang.String)
	 */

}
