/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.tarifa.impl;

import br.com.ggas.util.FormatoImpressao;

/**
 * 
 * Filtro Tarifa Vigencia Descontos
 *
 */
public class FiltroTarifaVigenciaDescontos {

	private FormatoImpressao formatoImpressao;

	private Long chavePrimariaTarifa;

	private Long chavePrimariaTarifaVigencia;

	private Long chavePrimariaTarifaVigenciaDesconto;

	private String tipoDesconto;

	
	/**
	 * Montar filtro tarifa vigencia descontos.
	 * 
	 * @param chavePrimariaVigenciaDesconto - {@link Long}
	 * @param chavePrimariaTarifa - {@link Long}
	 * @param chavePrimariaVigencia - {@link Long}
	 * @param descricaoTipoDesconto - {@link String}
	 * @param tipoExportacao - {@link String}
	 *            
	 * @return filtro de tarifa vigencia descontos - {@link FiltroTarifaVigenciaDescontos}
	 */
	public static FiltroTarifaVigenciaDescontos montarFiltroTarifaVigenciaDescontos(Long chavePrimariaTarifa,
			Long chavePrimariaVigencia, Long chavePrimariaVigenciaDesconto, String descricaoTipoDesconto, 
			String tipoExportacao) {

	
		FiltroTarifaVigenciaDescontos filtro = new FiltroTarifaVigenciaDescontos();
		filtro.setFormatoImpressao(FormatoImpressao.getFormatoImpressao(tipoExportacao));
		filtro.setChavePrimariaTarifa(chavePrimariaTarifa);
		filtro.setChavePrimariaTarifaVigencia(chavePrimariaVigencia);
		filtro.setTipoDesconto(descricaoTipoDesconto);
		
		if(chavePrimariaVigenciaDesconto > 0) {
			filtro.setChavePrimariaTarifaVigenciaDesconto(chavePrimariaVigenciaDesconto);
		}

		return filtro;
	}
	

	/**
	 * @return the formatoImpressao
	 */
	public FormatoImpressao getFormatoImpressao() {

		return formatoImpressao;
	}

	/**
	 * @param formatoImpressao
	 *            the formatoImpressao to set
	 */
	public void setFormatoImpressao(FormatoImpressao formatoImpressao) {

		this.formatoImpressao = formatoImpressao;
	}

	/**
	 * @return the chavePrimariaTarifa
	 */
	public Long getChavePrimariaTarifa() {

		return chavePrimariaTarifa;
	}

	/**
	 * @param chavePrimariaTarifa
	 *            the chavePrimariaTarifa to set
	 */
	public void setChavePrimariaTarifa(Long chavePrimariaTarifa) {

		this.chavePrimariaTarifa = chavePrimariaTarifa;
	}

	/**
	 * @return the chavePrimariaTarifaVigencia
	 */
	public Long getChavePrimariaTarifaVigencia() {

		return chavePrimariaTarifaVigencia;
	}

	/**
	 * @param chavePrimariaTarifaVigencia
	 *            the chavePrimariaTarifaVigencia to set
	 */
	public void setChavePrimariaTarifaVigencia(Long chavePrimariaTarifaVigencia) {

		this.chavePrimariaTarifaVigencia = chavePrimariaTarifaVigencia;
	}

	/**
	 * @return the chavePrimariaTarifaVigenciaDesconto
	 */
	public Long getChavePrimariaTarifaVigenciaDesconto() {

		return chavePrimariaTarifaVigenciaDesconto;
	}

	/**
	 * @param chavePrimariaTarifaVigenciaDesconto
	 *            the chavePrimariaTarifaVigenciaDesconto to set
	 */
	public void setChavePrimariaTarifaVigenciaDesconto(Long chavePrimariaTarifaVigenciaDesconto) {

		this.chavePrimariaTarifaVigenciaDesconto = chavePrimariaTarifaVigenciaDesconto;
	}

	/**
	 * @return tipoDesconto.
	 */
	public String getTipoDesconto() {

		return tipoDesconto;
	}

	/**
	 * @param tipoDesconto
	 */
	public void setTipoDesconto(String tipoDesconto) {

		this.tipoDesconto = tipoDesconto;
	}

}
