/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.exception;

import java.util.Map;
import java.util.ResourceBundle;

import br.com.ggas.geral.exception.NegocioException;

/**
 * Classe responsável por representar uma exceção relacionada a variação cambial.
 *
 */
public class VariacaoCambialException extends NegocioException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5933846679030272574L;
	private Map<String, Object> erros;
	
	private static final String MENSAGEM = "Não foi possível recuperar a taxa de câmbio no período informado.";

	/**
	 * Construtor padrão.
	 */
	public VariacaoCambialException() {

		super(MENSAGEM);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public VariacaoCambialException(String message, Throwable cause) {

		super(message, cause);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param message
	 *            the message
	 */
	public VariacaoCambialException(String message) {

		super(message);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public VariacaoCambialException(Exception cause) {

		super(cause);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param erros
	 *            the erros
	 */
	public VariacaoCambialException(Map<String, Object> erros) {

		super();
		this.erros = erros;
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public VariacaoCambialException(ResourceBundle rb, String chave, Object[] param) {

		super(rb, chave, param);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 */
	public VariacaoCambialException(ResourceBundle rb, String chave) {

		super(rb, chave);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param rootCause
	 *            the root cause
	 */
	public VariacaoCambialException(ResourceBundle rb, String chave, Exception rootCause) {

		super(rb, chave, rootCause);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public VariacaoCambialException(ResourceBundle rb, String chave, Object param) {

		super(rb, chave, param);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 * @param rootCause
	 *            the root cause
	 */
	public VariacaoCambialException(ResourceBundle rb, String chave, Object[] param, Exception rootCause) {

		super(rb, chave, param, rootCause);
	}

	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param chave
	 *            the chave
	 * @param rootCause
	 *            the root cause
	 */
	public VariacaoCambialException(String chave, Exception rootCause) {

		super(chave, rootCause);
	}
	
	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public VariacaoCambialException(String chave, Object param) {

		super(chave, param);
	}
	
	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param chave
	 *            the chave
	 * @param key
	 *            the key
	 */
	public VariacaoCambialException(String chave, boolean key) {

		super(chave, chave);
	}
	
	/**
	 * Instantiates a new variacao cambial exception.
	 * 
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public VariacaoCambialException(String chave, Object[] param) {

		super(chave, param);
	}
	
	/**
	 * @return Retorna o atributo erros.
	 */
	@Override
	public Map<String, Object> getErros() {

		return erros;
	}

	/**
	 * @param erros
	 *            O valor a ser atribuído ao
	 *            atributo erros.
	 */
	@Override
	public void setErros(Map<String, Object> erros) {

		this.erros = erros;
	}
}
