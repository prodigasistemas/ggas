/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.fatura.Serie;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Interface responsável pela assinatua dos métodos 
 * relacionados ao documento fiscal
 *
 */
public interface DocumentoFiscal extends EntidadeNegocio {

	String BEAN_ID_DOCUMENTO_FISCAL = "documentoFiscal";

	/**
	 * Gets the fatura.
	 *
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * Sets the fatura.
	 *
	 * @param fatura the new fatura
	 */
	void setFatura(Fatura fatura);

	Fatura getFaturaOrigemDevolucao();

	void setFaturaOrigemDevolucao(Fatura faturaOrigemDevolucao);

	/**
	 * Gets the inscricao estadual subs.
	 *
	 * @return the inscricao estadual subs
	 */
	String getInscricaoEstadualSubs();

	/**
	 * Sets the inscricao estadual subs.
	 *
	 * @param inscricaoEstadualSubs the new inscricao estadual subs
	 */
	void setInscricaoEstadualSubs(String inscricaoEstadualSubs);

	/**
	 * Gets the hora saida.
	 *
	 * @return the hora saida
	 */
	Date getHoraSaida();

	/**
	 * Sets the hora saida.
	 *
	 * @param horaSaida the new hora saida
	 */
	void setHoraSaida(Date horaSaida);

	/**
	 * Gets the endereco transportadora.
	 *
	 * @return the endereco transportadora
	 */
	String getEnderecoTransportadora();

	/**
	 * Sets the endereco transportadora.
	 *
	 * @param enderecoTransportadora the new endereco transportadora
	 */
	void setEnderecoTransportadora(String enderecoTransportadora);

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	Long getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero            the numero to set
	 */
	void setNumero(Long numero);

	/**
	 * Gets the descricao serie.
	 *
	 * @return the descricao serie
	 */
	String getDescricaoSerie();

	/**
	 * Sets the descricao serie.
	 *
	 * @param descricaoSerie the new descricao serie
	 */
	void setDescricaoSerie(String descricaoSerie);

	/**
	 * Gets the codigo ANTT.
	 *
	 * @return the codigo ANTT
	 */
	Integer getCodigoANTT();

	/**
	 * Sets the codigo ANTT.
	 *
	 * @param codigoANTT the new codigo ANTT
	 */
	void setCodigoANTT(Integer codigoANTT);

	/**
	 * Gets the nome transportador.
	 *
	 * @return the nome transportador
	 */
	String getNomeTransportador();

	/**
	 * Sets the nome transportador.
	 *
	 * @param nomeTransportador the new nome transportador
	 */
	void setNomeTransportador(String nomeTransportador);

	/**
	 * Gets the insc estadual transportadora.
	 *
	 * @return the insc estadual transportadora
	 */
	String getInscEstadualTransportadora();

	/**
	 * Sets the insc estadual transportadora.
	 *
	 * @param inscEstadualTransportadora the new insc estadual transportadora
	 */
	void setInscEstadualTransportadora(String inscEstadualTransportadora);

	/**
	 * Gets the placa veiculo.
	 *
	 * @return the placa veiculo
	 */
	String getPlacaVeiculo();

	/**
	 * Sets the placa veiculo.
	 *
	 * @param placaVeiculo the new placa veiculo
	 */
	void setPlacaVeiculo(String placaVeiculo);

	/**
	 * Gets the uf placa.
	 *
	 * @return the uf placa
	 */
	String getUfPlaca();

	/**
	 * Sets the uf placa.
	 *
	 * @param ufPlaca the new uf placa
	 */
	void setUfPlaca(String ufPlaca);

	/**
	 * Gets the cpf CNPJ transportadora.
	 *
	 * @return the cpf CNPJ transportadora
	 */
	String getCpfCNPJTransportadora();

	/**
	 * Sets the cpf CNPJ transportadora.
	 *
	 * @param cpfCNPJTraspostadora the new cpf CNPJ transportadora
	 */
	void setCpfCNPJTransportadora(String cpfCNPJTraspostadora);

	/**
	 * Gets the municipio transportadora.
	 *
	 * @return the municipio transportadora
	 */
	String getMunicipioTransportadora();

	/**
	 * Sets the municipio transportadora.
	 *
	 * @param municipioTransportadora the new municipio transportadora
	 */
	void setMunicipioTransportadora(String municipioTransportadora);

	/**
	 * Gets the uf transportadora.
	 *
	 * @return the uf transportadora
	 */
	String getUfTransportadora();

	/**
	 * Sets the uf transportadora.
	 *
	 * @param ufTransportadora the new uf transportadora
	 */
	void setUfTransportadora(String ufTransportadora);

	/**
	 * Gets the valor total.
	 *
	 * @return the valorTotal
	 */
	BigDecimal getValorTotal();

	/**
	 * Sets the valor total.
	 *
	 * @param valorTotal            the valorTotal to set
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * Gets the nome cliente.
	 *
	 * @return the nomeCliente
	 */
	String getNomeCliente();

	/**
	 * Sets the nome cliente.
	 *
	 * @param nomeCliente            the nomeCliente to set
	 */
	void setNomeCliente(String nomeCliente);

	/**
	 * Gets the cpf cpnj.
	 *
	 * @return the cpfCpnj
	 */
	String getCpfCpnj();

	/**
	 * Sets the cpf cpnj.
	 *
	 * @param cpfCpnj            the cpfCpnj to set
	 */
	void setCpfCpnj(String cpfCpnj);

	/**
	 * Gets the inscricao estadual.
	 *
	 * @return the inscricaoEstadual
	 */
	String getInscricaoEstadual();

	/**
	 * Sets the inscricao estadual.
	 *
	 * @param inscricaoEstadual            the inscricaoEstadual to set
	 */
	void setInscricaoEstadual(String inscricaoEstadual);

	/**
	 * Gets the rg.
	 *
	 * @return the rg
	 */
	String getRg();

	/**
	 * Sets the rg.
	 *
	 * @param rg            the rg to set
	 */
	void setRg(String rg);

	/**
	 * Gets the endereco.
	 *
	 * @return the endereco
	 */
	String getEndereco();

	/**
	 * Sets the endereco.
	 *
	 * @param endereco            the endereco to set
	 */
	void setEndereco(String endereco);

	/**
	 * Gets the complemento.
	 *
	 * @return the complemento
	 */
	String getComplemento();

	/**
	 * Sets the complemento.
	 *
	 * @param complemento            the complemento to set
	 */
	void setComplemento(String complemento);

	/**
	 * Gets the bairro.
	 *
	 * @return the bairro
	 */
	String getBairro();

	/**
	 * Sets the bairro.
	 *
	 * @param bairro            the bairro to set
	 */
	void setBairro(String bairro);

	/**
	 * Gets the cep.
	 *
	 * @return the cep
	 */
	String getCep();

	/**
	 * Sets the cep.
	 *
	 * @param cep            the cep to set
	 */
	void setCep(String cep);

	/**
	 * Gets the municipio.
	 *
	 * @return the municipio
	 */
	String getMunicipio();

	/**
	 * Sets the municipio.
	 *
	 * @param municipio            the municipio to set
	 */
	void setMunicipio(String municipio);

	/**
	 * Gets the uf.
	 *
	 * @return the uf
	 */
	String getUf();

	/**
	 * Sets the uf.
	 *
	 * @param uf            the uf to set
	 */
	void setUf(String uf);

	/**
	 * Gets the apresentacao.
	 *
	 * @return the apresentacao
	 */
	Date getApresentacao();

	/**
	 * Sets the apresentacao.
	 *
	 * @param apresentacao            the apresentacao to set
	 */
	void setApresentacao(Date apresentacao);

	/**
	 * Gets the data emissao.
	 *
	 * @return the dataEmissao
	 */
	Date getDataEmissao();

	/**
	 * Sets the data emissao.
	 *
	 * @param dataEmissao            the dataEmissao to set
	 */
	void setDataEmissao(Date dataEmissao);

	/**
	 * Gets the descricao ponto consumo.
	 *
	 * @return the descricaoPontoConsumo
	 */
	String getDescricaoPontoConsumo();

	/**
	 * Sets the descricao ponto consumo.
	 *
	 * @param descricaoPontoConsumo            the descricaoPontoConsumo to set
	 */
	void setDescricaoPontoConsumo(String descricaoPontoConsumo);

	/**
	 * Gets the mensagem.
	 *
	 * @return the mensagem
	 */
	String getMensagem();

	/**
	 * Sets the mensagem.
	 *
	 * @param mensagem            the mensagem to set
	 */
	void setMensagem(String mensagem);

	/**
	 * Gets the tipo operacao.
	 *
	 * @return the tipoOperacao
	 */
	EntidadeConteudo getTipoOperacao();

	/**
	 * Sets the tipo operacao.
	 *
	 * @param tipoOperacao            the tipoOperacao to set
	 */
	void setTipoOperacao(EntidadeConteudo tipoOperacao);

	/**
	 * Gets the tipo faturamento.
	 *
	 * @return the tipoFaturamento
	 */
	EntidadeConteudo getTipoFaturamento();

	/**
	 * Sets the tipo faturamento.
	 *
	 * @param tipoFaturamento            the tipoFaturamento to set
	 */
	void setTipoFaturamento(EntidadeConteudo tipoFaturamento);

	/**
	 * Gets the natureza operacao CFOP.
	 *
	 * @return the naturezaOperacaoCFOP
	 */
	NaturezaOperacaoCFOP getNaturezaOperacaoCFOP();

	/**
	 * Sets the natureza operacao CFOP.
	 *
	 * @param naturezaOperacaoCFOP            the naturezaOperacaoCFOP to set
	 */
	void setNaturezaOperacaoCFOP(NaturezaOperacaoCFOP naturezaOperacaoCFOP);

	/**
	 * Gets the serie.
	 *
	 * @return the serie
	 */
	Serie getSerie();

	/**
	 * Sets the serie.
	 *
	 * @param serie the new serie
	 */
	void setSerie(Serie serie);

	/**
	 * Gets the tipo emissao nfe.
	 *
	 * @return the tipo emissao nfe
	 */
	EntidadeConteudo getTipoEmissaoNfe();

	/**
	 * Sets the tipo emissao nfe.
	 *
	 * @param tipoEmissaoNfe the new tipo emissao nfe
	 */
	void setTipoEmissaoNfe(EntidadeConteudo tipoEmissaoNfe);

	/**
	 * Gets the status nfe.
	 *
	 * @return the status nfe
	 */
	EntidadeConteudo getStatusNfe();

	/**
	 * Sets the status nfe.
	 *
	 * @param statusNfe the new status nfe
	 */
	void setStatusNfe(EntidadeConteudo statusNfe);

	/**
	 * Gets the chave acesso.
	 *
	 * @return the chave acesso
	 */
	String getChaveAcesso();

	/**
	 * Sets the chave acesso.
	 *
	 * @param chaveAcesso the new chave acesso
	 */
	void setChaveAcesso(String chaveAcesso);

	/**
	 * Gets the numero protocolo.
	 *
	 * @return the numero protocolo
	 */
	String getNumeroProtocolo();

	/**
	 * Sets the numero protocolo.
	 *
	 * @param numeroProtocolo the new numero protocolo
	 */
	void setNumeroProtocolo(String numeroProtocolo);

	/**
	 * Gets the indicador tipo nota.
	 *
	 * @return the indicador tipo nota
	 */
	Integer getIndicadorTipoNota();

	/**
	 * Sets the indicador tipo nota.
	 *
	 * @param indicadorTipoNota the new indicador tipo nota
	 */
	void setIndicadorTipoNota(Integer indicadorTipoNota);

	/**
	 * Recupera o arquivo xml referente à NFe emitida.
	 *
	 * @return o arquivo xml
	 */
	byte[] getArquivoXml();

	/**
	 * Altera o arquivo xml
	 *
	 * @param arquivoXml the new indicador tipo nota
	 */
	void setArquivoXml(byte[] arquivoXml);

	/**
	 * Checks if is tipo operacao entrada.
	 *
	 * @return true, if is tipo operacao entrada
	 */
	boolean isTipoOperacaoEntrada();

	/**
	 * Verifica se a nota foi emitida pelo cliente
	 * @return true se a nota foi emitida pelo cliente e false caso contrário
	 */
	Integer getEmissaoCliente();

	/**
	 * Define se a nota foi emitida pelo cliente
	 * @param emissaoCliente true se a nota foi emitida pelo cliente false caso contrário
	 */
	void setEmissaoCliente(Integer emissaoCliente);
}
