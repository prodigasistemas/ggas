/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Inteface responsável pela assinatura dos métodos relacionados 
 * aos itens de fatura
 *
 */
public interface FaturaItem extends EntidadeNegocio, Cloneable {

	String BEAN_ID_FATURA_ITEM = "faturaItem";

	String RUBRICA = "ROTULO_FATURA_ITEM_RUBRICA";

	String QUANTIDADE = "ROTULO_FATURA_ITEM_QUANTIDADE";

	String VALOR_UNITARIO = "ROTULO_FATURA_ITEM_VALOR_UNITARIO";

	/**
	 * @return NaturezaOperacaoCFOP - Retorna objeto natureza operação CFOP.
	 */
	NaturezaOperacaoCFOP getNaturezaOperacaoCFOP();

	/**
	 * @param naturezaOperacaoCFOP - Set natureza operação CFOP.
	 */
	void setNaturezaOperacaoCFOP(NaturezaOperacaoCFOP naturezaOperacaoCFOP);

	/**
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 *            the fatura to set
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return the tarifa
	 */
	Tarifa getTarifa();

	/**
	 * @param tarifa
	 *            the tarifa to set
	 */
	void setTarifa(Tarifa tarifa);

	/**
	 * @return the rubrica
	 */
	Rubrica getRubrica();

	/**
	 * @param rubrica
	 *            the rubrica to set
	 */
	void setRubrica(Rubrica rubrica);

	/**
	 * @return the creditoDebitoARealizar
	 */
	CreditoDebitoARealizar getCreditoDebitoARealizar();

	/**
	 * @param creditoDebitoARealizar
	 *            the creditoDebitoARealizar to
	 *            set
	 */
	void setCreditoDebitoARealizar(CreditoDebitoARealizar creditoDebitoARealizar);

	/**
	 * @return the quantidade
	 */
	BigDecimal getQuantidade();

	/**
	 * @param quantidade
	 *            the quantidade to set
	 */
	void setQuantidade(BigDecimal quantidade);

	/**
	 * @return the valorTotal
	 */
	BigDecimal getValorTotal();

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * @return the valorUnitario
	 */
	BigDecimal getValorUnitario();

	/**
	 * @param valorUnitario
	 *            the valorUnitario to set
	 */
	void setValorUnitario(BigDecimal valorUnitario);

	/**
	 * @return the quantidadeTotalPrestacao
	 */
	Integer getQuantidadeTotalPrestacao();

	/**
	 * @param quantidadeTotalPrestacao
	 *            the quantidadeTotalPrestacao to
	 *            set
	 */
	void setQuantidadeTotalPrestacao(Integer quantidadeTotalPrestacao);

	/**
	 * @return the medidaConsumo
	 */
	BigDecimal getMedidaConsumo();

	/**
	 * @param medidaConsumo
	 *            the medidaConsumo to set
	 */
	void setMedidaConsumo(BigDecimal medidaConsumo);

	/**
	 * @param numeroSequencial - Set número sequencial.
	 */
	void setNumeroSequencial(Integer numeroSequencial);

	/**
	 * @return Integer- Retorna número sequencial.
	 */
	Integer getNumeroSequencial();

	/**
	 * @return Segmento - Retorna objeto segmento.
	 */
	Segmento getSegmento();

	/**
	 * @param segmento - Set segmento.
	 */
	void setSegmento(Segmento segmento);

	/**
	 * Clona a fatura
	 * @return a fatura clonada
	 */
	FaturaItem clone();

	//-------------- Faixa Tarifa

	/**
	 * Gets valorFixo
	 * @return valorFixo
	 */
	BigDecimal getValorFixoFaixaTarifa();

	/**
	 * Sets valorFixo
	 * @param valorFixo valorFixo
	 */
	void setValorFixoFaixaTarifa(BigDecimal valorFixo);

	/**
	 * Gets valorVariavel
	 * @return valorVariavel
	 */
	BigDecimal getValorVariavelFaixaTarifa();

	/**
	 * Sets valorVariavel
	 * @param valorVariavel valorVariavel
	 */
	void setValorVariavelFaixaTarifa(BigDecimal valorVariavel);

	/**
	 * Gets volumeInicial
	 * @return volumeInicial
	 */
	BigDecimal getVolumeInicialFaixaTarifa();

	/**
	 * Sets volumeInicial
	 * @param volumeInicial volumeInicial
	 */
	void setVolumeInicialFaixaTarifa(BigDecimal volumeInicial);

	/**
	 * Gets volumeFinal
	 * @return volumeFinal
	 */
	BigDecimal getVolumeFinalFaixaTarifa();

	/**
	 * Sets volumeFinal
	 * @param volumeFinal volumeFinal
	 */
	void setVolumeFinalFaixaTarifa(BigDecimal volumeFinal);
}
