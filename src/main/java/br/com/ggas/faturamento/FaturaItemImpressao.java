/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados 
 * a impressão do item fatura
 */
public interface FaturaItemImpressao extends EntidadeNegocio {

	String BEAN_ID_FATURA_ITEM_IMPRESSAO = "faturaItemImpressao";

	String FATURA_ITEM = "FATURA_ITEM";

	String TARIFA = "TARIFA";

	String DATA_INICIAL = "FATURA_ITEM_IMPRESSAO_DATA_INICIAL";

	String DATA_FINAL = "FATURA_ITEM_IMPRESSAO_DATA_FINAL";

	String QUANTIDADE_DIAS = "FATURA_ITEM_IMPRESSAO_QUANTIDADE_DIAS";

	String CONSUMO = "FATURA_ITEM_IMPRESSAO_CONSUMO";

	String VALOR_UNITARIO = "FATURA_ITEM_IMPRESSAO_VALOR_UNITARIO";

	String VALOR_TOTAL = "FATURA_ITEM_IMPRESSAO_VALOR_TOTAL";

	String DESCRICAO_DESCONTO = "FATURA_ITEM_IMPRESSAO_DESCRICAO_DESCONTO";

	/**
	 * @return o FaturaItem
	 */
	FaturaItem getFaturaItem();

	/**
	 * @param faturaItem - Set Fatura item.
	 */
	void setFaturaItem(FaturaItem faturaItem);

	/**
	 * @return o Tarifa
	 */
	Tarifa getTarifa();

	/**
	 * @param tarifa - Set tarifa.
	 */
	void setTarifa(Tarifa tarifa);

	/**
	 * @return a dataInicial
	 */
	Date getDataInicial();

	/**
	 * @param dataInicial - Set Data final.
	 */
	void setDataInicial(Date dataInicial);

	/**
	 * @return a dataFinal
	 */
	Date getDataFinal();

	/**
	 * @param dataFinal - Set Data final.
	 */
	void setDataFinal(Date dataFinal);

	/**
	 * @return a quantidadeDias
	 */
	Integer getQuantidadeDias();
	
	/**
	 * @param quantidadeDias - Set Quantidade de dias.
	 */
	void setQuantidadeDias(Integer quantidadeDias);

	/**
	 * @return o consumo
	 */
	BigDecimal getConsumo();

	/**
	 * @param consumo - Set consumo.
	 */
	void setConsumo(BigDecimal consumo);

	/**
	 * @return o valorUnitario
	 */
	BigDecimal getValorUnitario();

	/**
	 * @param valorUnitario - SEt Valor unitário.
	 */
	void setValorUnitario(BigDecimal valorUnitario);

	/**
	 * @return o valorTotal
	 */
	BigDecimal getValorTotal();

	/**
	 * @param valorTotal - Set Valor total.
	 */
	void setValorTotal(BigDecimal valorTotal);

	/**
	 * @return a descricaoDesconto
	 */
	String getDescricaoDesconto();

	/**
	 * @param descricaoDesconto - Set Descrição de desconto.
	 */
	void setDescricaoDesconto(String descricaoDesconto);

	/**
	 * @return o indicadorDesconto
	 */
	public Boolean getIndicadorDesconto();

	/**
	 * @param indicadorDesconto
	 *            the indicadorDesconto to set
	 */
	public void setIndicadorDesconto(Boolean indicadorDesconto);
}
