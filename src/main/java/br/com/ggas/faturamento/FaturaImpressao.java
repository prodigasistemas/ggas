/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.rota.Rota;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a impressão de fatura
 *
 */
public interface FaturaImpressao extends EntidadeNegocio {

	String BEAN_ID_FATURA_IMPRESSAO = "faturaImpressao";

	/**
	 * @return the grupoFaturamento
	 */
	GrupoFaturamento getGrupoFaturamento();

	/**
	 * @param grupoFaturamento
	 *            the grupoFaturamento to set
	 */
	void setGrupoFaturamento(GrupoFaturamento grupoFaturamento);

	/**
	 * @return the fatura
	 */
	Fatura getFatura();

	/**
	 * @param fatura
	 *            the fatura to set
	 */
	void setFatura(Fatura fatura);

	/**
	 * @return the faturamentoAnormalidade
	 */
	FaturamentoAnormalidade getFaturamentoAnormalidade();

	/**
	 * @param faturamentoAnormalidade
	 *            the faturamentoAnormalidade to
	 *            set
	 */
	void setFaturamentoAnormalidade(FaturamentoAnormalidade faturamentoAnormalidade);

	/**
	 * @return the usuario
	 */
	Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	void setUsuario(Usuario usuario);

	/**
	 * @return the dataGeracao
	 */
	Date getDataGeracao();

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	void setDataGeracao(Date dataGeracao);

	/**
	 * @return the dataImpressao
	 */
	Date getDataImpressao();

	/**
	 * @param dataImpressao
	 *            the dataImpressao to set
	 */
	void setDataImpressao(Date dataImpressao);

	/**
	 * @return the anoMesReferencia
	 */
	Integer getAnoMesReferencia();

	/**
	 * @param anoMesReferencia
	 *            the anoMesReferencia to set
	 */
	void setAnoMesReferencia(Integer anoMesReferencia);

	/**
	 * @return the valor
	 */
	BigDecimal getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	void setValor(BigDecimal valor);

	/**
	 * @return the numeroLote
	 */
	Integer getNumeroLote();

	/**
	 * @param numeroLote
	 *            the numeroLote to set
	 */
	void setNumeroLote(Integer numeroLote);

	/**
	 * @return the sequenciaImpressao
	 */
	Integer getSequenciaImpressao();

	/**
	 * @param sequenciaImpressao
	 *            the sequenciaImpressao to set
	 */
	void setSequenciaImpressao(Integer sequenciaImpressao);

	/**
	 * @return the indicadorImpressao
	 */
	Boolean getIndicadorImpressao();

	/**
	 * @param indicadorImpressao
	 *            the indicadorImpressao to set
	 */
	void setIndicadorImpressao(Boolean indicadorImpressao);

	/**
	 * @return the formaCobranca
	 */
	EntidadeConteudo getFormaCobranca();

	/**
	 * @param formaCobranca
	 *            the formaCobranca to set
	 */
	void setFormaCobranca(EntidadeConteudo formaCobranca);

	/**
	 * @return the rota
	 */
	Rota getRota();

	/**
	 * @param rota
	 *            the rota to set
	 */
	void setRota(Rota rota);
}
