
package br.com.ggas.faturamento.cronograma.impl;

import java.util.Date;

import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.Rota;
/**
 * 
 * Classe responsável pelo VO
 * de CronogramaRota
 *
 */
public class CronogramaRotaVO {

	private Long chavePrimaria;

	private Rota rota;

	private Date dataPrevista;

	private Date dataRealizacao;

	private Leiturista leituristaAnterior;

	private Leiturista leituristaAtual;

	private Leiturista leituristaSuplente;

	public Leiturista getLeituristaAtual() {

		return leituristaAtual;
	}

	public void setLeituristaAtual(Leiturista leituristaAtual) {

		this.leituristaAtual = leituristaAtual;
	}

	public Leiturista getLeituristaSuplente() {

		return leituristaSuplente;
	}

	public void setLeituristaSuplente(Leiturista leituristaSuplente) {

		this.leituristaSuplente = leituristaSuplente;
	}

	public Rota getRota() {

		return rota;
	}

	public void setRota(Rota rota) {

		this.rota = rota;
	}

	public Date getDataPrevista() {
		Date data = null;
		if (this.dataPrevista != null) {
			data = (Date) dataPrevista.clone();
		}
		return data;
	}

	public void setDataPrevista(Date dataPrevista) {
		if(dataPrevista != null) {
			this.dataPrevista = (Date) dataPrevista.clone();
		} else {
			this.dataPrevista = null;
		}
	}

	public Date getDataRealizacao() {
		Date data = null;
		if (this.dataRealizacao != null) {
			data = (Date) dataRealizacao.clone();
		}
		return data;
	}

	public void setDataRealizacao(Date dataRealizacao) {
		if (dataRealizacao != null) {
			this.dataRealizacao = (Date) dataRealizacao.clone();
		} else {
			this.dataRealizacao = null;
		}
	}

	public Leiturista getLeituristaAnterior() {

		return leituristaAnterior;
	}

	public void setLeituristaAnterior(Leiturista leituristaAnterior) {

		this.leituristaAnterior = leituristaAnterior;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

}
