/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.cronograma.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * CronogramaAtividadeFaturamentoImpl
 * 
 * @author gmatos
 * 
 * Classe reponsável por Cronograma Atividade Faturamento.
 * 
 */
public class CronogramaAtividadeFaturamentoImpl extends EntidadeNegocioImpl implements CronogramaAtividadeFaturamento {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 3892228709328638568L;

	private CronogramaFaturamento cronogramaFaturamento;

	private AtividadeSistema atividadeSistema;

	private Date dataInicio;

	private Date dataFim;

	private Date dataRealizacao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getdataRealizacaoFormatada() {

		if(dataRealizacao != null) {
			return Util.converterDataParaStringSemHora(dataRealizacao, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getdataInicioFormatada() {

		if(dataInicio != null) {
			return Util.converterDataParaStringSemHora(dataInicio, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getdataFimFormatada() {

		if(dataInicio != null) {
			return Util.converterDataParaStringSemHora(dataFim, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaAtividadeFaturamento
	 * #getCronogramaFaturamento()
	 */
	@Override
	public CronogramaFaturamento getCronogramaFaturamento() {

		return cronogramaFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaAtividadeFaturamento
	 * #setCronogramaFaturamento
	 * (br.com.ggas.faturamento
	 * .cronograma.CronogramaFaturamento)
	 */
	@Override
	public void setCronogramaFaturamento(CronogramaFaturamento cronogramaFaturamento) {

		this.cronogramaFaturamento = cronogramaFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaAtividadeFaturamento
	 * #getAtividadeSistema()
	 */
	@Override
	public AtividadeSistema getAtividadeSistema() {

		return atividadeSistema;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaAtividadeFaturamento
	 * #setAtividadeSistema
	 * (br.com.ggas.faturamento
	 * .cronograma.AtividadeSistema)
	 */
	@Override
	public void setAtividadeSistema(AtividadeSistema atividadeSistema) {

		this.atividadeSistema = atividadeSistema;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.cronograma. CronogramaAtividadeFaturamento
	 * #getDataInicio()
	 */
	@Override
	public Date getDataInicio() {
		Date data = null;
		if (this.dataInicio != null) {
			data = (Date) dataInicio.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * CronogramaAtividadeFaturamento
	 * #setDataInicio(java.util.Date)
	 */
	@Override
	public void setDataInicio(Date dataInicio) {
		if(dataInicio != null) {
			this.dataInicio = (Date) dataInicio.clone();
		} else {
			this.dataInicio = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.cronograma.
	 * CronogramaAtividadeFaturamento#getDataFim()
	 */
	@Override
	public Date getDataFim() {
		Date data = null;
		if (this.dataFim != null) {
			data = (Date) dataFim.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.cronograma. CronogramaAtividadeFaturamento
	 * #setDataFim(java.util.Date)
	 */
	@Override
	public void setDataFim(Date dataFim) {
		if (dataFim != null) {
			this.dataFim = (Date) dataFim.clone();
		} else {
			this.dataFim = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaAtividadeFaturamento #getDataRealizacao()
	 */
	@Override
	public Date getDataRealizacao() {
		Date data = null;
		if (this.dataRealizacao != null) {
			data = (Date) dataRealizacao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaAtividadeFaturamento #setDataRealizacao(java.util.Date)
	 */
	@Override
	public void setDataRealizacao(Date dataRealizacao) {
		if (dataRealizacao != null) {
			this.dataRealizacao = (Date) dataRealizacao.clone();
		} else {
			this.dataRealizacao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		String camposObrigatorios = null;

		if(cronogramaFaturamento == null) {
			sb.append(CronogramaFaturamento.ROTULO_CRONOGRAMA_FATURAMENTO);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(atividadeSistema == null) {
			sb.append(AtividadeSistema.ROTULO_ATIVIDADE_SISTEMA);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = sb.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, sb.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}
}
