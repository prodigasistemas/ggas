/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.cronograma;

import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a atividade de sistema
 *
 */
public interface AtividadeSistema extends EntidadeNegocio {

	/**
	 * CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA
	 */
	static final long CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA = 1;

	/**
	 * CODIGO_ATIVIDADE_REALIZAR_LEITURA
	 */
	static final long CODIGO_ATIVIDADE_REALIZAR_LEITURA = 2;

	/**
	 * CODIGO_ATIVIDADE_RETORNAR_LEITURA
	 */
	static final long CODIGO_ATIVIDADE_RETORNAR_LEITURA = 3;

	/**
	 * CODIGO_ATIVIDADE_CONSISTIR_LEITURA
	 */
	static final long CODIGO_ATIVIDADE_CONSISTIR_LEITURA = 4;

	/**
	 * CODIGO_ATIVIDADE_SIMULAR_FATURAMENTO
	 */
	static final long CODIGO_ATIVIDADE_SIMULAR_FATURAMENTO = 6;

	/**
	 * CODIGO_ATIVIDADE_FATURAR
	 */
	static final long CODIGO_ATIVIDADE_FATURAR = 8;

	/**
	 * CODIGO_ATIVIDADE_EMITIR_FATURA
	 */
	static final long CODIGO_ATIVIDADE_EMITIR_FATURA = 9;
	
	/**
	 * BEAN_ID_ATIVIDADE_SISTEMA
	 */
	String BEAN_ID_ATIVIDADE_SISTEMA = "atividadeSistema";

	/**
	 * ROTULO_ATIVIDADE_SISTEMA
	 */
	String ROTULO_ATIVIDADE_SISTEMA = "ATIVIDADE_SISTEMA";

	/**
	 * @return the operacao
	 */
	Operacao getOperacao();

	
	/**
	 * @param operacao
	 *            the operacao to set
	 */
	void setOperacao(Operacao operacao);
	
	/**
	 * @return the modulo
	 */
	Modulo getModulo();

	/**
	 * @param modulo
	 *            the modulo to set
	 */

	void setModulo(Modulo modulo);
	
	
	/**
	 * @return the atividadeSistema
	 */
	AtividadeSistema getAtividadeSistema();

	/**
	 * @param atividadeSistema
	 *            the atividadeSistema to set
	 */
	void setAtividadeSistema(AtividadeSistema atividadeSistema);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the sequencia
	 */
	Integer getSequencia();

	/**
	 * @param sequencia
	 *            the sequencia to set
	 */
	void setSequencia(Integer sequencia);

	/**
	 * @return the diasIntervalo
	 */
	Integer getDiasIntervalo();

	/**
	 * @param diasIntervalo
	 *            the diasIntervalo to set
	 */
	void setDiasIntervalo(Integer diasIntervalo);

	/**
	 * @return the diasDuracao
	 */
	Integer getDiasDuracao();

	/**
	 * @param diasDuracao
	 *            the diasDuracao to set
	 */
	void setDiasDuracao(Integer diasDuracao);

	/**
	 * @return the obrigatoria
	 */
	boolean isObrigatoria();

	/**
	 * @param obrigatoria
	 *            the obrigatoria to set
	 */
	void setObrigatoria(boolean obrigatoria);

	/**
	 * @return the agendada
	 */
	boolean isAgendada();

	/**
	 * @param agendada
	 *            the agendada to set
	 */
	void setAgendada(boolean agendada);

	/**
	 * @return the externa
	 */
	boolean isExterna();

	/**
	 * @param externa
	 *            the externa to set
	 */
	void setExterna(boolean externa);

	/**
	 * @return the detalhaRota
	 */
	boolean isDetalhaRota();

	/**
	 * @param detalhaRota
	 *            the detalhaRota to set
	 */
	void setDetalhaRota(boolean detalhaRota);

	/**
	 * @return the cronograma
	 */
	boolean isCronograma();

	/**
	 * @param cronograma
	 *            the cronograma to set
	 */
	void setCronograma(boolean cronograma);

	/**
	 * @return the horaInicial
	 */
	Integer getHoraInicial();

	/**
	 * @param horaInicial
	 *            the horaInicial to set
	 */
	void setHoraInicial(Integer horaInicial);

	/**
	 * @return the horaFinal
	 */
	Integer getHoraFinal();

	/**
	 * @param horaFinal
	 *            the horaFinal to set
	 */
	void setHoraFinal(Integer horaFinal);

	/**
	 * @return the email
	 */
	String getEmail();

	/**
	 * @param email
	 *            the email to set
	 */
	void setEmail(String email);

	/**
	 * @return the enviaEmail
	 */
	boolean isEnviaEmail();

	/**
	 * @param enviaEmail
	 *            the enviaEmail to set
	 */
	void setEnviaEmail(boolean enviaEmail);

	/**
	 * @return the AtividadeSistema
	 */
	public AtividadeSistema getAtividadePrecedente();

	/**
	 * @param atividadePrecedente
	 *            the atividadePrecedente to set
	 */
	public void setAtividadePrecedente(AtividadeSistema atividadePrecedente);

	/**
	 * @return the descricaoEmailRemetente
	 */
	String getDescricaoEmailRemetente();

	/**
	 * @param descricaoEmailRemetente
	 *            the descricaoEmailRemetente to
	 *            set
	 */
	void setDescricaoEmailRemetente(String descricaoEmailRemetente);

}
