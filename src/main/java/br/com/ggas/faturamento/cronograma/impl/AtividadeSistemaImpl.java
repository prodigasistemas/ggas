/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.cronograma.impl;

import java.util.Map;

import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * AtividadeSistemaImpl
 * 
 * @author ralencar
 * 
 * Classe responsável por Atividade Sistema.
 * 
 */
public class AtividadeSistemaImpl extends EntidadeNegocioImpl implements AtividadeSistema {

	private static final long serialVersionUID = -7610914652165494712L;

	private Operacao operacao;
	
	private Modulo modulo;

	private AtividadeSistema atividadeSistema;

	private AtividadeSistema atividadePrecedente;

	private String descricao;

	private Integer sequencia;

	private Integer diasIntervalo;

	private Integer diasDuracao;

	private boolean enviaEmail;

	private boolean obrigatoria;

	private boolean agendada;

	private boolean externa;

	private boolean detalhaRota;

	private boolean cronograma;

	private Integer horaInicial;

	private Integer horaFinal;

	private String email;

	private String descricaoEmailRemetente;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getOperacao()
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setOperacao(br.com.ggas.controleacesso
	 * .Operacao)
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setModulo(br.com.ggas.controleacesso
	 * .Modulo)
	 */
	@Override
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #getModulo(br.com.ggas.controleacesso
	 * .Modulo)
	 */
	@Override
	public Modulo getModulo() {

		return modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema#getAtividadeSistema()
	 */
	@Override
	public AtividadeSistema getAtividadeSistema() {

		return atividadeSistema;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema
	 * #setAtividadeSistema(br.com.
	 * ggas.faturamento
	 * .cronograma.AtividadeSistema)
	 */
	@Override
	public void setAtividadeSistema(AtividadeSistema atividadeSistema) {

		this.atividadeSistema = atividadeSistema;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema#isEnviaEmail()
	 */
	@Override
	public boolean isEnviaEmail() {

		return enviaEmail;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema#setEnviaEmail(boolean)
	 */
	@Override
	public void setEnviaEmail(boolean enviaEmail) {

		this.enviaEmail = enviaEmail;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getSequencia()
	 */
	@Override
	public Integer getSequencia() {

		return sequencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setSequencia(java.lang.Integer)
	 */
	@Override
	public void setSequencia(Integer sequencia) {

		this.sequencia = sequencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getDiasIntervalo()
	 */
	@Override
	public Integer getDiasIntervalo() {

		return diasIntervalo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setDiasIntervalo(java.lang.Integer)
	 */
	@Override
	public void setDiasIntervalo(Integer diasIntervalo) {

		this.diasIntervalo = diasIntervalo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getDiasDuracao()
	 */
	@Override
	public Integer getDiasDuracao() {

		return diasDuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setDiasDuracao(java.lang.Integer)
	 */
	@Override
	public void setDiasDuracao(Integer diasDuracao) {

		this.diasDuracao = diasDuracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#isObrigatoria()
	 */
	@Override
	public boolean isObrigatoria() {

		return obrigatoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#setObrigatoria(boolean)
	 */
	@Override
	public void setObrigatoria(boolean obrigatoria) {

		this.obrigatoria = obrigatoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#isAgendada()
	 */
	@Override
	public boolean isAgendada() {

		return agendada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#setAgendada(boolean)
	 */
	@Override
	public void setAgendada(boolean agendada) {

		this.agendada = agendada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#isExterna()
	 */
	@Override
	public boolean isExterna() {

		return externa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#setExterna(boolean)
	 */
	@Override
	public void setExterna(boolean externa) {

		this.externa = externa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#isDetalhaRota()
	 */
	@Override
	public boolean isDetalhaRota() {

		return detalhaRota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#setDetalhaRota(boolean)
	 */
	@Override
	public void setDetalhaRota(boolean detalhaRota) {

		this.detalhaRota = detalhaRota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#isCronograma()
	 */
	@Override
	public boolean isCronograma() {

		return cronograma;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#setCronograma(boolean)
	 */
	@Override
	public void setCronograma(boolean cronograma) {

		this.cronograma = cronograma;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getHoraInicial()
	 */
	@Override
	public Integer getHoraInicial() {

		return horaInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setHoraInicial(java.lang.Integer)
	 */
	@Override
	public void setHoraInicial(Integer horaInicial) {

		this.horaInicial = horaInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getHoraFinal()
	 */
	@Override
	public Integer getHoraFinal() {

		return horaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setHoraFinal(java.lang.Integer)
	 */
	@Override
	public void setHoraFinal(Integer horaFinal) {

		this.horaFinal = horaFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema#getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .AtividadeSistema
	 * #setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema#getAtividadePrecedente()
	 */
	@Override
	public AtividadeSistema getAtividadePrecedente() {

		return atividadePrecedente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema
	 * #setAtividadePrecedente(br.com
	 * .ggas.faturamento
	 * .cronograma.AtividadeSistema)
	 */
	@Override
	public void setAtividadePrecedente(AtividadeSistema atividadePrecedente) {

		this.atividadePrecedente = atividadePrecedente;
	}

	@Override
	public String getDescricaoEmailRemetente() {

		return descricaoEmailRemetente;
	}

	@Override
	public void setDescricaoEmailRemetente(String descricaoEmailRemetente) {

		this.descricaoEmailRemetente = descricaoEmailRemetente;
	}

}
