/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.cronograma;


import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.rota.Rota;

/**
 * ControladorCronogramaAtividadeFaturamento
 * 
 * @author ralencar
 * 
 * Classe responsável por Controlador Cronograma Atividade Faturamento.
 * 
 */
public interface ControladorCronogramaAtividadeFaturamento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CRONOGRAMA_ATIVIDADE_FATURAMENTO = "controladorCronogramaAtividadeFaturamento";

	/**
	 * Método responsável por listar atividades do sistema pela operação passada por parãmetro.
	 * @return lista de Atividades Sistema
	 * @throws NegocioException the negocio exception
	 */
	Collection<CronogramaAtividadeFaturamento> listarCronogramaAtividadeFaturamentos() throws NegocioException;

	/**
	 * Consultar cronograma atividade faturamento.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(Map<String, Object> filtro)
					throws NegocioException;

	/**
	 * Consultar Cronograma Faturamento por Grupo de Faturamento, Ano/Mes e Ciclo
	 *
	 * @param grupoFaturamento - {@link Long}
	 * @param anoMesreferencia - {@link Integer}
	 * @param numerociclo - {@link Integer}
	 * @return cronogramaFaturamento - {@link CronogramaFaturamento}
	 * @throws NegocioException - the negocio exception
	 */
	CronogramaFaturamento consultarCronogramaPorGrupoFaturamentoAnoMesCiclo(Long grupoFaturamento,
			Integer anoMesreferencia, Integer numerociclo) throws NegocioException;

	/**
	 * Obtém um cronograma atividade faturamento a partir da chave da rota e da chave da atividade do sistema
	 *
	 * @param grupoFaturamento chave primária do grupo de faturamento
	 * @param atvidadeSistema  chave primária da atividade sistema
	 * @param anoMesReferencia informa o ano mes / referencia
	 * @return retorna o cronograma atividade faturamento
	 */
	Optional<CronogramaAtividadeFaturamento> obterCrononogramaAtividadePorRotaAtividade(Long grupoFaturamento, Long atvidadeSistema,
			Integer anoMesReferencia);

	/**
	 * Obtém um cronograma atividade faturamento a partir da chave da rota, da chave da atividade do sistema e do cronograma
	 *
	 * @param grupoFaturamento chave primária do grupo de faturamento
	 * @param atvidadeSistema chave primária da atividade sistema
	 * @param anoMesReferencia informa o ano mes / referencia
	 * @param cronograma chave primaria do cronograma
	 * @return retorna o cronograma atividade faturamento
	 */
	Optional<CronogramaAtividadeFaturamento> obterCrononogramaAtividadePorRotaAtividade(Long grupoFaturamento, Long atvidadeSistema,
			Integer anoMesReferencia, Long cronograma);

	/**
	 * Atualiza um {@link CronogramaAtividadeFaturamento} com a data de realização para a hora atual,
	 * caso exista algum para os parametros informados
	 *
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param atividadeSistema chave da atividade de sistema
	 * @param anoMesReferencia ano mes de referencia
	 */
	void atualizarAtividadeFaturamentoSeExistir(Long grupoFaturamento, Integer atividadeSistema, Integer anoMesReferencia);

	/**
	 * Atualiza atividades de faturamento se existir
	 *
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param atividadeSistema chave da atividade de sistema
	 * @param anoMesReferencia ano mes de referencia
	 * @param cronograma chave primaria do cronograma
	 */
	void atualizarAtividadeFaturamentoSeExistir(Long grupoFaturamento, Integer atividadeSistema,
			Integer anoMesReferencia, Long cronograma, StringBuilder logProcessamento);

	/**
	 * Consulta Quantidade de Pontos de Consumo Emissao Fatura
	 *
	 * @param grupoFaturamento - {@link Long}
	 * @param anoMesFaturamento - {@link Integer}
	 * @param ciclo - {@link Integer}
	 * @param cronograma - {@link Long}
	 * @param atividade -{@link Long}
	 * @return Quantidade de Pontos de Consumo - {@link Long}
	 */
	Long consultarQuantidadePontosConsumoEmissaoFatura(Long grupoFaturamento, Integer anoMesFaturamento, Integer ciclo,
			Long cronograma, Long atividade);

	/**
	 * Obtem um CronogramaAtividadeFaturamento a partir de um cronograma e uma atividade
	 * @param idCronogramaFaturamento A chave primária do cronograma
	 * @param idAtividadeSistema A chave primária da atividade
	 * @return O objeto CronogramaAtividadeFaturamento referente ao cronograma e à atividade informada.
	 */
	CronogramaAtividadeFaturamento obterCronogramaAtividadeFaturamento(Long idCronogramaFaturamento, Long idAtividadeSistema);
	
	/**
	 * Atualiza a data de realizacao da atividade para null
	 * @param idAtividade A chave primária da atividade do cronograma
	 * @param rota A rota a ser desfeita
	 */
	void desfazerAtividadeCronograma(Long idAtividade, Rota rota);
	
	/**
	 * Consulta a atividade do cronograma de um determinado grupo
	 * @param idAtividade A chave primária da atividade do cronograma
	 * @param grupoFaturamento O grupo de faturamento
	 * @return A atividade - {@link CronogramaAtividadeFaturamento}}
	 */
	public CronogramaAtividadeFaturamento consultarAtividadeCronograma(Long idAtividade, GrupoFaturamento grupoFaturamento);

	Long obterQuantidadeRotaCronogramaAtualizado(Long cronogramaAtividadeFaturamento, Integer atividadeSistema);
	
}
