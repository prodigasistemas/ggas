/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.cronograma.impl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.geral.exception.GGASRuntimeException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.CronogramaRotaImpl;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;

/**
 * @author ralencar
 */
class ControladorCronogramaAtividadeFaturamentoImpl extends ControladorNegocioImpl implements ControladorCronogramaAtividadeFaturamento {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeAtividadeCronogramaFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorSegmento
	 * #
	 * consultarCronogramaFaturamento(java.util.Map
	 * )
	 */
	@Override
	@SuppressWarnings({"unchecked", "squid:S1192"})
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(Map<String, Object> filtro)
					throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {

			criteria.createAlias("atividadeSistema", "atividadeSistema");
			criteria.createAlias("cronogramaFaturamento", "cronogramaFaturamento");

			Long codigoAtividadeSistema = (Long) filtro.get("chavePrimaria");
			if(codigoAtividadeSistema != null && codigoAtividadeSistema > 0) {
				criteria.add(Restrictions.eq("chavePrimaria", codigoAtividadeSistema));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #listarSegmento()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaAtividadeFaturamento> listarCronogramaAtividadeFaturamentos() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Obter cronograma atividade faturamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the cronograma atividade faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public CronogramaAtividadeFaturamento obterCronogramaAtividadeFaturamento(Long chavePrimaria) throws NegocioException {

		return (CronogramaAtividadeFaturamento) this.obter(chavePrimaria, getClasseEntidadeAtividadeCronogramaFaturamento());

	}

	@Override
	@SuppressWarnings("squid:S1192")
	public Optional<CronogramaAtividadeFaturamento> obterCrononogramaAtividadePorRotaAtividade(Long grupoFaturamento, Long atividadeSistema,
			Integer anoMesReferencia) {
		return createHQLQuery("select c from CronogramaAtividadeFaturamentoImpl c inner join c.cronogramaFaturamento cf "
				+ "where c.atividadeSistema.chavePrimaria = :atividadeSistema and cf.grupoFaturamento.chavePrimaria = :grupoFaturamento "
				+ "and cf.anoMesFaturamento = :anoMesReferencia")
				.setLong("atividadeSistema", atividadeSistema)
				.setLong("grupoFaturamento", grupoFaturamento)
				.setParameter("anoMesReferencia", anoMesReferencia)
				.list().stream().findFirst();
	}

	@Override
	@SuppressWarnings("squid:S1192")
	public Optional<CronogramaAtividadeFaturamento> obterCrononogramaAtividadePorRotaAtividade(Long grupoFaturamento, Long atividadeSistema,
																							   Integer anoMesReferencia,Long cronograma) {
		return createHQLQuery("select c from CronogramaAtividadeFaturamentoImpl c inner join c.cronogramaFaturamento cf "
				+ "where c.atividadeSistema.chavePrimaria = :atividadeSistema and cf.grupoFaturamento.chavePrimaria = :grupoFaturamento "
				+ "and cf.anoMesFaturamento = :anoMesReferencia and c.cronogramaFaturamento.chavePrimaria = :cronograma")
				.setLong("atividadeSistema", atividadeSistema)
				.setLong("grupoFaturamento", grupoFaturamento)
				.setParameter("anoMesReferencia", anoMesReferencia)
				.setParameter("cronograma", cronograma)
				.list().stream().findFirst();
	}
	
	
	@Override
	public Long obterQuantidadeRotaCronogramaAtualizado(Long cronogramaAtividadeFaturamento, Integer atividadeSistema) {
		return (Long) createHQLQuery("select count(rocr) from CronogramaRotaImpl rocr "
				+ "inner join rocr.cronogramaAtividadeFaturamento faac "
				+ "inner join faac.atividadeSistema atsi "
				+ "where faac.chavePrimaria = :cronogramaAtividadeFaturamento and atsi.chavePrimaria = :atividadeSistema"
				+ " and rocr.dataRealizada is null")
						.setLong("cronogramaAtividadeFaturamento", cronogramaAtividadeFaturamento)
						.setLong("atividadeSistema", atividadeSistema).uniqueResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void atualizarAtividadeFaturamentoSeExistir(Long grupoFaturamento, Integer atividadeSistema, Integer anoMesReferencia) {
		final Optional<CronogramaAtividadeFaturamento> optCronograma = this.obterCrononogramaAtividadePorRotaAtividade(
				grupoFaturamento, Long.valueOf(atividadeSistema), anoMesReferencia);

		if (optCronograma.isPresent()) {
			final CronogramaAtividadeFaturamento cronograma = optCronograma.get();
			cronograma.setDataRealizacao(DataUtil.toDate(LocalDateTime.now()));
			try {
				this.atualizar(cronograma);
			} catch (Exception e) {
				LOG.error("Falha ao atualizar a atividade faturamento do cronograma", e);
				throw new GGASRuntimeException(e);
			}
		}
	}

	@Override
	public void atualizarAtividadeFaturamentoSeExistir(Long grupoFaturamento, Integer atividadeSistema,
			Integer anoMesReferencia, Long cronogramaFaturamento, StringBuilder logProcessamento) {
		final Optional<CronogramaAtividadeFaturamento> optCronograma = this.obterCrononogramaAtividadePorRotaAtividade(
				grupoFaturamento, Long.valueOf(atividadeSistema), anoMesReferencia, cronogramaFaturamento);

		if (optCronograma.isPresent()) {
			final CronogramaAtividadeFaturamento cronograma = optCronograma.get();

			if (this.obterQuantidadeRotaCronogramaAtualizado(cronograma.getChavePrimaria(), atividadeSistema) == 0) {
				cronograma.setDataRealizacao(DataUtil.toDate(LocalDateTime.now()));
				try {
					this.atualizar(cronograma);
				} catch (Exception e) {
					LOG.error("Falha ao atualizar a atividade faturamento do cronograma", e);
					throw new GGASRuntimeException(e);
				}
			} else {
				logProcessamento.append("\n");
				logProcessamento.append("Não foi possível atualizar o cronograma do grupo!");
			}
		}
	}

	@Override
	public Long consultarQuantidadePontosConsumoEmissaoFatura(Long grupoFaturamento,Integer anoMesReferencia, Integer ciclo,Long cronograma
			,Long atividade) {
		return (Long) createHQLQuery("select count(f) from CronogramaAtividadeFaturamentoImpl f " +
				"where f.atividadeSistema.chavePrimaria = :atividadeSistema and f.cronogramaFaturamento.grupoFaturamento.chavePrimaria"
				+ " = :grupoFaturamento " +
				"and f.cronogramaFaturamento.anoMesFaturamento = :anoMesReferencia and f.cronogramaFaturamento.numeroCiclo = :ciclo"
				+ " and f.cronogramaFaturamento.chavePrimaria = :cronograma " +
				"and f.dataRealizacao is not null")
				.setParameter("atividadeSistema", atividade)
				.setParameter("grupoFaturamento", grupoFaturamento)
				.setParameter("anoMesReferencia", anoMesReferencia)
				.setParameter("ciclo", ciclo)
				.setParameter("cronograma", cronograma)
				.uniqueResult();
	}

	@Override
	public CronogramaFaturamento consultarCronogramaPorGrupoFaturamentoAnoMesCiclo(Long grupoFaturamento,Integer anoMesReferencia,
			Integer numeroCiclo) {
		return (CronogramaFaturamento) createHQLQuery("select f.cronogramaFaturamento from CronogramaAtividadeFaturamentoImpl f " +
				"where  f.cronogramaFaturamento.grupoFaturamento.chavePrimaria = :grupoFaturamento and "
				+ " f.cronogramaFaturamento.anoMesFaturamento = :anoMesReferencia" +
				" and f.cronogramaFaturamento.numeroCiclo = :ciclo")
				.setParameter("grupoFaturamento", grupoFaturamento)
				.setParameter("anoMesReferencia", anoMesReferencia)
				.setParameter("ciclo", numeroCiclo)
				.uniqueResult();
	}

	@Override
	public CronogramaAtividadeFaturamento obterCronogramaAtividadeFaturamento(Long idCronogramaFaturamento, Long idAtividadeSistema) {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("atividadeSistema.chavePrimaria", idAtividadeSistema));
		criteria.add(Restrictions.eq("cronogramaFaturamento.chavePrimaria", idCronogramaFaturamento));

		return (CronogramaAtividadeFaturamento) criteria.uniqueResult();
	}
	
	@Override
	public CronogramaAtividadeFaturamento consultarAtividadeCronograma(Long idAtividade, GrupoFaturamento grupoFaturamento) {
		
		CronogramaFaturamento cronograma = this.consultarCronogramaPorGrupoFaturamentoAnoMesCiclo(grupoFaturamento.getChavePrimaria(), 
				grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo());
		
		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("atividadeSistema.chavePrimaria", idAtividade));
		criteria.add(Restrictions.eq("cronogramaFaturamento.chavePrimaria", cronograma.getChavePrimaria()));

		return (CronogramaAtividadeFaturamento) criteria.uniqueResult();
	}
	
	private CronogramaRotaImpl consultarAtividadeCronogramaRota(CronogramaAtividadeFaturamento atividade, Rota rota) {
		
		StringBuilder query = new StringBuilder();
		
		query.append("select c from CronogramaRotaImpl c ");
		query.append(" where  c.cronogramaAtividadeFaturamento.chavePrimaria = :atividade ");
		query.append(" and c.rota.chavePrimaria = :rota");
		
		CronogramaRotaImpl cronogramaRota  =  (CronogramaRotaImpl) createHQLQuery(query.toString())
				.setParameter("atividade", atividade.getChavePrimaria())
				.setParameter("rota", rota.getChavePrimaria())
				.uniqueResult();
		
		return cronogramaRota;
	}

	@Override
	public void desfazerAtividadeCronograma(Long idAtividade, Rota rota) {
		
		CronogramaAtividadeFaturamento atividade = consultarAtividadeCronograma(idAtividade, rota.getGrupoFaturamento());
		atividade.setDataRealizacao(null);

		CronogramaRota cronogramaRota = consultarAtividadeCronogramaRota(atividade, rota);
		cronogramaRota.setDataRealizada(null);
		try {
			this.atualizar(atividade);
			this.atualizar(cronogramaRota);
		} catch (Exception e) {
			LOG.error("Erro ao desfazer a atividade faturamento do cronograma", e);
			throw new GGASRuntimeException(e);
		}
	}

}
