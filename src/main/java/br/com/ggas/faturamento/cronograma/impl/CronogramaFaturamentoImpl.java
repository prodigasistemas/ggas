/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento.cronograma.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * CronogramaFaturamentoImpl
 * 
 * @author gmatos
 * 
 * Classe responsável Cronograma Faturamento.
 * 
 */
public class CronogramaFaturamentoImpl extends EntidadeNegocioImpl implements CronogramaFaturamento {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	public static final String CONSTANTE_SITUACAO_AGENDADO = "Agendado";

	public static final String CONSTANTE_SITUACAO_EM_ANDAMENTO = "Em Andamento";

	public static final String CONSTANTE_SITUACAO_CONCLUIDO = "Concluído";

	/**
     * 
     */
	private static final long serialVersionUID = -5463508551067328254L;

	private GrupoFaturamento grupoFaturamento;

	private int anoMesFaturamento;

	private Integer numeroCiclo;
	
	private CronogramaAtividadeFaturamento registrarLeitura;

	private String periodoLeitura;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getAnoMesFaturamentoFormatado() {

		return Util.formatarAnoMes(anoMesFaturamento);
	}
	
	//Máscara para Ano/Mês YYYY/MM
	@Override
	public String getAnoMesFaturamentoMascara() {
		
		return Util.formatarAnoMesComMascara(anoMesFaturamento);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getGrupoFaturamento()
	 */
	@Override
	public GrupoFaturamento getGrupoFaturamento() {

		return grupoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #setGrupoFaturamento(br
	 * .com.ggas.cadastro.imovel.GrupoFaturamento)
	 */
	@Override
	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamento()
	 */
	@Override
	public int getAnoMesFaturamento() {

		return anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #setAnoMesFaturamento(int)
	 */
	@Override
	public void setAnoMesFaturamento(int anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento#getNumeroCiclo()
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #setNumeroCiclo(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder sb = new StringBuilder();
		String camposObrigatorios = null;

		if(grupoFaturamento == null) {
			sb.append(GrupoFaturamento.ROTULO_GRUPO_FATURAMENTO);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(anoMesFaturamento <= 0) {
			sb.append(GrupoFaturamento.ROTULO_ANO_MES_FATURAMENTO);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = sb.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, sb.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	public String getSituacaoAgendado() {

		return CONSTANTE_SITUACAO_AGENDADO;
	}

	public String getSituacaoAndamento() {

		return CONSTANTE_SITUACAO_EM_ANDAMENTO;
	}

	public String getSituacaoConcluido() {

		return CONSTANTE_SITUACAO_CONCLUIDO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.CronogramaFaturamento#obterListaSituacao()
	 */
	@Override
	public String[] obterListaSituacao() {

		int size = 3;
		return Arrays.copyOf(
						new ArrayList<String>(Arrays.asList(CONSTANTE_SITUACAO_AGENDADO, CONSTANTE_SITUACAO_CONCLUIDO,
										CONSTANTE_SITUACAO_EM_ANDAMENTO)).toArray(), size, String[].class);
	}

	@Override
	public CronogramaAtividadeFaturamento getRegistrarLeitura() {
		return registrarLeitura;
	}

	@Override
	public void setRegistrarLeitura(CronogramaAtividadeFaturamento registrarLeitura) {
		this.registrarLeitura = registrarLeitura;		
	}

	@Override
	public String getPeriodoLeitura() {
		return this.periodoLeitura;
	}

	@Override
	public void setPeriodoLeitura(String periodoLeitura) {
		this.periodoLeitura = periodoLeitura;
	}

}
