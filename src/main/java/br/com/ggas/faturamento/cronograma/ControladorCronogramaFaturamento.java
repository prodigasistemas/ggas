/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.cronograma;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.web.faturamento.cronograma.CronogramaAction;
import br.com.ggas.web.faturamento.cronograma.CronogramaAtividadeSistemaVO;

/**
 * ControladorCronogramaFaturamento
 * 
 * @author ralencar
 * 
 * Classe responsável por Controlador Cronograma Faturamento.
 * 
 */
public interface ControladorCronogramaFaturamento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO = "controladorCronogramaFaturamento";

	String ERRO_NEGOCIO_CRONOGRAMA_FATURAMENTO_EXISTENTE = "ERRO_NEGOCIO_CRONOGRAMA_FATURAMENTO_EXISTENTE";

	String ERRO_PESQUISA_REFERENCIA_INICIAL_MAIOR = "ERRO_PESQUISA_REFERENCIA_INICIAL_MAIOR";

	String ERRO_NEGOCIO_CRONOGRAMA_FATURAMENTO_INVALIDA_EXCLUSAO = "ERRO_NEGOCIO_CRONOGRAMA_FATURAMENTO_INVALIDA_EXCLUSAO";

	/**
	 * Listar atividade sistema por operacao.
	 * 
	 * @param idOperacao
	 *            the id operacao
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<AtividadeSistema> listarAtividadeSistemaPorOperacao(Long idOperacao) throws GGASException;

	/**
	 * Listar atividade sistema por cronograma.
	 * 
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<AtividadeSistema> listarAtividadeSistemaPorCronograma() throws GGASException;

	/**
	 * Obter cronograma faturamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the cronograma faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaFaturamento obterCronogramaFaturamento(Long chavePrimaria) throws NegocioException;

	/**
	 * Consultar cronograma faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaFaturamento> consultarCronogramaFaturamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Listar cronograma faturamentos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaFaturamento> listarCronogramaFaturamentos() throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFiltroPesquisaCronogramaFaturamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaFaturamento> listarCronogramaFaturamentosPorGrupo(Long idGrupoFaturamento) throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param idCronogramaFaturamento
	 *            the id cronograma faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(Long idCronogramaFaturamento)
					throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param idAtividade
	 *            the id atividade
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaAtividadeFaturamento> listarCronogramasRotasPorAtividade(Long idAtividade) throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerCronogramaAtvidadeFaturamento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param cronogramaFaturamento
	 *            the cronograma faturamento
	 * @param fisicamente
	 *            the fisicamente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerCronogramaFaturamento(CronogramaFaturamento cronogramaFaturamento, boolean fisicamente) throws NegocioException;

	/**
	 * TODO: [fabiana] Comentar.
	 * 
	 * @param cronogramaAtividadeFaturamento
	 *            the cronograma atividade faturamento
	 * @param fisicamente
	 *            the fisicamente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerCronogramaAtividade(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, boolean fisicamente)
					throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * atividades de cronograma.
	 * 
	 * @param atividadeSistema
	 *            A atividade do sistema.
	 * @param cronogramaFaturamento
	 *            the cronograma faturamento
	 * @return Uma coleção de
	 *         CronogramaAtividadeFaturamento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CronogramaAtividadeFaturamento consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema,
					CronogramaFaturamento cronogramaFaturamento) throws NegocioException;

	/**
	 * Obter atividade sistema por operacao.
	 * 
	 * @param idOperacao
	 *            the id operacao
	 * @return the atividade sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	AtividadeSistema obterAtividadeSistemaPorOperacao(Long idOperacao) throws NegocioException;

	/**
	 * Consultar cronograma rota.
	 * 
	 * @param cronogramaAtividadeFaturamento
	 *            the cronograma atividade faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaRota> consultarCronogramaRota(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento)
					throws NegocioException;

	/**
	 * Consultar cronograma atividade faturamento.
	 * 
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema)
					throws NegocioException;

	/**
	 * Obter cronograma atividade faturamento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the cronograma atividade faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaAtividadeFaturamento obterCronogramaAtividadeFaturamento(long chavePrimaria) throws NegocioException;

	/**
	 * Obter atividade sistema.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the atividade sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	AtividadeSistema obterAtividadeSistema(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o ultimo
	 * cronograma de faturamento (baseado no
	 * ano/mes) de um grupo de faturamento.
	 * 
	 * @param chaveGrupoFaturamento
	 *            chave do grupo de faturamento
	 * @return cronograma de faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaFaturamento obterUltimoCronogramaFaturamentoPorGrupoFaturamento(Long chaveGrupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por validar os campos
	 * obrigatórios da função Gerar Cronograma do
	 * cronograma de faturamento;.
	 * 
	 * @param dados
	 *            dados a serem validados.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarCamposObrigatoriosGerarCronogramas(Map<String, Object> dados) throws NegocioException;

	/**
	 * Método responsável por validar se os novos
	 * cronogramas estão na capacidade de cadastro
	 * do registro.
	 * 
	 * @param idGrupoFaturamento
	 *            id do faturamento dos novos
	 *            cronogramas.
	 * @param quantidadeCronogramas
	 *            quantidade de cronogramas a
	 *            serem cadastrados.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarCronogramas(Long idGrupoFaturamento, Integer quantidadeCronogramas) throws NegocioException;

	/**
	 * Método responsável por retornar uma lista
	 * de VO de cronograma atividade que será
	 * exibida na tela de gerar Cronogramas.
	 * 
	 * @param chavesPrimarias
	 *            chaves primárias das atividades
	 *            de sistema
	 * @param duracoes
	 *            durações da cada atividade
	 *            sistema
	 * @param datasPrevistas
	 *            datas previstas de cada
	 *            atividade sistema
	 * @param chavesPrimariasMarcadas
	 *            lista de chaves primarias das
	 *            atividades sistema selecionadas
	 * @param mesAnoPartida
	 *            mes/ano de partida de acordo com
	 *            o grupo de faturamento
	 * @param cicloPartida
	 *            ciclo de partida do grupo de
	 *            faturamento
	 * @param qtdCronogramas
	 *            quantidade de cronogramas de
	 *            faturamento a serem gerados
	 * @param idGrupoFaturamento
	 *            chave do grupo de faturamento
	 * @param datasAtividadesIniciais
	 *            the datas atividades iniciais
	 * @param datasAtividadesFinais
	 *            the datas atividades finais
	 * @param isPrimeiroCronograma
	 * 			 the is Primeiro Cronograma
	 *          
	 * @return lista de VO de cronogramas
	 *         atividade sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaAtividadeSistemaVO> gerarListaCronogramaAtividade(Long[] chavesPrimarias, Integer[] duracoes,
					String[] datasPrevistas, Long[] chavesPrimariasMarcadas, String mesAnoPartida, String cicloPartida,
					Integer qtdCronogramas, Long idGrupoFaturamento, String[] datasAtividadesIniciais, String[] datasAtividadesFinais,
					Boolean isPrimeiroCronograma) throws NegocioException;

	/**
	 * Método responsável por atualizar o
	 * cronogramaFaturamento recebendo um VO de
	 * cronogramaAtividadeSistema como informação.
	 * 
	 * @param cronogramaAtividadeSistemaVO
	 *            the cronograma atividade sistema vo
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarCronogramaFaturamento(CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO, DadosAuditoria dadosAuditoria)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por fazer uma consulta
	 * de cronograma de atividades de faturamento
	 * utilizando o id do cronograma de
	 * faturamento e a lista de
	 * atividades do sistema como parâmetro.
	 * 
	 * @param idCronogramaFaturamento
	 *            the id cronograma faturamento
	 * @param listaIdAtividadeSistema
	 *            the lista id atividade sistema
	 * @return the collection
	 */
	Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtivFaturamento(Long idCronogramaFaturamento,
					Long[] listaIdAtividadeSistema);

	/**
	 * Método reponsável por inserir o Cronograma
	 * Faturamento recebendo uma lista do VO
	 * CronogramaSistema.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @param listaCronogramaAtividadeSistemaVO
	 *            the lista cronograma atividade sistema vo
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void inserirCronogramaFaturamento(Long idGrupoFaturamento, Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por validar os campos
	 * obrigatórios da lista de atividades para a
	 * função Gerar Cronograma do cronograma de
	 * faturamento;.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param chavesPrimariasMarcadas
	 *            the chaves primarias marcadas
	 * @param duracoes
	 *            the duracoes
	 * @param datasPrevistas
	 *            the datas previstas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarListaCamposObrigatoriosGerarCronogramas(Long[] chavesPrimarias, Long[] chavesPrimariasMarcadas, Integer[] duracoes,
					String[] datasPrevistas) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * cronogramas rotas referentes a um mesmo
	 * cronograma de faturamento.
	 * 
	 * @param cronogramaFaturamento
	 *            cronograma de faturamento a ser
	 *            usado como variável na consulta.
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaRota> consultarCronogramaRotaPorCronogramaFaturamento(CronogramaFaturamento cronogramaFaturamento)
					throws NegocioException;

	/**
	 * Método responsável por remover um
	 * cronograma de faturamento, e suas
	 * dependências como cronogramas de atividade
	 * de faturamento e
	 * cronogramas rota, caso estejam de acordo
	 * com a regra de negócio.
	 * 
	 * @param idCronogramaFaturamento
	 *            the id cronograma faturamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerCronogramaFaturamento(Long idCronogramaFaturamento, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por atualizar a
	 * listaCronogramaAtividadeSistemaVO após
	 * aplicar alteração nas datas.
	 * 
	 * @param listaCronogramaAtividadeSistemaVO
	 *            the lista cronograma atividade sistema vo
	 * @param cronogramaAtividadeSistemaVO
	 *            the cronograma atividade sistema vo
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void atualizarListaCronogramaAtividadeSistema(Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO,
					CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO, Long idGrupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por obter um cronograma
	 * de atividade faturamento.
	 * 
	 * @param atividadeSistema
	 *            A atividade do sistema
	 * @param grupoFaturamento
	 *            O grupo de faturamento
	 * @return Uma cronograma de atividade de
	 *         faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaAtividadeFaturamento consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema,
					GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * Consultar cronograma atividade faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método para validar se o ano mês
	 * faturamento informado é maior ou igual ao
	 * parâmetro do sistema.
	 * 
	 * @param mesAnoPartida
	 *            the mes ano partida
	 * @param cicloPartida {@link String}
	 *          
	 * @param grupoFaturamento {@link GrupoFaturamento}
	 *         
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAnoMesFaturamento(String mesAnoPartida, String cicloPartida, GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por validar se existe
	 * cronograma rota realizado para um
	 * cronograma de faturamento.
	 * 
	 * @param idCronogramaFaturamento
	 *            the id cronograma faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarCronogramaRealizado(Long idCronogramaFaturamento) throws NegocioException;

	/**
	 * Método responsável por consultar
	 * cronogramas atividades por grupo de
	 * faturamento atraves do ano, mes, ciclo e
	 * atividade de sistema.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param ciclo
	 *            the ciclo
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @return lista de cronogramas atividades
	 *         faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo(Long chavePrimaria,
					Integer anoMesReferencia, Integer ciclo, AtividadeSistema atividadeSistema) throws NegocioException;

	/**
	 * Método responsável por criar o
	 * cronogramaRota para a Rota inserida.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @param idRota
	 *            the id rota
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	void processarInclusaoRota(Long idGrupoFaturamento, Long idRota) throws NegocioException, IllegalAccessException,
					InvocationTargetException;

	/**
	 * Método responsável por remover o
	 * cronogramaRota para a Rota excluída.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @param rota
	 *            the rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void processarExclusaoRota(Long idGrupoFaturamento, Rota rota) throws NegocioException;

	/**
	 * Consulta de rotas não realizadas.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @param idRota
	 *            the id rota
	 * @return the collection
	 */
	Collection<CronogramaRota> listarCronogramaRotaNaoRealizadosPorGrupo(Long idGrupoFaturamento, Long idRota);

	/**
	 * Método responsável por alterar o
	 * cronogramaRota para a Rota excluída.
	 * 
	 * @param rotaAntiga
	 *            the rota antiga
	 * @param rota
	 *            the rota
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	void processarAlteracaoRota(Rota rotaAntiga, Rota rota) throws NegocioException, IllegalAccessException, InvocationTargetException;

	/**
	 * Método responsável por consultar
	 * cronogramas atividades por cronograma.
	 * 
	 * @param cronogramaFaturamento
	 *            the cronograma faturamento
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(
					CronogramaFaturamento cronogramaFaturamento) throws NegocioException;

	/**
	 * Método para retornar um array com as
	 * situações possiveis do cronograma.
	 * 
	 * @return the string[]
	 */
	String[] obterListaSituacao();

	/**
	 * Método para obter o último cronograma rota
	 * realizado da ativida de consistir leitura
	 * por ponto de consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the cronograma rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaRota obterUltimoCronogramaRealizadoPorPontoConsumo(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método para obter uma lista de
	 * cronogramaFaturamento não iniciados.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CronogramaFaturamento> listarCronogramaFaturamentosNaoIniciadosPorGrupo(Long idGrupoFaturamento) throws NegocioException;

	/**
	 * Verificar cronograma grupo faturamento.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @param mesAnoPartida
	 *            the mes ano partida
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void verificarCronogramaGrupoFaturamento(Long idGrupoFaturamento, Integer mesAnoPartida, Integer numeroCiclo) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Método responsável por retornar um mapa com
	 * as informaçoes necessárias para o
	 * funcionamento
	 * do ajax dp cronogramaFaturamento.
	 * 
	 * @param idGrupoFaturamento
	 *            id do grupo de faturamento para
	 *            realizar a pesquisa.
	 * @param idCronogramaFaturamento
	 *            the id cronograma faturamento
	 * @param mesAnoPartida {@link String}
	 *            
	 * @param cicloPartida
	 *            the ciclo partida
	 * @return mapa com os dados necessários para
	 *         o componente ajax da tela de
	 *         cronograma
	 *         faturamento.
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Map<String, String> mapearCamposGrupoFaturamento(Long idGrupoFaturamento, Long idCronogramaFaturamento, String mesAnoPartida,
					String cicloPartida) throws GGASException;

	/**
	 * Validar duracao dias.
	 * 
	 * @param duracao
	 *            the duracao
	 * @param dataInicio
	 *            the data inicio
	 * @param dataLimite
	 *            the data limite
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDuracaoDias(Integer duracao, Date dataInicio, Date dataLimite) throws NegocioException;

	/**
	 * Obter datas sugestao cronograma.
	 * 
	 * @param dataPrevistaCicloAnterior
	 *            the data prevista ciclo anterior
	 * @param periodicidade
	 *            the periodicidade
	 * @param listaDatasPrevistas
	 *            the lista datas previstas
	 * @param listaDatasIniciais
	 *            the lista datas iniciais
	 * @param listaDatasFinais
	 *            the lista datas finais
	 * @param isVerificarDataPrevista
	 *            the is verificar data prevista
	 * @param idTipoLeitura
	 *            the id tipo leitura
	 * @param ciclo
	 *            the ciclo
	 * @param rota
	 *            the rota
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Map<String, Object> obterDatasSugestaoCronograma(DateTime dataPrevistaCicloAnterior, Periodicidade periodicidade,
					StringBuilder listaDatasPrevistas, StringBuilder listaDatasIniciais, StringBuilder listaDatasFinais,
					Boolean isVerificarDataPrevista, TipoLeitura idTipoLeitura, Integer ciclo, Rota rota) throws
					GGASException;

	/**
	 * Consultar grupos faturamento por ano mes.
	 * 
	 * @param anoMes
	 *            the ano mes
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<GrupoFaturamento> consultarGruposFaturamentoPorAnoMes(Integer anoMes) throws NegocioException;

	/**
	 * Consultar grupos faturamento.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<GrupoFaturamento> consultarGruposFaturamento() throws NegocioException;

	/**
	 * Consultar menor referencia ciclo grupo faturamento com cronograma.
	 * 
	 * @return the integer
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Integer consultarMenorReferenciaCicloGrupoFaturamentoComCronograma() throws NegocioException;

	/**
	 * Consultar cronograma por grupo faturamento ano mes ciclo.
	 * 
	 * @param grupoFaturamento
	 *            the grupo faturamento
	 * @return the cronograma faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaFaturamento consultarCronogramaPorGrupoFaturamentoAnoMesCiclo(GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * Verificar dia util.
	 * 
	 * @param data
	 *            the data
	 * @param listaDatas
	 *            the lista datas
	 * @return the date time
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DateTime verificarDiaUtil(DateTime data, StringBuilder listaDatas) throws NegocioException;

	/**
	 * Consultar cronograma atividade faturamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	// TODO: REMOVER O CONTROLADOR CRONOGRAMA ATIVIDADE FATURAMENTO
	Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException;

	/**
	 * Consultar cronograma rota.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param idAtividadeSistema
	 *            the id atividade sistema
	 * @return the cronograma rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaRota consultarCronogramaRota(Rota rota, Integer anoMesReferencia, Integer numeroCiclo, Long idAtividadeSistema)
					throws NegocioException;

	/**
	 * Desfazer consistir leitura.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void desfazerConsistirLeitura(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException;

	/**
	 * Desfazer registrar leitura.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void desfazerRegistrarLeitura(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar dados registrar leitura.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the collection
	 */
	Collection<HistoricoMedicao> consultarDadosRegistrarLeitura(Map<String, Object> parametros);

	/**
	 * Remover data atividade cronograma.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void removerDataAtividadeCronograma(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar cronograma faturamento escalonamento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<CronogramaRota> consultarCronogramaFaturamentoEscalonamento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consulta o último cronograma realizado das Rotas especificadas
	 * no parâmetro. Obtém os seguintes atributos, do CronogramaRota:
	 * {@code chavePrimaria}, {@code anoMesReferencia}, {@code numeroCiclo}.
	 * Obtém os seguintes atributos da Rota: {@code chavePrimaria}.
	 * @param rotas {@link List}
	 * @return mapa de CronogramaRota por Rota
	 */
	Map<Rota, CronogramaRota> obterUltimoCronogramaRealizadoPorRotas(List<Rota> rotas);
	
	/**
	 * Listar os Ciclos existentes do Supervisório
	 * @param idGrupoFaturamento
	 * @return lista de ciclos 
	 */
	public Collection<Integer> listarCiclosCronograma() ;
	

	/**
	 * Consultar a data menor ou menor de leitura de um período de faturamento supervisorio
	* @param anoMesFaturamento
	 * 			the ano e mês de faturamento
	 * @param numeroCiclo
	 * 			the número do ciclo
	 * @param grupoFaturamento
	 * 			the grupo de faturamento
	 * @param isMaxima
	 * 			the isMaxima
	 * @return Date dataFinal
	 */
	public Date obterDataMaximaOuMininaSupervisorioCronograma(Integer anoMesFaturamento, Integer numeroCiclo,
			Long grupoFaturamento, Boolean isMaxima); 
	
	/**
	 * Método responsável por consultar
	 * cronogramas atividades por grupo de
	 * faturamento atraves do ano, mes e ciclo.
	 * 
	 * @param grupoFaturamento
	 *            the grupo de faturamento
	 * @return lista de cronogramas atividades
	 *         faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	
	public List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo(
			GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<Long> listarChavesAtividadeSistemaPorCronograma() throws NegocioException;
	
	/**
	 * Consultar dados consistir leitura.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the collection
	 */
	public Collection<HistoricoConsumo> consultarDadosConsistirLeitura(Map<String, Object> parametros);

	/**
	 *
	 * @return Collection<AtividadeSistema>
	 * @throws NegocioException
	 */
	public List<Long> listaChavesObrigatorias() throws NegocioException;

	/**
	 *
	 * @return Collection<AtividadeSistema>
	 * @throws NegocioException
	 */

	void desfazerGerarDados(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException;

	/**
	 * Obter grupo de faturamento pelo codigo do supervisorio do ponto de consumo
	 * @param descricaoPontoConsumo - {@link String}
	 * @return GrupoFaturamento - {@link GrupoFaturamento}
	 */
	GrupoFaturamento obterGrupoFaturamentoPorCodigoSupervisorio(String codigoSupervisorio);

	CronogramaFaturamento obterCronogramaFaturamentoAnterior(CronogramaFaturamento cronogramaFaturamento);

	GrupoFaturamento obterGrupoFaturamentoPorDescricao(String descricao);

}
