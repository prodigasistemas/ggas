/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.faturamento.cronograma.impl;

import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorGrupoFaturamento;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.diaria.impl.SupervisorioMedicaoDiariaImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.impl.HistoricoConsumoImpl;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.impl.ControladorLeituraMovimentoImpl;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.leitura.impl.HistoricoMedicaoImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.CronogramaRotaImpl;
import br.com.ggas.medicao.rota.impl.RotaImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.cronograma.CronogramaAtividadeFaturamentoVO;
import br.com.ggas.web.faturamento.cronograma.CronogramaAtividadeSistemaVO;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ralencar
 * 
 * Classe responsável por Controlador Cronograma Faturamento.
 * 
 */
class ControladorCronogramaFaturamentoImpl extends ControladorNegocioImpl implements ControladorCronogramaFaturamento {

	private static final int CHAVE_PRIMARIA_ATIVIDADE_REGISTRAR_LEITURA = 3;

	private static final int MES_OUTUBRO = 10;

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final String ERRO_NEGOCIO_CRONOGRAMA_ROTA_REALIZADO = "ERRO_NEGOCIO_CRONOGRAMA_ROTA_REALIZADO";

	private static final String ERRO_NEGOCIO_ULTIMO_CRONOGRAMA = "ERRO_NEGOCIO_ULTIMO_CRONOGRAMA";

	private static final String ANO_MES_FATURAMENTO_INICIAL = "mesAnoFaturamentoInicial";

	private static final String ANO_MES_FATURAMENTO_FINAL = "mesAnoFaturamentoFinal";

	private static final String PERIODO_REFERENCIA_FATURAMENTO = "Período de Referência de Faturamento";

	private static final String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_INFERIOR_MINIMO = "ERRO_NEGOCIO_QUANTIDADE_"
			+ "CRONOGRAMAS_ABERTOS_INFERIOR_MINIMO";

	private static final String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_EXCEDIDO =
			"ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_EXCEDIDO";

	private static final String ERRO_NEGOCIO_EXCLUSAO_CRONOGRAMA = "ERRO_NEGOCIO_EXCLUSAO_CRONOGRAMA";

	private static final String ERRO_NEGOCIO_ALTERACAO_CRONOGRAMA_ROTA_REALIZADO = "ERRO_NEGOCIO_ALTERACAO_CRONOGRAMA_ROTA_REALIZADO";

	private static final Logger LOG = Logger.getLogger(ControladorCronogramaFaturamentoImpl.class);

	private static final String DATA_INICIAL = "dataInicial";

	private static final String DATA_FINAL = "dataFinal";

	private static final String DATA_PREVISTA = "dataPrevista";

	private static final String CHAVE_ROTA = "chaveRota";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CronogramaFaturamento.BEAN_ID_CRONOGRAMA_FATURAMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaFaturamento.BEAN_ID_CRONOGRAMA_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeCronogramaRota() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);
	}

	public Class<?> getClasseEntidadeGrupoFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeCronogramaFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaFaturamento.BEAN_ID_CRONOGRAMA_FATURAMENTO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}
	/**
	 * Criar atividade sistema.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAtividadeSistema() {

		return (AtividadeSistema) ServiceLocator.getInstancia().getBeanPorID(AtividadeSistema.BEAN_ID_ATIVIDADE_SISTEMA);
	}

	public Class<?> getClasseEntidadeAtividadeSistema() {

		return ServiceLocator.getInstancia().getClassPorID(AtividadeSistema.BEAN_ID_ATIVIDADE_SISTEMA);
	}

	/**
	 * Criar cronograma atividade faturamento.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarCronogramaAtividadeFaturamento() {

		return (CronogramaAtividadeFaturamento) ServiceLocator.getInstancia().getBeanPorID(
						CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	/**
	 * Criar cronograma rota.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarCronogramaRota() {

		return (CronogramaRota) ServiceLocator.getInstancia().getBeanPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);
	}

	public Class<?> getClasseEntidadeCronogramaAtividadeFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeHistoricoMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeSupervisorioMedicaoDiaria() {

		return ServiceLocator.getInstancia().getClassPorID(SupervisorioMedicaoDiaria.BEAN_ID_SUPERVISORIO_MEDICAO_DIARIA);
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #obterCronogramaAtividadeFaturamento(long)
	 */
	@Override
	public CronogramaAtividadeFaturamento obterCronogramaAtividadeFaturamento(long chavePrimaria) throws NegocioException {

		return (CronogramaAtividadeFaturamento) obter(chavePrimaria, getClasseEntidadeCronogramaAtividadeFaturamento());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #obterAtividadeSistema(long)
	 */
	@Override
	public AtividadeSistema obterAtividadeSistema(long chavePrimaria) throws NegocioException {

		return (AtividadeSistema) obter(chavePrimaria, getClasseEntidadeAtividadeSistema());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorSegmento
	 * #
	 * consultarCronogramaFaturamento(java.util.Map
	 * )
	 */
	@Override
	@SuppressWarnings({"unchecked", "squid:S1192"})
	public Collection<CronogramaFaturamento> consultarCronogramaFaturamento(Map<String, Object> filtro) throws NegocioException {

		new StringBuilder();

		List<String> filtrosWhere = new ArrayList<>();
		if (filtro != null) {
			Long codigoGrupoFaturamento = (Long) filtro.get("grupoFaturamento");

			if (codigoGrupoFaturamento != null && codigoGrupoFaturamento > 0) {
				filtrosWhere.add("g.chavePrimaria = " + codigoGrupoFaturamento);
			}

			Long codigoPeriodicidade = (Long) filtro.get("periodicidade");
			if (codigoPeriodicidade != null && codigoPeriodicidade > 0) {
				filtrosWhere.add("p.chavePrimaria = " + codigoPeriodicidade);
			}

			String mesAnoFaturamentoInicial = (String) filtro.get("mesAnoFaturamentoInicial");
			if (!StringUtils.isEmpty(mesAnoFaturamentoInicial)) {
				filtrosWhere.add("c.anoMesFaturamento >= " + mesAnoFaturamentoInicial);
			}

			String mesAnoFaturamentoFinal = (String) filtro.get("mesAnoFaturamentoFinal");
			if (!StringUtils.isEmpty(mesAnoFaturamentoFinal)) {
				filtrosWhere.add("c.anoMesFaturamento <= " + mesAnoFaturamentoFinal);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				filtrosWhere.add("c.habilitado = " + BooleanUtils.toStringTrueFalse(habilitado));
			}else {
				filtrosWhere.add("c.habilitado is not null");
			}

			String situacao = (String) filtro.get("situacao");
			if (!StringUtils.isEmpty(situacao)) {
				if ("Agendado".equals(situacao)) {
					filtrosWhere.add("(c.anoMesFaturamento > g.anoMesReferencia or c.anoMesFaturamento = g.anoMesReferencia "
							+ "and c.numeroCiclo > g.numeroCiclo)");

				} else if ("Em Andamento".equals(situacao)) {
					filtrosWhere.add("(c.anoMesFaturamento = g.anoMesReferencia and c.numeroCiclo = g.numeroCiclo)");

				} else if ("Concluído".equals(situacao)) {
					filtrosWhere.add("(c.anoMesFaturamento < g.anoMesReferencia or c.anoMesFaturamento = g.anoMesReferencia "
							+ "and c.numeroCiclo > g.numeroCiclo)");
				}

			}

			final Long ultimaEtapa = (Long) filtro.get("ultimaEtapa");
			if (ultimaEtapa != null) {
				filtrosWhere.add(" exists ("
					+ "from CronogramaAtividadeFaturamentoImpl cf where cf.cronogramaFaturamento.chavePrimaria = c.chavePrimaria "
					+ "and cf.atividadeSistema.chavePrimaria = " + ultimaEtapa
					+ "and cf.dataRealizacao in ("
						+ "select max(cf2.dataRealizacao) from CronogramaAtividadeFaturamentoImpl cf2 "
						+ "where cf2.cronogramaFaturamento.chavePrimaria = c.chavePrimaria"
					+ ")"
				+ ")");
			}

		}

		return createHQLQuery("from CronogramaFaturamentoImpl c join fetch c.grupoFaturamento g join fetch g.periodicidade p"
				+ filtrosWhere.stream().collect(Collectors.joining(" AND ", " WHERE ", ""))
				+ " ORDER BY c.grupoFaturamento, c.anoMesFaturamento asc, c.numeroCiclo asc"
		).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento#
	 * obterUltimoCronogramaFaturamentoPorGrupoFaturamento
	 * (java.lang.Long)
	 */
	@Override
	public CronogramaFaturamento obterUltimoCronogramaFaturamentoPorGrupoFaturamento(Long chaveGrupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento");
		hql.append(" inner join fetch cronogramaFaturamento.grupoFaturamento grupo ");
		hql.append(" inner join fetch grupo.periodicidade periodicidade ");
		hql.append(" WHERE ");
		hql.append(" cronogramaFaturamento.habilitado = true ");
		hql.append(" AND ");
		hql.append(" grupo.chavePrimaria = :chaveGrupoFaturamento");
		hql.append(" order by cronogramaFaturamento.anoMesFaturamento desc, cronogramaFaturamento.numeroCiclo desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveGrupoFaturamento", chaveGrupoFaturamento);
		query.setMaxResults(1);

		return (CronogramaFaturamento) query.uniqueResult();
	}
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #removerCronogramaFaturamento
	 * (br.com.ggas.faturamento
	 * .cronograma.CronogramaFaturamento,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerCronogramaFaturamento(Long idCronogramaFaturamento, DadosAuditoria dadosAuditoria) throws NegocioException {

		CronogramaFaturamento cronogramaFaturamento = obterCronogramaFaturamento(idCronogramaFaturamento);

		CronogramaFaturamento ultimoCronogramaFaturamento = obterUltimoCronogramaFaturamentoPorGrupoFaturamento(cronogramaFaturamento
						.getGrupoFaturamento().getChavePrimaria());

		if (cronogramaFaturamento.getChavePrimaria() == ultimoCronogramaFaturamento.getChavePrimaria()) {

			Collection<CronogramaFaturamento> listaCronogramaFaturamento = this.listarCronogramaFaturamentosPorGrupo(cronogramaFaturamento
							.getGrupoFaturamento().getChavePrimaria());

			if ((listaCronogramaFaturamento != null) && (listaCronogramaFaturamento.size() <= 1)) {
				throw new NegocioException(ERRO_NEGOCIO_EXCLUSAO_CRONOGRAMA, true);
			}

			Collection<CronogramaRota> listaCronogramaRota = consultarCronogramaRotaPorCronogramaFaturamento(cronogramaFaturamento);
			boolean indicadorCronogramaRealizado = false;

			for (CronogramaRota cronogramaRota : listaCronogramaRota) {
				if (cronogramaRota.getDataRealizada() != null) {
					indicadorCronogramaRealizado = true;
					break;
				}

			}

			if (!indicadorCronogramaRealizado) {
				ultimoCronogramaFaturamento = null;

				for (CronogramaRota cronogramaRota : listaCronogramaRota) {
					this.remover(cronogramaRota);
				}

				Collection<CronogramaAtividadeFaturamento> listaCronogramaAtividadeFaturamento =
						consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(cronogramaFaturamento
								.getChavePrimaria());

				for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : listaCronogramaAtividadeFaturamento) {
					cronogramaAtividadeFaturamento.setDadosAuditoria(dadosAuditoria);
					this.remover(cronogramaAtividadeFaturamento);
				}
				cronogramaFaturamento.setDadosAuditoria(dadosAuditoria);
				this.remover(cronogramaFaturamento);
			} else {
				throw new NegocioException(ERRO_NEGOCIO_CRONOGRAMA_ROTA_REALIZADO, true);
			}

		} else {
			throw new NegocioException(ERRO_NEGOCIO_ULTIMO_CRONOGRAMA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #validarCronogramaRealizado(java.lang.Long)
	 */
	@Override
	public void validarCronogramaRealizado(Long idCronogramaFaturamento) throws NegocioException {

		CronogramaFaturamento cronogramaFaturamento = obterCronogramaFaturamento(idCronogramaFaturamento);

		Collection<CronogramaRota> listaCronogramaRota = consultarCronogramaRotaPorCronogramaFaturamento(cronogramaFaturamento);
		boolean indicadorCronogramaRealizado = false;

		for (CronogramaRota cronogramaRota : listaCronogramaRota) {
			if (cronogramaRota.getDataRealizada() != null) {
				indicadorCronogramaRealizado = true;
				break;
			}
		}

		if (indicadorCronogramaRealizado) {
			throw new NegocioException(ERRO_NEGOCIO_ALTERACAO_CRONOGRAMA_ROTA_REALIZADO, true);
		}
	}

	/**
	 * Consultar cronograma por grupo faturamento ano mes sem data realizacao.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param aNoMesReferencia
	 *            the ano mes referencia
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #consultarCronogramaPorGrupoFaturamentoAnoMes
	 * (GrupoFaturamento)
	 */
	@SuppressWarnings("unchecked")
	public List<CronogramaFaturamento> consultarCronogramaPorGrupoFaturamentoAnoMesSemDataRealizacao(Long chavePrimaria,
					Integer aNoMesReferencia) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT DISTINCT cronogramaFaturamento ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento, ");
		hql.append(" CronogramaAtividadeFaturamentoImpl ");
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" WHERE ");
		hql.append(" cronogramaFaturamento.habilitado = true ");
		hql.append(" AND ");
		hql.append(" cronogramaFaturamento.grupoFaturamento.chavePrimaria = :chaveGrupoFaturamento");
		hql.append(" AND cronogramaFaturamento.anoMesFaturamento >= :anoMesReferencia ");
		hql.append(" AND cronogramaFaturamento.chavePrimaria = cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria ");
		hql.append(" AND cronogramaAtividadeFaturamento.dataRealizacao is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveGrupoFaturamento", chavePrimaria);
		query.setInteger("anoMesReferencia", aNoMesReferencia);

		return query.list();
	}

	/**
	 * Consulta o cronograma por grupo de faturamento
	 * @param chavePrimaria chave primaira
	 * @return retorna a lista de cronograma
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	public List<CronogramaFaturamento> consultarCronogramaPorGrupoFaturamentoSemDataRealizacao(Long chavePrimaria) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT DISTINCT cronogramaFaturamento ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento, ");
		hql.append(" CronogramaAtividadeFaturamentoImpl ");
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" WHERE ");
		hql.append(" cronogramaFaturamento.habilitado = true ");
		hql.append(" AND ");
		hql.append(" cronogramaFaturamento.grupoFaturamento.chavePrimaria = :chaveGrupoFaturamento");
		hql.append(" AND cronogramaFaturamento.chavePrimaria = cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria ");
		hql.append(" AND cronogramaAtividadeFaturamento.dataRealizacao is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveGrupoFaturamento", chavePrimaria);

		return query.list();
}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #listarAtividadeSistemaPorOperacao
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AtividadeSistema> listarAtividadeSistemaPorOperacao(Long idOperacao) throws GGASException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" operacao.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idOperacao);

		return query.list();
	}
		
	/**
	 *
	 * @return Collection<AtividadeSistema>
	 * @throws NegocioException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Long> listaChavesObrigatorias() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select atividadeSistema.chavePrimaria ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" atividadeSistema ");
		hql.append(" WHERE ");
		hql.append(" obrigatoria = true and ");
		hql.append(" chavePrimaria != 28 and ");
		hql.append(" chavePrimaria != 29 ");
		

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	
	/**
	 *
	 * @return Collection<AtividadeSistema>
	 * @throws NegocioException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AtividadeSistema> listarAtividadeSistemaPorCronograma() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" atividadeSistema ");
		hql.append(" left join fetch atividadeSistema.modulo modulo ");
		hql.append(" left join fetch atividadeSistema.operacao operacao ");
		hql.append(" WHERE ");
		hql.append(" atividadeSistema.habilitado = true and");
		hql.append(" atividadeSistema.cronograma = true ");
		hql.append(" order by ");
		hql.append(" atividadeSistema.sequencia");
		hql.append(" asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #listarSegmento()
	 */
	
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaFaturamento> listarCronogramaFaturamentos() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#listarCronogramaFaturamentosPorGrupo(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaFaturamento> listarCronogramaFaturamentosPorGrupo(Long idGrupoFaturamento) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true and");
		hql.append(" grupoFaturamento.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idGrupoFaturamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(java.lang
	 * .Long)
	 */
	@Override
	@SuppressWarnings({"unchecked"})
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(
					Long idCronogramaFaturamento) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cronogramaAtividadeFaturamento from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" left join fetch atividadeSistema.atividadePrecedente atividadePrecedente ");
		hql.append(" where ");
		hql.append(" cronogramaAtividadeFaturamento.habilitado = true ");
		hql.append(" and cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria = ? ");
		hql.append(" order by atividadeSistema.sequencia ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idCronogramaFaturamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarCronogramaAtivFaturamento(java.lang.Long,
	 * java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtivFaturamento(Long idCronogramaFaturamento,
					Long[] listaIdAtividadeSistema) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cronogramaAtividadeFaturamento from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" where ");
		hql.append(" cronogramaAtividadeFaturamento.habilitado = true ");
		hql.append(" and cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria = :idCronogramaFaturamento ");
		hql.append(" and cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria in (:idsAtividadeSistema)");
		hql.append(" order by cronogramaAtividadeFaturamento.atividadeSistema.sequencia ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCronogramaFaturamento", idCronogramaFaturamento);
		query.setParameterList("idsAtividadeSistema", listaIdAtividadeSistema);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#obterCronogramaFaturamento(java.lang.Long)
	 */
	@Override
	public CronogramaFaturamento obterCronogramaFaturamento(Long chavePrimaria) throws NegocioException {

		return (CronogramaFaturamento) this.obter(chavePrimaria, getClasseEntidadeCronogramaFaturamento(), "grupoFaturamento");

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #validarFiltroPesquisaCronogramaFaturamento
	 * (java.util.Map)
	 */
	@Override
	public void validarFiltroPesquisaCronogramaFaturamento(Map<String, Object> filtro) throws NegocioException {

		String anoMesFaturamentoInicial = (String) filtro.get(ANO_MES_FATURAMENTO_INICIAL);
		String anoMesFaturamentoFinal = (String) filtro.get(ANO_MES_FATURAMENTO_FINAL);

		if (anoMesFaturamentoInicial != null && anoMesFaturamentoFinal == null || anoMesFaturamentoFinal != null
						&& anoMesFaturamentoInicial == null) {

			throw new NegocioException(ControladorRecebimento.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA_CONJUNTAMENTE, true);

		} else if (anoMesFaturamentoInicial != null) {

			Integer anoMesInicial;
			Integer anoMesFinal;
			try {
				anoMesInicial = Util.converterCampoStringParaValorAnoMes(PERIODO_REFERENCIA_FATURAMENTO, anoMesFaturamentoInicial);
				anoMesFinal = Util.converterCampoStringParaValorAnoMes(PERIODO_REFERENCIA_FATURAMENTO, anoMesFaturamentoFinal);
			} catch (FormatoInvalidoException e) {
				throw new NegocioException(e);
			}

			if (anoMesInicial > anoMesFinal) {
				throw new NegocioException(ControladorCronogramaFaturamento.ERRO_PESQUISA_REFERENCIA_INICIAL_MAIOR, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #listarCronogramasRotasPorAtividade
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaAtividadeFaturamento> listarCronogramasRotasPorAtividade(Long idAtividade) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cronogramaAtividadeFaturamento from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaRota.atividadeSistema atividadeSistema ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaRota.rota.leiturista leiturista ");
		hql.append(" where cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idAtividade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #removerCronogramaAtvidadeFaturamento
	 * (java.lang.Long[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void removerCronogramaAtvidadeFaturamento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select cronogramaAtividadeFaturamento from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" where cronogramaFaturamento.chavePrimaria in (:chavesPrimarias) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesPrimarias", chavesPrimarias);
		Collection<CronogramaAtividadeFaturamento> listaAtividadeFaturamentos = query.list();

		if (listaAtividadeFaturamentos != null && !listaAtividadeFaturamentos.isEmpty()) {
			for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : listaAtividadeFaturamentos) {

				if (cronogramaAtividadeFaturamento.getDataRealizacao() != null) {
					removerCronogramaAtividade(cronogramaAtividadeFaturamento, true);
					removerCronogramaFaturamento(cronogramaAtividadeFaturamento.getCronogramaFaturamento(), true);
				} else {
					removerCronogramaAtividade(cronogramaAtividadeFaturamento, false);
					removerCronogramaFaturamento(cronogramaAtividadeFaturamento.getCronogramaFaturamento(), false);
				}
			}

		} else {

			StringBuilder hqlCronograma = new StringBuilder();
			hqlCronograma.append(" select cronogramaFaturamento from ");
			hqlCronograma.append(getClasseEntidadeCronogramaFaturamento().getSimpleName());
			hqlCronograma.append(" cronogramaFaturamento ");
			hqlCronograma.append(" where cronogramaFaturamento.chavePrimaria in (:chavesPrimarias) ");

			Query queryCronograma = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			queryCronograma.setParameterList("chavesPrimarias", chavesPrimarias);
			Collection<CronogramaFaturamento> listaCronogramaFaturamentos = queryCronograma.list();

			if (listaCronogramaFaturamentos != null && !listaCronogramaFaturamentos.isEmpty()) {
				for (CronogramaFaturamento cronogramaFaturamento : listaCronogramaFaturamentos) {

					removerCronogramaFaturamento(cronogramaFaturamento, true);
				}

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #consultarCronogramaRota
	 * (br.com.ggas.faturamento
	 * .cronograma.CronogramaAtividadeFaturamento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaRota> consultarCronogramaRota(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" cronogramaAtividadeFaturamento.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, cronogramaAtividadeFaturamento.getChavePrimaria());

		return query.list();
	}

	/**
	 * Obter primeiro cronograma rota.
	 *
	 * @param cronogramaAtividadeFaturamento
	 *            the cronograma atividade faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<CronogramaRota> obterPrimeiroCronogramaRota(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" cronogramaAtividadeFaturamento.chavePrimaria = ? ");
		hql.append(" order by chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, cronogramaAtividadeFaturamento.getChavePrimaria());

		query.setMaxResults(1);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento#
	 * consultarCronogramaRotaPorCronogramaFaturamento
	 * (br.com.ggas.faturamento.cronograma.
	 * CronogramaFaturamento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaRota> consultarCronogramaRotaPorCronogramaFaturamento(CronogramaFaturamento cronogramaFaturamento)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria = ? ");
		hql.append(" order by ");
		hql.append(" cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, cronogramaFaturamento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #obterAtividadeSistemaPorOperacao
	 * (java.lang.Long)
	 */
	@Override
	public AtividadeSistema obterAtividadeSistemaPorOperacao(Long idOperacao) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" operacao.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idOperacao);

		return (AtividadeSistema) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #consultarCronogramaAtividadeFaturamento
	 * (br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" atividadeSistema.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, atividadeSistema.getChavePrimaria());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #consultarCronogramaAtividadeFaturamento
	 * (br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema,
	 * br.com.ggas.faturamento.cronograma
	 * .CronogramaFaturamento)
	 */
	@Override
	public CronogramaAtividadeFaturamento consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema,
					CronogramaFaturamento cronogramaFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" habilitado = true AND ");
		hql.append(" atividadeSistema.chavePrimaria = ? AND ");
		hql.append(" cronogramaFaturamento = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, atividadeSistema.getChavePrimaria());
		query.setLong(1, cronogramaFaturamento.getChavePrimaria());

		return (CronogramaAtividadeFaturamento) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #consultarCronogramaAtividadeFaturamento
	 * (br.com.ggas.faturamento.cronograma.
	 * AtividadeSistema,
	 * br.com.ggas.faturamento.cronograma
	 * .CronogramaFaturamento)
	 */
	@Override
	public CronogramaAtividadeFaturamento consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema,
					GrupoFaturamento grupoFaturamento) throws NegocioException {

		CronogramaAtividadeFaturamento retorno = null;
		Criteria criteria = this.createCriteria(getClasseEntidadeCronogramaAtividadeFaturamento());

		criteria.createAlias("cronogramaFaturamento", "cronogramaFaturamento");
		criteria.add(Restrictions.eq("cronogramaFaturamento.grupoFaturamento.chavePrimaria", grupoFaturamento.getChavePrimaria()));
		criteria.add(Restrictions.eq("cronogramaFaturamento.anoMesFaturamento", grupoFaturamento.getAnoMesReferencia()));
		criteria.add(Restrictions.eq("cronogramaFaturamento.numeroCiclo", grupoFaturamento.getNumeroCiclo()));
		criteria.add(Restrictions.eq("atividadeSistema.chavePrimaria", atividadeSistema.getChavePrimaria()));

		if(criteria.list() != null && !criteria.list().isEmpty()) {
			retorno = (CronogramaAtividadeFaturamento) criteria.list().get(0);
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorSegmento
	 * #
	 * consultarCronogramaFaturamento(java.util.Map
	 * )
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(Map<String, Object> filtro)
					throws NegocioException {

		Criteria criteria = this.createCriteria(getClasseEntidadeCronogramaAtividadeFaturamento());
		if (filtro != null) {

			criteria.createAlias("atividadeSistema", "atividadeSistema");
			criteria.createAlias("cronogramaFaturamento", "cronogramaFaturamento");

			Long codigoCronogramaAtividadeSistemaFaturamento = (Long) filtro.get("chavePrimaria");
			if (codigoCronogramaAtividadeSistemaFaturamento != null && codigoCronogramaAtividadeSistemaFaturamento > 0) {
				criteria.add(Restrictions.eq("chavePrimaria", codigoCronogramaAtividadeSistemaFaturamento));
			}			

			Integer anoMesReferencia = (Integer) filtro.get("anoMesReferencia");
			if (anoMesReferencia != null && anoMesReferencia > 0) {
				criteria.add(Restrictions.eq("cronogramaFaturamento.anoMesFaturamento", anoMesReferencia));
			}

			Integer numeroCiclo = (Integer) filtro.get("numeroCiclo");
			if (numeroCiclo != null && numeroCiclo > 0) {
				criteria.add(Restrictions.eq("cronogramaFaturamento.numeroCiclo", numeroCiclo));
			}

			Long codigoGrupoFaturamento = (Long) filtro.get("codigoGrupoFaturamento");
			if (codigoGrupoFaturamento != null && codigoGrupoFaturamento > 0) {
				criteria.add(Restrictions.eq("cronogramaFaturamento.grupoFaturamento.chavePrimaria", codigoGrupoFaturamento));
			}

			Long codigoAtividadeSistema = (Long) filtro.get("codigoAtividadeSistema");
			if (codigoAtividadeSistema != null && codigoAtividadeSistema > 0) {
				criteria.add(Restrictions.eq("atividadeSistema.chavePrimaria", codigoAtividadeSistema));
			}
			
			
			String descricaoAtividadeSistema = (String) filtro.get("descricaoAtividadeSistema");
			if (descricaoAtividadeSistema != null && !descricaoAtividadeSistema.isEmpty()) {
				criteria.add(Restrictions.like("atividadeSistema.descricao", descricaoAtividadeSistema));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}
			
			Boolean realizado = (Boolean) filtro.get("realizado");
			if(realizado != null) {
				criteria.add(Restrictions.isNotNull("dataRealizacao"));
				criteria.addOrder(Order.desc("cronogramaFaturamento.anoMesFaturamento"));
				criteria.addOrder(Order.desc("cronogramaFaturamento.numeroCiclo"));
				criteria.setMaxResults(1);
			}

		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#removerCronogramaAtividade(br.com.ggas.faturamento.cronograma.
	 * CronogramaAtividadeFaturamento, boolean)
	 */
	@Override
	public void removerCronogramaAtividade(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, boolean fisicamente)
					throws NegocioException {

		super.remover(cronogramaAtividadeFaturamento, getClasseEntidadeCronogramaAtividadeFaturamento(), fisicamente);
	}

	/**
	 * Remover cronograma atividade faturamento.
	 *
	 * @param cronogramaAtividadeFaturamento
	 *            the cronograma atividade faturamento
	 * @param fisicamente
	 *            the fisicamente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void removerCronogramaAtividadeFaturamento(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, boolean fisicamente)
					throws NegocioException {

		super.remover(cronogramaAtividadeFaturamento, getClasseEntidadeCronogramaAtividadeFaturamento(), fisicamente);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#removerCronogramaFaturamento(br.com.ggas.faturamento.cronograma.
	 * CronogramaFaturamento, boolean)
	 */
	@Override
	public void removerCronogramaFaturamento(CronogramaFaturamento cronogramaFaturamento, boolean fisicamente) throws NegocioException {

		super.remover(cronogramaFaturamento, getClasseEntidadeCronogramaFaturamento(), fisicamente);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #validarCamposObrigatoriosGerarCronogramas
	 * (java.util.Map)
	 */
	@Override
	public void validarCamposObrigatoriosGerarCronogramas(Map<String, Object> dados) throws NegocioException {		
		ControladorGrupoFaturamento controladorGrupoFaturamento = ServiceLocator.getInstancia().getControladorGrupoFaturamento();

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		Long grupoFaturamento = (Long) dados.get("grupoFaturamento");
		String mesAnoPartida = (String) dados.get("mesAnoPartida");
		String cicloPartida = (String) dados.get("cicloPartida");
		String quantidadeCronogramas = (String) dados.get("quantidadeCronogramas");

		if (grupoFaturamento == null) {
			stringBuilder.append(GrupoFaturamento.ROTULO_GRUPO_FATURAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(mesAnoPartida)) {
			stringBuilder.append("Mês/Ano de Partida");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(cicloPartida)) {
			stringBuilder.append("ciclo de Partida");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(quantidadeCronogramas)) {
			stringBuilder.append("Quantidade de Cronogramas");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		// validar ciclo
		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		GrupoFaturamento objGrupoFaturamento = controladorRota.obterGrupoFaturamento(grupoFaturamento);
		Integer ciclo = Integer.parseInt(cicloPartida);

		if (!controladorGrupoFaturamento.isPeriodicidadeGrupoMultiplosCiclos(objGrupoFaturamento)
				&& (ciclo > objGrupoFaturamento.getPeriodicidade().getQuantidadeCiclo() || ciclo < 1)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_VALIDACAO_CICLO,
					objGrupoFaturamento.getPeriodicidade().getQuantidadeCiclo());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #validarCronogramas(java.lang.Long,
	 * java.lang.Integer)
	 */
	@Override
	public void validarCronogramas(Long idGrupoFaturamento, Integer quantidadeCronogramas) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
		Collection<CronogramaFaturamento> listaCronogramaFaturamento = this.consultarCronogramaPorGrupoFaturamentoAnoMesSemDataRealizacao(
						idGrupoFaturamento, grupoFaturamento.getAnoMesReferencia());

		Integer quantidadeMinimaCronogramaGerados = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS));

		Integer totalCronogramas = quantidadeCronogramas;

		if (listaCronogramaFaturamento != null && !listaCronogramaFaturamento.isEmpty()) {
			totalCronogramas = totalCronogramas + listaCronogramaFaturamento.size();
		}

		if (totalCronogramas < quantidadeMinimaCronogramaGerados) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_INFERIOR_MINIMO, quantidadeMinimaCronogramaGerados);
		}

		Integer quantidadeMaximaCronogramasAbertos = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS));

		if (totalCronogramas > quantidadeMaximaCronogramasAbertos) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_EXCEDIDO, quantidadeMaximaCronogramasAbertos);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento
	 * #atualizarCronogramaFaturamento(br.com.ggas.web.faturamento.cronograma
	 * CronogramaAtividadeSistemaVO, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void atualizarCronogramaFaturamento(CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO, DadosAuditoria dadosAuditoria)
					throws NegocioException, ConcorrenciaException {

		Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = cronogramaAtividadeSistemaVO
						.getListaCronogramaAtividadeFaturamentoVO();

		Collection<CronogramaAtividadeFaturamento> atividadesMarcadas = new ArrayList<>();

		for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento) this
							.criarCronogramaAtividadeFaturamento();

			if (cronogramaAtividadeFaturamentoVO.getDataPrevista() != null) {

				if(cronogramaAtividadeFaturamentoVO.getChavePrimaria() != null){
					cronogramaAtividadeFaturamento.setChavePrimaria(cronogramaAtividadeFaturamentoVO.getChavePrimaria());
				} else {
					cronogramaAtividadeFaturamento.setChavePrimaria(0);
				}

				cronogramaAtividadeFaturamento.setAtividadeSistema(cronogramaAtividadeFaturamentoVO.getAtividadeSistema());
				cronogramaAtividadeFaturamento.setDadosAuditoria(dadosAuditoria);
				cronogramaAtividadeFaturamento.setCronogramaFaturamento(cronogramaAtividadeSistemaVO.getCronogramaFaturamento());
				cronogramaAtividadeFaturamento.setDataFim(cronogramaAtividadeFaturamentoVO.getDataPrevista());
				cronogramaAtividadeFaturamento.setHabilitado(Boolean.TRUE);
				cronogramaAtividadeFaturamento.setVersao(cronogramaAtividadeFaturamentoVO.getVersao());

				DateTime dataPrevistaAux = new DateTime(cronogramaAtividadeFaturamentoVO.getDataPrevista());
				Integer duracaoAux = cronogramaAtividadeFaturamentoVO.getDuracao() - 1;
				cronogramaAtividadeFaturamento.setDataInicio(dataPrevistaAux.minusDays(duracaoAux).toDate());
				atividadesMarcadas.add(cronogramaAtividadeFaturamento);

				Collection<CronogramaRota> listaCronogramaRota = cronogramaAtividadeFaturamentoVO.getListaCronogramaRota();

				if (listaCronogramaRota != null && !listaCronogramaRota.isEmpty()) {

					for (CronogramaRota cronogramaRota : listaCronogramaRota) {

						super.atualizar(cronogramaRota, CronogramaRotaImpl.class);
					}
				} else {

					if (cronogramaAtividadeFaturamentoVO.getChavePrimaria() != null
									&& cronogramaAtividadeFaturamentoVO.getChavePrimaria() > 0) {

						super.atualizar(cronogramaAtividadeFaturamento, CronogramaAtividadeFaturamentoImpl.class);
					} else {

						super.inserir(cronogramaAtividadeFaturamento);
					}
				}

			}

		}

		removerAtividadesOrfas(cronogramaAtividadeSistemaVO, atividadesMarcadas);

	}

	private void removerAtividadesOrfas(CronogramaAtividadeSistemaVO cronograma,
			Collection<CronogramaAtividadeFaturamento> atividadesMarcadas) throws NegocioException {
		List<CronogramaAtividadeFaturamento> atividadesRelacionadas =
				consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(cronograma.getCronogramaFaturamento());
		atividadesRelacionadas.removeAll(atividadesMarcadas);
		removerAtividadesAssociadas(atividadesRelacionadas);
	}

	private void removerAtividadesAssociadas(List<CronogramaAtividadeFaturamento> atividades)
			throws NegocioException {
		for (CronogramaAtividadeFaturamento atividade : atividades) {
			removerRotasAssociadas(atividade);
			this.removerCronogramaAtividadeFaturamento(atividade, true);
		}
	}

	private void removerRotasAssociadas(CronogramaAtividadeFaturamento atividade) throws NegocioException {
		Collection<CronogramaRota> rotas = consultarCronogramaRota(atividade);
		for (CronogramaRota rota : rotas) {
			this.remover(rota);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #inserirCronogramaFaturamento
	 * (java.util.Collection,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void inserirCronogramaFaturamento(Long idGrupoFaturamento,
					Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO, DadosAuditoria dadosAuditoria)
					throws NegocioException, ConcorrenciaException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);

		Boolean primeiroCronograma = Boolean.FALSE;
		Boolean atualizarGrupoFaturamento = Boolean.FALSE;
		long chavePrimariaCronogramaFaturamento;
		long chavePrimariaRegistrarLeitura;

		Collection<CronogramaFaturamento> listaCronogramaFaturamento = this.listarCronogramaFaturamentosPorGrupo(grupoFaturamento
						.getChavePrimaria());
		if(listaCronogramaFaturamento != null && listaCronogramaFaturamento.isEmpty()) {
			primeiroCronograma = Boolean.TRUE;
			atualizarGrupoFaturamento = Boolean.TRUE;
		}

		for (CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO : listaCronogramaAtividadeSistemaVO) {

			CronogramaFaturamento cronogramaFaturamento = (CronogramaFaturamento) this.criar();
			cronogramaFaturamento.setGrupoFaturamento(grupoFaturamento);
			cronogramaFaturamento.setAnoMesFaturamento(cronogramaAtividadeSistemaVO.getAnoMes());
			cronogramaFaturamento.setNumeroCiclo(cronogramaAtividadeSistemaVO.getCiclo());
			cronogramaFaturamento.setDadosAuditoria(dadosAuditoria);
			chavePrimariaCronogramaFaturamento = this.inserir(cronogramaFaturamento);			

			if (primeiroCronograma && atualizarGrupoFaturamento) {
				grupoFaturamento.setAnoMesReferencia(cronogramaFaturamento.getAnoMesFaturamento());
				grupoFaturamento.setNumeroCiclo(cronogramaFaturamento.getNumeroCiclo());
				this.atualizar(grupoFaturamento, getClasseEntidadeGrupoFaturamento());
				atualizarGrupoFaturamento = Boolean.FALSE;
			}

			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFaturamentoVO = cronogramaAtividadeSistemaVO
							.getListaCronogramaAtividadeFaturamentoVO();
			for (CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO : listaCronogramaAtividadeFaturamentoVO) {
				CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento) this
								.criarCronogramaAtividadeFaturamento();

				cronogramaAtividadeFaturamento.setAtividadeSistema(cronogramaAtividadeFaturamentoVO.getAtividadeSistema());
				cronogramaAtividadeFaturamento.setCronogramaFaturamento(cronogramaFaturamento);
				cronogramaAtividadeFaturamento.setDadosAuditoria(dadosAuditoria);
				cronogramaAtividadeFaturamento.setDataFim(cronogramaAtividadeFaturamentoVO.getDataPrevista());

				DateTime dataPrevistaAux = new DateTime(cronogramaAtividadeFaturamentoVO.getDataPrevista());
				Integer duracaoAux = cronogramaAtividadeFaturamentoVO.getDuracao() - 1;
				cronogramaAtividadeFaturamento.setDataInicio(dataPrevistaAux.minusDays(duracaoAux).toDate());
				chavePrimariaRegistrarLeitura = this.inserir(cronogramaAtividadeFaturamento);
				
				if("REGISTRAR LEITURA".equals(cronogramaAtividadeFaturamento.getAtividadeSistema().getDescricao())) {
					cronogramaAtividadeFaturamento.setChavePrimaria(chavePrimariaRegistrarLeitura);
					cronogramaFaturamento.setChavePrimaria(chavePrimariaCronogramaFaturamento);
					cronogramaFaturamento.setRegistrarLeitura(cronogramaAtividadeFaturamento);
					this.atualizar(cronogramaFaturamento);
				}

				Collection<CronogramaRota> listaCronogramaRota = cronogramaAtividadeFaturamentoVO.getListaCronogramaRota();
				for (CronogramaRota cronogramaRota : listaCronogramaRota) {

					cronogramaRota.setCronogramaAtividadeFaturamento(cronogramaAtividadeFaturamento);
					this.inserir(cronogramaRota);
				}

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#gerarListaCronogramaAtividade(java.lang.Long[],
	 * java.lang.Integer[],
	 * java.lang.String[], java.lang.Long[], java.lang.String, java.lang.String, java.lang.Integer, java.lang.Long, java.lang.String[],
	 * java.lang.String[], java.lang.Boolean)
	 */
	@Override
	public Collection<CronogramaAtividadeSistemaVO> gerarListaCronogramaAtividade(Long[] chavesPrimarias, Integer[] duracoes,
					String[] datasPrevistasTmp, Long[] chavesPrimariasMarcadas, String mesAnoPartida, String cicloPartida,
					Integer qtdCronogramas, Long idGrupoFaturamento, String[] listaDatasAtividadesIniciais,
					String[] listaDatasAtividadesFinais, Boolean isPrimeiroCronogramaTmp) throws NegocioException {
		String[] datasPrevistas = datasPrevistasTmp;

 		Boolean isPrimeiroCronograma = isPrimeiroCronogramaTmp;
		Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO = new ArrayList<>();
		CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO = null;

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(	ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		ControladorGrupoFaturamento controladorGrupoFaturamento = ServiceLocator.getInstancia().getControladorGrupoFaturamento();

		Integer anoMes = null;
		Integer ciclo = null;

		if (!StringUtils.isEmpty(mesAnoPartida) && !StringUtils.isEmpty(cicloPartida)) {
			anoMes = Integer.parseInt(Util.converterMesAnoEmAnoMes(mesAnoPartida));
			ciclo = Integer.parseInt(cicloPartida);
		}

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
		Collection<Rota> listaRotas = controladorRota.listarRotasPorGrupoFaturamento(grupoFaturamento.getChavePrimaria());

		String[] dataPrevistaAnterior = null;
		String [] dataPrevistaTemporaria = Arrays.copyOf(datasPrevistas, datasPrevistas.length);
		DateTime dataPeriodoLeituraAnterior = null;
		Boolean isCronogramaComplementar = Boolean.FALSE;

		ArrayList<AtividadeSistema> atividadeSistemaAux = new ArrayList<AtividadeSistema>(
				this.listarAtividadeSistemaPorCronograma());
		int indexPeriodoLeitura = 0;

		for(int i = 0; i < atividadeSistemaAux.size(); i++) {
			if("REGISTRAR LEITURA".equals(atividadeSistemaAux.get(i).getDescricao())) {
				indexPeriodoLeitura = i;
				break;
			}
		}
				
		Boolean isCronogramaNaoPrimeiroCiclo = Boolean.FALSE;
		DateTime dataAtualRegistrarLeitura = new DateTime(
				DataUtil.converterParaData(datasPrevistas[indexPeriodoLeitura]));	
		
		//Separar em metodo
		if (verificarSeNaoEhPrimeiroCiclo(isPrimeiroCronograma, cicloPartida, grupoFaturamento, dataAtualRegistrarLeitura)) {
			dataAtualRegistrarLeitura = dataAtualRegistrarLeitura.minus(grupoFaturamento.getPeriodicidade().getQuantidadeDias());
			isPrimeiroCronograma = Boolean.FALSE;
			isCronogramaNaoPrimeiroCiclo = Boolean.TRUE;
		}
		
		if(!isPrimeiroCronograma) {
			dataPrevistaAnterior = Arrays.copyOf(datasPrevistas, datasPrevistas.length);
		}
		
		for (int i = 0; i < qtdCronogramas; i++) {

			cronogramaAtividadeSistemaVO = new CronogramaAtividadeSistemaVO();
			Map<String, Integer> referenciaCiclo = null;
			dataPeriodoLeituraAnterior = new DateTime(DataUtil.converterParaData(datasPrevistas[indexPeriodoLeitura]));

			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFatVO = new ArrayList<>();
			String[] datasPrevistasParaProximoCiclo = null;

			if (verificarSePrecisaDeCronogramaComplementar(isPrimeiroCronograma, grupoFaturamento, dataPeriodoLeituraAnterior, isCronogramaNaoPrimeiroCiclo)) {

				isCronogramaComplementar = Boolean.TRUE;
				Integer diferencaDias = 0;
				
				if (isCronogramaNaoPrimeiroCiclo) {
					diferencaDias = retornaDiferencaDiasParaCronogramaCorrente(dataPrevistaAnterior, grupoFaturamento,
							datasPrevistas, dataPeriodoLeituraAnterior);
				}else {
					diferencaDias = DataUtil.diferencaDiasEntreDatas(dataPeriodoLeituraAnterior.toDate(),
							Util.obterUltimoDiaDoMesParaData(dataPeriodoLeituraAnterior).toDate()) + 1;
				}
				if(dataPrevistaAnterior != null) {
					String [] dataPrevistaAnteriorAux = Arrays.copyOf(dataPrevistaAnterior, dataPrevistaAnterior.length);
					datasPrevistas = retornaDatasPrevistasCronogramaComplementar(dataPrevistaAnteriorAux, diferencaDias);
				}
				
				datasPrevistasParaProximoCiclo = this.popularListaAtividadeSistemaVO(duracoes, datasPrevistas,
						chavesPrimariasMarcadas, listaRotas, anoMes, ciclo, grupoFaturamento.getPeriodicidade(),
						isPrimeiroCronograma, listaCronogramaAtividadeFatVO, listaDatasAtividadesIniciais,
						listaDatasAtividadesFinais, grupoFaturamento.getTipoLeitura(), isCronogramaComplementar);

				dataPeriodoLeituraAnterior = new DateTime(DataUtil.converterParaData(datasPrevistas[indexPeriodoLeitura]));
			} else {
				datasPrevistasParaProximoCiclo = this.popularListaAtividadeSistemaVO(duracoes, datasPrevistas,
						chavesPrimariasMarcadas, listaRotas, anoMes, ciclo, grupoFaturamento.getPeriodicidade(),
						isPrimeiroCronograma, listaCronogramaAtividadeFatVO, listaDatasAtividadesIniciais,
						listaDatasAtividadesFinais, grupoFaturamento.getTipoLeitura(), isCronogramaComplementar);
			}
			
			if(!isPrimeiroCronograma && dataPrevistaAnterior != null) {
				dataPrevistaTemporaria = Arrays.copyOf(dataPrevistaAnterior, dataPrevistaAnterior.length);
			}

			cronogramaAtividadeSistemaVO.setAnoMes(anoMes);
			cronogramaAtividadeSistemaVO.setCiclo(ciclo);

			cronogramaAtividadeSistemaVO.setListaCronogramaAtividadeFaturamentoVO(listaCronogramaAtividadeFatVO);
			listaCronogramaAtividadeSistemaVO.add(cronogramaAtividadeSistemaVO);

			isPrimeiroCronograma = Boolean.FALSE;

			datasPrevistas = datasPrevistasParaProximoCiclo;
			dataPrevistaAnterior = Arrays.copyOf(datasPrevistas, datasPrevistas.length);



			if (isCronogramaComplementar) {
				
				if(isCronogramaNaoPrimeiroCiclo) {
					isCronogramaNaoPrimeiroCiclo = Boolean.FALSE;
				}
				isCronogramaComplementar = Boolean.FALSE;
				referenciaCiclo = Util.gerarProximaReferenciaCiclo(anoMes, ciclo,
						grupoFaturamento.getPeriodicidade().getQuantidadeDias(), dataPeriodoLeituraAnterior.minusDays(1));

				ciclo = referenciaCiclo.get("ciclo");
				anoMes = referenciaCiclo.get("referencia");

				cronogramaAtividadeSistemaVO = new CronogramaAtividadeSistemaVO();

				listaCronogramaAtividadeFatVO = new ArrayList<>();
				datasPrevistasParaProximoCiclo = this.popularListaAtividadeSistemaVO(duracoes, dataPrevistaTemporaria,
						chavesPrimariasMarcadas, listaRotas, anoMes, ciclo, grupoFaturamento.getPeriodicidade(),
						isPrimeiroCronograma, listaCronogramaAtividadeFatVO, listaDatasAtividadesIniciais,
						listaDatasAtividadesFinais, grupoFaturamento.getTipoLeitura(), isCronogramaComplementar);

				cronogramaAtividadeSistemaVO.setAnoMes(anoMes);
				cronogramaAtividadeSistemaVO.setCiclo(ciclo);

				cronogramaAtividadeSistemaVO.setListaCronogramaAtividadeFaturamentoVO(listaCronogramaAtividadeFatVO);
				listaCronogramaAtividadeSistemaVO.add(cronogramaAtividadeSistemaVO);

				datasPrevistas = datasPrevistasParaProximoCiclo;

				dataPeriodoLeituraAnterior = new DateTime(
						DataUtil.converterParaData(datasPrevistas[indexPeriodoLeitura]));
			}

			if (controladorGrupoFaturamento.isPeriodicidadeGrupoMultiplosCiclos(grupoFaturamento)) {
				referenciaCiclo = Util.gerarProximaReferenciaCiclo(anoMes, ciclo, grupoFaturamento.getPeriodicidade()
						.getQuantidadeDias(), dataPeriodoLeituraAnterior.minusDays(1));
			} else {
				referenciaCiclo = Util.rolarReferenciaCiclo(anoMes, ciclo, grupoFaturamento.getPeriodicidade()
						.getQuantidadeCiclo());
			}

			ciclo = referenciaCiclo.get("ciclo");
			anoMes = referenciaCiclo.get("referencia");
		}

		return listaCronogramaAtividadeSistemaVO;
	}


	/**
	 * Retorna a primeira data valida do array
	 *
	 * @param datasPrevistas
	 * @return
	 * @throws NegocioException
	 */
	private DateTime obterPrimeiraDataValidaDoArrayDeDatas(String[] datasPrevistas) throws NegocioException {
		String primeiraData = Arrays.asList(datasPrevistas).stream().filter(data -> DataUtil.validarData(data)).findFirst().orElse(null);
		if (primeiraData != null) {
			return new DateTime(DataUtil.converterParaData(primeiraData));
		}
		throw new NegocioException(Constantes.ERRO_CRONOGRAMA_NENHUMA_DATA_ATIVIDADE_INFORMADA);
	}

	/**
	 * Popular lista atividade sistema vo.
	 *
	 * @param duracoes
	 *            the duracoes
	 * @param datasPrevistas
	 *            the datas previstas
	 * @param chavesPrimariasMarcadas
	 *            the chaves primarias marcadas
	 * @param listaRotas
	 *            the lista rotas
	 * @param anoMes
	 *            the ano mes
	 * @param ciclo
	 *            the ciclo
	 * @param periodicidade
	 *            the periodicidade
	 * @param isPrimeiroCronograma
	 *            the is primeiro cronograma
	 * @param listaCronogramaAtividadeFatVO
	 *            the lista cronograma atividade fat vo
	 * @param listaDatasAtividadesIniciais
	 *            the lista datas atividades iniciais
	 * @param listaDatasAtividadesFinais
	 *            the lista datas atividades finais
	 * @param tipoLeitura
	 *            the tipo leitura
	 * @return the string[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #popularListaAtividadeSistemaVO
	 * (java.lang.Long[], java.lang.Integer[],
	 * java.lang.Integer[], java.lang.String[],
	 * java.lang.Long[])
	 */
	private String[] popularListaAtividadeSistemaVO(Integer[] duracoes, String[] datasPrevistas, Long[] chavesPrimariasMarcadas,
			Collection<Rota> listaRotas, Integer anoMes, Integer ciclo, Periodicidade periodicidade, Boolean isPrimeiroCronograma,
			Collection<CronogramaAtividadeFaturamentoVO> listaCronogramaAtividadeFatVO, String[] listaDatasAtividadesIniciais,
			String[] listaDatasAtividadesFinais, TipoLeitura tipoLeitura, Boolean isCronogramaComplementar) throws NegocioException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Collection<AtividadeSistema> listaAtividadeSistemas = this.listarAtividadeSistemaPorCronograma();
		CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO = null;

		int j = 0;
		int i = 0;
		String[] datasPrevistasParaProximoCiclo = new String[listaAtividadeSistemas.size()];
		Date dataPrevistaAtividade = null;
		String dataInicialAtividade = null;
		String dataFinalAtividade = null;

		for (AtividadeSistema atividadeSistema : listaAtividadeSistemas) {
			for (; i < chavesPrimariasMarcadas.length;) {
				if (atividadeSistema.getChavePrimaria() == Long.parseLong(chavesPrimariasMarcadas[i].toString())) {

					cronogramaAtividadeFaturamentoVO = new CronogramaAtividadeFaturamentoVO();
					try {
						if (isPrimeiroCronograma != null && isPrimeiroCronograma.booleanValue()) {

							dataPrevistaAtividade =
									Util.converterCampoStringParaData("Prevista", datasPrevistas[j], Constantes.FORMATO_DATA_BR);
							dataInicialAtividade = listaDatasAtividadesIniciais[j];
							dataFinalAtividade = listaDatasAtividadesFinais[j];
						} else {

							DateTime dataPrevista = new DateTime(
									Util.converterCampoStringParaData("Prevista", datasPrevistas[j], Constantes.FORMATO_DATA_BR));

							Map<String, Object> mapaDatas = null;
							if (controladorParametroSistema.obterValorParametroUtilizacaoMultiplosCiclos() && periodicidade
									.getQuantidadeDias() < Constantes.QUANTIDADE_MINIMA_DE_DIAS_PARA_SER_CONSIDERADO_MENSAL) {
								mapaDatas = this.obterDatasSugestaoCronograma(dataPrevista, periodicidade, isCronogramaComplementar);
							} else {
								mapaDatas = this.obterDatasSugestaoCronograma(dataPrevista, periodicidade, null, null, null, Boolean.TRUE,
										tipoLeitura, ciclo, null);
							}

							dataPrevistaAtividade = (Date) mapaDatas.get(DATA_PREVISTA);
							dataInicialAtividade =
									Util.converterDataParaStringSemHora((Date) mapaDatas.get(DATA_INICIAL), Constantes.FORMATO_DATA_BR);
							dataFinalAtividade =
									Util.converterDataParaStringSemHora((Date) mapaDatas.get(DATA_FINAL), Constantes.FORMATO_DATA_BR);
						}

						cronogramaAtividadeFaturamentoVO.setDataPrevista(dataPrevistaAtividade);
						cronogramaAtividadeFaturamentoVO.setDataInicialAtividade(dataInicialAtividade);
						cronogramaAtividadeFaturamentoVO.setDataFinalAtividade(dataFinalAtividade);

						datasPrevistasParaProximoCiclo[j] =
								(Util.converterDataParaStringSemHora(cronogramaAtividadeFaturamentoVO.getDataPrevista(),
										Constantes.FORMATO_DATA_BR));

					} catch (GGASException e) {
						LOG.error(e.getMessage(), e);
						throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
					}

					Integer duracao = this.getDuracao(j, duracoes);

					cronogramaAtividadeFaturamentoVO.setDuracao(duracao);

					cronogramaAtividadeFaturamentoVO.setAtividadeSistema(atividadeSistema);

					Collection<CronogramaRota> listaCronogramaRota = new ArrayList<>();
					for (Rota rota : listaRotas) {

						CronogramaRota cronogramaRota = (CronogramaRota) controladorRota.criarCronogramaRota();
						this.popularCronogramaRota(cronogramaAtividadeFaturamentoVO, cronogramaRota, rota, anoMes, ciclo, tipoLeitura);
						listaCronogramaRota.add(cronogramaRota);
					}
					cronogramaAtividadeFaturamentoVO.setListaCronogramaRota(listaCronogramaRota);

					listaCronogramaAtividadeFatVO.add(cronogramaAtividadeFaturamentoVO);
					i++;
					break;
				} else {
					datasPrevistasParaProximoCiclo[j] = ",";
					break;
				}

			}
			j++;

		}
		return datasPrevistasParaProximoCiclo;
	}

	private Integer getDuracao(Integer index, Integer[] duracoes) {
		Integer duracao = 0;
		if(duracoes != null) {
			duracao = duracoes[index];
		}
		return duracao;
	}

	/**
	 * Popular cronograma rota.
	 *
	 * @param cronogramaAtividadeFaturamentoVO
	 *            the cronograma atividade faturamento vo
	 * @param cronogramaRota
	 *            the cronograma rota
	 * @param rota
	 *            the rota
	 * @param anoMes
	 *            the ano mes
	 * @param ciclo
	 *            the ciclo
	 * @param tipoLeitura
	 *            the tipo leitura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularCronogramaRota(CronogramaAtividadeFaturamentoVO cronogramaAtividadeFaturamentoVO, CronogramaRota cronogramaRota,
					Rota rota, Integer anoMes, Integer ciclo, TipoLeitura tipoLeitura) throws NegocioException {

		ControladorFeriado controladorFeriado = (ControladorFeriado) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistemaTipoLeituraEletroCorretor = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);

		Date dataPrevista = cronogramaAtividadeFaturamentoVO.getDataPrevista();

		if (tipoLeitura.getChavePrimaria() != Integer.parseInt(constanteSistemaTipoLeituraEletroCorretor.getValor()) && rota != null
						&& rota.getSetorComercial() != null) {

			Long idMunicipio = rota.getSetorComercial().getMunicipio().getChavePrimaria();

			if (!controladorFeriado.isDiaUtil(dataPrevista, idMunicipio, Boolean.TRUE)) {
				dataPrevista = controladorFeriado.obterProximoDiaUtil(dataPrevista, idMunicipio, Boolean.TRUE);
			}
		}

		cronogramaRota.setDataPrevista(dataPrevista);
		cronogramaRota.setRota(rota);
		cronogramaRota.setChavePrimaria(0);
		cronogramaRota.setHabilitado(true);
		cronogramaRota.setAnoMesReferencia(anoMes);
		cronogramaRota.setNumeroCiclo(ciclo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #atualizarListaCronogramaAtividadeSistema
	 * (java.util.Collection,
	 * br.com.ggas.web.faturamento
	 * .cronograma.CronogramaAtividadeSistemaVO,
	 * java.lang.Long)
	 */

	@Override
	public void atualizarListaCronogramaAtividadeSistema(Collection<CronogramaAtividadeSistemaVO> listaCronogramaAtividadeSistemaVO,
					CronogramaAtividadeSistemaVO cronogramaAtividadeSistemaVO, Long idGrupoFaturamento) throws NegocioException {

		throw new UnsupportedOperationException();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #validarListaCamposObrigatoriosGerarCronogramas
	 * (java.lang.Long[], java.lang.Integer[],
	 * java.lang.Integer[], java.lang.String[])
	 */
	@Override
	public void validarListaCamposObrigatoriosGerarCronogramas(Long[] chavesPrimarias, Long[] chavesPrimariasMarcadas, Integer[] duracoes,
					String[] datasPrevistas) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		StringBuilder stringBuilderInvalidos = new StringBuilder();
		String camposInvalidos = null;

		Boolean duracao = false;
		Boolean dataLimite = false;

		if (chavesPrimariasMarcadas != null) {
			int j = 0;
			for (int i = 0; i < chavesPrimarias.length; i++) {
				if (chavesPrimarias[i] != null && Arrays.binarySearch(chavesPrimariasMarcadas, chavesPrimarias[i]) >= 0) {
					if ((duracoes == null || duracoes[i] == null || duracoes[i] == 0) && (!duracao)) {
						stringBuilder.append("Duração");
						stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
						duracao = true;
					

						if (StringUtils.isEmpty(datasPrevistas[j]) && !dataLimite) {
							stringBuilder.append("Data Limite");
							stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
							dataLimite = true;
						}

						if (!StringUtils.isEmpty(datasPrevistas[j]) && !Util.isDataValida(datasPrevistas[j], Constantes.FORMATO_DATA_BR)) {
							stringBuilderInvalidos.append("Data Limite");
							break;
						}
						j++;
				   }

				}

			}
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		camposInvalidos = stringBuilderInvalidos.toString();

		if (camposInvalidos.length() > 0) {
			throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, stringBuilderInvalidos.toString());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#validarAnoMesFaturamento(java.lang.String, java.lang.String,
	 * br.com.ggas.cadastro.imovel.GrupoFaturamento)
	 */
	@Override
	public void validarAnoMesFaturamento(String mesAnoPartida, String cicloPartida, GrupoFaturamento grupoFaturamento)
					throws NegocioException {

		Integer anoMesCicloInformado = Integer.parseInt(Util.converterMesAnoEmAnoMes(mesAnoPartida) + cicloPartida);
		Integer anoMesCicloGrupoFaturamento = Integer.parseInt(String.valueOf(grupoFaturamento.getAnoMesReferencia())
						+ String.valueOf(grupoFaturamento.getNumeroCiclo()));

		if (anoMesCicloInformado < anoMesCicloGrupoFaturamento || anoMesCicloInformado.equals(anoMesCicloGrupoFaturamento)
						&& Integer.parseInt(cicloPartida) < grupoFaturamento.getNumeroCiclo()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MES_ANO_PARTIDA_MENOR_MES_ANO_GRUPO_FATURAMENTO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#validarDuracaoDias(java.lang.Integer, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public void validarDuracaoDias(Integer duracaoDias, Date dataInicio, Date dataLimite) throws NegocioException {

		Integer duracao = duracaoDias;
		duracao = duracao - 1;

		DateTime dataLimiteAux = new DateTime(dataLimite);
		Date dataInicioAux = dataLimiteAux.minusDays(duracao).toDate();

		if (dataInicioAux.compareTo(dataInicio) < 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_DURACAO_ULTRAPASSANDO_DATA_INICIO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarGruposFaturamentoPorAnoMes(java.lang.Integer)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<GrupoFaturamento> consultarGruposFaturamentoPorAnoMes(Integer anoMes) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" grupoFaturamento.habilitado = true AND ");
		hql.append(" grupoFaturamento.anoMesReferencia = :anoMes");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMes", anoMes);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarGruposFaturamento()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<GrupoFaturamento> consultarGruposFaturamento() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" grupoFaturamento.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarMenorReferenciaCicloGrupoFaturamentoComCronograma()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Integer consultarMenorReferenciaCicloGrupoFaturamentoComCronograma() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT MIN(grupoFaturamento.anoMesReferencia) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" grupoFaturamento.habilitado = true ");
		hql.append(" and ( ");
		hql.append(" 	select count(*) ");
		hql.append(" 	from ");
		hql.append(getClasseEntidadeCronogramaFaturamento().getSimpleName());
		hql.append("    cronogramaFaturamento ");
		hql.append("    where cronogramaFaturamento.habilitado = true ");
		hql.append("    and cronogramaFaturamento.grupoFaturamento.chavePrimaria = grupoFaturamento.chavePrimaria ) ");
		hql.append("	> 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Integer) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento#
	 * consultarCronogramaPorGrupoFaturamentoAnoMesCiclo
	 * (GrupoFaturamento)
	 */
	@Override
	public CronogramaFaturamento consultarCronogramaPorGrupoFaturamentoAnoMesCiclo(GrupoFaturamento grupoFaturamento)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento");
		hql.append(" WHERE ");
		hql.append(" habilitado = true ");
		hql.append(" AND ");
		hql.append(" cronogramaFaturamento.grupoFaturamento.chavePrimaria = :chaveGrupoFaturamento");
		hql.append(" AND cronogramaFaturamento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" AND cronogramaFaturamento.numeroCiclo = :ciclo ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveGrupoFaturamento", grupoFaturamento.getChavePrimaria());
		query.setInteger("anoMesReferencia", grupoFaturamento.getAnoMesReferencia());
		query.setInteger("ciclo", grupoFaturamento.getNumeroCiclo());
		query.setMaxResults(1);

		return (CronogramaFaturamento) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento#
	 * consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo
	 * (Long chavePrimaria, Integer
	 * AnoMesReferencia, Integer ciclo,
	 * AtividadeSistema atividadeSistema)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo(Long chavePrimaria,
					Integer anoMesReferencia, Integer ciclo, AtividadeSistema atividadeSistema) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder(obterSqlConsultarCronogramaAtividadesPorGrupo());
		hql.append(" AND cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :chaveAtividadeSistemas");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveGrupoFaturamento", chavePrimaria);
		query.setInteger("anoMesReferencia", anoMesReferencia);
		query.setInteger("ciclo", ciclo);
		query.setLong("chaveAtividadeSistemas", atividadeSistema.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento#
	 * listarCronogramaFaturamentosNaoIniciadosPorGrupo
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaFaturamento> listarCronogramaFaturamentosNaoIniciadosPorGrupo(Long idGrupoFaturamento)
					throws NegocioException {


		// TKT: 2817 (18/05/2011): recupera os
		// cronogramas do grupo de faturamento
		// informado
		// cuja atividade inicial ainda não tenha
		// sido realizada.
		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct atividadeFaturamento.cronogramaFaturamento from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName()).append(" atividadeFaturamento ");
		hql.append(" inner join atividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" inner join cronogramaFaturamento.grupoFaturamento grupoFaturamento ");
		hql.append(" where ");
		hql.append(" grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
		hql.append(" and atividadeFaturamento.dataRealizacao is null ");
		hql.append(" and not exists  ( ");
		hql.append(" 			select 'X' ");
		hql.append(" 			from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" 			cronogramaAtividade ");
		hql.append(" 			where cronogramaAtividade.cronogramaFaturamento.chavePrimaria = cronogramaFaturamento.chavePrimaria");
		hql.append(" 			and cronogramaAtividade.dataRealizacao is not null");
		hql.append(" 			) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idGrupoFaturamento", idGrupoFaturamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #processarInclusaoRota(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public void processarInclusaoRota(Long idGrupoFaturamento, Long idRota) throws NegocioException, IllegalAccessException,
					InvocationTargetException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
		Rota rota = (Rota) controladorRota.obter(idRota);

		Collection<CronogramaFaturamento> listaCronogramaFaturamento = this
						.listarCronogramaFaturamentosNaoIniciadosPorGrupo(grupoFaturamento.getChavePrimaria());

		if ((listaCronogramaFaturamento != null) && (!listaCronogramaFaturamento.isEmpty())) {
			for (CronogramaFaturamento cronogramaFaturamento : listaCronogramaFaturamento) {
				Collection<CronogramaAtividadeFaturamento> listaCronogramaAtividadeFaturamento = this
								.consultarCronogramaAtivFaturamentoPorCronogramaFaturamento(cronogramaFaturamento.getChavePrimaria());
				for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : listaCronogramaAtividadeFaturamento) {
					Collection<CronogramaRota> listaCronogramaRota = this.obterPrimeiroCronogramaRota(cronogramaAtividadeFaturamento);
					for (CronogramaRota cronogramaRota : listaCronogramaRota) {
						CronogramaRota novoCronogramaRota = (CronogramaRota) this.criarCronogramaRota();
						Util.copyProperties(novoCronogramaRota, cronogramaRota);

						novoCronogramaRota.setChavePrimaria(0);
						novoCronogramaRota.setCronogramaAtividadeFaturamento(cronogramaAtividadeFaturamento);
						novoCronogramaRota.setUltimaAlteracao(Calendar.getInstance().getTime());
						novoCronogramaRota.setRota(rota);
						this.inserir(novoCronogramaRota);
					}
				}

			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #listarCronogramaRotaNaoRealizadosPorGrupo
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CronogramaRota> listarCronogramaRotaNaoRealizadosPorGrupo(Long idGrupoFaturamento, Long idRota) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" inner join fetch cronogramaRota.rota rota ");
		hql.append(" where rota.chavePrimaria = ?");
		hql.append(" and ");
		hql.append(" cronogramaFaturamento.habilitado = true ");
		hql.append(" and ");
		hql.append(" cronogramaFaturamento.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" and not exists  ( ");
		hql.append(" 			select 'X' ");
		hql.append(" 			from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" 			cronogramaAtividade ");
		hql.append(" 			where cronogramaAtividade.cronogramaFaturamento.chavePrimaria = cronogramaFaturamento.chavePrimaria");
		hql.append(" 			and cronogramaAtividade.dataRealizacao is not null");
		hql.append(" 			) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idRota);
		query.setLong(1, idGrupoFaturamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #processarAlteracaoRota(java.lang.Long,
	 * br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public void processarExclusaoRota(Long idGrupoFaturamentoAnterior, Rota rota) throws NegocioException {

		// Remover os cronogramas Rota que não
		// foram realizados
		Collection<CronogramaRota> listaCronogramaRota = this.listarCronogramaRotaNaoRealizadosPorGrupo(idGrupoFaturamentoAnterior,
						rota.getChavePrimaria());

		for (CronogramaRota cronogramaRota : listaCronogramaRota) {
			this.remover(cronogramaRota);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #processarAlteracaoRota
	 * (br.com.ggas.medicao.rota.Rota,
	 * br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public void processarAlteracaoRota(Rota rotaAntiga, Rota rota) throws NegocioException, IllegalAccessException,
					InvocationTargetException {

		Boolean alterouGrupo = Boolean.FALSE;
		Boolean alterouHabilitado = Boolean.FALSE;

		if (rota.getGrupoFaturamento().getChavePrimaria() != rotaAntiga.getGrupoFaturamento().getChavePrimaria()) {
			alterouGrupo = Boolean.TRUE;
		}

		if (rota.isHabilitado() != rotaAntiga.isHabilitado()) {
			alterouHabilitado = Boolean.TRUE;
		}

		// Se alterou Grupo e não mudou
		// Habilitado: Deve excluir os cronogramas
		// Rota do grupo anterior
		// e inserir novos cronogramas para o novo
		// grupo
		if (alterouGrupo && (!alterouHabilitado)) {
			if (rota.isHabilitado()) {
				this.processarExclusaoRota(rotaAntiga.getGrupoFaturamento().getChavePrimaria(), rota);
				this.processarInclusaoRota(rota.getGrupoFaturamento().getChavePrimaria(), rota.getChavePrimaria());
			} else {
				this.processarExclusaoRota(rotaAntiga.getGrupoFaturamento().getChavePrimaria(), rota);
			}

			// Se alteraou os dois: Deve verificar
			// se habilitou ou desabilitou para
			// continuar o processamento
			// de remover os cronogramas do grupo
			// anterior e inserir os novos
			// cronogramas do novo grupo
		} else if (alterouGrupo && alterouHabilitado) {
			if (rota.isHabilitado()) {
				this.processarExclusaoRota(rotaAntiga.getGrupoFaturamento().getChavePrimaria(), rota);
				this.processarInclusaoRota(rota.getGrupoFaturamento().getChavePrimaria(), rota.getChavePrimaria());
			} else {
				this.processarExclusaoRota(rotaAntiga.getGrupoFaturamento().getChavePrimaria(), rota);
			}

			// Se não alterou o grupo e alterou
			// Habilitado: Deve verificar se
			// habilitou ou desabilitou para
			// continuar o processamento,
			// inserindo os novos cronograms ou
			// excuindo
		} else if ((!alterouGrupo) && alterouHabilitado) {
			if (rota.isHabilitado()) {
				this.processarInclusaoRota(rota.getGrupoFaturamento().getChavePrimaria(), rota.getChavePrimaria());
			} else {
				this.processarExclusaoRota(rota.getGrupoFaturamento().getChavePrimaria(), rota);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(br
	 * .com.
	 * ggas.faturamento.cronograma.CronogramaFaturamento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(
					CronogramaFaturamento cronogramaFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT cronogramaAtividadeFaturamento ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento, ");
		hql.append(" CronogramaAtividadeFaturamentoImpl ");
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" WHERE ");
		hql.append(" cronogramaFaturamento.chavePrimaria = cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria ");
		hql.append(" AND cronogramaFaturamento.habilitado = true ");
		hql.append(" AND cronogramaFaturamento.chavePrimaria = :chaveCronogramaFaturamento");
		hql.append(" AND cronogramaFaturamento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" AND cronogramaFaturamento.numeroCiclo = :ciclo ");
		hql.append(" order by cronogramaAtividadeFaturamento.chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveCronogramaFaturamento", cronogramaFaturamento.getChavePrimaria());
		query.setInteger("anoMesReferencia", cronogramaFaturamento.getAnoMesFaturamento());
		query.setInteger("ciclo", cronogramaFaturamento.getNumeroCiclo());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #obterListaSituacao()
	 */
	@Override
	public String[] obterListaSituacao() {

		CronogramaFaturamento cronograma = (CronogramaFaturamento) this.criar();

		return cronograma.obterListaSituacao();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#obterUltimoCronogramaRealizadoPorPontoConsumo(java.lang.Long)
	 */
	@Override
	public CronogramaRota obterUltimoCronogramaRealizadoPorPontoConsumo(Long idRota) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT cronogramaRota ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota, ");
		hql.append(" RotaImpl rota, ");
		hql.append(" CronogramaAtividadeFaturamentoImpl cronogramaAtividadeFaturamento ");
		hql.append(" WHERE ");
		hql.append(" cronogramaRota.habilitado = true ");
		hql.append(" AND cronogramaRota.dataRealizada is not null ");
		hql.append(" AND rota.chavePrimaria = :chavePrimariaRota ");
		hql.append(" AND cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :chaveAtividadeSistema ");
		hql.append(" order by cronogramaRota.anoMesReferencia desc ");
		hql.append("        , cronogramaRota.numeroCiclo desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaRota", idRota);
		query.setLong("chaveAtividadeSistema", AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA);

		query.setMaxResults(1);
		return (CronogramaRota) query.uniqueResult();
	}

	/**
	 * Consulta o último cronograma realizado das Rotas especificadas
	 * no parâmetro. Obtém os seguintes atributos, do CronogramaRota:
	 * {@code chavePrimaria}, {@code anoMesReferencia}, {@code numeroCiclo}.
	 * Obtém os seguintes atributos da Rota: {@code chavePrimaria}.
	 * @param listaAtributos
	 * @return mapa de CronogramaRota por Rota
	 */
	@Override
	public Map<Rota, CronogramaRota> obterUltimoCronogramaRealizadoPorRotas(List<Rota> rotas) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT rota.chavePrimaria, cronogramaRota.chavePrimaria,  ");
		hql.append(" cronogramaRota.anoMesReferencia, cronogramaRota.numeroCiclo ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota, ");
		hql.append(" RotaImpl rota, ");
		hql.append(" CronogramaAtividadeFaturamentoImpl cronogramaAtividadeFaturamento ");
		hql.append(" WHERE ");
		hql.append(" cronogramaRota.habilitado = true ");
		hql.append(" AND cronogramaRota.dataRealizada is not null ");
		hql.append(" AND rota IN (:rotas) ");
		hql.append(" AND cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :chaveAtividadeSistema ");
		hql.append(" order by cronogramaRota.anoMesReferencia desc ");
		hql.append(" ,cronogramaRota.numeroCiclo desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("rotas", rotas);
		query.setLong("chaveAtividadeSistema", AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA);

		return this.construirMapaRotaPontoConsumo(query.list() );
	}
	/**
	 *  Constrói o mapa de CronogramaRota por Rota, a partir da lista de atributos
	 *  recebida por parâmetro. Obtém os seguintes atributos, do CronogramaRota:
	 *  {@code chavePrimaria}, {@code anoMesReferencia}, {@code numeroCiclo}.
	 *  Obtém os seguintes atributos da Rota: {@code chavePrimaria}.
	 * @param listaAtributos
	 * @return mapa de CronogramaRota por Rota
	 */
	private Map<Rota, CronogramaRota> construirMapaRotaPontoConsumo(List<Object[]> listaAtributos) {
		Map<Rota, CronogramaRota>  mapa = new HashMap<>();
		for ( Object[] atributos : listaAtributos ) {
			Rota rota = new RotaImpl();
			rota.setChavePrimaria((long) atributos[0]);
			CronogramaRota cronogramaRota = (CronogramaRota) criarCronogramaRota();
			cronogramaRota.setChavePrimaria((long) atributos[1]);
			cronogramaRota.setAnoMesReferencia((Integer) atributos[2]);
			cronogramaRota.setNumeroCiclo((Integer) atributos[3]);
			if ( !mapa.containsKey(rota) ) {
				mapa.put(rota, cronogramaRota);
			}
		}
		return mapa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#verificarCronogramaGrupoFaturamento(java.lang.Long,
	 * java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public void verificarCronogramaGrupoFaturamento(Long idGrupoFaturamento, Integer mesAnoPartida, Integer numeroCiclo)
					throws NegocioException, ConcorrenciaException {

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		CronogramaFaturamento cronogramaFaturamento = this.obterUltimoCronogramaFaturamentoPorGrupoFaturamento(idGrupoFaturamento);

		if (cronogramaFaturamento == null) {

			GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
			grupoFaturamento.setAnoMesReferencia(mesAnoPartida);
			grupoFaturamento.setNumeroCiclo(numeroCiclo);
			atualizar(grupoFaturamento, grupoFaturamento.getClass());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * mapearCamposGrupoFaturamento(java.lang.
	 * Long)
	 */
	@Override
	public Map<String, String> mapearCamposGrupoFaturamento(Long idGrupoFaturamento, Long idCronogramaFaturamento, String mesAnoPartida,
					String cicloPartida) throws GGASException {

		Map<String, String> dados = new LinkedHashMap<>();

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		ControladorGrupoFaturamento controladorGrupoFaturamento = ServiceLocator.getInstancia().getControladorGrupoFaturamento();
		ParametroSistema parametroSistema = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);

		Boolean primeiroCronograma = true;
		CronogramaFaturamento cronogramaFaturamento = null;
		Object[] arrayCronogramaAtividadeFaturamento = null;
		Integer anoMes = null;
		Integer ciclo = 1;
		StringBuilder listaChaveAtividades = new StringBuilder();
		StringBuilder listaDatasPrevistas = new StringBuilder();
		StringBuilder listaDatasIniciais = new StringBuilder();
		StringBuilder listaDatasFinais = new StringBuilder();
		DateTime dataUltimaAtividadeCicloAnterior = null;

		// dados gerais
		Integer totalRotas = controladorRota.quantidadeRotasPorGrupoFaturamento(idGrupoFaturamento);
		Integer totalMedidores = controladorPontoConsumo.quantidadeMedidoresPorGrupoFaturamento(idGrupoFaturamento);
		Periodicidade periodicidade = controladorRota.obterPeriodicidadePorGrupoFaturamento(idGrupoFaturamento);
		GrupoFaturamento grupoFaturamento = (GrupoFaturamento) super.obter(idGrupoFaturamento, GrupoFaturamentoImpl.class);
		Collection<CronogramaFaturamento> listaCronogramaFaturamento =
				this.consultarCronogramaPorGrupoFaturamentoSemDataRealizacao(idGrupoFaturamento);
		Integer qtdeMaximaCronogramasAbertos = Integer.valueOf((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS));

		dados.put("qtdRotas", String.valueOf(totalRotas));
		dados.put("qtdMedidores", String.valueOf(totalMedidores));
		anoMes = Integer.parseInt(parametroSistema.getValor());
		dados.put("referenciaAtual", Util.formatarAnoMes(anoMes));
		dados.put("cronogramasAbertos", String.valueOf(listaCronogramaFaturamento.size()));
		dados.put("qtdeMaximaCronogramasAbertos", String.valueOf(qtdeMaximaCronogramasAbertos - (listaCronogramaFaturamento.size())));

		if (periodicidade != null) {
			dados.put("descPeriodicidade", periodicidade.getDescricao());
		}

		// obtém o cronograma do ciclo anterior
		cronogramaFaturamento = controladorCronogramaFaturamento.obterUltimoCronogramaFaturamentoPorGrupoFaturamento(idGrupoFaturamento);

		Collection<Long> listaAtividadeSistemas = this.listarChavesAtividadeSistemaPorCronograma();

		if (cronogramaFaturamento != null) {
			List<CronogramaAtividadeFaturamento> listaCronogramaAtividadeFaturamentos = controladorCronogramaFaturamento
							.consultarCronogramaAtividadeFaturamentoPorCronogramaFaturamento(cronogramaFaturamento);

			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamentoCicloAnterior = listaCronogramaAtividadeFaturamentos
							.get(listaCronogramaAtividadeFaturamentos.size() - 1);

			dataUltimaAtividadeCicloAnterior = new DateTime(cronogramaAtividadeFaturamentoCicloAnterior.getDataFim());

			if (cronogramaAtividadeFaturamentoCicloAnterior.getDataRealizacao() != null) {

				dataUltimaAtividadeCicloAnterior = new DateTime(cronogramaAtividadeFaturamentoCicloAnterior.getDataRealizacao());
			}

			arrayCronogramaAtividadeFaturamento = listaCronogramaAtividadeFaturamentos.toArray();

			anoMes = cronogramaFaturamento.getAnoMesFaturamento();
			ciclo = cronogramaFaturamento.getNumeroCiclo();

			Map<String, Integer> referenciaCiclo = null;
			if (controladorGrupoFaturamento.isPeriodicidadeGrupoMultiplosCiclos(grupoFaturamento)) {
				referenciaCiclo = Util.gerarProximaReferenciaCiclo(anoMes, ciclo, periodicidade.getQuantidadeDias(),
						dataUltimaAtividadeCicloAnterior);
			} else {
				referenciaCiclo = Util.rolarReferenciaCiclo(anoMes, ciclo, periodicidade.getQuantidadeCiclo());
			}

			anoMes = referenciaCiclo.get("referencia");
			ciclo = referenciaCiclo.get("ciclo");

		} else {
			// caso o usuário altere o anoMesPartida na tela
			if (mesAnoPartida != null && !"__/____".equals(mesAnoPartida)) {
				if (mesAnoPartida.length() == 7) {

					try {
						anoMes = Integer.parseInt(Util.converterMesAnoEmAnoMes(mesAnoPartida));
						ciclo = Integer.parseInt(cicloPartida);
					} catch (NumberFormatException e) {
						throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO);
					}
				} else {
					throw new NegocioException(Constantes.ERRO_FORMATO_INVALIDO);
				}

			} else {

				anoMes = grupoFaturamento.getAnoMesReferencia();
				ciclo = grupoFaturamento.getNumeroCiclo();
			}
		}

		int i = 0;
		int j = 0;
		for (Long atividadeSistema : listaAtividadeSistemas) {
			if (arrayCronogramaAtividadeFaturamento != null && arrayCronogramaAtividadeFaturamento.length > 0) {

				primeiroCronograma = false;

				if (j == arrayCronogramaAtividadeFaturamento.length) {

					this.obterDatasSugestaoCronograma(dataUltimaAtividadeCicloAnterior, periodicidade, listaDatasPrevistas,
									listaDatasIniciais, listaDatasFinais, null, grupoFaturamento.getTipoLeitura(), ciclo, null);

					listaChaveAtividades.append(atividadeSistema);
				} else {

					for (; j < arrayCronogramaAtividadeFaturamento.length;) {

						CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento)
								arrayCronogramaAtividadeFaturamento[j];
						if (atividadeSistema == cronogramaAtividadeFaturamento.getAtividadeSistema().getChavePrimaria()) {

							listaChaveAtividades.append(atividadeSistema);

							DateTime dataPrevistaCicloAnterior = new DateTime(cronogramaAtividadeFaturamento.getDataFim());

							if (cronogramaAtividadeFaturamento.getDataRealizacao() != null) {

								dataPrevistaCicloAnterior = new DateTime(cronogramaAtividadeFaturamento.getDataRealizacao());
							}

							this.obterDatasSugestaoCronograma(dataPrevistaCicloAnterior, periodicidade, listaDatasPrevistas,
											listaDatasIniciais, listaDatasFinais, Boolean.TRUE, grupoFaturamento.getTipoLeitura(), ciclo,
											null);

							j++;
							break;

						} else {

							this.obterDatasSugestaoCronograma(dataUltimaAtividadeCicloAnterior, periodicidade, listaDatasPrevistas,
											listaDatasIniciais, listaDatasFinais, null, grupoFaturamento.getTipoLeitura(), ciclo, null);

							listaChaveAtividades.append(atividadeSistema);
							break;
						}

					}
				}

			} else {

				// fluxo para grupo de faturamento sem cronograma anterior
				Map<String, DateTime> mapaDatas = this.obterDataInicialFinal(anoMes);
				DateTime dataInicial = mapaDatas.get("dataInicial");
				DateTime dataFinal = mapaDatas.get("dataFinal");
				listaChaveAtividades.append(atividadeSistema);
				listaDatasIniciais.append(Util.converterDataParaStringSemHora(dataInicial.toDate(), Constantes.FORMATO_DATA_BR));
				listaDatasFinais.append(Util.converterDataParaStringSemHora(dataFinal.toDate(), Constantes.FORMATO_DATA_BR));

			}

			if (i < listaAtividadeSistemas.size()) {
				listaChaveAtividades.append(",");
				listaDatasIniciais.append(",");
				listaDatasFinais.append(",");
				listaDatasPrevistas.append(",");
			}

			i++;
		}

		dados.put("listaDatasPrevistas", listaDatasPrevistas.toString());
		dados.put("primeiroCronograma", String.valueOf(primeiroCronograma));
		dados.put("mesAnoPartida", Util.formatarAnoMes(anoMes));
		dados.put("qtdCiclos", String.valueOf(ciclo));
		dados.put("listaChaveAtividades", listaChaveAtividades.toString());
		dados.put("listaDatasIniciais", listaDatasIniciais.toString());
		dados.put("listaDatasFinais", listaDatasFinais.toString());

		return dados;

	}

	/**
	 * @param dataPrevistaCicloAnterior - a data prevista do ciclo anterior
	 * @param periodicidade - a periodicidade do grupo de faturamento
	 * @return Map - Retonra um mapa com as datas de sugestão do cronograma
	 */
	private Map<String, Object> obterDatasSugestaoCronograma(DateTime dataPrevistaCicloAnterior,
			Periodicidade periodicidade, Boolean isCronogramaComplementar) {
		Map<String, Object> mapaDatas = new LinkedHashMap<>();
		DateTime dataInicialCiclo = dataPrevistaCicloAnterior.plusDays(1);
		DateTime dataPrevista = dataInicialCiclo;
		DateTime dataInicial = null;
		DateTime dataFinal = null;

		int quantidadeDiasPeriodicidade = periodicidade.getQuantidadeDias();
		if(isCronogramaComplementar) {
			dataPrevista = dataPrevistaCicloAnterior;
		}else{
			dataPrevista = dataInicialCiclo.plusDays(quantidadeDiasPeriodicidade).minusDays(1);
		}
		mapaDatas.put(DATA_PREVISTA, dataPrevista.toDate());

		Integer numeroMinimoDiasCiclo = periodicidade.getNumeroMinimoDiasCiclo();
		if (numeroMinimoDiasCiclo != null) {
			int minimoDias = quantidadeDiasPeriodicidade - numeroMinimoDiasCiclo;
			dataInicial = dataPrevista.minusDays(minimoDias);
		} else {
			dataInicial = dataPrevista;
		}
		mapaDatas.put(DATA_INICIAL, dataInicial.toDate());

		Integer numeroMaxiDiasCiclo = periodicidade.getNumeroMaximoDiasCiclo();
		if (numeroMaxiDiasCiclo != null) {
			int maximoDias = numeroMaxiDiasCiclo - quantidadeDiasPeriodicidade;
			dataFinal = dataPrevista.plusDays(maximoDias);
		} else {
			dataFinal = dataPrevista;
		}
		mapaDatas.put(DATA_FINAL, dataFinal.toDate());
		return mapaDatas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#obterDatasSugestaoCronograma(org.joda.time.DateTime,
	 * br.com.ggas.medicao.rota.Periodicidade, java.lang.StringBuffer, java.lang.StringBuffer, java.lang.StringBuffer, java.lang.Boolean,
	 * br.com.ggas.cadastro.imovel.TipoLeitura, java.lang.Integer, br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public Map<String, Object> obterDatasSugestaoCronograma(DateTime dataPrevistaCicloAnterior, Periodicidade periodicidade,
					StringBuilder listaDatasPrevistas, StringBuilder listaDatasIniciais, StringBuilder listaDatasFinais,
					Boolean verificarDataPrevista, TipoLeitura tipoLeitura, Integer cicloTmp, Rota rota) throws GGASException {

		int ultimoDiaMesAnterior = dataPrevistaCicloAnterior.dayOfMonth().getMaximumValue();
		Map<String, Object> mapaDatas = new LinkedHashMap<>();
		DateTime dataInicialCiclo = dataPrevistaCicloAnterior.plusDays(1);
		DateTime dataPrevista = dataInicialCiclo;
		DateTime dataInicial = null;
		DateTime dataFinal = null;
		Integer ciclo = cicloTmp;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistemaTipoLeituraEletroCorretor = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);
		ControladorFeriado controladorFeriado = (ControladorFeriado) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);

		int anoMesInicial = Util.obterAnoMes(dataInicialCiclo.toDate());

		dataPrevista = dataInicialCiclo.plusDays(periodicidade.getQuantidadeDias());
		int ultimoDiaMes = dataInicialCiclo.dayOfMonth().getMaximumValue();

		if (verificarDataPrevista != null && verificarDataPrevista) {

			if (tipoLeitura.getChavePrimaria() == Integer.parseInt(constanteSistemaTipoLeituraEletroCorretor.getValor())) {
				if (periodicidade.getConsideraMesCivil() != null && periodicidade.getConsideraMesCivil()) {

					int anoMesFinal = Util.obterAnoMes(dataPrevista.toDate());

					// TODO: ajuste para o mês/ano-ciclo não quebrar quando a periodicidade tiver 14 dias e 3 ciclos
					if (periodicidade.getQuantidadeDias() == Constantes.QUANTIDADE_DIAS_PERIODICIDADE
									&& periodicidade.getQuantidadeCiclo() == Constantes.QUANTIDADE_CICLOS_PERIODICIDADE
									&& ultimoDiaMesAnterior == Constantes.ULTIMO_DIA_FEVEREIRO
									&& ultimoDiaMes != Constantes.ULTIMO_DIA_FEVEREIRO
									|| periodicidade.getQuantidadeDias() == Constantes.QUANTIDADE_DIAS_PERIODICIDADE_SEMANAL
									&& periodicidade.getQuantidadeCiclo() == Constantes.QUANTIDADE_CICLOS_PERIODICIDADE_SEMANAL
									&& ultimoDiaMesAnterior != Constantes.ULTIMO_DIA_FEVEREIRO
									&& ultimoDiaMes == Constantes.ULTIMO_DIA_FEVEREIRO
									|| periodicidade.getQuantidadeDias() == Constantes.QUANTIDADE_DIAS_PERIODICIDADE_SEMANAL
									&& periodicidade.getQuantidadeCiclo() == Constantes.QUANTIDADE_CICLOS_PERIODICIDADE_SEMANAL
									&& ultimoDiaMesAnterior == Constantes.ULTIMO_DIA_FEVEREIRO
									&& ultimoDiaMes != Constantes.ULTIMO_DIA_FEVEREIRO) {

						ciclo = 1;
					}

					if (anoMesFinal > anoMesInicial || ciclo.equals(periodicidade.getQuantidadeCiclo())) {
						String mes = String.valueOf(dataInicialCiclo.getMonthOfYear());

						if (dataInicialCiclo.getMonthOfYear() < MES_OUTUBRO) {
							mes = "0" + dataInicialCiclo.getMonthOfYear();
						}

						int ano = dataInicialCiclo.getYear();
						String dataPrevistaAux = ultimoDiaMes + "/" + mes + "/" + ano;
						dataPrevista = new DateTime(Util.converterCampoStringParaData("", dataPrevistaAux, Constantes.FORMATO_DATA_BR)).plusDays(1);
					}

				}

				if (listaDatasPrevistas != null) {

					listaDatasPrevistas.append(Util.converterDataParaStringSemHora(dataPrevista.toDate(), Constantes.FORMATO_DATA_BR));
				}
			} else {

				dataPrevista = this.verificarDiaUtil(dataPrevista, listaDatasPrevistas);

				if (rota != null && rota.getSetorComercial() != null) {

					Long idMunicipio = rota.getSetorComercial().getMunicipio().getChavePrimaria();

					if (!controladorFeriado.isDiaUtil(dataPrevista.toDate(), idMunicipio, Boolean.TRUE)) {
						dataPrevista = new DateTime(controladorFeriado.obterProximoDiaUtil(dataPrevista.toDate(), idMunicipio, Boolean.TRUE));
					}
				}
			}
			mapaDatas.put(DATA_PREVISTA, dataPrevista.toDate());
		}

		if (periodicidade.getNumeroMinimoDiasCiclo() != null) {

			int minimoDias = periodicidade.getQuantidadeDias() - periodicidade.getNumeroMinimoDiasCiclo();
			dataInicial = dataPrevista.minusDays(minimoDias);

		} else {
			dataInicial = dataPrevista;
		}
		dataInicial = this.verificarDiaUtil(dataInicial, listaDatasIniciais);
		mapaDatas.put("dataInicial", dataInicial.toDate());

		if (periodicidade.getNumeroMaximoDiasCiclo() != null) {

			int maximoDias = periodicidade.getNumeroMaximoDiasCiclo() - periodicidade.getQuantidadeDias();
			dataFinal = dataPrevista.plusDays(maximoDias);

		} else {
			dataFinal = dataPrevista;
		}
		dataFinal = this.verificarDiaUtil(dataFinal, listaDatasFinais);
		mapaDatas.put("dataFinal", dataFinal.toDate());

		mapaDatas.put("anoMesInicial", anoMesInicial);
		mapaDatas.put("ciclo", ciclo);

		return mapaDatas;
	}

	/**
	 * Obter data inicial final.
	 *
	 * @param anoMes
	 *            the ano mes
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Map<String, DateTime> obterDataInicialFinal(Integer anoMes) throws NegocioException {

		Map<String, DateTime> mapaDatas = new LinkedHashMap<>();

		try {
			DateTime dataInicial = new DateTime(Util.converterCampoStringParaData("Inicial Atividade", "01/" + Util.formatarAnoMes(anoMes),
							Constantes.FORMATO_DATA_BR));

			DateTime dataFinal = new DateTime(Util.converterCampoStringParaData("Final Atividade", "01/" + Util.formatarAnoMes(anoMes),
							Constantes.FORMATO_DATA_BR));

			dataInicial = dataInicial.minusMonths(1);
			dataFinal = dataFinal.plusMonths(1);

			ControladorFeriado controladorFeriado = (ControladorFeriado) ServiceLocator.getInstancia().getBeanPorID(
							ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);

			if (!controladorFeriado.isDiaUtil(dataInicial.toDate())) {
				dataInicial = new DateTime(controladorFeriado.obterProximoDiaUtil(dataInicial.toDate()));
			}

			dataFinal = new DateTime(Util.converterCampoStringParaData("Final Atividade",
							Util.obterUltimoDiaMes(dataFinal.toDate(), Constantes.FORMATO_DATA_BR), Constantes.FORMATO_DATA_BR));
			if (!controladorFeriado.isDiaUtil(dataFinal.toDate())) {
				dataFinal = new DateTime(controladorFeriado.obterProximoDiaUtil(dataFinal.toDate()));
			}

			mapaDatas.put("dataInicial", dataInicial);
			mapaDatas.put("dataFinal", dataFinal);

		} catch (GGASException e) {
			LOG.error(e.getStackTrace(), e);
			throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, true);
		}

		return mapaDatas;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#verificarDiaUtil(org.joda.time.DateTime,
	 * java.lang.StringBuffer)
	 */
	@Override
	public DateTime verificarDiaUtil(DateTime data, StringBuilder listaDatas) throws NegocioException {

		ControladorFeriado controladorFeriado = (ControladorFeriado) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);

		if (!controladorFeriado.isDiaUtil(data.toDate())) {
			data = new DateTime(controladorFeriado.obterProximoDiaUtil(data.toDate()));
		}

		if (listaDatas != null) {

			listaDatas.append(Util.converterDataParaStringSemHora(data.toDate(), Constantes.FORMATO_DATA_BR));
		}

		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarCronogramaAtividadeFaturamento(java.util.Map,
	 * java.lang.String, java.lang.String[])
	 */
	// TODO: REMOVER O CONTROLADOR CRONOGRAMA ATIVIDADE FATURAMENTO
	@Override
	@SuppressWarnings({"unchecked"})
	public Collection<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamento(Map<String, Object> filtro, String ordenacao,
					String... propriedadesLazy) throws NegocioException {

		Query query = null;
		Boolean habilitado = null;
		Long idGrupoFaturamento = null;
		Date dataLeitura = null;
		Long idAtividade = null;
		Boolean atividadeRealizada = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamento ");

		hql.append("inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");

		if (!filtro.isEmpty()) {
			habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			idGrupoFaturamento = (Long) filtro.get("idGrupoFaturamento");
			dataLeitura = (Date) filtro.get("dataLeitura");
			idAtividade = (Long) filtro.get("idAtividade");
			atividadeRealizada = (Boolean) filtro.get("atividadeRealizada");

			hql.append(" where 1 = 1 ");

			if (habilitado != null) {
				hql.append(" and cronogramaAtividadeFaturamento.habilitado =:habilitado ");
			}

			if (idGrupoFaturamento != null) {
				hql.append(" and cronogramaFaturamento.grupoFaturamento =:idGrupoFaturamento ");
			}

			if (dataLeitura != null) {
				hql.append(" and cronogramaAtividadeFaturamento.dataFim >=:dataLeitura");
			}

			if (idAtividade != null) {
				hql.append(" and cronogramaAtividadeFaturamento.atividadeSistema =:idAtividade ");
			}

			if (atividadeRealizada != null) {
				if (atividadeRealizada) {

					hql.append(" and cronogramaAtividadeFaturamento.dataRealizacao is not null ");
				} else {
					hql.append(" and cronogramaAtividadeFaturamento.dataRealizacao is null ");
				}

			}

		}

		if (ordenacao != null) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!filtro.isEmpty()) {

			if (habilitado != null) {
				query.setBoolean(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
			}

			if (idGrupoFaturamento != null) {
				query.setLong("idGrupoFaturamento", idGrupoFaturamento);
			}

			if (dataLeitura != null) {
				query.setDate("dataLeitura", dataLeitura);
			}

			if (idAtividade != null) {
				query.setLong("idAtividade", idAtividade);
			}

			// Paginação em banco dados
			ColecaoPaginada colecaoPaginada = null;
			if (filtro.containsKey("colecaoPaginada") && filtro.get("colecaoPaginada") != null) {
				colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

				HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query, getClasseEntidadeCronogramaFaturamento().getSimpleName(),
								hql.toString());

			}

			if (colecaoPaginada != null) {

				if (colecaoPaginada.getIndex() == 1) {
					query.setFirstResult(0);
				} else {
					query.setFirstResult(colecaoPaginada.getIndex());
				}

				query.setMaxResults(colecaoPaginada.getObjectsPerPage());
			}
		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > 0) {

			lista = super.inicializarLazyColecao(lista, propriedadesLazy);

		}

		return (Collection<CronogramaAtividadeFaturamento>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento
	 * #consultarCronogramaRota
	 * (br.com.ggas.faturamento
	 * .cronograma.CronogramaAtividadeFaturamento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public CronogramaRota consultarCronogramaRota(Rota rota, Integer anoMesReferencia, Integer numeroCiclo, Long idAtividadeSistema)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cronogramaRota from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" where cronogramaRota.rota.chavePrimaria = :idRota ");
		hql.append(" and cronogramaRota.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and cronogramaRota.numeroCiclo = :ciclo ");
		hql.append(" and cronogramaRota.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividadeSistema ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter("ciclo", numeroCiclo);
		query.setParameter("idAtividadeSistema", idAtividadeSistema);

		query.setMaxResults(1);
		return (CronogramaRota) query.uniqueResult();
	}
	
	 /*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#desfazerGerarDados(java.util.Map)
	 */
	@Override
	public void desfazerGerarDados(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException {	
		    
		    
		    ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator.getInstancia().getControladorNegocio(
		    		ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
		   
	        
	        
	        	Collection<LeituraMovimento> listaLeituraMovimento = controladorLeituraMovimento.consultarMovimentosLeituraNaoProcessadosPorRota(parametros);
	        	if (!listaLeituraMovimento.isEmpty()) {
	        		
	        	for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {
					this.remover(leituraMovimento, ControladorLeituraMovimentoImpl.class);
					
				}
	
	        		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
	    			
		        	parametros.put("chaveAtividadeSistema", AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA);
	    			this.removerDataAtividadeCronograma(parametros);
	        	} else {
	    			throw new NegocioException(Constantes.ERRO_DESFAZER_GERAR_DADOS_LEITURA, true);
	    		}
		
				
	 }


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#desfazerConsistirLeitura(java.util.Map)
	 */
	@Override
	public void desfazerConsistirLeitura(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Collection<Fatura> listaFatura = controladorFatura.consultarFaturasPorRota(parametros);

		if (listaFatura.isEmpty()) {
			Collection<HistoricoConsumo> listaHistoricoConsumo = this.consultarDadosConsistirLeitura(parametros);

			for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
				this.remover(historicoConsumo, HistoricoConsumoImpl.class);
			}

			getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
			
			parametros.put("chaveAtividadeSistema", AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA);
			this.removerDataAtividadeCronograma(parametros);

		} else {
			throw new NegocioException(Constantes.ERRO_DESFAZER_CONSISTIR_LEITURA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#removerDataAtividadeCronograma(java.util.Map)
	 */
	@Override
	public void removerDataAtividadeCronograma(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException {

		Long chaveCronogramaFaturamento = (Long) parametros.get("chaveCronogramaFaturamento");
		Integer numeroCiclo = (Integer) parametros.get("numeroCiclo");
		Integer anoMesFaturamento = (Integer) parametros.get("anoMesFaturamento");
		Long chaveAtividadeSistema = (Long) parametros.get("chaveAtividadeSistema");
		Long chaveRota = (Long) parametros.get(CHAVE_ROTA);
		StringBuilder hql = new StringBuilder();

		hql.append(" select cronogramaRota, cronogramaAtividadeFaturamento from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join cronogramaRota.rota rota  ");
		hql.append(" inner join cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento  ");
		hql.append(" where cronogramaAtividadeFaturamento.chavePrimaria in ( ");

		hql.append(" select cronogramaAtividadeFaturamentoSubConsulta.chavePrimaria from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamentoSubConsulta ");
		hql.append(" inner join cronogramaAtividadeFaturamentoSubConsulta.atividadeSistema atividadeSistema ");
		hql.append(" inner join cronogramaAtividadeFaturamentoSubConsulta.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" where 1 = 1 ");

		if (chaveCronogramaFaturamento != null) {
			hql.append(" and cronogramaFaturamento.chavePrimaria = :chaveCronogramaFaturamento ");
		}

		if (numeroCiclo != null) {
			hql.append(" and cronogramaFaturamento.numeroCiclo = :numeroCiclo ");
		}

		if (anoMesFaturamento != null) {
			hql.append(" and cronogramaFaturamento.anoMesFaturamento = :anoMesFaturamento ");
		}

		hql.append(" and atividadeSistema.chavePrimaria = :chaveAtividadeSistema ) ");

		hql.append(" and cronogramaRota.dataRealizada is not null ");
		hql.append(" and rota.chavePrimaria = :chaveRota ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chaveCronogramaFaturamento != null) {
			query.setParameter("chaveCronogramaFaturamento", chaveCronogramaFaturamento);
		}

		if (numeroCiclo != null) {
			query.setParameter("numeroCiclo", numeroCiclo);
		}

		if (anoMesFaturamento != null) {
			query.setParameter("anoMesFaturamento", anoMesFaturamento);
		}

		query.setParameter("chaveAtividadeSistema", chaveAtividadeSistema);

		query.setParameter(CHAVE_ROTA, chaveRota);

		@SuppressWarnings("unchecked")
		Collection<Object[]> listaCronogramaRota = query.list();

		for (Object[] objeto : listaCronogramaRota) {

			CronogramaRota cronogramaRota = (CronogramaRota) objeto[0];
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento) objeto[1];
			cronogramaRota.setDataRealizada(null);
			cronogramaAtividadeFaturamento.setDataRealizacao(null);
			this.atualizar(cronogramaRota, CronogramaRotaImpl.class);
			this.atualizar(cronogramaAtividadeFaturamento, CronogramaAtividadeFaturamentoImpl.class);
		}

	}

	/**
	 * Consultar dados consistir leitura.
	 *
	 * @param parametros
	 *            the parametros
	 * @return the collection
	 */
	public Collection<HistoricoConsumo> consultarDadosConsistirLeitura(Map<String, Object> parametros) {

		Long chaveRota = (Long) parametros.get(CHAVE_ROTA);
		Integer anoMesFaturamento = (Integer) parametros.get("anoMesFaturamento");
		Integer numeroCiclo = (Integer) parametros.get("numeroCiclo");

		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoConsumo from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" inner join historicoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" where ");
		hql.append(" historicoConsumo.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historicoConsumo.numeroCiclo = :numeroCiclo ");

		if (chaveRota != null && chaveRota > 0) {
			hql.append(" and rota.chavePrimaria = :chaveRota ");
		}
		hql.append(" and historicoConsumo.chavePrimaria not in ( ");

		hql.append(" select historicoConsumoSubConsulta.chavePrimaria from ");
		hql.append(" FaturaImpl ");
		hql.append(" fatura ");
		hql.append(" inner join fatura.historicoConsumo historicoConsumoSubConsulta ");
		hql.append(" inner join fatura.pontoConsumo pontoConsumoSubConsulta ");
		hql.append(" inner join pontoConsumoSubConsulta.rota rotaSubConsulta ");
		hql.append(" where ");
		hql.append(" fatura.anoMesReferencia = :anoMesFaturamento ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");

		if (chaveRota != null && chaveRota > 0) {
			hql.append(" and rotaSubConsulta.chavePrimaria = :chaveRota ");
		}

		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("anoMesFaturamento", anoMesFaturamento);
		query.setParameter("numeroCiclo", numeroCiclo);

		if (chaveRota != null && chaveRota > 0) {
			query.setParameter(CHAVE_ROTA, chaveRota);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarDadosRegistrarLeitura(java.util.Map)
	 */
	@Override
	public Collection<HistoricoMedicao> consultarDadosRegistrarLeitura(Map<String, Object> parametros) {

		Long chaveRota = (Long) parametros.get(CHAVE_ROTA);
		Integer numeroCiclo = (Integer) parametros.get("numeroCiclo");
		Integer anoMesFaturamento = (Integer) parametros.get("anoMesFaturamento");
		String codigoPontoConsumoSupervisorio = (String) parametros.get("codigoPontoConsumoSupervisorio");
		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoMedicao from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" historicoMedicao ");
		hql.append(" inner join historicoMedicao.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" where ");
		hql.append(" historicoMedicao.anoMesLeitura = :anoMesFaturamento ");
		hql.append(" and historicoMedicao.numeroCiclo = :numeroCiclo ");

		if (chaveRota != null) {
			hql.append(" and rota.chavePrimaria = :chaveRota ");
		}
		if (codigoPontoConsumoSupervisorio != null) {
			hql.append(" and pontoConsumo.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio ");
		}
		hql.append(" and historicoMedicao.chavePrimaria not in ");

		hql.append(" ( select historicoAtual.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumoSubConsulta_1 ");
		hql.append(" inner join historicoConsumoSubConsulta_1.historicoAtual historicoAtual ");
		hql.append(" inner join historicoConsumoSubConsulta_1.pontoConsumo pontoConsumoSubConsulta_1 ");
		hql.append(" inner join pontoConsumoSubConsulta_1.rota rotaSubConsulta_1 ");
		hql.append(" where 1 = 1 ");

		if (chaveRota != null) {
			hql.append(" and rotaSubConsulta_1.chavePrimaria = :chaveRota ");
		}
		if (codigoPontoConsumoSupervisorio != null) {
			hql.append(" and pontoConsumoSubConsulta_1.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio ");
		}

		hql.append(" and historicoConsumoSubConsulta_1.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historicoConsumoSubConsulta_1.numeroCiclo = :numeroCiclo ) ");

		hql.append(" and historicoMedicao.chavePrimaria not in ");

		hql.append(" ( select historicoAnterior.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumoSubConsulta_2 ");

		hql.append(" inner join historicoConsumoSubConsulta_2.historicoAnterior historicoAnterior ");
		hql.append(" inner join historicoConsumoSubConsulta_2.pontoConsumo pontoConsumoSubConsulta_2 ");
		hql.append(" inner join pontoConsumoSubConsulta_2.rota rotaSubConsulta_2 ");
		hql.append(" where 1 = 1 ");

		if (chaveRota != null) {
			hql.append(" and rotaSubConsulta_2.chavePrimaria = :chaveRota ");
		}
		if (codigoPontoConsumoSupervisorio != null) {
			hql.append(" and pontoConsumoSubConsulta_2.codigoPontoConsumoSupervisorio = :codigoPontoConsumoSupervisorio ");
		}

		hql.append(" and historicoConsumoSubConsulta_2.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historicoConsumoSubConsulta_2.numeroCiclo = :numeroCiclo ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("anoMesFaturamento", anoMesFaturamento);

		query.setParameter("numeroCiclo", numeroCiclo);

		if (chaveRota != null) {
			query.setParameter(CHAVE_ROTA, chaveRota);
		}
		if (codigoPontoConsumoSupervisorio != null) {
			query.setParameter("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#desfazerRegistrarLeitura(java.util.Map)
	 */
	@Override
	public void desfazerRegistrarLeitura(Map<String, Object> parametros) throws NegocioException, ConcorrenciaException {

		Collection<HistoricoConsumo> listaHistoricoConsumo = this.consultarDadosConsistirLeitura(parametros);

		if (listaHistoricoConsumo.isEmpty()) {
			Collection<HistoricoMedicao> listaHistoricoMedicao = this.consultarDadosRegistrarLeitura(parametros);

			for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
				this.remover(historicoMedicao, HistoricoMedicaoImpl.class);
			}

			parametros.put("chaveAtividadeSistema", AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA);
			this.removerDataAtividadeCronograma(parametros);

			Long chaveRota = (Long) parametros.get(CHAVE_ROTA);

			ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

			ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

			ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

			Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.listarPontosConsumoPorChaveRota(chaveRota);

			Long anoMesReferenciaLido = Long.parseLong(parametros.get("anoMesFaturamento").toString());
			Long numeroCiclo = Long.parseLong(parametros.get("numeroCiclo").toString());

			parametros.put("colecaoPaginada", null);
			parametros.put(CHAVE_ROTA, null);
			parametros.put("chaveCronogramaFaturamento", null);
			parametros.put("numeroCiclo", numeroCiclo);
			parametros.put("anoMesReferenciaLido", anoMesReferenciaLido);
			parametros.put("anoMesFaturamento", null);
			parametros.put("indicadorProcessado", Boolean.TRUE);
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {

				String codigoPontoConsumoSupervisorio = pontoConsumo.getCodigoPontoConsumoSupervisorio();
				if (codigoPontoConsumoSupervisorio != null) {
					parametros.put("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio);

					Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = controladorSupervisorio
									.consultarSupervisorioMedicaoDiaria(parametros, null, new String[] {});

					for (SupervisorioMedicaoDiaria supervisorioMedicaoDiaria : listaSupervisorioMedicaoDiaria) {
						supervisorioMedicaoDiaria.setIndicadorProcessado(Boolean.FALSE);
						this.atualizar(supervisorioMedicaoDiaria, SupervisorioMedicaoDiariaImpl.class);
					}

				}

				Collection<LeituraMovimento> listaLeituraMovimento = controladorLeituraMovimento.consultarLeituraMovimentoPorRota(
								chaveRota, Integer.valueOf(anoMesReferenciaLido.toString()), null, pontoConsumo.getChavePrimaria(),
								Integer.valueOf(numeroCiclo.toString()));

				for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {
					leituraMovimento.setSituacaoLeitura(defineSituacaoLeitura(leituraMovimento, controladorLeituraMovimento));
					this.atualizar(leituraMovimento, LeituraMovimento.class);
				}

			}

		} else {
			throw new NegocioException(Constantes.ERRO_DESFAZER_REGISTRAR_LEITURA, true);
		}
	}

	private SituacaoLeituraMovimento defineSituacaoLeitura(LeituraMovimento leituraMovimento,
			ControladorLeituraMovimento controladorLeituraMovimento) throws NegocioException {
		if (leituraMovimento.getCodigoAnormalidadeLeitura() != null || leituraMovimento.getDataLeitura() != null) {
			return controladorLeituraMovimento.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.LEITURA_RETORNADA);
		} else {
			return controladorLeituraMovimento.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.GERADO);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento#consultarCronogramaFaturamentoEscalonamento(java.util.Map)
	 */
	@Override
	public Collection<CronogramaRota> consultarCronogramaFaturamentoEscalonamento(Map<String, Object> filtro) throws NegocioException {

		Long chaveCronogramaFaturamento = (Long) filtro.get("chaveCronogramaFaturamento");

		long registrarLeitura = CHAVE_PRIMARIA_ATIVIDADE_REGISTRAR_LEITURA;

		StringBuilder hql = new StringBuilder();

		hql.append(" select cronogramaRota from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" inner join fetch cronogramaFaturamento.grupoFaturamento grupoFaturamento ");
		hql.append(" inner join fetch grupoFaturamento.periodicidade periodicidade ");
		hql.append(" inner join fetch cronogramaRota.rota rota ");
		hql.append(" left join  rota.leiturista leiturista ");
		hql.append(" left join  leiturista.funcionario funcionario ");
		hql.append(" left join  leiturista.funcionario funcionario ");
		hql.append(" left join  funcionario.usuario usuario ");
		hql.append(" left join  rota.leituristaSuplente leituristaSuplente ");
		hql.append(" left join  leituristaSuplente.funcionario funcionarioSuplente ");
		hql.append(" where ");
		hql.append(" cronogramaFaturamento.chavePrimaria = :chaveCronogramaFaturamento ");
		hql.append(" and cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :registrarLeitura ");
		hql.append(" order by rota.numeroRota ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chaveCronogramaFaturamento", chaveCronogramaFaturamento);

		query.setParameter("registrarLeitura", registrarLeitura);

		return query.list();
	}

	/**
	 * Listar os Ciclos existentes do Supervisório
	 * @param idGrupoFaturamento
	 * @return lista de ciclos
	 */
	public Collection<Integer> listarCiclosCronograma() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cronogramaFaturamento.numeroCiclo ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaFaturamento().getSimpleName());
		hql.append(" cronogramaFaturamento ");
		hql.append(" order by cronogramaFaturamento.numeroCiclo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}



	/**
	 *  Consultar a data maior ou menor de leitura de um período de faturamento supervisório
	 */
	public Date obterDataMaximaOuMininaSupervisorioCronograma(Integer anoMesFaturamento,
					Integer numeroCiclo, Long grupoFaturamento, Boolean isMaxima) {

		StringBuilder hql = new StringBuilder();

		if(isMaxima) {
			hql.append(" select max(superMedicaoDiaria.dataRealizacaoLeitura) from  ");
		}else {
			hql.append(" select min(superMedicaoDiaria.dataRealizacaoLeitura) from  ");
		}

		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName());
		hql.append(" superMedicaoDiaria ");
		hql.append(" where superMedicaoDiaria.dataReferencia=:anoMesFaturamento");
		hql.append(" and superMedicaoDiaria.numeroCiclo= :numeroCiclo ");
		hql.append(" and superMedicaoDiaria.indicadorIntegrado is true ");
		hql.append(" and superMedicaoDiaria.habilitado is true ");
		hql.append(" and superMedicaoDiaria.codigoPontoConsumoSupervisorio in (");

		hql.append("		select pontoConsumo.codigoPontoConsumoSupervisorio from ");
		hql.append(			getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" 		pontoConsumo ");
		hql.append(" 		inner join pontoConsumo.rota rota ");
		hql.append(" 		where rota.grupoFaturamento.chavePrimaria=:grupoFaturamento)");


		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("anoMesFaturamento", anoMesFaturamento);

		query.setParameter("numeroCiclo", numeroCiclo);

		query.setParameter("grupoFaturamento", grupoFaturamento);

		return (Date) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.faturamento.cronograma.
	 * ControladorCronogramaFaturamento#
	 * consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo
	 * (GrupoFaturamento grupoFaturamento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<CronogramaAtividadeFaturamento> consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo(
			GrupoFaturamento grupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder(obterSqlConsultarCronogramaAtividadesPorGrupo());
		hql.append(" AND cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :chaveAtividadeSistemas");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveGrupoFaturamento", grupoFaturamento.getChavePrimaria());
		query.setInteger("anoMesReferencia", grupoFaturamento.getAnoMesReferencia());
		query.setInteger("ciclo", grupoFaturamento.getNumeroCiclo());

		return query.list();
	}
	
	/**
	 * Retorna o sql da consulta de cronograma atividade por 
	 * 		grupo, referencia e ciclo.
	 * 
	 * @return the sql
	 */
	private String obterSqlConsultarCronogramaAtividadesPorGrupo() {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT cronogramaAtividadeFaturamento ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento, ");
		hql.append(" CronogramaAtividadeFaturamentoImpl ");
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(" WHERE ");
		hql.append(" cronogramaFaturamento.chavePrimaria = cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria ");
		hql.append(" AND cronogramaFaturamento.habilitado = true ");
		hql.append(" AND ");
		hql.append(" cronogramaFaturamento.grupoFaturamento.chavePrimaria = :chaveGrupoFaturamento");
		hql.append(" AND cronogramaFaturamento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" AND cronogramaFaturamento.numeroCiclo = :ciclo ");
		
		return hql.toString();
	}

	/**
	 * Verifica se houve virada de mes do periodo de leitura adicionado com a periodicidade
	 * @param dataPeriodoDeLeitura - data periodo de leitura (Registrar Leitura)
	 * @param periodicidade - periodicidade do grupo de faturamento
	 * @return Boolean - TRUE se a data com a periodicidade virou o mes, false se o contrario
	 */
	public Boolean verificaViradaMesPeriodoLeituraComPeriodicade(DateTime dataPeriodoDeLeitura, Integer periodicidade) {
		DateTime dataComPeriodicidade = dataPeriodoDeLeitura.plusDays(periodicidade);
		DateTime ultimoDiaMes = Util.obterUltimoDiaDoMesParaData(dataPeriodoDeLeitura);
		Integer primeiroDia = dataComPeriodicidade.toCalendar(Locale.getDefault()).get(Calendar.DAY_OF_MONTH);
		if(dataComPeriodicidade.compareTo(ultimoDiaMes) > 0 && primeiroDia != 1) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	/**
	 * Retorna datas corretamente modificadas para criação do cronograma complementar
	 *
	 * @param datasPrevistas - Lista de datas previstas a serem modificadas
	 * @param diferencaDias - diferencia de dias do cronograma
	 * @return lista de datas modificadas
	 * @throws NegocioException the negocio exception
	 */
	public String[] retornaDatasPrevistasCronogramaComplementar(String[] datasPrevistas, Integer diferencaDias)
			throws NegocioException {
		for (int i = 0; i < datasPrevistas.length; i++) {
			if ((!",".equals(datasPrevistas[i]) && !"".equals(datasPrevistas[i])) && datasPrevistas[i]!= null) {
				Date dataAux = DataUtil.converterParaData(datasPrevistas[i]);
				DateTime dataAux1 = new DateTime(dataAux).plusDays(diferencaDias);
				datasPrevistas[i] = Util.converterDataParaStringSemHora(
						dataAux1.toDate(), Constantes.FORMATO_DATA_BR);
			}
		}
		return datasPrevistas;
	}
	/**
	 * 
	 * @return Collection<AtividadeSistema>
	 * @throws NegocioException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Long> listarChavesAtividadeSistemaPorCronograma() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select atividadeSistema.chavePrimaria ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" atividadeSistema ");
		hql.append(" left join atividadeSistema.modulo modulo ");
		hql.append(" left join atividadeSistema.operacao operacao ");
		hql.append(" WHERE ");
		hql.append(" atividadeSistema.habilitado = true and");
		hql.append(" atividadeSistema.cronograma = true ");
		hql.append(" order by ");
		hql.append(" atividadeSistema.sequencia");
		hql.append(" asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	
	
	private Boolean verificarSeNaoEhPrimeiroCiclo(Boolean isPrimeiroCronograma, String cicloPartida, GrupoFaturamento grupoFaturamento, DateTime dataAtualRegistrarLeitura ) {
		if (!"1".equals(cicloPartida) && isPrimeiroCronograma
				&& verificaViradaMesPeriodoLeituraComPeriodicade(
						dataAtualRegistrarLeitura.minusDays(grupoFaturamento.getPeriodicidade().getQuantidadeDias()),
						grupoFaturamento.getPeriodicidade().getQuantidadeDias())) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	
	private Boolean verificarSePrecisaDeCronogramaComplementar(Boolean isPrimeiroCronograma, GrupoFaturamento grupoFaturamento, DateTime dataPeriodoLeituraAnterior, Boolean isCronogramaNaoPrimeiroCiclo) throws NegocioException {
		ControladorGrupoFaturamento controladorGrupoFaturamento = ServiceLocator.getInstancia().getControladorGrupoFaturamento();
		if (!isPrimeiroCronograma && controladorGrupoFaturamento.isPeriodicidadeGrupoMultiplosCiclos(grupoFaturamento)
				&& dataPeriodoLeituraAnterior != null
				&& (verificaViradaMesPeriodoLeituraComPeriodicade(dataPeriodoLeituraAnterior,
						grupoFaturamento.getPeriodicidade().getQuantidadeDias())
						|| (verificaViradaMesPeriodoLeituraComPeriodicade(
								dataPeriodoLeituraAnterior
										.minusDays(grupoFaturamento.getPeriodicidade().getQuantidadeDias()),
								grupoFaturamento.getPeriodicidade().getQuantidadeDias())
								&& isCronogramaNaoPrimeiroCiclo))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	private Integer retornaDiferencaDiasParaCronogramaCorrente(String[] dataPrevistaAnterior, GrupoFaturamento grupoFaturamento,String[] datasPrevistas, DateTime dataPeriodoLeituraAnterior) throws NegocioException {
		Integer diferencaDias = 0;
		
		for (int k = 0; k < dataPrevistaAnterior.length; k++) {
			if ((!",".equals(dataPrevistaAnterior[k]) && !"".equals(dataPrevistaAnterior[k])) && dataPrevistaAnterior[k]!= null) {
				Date dataAux = DataUtil.converterParaData(datasPrevistas[k]);
				DateTime dataAux1 = new DateTime(dataAux).minusDays(grupoFaturamento.getPeriodicidade().getQuantidadeDias());
				dataPrevistaAnterior[k] = Util.converterDataParaStringSemHora(
						dataAux1.toDate(), Constantes.FORMATO_DATA_BR);
			}
		} 
		diferencaDias = DataUtil.diferencaDiasEntreDatas(dataPeriodoLeituraAnterior.minusDays(grupoFaturamento.getPeriodicidade().getQuantidadeDias()).toDate(),
				Util.obterUltimoDiaDoMesParaData(dataPeriodoLeituraAnterior.minusDays(grupoFaturamento.getPeriodicidade().getQuantidadeDias())).toDate()) + 1;
		
		return diferencaDias;
	}
	
	
	@Override
	public GrupoFaturamento obterGrupoFaturamentoPorCodigoSupervisorio(String codigoSupervisorio) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select grupoFaturamento ");
		hql.append(" FROM ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" grupoFaturamento.habilitado = true ");
		hql.append(" and pontoConsumo.codigoPontoConsumoSupervisorio like :codigoSupervisorio ");
		hql.append(" and pontoConsumo.habilitado = true " );
		
		
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setString("codigoSupervisorio", codigoSupervisorio);
		
		return (GrupoFaturamento) query.uniqueResult();
	}
	
	
	@Override
	public CronogramaFaturamento obterCronogramaFaturamentoAnterior(CronogramaFaturamento cronogramaFaturamento) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cronogramaFaturamento ");
		hql.append(" WHERE cronogramaFaturamento.grupoFaturamento.chavePrimaria = :grupoFaturamento ");
		hql.append(" AND cronogramaFaturamento.habilitado = true  ");
		hql.append(" AND (concat(str(cronogramaFaturamento.anoMesFaturamento),str(cronogramaFaturamento.numeroCiclo))) < :anoMesCiclo ");
		hql.append(" ORDER BY cronogramaFaturamento.chavePrimaria DESC");
		
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setLong("grupoFaturamento", cronogramaFaturamento.getGrupoFaturamento().getChavePrimaria());
		query.setString("anoMesCiclo",
				String.valueOf(cronogramaFaturamento.getAnoMesFaturamento()) + String.valueOf(cronogramaFaturamento.getNumeroCiclo()));
		
		query.setMaxResults(1);
		
		return (CronogramaFaturamento) query.uniqueResult();

	}
	
	
	@Override
	public GrupoFaturamento obterGrupoFaturamentoPorDescricao(String descricao) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" grupoFaturamento.habilitado = true ");
		hql.append(" and grupoFaturamento.descricao like :descricao ");
		
		
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setString("descricao", descricao.toUpperCase());
		
		return (GrupoFaturamento) query.uniqueResult();
	}
}
