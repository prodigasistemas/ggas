package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.util.StringUtil;

/**
 * DocumentoCobrancaVO
 * 
 * Classe responsável por documento cobranca VO.
 */
public class DocumentoCobrancaVO {
	private Long chavePrimaria;
	private Date dataEmissao;
	private BigDecimal valorTotal;
	private BigDecimal valorAtualizado;
	private Date dataVencimento;
	private String situacaoDescricao;
	private String tipoDocDescricao;
	private	Long faturaID;
	private Date dataPagamento;
	private Long docCobrancaItemID;
	private BigDecimal percentualMulta;
	private BigDecimal percentualJurosMora;
	private BigDecimal valorTotalAtualizado;
	
	private Recebimento recebimento;

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public Date getDataEmissao() {
		Date dataEmissaoTmp = null;
		if(this.dataEmissao != null){
			dataEmissaoTmp = (Date) this.dataEmissao.clone();
		}		
		return dataEmissaoTmp;
	}

	public String getDataEmissaoFormatada() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return	sdf.format(dataEmissao);
	}

	public void setDataEmissao(Date dataEmissao) {
		if(dataEmissao != null){
			this.dataEmissao = (Date) dataEmissao.clone();
		} else {
			this.dataEmissao = null;
		}
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getSituacaoDescricao() {
		return situacaoDescricao;
	}

	public void setSituacaoDescricao(String situacaoDescricao) {
		this.situacaoDescricao = situacaoDescricao;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public String getDataVencimentoFormatada() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return	sdf.format(dataVencimento);
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getTipoDocDescricao() {
		return StringUtil.capitalize(tipoDocDescricao);
	}

	public void setTipoDocDescricao(String tipoDocDescricao) {
		this.tipoDocDescricao = tipoDocDescricao;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}
	public String getDataPagamentoFormatada() {
		if(dataPagamento != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return	sdf.format(dataPagamento);
		}
		return "-";
	}
	
	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Long getDocCobrancaItemID() {
		return docCobrancaItemID;
	}

	public void setDocCobrancaItemID(Long docCobrancaItemID) {
		this.docCobrancaItemID = docCobrancaItemID;
	}

	public Recebimento getRecebimento() {
		return recebimento;
	}

	public void setRecebimento(Recebimento recebimento) {
		this.recebimento = recebimento;
	}

	public BigDecimal getValorAtualizado() {
		return valorAtualizado;
	}

	public void setValorAtualizado(BigDecimal valorAtualizado) {
		this.valorAtualizado = valorAtualizado;
	}

	public BigDecimal getPercentualMulta() {
		return percentualMulta;
	}

	public void setPercentualMulta(BigDecimal percentualMulta) {
		this.percentualMulta = percentualMulta;
	}

	public BigDecimal getPercentualJurosMora() {
		return percentualJurosMora;
	}

	public void setPercentualJurosMora(BigDecimal percentualJurosMora) {
		this.percentualJurosMora = percentualJurosMora;
	}

	public BigDecimal getValorTotalAtualizado() {
		return valorTotalAtualizado;
	}

	public void setValorTotalAtualizado(BigDecimal valorTotalAtualizado) {
		this.valorTotalAtualizado = valorTotalAtualizado;
	}

	public Long getFaturaID() {
		return faturaID;
	}

	public void setFaturaID(Long faturaID) {
		this.faturaID = faturaID;
	}

	/**
	 * Enum responsável pelos dados do documento da cobrança.
	 * 
	 * @param chavePrimaria
	 * 				the chave primaria       
	 * @param dataEmissao
	 * 			the data emissão	
	 * @param valorTotal
	 * 			the valor totatl
	 * @param  dataVencimento
	 * 			data vencimento
	 * @param  situacaoDescricao
	 * 			the situacao descricao
	 * @param  tipoDocDescricao
	 * 			the situacao doc descricao
	 * @param  dataPagamento
	 * 			the data pagamento
	 * @param  docCobrancaItemID
	 * 			the doc cobranca item id
	 * @param  recebimento
	 * 				the recebimento
	 * @param  faturaID
	 * 			the fatura id
	 */
	public enum Field {
		chavePrimaria("chavePrimaria"),
		dataEmissao("dataEmissao"),
		valorTotal("valorTotal"),
		dataVencimento("dataVencimento"),
		situacaoDescricao("situacaoDescricao"),
		tipoDocDescricao("tipoDocDescricao"),
		dataPagamento("dataPagamento"),
		docCobrancaItemID("docCobrancaItemID"),
		recebimento("recebimento"),
		faturaID("faturaID"),
		;

		private String descricao;

		Field(String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		@Override
		public String toString() {
			return descricao;
		}
	}

}
