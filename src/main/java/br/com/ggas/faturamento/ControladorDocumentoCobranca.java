/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados 
 * ao controador de Documento de cobrança
 *
 */
public interface ControladorDocumentoCobranca extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA = "controladorDocumentoCobranca";

	/**
	 * Método responsável por buscar um documento
	 * de cobrança.
	 * 
	 * @param chavePrimaria
	 *            A chavePrimaria do documento
	 * @return Lista de documento de cobrança
	 */
	Collection<DocumentoCobranca> buscarDocumentoCobranca(Long chavePrimaria);

	/**
	 * Método responsável por 'Nosso Número'.
	 * 
	 * @param nossoNumero
	 *            Nosso Número - Identificação do
	 *            título no banco.
	 * @return Lista de documento de cobrança
	 */
	Collection<DocumentoCobranca> buscarDocumentoCobrancaNossoNumero(Long nossoNumero);

	/**
	 * Método responsável por consultar Faturas
	 * pelo documento de cobrança, utilizando uma
	 * fatura pai como chave.
	 * 
	 * @param chaveFatura
	 *            the chave fatura
	 * @param chaveDocumentoCobranca
	 *            the chave documento cobranca
	 * @return the collection
	 */
	Collection<Fatura> consultarFaturasPorDocumentoCobranca(Long chaveFatura, Long chaveDocumentoCobranca);

	/**
	 * Consultar documento cobranca pela fatura.
	 * 
	 * @param chaveFatura
	 *            the chave fatura
	 * @param tipoDocumento
	 *            the tipo documento
	 * @return the collection
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<DocumentoCobranca> consultarDocumentoCobrancaPelaFatura(Long chaveFatura, TipoDocumento tipoDocumento)
					throws NegocioException;

	/**
	 * Método responsável por obter o documento de
	 * cobrança mais recente de uma fatura.
	 * 
	 * @param chaveFatura
	 *            the chave fatura
	 * @return the documento cobranca
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public DocumentoCobranca obterUltimoDocumentoCobrancaFatura(Long chaveFatura, Boolean isConsiderarTipoDocumento) throws NegocioException;

	/**
	 * Consultar documento cobranca item pela fatura.
	 * 
	 * @param chaveFatura
	 *            the chave fatura
	 * @return the collection
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<DocumentoCobrancaItem> consultarDocumentoCobrancaItemPelaFatura(Long chaveFatura) throws NegocioException;
	
	/**
	 * Consultar fatura por documento cobranca item.
	 *
	 * @param chaveDocumentoCobrancaItem the chave documento cobranca item
	 * @return the fatura
	 */
	public Fatura consultarFaturaPorDocumentoCobrancaItem(Long chaveDocumentoCobrancaItem);
	
	/**
	 * Obtem o ultimo documento de cobranca pela chave primaria da fatura
	 * 
	 * @param chaveFatura the chaveFatura
	 * @return Documento Cobranca the documentoCobranca
	 * @throws NegocioException the negocioException
	 */
	public DocumentoCobranca obterUltimoDocumentoCobranca(Long chaveFatura) throws NegocioException;

	/**
	 * Soma os valores dos itens documento de cobrança pela fatura
	 * 
	 * @param chaveFatura {@link Long}
	 * @return {@link BigDecimal}
	 * @throws NegocioException {@link NegocioException}
	 */
	BigDecimal somarValorDocumentoCobrancaItemPelaFatura(Long chaveFatura) throws NegocioException;

	/**
	 * Método responsável por trazer coleção com chave primaria e a data de emissão
	 * relacionada a um Documento de cobrança
	 * 
	 * @param chaveFatura   {@link Long}
	 * @param documentoTipo {@link TipoDocumento}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<DocumentoCobranca> consultarDadosDocumentoCobrancaPelaFatura(Long chaveFatura,
			TipoDocumento documentoTipo) throws NegocioException;

	/**
	 * Método responsável por trazer o ciclo de referência e o valor toral
	 * 
	 * @param chaveFatura {@link Long}
	 * @param chaveDocumentoCobranca {@link Long}
	 * @return Collection {@link Collection}
	 */
	public Collection<Fatura> consultarDadosFaturasPorDocumentoCobranca(Long chaveFatura, Long chaveDocumentoCobranca);

	
}

