/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.faturamento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados 
 * ao controlador de simulação de calculos de fornecimento de gás
 *
 */
public interface ControladorSimularCalculoFornecimentoGas extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_SIMULAR_CALCULO_FORNECIMENTO_GAS = "controladorSimularCalculoFornecimentoGas";

	// TODO ajustar valores dos tipos dos calculos
	int UNICA_VIGENCIA_INTERVALO = 1;

	Long TIPO_CALCULO_FAIXA = null;

	Long TIPO_CALCULO_CASCATA = null;

	String TIPO_TRIBUTO_CONVENCIONAL = "1";

	String TIPO_TRIBUTO_SUBSTITUICAO_MVA = "2";

	String TIPO_TRIBUTO_SUBSTITUICAO_PMPF = "3";

	String DESCRICAO_ICMS = "ICMS";

	String DESCRICAO_MVA = "MVA";

	String DESCRICAO_PMPF = "PMPF";

	/**
	 * Consultar tarifa vigencia validas.
	 * 
	 * @param tarifa
	 *            the tarifa
	 * @param dataVigenciaInicial
	 *            the data vigencia inicial
	 * @param dataVigenciaFinal
	 *            the data vigencia final
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<TarifaVigencia> consultarTarifaVigenciaValidas(Tarifa tarifa, Date dataVigenciaInicial, Date dataVigenciaFinal)
					throws GGASException;

	/**
	 * Calcular valor fornecimento gas.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param tarifa
	 *            the tarifa
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @param valorConsumo
	 *            the valor consumo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void calcularValorFornecimentoGas(PontoConsumo pontoConsumo, Tarifa tarifa, Date dataInicio, Date dataFim, BigDecimal valorConsumo)
					throws GGASException;

}
