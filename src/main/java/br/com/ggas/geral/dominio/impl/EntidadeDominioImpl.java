/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.dominio.impl;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.geral.dominio.EntidadeDominio;

/**
 * Clase abstrata EntidadeDominioImpl.
 * 
 */
public abstract class EntidadeDominioImpl implements EntidadeDominio {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int codigo;

	private String descricao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.dominio.EntidadeDominio
	 * #getCodigo()
	 */
	@Override
	public int getCodigo() {

		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.dominio.EntidadeDominio
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.dominio.EntidadeDominio
	 * #setCodigo(int)
	 */
	@Override
	public void setCodigo(int codigo) {

		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.dominio.EntidadeDominio
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj){ 
			return true;
		}
		if(!(obj instanceof EntidadeDominioImpl)) {
			return false;
		}
		if(this.getCodigo() > 0) {
			EntidadeDominioImpl that = (EntidadeDominioImpl) obj;
			return new EqualsBuilder().append(this.getCodigo(), that.getCodigo()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.getCodigo() > 0) {
			return new HashCodeBuilder(3, 5).append(this.getCodigo()).toHashCode();
		} else {
			return super.hashCode();
		}

	}
}
