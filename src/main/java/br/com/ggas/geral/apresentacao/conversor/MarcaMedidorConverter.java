package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.util.Fachada;
/**
 * Marca Medidor
 * 
 * @author Pedro Silva
 *
 * Classe responsável pela conversão de uma entidade do tipo
 * 
 * 
 * Método que recebe uma String e retorna 
 * um objeto MarcaMedidor
 */
public class MarcaMedidorConverter implements Converter<String, MarcaMedidor>{
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public MarcaMedidor convert(String id) {
		MarcaMedidor marcaMedidor = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			
			try {
				marcaMedidor = Fachada.getInstancia().obterMarcaMedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return marcaMedidor;
	}

}
