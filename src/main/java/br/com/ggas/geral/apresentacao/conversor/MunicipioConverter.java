package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Municipio Converter
 * 
 * @author pedro
 * Classe responsável por converter uma String que representa uma chave primaria
 * em uma entidade Municipio.
 * 
 */
public class MunicipioConverter implements Converter<String, Municipio> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Municipio convert(String id) {
		Municipio municipio = null;
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				municipio = Fachada.getInstancia().buscarMunicipioChave(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return municipio;
	}
}
