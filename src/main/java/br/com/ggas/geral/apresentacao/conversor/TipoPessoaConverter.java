package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável por mapear uma chave primária em uma entidade que representa um tipo de pessoa.
 * 
 * @author esantana
 */
public class TipoPessoaConverter implements Converter<String, TipoPessoa> {

	/**
	 * 
	 * Atributo referente ao Log de dados.
	 * 
	 */
	private static final Logger LOG = Logger.getLogger(TipoPessoaConverter.class);

	/**
	 * 
	 * Método responsável por converter o identificador em um objeto.
	 * 
	 * @param id - {@link String}
	 * @return tipo de pessoa
	 */
	public TipoPessoa convert(String id) {
		
		TipoPessoa tipoPessoa = null;

		try {
			tipoPessoa = Fachada.getInstancia().criarTipoPessoa();
			tipoPessoa.setCodigo(Integer.parseInt(id));
		} catch (Exception e) {
			LOG.debug(e.getStackTrace(), e);
		}

		return tipoPessoa;
	}
}
