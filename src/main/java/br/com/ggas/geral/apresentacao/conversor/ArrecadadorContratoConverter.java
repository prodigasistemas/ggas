package br.com.ggas.geral.apresentacao.conversor;

import org.springframework.core.convert.converter.Converter;
import org.apache.log4j.Logger;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * Converter de ArrecadadorContrato
 * 
 * @category converter
 */
public class ArrecadadorContratoConverter implements Converter<String, ArrecadadorContrato> {

	
	/**
	 * Arrecadador Contrato
	 * 
	 * @param chave
	 * 			the chave
	 * @return ArrecadadorContrato
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public ArrecadadorContrato convert(String chave) {

		ArrecadadorContrato arrecadadorContrato = null;
		if (chave != null && !"".equals(chave)) {
			try {
				arrecadadorContrato =
						ServiceLocator.getInstancia().getControladorArrecadadorConvenio().obterArrecadadorContrato(Long.parseLong(chave));
			} catch (NegocioException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return arrecadadorContrato;
	}

}
