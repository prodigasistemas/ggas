package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Profissao Converter
 * 
 * @author pedro
 * Classe responsável pela conversão
 * de uma String que representa uma chave primaria
 * em um objeto Profissão
 */
public class ProfissaoConverter implements Converter<String, Profissao>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Profissao convert(String id) {

		Profissao profissao = null;
		
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				profissao = Fachada.getInstancia().obterProfissao(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return profissao;
	}

}
