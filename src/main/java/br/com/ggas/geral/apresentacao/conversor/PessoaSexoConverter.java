package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Pessoa Sexo Converter
 * 
 * @author pedro
 * Classe responsável pela conversão
 * de uma String que representa uma chave primaria
 * em um objeto PessoaSexo
 */
public class PessoaSexoConverter implements Converter<String, PessoaSexo> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public PessoaSexo convert(String id) {
		PessoaSexo pessoaSexo = null;
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				pessoaSexo = Fachada.getInstancia().obterPessoaSexo(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return pessoaSexo;
	}

}
