
package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável por converter os dados do formulário
 *
 */
public class AgenteConverter implements Converter<String, Agente> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Agente convert(String source) {

		Long chavePrimaria = Long.parseLong(source);

		Agente agente = null;

		try {
			agente = Fachada.getInstancia().obterAgentePorChavePrimaria(chavePrimaria);
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return agente;
	}

}
