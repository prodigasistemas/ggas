package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.DiametroMedidor;
import br.com.ggas.util.Fachada;
/**
 * Diametro Medidor Converter
 * 
 * @author Pedro Silva
 * 
 *	classe responsável pela conversão de uma entidade do tipo
 *	
 *	DiametroMedidor
 *	Método que recebe uma String e retorna 
 *	um objeto DiametroMedidor
 */
public class DiametroMedidorConverter  implements Converter<String, DiametroMedidor>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public DiametroMedidor convert(String id) {
		DiametroMedidor diametroMedidor = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			
			try {
				diametroMedidor = Fachada.getInstancia().obterDiametroMedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return diametroMedidor;
	}

}
