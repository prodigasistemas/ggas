package br.com.ggas.geral.apresentacao.conversor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;

/**
 * Conta Contabil Converter
 * 
 * @author pedro
 * Converter que ultiliza o número da ContaContábil
 */
public class ContaContabilConverter implements Converter<String, ContaContabil> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public ContaContabil convert(String id) {
		ContaContabil contaContabil = null;
		if (id != null && !"".equals(id)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("numeroConta", Util.removerCaracteresEspeciais(id));
			contaContabil = Fachada.getInstancia().obterContaContabil(filtro);
		}
		return contaContabil;
	}

}
