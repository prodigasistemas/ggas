package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.impl.EsferaPoderImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável por mapear uma chave primária em uma entidade que representa uma esfera de poder.
 * 
 * @author esantana
 */
public class EsferaPoderConverter implements Converter<String, EsferaPoder> {

	/**
	 * 
	 * Atributo referente ao Log de dados.
	 * 
	 */
	private static final Logger LOG = Logger.getLogger(EsferaPoderConverter.class);

	/**
	 * 
	 * Método responsável por converter o identificador em um objeto.
	 * 
	 * @param id - {@link String}
	 * @return esfera de poder
	 */
	public EsferaPoder convert(String id) {
		ControladorCliente controladorCliente =
				(ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		EsferaPoder esferaPoder = null;

		try {

			esferaPoder = (EsferaPoder) controladorCliente.obter(Long.parseLong(id), EsferaPoderImpl.class);
		} catch (Exception e) {
			LOG.debug(e.getStackTrace(), e);
		}

		return esferaPoder;
	}
}
