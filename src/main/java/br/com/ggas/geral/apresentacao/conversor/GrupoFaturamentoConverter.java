/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Grupo Faturamento Converter
 * 
 * @author pedro
 * 
 * Classe responsável por converter uma String que representa uma
 * chave primaria, em uma entidade do tipo 
 * GrupoFaturamento
 *         
 *         
 */
public class GrupoFaturamentoConverter implements Converter<String, GrupoFaturamento> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 * 
	 */
	@Override
	public GrupoFaturamento convert(String id) {

		GrupoFaturamento grupoFaturamento = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				grupoFaturamento = Fachada.getInstancia().obterGrupoFaturamento(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return grupoFaturamento;
	}
}
