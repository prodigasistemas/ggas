package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.util.Fachada;
/**
 * Classe responsável pela conversão de uma entidade do tipo
 * 
 * @author Pedro Silva
 * 
 * 
 * SituacaoMedidor
 * Método que recebe uma String e retorna 
 * um objeto SituacaoMedidor
 */
public class SituacaoMedidorConverter implements Converter<String, SituacaoMedidor>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public SituacaoMedidor convert(String id) {
		SituacaoMedidor situacaoMedidor = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			
			try {
				situacaoMedidor = Fachada.getInstancia().obterSituacaoMedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return situacaoMedidor;
	}

}
