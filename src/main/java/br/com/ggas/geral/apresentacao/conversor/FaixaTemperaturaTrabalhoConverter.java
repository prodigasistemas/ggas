package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.util.Fachada;
/**
 * Faixa Temperatura Trabalho
 * 
 * @author Pedro Silva
 *
 * Classe responsável pela conversão de uma entidade do tipo
 * 
 * 
 *  Método que recebe uma String e retorna 
 *	  um objeto FaixaTemperaturaTrabalho
 */
public class FaixaTemperaturaTrabalhoConverter implements Converter<String, FaixaTemperaturaTrabalho>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public FaixaTemperaturaTrabalho convert(String id) {
		FaixaTemperaturaTrabalho faixaTemperaturaTrabalho = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			
			try {
				faixaTemperaturaTrabalho = Fachada.getInstancia().obterFaixaTemperaturaTrabalho(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return faixaTemperaturaTrabalho;
	}

}
