package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.localidade.LocalidadePorte;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Localidade Porte Converter 
 * 
 * @author pedro
 * Classe responsável pela conversão de uma String que representa uma chave primária
 * em uma entidade do tipo LocalidadePorte
 */
public class LocalidadePorteConverter implements Converter<String, LocalidadePorte>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public LocalidadePorte convert(String id) {

		LocalidadePorte localidadePorte = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				localidadePorte = Fachada.getInstancia().buscarLocalidadePorteChave(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return localidadePorte;
	}
}
