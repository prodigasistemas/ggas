package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Grupo Economico Converter
 * 
 * @author Pedro Silva
 * 
 * Classe responsável pela conversão
 * de uma String que representa a chave primaria
 * em um objeto GrupoEconomico
 *
 */
public class GrupoEconomicoConverter implements Converter<String, GrupoEconomico> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public GrupoEconomico convert(String id) {

		GrupoEconomico grupoEconomico = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				grupoEconomico = Fachada.getInstancia().obterGrupoEconomico(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return grupoEconomico;
	}

}
