/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.apresentacao.deserializadorjson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Deserializador JSON de LocalDate.
 * Use em conjunto com a anotação {@link com.fasterxml.jackson.databind.annotation.JsonDeserialize}
 *
 * @author jose.victor@logiquesistemas.com.br
 *
 * SupressWarning: common-java:InsufficientCommentDensity: A classe está suficientemente documentada
 */
@SuppressWarnings("common-java:InsufficientCommentDensity")
public class LocalDateDeserializer extends StdScalarDeserializer<LocalDate> {

	private static final Logger LOG = Logger.getLogger(LocalDateDeserializer.class);

	/**
	 * Sobrescreve construtor padrão informando a classe pai o tipo que será deserializado.
	 */
	protected LocalDateDeserializer() {
		super(LocalDate.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
		try {
			final String valor = jp.getValueAsString();
			LocalDate data = null;
			if (StringUtils.isNotBlank(valor)) {
				data = LocalDate.parse(valor, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			}

			return data;
		} catch (Exception e) {
			LOG.error("Falha ao converter objeto em LocalDate", e);
			throw e;
		}
	}
}
