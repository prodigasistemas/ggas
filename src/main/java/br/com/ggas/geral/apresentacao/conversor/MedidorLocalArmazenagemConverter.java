package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Medidor Local Armazenagem
 * 
 * @author Pedro Silva
 *
 * Classe responsável pela conversão de uma entidade do tipo
 *
 * 
 * Método que recebe uma String e retorna 
 * um objeto MedidorLocalArmazenagem
 */
public class MedidorLocalArmazenagemConverter implements Converter<String, MedidorLocalArmazenagem> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public MedidorLocalArmazenagem convert(String id) {
		MedidorLocalArmazenagem medidorLocalArmazenagem = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				medidorLocalArmazenagem = Fachada.getInstancia().obterMedidorLocalArmazenagem(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return medidorLocalArmazenagem;
	}

}
