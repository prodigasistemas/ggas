package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.util.Fachada;

/**
 * Periodicidade Converter
 * 
 * @author Pedro
 * 
 *  Classe responsável por converter uma String
 *  que representa a chave primária 
 *  e converter em uma Preíodicidade 
 *
 */
public class PeriodicidadeConverter implements Converter<String, Periodicidade>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Periodicidade convert(String id) {

		Periodicidade periodicidade = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				periodicidade = Fachada.getInstancia().obterPeriodicidade(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return periodicidade;
	}
}
