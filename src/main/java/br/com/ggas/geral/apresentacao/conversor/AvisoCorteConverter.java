package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Aviso Corte Converter
 * 
 * @author Pedro Silva
 */
public class AvisoCorteConverter implements Converter<String, AvisoCorte> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public AvisoCorte convert(String id) {

		AvisoCorte avisoCorte = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				avisoCorte = Fachada.getInstancia().obterAvisoCorte(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return avisoCorte;
	}
}
