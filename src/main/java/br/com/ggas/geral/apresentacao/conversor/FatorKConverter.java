package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.FatorK;
import br.com.ggas.util.Fachada;
/**
 * FatorK
 * 
 * @author Pedro Silva
 * 
 * Classe responsável pela conversão de uma entidade do tipo
 * Método que recebe uma String e retorna
 * um objeto FatorK
 */
public class FatorKConverter implements Converter<String, FatorK> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public FatorK convert(String id) {

		FatorK fatorK = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				fatorK = Fachada.getInstancia().obterFatorK(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return fatorK;
	}

}
