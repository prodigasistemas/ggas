package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

public class VeiculoConverter implements Converter<String, Veiculo> {

	@Override
	public Veiculo convert(String chave) {
		Long chavePrimaria = Long.parseLong(chave);

		Veiculo veiculo = null;

		try {
			veiculo = (Veiculo) ServiceLocator.getInstancia().getControladorVeiculo().obter(chavePrimaria);
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return veiculo;
	}

}
