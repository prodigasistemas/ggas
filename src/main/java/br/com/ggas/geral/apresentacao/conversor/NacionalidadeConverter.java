package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Nacionalidade Converter
 * 
 * @author pedro
 * Classe responsável pela conversão
 * de uma String que representa a chave primaria
 * 
 */
public class NacionalidadeConverter implements Converter<String, Nacionalidade> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Nacionalidade convert(String id) {
		Nacionalidade nacionalidade = null;
		
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				nacionalidade = Fachada.getInstancia().obterNacionalidade(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return nacionalidade;
	}

}
