package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.TipoMedidor;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável pela conversão de um TipoMedidor
 * 
 * @author Pedro Silva
 * 
 * 
 * Método resposável por receber uma String e retornar 
 * um objeto TipoMedidor
 * 
 * 
 */
public class TipoMedidorConverter implements Converter<String, TipoMedidor> {

	/**
	 * Obtém um tipo de medidor por chave primária.
	 * @param id
	 */
	@Override
	public TipoMedidor convert(String id) {

		TipoMedidor tipoMedidor = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				tipoMedidor = Fachada.getInstancia().obterTipoMedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return tipoMedidor;
	}
}
