package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Classe responsável pela conversão
 * de uma String que representa um chave primaria
 * em um objeto UnidadeFederacao
 * 
 * @author pedro
 */
public class UnidadeFederacaoConverter implements Converter<String, UnidadeFederacao> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public UnidadeFederacao convert(String id) {

		UnidadeFederacao unidadeFederacao = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				unidadeFederacao = Fachada.getInstancia().obterUnidadeFederacao(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return unidadeFederacao;
	}

}
