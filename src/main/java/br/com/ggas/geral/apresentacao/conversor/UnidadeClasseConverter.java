package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável por converter os dados do formulário
 *
 */
public class UnidadeClasseConverter implements Converter<String, ClasseUnidade> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */

	@Override
	public ClasseUnidade convert(String source) {

		ControladorTabelaAuxiliar controladorTabelaAuxiliar = ServiceLocator.getInstancia().getControladorTabelaAuxiliar();

		Long chavePrimaria = Long.parseLong(source);
		ClasseUnidade unidadeClasse = null;

		try {
			unidadeClasse = (ClasseUnidade) controladorTabelaAuxiliar.obter(chavePrimaria, ClasseUnidadeImpl.class);
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return unidadeClasse;
	}

}
