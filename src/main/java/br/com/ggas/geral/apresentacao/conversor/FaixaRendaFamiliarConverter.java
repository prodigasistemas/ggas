package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Faixa Renda Familiar Converter
 * 
 * @author pedro
 * 
 * Classe responsável pela conversão
 * de uma string que representa a chave primária
 * para um objeto FaixaRendaFamiliar
 */
public class FaixaRendaFamiliarConverter implements Converter<String, FaixaRendaFamiliar> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public FaixaRendaFamiliar convert(String id) {
		FaixaRendaFamiliar faixaRendaFamiliar = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				faixaRendaFamiliar = Fachada.getInstancia().obterFaixaRendaFamiliar(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return faixaRendaFamiliar;
	}

}
