package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Rede Interna Imovel Converter
 * 
 * @author pedro
 *
 */
public class RedeInternaImovelConverter implements Converter<String, RedeInternaImovel>{

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public RedeInternaImovel convert(String id) {

		RedeInternaImovel redeInternaImovel = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				redeInternaImovel = (RedeInternaImovel) Fachada.getInstancia().obterEntidadePorClasse(Long.parseLong(id), RedeInternaImovel.class);
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return redeInternaImovel;
	}

}
