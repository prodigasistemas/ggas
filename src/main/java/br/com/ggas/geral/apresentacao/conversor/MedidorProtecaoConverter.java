package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.util.Fachada;

public class MedidorProtecaoConverter implements Converter<String, MedidorProtecao> {

	@Override
	public MedidorProtecao convert(String chave) {
		MedidorProtecao medidorProtecao = null;
		
		// TODO Auto-generated method stub
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {

			try {
				medidorProtecao = Fachada.getInstancia().obterMedidorProtecao(Long.parseLong(chave));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return medidorProtecao;
	}

}
