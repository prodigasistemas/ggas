package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Empresa Converter
 * 
 * @author Pedro Silva
 * 
 */
public class EmpresaConverter implements Converter<String, Empresa> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Empresa convert(String id) {

		Empresa empresa = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				empresa = Fachada.getInstancia().obterEmpresa(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return empresa;
	}

}
