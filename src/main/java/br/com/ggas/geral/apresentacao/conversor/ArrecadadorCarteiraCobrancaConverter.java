package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Converter de ArrecadadorCarteiraCobranca
 * @author Pedro Silva
 */
public class ArrecadadorCarteiraCobrancaConverter implements Converter<String, ArrecadadorCarteiraCobranca>{

	@Override
	public ArrecadadorCarteiraCobranca convert(String id) {
		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca = null;
		
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				arrecadadorCarteiraCobranca = Fachada.getInstancia().obterArrecadadorCarteiraCobranca(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		
		return arrecadadorCarteiraCobranca;
	}

}
