package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.util.Fachada;

/**
 * Converter da classe RamoAtividade.
 * @author esantana
 *
 */
public class RamoAtividadeConverter implements Converter<String, RamoAtividade>  {

	/**
	 * Atributo referente ao Log de dados.
	 */
	private static final Logger LOG = Logger.getLogger(RamoAtividadeConverter.class);
	
	/**
	 * Método responsável por converter o identificador do RamoAtividade em um objeto.
	 * @param idRamoAtividade - {@link String}
	 */
	@Override
	public RamoAtividade convert(String idRamoAtividade) {
		
		RamoAtividade ramoAtividade = null;

		try {
			Long chavePrimaria = Long.parseLong(idRamoAtividade);
			ramoAtividade = Fachada.getInstancia().buscarRamoAtividadeChave(chavePrimaria);
		} catch (Exception e) {
			LOG.error(e);
		} 
		
		return ramoAtividade;
	}

}
