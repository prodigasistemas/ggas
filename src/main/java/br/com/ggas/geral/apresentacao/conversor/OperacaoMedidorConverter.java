package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.util.Fachada;

public class OperacaoMedidorConverter implements Converter<String, OperacaoMedidor> {

	@Override
	public OperacaoMedidor convert(String chave) {
		
		OperacaoMedidor operacaoMedidor = null;
		// TODO Auto-generated method stub
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {

			try {
				operacaoMedidor = Fachada.getInstancia().obterOperacaoMedidor(Long.parseLong(chave));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return operacaoMedidor;
	}

}
