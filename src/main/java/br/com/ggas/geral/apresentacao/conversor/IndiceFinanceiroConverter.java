package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Indice Financeiro Converter
 * 
 */
public class IndiceFinanceiroConverter implements Converter<String, IndiceFinanceiro>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public IndiceFinanceiro convert(String id) {
		
		IndiceFinanceiro indiceFinanceiro = null;
		
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				indiceFinanceiro = Fachada.getInstancia().obterIndiceFinanceiro(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		
		return indiceFinanceiro;
	}
	
}
