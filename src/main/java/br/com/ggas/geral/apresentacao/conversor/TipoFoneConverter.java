package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
/** 
 * Classe responsável pela conversão
 * de uma String que representa uma chave primaria
 * em um objeto TipoFone
 * 
 * @author pedro
 */
public class TipoFoneConverter implements Converter<String, TipoFone> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public TipoFone convert(String chave) {
		Long chavePrimaria = Long.parseLong(chave);
		TipoFone tipoFone = null;
		
		try {
			tipoFone = (TipoFone) ServiceLocator.getInstancia().getControladorTabelaAuxiliar().obter(chavePrimaria, TipoFoneImpl.class);
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return tipoFone;
	}

}
