package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * Converter de ArrecadadorContratoConvenio
 *
 * @author Pedro Silva
 * Classe responsável pela conversão de uma instância de objeto através da chave primaria
 */
public class ArrecadadorContratoConvenioConverter implements Converter<String, ArrecadadorContratoConvenio>{

	@Override
	public ArrecadadorContratoConvenio convert(String chave) {

		ArrecadadorContratoConvenio arrecadadorContratoConvenio = null;
		if (chave != null && !"".equals(chave)) {
			try {
				arrecadadorContratoConvenio =
						ServiceLocator.getInstancia().getControladorArrecadadorConvenio().obterArrecadadorConvenio(Long.parseLong(chave));
			} catch (NegocioException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return arrecadadorContratoConvenio;
	}

}
