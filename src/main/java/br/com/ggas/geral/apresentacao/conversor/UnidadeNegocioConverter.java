package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável por conveter uma String
 * que representa uma chave primária em uma entidade do tipo UnidadeNegocio
 * 
 * @author pedro
 */
public class UnidadeNegocioConverter implements Converter<String, UnidadeNegocio>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public UnidadeNegocio convert(String id) {

		UnidadeNegocio unidadeNegocio = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				unidadeNegocio = Fachada.getInstancia().buscarUnidadeNegocioChave(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return unidadeNegocio;
	}
}
