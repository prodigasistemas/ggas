package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.MotivoOperacaoMedidorImpl;
import br.com.ggas.util.ServiceLocator;

public class MotivoOperacaoMedidorConverter implements Converter<String, MotivoOperacaoMedidor> {

	@Override
	public MotivoOperacaoMedidor convert(String chave) {
		Long chavePrimaria = Long.parseLong(chave);

		MotivoOperacaoMedidor motivoOperacaoMedidor = null;

		try {
			motivoOperacaoMedidor = (MotivoOperacaoMedidor) ServiceLocator.getInstancia().getControladorMedidor()
					.obter(chavePrimaria, MotivoOperacaoMedidorImpl.class);
		} catch (NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return motivoOperacaoMedidor;
	}
}

