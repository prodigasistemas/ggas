package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.impl.CepImpl;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * Cep Converter
 * 
 * @author pedro
 * 
 *         Classe responsável pela conversão de uma String que é numeração do
 *         Cep para obtenção de um objeto Cep.
 * 
 */
public class CepConverter implements Converter<String, Cep> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public Cep convert(String numeracao) {
		Cep cep = new CepImpl();

		if (numeracao != null && !"".equals(numeracao)) {
			try {
				if(numeracao.contains("-")) {
					cep = ServiceLocator.getInstancia().getControladorEndereco().obterCep(numeracao);					
				} else {
					cep = ServiceLocator.getInstancia().getControladorEndereco().obterCepPorChave(Long.parseLong(numeracao));
				}
			} catch (NegocioException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return cep;
	}

}
