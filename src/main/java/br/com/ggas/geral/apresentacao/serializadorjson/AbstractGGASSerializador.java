/*
 *
 *  Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 *  Este programa é um software livre; você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 *  publicada pela Free Software Foundation; versão 2 da Licença.
 *
 *  O GGAS é distribuído na expectativa de ser útil,
 *  mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 *  COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 *  Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa; se não, escreva para Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 *  GGAS is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  GGAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *
 */

package br.com.ggas.geral.apresentacao.serializadorjson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Serializador padrão da aplicação do GGAS
 *
 * @param <T> Tipo do objeto a ser serializado
 * @author jose.victor@logiquesistemas.com.br
 */
public abstract class AbstractGGASSerializador<T> extends StdSerializer<T> {

	/**
	 * Construtor default do serializador
	 */
	public AbstractGGASSerializador() {
		this(null);
	}

	/**
	 * Inicializador padrão do serializador do ggas
	 * @param t tipo da classe que será serializada
	 */
	public AbstractGGASSerializador(Class<T> t) {
		super(t);
	}

	@Override
	public final void serialize(T value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
		final GenericSafeJsonGenerator safeJsonGenerator = new GenericSafeJsonGenerator(jgen);
		this.serializar(value, safeJsonGenerator, provider);
	}

	/**
	 * Lógica de serialização de um objeto
	 * @param valor valor informado para a seriaçização
	 * @param generator gerador de json
	 * @param provider provider
	 * @throws IOException exceção lançada caso haja erro de leitura / escrita
	 */
	public abstract void serializar(T valor, GenericSafeJsonGenerator generator, SerializerProvider provider) throws IOException;

}
