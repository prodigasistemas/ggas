package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Converter de ContaBancaria
 *
 * @category converter
 */
public class ContaBancariaConverter implements Converter<String, ContaBancaria> {

	@Override
	public ContaBancaria convert(String id) {
		ContaBancaria contaBancaria = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				contaBancaria = Fachada.getInstancia().obterContaBancaria(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return contaBancaria;
	}

}
