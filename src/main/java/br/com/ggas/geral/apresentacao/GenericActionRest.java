/*
 *
 *  Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 *  Este programa é um software livre; você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 *  publicada pela Free Software Foundation; versão 2 da Licença.
 *
 *  O GGAS é distribuído na expectativa de ser útil,
 *  mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 *  COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 *  Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa; se não, escreva para Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 *  This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 *  GGAS is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; version 2 of the License.
 *
 *  GGAS is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *
 */

package br.com.ggas.geral.apresentacao;

import br.com.ggas.geral.exception.GGASException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementação genérica de action rests
 *
 * Define uma herança padrão de controladores rest no GGAS
 * Contém métodos comuns para seriação de json de forma genérica
 *
 * @author jose.victor@logiquesistemas.com.br
 */
public class GenericActionRest {

	private static final Logger LOG = Logger.getLogger(GenericActionRest.class);

	/**
	 * Serializa um objeto em uma string json
	 * Utiliza o {@link GenericActionRest#getMapaSerializadores()}
	 * @param objeto objeto a ser serializado
	 * @param <T> tipo genérico do objeto a ser serializado
	 * @return retorna a string json do objeto
	 * @throws GGASException exceção lançada caso ocorra falha durante a serialização do objeto
	 */
	protected <T> String serializarJson(T objeto) throws GGASException {
		SimpleModule modulo = new SimpleModule();
		final Map<Class, JsonSerializer> mapaSerializadores = getMapaSerializadores();

		if (!mapaSerializadores.isEmpty()) {
			for (Map.Entry<Class, JsonSerializer> entry : mapaSerializadores.entrySet()) {
				modulo.addSerializer(entry.getKey(), entry.getValue());
			}
		}

		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(modulo);
		mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		try {
			return mapper.writeValueAsString(objeto);
		} catch (JsonProcessingException e) {
			LOG.error("Falha ao serializar um objeto em string", e);
			throw new GGASException(e);
		}

	}

	/**
	 * Obtém a lista de serializadores customizáveis. A implementação padrão é um mapa vazio
	 * @return retorna um mapa indicando as implementações dos serializadores customizados.
	 */
	protected Map<Class, JsonSerializer> getMapaSerializadores() {
		return new HashMap<>();
	}

}
