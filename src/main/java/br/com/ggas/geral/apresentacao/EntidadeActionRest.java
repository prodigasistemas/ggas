/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.apresentacao;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Implementação genérica default do action Rest de {@link EntidadeNegocio}
 *
 * Contém os metodos padrão para salvar, obter atualizar e remover uma entidade, utilizando
 * os métodos POST, GET, PUT e DELETE respectivamente.
 *
 * Esta classe utiliza o serializador json default da action {@link GenericActionRest}.
 * É aconselhável sobrescrever o método {@link GenericActionRest#getMapaSerializadores()} e implementar
 * os serializadores json padrão para a entidade desejada
 *
 * Para exemplos de implementações veja {@link br.com.ggas.web.medicao.rota.rest.RotaActionRest}
 * @param <T> tipo da entidade que herda de {@link EntidadeNegocio}
 *
 * @author jose.victor@logiquesistemas.com.br
 * SuppresWarnings: squid:S00118, common-java:InsufficientLineCoverage suficientemente doucumentado
 */
@SuppressWarnings({"squid:S00118", "common-java:InsufficientLineCoverage"})
public abstract class EntidadeActionRest<T extends EntidadeNegocio> extends GenericActionRest {

	protected final ControladorNegocio controlador;

	/**
	 * Construtor padrão da EntidadeActionRest.
	 * @param controlador controlador de negócio que realiza acesso às operações da camada de DAO
	 */
	public EntidadeActionRest(ControladorNegocio controlador) {
		this.controlador = controlador;
	}

	/**
	 * Obtém a entidade desejada pela sua chave primaria
	 * @param chavePrimaria id da entidade
	 * @return retorna uma resposta json com a entidade serializada
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/", produces = "application/json; charset=utf-8",
			method = { RequestMethod.GET})
	@ResponseBody
	public ResponseEntity<String> get(@RequestParam Long chavePrimaria) throws GGASException {

		final EntidadeNegocio entidade = this.controlador.obter(chavePrimaria);
		final String json = serializarJson(entidade);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Salva a entidade no banco
	 * @param entidade entidade a ser salva
	 * @return retorna a resposta ok, com a chave primaria da entidade salva no banco
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/", produces = "application/json; charset=utf-8",
			method = { RequestMethod.POST})
	@ResponseBody
	public ResponseEntity<String> salvar(T entidade) throws GGASException {

		final long chavePrimaria = this.controlador.inserir(entidade);
		final String json = serializarJson(chavePrimaria);
		return new ResponseEntity<>(json, HttpStatus.OK);
	}

	/**
	 * Atualiza uma entidade no banco
	 * @param entidade entidade a ser atualizada
	 * @return retorna o status de ok
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/", produces = "application/json; charset=utf-8",
			method = { RequestMethod.PUT})
	@ResponseBody
	public ResponseEntity<String> atualizar(T entidade) throws GGASException {
		this.controlador.atualizar(entidade);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Remove uma entidade no banco
	 * @param chavePrimaria chave identificadora da entidade
	 * @return retorna status de ok
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@RequestMapping(value = "/", produces = "application/json; charset=utf-8",
			method = { RequestMethod.DELETE})
	@ResponseBody
	public ResponseEntity<String> remover(@RequestParam Long chavePrimaria) throws GGASException {
		this.controlador.remover(this.controlador.obter(chavePrimaria));
		return new ResponseEntity<>(HttpStatus.OK);
	}




}
