package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Orgao Expedidor Converter
 * 
 * @author pedro
 * Classe responsável pela conversão
 * de uma String que representa a chave primaria
 * em um objeto OrgaoExpedidor
 */
public class OrgaoExpedidorConverter implements Converter<String, OrgaoExpedidor>{
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public OrgaoExpedidor convert(String id) {

		OrgaoExpedidor orgaoExpedidor = null;
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				orgaoExpedidor = Fachada.getInstancia().obterOrgaoExpedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return orgaoExpedidor;
	}

}
