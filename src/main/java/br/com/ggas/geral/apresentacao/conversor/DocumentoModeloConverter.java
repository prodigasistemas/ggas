package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.arrecadacao.documento.DocumentoModelo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * DocumentoModeloConverter
 * 
 * Classe rsponsável por converter uma string que representa uma chave primaria
 * em uma entidade do tipo DocumentoModelo
 *
 */
public class DocumentoModeloConverter implements Converter<String, DocumentoModelo> {
	
	/**
	 * Método responsável por obter um {@link DocumentoModelo} pelo seu identificador único.
	 * @param id - {@link String}
	 * @return modelo - {@link DocumentoModelo}
	 */
	public DocumentoModelo convert(String id) {

		DocumentoModelo modelo = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				modelo = Fachada.getInstancia().obterDocumentoModelo(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return modelo;
	}
}
