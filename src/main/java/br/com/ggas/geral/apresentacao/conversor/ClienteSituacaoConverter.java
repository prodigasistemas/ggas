package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Cliente Situacao Converter
 * 
 * @author Pedro Silva
 * 
 * Classe responsável pela conversão
 * de uma String que representa a chave primaria
 * em um objeto ClienteSituacao
 *
 */
public class ClienteSituacaoConverter implements Converter<String, ClienteSituacao> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public ClienteSituacao convert(String id) {

		ClienteSituacao clienteSituacao = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				clienteSituacao = Fachada.getInstancia().obterClienteSituacao(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return clienteSituacao;
	}
}
