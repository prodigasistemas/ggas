package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Gerencia Regional Converter
 * 
 * @author pedro
 * Classe responsável por conveter uma String
 * que representa uma chave primária em uma entidade do tipo GerenciaRegional
 * 
 */
public class GerenciaRegionalConverter implements Converter<String, GerenciaRegional>{
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public GerenciaRegional convert(String id) {
		GerenciaRegional gerenciaRegional = null;
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				gerenciaRegional = Fachada.getInstancia().buscarGerenciaRegionalPorChave(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return gerenciaRegional;
	}
}
