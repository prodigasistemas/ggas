/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 23/09/2013 10:02:57
 @author vtavares
 */

package br.com.ggas.geral.apresentacao.conversor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável por converter uma string que representa uma chave primaria em uma entidade do tipo PontoConsumo
 *
 */
public class PontoConsumoConverter implements Converter<String, PontoConsumo> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public PontoConsumo convert(String chave) {

		Long chavePrimaria = null;
		if(!StringUtils.isEmpty(chave)) {
			chavePrimaria = Long.parseLong(chave);
		} else {
			chavePrimaria = Long.valueOf(-1);
		}

		PontoConsumo pontoConsumo = null;

		try {
			pontoConsumo = (PontoConsumo) ServiceLocator.getInstancia().getControladorPontoConsumo().obter(chavePrimaria);
		} catch(NegocioException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return pontoConsumo;
	}
}
