package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import br.com.ggas.cadastro.localidade.LocalidadeClasse;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Localidade Classe Converter
 * 
 * @author pedro
 * Classe responsável por converter uma String que representa uma chave primária em uma entidade
 * do tipo LocalidadeClasse.
 */
public class LocalidadeClasseConverter implements Converter<String, LocalidadeClasse>{
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public LocalidadeClasse convert(String id) {

		LocalidadeClasse localidadeClasse = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				localidadeClasse = Fachada.getInstancia().buscarLocalidadeClasseChave(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return localidadeClasse;
	}
}
