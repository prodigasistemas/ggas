package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.util.Fachada;

public class MedidorLocalInstalacaoConverter implements Converter<String, MedidorLocalInstalacao>{

	@Override
	public MedidorLocalInstalacao convert(String chave) {
		MedidorLocalInstalacao medidorLocalInstalacao = null;
		
		
		// TODO Auto-generated method stub
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {

			try {
				medidorLocalInstalacao = Fachada.getInstancia().obterMedidorLocalInstalacao(Long.parseLong(chave));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return medidorLocalInstalacao;
	}

}
