
package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Banco Converter
 * 
 * @author Pedro Silva
 */
public class BancoConverter implements Converter<String, Banco> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Banco convert(String id) {

		Banco banco = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				banco = Fachada.getInstancia().obterBanco(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return banco;
	}

}
