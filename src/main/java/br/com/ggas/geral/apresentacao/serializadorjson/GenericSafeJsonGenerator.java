
/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.apresentacao.serializadorjson;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.Version;

/**
 * Gerador de JSON genérico que não serializa objetos nulos
 * @author jose.victor@logiquesistemas.com.br
 */
public class GenericSafeJsonGenerator extends JsonGenerator {

	private JsonGenerator generator;

	/**
	 * Construtor padrão do gerador de json safe
	 * @param generator implementação de um gerador de json
	 */
	public GenericSafeJsonGenerator(JsonGenerator generator) {
		this.generator = generator;
	}

	@Override
	public Version version() {
		return generator.version();
	}

	@Override
	public JsonGenerator enable(Feature f) {
		return generator.enable(f);
	}

	@Override
	public JsonGenerator disable(Feature f) {
		return generator.disable(f);
	}

	@Override
	public boolean isEnabled(Feature f) {
		return generator.isEnabled(f);
	}

	@Override
	public JsonGenerator setCodec(ObjectCodec oc) {
		return generator.setCodec(oc);
	}

	@Override
	public ObjectCodec getCodec() {
		return generator.getCodec();
	}

	@Override
	public JsonGenerator useDefaultPrettyPrinter() {
		return generator.useDefaultPrettyPrinter();
	}

	@Override
	public void writeStartArray() throws IOException {
		this.generator.writeStartArray();
	}

	@Override
	public void writeEndArray() throws IOException {
		this.generator.writeEndArray();
	}

	@Override
	public void writeStartObject() throws IOException {
		this.generator.writeStartObject();
	}

	@Override
	public void writeEndObject() throws IOException {
		this.generator.writeEndObject();
	}

	@Override
	public void writeFieldName(String name) throws IOException {
		this.generator.writeFieldName(name);
	}

	@Override
	public void writeFieldName(SerializableString name) throws IOException {
		this.generator.writeFieldName(name);
	}

	@Override
	public void writeString(String text) throws IOException {
		if (text != null) {
			this.generator.writeString(text);
		}

	}

	@Override
	public void writeString(char[] text, int offset, int len) throws IOException {
		if (text != null){
			this.generator.writeString(text, offset, len);
		}

	}

	@Override
	public void writeString(SerializableString text) throws IOException {
		if (text != null) {
			this.generator.writeString(text);
		}

	}

	@Override
	public void writeRawUTF8String(byte[] text, int offset, int length) throws IOException {
		if (text != null){
			this.generator.writeRawUTF8String(text, offset, length);
		}

	}

	@Override
	public void writeUTF8String(byte[] text, int offset, int length) throws IOException {
		if (text != null){
			this.generator.writeUTF8String(text, offset, length);
		}

	}

	@Override
	public void writeRaw(String text) throws IOException {
		if (text != null) {
			this.generator.writeRaw(text);
		}

	}

	@Override
	public void writeRaw(String text, int offset, int len) throws IOException {
		if (text != null) {
			this.generator.writeRaw(text, offset, len);
		}

	}

	@Override
	public void writeRaw(char[] text, int offset, int len) throws IOException {
		if (text != null){
			this.generator.writeRaw(text, offset, len);
		}

	}

	@Override
	public void writeRaw(char c) throws IOException {
		this.generator.writeRaw(c);
	}

	@Override
	public void writeRawValue(String text) throws IOException {
		if (text != null) {
			this.generator.writeRawValue(text);
		}

	}

	@Override
	public void writeRawValue(String text, int offset, int len) throws IOException {
		if (text != null) {
			this.generator.writeRawValue(text, offset, len);
		}

	}

	@Override
	public void writeRawValue(char[] text, int offset, int len) throws IOException {
		if (text != null){
			this.generator.writeRawValue(text, offset, len);
		}

	}

	@Override
	public void writeBinary(Base64Variant b64variant, byte[] data, int offset, int len) throws IOException {
		if (b64variant != null) {
			this.generator.writeBinary(b64variant, data, offset, len);
		}

	}

	@Override
	public int writeBinary(Base64Variant b64variant, InputStream data, int dataLength) throws IOException {
		return this.generator.writeBinary(b64variant, data, dataLength);
	}

	@Override
	public void writeNumber(int v) throws IOException {
		this.generator.writeNumber(v);
	}

	@Override
	public void writeNumber(long v) throws IOException {
		this.generator.writeNumber(v);
	}

	@Override
	public void writeNumber(BigInteger v) throws IOException {
		if (v != null) {
			this.generator.writeNumber(v);
		}

	}

	@Override
	public void writeNumber(double d) throws IOException {
		this.generator.writeNumber(d);
	}

	@Override
	public void writeNumber(float f) throws IOException {
		this.generator.writeNumber(f);
	}

	@Override
	public void writeNumber(BigDecimal dec) throws IOException {
		if (dec != null) {
			this.generator.writeNumber(dec);
		}

	}

	@Override
	public void writeNumber(String encodedValue) throws IOException {
		if (encodedValue != null) {
			this.generator.writeNumber(encodedValue);
		}

	}

	@Override
	public void writeBoolean(boolean state) throws IOException {
		this.generator.writeBoolean(state);
	}

	@Override
	public void writeNull() throws IOException {
		this.generator.writeNull();
	}

	@Override
	public void writeObject(Object pojo) throws IOException {
		if (pojo != null) {
			this.generator.writeObject(pojo);
		}
	}

	@Override
	public void writeTree(TreeNode rootNode) throws IOException {
		if (rootNode != null) {
			this.generator.writeTree(rootNode);
		}
	}

	@Override
	public void copyCurrentEvent(JsonParser jp) throws IOException {
		this.generator.copyCurrentEvent(jp);
	}

	@Override
	public void copyCurrentStructure(JsonParser jp) throws IOException {
		this.generator.copyCurrentStructure(jp);
	}

	@Override
	public JsonStreamContext getOutputContext() {
		return generator.getOutputContext();
	}

	/**
	 * Exporta um valor string no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param value valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	@Override
	public void writeStringField(String fieldName, String value) throws IOException {
		if (value != null) {
			this.generator.writeStringField(fieldName, value);
		}
	}

	/**
	 * Exporta um valor enum utilizando o seu name. Este método é nullsafe
	 * @param fieldName nome do campo a ser exportado
	 * @param value valor do enum
	 * @throws IOException exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeEnumField(String fieldName, Enum value) throws IOException {
		if (value != null) {
			this.generator.writeStringField(fieldName, value.name());
		}
	}

	/**
	 * Exporta um valor numérico no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeNumberField(String fieldName, Integer valor) throws IOException {
		if (valor != null) {
			this.generator.writeNumberField(fieldName, valor);
		}
	}

	/**
	 * Exporta um valor numérico no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeNumberField(String fieldName, Long valor) throws IOException {
		if (valor != null) {
			this.generator.writeNumberField(fieldName, valor);
		}
	}

	/**
	 * Exporta um valor numérico no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeNumberField(String fieldName, Float valor) throws IOException {
		if (valor != null) {
			this.generator.writeNumberField(fieldName, valor);
		}
	}

	/**
	 * Exporta um valor numérico no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeNumberField(String fieldName, Double valor) throws IOException {
		if (valor != null) {
			this.generator.writeNumberField(fieldName, valor);
		}
	}

	/**
	 * Exporta um valor booleano no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeBooleanField(String fieldName, Boolean valor) throws IOException {
		if (valor != null) {
			this.generator.writeBooleanField(fieldName, valor);
		}
	}

	/**
	 * Exporta um valor booleano no JSON, caso for nulo não é colocado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeObjectField(String fieldName, Boolean valor) throws IOException {
		if (valor != null) {
			this.generator.writeObjectField(fieldName, valor);
		}
	}

	/**
	 * Expora um objeto no json tratando a possibilidade dele ser nulo.
	 * Caso nulo, não é exportado
	 * @param fieldName nome do campo
	 * @param valor valor do campo
	 * @throws IOException Exceção lançada caso ocorra alguma falha durante a escrita
	 */
	public void writeObjectFieldNullSafe(String fieldName, Object valor) throws IOException {
		if (valor != null) {
			this.generator.writeObjectField(fieldName, valor);
		}
	}

	@Override
	public void flush() throws IOException {
		this.generator.flush();
	}

	@Override
	public boolean isClosed() {
		return this.generator.isClosed();
	}

	@Override
	public void close() throws IOException {
		this.generator.close();
	}

	public int getFeatureMask() {
		return 0;
	}

	public JsonGenerator setFeatureMask(int values) {
		return null;
	}
	

}
