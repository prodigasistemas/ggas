package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/**
 * Classe responsável pela conversao
 * de um string que representa uma chave primaria
 * em um objeto TipoCliente
 * 
 * @author pedro
 */
public class TipoClienteConverter implements Converter<String, TipoCliente>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public TipoCliente convert(String id) {
		TipoCliente tipoCliente = null;
		
		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				tipoCliente = Fachada.getInstancia().obterTipoCliente(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		
		return tipoCliente;
	}

}
