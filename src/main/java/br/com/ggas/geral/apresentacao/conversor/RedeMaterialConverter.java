
package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Fachada;
/**
 * Classe responsável por converter uma string que representa uma chave primaria em uma entidade do tipo RedeMaterial
 *
 */
public class RedeMaterialConverter implements Converter<String, RedeMaterial> {

	private static final Logger LOG = Logger.getLogger(RedeMaterialConverter.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public RedeMaterial convert(String value) {

		Long chavePrimaria = Long.parseLong(value);

		RedeMaterial redeMaterial = null;

		try {
			redeMaterial = Fachada.getInstancia().buscarRedeMaterialPorChave(chavePrimaria);
		} catch (NegocioException e) {
			LOG.error(e);
		}

		return redeMaterial;
	}

}
