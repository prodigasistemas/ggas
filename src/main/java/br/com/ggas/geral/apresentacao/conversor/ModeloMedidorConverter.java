package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.util.Fachada;
/**
 * Modelo Medidor
 * 
 * @author Pedro Silva
 *
 * Classe responsável pela conversão de uma entidade do tipo
 *
 * Método que recebe uma String e retorna 
 * um objeto ModeloMedidor
 */
public class ModeloMedidorConverter implements Converter<String, ModeloMedidor>{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public ModeloMedidor convert(String id) {
		ModeloMedidor modeloMedidor = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			
			try {
				modeloMedidor = Fachada.getInstancia().obterModeloMedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return modeloMedidor;
	}

}
