package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Localidade Converter
 * 
 * @author pedro 
 * 
 * Classe responsável por converter uma String que representa uma
 * chave primária em uma entidade Localidade
 * 
 */
public class LocalidadeConverter implements Converter<String, Localidade> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public Localidade convert(String id) {
		Localidade localidade = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				localidade = Fachada.getInstancia().buscarLocalidadeChave(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}
		return localidade;
	}
}
