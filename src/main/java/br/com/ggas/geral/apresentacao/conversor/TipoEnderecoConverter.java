package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pela conversao
 * de um string que representa uma chave primaria
 * em um objeto TipoEndereco
 * 
 * @author pedro
 */
public class TipoEnderecoConverter implements Converter<String, TipoEndereco> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public TipoEndereco convert(String id) {
		Long chavePrimaria = Long.parseLong(id);
		TipoEndereco tipoEndereco = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				tipoEndereco = ServiceLocator.getInstancia().getControladorCliente().buscarTipoEndereco(chavePrimaria);
			} catch (NegocioException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return tipoEndereco;
	}

}
