package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Converter de DocumentoCobrancaConverter
 * 
 * @category converter
 * 
 *
 */
public class DocumentoCobrancaConverter implements Converter<String, DocumentoCobranca> {

	@Override
	public DocumentoCobranca convert(String id) {
		DocumentoCobranca documentoCobranca = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				documentoCobranca = Fachada.getInstancia().obterDocumentoCobranca(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return documentoCobranca;
	}
}
