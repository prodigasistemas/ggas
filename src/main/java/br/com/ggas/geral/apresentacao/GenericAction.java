package br.com.ggas.geral.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.GGASApplicationContext;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.TokenProcessor;
import br.com.ggas.util.Util;

/**
 *GenericAction
 *
 */
public class GenericAction extends GenericActionRest {

	private static final String MENSAGEM_ALERTA = "mensagemAlerta";

	private static final String MSG = "msg";

	private static final int MAXIMO_POR_PAGINA = 15;

	private static final String TAMANHO_PAGINA = "tamanhoPagina";

	private static final String MENSAGEM_SUCESSO = "mensagemSucesso";

	private static final String MENSAGEM_ERRO = "mensagemErro";

	private static final String MENSAGEM_TIPO = "mensagemTipo";

	private static final String TELA_EXIBIR_MENSAGEM_TELA = "exibirMensagemTela";
	
	protected static final String ATRIBUTO_ABA_ID = "abaId";

	protected static final Logger LOG = Logger.getLogger(GenericAction.class);

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	private static final String BUNDLE_NAME = "mensagens";

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	private static TokenProcessor token = TokenProcessor.getInstance();
	
	/**
	 * Mensagem sucesso.
	 * 
	 * @param model the model
	 * @param msg the msg
	 */
	public void mensagemSucesso(ModelAndView model, String msg) {

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MSG, obterMensagem(msg));
		model.addObject(MENSAGEM_TIPO, MENSAGEM_SUCESSO);
	}

	/**
	 * Mensagem advertencia.
	 * 
	 * @param model the model
	 * @param msg the msg
	 */
	public void mensagemAdvertencia(ModelAndView model, String msg) {

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MSG, obterMensagem(msg));
		model.addObject(MENSAGEM_TIPO, MENSAGEM_ALERTA);
	}

	/**
	 * Mensagem sucesso.
	 * 
	 * @param model the model
	 * @param msg the msg
	 * @param params the params
	 */
	public void mensagemSucesso(ModelAndView model, String msg, Object... params) {

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MSG, obterMensagem(msg, params));
		model.addObject(MENSAGEM_TIPO, MENSAGEM_SUCESSO);
	}
	
	/**
	 * Mensagem sucesso.
	 * 
	 * @param model the model
	 * @param msg the msg
	 * @param params the params
	 */
	public void mensagemSucesso(Model model, String msg, Object... params) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MSG, obterMensagem(msg, params));
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_SUCESSO);
	}
	
	/**
	 * Mensagem sucesso.
	 *
	 * @param model the model
	 * @param msg A mensagem que será exibia
	 * @param request the request
	 * @param params Os parametros da mensagem
	 */
	public void mensagemSucesso(Model model, String msg, HttpServletRequest request, Object... params) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MSG, obterMensagem(msg, this.tratarParametros(params, request)));
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_SUCESSO);
	}
	
	/**
	 * Mensagem sucesso.
	 * 
	 * @param model the model
	 * @param msg A mensagem que será exibia
	 * @param request the request
	 * @param params Os parametros da mensagem
	 */
	public void mensagemErroParametrizado(Model model, String msg, HttpServletRequest request, Object... params) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MSG, obterMensagem(msg, this.tratarParametros(params, request)));
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}

	/**
	 * Mensagem de erro parametrizado.
	 * 
	 * @param model the model
	 * @param e the e
	 */
	public void mensagemErroParametrizado(ModelAndView model, GGASException e) {

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);

		if (e.getChaveErro() != null) {
			model.addObject(MSG, obterMensagem(e.getChaveErro(), e.getParametrosErro()));
		} else if (e.getMessage() != null) {
			model.addObject(MSG, obterMensagem(e.getMessage(), e.getParametrosErro()));
		} else if (e instanceof NegocioException) {
			mensagemErro(model, (NegocioException) e);
		}

		model.addObject(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}

	/**
	 * Exibe uma mensagem de erro com parâmetros
	 * @param model the model
	 * @param e A exceção lançada
	 */
	public void mensagemErroParametrizado(Model model, GGASException e) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);

		if (e.getChaveErro() != null) {
			model.addAttribute(MSG, obterMensagem(e.getChaveErro(), e.getParametrosErro()));
		} else if (e.getMessage() != null) {
			model.addAttribute(MSG, obterMensagem(e.getMessage(), e.getParametrosErro()));
		} else if (e instanceof NegocioException) {
			mensagemErro(model, (NegocioException) e);
		}

		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}
	
	/**
	 * Mensagem de erro parametrizado.
	 * 
	 * @param model the model
	 * @param request the request
	 * @param e A exceção lançada
	 */
	public void mensagemErroParametrizado(Model model, HttpServletRequest request, GGASException e) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);

		if (e.getChaveErro() != null) {
			model.addAttribute(MSG, obterMensagem(e.getChaveErro(), tratarParametros(e.getParametrosErro(), request)));
		} else if(e.getMessage() != null) {
			model.addAttribute(MSG, obterMensagem(e.getMessage(), tratarParametros(e.getParametrosErro(), request)));
		} else if(e instanceof NegocioException) {
			mensagemErro(model, request, (NegocioException) e);
		}

		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}
	
	/**
	 * Mensagem erro.
	 * 
	 * @param model the model
	 * @param e the e
	 */
	public void mensagemErro(ModelAndView model, NegocioException e) {

		Set<String> chaves = e.getErros().keySet();
		for (String msg : chaves) {
			model.addObject(MSG, obterMensagem(msg, e.getErros().get(msg)));
		}

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}
	
	/**
	 * Mensagem erro.
	 * 
	 * @param model - {@link Model}
	 * @param msg - {@link String}
	 */
	public void mensagemErro(Model model, String msg) {

		model.addAttribute(MSG, msg);
		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}
	
	/**
	 * Exibe uma mensagem de erro a partir de uma exceção
	 * @param model the model
	 * @param e A exceção
	 */
	public void mensagemErro(Model model, NegocioException e) {

		if (e.getErros() != null) {
			Set<String> chaves = e.getErros().keySet();
			for (String msg : chaves) {
				model.addAttribute(MSG, obterMensagem(msg, e.getErros().get(msg)));
			}
		} else {
			if( e.getMessage() != null) {
				model.addAttribute(MSG, obterMensagem(e.getMessage()));				
			}else if( e.getChaveErro() != null) {
				model.addAttribute(MSG, obterMensagem(e.getChaveErro()));				
			}
		}

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}

	/**
	 * Métod responsável pela exibição de mensagem de erro.
	 * 
	 * @param model the model
	 * @param request the request
	 * @param e A exceção lançada
	 */
	public void mensagemErro(Model model, HttpServletRequest request, NegocioException e) {

		if (e.getErros() != null) {
			Set<String> chaves = e.getErros().keySet();
			for (String msg : chaves) {
				model.addAttribute(MSG, obterMensagem(msg,
						this.obterMensagem(String.valueOf(e.getErros().get(msg)), request.getLocale())));
			}
		} else {
			model.addAttribute(MSG, obterMensagem(e.getMessage()));
		}

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}

	/**
	 * Mensagem erro.
	 *
	 * @param model the model
	 * @param listErros the list erros
	 */
	public void mensagemErro(ModelAndView model, List<String> listErros) {

		model.addObject(MSG, StringUtils.join(listErros, Constantes.STRING_ESPACO));
		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MENSAGEM_TIPO, MENSAGEM_ERRO);
		
	}

	/**
	 * Obter mensagem.
	 * 
	 * @param key the key
	 * @return the string
	 */
	public static String obterMensagem(String key) {

		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch(MissingResourceException e) {
			LOG.error(e.getMessage(), e);
			return '!' + key + '!';
		}
	}

	/**
	 * Obter mensagem.
	 * 
	 * @param key the key
	 * @param params the params
	 * @return the string
	 */
	public static String obterMensagem(String key, Object... params) {

		try {
			return MessageFormat.format(RESOURCE_BUNDLE.getString(key), params);
		} catch (MissingResourceException e) {
			LOG.error(e.getMessage(), e);
			return '!' + key + '!';
		}
	}

	/**
	 * Tratar Parametros.
	 * 
	 * @param parametrosErro
	 * @param request
	 * @return
	 */
	private Object[] tratarParametros(Object[] parametrosErro, HttpServletRequest request) {

		Object[] parametrosErroLocalizado = null;
		if (parametrosErro != null) {
			parametrosErroLocalizado = new String[parametrosErro.length];
			for (int i = 0; i < parametrosErro.length; i++) {
				parametrosErroLocalizado[i] = this.obterMensagem(String.valueOf(parametrosErro[i]),
						request.getLocale());

			}
		}
		return parametrosErroLocalizado;
	}

	/**
	 * Validar entidade.
	 * 
	 * @param entidade the entidade
	 * @throws NegocioException the negocio exception
	 */
	public void validarEntidade(EntidadeNegocio entidade) throws NegocioException {

		if (!entidade.validarDados().isEmpty()) {
			throw new NegocioException(entidade.validarDados());
		}
	}

	/**
	 * Validar chamado tipo existente.
	 * 
	 * @param chamadoTipo the chamado tipo
	 * @throws NegocioException the negocio exception
	 */
	public void validarChamadoTipoExistente(ChamadoTipo chamadoTipo) throws NegocioException {

		ChamadoTipo tipo = controladorChamadoTipo.validarChamadoTipoExistente(chamadoTipo);

		if (tipo != null && tipo.getChavePrimaria() != chamadoTipo.getChavePrimaria()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_EXISTENTE);
		}
	}

	/**
	 * Retorna o contentType relativo ao formato de impressão informado.
	 * 
	 * @param formatoImpressao Formato de impressão informado
	 * @return contentType
	 */
	public String obterContentTypeRelatorio(FormatoImpressao formatoImpressao) {

		String retorno = null;

		if (formatoImpressao.equals(FormatoImpressao.PDF)) {
			retorno = "application/pdf";
		} else if (formatoImpressao.equals(FormatoImpressao.XLS)) {
			retorno = "application/vnd.ms-excel";
		} else if (formatoImpressao.equals(FormatoImpressao.RTF)) {
			retorno = "application/rtf";
		}

		return retorno;
	}
	
	/**
	 * Retorna extensão, a ser exibina no
	 * download, relativa ao formato de impressão
	 * informado.
	 * 
	 * @param formatoImpressao
	 *            Formato de impressão informado
	 * @return extensão
	 */
	protected String obterFormatoRelatorio(FormatoImpressao formatoImpressao) {

		String retorno = null;

		if(formatoImpressao.equals(FormatoImpressao.PDF)) {
			retorno = ".pdf";
		} else if(formatoImpressao.equals(FormatoImpressao.XLS)) {
			retorno = ".xls";
		} else if(formatoImpressao.equals(FormatoImpressao.RTF)) {
			retorno = ".rtf";
		}

		return retorno;
	}

	/**
	 * Validar assunto existente.
	 * 
	 * @param assunto the assunto
	 * @param lista the lista
	 * @param indexList the index list
	 * @throws NegocioException the negocio exception
	 */
	public void validarAssuntoExistente(ChamadoAssunto assunto, List<ChamadoAssunto> lista, Integer indexList)
			throws NegocioException {

		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).getDescricao().equalsIgnoreCase(assunto.getDescricao()) && i != indexList) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CHAMADO_ASSUNTO_DESCRICAO);
			}

		}
	}

	/**
	 * Gets the dados auditoria.
	 * 
	 * @param request the request
	 * @return the dados auditoria
	 * @throws NegocioException the Negocio exception
	 */
	public DadosAuditoria getDadosAuditoria(HttpServletRequest request) throws NegocioException {

		return Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
				(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());
	}

	/**
	 * Mensagem Alerta.
	 * 
	 * @param model the model
	 * @param msg A mensagem que será exibida
	 * @param params Os parâmetros da mensagem
	 */
	public void mensagemAlerta(ModelAndView model, String msg, Object... params) {

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MSG, obterMensagem(msg, params));
		model.addObject(MENSAGEM_TIPO, MENSAGEM_ALERTA);
	}
	
	/**
	 * Mensagem Alerta.
	 *
	 * @param model the model
	 * @param msg A mensagem que será exibia
	 * @param params Os parametros da mensagem
	 */
	public void mensagemAlerta(Model model, String msg, Object... params) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MSG, obterMensagem(msg, params));
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ALERTA);
	}

	/**
	 * Mensagem Alerta.
	 * 
	 * @param model the model
	 * @param msg A mensagem que será exibida
	 */
	public void mensagemAlerta(ModelAndView model, String msg) {

		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MSG, msg);
		model.addObject(MENSAGEM_TIPO, MENSAGEM_ALERTA);
	}
	
	/**
	 * Método responsável por criar uma coleção paginada para o displaytag.
	 * 
	 * @param request O request
	 * @param filtro O filtro
	 * @param colecao A colecao original
	 * @return Uma coleção paginada
	 */
	protected PaginatedList criarColecaoPaginada(HttpServletRequest request, Map<String, Object> filtro,
			Collection<?> colecao) {

		ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

		if (colecaoPaginada != null) {
			List<?> list = new ArrayList(colecao);
			colecaoPaginada.setList(list);
			request.setAttribute("pageSize", colecaoPaginada.getFullListSize());
		}

		return colecaoPaginada;
	}
	
	/**
	 * Método responsável por adicionar os dados de paginação no filtro.
	 * 
	 * @param request O request
	 * @param filtro O filtro da consulta
	 */
	protected void adicionarFiltroPaginacao(HttpServletRequest request, Map<String, Object> filtro) {

		ColecaoPaginada colecaoPaginada = new ColecaoPaginada();
		String page = request.getParameter(ColecaoPaginada.PAGE);
		String sortCriterion = request.getParameter(ColecaoPaginada.SORT);
		
		int pageNumber = 1;
		int index = 0;
		int objectsPerPage = MAXIMO_POR_PAGINA;
		
		String tamanhoPagina = null;

		if (request.getParameter(TAMANHO_PAGINA) != null) {
			tamanhoPagina = request.getParameter(TAMANHO_PAGINA);
		} else {
			tamanhoPagina = (String) request.getSession().getAttribute(TAMANHO_PAGINA);
		}

		if (tamanhoPagina != null && !"".equals(tamanhoPagina)) {
			objectsPerPage = Integer.parseInt(tamanhoPagina);
		}
		
		request.getSession().setAttribute(TAMANHO_PAGINA, String.valueOf(objectsPerPage));

		if (page != null) {
			pageNumber = Integer.parseInt(page);
			if (pageNumber == 1) {
				index = 0;
			} else if (pageNumber > 1) {
				index = (pageNumber - 1) * objectsPerPage;
			} else {
				index = pageNumber;
			}
		}

		colecaoPaginada.setIndex(index);
		colecaoPaginada.setPageNumber(pageNumber);
		colecaoPaginada.setObjectsPerPage(objectsPerPage);
		
		if (ColecaoPaginada.DESC.equals(request.getParameter(ColecaoPaginada.DIRECTION))) {
			colecaoPaginada.setSortDirection(SortOrderEnum.DESCENDING);
		} else {
			colecaoPaginada.setSortDirection(SortOrderEnum.ASCENDING);
		}

		colecaoPaginada.setSortCriterion(sortCriterion);

		filtro.put("colecaoPaginada", colecaoPaginada);
	}
	
	/**
	 * Verifica se um array de chaves é vazio
	 * @param chavesPrimarias O array de chaves
	 * @throws NegocioException the NegocioException
	 */
	public void validarSelecaoUmOuMais(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}
	
	/**
	 * Método responsável por identificar se a ação é um postback.
	 * 
	 * @param request O request
	 * @return True ou false
	 */
	protected boolean isPostBack(HttpServletRequest request) {

		String postBack = request.getParameter("postBack");
		return Boolean.valueOf(postBack) || this.hasException(request);
	}
	
	/**
	 * Método responsável por verificar se existe um exception no request.
	 * 
	 * @param request O Request
	 * @return Verdade ou falso, dependendo se houve um exception ou não
	 */
	protected boolean hasException(HttpServletRequest request) {

		Object objEx = request.getAttribute("javax.servlet.jsp.jspException");
		return objEx != null;
	}

	/**
	 * Obtem a mensagem a partir da chave.
	 * 
	 * @param chave A chave
	 * @param locale O locale
	 * @return A Mensagem
	 */
	private String obterMensagem(String chave, Locale locale) {

		String mensagem = null;
		String chaveAux = null;
		StringBuilder stringBuilder = new StringBuilder();
		String[] arrChaves = chave.split(Constantes.STRING_VIRGULA_ESPACO);
		int countTokens = arrChaves.length;
		boolean colocarVirgula = true;

		if (countTokens > 0) {
			for (int i = 0; i < arrChaves.length; i++) {
				colocarVirgula = true;
				chaveAux = arrChaves[i];
				try {
					if (i == 0) {
						colocarVirgula = false;
					}
					if (locale != null) {
						String[] nomeAba = chaveAux.split(":");

						if (nomeAba.length > 1) {
							mensagem = nomeAba[0] + ": " + MensagemUtil
									.obterMensagem(ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, locale), nomeAba[1].trim());
							colocarVirgula = false;
						} else {
							mensagem = MensagemUtil.obterMensagem(
									ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, locale), chaveAux);
						}
					} else {
						mensagem = chaveAux;
					}
				} catch (MissingResourceException e) {
					LOG.error(e.getMessage(), e);
					mensagem = chaveAux;
				}
				if (countTokens >= 1 && colocarVirgula) {
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				stringBuilder.append(mensagem);
				countTokens--;
			}
		} else {
			if (locale != null) {
				try {
					mensagem = MensagemUtil
							.obterMensagem(ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, locale), chave);
				} catch (MissingResourceException e) {
					LOG.error(e.getMessage(), e);
					mensagem = chave;
				}
				stringBuilder.append(mensagem);
			} else {
				stringBuilder.append(chave);
			}
		}

		return stringBuilder.toString();
	}

	/**
	 * Método responsável por validar o token do form submetido.
	 * 
	 * @param request O request
	 * @throws GGASException the GGAS exception
	 */
	protected void validarToken(HttpServletRequest request) throws GGASException {

		token.generateToken(request);

		if (!token.isTokenValid(request, true)) {
			throw new GGASException(Constantes.ERRO_DUPLA_SUBMISSAO);
		}
	}

	/**
	 * Save a new transaction token in the user's current session, creating a new session if necessary.
	 *
	 * @param request The servlet request we are processing
	 */
	protected void saveToken(HttpServletRequest request) {
		token.saveToken(request);
	}

	/**
	 * 
	 * @param request -{@link HttpServletRequest}
	 * @return String -{@link String}
	 */
	protected String generateToken(HttpServletRequest request) {
		return token.generateToken(request);
	}

	/**
	 * Método responsável por salvar pesquisa anterior
	 * @param request - {@link HttpServletRequest}
	 * @param object - {@link Object}
	 * @param entidade - {@link String}
	 *  
	 */
	public void salvarPesquisa(HttpServletRequest request, Object object, String entidade) {
		
		if(request.getSession().getAttribute(entidade) == null) {
			request.getSession().setAttribute(entidade, object);
		}
	}
	/**
	 * Método responsável por retornar entidade salva em request.
	 * 
	 * @param request -{@link HttpServletRequest}
	 * @param entidade -{@link String}
	 * @return Object -{@link Object}
	 */
	public Object retornaPesquisa(HttpServletRequest request, String entidade) {
		
		return request.getSession().getAttribute(entidade);
	}
	
	/**
	 * Mensagem erro.
	 *
	 * @param model the model -{@link Model}
	 * @param erro -{@link String}
	 * @param request -{@link HttpServletRequest}
	 * 
	 */
	public void mensagemErro(Model model, HttpServletRequest request, String erro) {

		model.addAttribute(MSG, erro);
		model.addAttribute(MSG, obterMensagem(erro,
				this.obterMensagem(erro, request.getLocale())));
		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
		
	}

	
	/**
	 * Método responsável por gerar um pdf
	 * 
	 * @param nomeArquivo {@link String}
	 * @param relatorio {@link byte}
	 * @param response {@link HttpServletResponse}
	 */
	public void imprimirPDFRelatorio(String nomeArquivo, byte[] relatorio, HttpServletResponse response) {

		if(relatorio != null) {
			try {
				response.setContentType("application/pdf");
				response.setContentLength(relatorio.length);
				response.addHeader("Content-Disposition", "attachment; filename=" + nomeArquivo + ".pdf");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(relatorio, 0, relatorio.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch(IOException e) {
				LOG.error(e);
				throw new InfraestruturaException(e);
			}
		}
	}

	/**
	 * Método responsável por baixar um arquivo zip
	 *
	 * @param nomeArquivo {@link String}
	 * @param arquivo {@link byte}
	 * @param response {@link HttpServletResponse}
	 */
	public void baixarZip(String nomeArquivo, byte[] arquivo, HttpServletResponse response) {

		if(arquivo != null) {
			try {
				response.setContentLength(arquivo.length);
				response.addHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(arquivo, 0, arquivo.length);
				servletOutputStream.flush();
				servletOutputStream.close();
			} catch(IOException e) {
				LOG.error(e);
				throw new InfraestruturaException(e);
			}
		}
	}

	/**
	 * Gerar as mensagens de erro em json
	 * @param excecao exceção com as mensagens de erro
	 * @return json com as mensagens de erro geradas
	 */
	protected String gerarJsonMensagemErro(NegocioException excecao) {
		List<String> mensagens = new ArrayList<>();

		if (excecao.getErros() != null) {
			excecao.getErros().forEach((key, value) -> mensagens.add(Util.isoToUtf8(MensagemUtil.obterMensagem(key, value))));
		} else if (excecao.getChaveErro() != null) {
			mensagens.add(Util.isoToUtf8(obterMensagem(excecao.getChaveErro())));
		} else if (excecao.getMessage() != null) {
			mensagens.add(Util.isoToUtf8(excecao.getMessage()));
		} else {
			mensagens.add(Util.isoToUtf8(MensagemUtil.obterMensagem(Constantes.ERRO_INESPERADO)));
		}

		final Gson gson = new GsonBuilder().create();
		return gson.toJson(ImmutableMap.builder().put("erros", mensagens).build());
	}

	/**
	 * Adiciona o parâmetro do sistema "Campos texto devem ser digitados apenas em CAIXA ALTA?"
	 * como atributo do request
	 *
	 * @param request a requisição atual
	 * @throws GGASException caso ocorra algum erro na obtenção do parâmetro
	 */
	protected void adicionarParametroCaixaAlta(HttpServletRequest request) throws GGASException {
		String isCaixaAlta = getParametroCaixaAlta();
		request.getSession().setAttribute("isCaixaAlta", "1".equals(isCaixaAlta));
	}

	/**
	 * Adiciona o parâmetro do sistema "Campos texto devem ser digitados apenas em CAIXA ALTA?"
	 * como atributo da sessão
	 *
	 * @param session a sessão atual
	 * @throws GGASException caso ocorra algum erro na obtenção do parâmetro
	 */
	protected void adicionarParametroCaixaAlta(HttpSession session) throws GGASException {
		String isCaixaAlta = getParametroCaixaAlta();
		session.setAttribute("isCaixaAlta", "1".equals(isCaixaAlta));
	}

	private String getParametroCaixaAlta() throws GGASException {
		Fachada fachada = Fachada.getInstancia();
		ParametroSistema parametroCaixaAlta = fachada.obterParametroPorCodigo(Constantes.PARAMETRO_CAIXA_ALTA);
		String isCaixaAlta = "";
		if (parametroCaixaAlta != null) {
			isCaixaAlta = parametroCaixaAlta.getValor();
		}
		return isCaixaAlta;
	}
	
	/**
	 * Obtem operacao a partir da requisicao
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	protected Operacao obterOperacao(HttpServletRequest request) {

		return (Operacao) request.getAttribute(ATRIBUTO_OPERACAO);
	}
	
	/**
	 * 
	 * @param request
	 * @return Usuario
	 */
	protected Usuario obterUsuario(HttpServletRequest request) {

		return (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
	}

	/**
	 * Registrar Lock
	 *
	 * @param request
	 * @param operacaoLock
	 * @param mensagemLock
	 * @param chavesPrimarias
	 * @return
	 * @throws GGASException
	 */
	protected void registrarLock(HttpServletRequest request, String operacaoLock, String mensagemLock,
								 Long... chavesPrimarias) throws GGASException {
		Usuario usuarioLogado = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		for (Long chave : chavesPrimarias) {
			GGASApplicationContext.getInstancia().
					registrarLockOperacaoEntidadeUsuario(operacaoLock, chave, usuarioLogado, mensagemLock);
		}
	}

	/**
	 * Remover Lock
	 *
	 * @param request
	 * @param operacaoLock
	 * @param chavesPrimarias
	 * @return
	 * @throws GGASException
	 */
	protected void removerLock(HttpServletRequest request, String operacaoLock,
							   Long... chavesPrimarias) throws GGASException {
		Usuario usuarioLogado = (Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO);
		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			for (Long chave : chavesPrimarias) {
				GGASApplicationContext.getInstancia().
						desregistrarLockOperacaoEntidadeUsuario(operacaoLock, chave, usuarioLogado);
			}
		} else {
			GGASApplicationContext.getInstancia().desregistrarLockOperacaoEntidadeUsuario(operacaoLock, usuarioLogado);
		}
	}
	
	/**
	 * Mensagem Erro.
	 * 
	 * @param model the model
	 * @param msg the msg
	 * @param params the params
	 */
	public void mensagemErro(Model model, String msg, Object... params) {

		model.addAttribute(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addAttribute(MSG, obterMensagem(msg, params));
		model.addAttribute(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}
	
	/**
	 * Mensagem erro.
	 * 
	 * @param model - {@link Model}
	 * @param msg - {@link String}
	 */
	public void mensagemErro(ModelAndView model, String msg) {

		model.addObject(MSG, msg);
		model.addObject(TELA_EXIBIR_MENSAGEM_TELA, true);
		model.addObject(MENSAGEM_TIPO, MENSAGEM_ERRO);
	}
}
