package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.imovel.TipoSegmento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;
/** 
 * Classe responsável por converter uma String 
 * que representa uma chave primaria
 * em um tipo de segmento.
 * 
 * @author Pedro
 *
 */
public class TipoSegmentoConverter implements Converter<String, TipoSegmento> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public TipoSegmento convert(String id) {
		TipoSegmento tipoSegmento = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				tipoSegmento = Fachada.getInstancia().obterTipoSegmento(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return tipoSegmento;
	}

}
