package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;

import org.springframework.core.convert.converter.Converter;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.medidor.CapacidadeMedidor;
import br.com.ggas.util.Fachada;

/**
 * Capacidade Medidor Converter
 * @author Pedro Silva
 * 
 * classe responsável pela conversão de uma entidade
 * do tipo CapacidadeMedidor
 * 
 * Método que recebe uma String e retorna
 * um objeto CapacidadeMedidor
 */
public class CapacidadeMedidorConverter implements Converter<String, CapacidadeMedidor> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public CapacidadeMedidor convert(String id) {
		CapacidadeMedidor capacidadeMedidor = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				capacidadeMedidor = Fachada.getInstancia().obterCapacidadeMedidor(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return capacidadeMedidor;
	}

}
