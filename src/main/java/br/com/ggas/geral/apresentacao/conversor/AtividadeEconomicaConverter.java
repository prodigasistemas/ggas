package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

/**
 * Atividade Economica Converter
 * 
 * @author pedro
 * 
 *         Classe responsável pela conversão de uma String que represnta a
 *         chaveprimaria em um objeto AtividadeEconomica
 *
 */
public class AtividadeEconomicaConverter implements Converter<String, AtividadeEconomica> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public AtividadeEconomica convert(String id) {
		AtividadeEconomica atividadeEconomica = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {
			try {
				atividadeEconomica = Fachada.getInstancia().obterAtividadeEconomica(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return atividadeEconomica;
	}

}
