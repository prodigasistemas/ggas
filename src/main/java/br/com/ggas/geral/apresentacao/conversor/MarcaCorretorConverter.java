/*
 *
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 *
 *
 * Copyright (C) <2011> GGAS - Sistema de Gestión Comercial (Billing) de Servicios de Distribución de Gas
 *
 * Este archivo es parte del GGAS, un sistema de gestión comercial de Servicios de Distribución de Gas
 *
 * Este programa es un software libre; usted puede redistribuirlo y/o
 * modificarlo bajo los términos de la licencia pública general GNU, de conformidad
 * publicada por la Free Software Foundation; versión 2 de la licencia.
 *
 * El GGAS se distribuye a la espera de ser útil,
 * pero SIN CUALQUIER GARANTÍA; sin la garantía implícita de
 * COMERCIALIZACIÓN o de ADECUACIÓN A CUALQUIER PROPÓSITO EN PARTICULAR.
 * Consulte la Licencia Pública General de GNU para obtener más detalles.
 *
 * Usted debe haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa; si no, escriba a Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 * 
 * Copyright (C) 2011-2011 le GGAS - Sistema de Gestão Comercial (Facturation) de Serviços de Distribuição de Gás
 *
 * Ce fichier fait partie de GGAS, un système de gestion commercial pour les services de distribution de gaz
 *
 * GGAS est un logiciel libre; vous pouvez le redistribuer et / ou le modifier
 * sous les termes de la licence publique générale GNU publiée par
 * la Free Software Foundation; version 2 de la licence.
 *
 * GGAS est distribué dans l'espoir qu'il sera utile,
 * mais SANS AUCUNE GARANTIE; sans même la garantie implicite de
 * VALEUR MARCHANDE ou APTITUDE À UN USAGE PARTICULIER. Voir le
 * Licence publique générale GNU pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la licence publique générale GNU
 * avec ce programme; sinon, écrivez au logiciel libre
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, États-Unis
 *
 */

package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.util.ServiceLocator;

/**
 * Marca Corretor Converter
 * 
 * @author bruno silva
 * 
 * 
 *         Classe responsável por converter uma String que representa uma chave
 *         primária em uma entidade MarcaCorretor
 * 
 */
public class MarcaCorretorConverter implements Converter<String, MarcaCorretor> {
	/*
	 * 
	 * 
	 * (non-Javadoc)
	 * 
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 * 
	 * 
	 */
	@Override
	public MarcaCorretor convert(String id) {

		Long chavePrimaria = Long.parseLong(id);

		MarcaCorretor marcaCorretor = null;

		if (id != null && !"".equals(id) && chavePrimaria > 0) {

			try {
				marcaCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor()
						.obterMarcaVazaoCorretor(chavePrimaria);
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return marcaCorretor;
	}
}
