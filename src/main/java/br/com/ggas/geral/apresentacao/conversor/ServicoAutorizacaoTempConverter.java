package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoTemp;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Fachada;

public class ServicoAutorizacaoTempConverter implements Converter<String, ServicoAutorizacaoTemp> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.
	 * Object)
	 */
	@Override
	public ServicoAutorizacaoTemp convert(String id) {
		ServicoAutorizacaoTemp temp = null;

		if (id != null && !"".equals(id) && Long.parseLong(id) > 0) {

			try {
				temp = Fachada.getInstancia().obterServicoAutorizacaoTemp(Long.parseLong(id));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return temp;
	}
}
