
package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável pela conversão de uma chave primária
 *  em uma entidade referente ao tipo de botijão.
 * 
 *
 */
public class TipoBotijaoConverter implements Converter<String, TipoBotijao> {

	private static final Logger LOG = Logger.getLogger(TipoBotijaoConverter.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public TipoBotijao convert(String chave) {

		Long chavePrimaria = Long.parseLong(chave);

		TipoBotijao tipoBotijao = null;

		try {
			tipoBotijao = Fachada.getInstancia().buscarTipoBotijaoPorChave(chavePrimaria);
		} catch (NegocioException e) {
			LOG.error(e);
		}

		return tipoBotijao;
	}
}
