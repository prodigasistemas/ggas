package br.com.ggas.geral.apresentacao.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.util.Fachada;

public class FaixaPressaoFornecimentoConverter implements Converter<String, FaixaPressaoFornecimento>{

	@Override
	public FaixaPressaoFornecimento convert(String chave) {
		FaixaPressaoFornecimento faixaPressaoFornecimento = null;
		// TODO Auto-generated method stub
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {

			try {
				faixaPressaoFornecimento = Fachada.getInstancia().obterFaixaPressaoFornecimento(Long.parseLong(chave));
			} catch (GGASException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		return faixaPressaoFornecimento;
	}

}
