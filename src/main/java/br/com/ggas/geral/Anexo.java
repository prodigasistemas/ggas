/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * @since 28/01/2014 17:14:56
 * @author ccavalcanti
 */

package br.com.ggas.geral;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface Anexo.
 */
public abstract interface Anexo extends EntidadeNegocio {

	/** The erro negocio tipo arquivo anexo. */
	String ERRO_NEGOCIO_TIPO_ARQUIVO_ANEXO = "ERRO_NEGOCIO_TIPO_ARQUIVO_ANEXO";

	/** The erro negocio tamanho maximo arquivo anexo. */
	String ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO = "ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO_ANEXO";

	/**
	 * Gets the descricao anexo.
	 *
	 * @return the descricao anexo
	 */
	String getDescricaoAnexo();
	/**
	 * Sets the descricao anexo.
	 *
	 * @param descricaoAnexo the new descricao anexo
	 */
	void setDescricaoAnexo(String descricaoAnexo);
	/**
	 * Gets the documento anexo.
	 *
	 * @return the documento anexo
	 */
	byte[] getDocumentoAnexo();

	/**
	 * Sets the documento anexo.
	 *
	 * @param documentoAnexo the new documento anexo
	 */
	void setDocumentoAnexo(byte[] documentoAnexo);

	/**
	 * 
	 * @return the nomeArquivo
	 */
	String getNomeArquivo();

	/**
	 * 
	 * @param nomeArquivo the nomeArquivo to set
	 */
	void setNomeArquivo(String nomeArquivo);
}
