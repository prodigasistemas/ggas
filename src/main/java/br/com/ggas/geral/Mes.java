/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral;

import java.util.Map;
import java.util.TreeMap;

import br.com.ggas.geral.dominio.EntidadeDominio;

/**
 * Mes
 * 
 * Classe responsavel pelos meses.
 * 
 */
public interface Mes extends EntidadeDominio {

	static final int JANEIRO = 1;

	static final int FEVEREIRO = 2;

	static final int MARCO = 3;

	static final int ABRIL = 4;

	static final int MAIO = 5;

	static final int JUNHO = 6;

	static final int JULHO = 7;

	static final int AGOSTO = 8;

	static final int SETEMBRO = 9;

	static final int OUTUBRO = 10;

	static final int NOVEMBRO = 11;

	static final int DEZEMBRO = 12;

	String DESCRICAO_JANEIRO = "Janeiro";

	String DESCRICAO_FEVEREIRO = "Fevereiro";

	String DESCRICAO_MARCO = "Março";

	String DESCRICAO_ABRIL = "Abril";

	String DESCRICAO_MAIO = "Maio";

	String DESCRICAO_JUNHO = "Junho";

	String DESCRICAO_JULHO = "Julho";

	String DESCRICAO_AGOSTO = "Agosto";

	String DESCRICAO_SETEMBRO = "Setembro";

	String DESCRICAO_OUTUBRO = "Outubro";

	String DESCRICAO_NOVEMBRO = "Novembro";

	String DESCRICAO_DEZEMBRO = "Dezembro";

	Map<Integer, String> MESES_ANO = new TreeMap<Integer, String>(){

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(JANEIRO, DESCRICAO_JANEIRO);
			put(FEVEREIRO, DESCRICAO_FEVEREIRO);
			put(MARCO, DESCRICAO_MARCO);
			put(ABRIL, DESCRICAO_ABRIL);
			put(MAIO, DESCRICAO_MAIO);
			put(JUNHO, DESCRICAO_JUNHO);
			put(JULHO, DESCRICAO_JULHO);
			put(AGOSTO, DESCRICAO_AGOSTO);
			put(SETEMBRO, DESCRICAO_SETEMBRO);
			put(OUTUBRO, DESCRICAO_OUTUBRO);
			put(NOVEMBRO, DESCRICAO_NOVEMBRO);
			put(DEZEMBRO, DESCRICAO_DEZEMBRO);
		}
	};
}
