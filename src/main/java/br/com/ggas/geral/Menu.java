/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral;

import java.util.Collection;

import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Menu
 * 
 * Classe responsavel pelo Menu.
 * 
 */
public interface Menu extends EntidadeNegocio {

	String BEAN_ID_MENU = "menu";

	String DESCRICAO = "MENU_DESCRICAO";

	/**
	 * @return Retorna o atributo descricao.
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            O valor a ser atribuído ao
	 *            atributo descricao.
	 */
	void setDescricao(String descricao);

	/**
	 * @return the menuPai
	 */
	Menu getMenuPai();

	/**
	 * @param menuPai
	 *            the menuPai to set
	 */
	void setMenuPai(Menu menuPai);

	/**
	 * @return indicadorAlcada
	 */
	Boolean getIndicadorAlcada();

	/**
	 * @param indicadorAlcada
	 *            the indicadorAlcada to set
	 */
	void setIndicadorAlcada(Boolean indicadorAlcada);

	/**
	 * @return Colleciton<Menu> -  Retorna Coleção Menu.
	 */
	Collection<Menu> getListaMenu();

	/**
	 * @param listaMenu - Set lista menu.
	 */
	void setListaMenu(Collection<Menu> listaMenu);

	/**
	 * @return Integer - Retorna Numero Ordem.
	 */
	Integer getNumeroOrdem();

	/**
	 * @param numeroOrdem  -Set Número ordem.
	 */
	void setNumeroOrdem(Integer numeroOrdem);

	/**
	 * @return Recurso - Retorna Recurso.
	 */
	Recurso getRecurso();

	/**
	 * @param recurso - Set recurso.
	 */
	void setRecurso(Recurso recurso);

	/**
	 * @return String - Retorna Url.
	 */
	String getUrl();

	/**
	 * @param url - Set url.
	 */
	void setUrl(String url);

	/**
	 * @return Boolean - Retorna Favorito.
	 */
	boolean isFavorito();

	/**
	 * @param favorito - Set favorito.
	 */
	void setFavorito(boolean favorito);

	/**
	 * @return Collection<Permissao> - Retorna uma Coleção de Permissão.
	 */
	Collection<Permissao> getListaPermissao();

	/**
	 * @param listaPermissao - Set lista permissão. 
	 */
	void setListaPermissao(Collection<Permissao> listaPermissao);

	/**
	 * @return Boolean - Retorna Indicador Mostrar Menu.
	 */
	boolean isIndicadorMostrarMenu();

	/**
	 * @param indicadorMostrarMenu - Set indicador mostrar menu.
	 */
	void setIndicadorMostrarMenu(boolean indicadorMostrarMenu);

	boolean isIndicadorExibirSemFilhos();

	void setIndicadorExibirSemFilhos(boolean indicadorExibirSemFilhos);
}
