	
package br.com.ggas.geral;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Repositorio Generico
 *
 */
public abstract class RepositorioGenerico extends HibernateDaoSupport implements ControladorNegocio {

	private static final Logger LOG = Logger.getLogger(ControladorNegocioImpl.class);

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #criar()
	 */
	@Override
	public abstract EntidadeNegocio criar();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #getClasseEntidade()
	 */
	@Override
	public abstract Class<?> getClasseEntidade();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #inserir(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public long inserir(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Long chavePrimaria = null;
		long chave = 0;

		validarDadosEntidade(entidadeNegocio);
		preInsercao(entidadeNegocio);
		
		if(!entidadeNegocio.getClass().getName().equals(Constantes.CHAMADO_HISTORICO)) {
			entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		}

		if(!entidadeNegocio.getClass().getName().equals(Constantes.AREA_CONSTRUIDA)
						&& !entidadeNegocio.getClass().getName().equals(Constantes.CHAMADO_PROTOCOLO)) {
			entidadeNegocio.setHabilitado(Boolean.TRUE);
		}

		if(entidadeNegocio.getClass().getName().equals(Constantes.FERIADO)) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(entidadeNegocio);
		} else {
			chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);
		}

		if(chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}
		return chave;
	}
	

	/**
	 * Método utilizado, durante os processamentos em batch,
	 * para a inserção de uma entidade de negócio ativa ou 
	 * inativa, de acordo com o parâmetro {@code habilitado}
	*  @param entidadeNegocio - A entidade de negócio
	 * @return long - a chave primária da entidade
	 * @throws NegocioException - Caso ocorra alguma violação nas regras de negócio
	 */
	@Override
	public long inserirEmBatch(EntidadeNegocio entidadeNegocio, Boolean habilitado) throws NegocioException {

		Long chavePrimaria = null;
		long chave = 0;

		validarDadosEntidade(entidadeNegocio);
		preInsercao(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		entidadeNegocio.setHabilitado(habilitado);

		chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);

		if (chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}

		return chave;
	}

	/**
	 * Inserir sem validacao.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #inserir(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	public long inserirSemValidacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Long chavePrimaria = null;
		long chave = 0;

		preInsercao(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());

		if(!entidadeNegocio.getClass().getName().equals(Constantes.AREA_CONSTRUIDA)) {
			entidadeNegocio.setHabilitado(Boolean.TRUE);
		}

		if(entidadeNegocio.getClass().getName().equals(Constantes.FERIADO)) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(entidadeNegocio);
		} else {
			chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);
		}

		if(chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}
		return chave;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #obter(long)
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria) throws NegocioException {

		return this.obter(chavePrimaria, getClasseEntidade());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #obter(long, java.lang.Class)
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe) throws NegocioException {

		return this.obter(chavePrimaria, classe, (String[]) null);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #obter(long, java.lang.String[])
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return this.obter(chavePrimaria, getClasseEntidade(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #obter(long, java.lang.Class, boolean)
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe, String... propriedadesLazy) throws NegocioException {

		return obter(chavePrimaria, classe, null, propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#obter(long, java.lang.Boolean, java.lang.String[])
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Boolean habilitado, String... propriedadesLazy) throws NegocioException {

		return obter(chavePrimaria, getClasseEntidade(), habilitado, propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#obter(long,
	 * java.lang.String[])
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe, Boolean habilitado, String... propriedadesLazy)
					throws NegocioException {

		EntidadeNegocio entidade = null;

		StringBuilder hql = new StringBuilder();
		hql.append("from ");
		hql.append(classe.getSimpleName());
		hql.append(" entidade where ");
		hql.append(" entidade.chavePrimaria = :chavePrimaria ");
		if(habilitado != null) {
			hql.append(" and entidade.habilitado = :habilitado ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chavePrimaria);
		if(habilitado != null) {
			query.setBoolean("habilitado", habilitado);
		}

		entidade = (EntidadeNegocio) query.uniqueResult();

		if(entidade == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, classe.getName());
		}

		inicializarLazy(entidade, propriedadesLazy);

		return entidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #atualizar(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void atualizar(EntidadeNegocio entidadeNegocio) throws ConcorrenciaException, NegocioException {

		atualizar(entidadeNegocio, getClasseEntidade());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #atualizar(br.com.ggas.geral.negocio.
	 * EntidadeNegocio, java.lang.Class)
	 */
	@Override
	public void atualizar(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException {

		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(entidadeNegocio);
		validarVersaoEntidade(entidadeNegocio, classe);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		validarDadosEntidade(entidadeNegocio);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();		

		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(entidadeNegocio);
		posAtualizacao(entidadeNegocio);
		getSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #atualizar(br.com.ggas.geral.negocio.
	 * EntidadeNegocio, java.lang.Class)
	 */
	@Override
	public void atualizarEmBatch(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException {

		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		validarDadosEntidade(entidadeNegocio);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();

		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(entidadeNegocio);

		posAtualizacao(entidadeNegocio);
	}

	/**
	 * Atualizar sem validar.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @param classe
	 *            the classe
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void atualizarSemValidar(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException {

		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(entidadeNegocio);
		validarVersaoEntidade(entidadeNegocio, classe);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());

		getHibernateTemplate().getSessionFactory().getCurrentSession().update(entidadeNegocio);
		posAtualizacao(entidadeNegocio);
		getSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#atualizarColecao(java.lang.Object[], java.lang.Class)
	 */
	@Override
	public void atualizarColecao(Object[] entidade, Class<?> classe) throws NegocioException, ConcorrenciaException {

		if(entidade != null && entidade.length > 0) {
			for (int i = 0; i < entidade.length; i++) {
				this.atualizar((EntidadeNegocio) entidade[i], classe);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void remover(EntidadeNegocio entidadeNegocio) throws NegocioException {

		this.remover(entidadeNegocio, getClasseEntidade(), true);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio.
	 * EntidadeNegocio, boolean)
	 */
	@Override
	public void remover(EntidadeNegocio entidadeNegocio, boolean fisicamente) throws NegocioException {

		this.remover(entidadeNegocio, getClasseEntidade(), fisicamente);
	}

	/**
	 * Remover.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @param classe
	 *            the classe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio.
	 * EntidadeNegocio, java.lang.Class)
	 */
	public void remover(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException {

		this.remover(entidadeNegocio, classe, true);
	}

	/**
	 * Remover.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @param classe
	 *            the classe
	 * @param fisicamente
	 *            the fisicamente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio.
	 * EntidadeNegocio, java.lang.Class, boolean)
	 */
	public void remover(EntidadeNegocio entidadeNegocio, Class<?> classe, boolean fisicamente) throws NegocioException {

		preRemocao(entidadeNegocio);
		if(fisicamente) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().delete(entidadeNegocio);
		} else {
			entidadeNegocio.setHabilitado(Boolean.FALSE);
			getHibernateTemplate().getSessionFactory().getCurrentSession().update(entidadeNegocio);
		}

		posRemocao(entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void remover(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where ");
			hql.append(" chavePrimaria in (:chavesPrimarias) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesPrimarias", chavesPrimarias);
			Collection<EntidadeNegocio> entidades = query.list();
			if((entidades != null) && (!entidades.isEmpty())) {
				for (EntidadeNegocio entidade : entidades) {
					entidade.setDadosAuditoria(dadosAuditoria);
					this.remover(entidade);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#remover(java.lang.Long[], java.lang.Object, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void remover(Long[] chavesPrimarias, Object classe, DadosAuditoria dadosAuditoria) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(classe);
			hql.append(" where ");
			hql.append(" chavePrimaria in (:chavesPrimarias) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesPrimarias", chavesPrimarias);
			Collection<EntidadeNegocio> entidades = query.list();
			if((entidades != null) && (!entidades.isEmpty())) {
				for (EntidadeNegocio entidade : entidades) {
					entidade.setDadosAuditoria(dadosAuditoria);
					this.remover(entidade);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #listar()
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodas() throws NegocioException {

		return obterTodas(getClasseEntidade(), null);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #listar(boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodas(boolean habilitado) throws NegocioException {

		return obterTodas(getClasseEntidade(), habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #listar(java.lang.Class, java.lang.Boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodas(Class<?> classe, Boolean habilitado) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe.getSimpleName());

		if(habilitado != null) {
			hql.append(" where habilitado = ? ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(habilitado != null) {
			query.setBoolean(0, habilitado);
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #
	 * validarDadosEntidade(br.com.ggas.geral.negocio
	 * .EntidadeNegocio)
	 */
	@Override
	public void validarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException {
		
		
		Map<String, Object> erros = entidadeNegocio.validarDados();
		if(erros != null && !erros.isEmpty()) {
			throw new NegocioException(erros);
		}

		posValidarDadosEntidade(entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #
	 * validarVersaoEntidade(br.com.ggas.geral.negocio
	 * .EntidadeNegocio)
	 */
	@Override
	public void validarVersaoEntidade(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException {

		Integer versaoAtual = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select entidade.versao from ");
		hql.append(classe.getSimpleName());
		hql.append(" entidade ");
		hql.append(" where entidade.chavePrimaria = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, entidadeNegocio.getChavePrimaria());

		getSession().flush();

		versaoAtual = (Integer) query.uniqueResult();

		if(entidadeNegocio.getVersao() != versaoAtual.intValue()) {
			throw new ConcorrenciaException(Constantes.ERRO_ENTIDADE_VERSAO_DESATUALIZADA, classe.getName());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #posAtualizacao(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void posAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #posInsercao(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void posInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #posRemocao(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void posRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #preAtualizacao(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #preInsercao(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #preRemocao(br.com.ggas.geral.negocio.
	 * EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:23

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.ControladorNegocio
	 * #posValidarDadosEntidade(br.com.ggas.geral.
	 * negocio.EntidadeNegocio)
	 */
	@Override
	public void posValidarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:23

	/**
	 * Método responsável por criar um objeto
	 * criteria.
	 * A classe do criteria será disponibilizada
	 * pelo método getClasseEntidade()
	 * 
	 * @return Um objeto criteria
	 */
	protected Criteria getCriteria() {

		return createCriteria(getClasseEntidade());
	}

	/**
	 * Método responsável por criar um criteria.
	 * 
	 * @param classe
	 *            A classe do criteria
	 * @return Um objeto criteria
	 */
	protected Criteria createCriteria(Class<?> classe) {

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(classe);
	}

	/**
	 * Método responável por criar um objeto Query.
	 *
	 * @param hql Consulta hql que será utilizada no objeto Query
	 * @return Retorna um objeto do tipo Query
	 */
	protected Query getQuery(String hql) {
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
	}

	/**
	 * Método responsável por criar um criteria.
	 * 
	 * @param classe
	 *            A classe do criteria
	 * @param alias
	 *            O alias do criteria
	 * @return Um objeto criteria
	 */
	protected Criteria createCriteria(Class<?> classe, String alias) {

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(classe, alias);
	}

	/**
	 * Método responsável por inicializar o lazy
	 * de um objeto.
	 * 
	 * @param entidade
	 *            O objeto
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            inicializadas
	 */
	protected void inicializarLazy(Object entidade, String... propriedadesLazy) {

		if(entidade != null) {
			Hibernate.initialize(entidade);
			if(propriedadesLazy != null && propriedadesLazy.length > 0) {
				for (String propriedade : propriedadesLazy) {
					this.obterValorPropriedade(entidade, propriedade);
				}
			}

		}
	}

	private void obterValorPropriedade(Object entidade, String propriedade) {
		try {
			BeanUtils.getProperty(entidade, propriedade);
		} catch (Exception e) {
			LOG.error("Erro ao tentar obter o valor da propriedade: " + propriedade, e);
		}
	}
	
	/**
	 * Método responsável por inicializar o lazy
	 * de uma coleção.
	 * 
	 * @param colecao
	 *            the colecao
	 * @param propriedadesLazy
	 *            As propriedades que serão
	 *            inicializadas
	 * @return tudo
	 */
	protected Collection<Object> inicializarLazyColecao(Collection<Object> colecao, String... propriedadesLazy) {

		if(colecao != null && !colecao.isEmpty()) {
			for (Object object : colecao) {
				Hibernate.initialize(object);
				if (propriedadesLazy != null && propriedadesLazy.length > 0) {
					this.obterValorPropriedade(object, propriedadesLazy);
				}
			}

		}
		return colecao;
	}

	private void obterValorPropriedade(Object object, String[] propriedadesLazy) {

		for (String propriedade : propriedadesLazy) {
			try {
				BeanUtils.getProperty(object, propriedade);
			} catch (Exception e) {
				LOG.error("Erro ao tentar obter o valor da propriedade: " + propriedade, e);
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#validarExistenciaEntidades(br.com.ggas.geral.negocio.EntidadeNegocio,
	 * br.com.ggas.geral.negocio.EntidadeNegocio, java.util.Map)
	 */
	@Override
	public String validarExistenciaEntidades(EntidadeNegocio classeOrigem, EntidadeNegocio objetoValor,
					Map<String, Object[]> mapaInconsistencias) throws NegocioException {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#obter(long, java.lang.Class, java.lang.Boolean)
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe, Boolean habilitado) {

		return null;
	}
	
	@Override
	public long inserirInativo(EntidadeNegocio entidadeNegocio) throws NegocioException {
		
		Long chavePrimaria = null;
		long chave = 0;
		
		validarDadosEntidade(entidadeNegocio);
		preInsercao(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		
		entidadeNegocio.setHabilitado(Boolean.FALSE);
		
		chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);
		
		if(chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}
		
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		return chave;
	}
	
	@Override
	public void atualizarSaveOrUpdate(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException {
		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		validarDadosEntidade(entidadeNegocio);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();
		getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(entidadeNegocio);

		posAtualizacao(entidadeNegocio);
		getSession().flush();
		
	}

	

}
