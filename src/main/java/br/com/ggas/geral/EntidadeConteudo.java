/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral;

import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;

/**
 * EntidadeConteudo para Entidade Conteudo
 *
 */
public interface EntidadeConteudo extends TabelaAuxiliar {

	String BEAN_ID_ENTIDADE_CONTEUDO = "entidadeConteudo";

	String PONTO_CONSUMO = "PONTO DE CONSUMO";

	String DISTRIBUIDORA = "DISTRIBUIDORA";

	String SUPRIDORA = "SUPRIDORA";

	String FIXO_CITY_GATE = "FIXO POR CITY GATE";

	String AMORTIZACAO = "ENTIDADE_CONTEUDO_AMORTIZACAO";
	
	String ENTIDADE_CONTEUDO_ROTULO = "ENTIDADE_CONTEUDO_ROTULO";


	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_DISTRIBUIDORA = 2;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_SUPRIDORA = 3;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PARADA_PROGRAMADA = 71;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PARADA_NAO_PROGRAMA = 72;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PARADA_CASO_FORTUITO = 73;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_FALHA_FORNECIMENTO = 74;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_DIARIA = 159;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_MEDIA_DIARIA = 158;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PERIODICA = 157;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CASCATA_COMPLEMENTAR = 190;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_MVA = 155;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PMPF = 156;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PETROBRAS = 194;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CDL = 195;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_AGRUPAMENTO_POR_VOLUME = 100;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_AGRUPAMENTO_POR_VALOR = 101;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_TIPO_NOTA_SERVICO = 16;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_TIPO_NOTA_PRODUTO = 15;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CALCULO_CASCATA = 138;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CALCULO_FAIXA = 161;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final String CODIGO_TIPO_CONVENIO_CLIENTE = "C";
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final String CODIGO_TIPO_CONVENIO_BANCO = "B";
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_UNIDADE_MONETARIA_DOLAR = 63;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_TIPO_OPERACAO_ENTRADA = 50;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PERIODICIDADE_PENALIDADE_MENSAL = 33;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_PERIODICIDADE_PENALIDADE_ANUAL = 34;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_BASE_APURACAO_PENALIDADE_DIARIA = 107;

	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_BASE_APURACAO_PENALIDADE_MENSAL = 108;

	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_QDC = 86;

	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_QNR = 87;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CONSUMO_REFERENCIA_PENALIDADE_QDC = 20;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CONSUMO_REFERENCIA_PENALIDADE_QDP = 21;
	
	/**
	 * @deprecated (Será removido) 
	 */
	@Deprecated
	static final long CHAVE_CONSUMO_REFERENCIA_PENALIDADE_MENOR_ENTRE_QDC_QDP = 22;

}
