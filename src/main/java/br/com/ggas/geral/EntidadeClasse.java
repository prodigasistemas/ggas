/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral;

import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;

/**
 * Entidade Classe
 * 
 * Classe responsavel pela Entidade de Classe.
 *
 */
public interface EntidadeClasse extends TabelaAuxiliar {

	String BEAN_ID_ENTIDADE_CLASSE = "entidadeClasse";

	String CODIGO_GARANTIA_FINANCEIRA = "GARANTIA_FINANCEIRA";

	String CODIGO_REGIME_CONSUMO = "REGIME_CONSUMO";

	String CODIGO_CRITERIO_USO = "CRITERIO_USO";
	
	String DESCRICAO = "ENTIDADE_CLASSE_DESCRICAO";
	
	String ENTIDADE_CLASSE = "ENTIDADE_CLASSE_ROTULO";

	/* 
	 * Ainda esta sendo definido
	 */
	String CODIGO_FORMA_PAGAMENTO = "FORMA_PAGAMENTO";
	/* 
	 * Ainda esta sendo definido
	 */
	String CODIGO_TIPO_FATURAMENTO = "TIPO_FATURAMENTO";

	String CODIGO_CONSUMO_REFERENCIAL = "CONSUMO_REFERENCIAL";

	String CODIGO_ITEM_FATURA = "ITEM_FATURA";

	String CODIGO_ENDERECO_PADRAO = "ENDERECO_FATURA";

	String CODIGO_FASE_REFERENCIAL_VENCIMENTO = "FASE_REFERENCIAL_VENCIMENTO";

	String CODIGO_OPCAO_FASE_REFERENC_VENCIMENTO = "OPCAO_FASE_REFERENC_VENCIMENTO";

	String CODIGO_PERIODICIDADE_TOP = "PERIODICIDADE_TOP";

	String CODIGO_PERIODICIDADE_SOP = "PERIODICIDADE_SOP";

	String CODIGO_HORA_INICIAL_DIA = "HORA_INICIAL_DIA";

	/*
	 *  vai ser um parametro do sistema.
	 */
	String CODIGO_LOCAL_AMOSTRAGEM_PCS = "LOCAL_AMOSTRAGEM_PCS";

	String CODIGO_ENCARGOS_TRIBUTARIOS = "ENCARGOS_TRIBUTARIOS";

	String CODIGO_UNIDADES_MONETARIAS = "UNIDADES_MONETARIAS";

	String CODIGO_RESPONSABILIDADES = "RESPONSABILIDADES";

	String CODIGO_TIPO_PARADA = "TIPO_PARADA";

	String CODIGO_TIPO_ATRIBUTO = "TIPO_ATRIBUTO";

	String CODIGO_TIPO_INTEGRACAO = "TIPO_INTEGRACAO";

	String CODIGO_FORMA_ARRECADACAO = "FORMA_ARRECADACAO";

	String CODIGO_TIPO_DEVOLUCAO = "TIPO_DEVOLUCAO";

	String CODIGO_OPCAO_DIAS = "OPCAO_DIAS";

	String CODIGO_TIPO_DOCUMENTO_RECEBIMENTO = "TIPO_DOCUMENTO_RECEBIMENTO";

	String CODIGO_TIPO_CALCULO = "TIPO_CALCULO";

	String CODIGO_BASE_APURACAO = "BASE_APURACAO";

	String CODIGO_PERIODICIDADE_COBRANCA = "PERIODICIDADE_COBRANCA";

	String CODIGO_INICIO_COBRANCA = "INICIO_COBRANCA";

	String CODIGO_MOTIVO_CANCELAMENTO = "MOTIVO_CANCELAMENTO";

	String CODIGO_MOTIVO_REFATURAMENTO_FATURA = "MOTIVO_REFATURAMENTO_FATURA";

	String CODIGO_TIPO_CREDITO_DEBITO = "TIPO_CREDITO_DEBITO";

	String CODIGO_DATA_REFERENCIA_CAMBIAL = "DATA_REFERENCIA_CAMBIAL";

	String CODIGO_DIA_COTACAO = "DIA_COTACAO";

	String CODIGO_TIPO_SUSBTITUTO = "TIPO_SUSBTITUTO";

	String CODIGO_CONTRATO_COMPRA = "CONTRATO_COMPRA";

	String CODIGO_ACAO_RECEBIMENTO_A_MENOR = "ACAO_RECEBIMENTO_A_MENOR";

	String CODIGO_AMORTIZACAO = "AMORTIZACAO";

	String CODIGO_TIPO_AGRUPAMENTO = "TIPO_AGRUPAMENTO";

	String CODIGO_TIPO_MENSAGEM = "TIPO_MENSAGEM";

	String CODIGO_TIPO_MULTA_RECISORIA = "TIPO_MULTA_RECISORIA";

	String CODIGO_BASE_APURACAO_PENALIDADE_RETIRADA_MAIOR_MENOR = "BASE_APURACAO_PENALIDADE_RETIRADA_MAIOR_MENOR";

	String CODIGO_MODALIDADE_USO = "MODALIDADE_USO";

	String CODIGO_TIPO_PARAMETRO = "TIPO_PARAMETRO";

	String CODIGO_BAIXA_RECEBIMENTO_MENOR_DESPESA = "CODIGO_BAIXA_RECEBIMENTO_MENOR_DESPESA";

	String CODIGO_FORMA_COBRANCA_PARCELAMENTO = "FORMA_COBRANCA_PARCELAMENTO";

	String CODIGO_FORMA_INCLUSAO_FATURA = "FORMA_INCLUSAO_FATURA";

	String CODIGO_SITUACAO_PAGAMENTO = "SITUACAO_PAGAMENTO";

	String CODIGO_TIPO_OPERACAO = "TIPO_OPERACAO";

	String CODIGO_REAJUSTE_CASCATA = "REAJUSTE_CASCATA";

	String CODIGO_VOLUME_REFERENCIA_TARIFA_MEDIA_TOP = "VOLUME_REFERENCIA_TARIFA_MEDIA_TOP";

	String CODIGO_SITUACAO_TRIBUTARIA_PIS_COFINS = "SITUACAO_TRIBUTARIA_PIS_COFINS";

	String CODIGO_REGIME_TRIBUTARIO = "REGIME_TRIBUTARIO";

	String CODIGO_STATUS_NOTA_FISCAL_ELETRONICA = "STATUS_NOTA_FISCAL_ELETRONICA";

	String CODIGO_AMBIENTE_SISTEMA_NFE = "AMBIENTE_SISTEMA_NFE";

	String CODIGO_STATUS_TARIFA = "STATUS_TARIFA";

	String CODIGO_ARREDONDAMENTO = "ARREDONDAMENTO";

	String EQUIPAMENTO = "EQUIPAMENTO";

	String TURNO = "TURNO";

	String CODIGO_TIPO_ANEXO = "TIPO_ANEXO";

	String TIPO_FALE_CONOSCO = "TIPO_FALE_CONOSCO";

	String TIPO_MODELO_CONTRATO = "TIPO_MODELO_CONTRATO";

	String TIPO_APURACAO = "TIPO_APURACAO";

	String PRECO_COBRANCA = "PRECO_COBRANCA";

	String APURACAO_PARADA_PROGRAMADA = "APURACAO_PARADA_PROGRAMADA";

	String APURACAO_PARADA_NAO_PROGRAMADA = "APURACAO_PARADA_NAO_PROGRAMADA";

	String APURACAO_CASO_FORTUITO = "APURACAO_CASO_FORTUITO";

	String APURACAO_FALHA_FORNECIMENTO = "APURACAO_FALHA_FORNECIMENTO";

	String TIPO_AGRUPAMENTO_CONTRATO = "TIPO_AGRUPAMENTO_CONTRATO";

	String STATUS_IMOVEL = "STATUS_IMOVEL";

	String CODIGO_TIPO_COMBUSTIVEL = "TIPO_COMBUSTIVEL";
	
	String CODIGO_TIPO_CONSUMO_FATURAMENTO = "TIPO_CONSUMO_FATURAMENTO";
}
