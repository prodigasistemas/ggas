/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Controlador Menu
 *
 */
public interface ControladorMenu extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_MENU = "controladorMenu";

	/**
	 * Método responsável por listar todos os
	 * módulos.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return módulos do sistema
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<Menu> listarModulos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Listar funcionalidades por modulo.
	 * 
	 * @param idModulo
	 *            the id modulo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Menu> listarFuncionalidadesPorModulo(Long idModulo) throws NegocioException;

	/**
	 * Listar funcionalidades filho.
	 * 
	 * @return Collection<Menu>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Menu> listarFuncionalidadesFilho() throws NegocioException;

	/**
	 * Criar favoritos.
	 * 
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarFavoritos();

	/**
	 * Inserir favorito.
	 * 
	 * @param favorito
	 *            the favorito
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	String inserirFavorito(Favoritos favorito) throws GGASException;

	/**
	 * Consulta um menu pelo codigo da tabela
	 * @param codigoTabela O codigo da tabela
	 * @return O menu
	 * @throws NegocioException O NegocioException
	 */
	Menu consultarMenu(Long codigoTabela) throws NegocioException;

	/**
	 * Listar modulos.
	 * 
	 * @param usuarioLogado
	 *            the usuario logado
	 * @return the collection
	 */
	Collection<Menu> listarModulos(Usuario usuarioLogado);
}
