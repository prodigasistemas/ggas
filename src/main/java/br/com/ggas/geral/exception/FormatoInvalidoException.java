/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.exception;

import java.util.ResourceBundle;

/**
 * Classe FormatoInvalidoException.
 * 
 * 
 */
public class FormatoInvalidoException extends GGASException {

	/**
     * 
     */
	private static final long serialVersionUID = 3167684934352610145L;

	/**
	 * Instantiates a new formato invalido exception.
	 */
	public FormatoInvalidoException() {

		super();
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param rootCause
	 *            the root cause
	 */
	public FormatoInvalidoException(ResourceBundle rb, String chave, Exception rootCause) {

		super(rb, chave, rootCause);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public FormatoInvalidoException(ResourceBundle rb, String chave, Object param) {

		super(rb, chave, param);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 * @param rootCause
	 *            the root cause
	 */
	public FormatoInvalidoException(ResourceBundle rb, String chave, Object[] param, Exception rootCause) {

		super(rb, chave, param, rootCause);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public FormatoInvalidoException(ResourceBundle rb, String chave, Object[] param) {

		super(rb, chave, param);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param rb
	 *            the rb
	 * @param chave
	 *            the chave
	 */
	public FormatoInvalidoException(ResourceBundle rb, String chave) {

		super(rb, chave);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param chave
	 *            the chave
	 * @param rootCause
	 *            the root cause
	 */
	public FormatoInvalidoException(String chave, Exception rootCause) {

		super(chave, rootCause);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public FormatoInvalidoException(String message, Throwable cause) {

		super(message, cause);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param message
	 *            the message
	 */
	public FormatoInvalidoException(String message) {

		super(message);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public FormatoInvalidoException(Throwable cause) {

		super(cause);
	}

	/**
	 * Instantiates a new formato invalido exception.
	 * 
	 * @param chave
	 *            the chave
	 * @param param
	 *            the param
	 */
	public FormatoInvalidoException(String chave, Object param) {

		super(chave, param);
	}

}
