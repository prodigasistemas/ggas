/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe Menu.
 * 
 */
public class MenuImpl extends EntidadeNegocioImpl implements Menu {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -2562673070288512470L;

	private Menu menuPai;

	private String descricao;

	private Boolean indicadorAlcada;

	private Collection<Menu> listaMenu;

	private Integer numeroOrdem;

	private Recurso recurso;

	private String url;

	private boolean favorito;

	private Collection<Permissao> listaPermissao;

	private boolean indicadorMostrarMenu;

	private boolean indicadorExibirSemFilhos;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.menu.Menu#getMenuPai()
	 */
	@Override
	public Menu getMenuPai() {

		return menuPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.dominio.EntidadeDominio
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.menu.Menu#setMenuPai(
	 * br.com.ggas.comum.menu.Menu)
	 */
	@Override
	public void setMenuPai(Menu menuPai) {

		this.menuPai = menuPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.dominio.EntidadeDominio
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.Menu#getIndicadorAlcada()
	 */
	@Override
	public Boolean getIndicadorAlcada() {

		return indicadorAlcada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.Menu#setIndicadorAlcada
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorAlcada(Boolean indicadorAlcada) {

		this.indicadorAlcada = indicadorAlcada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof EntidadeNegocioImpl)) {
			return false;
		}
		if(this.getChavePrimaria() > 0) {
			EntidadeNegocioImpl that = (EntidadeNegocioImpl) obj;
			return new EqualsBuilder().append(this.getChavePrimaria(), that.getChavePrimaria()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.getChavePrimaria() > 0) {
			return new HashCodeBuilder(3, 5).append(this.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}
		return erros;
	}

	@Override
	public Collection<Menu> getListaMenu() {

		return listaMenu;
	}

	@Override
	public void setListaMenu(Collection<Menu> listaMenu) {

		this.listaMenu = listaMenu;
	}

	@Override
	public Integer getNumeroOrdem() {

		return numeroOrdem;
	}

	@Override
	public void setNumeroOrdem(Integer numeroOrdem) {

		this.numeroOrdem = numeroOrdem;
	}

	@Override
	public Recurso getRecurso() {

		return recurso;
	}

	@Override
	public void setRecurso(Recurso recurso) {

		this.recurso = recurso;
	}

	@Override
	public String getUrl() {

		return url;
	}

	@Override
	public void setUrl(String url) {

		this.url = url;
	}

	@Override
	public boolean isFavorito() {

		return favorito;
	}

	@Override
	public void setFavorito(boolean favorito) {

		this.favorito = favorito;
	}

	@Override
	public Collection<Permissao> getListaPermissao() {

		return listaPermissao;
	}

	@Override
	public void setListaPermissao(Collection<Permissao> listaPermissao) {

		this.listaPermissao = listaPermissao;
	}

	@Override
	public boolean isIndicadorMostrarMenu() {

		return indicadorMostrarMenu;
	}

	@Override
	public void setIndicadorMostrarMenu(boolean indicadorMostrarMenu) {

		this.indicadorMostrarMenu = indicadorMostrarMenu;
	}

	public boolean isIndicadorExibirSemFilhos() {
		return indicadorExibirSemFilhos;
	}

	public void setIndicadorExibirSemFilhos(boolean indicadorExibirSemFilhos) {
		this.indicadorExibirSemFilhos = indicadorExibirSemFilhos;
	}
}
