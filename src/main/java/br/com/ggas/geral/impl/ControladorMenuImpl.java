/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 *
 */
class ControladorMenuImpl extends ControladorNegocioImpl implements ControladorMenu {

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String MENU_PAI = "menuPai";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Menu.BEAN_ID_MENU);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Menu.BEAN_ID_MENU);
	}

	public Class<?> getClasseEntidadePapel() {

		return ServiceLocator.getInstancia().getClassPorID(Papel.BEAN_ID_PAPEL);
	}

	public Class<?> getClasseEntidadeFavoritos() {

		return ServiceLocator.getInstancia().getClassPorID(Favoritos.BEAN_ID_FAVORITOS);
	}

	/**
	 * Consultar menu.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	public Collection<Menu> consultarMenu(Map<String, Object> filtro) {

		Criteria criteria = this.createCriteria(getClasseEntidade());

		if(filtro != null) {
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if((chavePrimaria != null) && (chavePrimaria > 0)) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			String nome = (String) filtro.get(DESCRICAO);
			if(!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.like(DESCRICAO, nome, MatchMode.ANYWHERE));
			}

			String habilitado = (String) filtro.get(HABILITADO);
			if(!StringUtils.isEmpty(habilitado)) {
				criteria.add(Restrictions.eq(HABILITADO, Boolean.valueOf(habilitado)));
			}

			Long pai = (Long) filtro.get(MENU_PAI);
			if(pai != null && pai > 0) {
				criteria.add(Restrictions.eq(MENU_PAI, pai));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorMenu#listarModulos
	 * ()
	 */
	@Override
	public Collection<Menu> listarModulos(Map<String, Object> filtro) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" menu");
		hql.append(" where ");
		hql.append(" menu.menuPai is null and menu.habilitado = 1");
		hql.append(" order by menu.numeroOrdem ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}


	@Override
	public Menu consultarMenu(Long codigoTabela) throws NegocioException {
		return (Menu) createHQLQuery("select a.menu from TabelaImpl a " +
				"where a.chavePrimaria = :codigoTabela")
				.setParameter("codigoTabela", codigoTabela)
				.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorMenu#listarModulos(br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<Menu> listarModulos(Usuario usuarioLogado) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" menu ");
		hql.append(" where ");
		hql.append(" menu.menuPai is null and menu.habilitado = 1");
		hql.append(" order by menu.numeroOrdem ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		List<Menu> modulos = query.list();

		Long[] idPapelUsuario = new Long[usuarioLogado.getPapeis().size()];
		int posicao = 0;
		for (Papel currentPapel : usuarioLogado.getPapeis()) {
			idPapelUsuario[posicao] = currentPapel.getChavePrimaria();
			posicao++;
		}

		for (Menu currentMenu : modulos) {
			Util.removeMenuSemPermissaoSistema(currentMenu, idPapelUsuario);
		}
		return modulos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorMenu#listarFuncionalidadesPorModulo(java.lang.Long)
	 */
	@Override
	public Collection<Menu> listarFuncionalidadesPorModulo(Long idModulo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" menu ");
		hql.append(" where ");
		hql.append(" menu.menuPai.chavePrimaria = :idModulo ");
		hql.append(" order by descricao asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idModulo", idModulo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorMenu#listarFuncionalidadesFilho()
	 */
	@Override
	public Collection<Menu> listarFuncionalidadesFilho() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" menu ");
		hql.append(" where ");
		hql.append(" menu.menuPai is not null");
		hql.append(" and menu.indicadorAlcada is true");
		hql.append(" order by menu.descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorMenu#criarFavoritos()
	 */
	@Override
	public EntidadeNegocio criarFavoritos() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Favoritos.BEAN_ID_FAVORITOS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorMenu#inserirFavorito(br.com.ggas.geral.Favoritos)
	 */
	@Override
	public String inserirFavorito(Favoritos favorito) throws GGASException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select count(*) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFavoritos().getSimpleName());
		hql.append(" favoritos ");
		hql.append(" where ");
		hql.append(" favoritos.usuario.chavePrimaria = :idUsuario");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idUsuario", favorito.getUsuario().getChavePrimaria());

		Long resultados = (Long) query.uniqueResult();

		if(resultados >= 10) {
			return Constantes.ERRO_LIMITE_FAVORITOS;
		} else {
			hql = new StringBuilder();
			hql.append("select count(*) ");
			hql.append(" from ");
			hql.append(getClasseEntidadeFavoritos().getSimpleName());
			hql.append(" favoritos ");
			hql.append(" where ");
			hql.append(" favoritos.menu.chavePrimaria = :menu");
			hql.append(" and favoritos.usuario.chavePrimaria = :idUsuario");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("menu", favorito.getMenu().getChavePrimaria());
			query.setLong("idUsuario", favorito.getUsuario().getChavePrimaria());

			resultados = (Long) query.uniqueResult();

			if(resultados > 0) {
				return Constantes.ERRO_LIMITE_FAVORITOS_DUPLICADO;
			} else {
				this.inserir(favorito);
				return null;
			}
		}
	}

}
