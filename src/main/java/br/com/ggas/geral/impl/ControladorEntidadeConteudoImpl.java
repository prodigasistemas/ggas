/*
 Copyright (C) <2011> GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS ? Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place ? Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * @author arthurtaborda
 */
class ControladorEntidadeConteudoImpl extends ControladorNegocioImpl implements ControladorEntidadeConteudo {
	
	private static final String TIPO_DE_CONVENIO = "Tipo de Convênio";

	/**
	 * @deprecated
	 */
	@Deprecated
	private static final String ARQUIVO_PROPRIEDADES_ENTIDADE_CLASSE = "entidadeClasse.properties";

	private static Map<String, String> mapaCodigoEntidadeClasse;

	private Map<String, Object> cacheEntidadeConteudo = new HashMap<String, Object>();
	
	static {
		inicializarPropriedades();
	}
	
	private ControladorConstanteSistema getControladorConstanteSistema() {
		
		return (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
	}
	
	
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#criarEntidadeClasse()
	 */
	@Override
	public EntidadeNegocio criarEntidadeClasse() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EntidadeClasse.BEAN_ID_ENTIDADE_CLASSE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#criarUnidadeClasse()
	 */
	@Override
	public EntidadeNegocio criarUnidadeClasse() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClasseUnidade.BEAN_ID_CLASSE_UNIDADE);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseEntidadeClasse() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeClasse.BEAN_ID_ENTIDADE_CLASSE);
	}

	public Class<?> getClasseClasseUnidade() {

		return ServiceLocator.getInstancia().getClassPorID(ClasseUnidade.BEAN_ID_CLASSE_UNIDADE);
	}
	
	/**
	 * Gets the classe entidade documento fiscal.
	 *
	 * @return the classe entidade documento fiscal
	 */
	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}


	/**
	 * Montar query entidade conteudo.
	 * 
	 * @param chaveEntidadeClasse
	 *            the chave entidade classe
	 * @return the query
	 */
	private Query montarQueryEntidadeConteudo(Long chaveEntidadeClasse) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidade WHERE ");
		hql.append(" entidade.entidadeClasse.chavePrimaria = :CHAVE_ENTIDADE_CLASSE ");
		hql.append(" and entidade.habilitado = :IS_HABILITADO ");
		hql.append(" order by entidade.descricao asc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_ENTIDADE_CLASSE", chaveEntidadeClasse);
		query.setBoolean("IS_HABILITADO", Boolean.TRUE);
		
		return query;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obter(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obter(Long chaveEntidade) {
		return obterEntidadeConteudo(chaveEntidade);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterEntidadeConteudo(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterEntidadeConteudo(Long chaveEntidade) {
		if (!getCacheEntidadeConteudo().containsKey(String.valueOf(chaveEntidade))) {
			Criteria criteria = createCriteria(getClasseEntidade(), "ent_cont");
			criteria.add(Restrictions.eq("ent_cont.chavePrimaria", chaveEntidade));
			criteria.setCacheable(true);
			criteria.setCacheMode(CacheMode.NORMAL);
			this.updateCache((EntidadeConteudo) criteria.uniqueResult());
		}

		return (EntidadeConteudo) getCacheEntidadeConteudo().getOrDefault(String.valueOf(chaveEntidade), null);
	}

	@Override
	public EntidadeConteudo obterEntidadeConteudoPorConstanteSistema(String codigo) 
					throws NegocioException {

		ConstanteSistema constante = getControladorConstanteSistema().obterConstantePorCodigo(codigo);
		
		if (constante != null && StringUtils.isNotBlank(constante.getValor()) && StringUtils.isNumeric(constante.getValor())) {
			Long chavePrimaria = Long.valueOf(constante.getValor());
			return this.obterEntidadeConteudo(chavePrimaria);
		}
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoGarantiaFinanceira()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoGarantiaFinanceira() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_GARANTIA_FINANCEIRA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoParada()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoParada() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_PARADA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaRegimeConsumo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaRegimeConsumo() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_REGIME_CONSUMO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaCriterioUso()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaCriterioUso() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_CRITERIO_USO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaFormaPagamento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaFormaPagamento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_FORMA_PAGAMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoFaturamento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoFaturamento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_FATURAMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaFaseReferencialVencimento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaFaseReferencialVencimento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_FASE_REFERENCIAL_VENCIMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaOpcaoFaseRefVencimento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaOpcaoFaseRefVencimento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_OPCAO_FASE_REFERENC_VENCIMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaOpcaoFaseRefVencimento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaOpcaoDias() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_OPCAO_DIAS)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaHoraInicialDia()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaHoraInicialDia() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_HORA_INICIAL_DIA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaItemFatura()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaItemFatura() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_ITEM_FATURA)).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo #obterListaEnderecoPadrao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaEnderecoPadrao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_ENDERECO_PADRAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaContratoCompra()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaContratoCompra() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_CONTRATO_COMPRA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaItemFaturaPrecoGas()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaItemFaturaPrecoGas() throws NegocioException {

		Query query = null;

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String codigoItemFatura = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_FILTRO_ITEM_FATURA_PRECO_GAS);

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidade WHERE ");
		hql.append(" entidade.entidadeClasse.chavePrimaria = :CHAVE_ENTIDADE_CLASSE ");

		if (!codigoItemFatura.isEmpty()) {
			hql.append(" and entidade.chavePrimaria <> :PARAMETRO_FILTRO_ITEM_FATURA_PRECO_GAS ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_ENTIDADE_CLASSE", getCodigoEntidadeClasse(EntidadeClasse.CODIGO_ITEM_FATURA));

		if (!codigoItemFatura.isEmpty()) {
			query.setParameter("PARAMETRO_FILTRO_ITEM_FATURA_PRECO_GAS", Long.valueOf(codigoItemFatura));
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaPeriodicidadeTOP()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaPeriodicidadeTOP() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_PERIODICIDADE_TOP)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaPeriodicidadeSOP()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaPeriodicidadeSOP() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_PERIODICIDADE_TOP)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaConsumoReferencial()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaConsumoReferencial() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_CONSUMO_REFERENCIAL)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaResponsabilidades()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaResponsabilidades() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_RESPONSABILIDADES)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaModalidadesUso()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTiposCombustiveis() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_COMBUSTIVEL)).list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaModalidadesUso()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaModalidadesUso() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_MODALIDADE_USO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarLocalAmostragemPCS()
	 */
	@Override
	public Collection<EntidadeConteudo> listarLocalAmostragemPCS() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_LOCAL_AMOSTRAGEM_PCS)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #
	 * permiteFaseReferencialVencimentoPorOpcaoVencimento
	 * (long)
	 */
	@Override
	public boolean permiteFaseReferencialVencimentoPorOpcaoVencimento(long chaveOpcaoVencimento) throws NegocioException {

		boolean retorno = true;
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoOpcaoDiaFixo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_VENCIMENTO_DIA_FIXO);

		String codigoOpcaoDiaFixoExtendido = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_VENCIMENTO_DIA_FIXO_ESTENDIDO);

		if (chaveOpcaoVencimento == Long.parseLong(codigoOpcaoDiaFixo)
						|| chaveOpcaoVencimento == Long.parseLong(codigoOpcaoDiaFixoExtendido)) {
			retorno = false;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarAmortizacoes()
	 */
	@Override
	public Collection<EntidadeConteudo> listarAmortizacoes() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_AMORTIZACAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterTipoConvenio(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterTipoConvenio(Long chaveEntidadeClasse) throws NegocioException {

		return this.obterEntidadeConteudo(chaveEntidadeClasse);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaAmortizacao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaAmortizacao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_AMORTIZACAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaMotivoCancelamento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaMotivoCancelamento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_MOTIVO_CANCELAMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaMotivoInclusao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaMotivoInclusao() throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idMotivoInclusaoFatura = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CLASSE_MOTIVO_INCLUSAO));
		return this.montarQueryEntidadeConteudo(idMotivoInclusaoFatura).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterPorCodigo(java.lang.String)
	 */
	@Override
	public EntidadeConteudo obterPorCodigo(String codigo, String codigoEntidadeClasse) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidade WHERE ");
		hql.append(" entidade.codigo = :codigo ");
		hql.append(" and entidade.entidadeClasse.chavePrimaria = :classe ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("codigo", codigo);
		query.setParameter("classe", this.getCodigoEntidadeClasse(codigoEntidadeClasse));
		return (EntidadeConteudo) query.uniqueResult();
	}
	

	/**
	 * Listar as situações de pagamento
	 * @param idGrupoFaturamento
	 * @return lista de situaçao pagamento
	 */
	public Collection<EntidadeConteudo> listarSituacaoPagamento() {
        //select * from entidade_conteudo where encl_cd = 25
		StringBuilder hql = new StringBuilder();
		
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidadeConteudo ");
		hql.append(" where entidadeConteudo.entidadeClasse.descricao = 'Situação de Pagamento' ");
		

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	
		return query.list();
	}
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoValor()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoValor() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		return this.montarQueryEntidadeConteudo(
						getCodigoEntidadeClasse(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_VALOR)))
						.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #
	 * obterEntidadeConteudoPadraoPorClasse(java.lang
	 * .Long)
	 */
	@Override
	public EntidadeConteudo obterEntidadeConteudoPadraoPorClasse(String chaveEntidadeClasse) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" entidade ");
		hql.append(" where ");
		hql.append(" entidade.entidadeClasse.chavePrimaria = :idClasse ");
		hql.append(" and entidade.indicadorPadrao = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idClasse", getCodigoEntidadeClasse(chaveEntidadeClasse));

		return (EntidadeConteudo) query.uniqueResult();
	}

	// Ecoelho

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoCalculo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoCalculo() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_CALCULO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoCalculo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaBaseApuracao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_BASE_APURACAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoCalculo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaUnidadeMonetaria() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_UNIDADES_MONETARIAS)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterItemFatura(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterItemFatura(Long chaveEntidadeClasse) throws NegocioException {

		return this.obterEntidadeConteudo(chaveEntidadeClasse);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterUnidadeMonetaria(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterUnidadeMonetaria(Long chaveEntidadeClasse) throws NegocioException {

		return this.obterEntidadeConteudo(chaveEntidadeClasse);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterTipoCalculo(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterTipoCalculo(Long chaveEntidadeClasse) throws NegocioException {

		return this.obterEntidadeConteudo(chaveEntidadeClasse);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterBaseApuracao(java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterBaseApuracao(Long chaveEntidadeClasse) throws NegocioException {

		return this.obterEntidadeConteudo(chaveEntidadeClasse);
	}

	// Ecoelho Fim

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterTipoOperacaoDocumentoFiscal()
	 */
	@Override
	public EntidadeConteudo obterTipoOperacaoDocumentoFiscal(Long chaveEntidadeClasse) throws NegocioException {

		return this.obterEntidadeConteudo(chaveEntidadeClasse);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaPeriodicidadeCobranca()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaPeriodicidadeCobranca() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_PERIODICIDADE_COBRANCA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoAtributo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoAtributo() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_ATRIBUTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoIntegracao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoIntegracao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_INTEGRACAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaPeriodicidadeJuros()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaPeriodicidadeJuros() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		return this.montarQueryEntidadeConteudo(
						getCodigoEntidadeClasse(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CLASSE_PERIODICIDADE_JUROS))).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaDiasAposInicioCobranca()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaDiasAposInicioCobranca() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_INICIO_COBRANCA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoValorTarifa()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoValorTarifa() throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_VALOR);
		Long tipoValor = constante.getClasse();
		return this.montarQueryEntidadeConteudo(tipoValor).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaDataReferenciaCambial()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaDataReferenciaCambial() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_DATA_REFERENCIA_CAMBIAL)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaDiaCotacao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaDiaCotacao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_DIA_COTACAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarTipoDevolucao()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoDevolucao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_DEVOLUCAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoAgrupamento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoAgrupamento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_AGRUPAMENTO)).list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarTipoMensagem()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoMensagem() throws NegocioException {

		if(!getCacheEntidadeConteudo().containsKey(EntidadeClasse.CODIGO_TIPO_MENSAGEM)) {
			getCacheEntidadeConteudo().put(EntidadeClasse.CODIGO_TIPO_MENSAGEM, this
					.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_MENSAGEM)).list());
		}
		return (Collection<EntidadeConteudo>) getCacheEntidadeConteudo().get(EntidadeClasse.CODIGO_TIPO_MENSAGEM);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarTipoAnexo()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoAnexo() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_ANEXO)).list();
	}

	/**
	 * Inicializar propriedades.
	 */
	private static void inicializarPropriedades() {

		try {
			Properties propriedades = new Properties();
			InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(ARQUIVO_PROPRIEDADES_ENTIDADE_CLASSE);
			propriedades.load(stream);

			mapaCodigoEntidadeClasse = new HashMap<String, String>();
			Iterator iterator = propriedades.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				mapaCodigoEntidadeClasse.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
			}
			stream.close();
		} catch (Exception ex) {
			LOG.error(ex.getStackTrace(), ex);
			Logger.getLogger("ERROR").debug(ex.getStackTrace());
		}
	}

	/**
	 * Gets the codigo entidade classe.
	 * 
	 * @param chaveEntidadeClasse
	 *            the chave entidade classe
	 * @return the codigo entidade classe
	 */
	private long getCodigoEntidadeClasse(String chaveEntidadeClasse) {

		long retorno = -1;

		if (!StringUtils.isEmpty(chaveEntidadeClasse) && mapaCodigoEntidadeClasse.containsKey(chaveEntidadeClasse)) {
			retorno = Long.parseLong(mapaCodigoEntidadeClasse.get(chaveEntidadeClasse).trim());
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaMotivoRefaturar()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaMotivoRefaturar() throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		Long idMotivoRefaturarFatura = Long.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_ENTIDADE_CLASSE_MOTIVO_REFATURAR));
		return this.montarQueryEntidadeConteudo(idMotivoRefaturarFatura).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarBasesApuracaoPenalidadeMaiorMenor()
	 */
	@Override
	public Collection<EntidadeConteudo> listarBasesApuracaoPenalidadeMaiorMenor() throws NegocioException {

		return this.montarQueryEntidadeConteudo(
						getCodigoEntidadeClasse(EntidadeClasse.CODIGO_BASE_APURACAO_PENALIDADE_RETIRADA_MAIOR_MENOR)).list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #obterListaTipoGarantiaFinanceira()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoMultaRecisoria() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_MULTA_RECISORIA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarTiposParametro()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoParametro() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_TIPO_PARAMETRO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarClasseUnidade()
	 */
	@Override
	public Collection<ClasseUnidade> listarClasseUnidade() throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append("select entidade");
		hql.append(" from ");
		hql.append(getClasseClasseUnidade().getSimpleName());
		hql.append(" entidade");
		hql.append(" order by entidade.descricao asc");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarEntidadeConteudo()
	 */
	@Override
	public Collection<EntidadeConteudo> listarEntidadeConteudo(String codigoEntidadeClasse) throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeConteudo.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.add(Restrictions.eq("entidadeClasse.chavePrimaria", this.getCodigoEntidadeClasse(codigoEntidadeClasse)));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarEntidadeConteudo(java.lang.Long)
	 */
	@Override
	public Collection<EntidadeConteudo> listarEntidadeConteudo(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeConteudo.class);
		criteria.add(Restrictions.eq("entidadeClasse.chavePrimaria", chavePrimaria));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarEntidadeClasse()
	 */
	@Override
	public Collection<EntidadeClasse> listarEntidadeClasse() throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeClasse.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaMotivoDevolucao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaMotivoDevolucao() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ConstanteSistema constanteSistemaMotivoDevolucao = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_MOTIVO_DEVOLUCAO);

		Long idMotivoDevolucaoFatura = Long.valueOf(constanteSistemaMotivoDevolucao.getValor());

		return this.montarQueryEntidadeConteudo(idMotivoDevolucaoFatura).list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarEntidadeClasse(java.lang.String)
	 */
	@Override
	public Collection<EntidadeClasse> listarEntidadeClasse(String codigoEntidadeClasse) throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeClasse.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.add(Restrictions.eq("entidadeClasse.chavePrimaria", this.getCodigoEntidadeClasse(codigoEntidadeClasse)));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterEntidadeConteudoPorChave(java.lang.Long, java.lang.Long)
	 */
	@Override
	public EntidadeConteudo obterEntidadeConteudoPorChave(Long chavePrimaria, Long codigoEntidadeClasse) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidade WHERE ");
		hql.append(" entidade.chavePrimaria = :id ");
		hql.append(" and entidade.entidadeClasse.chavePrimaria = :classe ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("id", chavePrimaria);
		query.setParameter("classe", codigoEntidadeClasse);
		return (EntidadeConteudo) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarEntidadeConteudoPorDescricaoEntidadeClasse(java.lang.String)
	 */
	@Override
	public Collection<EntidadeConteudo> listarEntidadeConteudoPorDescricaoEntidadeClasse(String descricaoEntidadeClasse)
					throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeConteudo.class);
		criteria.createCriteria("entidadeClasse", "entidadeClasse", Criteria.INNER_JOIN,
						Restrictions.eq("entidadeClasse.descricao", descricaoEntidadeClasse));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaEquipamento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaEquipamento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.EQUIPAMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTurno()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTurno() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.TURNO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarDiasVencimento(java.lang.Long)
	 */
	@Override
	public List<Long> listarDiasVencimento(Long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select entidade.descricao from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidade WHERE ");
		hql.append(" entidade.entidadeClasse.chavePrimaria = :classe ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("classe", chavePrimaria);

		List<String> lista = query.list();

		for (int i = 0; i < lista.size(); i++) {
			for (int j = 0; j < lista.get(i).length(); j++) {
				Character caractere = lista.get(i).charAt(j);
				if (!Character.isDigit(caractere)) {
					lista.remove(i);
					break;
				}
			}
		}
		List<Long> diasVencimento = new ArrayList<Long>();
		for (String dia : lista) {
			diasVencimento.add(Long.valueOf(dia));
		}
		Collections.sort(diasVencimento);
		return diasVencimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoModelo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoModelo() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idTipoModeloContrato = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_MODELO_CONTRATO);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idTipoModeloContrato)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaReferenciaQFParadaProgramada()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaReferenciaQFParadaProgramada() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idReferenciaQFParadaProgramada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_REFERENCIA_QF_PARADA_PROGRAMADA);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idReferenciaQFParadaProgramada)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoApuracao()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoApuracao() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.TIPO_APURACAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaPrecoCobranca()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaPrecoCobranca() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.PRECO_COBRANCA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaApuracaoParadaProgramada()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaApuracaoParadaProgramada() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.APURACAO_PARADA_PROGRAMADA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaApuracaoParadaNaoProgramada()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaApuracaoParadaNaoProgramada() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.APURACAO_PARADA_NAO_PROGRAMADA)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaApuracaoCasoFortuito()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaApuracaoCasoFortuito() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.APURACAO_CASO_FORTUITO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaApuracaoFalhaFornecimento()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaApuracaoFalhaFornecimento() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.APURACAO_FALHA_FORNECIMENTO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTipoAgrupamentoContrato()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoAgrupamentoContrato() throws NegocioException {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.TIPO_AGRUPAMENTO_CONTRATO)).list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaPeriodicidadeRetiradaMaiorMenor()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaPeriodicidadeRetiradaMaiorMenor() {

		return this.montarQueryEntidadeConteudo(getCodigoEntidadeClasse(EntidadeClasse.CODIGO_PERIODICIDADE_TOP)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaStatusImovel()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaStatusImovel() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idStatusImovel = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_STATUS);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idStatusImovel)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterStatusImovelPorCodigo(java.lang.Long)
	 */
	@Override
	public Collection<EntidadeConteudo> obterStatusImovelPorCodigo(Long chavePrimaria) {

		return this.montarQueryEntidadeConteudo(chavePrimaria).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaFornecedoresCombustiveis()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaFornecedoresCombustiveis() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idEntidadeClasse = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_FORNECEDORES_OUTROS_COMBUSTIVEIS);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idEntidadeClasse)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaAparelhosMaiorIndiceConsumo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaAparelhosMaiorIndiceConsumo() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idEntidadeClasse = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_APARELHOS_MAIOR_INDICE_CONSUMO);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idEntidadeClasse)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaTiposFontesEnergeticas()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTiposFontesEnergeticas() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idEntidadeClasse = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPOS_FONTES_ENERGETICAS);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idEntidadeClasse)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaEstadoRede()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaEstadoRede() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idEntidadeClasse = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ESTADO_REDE_GLP);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idEntidadeClasse)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterListaMotivoCcomplementoFatura()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaMotivoCcomplementoFatura() {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String idEntidadeClasse = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPOS_MOTIVO_COMPLEMENTO_FATURA);

		return this.montarQueryEntidadeConteudo(Long.valueOf(idEntidadeClasse)).list();
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.geral
	 * .ControladorEntidadeConteudo
	 * #obterListaTipoAplicacaoTributoAliquota()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoAplicacaoTributoAliquota() 
					throws NegocioException {

		Long chaveEntidade = getControladorConstanteSistema()
						.obterConstanteTipoAplicacaoTributoAliquota().getValorLong();

		return this.montarQueryEntidadeConteudo(chaveEntidade).list();
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.geral
	 * .ControladorEntidadeConteudo
	 * #obterListaTipoAplicacaoReducaoBaseCalculo()
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoAplicacaoReducaoBaseCalculo() 
					throws NegocioException {

		Long chaveEntidade = getControladorConstanteSistema()
						.obterConstanteTipoAplicacaoReducaoBaseCalculo().getValorLong();

		return this.montarQueryEntidadeConteudo(chaveEntidade).list();
	}
	
	@Override
	public EntidadeConteudo obterEntidadeConteudoTipoAplicacaoTributoAliquotaPercentual() 
					throws NegocioException {
		
		Long chaveEntidade = getControladorConstanteSistema()
						.obterConstanteTipoAplicacaoTributoAliquotaPercentual().getValorLong();
		
		return obterEntidadeConteudo(chaveEntidade);
		
	}

	public Map<String, Object> getCacheEntidadeConteudo() {

		return cacheEntidadeConteudo;
	}

	
	@Override
	public void updateCache(EntidadeConteudo entidadeConteudo) {
		if(entidadeConteudo != null) {
			this.cacheEntidadeConteudo.put(String.valueOf(entidadeConteudo.getChavePrimaria()), entidadeConteudo);
		}
	}

	@Override
	public Collection<EntidadeConteudo> obterListaTipoVigenciaSubstituicaoTributaria() throws NegocioException {
		Long chaveEntidade = getControladorConstanteSistema()
				.obterConstanteTipoVigenciaSubstituicaoTributaria().getValorLong();

		return this.montarQueryEntidadeConteudo(chaveEntidade).list();
	}



	@Override
	public Collection<EntidadeConteudo> listarTiposDeConvenio() throws NegocioException {
		return this.listarEntidadeConteudoPorDescricaoEntidadeClasse(TIPO_DE_CONVENIO);
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.ControladorEntidadeConteudo
	 * #listarFormaCobranca()
	 */
	@Override
	public Collection<EntidadeConteudo> listarFormaCobranca() throws NegocioException {
		Collection<EntidadeConteudo> convenios = listarTiposDeConvenio();
		convenios.remove(obter(Long.valueOf(getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(
						Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO))));
		return convenios;
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarStatusNFEDocumentoFiscal()
	 */
	@Override
	public Collection<EntidadeConteudo> listarStatusNFEDocumentoFiscal() throws NegocioException {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select entidadeConteudo from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidadeConteudo");
		hql.append(" inner join entidadeConteudo.entidadeClasse entidadeClasse");
		hql.append(" where entidadeClasse.descricao like 'Status NFe'");
		hql.append(" and entidadeConteudo.chavePrimaria in (select distinct documentoFiscal.statusNfe from ");
		hql.append(getClasseEntidadeDocumentoFiscal().getSimpleName());
		hql.append(" documentoFiscal) order by entidadeConteudo.descricao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		return query.list();


	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#obterEntidadeConteudoPorChave(java.String)
	 */
	@Override
	public EntidadeConteudo obterEntidadeConteudoLikeDescricao(String descricao) throws NegocioException {

		Criteria criteria = this.createCriteria(EntidadeConteudo.class);
		criteria.add(Restrictions.ilike("descricao",descricao));
		
		return (EntidadeConteudo) criteria.uniqueResult();

	}
	
	/*
	 * Listar Tipo de Consumo de Faturamento
	 */
	@Override
	public Collection<EntidadeConteudo> obterListaTipoConsumoFaturamento() throws NegocioException {
		
		ConstanteSistema constante = getControladorConstanteSistema().obterConstantePorCodigo(Constantes.TIPO_CONSUMO_FATURAMENTO);
		
		return this.montarQueryEntidadeConteudo(constante.getValorLong()).list();
	}
	
	
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.ControladorEntidadeConteudo#listarEntidadeConteudoPorDescricaoEntidadeClasse(java.lang.String)
	 */
	@Override
	public EntidadeConteudo listarEntidadeConteudoPorEntidadeClasseEDescricao(String descricaoEntidadeClasse, String descricao)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select entidadeConteudo from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidadeConteudo");
		hql.append(" inner join entidadeConteudo.entidadeClasse entidadeClasse");
		hql.append(" where entidadeClasse.descricao like :descricaoEntidadeClasse");
		hql.append(" and entidadeConteudo.descricao like :descricao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("descricaoEntidadeClasse", descricaoEntidadeClasse);
		query.setParameter("descricao", descricao);
		
		return (EntidadeConteudo) query.uniqueResult();

	}
	
	@Override
	public EntidadeConteudo obterEntidadeConteudoPorChaveEntidadeClasse(Long chaveEntidadeClasse) throws NegocioException {
		StringBuilder hql = new StringBuilder();
		hql.append(" select entidadeConteudo from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidadeConteudo ");
		hql.append(" where entidadeConteudo.entidadeClasse.chavePrimaria =:chaveEntidadeClasse");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("chaveEntidadeClasse", chaveEntidadeClasse);
		
		return (EntidadeConteudo) query.uniqueResult();
	}
	
	@Override
	public Collection<EntidadeConteudo> listaEntidadeConteudoPorDescricaoEntidadeClasse(String descricaoEntidadeClasse){
		StringBuilder hql = new StringBuilder();
		hql.append(" select entidadeConteudo from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" entidadeConteudo ");
		hql.append(" where entidadeConteudo.entidadeClasse.descricao =:descricaoEntidadeClasse");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("descricaoEntidadeClasse", descricaoEntidadeClasse);
		
		return query.list();
	}

}
