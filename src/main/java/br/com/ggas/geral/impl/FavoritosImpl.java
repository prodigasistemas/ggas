/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.impl;

import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe Favoritos.
 * 
 */
public class FavoritosImpl extends EntidadeNegocioImpl implements Favoritos {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2354701977795321804L;

	private String nome;

	private String link;

	private Usuario usuario;

	private Menu menu;

	@Override
	public String getNome() {

		return nome;
	}

	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getLink() {

		return link;
	}

	@Override
	public void setLink(String link) {

		this.link = link;
	}

	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	@Override
	public Menu getMenu() {

		return menu;
	}

	@Override
	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof EntidadeNegocioImpl)) {
			return false;
		}
		if(this.getChavePrimaria() > 0) {
			EntidadeNegocioImpl that = (EntidadeNegocioImpl) obj;
			return new EqualsBuilder().append(this.getMenu().getChavePrimaria(), that.getChavePrimaria()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.getChavePrimaria() > 0) {
			return new HashCodeBuilder(3, 5).append(this.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
