/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.impl;

import br.com.ggas.geral.Mes;
import br.com.ggas.geral.dominio.impl.EntidadeDominioImpl;

/**
 * 
 * 
 */
class MesImpl extends EntidadeDominioImpl implements Mes {

	/**
     * 
     */
	private static final long serialVersionUID = -8947999066322804750L;

	/**
	 * Instantiates a new mes impl.
	 */
	public MesImpl() {

		super();
	}

	/**
	 * Instantiates a new mes impl.
	 * 
	 * @param codigo
	 *            the codigo
	 * @param descricao
	 *            the descricao
	 */
	public MesImpl(int codigo, String descricao) {

		super();
		this.setCodigo(codigo);
		// by gmatos
		// on 24/07/10
		// 13:45
	}

	@Override
	public void setCodigo(int codigo) {

		super.setCodigo(codigo);

		switch(codigo) {
			case JANEIRO :
				super.setDescricao(DESCRICAO_JANEIRO);
				break;
			case FEVEREIRO :
				super.setDescricao(DESCRICAO_FEVEREIRO);
				break;
			case MARCO :
				super.setDescricao(DESCRICAO_MARCO);
				break;
			case ABRIL :
				super.setDescricao(DESCRICAO_ABRIL);
				break;
			case MAIO :
				super.setDescricao(DESCRICAO_MAIO);
				break;
			case JUNHO :
				super.setDescricao(DESCRICAO_JUNHO);
				break;
			case JULHO :
				super.setDescricao(DESCRICAO_JULHO);
				break;
			case AGOSTO :
				super.setDescricao(DESCRICAO_AGOSTO);
				break;
			case SETEMBRO :
				super.setDescricao(DESCRICAO_SETEMBRO);
				break;
			case OUTUBRO :
				super.setDescricao(DESCRICAO_OUTUBRO);
				break;
			case NOVEMBRO :
				super.setDescricao(DESCRICAO_NOVEMBRO);
				break;
			case DEZEMBRO :
				super.setDescricao(DESCRICAO_DEZEMBRO);
				break;
			default :
				break;
		}
	}
}
