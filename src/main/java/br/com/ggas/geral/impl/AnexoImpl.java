package br.com.ggas.geral.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe abstrata utilizada para reuso de código.
 * 
 * @class AnexoImpl Classe abstrata utilizada para reuso de código.
 */
public abstract class AnexoImpl extends EntidadeNegocioImpl {



	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 5269437553030103198L;

	/**
	 * Limite campo
	 */
	private static final int LIMITE_CAMPO = 2;

	/**
	 * Bytes do documento anexo
	 */
	protected byte[] documentoAnexo;

	/**
	 * Descrição do anexo
	 */
	protected String descricaoAnexo;

	/**
	 * Nome do arquivo anexo
	 */
	protected String nomeArquivo;

	/**
	 * @return descrição do anexo
	 */
	public String getDescricaoAnexo() {
		return descricaoAnexo;
	}

	/**
	 * @param descricaoAnexo
	 * 			a descrição do arquivo anexo
	 */
	public void setDescricaoAnexo(String descricaoAnexo) {

		this.descricaoAnexo = descricaoAnexo;
	}

	/**
	 * @return documentoAnexoTmp
	 * 				os bytes do documento anexo
	 */
	public byte[] getDocumentoAnexo() {
		byte[] documentoAnexoTemp = null;
		if (documentoAnexo != null) {
			documentoAnexoTemp = documentoAnexo.clone();
		}
		return documentoAnexoTemp;
	}

	/**
	 * @param documentoAnexo
	 * 			os bytes do arquivo anexo
	 */
	public void setDocumentoAnexo(byte[] documentoAnexoParam) {
		if (documentoAnexoParam != null) {
			this.documentoAnexo = documentoAnexoParam.clone();
		} else {
			this.documentoAnexo = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		final Map<String, Object> erros = new HashMap<String, Object>();
		final StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (descricaoAnexo == null || descricaoAnexo.isEmpty()) {
			stringBuilder.append("Descrição Documento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (documentoAnexo == null) {
			stringBuilder.append("Incluir Documento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * 
	 * @return nomeArquivo
	 * 			nome do arquivo anexo
	 */
	public String getNomeArquivo() {
		return nomeArquivo;
	}

	/**
	 * 
	 * @param nomeArquivo
	 * 			nome do arquivo anexo
	 */
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

}
