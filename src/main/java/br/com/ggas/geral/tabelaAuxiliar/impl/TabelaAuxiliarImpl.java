/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.tabelaAuxiliar.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.geografico.impl.Pais;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.util.NumeroUtil;
/**
 * 
 * Classe responsável pela implementação de métodos 
 * para atributos de TabelaAuxiliar
 *
 */
public class TabelaAuxiliarImpl extends EntidadeNegocioImpl implements TabelaAuxiliar {

	private static final long serialVersionUID = 1L;

	private String descricao;

	private String descricaoAbreviada;

	private boolean indicadorPadrao;

	/*
	 * O atributo abaixo é usado para a tabela de
	 * entidade conteudo e para a tabela de
	 * atividade
	 * economica
	 * em entidade conteudo a coluna é
	 * ENCO_NR_CODIGO
	 * e em atividade economica a coluna é
	 * ATEC_CD_CNAE
	 */
	private String codigo;

	private EntidadeClasse entidadeClasse;

	private ClasseUnidade classeUnidade;

	private Long codigoOriginal;

	private Regiao regiao;

	private String tipo;

	private Integer nivel;

	/*
	 * se o indicadorServico for false
	 * então o tributo será por produto
	 */
	private boolean indicadorServico;

	private boolean indicadorProduto;

	private EsferaPoder esferaPoder;

	private Tributo tributo;

	private Date dataVigencia;
	
	private EntidadeConteudo tipoAplicacaoTributoAliquota;
	
	private BigDecimal valorAliquota;
	
	private boolean indicadorReducaoBaseCalculo;
	
	private EntidadeConteudo tipoAplicacaoReducaoBaseCalculo;
	
	private BigDecimal valorReducaoBaseCalculo;
	
	private Municipio municipio;

	private Date data;

	private String dataFormatada;

	private boolean indicadorMunicipal;

	private boolean indicadorTipo;

	private String descricaoDataVigencia;

	private String sigla;

	private Long unidadeFederativaIBGE;

	private Pais pais;

	private Segmento segmento;

	private Periodicidade periodicidade;

	private UnidadeFederacao unidadeFederacao;

	private Microrregiao microrregiao;

	private String cepInicio;

	private String cepFim;

	private Integer ddd;

	private TipoPessoa tipoPessoa;

	private CityGate cityGate;

	private Endereco endereco;

	private Localidade localidade;

	private Integer menorFaixa;

	private Integer maiorFaixa;

	private GerenciaRegional gerenciaRegional;

	private Cliente cliente;

	private int diaVencimento;

	private Integer anoMesReferencia;

	private Integer numeroCiclo;

	private boolean principal;

	private Integer quantidadeDias;

	private Integer quantidadeCiclo;

	private Integer numeroMaximoDiasCiclo;

	private Integer numeroMinimoDiasCiclo;

	private TipoLeitura tipoLeitura;

	private RedeIndicador redeIndicador;

	private Boolean consideraMesCivil;

	private Boolean indicadorDtVencimentoUltimoDiaCiclo;

	private Boolean indicadorContinuidadeCascataTarifa;
	
	private Boolean indicadorVencimentoIgualFatura;
	
	/**
	 * Instantiates a new tabela auxiliar impl.
	 */
	public TabelaAuxiliarImpl() {

	}

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public String getDescricaoUpperCase() {

		if (descricao != null) {
			return descricao.toUpperCase();
		} else {
			return descricao;
		}
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;

	}

	@Override
	public boolean isIndicadorPadrao() {

		return indicadorPadrao;
	}

	@Override
	public void setIndicadorPadrao(boolean indicadorPadrao) {

		this.indicadorPadrao = indicadorPadrao;
	}

	@Override
	public EntidadeClasse getEntidadeClasse() {

		return entidadeClasse;
	}

	@Override
	public void setEntidadeClasse(EntidadeClasse entidadeClasse) {

		this.entidadeClasse = entidadeClasse;
	}

	@Override
	public ClasseUnidade getClasseUnidade() {

		return classeUnidade;
	}

	@Override
	public void setClasseUnidade(ClasseUnidade classeUnidade) {

		this.classeUnidade = classeUnidade;
	}

	@Override
	public String getCodigo() {

		return codigo;
	}

	@Override
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	@Override
	public Long getCodigoOriginal() {

		return codigoOriginal;
	}

	@Override
	public void setCodigoOriginal(Long codigoOriginal) {

		this.codigoOriginal = codigoOriginal;
	}

	@Override
	public Regiao getRegiao() {

		return regiao;
	}

	@Override
	public void setRegiao(Regiao regiao) {

		this.regiao = regiao;
	}

	@Override
	public String getTipo() {

		return tipo;
	}

	@Override
	public void setTipo(String tipo) {

		this.tipo = tipo;
	}

	@Override
	public Integer getNivel() {

		return nivel;
	}

	@Override
	public void setNivel(Integer nivel) {

		this.nivel = nivel;
	}

	@Override
	public boolean isIndicadorServico() {

		return indicadorServico;
	}

	@Override
	public void setIndicadorServico(boolean indicadorServico) {

		this.indicadorServico = indicadorServico;
	}

	@Override
	public boolean isIndicadorProduto() {

		return indicadorProduto;
	}

	@Override
	public void setIndicadorProduto(boolean indicadorProduto) {

		this.indicadorProduto = indicadorProduto;
	}

	@Override
	public EsferaPoder getEsferaPoder() {

		return esferaPoder;
	}

	@Override
	public void setEsferaPoder(EsferaPoder esferaPoder) {

		this.esferaPoder = esferaPoder;
	}

	@Override
	public Tributo getTributo() {

		return tributo;
	}

	@Override
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	@Override
	public Date getDataVigencia() {
		Date retorno = null;
		if (dataVigencia != null) {
			retorno = (Date) dataVigencia.clone();
		}
		return retorno;
	}

	@Override
	public void setDataVigencia(Date dataVigencia) {
		if (dataVigencia != null) {
			this.dataVigencia = (Date) dataVigencia.clone();
		} else {
			this.dataVigencia = null;
		}
	}

	@Override
	public BigDecimal getValorAliquota() {

		return valorAliquota;
	}
	
	@Override
	public BigDecimal getValorPercentualAliquota() {

		return NumeroUtil.converterValorAbsolutoParaPercentual(valorAliquota);
	}

	@Override
	public void setValorAliquota(BigDecimal valorAliquota) {

		this.valorAliquota = valorAliquota;
	}

	@Override
	public Municipio getMunicipio() {

		return this.municipio;
	}

	@Override
	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	@Override
	public Date getData() {
		Date dataTmp = null;
		if(this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	@Override
	public void setData(Date data) {
		if (data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	@Override
	public boolean isIndicadorMunicipal() {

		return indicadorMunicipal;
	}

	@Override
	public void setIndicadorMunicipal(boolean indicadorMunicipal) {

		this.indicadorMunicipal = indicadorMunicipal;
	}

	@Override
	public boolean isIndicadorTipo() {

		return indicadorTipo;
	}

	@Override
	public void setIndicadorTipo(boolean indicadorTipo) {

		this.indicadorTipo = indicadorTipo;
	}

	@Override
	public String getDescricaoDataVigencia() {

		return descricaoDataVigencia;
	}

	@Override
	public void setDescricaoDataVigencia(String descricaoDataVigencia) {

		this.descricaoDataVigencia = descricaoDataVigencia;
	}

	@Override
	public String getSigla() {

		return sigla;
	}

	@Override
	public void setSigla(String sigla) {

		this.sigla = sigla;
	}

	@Override
	public Long getUnidadeFederativaIBGE() {

		return unidadeFederativaIBGE;
	}

	@Override
	public void setUnidadeFederativaIBGE(Long unidadeFederativaIBGE) {

		this.unidadeFederativaIBGE = unidadeFederativaIBGE;
	}

	@Override
	public void setPais(Pais pais) {

		this.pais = pais;
	}

	@Override
	public Pais getPais() {

		return pais;
	}

	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	@Override
	public Periodicidade getPeriodicidade() {

		return periodicidade;
	}

	@Override
	public void setPeriodicidade(Periodicidade periodicidade) {

		this.periodicidade = periodicidade;
	}

	@Override
	public UnidadeFederacao getUnidadeFederacao() {

		return unidadeFederacao;
	}

	@Override
	public void setUnidadeFederacao(UnidadeFederacao unidadeFederacao) {

		this.unidadeFederacao = unidadeFederacao;
	}

	@Override
	public Microrregiao getMicrorregiao() {

		return microrregiao;
	}

	@Override
	public void setMicrorregiao(Microrregiao microrregiao) {

		this.microrregiao = microrregiao;
	}

	@Override
	public String getCepInicio() {

		return cepInicio;
	}

	@Override
	public void setCepInicio(String cepInicio) {

		this.cepInicio = cepInicio;
	}

	@Override
	public String getCepFim() {

		return cepFim;
	}

	@Override
	public void setCepFim(String cepFim) {

		this.cepFim = cepFim;
	}

	@Override
	public Integer getDdd() {

		return ddd;
	}

	@Override
	public void setDdd(Integer ddd) {

		this.ddd = ddd;
	}

	@Override
	public TipoPessoa getTipoPessoa() {

		return tipoPessoa;
	}

	@Override
	public void setTipoPessoa(TipoPessoa tipoPessoa) {

		this.tipoPessoa = tipoPessoa;
	}

	@Override
	public EntidadeConteudo getTipoAplicacaoTributoAliquota() {
	
		return tipoAplicacaoTributoAliquota;
	}
	
	@Override
	public void setTipoAplicacaoTributoAliquota(EntidadeConteudo tipoAplicacaoTributoAliquota) {
	
		this.tipoAplicacaoTributoAliquota = tipoAplicacaoTributoAliquota;
	}

	@Override
	public CityGate getCityGate() {

		return cityGate;
	}

	@Override
	public void setCityGate(CityGate cityGate) {

		this.cityGate = cityGate;
	}

	@Override
	public Endereco getEndereco() {

		return endereco;
	}

	@Override
	public void setEndereco(Endereco endereco) {

		this.endereco = endereco;
	}

	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	@Override
	public String getDataFormatada() {

		return dataFormatada;
	}

	@Override
	public void setDataFormatada(String dataFormatada) {

		this.dataFormatada = dataFormatada;
	}

	@Override
	public Integer getMenorFaixa() {

		return menorFaixa;
	}

	@Override
	public void setMenorFaixa(Integer menorFaixa) {

		this.menorFaixa = menorFaixa;
	}

	@Override
	public Integer getMaiorFaixa() {

		return maiorFaixa;
	}

	@Override
	public void setMaiorFaixa(Integer maiorFaixa) {

		this.maiorFaixa = maiorFaixa;
	}

	@Override
	public GerenciaRegional getGerenciaRegional() {

		return gerenciaRegional;
	}

	@Override
	public void setGerenciaRegional(GerenciaRegional gerenciaRegional) {

		this.gerenciaRegional = gerenciaRegional;
	}

	@Override
	public Cliente getCliente() {

		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	@Override
	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	@Override
	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	@Override
	public int getDiaVencimento() {

		return diaVencimento;
	}

	@Override
	public void setDiaVencimento(int diaVencimento) {

		this.diaVencimento = diaVencimento;
	}

	@Override
	public boolean isPrincipal() {

		return principal;
	}

	@Override
	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}

	@Override
	public Integer getQuantidadeDias() {

		return quantidadeDias;
	}

	@Override
	public void setQuantidadeDias(Integer quantidadeDias) {

		this.quantidadeDias = quantidadeDias;
	}

	@Override
	public Integer getQuantidadeCiclo() {

		return quantidadeCiclo;
	}

	@Override
	public void setQuantidadeCiclo(Integer quantidadeCiclo) {

		this.quantidadeCiclo = quantidadeCiclo;
	}

	@Override
	public Integer getNumeroMaximoDiasCiclo() {

		return numeroMaximoDiasCiclo;
	}

	@Override
	public void setNumeroMaximoDiasCiclo(Integer numeroMaximoDiasCiclo) {

		this.numeroMaximoDiasCiclo = numeroMaximoDiasCiclo;
	}

	@Override
	public Integer getNumeroMinimoDiasCiclo() {

		return numeroMinimoDiasCiclo;
	}

	@Override
	public void setNumeroMinimoDiasCiclo(Integer numeroMinimoDiasCiclo) {

		this.numeroMinimoDiasCiclo = numeroMinimoDiasCiclo;
	}

	@Override
	public TipoLeitura getTipoLeitura() {

		return tipoLeitura;
	}

	@Override
	public void setTipoLeitura(TipoLeitura tipoLeitura) {

		this.tipoLeitura = tipoLeitura;
	}

	@Override
	public RedeIndicador getRedeIndicador() {

		return redeIndicador;
	}

	@Override
	public void setRedeIndicador(RedeIndicador redeIndicador) {

		this.redeIndicador = redeIndicador;
	}

	@Override
	public Boolean getConsideraMesCivil() {

		return consideraMesCivil;
	}

	@Override
	public void setConsideraMesCivil(Boolean consideraMesCivil) {

		this.consideraMesCivil = consideraMesCivil;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public Boolean getIndicadorDtVencimentoUltimoDiaCiclo() {

		return indicadorDtVencimentoUltimoDiaCiclo;
	}

	@Override
	public void setIndicadorDtVencimentoUltimoDiaCiclo(Boolean indicadorDtVencimentoUltimoDiaCiclo) {

		this.indicadorDtVencimentoUltimoDiaCiclo = indicadorDtVencimentoUltimoDiaCiclo;
	}

	
	/**
	 * @return the indicadorReducaoBaseCalculo
	 */
	@Override
	public boolean isIndicadorReducaoBaseCalculo() {
	
		return indicadorReducaoBaseCalculo;
	}

	
	/**
	 * @param indicadorReducaoBaseCalculo the indicadorReducaoBaseCalculo to set
	 */
	@Override
	public void setIndicadorReducaoBaseCalculo(boolean indicadorReducaoBaseCalculo) {
	
		this.indicadorReducaoBaseCalculo = indicadorReducaoBaseCalculo;
	}

	
	/**
	 * @return the tipoAplicacaoReducaoBaseCalculo
	 */
	@Override
	public EntidadeConteudo getTipoAplicacaoReducaoBaseCalculo() {
	
		return tipoAplicacaoReducaoBaseCalculo;
	}

	
	/**
	 * @param tipoAplicacaoReducaoBaseCalculo the tipoAplicacaoReducaoBaseCalculo to set
	 */
	@Override
	public void setTipoAplicacaoReducaoBaseCalculo(EntidadeConteudo tipoAplicacaoReducaoBaseCalculo) {
	
		this.tipoAplicacaoReducaoBaseCalculo = tipoAplicacaoReducaoBaseCalculo;
	}

	
	/**
	 * @return the valorReducaoBaseCalculo
	 */
	@Override
	public BigDecimal getValorReducaoBaseCalculo() {
	
		return valorReducaoBaseCalculo;
	}

	
	/**
	 * @param valorReducaoBaseCalculo the valorReducaoBaseCalculo to set
	 */
	@Override
	public void setValorReducaoBaseCalculo(BigDecimal valorReducaoBaseCalculo) {
	
		this.valorReducaoBaseCalculo = valorReducaoBaseCalculo;
	}
	
	/**
	 * @return the indicadorContinuidadeCascataTarifa
	 */
	@Override
	public Boolean getIndicadorContinuidadeCascataTarifa() {
		return indicadorContinuidadeCascataTarifa;
	}

	/**
	 * @param indicadorContinuidadeCascataTarifa the indicadorContinuidadeCascataTarifa to set
	 */
	@Override
	public void setIndicadorContinuidadeCascataTarifa(Boolean indicadorContinuidadeCascataTarifa) {
		this.indicadorContinuidadeCascataTarifa = indicadorContinuidadeCascataTarifa;
	}

	/**
	 * @return the indicadorVencimentoIgualFatura
	 */
	@Override
	public Boolean getIndicadorVencimentoIgualFatura() {
		return indicadorVencimentoIgualFatura;
	}

	/**
	 * @param indicadorVencimentoIgualFatura the indicadorVencimentoIgualFatura to set
	 */
	@Override
	public void setIndicadorVencimentoIgualFatura(Boolean indicadorVencimentoIgualFatura) {
		this.indicadorVencimentoIgualFatura = indicadorVencimentoIgualFatura;
	}

}
