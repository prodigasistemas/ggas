/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.tabelaAuxiliar;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.geografico.impl.Pais;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;

/**
 * The Interface TabelaAuxiliar.
 */
public interface TabelaAuxiliar extends EntidadeNegocio {

	/** The bean id tabela auxiliar. */
	String BEAN_ID_TABELA_AUXILIAR = "tabelaAuxiliar";

	String ATRIBUTO_DESCRICAO = "descricao";
	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * Gets the descricao upper case.
	 *
	 * @return the descricao upper case
	 */
	String getDescricaoUpperCase();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao the new descricao
	 */
	void setDescricao(String descricao);

	/**
	 * Gets the descricao abreviada.
	 *
	 * @return the descricao abreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * Sets the descricao abreviada.
	 *
	 * @param descricaoAbreviada the new descricao abreviada
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * Checks if is indicador padrao.
	 *
	 * @return true, if is indicador padrao
	 */
	boolean isIndicadorPadrao();

	/**
	 * Sets the indicador padrao.
	 *
	 * @param indicadorPadrao the new indicador padrao
	 */
	void setIndicadorPadrao(boolean indicadorPadrao);

	/**
	 * Gets the entidade classe.
	 *
	 * @return the entidade classe
	 */
	EntidadeClasse getEntidadeClasse();

	/**
	 * Gets the codigo.
	 *
	 * @return the codigo
	 */
	public String getCodigo();

	/**
	 * Sets the codigo.
	 *
	 * @param codigo the new codigo
	 */
	public void setCodigo(String codigo);

	/**
	 * Sets the entidade classe.
	 *
	 * @param entidadeClasse the new entidade classe
	 */
	void setEntidadeClasse(EntidadeClasse entidadeClasse);

	/**
	 * Gets the classe unidade.
	 *
	 * @return the classe unidade
	 */
	ClasseUnidade getClasseUnidade();

	/**
	 * Sets the classe unidade.
	 *
	 * @param classeunidade the new classe unidade
	 */
	void setClasseUnidade(ClasseUnidade classeunidade);

	/**
	 * Gets the codigo original.
	 *
	 * @return the codigo original
	 */
	Long getCodigoOriginal();

	/**
	 * Sets the codigo original.
	 *
	 * @param codigoOriginal the new codigo original
	 */
	void setCodigoOriginal(Long codigoOriginal);

	/**
	 * Gets the regiao.
	 *
	 * @return the regiao
	 */
	Regiao getRegiao();

	/**
	 * Sets the regiao.
	 *
	 * @param regiao the new regiao
	 */
	void setRegiao(Regiao regiao);

	/**
	 * Gets the tipo.
	 *
	 * @return the tipo
	 */
	String getTipo();

	/**
	 * Sets the tipo.
	 *
	 * @param tipo the new tipo
	 */
	void setTipo(String tipo);

	/**
	 * Gets the nivel.
	 *
	 * @return the nivel
	 */
	Integer getNivel();

	/**
	 * Sets the nivel.
	 *
	 * @param nivel the new nivel
	 */
	void setNivel(Integer nivel);

	/**
	 * Checks if is indicador servico.
	 *
	 * @return true, if is indicador servico
	 */
	boolean isIndicadorServico();

	/**
	 * Sets the indicador servico.
	 *
	 * @param indicadorServico the new indicador servico
	 */
	void setIndicadorServico(boolean indicadorServico);

	/**
	 * Checks if is indicador produto.
	 *
	 * @return true, if is indicador produto
	 */
	boolean isIndicadorProduto();

	/**
	 * Sets the indicador produto.
	 *
	 * @param indicadorProduto the new indicador produto
	 */
	void setIndicadorProduto(boolean indicadorProduto);

	/**
	 * Gets the esfera poder.
	 *
	 * @return the esfera poder
	 */
	EsferaPoder getEsferaPoder();

	/**
	 * Sets the esfera poder.
	 *
	 * @param esferaPoder the new esfera poder
	 */
	void setEsferaPoder(EsferaPoder esferaPoder);

	/**
	 * Gets the tributo.
	 *
	 * @return the tributo
	 */
	Tributo getTributo();

	/**
	 * Sets the tributo.
	 *
	 * @param tributo the new tributo
	 */
	void setTributo(Tributo tributo);

	/**
	 * Gets the data vigencia.
	 *
	 * @return the data vigencia
	 */
	Date getDataVigencia();

	/**
	 * Sets the data vigencia.
	 *
	 * @param dataVigencia the new data vigencia
	 */
	void setDataVigencia(Date dataVigencia);

	/**
	 * Gets the valor aliquota.
	 *
	 * @return the valor aliquota
	 */
	BigDecimal getValorAliquota();

	/**
	 * Sets the valor aliquota.
	 *
	 * @param valorAliquota the new valor aliquota
	 */
	void setValorAliquota(BigDecimal valorAliquota);

	/**
	 * Gets the municipio.
	 *
	 * @return the municipio
	 */
	Municipio getMunicipio();

	/**
	 * Sets the municipio.
	 *
	 * @param municipio the new municipio
	 */
	void setMunicipio(Municipio municipio);

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	Date getData();

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	void setData(Date data);

	/**
	 * Gets the data formatada.
	 *
	 * @return the data formatada
	 */
	String getDataFormatada();

	/**
	 * Sets the data formatada.
	 *
	 * @param dataFormatada the new data formatada
	 */
	void setDataFormatada(String dataFormatada);

	/**
	 * Checks if is indicador municipal.
	 *
	 * @return true, if is indicador municipal
	 */
	boolean isIndicadorMunicipal();

	/**
	 * Sets the indicador municipal.
	 *
	 * @param indicadorFeriadoMunicipal the new indicador municipal
	 */
	void setIndicadorMunicipal(boolean indicadorFeriadoMunicipal);

	/**
	 * Checks if is indicador tipo.
	 *
	 * @return true, if is indicador tipo
	 */
	boolean isIndicadorTipo();

	/**
	 * Sets the indicador tipo.
	 *
	 * @param indicadorFeriadoMovel the new indicador tipo
	 */
	void setIndicadorTipo(boolean indicadorFeriadoMovel);

	/**
	 * Gets the descricao data vigencia.
	 *
	 * @return the descricao data vigencia
	 */
	String getDescricaoDataVigencia();

	/**
	 * Sets the descricao data vigencia.
	 *
	 * @param descricaoDataVigencia the new descricao data vigencia
	 */
	void setDescricaoDataVigencia(String descricaoDataVigencia);

	/**
	 * Gets the sigla.
	 *
	 * @return the sigla
	 */
	String getSigla();

	/**
	 * Sets the sigla.
	 *
	 * @param sigla the new sigla
	 */
	void setSigla(String sigla);

	/**
	 * Gets the unidade federativa ibge.
	 *
	 * @return the unidade federativa ibge
	 */
	Long getUnidadeFederativaIBGE();

	/**
	 * Sets the unidade federativa ibge.
	 *
	 * @param unidadeFederativaIBGE the new unidade federativa ibge
	 */
	void setUnidadeFederativaIBGE(Long unidadeFederativaIBGE);

	/**
	 * Sets the pais.
	 *
	 * @param pais the new pais
	 */
	void setPais(Pais pais);

	/**
	 * Gets the pais.
	 *
	 * @return the pais
	 */
	Pais getPais();

	/**
	 * Gets the periodicidade.
	 *
	 * @return the periodicidade
	 */
	Periodicidade getPeriodicidade();

	/**
	 * Sets the periodicidade.
	 *
	 * @param periodicidade the new periodicidade
	 */
	void setPeriodicidade(Periodicidade periodicidade);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	void setSegmento(Segmento segmento);

	/**
	 * Gets the unidade federacao.
	 *
	 * @return the unidade federacao
	 */
	UnidadeFederacao getUnidadeFederacao();

	/**
	 * Sets the unidade federacao.
	 *
	 * @param unidadeFederacao the new unidade federacao
	 */
	void setUnidadeFederacao(UnidadeFederacao unidadeFederacao);

	/**
	 * Gets the microrregiao.
	 *
	 * @return the microrregiao
	 */
	Microrregiao getMicrorregiao();

	/**
	 * Sets the microrregiao.
	 *
	 * @param microrregiao the new microrregiao
	 */
	void setMicrorregiao(Microrregiao microrregiao);

	/**
	 * Gets the cep inicio.
	 *
	 * @return the cep inicio
	 */
	String getCepInicio();

	/**
	 * Sets the cep inicio.
	 *
	 * @param cepInicio the new cep inicio
	 */
	void setCepInicio(String cepInicio);

	/**
	 * Gets the cep fim.
	 *
	 * @return the cep fim
	 */
	String getCepFim();

	/**
	 * Sets the cep fim.
	 *
	 * @param cepFim the new cep fim
	 */
	void setCepFim(String cepFim);

	/**
	 * Gets the ddd.
	 *
	 * @return the ddd
	 */
	Integer getDdd();

	/**
	 * Sets the ddd.
	 *
	 * @param ddd the new ddd
	 */
	void setDdd(Integer ddd);

	/**
	 * Gets the tipo pessoa.
	 *
	 * @return the tipo pessoa
	 */
	TipoPessoa getTipoPessoa();

	/**
	 * Sets the tipo pessoa.
	 *
	 * @param tipoPessoa the new tipo pessoa
	 */
	void setTipoPessoa(TipoPessoa tipoPessoa);

	/**
	 * Gets the tipo aplicacao tributo aliquota.
	 *
	 * @return the tipo aplicacao tributo aliquota
	 */
	EntidadeConteudo getTipoAplicacaoTributoAliquota();
	
	/**
	 * Sets the tipo aplicacao tributo aliquota.
	 *
	 * @param tipoAplicacaoTributoAliquota the new tipo aplicacao tributo aliquota
	 */
	void setTipoAplicacaoTributoAliquota(EntidadeConteudo tipoAplicacaoTributoAliquota);
	
	/**
	 * Gets the city gate.
	 *
	 * @return the city gate
	 */
	CityGate getCityGate();

	/**
	 * Sets the city gate.
	 *
	 * @param cityGate the new city gate
	 */
	void setCityGate(CityGate cityGate);

	/**
	 * Gets the endereco.
	 *
	 * @return the endereco
	 */
	Endereco getEndereco();

	/**
	 * Sets the endereco.
	 *
	 * @param endereco the new endereco
	 */
	void setEndereco(Endereco endereco);

	/**
	 * Gets the localidade.
	 *
	 * @return the localidade
	 */
	Localidade getLocalidade();

	/**
	 * Sets the localidade.
	 *
	 * @param localidade the new localidade
	 */
	void setLocalidade(Localidade localidade);

	/**
	 * Gets the menor faixa.
	 *
	 * @return the menor faixa
	 */
	Integer getMenorFaixa();

	/**
	 * Sets the menor faixa.
	 *
	 * @param menorFaixa the new menor faixa
	 */
	void setMenorFaixa(Integer menorFaixa);

	/**
	 * Gets the maior faixa.
	 *
	 * @return the maior faixa
	 */
	Integer getMaiorFaixa();

	/**
	 * Sets the maior faixa.
	 *
	 * @param maiorFaixa the new maior faixa
	 */
	void setMaiorFaixa(Integer maiorFaixa);

	/**
	 * Gets the gerencia regional.
	 *
	 * @return the gerencia regional
	 */
	GerenciaRegional getGerenciaRegional();

	/**
	 * Sets the gerencia regional.
	 *
	 * @param gerenciaRegional the new gerencia regional
	 */
	void setGerenciaRegional(GerenciaRegional gerenciaRegional);

	/**
	 * Gets the cliente.
	 *
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * Sets the cliente.
	 *
	 * @param cliente the new cliente
	 */
	void setCliente(Cliente cliente);

	/**
	 * Gets the dia vencimento.
	 *
	 * @return the dia vencimento
	 */
	int getDiaVencimento();

	/**
	 * Sets the dia vencimento.
	 *
	 * @param diaVencimento the new dia vencimento
	 */
	void setDiaVencimento(int diaVencimento);

	/**
	 * Gets the ano mes referencia.
	 *
	 * @return the ano mes referencia
	 */
	Integer getAnoMesReferencia();

	/**
	 * Sets the ano mes referencia.
	 *
	 * @param anoMesReferencia the new ano mes referencia
	 */
	void setAnoMesReferencia(Integer anoMesReferencia);

	/**
	 * Gets the numero ciclo.
	 *
	 * @return the numero ciclo
	 */
	Integer getNumeroCiclo();

	/**
	 * Sets the numero ciclo.
	 *
	 * @param numeroCiclo the new numero ciclo
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * Checks if is principal.
	 *
	 * @return true, if is principal
	 */
	boolean isPrincipal();

	/**
	 * Sets the principal.
	 *
	 * @param principal the new principal
	 */
	void setPrincipal(boolean principal);

	/**
	 * Gets the quantidade dias.
	 *
	 * @return the quantidade dias
	 */
	Integer getQuantidadeDias();

	/**
	 * Sets the quantidade dias.
	 *
	 * @param quantidadeDias the new quantidade dias
	 */
	void setQuantidadeDias(Integer quantidadeDias);

	/**
	 * Gets the quantidade ciclo.
	 *
	 * @return the quantidade ciclo
	 */
	Integer getQuantidadeCiclo();

	/**
	 * Sets the quantidade ciclo.
	 *
	 * @param quantidadeCiclo the new quantidade ciclo
	 */
	void setQuantidadeCiclo(Integer quantidadeCiclo);

	/**
	 * Gets the numero maximo dias ciclo.
	 *
	 * @return the numero maximo dias ciclo
	 */
	Integer getNumeroMaximoDiasCiclo();

	/**
	 * Sets the numero maximo dias ciclo.
	 *
	 * @param numeroMaximoDiasCiclo the new numero maximo dias ciclo
	 */
	void setNumeroMaximoDiasCiclo(Integer numeroMaximoDiasCiclo);

	/**
	 * Gets the numero minimo dias ciclo.
	 *
	 * @return the numero minimo dias ciclo
	 */
	Integer getNumeroMinimoDiasCiclo();

	/**
	 * Sets the numero minimo dias ciclo.
	 *
	 * @param numeroMinimoDiasCiclo the new numero minimo dias ciclo
	 */
	void setNumeroMinimoDiasCiclo(Integer numeroMinimoDiasCiclo);

	/**
	 * Gets the tipo leitura.
	 *
	 * @return the tipo leitura
	 */
	TipoLeitura getTipoLeitura();

	/**
	 * Sets the tipo leitura.
	 *
	 * @param tipoLeitura the new tipo leitura
	 */
	void setTipoLeitura(TipoLeitura tipoLeitura);

	/**
	 * Gets the rede indicador.
	 *
	 * @return the rede indicador
	 */
	RedeIndicador getRedeIndicador();

	/**
	 * Sets the rede indicador.
	 *
	 * @param redeIndicador the new rede indicador
	 */
	void setRedeIndicador(RedeIndicador redeIndicador);

	/**
	 * Gets the considera mes civil.
	 *
	 * @return the considera mes civil
	 */
	Boolean getConsideraMesCivil();

	/**
	 * Sets the considera mes civil.
	 *
	 * @param consideraMesCivil the new considera mes civil
	 */
	void setConsideraMesCivil(Boolean consideraMesCivil);

	/**
	 * Gets the indicador dt vencimento ultimo dia ciclo.
	 *
	 * @return the indicador dt vencimento ultimo dia ciclo
	 */
	Boolean getIndicadorDtVencimentoUltimoDiaCiclo();

	/**
	 * Sets the indicador dt vencimento ultimo dia ciclo.
	 *
	 * @param indicadorDtVencimentoUltimoDiaCiclo the new indicador dt vencimento ultimo dia ciclo
	 */
	void setIndicadorDtVencimentoUltimoDiaCiclo(Boolean indicadorDtVencimentoUltimoDiaCiclo);

	/**
	 * Gets the valor percentual aliquota.
	 *
	 * @return the valor percentual aliquota
	 */
	BigDecimal getValorPercentualAliquota();

	/**
	 * Sets the valor reducao base calculo.
	 *
	 * @param valorReducaoBaseCalculo the new valor reducao base calculo
	 */
	void setValorReducaoBaseCalculo(BigDecimal valorReducaoBaseCalculo);

	/**
	 * Gets the valor reducao base calculo.
	 *
	 * @return the valor reducao base calculo
	 */
	BigDecimal getValorReducaoBaseCalculo();

	/**
	 * Sets the tipo aplicacao reducao base calculo.
	 *
	 * @param tipoAplicacaoReducaoBaseCalculo the new tipo aplicacao reducao base calculo
	 */
	void setTipoAplicacaoReducaoBaseCalculo(EntidadeConteudo tipoAplicacaoReducaoBaseCalculo);

	/**
	 * Gets the tipo aplicacao reducao base calculo.
	 *
	 * @return the tipo aplicacao reducao base calculo
	 */
	EntidadeConteudo getTipoAplicacaoReducaoBaseCalculo();

	/**
	 * Sets the indicador reducao base calculo.
	 *
	 * @param indicadorReducaoBaseCalculo the new indicador reducao base calculo
	 */
	void setIndicadorReducaoBaseCalculo(boolean indicadorReducaoBaseCalculo);

	/**
	 * Checks if is indicador reducao base calculo.
	 *
	 * @return true, if is indicador reducao base calculo
	 */
	boolean isIndicadorReducaoBaseCalculo();
	
	/**
	 * Gets the indicador continuidade cascata tarifaria.
	 *
	 * @return boolean
	 */
	Boolean getIndicadorContinuidadeCascataTarifa();

	/**
	 * Sets the indicador continuidade cascata tarifaria.
	 *
	 * @param indicadorContinuidadeCascataTarifa the new indicador continuidade cascata tarifaria
	 */
	void setIndicadorContinuidadeCascataTarifa(Boolean indicadorContinuidadeCascataTarifa);
	
	/**
	 * Gets the indicador dt vencimento igual fatura.
	 *
	 * @return boolean
	 */
	Boolean getIndicadorVencimentoIgualFatura();
	
	/**
	 * Sets the indicador dt vencimento ultimo dia ciclo.
	 *
	 * @param indicadorVencimentoIgualFatura the new indicador dt vencimento igual fatura
	 */
	void setIndicadorVencimentoIgualFatura(Boolean indicadorVencimentoIgualFatura);

}
