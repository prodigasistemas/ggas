
package br.com.ggas.geral.tabelaAuxiliar;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.rota.Periodicidade;


/**
 * Interface Controlador Tabela Auxiliar.
 * 
 *
 */

public interface ControladorTabelaAuxiliar extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_TABELA_AUXILIAR = "controladorTabelaAuxiliar";

	String ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_DESCRICAO = "ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_DESCRICAO";

	String ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_CODIGO_CNAE = "ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_CODIGO_CNAE";

	String ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_CODIGO = "ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_CODIGO";

	String ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO = "ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO";

	String ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO = "ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO";

	String ERRO_NEGOCIO_DATA_INVALIDA_FERIADO = "ERRO_NEGOCIO_DATA_INVALIDA_FERIADO";

	String ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_FERIADO = "ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_FERIADO";

	String ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_TRIBUTO = "ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_TRIBUTO";

	String ERRO_NEGOCIO_REMOVER_TRIBUTO_COM_MAIS_DE_UMA_ALIQUOTA = "ERRO_NEGOCIO_REMOVER_TRIBUTO_COM_MAIS_DE_UMA_ALIQUOTA";

	String ERRO_NEGOCIO_INSERIR_CODIGO_CNAE = "ERRO_NEGOCIO_INSERIR_CODIGO_CNAE";

	String ERRO_NEGOCIO_TAMANHO_MAXIMO_PERCENTAGEM = "ERRO_NEGOCIO_TAMANHO_MAXIMO_PERCENTAGEM";

	String ERRO_NEGOCIO_DATA_FERIADO_POSTERIOR_DATA_ATUAL = "ERRO_NEGOCIO_DATA_FERIADO_POSTERIOR_DATA_ATUAL";

	String ERRO_NEGOCIO_INSERIR_ENTIDADE = "ERRO_NEGOCIO_INSERIR_ENTIDADE";

	String ERRO_NEGOCIO_INSERIR_CITY_GATE = "ERRO_NEGOCIO_INSERIR_CITY_GATE";

	String ERRO_NEGOCIO_INSERIR_CODIGO_IBGE = "ERRO_NEGOCIO_INSERIR_CODIGO_IBGE";

	String ERRO_NEGOCIO_INCLUIR_FERIADO_DATA = "ERRO_NEGOCIO_INCLUIR_FERIADO_DATA";

	String ERRO_NEGOCIO_INSERIR_MUNICIPIO = "ERRO_NEGOCIO_INSERIR_MUNICIPIO";

	String ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_MUNICIPIO = "ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_MUNICIPIO";

	String ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO = "ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO";

	String ERRO_NEGOCIO_INCLUIR_TRIBUTO = "ERRO_NEGOCIO_INCLUIR_TRIBUTO";

	String ERRO_NEGOCIO_REMOVER_TRIBUTO = "ERRO_NEGOCIO_REMOVER_TRIBUTO";

	String ERRO_NEGOCIO_INCLUIR_TRIBUTO_ALIQUOTA = "ERRO_NEGOCIO_INCLUIR_TRIBUTO_ALIQUOTA";

	String ERRO_NEGOCIO_INCLUIR_FAIXA_FIM = "ERRO_NEGOCIO_INCLUIR_FAIXA_FIM";

	String ERRO_NEGOCIO_FAIXA_FIM_ZERO = "ERRO_NEGOCIO_FAIXA_FIM_ZERO";

	String ERRO_TAMANHO_CODIGO = "ERRO_TAMANHO_CODIGO";

	String ERRO_CODIGO_ATIVIDADE_ECONOMICA_NAO_INFORMADO = "ERRO_CODIGO_ATIVIDADE_ECONOMICA_NAO_INFORMADO";

	String ERRO_QUANTIDADE_MINIMA_MAIOR_QUANTIDADE_DIAS = "ERRO_QUANTIDADE_MINIMA_MAIOR_QUANTIDADE_DIAS";

	String ERRO_QUANTIDADE_MAXIMA_MENOR_QUANTIDADE_DIAS = "ERRO_QUANTIDADE_MAXIMA_MENOR_QUANTIDADE_DIAS";

	String ERRO_NEGOCIO_INSERIR_DESCRICAO_UNIDADE_FEDERATIVA = "ERRO_NEGOCIO_INSERIR_DESCRICAO_UNIDADE_FEDERATIVA";

	String ERRO_NEGOCIO_INSERIR_SIGLA_UNIDADE_FEDERATIVA = "ERRO_NEGOCIO_INSERIR_SIGLA_UNIDADE_FEDERATIVA";

	/**
	 * Pesquisar tabela auxiliar.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param classe
	 *            the classe
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<TabelaAuxiliar> pesquisarTabelaAuxiliar(Map<String, Object> filtro, Object classe) throws GGASException;

	/**
	 * Validar dados tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws NegocioException;

	/**
	 * Remover tabela auxiliar.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param classe
	 *            the classe
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void removerTabelaAuxiliar(Long[] chavesPrimarias, Class<?> classe, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Pesquisa descricao tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisaDescricaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Verificar tamanho descricao.
	 * 
	 * @param descricao
	 *            the descricao
	 * @param classe
	 *            the classe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarTamanhoDescricao(String descricao, String classe) throws NegocioException;

	/**
	 * Verificar tamanho descricao abreviada.
	 * 
	 * @param descricaoAbreviada
	 *            the descricao abreviada
	 * @param classe
	 *            the classe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarTamanhoDescricaoAbreviada(String descricaoAbreviada, String classe) throws NegocioException;

	/**
	 * Pesquisa codigo cnae tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisaCodigoCnaeTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisa codigo tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisaCodigoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Verificar data valida.
	 * 
	 * @param data
	 *            the data
	 * @param mascara
	 *            the mascara
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarDataValida(String data, String mascara) throws NegocioException;

	/**
	 * Pesquisar feriado tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarFeriadoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisar tributo tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarTributoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws NegocioException;

	/**
	 * Verificar codigo original codigo cnae.
	 * 
	 * @param codigoOriginal
	 *            the codigo original
	 * @param codigoCnae
	 *            the codigo cnae
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarCodigoOriginalCodigoCNAE(String codigoOriginal, String codigoCnae) throws NegocioException;

	/**
	 * Verificarpercentagem maxima.
	 * 
	 * @param percentualAliquotaAux
	 *            the percentual aliquota aux
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarpercentagemMaxima(Double percentualAliquotaAux) throws NegocioException;

	/**
	 * Pesquisar unidade federacao tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarUnidadeFederacaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisar municipio tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarMunicipioTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisar tronco tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarTroncoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisar tipo cliente tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarTipoClienteTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisar codigo ibge tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarCodigoIBGETabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Pesquisar microrregiao tabela auxiliar.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void pesquisarMicrorregiaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException;

	/**
	 * Inserir feriado.
	 * 
	 * @param listaDatasFeriado
	 *            the lista datas feriado
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	String inserirFeriado(String[] listaDatasFeriado, TabelaAuxiliar tabelaAuxiliar) throws GGASException;

	/**
	 * Inserir feriado replicado.
	 * 
	 * @param listaTabelaAuxiliar
	 *            the lista tabela auxiliar
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void inserirFeriadoReplicado(Collection<TabelaAuxiliar> listaTabelaAuxiliar) throws GGASException;

	/**
	 * Ordenar lista tabela auxiliar.
	 * 
	 * @param listaTabelaAuxiliar
	 *            the lista tabela auxiliar
	 * @return the collection
	 */
	Collection<TabelaAuxiliar> ordenarListaTabelaAuxiliar(Collection<TabelaAuxiliar> listaTabelaAuxiliar);

	/**
	 * Validar faixas tarifa.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	void validarFaixasTarifa(Collection<TabelaAuxiliar> listaDadosFaixa) throws NegocioException, FormatoInvalidoException;

	/**
	 * Validar faixas zeradas tarifa.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	void validarFaixasZeradasTarifa(List<TabelaAuxiliar> listaDadosFaixa) throws NegocioException, FormatoInvalidoException;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#criar()
	 */
	@Override
	TabelaAuxiliar criar();

	/**
	 * Verificar area contruida.
	 * 
	 * @param faixaHabilitadas
	 *            the faixa habilitadas
	 * @param listaDadosFaixaAux
	 *            the lista dados faixa aux
	 * @return the list
	 */
	List<TabelaAuxiliar> verificarAreaContruida(String[] faixaHabilitadas, List<TabelaAuxiliar> listaDadosFaixaAux);

	/**
	 * Verificar ano mes referencia.
	 * 
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarAnoMesReferencia(Integer anoMesReferencia) throws NegocioException;

	/**
	 * Verificar periodicidade numero ciclos.
	 * 
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param periodicidade
	 *            the periodicidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarPeriodicidadeNumeroCiclos(Integer numeroCiclo, Periodicidade periodicidade) throws NegocioException;

	/**
	 * Verificar remocao marca medidor.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarRemocaoMarcaMedidor(Map<String, Object> filtro, String mensagem) throws NegocioException;

	/**
	 * Validar existe principal.
	 * 
	 * @param listaTabelaAuxiliar
	 *            the lista tabela auxiliar
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarExistePrincipal(Collection<TabelaAuxiliar> listaTabelaAuxiliar, TabelaAuxiliar tabelaAuxiliar) throws NegocioException;

	/**
	 * Verificar quantidade minima maxima periodicidade.
	 * 
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarQuantidadeMinimaMaximaPeriodicidade(TabelaAuxiliar tabelaAuxiliar) throws NegocioException;

	/**
	 * Definir valor aliquota tributo por percentual.
	 *
	 * @param tributoAliquota the tributo aliquota
	 * @param valor the valor
	 * @throws NegocioException the negocio exception
	 */
	void definirValorAliquotaTributoPorPercentual(
					TributoAliquota tributoAliquota, BigDecimal valor) 
									throws NegocioException;
}
