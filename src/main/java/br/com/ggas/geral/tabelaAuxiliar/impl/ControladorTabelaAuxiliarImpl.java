/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.tabelaAuxiliar.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.Query;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.dadocensitario.ControladorSetorCensitario;
import br.com.ggas.cadastro.dadocensitario.SetorCensitario;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.ControladorUnidadeNegocio;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaTributo;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.feriado.Feriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorCityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorTabelaAuxiliarImpl extends ControladorNegocioImpl implements ControladorTabelaAuxiliar {

	private static final int CONSTANTE_NUMERO_DOIS = 2;

	private static final int CONSTANTE_NUMERO_SETE = 7;

	private static final int CONSTANTE_NUMERO_CINCO = 5;

	private static final int CONSTANTE_NUMERO_DUZENTOS_CINQUENTA = 250;

	private static final int CONSTANTE_NUMERO_CINQUENTA = 50;

	private static final int CONSTANTE_NUMERO_DUZENTOS = 200;

	private static final int CONSTANTE_NUMERO_DUZENTOS_CINQUENTA_CINCO = 255;

	private static final int CONSTANTE_NUMERO_VINTE = 20;

	private static final int CONSTANTE_NUMERO_CENTO_CINQUENTA = 150;

	private static final int CONSTANTE_NUMERO_TRINTA = 30;

	private static final int CONSTANTE_NUMERO_SEIS = 6;

	private static final int CONSTANTE_NUMERO_QUATRO = 4;

	private static final String DESCRICAO = "Descrição";

	private static final String ENTIDADE_CLASSE = "Entidade Classe";

	private static final String UNIDADE_CLASSE = "Unidade Classe";

	private static final String CODIGO = "Código";

	private static final String REGIAO = "Região";

	private static final String ESFERA_PODER = "Esfera de Poder";

	private static final String DATA_VIGENCIA = "Data Vigência";

	private static final String VALOR = "Valor Alíquota";

	private static final String TIPO = "Tipo";

	private static final String PAIS = "País";

	private static final String MICRORREGIAO = "Microrregião";

	private static final String UNIDADE_FEDERACAO = "Unidade Federação";

	private static final String TIPO_PESSOA = "Tipo Pessoa";

	private static final String INDICADOR_ALIQUOTA = "Alíquota";

	private static final String CITY_GATE = "City Gate";

	private static final String LOCALIDADE = "Localidade";

	private static final String DESCRICAO_ABREVIADA = "Descrição Abreviada";

	private static final String GERENCIA_REGIONAL = "Gerência Regional";

	private static final String PERIODICIDADE = "Periodicidade";

	private static final String MEIO_LEITURA = "Meio Leitura";

	private static final String ANO_MES_REFERENCIA = "Mês/Ano";

	private static final String NUMERO_CICLO = "Número do ciclo";

	private static final String CHAVE = "chave";

	private static final String QUANTIDADE_CICLOS = "Quantidade Ciclos";

	private static final String QUANTIDADE_DIAS = "Quantidade Dias";

	private static final String NUMERO_MAXIMO_DIAS = "Número Máximo Dias";

	private static final String NUMERO_MINIMO_DIAS = "Número Mínimo Dias";

	private static final String CEP_INICIO = "Cep Início";

	private static final String CEP_FIM = "Cep Fim";

	private static final String PESSOA_TIPO = "Tipo Pessoa";

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(TabelaAuxiliar.BEAN_ID_TABELA_AUXILIAR);
	}

	public Class<?> getClasseEntidadeTabelaAuxiliar() {

		return ServiceLocator.getInstancia().getClassPorID(TabelaAuxiliar.BEAN_ID_TABELA_AUXILIAR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	// @Override
	@Override
	public TabelaAuxiliar criar() {

		return (TabelaAuxiliar) ServiceLocator.getInstancia().getBeanPorID(TabelaAuxiliar.BEAN_ID_TABELA_AUXILIAR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarTabelaAuxiliar(java.util.Map, java.lang.Object)
	 */
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public Collection<TabelaAuxiliar> pesquisarTabelaAuxiliar(Map<String, Object> filtro, Object classe) throws GGASException {

		Map<String, Object> parameters = new HashMap<String, Object>();
		Query query = null;
		final StringBuilder hql = new StringBuilder();

		hql.append(" from ");

		if (classe.equals(Constantes.TRIBUTO)) {

			hql.append(Constantes.TRIBUTO);
			hql.append(" tributo ");
			hql.append(" inner join fetch tributo.esferaPoder ");

		} else if (classe.equals(Constantes.FERIADO)) {

			hql.append(Constantes.FERIADO);
			hql.append(" feriado ");
			hql.append(" left join fetch feriado.municipio ");

		} else {
			hql.append(classe);
		}

		hql.append(" where ");

		if (filtro != null) {
			final Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				if (classe.equals(Constantes.TRIBUTO)) {

					hql.append("tributo.chavePrimaria = :chave ");
					parameters.put(CHAVE, chavePrimaria);
				} else {

					hql.append("chavePrimaria = :chave ");
					parameters.put(CHAVE, chavePrimaria);
				}
			} else {

				if (classe.equals(Constantes.TRIBUTO)) {

					this.adicionarCamposTributoEmPesquisa(hql, filtro, parameters);

				} else if (classe.equals(Constantes.FERIADO)) {

					this.adicionarCamposFeriadoEmPesquisa(hql, filtro, parameters);

				} else {
					String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
					if (descricao != null && !StringUtils.isEmpty(descricao)) {
						hql.append("upper (descricao) like upper(:desc) and ");
						parameters.put("desc", Util.formatarTextoConsulta(descricao));

					}

					String descricaoAbreviada = (String) filtro.get("descricaoAbreviada");
					if (descricaoAbreviada != null && !StringUtils.isEmpty(descricaoAbreviada)) {
						hql.append("upper (descricaoAbreviada) like upper (:descricaoAbrev) and ");
						parameters.put("descricaoAbrev", Util.formatarTextoConsulta(descricaoAbreviada));
					}

					Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
					if (habilitado != null) {
						hql.append("habilitado = :habilit and ");
						parameters.put("habilit", habilitado);
					}
				}

				if (classe.equals(Constantes.ENTIDADE_CONTEUDO)) {
					this.adicionarCampoIdEntidadeClasseEmPesquisa(hql, filtro, parameters);

				} else if (classe.equals(Constantes.UNIDADE)) {
					Long idUnidadeClasse = (Long) filtro.get("idUnidadeClasse");
					if (idUnidadeClasse != null && idUnidadeClasse != -1) {
						hql.append(" classeUnidade.chavePrimaria = :chave ");
						parameters.put(CHAVE, idUnidadeClasse);
					}
				} else if (classe.equals(Constantes.ATIVIDADE_ECONOMICA)) {

					this.adicionarCampoCodigoAtividadeEconomicaEmPesquisa(hql, filtro, parameters);

				} else if (classe.equals(Constantes.MICRORREGIAO)) {
					Long idRegiao = (Long) filtro.get("idRegiao");
					if (idRegiao != null && idRegiao != -1) {
						hql.append(" regiao.chavePrimaria = :reg ");
						parameters.put("reg", idRegiao);
					}
				} else if (classe.equals(Constantes.UNIDADE_TIPO)) {
					String nivel = (String) filtro.get("nivel");
					if (nivel != null && !nivel.isEmpty() && !"-1".equals(nivel)) {
						hql.append(" nivel = :niv and ");
						parameters.put("niv", Integer.valueOf(nivel));
					}

					String tipo = (String) filtro.get("tipo");
					if (tipo != null && !tipo.isEmpty() && !"-1".equals(tipo)) {
						hql.append(" tipo = :tip ");
						parameters.put("tip", tipo);
					}
				} else if (classe.equals(Constantes.TRIBUTO)) {
					Long idEsferaPoder = (Long) filtro.get("idEsferaPoder");
					Boolean indicadorServico = (Boolean) filtro.get("indicadorServico");
					this.adicionarCamposTributoEmPesquisa(hql, parameters, idEsferaPoder, indicadorServico);

				} else if (classe.equals(Constantes.UNIDADE_FEDERACAO)) {

					this.adicionarCamposUnidadeFederacaoEmPesquisa(hql, filtro, parameters);

				} else if (classe.equals(Constantes.MUNICIPIO)) {

					this.adicionarCamposMunicipioEmPesquisa(hql, filtro, parameters);

				} else if (classe.equals(Constantes.TIPO_CLIENTE)) {
					Long idEsferaPoder = (Long) filtro.get("idEsferaPoder");
					if (idEsferaPoder != null && idEsferaPoder != -1) {
						hql.append(" esferaPoder.chavePrimaria = :esf and ");
						parameters.put("esf", idEsferaPoder);
					}

					Integer idTipoPessoa = (Integer) filtro.get("idTipoPessoa");
					if (idTipoPessoa != null && idTipoPessoa != -1) {
						hql.append(" tipoPessoa.codigo = :idTipoPes and ");
						parameters.put("idTipoPes", idTipoPessoa);
					}

				} else if (classe.equals(Constantes.TRONCO)) {
					Long idCityGate = (Long) filtro.get("idCityGate");
					if (idCityGate != null && idCityGate != -1) {
						hql.append(" cityGate.chavePrimaria = :city and ");
						parameters.put("city", idCityGate);
					}

				} else if (classe.equals(Constantes.CITY_GATE)) {
					Long idLocalidade = (Long) filtro.get("idLocalidade");
					if (idLocalidade != null && idLocalidade != -1) {
						hql.append(" localidade.chavePrimaria = :loca and ");
						parameters.put("loca", idLocalidade);
					}

				} else if (classe.equals(Constantes.AREA_CONSTRUIDA)) {
					Integer faixa = (Integer) filtro.get("faixa");
					if (faixa != null && faixa != -1) {
						hql.append(" menorFaixa <= :menorFaixa and maiorFaixa >= :maiorFaixa ");
						parameters.put("menorFaixa", faixa);
						parameters.put("maiorFaixa", faixa);
					}

				} else if (classe.equals(Constantes.TIPO_RELACIONAMETO_CLIENTE_IMOVEL)) {
					Boolean indicadorPrincipal = (Boolean) filtro.get("indicadorPrincipal");
					if (indicadorPrincipal != null) {
						hql.append(" principal = :indicadorPrin ");
						parameters.put("indicadorPrin", indicadorPrincipal);
					}

				} else if (classe.equals(Constantes.SETOR_CENSITARIO)) {
					Long idMunicipio = (Long) filtro.get("idMunicipio");
					if (idMunicipio != null && idMunicipio != -1) {
						hql.append(" municipio.chavePrimaria = :muni ");
						parameters.put("muni", idMunicipio);
					}

				} else if (classe.equals(Constantes.GRUPO_FATURAMENTO)) {
					Long idPeriodicidade = (Long) filtro.get("idPeriodicidade");
					if (idPeriodicidade != null && idPeriodicidade != -1) {
						hql.append(" periodicidade.chavePrimaria = :peri and ");
						parameters.put("peri", idPeriodicidade);
					}

					Long idTipoLeitura = (Long) filtro.get("idTipoLeitura");
					if (idTipoLeitura != null && idTipoLeitura != -1) {
						hql.append(" tipoLeitura.chavePrimaria = :tipoLei and ");
						parameters.put("tipoLei", idTipoLeitura);
					}

				} else if (classe.equals(Constantes.SITUACAO_IMOVEL)) {
					String contemIndicadorRede = (String) filtro.get("contemIndicadorRede");
					if (contemIndicadorRede != null && !StringUtils.isEmpty(contemIndicadorRede)) {
						hql.append(" redeIndicador.codigo = :redeInd and ");
						parameters.put("redeInd", Integer.valueOf(contemIndicadorRede));
					}
				} else if (classe.equals(Constantes.TIPO_CONTATO)) {
					int pessoaTipo = (Integer) filtro.get("pessoaTipo");
					hql.append(" pessoaTipo = :pessoaTipo and ");
					parameters.put("pessoaTipo", pessoaTipo);
				}
			}
		}

		// inserir order by

		String hqlAux = hql.substring(hql.length() - CONSTANTE_NUMERO_QUATRO, hql.length());
		String hqlAuxiliar = null;

		if ("and ".equals(hqlAux)) {
			hqlAuxiliar = hql.substring(0, hql.length() - CONSTANTE_NUMERO_QUATRO);

		} else {

			hqlAux = "";
			hqlAux = hql.substring(hql.length() - CONSTANTE_NUMERO_SEIS, hql.length());

			if ("where ".equals(hqlAux)) {

				hqlAuxiliar = hql.substring(0, hql.length() - CONSTANTE_NUMERO_SEIS);

			} else {

				hqlAuxiliar = hql.toString();

			}
		}

		if (!classe.equals(Constantes.FERIADO) && !classe.equals(Constantes.TRIBUTO)) {

			// Paginação em banco dados
			boolean paginacaoPadrao = false;
			if (filtro.containsKey("colecaoPaginada")) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
				if (colecaoPaginada.getSortCriterion() != null && StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}
			} else {
				paginacaoPadrao = true;
			}

			StringBuilder hqlAuxiliarBuffer = new StringBuilder();

			if (paginacaoPadrao) {
				hqlAuxiliarBuffer.append(hqlAuxiliar);
				if (classe.equals(Constantes.TRIBUTO)) {
					hqlAuxiliarBuffer.append("order by trib.descricao asc ");
				} else if (classe.equals(Constantes.FERIADO)) {
					hqlAuxiliarBuffer.append("order by feriado.descricao asc ");
				} else if (classe.equals(Constantes.AREA_CONSTRUIDA)) {
					hqlAuxiliarBuffer.append("order by menorFaixa asc ");
				} else {
					hqlAuxiliarBuffer.append("order by descricao asc ");
				}
				hqlAuxiliar = hqlAuxiliarBuffer.toString();
			}
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlAuxiliar);

		String hqlUtil = hqlAuxiliar;

		for (Map.Entry<String, Object> entry : parameters.entrySet()) {
			String key = entry.getKey();
			if (entry.getValue() instanceof Boolean) {
				query.setParameter(key, entry.getValue());
			} else if (parameters.get(key) instanceof String) {
				query.setParameter(key, entry.getValue());
			} else {
				query.setParameter(key, entry.getValue());
			}
		}

		if (!classe.equals(Constantes.FERIADO) && !classe.equals(Constantes.TRIBUTO) && filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			if (classe.equals(Constantes.AREA_CONSTRUIDA)) {
				colecaoPaginada.adicionarOrdenacaoEspecial("menorFaixa", new OrdenacaoEspecialMenorFaixa());
				colecaoPaginada.adicionarOrdenacaoEspecial("maiorFaixa", new OrdenacaoEspecialMenorFaixa());
			} else {

				colecaoPaginada.adicionarOrdenacaoEspecial(TabelaAuxiliar.ATRIBUTO_DESCRICAO, new OrdenacaoEspecialDescricao());
			}

			hqlAuxiliar = HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query, classe, hqlAuxiliar);

			if (hqlAuxiliar != null) {

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlAuxiliar);

				for (Map.Entry<String, Object> entry : parameters.entrySet()) {
					Object aux = entry.getValue();
					String key = entry.getKey();
					if (aux instanceof Boolean) {
						query.setParameter(key, aux);
					} else if (aux instanceof String) {
						query.setParameter(key, aux);
					} else {
						query.setParameter(key, aux);
					}
				}
			}

			query.setFirstResult(colecaoPaginada.getIndex());
			query.setMaxResults(colecaoPaginada.getObjectsPerPage());

		}

		if (hqlAuxiliar == null && classe.equals(Constantes.AREA_CONSTRUIDA)) {

			Integer faixa = (Integer) filtro.get("faixa");
			if (faixa == null || faixa == -1) {

				StringBuilder hqlUtilBuilder = new StringBuilder();
				hqlUtilBuilder.append(hqlUtil);
				hqlUtilBuilder.append(" order by menorFaixa asc ");
				hqlUtil = hqlUtilBuilder.toString();

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlUtil);

				for (Map.Entry<String, Object> entry : parameters.entrySet()) {

					String key = entry.getKey();
					if (parameters.get(key) instanceof Boolean) {
						query.setParameter(key, entry.getValue());
					} else if (parameters.get(key) instanceof String) {
						query.setParameter(key, entry.getValue());
					} else {
						query.setParameter(key, entry.getValue());
					}
				}
			}

		}

		return query.list();
	}

	private void adicionarCamposTributoEmPesquisa(StringBuilder hql, Map<String, Object> filtro, Map<String, Object> parameters) {
		String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			hql.append("upper (tributo.descricao) like upper(:desc) and ");
			parameters.put("desc", Util.formatarTextoConsulta(descricao));

		}

		String descricaoAbreviada = (String) filtro.get("descricaoAbreviada");
		if (descricaoAbreviada != null && !StringUtils.isEmpty(descricaoAbreviada)) {
			hql.append("upper (tributo.descricaoAbreviada) like upper (:descricaoAbrev) and ");
			parameters.put("descricaoAbrev", Util.formatarTextoConsulta(descricaoAbreviada));
		}

		Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
		if (habilitado != null) {
			hql.append("tributo.habilitado = :habilit and ");
			parameters.put("habilit", habilitado);
		}
	}

	private void adicionarCamposFeriadoEmPesquisa(StringBuilder hql, Map<String, Object> filtro, Map<String, Object> parameters) {

		String descricao = (String) filtro.get("descricao");
		if (descricao != null && !StringUtils.isEmpty(descricao)) {
			hql.append("upper (feriado.descricao) like upper(:desc) and ");
			parameters.put("desc", Util.formatarTextoConsulta(descricao));

		}

		Boolean habilitado = (Boolean) filtro.get("habilitado");
		if (habilitado != null) {
			hql.append("feriado.habilitado = :habilit and ");
			parameters.put("habilit", habilitado);
		}

	}

	private void adicionarCampoIdEntidadeClasseEmPesquisa(StringBuilder hql, Map<String, Object> filtro, Map<String, Object> parameters) {

		Long idEntidadeClasse = (Long) filtro.get("idEntidadeClasse");
		if (idEntidadeClasse != null && idEntidadeClasse != -1) {
			hql.append(" entidadeClasse.chavePrimaria = :chave and ");
			parameters.put(CHAVE, idEntidadeClasse);
		}
	}

	private void adicionarCampoCodigoAtividadeEconomicaEmPesquisa(StringBuilder hql, Map<String, Object> filtro,
			Map<String, Object> parameters) {
		String codigoCnae = (String) filtro.get("codigoCnae");
		if (codigoCnae != null && codigoCnae.length() > 0) {
			hql.append(" codigo = :cod ");
			parameters.put("cod", codigoCnae);
		}
	}

	private void adicionarCamposTributoEmPesquisa(StringBuilder hql, Map<String, Object> parameters,
			Long idEsferaPoderAux, Boolean indicadorServicoAux) {

		Long idEsferaPoder = idEsferaPoderAux;
		Boolean indicadorServico = indicadorServicoAux;
		if (idEsferaPoder != null && idEsferaPoder != -1) {
			hql.append(" tributo.esferaPoder.chavePrimaria = :esf and ");
			parameters.put("esf", idEsferaPoder);
		}

		if (indicadorServico != null) {
			if (indicadorServico) {
				hql.append(" tributo.indicadorServico = :serv ");
				parameters.put("serv", indicadorServico);
			} else {
				hql.append(" tributo.indicadorProduto = :prod and ");
				parameters.put("prod", !indicadorServico);
			}
		}
	}

	private void adicionarCamposMunicipioEmPesquisa(StringBuilder hql, Map<String, Object> filtro, Map<String, Object> parameters) {

		Long idMicrorregiao = (Long) filtro.get("idMicrorregiao");
		if (idMicrorregiao != null && idMicrorregiao != -1) {
			hql.append(" microrregiao.chavePrimaria = :idMicrorregiao and ");
			parameters.put("idMicrorregiao", idMicrorregiao);
		}

		Long idUnidadeFederacao = (Long) filtro.get("idUnidadeFederacao");
		if (idUnidadeFederacao != null && idUnidadeFederacao != -1) {
			hql.append(" unidadeFederacao.chavePrimaria = :idUnidadeFederacao and ");
			parameters.put("idUnidadeFederacao", idUnidadeFederacao);
		}

		Long idRegiao = (Long) filtro.get("idRegiao");
		if (idRegiao != null && idRegiao != -1) {
			hql.append(" regiao.chavePrimaria = :idRegiao and ");
			parameters.put("idRegiao", idRegiao);
		}
		Long codigoIBGE = (Long) filtro.get("codigoIBGE");
		if (codigoIBGE != null && codigoIBGE > 0) {
			hql.append(" unidadeFederativaIBGE = :codigoIBGE ");
			parameters.put("codigoIBGE", codigoIBGE);
		}
	}

	private void adicionarCamposUnidadeFederacaoEmPesquisa(StringBuilder hql, Map<String, Object> filtro, Map<String, Object> parameters) {
		Long idPais = (Long) filtro.get("idPais");
		if (idPais != null && idPais != -1) {
			hql.append(" pais.chavePrimaria = :idPais and ");
			parameters.put("idPais", idPais);
		}

		String sigla = (String) filtro.get("sigla");
		if (sigla != null && sigla.length() > 0) {
			hql.append("upper (sigla) like upper(:sig) and ");
			parameters.put("sig", sigla + "%");
		}

		Long codigoIBGE = (Long) filtro.get("codigoIBGE");
		if (codigoIBGE != null && codigoIBGE > 0) {
			hql.append(" unidadeFederativaIBGE = :codigoIBGE ");
			parameters.put("codigoIBGE", codigoIBGE);
		}
	}

	/**
	 * Classe privada reponsï¿½vel por criar uma
	 * ordenaï¿½ï¿½o pela descriï¿½ï¿½o
	 *
	 * @author ccavalcanti
	 */
	public class OrdenacaoEspecialDescricao implements OrdenacaoEspecial {

		/**
		 * Metódo reponsável por criar Alias.
		 *
		 * @param criteria
		 *            the criteria
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/**
		 * Metódo reponsável por adicionar
		 * ordenação.
		 *
		 * @param criteria
		 *            the criteria
		 * @param sortDirection
		 *            the sort direction
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			String hqlAux = null;
			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {

				if (classe.equals(Constantes.TRIBUTO)) {

					hqlAux = hqlAuxiliar + "order by trib.descricao asc ";
				} else if (classe.equals(Constantes.FERIADO)) {

					hqlAux = hqlAuxiliar + "order by feriado.descricao asc";
				} else {

					hqlAux = hqlAuxiliar + "order by descricao asc";
				}

			} else {
				if (classe.equals(Constantes.TRIBUTO)) {

					hqlAux = hqlAuxiliar + "order by trib.descricao desc ";
				} else if (classe.equals(Constantes.FERIADO)) {

					hqlAux = hqlAuxiliar + "order by feriado.descricao desc";
				} else {

					hqlAux = hqlAuxiliar + "order by descricao desc";
				}
			}
			return hqlAux;
		}

	}

	/**
	 * Classe privada reponsï¿½vel por criar uma
	 * ordenaï¿½ï¿½o pela descriï¿½ï¿½o
	 *
	 * @author ccavalcanti
	 */
	public class OrdenacaoEspecialMenorFaixa implements OrdenacaoEspecial {

		/**
		 * Método reponsável por criar Alias.
		 *
		 * @param criteria
		 *            the criteria
		 */
		@Override
		public void createAlias(Criteria criteria) {
			throw new UnsupportedOperationException();
		}

		/**
		 * Metódo reponsável por adicionar
		 * ordenação.
		 *
		 * @param criteria
		 *            the criteria
		 * @param sortDirection
		 *            the sort direction
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {
			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			String hqlAux = null;
			if (classe.equals(Constantes.AREA_CONSTRUIDA)) {

				if (SortOrderEnum.ASCENDING.equals(sortDirection)) {

					hqlAux = hqlAuxiliar + "order by menorFaixa asc, maiorFaixa desc ";
				} else {

					hqlAux = hqlAuxiliar + "order by menorFaixa desc, maiorFaixa asc ";
				}
			}
			return hqlAux;
		}

	}

	// verificar se jï¿½ existe registro no banco
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisaDescricaoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar,
	 * java.lang.String)
	 */
	// com aquela descriï¿½ï¿½o
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisaDescricaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append("upper (descricao) like upper(:desc) ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setString("desc", descricao);

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
							ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_DESCRICAO, titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarTamanhoDescricao(java.lang.String, java.lang.String)
	 */
	@Override
	public void verificarTamanhoDescricao(String descricao, String classe) throws NegocioException {

		if (classe.equals(Constantes.ENTIDADE_CLASSE) || classe.equals(Constantes.UNIDADE) || classe.equals(Constantes.FERIADO)) {
			if (descricao.length() > CONSTANTE_NUMERO_TRINTA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, CONSTANTE_NUMERO_TRINTA);
			}
		} else if (classe.equals(Constantes.ENTIDADE_CONTEUDO)) {
			if (descricao.length() > CONSTANTE_NUMERO_CENTO_CINQUENTA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, CONSTANTE_NUMERO_CENTO_CINQUENTA);
			}
		} else if (classe.equals(Constantes.UNIDADE_CLASSE) || classe.equals(Constantes.MICRORREGIAO)
						|| classe.equals(Constantes.CORRETOR_MARCA) || classe.equals(Constantes.CORRETOR_MODELO)
						|| classe.equals(Constantes.MEDIDOR_MODELO) || classe.equals(Constantes.MEDIDOR_MARCA)
						|| classe.equals(Constantes.UNIDADE_TIPO) || classe.equals(Constantes.PERFIL_QUADRA)
						|| classe.equals(Constantes.FERIADO) || classe.equals(Constantes.TIPO_SEGMENTO)
						|| classe.equals(Constantes.SITUACAO_IMOVEL) || classe.equals(Constantes.CLIENTE_SITUACAO)
						|| classe.equals(Constantes.PAVIMENTO_CALCADA) || classe.equals(Constantes.PAVIMENTO_RUA)
						|| classe.equals(Constantes.PADRAO_CONSTRUCAO) || classe.equals(Constantes.TIPO_BOTIJAO)
						|| classe.equals(Constantes.PERFIL_IMOVEL) || classe.equals(Constantes.TIPO_RELACIONAMETO_CLIENTE_IMOVEL)
						|| classe.equals(Constantes.CANAL_ATENDIMENTO) || classe.equals(Constantes.ZEIS)
						|| classe.equals(Constantes.ZONA_BLOQUEIO) || classe.equals(Constantes.AREA_TIPO)
						|| classe.equals(Constantes.TIPO_ENDERECO) || classe.equals(Constantes.TIPO_FONE)
						|| classe.equals(Constantes.PERIODICIDADE) || classe.equals(Constantes.REDE_MATERIAL)) {
			if (descricao.length() > CONSTANTE_NUMERO_VINTE) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO, CONSTANTE_NUMERO_VINTE);
			}

		} else if (classe.equals(Constantes.CORRETOR_MODELO)) {
			if (descricao.length() > CONSTANTE_NUMERO_DUZENTOS_CINQUENTA_CINCO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						CONSTANTE_NUMERO_DUZENTOS_CINQUENTA_CINCO);
			}
		} else if (classe.equals(Constantes.ATIVIDADE_ECONOMICA)) {
			if (descricao.length() > CONSTANTE_NUMERO_DUZENTOS) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						CONSTANTE_NUMERO_DUZENTOS);
			}
		} else if (classe.equals(Constantes.TRIBUTO) || classe.equals(Constantes.UNIDADE_FEDERACAO) || classe.equals(Constantes.MUNICIPIO)
						|| classe.equals(Constantes.CITY_GATE) || classe.equals(Constantes.TIPO_CLIENTE)
						|| classe.equals(Constantes.TRONCO) || classe.equals(Constantes.GRUPO_FATURAMENTO)
						|| classe.equals(Constantes.UNIDADE_NEGOCIO)) {
			if (descricao.length() > CONSTANTE_NUMERO_CINQUENTA) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
						CONSTANTE_NUMERO_CINQUENTA);
			}
		} else if (classe.equals(Constantes.SETOR_CENSITARIO) && descricao.length() > CONSTANTE_NUMERO_DUZENTOS_CINQUENTA) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_MAIOR_TAMANHO_MAXIMO,
					CONSTANTE_NUMERO_DUZENTOS_CINQUENTA);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarTamanhoDescricaoAbreviada(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void verificarTamanhoDescricaoAbreviada(String descricaoAbreviada, String classe) throws NegocioException {

		if (classe.equals(Constantes.ENTIDADE_CLASSE) || classe.equals(Constantes.MEDIDOR_MARCA)
						|| classe.equals(Constantes.ENTIDADE_CONTEUDO) || classe.equals(Constantes.UNIDADE_CLASSE)
						|| classe.equals(Constantes.CORRETOR_MARCA)) {
			if (descricaoAbreviada.length() > CONSTANTE_NUMERO_SEIS) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO, CONSTANTE_NUMERO_SEIS);
			}
		} else if (classe.equals(Constantes.CORRETOR_MODELO)
						|| classe.equals(Constantes.MEDIDOR_MODELO)
						|| (classe.equals(Constantes.UNIDADE_TIPO) || (classe.equals(Constantes.TRIBUTO) || (classe
										.equals(Constantes.TIPO_SEGMENTO)
										|| (classe.equals(Constantes.CITY_GATE))
										|| classe.equals(Constantes.PAVIMENTO_CALCADA)
										|| classe.equals(Constantes.PAVIMENTO_RUA)
										|| classe.equals(Constantes.GRUPO_FATURAMENTO)
										|| classe.equals(Constantes.UNIDADE_NEGOCIO)
										|| classe.equals(Constantes.CANAL_ATENDIMENTO)
										|| classe.equals(Constantes.ZEIS)
										|| classe.equals(Constantes.AREA_TIPO) || classe.equals(Constantes.PERIODICIDADE))))) {
			if (descricaoAbreviada.length() > CONSTANTE_NUMERO_CINCO) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO, CONSTANTE_NUMERO_CINCO);
			}
		} else if (classe.equals(Constantes.UNIDADE) && descricaoAbreviada.length() > CONSTANTE_NUMERO_SETE) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DESCRICAO_ABREVIADA_MAIOR_TAMANHO_MAXIMO, CONSTANTE_NUMERO_SETE);

		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#validarDadosTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar
	 * )
	 */
	@Override
	public void validarDadosTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws NegocioException {

		Map<String, Object> errosTabelaAuxiliar = this.validarDados(tabelaAuxiliar);

		if (errosTabelaAuxiliar != null && !errosTabelaAuxiliar.isEmpty()) {
			throw new NegocioException(errosTabelaAuxiliar);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarQuantidadeMinimaMaximaPeriodicidade(br.com.ggas.geral.tabelaAuxiliar
	 * .
	 * TabelaAuxiliar)
	 */
	@Override
	public void verificarQuantidadeMinimaMaximaPeriodicidade(TabelaAuxiliar tabelaAuxiliar) throws NegocioException {

		if (tabelaAuxiliar.getNumeroMinimoDiasCiclo() > tabelaAuxiliar.getQuantidadeDias()) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_QUANTIDADE_MINIMA_MAIOR_QUANTIDADE_DIAS, true);
		}

		if (tabelaAuxiliar.getNumeroMaximoDiasCiclo() < tabelaAuxiliar.getQuantidadeDias()) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_QUANTIDADE_MAXIMA_MENOR_QUANTIDADE_DIAS, true);
		}
	}

	/**
	 * Validar dados.
	 *
	 * @param tabelaAuxiliar - {@link TabelaAuxiliar}
	 * @return erros de validação - {@link Map}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public Map<String, Object> validarDados(TabelaAuxiliar tabelaAuxiliar)
					throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (!tabelaAuxiliar.getClass().getName().equals(Constantes.TRIBUTO_ALIQUOTA)
				&& StringUtils.isEmpty(tabelaAuxiliar.getDescricao())) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (tabelaAuxiliar.getClass().getName().equals(Constantes.ENTIDADE_CONTEUDO)) {
			if (tabelaAuxiliar.getEntidadeClasse() == null) {
				stringBuilder.append(ENTIDADE_CLASSE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.UNIDADE)) {
			if (tabelaAuxiliar.getClasseUnidade() == null) {
				stringBuilder.append(UNIDADE_CLASSE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.ATIVIDADE_ECONOMICA)) {
			if (tabelaAuxiliar.getCodigoOriginal() == null) {
				stringBuilder.append(CODIGO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.MICRORREGIAO)) {
			if (tabelaAuxiliar.getRegiao() == null) {
				stringBuilder.append(REGIAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.TRIBUTO_ALIQUOTA)) {

			if (tabelaAuxiliar.getDataVigencia() == null) {
				stringBuilder.append(DATA_VIGENCIA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getTipoAplicacaoTributoAliquota() == null) {
				stringBuilder.append(INDICADOR_ALIQUOTA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getValorAliquota() == null) {
				stringBuilder.append(VALOR);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.UNIDADE_TIPO)) {
			if (tabelaAuxiliar.getTipo() == null) {
				stringBuilder.append(TIPO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.TRIBUTO)) {

			if (tabelaAuxiliar.getEsferaPoder() == null) {
				stringBuilder.append(ESFERA_PODER);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (!tabelaAuxiliar.isIndicadorProduto() && !tabelaAuxiliar.isIndicadorServico()) {
				stringBuilder.append(TIPO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.UNIDADE_FEDERACAO)) {
			if (tabelaAuxiliar.getPais() == null) {
				stringBuilder.append(PAIS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.MUNICIPIO)) {
			if (tabelaAuxiliar.getRegiao() == null) {
				stringBuilder.append(REGIAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getMicrorregiao() == null) {
				stringBuilder.append(MICRORREGIAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getUnidadeFederacao() == null) {
				stringBuilder.append(UNIDADE_FEDERACAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (StringUtils.isBlank(tabelaAuxiliar.getCepInicio())) {
				stringBuilder.append(CEP_INICIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (StringUtils.isBlank(tabelaAuxiliar.getCepFim())) {
				stringBuilder.append(CEP_FIM);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.TIPO_CLIENTE)) {
			TipoPessoa tipoPessoa = tabelaAuxiliar.getTipoPessoa();
			if (tipoPessoa == null || tipoPessoa.getCodigo() <= 0) {
				stringBuilder.append(TIPO_PESSOA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.TRONCO)) {
			if (tabelaAuxiliar.getCityGate() == null) {
				stringBuilder.append(CITY_GATE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.CITY_GATE)) {
			if (tabelaAuxiliar.getLocalidade() == null) {
				stringBuilder.append(LOCALIDADE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.PAVIMENTO_CALCADA)
						|| tabelaAuxiliar.getClass().getName().equals(Constantes.PAVIMENTO_RUA)) {
			if (tabelaAuxiliar.getDescricaoAbreviada() == null) {
				stringBuilder.append(DESCRICAO_ABREVIADA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.UNIDADE_NEGOCIO)) {
			if (tabelaAuxiliar.getGerenciaRegional() == null) {
				stringBuilder.append(GERENCIA_REGIONAL);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getDescricaoAbreviada() == null) {
				stringBuilder.append(DESCRICAO_ABREVIADA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.GRUPO_FATURAMENTO)) {

			if (tabelaAuxiliar.getDescricaoAbreviada() == null) {
				stringBuilder.append(DESCRICAO_ABREVIADA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getNumeroCiclo() == null || tabelaAuxiliar.getNumeroCiclo() == 0) {
				stringBuilder.append(NUMERO_CICLO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getPeriodicidade() == null) {
				stringBuilder.append(PERIODICIDADE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getTipoLeitura() == null) {
				stringBuilder.append(MEIO_LEITURA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getAnoMesReferencia() == null) {
				stringBuilder.append(ANO_MES_REFERENCIA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.PERIODICIDADE)) {
			if (tabelaAuxiliar.getQuantidadeCiclo() == null) {
				stringBuilder.append(QUANTIDADE_CICLOS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getQuantidadeDias() == null) {
				stringBuilder.append(QUANTIDADE_DIAS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getNumeroMinimoDiasCiclo() == null) {
				stringBuilder.append(NUMERO_MINIMO_DIAS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (tabelaAuxiliar.getNumeroMaximoDiasCiclo() == null) {
				stringBuilder.append(NUMERO_MAXIMO_DIAS);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.CANAL_ATENDIMENTO)) {
			if (tabelaAuxiliar.getDescricao() == null || tabelaAuxiliar.getDescricao().trim().length() == 0
							|| tabelaAuxiliar.getDescricao().isEmpty()) {
				stringBuilder.append(DESCRICAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		} else if (tabelaAuxiliar.getClass().getName().equals(Constantes.TIPO_CONTATO)) {
			TipoContato tipoContato = (TipoContato) tabelaAuxiliar;
			if (tipoContato.getPessoaTipo() == -1) {
				stringBuilder.append(PESSOA_TIPO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
		}
		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#removerTabelaAuxiliar(java.lang.Long[], java.lang.Class,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void removerTabelaAuxiliar(Long[] chavesPrimarias, Class<?> classe, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();
		ControladorTributo controladorTributo = ServiceLocator.getInstancia().getControladorTributo();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorUnidadeOrganizacional controladorUnidadeOrganizacional = ServiceLocator.getInstancia()
						.getControladorUnidadeOrganizacional();

		Collection lista = null;
		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			if (classe.getName().equals(Constantes.ENTIDADE_CLASSE)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] != 0) {
						lista = getControladorEntidadeConteudo().listarEntidadeConteudo(chavesPrimarias[i]);

						if (lista != null && !lista.isEmpty()) {
							throw new NegocioException(ControladorEntidadeConteudo.ERRO_NEGOCIO_REMOVER_ENTIDADE_CONTEUDO, true);
						}
					}
				}
			} else if (classe.getName().equals(Constantes.UNIDADE_CLASSE)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] != 0) {
						lista = controladorUnidade.listarUnidade(chavesPrimarias[i]);

						if (lista != null && !lista.isEmpty()) {
							throw new NegocioException(ControladorUnidade.ERRO_NEGOCIO_REMOVER_UNIDADE_CLASSE, true);
						}
					}
				}
			} else if (classe.getName().equals(Constantes.TRIBUTO)) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						verificarRemocaoTributo(chavesPrimarias[i], ControladorTributo.ERRO_NEGOCIO_REMOVER_TRIBUTO);

						Tributo tributo = (Tributo) controladorTributo.criarTributo();
						tributo.setChavePrimaria(chavesPrimarias[i]);
						filtro.put("tributo", tributo);
						Collection<TributoAliquota> listaTributoAliquota = controladorTributo.consultarTributosAliquota(filtro);

						if (listaTributoAliquota != null && !listaTributoAliquota.isEmpty()) {
							Long[] idTributoAliquota = new Long[listaTributoAliquota.size()];

							int count = 0;
							for (TributoAliquota tributoAliquota : listaTributoAliquota) {

								idTributoAliquota[count] = tributoAliquota.getChavePrimaria();
								count++;
							}
							this.remover(idTributoAliquota, Constantes.TRIBUTO_ALIQUOTA, dadosAuditoria);
						}
					}

				}

			} else if (classe.getName().equals(Constantes.ATIVIDADE_ECONOMICA)) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						filtro.put("idAtividadeEconomia", chavesPrimarias[i]);

						lista = controladorCliente.consultarClientes(filtro);

						if (lista != null && !lista.isEmpty()) {
							throw new NegocioException(ControladorCliente.ERRO_NEGOCIO_REMOVER_ATIVIDADE_ECONOMICA, true);
						}
					}
				}

			} else if (classe.getName().equals(Constantes.UNIDADE_TIPO)) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						filtro.put("idUnidadeTipo", chavesPrimarias[i]);

						lista = controladorUnidadeOrganizacional.consultarUnidadeOrganizacional(filtro);

						if (lista != null && !lista.isEmpty()) {
							throw new NegocioException(ControladorUnidadeOrganizacional.ERRO_NEGOCIO_REMOVER_UNIDADE_TIPO, true);
						}
					}
				}

			} else if (classe.getName().equals(Constantes.UNIDADE_FEDERACAO)) {
				Map<String, Object> filtro = new HashMap<String, Object>();
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						filtro.put("idUnidadeFederacao", chavesPrimarias[i]);

						verificarRemocaoUnidadeFederacao(filtro, ControladorMunicipio.ERRO_NEGOCIO_REMOVER_UNIDADE_FEDERACAO);
					}
				}

			} else if (classe.getName().equals(Constantes.MUNICIPIO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idMunicipio", chavesPrimarias[i]);

						verificarRemocaoMunicipio(filtro, ControladorMunicipio.ERRO_NEGOCIO_REMOVER_MUNICIPIO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_SEGMENTO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTipoSegmento", chavesPrimarias[i]);

						verificarRemocaoTipoSegmento(filtro, ControladorSegmento.ERRO_NEGOCIO_REMOVER_TIPO_SEGMENTO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_CLIENTE)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTipoCliente", chavesPrimarias[i]);

						verificarRemocaoTipoCliente(filtro, ControladorCliente.ERRO_NEGOCIO_REMOVER_TIPO_CLIENTE);
					}
				}

			} else if (classe.getName().equals(Constantes.TRONCO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTronco", chavesPrimarias[i]);

						verificarRemocaoTronco(filtro, ControladorRede.ERRO_NEGOCIO_REMOVER_REDE);
					}
				}

			} else if (classe.getName().equals(Constantes.CLIENTE_SITUACAO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idClienteSituacao", chavesPrimarias[i]);

						verificarRemocaoClienteSituacao(filtro, ControladorCliente.ERRO_NEGOCIO_REMOVER_CLIENTE_SITUACAO);
					}
				}

			} else if (classe.getName().equals(Constantes.SITUACAO_IMOVEL)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idSituacaoImovel", chavesPrimarias[i]);

						verificarRemocaoSituacaoImovel(filtro, ControladorImovel.ERRO_NEGOCIO_REMOVER_SITUACAO_IMOVEL);
					}
				}

			} else if (classe.getName().equals(Constantes.CITY_GATE)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idCityGate", chavesPrimarias[i]);

						verificarRemocaoSituacaoCityGate(filtro, ControladorTronco.ERRO_NEGOCIO_REMOVER_CITY_GATE);
					}
				}

			} else if (classe.getName().equals(Constantes.PAVIMENTO_CALCADA)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idPavimentoCalcada", chavesPrimarias[i]);

						verificarRemocaoTabelaAuxiliar(filtro, ControladorImovel.ERRO_NEGOCIO_REMOVER_PAVIMENTO_CALCADA);
					}
				}

			} else if (classe.getName().equals(Constantes.PAVIMENTO_RUA)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idPavimentoRua", chavesPrimarias[i]);

						verificarRemocaoTabelaAuxiliar(filtro, ControladorImovel.ERRO_NEGOCIO_REMOVER_PAVIMENTO_RUA);
					}
				}

			} else if (classe.getName().equals(Constantes.PADRAO_CONSTRUCAO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idPadraoConstrucao", chavesPrimarias[i]);

						verificarRemocaoTabelaAuxiliar(filtro, ControladorImovel.ERRO_NEGOCIO_REMOVER_PADRAO_CONSTRUCAO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_BOTIJAO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTipoBotijao", chavesPrimarias[i]);

						verificarRemocaoTabelaAuxiliar(filtro, ControladorImovel.ERRO_NEGOCIO_REMOVER_TIPO_BOTIJAO);
					}
				}

			} else if (classe.getName().equals(Constantes.PERFIL_IMOVEL)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idPerfilImovel", chavesPrimarias[i]);

						verificarRemocaoTabelaAuxiliar(filtro, ControladorImovel.ERRO_NEGOCIO_REMOVER_PERFIL_IMOVEL);
					}
				}

			} else if (classe.getName().equals(Constantes.REDE_MATERIAL)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idRedeMaterial", chavesPrimarias[i]);

						verificarRemocaoRedeMaterial(filtro, ControladorRede.ERRO_NEGOCIO_REMOVER_REDE_MATERIAL);
					}
				}

			} else if (classe.getName().equals(Constantes.REDE_DIAMETRO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idRedeDiametro", chavesPrimarias[i]);

						verificarRemocaoRedeDiametro(filtro, ControladorRede.ERRO_NEGOCIO_REMOVER_REDE_DIAMETRO);
					}
				}

			} else if (classe.getName().equals(Constantes.PROFISSAO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idProfissao", chavesPrimarias[i]);

						verificarRemocaoProfissao(filtro, ControladorCliente.ERRO_NEGOCIO_REMOVER_PROFISSAO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_CONTATO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idContatoTipo", chavesPrimarias[i]);

						verificarRemocaoContatoTipo(filtro, ControladorCliente.ERRO_NEGOCIO_REMOVER_TIPO_CONTATO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_RELACIONAMETO_CLIENTE_IMOVEL)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTipoRelacionamentoClienteImovel", chavesPrimarias[i]);

						verificarRemocaoTipoRelacionamentoClienteImovel(filtro,
										ControladorClienteImovel.ERRO_NEGOCIO_REMOVER_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL);
					}
				}

			} else if (classe.getName().equals(Constantes.UNIDADE_NEGOCIO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idUnidadeNegocio", chavesPrimarias[i]);

						verificarRemocaoUnidadeNegocio(filtro, ControladorUnidadeNegocio.ERRO_NEGOCIO_REMOVER_UNIDADE_NEGOCIO);
					}
				}

			} else if (classe.getName().equals(Constantes.CANAL_ATENDIMENTO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idCanalAtendimento", chavesPrimarias[i]);
					}
				}

			} else if (classe.getName().equals(Constantes.ZEIS)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idZeis", chavesPrimarias[i]);

						verificarRemocaoEntidades(filtro, ControladorUnidadeOrganizacional.ERRO_NEGOCIO_REMOVER_ZEIS);
					}
				}

			} else if (classe.getName().equals(Constantes.SETOR_CENSITARIO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idSetorCensitario", chavesPrimarias[i]);

						verificarRemocaoEntidades(filtro, ControladorUnidadeOrganizacional.ERRO_NEGOCIO_REMOVER_SETOR_CENSITARIO);
					}
				}

			} else if (classe.getName().equals(Constantes.ZONA_BLOQUEIO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idZonaBloqueio", chavesPrimarias[i]);

						verificarRemocaoEntidades(filtro, ControladorUnidadeOrganizacional.ERRO_NEGOCIO_REMOVER_ZONA_BLOQUEIO);
					}
				}

			} else if (classe.getName().equals(Constantes.AREA_TIPO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idAreaTipo", chavesPrimarias[i]);

						verificarRemocaoEntidades(filtro, ControladorUnidadeOrganizacional.ERRO_NEGOCIO_REMOVER_AREA_TIPO);
					}
				}

			} else if (classe.getName().equals(Constantes.GRUPO_FATURAMENTO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idGrupoFaturamento", chavesPrimarias[i]);
						filtro.put("grupoFaturamento", chavesPrimarias[i]);

						verificarRemocaoGrupoFaturamento(filtro, ControladorFatura.ERRO_NEGOCIO_REMOVER_GRUPO_FATURAMENTO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_ENDERECO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTipoEndereco", chavesPrimarias[i]);

						verificarRemocaoTipoEndereco(filtro, ControladorCliente.ERRO_NEGOCIO_REMOVER_TIPO_ENDERECO);
					}
				}

			} else if (classe.getName().equals(Constantes.TIPO_FONE)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idTipoTelefone", chavesPrimarias[i]);

						verificarRemocaoTipoTelefone(filtro, ControladorCliente.ERRO_NEGOCIO_REMOVER_TIPO_TELEFONE);
					}
				}

			} else if (classe.getName().equals(Constantes.PERIODICIDADE)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idPeriodicidade", chavesPrimarias[i]);

						verificarRemocaoPeriodicidade(filtro, ControladorFatura.ERRO_NEGOCIO_REMOVER_PERIODICIDADE);
					}
				}
			} else if (classe.getName().equals(Constantes.MEDIDOR_MARCA)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idMarcaMedidor", chavesPrimarias[i]);

						verificarRemocaoMarcaMedidor(filtro, ControladorMedidor.ERRO_NEGOCIO_REMOVER_MARCA_MEDIDOR);
					}
				}
			} else if (classe.getName().equals(Constantes.MEDIDOR_MODELO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idModeloMedidor", chavesPrimarias[i]);

						verificarRemocaoModeloMedidor(filtro, ControladorMedidor.ERRO_NEGOCIO_REMOVER_MODELO_MEDIDOR);
					}
				}
			} else if (classe.getName().equals(Constantes.CORRETOR_MARCA)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idMarcaCorretor", chavesPrimarias[i]);

						verificarRemocaoMarcaVazaoCorretor(filtro, ControladorMedidor.ERRO_NEGOCIO_REMOVER_MARCA_CORRETOR);
					}
				}
			} else if (classe.getName().equals(Constantes.CORRETOR_MODELO)) {
				for (int i = 0; i < chavesPrimarias.length; i++) {
					if (chavesPrimarias[i] > 0) {

						Map<String, Object> filtro = new HashMap<String, Object>();
						filtro.put("idModeloCorretor", chavesPrimarias[i]);

						verificarRemocaoModeloCorretor(filtro, ControladorMedidor.ERRO_NEGOCIO_REMOVER_MODELO_CORRETOR);
					}
				}
			}

			this.remover(chavesPrimarias, classe.getName(), dadosAuditoria);

		} else if (classe.getName().equals(Constantes.AREA_CONSTRUIDA)) {

			StringBuilder hql = new StringBuilder();
			Query query = null;

			hql.append(" delete ");
			hql.append(" from ");
			hql.append(Constantes.AREA_CONSTRUIDA);

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.executeUpdate();
		}
	}

	private ControladorEntidadeConteudo getControladorEntidadeConteudo() {

		return ServiceLocator.getInstancia().getControladorEntidadeConteudo();
	}

	/**
	 * Verificar remocao periodicidade.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoPeriodicidade(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorRamoAtividade controladorRamoAtividade = ServiceLocator.getInstancia().getControladorRamoAtividade();

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = controladorContrato.consultarContratoPontoConsumo(filtro);
		Collection<GrupoFaturamento> listaGrupoFaturamento = controladorFatura.consultarGrupoFaturamento(filtro);
		Collection<RamoAtividade> listaRamoAtividade = controladorRamoAtividade.consultarRamoAtividade(filtro);
		Collection<Rota> listaRota = controladorRota.consultarRota(filtro);
		Collection<Segmento> listaSegmento = controladorSegmento.consultarSegmento(filtro);

		if ((listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty())
						|| (listaGrupoFaturamento != null && !listaGrupoFaturamento.isEmpty())
						|| (listaRamoAtividade != null && !listaRamoAtividade.isEmpty()) || (listaRota != null && !listaRota.isEmpty())
						|| (listaSegmento != null && !listaSegmento.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tipo endereco.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTipoEndereco(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		Collection<ClienteEndereco> listaClienteEndereco = controladorCliente.consultarClienteEndereco(filtro);

		if (listaClienteEndereco != null && !listaClienteEndereco.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tipo telefone.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTipoTelefone(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		Collection<ClienteFone> listaClienteTelefone = controladorCliente.consultarClienteTelefone(filtro);

		if (listaClienteTelefone != null && !listaClienteTelefone.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao grupo faturamento.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoGrupoFaturamento(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = ServiceLocator.getInstancia()
						.getControladorCronogramaFaturamento();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();

		Collection<CronogramaFaturamento> listaCronogramaFaturamento = controladorCronogramaFaturamento
						.consultarCronogramaFaturamento(filtro);
		Collection<FaturaImpressao> listaFaturaImpressao = controladorFatura.consultarFaturaImpressao(filtro);
		Collection<Rota> listaRota = controladorRota.consultarRota(filtro);

		if ((listaCronogramaFaturamento != null && !listaCronogramaFaturamento.isEmpty())
						|| (listaFaturaImpressao != null && !listaFaturaImpressao.isEmpty()) || (listaRota != null && !listaRota.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/*
	 * Esse metódo é utilizado para as
	 * entidades:
	 * AREA TIPO, Zona de Bloqueio, Setor
	 * Censitï¿½rio, Zeis
	 */

	/**
	 * Verificar remocao entidades.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoEntidades(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorQuadra controladorQuadra = ServiceLocator.getInstancia().getControladorQuadra();
		Collection<Quadra> listaQuadra = controladorQuadra.consultarQuadras(filtro);

		if (listaQuadra != null && !listaQuadra.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	// O código foi comentado porque o tratamento da exceção está na tabelaAuxiliarAction método removerTabelaAuxiliar

	/**
	 * Verificar remocao canal atendimento.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoCanalAtendimento(Map<String, Object> filtro, String mensagem) throws NegocioException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Verificar remocao unidade negocio.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoUnidadeNegocio(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorLocalidade controladorLocalidade = ServiceLocator.getInstancia().getControladorLocalidade();
		Collection<Localidade> listaLocalidade = controladorLocalidade.consultarLocalidades(filtro);

		if (listaLocalidade != null && !listaLocalidade.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao rede material.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoRedeMaterial(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorRede controladorRede = ServiceLocator.getInstancia().getControladorRede();

		Collection<Rede> listaRede = controladorRede.consultarRedes(filtro);
		Collection<RedeInternaImovel> listaRedeInternaImovel = controladorRede.consultarRedeInternaImovel(filtro);

		if ((listaRede != null && !listaRede.isEmpty()) || (listaRedeInternaImovel != null && !listaRedeInternaImovel.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao profissao.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoProfissao(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();

		Collection<ContatoImovel> listaContatoImovel = controladorImovel.consultarContatoImovel(filtro);
		Collection<ContatoCliente> listaContatoCliente = controladorCliente.consultarContatoCliente(filtro);
		Collection<Cliente> listaCliente = controladorCliente.consultarClientes(filtro);

		if ((listaContatoImovel != null && !listaContatoImovel.isEmpty())
						|| (listaContatoCliente != null && !listaContatoCliente.isEmpty())
						|| (listaCliente != null && !listaCliente.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao contato tipo.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoContatoTipo(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();

		Collection<ContatoImovel> listaContatoImovel = controladorImovel.consultarContatoImovel(filtro);
		Collection<ContatoCliente> listaContatoCliente = controladorCliente.consultarContatoCliente(filtro);

		if ((listaContatoImovel != null && !listaContatoImovel.isEmpty())
						|| (listaContatoCliente != null && !listaContatoCliente.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tipo relacionamento cliente imovel.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTipoRelacionamentoClienteImovel(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorClienteImovel controladorClienteImovel = ServiceLocator.getInstancia().getControladorClienteImovel();
		Collection<ClienteImovel> listaClienteImovel = controladorClienteImovel.consultarClienteImovel(filtro);

		if (listaClienteImovel != null && !listaClienteImovel.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao rede diametro.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoRedeDiametro(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorRede controladorRede = ServiceLocator.getInstancia().getControladorRede();

		Collection<Rede> listaRede = controladorRede.consultarRedes(filtro);

		if (listaRede != null && !listaRede.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tabela auxiliar.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * Esse metódo é utilizado para as
	 * entidades:
	 * PavimentoCalcada, PavimentoRua,
	 * PadraoConstrucao, TipoBotijao
	 */
	public void verificarRemocaoTabelaAuxiliar(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();
		Collection<Imovel> listaImoveis = controladorImovel.consultarImoveis(filtro);

		if (listaImoveis != null && !listaImoveis.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao situacao city gate.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoSituacaoCityGate(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorTronco controladorTronco = ServiceLocator.getInstancia().getControladorTronco();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorCityGateMedicao controladorCityGateMedicao = ServiceLocator.getInstancia().getControladorCityGateMedicao();

		Collection<Tronco> listaTronco = controladorTronco.consultarTronco(filtro);
		Collection<PontoConsumoCityGate> listaPontoConsumo = controladorPontoConsumo.consultarPontosConsumoCityGate(filtro);
		Collection<CityGateMedicao> listaCityGateMedicao = controladorCityGateMedicao.consultarPontosConsumoCityGateMedicao(filtro);

		if ((listaTronco != null && !listaTronco.isEmpty()) || (listaPontoConsumo != null && !listaPontoConsumo.isEmpty())
						|| (listaCityGateMedicao != null && !listaCityGateMedicao.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao situacao imovel.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoSituacaoImovel(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();
		Collection<Imovel> listaImovel = controladorImovel.consultarImoveis(filtro);

		if (listaImovel != null && !listaImovel.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao cliente situacao.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoClienteSituacao(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		Collection<Cliente> listaCliente = controladorCliente.consultarClientes(filtro);

		if (listaCliente != null && !listaCliente.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tronco.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTronco(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorRede controladorRede = ServiceLocator.getInstancia().getControladorRede();
		Collection<Rede> listaRede = controladorRede.consultarRedeDistribuicaoTronco(filtro);

		if (listaRede != null && !listaRede.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tipo cliente.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTipoCliente(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		Collection<Cliente> listaCliente = controladorCliente.consultarClientes(filtro);

		if (listaCliente != null && !listaCliente.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tipo segmento.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTipoSegmento(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();

		Collection<Segmento> listaSegmento = controladorSegmento.consultarSegmento(filtro);

		if (listaSegmento != null && !listaSegmento.isEmpty()) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao municipio.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoMunicipio(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorTributo controladorTributo = ServiceLocator.getInstancia().getControladorTributo();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorTemperaturaPressaoMedicao controladorTemperaturaPressaoMedicao = ServiceLocator.getInstancia()
						.getControladorTemperaturaPressaoMedicao();
		ControladorUnidadeOrganizacional controladorUnidadeOrganizacional = ServiceLocator.getInstancia()
						.getControladorUnidadeOrganizacional();
		ControladorFeriado controladorFeriado = ServiceLocator.getInstancia().getControladorFeriado();
		ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();
		ControladorSetorCensitario controladorSetorCensitario = ServiceLocator.getInstancia().getControladorSetorCensitario();
		ControladorSetorComercial controladorSetorComercial = ServiceLocator.getInstancia().getControladorSetorComercial();

		Collection<Cep> listaCep = controladorEndereco.consultarCeps(filtro);
		Collection<ClienteEndereco> listaClienteEndereco = controladorCliente.consultarClienteEndereco(filtro);
		Collection<Feriado> listaFeriado = controladorFeriado.consultarFeriado(filtro);
		Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao = controladorTemperaturaPressaoMedicao
						.consultarTemperaturaPressaoMedicao(filtro);
		Collection<SetorCensitario> listaSetorCensitario = controladorSetorCensitario.consultarSetorCensitario(filtro);
		Collection<SetorComercial> listaSetorComercial = controladorSetorComercial.consultarSetorComercial(filtro);
		Collection<TributoAliquota> listaTributoAliquota = controladorTributo.consultarTributosAliquota(filtro);
		Collection<UnidadeOrganizacional> listaUnidadeOrganizacional = controladorUnidadeOrganizacional
						.consultarUnidadeOrganizacional(filtro);

		if ((listaCep != null && !listaCep.isEmpty())
						|| (listaTemperaturaPressaoMedicao != null && !listaTemperaturaPressaoMedicao.isEmpty())
						|| (listaClienteEndereco != null && !listaClienteEndereco.isEmpty())
						|| (listaFeriado != null && !listaFeriado.isEmpty())
						|| (listaSetorCensitario != null && !listaSetorCensitario.isEmpty())
						|| (listaSetorComercial != null && !listaSetorComercial.isEmpty())
						|| (listaTributoAliquota != null && !listaTributoAliquota.isEmpty())
						|| (listaUnidadeOrganizacional != null && !listaUnidadeOrganizacional.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao unidade federacao.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoUnidadeFederacao(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorTemperaturaPressaoMedicao controladorTemperaturaPressaoMedicao = ServiceLocator.getInstancia()
						.getControladorTemperaturaPressaoMedicao();
		ControladorMunicipio controladorMunicipio = ServiceLocator.getInstancia().getControladorMunicipio();

		Collection<Cliente> listaCliente = controladorCliente.consultarClientes(filtro);
		Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao = controladorTemperaturaPressaoMedicao
						.consultarTemperaturaPressaoMedicao(filtro);
		Collection<Municipio> listaMunicipio = controladorMunicipio.consultarMunicipios(filtro);

		if ((listaCliente != null && !listaCliente.isEmpty())
						|| (listaTemperaturaPressaoMedicao != null && !listaTemperaturaPressaoMedicao.isEmpty())
						|| (listaMunicipio != null && !listaMunicipio.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/**
	 * Verificar remocao tributo.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoTributo(Long chavePrimaria, String mensagem) throws NegocioException {

		ControladorTributo controladorTributo = ServiceLocator.getInstancia().getControladorTributo();

		Collection<TarifaVigenciaTributo> listaTarifaTributo = controladorTributo.verificaTabelasTributo(chavePrimaria,
						Constantes.TARIFA_VIGENCIA_TRIBUTO);
		Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributo = controladorTributo.verificaTabelasTributo(chavePrimaria,
						Constantes.PONTO_CONSUMO_TRIBUTO);
		Collection<RubricaTributo> listaRubricaTributo = controladorTributo.verificaTabelasTributo(chavePrimaria,
						Constantes.RUBRICA_TRIBUTO);

		if ((listaTarifaTributo != null && !listaTarifaTributo.isEmpty())
						|| (listaPontoConsumoTributo != null && !listaPontoConsumoTributo.isEmpty())
						|| (listaRubricaTributo != null && !listaRubricaTributo.isEmpty())) {
			throw new NegocioException(mensagem, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisaCodigoCnaeTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisaCodigoCnaeTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String codigoCnae = tabelaAuxiliar.getCodigo();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (codigoCnae != null && !StringUtils.isEmpty(codigoCnae)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append(" codigo = :cod ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setString("cod", codigoCnae);

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
							ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_CODIGO_CNAE, titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisaCodigoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar
	 * ,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisaCodigoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		Long codigo = tabelaAuxiliar.getCodigoOriginal();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (codigo != null && codigo > 0) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append(" codigoOriginal = :cod ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("cod", codigo);

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
							ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_CODIGO, titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarDataValida(java.lang.String, java.lang.String)
	 */
	@Override
	public void verificarDataValida(String data, String mascara) throws NegocioException {

		Boolean isDataValida = Util.isDataValida(data, mascara);
		if (!isDataValida) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_DATA_INVALIDA_FERIADO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#ordenarListaTabelaAuxiliar(java.util.Collection)
	 */
	@Override
	public Collection<TabelaAuxiliar> ordenarListaTabelaAuxiliar(Collection<TabelaAuxiliar> listaTabelaAuxiliar) {

		Collection<Long> listaIdTributo = new ArrayList<Long>();
		for (TabelaAuxiliar tabelaAuxiliar : listaTabelaAuxiliar) {

			if (!listaIdTributo.contains(tabelaAuxiliar.getTributo().getChavePrimaria())) {
				listaIdTributo.add(tabelaAuxiliar.getTributo().getChavePrimaria());
			}

		}

		Collection<TabelaAuxiliar> listaIdTributoAliquota = new ArrayList<TabelaAuxiliar>();
		Collection<Date> listaDataVigencia = new ArrayList<Date>();
		for (Long idTributo : listaIdTributo) {
			for (TabelaAuxiliar tabelaAuxiliar : listaTabelaAuxiliar) {
				if (tabelaAuxiliar.getTributo().getChavePrimaria() == idTributo) {

					listaDataVigencia.add(tabelaAuxiliar.getDataVigencia());

					for (Date data : listaDataVigencia) {
						if (tabelaAuxiliar.getDataVigencia().compareTo(data) < 0) {
							listaIdTributoAliquota.add(tabelaAuxiliar);
						}
					}

				}

			}
			listaDataVigencia = new ArrayList<Date>();
		}

		for (TabelaAuxiliar tabelaAuxiliar : listaIdTributoAliquota) {
			if (listaTabelaAuxiliar.contains(tabelaAuxiliar)) {
				listaTabelaAuxiliar.remove(tabelaAuxiliar);
			}

		}

		return listaTabelaAuxiliar;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar
	 * #pesquisarFeriadoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar
	 * ,
	 * java.lang.String)
	 */
	@Override
	public void pesquisarFeriadoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		Long idMunicipio = null;
		if (tabelaAuxiliar.getMunicipio() != null) {
			idMunicipio = tabelaAuxiliar.getMunicipio().getChavePrimaria();
		}

		String descricao = tabelaAuxiliar.getDescricao();
		Date dataFeriado = tabelaAuxiliar.getData();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(tabelaAuxiliar.getClass().getName());
		hql.append(" where ");
		hql.append("upper (descricao) like upper(:desc) and ");
		hql.append(" data = :dataFeriado  ");

		if (tabelaAuxiliar.getMunicipio() != null) {
			hql.append(" and municipio.chavePrimaria = :muni ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("desc", descricao);
		query.setDate("dataFeriado", dataFeriado);

		if (tabelaAuxiliar.getMunicipio() != null) {
			query.setLong("muni", idMunicipio);
		}

		listaTabelaAuxiliar = query.list();

		this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
						ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_FERIADO, titulo);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar
	 * #pesquisarTributoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar
	 * )
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarTributoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar) throws NegocioException {

		Long idEsferaPoder = tabelaAuxiliar.getEsferaPoder().getChavePrimaria();
		String descricaoTributo = tabelaAuxiliar.getDescricao();
		Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(Constantes.TRIBUTO);
		hql.append(" where ");
		hql.append("upper (descricao) like upper(:desc) and ");
		hql.append(" esferaPoder.chavePrimaria = :idEsferaPoder ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idEsferaPoder", idEsferaPoder);
		query.setString("desc", descricaoTributo);

		listaTabelaAuxiliar = query.list();

		if (listaTabelaAuxiliar != null && !listaTabelaAuxiliar.isEmpty()) {
			if (chavePrimaria != null && chavePrimaria > 0) {
				Long chave = listaTabelaAuxiliar.iterator().next().getChavePrimaria();

				if (!chave.equals(chavePrimaria)) {
					throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_TRIBUTO, true);
				}
			} else {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_TABELA_AUXILIAR_TRIBUTO, true);
			}
		}
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar
	 * .ControladorTabelaAuxiliar
	 * #definirValorAliquotaTributoPorPercentual(
	 * br.com.ggas.faturamento.tributo.TributoAliquota,
	 * java.math.BigDecimal)
	 */
	@Override
	public void definirValorAliquotaTributoPorPercentual(
					TributoAliquota tributoAliquota, BigDecimal valor) throws NegocioException {

		EntidadeConteudo tipoAplicacaoTributoAliquota = getControladorEntidadeConteudo()
						.obterEntidadeConteudoTipoAplicacaoTributoAliquotaPercentual();

		tributoAliquota.setTipoAplicacaoTributoAliquota(tipoAplicacaoTributoAliquota);
		tributoAliquota.setValorAliquota(NumeroUtil.converterValorAbsolutoParaPercentual(valor));

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarCodigoOriginalCodigoCNAE(java.lang.String, java.lang.String)
	 */
	@Override
	public void verificarCodigoOriginalCodigoCNAE(String codigoOriginal, String codigoCnae) throws NegocioException {

		int tam = codigoOriginal.length();
		if (tam == 0) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_CODIGO_ATIVIDADE_ECONOMICA_NAO_INFORMADO, true);
		}
		if (tam > 0 && tam < CONSTANTE_NUMERO_SEIS) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_TAMANHO_CODIGO, true);
		}
		String codigoCnaeAux = null;
		if (codigoCnae != null && codigoCnae.length() > 0) {
			if (tam == CONSTANTE_NUMERO_SEIS) {
				codigoCnaeAux = codigoCnae.substring(CONSTANTE_NUMERO_DOIS, codigoCnae.length());
			} else if (tam == CONSTANTE_NUMERO_SETE) {
				codigoCnaeAux = codigoCnae.substring(1, codigoCnae.length());
			}

			String codigoCnaeSemCaracteres = null;

			if (codigoCnaeAux != null) {
				codigoCnaeSemCaracteres = codigoCnaeAux.replace("-", "").replace("/", "");
			}

			if (!codigoOriginal.equals(codigoCnaeSemCaracteres)) {
				throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_CODIGO_CNAE, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarpercentagemMaxima(java.lang.Double)
	 */
	@Override
	public void verificarpercentagemMaxima(Double percentualAliquotaAux) throws NegocioException {

		if (percentualAliquotaAux > Double.parseDouble(Constantes.TAMANHO_MAXIMO_PERCENTUAL)) {
			throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_TAMANHO_MAXIMO_PERCENTAGEM, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarUnidadeFederacaoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar
	 * , java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarUnidadeFederacaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();
		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			validarUnidadeFederativaDescricaoExistente(tabelaAuxiliar, titulo);
			validarUnidadeFederativaSiglaExistente(tabelaAuxiliar, titulo);

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where 1=1 ");

			if (tabelaAuxiliar.getPais() != null) {
				hql.append(" and pais = :pais ");
			}

			if (tabelaAuxiliar.getUnidadeFederativaIBGE() != null) {
				hql.append(" and unidadeFederativaIBGE = :codigoIBGE ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			if (tabelaAuxiliar.getUnidadeFederativaIBGE() != null) {
				query.setLong("codigoIBGE", tabelaAuxiliar.getUnidadeFederativaIBGE());
			}

			if (tabelaAuxiliar.getPais() != null) {
				query.setLong("pais", tabelaAuxiliar.getPais().getChavePrimaria());
			}

			listaTabelaAuxiliar = query.list();

			if (tabelaAuxiliar.getUnidadeFederativaIBGE() != null) {
				this.verificarUnidadeFederativaIBGEExistente(listaTabelaAuxiliar, chavePrimaria, titulo);
			}

		}
	}

	/**
	 * Verificar unidade federativa ibge existente.
	 *
	 * @param listaTabelaAuxiliar
	 *            the lista tabela auxiliar
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarUnidadeFederativaIBGEExistente(Collection<TabelaAuxiliar> listaTabelaAuxiliar,
			Long chavePrimaria, String titulo) throws NegocioException {

		if (listaTabelaAuxiliar.size() == 1) {
			for (TabelaAuxiliar tabelaAuxiliar : listaTabelaAuxiliar) {
				if (chavePrimaria != tabelaAuxiliar.getChavePrimaria() && tabelaAuxiliar.getUnidadeFederativaIBGE() != null) {
					this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
									ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_CODIGO_IBGE, titulo);
				}
			}
		}
	}

	/**
	 * Verificar lista tabela auxiliar.
	 *
	 * @param listaTabelaAuxiliar
	 *            the lista tabela auxiliar
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param erro
	 *            the erro
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarListaTabelaAuxiliar(Collection<TabelaAuxiliar> listaTabelaAuxiliar, Long chavePrimaria, String erro, String titulo)
					throws NegocioException {

		if (listaTabelaAuxiliar != null && !listaTabelaAuxiliar.isEmpty()) {
			if (chavePrimaria != null && chavePrimaria > 0) {
				Long chave = listaTabelaAuxiliar.iterator().next().getChavePrimaria();

				if (!chave.equals(chavePrimaria)) {
					throw new NegocioException(erro, titulo);
				}
			} else {
				throw new NegocioException(erro, titulo);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarTroncoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar
	 * ,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarTroncoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		CityGate cityGate = tabelaAuxiliar.getCityGate();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append("upper (descricao) like upper(:desc) ");

			if (cityGate != null) {
				hql.append(" and cityGate.chavePrimaria = :city ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setString("desc", descricao);

			if (cityGate != null) {
				query.setLong("city", cityGate.getChavePrimaria());
			}

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria, ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_CITY_GATE,
							titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarMicrorregiaoTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarMicrorregiaoTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		Regiao regiao = tabelaAuxiliar.getRegiao();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append("upper (descricao) like upper(:desc) ");

			if (regiao != null) {
				hql.append(" and regiao.chavePrimaria = :reg ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setString("desc", descricao);

			if (regiao != null) {
				query.setLong("reg", regiao.getChavePrimaria());
			}

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria, ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_ENTIDADE,
							titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarMunicipioTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarMunicipioTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append("upper (descricao) like upper(:desc) ");

			if (tabelaAuxiliar.getUnidadeFederacao() != null) {
				hql.append(" and unidadeFederacao = :unidadeFed ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setString("desc", descricao);

			if (tabelaAuxiliar.getUnidadeFederacao() != null) {
				query.setLong("unidadeFed", tabelaAuxiliar.getUnidadeFederacao().getChavePrimaria());
			}
			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria, ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_MUNICIPIO,
							titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarTipoClienteTabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarTipoClienteTabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");
			hql.append("upper (descricao) like upper(:desc) ");

			if (tabelaAuxiliar.getTipoPessoa() != null) {
				hql.append(" and tipoPessoa.codigo = :cod ");
			}

			if (tabelaAuxiliar.getEsferaPoder() != null) {
				hql.append(" and esferaPoder = :esf ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setString("desc", descricao);

			if (tabelaAuxiliar.getTipoPessoa() != null) {
				query.setLong("cod", tabelaAuxiliar.getTipoPessoa().getCodigo());
			}

			if (tabelaAuxiliar.getEsferaPoder() != null) {
				query.setLong("esf", tabelaAuxiliar.getEsferaPoder().getChavePrimaria());
			}
			listaTabelaAuxiliar = query.list();

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria, ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_ENTIDADE,
							titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#pesquisarCodigoIBGETabelaAuxiliar(br.com.ggas.geral.tabelaAuxiliar.
	 * TabelaAuxiliar,
	 * java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void pesquisarCodigoIBGETabelaAuxiliar(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		String descricao = tabelaAuxiliar.getDescricao();
		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		if (descricao != null && !StringUtils.isEmpty(descricao)) {

			Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(tabelaAuxiliar.getClass().getName());
			hql.append(" where ");

			if (tabelaAuxiliar.getUnidadeFederativaIBGE() != null) {
				hql.append(" unidadeFederativaIBGE = :codigoIBGE ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			if (tabelaAuxiliar.getUnidadeFederativaIBGE() != null) {
				query.setLong("codigoIBGE", tabelaAuxiliar.getUnidadeFederativaIBGE());
			}

			listaTabelaAuxiliar = query.list();

			this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
							ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_CODIGO_IBGE, titulo);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#inserirFeriado(java.lang.String[],
	 * br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar)
	 */
	@Override
	public String inserirFeriado(String[] listaDatasFeriadoArray, TabelaAuxiliar tabelaAuxiliar) throws GGASException {

		ControladorFeriado controladorFeriado = ServiceLocator.getInstancia().getControladorFeriado();

		Collection<TabelaAuxiliar> listaDatasFeriadoEmComum = null;
		Collection<TabelaAuxiliar> listaDatasFeriadoAtualizar = new ArrayList<TabelaAuxiliar>();
		Long[] chavesFeriadoRemocao = null;
		Map<String, Object> filtro = null;
		String mensagemErro = null;
		Boolean inseriuFeriado = false;

		if (tabelaAuxiliar.getDescricao() != null && tabelaAuxiliar.getMunicipio() == null && tabelaAuxiliar.isIndicadorMunicipal()) {
			mensagemErro = ControladorTabelaAuxiliar.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_MUNICIPIO;
		}

		if (mensagemErro == null) {

			listaDatasFeriadoEmComum = this.pesquisarTabelaAuxiliar(filtro, Constantes.FERIADO);

			boolean achouData = false;

			// verifica se existem feriados para
			// serem removidos
			for (TabelaAuxiliar tabelaAux : listaDatasFeriadoEmComum) {

				achouData = false;

				for (String data : listaDatasFeriadoArray) {

					Date dataAux = Util.converterCampoStringParaData("Data", data, Constantes.FORMATO_DATA_BR);

					if (tabelaAux.getData().compareTo(dataAux) == 0) {

						achouData = true;
						break;
					}
				}

				if (!achouData) {
					chavesFeriadoRemocao = (Long[]) ArrayUtils.add(chavesFeriadoRemocao, tabelaAux.getChavePrimaria());
				} else {
					listaDatasFeriadoAtualizar.add(tabelaAux);
				}

			}

			// remove os feriados
			if ((chavesFeriadoRemocao != null) && (chavesFeriadoRemocao.length > 0)) {
				this.removerTabelaAuxiliar(chavesFeriadoRemocao, tabelaAuxiliar.getClass(), tabelaAuxiliar.getDadosAuditoria());
			}

			// atualiza os feriados
			for (TabelaAuxiliar tabelaAux : listaDatasFeriadoAtualizar) {

				this.atualizar(tabelaAux, tabelaAuxiliar.getClass());
			}

			// monta a lista para inserir os
			// feriados
			for (String data : listaDatasFeriadoArray) {

				achouData = false;

				for (TabelaAuxiliar tabelaAux : listaDatasFeriadoEmComum) {

					Date dataAux = Util.converterCampoStringParaData("Data", data, Constantes.FORMATO_DATA_BR);

					if (tabelaAux.getData().compareTo(dataAux) == 0) {

						achouData = true;
						break;
					}
				}

				if (!achouData) {

					// valida se a descriï¿½ï¿½o
					// foi preenchida
					if (tabelaAuxiliar.getDescricao() == null || StringUtils.isEmpty(tabelaAuxiliar.getDescricao())) {

						mensagemErro = Constantes.ERRO_DESCRICAO;
						break;

					} else {

						Feriado feriado = (Feriado) controladorFeriado.criar();

						feriado.setData(Util.converterCampoStringParaData("Data", data, Constantes.FORMATO_DATA_BR));
						feriado.setDescricao(tabelaAuxiliar.getDescricao());
						feriado.setIndicadorTipo(tabelaAuxiliar.isIndicadorTipo());
						feriado.setIndicadorMunicipal(tabelaAuxiliar.isIndicadorMunicipal());

						if (tabelaAuxiliar.getMunicipio() != null) {
							feriado.setMunicipio(tabelaAuxiliar.getMunicipio());
						}

						this.inserir(feriado);
						inseriuFeriado = true;
					}

				}

			}

			if ((listaDatasFeriadoAtualizar.size() == listaDatasFeriadoEmComum.size()) && chavesFeriadoRemocao == null && !inseriuFeriado
							&& mensagemErro == null) {

				mensagemErro = ControladorTabelaAuxiliar.ERRO_NEGOCIO_INCLUIR_FERIADO_DATA;
			}
		}

		return mensagemErro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#inserirFeriadoReplicado(java.util.Collection)
	 */
	@Override
	public void inserirFeriadoReplicado(Collection<TabelaAuxiliar> listaTabelaAuxiliar) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorFeriado controladorFeriado = ServiceLocator.getInstancia().getControladorFeriado();

		for (TabelaAuxiliar tabelaAux : listaTabelaAuxiliar) {
			if (!tabelaAux.isIndicadorTipo()) {

				GregorianCalendar calendario = new GregorianCalendar();
				calendario.setTime(tabelaAux.getData());

				GregorianCalendar calendarioAux = new GregorianCalendar();
				calendarioAux.setTime(new Date());

				if (calendario.get(GregorianCalendar.YEAR) == calendarioAux.get(GregorianCalendar.YEAR)) {

					ParametroSistema parametroQuantidadeAnosVigenciaFeriado = controladorParametroSistema
									.obterParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ANOS_VIGENCIA_FERIADO);
					int count = 1;

					String data = Util.converterDataParaString(tabelaAux.getData());

					while (count <= Integer.parseInt(parametroQuantidadeAnosVigenciaFeriado.getValor())) {

						Feriado feriadoAux = (Feriado) controladorFeriado.criar();

						povoarObjetoFeriado(tabelaAux, feriadoAux);

						montarObjetoParaReplicarFeriado(data, count, feriadoAux);

						if (!verificarDataJaExistente(feriadoAux.getData(), listaTabelaAuxiliar)) {
							this.inserir(feriadoAux);
						}

						count++;
					}
				}
			}
		}
	}

	/**
	 * Verificar data ja existente.
	 *
	 * @param data
	 *            the data
	 * @param listaTabelaAuxiliar
	 *            the lista tabela auxiliar
	 * @return true, if successful
	 */
	private boolean verificarDataJaExistente(Date data, Collection<TabelaAuxiliar> listaTabelaAuxiliar) {

		boolean achouData = false;

		for (TabelaAuxiliar tabelaAux : listaTabelaAuxiliar) {

			if (tabelaAux.getData().compareTo(data) == 0) {

				achouData = true;
				break;
			}
		}

		return achouData;
	}

	/**
	 * Montar objeto para replicar feriado.
	 *
	 * @param data
	 *            the data
	 * @param count
	 *            the count
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void montarObjetoParaReplicarFeriado(String data, int count, TabelaAuxiliar tabelaAuxiliar) throws GGASException {

		String diaMes = data.substring(0, data.length() - CONSTANTE_NUMERO_CINCO);
		String ano = data.substring(data.length() - CONSTANTE_NUMERO_QUATRO, data.length());

		Integer anoInteiro = Integer.parseInt(ano);
		int somaAno = anoInteiro + count;

		String dataFinal = diaMes.concat("/").concat(String.valueOf(somaAno));

		Date dataAux = Util.converterCampoStringParaData("Data", dataFinal, Constantes.FORMATO_DATA_BR);

		tabelaAuxiliar.setData(dataAux);

	}

	/**
	 * Povoar objeto feriado.
	 *
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param tabelaAuxiliarAux
	 *            the tabela auxiliar aux
	 */
	private void povoarObjetoFeriado(TabelaAuxiliar tabelaAuxiliar, TabelaAuxiliar tabelaAuxiliarAux) {

		tabelaAuxiliarAux.setDescricao(tabelaAuxiliar.getDescricao());
		tabelaAuxiliarAux.setMunicipio(tabelaAuxiliar.getMunicipio());
		tabelaAuxiliarAux.setIndicadorTipo(tabelaAuxiliar.isIndicadorTipo());
		tabelaAuxiliarAux.setIndicadorMunicipal(tabelaAuxiliar.isIndicadorMunicipal());

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#validarFaixasTarifa(java.util.Collection)
	 */
	@Override
	public void validarFaixasTarifa(Collection<TabelaAuxiliar> listaDadosFaixa) throws NegocioException, FormatoInvalidoException {

		TabelaAuxiliar dadosFaixasTarifaAnterior = null;
		for (TabelaAuxiliar dadosFaixasTarifa : listaDadosFaixa) {
			if (dadosFaixasTarifaAnterior != null) {

				Long faixaFinalAnterior = dadosFaixasTarifaAnterior.getMaiorFaixa().longValue();
				Long faixaInicialAtual = dadosFaixasTarifa.getMenorFaixa().longValue();
				if (faixaInicialAtual.longValue() < faixaFinalAnterior.longValue()) {
					throw new NegocioException(ERRO_NEGOCIO_VALOR_FAIXA_TARIFA_INVALIDO, true);
				}
			}
			dadosFaixasTarifaAnterior = dadosFaixasTarifa;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#validarFaixasZeradasTarifa(java.util.List)
	 */
	@Override
	public void validarFaixasZeradasTarifa(List<TabelaAuxiliar> listaDadosFaixa) throws NegocioException, FormatoInvalidoException {

		int countValores = 0;
		for (TabelaAuxiliar tabelaAuxiliar : listaDadosFaixa) {
			if (tabelaAuxiliar.getMaiorFaixa() == null || tabelaAuxiliar.getMaiorFaixa().equals(Integer.valueOf(0))) {
				countValores++;
			}
		}
		if (countValores > 1) {
			throw new NegocioException(Tarifa.ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarAreaContruida(java.lang.String[], java.util.List)
	 */
	@Override
	public List<TabelaAuxiliar> verificarAreaContruida(String[] faixaHabilitadas, List<TabelaAuxiliar> listaDadosFaixaAux) {

		Long chavePrimariaNula = 0L;
		boolean achouAreaContruida = false;

		if (faixaHabilitadas.length == 0) {
			for (TabelaAuxiliar tabelaAux : listaDadosFaixaAux) {
				tabelaAux.setHabilitado(Boolean.FALSE);
				tabelaAux.setChavePrimaria(chavePrimariaNula);
			}
		}

		for (TabelaAuxiliar tabelaAuxiliar : listaDadosFaixaAux) {
			for (int i = 0; i < faixaHabilitadas.length; i++) {
				if (Long.parseLong(faixaHabilitadas[i]) == 0) {
					tabelaAuxiliar.setHabilitado(Boolean.FALSE);
					tabelaAuxiliar.setChavePrimaria(chavePrimariaNula);
				} else {
					if (tabelaAuxiliar.getChavePrimaria() == Long.parseLong(faixaHabilitadas[i])) {
						tabelaAuxiliar.setHabilitado(Boolean.TRUE);
						tabelaAuxiliar.setChavePrimaria(chavePrimariaNula);
						achouAreaContruida = true;
						break;
					}
				}
			}

			if (!achouAreaContruida) {
				tabelaAuxiliar.setHabilitado(Boolean.FALSE);
				tabelaAuxiliar.setChavePrimaria(chavePrimariaNula);
			}
		}

		return listaDadosFaixaAux;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarAnoMesReferencia(java.lang.Integer)
	 */
	@Override
	public void verificarAnoMesReferencia(Integer anoMesReferencia) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if (!Util.validarAnoMes(anoMesReferencia)) {
			throw new NegocioException(GrupoFaturamento.ERRO_ANO_MES_REFERENCIA_INVALIDO);
		}

		ParametroSistema parametroSistema = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);

		if (anoMesReferencia < Integer.parseInt(parametroSistema.getValor())) {
			throw new NegocioException(GrupoFaturamento.ERRO_ANO_MES_REFERENCIA_MENOR_PARAMETRO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarPeriodicidadeNumeroCiclos(java.lang.Integer,
	 * br.com.ggas.medicao.rota.Periodicidade)
	 */
	@Override
	public void verificarPeriodicidadeNumeroCiclos(Integer numeroCiclo, Periodicidade periodicidade) throws NegocioException {

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();

		Periodicidade periodicidadeAux = controladorRota.obterPeriodicidade(periodicidade.getChavePrimaria());

		if (numeroCiclo > periodicidadeAux.getQuantidadeCiclo()) {
			throw new NegocioException(ControladorRota.ERRO_NEGOCIO_NUMERO_CICLOS_INVALIDOS, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#verificarRemocaoMarcaMedidor(java.util.Map, java.lang.String)
	 */
	@Override
	public void verificarRemocaoMarcaMedidor(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		Long marcaEmUso = controladorMedidor.consultarMarcaMedidorEmUso((Long) filtro.get("idMarcaMedidor"));

		if (marcaEmUso != null && marcaEmUso > 0) {
			throw new NegocioException(mensagem, true);
		}

	}

	/**
	 * Verificar remocao marca vazao corretor.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoMarcaVazaoCorretor(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();

		Long marcaEmUso = controladorVazaoCorretor.consultarMarcaCorretorEmUso((Long) filtro.get("idMarcaCorretor"));

		if (marcaEmUso != null && marcaEmUso > 0) {
			throw new NegocioException(mensagem, true);
		}

	}

	/**
	 * Verificar remocao modelo medidor.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoModeloMedidor(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		Long modeloEmUso = controladorMedidor.consultarModeloMedidorEmUso((Long) filtro.get("idModeloMedidor"));

		if (modeloEmUso != null && modeloEmUso > 0) {
			throw new NegocioException(mensagem, true);
		}

	}

	/**
	 * Verificar remocao modelo corretor.
	 *
	 * @param filtro
	 *            the filtro
	 * @param mensagem
	 *            the mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void verificarRemocaoModeloCorretor(Map<String, Object> filtro, String mensagem) throws NegocioException {

		ControladorVazaoCorretor controladorVazaoCorretor = ServiceLocator.getInstancia().getControladorVazaoCorretor();

		Long modeloEmUso = controladorVazaoCorretor.consultarModeloCorretorEmUso((Long) filtro.get("idModeloCorretor"));

		if (modeloEmUso != null && modeloEmUso > 0) {
			throw new NegocioException(mensagem, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar#validarExistePrincipal(java.util.Collection,
	 * br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar)
	 */
	// Verifica se existe Relacionamento Principal
	@SuppressWarnings("unused")
	@Override
	public void validarExistePrincipal(Collection<TabelaAuxiliar> listaTabelaAuxiliar, TabelaAuxiliar tabelaAuxiliar)
					throws NegocioException {

		if (tabelaAuxiliar.isPrincipal() && tabelaAuxiliar.isHabilitado()) {
			for (TabelaAuxiliar tabela : listaTabelaAuxiliar) {
				if (tabela.isPrincipal() && tabela.isHabilitado() && tabela.getChavePrimaria() != tabelaAuxiliar.getChavePrimaria()) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_RELACIONAMENTO_PRINCIPAL_EXISTENTE, true);
				}
			}
		}
	}

	/**
	 * Validar unidade federativa descricao existente.
	 *
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public void validarUnidadeFederativaDescricaoExistente(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(tabelaAuxiliar.getClass().getName());
		hql.append(" where 1=1 ");

		if (tabelaAuxiliar.getPais() != null) {
			hql.append(" and pais = :pais ");
		}
		if (tabelaAuxiliar.getPais() != null) {
			hql.append(" and descricao = :descricao ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (tabelaAuxiliar.getPais() != null) {
			query.setLong("pais", tabelaAuxiliar.getPais().getChavePrimaria());
		}
		if (tabelaAuxiliar.getDescricao() != null) {
			query.setString("descricao", tabelaAuxiliar.getDescricao());
		}

		listaTabelaAuxiliar = query.list();

		if (listaTabelaAuxiliar != null && !listaTabelaAuxiliar.isEmpty()) {
			for (TabelaAuxiliar unidadeFederativa : listaTabelaAuxiliar) {
				if (chavePrimaria != unidadeFederativa.getChavePrimaria()) {
					this.verificarListaTabelaAuxiliar(listaTabelaAuxiliar, chavePrimaria,
									ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_DESCRICAO_UNIDADE_FEDERATIVA, titulo);
				}
			}
		}
	}

	/**
	 * Validar unidade federativa sigla existente.
	 *
	 * @param tabelaAuxiliar
	 *            the tabela auxiliar
	 * @param titulo
	 *            the titulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public void validarUnidadeFederativaSiglaExistente(TabelaAuxiliar tabelaAuxiliar, String titulo) throws NegocioException {

		Long chavePrimaria = tabelaAuxiliar.getChavePrimaria();

		Collection<TabelaAuxiliar> listaTabelaAuxiliar = null;

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(tabelaAuxiliar.getClass().getName());
		hql.append(" where 1=1 ");

		if (tabelaAuxiliar.getPais() != null) {
			hql.append(" and pais = :pais ");
		}
		if (tabelaAuxiliar.getPais() != null) {
			hql.append(" and sigla = :sigla ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (tabelaAuxiliar.getPais() != null) {
			query.setLong("pais", tabelaAuxiliar.getPais().getChavePrimaria());
		}
		if (tabelaAuxiliar.getDescricao() != null) {
			query.setString("sigla", tabelaAuxiliar.getSigla());
		}

		listaTabelaAuxiliar = query.list();

		if (listaTabelaAuxiliar != null && !listaTabelaAuxiliar.isEmpty()) {
			for (TabelaAuxiliar unidadeFederativa : listaTabelaAuxiliar) {
				if (chavePrimaria != unidadeFederativa.getChavePrimaria()) {
					throw new NegocioException(ControladorTabelaAuxiliar.ERRO_NEGOCIO_INSERIR_SIGLA_UNIDADE_FEDERATIVA, titulo);
				}
			}
		}
	}
}
