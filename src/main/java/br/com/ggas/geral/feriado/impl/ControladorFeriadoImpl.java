/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.feriado.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.feriado.Feriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos
 * de consulta e persistência
 * relativos a entidade Feriado.
 */
class ControladorFeriadoImpl extends ControladorNegocioImpl implements ControladorFeriado {

	private static final int TRES_DIAS = 3;
	private static final int DOIS_DIAS = 2;
	public static final String ID_MUNICIPIO = "idMunicipio";
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Feriado.BEAN_ID_FERIADO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Feriado.BEAN_ID_FERIADO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.feriado.ControladorFeriado#isDiaUtil(java.util.Date, java.lang.Long)
	 */
	@Override
	public boolean isDiaUtil(Date data, Long idMunicipio, Boolean isConsiderarDiaUtil) throws NegocioException {

		boolean retorno = true;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		if(isConsiderarDiaUtil && (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
			retorno = false;
		}

		if(retorno) {

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select count(chavePrimaria) from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where habilitado = true ");
			hql.append(" and data = :data ");
			if(idMunicipio == null) {
				hql.append(" and indicadorMunicipal = false ");
			} else {
				hql.append(" and ((municipio.chavePrimaria = :idMunicipio ");
				hql.append(" and indicadorMunicipal = true) ");
				hql.append(" or indicadorMunicipal = false) ");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setDate("data", data);
			if(idMunicipio != null) {
				query.setLong(ID_MUNICIPIO, idMunicipio);
			}

			Long resultado = (Long) query.uniqueResult();
			if(resultado != null && resultado > 0) {
				retorno = false;
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.feriado.ControladorFeriado#isDiaUtil(java.util.Date)
	 */
	@Override
	public boolean isDiaUtil(Date data) throws NegocioException {

		return isDiaUtil(data, null, Boolean.TRUE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.feriado.ControladorFeriado#obterProximoDiaUtil(java.util.Date, long)
	 */
	@Override
	public Date obterProximoDiaUtil(Date data, long idMunicipio, Boolean isConsiderarDiaUtil) throws NegocioException {

		DateTime proximaDataUtil = new DateTime(data);
		while(!this.isDiaUtil(proximaDataUtil.toDate(), idMunicipio, isConsiderarDiaUtil)) {
			proximaDataUtil = proximaDataUtil.plusDays(1);
		}

		return proximaDataUtil.toDate();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.feriado.ControladorFeriado#obterProximoDiaUtil(java.util.Date)
	 */
	@Override
	public Date obterProximoDiaUtil(Date data) throws NegocioException {

		DateTime proximaDataUtil = new DateTime(data);
		while(!this.isDiaUtil(proximaDataUtil.toDate())) {
			proximaDataUtil = proximaDataUtil.plusDays(1);
		}

		return proximaDataUtil.toDate();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.feriado.ControladorFeriado#adicionarDiasUteis(java.util.Date, java.lang.Integer, long)
	 */
	@Override
	public Date adicionarDiasUteis(Date data, Integer quantidadeDias, long idMunicipio) throws NegocioException {

		DateTime dataUtil = new DateTime(data);

		int i = 0;
		do {
			dataUtil = dataUtil.plusDays(1);

			if(this.isDiaUtil(dataUtil.toDate(), idMunicipio, Boolean.TRUE)) {
				i++;
			}

		} while(i < quantidadeDias);

		return dataUtil.toDate();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.feriado.ControladorFeriado#consultarFeriado(java.util.Map)
	 */
	@Override
	public Collection<Feriado> consultarFeriado(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long idMunicipio = (Long) filtro.get(ID_MUNICIPIO);
			if(idMunicipio != null && idMunicipio > 0) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}
		}

		return criteria.list();
	}
	
	/**
	 * Consulta as datas dos feriados por munícipio.
	 * 
	 * @param municipios - {@link Collection}
	 * @return datas de feriado por Município - {@link Map}
	 */
	@Override
	@Cacheable("feriados")
	public Map<Long, Collection<Date>> consultarFeriados(Collection<Municipio> municipios) {

		Map<Long, Collection<Date>> feriados = null;
		
		if (!Util.isNullOrEmpty(municipios)) {
			Long[] chavesMunicipios = Util.collectionParaArrayChavesPrimarias(municipios);
			
			StringBuilder hql = new StringBuilder();
			hql.append(" select municipio.chavePrimaria, feriado.data ");
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" as feriado ");
			hql.append(" inner join feriado.municipio as municipio ");
			hql.append(" where feriado.habilitado = true ");
			hql.append(" and (municipio.chavePrimaria IN (:chavesMunicipios) ");
			hql.append(" and feriado.indicadorMunicipal = true) ");

			Query query = getSession().createQuery(hql.toString());

			query.setParameterList("chavesMunicipios", chavesMunicipios);

			feriados = agruparFeriadosPorMunicipio(query.list());
		}

		return feriados;
	}

	/**
	 * Agrupa datas de feriados por Município.
	 * 
	 * @param resultadoConsulta
	 *            - {@link List}
	 * @return datas de feriado por Município - {@link Map}
	 */
	private Map<Long, Collection<Date>> agruparFeriadosPorMunicipio(List<Object[]> resultadoConsulta) {
		Collection<Date> datas = null;

		Map<Long, Collection<Date>> feriados = new HashMap<>();

		for (Object[] objetos : resultadoConsulta) {
			
			Long chaveMunicipio = (Long) objetos[0];
			Date dataFeriado = (Date) objetos[1];

			if (!feriados.containsKey(chaveMunicipio)) {
				datas = new HashSet<>();
				feriados.put(chaveMunicipio, datas);
			} else {
				datas = feriados.get(chaveMunicipio);
			}

			datas.add(dataFeriado);

		}
		return feriados;
	}

	/**
	 * Consultar datas de feriados não municipais.
	 * 
	 * @return datas de feriados - {@link Collection}
	 */
	@Override
	@Cacheable("feriadosNaoMunicipais")
	public Collection<Date> consultarFeriadosNaoMunicipais() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select feriado.data ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" as feriado ");
		hql.append(" where feriado.habilitado = true ");
		hql.append(" and feriado.indicadorMunicipal = false ");

		Query query = getSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Caso a data seja um domingo ou sábado e não esteja presente na coleção de
	 * feriados, passada por parâmetro, este método retorna {@code true}. Caso
	 * contrário, retorna {@code false}.
	 * 
	 * @param feriados
	 *            - {@link Collection}.
	 * @param data
	 *            - {@link Date}
	 * @return é um dia útil {@link Boolean}
	 */
	@Override
	public Boolean isDiaUtil(Collection<Date> feriados, Date data) {

		Boolean diaUtil = Boolean.TRUE;

		Calendar calendario = Calendar.getInstance();

		calendario.setTime(data);

		if (calendario.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				|| calendario.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			diaUtil = Boolean.FALSE;
		} else {
			if (!Util.isNullOrEmpty(feriados)) {
				diaUtil = !feriados.contains(data);
			}
		}

		return diaUtil;
	}
	/**
	 * Validar um dia não util.
	 * 
	 * @param date - {@link Date}
	 * @return quantidade de dias - {@link int}
	 */
	@Override
	public int validarDiaNaoUtil(Date data)throws NegocioException{
		int count = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		
		if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			count = count+DOIS_DIAS;
		}else if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){
			count = count+TRES_DIAS;
		}else{
			count ++;
		}
		return count;
	}
}
