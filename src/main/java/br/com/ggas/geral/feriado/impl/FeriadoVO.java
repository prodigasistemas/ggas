package br.com.ggas.geral.feriado.impl;

import java.io.Serializable;

/**
 * Classe responsável rela representação dos dados de um feriado.
 * 
 * @author orube
 *
 */
public class FeriadoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4812877987768929384L;

	private String datasFeriado;

	private String descricao;

	private String indicadorTipo = "false";

	private String indicadorMunicipal = "false";

	private Long idMunicipio;

	private String habilitado = "true";

	public String getDatasFeriado() {
		return datasFeriado;
	}

	public void setDatasFeriado(String datasFeriado) {
		this.datasFeriado = datasFeriado;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getIndicadorTipo() {
		return indicadorTipo;
	}

	public void setIndicadorTipo(String indicadorTipo) {
		this.indicadorTipo = indicadorTipo;
	}

	public String getIndicadorMunicipal() {
		return indicadorMunicipal;
	}

	public void setIndicadorMunicipal(String indicadorMunicipal) {
		this.indicadorMunicipal = indicadorMunicipal;
	}

	public Long getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Long idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
