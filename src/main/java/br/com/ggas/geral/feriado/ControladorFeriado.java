/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.feriado;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável por fornecer os métodos
 * de consulta e persistência
 * relativos a entidade Feriado.
 */
public interface ControladorFeriado extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_FERIADO = "controladorFeriado";

	/**
	 * Método responsável por verificar se uma
	 * data é um dia útil.
	 * 
	 * @param data
	 *            the data
	 * @param idMunicipio
	 *            A chave primária do município.
	 * @return Coleção de feriados
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean isDiaUtil(Date data, Long idMunicipio, Boolean isConsiderarDiaUtil) throws NegocioException;

	/**
	 * Método responsável por verificar se uma
	 * data é um dia útil.
	 * 
	 * @param data
	 *            the data
	 * @return Coleção de feriados
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean isDiaUtil(Date data) throws NegocioException;

	/**
	 * Método responsável por procurar o próximo
	 * dia útil considerando fins de semana e
	 * feriados
	 * municipais e nacionais.
	 * 
	 * @param data
	 *            A data a ser alterada.
	 * @param idMunicipio
	 *            A chave primária do munícipio
	 * @return Uma data com um dia útil.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date obterProximoDiaUtil(Date data, long idMunicipio, Boolean isConsiderarDiaUtil) throws NegocioException;

	/**
	 * Método responsável por procurar o próximo
	 * dia útil considerando fins de semana e
	 * feriados.
	 * 
	 * @param data
	 *            A data a ser alterada.
	 * @return Uma data com um dia útil.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date obterProximoDiaUtil(Date data) throws NegocioException;

	/**
	 * Método responsável por adicionar dias úteis
	 * em uma determinada data.
	 * 
	 * @param data
	 *            Data que será adicionada os
	 *            dias.
	 * @param quantidadeDias
	 *            Quantidade de dias para
	 *            adicionar.
	 * @param idMunicipio
	 *            Chave primária do município.
	 * @return A data com dias úteis adicionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invoca
	 */
	Date adicionarDiasUteis(Date data, Integer quantidadeDias, long idMunicipio) throws NegocioException;

	/**
	 * Consultar feriado.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Feriado> consultarFeriado(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consulta as datas dos feriados por munícipio.
	 * 
	 * @param municipios - {@link Collection}
	 * @return datas de feriado por Município - {@link Map}
	 */
	Map<Long, Collection<Date>> consultarFeriados(Collection<Municipio> municipios);

	/**
	 * Caso a data seja um domingo ou sábado e não esteja presente na coleção de feriados, passada por parâmetro, este método retorna
	 * {@code true}. Caso contrário, retorna {@code false}.
	 * 
	 * @param feriados - {@link Collection}.
	 * @param data - {@link Date}
	 * @return é um dia útil {@link Boolean}
	 */
	Boolean isDiaUtil(Collection<Date> feriados, Date data);

	/**
	 * Consultar datas de feriados não municipais.
	 * 
	 * @return datas de feriados - {@link Collection}
	 */
	Collection<Date> consultarFeriadosNaoMunicipais();

	/**
	 * Validar um dia não util.
	 * 
	 * @param data - {@link Date}
	 * @return quantidade de dias - {@link int}
	 * @throws NegocioException {@link NegocioException}
	 */
	int validarDiaNaoUtil(Date data)throws NegocioException;
}
