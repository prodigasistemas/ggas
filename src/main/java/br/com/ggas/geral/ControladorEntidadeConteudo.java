/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral;

import java.util.Collection;
import java.util.List;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;

/**
 * 
 * Controlador Entidade Conteudo
 *
 */
public interface ControladorEntidadeConteudo extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO = "controladorEntidadeConteudo";

	String ERRO_NEGOCIO_REMOVER_ENTIDADE_CONTEUDO = "ERRO_NEGOCIO_REMOVER_ENTIDADE_CONTEUDO";

	/**
	 * Método responsável por obter a lista de
	 * garantias financeiras.
	 * 
	 * @return coleção de garantias financeiras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoGarantiaFinanceira() throws NegocioException;

	/**
	 * Método responsável por obter a lsita de
	 * tipos de parada.
	 * 
	 * @return Uma coleção de tipos de parada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoParada() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * regime de consumo.
	 * 
	 * @return coleção de regime de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaRegimeConsumo() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * critério de uso.
	 * 
	 * @return coleção de critério de uso.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaCriterioUso() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * forma de pagamento.
	 * 
	 * @return coleção de forma de pagamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaFormaPagamento() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * tipo de faturamento.
	 * 
	 * @return coleção de tipo de faturamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoFaturamento() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * tipo de leitura faturada.
	 * 
	 * @return coleção de tipo de leitura
	 *         faturada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaFaseReferencialVencimento() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * dias úteis e corridos.
	 * 
	 * @return coleção de tipo de dias úteis e
	 *         corridos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaOpcaoFaseRefVencimento() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * dias úteis e corridos para ação de
	 * cobrança.
	 * 
	 * @return coleção de tipo de dias úteis e
	 *         corridos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaOpcaoDias() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * hora inicial do dia.
	 * 
	 * @return coleção de hora inicial do dia.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaHoraInicialDia() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * itens da fatura.
	 * 
	 * @return coleção de itens da fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaItemFatura() throws NegocioException;

	/**
	 * Método responsável por obter a lista de endereco padrão
	 * 
	 * @return coleção de itens endereco padrao
	 * @throws NegocioException - {@link NegocioException}
	 *             
	 */
	Collection<EntidadeConteudo> obterListaEnderecoPadrao() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * periodicidade TOP.
	 * 
	 * @return coleção de periodicidade TOP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaPeriodicidadeTOP() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * periodicidade SOP.
	 * 
	 * @return coleção de periodicidade SOP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaPeriodicidadeSOP() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * periodicidade SOP.
	 * 
	 * @return coleção de periodicidade SOP.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public Collection<EntidadeConteudo> listarSituacaoPagamento() ;
	
	/**
	 * Método responsável por obter a lista de
	 * consumo referencial.
	 * 
	 * @return coleção de consumo referencial..
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaConsumoReferencial() throws NegocioException;

	/**
	 * Método responsável por listar locais de
	 * amostragem de PCS.
	 * 
	 * @return coleção de locais de amostragem de
	 *         PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarLocalAmostragemPCS() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * responsabilidades de ContratoCliente.
	 * 
	 * @return Uma coleção de responsabilidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaResponsabilidades() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * modalidades de uso do ponto de consumo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaModalidadesUso() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * tipos de combustíveis.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTiposCombustiveis() throws NegocioException;

	/**
	 * Método responsável por validar se é
	 * permitido a seleção de uma fase de acordo
	 * com uma opção de vencimento.
	 * 
	 * @param chaveOpcaoVencimento
	 *            A chave primária da opção de
	 *            vencimento.
	 * @return true caso permita.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean permiteFaseReferencialVencimentoPorOpcaoVencimento(long chaveOpcaoVencimento) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * amortizações cadastradas.
	 * 
	 * @return Uma coleção de amortizações.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarAmortizacoes() throws NegocioException;

	/**
	 * Método responsável por Obter um Tipo
	 * Convênio do sistema.
	 * 
	 * @param chaveEntidadeClasse
	 *            A Chave Primaria do Entidade
	 *            Conteudo
	 * @return Uma Entidade Conteudo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterTipoConvenio(Long chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * Amortização.
	 * 
	 * @return coleção de Amortização.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaAmortizacao() throws NegocioException;

	/**
	 * Método responsável por obter uma entidade
	 * conteúdo por código.
	 * 
	 * @param codigo
	 *            O código
	 * @param classe
	 *            the classe
	 * @return Uma entidade conteúdo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterPorCodigo(String codigo, String classe) throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * tipo valor.
	 * 
	 * @return coleção de tipo valor
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoValor() throws NegocioException;

	/**
	 * Método responsável por obter a entidade
	 * conteúdo padrão de uma classe.
	 * 
	 * @param chaveEntidadeClasse
	 *            Chave da entidade classe.
	 * @return Uma EntidadeConteudo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterEntidadeConteudoPadraoPorClasse(String chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de calculo.
	 * 
	 * @return Uma coleção de tipos de parada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoCalculo() throws NegocioException;

	/**
	 * Método responsável por obter a lsita das
	 * bases de apuração.
	 * 
	 * @return Uma coleção de tipos de parada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaBaseApuracao() throws NegocioException;

	/**
	 * Método responsável por obter a lsita das
	 * bases de apuração.
	 * 
	 * @return Uma coleção de tipos de parada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaUnidadeMonetaria() throws NegocioException;

	/**
	 * Método responsável por obter a lsita das
	 * bases de apuração.
	 * 
	 * @param chaveEntidadeClasse
	 *            the chave entidade classe
	 * @return Uma coleção de tipos de parada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterItemFatura(Long chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por obter a unidade
	 * monetaria.
	 * 
	 * @param chaveEntidadeClasse
	 *            the chave entidade classe
	 * @return Uma a unidade monetaria.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterUnidadeMonetaria(Long chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por obter o tipo do
	 * calculo.
	 * 
	 * @param chaveEntidadeClasse
	 *            the chave entidade classe
	 * @return Um tipo de calculo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterTipoCalculo(Long chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por obter a base de
	 * apuração.
	 * 
	 * @param chaveEntidadeClasse
	 *            the chave entidade classe
	 * @return Uma base de apuração.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterBaseApuracao(Long chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * Periodicidade Cobrança.
	 * 
	 * @return coleção de tipo valor
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaPeriodicidadeCobranca() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * Periodicidade Juros.
	 * 
	 * @return coleção de tipo valor
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaPeriodicidadeJuros() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * Dias após início cobrança.
	 * 
	 * @return coleção de tipo valor
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaDiasAposInicioCobranca() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * motivos de cancelamento de fatura.
	 * 
	 * @return Uma coleção de motivos de
	 *         cancelamento de fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaMotivoCancelamento() throws NegocioException;

	/**
	 * Método responsável por obter a lsita dos
	 * tipos de valores.
	 * 
	 * @return Uma coleção de tipos de valores
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoValorTarifa() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * data referência cambial.
	 * 
	 * @return coleção de data referência cambial
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaDataReferenciaCambial() throws NegocioException;

	/**
	 * Método responsável por obter a lista opções
	 * de dias da cotação.
	 * 
	 * @return coleção de opções de dias da
	 *         cotação
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaDiaCotacao() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * ítens de fatura do preço do gás.
	 * 
	 * @return Uma coleção de itens de fatura do
	 *         preço do gás.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaItemFaturaPrecoGas() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * motivos de inclusão da fatura.
	 * 
	 * @return Uma coleção de motivos de inclusão
	 *         de fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaMotivoInclusao() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * motivos para refaturar fatura.
	 * 
	 * @return Uma coleção de motivos para
	 *         refaturar fatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaMotivoRefaturar() throws NegocioException;

	/**
	 * Retorna uma lista os contratos de compra.
	 * 
	 * @return lista de contrato de compra
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaContratoCompra() throws NegocioException;

	/**
	 * Retorna uma lista com os tipos de
	 * devoluções.
	 * 
	 * @return lista de tipo de devoluções
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarTipoDevolucao() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * tipos de agrupamento.
	 * 
	 * @return coleção de tipos de agrupamento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterListaTipoAgrupamento() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * tipos de mensagem.
	 * 
	 * @return coleção de tipos de mensagem
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarTipoMensagem() throws NegocioException;

	/**
	 * Método responsável por obter a lista de
	 * formas de cobrança.
	 * 
	 * @return coleção de formas de cobrança
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarFormaCobranca() throws NegocioException;

	/**
	 * *
	 * Método responsável por obter um tipo de
	 * operacao de um documento fiscal.
	 * 
	 * @param chaveEntidadeClasse
	 *            A Chave Primaria do Entidade
	 *            Conteudo
	 *            return Um tipo de operação de um
	 *            documento fiscal
	 * @return the entidade conteudo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */

	EntidadeConteudo obterTipoOperacaoDocumentoFiscal(Long chaveEntidadeClasse) throws NegocioException;

	/**
	 * Método responsável por listar as bases de
	 * apuração por penalidade.
	 * 
	 * @return basesApuracaoPenalidadeMaiorMenor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarBasesApuracaoPenalidadeMaiorMenor() throws NegocioException;

	/**
	 * Retorna uma lista de tipos de multa
	 * recisoria.
	 * 
	 * @return lista de tipos de multa recisoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoMultaRecisoria() throws NegocioException;

	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @param chaveEntidade the chave entidade conteudo
	 *            
	 * @return EntidadeConteudo
	 */
	EntidadeConteudo obter(Long chaveEntidade);
	
	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @param chaveEntidadeConteudo
	 *            the chave entidade conteudo
	 * @return EntidadeConteudo
	 */
	EntidadeConteudo obterEntidadeConteudo(Long chaveEntidadeConteudo);

	/**
	 * Método responsável por obter uma {@link EntidadeConteudo} 
	 * por código de constante do sistema.
	 * @param codigo - {@link String}
	 * @return entidade de conteúdo - {@link EntidadeConteudo}
	 * @throws NegocioException - {@link NegocioException}
	 */
	EntidadeConteudo obterEntidadeConteudoPorConstanteSistema(String codigo) 
					throws NegocioException;

	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @return EntidadeConteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarTipoParametro() throws NegocioException;

	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoAtributo() throws NegocioException;

	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoIntegracao() throws NegocioException;

	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @return EntidadeConteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeClasse> listarEntidadeClasse() throws NegocioException;

	/**
	 * Retorna uma lista de Entidade Conteudo.
	 * 
	 * @param codigoEntidadeClasse
	 *            the codigo entidade classe
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarEntidadeConteudo(String codigoEntidadeClasse) throws NegocioException;

	/**
	 * Obtém a entidadeConteudo através do codigo.
	 * 
	 * @return EntidadeConteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClasseUnidade> listarClasseUnidade() throws NegocioException;

	/**
	 * Criar entidade classe.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarEntidadeClasse();

	/**
	 * Listar entidade conteudo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarEntidadeConteudo(Long chavePrimaria) throws NegocioException;

	/**
	 * Criar unidade classe.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarUnidadeClasse();

	/**
	 * Obter lista motivo devolucao.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaMotivoDevolucao() throws NegocioException;

	/**
	 * Listar entidade classe.
	 * 
	 * @param codigoEntidadeClasse
	 *            the codigo entidade classe
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeClasse> listarEntidadeClasse(String codigoEntidadeClasse) throws NegocioException;

	/**
	 * Obter entidade conteudo por chave.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param classeEntidade
	 *            the classe entidade
	 * @return the entidade conteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EntidadeConteudo obterEntidadeConteudoPorChave(Long chavePrimaria, Long classeEntidade) throws NegocioException;

	/**
	 * Listar entidade conteudo por descricao entidade classe.
	 * 
	 * @param descricaoEntidadeClasse
	 *            the descricao entidade classe
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarEntidadeConteudoPorDescricaoEntidadeClasse(String descricaoEntidadeClasse) throws NegocioException;

	/**
	 * Obter lista equipamento.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaEquipamento() throws NegocioException;

	/**
	 * Obter lista turno.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTurno() throws NegocioException;

	/**
	 * Listar tipo anexo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarTipoAnexo() throws NegocioException;

	/**
	 * Listar dias vencimento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Long> listarDiasVencimento(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter lista tipo modelo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoModelo() throws NegocioException;

	/**
	 * Obter lista referencia qf parada programada.
	 * 
	 * @return the collection
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaReferenciaQFParadaProgramada() throws NegocioException;

	/**
	 * Obter lista tipo apuracao.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoApuracao() throws NegocioException;

	/**
	 * Obter lista preco cobranca.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaPrecoCobranca() throws NegocioException;

	/**
	 * Obter lista apuracao parada programada.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaApuracaoParadaProgramada() throws NegocioException;

	/**
	 * Obter lista apuracao parada nao programada.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaApuracaoParadaNaoProgramada() throws NegocioException;

	/**
	 * Obter lista apuracao caso fortuito.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaApuracaoCasoFortuito() throws NegocioException;

	/**
	 * Obter lista apuracao falha fornecimento.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaApuracaoFalhaFornecimento() throws NegocioException;

	/**
	 * Obter lista tipo agrupamento contrato.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoAgrupamentoContrato() throws NegocioException;

	/**
	 * Obter lista periodicidade retirada maior menor.
	 * 
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterListaPeriodicidadeRetiradaMaiorMenor();

	/**
	 * Obter lista status imovel.
	 * 
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterListaStatusImovel();

	/**
	 * Obter status imovel por codigo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterStatusImovelPorCodigo(Long chavePrimaria);

	/**
	 * Obter lista fornecedores combustiveis.
	 * 
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterListaFornecedoresCombustiveis();

	/**
	 * Obter lista aparelhos maior indice consumo.
	 * 
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterListaAparelhosMaiorIndiceConsumo();

	/**
	 * Obter lista tipos fontes energeticas.
	 * 
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterListaTiposFontesEnergeticas();

	/**
	 * Obter lista estado rede.
	 * 
	 * @return the collection
	 */
	Collection<EntidadeConteudo> obterListaEstadoRede();

	/**
	 * Método responsável por obter a lista de motivos de complemento de fatura.
	 * 
	 * @return Uma coleção de motivos de complemento de fatura.
	 */
	Collection<EntidadeConteudo> obterListaMotivoCcomplementoFatura();

	/**
	 * Obter lista tipo aplicacao tributo aliquota.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoAplicacaoTributoAliquota() 
					throws NegocioException;

	/**
	 * Obter entidade conteudo tipo aplicacao tributo aliquota percentual.
	 *
	 * @return the entidade conteudo
	 * @throws NegocioException the negocio exception
	 */
	EntidadeConteudo obterEntidadeConteudoTipoAplicacaoTributoAliquotaPercentual() 
					throws NegocioException;

	/**
	 * Obter lista tipo aplicacao reducao base calculo.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoAplicacaoReducaoBaseCalculo() 
					throws NegocioException;

	/**
	 * Obter lista tipo vigencia substituicao tributaria.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EntidadeConteudo> obterListaTipoVigenciaSubstituicaoTributaria()
			throws NegocioException;

	/**
	 * Listar tipos de convenio.
	 *
	 * @return a coleção
	 * @throws NegocioException em caso de erro
	 */
	Collection<EntidadeConteudo> listarTiposDeConvenio() throws NegocioException;

	/**
	 * Listar Status da NFE por Documento Fiscal
	 * @return status de nfe de documentos fiscais
	 * @throws NegocioException caso ocorra falha na operação
	 */
	Collection<EntidadeConteudo> listarStatusNFEDocumentoFiscal() throws NegocioException;

	/**
	 * Listar Entidade Conteudo
	 * @param descricao
	 * 			the descricao
	 * @return a entidade
	 * @throws NegocioException em caso de erro
	 */
	EntidadeConteudo obterEntidadeConteudoLikeDescricao(String descricao) throws NegocioException;
	
	/**
	 * Listar Tipo de Consumo de Faturamento
	 * @return a entidade
	 * @throws NegocioException em caso de erro
	 */
	Collection<EntidadeConteudo> obterListaTipoConsumoFaturamento() throws NegocioException;


	/**
	 * Método responsável por atualizar uma entidade conteúdo no cache.
	 * @param entidadeConteudo - {@link EntidadeConteudo}
	 */
	void updateCache(EntidadeConteudo entidadeConteudo);

	/**
	 * Metodo para consultar entidade conteudo pela entidade classe e pela descricao
	 * @param descricaoEntidadeClasse - descricao entidade clase
	 * @param descricao - descricao entidade conteudo
	 * @return Entidade Conteudo consultada
	 * @throws NegocioException the Negocio Exception
	 */
	EntidadeConteudo listarEntidadeConteudoPorEntidadeClasseEDescricao(String descricaoEntidadeClasse,
			String descricao) throws NegocioException;
	
	EntidadeConteudo obterEntidadeConteudoPorChaveEntidadeClasse(Long chaveEntidadeClasse) throws NegocioException;

	Collection<EntidadeConteudo> listaEntidadeConteudoPorDescricaoEntidadeClasse(String descricaoEntidadeClasse);
}
