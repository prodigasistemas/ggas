/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.negocio;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * 
 */
public interface ControladorNegocio {

	/**
	 * Método responsável por criar uma entidade.
	 * 
	 * @return Uma nova entidade de negócio
	 */
	EntidadeNegocio criar();

	/**
	 * Método responsável por inserir uma
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @return long a chave primária da entidade
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	long inserir(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por remover uma
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void remover(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por remover uma
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @param fisicamente
	 *            Se a exclusão deve ser fisica ou
	 *            lógica
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void remover(EntidadeNegocio entidadeNegocio, boolean fisicamente) throws NegocioException;

	/**
	 * Método responsável por remover varias
	 * entidades.
	 * 
	 * @param chavesPrimarias
	 *            Uma lista de chaves
	 * @param dadosAuditoria
	 *            Os dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void remover(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por obter uma entidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade de
	 *            negócio
	 * @return EntidadeBase
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	EntidadeNegocio obter(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma entidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade de
	 *            negócio
	 * @param propriedadesLazy
	 *            Os atributos que serão
	 *            inicializados
	 * @return entidadeBase
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	EntidadeNegocio obter(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter uma entidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade de
	 *            negócio
	 * @param classe
	 *            A classe da entidade de negócio
	 * @return entidadeBase
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	EntidadeNegocio obter(long chavePrimaria, Class<?> classe) throws NegocioException;

	/**
	 * Método responsável por obter uma entidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade de
	 *            negócio
	 * @param classe
	 *            A classe da entidade de negócio
	 * @param propriedadesLazy
	 *            Os atributos que serão
	 *            inicializados
	 * @return entidadeBase
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	EntidadeNegocio obter(long chavePrimaria, Class<?> classe, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter uma entidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade de
	 *            negócio
	 * @param habilitado
	 *            Indicador se a entidade deve
	 *            estar habilitada ou não. Caso
	 *            nulo não restringe
	 * @param propriedadesLazy
	 *            Os atributos que serão
	 *            inicializados
	 * @return entidadeBase
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	EntidadeNegocio obter(long chavePrimaria, Boolean habilitado, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter uma entidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da entidade de
	 *            negócio
	 * @param classe
	 *            A classe da entidade de negócio
	 * @param habilitado
	 *            Indicador se a entidade deve
	 *            estar habilitada ou não. Caso
	 *            nulo não restringe
	 * @param propriedadesLazy
	 *            Os atributos que serão
	 *            inicializados
	 * @return entidadeBase
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	EntidadeNegocio obter(long chavePrimaria, Class<?> classe, Boolean habilitado, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por atualizar uma
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void atualizar(EntidadeNegocio entidadeNegocio) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por atualizar uma
	 * entidade durante a execucao de um processo batch.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @param classe
	 *            the classe
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void atualizarEmBatch(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por atualizar uma
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @param classe
	 *            A classe da entidade
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void atualizar(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por tratar a
	 * pre-inserção da entidade, deverá ser
	 * sobrescrito pelas subclasses caso
	 * necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por tratar a
	 * pos-inserção da entidade, deverá ser
	 * sobrescrito pelas subclasses caso
	 * necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void posInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por tratar a
	 * pre-atualização da entidade, deverá ser
	 * sobrescrito pelas subclasses caso
	 * necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por tratar a
	 * pos-atualização da entidade, deverá ser
	 * sobrescrito pelas subclasses caso
	 * necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void posAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por tratar a pre-remoção
	 * da entidade, deverá ser sobrescrito pelas
	 * subclasses caso necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por tratar a pos-remoção
	 * da entidade, deverá ser sobrescrito pelas
	 * subclasses caso necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void posRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por validar os dados da
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */

	void validarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por tratar a
	 * pos-validacao da entidade, deverá ser
	 * sobrescrito pelas subclasses caso
	 * necessário.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void posValidarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método responsável por obter a classe da
	 * entidade.
	 * 
	 * @return Class O tipo da classe da entidade
	 */
	Class<?> getClasseEntidade();

	/**
	 * Método responsável por validar a versão da
	 * entidade.
	 * 
	 * @param entidadeNegocio
	 *            a entidade de negócio
	 * @param classe
	 *            A classe da entidade
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum problema de
	 *             concorrência
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	void validarVersaoEntidade(EntidadeNegocio entidadeNegocio, Class<?> classe) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por obter todas as
	 * entidades.
	 * 
	 * @return Uma coleção de entidades
	 * @throws NegocioException
	 *             NegocioException Caso ocorra
	 *             alguma violação nas regras de
	 *             negócio
	 */
	Collection<?> obterTodas() throws NegocioException;

	/**
	 * Método responsável por obter as entidades
	 * habilitadas ou não.
	 * 
	 * @param habilitado
	 *            Se True ou False
	 * @return Uma coleção de entidades
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	Collection<?> obterTodas(boolean habilitado) throws NegocioException;

	/**
	 * Método responsável por obter as entidades
	 * habilitadas ou não, de uma determinada
	 * classe.
	 * 
	 * @param classe
	 *            A classe da entidade
	 * @param habilitado
	 *            True para habilitadas, False
	 *            para desabilitadas e null para
	 *            todas
	 * @return Uma coleção de entidades
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	Collection<?> obterTodas(Class<?> classe, Boolean habilitado) throws NegocioException;

	/**
	 * Remover.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param classe
	 *            the classe
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void remover(Long[] chavesPrimarias, Object classe, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar colecao.
	 * 
	 * @param entidade
	 *            the entidade
	 * @param classe
	 *            the classe
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarColecao(Object[] entidade, Class<?> classe) throws NegocioException, ConcorrenciaException;

	/**
	 * Validar existencia entidades.
	 * 
	 * @param classeOrigem
	 *            the classe origem
	 * @param objetoValor
	 *            the objeto valor
	 * @param mapaInconsistencias
	 *            the mapa inconsistencias
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	String validarExistenciaEntidades(EntidadeNegocio classeOrigem, EntidadeNegocio objetoValor, Map<String, Object[]> mapaInconsistencias)
					throws NegocioException;

	/**
	 * Obter.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param classe
	 *            the classe
	 * @param habilitado
	 *            the habilitado
	 * @return the entidade negocio
	 */
	EntidadeNegocio obter(long chavePrimaria, Class<?> classe, Boolean habilitado);

	/**
	 * Método responsável por inserir uma
	 * entidade inativa.
	 * 
	 * @param entidadeNegocio
	 *            A entidade de negócio
	 * @return long a chave primária da entidade
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	long inserirInativo(EntidadeNegocio entidadeNegocio) throws NegocioException;

	/**
	 * Método utilizado, durante os processamentos em batch,
	 * para a inserção de uma entidade de negócio ativa ou 
	 * inativa, de acordo com o parâmetro {@code habilitado}
	*  @param entidadeNegocio - A entidade de negócio
	*  @param habilitado - status da entidade de negócio
	 * @return long - a chave primária da entidade
	 * @throws NegocioException - Caso ocorra alguma violação nas regras de negócio
	 */
	long inserirEmBatch(EntidadeNegocio entidadeNegocio, Boolean habilitado) throws NegocioException;

	void atualizarSaveOrUpdate(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException;

}
