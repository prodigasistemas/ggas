/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.negocio.impl;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.Logger;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;

/**
 * Classe abstrata Entidade Negocio.
 * 
 * 
 */
public abstract class EntidadeNegocioImpl implements EntidadeNegocio {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Date ultimaAlteracao;

	private int versao;

	private long chavePrimaria;

	private boolean habilitado;

	private DadosAuditoria dadosAuditoria;

	private static final Logger LOG = Logger.getLogger(EntidadeNegocioImpl.class);

	@Override
	public boolean isHabilitado() {

		return habilitado;
	}

	@Override
	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	@Override
	public long getChavePrimaria() {

		return chavePrimaria;
	}

	@Override
	public void setChavePrimaria(long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio #getUltimaAlteracao()
	 */
	@Override
	public Date getUltimaAlteracao() {
		Date data = null;
		if (ultimaAlteracao != null) {
			data = (Date) ultimaAlteracao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio
	 * #setUltimaAlteracao(java.util.Date)
	 */
	@Override
	public void setUltimaAlteracao(Date ultimaAlteracao) {
		if (ultimaAlteracao != null) {
			this.ultimaAlteracao = (Date) ultimaAlteracao.clone();
		} else {
			this.ultimaAlteracao = null;
		}
	}

	@Override
	public int getVersao() {

		return versao;
	}

	@Override
	public void setVersao(int versao) {

		this.versao = versao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.EntidadeNegocio
	 * #incrementarVersao()
	 */
	@Override
	public void incrementarVersao() {

		this.versao++;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.
	 * EntidadeNegocioAuditavel
	 * #getDadosAuditoria()
	 */
	@Override
	public DadosAuditoria getDadosAuditoria() {

		return dadosAuditoria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.
	 * EntidadeNegocioAuditavel
	 * #setDadosAuditoria(br
	 * .com.ggas.auditoria.vo.DadosAuditoria)
	 */
	@Override
	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {

		this.dadosAuditoria = dadosAuditoria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.EntidadeNegocio
	 * #validarDados()
	 */

	@Override
	public abstract Map<String, Object> validarDados();

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null) {
			return false;
		}
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof EntidadeNegocioImpl)) {
			return false;
		}
		if(this.getChavePrimaria() > 0) {
			EntidadeNegocioImpl that = (EntidadeNegocioImpl) obj;
			return new EqualsBuilder().append(this.getChavePrimaria(), that.getChavePrimaria()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.getChavePrimaria() > 0) {
			return new HashCodeBuilder(3, 5).append(this.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.EntidadeNegocio
	 * #getLabel()
	 */
	@Override
	public String getLabel() {

		String resultado = "";
		for (Field campo : this.getClass().getDeclaredFields()) {
			campo.setAccessible(true);
			for (String nomeCampo : CAMPOS_TO_STRING) {
				if(campo.getName().equals(nomeCampo)) {
					try {
						resultado = campo.get(this).toString();
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
					break;
				}
			}
		}
		if(resultado.length() == 0) {
			resultado = this.getChavePrimaria() + "";
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.
	 * EntidadeNegocioAuditavel
	 * #validarDadosAuditoria()
	 */
	@Override
	public Map<String, Object> validarDadosAuditoria() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.dadosAuditoria != null) {
			erros = this.dadosAuditoria.validarDados();
		} else {
			stringBuilder.append(DadosAuditoria.ENTIDADE_ROTULO_DADOS_AUDITORIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);

			camposObrigatorios = stringBuilder.toString();

			if(camposObrigatorios.length() > 0) {
				erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
						camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
			}
		}

		return erros;
	}
	
	@Override
	public String hashString() {
		return toString();
	}
	
}
