/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.negocio;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;

/**
 * 
 */
public interface EntidadeNegocio extends Serializable {

	String[] CAMPOS_TO_STRING = {"nome", "descricao"};

	/**
	 * ATRIBUTO_CHAVE_PRIMARIA
	 */
	String ATRIBUTO_CHAVE_PRIMARIA = "chavePrimaria";
	
	/**
	 * ATRIBUTO_CHAVES_PRIMARIAS
	 */
	String ATRIBUTO_CHAVES_PRIMARIAS = "chavesPrimarias";
	
	/**
	 * ATRIBUTO_CHAVE_PRIMARIA
	 */
	String ATRIBUTO_HABILITADO= "habilitado";

	/**
	 * @return the habilitado
	 */
	boolean isHabilitado();

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	void setHabilitado(boolean habilitado);

	/**
	 * @return the ultimaAlteracao
	 */
	Date getUltimaAlteracao();

	/**
	 * @param ultimaAlteracao
	 *            the ultimaAlteracao to set
	 */
	void setUltimaAlteracao(Date ultimaAlteracao);

	/**
	 * @return Retorna o atributo chavePrimaria.
	 */
	long getChavePrimaria();

	/**
	 * @param chavePrimaria
	 *            O valor a ser atribuído ao
	 *            atributo chavePrimaria.
	 */
	void setChavePrimaria(long chavePrimaria);

	/**
	 * @return Retorna o atributo versao.
	 */
	int getVersao();

	/**
	 * @param versao
	 *            O valor a ser atribuído ao
	 *            atributo versao.
	 */
	void setVersao(int versao);

	/**
	 * Método responsável por incrementar a versão
	 * da entidade.
	 */
	void incrementarVersao();

	/**
	 * Validar dados.
	 * 
	 * @return Retorna o atributo map.
	 */
	Map<String, Object> validarDados();

	/**
	 * Retorna uma string representando esse
	 * objeto para exibição em tela.
	 * O valor retornado é o de uma das
	 * propriedade listadas por
	 * "String[] camposToString"
	 * ou o da chave primária caso a entidade não
	 * possua nenhuma das propriedades listadas.
	 * 
	 * @return label
	 */
	String getLabel();

	/**
	 * @return the dadosAuditoria
	 */
	DadosAuditoria getDadosAuditoria();

	/**
	 * @param dadosAuditoria
	 *            the dadosAuditoria to set
	 */
	void setDadosAuditoria(DadosAuditoria dadosAuditoria);

	/**
	 * Validar dados auditoria.
	 * 
	 * @return Retorna o atributo map.
	 */
	Map<String, Object> validarDadosAuditoria();

	/**
	 * Utilizado para mapear objetos atraves de uma string
	 * no metodo Util.trasformarParaMapa.
	 * Caso não seja sobrescrito, este metodo retornara toString().
	 * 
	 * @return string utilizada para mapear objetos
	 */
	String hashString();

}
