/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.geral.negocio.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.ExistenciaClasseVO;
import br.com.ggas.integracao.bens.IntegracaoBem;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas operações realizadas em uma entidade de negócio.
 * 
 * 
 */
public abstract class ControladorNegocioImpl extends HibernateDaoSupport implements ControladorNegocio {

	protected static final Logger LOG = Logger.getLogger(ControladorNegocioImpl.class);

	private static final ControladorConstanteSistema CONTROLADORCONSTANTESISTEMA = (ControladorConstanteSistema) ServiceLocator
			.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

	private static final int BATCH_SIZE = 50;

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #criar()
	 */
	@Override
	public abstract EntidadeNegocio criar();

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #getClasseEntidade()
	 */
	@Override
	public abstract Class<?> getClasseEntidade();

	/**
	 * 
	 * @param EntidadeNegocio entidadeNegocio
	 * @return long
	 * @throws NegocioException
	 */
	@Override
	public long inserir(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Long chavePrimaria = null;
		long chave = 0;

		validarDadosEntidade(entidadeNegocio);
		preInsercao(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());

		if (!entidadeNegocio.getClass().getName().equals(Constantes.AREA_CONSTRUIDA)
				&& !(entidadeNegocio instanceof ModeloContrato) && !(entidadeNegocio instanceof Contrato)) {
			entidadeNegocio.setHabilitado(Boolean.TRUE);
		}

		if (entidadeNegocio.getClass().getName().equals(Constantes.FERIADO) || entidadeNegocio.getClass().getName().equals(Constantes.INSTALACAO_MEDIDOR)) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
			getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(entidadeNegocio);
		} else {
			chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);
		}

		if (chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}

		if (flushNecessario(entidadeNegocio)) {
			try {
			getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return chave;
	}

	/**
	 * 
	 * @param EntidadeNegocio entidadeNegocio
	 * @return boolean
	 */
	private boolean flushNecessario(EntidadeNegocio entidadeNegocio) {

		String className = entidadeNegocio.getClass().getSimpleName();

		if ("IntegracaoTituloReceberTributoImpl".equals(className) || "FaturaItemImpressaoImpl".equals(className)
				|| "CreditoDebitoDetalhamentoImpl".equals(className) || "DocumentoCobrancaImpl".equals(className)
				|| "CobrancaBancariaMovimentoImpl".equals(className)
				|| "DebitoAutomaticoMovimentoImpl".equals(className)
				|| "HistoricoOperacaoMedidorImpl".equals(className)) {

			return false;
		}

		return true;
	}

	/**
	 * Método utilizado, durante os processamentos em batch, para a inserção de uma
	 * entidade de negócio ativa ou inativa, de acordo com o parâmetro
	 * {@code habilitado}
	 * 
	 * @param entidadeNegocio - A entidade de negócio
	 * @return long - a chave primária da entidade
	 * @throws NegocioException - Caso ocorra alguma violação nas regras de negócio
	 */
	@Override
	public long inserirEmBatch(EntidadeNegocio entidadeNegocio, Boolean habilitado) throws NegocioException {

		Long chavePrimaria = null;
		long chave = 0;

		validarDadosEntidade(entidadeNegocio);
		preInsercao(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		entidadeNegocio.setHabilitado(habilitado);

		chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);

		if (chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}

		return chave;
	}

	/**
	 * 
	 * @param EntidadeNegocio entidadeNegocio
	 * @return long
	 */
	@Override
	public long inserirInativo(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Long chavePrimaria = null;
		long chave = 0;

		validarDadosEntidade(entidadeNegocio);
		preInsercao(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());

		entidadeNegocio.setHabilitado(Boolean.FALSE);

		chavePrimaria = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(entidadeNegocio);

		if (chavePrimaria != null) {
			entidadeNegocio.setChavePrimaria(chavePrimaria);
			posInsercao(entidadeNegocio);
			chave = chavePrimaria.longValue();
		}

		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		return chave;
	}

	/**
	 * 
	 * @param long chavePrimaria
	 * @return EntidadeNegocio
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria) throws NegocioException {

		return this.obter(chavePrimaria, getClasseEntidade());
	}

	/**
	 * 
	 * @param long   chavePrimaria
	 * @param classe
	 * @return EntidadeNegocio
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe) throws NegocioException {

		return this.obter(chavePrimaria, classe, (String[]) null);
	}

	/**
	 * 
	 * @param long      chavePrimaria
	 * @param String... propriedadesLazy
	 * @return EntidadeNegocio
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return this.obter(chavePrimaria, getClasseEntidade(), propriedadesLazy);
	}

	/**
	 * 
	 * @param long      chavePrimaria
	 * @param String... propriedadesLazy
	 * @return EntidadeNegocio
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe, String... propriedadesLazy)
			throws NegocioException {

		return obter(chavePrimaria, classe, null, propriedadesLazy);
	}

	/**
	 * 
	 * @param long      chavePrimaria
	 * @param Boolean   habilitado
	 * @param String... propriedadesLazy
	 * @return EntidadeNegocio
	 * @throws NegocioException
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Boolean habilitado, String... propriedadesLazy)
			throws NegocioException {

		return obter(chavePrimaria, getClasseEntidade(), habilitado, propriedadesLazy);
	}

	/**
	 * 
	 * @param long      chavePrimaria
	 * @param classe
	 * @param Boolean   habilitado
	 * @param String... propriedadesLazy
	 * @return EntidadeNegocio
	 * @throws NegocioException
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe, Boolean habilitado, String... propriedadesLazy)
			throws NegocioException {

		EntidadeNegocio entidade = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe.getSimpleName());
		hql.append(" entidade where ");
		hql.append(" entidade.chavePrimaria = :chavePrimaria ");
		if (habilitado != null) {
			hql.append(" and entidade.habilitado = :habilitado ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chavePrimaria);
		if (habilitado != null) {
			query.setBoolean("habilitado", habilitado);
		}

		entidade = (EntidadeNegocio) query.uniqueResult();

		if (entidade == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, classe.getName());
		}

		inicializarLazy(entidade, propriedadesLazy);

		return entidade;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#obter(long,
	 * java.lang.Class, java.lang.Boolean)
	 */
	@Override
	public EntidadeNegocio obter(long chavePrimaria, Class<?> classe, Boolean habilitado) {

		EntidadeNegocio entidade = null;

		StringBuilder hql = new StringBuilder();
		hql.append("from ");
		hql.append(classe.getSimpleName());
		hql.append(" entidade where ");
		hql.append(" entidade.chavePrimaria = :chavePrimaria ");
		if (habilitado != null) {
			hql.append(" and entidade.habilitado = :habilitado ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chavePrimaria);
		if (habilitado != null) {
			query.setBoolean("habilitado", habilitado);
		}

		entidade = (EntidadeNegocio) query.uniqueResult();

		return entidade;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #atualizar(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void atualizar(EntidadeNegocio entidadeNegocio) throws ConcorrenciaException, NegocioException {

		atualizar(entidadeNegocio, getClasseEntidade());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #atualizar(br.com.ggas.geral.negocio. EntidadeNegocio, java.lang.Class)
	 */
	@Override
	public void atualizar(EntidadeNegocio entidadeNegocio, Class<?> classe)
			throws ConcorrenciaException, NegocioException {

		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		validarDadosEntidade(entidadeNegocio);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(entidadeNegocio);

		posAtualizacao(entidadeNegocio);
		getSession().flush();
	}
	
	@Override
	public void atualizarSaveOrUpdate(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException {
		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(entidadeNegocio);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		validarDadosEntidade(entidadeNegocio);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();
		getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(entidadeNegocio);

		posAtualizacao(entidadeNegocio);
		getSession().flush();
		
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #atualizar(br.com.ggas.geral.negocio. EntidadeNegocio, java.lang.Class)
	 */
	@Override
	public void atualizarEmBatch(EntidadeNegocio entidadeNegocio, Class<?> classe)
			throws ConcorrenciaException, NegocioException {

		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		validarDadosEntidade(entidadeNegocio);
		preAtualizacao(entidadeNegocio);
		entidadeNegocio.incrementarVersao();

		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(entidadeNegocio);

		posAtualizacao(entidadeNegocio);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#atualizarColecao(java.lang.
	 * Object[], java.lang.Class)
	 */
	@Override
	public void atualizarColecao(Object[] entidade, Class<?> classe) throws NegocioException, ConcorrenciaException {

		if (entidade != null && entidade.length > 0) {
			for (int i = 0; i < entidade.length; i++) {
				this.atualizar((EntidadeNegocio) entidade[i], classe);
			}
		}
	}

	/**
	 * Atualiza uma coleção de entidades do tipo {@link EntidadeNegocio}.
	 * 
	 * @param <T>       the T
	 * @param entidades the entidades
	 * @throws NegocioException - {@link NegocioException}
	 */
	public <T extends EntidadeNegocio> void atualizarColecao(Collection<T> entidades) throws NegocioException {

		int count = 0;

		for (EntidadeNegocio entidade : entidades) {

			entidade.setUltimaAlteracao(Calendar.getInstance().getTime());
			validarDadosEntidade(entidade);
			preAtualizacao(entidade);
			entidade.incrementarVersao();

			super.getSession().merge(entidade);

			count += 1;

			posAtualizacao(entidade);

			if (count % BATCH_SIZE == 0) {
				super.getSession().flush();
			}
		}
		super.getSession().flush();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void remover(EntidadeNegocio entidadeNegocio) throws NegocioException {

		this.remover(entidadeNegocio, getClasseEntidade(), true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio. EntidadeNegocio, boolean)
	 */
	@Override
	public void remover(EntidadeNegocio entidadeNegocio, boolean fisicamente) throws NegocioException {

		this.remover(entidadeNegocio, getClasseEntidade(), fisicamente);
	}

	/**
	 * Remover.
	 * 
	 * @param entidadeNegocio the entidade negocio
	 * @param classe          the classe
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio. EntidadeNegocio, java.lang.Class)
	 */
	public void remover(EntidadeNegocio entidadeNegocio, Class<?> classe) throws NegocioException {

		this.remover(entidadeNegocio, classe, true);
	}

	/**
	 * Remover.
	 * 
	 * @param entidadeNegocio the entidade negocio
	 * @param classe          the classe
	 * @param fisicamente     the fisicamente
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #remover(br.com.ggas.geral.negocio. EntidadeNegocio, java.lang.Class,
	 * boolean)
	 */
	public void remover(EntidadeNegocio entidadeNegocio, Class<?> classe, boolean fisicamente) throws NegocioException {

		preRemocao(entidadeNegocio);
		if (fisicamente) {
			getHibernateTemplate().getSessionFactory().getCurrentSession().delete(entidadeNegocio);
		} else {
			entidadeNegocio.setHabilitado(Boolean.FALSE);
			getHibernateTemplate().getSessionFactory().getCurrentSession().update(entidadeNegocio);
		}

		posRemocao(entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #remover(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void remover(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where ");
			hql.append(" chavePrimaria in (:chavesPrimarias) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<EntidadeNegocio> entidades = query.list();
			if ((entidades != null) && (!entidades.isEmpty())) {
				for (EntidadeNegocio entidade : entidades) {
					entidade.setDadosAuditoria(dadosAuditoria);
					this.remover(entidade);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#remover(java.lang.Long[],
	 * java.lang.Object, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void remover(Long[] chavesPrimarias, Object classe, DadosAuditoria dadosAuditoria) throws NegocioException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(classe);
			hql.append(" where ");
			hql.append(" chavePrimaria in (:chavesPrimarias) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<EntidadeNegocio> entidades = query.list();
			if ((entidades != null) && (!entidades.isEmpty())) {
				for (EntidadeNegocio entidade : entidades) {
					entidade.setDadosAuditoria(dadosAuditoria);
					remover(entidade);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #listar()
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodas() throws NegocioException {

		return obterTodas(getClasseEntidade(), null);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #listar(boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodas(boolean habilitado) throws NegocioException {

		return obterTodas(getClasseEntidade(), habilitado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #listar(java.lang.Class,
	 * java.lang.Boolean)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EntidadeNegocio> obterTodas(Class<?> classe, Boolean habilitado) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe.getSimpleName());

		if (habilitado != null) {
			hql.append(" where habilitado = ? ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (habilitado != null) {
			query.setBoolean(0, habilitado);
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #
	 * validarDadosEntidade(br.com.ggas.geral.negocio .EntidadeNegocio)
	 */
	@Override
	public void validarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Map<String, Object> erros = entidadeNegocio.validarDados();
		if (erros != null && !erros.isEmpty()) {
			throw new NegocioException(erros);
		}

		posValidarDadosEntidade(entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio #
	 * validarVersaoEntidade(br.com.ggas.geral.negocio .EntidadeNegocio)
	 */
	@Override
	public void validarVersaoEntidade(EntidadeNegocio entidadeNegocio, Class<?> classe)
			throws ConcorrenciaException, NegocioException {

		Integer versaoAtual = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select entidade.versao from ");
		hql.append(classe.getSimpleName());
		hql.append(" entidade ");
		hql.append(" where entidade.chavePrimaria = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, entidadeNegocio.getChavePrimaria());

		getSession().flush();

		versaoAtual = (Integer) query.uniqueResult();

		if (entidadeNegocio.getVersao() != versaoAtual.intValue()) {
			throw new ConcorrenciaException(Constantes.ERRO_ENTIDADE_VERSAO_DESATUALIZADA, classe.getName());
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #posAtualizacao(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void posAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #posInsercao(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void posInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #posRemocao(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void posRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #preAtualizacao(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #preInsercao(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:21

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #preRemocao(br.com.ggas.geral.negocio. EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:23

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.comum.negocio.ControladorNegocio
	 * #posValidarDadosEntidade(br.com.ggas.geral. negocio.EntidadeNegocio)
	 */
	@Override
	public void posValidarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException {

	}

	// by gmatos on 15/10/09 10:23

	/**
	 * Método responsável por criar um objeto criteria. A classe do criteria será
	 * disponibilizada pelo método getClasseEntidade()
	 * 
	 * @return Um objeto criteria
	 */
	protected Criteria getCriteria() {

		return createCriteria(getClasseEntidade());
	}

	/**
	 * Método responsável por criar um criteria.
	 * 
	 * @param classe A classe do criteria
	 * @return Um objeto criteria
	 */
	protected Criteria createCriteria(Class<?> classe) {

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(classe);
	}

	/**
	 * Método responsável por criar um criteria.
	 * 
	 * @param classe A classe do criteria
	 * @param alias  O alias do criteria
	 * @return Um objeto criteria
	 */
	protected Criteria createCriteria(Class<?> classe, String alias) {

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(classe, alias);
	}

	/**
	 * Método responsável por criar um SQL Nativo.
	 * 
	 * @param query query em string
	 * @return Um objeto SQLQuery
	 */
	protected SQLQuery createSQLQuery(String query) {

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(query);
	}

	/**
	 * Método responsável por criar um HQL
	 * 
	 * @param query query em string
	 * @return Um objeto Query
	 */
	protected Query createHQLQuery(String query) {
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(query);
	}

	/**
	 * Método responsável por inicializar o lazy de um objeto.

	 * 
	 * @param entidade         O objeto
	 * @param propriedadesLazy As propriedades que serão inicializadas
	 */
	protected void inicializarLazy(Object entidade, String... propriedadesLazy) {

		if (entidade != null) {
			Hibernate.initialize(entidade);
			if (propriedadesLazy != null && propriedadesLazy.length > 0) {
				for (String propriedade : propriedadesLazy) {
					try {
						if (propriedade.contains(".")) {
							tratarSubnivel(entidade, propriedade);
						} else {
							BeanUtils.getProperty(entidade, propriedade);
						}
					} catch (Exception e) {
						LOG.error("Erro ao tentar obter o valor da propriedade: " + propriedade, e);
					}
				}
			}

		}
	}

	/**
	 * Tratar subnivel.
	 * 
	 * @param entidade    the entidade
	 * @param propriedade the propriedade
	 * @throws IllegalAccessException    the illegal access exception
	 * @throws InvocationTargetException the invocation target exception
	 * @throws NoSuchMethodException     the no such method exception
	 */
	private void tratarSubnivel(Object entidade, String propriedade)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		propriedade = propriedade.replace(".", ":");
		String[] p = propriedade.split(":");
		BeanUtils.getProperty(entidade, p[0]);

		String metodo = "get" + p[0].substring(0, 1).toUpperCase() + p[0].substring(1, p[0].length());

		Method m = entidade.getClass().getMethod(metodo);

		Object o = m.invoke(entidade);

		if (o instanceof Collection) {
			inicializarLazyColecao((Collection<Object>) o, p[1]);
		} else {
			inicializarLazy(o, p[1]);
		}

	}

	/**
	 * Método responsável por inicializar o lazy de uma coleção.
	 * 
	 * @param colecao          the colecao
	 * @param propriedadesLazy As propriedades que serão inicializadas
	 * @return colecao inicializada
	 */
	protected Collection<Object> inicializarLazyColecao(Collection<Object> colecao, String... propriedadesLazy) {

		if (colecao != null && !colecao.isEmpty()) {
			for (Object object : colecao) {
				Hibernate.initialize(object);
				if (propriedadesLazy != null && propriedadesLazy.length > 0) {
					for (String propriedade : propriedadesLazy) {
						try {
							if (propriedade != null && !propriedade.contentEquals("")) {
								BeanUtils.getProperty(object, propriedade);
							}
						} catch (Exception e) {
							LOG.error("Erro ao tentar obter o valor da propriedade: " + propriedade, e);
						}
					}
				}
			}

		}
		return colecao;
	}

	/**
	 * Validar existencia entidades.
	 * 
	 * @param mapEntidades        the map entidades
	 * @param mapaInconsistencias the mapa inconsistencias
	 * @return the string
	 */
	@SuppressWarnings("rawtypes")
	protected String validarExistenciaEntidades(Map<String, ExistenciaClasseVO> mapEntidades,
			Map<String, Object[]> mapaInconsistencias) {

		int count = 0;
		ResourceBundle mensagens = ResourceBundle.getBundle("mensagens");
		String entidadesInexistentes = "";
		Iterator it = mapEntidades.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry entidade = (Map.Entry) it.next();
			StringBuilder hql = new StringBuilder();
			hql.append(" select  count(*) from ");
			hql.append(entidade.getKey());
			hql.append(" entidade ");
			hql.append(" where entidade.chavePrimaria = ? ");
			hql.append(" and entidade.habilitado = true ");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			if (entidade.getValue() != null) {

				if (((ExistenciaClasseVO) entidade.getValue()).getChavePrimaria() != null) {

					query.setLong(0, ((ExistenciaClasseVO) entidade.getValue()).getChavePrimaria());
				} else {

					query.setLong(0, 0L);
				}

			} else {
				query.setLong(0, 0L);
			}
			query.setCacheable(true);
			boolean exists = (Long) query.uniqueResult() > 0;
			getSession().flush();
			if (!exists) {
				entidadesInexistentes = this.consultarNomeClasse(entidadesInexistentes,
						((ExistenciaClasseVO) entidade.getValue()).getNomeClasse());
				count++;
			}
		}

		if (entidadesInexistentes != null && !entidadesInexistentes.isEmpty()) {

			if (mapaInconsistencias != null && !mapaInconsistencias.isEmpty()) {

				for (Map.Entry<String, Object[]> entry : mapaInconsistencias.entrySet()) {

					Object[] listaAux = entry.getValue();
					String constante = entry.getKey();

					if (constante.equals(Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA)) {

						Integer contadorInconsistencias = (Integer) listaAux[0];
						contadorInconsistencias = contadorInconsistencias + count;
						listaAux[0] = contadorInconsistencias;

						mapaInconsistencias.put(constante, listaAux);
					}
				}
			} else {

				Object[] listaAux = new Object[2];
				listaAux[0] = count;
				listaAux[1] = Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA;

				if (mapaInconsistencias != null) {
					mapaInconsistencias.put(Constantes.ERRO_NEGOCIO_ENTIDADE_INVALIDA, listaAux);
				}
			}

			entidadesInexistentes = MensagemUtil.obterMensagem(mensagens,
					Constantes.ERRO_NEGOCIO_ENTIDADE_NAO_CADASTRADA, entidadesInexistentes);
		}

		return entidadesInexistentes;
	}

	/**
	 * Consultar nome classe.
	 * 
	 * @param entidadesInexistentes the entidades inexistentes
	 * @param nomeClasse            the nome classe
	 * @return the string
	 */
	@SuppressWarnings("rawtypes")
	private String consultarNomeClasse(String entidadesInexistentes, String nomeClasse) {

		StringBuilder hql;
		Query query;
		hql = new StringBuilder();
		hql.append(" select descricao from ");
		hql.append("TabelaImpl ");
		hql.append(" tabela ");
		hql.append(" where tabela.nomeClasse = '").append(nomeClasse).append("' ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setCacheable(true);
		nomeClasse = (String) query.uniqueResult();
		if (!entidadesInexistentes.isEmpty()) {
			entidadesInexistentes = entidadesInexistentes.concat(" ," + nomeClasse);
		} else {
			entidadesInexistentes = nomeClasse;
		}

		return entidadesInexistentes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#validarExistenciaEntidades(
	 * br.com.ggas.geral.negocio.EntidadeNegocio,
	 * br.com.ggas.geral.negocio.EntidadeNegocio, java.util.Map)
	 */
	@Override
	public String validarExistenciaEntidades(EntidadeNegocio classeOrigem, EntidadeNegocio objetoValor,
			Map<String, Object[]> mapaInconsistencias) throws NegocioException {

		Map<String, ExistenciaClasseVO> mapValidarEntidades = null;

		mapValidarEntidades = criarMapaEntidadesValidacao(classeOrigem, objetoValor);

		return validarExistenciaEntidades(mapValidarEntidades, mapaInconsistencias);

	}

	/**
	 * Criar mapa entidades validacao.
	 * 
	 * @param classeOrigem the classe origem
	 * @param objetoValor  the objeto valor
	 * @return the hash map
	 * @throws NegocioException the negocio exception
	 */
	private Map<String, ExistenciaClasseVO> criarMapaEntidadesValidacao(EntidadeNegocio classeOrigem,
			EntidadeNegocio objetoValor) throws NegocioException {

		Map<String, ExistenciaClasseVO> mapValidarEntidades = new HashMap<String, ExistenciaClasseVO>();
		ExistenciaClasseVO vo = null;
		for (Field field : classeOrigem.getClass().getDeclaredFields()) {
			if (!(field.getType().isPrimitive() || Util.isWrapperType(field.getType()))) {
				Object obj = null;
				try {
					vo = new ExistenciaClasseVO();
					if (field.getType().getName().equals(Constantes.VAZAO_CORRETOR)
							|| field.getType().getName().equals(Constantes.MEDIDOR)) {
						obj = objetoValor.getClass().getDeclaredMethod(Util.montarNomeGet(Constantes.BEM))
								.invoke(objetoValor);

					} else {
						obj = objetoValor.getClass().getDeclaredMethod(Util.montarNomeGet(field.getName()))
								.invoke(objetoValor);
					}

					if (obj != null) {
						if (field.getType().getName().equals(Constantes.VAZAO_CORRETOR)
								|| field.getType().getName().equals(Constantes.MEDIDOR)) {

							IntegracaoBem entidade = (IntegracaoBem) obj;
							vo.setChavePrimaria(entidade.getBemDestino());
							vo.setNomeClasse(field.getType().getName());
							mapValidarEntidades.put(field.getType().getSimpleName() + Util.REFLECTION_CLASS, vo);

						} else {
							vo.setChavePrimaria((Long) obj);
							vo.setNomeClasse(field.getType().getName());
							mapValidarEntidades.put(field.getType().getSimpleName() + Util.REFLECTION_CLASS, vo);

						}
					}
				} catch (NoSuchMethodException e) {
					// Caso existe na classe origem atributos que não existem no
					// objeto valor
					LOG.error(e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(CONTROLADORCONSTANTESISTEMA
							.obterConstantePorCodigo(Constantes.C_ERRO_NEGOCIO_BUSCAR_VALOR_METODO).getValor() + " "
							+ field.getName());
				}
			}

		}

		return mapValidarEntidades;
	}

	/**
	 * Realiza uma consulta por entidades de negócio, utilizando a chave primaria,
	 * consultando, além dos campos primitivos e classes nativas, os campos
	 * especificados em um objeto JSON, no seguinte formato: <br />
	 * <code>
	 *  {
	 *  	"imovel":{ "agente":{} },
	 *  	"situacaoConsumo":{}
	 *  }
	 *  </code>
	 * 
	 * @param primaryKeys    - Array de Chaves Primárias
	 * @param jsonProperties - {@link String}
	 * @param orderBy        - {@link String}
	 * @return coleção de entidades de negócio
	 * @throws Exception {@link Exception}
	 **/
	public <T extends EntidadeNegocio> Collection<T> obter(Long[] primaryKeys, String jsonProperties, String orderBy)
			throws Exception {

		Collection<T> objects = new ArrayList<>();
		if (primaryKeys != null && primaryKeys.length > 0) {
			Class<?> rootClass = getClasseEntidade();

			Set<Entry<String, JsonElement>> requiredFields = getRequiredFields(jsonProperties);

			Map<Class<?>, List<String>> fieldsPerRootClass = this.getMappedFields(rootClass, requiredFields);

			String hqlQuery = this.buildHqlQuery(fieldsPerRootClass, rootClass, requiredFields, orderBy);

			Query query = getSession().createQuery(hqlQuery);
			query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

			query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, primaryKeys);

			objects.addAll((Collection<? extends T>) this.createObjects(fieldsPerRootClass, query.list(), rootClass,
					requiredFields));
		}

		return objects;
	}

	private <T> Collection<T> createObjects(Map<Class<?>, List<String>> fieldsPerRootClass,
			List<Map<String, Object>> queryResultList, Class<T> rootClass,
			Set<Entry<String, JsonElement>> requiredFields) throws Exception {

		Collection<T> objects = new ArrayList<>();
		for (Map<String, Object> queryResult : queryResultList) {

			Object object = this.createObjects(fieldsPerRootClass, rootClass, requiredFields, queryResult);
			objects.add((T) object);
		}

		return objects;
	}

	private Object createObjects(Map<Class<?>, List<String>> fieldsPerRootClass, Class<?> rootClass,
			Set<Entry<String, JsonElement>> requiredFields, Map<String, Object> queryResult) throws Exception {

		List<String> fields = fieldsPerRootClass.get(rootClass);
		Object rootObject = fillObjectWithMappedFields(fields, rootClass, queryResult);

		ClassMetadata classMetadata = getSessionFactory().getClassMetadata(rootClass);
		for (Entry<String, JsonElement> entry : requiredFields) {
			String requiredFieldName = entry.getKey();
			Field requiredField = this.getDeclaredField(requiredFieldName, rootClass);
			Type requiredFieldType = classMetadata.getPropertyType(requiredFieldName);
			Class<?> requiredFieldClass = requiredFieldType.getReturnedClass();
			Set<Entry<String, JsonElement>> nextRequiredFields = this.getRequiredFields(entry.getValue());
			Object childObject = createObjects(fieldsPerRootClass, requiredFieldClass, nextRequiredFields, queryResult);
			if (childObject != null) {
				requiredField.set(rootObject, childObject);
			}

		}
		return rootObject;
	}

	/**
	 *
	 * @param fieldName
	 * @param rootClass
	 * @return
	 */
	private Field getDeclaredField(String fieldName, Class<?> rootClass) {

		Field field = null;
		try {
			field = rootClass.getDeclaredField(fieldName);
			field.setAccessible(Boolean.TRUE);
		} catch (NoSuchFieldException e) {
			LOG.debug(e.getMessage(), e);
			if (rootClass.getSuperclass() != null) {
				field = getDeclaredField(fieldName, rootClass.getSuperclass());
			}
		}
		return field;
	}

	/**
	 *
	 * @param mappedFields
	 * @param rootClass
	 * @param queryResult
	 * @return
	 * @throws @throws   NoSuchMethodException
	 * @throws Exception
	 */
	private Object fillObjectWithMappedFields(List<String> mappedFields, Class<?> rootClass,
			Map<String, Object> queryResult) throws GGASException {

		Constructor<?> constructor;
		try {
			constructor = rootClass.getDeclaredConstructor();
			constructor.setAccessible(Boolean.TRUE);
		} catch (NoSuchMethodException | SecurityException e) {

			throw new GGASException(e);
		}
		Object rootObject = null;
		for (String fieldName : mappedFields) {

			Field mappedField = this.getDeclaredField(fieldName, rootClass);
			String mappedFieldAlias = fieldName.concat(rootClass.getSimpleName());
			Object mappedFieldValue = queryResult.get(mappedFieldAlias);
			if (mappedFieldValue != null) {
				if (rootObject == null) {
					try {
						rootObject = constructor.newInstance();
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {

						throw new GGASException(e);
					}
				}
				try {
					mappedField.set(rootObject, mappedFieldValue);
				} catch (IllegalArgumentException | IllegalAccessException e) {

					throw new GGASException(e);
				}
			}

		}
		return rootObject;
	}

	/**
	 *
	 * @param jsonProperties
	 * @return
	 */
	private Set<Entry<String, JsonElement>> getRequiredFields(String jsonProperties) {

		JsonParser parser = new JsonParser();
		JsonElement rootNode = parser.parse(jsonProperties);
		return this.getRequiredFields(rootNode);
	}

	/**
	 *
	 * @param rootNode
	 * @return
	 */
	private Set<Entry<String, JsonElement>> getRequiredFields(JsonElement rootNode) {

		JsonObject jsonObject = rootNode.getAsJsonObject();
		return jsonObject.entrySet();
	}

	/**
	 *
	 * @param rootClass
	 * @param requiredFields
	 * @return
	 */
	private Map<Class<?>, List<String>> getMappedFields(Class<?> rootClass,
			Set<Entry<String, JsonElement>> requiredFields) {

		List<String> mappedFields = this.getMappedFields(rootClass);
		Map<Class<?>, List<String>> fieldsPerRootClass = new HashMap<>();
		List<String> fields = null;
		for (String field : mappedFields) {
			Type fieldMappedType = this.getFieldMappedType(rootClass, field);
			Class<?> fieldClass = fieldMappedType.getReturnedClass();
			if (isSimpleAttribute(fieldMappedType)) {
				if (!fieldsPerRootClass.containsKey(rootClass)) {
					fields = new ArrayList<>();
					fields.add(field);
					fieldsPerRootClass.put(rootClass, fields);
				} else {
					fields = fieldsPerRootClass.get(rootClass);
					fields.add(field);
				}
			} else {
				Set<Entry<String, JsonElement>> nextRequiredFields = this.getRequiredFields(field, requiredFields);
				if (nextRequiredFields != null) {
					fieldsPerRootClass.putAll(this.getMappedFields(fieldClass, nextRequiredFields));
				}
			}
		}
		return fieldsPerRootClass;
	}

	/**
	 *
	 * @param classe
	 * @return
	 */
	private List<String> getMappedFields(Class<?> classe) {

		ClassMetadata classMetadata = getSessionFactory().getClassMetadata(classe);

		List<String> propertyNames = new ArrayList<>();
		propertyNames.addAll(Arrays.asList(classMetadata.getPropertyNames()));
		propertyNames.add(classMetadata.getIdentifierPropertyName());

		return propertyNames;
	}

	/**
	 *
	 * @param classe
	 * @param propertyName
	 * @return
	 */
	private Class<?> getFieldClass(Class<?> classe, String propertyName) {

		ClassMetadata classMetadata = getSessionFactory().getClassMetadata(classe);
		return classMetadata.getPropertyType(propertyName).getReturnedClass();
	}

	/**
	 *
	 * @param classe
	 * @param propertyName
	 * @return
	 */
	private Type getFieldMappedType(Class<?> classe, String propertyName) {

		ClassMetadata classMetadata = getSessionFactory().getClassMetadata(classe);
		return classMetadata.getPropertyType(propertyName);
	}

	/**
	 *
	 * @param fieldMappedType
	 * @return
	 */
	private Boolean isSimpleAttribute(Type fieldMappedType) {

		Boolean isSimple = Boolean.FALSE;
		Class<?> fieldClass = fieldMappedType.getReturnedClass();
		if (!(isBussinessEntity(fieldClass) || fieldMappedType.isCollectionType()
				|| fieldMappedType.isComponentType())) {
			isSimple = Boolean.TRUE;
		}

		return isSimple;
	}

	/**
	 *
	 * @param fieldType
	 * @return
	 */
	private Boolean isBussinessEntity(Class<?> fieldType) {

		Class<?> baseClass = EntidadeNegocioImpl.class;
		return baseClass.isAssignableFrom(fieldType);
	}

	/**
	 *
	 * @param fieldName
	 * @param requiredFields
	 * @return
	 */
	private Set<Entry<String, JsonElement>> getRequiredFields(String fieldName,
			Set<Entry<String, JsonElement>> requiredFields) {

		Set<Entry<String, JsonElement>> nextRequiredFields = null;
		if (requiredFields != null) {
			for (Entry<String, JsonElement> fields : requiredFields) {
				String requiredFieldName = fields.getKey();
				if (fieldName.equals(requiredFieldName)) {
					nextRequiredFields = this.getRequiredFields(fields.getValue());
				}
			}
		}
		return nextRequiredFields;
	}

	/**
	 *
	 * @param fieldsPerRootClass
	 * @param rootClass
	 * @param requiredFields
	 * @param orderBy
	 * @return
	 */
	private String buildHqlQuery(Map<Class<?>, List<String>> fieldsPerRootClass, Class<?> rootClass,
			Set<Entry<String, JsonElement>> requiredFields, String orderBy) {

		StringBuilder hql = new StringBuilder();
		String classSimpleName = rootClass.getSimpleName();

		hql.append(this.buildHqlSelectFields(fieldsPerRootClass));
		hql.append(" from ");
		hql.append(classSimpleName);
		hql.append(" as ");
		hql.append(classSimpleName);
		hql.append(this.buildJoinWithRequiredFields(rootClass, requiredFields));
		hql.append(" where ");
		hql.append(classSimpleName);
		hql.append(".chavePrimaria IN (:chavesPrimarias) ");
		hql.append(" order by ");
		hql.append(classSimpleName);
		hql.append(".");
		hql.append(orderBy);

		return hql.toString();
	}

	/**
	 *
	 * @param rootFieldClass
	 * @param requiredFields
	 * @return
	 */
	private String buildJoinWithRequiredFields(Class<?> rootFieldClass,
			Set<Entry<String, JsonElement>> requiredFields) {

		StringBuilder hql = new StringBuilder();
		for (Entry<String, JsonElement> entry : requiredFields) {
			String fieldName = entry.getKey();
			hql.append(" left join ");
			hql.append(rootFieldClass.getSimpleName());
			hql.append(".");
			hql.append(fieldName);
			hql.append(" as ");
			Class<?> nextRootFieldClass = this.getFieldClass(rootFieldClass, fieldName);
			hql.append(nextRootFieldClass.getSimpleName());
			hql.append(" ");
			Set<Entry<String, JsonElement>> nextRequiredFields = this.getRequiredFields(entry.getValue());
			hql.append(this.buildJoinWithRequiredFields(nextRootFieldClass, nextRequiredFields));
		}
		return hql.toString();
	}

	/**
	 *
	 * @param fieldsPerRootClass
	 * @return
	 */
	private String buildHqlSelectFields(Map<Class<?>, List<String>> fieldsPerRootClass) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		for (Entry<Class<?>, List<String>> entry : fieldsPerRootClass.entrySet()) {
			Class<?> fieldClass = entry.getKey();
			List<String> fields = entry.getValue();
			for (int i = 0; i < fields.size(); i++) {
				String fieldName = fields.get(i);
				if (fieldName != null) {
					hql.append(fieldClass.getSimpleName().concat("."));
					hql.append(fieldName);
					hql.append(" as ");
					hql.append(fieldName.concat(fieldClass.getSimpleName()));
					hql.append(", ");
				}
			}
		}
		hql.delete(hql.length() - 2, hql.length());
		return hql.toString();
	}

	protected <E extends EntidadeNegocio, T extends EntidadeNegocio> Collection<T> obter(Integer tamanhoLimiteLista,
			List<E> listaEntidadeNegocio, Function<Long[], Collection<T>> buscaPorChaves) {

		Collection<T> colecao = new ArrayList<>();

		while (listaEntidadeNegocio.size() > tamanhoLimiteLista) {
			List<E> subLista = listaEntidadeNegocio.subList(0, tamanhoLimiteLista);

			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(subLista);

			colecao.addAll(buscaPorChaves.apply(chavesPrimarias));

			listaEntidadeNegocio.subList(0, tamanhoLimiteLista).clear();
		}

		if (!listaEntidadeNegocio.isEmpty()) {
			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(listaEntidadeNegocio);
			colecao.addAll(buscaPorChaves.apply(chavesPrimarias));
		}

		return colecao;
	}

	protected <K, V, E extends EntidadeNegocio> Map<K, V> obterMapa(Integer tamanhoLimiteLista,
			List<E> listaEntidadeNegocio, Function<Long[], Map<K, V>> buscaPorChaves) {

		Map<K, V> mapa = new HashMap<>();

		while (listaEntidadeNegocio.size() > tamanhoLimiteLista) {
			List<E> subLista = listaEntidadeNegocio.subList(0, tamanhoLimiteLista);

			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(subLista);

			mapa.putAll(buscaPorChaves.apply(chavesPrimarias));

			listaEntidadeNegocio.subList(0, tamanhoLimiteLista).clear();
		}

		if (!listaEntidadeNegocio.isEmpty()) {
			Long[] chavesPrimarias = Util.collectionParaArrayChavesPrimarias(listaEntidadeNegocio);
			mapa.putAll(buscaPorChaves.apply(chavesPrimarias));
		}

		return mapa;
	}

	protected <K, V> Map<K, V> obterMapa(Function<List<Long>, Map<K, V>> buscaPorChaves,
			List<Long> listaChavesPrimarias, Integer tamanhoLimiteLista) {

		Map<K, V> mapa = new HashMap<>();

		while (listaChavesPrimarias.size() > tamanhoLimiteLista) {
			List<Long> subLista = listaChavesPrimarias.subList(0, tamanhoLimiteLista);

			LOG.info(subLista.size());
			
			mapa.putAll(buscaPorChaves.apply(subLista));

			listaChavesPrimarias.subList(0, tamanhoLimiteLista).clear();
		}

		if (!listaChavesPrimarias.isEmpty()) {
			mapa.putAll(buscaPorChaves.apply(listaChavesPrimarias));
		}

		return mapa;
	}

}
