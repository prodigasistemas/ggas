/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso;

import java.util.Collection;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Clase de ControladorAcesso
 * 
 * Interface responsável pelas operações referentes 
 * a autenticação e autorização do usuário.
 * 
 *
 */
public interface ControladorAcesso extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ACESSO = "controladorAcesso";

	/**
	 * Método responsável por autenticar um
	 * usuário no sistema.
	 * 
	 * @param usuario
	 *            o usuário a ser autenticado
	 * @return o ojecto Usuario totalmetne
	 *         preenchido
	 * @throws GGASException
	 *             caso ocorra algum erro
	 */
	Usuario autenticarUsuario(Usuario usuario) throws GGASException;

	/**
	 * Método responsável por buscar a permissão
	 * do usuário em um determinado
	 * modulo/recurso.
	 * 
	 * @param usuario
	 *            O usuário que está tentando
	 *            acessar o recurso.
	 * @param operacao
	 *            A operacao que o usuário
	 *            pretente acessar
	 * @return Uma permissão caso o usuário possua
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Permissao buscarPermissao(Usuario usuario, Operacao operacao) throws NegocioException;

	/**
	 * Método responsável por buscar a permissão do usuário em um determinado modulo/recurso.
	 * 
	 * @param usuario
	 * 			 O usuário que está tentando
	 *            acessar o recurso
	 * @param descricaoOperacao
	 * 			A operacao que o usuário
	 * 			pretente acessar
	 * @param descricaoModulo
	 * 			A descricao que o usuário
	 * 			pretente acessar
	 * @return Uma permissão caso o usuário possua
	 * @throws NegocioException the negocio exception
	 */
	Permissao buscarPermissao(Usuario usuario, String descricaoOperacao, String descricaoModulo) throws NegocioException;

	/**
	 * Método responsável por verificar se o
	 * usuário possui o papel de administrador.
	 * 
	 * @param usuario
	 *            O usuario logado
	 * @return Boolean True ou False
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean possuiPapelAdmin(Usuario usuario) throws NegocioException;

	/**
	 * Método responsável por listar a operações
	 * de uma funcionalidade.
	 * 
	 * @param idFuncionalidade
	 *            chave da funcionalidade
	 * @return operações da funcionalidade
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<Operacao> listarOperacoesPorFuncionalidade(Long idFuncionalidade) throws NegocioException;

	/**
	 * Método responsável por criar um objeto
	 * Permissao.
	 * 
	 * @return Permissao
	 */
	EntidadeNegocio criarPermissao();

	/**
	 * Recupera a chave da operacao de sistema.
	 * 
	 * @param operacao
	 *            the operacao
	 * @return valorChave
	 */
	Long obterChaveOperacaoSistema(ChaveOperacaoSistema operacao);

	/**
	 * Método para pré-validar o logon do usuário.
	 * 
	 * @param usuario
	 *            the usuario
	 * @return the boolean
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Boolean validarLogon(Usuario usuario) throws GGASException;

	/**
	 * Método responsável por consultar as
	 * permissões de um papel.
	 * 
	 * @param colecaoPapeis
	 *            coleção de papéis
	 * @return coleção de permissões
	 */
	Collection<Permissao> consultarPermissoes(Collection<Papel> colecaoPapeis);

	/**
	 * Listar recursos usuario.
	 * 
	 * @param chaveUsuario
	 *            the chave usuario
	 * @return the collection
	 */
	Collection<Recurso> listarRecursosUsuario(long chaveUsuario);

	/**
	 * Verificar unico usuario autorizado.
	 * 
	 * @param ultimoUsuarioAlteracao
	 *            the ultimo usuario alteracao
	 * @return true, if successful
	 */
	boolean verificarUnicoUsuarioAutorizado(Usuario ultimoUsuarioAlteracao);

	/**
	 * Consultar usuario dos papeis.
	 * 
	 * @param colecaoPapeis
	 *            the colecao papeis
	 * @return the collection
	 */
	Collection<Usuario> consultarUsuarioDosPapeis(Collection<Papel> colecaoPapeis);

	/**
	 * Consultar papeis autorizados tarifa.
	 * 
	 * @return the collection
	 */
	Collection<Papel> consultarPapeisAutorizadosTarifa();

	/**
	 * Obter operacao sistema.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the operacao
	 */
	Operacao obterOperacaoSistema(Long chavePrimaria);

	/**
	 * Consultar papeis autorizados programacao consumo.
	 * 
	 * @return the collection
	 */
	Collection<Papel> consultarPapeisAutorizadosProgramacaoConsumo();

	/**
	 * Consulta operacao sistema pela descricao
	 * @param descricao - {@link String}
	 * @return operacao - {@link Operacao}
	 */
	Operacao obterOperacaoSistemaPelaDescricao(String descricao);

	/**
	 * Consulta operacao sistema pela descricao da operacao e pela descricao do menu
	 * @param descricaoOperacao - {@link String}
	 * @param descricaoMenu - {@link String}
	 * @return operacao - {@link Operacao}
	 */
	Operacao obterOperacaoSistemaPelaDescricaoOperacaoEMenu(String descricaoOperacao, String descricaoMenu);

}
