/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela representação da entidade Alcada
 *
 */
public class AlcadaVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String descricaoPapel;

	@DateTimeFormat(pattern = Constantes.FORMATO_DATA_BR)
	private Date dataInicial;

	@DateTimeFormat(pattern = Constantes.FORMATO_DATA_BR)
	private Date dataFinal;

	private String descricaoFuncionalidade;

	private Long idMenu;

	private Long idPapel;

	private Boolean isDetalhar;

	private String listaAlcadasRemocao;

	private String dataInicialAlcada;

	private String dataFinalAlcada;

	private String[] listaValoresIniciais;

	private String[] listaValoresFinais;

	private Boolean habilitado;

	/**
	 * Instantiates a new alcada vo.
	 * 
	 * @param descricaoPapel          the descricao papel
	 * @param dataInicial             the data inicial
	 * @param dataFinal               the data final
	 * @param descricaoFuncionalidade the descricao funcionalidade
	 * @param idMenu                  the id menu
	 * @param idPapel                 the id papel
	 */
	public AlcadaVO(String descricaoPapel, Date dataInicial, Date dataFinal, String descricaoFuncionalidade,
			Long idMenu, Long idPapel) {

		super();
		this.descricaoPapel = descricaoPapel;
		if (dataInicial != null) {
			this.dataInicial = (Date) dataInicial.clone();
		}
		if (dataFinal != null) {
			this.dataFinal = (Date) dataFinal.clone();
		}
		this.descricaoFuncionalidade = descricaoFuncionalidade;
		this.idMenu = idMenu;
		this.idPapel = idPapel;
	}

	public AlcadaVO() {

	}

	public String getDescricaoPapel() {

		return descricaoPapel;
	}

	public void setDescricaoPapel(String descricaoPapel) {

		this.descricaoPapel = descricaoPapel;
	}

	public Date getDataInicial() {
		Date data = null;
		if (this.dataInicial != null) {
			data = (Date) dataInicial.clone();
		}
		return data;
	}

	public void setDataInicial(Date dataInicial) {
		if (dataInicial != null) {
			this.dataInicial = (Date) dataInicial.clone();
		} else {
			this.dataInicial = null;
		}
	}

	public Date getDataFinal() {
		Date data = null;
		if (this.dataFinal != null) {
			data = (Date) dataFinal.clone();
		}
		return data;
	}

	public void setDataFinal(Date dataFinal) {
		if (dataFinal != null) {
			this.dataFinal = (Date) dataFinal.clone();
		} else {
			this.dataFinal = null;
		}
	}

	public String getDescricaoFuncionalidade() {

		return descricaoFuncionalidade;
	}

	public void setDescricaoFuncionalidade(String descricaoFuncionalidade) {

		this.descricaoFuncionalidade = descricaoFuncionalidade;
	}

	public Long getIdMenu() {

		return idMenu;
	}

	public void setIdMenu(Long idMenu) {

		this.idMenu = idMenu;
	}

	public Long getIdPapel() {

		return idPapel;
	}

	public void setIdPapel(Long idPapel) {

		this.idPapel = idPapel;
	}

	public Boolean getIsDetalhar() {
		return isDetalhar;
	}

	public void setIsDetalhar(Boolean isDetalhar) {
		this.isDetalhar = isDetalhar;
	}

	public String getListaAlcadasRemocao() {
		return listaAlcadasRemocao;
	}

	public void setListaAlcadasRemocao(String listaAlcadasRemocao) {
		this.listaAlcadasRemocao = listaAlcadasRemocao;
	}

	public String getDataInicialAlcada() {
		return dataInicialAlcada;
	}

	public void setDataInicialAlcada(String dataInicialAlcada) {
		this.dataInicialAlcada = dataInicialAlcada;
	}

	public String getDataFinalAlcada() {
		return dataFinalAlcada;
	}

	public void setDataFinalAlcada(String dataFinalAlcada) {
		this.dataFinalAlcada = dataFinalAlcada;
	}

	public String[] getListaValoresIniciais() {

		String[] valoresIniciais = null;
		if (this.listaValoresIniciais != null) {
			valoresIniciais = this.listaValoresIniciais.clone();

		}
		return valoresIniciais;
	}

	public void setListaValoresIniciais(String[] listaValoresIniciais) {

		if (listaValoresIniciais != null) {
			this.listaValoresIniciais = listaValoresIniciais.clone();
		}else {
			this.listaValoresIniciais = null;
		}
	}

	public String[] getListaValoresFinais() {

		String[] valoresFinais = null;
		if (this.listaValoresFinais != null) {
			valoresFinais = this.listaValoresFinais.clone();

		}
		return valoresFinais;
	}

	public void setListaValoresFinais(String[] listaValoresFinais) {

		if (listaValoresFinais != null) {
			this.listaValoresFinais = listaValoresFinais.clone();
		}else {
			this.listaValoresFinais = null;
		}
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

}
