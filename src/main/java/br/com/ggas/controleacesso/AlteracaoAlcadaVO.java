/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.geral.Menu;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela representação da entidade AlteracaoAlcada 
 *
 */
public class AlteracaoAlcadaVO implements Serializable {

	private static final int NUMERO_DECIMAIS = 2;

	private static final long serialVersionUID = 1L;

	private Coluna coluna;

	private Alcada alcada;

	private Papel papel;

	private Menu menu;

	private BigDecimal valorInicial;

	private BigDecimal valorFinal;

	public Coluna getColuna() {

		return coluna;
	}

	public void setColuna(Coluna coluna) {

		this.coluna = coluna;
	}

	public Alcada getAlcada() {

		return alcada;
	}

	public void setAlcada(Alcada alcada) {

		this.alcada = alcada;
	}

	public Papel getPapel() {

		return papel;
	}

	public void setPapel(Papel papel) {

		this.papel = papel;
	}

	public Menu getMenu() {

		return menu;
	}

	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	public BigDecimal getValorInicial() {

		return valorInicial;
	}

	public void setValorInicial(BigDecimal valorInicial) {

		this.valorInicial = valorInicial;
	}

	public BigDecimal getValorFinal() {

		return valorFinal;
	}

	public void setValorFinal(BigDecimal valorFinal) {

		this.valorFinal = valorFinal;
	}

	public String getValorInicialFormatado() {

		String valorFormatado = "";
		if (valorInicial != null) {
			valorFormatado = Util.converterCampoValorDecimalParaString("Valor inicial", valorInicial,
					Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
		}
		return valorFormatado;
	}

	public String getValorFinalFormatado() {

		String valorFormatado = "";
		if (valorFinal != null) {
			valorFormatado = Util.converterCampoValorDecimalParaString("Valor final", valorFinal,
					Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
		}
		return valorFormatado;
	}
}
