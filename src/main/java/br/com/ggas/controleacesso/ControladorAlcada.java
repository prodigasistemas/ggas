/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;

/**
 * Interface responsável pela assinatura de métodos
 * relacionados ao ControladorAlcada
 * 
 */
public interface ControladorAlcada extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ALCADA = "controladorAlcada";

	/**
	 * Método para obter a lista de colunas por
	 * menu.
	 * 
	 * @param idMenu
	 *            the id menu
	 * @return the collection
	 */
	Collection<Coluna> obterListaColunasAlcadaPorMenu(Long idMenu);

	/**
	 * Método responsável por validar se foi
	 * sleecionado algum perfil para inserção da
	 * alçada.
	 * 
	 * @param idMenu
	 *            the id menu
	 * @param idPapel
	 *            the id papel
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAlcadas(Long idMenu, Long idPapel, Date dataInicial, Date dataFinal) throws NegocioException;

	/**
	 * Método para inserir uma coleção de alçadas.
	 * 
	 * @param listaAlcadas
	 *            the lista alcadas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirAlcadas(Collection<Alcada> listaAlcadas) throws NegocioException;

	/**
	 * Método para obter a lista de alçadas por
	 * menu.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<AlcadaVO> obterListaAlcadaVO(Map<String, Object> filtro);

	/**
	 * Método para obter os detalhes da alçada.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<Alcada> obterlistaAlcada(Map<String, Object> filtro);

	/**
	 * Método para remover alçadas.
	 * 
	 * @param alcadasRemocao
	 *            the alcadas remocao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void removerAlcada(String alcadasRemocao) throws GGASException;

	/**
	 * Metodo para obter a lista de Alçadas para
	 * alteração.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<AlteracaoAlcadaVO> obterListaAlteracaoAlcada(Map<String, Object> filtro);

	/**
	 * Método para processar a alteração da alçada.
	 * 
	 * @param listaAlcadas
	 *            the lista alcadas
	 * @param listaValoresIniciais
	 *            the lista valores iniciais
	 * @param listaValoresFinais
	 *            the lista valores finais
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param habilitado
	 *            the habilitado
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void alterarAlcada(Collection<AlteracaoAlcadaVO> listaAlcadas, String[] listaValoresIniciais, String[] listaValoresFinais,
					Date dataInicial, Date dataFinal, Boolean habilitado) throws GGASException;

	/**
	 * Método para validar se já existe uma alçada
	 * com os dados informados.
	 * 
	 * @param idMenu
	 *            the id menu
	 * @param idColuna
	 *            the id coluna
	 * @param idPapel
	 *            the id papel
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarAlcadaExistente(Long idMenu, Long idColuna, Long idPapel, Date dataInicial, Date dataFinal) throws GGASException;

	/**
	 * Método para obter a lista de alçadas por
	 * menu.
	 * 
	 * @param idMenu
	 *            the id menu
	 * @return the collection
	 */
	Collection<AlcadaVO> obterListaAlcadasPorMenu(Long idMenu);

	/**
	 * Método responsável por obter as Alcadas
	 * vigentes para uma determinada tabela.
	 * 
	 * @param chaveTabela
	 *            a chave da tabela
	 * @param papeis
	 *            os papeis
	 * @param dataAtual
	 *            a data atual
	 * @return the collection
	 */
	Collection<Alcada> obterListaAlcadasVigentes(Long chaveTabela, Collection<Papel> papeis, Date dataAtual);


	/**
	 *  Obter registros pendentes por usuario.
	 *  
	 * @param usuario
	 * 			the usuario
	 * @param isQtdeMinimaPendentes (quantidade limite de 10 registros se for true)
	 * @return the collection AutorizacoesPendentesVO
	 * @throws NegocioException
	 * 			the negocio exception
	 */			
	AutorizacoesPendentesVO obterRegistrosPendentesPorUsuario(Usuario usuario, boolean isQtdeMinimaPendentes) throws NegocioException;


	/**
	 * Obter creditos debitos pendentes por usuario 
	 * @param usuario
	 * 		 the usuario	
	 * @return the collection
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	Collection<CreditoDebitoNegociado> obterCreditosDebitosPendentesPorUsuario(Usuario usuario) throws NegocioException;

	/**
	 * Possui alcada autorizacao creditos debitos.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendente
	 *            the pendente
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean possuiAlcadaAutorizacaoCreditosDebitos(Usuario usuario, CreditoDebitoNegociado pendente) throws NegocioException;

	/**
	 * Obter parcelamento pendentes por usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Parcelamento> obterParcelamentoPendentesPorUsuario(Usuario usuario) throws NegocioException;

	/**
	 * Possui alcada autorizacao parcelamento.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendente
	 *            the pendente
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean possuiAlcadaAutorizacaoParcelamento(Usuario usuario, Parcelamento pendente) throws NegocioException;

	/**
	 * Obter tarifas pendentes por usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Tarifa> obterTarifasPendentesPorUsuario(Usuario usuario) throws NegocioException;

	/**
	 * Possui alcada autorizacao tarifa vigencia.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendente
	 *            the pendente
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean possuiAlcadaAutorizacaoTarifaVigencia(Usuario usuario, TarifaVigencia pendente) throws NegocioException;

	/**
	 * Possui alcada autorizacao supervisorio medicao diaria.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendente
	 *            the pendente
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoSupervisorioMedicaoDiaria(Usuario usuario, SupervisorioMedicaoDiaria pendente)
					throws NegocioException;

	/**
	 * Possui alcada autorizacao supervisorio medicao horaria.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendente
	 *            the pendente
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public boolean possuiAlcadaAutorizacaoSupervisorioMedicaoHoraria(Usuario usuario, SupervisorioMedicaoHoraria pendente)
					throws NegocioException;

	/**
	 * Obter supervisorio medicao diaria pendentes por usuario.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param usuario
	 *            the usuario
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario(Map<String, Object> filtro,
					Usuario usuario) throws NegocioException;

	/**
	 * Obter supervisorio medicao horaria pendentes por usuario.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param usuario
	 *            the usuario
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario(Map<String, Object> filtro,
					Usuario usuario) throws NegocioException;

	/**
	 * Obter supervisorio medicao diaria pendentes por usuario.
	 * @param usuario
	 * 		the usuario
	 * @param isQtdeMinimaPendente (quantidade limite de 10 registros se for true)
	 * @return the collection
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	public Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario
			(Usuario usuario, boolean isQtdeMinimaPendente) throws NegocioException;


	/**
	 * Obter supervisorio medicao horaria pendentes por usuario.
	 * 
	 * @param usuario 
	 * 			the usuario
	 * @param isQtdeMinimaPendente (quantidade limite de 10 registros se for true)
	 * @return the collection
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	public Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario
			(Usuario usuario, boolean isQtdeMinimaPendente) throws NegocioException;

	/**
	 * Consultar alcada.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<Alcada> consultarAlcada(Map<String, Object> filtro);

	/**
	 * Obter solicitacao consumo ponto consumo pendentes.
	 * 
	 * @param usuario
	 *            the usuario
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<SolicitacaoConsumoPontoConsumo> obterSolicitacaoConsumoPontoConsumoPendentes(Usuario usuario) throws GGASException;
}
