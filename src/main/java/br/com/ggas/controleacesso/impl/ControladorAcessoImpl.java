/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso.impl;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.controleacesso.ChaveOperacaoSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorAcessoImpl extends ControladorNegocioImpl implements ControladorAcesso {

	private static final Logger LOG = Logger.getLogger(ControladorAcessoImpl.class);

	private static final String CODIGO_NUMERO_DE_TENTATIVAS_SENHA_ERRADA = "CODIGO_NUMERO_DE_TENTATIVAS_SENHA_ERRADA";

	private static final String ARQUIVO_PROPRIEDADES_OPERACAO_SISTEMA = "operacaoSistema.properties";

	private static Map<String, String> mapaChavesOperacaoSistema;

	static {
		inicializarPropriedadesOperacaoSistema();
	}

	/**
	 * // * @return the controladorUsuario
	 * //.
	 * 
	 * @param usuario
	 *            the usuario
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #possuiPapelAdmin(br.com.procenge.ggas
	 * .controleacesso.Usuario)
	 */
	@Override
	public boolean possuiPapelAdmin(Usuario usuario) throws NegocioException {

		boolean possuiPapelAdmin = false;
		Papel papel = null;

		Collection<Papel> papeis = usuario.getPapeis();
		for (Iterator<Papel> iterator = papeis.iterator(); iterator.hasNext();) {
			papel = iterator.next();
			if(papel.isAdmin()) {
				possuiPapelAdmin = true;
				break;
			}
		}

		return possuiPapelAdmin;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #buscarPermissao(br.com.procenge.ggas
	 * .controleacesso.Usuario,
	 * br.com.ggas.controleacesso.Modulo,
	 * br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	public Permissao buscarPermissao(Usuario usuario, Operacao operacao) throws NegocioException {

		Permissao permissao = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select permissao ");
		hql.append(" from ");
		hql.append(getClasseEntidadePermissao().getSimpleName());
		hql.append(" permissao ");
		hql.append(" inner join fetch permissao.operacao op");
		hql.append(" where ");
		hql.append(" permissao.papel in ( :papeis ) and ");
		hql.append(" op in ( :operacao ) ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("papeis", usuario.getPapeis());
		query.setParameter("operacao", operacao);
		Collection<Permissao> permissoes = query.list();

		if(!permissoes.isEmpty()) {
			permissao = permissoes.iterator().next();
		}

		return permissao;
	}

	public Permissao buscarPermissao(Usuario usuario, String descricaoOperacao, String descricaoModulo) throws NegocioException {

		Permissao permissao = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select permissao ");
		hql.append(" from ");
		hql.append(getClasseEntidadePermissao().getSimpleName());
		hql.append(" permissao ");
		hql.append(" inner join fetch permissao.operacao op");
		hql.append(" inner join fetch op.modulo mo");
		hql.append(" where ");
		hql.append(" permissao.papel in ( :papeis ) and ");
		hql.append(" lower(op.descricao) like :operacao and ");
		hql.append(" lower(mo.descricao) like :modulo ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("papeis", usuario.getPapeis());
		query.setParameter("operacao", descricaoOperacao.toLowerCase());
		query.setParameter("modulo", descricaoModulo.toLowerCase());
		Collection<Permissao> permissoes = query.list();

		if (!permissoes.isEmpty()) {
			permissao = permissoes.iterator().next();
		}

		return permissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.
	 * ControladorAcesso
	 * #autenticarUsuario(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public synchronized Usuario autenticarUsuario(Usuario usuario) throws GGASException {

		ControladorUsuario controladorUsuario = (ControladorUsuario) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		Usuario usuarioAutenticado = null;
		String senha = "";
		if(usuario != null) {
			senha = usuario.getSenha();
			usuarioAutenticado = controladorUsuario.buscar(usuario.getLogin());
			if(usuarioAutenticado != null) {
				DadosAuditoria dadosAuditoria = usuario.getDadosAuditoria();
				dadosAuditoria.setUsuario(usuarioAutenticado);
				usuario.setDadosAuditoria(dadosAuditoria);
				usuarioAutenticado.setDadosAuditoria(usuario.getDadosAuditoria());
			}
		}

		String senhaCriptografada = Util.criptografarSenha(senha, senha, Constantes.HASH_CRIPTOGRAFIA);
		if(usuarioAutenticado != null && usuarioAutenticado.isHabilitado() && !usuarioAutenticado.isSenhaExpirada()) {

			if(usuarioAutenticado.getSenha().equals(senhaCriptografada)) {
				if(!(usuarioAutenticado.isPrimeiroAcesso() != null && usuarioAutenticado.isPrimeiroAcesso())) {
					usuarioAutenticado.setUltimoAcesso(Calendar.getInstance().getTime());
					usuarioAutenticado.setTentativasSenhaErrada(0);

					controladorUsuario.atualizar(usuarioAutenticado, getClasseEntidadeUsuario());
				}
			} else {
				String numeroTentativasSenhaErrada = (String) controladorParametroSistema
								.obterValorDoParametroPorCodigo(CODIGO_NUMERO_DE_TENTATIVAS_SENHA_ERRADA);
				int tentativasSenhaErrada = usuarioAutenticado.getTentativasSenhaErrada();
				++tentativasSenhaErrada;
				usuarioAutenticado.setTentativasSenhaErrada(tentativasSenhaErrada);
				if(usuarioAutenticado.getTentativasSenhaErrada() >= Integer.parseInt(numeroTentativasSenhaErrada)) {
					usuarioAutenticado.setSenhaExpirada(true);
				}

				LOG.info("Acesso negado: " + usuarioAutenticado.getLogin());
				controladorUsuario.atualizar(usuarioAutenticado);
				usuarioAutenticado = null;
				throw new NegocioException(Constantes.ERRO_ACESSO_NEGADO, true);
			}

		} else {
			usuarioAutenticado = null;
			throw new NegocioException(Constantes.ERRO_ACESSO_NEGADO, true);
		}

		return usuarioAutenticado;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #listarOperacoesPorFuncionalidade(java
	 * .lang.Long)
	 */

	@Override
	public Collection<Operacao> listarOperacoesPorFuncionalidade(Long idFuncionalidade) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeOperacao().getSimpleName());
		hql.append(" operacao ");
		hql.append(" where ");
		hql.append(" operacao.menu.chavePrimaria = :idFucionalidade ");
		hql.append(" order by descricao asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idFucionalidade", idFuncionalidade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeOperacao() {

		return ServiceLocator.getInstancia().getClassPorID(Operacao.BEAN_ID_OPERACAO);
	}

	public Class<?> getClasseEntidadeUsuario() {

		return ServiceLocator.getInstancia().getClassPorID(Usuario.BEAN_ID_USUARIO);
	}

	public Class<?> getClasseEntidadePermissao() {

		return ServiceLocator.getInstancia().getClassPorID(Permissao.BEAN_ID_PERMISSAO);
	}

	public Class<?> getClasseEntidadeRecurso() {

		return ServiceLocator.getInstancia().getClassPorID(Recurso.BEAN_ID_RECURSO);
	}

	public Class<?> getClasseEntidadePapel() {

		return ServiceLocator.getInstancia().getClassPorID(Papel.BEAN_ID_PAPEL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #criarPermissao()
	 */
	@Override
	public EntidadeNegocio criarPermissao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Permissao.BEAN_ID_PERMISSAO);
	}

	/**
	 * Método responsável por inicializar o mapa
	 * de código do arquivo
	 * operacaoSistema.properties
	 * 
	 * @return void
	 */
	private static void inicializarPropriedadesOperacaoSistema() {

		try {
			Properties propriedades = new Properties();
			InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(ARQUIVO_PROPRIEDADES_OPERACAO_SISTEMA);
			propriedades.load(stream);

			mapaChavesOperacaoSistema = new HashMap<>();
			for (Entry<Object, Object> entry : propriedades.entrySet()) {
				mapaChavesOperacaoSistema.put(String.valueOf(entry.getKey()).trim(), String.valueOf(entry.getValue()).trim());
			}
			stream.close();
		} catch(Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #consultarPermissoes(java.util.Collection
	 * )
	 */

	@Override
	public Collection<Permissao> consultarPermissoes(Collection<Papel> colecaoPapeis) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadePermissao().getSimpleName());
		hql.append(" permissao ");
		hql.append(" where ");
		hql.append(" permissao.papel.chavePrimaria in ( :idPapeis ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("idPapeis", Util.collectionParaArrayChavesPrimarias(colecaoPapeis));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#listarRecursosUsuario(long)
	 */
	@Override
	public Collection<Recurso> listarRecursosUsuario(long chaveUsuario) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct ");
		hql.append(" recursos ");
		hql.append(" from ");
		hql.append(getClasseEntidadePapel().getSimpleName());
		hql.append(" papel ");
		hql.append(" inner join  papel.permissoes permissoes");
		hql.append(" inner join  permissoes.operacao operacao");
		hql.append(" inner join  operacao.recursos recursos");
		hql.append(" inner join fetch recursos.operacao op");
		hql.append(" inner join fetch op.menu menu");
		hql.append(" inner join fetch op.modulo modulo");
		hql.append(" inner join  papel.usuarios usuarios");
		hql.append(" where usuarios.chavePrimaria = :chavePrimaria");
		hql.append(" ORDER BY recursos.chavePrimaria");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chaveUsuario);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso#
	 * obterChaveOperacaoSistema
	 * (br.com.ggas.controleacesso
	 * .impl.ChaveOperacaoSistema operacao)
	 */
	@Override
	public Long obterChaveOperacaoSistema(ChaveOperacaoSistema chaveOperacaoSistema) {

		String chave = mapaChavesOperacaoSistema.get(chaveOperacaoSistema.toString());

		if (chave == null) {
			return null;
		} else {
			return Long.valueOf(chave);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #validarLogon(br.com.ggas.
	 * controleacesso.Usuario)
	 */
	@Override
	public Boolean validarLogon(Usuario usuario) throws GGASException {

		Boolean efetuarLogon = Boolean.TRUE;
		ControladorUsuario controladorUsuario = (ControladorUsuario) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);

		if(usuario != null) {
			Usuario usuarioPesquisado = controladorUsuario.buscar(usuario.getLogin());
			if(usuarioPesquisado != null) {
				String senhaCriptografada = Util.criptografarSenha(usuario.getSenha(), usuario.getSenha(), Constantes.HASH_CRIPTOGRAFIA);
				if(usuarioPesquisado.getSenha().equals(senhaCriptografada)
								&& ((usuarioPesquisado.isPrimeiroAcesso() != null && usuarioPesquisado.isPrimeiroAcesso()) || controladorUsuario
												.verificarSenhaExpirada(usuarioPesquisado))) {

					efetuarLogon = Boolean.FALSE;
				}
			}

		}

		return efetuarLogon;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#verificarUnicoUsuarioAutorizado(br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public boolean verificarUnicoUsuarioAutorizado(Usuario ultimoUsuarioAlteracao) {

		Collection<Usuario> colecaoUsuario = null;
		Collection<Papel> colecaoPapeis = consultarPapeisAutorizadosTarifa();
		if(colecaoPapeis != null && !colecaoPapeis.isEmpty()) {
			colecaoUsuario = consultarUsuarioDosPapeis(colecaoPapeis);
		}

		boolean retorno = Boolean.FALSE;
		if(colecaoUsuario != null && colecaoUsuario.size() == 1 && colecaoUsuario.contains(ultimoUsuarioAlteracao)) {
			retorno = Boolean.TRUE;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#consultarUsuarioDosPapeis(java.util.Collection)
	 */
	@Override
	public Collection<Usuario> consultarUsuarioDosPapeis(Collection<Papel> colecaoPapeis) {

		Collection<Usuario> colecaoUsuario;
		StringBuilder hqlUsuario = new StringBuilder();
		hqlUsuario.append(" select distinct ");
		hqlUsuario.append(" usuario ");
		hqlUsuario.append(" from ");
		hqlUsuario.append(getClasseEntidadeUsuario().getSimpleName());
		hqlUsuario.append(" usuario ");
		hqlUsuario.append(" inner join usuario.papeis papeis ");
		hqlUsuario.append(" where papeis.chavePrimaria in ( :chavesPapeis ) ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlUsuario.toString());
		query.setParameterList("chavesPapeis", Util.collectionParaArrayChavesPrimarias(colecaoPapeis));
		colecaoUsuario = query.list();
		return colecaoUsuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#consultarPapeisAutorizadosTarifa()
	 */
	@Override
	public Collection<Papel> consultarPapeisAutorizadosTarifa() {

		StringBuilder hqlPermissao = new StringBuilder();
		hqlPermissao.append(" select ");
		hqlPermissao.append(" permissao.papel ");
		hqlPermissao.append(" from ");
		hqlPermissao.append(getClasseEntidadePermissao().getSimpleName());
		hqlPermissao.append(" permissao ");
		hqlPermissao.append(" inner join permissao.operacao operacao ");
		hqlPermissao.append(" inner join operacao.recursos recursos ");
		hqlPermissao.append(" where recursos.recurso like '%autorizarTarifa%' ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlPermissao.toString());
		return query.list();
	}

	/**
	 * Listar operacoes.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAcesso
	 * #listarOperacoes(java.lang.Long)
	 */
	public Collection<Operacao> listarOperacoes() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeOperacao().getSimpleName());
		hql.append(" operacao ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#obterOperacaoSistema(java.lang.Long)
	 */
	@Override
	public Operacao obterOperacaoSistema(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from OperacaoImpl ");
		hql.append(" operacao ");
		hql.append(" where operacao.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimaria", chavePrimaria);

		query.setCacheable(true);

		return (Operacao) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#consultarPapeisAutorizadosProgramacaoConsumo()
	 */
	@Override
	public Collection<Papel> consultarPapeisAutorizadosProgramacaoConsumo() {

		StringBuilder hqlPermissao = new StringBuilder();
		hqlPermissao.append(" select ");
		hqlPermissao.append(" permissao.papel ");
		hqlPermissao.append(" from ");
		hqlPermissao.append(getClasseEntidadePermissao().getSimpleName());
		hqlPermissao.append(" permissao ");
		hqlPermissao.append(" inner join permissao.operacao operacao ");
		hqlPermissao.append(" inner join operacao.recursos recursos ");
		hqlPermissao.append(" where recursos.recurso like '%manterProgramacaoConsumo%' ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlPermissao.toString());
		return query.list();
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#obterOperacaoSistema(java.lang.Long)
	 */
	@Override
	public Operacao obterOperacaoSistemaPelaDescricao(String descricao) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from OperacaoImpl ");
		hql.append(" operacao ");
		hql.append(" where operacao.descricao = :descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("descricao", descricao);

		query.setCacheable(true);

		return (Operacao) query.uniqueResult();
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAcesso#obterOperacaoSistema(java.lang.Long)
	 */
	@Override
	public Operacao obterOperacaoSistemaPelaDescricaoOperacaoEMenu(String descricaoOperacao, String descricaoMenu) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select operacao ");
		hql.append(" from OperacaoImpl ");
		hql.append(" operacao ");
		hql.append(" inner join operacao.menu");
		hql.append(" menu");
		hql.append(" where operacao.descricao = :descricaoOperacao ");
		hql.append(" and menu.descricao = :descricaoMenu ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("descricaoOperacao", descricaoOperacao);
		query.setParameter("descricaoMenu", descricaoMenu);

		query.setCacheable(true);

		return (Operacao) query.uniqueResult();
	}

}
