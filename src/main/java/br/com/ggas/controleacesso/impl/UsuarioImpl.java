/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por representar um usuário.
 *
 */
public class UsuarioImpl extends EntidadeNegocioImpl implements Usuario {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -7178167019883677266L;

	private String login;

	private String usuarioDominio;

	private String senha;

	private String senhaConfirmada;

	private Collection<Papel> papeis = new HashSet<>();

	private Date ultimoAcesso;

	private boolean senhaExpirada;

	private boolean administradorAtendimento;

	private int tentativasSenhaErrada;

	private Funcionario funcionario;

	private Date dataExpirarSenha;

	private Boolean primeiroAcesso;

	private Integer periodicidadeSenha;

	private Date dataCriacaoSenha;

	private Collection<Favoritos> favoritos = new HashSet<>();

	private String token;

	public UsuarioImpl() {
	}

	/**
	 * Construtor responsável por criar uma instancia de construtorUsuarioFuncionario
	 * @param chavePrimaria the chave Primaria
	 * @param chavePrimariaFuncionario the chave primaria funcionario
	 * @param nomeFuncionario the nome Funcionario
	 * @param chavePrimariaUsuarioFuncionario the chave Primaria Usuario Funcionario
	 *
	 */
	public UsuarioImpl(Long chavePrimaria, Long chavePrimariaFuncionario, String nomeFuncionario, Long chavePrimariaUsuarioFuncionario) {
		construtorUsuarioFuncionario(chavePrimaria, chavePrimariaFuncionario, nomeFuncionario, chavePrimariaUsuarioFuncionario);
	}

	/**
	 * Construtor responsável por criar uma instancia de construtorUsuarioFuncionario
	 * @param chavePrimaria the chave Primaria
	 * @param login the login
	 * @param senha the senha
	 * @param token the token
	 *
	 */
	public UsuarioImpl(Long chavePrimaria, String login, String senha, String token) {
		this.setChavePrimaria(chavePrimaria);
		this.login = login;
		this.senha = senha;
		this.token = token;
	}

	private void construtorUsuarioFuncionario(Long chavePrimaria, Long chavePrimariaFuncionario, String nomeFuncionario,
			Long chavePrimariaUsuarioFuncionario) {
		setChavePrimaria(chavePrimaria);
		this.funcionario = new FuncionarioImpl();
		this.funcionario.setChavePrimaria(chavePrimariaFuncionario);
		this.funcionario.setNome(nomeFuncionario);
		Usuario usuario = new UsuarioImpl();
		usuario.setChavePrimaria(chavePrimariaUsuarioFuncionario);
		this.funcionario.setUsuario(usuario);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getTentativasSenhaErrada()
	 */
	@Override
	public int getTentativasSenhaErrada() {

		return tentativasSenhaErrada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setTentativasSenhaErrada(int)
	 */
	@Override
	public void setTentativasSenhaErrada(int tentativasSenhaErrada) {

		this.tentativasSenhaErrada = tentativasSenhaErrada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario# getUltimoAcesso()
	 */
	@Override
	public Date getUltimoAcesso() {
		Date data = null;
		if (this.ultimoAcesso != null) {
			data = (Date) this.ultimoAcesso.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario# setUltimoAcesso(java.util.Date)
	 */
	@Override
	public void setUltimoAcesso(Date ultimoAcesso) {
		if (ultimoAcesso != null) {
			this.ultimoAcesso = (Date) ultimoAcesso.clone();
		} else {
			this.ultimoAcesso = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * isSenhaExpirada()
	 */
	@Override
	public boolean isSenhaExpirada() {

		return senhaExpirada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setSenhaExpirada(boolean)
	 */
	@Override
	public void setSenhaExpirada(boolean senhaExpirada) {

		this.senhaExpirada = senhaExpirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getLogin()
	 */
	@Override
	public String getLogin() {

		return login;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setLogin(java.lang.String)
	 */
	@Override
	public void setLogin(String login) {

		this.login = login;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getSenha()
	 */
	@Override
	public String getSenha() {

		return senha;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setSenha(java.lang.String)
	 */
	@Override
	public void setSenha(String senha) {

		this.senha = senha;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getSenhaConfirmada()
	 */
	@Override
	public String getSenhaConfirmada() {

		return senhaConfirmada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setSenhaConfirmada(java.lang.String)
	 */
	@Override
	public void setSenhaConfirmada(String senhaConfirmada) {

		this.senhaConfirmada = senhaConfirmada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getPapeis()
	 */
	@Override
	public Collection<Papel> getPapeis() {

		return papeis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setPapeis(java.util.Collection)
	 */
	@Override
	public void setPapeis(Collection<Papel> papeis) {

		this.papeis = papeis;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.login == null || this.login.length() == 0) {
			stringBuilder.append(USUARIO_ROTULO_LOGIN);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.senha == null || this.senha.length() == 0) {
			stringBuilder.append(USUARIO_ROTULO_SENHA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.papeis == null || this.papeis.isEmpty()) {
			stringBuilder.append(USUARIO_ROTULO_PAPEL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#getFuncionario()
	 */
	@Override
	public Funcionario getFuncionario() {

		return funcionario;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#setFuncionario(br.com.ggas.cadastro.funcionario.Funcionario)
	 */
	@Override
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getDataExpirarSenha()
	 */
	@Override
	public Date getDataExpirarSenha() {
		Date data = null;
		if(this.dataExpirarSenha != null) {
			data = (Date) dataExpirarSenha.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setDataExpirarSenha(java.util.Date)
	 */
	@Override
	public void setDataExpirarSenha(Date dataExpirarSenha) {
		if(dataExpirarSenha != null) {
			this.dataExpirarSenha = (Date) dataExpirarSenha.clone();
		} else {
			this.dataExpirarSenha = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * isPrimeiroAcesso()
	 */
	@Override
	public Boolean isPrimeiroAcesso() {

		return primeiroAcesso;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setPrimeiroAcesso(java.lang.Boolean)
	 */
	@Override
	public void setPrimeiroAcesso(Boolean primeiroAcesso) {

		this.primeiroAcesso = primeiroAcesso;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getPeriodicidadeSenha()
	 */
	@Override
	public Integer getPeriodicidadeSenha() {

		return periodicidadeSenha;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setPeriodicidadeSenha(java.lang.Integer)
	 */
	@Override
	public void setPeriodicidadeSenha(Integer periodicidadeSenha) {

		this.periodicidadeSenha = periodicidadeSenha;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getDataCriacaoSenha()
	 */
	@Override
	public Date getDataCriacaoSenha() {
		Date data = null;
		if(this.dataCriacaoSenha != null) {
			data = (Date) dataCriacaoSenha.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setDataCriacaoSenha(java.util.Date)
	 */
	@Override
	public void setDataCriacaoSenha(Date dataCriacaoSenha) {
		if(dataCriacaoSenha != null) {
			this.dataCriacaoSenha = (Date) dataCriacaoSenha.clone();
		} else {
			this.dataCriacaoSenha = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#getFavoritos()
	 */
	@Override
	public Collection<Favoritos> getFavoritos() {

		return favoritos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#setFavoritos(java.util.Collection)
	 */
	@Override
	public void setFavoritos(Collection<Favoritos> favoritos) {

		this.favoritos = favoritos;

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#getDescricao()
	 */
	@Override
	public String getDescricao(){
		return funcionario.getNome();
	}

	/**
	 * @return administradorAtendimento
	 */
	public boolean isAdministradorAtendimento() {
		return administradorAtendimento;
	}

	/**
	 * @param administradorAtendimento
	 */
	public void setAdministradorAtendimento(boolean administradorAtendimento) {
		this.administradorAtendimento = administradorAtendimento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#getUsuarioDominio()
	 */
	public String getUsuarioDominio() {
		return usuarioDominio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Usuario#setUsuarioDominio(java.lang.String)
	 */
	public void setUsuarioDominio(String usuarioDominio) {
		this.usuarioDominio = usuarioDominio;
	}

	@Override
	public String getToken() {
		return token;
	}

	@Override
	public void setToken(String token) {
		this.token = token;
	}
}
