/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso.impl;

import org.hibernate.Query;

import br.com.ggas.controleacesso.ControladorHistoricoSenha;
import br.com.ggas.controleacesso.HistoricoSenha;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorHistoricoSenhaImpl extends ControladorNegocioImpl implements ControladorHistoricoSenha {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(HistoricoSenha.BEAN_ID_HISTORICO_SENHA);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoSenha.BEAN_ID_HISTORICO_SENHA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.
	 * ControladorHistoricoSenha
	 * #verificarHistoricoSenha(java.lang.Long,
	 * java.lang.String)
	 */
	@Override
	public void verificarHistoricoSenha(Long chavePrimaria, String senha) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count (*) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" usuario.chavePrimaria = :idUsuario");
		hql.append(" and senha = :senha");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idUsuario", chavePrimaria);
		query.setString("senha", senha);

		Long quantidadeSenhas = (Long) query.uniqueResult();

		if(quantidadeSenhas > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_JA_UTILIZADA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.
	 * ControladorHistoricoSenha
	 * #inserirHistoricoSenha
	 * (br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void inserirHistoricoSenha(Usuario usuario) throws GGASException {

		HistoricoSenha historicoSenha = (HistoricoSenha) this.criar();

		historicoSenha.setDadosAuditoria(usuario.getDadosAuditoria());
		historicoSenha.setUsuario(usuario);
		historicoSenha.setSenha(usuario.getSenha());

		this.inserir(historicoSenha);

	}

}
