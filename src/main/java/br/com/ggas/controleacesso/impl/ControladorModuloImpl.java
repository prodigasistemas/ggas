/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.controleacesso.impl;

import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 *
 */
class ControladorModuloImpl extends ControladorNegocioImpl implements ControladorModulo {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Modulo.BEAN_ID_MODULO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #criarOperacao()
	 */
	@Override
	public EntidadeNegocio criarOperacao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Operacao.BEAN_ID_OPERACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #criarRecurso()
	 */
	@Override
	public EntidadeNegocio criarRecurso() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Recurso.BEAN_ID_RECURSO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Modulo.BEAN_ID_MODULO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #buscarOperacao(java.lang.String)
	 */
	@Override
	public Collection<Operacao> buscarOperacaoPorRecurso(String recurso) throws NegocioException {

		String consulta = "select op from OperacaoImpl op join fetch op.modulo mod join fetch op.recursos rec where op in "
						+ "(select r.operacao from RecursoImpl r join r.operacao operacao  where r.recurso = :nome)";
		Query hql = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(consulta);
		hql.setString("nome", recurso);

		return hql.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #consultarOperacoesBatch()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Operacao> consultarOperacoesBatch() throws NegocioException {

		Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();

		Criteria criteria = sessao.createCriteria(getClasseEntidadeOperacao()).add(Restrictions.eq("tipo", Operacao.BATCH))
						.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #consultarOperacoesBatch(br.com.ggas.
	 * controleacesso.Usuario)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Operacao> consultarOperacoesBatch(Usuario usuario) throws NegocioException {

		Collection<Operacao> operacoes = null;
		boolean papelAdmin = false;
		if(usuario != null) {
			Collection<Papel> papeis = usuario.getPapeis();
			List<Long> chavesPrimariasOperacoes = new ArrayList<>();
			if(papeis != null && !papeis.isEmpty()) {
				for (Papel papel : papeis) {
					if(papel.isAdmin()) {
						papelAdmin = true;
						break;
					}
					Collection<Permissao> permissoes = papel.getPermissoes();
					for (Permissao permissao : permissoes) {
						chavesPrimariasOperacoes.add(permissao.getOperacao().getChavePrimaria());
					}
				}
			}
			if(papelAdmin) {
				operacoes = this.consultarOperacoesBatch();
			} else {
				if(!chavesPrimariasOperacoes.isEmpty()) {
					Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
					Criteria criteria = sessao.createCriteria(getClasseEntidadeOperacao()).add(Restrictions.eq("tipo", Operacao.BATCH))
									.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimariasOperacoes.toArray()))
									.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
					operacoes = criteria.list();
				}
			}

		}

		return operacoes;
	}

	private Class<?> getClasseEntidadeOperacao() {

		return ServiceLocator.getInstancia().getClassPorID(Operacao.BEAN_ID_OPERACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #consultarOperacoes(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Operacao> consultarOperacoes(Long chaveModulo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeOperacao());
		criteria.createAlias("modulo", "modulo");
		criteria.add(Restrictions.eq("modulo.chavePrimaria", chaveModulo));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #consultarOperacoes(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Operacao> consultarOperacoes(Long[] chavesPrimarias) throws NegocioException {


		Criteria criteria = createCriteria(getClasseEntidadeOperacao());
		if(chavesPrimarias != null && chavesPrimarias.length > 0) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #consultarModulos(java.util.Map)
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Modulo> consultarModulos(Map<String, String> filtro) throws NegocioException {

		Criteria criteria = createCriteria(OperacaoImpl.class, "operacaoSistema");
		criteria.createAlias("operacaoSistema.modulo", "modulo", Criteria.INNER_JOIN);

		if(filtro != null) {
			String descricao = filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao));
			}

			String indicadorContabil = filtro.get("indicadorContabil");
			if(!StringUtils.isEmpty(indicadorContabil)) {
				if("true".equals(indicadorContabil)) {
					criteria.add(Restrictions.eq("modulo.indicadorContabil", true));
				} else {
					criteria.add(Restrictions.eq("modulo.indicadorContabil", false));
				}
			}
			String tipo = filtro.get("tipo");
			if(StringUtils.isEmpty(tipo)) {
				criteria.add(Restrictions.eq("tipo", Operacao.BATCH));
			}
		}

		criteria.addOrder(Order.asc("modulo.descricao"));

		Collection<Modulo> listaModulo = new ArrayList<>();
		Collection<Operacao> listaoperacao = criteria.list();

		for (Operacao operacao : listaoperacao) {
			if(!listaModulo.contains(operacao.getModulo())) {
				listaModulo.add(operacao.getModulo());
			}
		}

		return listaModulo;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #
	 * consultarModulosPorChaves(java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Modulo> consultarModulosPorChaves(Long[] chavesPrimarias) throws NegocioException {


		Criteria criteria = getCriteria();

		if(chavesPrimarias != null && chavesPrimarias.length > 0) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #
	 * verificarSeExisteOperacaoCadastrada(br.com.
	 * ggas.controleacesso.Modulo,
	 * br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	public void verificarSeExisteOperacaoCadastrada(Modulo modulo, Operacao operacao) throws NegocioException {

		for (Operacao operacaoExistente : modulo.getOperacoes()) {
			if(operacaoExistente.getDescricao().equals(operacao.getDescricao())
					&& operacaoExistente.getTipo() == operacao.getTipo()
					&& operacaoExistente.getChavePrimaria() != operacao.getChavePrimaria()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ENTIDADE_CADASTRADA, Operacao.ENTIDADE_ROTULO_OPERACAO);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #
	 * verificarSeExisteRecursoCadastrado(br.com.ggas
	 * .controleacesso.Operacao,
	 * br.com.ggas.controleacesso.Recurso)
	 */
	@Override
	public void verificarSeExisteRecursoCadastrado(Operacao operacao, Recurso recurso) throws NegocioException {

		for (Recurso recursoExistente : operacao.getRecursos()) {
			if (recursoExistente.getRecurso().equals(recurso.getRecurso())
							&& recursoExistente.getChavePrimaria() != recurso.getChavePrimaria()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ENTIDADE_CADASTRADA, Recurso.RECURSO_ROTULO_RECURSO);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Modulo modulo = (Modulo) entidadeNegocio;
		verificarSeExisteModuloCadastrado(modulo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #
	 * verificarSeExisteModuloCadastrado(br.com.ggas
	 * .controleacesso.Modulo)
	 */
	@Override
	public void verificarSeExisteModuloCadastrado(Modulo modulo) throws NegocioException {

		Map<String, String> filtro = new HashMap<>();
		filtro.put(TabelaAuxiliar.ATRIBUTO_DESCRICAO, modulo.getDescricao());
		Collection<Modulo> modulos = consultarModulos(filtro);
		if(modulos != null && !modulos.isEmpty()) {
			Modulo moduloExistente = modulos.iterator().next();
			if (moduloExistente.getDescricao().equals(modulo.getDescricao())
							&& moduloExistente.getChavePrimaria() != modulo.getChavePrimaria()) {
				throw new NegocioException(ERRO_NEGOCIO_MODULO_EXISTE_DESCRICAO, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #buscarOperacaoPorChave(java.lang.Long)
	 */
	@Override
	public Operacao buscarOperacaoPorChave(Long chavePrimaria) throws NegocioException {


		Criteria criteria = createCriteria(getClasseEntidadeOperacao());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		return (Operacao) criteria.uniqueResult();
	}

	private Class<?> getClasseEntidadeRecurso() {

		return ServiceLocator.getInstancia().getClassPorID(Recurso.BEAN_ID_RECURSO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #consultarRecursos()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Recurso> consultarRecursos(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeRecurso());
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long operacao = (Long) filtro.get("operacao");
			if(operacao != null && operacao > 0) {
				criteria.add(Restrictions.eq("operacao.chavePrimaria", operacao));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #buscarRecursoPorChave(java.lang.Long)
	 */
	@Override
	public Recurso buscarRecursoPorChave(Long chavePrimaria) throws NegocioException {


		Criteria criteria = createCriteria(getClasseEntidadeRecurso());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		return (Recurso) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #removerRecursos(java.lang.Long[])
	 */
	@Override
	public void removerRecursos(Long[] chavesPrimarias) throws NegocioException {


		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put("chavesPrimarias", chavesPrimarias);
			Collection<Recurso> recursos = consultarRecursos(filtro);
			if((recursos != null) && (!recursos.isEmpty())) {
				for (Recurso recurso : recursos) {
					super.remover(recurso);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #validarRemoverRecursos(java.lang.Long[])
	 */
	@Override
	public void validarRemoverRecursos(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #removerOperacoes(java.lang.Long[])
	 */
	@Override
	public void removerOperacoes(Long[] chavesPrimarias) throws NegocioException, ConcorrenciaException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {

			Operacao operacao = buscarOperacaoPorChave(chavesPrimarias[0]);
			Modulo modulo = operacao.getModulo();

			Collection<Operacao> operacoes = modulo.getOperacoes();

			Collection<Operacao> colecaoDeOperacoes = consultarOperacoes(chavesPrimarias);
			operacoes.removeAll(colecaoDeOperacoes);

			super.atualizar(modulo);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #validarRemoverOperacoes(java.lang.Long[])
	 */
	@Override
	public void validarRemoverOperacoes(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #removerModulos(java.lang.Long[])
	 */
	@Override
	public void removerModulos(Long[] chavesPrimarias) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {

			Collection<Modulo> modulos = consultarModulosPorChaves(chavesPrimarias);
			if((modulos != null) && (!modulos.isEmpty())) {
				for (Modulo modulo : modulos) {
					super.remover(modulo);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #validarRemoverModulos(java.lang.Long[])
	 */
	@Override
	public void validarRemoverModulos(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorModulo
	 * #
	 * consultarOperacoesBatchPorModulo(java.lang.
	 * Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Operacao> consultarOperacoesBatchPorModulo(long idModulo) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeOperacao().getSimpleName());
		hql.append(" where ");
		hql.append(" modulo.chavePrimaria = ? ");
		hql.append(" and tipo = ");
		hql.append(Operacao.BATCH);
		hql.append(" and exibir = :exibir");
		hql.append(" order by descricao asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idModulo);
		query.setParameter("exibir", true);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorModulo#consultarTodosModulos()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Modulo> consultarTodosModulos() throws NegocioException {

		Criteria criteria = createCriteria(ModuloImpl.class);

		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorModulo#obterModuloPorOperacaoSistemaMenu(java.lang.Long)
	 */
	@Override
	public Modulo obterModuloPorOperacaoSistemaMenu(Long chavePrimaria) {

		DetachedCriteria dc = DetachedCriteria.forClass(getClasseEntidadeOperacao());
		dc.createAlias("modulo", "modulo");
		dc.createAlias("menu", "menu");
		dc.createAlias("menu.menuPai", "menuPai");
		dc.add(Restrictions.eq("menuPai.chavePrimaria", chavePrimaria));
		dc.setProjection(Projections.property("modulo.chavePrimaria"));

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.add(Subqueries.propertyIn(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, dc));

		return (Modulo) criteria.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorModulo#buscarOperacaoPorRecurso(java.util.Collection)
	 */
	@Override
	public Collection<Operacao> buscarOperacaoPorRecurso(Collection<Recurso> listaRecursos) {

		String consulta = "select op from OperacaoImpl op join fetch op.modulo mod join fetch op.recursos rec where op in "
						+ "(select r.operacao from RecursoImpl r join r.operacao operacao  where r.recurso in :nome)";
		Query hql = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(consulta);
		hql.setParameterList("nome", listaRecursos);

		return hql.list();
	}

	@Override
	public Optional<Menu> buscarMenu(String descricaoMenu, String descricaoOperacao, Long modulo, List<Long> papeis) {

		if (papeis == null || papeis.isEmpty()) {
			papeis = Collections.singletonList(0L);
		}

		String consulta = "SELECT m FROM PermissaoImpl p INNER JOIN p.operacao op INNER JOIN op.menu m "
				+ "WHERE lower(m.descricao) LIKE :descricaoMenu AND lower(op.descricao) LIKE :descricaoOperacao "
				+ "AND op.modulo.chavePrimaria = :modulo AND p.papel.chavePrimaria in (:papeis)";
		Query hql = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(consulta);
		hql.setParameter("descricaoMenu", "%" + descricaoMenu.toLowerCase().trim() + "%");
		hql.setParameter("descricaoOperacao", "%" + descricaoOperacao.toLowerCase().trim() + "%");
		hql.setParameter("modulo", modulo);
		hql.setParameterList("papeis", papeis);

		LOG.info("Verificar Permissao: [Menu: " + descricaoMenu.toLowerCase().trim()
				+ ", Operacao: " + descricaoOperacao.toLowerCase().trim()
				+ ", Modulo: " + modulo
				+ ", Papeis: " + papeis
				+ "]");

		return hql.list().stream().findFirst();
	}

}
