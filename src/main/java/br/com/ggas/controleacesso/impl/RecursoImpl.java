/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 * 
 */
class RecursoImpl extends EntidadeNegocioImpl implements Recurso {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
     * 
     */
	private static final long serialVersionUID = -2605170974096290330L;

	private String recurso;

	private boolean web;

	private Operacao operacao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Recurso#isWeb()
	 */
	@Override
	public boolean isWeb() {

		return web;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Recurso#setWeb
	 * (boolean)
	 */
	@Override
	public void setWeb(boolean web) {

		this.web = web;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Recurso
	 * #getRecurso()
	 */
	@Override
	public String getRecurso() {

		return recurso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Recurso
	 * #setRecurso(java.lang.String)
	 */
	@Override
	public void setRecurso(String recurso) {

		this.recurso = recurso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Recurso#getOperacao
	 * ()
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Recurso#setOperacao
	 * (br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.recurso == null || this.recurso.length() == 0) {
			stringBuilder.append("RECURSO_ROTULO_RECURSO");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
