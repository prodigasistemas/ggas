/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pela alcada.
 */
public class AlcadaImpl extends EntidadeNegocioImpl implements Alcada {

	private static final int NUMERO_DECIMAIS = 2;

	private static final long serialVersionUID = -6286336458824647239L;

	private Coluna coluna;

	private BigDecimal valorInicial;

	private BigDecimal valorFinal;

	private Papel papel;

	private Date dataInicial;

	private Date dataFinal;

	private Menu menu;

	/**
	 * @return the coluna
	 */
	@Override
	public Coluna getColuna() {

		return coluna;
	}

	/**
	 * @param coluna
	 */
	@Override
	public void setColuna(Coluna coluna) {

		this.coluna = coluna;
	}

	/**
	 * @return the valorInicial
	 */
	@Override
	public BigDecimal getValorInicial() {

		return valorInicial;
	}

	/**
	 * @param valorInicial
	 */
	@Override
	public void setValorInicial(BigDecimal valorInicial) {

		this.valorInicial = valorInicial;
	}

	/**
	 * @return the valorFinal
	 */
	@Override
	public BigDecimal getValorFinal() {

		return valorFinal;
	}

	/**
	 * @param valorFinal
	 */
	@Override
	public void setValorFinal(BigDecimal valorFinal) {

		this.valorFinal = valorFinal;
	}

	/**
	 * @return the papeis
	 */
	@Override
	public Papel getPapel() {

		return papel;
	}

	/**
	 * @param papeis
	 */
	@Override
	public void setPapel(Papel papel) {

		this.papel = papel;
	}

	/**
	 * Validar dados.
	 * 
	 * @return the coluna
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Alcada#
	 * getDataInicial()
	 */
	@Override
	public Date getDataInicial() {
		Date data = null;
		if(this.dataInicial != null) {
			data = (Date) dataInicial.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.controleacesso.Alcada# setDataInicial(java.util.Date)
	 */
	@Override
	public void setDataInicial(Date dataInicial) {
		if (dataInicial != null) {
			this.dataInicial = (Date) dataInicial.clone();
		} else {
			this.dataInicial = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Alcada#getDataFinal
	 * ()
	 */
	@Override
	public Date getDataFinal() {
		Date data = null;
		if(this.dataFinal != null) {
			data = (Date) dataFinal.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Alcada#setDataFinal
	 * (java.util.Date)
	 */
	@Override
	public void setDataFinal(Date dataFinal) {
		if(dataFinal != null) {
			this.dataFinal = (Date) dataFinal.clone();
		} else {
			this.dataFinal = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Alcada#getMenu()
	 */
	@Override
	public Menu getMenu() {

		return menu;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Alcada#setMenu
	 * (br.com.ggas.geral.Menu)
	 */
	@Override
	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	@Override
	public String getValorInicialFormatado() {

		String valorFormatado = "";
		if (valorInicial != null) {
			valorFormatado = Util.converterCampoValorDecimalParaString("Valor inicial", valorInicial,
					Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
		}
		return valorFormatado;
	}

	@Override
	public String getValorFinalFormatado() {

		String valorFormatado = "";
		if (valorFinal != null) {
			valorFormatado = Util.converterCampoValorDecimalParaString("Valor final", valorFinal,
					Constantes.LOCALE_PADRAO, NUMERO_DECIMAIS);
		}
		return valorFormatado;
	}

}
