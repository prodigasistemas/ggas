/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos relacionados a entidade Operação 
 *
 */
public class OperacaoImpl extends EntidadeNegocioImpl implements Operacao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -6428841158894455754L;

	private int tipo;

	private Modulo modulo;

	private Menu menu;

	private String descricao;

	private boolean auditavel;

	private Collection<Recurso> recursos = new HashSet<>();

	private boolean exibir;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#isAuditavel
	 * ()
	 */
	@Override
	public boolean isAuditavel() {

		return auditavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Operacao#
	 * setAuditavel(boolean)
	 */
	@Override
	public void setAuditavel(boolean auditavel) {

		this.auditavel = auditavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#getTipo
	 * ()
	 */
	@Override
	public int getTipo() {

		return tipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#setTipo
	 * (int)
	 */
	@Override
	public void setTipo(int tipo) {

		this.tipo = tipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#getModulo
	 * ()
	 */
	@Override
	public Modulo getModulo() {

		return modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#setModulo
	 * (br.com.procenge
	 * .ggas.controleacesso.Modulo)
	 */
	@Override
	public void setModulo(Modulo modulo) {

		this.modulo = modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Operacao
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Operacao#
	 * setDescricao(java.lang
	 * .String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#getRecursos
	 * ()
	 */
	@Override
	public Collection<Recurso> getRecursos() {

		return recursos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#setRecursos
	 * (java.util.Collection
	 * )
	 */
	@Override
	public void setRecursos(Collection<Recurso> recursos) {

		this.recursos = recursos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#getMenu
	 * ()
	 */
	@Override
	public Menu getMenu() {

		return menu;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Operacao#setMenu
	 * (br.com.ggas.geral.Menu)
	 */
	@Override
	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	@Override
	public boolean isExibir() {
		return this.exibir;
	}

	@Override
	public void setExibir(boolean exibir) {
		this.exibir = exibir;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados
	 * ()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.descricao == null || this.descricao.length() == 0) {
			stringBuilder.append(OPERACAO_ROTULO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		for (Iterator<Recurso> iterator = this.recursos.iterator(); iterator.hasNext();) {
			Map<String, Object> validarDados = iterator.next().validarDados();
			erros.putAll(validarDados);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
