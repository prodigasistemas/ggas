/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cobranca.parcelamento.ControladorParcelamento;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.programacao.ControladorProgramacao;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.controleacesso.Alcada;
import br.com.ggas.controleacesso.AlcadaVO;
import br.com.ggas.controleacesso.AlteracaoAlcadaVO;
import br.com.ggas.controleacesso.AutorizacoesPendentesVO;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorAlcada;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Controlador responaável pela entidade Alcada
 *
 */
public class ControladorAlcadaImpl extends ControladorNegocioImpl implements ControladorAlcada {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final Logger LOG = Logger.getLogger(ControladorAlcadaImpl.class);

	private static final String ID_MENU = "idMenu";

	private static final String ID_PAPEL = "idPapel";

	private static final String DATA_INICIAL_ALCADA = "dataInicialAlcada";

	private static final String DATA_FINAL_ALCADA = "dataFinalAlcada";

	private static final String DESCRICAO_PAPEL = "descricaoPapel";

	private static final String DESCRICAO_FUNCIONALIDADE = "descricaoFuncionalidade";
	
	private static final String STATUS = "status";

	private static final Long CODIGO_STATUS_PENDENTE = initCodigoStatusPendente();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Alcada.BEAN_ID_ALCADA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Alcada.BEAN_ID_ALCADA);
	}

	public Class<?> getClasseEntidadeColuna() {

		return ServiceLocator.getInstancia().getClassPorID(Coluna.BEAN_ID_COLUNA_TABELA);
	}

	/**
	 * Inits the codigo status pendente.
	 * 
	 * @return the long
	 */
	private static Long initCodigoStatusPendente() {

		Long pendente = null;
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		try {
			pendente = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE));

		} catch (Exception e) {
			LOG.error(e);
			pendente = 0L;
		}

		return pendente;

	}

	public String getCodigoStatusPendente() {

		return CODIGO_STATUS_PENDENTE.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * obterListaColunasAlcadaPorMenu(java.lang.Long
	 * )
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Coluna> obterListaColunasAlcadaPorMenu(Long idMenu) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select coluna ");
		hql.append(" from ");
		hql.append(getClasseEntidadeColuna().getSimpleName());
		hql.append(" coluna ");
		hql.append(" inner join coluna.tabela tabela ");
		hql.append(" inner join tabela.menu menu ");
		hql.append(" where ");
		hql.append(" menu.chavePrimaria = :idMenu ");
		hql.append(" and coluna.alcada is true ");
		hql.append(" and tabela.alcada is true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_MENU, idMenu);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #validarPapeis(java.lang.Long[])
	 */
	@Override
	public void validarAlcadas(Long idMenu, Long idPapel, Date dataInicial, Date dataFinal) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatorios = new StringBuilder();

		if (Util.isEmpty(idMenu) || idMenu < 1) {
			camposObrigatorios.append("Menu");
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (Util.isEmpty(idPapel) || idPapel < 1) {
			camposObrigatorios.append("Papel");
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataInicial == null) {
			camposObrigatorios.append("Data de validade inicial");
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (dataFinal == null) {
			camposObrigatorios.append("Data de validade final");
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, camposObrigatorios.length() - LIMITE_CAMPO_OBRIGATORIO));
			throw new NegocioException(erros);
		} else {
			if (dataInicial != null && dataFinal != null && dataInicial.after(dataFinal)) {
				throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
			}
			existenteAlcadaPeriodo(idMenu, idPapel, null, dataInicial, dataFinal, null);
		}
	}

	/**
	 * Valida se o valor inicial é maior que o
	 * valor final caso ambos sejam informados.
	 * 
	 * @param campo
	 *            the campo
	 * @param valorInicial
	 *            the valor inicial
	 * @param valorFinal
	 *            the valor final
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarIntervaloValores(String campo, BigDecimal valorInicial, BigDecimal valorFinal) throws NegocioException {

		if (valorInicial != null && valorFinal != null && valorInicial.compareTo(valorFinal) > 0) {
			throw new NegocioException(Constantes.ERRO_INTERVALO_VALORES_INVALIDO, new Object[] {campo});
		}
	}

	/**
	 * Existente alcada periodo.
	 * 
	 * @param idMenu
	 *            the id menu
	 * @param idPapel
	 *            the id papel
	 * @param idColuna
	 *            the id coluna
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param idAlcadaAtual
	 *            the id alcada atual
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void existenteAlcadaPeriodo(Long idMenu, Long idPapel, Long idColuna, Date dataInicial, Date dataFinal, Long idAlcadaAtual)
					throws NegocioException {

		Long quantidade = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(alcada) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" where alcada.papel.chavePrimaria = :idPapel ");
		hql.append(" and alcada.menu.chavePrimaria = :idMenu ");

		if (idColuna != null && idColuna > 0) {
			hql.append(" and alcada.coluna.chavePrimaria = :idColuna ");
		}
		if (idAlcadaAtual != null && idAlcadaAtual > 0) {
			hql.append(" and alcada.chavePrimaria <> :idAlcadaAtual ");
		}

		// Verifica se existe interseção entre o
		// período de validade informado e os
		// períodos de alçadas já cadastradas.
		hql.append(" and (:dataInicial between alcada.dataInicial and alcada.dataFinal ");
		hql.append(" 	or :dataFinal between alcada.dataInicial and alcada.dataFinal ");
		hql.append(" 	or alcada.dataInicial between :dataInicial and :dataFinal ");
		hql.append(" 	or alcada.dataFinal between :dataInicial and :dataFinal ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_PAPEL, idPapel);
		query.setLong(ID_MENU, idMenu);
		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);

		if (idColuna != null && idColuna > 0) {
			query.setLong("idColuna", idColuna);
		}
		if (idAlcadaAtual != null && idAlcadaAtual > 0) {
			query.setLong("idAlcadaAtual", idAlcadaAtual);
		}

		quantidade = (Long) query.uniqueResult();

		if (quantidade > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ALCADA_JA_EXISTENTE, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #inserirAlcadas(java.util.Collection)
	 */
	@Override
	public void inserirAlcadas(Collection<Alcada> listaAlcada) throws NegocioException {

		for (Alcada alcada : listaAlcada) {
			validarIntervaloValores(alcada.getColuna().getDescricao(), alcada.getValorInicial(), alcada.getValorFinal());
			this.inserir(alcada);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #obterListaAlcadasPorMenu(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AlcadaVO> obterListaAlcadaVO(Map<String, Object> filtro) {

		Query query = null;

		Long idMenu = (Long) filtro.get(ID_MENU);

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct new br.com.ggas.controleacesso.AlcadaVO ( ");
		hql.append(" papel.descricao ");
		hql.append(" , alcada.dataInicial ");
		hql.append(" , alcada.dataFinal ");
		hql.append(" , menu.descricao ");
		hql.append(" , menu.chavePrimaria ");
		hql.append(" , papel.chavePrimaria ");
		hql.append(" ) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" inner join alcada.menu menu ");
		hql.append(" inner join alcada.papel papel ");
		if (idMenu != null && idMenu > 0) {
			hql.append(" where menu.chavePrimaria = :idMenu ");
			hql.append(" order by papel.descricao, alcada.dataInicial ");
		} else {
			hql.append(" order by menu.descricao, papel.descricao, alcada.dataInicial ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (idMenu != null && idMenu > 0) {
			query.setLong(ID_MENU, idMenu);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #obterDetalheAlcada(java.lang.String,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Alcada> obterlistaAlcada(Map<String, Object> filtro) {

		Query query = null;

		String descricaoFuncionalidade = (String) filtro.get(DESCRICAO_FUNCIONALIDADE);
		String descricaoPapel = (String) filtro.get(DESCRICAO_PAPEL);
		Date dataInicial = (Date) filtro.get(DATA_INICIAL_ALCADA);
		Date dataFinal = (Date) filtro.get(DATA_FINAL_ALCADA);

		StringBuilder hql = new StringBuilder();
		hql.append(" select alcada ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" inner join alcada.menu menu ");
		hql.append(" inner join alcada.papel papel ");
		hql.append(" where papel.descricao = :descricaoPapel ");
		hql.append(" and alcada.dataInicial = :dataInicial ");
		hql.append(" and alcada.dataFinal = :dataFinal ");
		if (!StringUtils.isEmpty(descricaoFuncionalidade)) {
			hql.append(" and menu.descricao = :descricaoFuncionalidade ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("descricaoPapel", descricaoPapel);
		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);
		if (!StringUtils.isEmpty(descricaoFuncionalidade)) {
			query.setString("descricaoFuncionalidade", descricaoFuncionalidade);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAlcada#consultarAlcada(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Alcada> consultarAlcada(Map<String, Object> filtro) {

		Long idTabela = (Long) filtro.get("idTabela");
		Long idPapel = (Long) filtro.get(ID_PAPEL);
		Date dataInicial = (Date) filtro.get("dataInicial");
		Date dataFinal = (Date) filtro.get("dataFinal");

		StringBuilder hql = new StringBuilder();
		hql.append(" select alcada ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" inner join alcada.papel papel ");
		hql.append(" inner join alcada.coluna.tabela tabela ");
		hql.append(" where 1=1 ");

		if (dataInicial != null) {
			hql.append(" and alcada.dataInicial = :dataInicial ");
		}
		if (dataFinal != null) {
			hql.append(" and alcada.dataFinal = :dataFinal ");
		}
		if (idPapel != null && idPapel > 0) {
			hql.append(" and papel.chavePrimaria = :idPapel ");
		}
		if (idTabela != null && idTabela > 0) {
			hql.append(" and tabela.chavePrimaria = :idTabela ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (dataInicial != null) {
			query.setDate("dataInicial", dataInicial);
		}
		if (dataFinal != null) {
			query.setDate("dataFinal", dataFinal);
		}
		if (idPapel != null && idPapel > 0) {
			query.setLong(ID_PAPEL, idPapel);
		}
		if (idTabela != null && idTabela > 0) {
			query.setLong("idTabela", idTabela);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #removerAlcada(java.util.Map)
	 */
	@Override
	public void removerAlcada(String alcadasRemocao) throws GGASException {

		String[] listaDadosAlcadas = alcadasRemocao.split(",");
		for (String dadosAlcada : listaDadosAlcadas) {
			String[] listaDados = dadosAlcada.split("_");

			Map<String, Object> filtro = this.montarFiltroRemocao(listaDados);

			Collection<Alcada> listaAlcadas = this.obterlistaAlcadaRemocao(filtro);

			for (Alcada alcada : listaAlcadas) {
				this.remover(alcada);
			}
		}

	}

	/**
	 * Obterlista alcada remocao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<Alcada> obterlistaAlcadaRemocao(Map<String, Object> filtro) {

		Query query = null;

		Long idMenu = (Long) filtro.get(ID_MENU);
		Long idPapel = (Long) filtro.get(ID_PAPEL);
		Date dataInicial = (Date) filtro.get(DATA_INICIAL_ALCADA);
		Date dataFinal = (Date) filtro.get(DATA_FINAL_ALCADA);

		StringBuilder hql = new StringBuilder();
		hql.append(" select alcada ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" inner join alcada.menu menu ");
		hql.append(" inner join alcada.papel papel ");
		hql.append(" where papel.chavePrimaria = :idPapel ");
		hql.append(" and menu.chavePrimaria = :idMenu ");
		hql.append(" and alcada.dataInicial = :dataInicial ");
		hql.append(" and alcada.dataFinal = :dataFinal ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_PAPEL, idPapel);
		query.setLong(ID_MENU, idMenu);
		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);

		return query.list();
	}

	/**
	 * Montar filtro remocao.
	 * 
	 * @param listaDados
	 *            the lista dados
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<String, Object> montarFiltroRemocao(String[] listaDados) throws GGASException {

		Map<String, Object> filtro = new HashMap<>();
		Long idMenu = Util.converterCampoStringParaValorLong("Menu", listaDados[0]);
		Long idPapel = Util.converterCampoStringParaValorLong("Papel", listaDados[1]);
		String dataInicialAux = listaDados[2].replace("-", "/");
		Date dataInicial = Util.converterCampoStringParaData("Data Inicial", dataInicialAux, Constantes.FORMATO_DATA_BR);
		String dataFinalAux = listaDados[3].replace("-", "/");
		Date dataFinal = Util.converterCampoStringParaData("Data Final", dataFinalAux, Constantes.FORMATO_DATA_BR);

		if ((idMenu != null) && (idMenu > 0)) {
			filtro.put(ID_MENU, idMenu);
		}

		if ((idPapel != null) && (idPapel > 0)) {
			filtro.put(ID_PAPEL, idPapel);
		}

		if (dataInicial != null) {
			filtro.put(DATA_INICIAL_ALCADA, dataInicial);
		}

		if (dataFinal != null) {
			filtro.put(DATA_FINAL_ALCADA, dataFinal);
		}

		return filtro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #obterListaAlteracaoAlcada(java.util.Map)
	 */
	@Override
	public Collection<AlteracaoAlcadaVO> obterListaAlteracaoAlcada(Map<String, Object> filtro) {

		Collection<AlteracaoAlcadaVO> listaAlteracaoAlcadaVO = new ArrayList<>();
		Collection<Alcada> listaAlcadas = this.obterlistaAlcadaRemocao(filtro);
		Menu menu = listaAlcadas.iterator().next().getMenu();
		Papel papel = listaAlcadas.iterator().next().getPapel();

		Collection<Coluna> listaColunas = this.obterListaColunasAlcadaPorMenu(menu.getChavePrimaria());

		for (Coluna coluna : listaColunas) {
			AlteracaoAlcadaVO alteracaoAlcadaVO = new AlteracaoAlcadaVO();
			alteracaoAlcadaVO.setColuna(coluna);
			alteracaoAlcadaVO.setPapel(papel);
			alteracaoAlcadaVO.setMenu(menu);
			for (Alcada alcada : listaAlcadas) {
				if (alcada.getColuna().equals(coluna)) {
					alteracaoAlcadaVO.setAlcada(alcada);
					alteracaoAlcadaVO.setValorInicial(alcada.getValorInicial());
					alteracaoAlcadaVO.setValorFinal(alcada.getValorFinal());
				}
			}
			listaAlteracaoAlcadaVO.add(alteracaoAlcadaVO);
		}

		return listaAlteracaoAlcadaVO;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #alterarAlcada(java.util.Collection,
	 * java.lang.String[], java.lang.String[],
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public void alterarAlcada(Collection<AlteracaoAlcadaVO> listaAlcadas, String[] listaValoresIniciais, String[] listaValoresFinais,
					Date dataInicial, Date dataFinal, Boolean habilitado) throws GGASException {

		int cont = 0;
		for (AlteracaoAlcadaVO alteracaoAlcadaVO : listaAlcadas) {

			Menu menu = alteracaoAlcadaVO.getMenu();
			Papel papel = alteracaoAlcadaVO.getPapel();
			Coluna coluna = alteracaoAlcadaVO.getColuna();

			BigDecimal valorInicial = this.obterValorBigDecimal(listaValoresIniciais[cont], "Valor Inicial");
			BigDecimal valorFinal = this.obterValorBigDecimal(listaValoresFinais[cont], "Valor Final");

			validarIntervaloValores(alteracaoAlcadaVO.getColuna().getDescricao(), valorInicial, valorFinal);

			if (alteracaoAlcadaVO.getAlcada() != null) {
				Alcada alcada = alteracaoAlcadaVO.getAlcada();
				if ((valorInicial != null && valorInicial.compareTo(BigDecimal.ZERO) > 0)
								|| (valorFinal != null && valorFinal.compareTo(BigDecimal.ZERO) > 0)) {
					if ((!alcada.getDataInicial().equals(dataInicial)) || (!alcada.getDataFinal().equals(dataFinal))) {
						if (dataInicial.after(dataFinal)) {
							throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
						}
						this.existenteAlcadaPeriodo(menu.getChavePrimaria(), papel.getChavePrimaria(), coluna.getChavePrimaria(),
										dataInicial, dataFinal, alcada.getChavePrimaria());
					}

					alcada.setValorInicial(valorInicial);
					alcada.setValorFinal(valorFinal);
					alcada.setDataInicial(dataInicial);
					alcada.setDataFinal(dataFinal);
					alcada.setHabilitado(habilitado);

					this.atualizar(alcada);
				} else {
					this.remover(alcada);
				}

				// Se a alçada estiver nula no VO
				// e ,pelo menos , um dos campos
				// de valor for maior que zero,
				// deve ser inserida uma nova
				// alçada.
			} else if ((valorInicial != null && valorInicial.compareTo(BigDecimal.ZERO) > 0)
							|| (valorFinal != null && valorFinal.compareTo(BigDecimal.ZERO) > 0)) {
				if (dataInicial.after(dataFinal)) {
					throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
				}
				this.existenteAlcadaPeriodo(menu.getChavePrimaria(), papel.getChavePrimaria(), coluna.getChavePrimaria(), dataInicial,
								dataFinal, null);

				Alcada alcada = (Alcada) this.criar();

				alcada.setValorInicial(valorInicial);
				alcada.setValorFinal(valorFinal);
				alcada.setDataInicial(dataInicial);
				alcada.setDataFinal(dataFinal);
				alcada.setColuna(coluna);
				alcada.setPapel(papel);
				alcada.setMenu(menu);

				this.inserir(alcada);

				// Devido a abordagem solicitada,
				// pode ser que seja alterada uma
				// alçada que está desabilitada.
				// Como na inserção, sempre insere
				// como habilitada, se faz
				// necessário este workaround.
				if (!habilitado) {
					alcada.setHabilitado(habilitado);
					this.atualizar(alcada);
				}

			}
			cont++;
		}
	}

	/**
	 * Obter valor big decimal.
	 * 
	 * @param valorAux
	 *            the valor aux
	 * @param campo
	 *            the campo
	 * @return the big decimal
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private BigDecimal obterValorBigDecimal(String valorAux, String campo) throws GGASException {

		BigDecimal valor = null;

		if (!StringUtils.isEmpty(valorAux)) {
			valor = Util.converterCampoStringParaValorBigDecimal(campo, valorAux, Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO);
		}

		return valor;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #validarAlcadaExistente(java.lang.Long,
	 * java.lang.Long, java.lang.Long,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public void validarAlcadaExistente(Long idMenu, Long idColuna, Long idPapel, Date dataInicial, Date dataFinal) throws GGASException {

		Long quantidade = 0L;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(alcada) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" inner join alcada.menu menu ");
		hql.append(" inner join alcada.papel papel ");
		hql.append(" inner join alcada.coluna coluna ");
		hql.append(" where papel.chavePrimaria = :idPapel ");
		hql.append(" and menu.chavePrimaria = :idMenu ");
		hql.append(" and coluna.chavePrimaria = :idColuna ");
		hql.append(" and alcada.dataInicial = :dataInicial ");
		hql.append(" and alcada.dataFinal = :dataFinal ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_PAPEL, idPapel);
		query.setLong(ID_MENU, idMenu);
		query.setLong("idColuna", idColuna);
		query.setDate("dataInicial", dataInicial);
		query.setDate("dataFinal", dataFinal);

		quantidade = (Long) query.uniqueResult();

		if (quantidade > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ALCADA_JA_EXISTENTE, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #obterListaAlcadasPorMenu(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AlcadaVO> obterListaAlcadasPorMenu(Long idMenu) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select new br.com.ggas.controleacesso.AlcadaVO ( ");
		hql.append(" papel.descricao ");
		hql.append(" , min(alcada.dataInicial) ");
		hql.append(" , min(alcada.dataFinal) ");
		hql.append(" ) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" inner join alcada.menu menu ");
		hql.append(" inner join alcada.papel papel ");
		hql.append(" where ");
		hql.append(" menu.chavePrimaria = :idMenu ");
		hql.append(" group by papel.descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_MENU, idMenu);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #obterListaAlcadasVigentes(java.lang.Long,
	 * java.util.Collection, java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Alcada> obterListaAlcadasVigentes(Long chaveTabela, Collection<Papel> papeis, Date dataAtual) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" alcada ");
		hql.append(" where ");
		hql.append(" alcada.coluna.tabela.chavePrimaria = :chaveTabela ");
		hql.append(" and alcada.papel.chavePrimaria in ( :chavesPapeis ) ");
		hql.append(" and :dataAtual between alcada.dataInicial and alcada.dataFinal ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveTabela", chaveTabela);
		query.setParameterList("chavesPapeis", Util.collectionParaArrayChavesPrimarias(papeis));
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		return query.list();
	}

	// Obtendo todos os registros pendentes por Usuario
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAlcada#obterRegistrosPendentesPorUsuario(br.com.ggas.controleacesso.Usuario)
	 */
	// usuário informado.
	@Override
	public AutorizacoesPendentesVO obterRegistrosPendentesPorUsuario(Usuario usuario, boolean isQtdeMinimaPendente) throws NegocioException {

		AutorizacoesPendentesVO pendentes = new AutorizacoesPendentesVO();

		pendentes.setCreditoDebitos(obterCreditosDebitosPendentesPorUsuario(usuario));
		pendentes.setParcelamentos(obterParcelamentoPendentesPorUsuario(usuario));
		pendentes.setTarifa(obterTarifasPendentesPorUsuario(usuario));
		pendentes.setSupervisorioMedicoesDiaria(obterSupervisorioMedicaoDiariaPendentesPorUsuario(usuario, isQtdeMinimaPendente));
		pendentes.setSupervisorioMedicoesHoraria(obterSupervisorioMedicaoHorariaPendentesPorUsuario(usuario, isQtdeMinimaPendente));
		pendentes.setSolicitacaoConsumoPontoConsumo(obterSolicitacaoConsumoPontoConsumoPendentes(usuario));
		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * obterSupervisorioMedicoesDiariaPendentesPorUsuario(br.
	 * com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario
			(Usuario usuario, boolean isQtdeMinimaPendente) throws NegocioException {

		Collection<SupervisorioMedicaoHoraria> pendentes = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("codigoStatusAlcada", Long.valueOf(paramStatusPendente));
		filtro.put("isQtdeMinimaPendente", isQtdeMinimaPendente);

		pendentes = controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtro, null, STATUS, "ultimoUsuarioAlteracao");

		obterSupervisorioMedicaoHorariaPendentesPorUsuario(usuario, pendentes);

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAlcada#obterSupervisorioMedicaoHorariaPendentesPorUsuario(java.util.Map,
	 * br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario(Map<String, Object> filtro,
					Usuario usuario) throws NegocioException {

		Collection<SupervisorioMedicaoHoraria> pendentes = null;

		ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

		filtro.put("codigoStatusAlcada", filtro.get(STATUS));

		pendentes = controladorSupervisorio.consultarSupervisorioMedicaoHoraria(filtro, null, STATUS, "ultimoUsuarioAlteracao");

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * obterSupervisorioMedicoesDiariaPendentesPorUsuario(br.
	 * com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario
			(Usuario usuario, boolean isQtdeMinimaPendente) throws NegocioException {

		Collection<SupervisorioMedicaoDiaria> pendentes = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("codigoStatusAlcada", Long.valueOf(paramStatusPendente));
		filtro.put("isQtdeMinimaPendente", isQtdeMinimaPendente);

		pendentes = controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, null, STATUS, "ultimoUsuarioAlteracao");

		obterSupervisorioMedicaoDiariaPendentesPorUsuario(usuario, pendentes);

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAlcada#obterSupervisorioMedicaoDiariaPendentesPorUsuario(java.util.Map,
	 * br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario(Map<String, Object> filtro,
					Usuario usuario) throws NegocioException {

		ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);
		Collection<SupervisorioMedicaoDiaria> pendentes = null;

		pendentes = controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, null, STATUS, "ultimoUsuarioAlteracao");

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * obterCreditosDebitosPendentesPorUsuario(br.
	 * com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<CreditoDebitoNegociado> obterCreditosDebitosPendentesPorUsuario(Usuario usuario) throws NegocioException {

		Collection<CreditoDebitoNegociado> pendentes = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorCreditoDebito controladorCreditoDebito = (ControladorCreditoDebito) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(STATUS, Long.valueOf(paramStatusPendente));

		pendentes = controladorCreditoDebito.consultarCreditoDebitoNegociado(filtro);

		obterCreditosDebitosPendentesPorUsuario(usuario, pendentes);

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * possuiAlcadaAutorizacaoCreditosDebitos(br.com
	 * .ggas.controleacesso.Usuario,
	 * br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoNegociado)
	 */
	@Override
	public boolean possuiAlcadaAutorizacaoCreditosDebitos(Usuario usuario, CreditoDebitoNegociado pendente) throws NegocioException {

		Collection<CreditoDebitoNegociado> pendentes = new ArrayList<>();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		if (pendente != null && pendente.getStatus() != null
						&& pendente.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
			pendentes.add(pendente);
			obterCreditosDebitosPendentesPorUsuario(usuario, pendentes);
		}

		return !pendentes.isEmpty();
	}

	/**
	 * Obter creditos debitos pendentes por usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendentes
	 *            the pendentes
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<CreditoDebitoNegociado> obterCreditosDebitosPendentesPorUsuario(Usuario usuario,
					Collection<CreditoDebitoNegociado> pendentes) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Collection<Papel> papeisUsuario = controladorPapel.consultarPapeisPorUsuario(usuario.getChavePrimaria());

		String chaveTabelaCredDeb = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_CREDITO_DEBITO_NEGOCIADO);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idTabela", Long.valueOf(chaveTabelaCredDeb));

		Class<?> interfacePendente = Util.buscarInterface(CreditoDebitoNegociado.class);
		for (Iterator<CreditoDebitoNegociado> iterator = pendentes.iterator(); iterator.hasNext();) {
			CreditoDebitoNegociado pendente = iterator.next();
			Collection<Papel> papeisAutorizados = new ArrayList<>();

			for (Papel papel : papeisUsuario) {
				filtro.put(ID_PAPEL, papel.getChavePrimaria());
				Collection<Alcada> alcadasCeditoDebito = this.consultarAlcada(filtro);

				for (Alcada alcada : alcadasCeditoDebito) {
					try {
						String nomeMetodo = Util.montarNomeGet(alcada.getColuna().getNomePropriedade());
						Object valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(pendente);

						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());

							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)
											&& (pendente.getUltimaAlteracao().after(alcada.getDataInicial()) && pendente
															.getUltimaAlteracao().before(alcada.getDataFinal()))) {

								papeisAutorizados.add(papel);
							}
						}
					} catch (Exception e) {
						LOG.error(e);
					}
				}
			}

			// Se o usuário tem papel para
			// autorização desse
			// CreditoDebitoNegociado pendente.
			if (!papeisAutorizados.isEmpty()) {
				// Se esse CreditoDebitoNegociado
				// pendente tem usuário da última
				// alteração.
				if (pendente.getUltimoUsuarioAlteracao() != null && pendente.getUltimoUsuarioAlteracao().equals(usuario)) {
					Collection<Usuario> usuarios = controladorAcesso.consultarUsuarioDosPapeis(papeisAutorizados);
					// Se existe outro usuário que
					// possa aprovar essa
					// pendência, não deixar o
					// usuário que a alterou
					// também autorizá-la.
					if (usuarios.size() > 1) {
						iterator.remove();
					}
				}
			} else {
				// Caso contrário, remove a pendência
				// da lista de retorno.
				iterator.remove();
			}
		}

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAlcada #
	 * obterParcelamentoPendentesPorUsuario(br.com .ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<Parcelamento> obterParcelamentoPendentesPorUsuario(Usuario usuario) throws NegocioException {

		Collection<Parcelamento> pendentes = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorParcelamento controladorParcelamento = (ControladorParcelamento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorParcelamento.BEAN_ID_CONTROLADOR_PARCELAMENTO);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);
		pendentes = controladorParcelamento.consultarParcelamentoCliente(null, null, null, null, Long.valueOf(paramStatusPendente), null);

		obterParcelamentoPendentesPorUsuario(usuario, pendentes);

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * possuiAlcadaAutorizacaoParcelamento(br.com.
	 * ggas.controleacesso.Usuario,
	 * br.com.ggas.cobranca
	 * .parcelamento.Parcelamento)
	 */
	@Override
	public boolean possuiAlcadaAutorizacaoParcelamento(Usuario usuario, Parcelamento pendente) throws NegocioException {

		Collection<Parcelamento> pendentes = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		if (pendente != null && pendente.getStatus() != null
						&& pendente.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
			pendentes = new ArrayList<>();
			pendentes.add(pendente);
			obterParcelamentoPendentesPorUsuario(usuario, pendentes);
		}

		boolean possuiAlcadaAutorizacao = false;
		if (pendentes != null && !pendentes.isEmpty()) {
			possuiAlcadaAutorizacao = true;
		}
		return possuiAlcadaAutorizacao;
	}

	/**
	 * Obter parcelamento pendentes por usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendentes
	 *            the pendentes
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<Parcelamento> obterParcelamentoPendentesPorUsuario(Usuario usuario, Collection<Parcelamento> pendentes)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Collection<Papel> papeisUsuario = controladorPapel.consultarPapeisPorUsuario(usuario.getChavePrimaria());

		String chaveTabelaParcelamento = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_PARCELAMENTO);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idTabela", Long.valueOf(chaveTabelaParcelamento));

		Class<?> interfacePendente = Util.buscarInterface(Parcelamento.class);
		for (Iterator<Parcelamento> iterator = pendentes.iterator(); iterator.hasNext();) {
			Parcelamento pendente = iterator.next();
			Collection<Papel> papeisAutorizados = new ArrayList<>();

			for (Papel papel : papeisUsuario) {
				filtro.put(ID_PAPEL, papel.getChavePrimaria());
				Collection<Alcada> alcadasParcelamento = this.consultarAlcada(filtro);

				for (Alcada alcada : alcadasParcelamento) {
					try {
						String nomeMetodo = Util.montarNomeGet(alcada.getColuna().getNomePropriedade());
						Object valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(pendente);

						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());

							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)
											&& (pendente.getUltimaAlteracao().after(alcada.getDataInicial()) && pendente
															.getUltimaAlteracao().before(alcada.getDataFinal()))) {

								papeisAutorizados.add(papel);
							}
						}
					} catch (Exception e) {
						LOG.error(e);
					}
				}
			}

			// Se o usuário tem papel para
			// autorização desse Parcelamento
			// pendente.
			if (!papeisAutorizados.isEmpty()) {
				// Se esse Parcelamento pendente
				// tem usuário da última
				// alteração.
				if (pendente.getUltimoUsuarioAlteracao() != null && pendente.getUltimoUsuarioAlteracao().equals(usuario)) {
					Collection<Usuario> usuarios = controladorAcesso.consultarUsuarioDosPapeis(papeisAutorizados);
					// Se existe outro usuário que
					// possa aprovar essa
					// pendência, não deixar o
					// usuário que a alterou
					// também autorizá-la.
					if (usuarios.size() > 1) {
						iterator.remove();
					}
				}
			} else {
				// Caso contrário, remove a pendência
				// da lista de retorno.
				iterator.remove();
			}
		}

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * obterTarifasPendentesPorUsuario(br.com.ggas
	 * .controleacesso.Usuario)
	 */
	@Override
	public Collection<Tarifa> obterTarifasPendentesPorUsuario(Usuario usuario) throws NegocioException {

		Collection<TarifaVigencia> pendentes = null;
		Collection<Tarifa> retorno = new ArrayList<>();

		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		// Verifica se o usuário pode autorizar
		// tarífas e se ele é o único nessa
		// condição
		Collection<Usuario> usuariosAutorizados = controladorAcesso.consultarUsuarioDosPapeis(controladorAcesso
						.consultarPapeisAutorizadosTarifa());
		boolean isAutorizador = usuariosAutorizados != null && usuariosAutorizados.contains(usuario);
		boolean isUnicoAutorizador = isAutorizador && usuariosAutorizados.size() == 1;

		if (isAutorizador) {
			ControladorTarifa controladorTarifa = (ControladorTarifa) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorTarifa.BEAN_ID_CONTROLADOR_TARIFA);
			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			Map<String, Object> filtro = new HashMap<>();
			filtro.put("habilitado", Boolean.TRUE);
			filtro.put(STATUS,
							Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE)));

			pendentes = controladorTarifa.consultarTarifasVigencia(filtro);
			if (pendentes != null) {
				for (Iterator<TarifaVigencia> iterator = pendentes.iterator(); iterator.hasNext();) {
					TarifaVigencia tarifaVigencia = iterator.next();
					if (!(tarifaVigencia.getUltimoUsuarioAlteracao() != null && 
									tarifaVigencia.getUltimoUsuarioAlteracao().equals(usuario) && !isUnicoAutorizador)) {
						retorno.add(tarifaVigencia.getTarifa());
					}
				}
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #possuiAlcadaAutorizacaoTarifa(br.com.ggas.
	 * controleacesso.Usuario,
	 * br.com.ggas.faturamento.tarifa.Tarifa)
	 */
	@Override
	public boolean possuiAlcadaAutorizacaoTarifaVigencia(Usuario usuario, TarifaVigencia pendente) throws NegocioException {

		boolean possuiAlcadaAutorizacao = false;

		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// Verifica se o usuário pode autorizar
		// tarífas e se ele é o único nessa
		// condição
		Collection<Usuario> usuariosAutorizados = controladorAcesso.consultarUsuarioDosPapeis(controladorAcesso
						.consultarPapeisAutorizadosTarifa());
		boolean isAutorizador = usuariosAutorizados != null && usuariosAutorizados.contains(usuario);
		boolean isUnicoAutorizador = isAutorizador && usuariosAutorizados.size() == 1;

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		if (pendente != null && pendente.getStatus() != null
						&& pendente.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)
						&& (isAutorizador && !usuario.equals(pendente.getUltimoUsuarioAlteracao()))
						|| (isUnicoAutorizador && (pendente != null &&  usuario.equals(pendente.getUltimoUsuarioAlteracao())))) {
			// Se o usuário for autorizador e a
			// tarifa foi alterada por outro ou se
			// ele for o único que puder
			// autorizá-la.
			possuiAlcadaAutorizacao = true;
		}

		return possuiAlcadaAutorizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * possuiAlcadaAutorizacaoCreditosDebitos(br.com
	 * .ggas.controleacesso.Usuario,
	 * br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoNegociado)
	 */
	@Override
	public boolean possuiAlcadaAutorizacaoSupervisorioMedicaoDiaria(Usuario usuario, SupervisorioMedicaoDiaria pendente)
					throws NegocioException {

		Collection<SupervisorioMedicaoDiaria> pendentes = new ArrayList<>();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		if (pendente != null && pendente.getStatus() != null
						&& pendente.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
			pendentes.add(pendente);
			obterSupervisorioMedicaoDiariaPendentesPorUsuario(usuario, pendentes);
		}

		return !pendentes.isEmpty();
	}

	/**
	 * Obter supervisorio medicao diaria pendentes por usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendentes
	 *            the pendentes
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<SupervisorioMedicaoDiaria> obterSupervisorioMedicaoDiariaPendentesPorUsuario(Usuario usuario,
					Collection<SupervisorioMedicaoDiaria> pendentes) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Collection<Papel> papeisUsuario = controladorPapel.consultarPapeisPorUsuario(usuario.getChavePrimaria());

		// FIXME: Substituir por consulta à tabela pela classe
		String chaveTabelaSupervisorioMedicaoDiaria = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_DIARIA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idTabela", Long.valueOf(chaveTabelaSupervisorioMedicaoDiaria));

		Class<?> interfacePendente = Util.buscarInterface(SupervisorioMedicaoDiaria.class);
		for (Iterator<SupervisorioMedicaoDiaria> iterator = pendentes.iterator(); iterator.hasNext();) {
			SupervisorioMedicaoDiaria pendente = iterator.next();
			Collection<Papel> papeisAutorizados = new ArrayList<>();

			for (Papel papel : papeisUsuario) {
				filtro.put(ID_PAPEL, papel.getChavePrimaria());
				Collection<Alcada> alcadasSupervisorioMedicaoDiaria = this.consultarAlcada(filtro);

				for (Alcada alcada : alcadasSupervisorioMedicaoDiaria) {
					try {
						String nomeMetodo = Util.montarNomeGet(alcada.getColuna().getNomePropriedade());
						Object valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(pendente);

						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());

							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)
											&& (pendente.getUltimaAlteracao() == null
															|| pendente.getUltimaAlteracao().after(alcada.getDataInicial())
															&& pendente.getUltimaAlteracao() == null || pendente.getUltimaAlteracao()
															.before(alcada.getDataFinal()))) {

								papeisAutorizados.add(papel);
							}
						}
					} catch (Exception e) {
						LOG.error(e);
					}
				}
			}

			// Se o usuário tem papel para
			// autorização desse
			// CreditoDebitoNegociado pendente.
			if (!papeisAutorizados.isEmpty()) {
				// Se esse CreditoDebitoNegociado
				// pendente tem usuário da última
				// alteração.
				if (pendente.getUltimoUsuarioAlteracao() != null
								&& pendente.getUltimoUsuarioAlteracao().getChavePrimaria() == (usuario.getChavePrimaria())) {
					Collection<Usuario> usuarios = controladorAcesso.consultarUsuarioDosPapeis(papeisAutorizados);
					// Se existe outro usuário que
					// possa aprovar essa
					// pendência, não deixar o
					// usuário que a alterou
					// também autorizá-la.
					if (usuarios.size() > 1) {
						iterator.remove();
					}
				}
			} else {
				// Caso contrário, remove a pendência
				// da lista de retorno.
				iterator.remove();
			}
		}

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorAlcada
	 * #
	 * possuiAlcadaAutorizacaoCreditosDebitos(br.com
	 * .ggas.controleacesso.Usuario,
	 * br.com.ggas.faturamento
	 * .creditodebito.CreditoDebitoNegociado)
	 */
	@Override
	public boolean possuiAlcadaAutorizacaoSupervisorioMedicaoHoraria(Usuario usuario, SupervisorioMedicaoHoraria pendente)
					throws NegocioException {

		Collection<SupervisorioMedicaoHoraria> pendentes = new ArrayList<>();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String paramStatusPendente = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALCADA_PENDENTE);

		if (pendente != null && pendente.getStatus() != null
						&& pendente.getStatus().getChavePrimaria() == Long.parseLong(paramStatusPendente)) {
			pendentes.add(pendente);
			obterSupervisorioMedicaoHorariaPendentesPorUsuario(usuario, pendentes);
		}

		return !pendentes.isEmpty();
	}

	/**
	 * Obter supervisorio medicao horaria pendentes por usuario.
	 * 
	 * @param usuario
	 *            the usuario
	 * @param pendentes
	 *            the pendentes
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<SupervisorioMedicaoHoraria> obterSupervisorioMedicaoHorariaPendentesPorUsuario(Usuario usuario,
					Collection<SupervisorioMedicaoHoraria> pendentes) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorPapel controladorPapel = (ControladorPapel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPapel.BEAN_ID_CONTROLADOR_PAPEL);
		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Collection<Papel> papeisUsuario = controladorPapel.consultarPapeisPorUsuario(usuario.getChavePrimaria());

		String chaveTabelaSupervisorioMedicaoHoraria = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_TABELA_SUPERVISORIO_MEDICAO_HORARIA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idTabela", Long.valueOf(chaveTabelaSupervisorioMedicaoHoraria));

		Class<?> interfacePendente = Util.buscarInterface(SupervisorioMedicaoHoraria.class);
		for (Iterator<SupervisorioMedicaoHoraria> iterator = pendentes.iterator(); iterator.hasNext();) {
			SupervisorioMedicaoHoraria pendente = iterator.next();
			Collection<Papel> papeisAutorizados = new ArrayList<>();

			for (Papel papel : papeisUsuario) {
				filtro.put(ID_PAPEL, papel.getChavePrimaria());
				Collection<Alcada> alcadasSupervisorioMedicaoHoraria = this.consultarAlcada(filtro);

				for (Alcada alcada : alcadasSupervisorioMedicaoHoraria) {
					try {
						String nomeMetodo = Util.montarNomeGet(alcada.getColuna().getNomePropriedade());
						Object valorMetodoObj = interfacePendente.getDeclaredMethod(nomeMetodo).invoke(pendente);

						if (valorMetodoObj != null) {
							BigDecimal valorMetodo = new BigDecimal(valorMetodoObj.toString());

							if ((alcada.getValorInicial() != null || alcada.getValorFinal() != null)
											&& (alcada.getValorFinal() == null || alcada.getValorFinal().compareTo(valorMetodo) >= 0)
											&& (alcada.getValorInicial() == null || alcada.getValorInicial().compareTo(valorMetodo) <= 0)
											&& (pendente.getUltimaAlteracao() == null
															|| pendente.getUltimaAlteracao().after(alcada.getDataInicial())
															&& pendente.getUltimaAlteracao() == null || pendente.getUltimaAlteracao()
															.before(alcada.getDataFinal()))) {

								papeisAutorizados.add(papel);
							}
						}
					} catch (Exception e) {
						LOG.error(e);
					}
				}
			}

			// Se o usuário tem papel para
			// autorização desse
			// CreditoDebitoNegociado pendente.
			if (!papeisAutorizados.isEmpty()) {
				// Se esse CreditoDebitoNegociado
				// pendente tem usuário da última
				// alteração.
				if (pendente.getUltimoUsuarioAlteracao() != null && pendente.getUltimoUsuarioAlteracao().equals(usuario)) {
					Collection<Usuario> usuarios = controladorAcesso.consultarUsuarioDosPapeis(papeisAutorizados);
					// Se existe outro usuário que
					// possa aprovar essa
					// pendência, não deixar o
					// usuário que a alterou
					// também autorizá-la.
					if (usuarios.size() > 1) {
						iterator.remove();
					}
				}
			} else {
				// Caso contrário, remove a pendência
				// da lista de retorno.
				iterator.remove();
			}
		}

		return pendentes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorAlcada#obterSolicitacaoConsumoPontoConsumoPendentes(br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public Collection<SolicitacaoConsumoPontoConsumo> obterSolicitacaoConsumoPontoConsumoPendentes(Usuario usuario) throws NegocioException {

		Collection<SolicitacaoConsumoPontoConsumo> pendentes = new ArrayList<>();

		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Collection<Usuario> usuariosAutorizados = controladorAcesso.consultarUsuarioDosPapeis(controladorAcesso
						.consultarPapeisAutorizadosProgramacaoConsumo());
		boolean isAutorizador = usuariosAutorizados != null && usuariosAutorizados.contains(usuario);

		if (isAutorizador) {
			ControladorProgramacao controladorProgramacao = (ControladorProgramacao) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorProgramacao.BEAN_ID_CONTROLADOR_PROGRAMACAO);

			pendentes = controladorProgramacao.listarSolicitacaoConsumoPendentes();

		}

		return pendentes;
	}

}
