
package br.com.ggas.controleacesso.impl;
/**
 * Classe responsável pela representação da entidade Modulo 
 *
 */
public class ModuloVO {

	private Long id;

	private String descricao;

	private FuncionalidadeVO[] listaFuncionalidade;

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public FuncionalidadeVO[] getListaFuncionalidade() {
		FuncionalidadeVO[] retorno = null;
		if (this.listaFuncionalidade != null) {
			retorno = this.listaFuncionalidade.clone();
		}
		return retorno;
	}

	public void setListaFuncionalidade(FuncionalidadeVO[] listaFuncionalidade) {
		if (listaFuncionalidade != null) {
			this.listaFuncionalidade = listaFuncionalidade.clone();
		} else {
			this.listaFuncionalidade = null;
		}
	}

}
