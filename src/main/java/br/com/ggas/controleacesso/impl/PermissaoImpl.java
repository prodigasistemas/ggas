/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela permissão.
 *
 */
public class PermissaoImpl extends EntidadeNegocioImpl implements Permissao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 3339388819588255596L;

	private Menu menu;

	private Operacao operacao;

	private int tipo;

	private Papel papel;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Permissao
	 * #getModulo()
	 */
	@Override
	public Menu getMenu() {

		return menu;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Permissao
	 * #setModulo
	 * (br.com.ggas.controleacesso.impl.Modulo)
	 */
	@Override
	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Permissao
	 * #getOperacao()
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Permissao
	 * #setOperacao
	 * (br.com.ggas.controleacesso.impl.Operacao)
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Permissao
	 * #getTipo()
	 */
	@Override
	public int getTipo() {

		return tipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Permissao
	 * #setTipo(int)
	 */
	@Override
	public void setTipo(int tipo) {

		this.tipo = tipo;
	}

	@Override
	public Papel getPapel() {

		return papel;
	}

	@Override
	public void setPapel(Papel papel) {

		this.papel = papel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.menu == null) {
			stringBuilder.append(Modulo.ENTIDADE_ROTULO_MODULO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.operacao == null) {
			stringBuilder.append(Operacao.ENTIDADE_ROTULO_OPERACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

}
