
package br.com.ggas.controleacesso.impl;
/**
 * Classe responsável pela representação da entidade Funcionalidade 
 *
 */
public class FuncionalidadeVO {

	private Long id;

	private String descricao;

	private FuncionalidadeVO[] subFuncionalidades;

	private OperacaoItemVO[] operacoes;

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public OperacaoItemVO[] getOperacoes() {
		OperacaoItemVO[] retorno = null;
		if (this.operacoes != null) {
			retorno = this.operacoes.clone();
		}
		return retorno;
	}

	public void setOperacoes(OperacaoItemVO[] operacoes) {
		if (operacoes != null) {
			this.operacoes = operacoes.clone();
		} else {
			this.operacoes = null;
		}
	}

	public FuncionalidadeVO[] getSubFuncionalidades() {
		FuncionalidadeVO[] retorno = null;
		if(this.subFuncionalidades != null) {
			retorno = subFuncionalidades.clone();
		}
		return retorno;
	}

	public void setSubFuncionalidades(FuncionalidadeVO[] subFuncionalidades) {
		if (subFuncionalidades != null) {
			this.subFuncionalidades = subFuncionalidades.clone();
		} else {
			this.subFuncionalidades = null;
		}
	}

}
