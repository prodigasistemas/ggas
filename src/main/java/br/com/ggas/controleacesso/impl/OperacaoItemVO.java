
package br.com.ggas.controleacesso.impl;
/**
 * Classe responsável pela representação da entidade OperacaoItem 
 *
 */
public class OperacaoItemVO {

	private Long idMenu;

	private Long idOperacaoSistema;

	private Long idPapel;

	private String descricaoOperacao;

	private Boolean isSelecionado;

	public Long getIdMenu() {

		return idMenu;
	}

	public void setIdMenu(Long idMenu) {

		this.idMenu = idMenu;
	}

	public Long getIdOperacaoSistema() {

		return idOperacaoSistema;
	}

	public void setIdOperacaoSistema(Long idOperacaoSistema) {

		this.idOperacaoSistema = idOperacaoSistema;
	}

	public Long getIdPapel() {

		return idPapel;
	}

	public void setIdPapel(Long idPapel) {

		this.idPapel = idPapel;
	}

	public String getDescricaoOperacao() {

		return descricaoOperacao;
	}

	public void setDescricaoOperacao(String descricaoOperacao) {

		this.descricaoOperacao = descricaoOperacao;
	}

	public Boolean getIsSelecionado() {

		return isSelecionado;
	}

	public void setIsSelecionado(Boolean isSelecionado) {

		this.isSelecionado = isSelecionado;
	}

}
